﻿'--------------------------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_tito_payment_report_sel.vb
'
'   DESCRIPTION :  Payment report
'
'        AUTHOR :  Rodrigo Gonzalez Rodriguez
'
' CREATION DATE :  21-OCT-2015
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 21-OCT-2015  RGR    Initial version
' 03-DIC-2015  RGR    Task 6719:Report payments first review changes
' 23-DIC-2015  RGR    Bug 8001:Cash Payments Report - Error in Transaction Type filter
' 28-DIC-2015  JPJ    Bug 8001:Cash Payments Report - Error in Transaction Type filter
' 28-DIC-2015  RGR    Bug 8001:Cash Payments Report - Error in Transaction Type filter
' 05-JAN-2016  JPJ    Bug 7956:Cash Payments Report - Chips purchase don't show up
' 05-APR-2016  EOR    Bug 11276:Cash Payments Report - Add Canceled Handpays
' 19-JUL-2016  FOS    Product Backlog Item 15068:Tables (Phase 4): Adapt reports for the multi currency chips
' 16-SEP-2016  OVS    Bug 16753:CountR: Falta añadir en los filtros "Usuario que paga/autoriza" ->"SYS-Redemption"
' 21-FEB-2018  RGR    Bug 31635: WIGOS-3785 Incorrect warning message when Several is selected in selection criterion
' 22-FEB-2018  RGR    Bug 31696: WIGOS-3820 Wrong behaviour in Transaction type selection criterion - "Reporte de pagos de caja"
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common.TITO
Imports WSI.Common
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class frm_tito_payment_report_sel
  Inherits GUI_Controls.frm_base_sel

#Region "Enums"

  Public Enum ENUM_HP_JACKPOT_TRANSLATE
    HP_JACKPOT = 0
    HP_JACKPOT_PROGRESSIVE = 10000
    HP_JACKPOT_MAJOR_PRIZE = 10001
  End Enum

  Public Enum NODE_TYPE
    FOLDER = 1
    FILE = 2
    FILE_INIT = 3
  End Enum

  Public Enum FILTER_ENUMS
    HANDPAY_TYPE = 1
    CASHIER_MOVEMENT = 2
  End Enum

#End Region

#Region "Structures"

  Private Structure TREE_FILTER
    Dim id As Integer
    Dim node_type As NODE_TYPE
    Dim filter_type As FILTER_ENUMS
    Dim nivel As Integer
    Dim filter_description As String
    Dim grid_description As String
    Dim codes As List(Of String)
    Dim id_father As Integer?
    Dim index_collapse As Integer
  End Structure

  Private Structure FRAGMENT
    Dim min As Integer
    Dim max As Integer
  End Structure

  Private Structure SELECT_FILTER
    Dim codes As String
    Dim descriptions As String
  End Structure

  Private Structure SELECT_GRID
    Dim codes_cm As String
    Dim codes_hp As String
    Dim descriptions As String
  End Structure

#End Region

#Region "Members"
  Private m_data_types As List(Of TREE_FILTER)
  Private m_from_date As DateTime?
  Private m_to_date As DateTime?
  Private m_cashiers As SELECT_FILTER
  Private m_type_movements As SELECT_GRID
  Private m_user_pay As SELECT_FILTER
  Private m_user_allow As SELECT_FILTER
  Private m_tax_transaction As String
  Private m_list_handpay_machine As List(Of String)
  Private m_last_hp_id As Int64
  Private m_tax_view As Integer
  Private m_is_all_types As Boolean

#End Region

#Region "Constructors"

  Public Sub New()
    InitializeComponent()
    m_data_types = Me.GetDataFilterGrid()

    m_cashiers = New SELECT_FILTER()

    m_type_movements = New SELECT_GRID()

    m_user_allow = New SELECT_FILTER()

    m_user_pay = New SELECT_FILTER()

    m_list_handpay_machine = New List(Of String)()
    m_list_handpay_machine.Add(Convert.ToInt32(HANDPAY_TYPE.CANCELLED_CREDITS))
    m_list_handpay_machine.Add(Convert.ToInt32(HANDPAY_TYPE.JACKPOT))
    m_list_handpay_machine.Add(Convert.ToInt32(HANDPAY_TYPE.ORPHAN_CREDITS))
    m_list_handpay_machine.Add(Convert.ToInt32(HANDPAY_TYPE.SPECIAL_PROGRESSIVE))
    m_last_hp_id = -1
  End Sub

#End Region

#Region "Constants"

#Region "Central Grid"
  Private Const GRID_COLUMNS As Integer = 12
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_CASHIER As Integer = 2
  Private Const GRID_COLUMN_TYPE As Integer = 3
  Private Const GRID_COLUMN_USER_PAY As Integer = 4
  Private Const GRID_COLUMN_USER_ALLOW As Integer = 5
  Private Const GRID_COLUMN_GROSS_AMOUNT_EXCHANGE_CURRENCY As Integer = 6
  Private Const GRID_COLUMN_GROSS_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_TAX_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_NET_AMOUNT As Integer = 9
  Private Const GRID_COLUMN_REASON As Integer = 10
  Private Const GRID_COLUMN_COMMENT As Integer = 11

  Private Const SQL_COLUMN_SELECT As Integer = 0
  Private Const SQL_COLUMN_DATE As Integer = 1
  Private Const SQL_COLUMN_CASHIER As Integer = 2
  Private Const SQL_COLUMN_TYPE As Integer = 3
  Private Const SQL_COLUMN_USER_PAY As Integer = 4
  Private Const SQL_COLUMN_USER_ALLOW As Integer = 5
  Private Const SQL_COLUMN_NET_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_GROSS_AMOUNT As Integer = 7
  Private Const SQL_COLUMN_REASON As Integer = 8
  Private Const SQL_COLUMN_COMMENT As Integer = 9
  Private Const SQL_COLUMN_SOURCE As Integer = 10
  Private Const SQL_COLUMN_TYPE_2 As Integer = 11
  Private Const SQL_COLUMN_GROSS_AMOUNT_EXCHANGE_CURRENCY As Integer = 12
  Private Const SQL_COLUMN_ISO_CODE As Integer = 13

  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 2100
  Private Const GRID_WIDTH_CASHIER As Integer = 2000
  Private Const GRID_WIDTH_TYPE As Integer = 4000
  Private Const GRID_WIDTH_USER_PAY As Integer = 2500
  Private Const GRID_WIDTH_USER_ALLOW As Integer = 2500
  Private Const GRID_WIDTH_BEFORE_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_EXCHANGE_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_AFTER_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_REASON As Integer = 3000
  Private Const GRID_WIDTH_COMMENT As Integer = 5000
#End Region

#Region "Type Movement Grid "

  Private Const GRID_2_COLUMNS As Integer = 9
  Private Const GRID_2_HEADER_ROWS As Integer = 0
  Private Const GRID_2_TAB As String = "    "
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_GROUP_CODE As Integer = 3
  Private Const GRID_2_COLUMN_NIVEL As Integer = 4
  Private Const GRID_2_COLUMN_ID As Integer = 5
  Private Const GRID_2_COLUMN_ID_FATHER As Integer = 6
  Private Const GRID_2_COLUMN_NODE_TYPE As Integer = 7
  Private Const GRID_2_COLUMN_INDEX_COLLAPSE As Integer = 8

  Private Const EXPAND_SYMBOL As String = "[+] "
  Private Const COLLAPSE_SYMBOL As String = "[-] "
  Private Const ROW_HEIGHT As Integer = 240

#End Region

#End Region

#Region "Overrides"

  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6783)
    Me.cmb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)

    Me.gpb_Types.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6791)
    Me.opt_some.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    Me.cmb_usrallow.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6792)

    Me.cmb_usrpay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6793)

    Me.gpb_transaction.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794)

    Me.chk_with_tax.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7041)

    Me.chk_without_tax.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7042)

    Me.GUI_StyleSheet()

    Me.SetDefaultValues()

    Call SetComboCashierAlias(cmb_terminal.Combo, True, True)

    Call SetCombo(Me.cmb_usrpay.Combo, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " _
                & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " AND GU_USER_TYPE IN (" & WSI.Common.GU_USER_TYPE.USER & " , " & WSI.Common.GU_USER_TYPE.SYS_REDEMPTION & ") ORDER BY GU_USERNAME")

    Call SetCombo(Me.cmb_usrallow.Combo, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " _
                & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " AND GU_USER_TYPE IN (" & WSI.Common.GU_USER_TYPE.USER & " , " & WSI.Common.GU_USER_TYPE.SYS_REDEMPTION & ") ORDER BY GU_USERNAME")

  End Sub

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TITO_PAYMENT_REPORT_SEL
    MyBase.GUI_SetFormId()
  End Sub

  Protected Overrides Sub GUI_FilterReset()
    Me.SetDefaultValues()
  End Sub

  Protected Overrides Sub GUI_ButtonClick(ButtonId As frm_base.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Me.GUI_StyleSheet()
    End Select
    MyBase.GUI_ButtonClick(ButtonId)
  End Sub

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _select As SELECT_GRID

    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to.Focus()
        Return False
      End If
    End If

    If (Me.opt_some.Checked) Then
      _select = Me.GetFilterSelect()
      If String.IsNullOrEmpty(String.Concat(_select.codes_cm, _select.codes_hp)) Then
        Me.opt_some.Checked = False
        Me.opt_all.Checked = True

      End If
    End If

    Return True
  End Function

  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  Protected Overrides Sub GUI_ReportUpdateFilters()

    If Me.dtp_from.Checked Then
      Me.m_from_date = dtp_from.Value
    Else
      Me.m_from_date = Nothing
    End If

    If Me.dtp_to.Checked Then
      Me.m_to_date = Me.dtp_to.Value
    Else
      Me.m_to_date = Nothing
    End If

    If Me.cmb_terminal.IsOneChecked() Then
      m_cashiers.codes = Me.cmb_terminal.Combo.Value
      m_cashiers.descriptions = Me.cmb_terminal.Combo.TextValue
    Else
      m_cashiers.codes = Nothing
      m_cashiers.descriptions = Nothing
    End If

    Me.m_is_all_types = Me.opt_all.Checked

    m_type_movements = Me.GetFilterSelect()

    If Me.cmb_usrpay.IsOneChecked() Then
      m_user_pay.codes = Me.cmb_usrpay.Combo.Value
      m_user_pay.descriptions = Me.cmb_usrpay.Combo.TextValue
    Else
      m_user_pay.codes = Nothing
      m_user_pay.descriptions = Nothing
    End If

    If Me.cmb_usrallow.IsOneChecked() Then
      m_user_allow.codes = Me.cmb_usrallow.Combo.Value
      m_user_allow.descriptions = Me.cmb_usrallow.Combo.TextValue
    Else
      m_user_allow.codes = Nothing
      m_user_allow.descriptions = Nothing
    End If

    m_tax_transaction = String.Empty
    If Me.chk_with_tax.Checked Then
      m_tax_transaction = Me.chk_with_tax.Text
      m_tax_view = 1
    End If

    If Me.chk_without_tax.Checked Then
      m_tax_transaction = Me.chk_without_tax.Text
      m_tax_view = 2
    End If

    If (Me.chk_with_tax.Checked AndAlso Me.chk_without_tax.Checked) OrElse (Not Me.chk_with_tax.Checked AndAlso Not Me.chk_without_tax.Checked) Then
      m_tax_transaction = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
      m_tax_view = 3
    End If

  End Sub

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand = New SqlCommand()

    _sql_command.CommandText = "GetCashierTransactionsTaxes"
    _sql_command.CommandType = CommandType.StoredProcedure
    _sql_command.Parameters.Add("@pStartDatetime", SqlDbType.DateTime).Value = IIf(Me.m_from_date Is Nothing, DBNull.Value, Me.m_from_date)
    _sql_command.Parameters.Add("@pEndDatetime", SqlDbType.DateTime).Value = IIf(Me.m_to_date Is Nothing, DBNull.Value, Me.m_to_date)
    _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = IIf(Me.m_cashiers.codes Is Nothing, DBNull.Value, Me.m_cashiers.codes)
    _sql_command.Parameters.Add("@pCmMovements", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Me.m_type_movements.codes_cm), DBNull.Value, Me.m_type_movements.codes_cm)
    _sql_command.Parameters.Add("@pHpMovements", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(Me.m_type_movements.codes_hp), DBNull.Value, Me.m_type_movements.codes_hp)
    _sql_command.Parameters.Add("@pPaymentUserId", SqlDbType.Int).Value = IIf(Me.m_user_pay.codes Is Nothing, DBNull.Value, Me.m_user_pay.codes)
    _sql_command.Parameters.Add("@pAuthorizeUserId", SqlDbType.Int).Value = IIf(Me.m_user_allow.codes Is Nothing, DBNull.Value, Me.m_user_allow.codes)
    _sql_command.Parameters.Add("@pNationalIsoCode", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency()

    Return _sql_command
  End Function

  Public Overrides Function GUI_CheckOutRowBeforeAdd(DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _net As Decimal
    Dim _gross As Decimal

    If Me.m_tax_view = 3 Then
      Return True
    End If

    _net = 0
    _gross = 0

    If Not DbRow.IsNull(SQL_COLUMN_NET_AMOUNT) Then
      _net = DbRow.Value(SQL_COLUMN_NET_AMOUNT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_GROSS_AMOUNT) Then
      _gross = DbRow.Value(SQL_COLUMN_GROSS_AMOUNT)
    End If

    If Me.m_tax_view = 1 Then
      If (_net <> _gross) Then
        Return True
      End If
    End If

    If Me.m_tax_view = 2 Then
      If _net = _gross Then
        Return True
      End If
    End If

    Return False

  End Function

  Public Overrides Function GUI_SetupRow(RowIndex As Integer, DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _net_amount As Decimal
    Dim _gross_amount As Decimal
    Dim _gross_amount_exchange_currency As Decimal

    _net_amount = 0
    _gross_amount = 0
    _gross_amount_exchange_currency = 0

    With MyBase.Grid
      If Not DbRow.IsNull(SQL_COLUMN_DATE) Then
        .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CASHIER) Then
        .Cell(RowIndex, GRID_COLUMN_CASHIER).Value = DbRow.Value(SQL_COLUMN_CASHIER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_TYPE) Then
        .Cell(RowIndex, GRID_COLUMN_TYPE).Value = Me.GetMovementDescription(DbRow.Value(SQL_COLUMN_TYPE), DbRow.Value(SQL_COLUMN_SOURCE))

        'EOR 05-04-2016 If type is ManualHandpayCancellation add Undone
        If Not DbRow.IsNull(SQL_COLUMN_TYPE_2) And DbRow.Value(SQL_COLUMN_TYPE_2) = MovementType.ManualHandpayCancellation Then
          .Cell(RowIndex, GRID_COLUMN_TYPE).Value = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & .Cell(RowIndex, GRID_COLUMN_TYPE).Value
        End If
      End If

      If Not DbRow.IsNull(SQL_COLUMN_USER_PAY) Then
        .Cell(RowIndex, GRID_COLUMN_USER_PAY).Value = DbRow.Value(SQL_COLUMN_USER_PAY)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_USER_ALLOW) Then
        .Cell(RowIndex, GRID_COLUMN_USER_ALLOW).Value = DbRow.Value(SQL_COLUMN_USER_ALLOW)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_NET_AMOUNT) Then
        _net_amount = DbRow.Value(SQL_COLUMN_NET_AMOUNT)
        .Cell(RowIndex, GRID_COLUMN_NET_AMOUNT).Value = GUI_FormatCurrency(_net_amount) & " " & CurrencyExchange.GetNationalCurrency()
      End If

      If Not DbRow.IsNull(SQL_COLUMN_GROSS_AMOUNT_EXCHANGE_CURRENCY) Then
        _gross_amount_exchange_currency = DbRow.Value(SQL_COLUMN_GROSS_AMOUNT_EXCHANGE_CURRENCY)
        If (_gross_amount_exchange_currency > 0) Then
          .Cell(RowIndex, GRID_COLUMN_GROSS_AMOUNT_EXCHANGE_CURRENCY).Value = GUI_FormatCurrency(_gross_amount_exchange_currency) & " " & _
                                                                              DbRow.Value(SQL_COLUMN_ISO_CODE)
        End If
      End If

      If Not DbRow.IsNull(SQL_COLUMN_GROSS_AMOUNT) Then
        _gross_amount = DbRow.Value(SQL_COLUMN_GROSS_AMOUNT)
        .Cell(RowIndex, GRID_COLUMN_GROSS_AMOUNT).Value = GUI_FormatCurrency(_gross_amount) & " " & CurrencyExchange.GetNationalCurrency()
      End If

      .Cell(RowIndex, GRID_COLUMN_TAX_AMOUNT).Value = GUI_FormatCurrency(_gross_amount - _net_amount) & " " & CurrencyExchange.GetNationalCurrency()

      If Not DbRow.IsNull(SQL_COLUMN_REASON) Then
        .Cell(RowIndex, GRID_COLUMN_REASON).Value = DbRow.Value(SQL_COLUMN_REASON)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_COMMENT) Then
        .Cell(RowIndex, GRID_COLUMN_COMMENT).Value = DbRow.Value(SQL_COLUMN_COMMENT)
      End If
    End With

    Return True
  End Function

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    If Me.m_from_date Is Nothing Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), String.Empty)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(Me.m_from_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    End If

    If Me.m_to_date Is Nothing Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), String.Empty)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(Me.m_to_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    End If

    If String.IsNullOrEmpty(Me.m_cashiers.codes) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(492), Me.m_cashiers.descriptions)
    End If

    If m_is_all_types Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6791), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))

    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6791), Me.m_type_movements.descriptions)

    End If

    If String.IsNullOrEmpty(Me.m_user_pay.codes) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6793), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))

    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6793), Me.m_user_pay.descriptions)
    End If

    If String.IsNullOrEmpty(Me.m_user_allow.codes) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6792), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6792), Me.m_user_allow.descriptions)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794), Me.m_tax_transaction)

  End Sub

#End Region

#Region "Public Functions"
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    MyBase.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    MyBase.MdiParent = MdiParent
    MyBase.Display(False)
  End Sub
#End Region

#Region "Private Functions"

  Private Sub SetDefaultDate()
    Dim _now As Date
    Dim _init_date As Date
    Dim _closing_time As Integer

    _now = WGDB.Now
    _closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 8)
    _init_date = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _init_date > _now Then
      _init_date = _init_date.AddDays(-1)
    End If

    Me.dtp_from.Value = _init_date
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _init_date.AddDays(1)
    Me.dtp_to.Checked = False

  End Sub

  Private Sub SetDefaultValues()

    Me.SetDefaultDate()
    Me.cmb_terminal.SetDefaultValues()
    Me.cmb_usrallow.SetDefaultValues()
    Me.cmb_usrpay.SetDefaultValues()
    Me.opt_all.Checked = True
    Me.opt_some.Checked = False

    Me.chk_with_tax.Checked = True
    Me.chk_without_tax.Checked = True

    Me.GUI_StyleSheetMovement()

    Me.GUI_FillMovementCode()

  End Sub


  Private Sub GUI_StyleSheet()

    With Me.Grid
      .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Column(GRID_COLUMN_SELECT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False


      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_DATE, "", GRID_WIDTH_DATE, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER, GLB_NLS_GUI_INVOICING.GetString(201))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_CASHIER, "", GRID_WIDTH_CASHIER, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_INVOICING.GetString(209))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TYPE, "", GRID_WIDTH_TYPE, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6791))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_USER_PAY, "", GRID_WIDTH_USER_PAY, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6793))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_USER_ALLOW, "", GRID_WIDTH_USER_ALLOW, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6792))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_GROSS_AMOUNT_EXCHANGE_CURRENCY, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6796), GRID_WIDTH_EXCHANGE_AMOUNT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5668))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_GROSS_AMOUNT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6796), GRID_WIDTH_AFTER_AMOUNT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2072))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_TAX_AMOUNT, "", GRID_WIDTH_AFTER_AMOUNT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(371))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_NET_AMOUNT, "", GRID_WIDTH_BEFORE_AMOUNT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6795))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_REASON, "", GRID_WIDTH_REASON, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6784))

      Call GUI_AddColumn(Me.Grid, GRID_COLUMN_COMMENT, "", GRID_WIDTH_COMMENT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6785))

    End With

  End Sub

  Private Sub GUI_StyleSheetMovement()

    With Me.sgd_Types
      Me.sgd_Types.Clear()
      .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Sortable = False
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 4105
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_2_COLUMN_GROUP_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_GROUP_CODE).WidthFixed = 0
      .Column(GRID_2_COLUMN_GROUP_CODE).IsColumnPrintable = False

      .Column(GRID_2_COLUMN_NIVEL).Header.Text = ""
      .Column(GRID_2_COLUMN_NIVEL).WidthFixed = 0
      .Column(GRID_2_COLUMN_NIVEL).IsColumnPrintable = False

      .Column(GRID_2_COLUMN_ID).Header.Text = ""
      .Column(GRID_2_COLUMN_ID).WidthFixed = 0
      .Column(GRID_2_COLUMN_ID).IsColumnPrintable = False

      .Column(GRID_2_COLUMN_ID_FATHER).Header.Text = ""
      .Column(GRID_2_COLUMN_ID_FATHER).WidthFixed = 0
      .Column(GRID_2_COLUMN_ID_FATHER).IsColumnPrintable = False

      .Column(GRID_2_COLUMN_NODE_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_NODE_TYPE).WidthFixed = 0
      .Column(GRID_2_COLUMN_NODE_TYPE).IsColumnPrintable = False

      .Column(GRID_2_COLUMN_INDEX_COLLAPSE).Header.Text = ""
      .Column(GRID_2_COLUMN_INDEX_COLLAPSE).WidthFixed = 0
      .Column(GRID_2_COLUMN_INDEX_COLLAPSE).IsColumnPrintable = False

    End With

  End Sub

  Private Sub GUI_FillMovementCode()
    Dim _description As String
    Dim _index As Integer

    For Each item As TREE_FILTER In Me.m_data_types
      _index = Me.sgd_Types.AddRow()
      _description = String.Empty

      For _count_nivel As Integer = 1 To item.nivel - 1
        _description += GRID_2_TAB
      Next

      If item.node_type = NODE_TYPE.FOLDER Then
        _description = String.Concat(_description, EXPAND_SYMBOL, item.filter_description)
      Else
        _description = String.Concat(_description, item.filter_description)
      End If

      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_DESC).Value = _description
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CODE).Value = String.Join(",", item.codes.ToArray())
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_GROUP_CODE).Value = item.filter_type
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_NIVEL).Value = item.nivel
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_ID).Value = item.id
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_ID_FATHER).Value = item.id_father
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_NODE_TYPE).Value = item.node_type
      Me.sgd_Types.Cell(_index, GRID_2_COLUMN_INDEX_COLLAPSE).Value = item.index_collapse
      Me.sgd_Types.Row(_index).Height = ROW_HEIGHT

      If item.node_type = NODE_TYPE.FILE OrElse item.nivel > 1 Then
        Me.sgd_Types.Row(_index).Height = 0
      End If

    Next

    If Me.sgd_Types.NumRows > 0 Then
      Me.sgd_Types.CurrentRow = 0
    End If

  End Sub

  Private Function CreateTreeFilter(ByVal id As Integer, ByVal idFather As Integer, ByVal indexCollapse As Integer, ByVal nodeType As NODE_TYPE, ByVal filterType As FILTER_ENUMS, ByVal nivel As String, ByVal filterDesc As String,
                                    ByVal gridDesc As String, ByVal codes As List(Of String)) As TREE_FILTER
    Dim _tf As TREE_FILTER

    _tf = New TREE_FILTER()
    _tf.node_type = nodeType
    _tf.filter_type = filterType
    _tf.nivel = nivel
    _tf.filter_description = filterDesc
    _tf.grid_description = gridDesc
    _tf.codes = codes
    _tf.id = id
    _tf.id_father = idFather
    _tf.index_collapse = indexCollapse

    Return _tf
  End Function

  Private Function GetDataFilterGrid() As List(Of TREE_FILTER)
    Dim _codesInit As List(Of String)
    Dim _ls_data_types As List(Of TREE_FILTER)
    Dim _filter_desc As String
    Dim _grid_desc As String

    _ls_data_types = New List(Of TREE_FILTER)

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6831)
    _grid_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6831)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE, Integer))
    _codesInit.Add(CType(CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE, Integer))
    _codesInit.Add(CType(CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE, Integer))
    _codesInit.Add(CType(CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(0, 0, 0, NODE_TYPE.FILE_INIT, FILTER_ENUMS.CASHIER_MOVEMENT, 1, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6610)
    _grid_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6610)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL, Integer))
    _codesInit.Add(CType(CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(1, 0, 1, NODE_TYPE.FILE_INIT, FILTER_ENUMS.CASHIER_MOVEMENT, 1, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654)
    _grid_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.CANCELLED_CREDITS, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(2, 0, 2, NODE_TYPE.FILE_INIT, FILTER_ENUMS.HANDPAY_TYPE, 1, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652)
    _grid_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652)
    _codesInit = New List(Of String)()
    _ls_data_types.Add(Me.CreateTreeFilter(3, 0, 6, NODE_TYPE.FOLDER, FILTER_ENUMS.HANDPAY_TYPE, 1, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652)
    _grid_desc = String.Concat(_filter_desc, " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.JACKPOT, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(4, 3, 4, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 2, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5656)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.JACKPOT, Integer) + CType(ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(5, 3, 5, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 2, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5657)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.JACKPOT, Integer) + CType(ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(6, 3, 6, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 2, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653)
    _grid_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653)
    _codesInit = New List(Of String)()
    _ls_data_types.Add(Me.CreateTreeFilter(7, 0, 13, NODE_TYPE.FOLDER, FILTER_ENUMS.HANDPAY_TYPE, 1, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(8, 7, 8, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 2, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _ls_data_types.Add(Me.CreateTreeFilter(9, 7, 12, NODE_TYPE.FOLDER, FILTER_ENUMS.HANDPAY_TYPE, 2, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653), " - ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.MANUAL_JACKPOT, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(10, 9, 10, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 3, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5656)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653), " - ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.MANUAL_JACKPOT, Integer) + CType(ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_PROGRESSIVE, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(11, 9, 11, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 3, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5657)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653), " - ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(HANDPAY_TYPE.MANUAL_JACKPOT, Integer) + CType(ENUM_HP_JACKPOT_TRANSLATE.HP_JACKPOT_MAJOR_PRIZE, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(12, 9, 12, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 3, _filter_desc, _grid_desc, _codesInit))

    _filter_desc = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5659)
    _grid_desc = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5653), " - ", _filter_desc)
    _codesInit = New List(Of String)()
    _codesInit.Add(CType(WSI.Common.HANDPAY_TYPE.MANUAL_OTHERS, Integer))
    _ls_data_types.Add(Me.CreateTreeFilter(13, 7, 13, NODE_TYPE.FILE, FILTER_ENUMS.HANDPAY_TYPE, 2, _filter_desc, _grid_desc, _codesInit))

    Return _ls_data_types

  End Function

  Private Function GetMovementDescription(ByVal code As Integer, ByVal source As FILTER_ENUMS) As String
    Dim _desc As String
    _desc = String.Empty

    For Each item As TREE_FILTER In Me.m_data_types
      If (item.filter_type = source AndAlso item.codes.Contains(code.ToString())) Then
        _desc = item.grid_description
        Exit For
      End If
    Next
    If String.IsNullOrEmpty(_desc) Then
      _desc = code
    End If

    Return _desc

  End Function

  Private Sub SetCheckedMovementGrid(ByVal isChecked)
    For _index As Integer = 0 To Me.sgd_Types.NumRows - 1
      If (isChecked) Then
        Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      Else
        Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If
    Next
  End Sub

  Private Function GetFilterSelect() As SELECT_GRID

    Dim _select As SELECT_GRID
    Dim _index As Integer
    Dim _value As String
    Dim _list_hanpay_machine As List(Of String)
    Dim _switchChecked As String

    _select.codes_cm = String.Empty
    _select.codes_hp = String.Empty
    _select.descriptions = String.Empty
    _list_hanpay_machine = New List(Of String)
    _switchChecked = uc_grid.GRID_CHK_CHECKED


    If opt_all.Checked Then
      _switchChecked = uc_grid.GRID_CHK_UNCHECKED

    End If

    For _index = 0 To Me.sgd_Types.NumRows - 1
      If (Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CHECKED).Value = _switchChecked _
          And Me.sgd_Types.Cell(_index, GRID_2_COLUMN_NODE_TYPE).Value <> NODE_TYPE.FOLDER) Then

        _value = Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CODE).Value

        Select Case (Me.sgd_Types.Cell(_index, GRID_2_COLUMN_GROUP_CODE).Value)
          Case FILTER_ENUMS.CASHIER_MOVEMENT
            If _select.codes_cm.Length = 0 Then
              _select.codes_cm = _value
            Else
              _select.codes_cm += String.Concat(", ", _value)
            End If
          Case FILTER_ENUMS.HANDPAY_TYPE
            If Me.sgd_Types.Cell(_index, GRID_2_COLUMN_CODE).Value = HANDPAY_TYPE.UNKNOWN Then
              _value = String.Join(",", Me.m_list_handpay_machine.ToArray())
            End If
            If _select.codes_hp.Length = 0 Then
              _select.codes_hp = _value
            Else
              _select.codes_hp += String.Concat(", ", _value)
            End If
        End Select
        If _select.descriptions.Length = 0 Then
          _select.descriptions = Me.sgd_Types.Cell(_index, GRID_2_COLUMN_DESC).Value.Trim()
        Else
          _select.descriptions += String.Concat(", ", Me.sgd_Types.Cell(_index, GRID_2_COLUMN_DESC).Value.Trim())
        End If
      End If
    Next

    Return _select
  End Function

  Private Function RowIsHeaderGroup(ByVal index As Integer) As Boolean
    If (Me.sgd_Types.Cell(index, GRID_2_COLUMN_NODE_TYPE).Value = NODE_TYPE.FOLDER) Then
      Return True
    End If
    Return False
  End Function

  Private Function GetFragmentsGroupIndex(ByVal index As Integer) As List(Of FRAGMENT)
    Dim _ls As List(Of FRAGMENT)
    Dim _father As Integer
    Dim _max_index As Integer

    _ls = New List(Of FRAGMENT)()

    With Me.sgd_Types
      _father = Me.sgd_Types.Cell(index, GRID_2_COLUMN_ID).Value

      For _i As Integer = index To .NumRows - 2
        _max_index = Me.LastIndexContinueFather(_i, _father)
        If _max_index <> -1 Then
          _ls.Add(Me.CreateFragment(_i + 1, _max_index))
          _i = _max_index
        End If

      Next

      If _ls.Count > 0 Then
        _ls.Insert(0, Me.CreateFragment(index, _ls(0).max))
        _ls.RemoveAt(1)
      End If

    End With

    Return _ls
  End Function

  Private Function LastIndexContinueFather(ByVal index As Integer, ByVal father As Integer) As Integer
    Dim _max_father As Integer
    Dim _index_search As Integer

    _index_search = -1
    With Me.sgd_Types
      For _idx As Integer = index + 1 To .NumRows - 1
        _max_father = Me.sgd_Types.Cell(_idx, GRID_2_COLUMN_ID_FATHER).Value
        If (father = _max_father) Then
          _index_search = _idx
        Else
          Exit For
        End If
      Next
    End With
    Return _index_search
  End Function

  Private Function CreateFragment(ByVal min As Integer, ByVal max As Integer)
    Dim _fragment As FRAGMENT

    _fragment = New FRAGMENT()
    _fragment.min = min
    _fragment.max = max

    Return _fragment
  End Function

  Private Function GetFirstRowHeaderGroupIndex(index As Integer) As Integer
    Dim _last_row As Integer

    _last_row = index
    With Me.sgd_Types
      For idx As Integer = index + 1 To .NumRows - 1
        If Me.RowIsHeaderGroup(idx) Then
          Exit For
        End If
        _last_row = idx
      Next
    End With
    Return _last_row
  End Function

  Private Function IsExpandedRow(ByVal index) As Boolean
    Dim _isExpanded As Boolean
    Dim _description As String

    _isExpanded = False
    _description = Me.sgd_Types.Cell(index, GRID_2_COLUMN_DESC).Value
    If _description.Contains(EXPAND_SYMBOL) Then
      _isExpanded = True
    End If

    Return _isExpanded
  End Function

  Private Sub ExpandRow(ByVal index As Integer, ByVal currentRow As Integer)
    Dim _type As NODE_TYPE
    Dim _description As String
    Dim _father As Integer

    _type = Me.sgd_Types.Cell(index, GRID_2_COLUMN_NODE_TYPE).Value

    Me.sgd_Types.Row(index).Height = ROW_HEIGHT
    _father = Me.sgd_Types.Cell(index, GRID_2_COLUMN_NODE_TYPE).Value

    If (_type = NODE_TYPE.FOLDER) AndAlso (index = currentRow) Then
      _description = Me.sgd_Types.Cell(index, GRID_2_COLUMN_DESC).Value
      _description = _description.Replace(EXPAND_SYMBOL, COLLAPSE_SYMBOL)
      Me.sgd_Types.Cell(index, GRID_2_COLUMN_DESC).Value = _description
    End If

  End Sub

  Private Sub CollapseRow(ByVal index As Integer, ByVal currentRow As Integer)
    Dim _type As NODE_TYPE
    Dim _description As String
    Dim _fragments As List(Of FRAGMENT)

    _type = Me.sgd_Types.Cell(index, GRID_2_COLUMN_NODE_TYPE).Value

    If index <> currentRow Then
      Me.sgd_Types.Row(index).Height = 0
    End If

    If _type = NODE_TYPE.FOLDER AndAlso (index = currentRow) Then
      _description = Me.sgd_Types.Cell(index, GRID_2_COLUMN_DESC).Value
      _description = _description.Replace(COLLAPSE_SYMBOL, EXPAND_SYMBOL)
      Me.sgd_Types.Cell(index, GRID_2_COLUMN_DESC).Value = _description
    End If

    If _type = NODE_TYPE.FOLDER AndAlso (index <> currentRow) Then
      _fragments = Me.GetFragmentsGroupIndex(index)
      For Each _item As FRAGMENT In _fragments
        For _index As Integer = _item.min To _item.max
          Call Me.CollapseRow(_index, index)
        Next
      Next
    End If

  End Sub

  Private Sub SelectRow(ByVal row)
    Dim _fragments As List(Of FRAGMENT)
    Dim _isFisrt As Boolean

    _fragments = Me.GetFragmentsGroupIndex(row)
    _isFisrt = True

    With Me.sgd_Types
      For Each _item As FRAGMENT In _fragments
        For index As Integer = _item.min To _item.max
          .Cell(index, GRID_2_COLUMN_CHECKED).Value = .Cell(row, GRID_2_COLUMN_CHECKED).Value
          If _isFisrt Then
            _isFisrt = False
          Else
            If .Cell(index, GRID_2_COLUMN_NODE_TYPE).Value = NODE_TYPE.FOLDER Then
              Call SelectRow(index)
            End If
          End If
        Next
      Next
    End With
  End Sub

#End Region

#Region "Events"

  Private Sub opt_some_Click(sender As Object, e As EventArgs) Handles opt_some.Click
    Me.SetCheckedMovementGrid(False)

  End Sub

  Private Sub opt_all_Click(sender As Object, e As EventArgs) Handles opt_all.Click
    Me.SetCheckedMovementGrid(False)

  End Sub

  Private Sub sgd_Types_DataSelectedEvent() Handles sgd_Types.DataSelectedEvent
    Dim _fragments As List(Of FRAGMENT)
    Dim _currentRow As Integer

    With Me.sgd_Types
      _currentRow = .CurrentRow
      If Me.RowIsHeaderGroup(_currentRow) Then
        _fragments = Me.GetFragmentsGroupIndex(_currentRow)

        If Me.IsExpandedRow(_currentRow) Then
          For Each _item As FRAGMENT In _fragments
            For _index As Integer = _item.min To _item.max
              Call Me.ExpandRow(_index, _currentRow)
            Next

          Next
        Else
          For Each _item As FRAGMENT In _fragments
            For _index As Integer = _item.min To _item.max
              Call Me.CollapseRow(_index, _currentRow)
            Next
          Next
        End If

      End If
    End With

  End Sub

  Private Sub sgd_Types_CellDataChangedEvent(Row As Integer, Column As Integer) Handles sgd_Types.CellDataChangedEvent
    Dim _is_select_all As Boolean

    With Me.sgd_Types
      If Me.RowIsHeaderGroup(Row) Then
        Call SelectRow(Row)

      End If
      _is_select_all = True
      For index As Integer = 0 To .NumRows - 1
        If (.Cell(index, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED) Then
          _is_select_all = False
          Exit For
        End If
      Next
      If _is_select_all Then
        Me.opt_all.Checked = True
        Me.opt_some.Checked = False
      Else
        Me.opt_all.Checked = False
        Me.opt_some.Checked = True
      End If
    End With
  End Sub

#End Region

End Class