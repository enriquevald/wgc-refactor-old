'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_win_control_report.vb
' DESCRIPTION:   Netwin Control report
' AUTHOR:        Francisco Vicente
' CREATION DATE: 20-MAY-2015
'
' REVISION HISTORY: 
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-MAY-2015  FAV    Initial version
' 01-OCT-2015  FAV    The current FormId is not valid
' 01-OCT-2015  ECP    Backlog Item 4158 GUI - Manual pays balance report 
' 09-NOV-2015  ECP    Backlog Item 5685 GUI - Manual pays balance report Revision
' 17-NOV-2015  ECP    Bug 6569 The column for select rows show zeros
' 17-NOV-2015  ECP    Backlog Item 6703 Sprint Review 15 Winions - Mex. Second Revision
' 14-DIC-2015  ECP    Bug 6569 Reopened - The column for select rows show zeros when no rows to show
' 17-DIC-2015  ECP    Bug 7831 Check Exclude rows without activity, bad positioned
' 17-DIC-2015  ECP    Bug 7833 Filter order in Excel are not the same like order in the window
' 06-APR-2016  EOR    Bug 11221 Handpays balance report error in the filters
' 16-MAR-2016  GDA    Product Backlog Item 10652:IN-33: GUI, reporte de control de Netwin: filtro para valores negativos
' 06-APR-2016  EOR    Bug 11221 Handpays balance report error in the filters
' 21-JUN-2016  EOR    Bug 11366 Handpays balance report: Only Handpays type paid
' 20-DEC-2017  AMF    Bug 31127:[WIGOS-5690]: WigosGUI - Statistics: A column not identified is shown on "Reporte de control de Netwin" report.
' 25-JUL-2018  AGS    Bug 33714:WIGOS-13830 [Ticket #15912] Fallo - Cuadratura de Pago Manuales Version V03.08.0027
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region ' Imports

Public Class frm_tito_win_control_report
  Inherits GUI_Controls.frm_base_sel

#Region "Instance Creation"

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_TITO_WIN_CONTROL_REPORT
    Me.m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT
    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId
    If FormId = ENUM_FORM.FORM_TITO_WIN_CONTROL_REPORT Then
      Me.m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT
    Else
      Me.m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT
    End If

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub
#End Region

#Region " Constants "
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' EOR 20-JUN-2016
  ' DB Columns TYPE_PAYS_BALANCE_REPORT
  Private Const SQL_COLUMN_TPBR_TE_PROVIDER_ID As Integer = 0
  Private Const SQL_COLUMN_TPBR_TE_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TPBR_TE_NAME As Integer = 2
  Private Const SQL_COLUMN_TPBR_DENOMINATION As Integer = 3
  Private Const SQL_COLUMN_TPBR_SYSTEM_CREDIT_CANCEL As Integer = 4
  Private Const SQL_COLUMN_TPBR_SYSTEM_JACKPOTS As Integer = 5
  Private Const SQL_COLUMN_TPBR_SYSTEM_PROGRESSIVE_JACKPOTS As Integer = 6
  Private Const SQL_COLUMN_TPBR_SYSTEM_HANDPAYS_TOTAL As Integer = 7
  Private Const SQL_COLUMN_TPBR_COUNTERS_CREDIT_CANCEL As Integer = 8
  Private Const SQL_COLUMN_TPBR_COUNTERS_JACKPOTS As Integer = 9
  Private Const SQL_COLUMN_TPBR_COUNTERS_HANDPAYS_TOTAL As Integer = 10
  Private Const SQL_COLUMN_TPBR_DIFFERENCE As Integer = 11

  ' EOR 20-JUN-2016
  ' DB Columns TYPE_NETWIN_REPORT
  Private Const SQL_COLUMN_TE_PROVIDER_ID As Integer = 0
  Private Const SQL_COLUMN_TE_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TE_NAME As Integer = 2
  Private Const SQL_COLUMN_DENOMINATION As Integer = 3
  Private Const SQL_COLUMN_ORDER_DATE As Integer = 4 '0
  Private Const SQL_COLUMN_ORDER_DATE_END As Integer = 5 '0
  Private Const SQL_COLUMN_TICKET_IN_COUNT As Integer = 7
  Private Const SQL_COLUMN_TI_IN_AMOUNT_RE As Integer = 8
  Private Const SQL_COLUMN_TI_IN_AMOUNT_NO_RE As Integer = 9
  Private Const SQL_COLUMN_TI_IN_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_TICKET_OUT_COUNT As Integer = 11
  Private Const SQL_COLUMN_TI_OUT_AMOUNT_RE As Integer = 12
  Private Const SQL_COLUMN_TI_OUT_AMOUNT_NO_RE As Integer = 13
  Private Const SQL_COLUMN_TI_OUT_AMOUNT As Integer = 14
  Private Const SQL_COLUMN_NET_TITO As Integer = 15
  Private Const SQL_COLUMN_MANUAL As Integer = 16
  Private Const SQL_COLUMN_CREDIT_CANCEL As Integer = 17
  Private Const SQL_COLUMN_SITE_JACKPOT As Integer = 18
  Private Const SQL_COLUMN_PROGRESIVES As Integer = 19
  Private Const SQL_COLUMN_NO_PROGRESIVES As Integer = 20
  Private Const SQL_COLUMN_HAND_PAYS_TOTAL As Integer = 21
  Private Const SQL_COLUMN_PROGRESIVE_PROVISIONS As Integer = 22
  Private Const SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS As Integer = 23
  Private Const SQL_COLUMN_GAP_BILL As Integer = 24 '0
  Private Const SQL_COLUMN_TOTAL_COIN_IN_CREDITS As Integer = 25
  Private Const SQL_COLUMN_TOTAL_COIN_OUT_CREDITS As Integer = 26
  Private Const SQL_COLUMN_TOTAL_JACKPOT_CREDITS As Integer = 27
  Private Const SQL_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS As Integer = 28

  Private Const SQL_FIRST_COLUMN_VARIABLE As Integer = 31

  'Grid Columns
  Private GRID_COLUMN_INDEX As Integer

  Private GRID_COLUMN_TE_NAME As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private TERMINAL_DATA_COLUMNS As Int32
  Private GRID_COLUMN_DENOMINATION As Integer

  Private GRID_COLUMN_MANUAL As Integer
  Private GRID_COLUMN_CREDIT_CANCEL As Integer
  Private GRID_COLUMN_SITE_JACKPOT As Integer

  Private GRID_COLUMN_PROGRESIVE_PROVISIONS As Integer
  Private GRID_COLUMN_PROVISIONED_JACKPOTS As Integer

  Private GRID_COLUMN_NO_PROGRESIVES As Integer 'Non provisioned Jackpots
  Private GRID_COLUMN_JACKPOTS As Integer 'Provisioned And Non Provisioned Jackpots
  Private GRID_COLUMN_PROGRESSIVE_JACKPOT As Integer

  Private GRID_COLUMN_NETWIN_TOTAL As Integer

  Private GRID_COLUMN_PLAYED As Integer  'Total coin in credits
  Private GRID_COLUMN_WON As Integer  'Total coin out credits
  Private GRID_COLUMN_TOTAL_JACKPOT_CREDITS As Integer
  Private GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS As Integer
  Private GRID_COLUMN_NETWIN_COUNTERS As Integer 'equal to: Played - Won - Jackpot

  Private GRID_COLUMN_DIFFERENCE As Integer 'equal to: NetWinTotal - NetWinCounters

  Private GRID_COLUMNS As Integer

  ' counters
  Private Const MAX_COUNTERS As Integer = 3
  Private Const COUNTER_DETAILS As Integer = 1
  Private Const COUNTER_SUBTOTALS As Integer = 2

#End Region

#Region " Enums "

  Public Enum ENUM_TITO_REPORT_TYPE

    TYPE_NETWIN_REPORT = 0
    TYPE_PAYS_BALANCE_REPORT = 1

  End Enum

#End Region ' Enums

#Region " Member "

  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_list_type As String
  Private m_subtotal_break As String
  Private m_query_as_datatable As DataTable
  Private m_counter_list(MAX_COUNTERS) As Integer
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False
  Private m_tito_report_type As ENUM_TITO_REPORT_TYPE 'EOR 20-JUN-2016
  Private m_exclude_rows_without_activity As String
  Private m_include_rows_only_negatives As String

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    ' Leave this sub empty, as the FormId is set now in the Constructor of the class.
    'Me.FormId = ENUM_FORM.FORM_TITO_WIN_CONTROL_REPORT

    'Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' EOR 06-APR-2016 Form title / Date time filter / Providers - Terminals
    Select Case Me.m_tito_report_type

      Case ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6356)
        Me.uc_dsl.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(5715), True, False)
        Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes())

      Case ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6781)
        Me.uc_dsl.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(5723), True, False)
        Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypeSasHost())

    End Select

    ' - Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Show 
    Me.gb_show.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5413)
    Me.chk_subtotal_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5415)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5517)

    ' Remarks
    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
      Me.lbl_remarks_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6360, _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(5435), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(5412)) ' "A: Columna '%1' del %2"
      Me.lbl_remarks_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6363, _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(537), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(798), _
                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(5849)) ' "B: %1 - %2 - %3"
      Me.chk_exclude_without_activity.Visible = False

      'EOR 08-APR-2016 No Visible CheckBox
      Me.chk_only_unbalance.Visible = False
      chk_include_only_negatives.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7183)
    Else
      Me.panel_filter.Height = 207
      Me.panel_data.Location = New System.Drawing.Point(5, 212)
      chk_exclude_without_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4738)
      Me.lbl_remarks_1.Visible = False
      Me.lbl_remarks_2.Visible = False

      Me.chk_only_unbalance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5043)
      Me.chk_include_only_negatives.Visible = False
    End If
    ' Style main Grid  (after DB read because it has dinamic columns)
    Call Me.GUI_StyleSheet()

    'Default values
    Call Me.SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call Me.SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function ' GUI_GetQueryType

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _sql_where As String
    Dim _row As DataRow
    Dim _final_time As DateTime

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    Dim _query_as_datatable As DataTable


    _final_time = New DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, Me.uc_dsl.ClosingTime, 0, 0)

    'EOR 21-JUN-2016
    _sql_where = GetSqlWhere()

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
      _sql_cmd = New SqlCommand("GetHandpaysBalanceReport")
      _sql_cmd.CommandType = CommandType.StoredProcedure

      _sql_cmd.Parameters.Add("@pFromDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.FromDateSelected, WGDB.Now(), Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime))
      _sql_cmd.Parameters.Add("@pToDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.ToDateSelected, _final_time.AddDays(1), Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime))
      _sql_cmd.Parameters.Add("@pClosingTime", SqlDbType.Int).Value = Me.uc_dsl.ClosingTime
      _sql_cmd.Parameters.Add("@pTerminalWhere", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where), " ", _sql_where)
      _sql_cmd.Parameters.Add("@pWithOutActivity", SqlDbType.Bit).Value = Me.chk_exclude_without_activity.Checked
      _sql_cmd.Parameters.Add("@pOnlyUnbalance", SqlDbType.Bit).Value = Me.chk_only_unbalance.Checked
    Else
      _sql_cmd = New SqlCommand("PR_collection_by_machine_and_date")
      _sql_cmd.CommandType = CommandType.StoredProcedure

      _sql_cmd.Parameters.Add("@pFromDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.FromDateSelected, WGDB.Now(), Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime))
      _sql_cmd.Parameters.Add("@pToDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.ToDateSelected, _final_time.AddDays(1), Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime))
      _sql_cmd.Parameters.Add("@pClosingTime", SqlDbType.Int).Value = Me.uc_dsl.ClosingTime
      _sql_cmd.Parameters.Add("@pMaskStatus", SqlDbType.Int).Value = WSI.Common.HANDPAY_STATUS.MASK_STATUS
      _sql_cmd.Parameters.Add("@pStatusPaid", SqlDbType.Int).Value = WSI.Common.HANDPAY_STATUS.PAID
      _sql_cmd.Parameters.Add("@pAll", SqlDbType.Int).Value = IIf(m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT, 0, 1) '1 = Only Paid
      _sql_cmd.Parameters.Add("@pTerminalWhere", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where), " ", _sql_where)
      _sql_cmd.Parameters.Add("@pBillDetail", SqlDbType.Bit).Value = 0
      _sql_cmd.Parameters.Add("@pShowMetters", SqlDbType.Bit).Value = 1
      _sql_cmd.Parameters.Add("@pOnlyGroupByTerminal", SqlDbType.Bit).Value = 1
      _sql_cmd.Parameters.Add("@pIncludeJackpotRoom", SqlDbType.Bit).Value = 1
    End If

    _query_as_datatable = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)
    m_query_as_datatable = _query_as_datatable


    Dim i As Integer = 0
    For Each column As DataColumn In m_query_as_datatable.Columns
      Debug.WriteLine(String.Format("{0} - {1}", column.ColumnName, i.ToString()))
      i += 1
    Next

    ' Style main Grid  (after DB read because it has dinamic columns)
    Call Me.GUI_StyleSheet()
    Call GUI_BeforeFirstRow()

    If m_query_as_datatable.Rows.Count > 0 Then
      For Each _row In m_query_as_datatable.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_tito_win_control_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If
  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else

        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_data As List(Of Object)
    Dim _temp_dec As Decimal
    Dim _temp_bills As Decimal
    Dim _temp_system_jackpots As Decimal
    Dim _temp_meters_handpays As Decimal

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    'EOR 21-JUN-2016
    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
      ' Terminal Report
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TPBR_TE_TERMINAL_ID), m_terminal_report_type)

      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next

      ' DENOMINATION
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = DbRow.Value(SQL_COLUMN_TPBR_DENOMINATION)

      'SYSTEM_CREDIT_CANCEL
      If DbRow.IsNull(SQL_COLUMN_TPBR_SYSTEM_CREDIT_CANCEL) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_SYSTEM_CREDIT_CANCEL)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'SYSTEM_PROGRESSIVE_JACKPOTS
      If DbRow.IsNull(SQL_COLUMN_TPBR_SYSTEM_PROGRESSIVE_JACKPOTS) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_SYSTEM_PROGRESSIVE_JACKPOTS)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_JACKPOT).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'SYSTEM_JACKPOTS
      If DbRow.IsNull(SQL_COLUMN_TPBR_SYSTEM_JACKPOTS) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_SYSTEM_JACKPOTS)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'SYSTEM_HANDPAYS_TOTAL TOTAL (A)
      If DbRow.IsNull(SQL_COLUMN_TPBR_SYSTEM_HANDPAYS_TOTAL) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_SYSTEM_HANDPAYS_TOTAL)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'COUNTERS_CREDIT_CANCEL
      If DbRow.IsNull(SQL_COLUMN_TPBR_COUNTERS_CREDIT_CANCEL) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_COUNTERS_CREDIT_CANCEL)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'COUNTERS_JACKPOTS
      If DbRow.IsNull(SQL_COLUMN_TPBR_COUNTERS_JACKPOTS) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_COUNTERS_JACKPOTS)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'COUNTERS_HANDPAYS_TOTAL TOTAL (B)
      If DbRow.IsNull(SQL_COLUMN_TPBR_COUNTERS_HANDPAYS_TOTAL) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_COUNTERS_HANDPAYS_TOTAL)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_COUNTERS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'DIFFERENCE
      If DbRow.IsNull(SQL_COLUMN_TPBR_DIFFERENCE) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TPBR_DIFFERENCE)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      ' Terminal Report
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next

      ' DENOMINATION
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = DbRow.Value(SQL_COLUMN_DENOMINATION)

      _temp_system_jackpots = 0
      _temp_bills = 0

      '' CREDIT_CANCEL
      'If DbRow.IsNull(SQL_COLUMN_CREDIT_CANCEL) Then
      '  _temp_dec = 0
      'Else
      '  _temp_dec = DbRow.Value(SQL_COLUMN_CREDIT_CANCEL)
      'End If

      'Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      '_temp_bills = _temp_bills + _temp_dec

      ' NO_PROGRESIVES
      If DbRow.IsNull(SQL_COLUMN_NO_PROGRESIVES) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_NO_PROGRESIVES)
      End If

      _temp_system_jackpots = _temp_system_jackpots + _temp_dec
      _temp_bills = _temp_bills + _temp_dec




      'Bills
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        _temp_bills = 0
        For _index As Integer = SQL_FIRST_COLUMN_VARIABLE To m_query_as_datatable.Columns.Count - 1
          If Not (DbRow.IsNull(_index)) Then
            _temp_bills += DbRow.Value(_index)
          End If
        Next
      End If

      ' PROVISIONED_JACKPOTS
      If DbRow.IsNull(SQL_COLUMN_PROGRESIVES) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_PROGRESIVES)
      End If
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVISIONED_JACKPOTS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      'JACKPOT COLUMN
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
        _temp_system_jackpots = _temp_system_jackpots + _temp_dec
        _temp_bills = _temp_bills + _temp_dec
        Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(_temp_system_jackpots, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' PROGRESIVE_PROVISIONS
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        If DbRow.IsNull(SQL_COLUMN_PROGRESIVE_PROVISIONS) Then
          _temp_dec = 0
        Else
          _temp_dec = DbRow.Value(SQL_COLUMN_PROGRESIVE_PROVISIONS)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If


      ' NETWIN_TOTAL - SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        If DbRow.IsNull(SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS) Then
          _temp_dec = 0
        Else
          _temp_dec = DbRow.Value(SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS)
        End If

        Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_bills - _temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_bills, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If


      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        ' PLAYED
        If DbRow.IsNull(SQL_COLUMN_TOTAL_COIN_IN_CREDITS) Then
          _temp_dec = 0
        Else
          _temp_dec = DbRow.Value(SQL_COLUMN_TOTAL_COIN_IN_CREDITS)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        ' WON
        If DbRow.IsNull(SQL_COLUMN_TOTAL_COIN_OUT_CREDITS) Then
          _temp_dec = 0
        Else
          _temp_dec = DbRow.Value(SQL_COLUMN_TOTAL_COIN_OUT_CREDITS)
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      _temp_meters_handpays = 0

      ' JACKPOT_CREDITS
      If DbRow.IsNull(SQL_COLUMN_TOTAL_JACKPOT_CREDITS) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TOTAL_JACKPOT_CREDITS)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _temp_meters_handpays = _temp_meters_handpays + _temp_dec


      ' HAND_PAID_CANCELLED_CREDITS
      If DbRow.IsNull(SQL_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(SQL_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _temp_meters_handpays = _temp_meters_handpays + _temp_dec

      ' NETWIN_COUNTERS
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        _temp_dec = Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value - Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value - Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value
      Else
        _temp_dec = _temp_meters_handpays
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_COUNTERS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


      ' DIFFERENCE
      _temp_dec = Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value - Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_COUNTERS).Value
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


      ' GDA 15-03-2016

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        If Not chk_include_only_negatives.Checked Then

          ' Colors
          If Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value < 0 Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          End If
          If Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_COUNTERS).Value < 0 Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_COUNTERS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          End If
          If Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value < 0 Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          End If
        Else

          ' Rows
          If Not (Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value < 0 Or Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_COUNTERS).Value < 0 Or Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value < 0) Then
            Return False
          End If
        End If
      End If
      '------------
    End If

    m_counter_list(COUNTER_DETAILS) += 1

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If
    m_refresh_grid = False

    m_subtotal_break = ""
    m_counter_list(COUNTER_DETAILS) = 0
    m_counter_list(COUNTER_SUBTOTALS) = 0
  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _text_subtotal As String
    If Not Me.chk_subtotal_provider.Checked Then
      Return True
    End If

    Dim _provider_id As String
    _provider_id = TerminalReport.GetReportData(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID), ReportColumn.Provider)

    If m_subtotal_break <> "" And m_subtotal_break <> _provider_id Then
      _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)

      ProcesSubTotal(False, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal & " " & m_subtotal_break)
    End If

    If Not String.IsNullOrEmpty(_provider_id) Then
      m_subtotal_break = _provider_id
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _text_subtotal As String
    Dim _text_total As String

    _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)
    _text_total = GLB_NLS_GUI_INVOICING.GetString(205)

    If Me.chk_subtotal_provider.Checked Then
      ProcesSubTotal(False, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal & " " & m_subtotal_break)
    End If
    ProcesSubTotal(True, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, _text_total)

    If Grid.Counter(COUNTER_DETAILS).Value <> m_counter_list(COUNTER_DETAILS) Then
      Grid.Counter(COUNTER_DETAILS).Value = m_counter_list(COUNTER_DETAILS)
    End If
    If Grid.Counter(COUNTER_SUBTOTALS).Value <> m_counter_list(COUNTER_SUBTOTALS) Then
      Grid.Counter(COUNTER_SUBTOTALS).Value = m_counter_list(COUNTER_SUBTOTALS)
    End If

  End Sub ' GUI_AfterLastRow

#End Region  ' Overrides

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5361), m_list_type)
    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
      PrintData.SetFilter(chk_exclude_without_activity.Text & ":", m_exclude_rows_without_activity)
    Else
      PrintData.SetFilter(chk_include_only_negatives.Text & ":", m_include_rows_only_negatives)
    End If

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    ' Date
    _date = uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _date = ToDate()
    If uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If


    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()
    If m_terminal Is Nothing Then m_terminal = ""

    m_list_type = ""

    If Me.chk_terminal_location.Checked Then
      m_list_type &= Me.chk_terminal_location.Text & ", "
    End If

    If Me.chk_subtotal_provider.Checked Then
      m_list_type &= Me.chk_subtotal_provider.Text & ", "
    End If

    If m_list_type.Length > 2 Then
      m_list_type = m_list_type.Substring(0, m_list_type.Length - 2)
    End If

    'Exclude rows without activity
    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
      If chk_exclude_without_activity.Checked Then
        m_exclude_rows_without_activity = GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_exclude_rows_without_activity = GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    Else
      If chk_include_only_negatives.Checked Then
        m_include_rows_only_negatives = GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_include_rows_only_negatives = GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    End If

  End Sub 'GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - MdiParent
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParentForm As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParentForm
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

#Region " Private Functions "

  ' PURPOSE: Put values to default
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_subtotal_provider.Checked = False
    Me.chk_terminal_location.Checked = False
    Me.chk_include_only_negatives.Checked = False

    ' Machines
    Call Me.uc_pr_list.SetDefaultValues()

  End Sub 'SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .Sortable = True
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        .Counter(COUNTER_DETAILS).Visible = True
        .Counter(COUNTER_DETAILS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        .Counter(COUNTER_DETAILS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Else
        .Counter(COUNTER_DETAILS).Visible = False
        .Counter(COUNTER_SUBTOTALS).Visible = False
      End If

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        If chk_subtotal_provider.Checked Then
          .Counter(COUNTER_SUBTOTALS).Visible = True
          .Counter(COUNTER_SUBTOTALS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          .Counter(COUNTER_SUBTOTALS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        Else
          .Counter(COUNTER_SUBTOTALS).Visible = False
        End If
      End If

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5417)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TE_NAME = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5417)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5421)
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DENOMINATION).Width = 1500

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
        ' CREDIT_CANCEL
        .Column(GRID_COLUMN_CREDIT_CANCEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)
        .Column(GRID_COLUMN_CREDIT_CANCEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654)
        .Column(GRID_COLUMN_CREDIT_CANCEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_CREDIT_CANCEL).Width = 2000

        '' JACKPOTS
        .Column(GRID_COLUMN_JACKPOTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)
        .Column(GRID_COLUMN_JACKPOTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5849) 'JAKCPOTS
        .Column(GRID_COLUMN_JACKPOTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_JACKPOTS).Width = 1800

        ' PROGRESSIVE_JACKPOT
        .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)
        .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5340)
        .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT).Width = 2000

      Else 'TYPE_NETWIN_REPORT

        ' PROGRESIVE_PROVISIONS
        .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6149) 'es lo mismo que 2268
        .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5434)
        .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Width = 2500

        ' PROVISIONED JACKPOTS
        .Column(GRID_COLUMN_PROVISIONED_JACKPOTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)
        .Column(GRID_COLUMN_PROVISIONED_JACKPOTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5432)
        .Column(GRID_COLUMN_PROVISIONED_JACKPOTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_PROVISIONED_JACKPOTS).Width = 2200

      End If

      ' NETWIN_TOTAL (Win Sistema)
      .Column(GRID_COLUMN_NETWIN_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        .Column(GRID_COLUMN_NETWIN_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6361) ' "Netwin (A)"
      Else
        .Column(GRID_COLUMN_NETWIN_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6786) ' "Total (A)"
      End If

      .Column(GRID_COLUMN_NETWIN_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NETWIN_TOTAL).Width = 1800

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        ' PLAYED
        .Column(GRID_COLUMN_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
        .Column(GRID_COLUMN_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) ' "Played"
        .Column(GRID_COLUMN_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_PLAYED).Width = 1800

        ' WON
        .Column(GRID_COLUMN_WON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
        .Column(GRID_COLUMN_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798) ' "Won"
        .Column(GRID_COLUMN_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_WON).Width = 1800
      End If

      ' TOTAL_JACKPOT_CREDITS
      .Column(GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
      .Column(GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5849) ' "Jackpots" 
      .Column(GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Width = 1800

      ' TOTAL_HAND_PAID_CANCELLED_CREDITS
      .Column(GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
      .Column(GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5654) ' "Canceled Credits"
      .Column(GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Width = 2000

      ' NETWIN_COUNTERS
      .Column(GRID_COLUMN_NETWIN_COUNTERS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5039)
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        .Column(GRID_COLUMN_NETWIN_COUNTERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6362) ' "Netwin (B)"
      Else
        .Column(GRID_COLUMN_NETWIN_COUNTERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6787) ' "Total (B)"
      End If
      .Column(GRID_COLUMN_NETWIN_COUNTERS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NETWIN_COUNTERS).Width = 2000

      ' DIFFERENCE
      .Column(GRID_COLUMN_DIFFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5040)
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        .Column(GRID_COLUMN_DIFFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6364) ' "Netwin (A-B)"
      Else
        .Column(GRID_COLUMN_DIFFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6788) ' "Total (A-B)"
      End If
      .Column(GRID_COLUMN_DIFFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DIFFERENCE).Width = 2000

    End With

  End Sub ' GUI_StyleSheet

  Private Function ToDate() As Date

    If uc_dsl.ToDateSelected Then
      Return New Date(uc_dsl.ToDate.Year, uc_dsl.ToDate.Month, uc_dsl.ToDate.Day, uc_dsl.ClosingTime, 0, 0)
    Else
      Return New Date(Today.Year, Today.Month, Today.Day, uc_dsl.ClosingTime, 0, 0).AddDays(1)
    End If

  End Function  ' ToDate

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As StringBuilder
    Dim _str_terminal_id As String

    _str_where = New StringBuilder()

    ' Filter Providers - Terminals
    _str_terminal_id = " TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _str_where.AppendLine(" AND " & _str_terminal_id)

    Return _str_where.ToString()

  End Function ' GetSqlWhere

  ' PURPOSE: Process subtotal value 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ProcesSubTotal(ByVal IsTotal As Boolean, ByVal IdxTotalColumn As Integer, ByVal ENUM_GUI As ControlColor.ENUM_GUI_COLOR, ByVal TextTotal As String)

    Dim _idx_row As Integer = Me.Grid.NumRows
    Dim _condition As String
    Dim _temp_obj As Object
    Dim _temp_dec As Decimal
    Dim _temp_bill As Decimal
    Dim _c_name As String
    Dim _temp_system_jackpots As Decimal
    Dim _temp_meters_handpays As Decimal

    If Not (Me.Grid.NumRows > 0 Or Me.chk_subtotal_provider.Checked) Then
      If IsTotal Then
        Call AddSubtotalWithoutValues(IdxTotalColumn)
      End If
      Return
    End If

    Me.Grid.AddRow()

    If IsTotal Then
      _condition = "1=1 "
    Else
      _condition = "TE_PROVIDER_ID = '" & m_subtotal_break & "'"
      m_counter_list(COUNTER_SUBTOTALS) += 1
    End If

    ' Label - TOTAL
    Me.Grid.Cell(_idx_row, IdxTotalColumn).Value = TextTotal

    'EOR 21-JUN-2016
    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then

      'SYSTEM_CREDIT_CANCEL
      _temp_dec = m_query_as_datatable.Compute("SUM(SYSTEM_CREDIT_CANCEL)", _condition & " AND SYSTEM_CREDIT_CANCEL IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'SYSTEM_JACKPOTS
      _temp_dec = m_query_as_datatable.Compute("SUM(SYSTEM_JACKPOTS)", _condition & " AND SYSTEM_JACKPOTS IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'SYSTEM_PROGRESSIVE_JACKPOTS
      _temp_dec = m_query_as_datatable.Compute("SUM(SYSTEM_PROGRESSIVE_JACKPOTS)", _condition & " AND SYSTEM_PROGRESSIVE_JACKPOTS IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'SYSTEM_HANDPAYS_TOTAL TOTAL (A)
      _temp_dec = m_query_as_datatable.Compute("SUM(SYSTEM_HANDPAYS_TOTAL)", _condition & " AND SYSTEM_HANDPAYS_TOTAL IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'COUNTERS_CREDIT_CANCEL
      _temp_dec = m_query_as_datatable.Compute("SUM(COUNTERS_CREDIT_CANCEL)", _condition & " AND COUNTERS_CREDIT_CANCEL IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'COUNTERS_JACKPOTS
      _temp_dec = m_query_as_datatable.Compute("SUM(COUNTERS_JACKPOTS)", _condition & " AND COUNTERS_JACKPOTS IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'COUNTERS_HANDPAYS_TOTAL TOTAL (B)
      _temp_dec = m_query_as_datatable.Compute("SUM(COUNTERS_HANDPAYS_TOTAL)", _condition & " AND COUNTERS_HANDPAYS_TOTAL IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_COUNTERS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'DIFFERENCE
      _temp_dec = m_query_as_datatable.Compute("SUM(DIFFERENCE)", _condition & " AND DIFFERENCE IS NOT NULL")
      If IsDBNull(_temp_dec) Then
        _temp_dec = 0
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Else
      _temp_system_jackpots = 0
      _temp_bill = 0

      '' CREDIT_CANCEL
      '_temp_obj = m_query_as_datatable.Compute("SUM(CREDIT_CANCEL)", _condition & " AND CREDIT_CANCEL IS NOT NULL")
      'If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      '  _temp_dec = 0
      'Else
      '  _temp_dec = m_query_as_datatable.Compute("SUM(CREDIT_CANCEL)", _condition & " AND CREDIT_CANCEL IS NOT NULL")
      'End If

      'Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      '_temp_bill = _temp_bill + _temp_dec



      ' PROGRESIVES
      _temp_obj = m_query_as_datatable.Compute("SUM(PROGRESIVES)", _condition & " AND PROGRESIVES IS NOT NULL")
      If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
        _temp_dec = 0
      Else
        _temp_dec = m_query_as_datatable.Compute("SUM(PROGRESIVES)", _condition & " AND PROGRESIVES IS NOT NULL")
      End If
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVISIONED_JACKPOTS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      _temp_system_jackpots = _temp_system_jackpots + _temp_dec
      _temp_bill = _temp_bill + _temp_dec

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        ' PROGRESIVE_PROVISIONS
        _temp_obj = m_query_as_datatable.Compute("SUM(PROGRESIVE_PROVISIONS)", _condition & " AND PROGRESIVE_PROVISIONS IS NOT NULL")
        If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
          _temp_dec = 0
        Else
          _temp_dec = m_query_as_datatable.Compute("SUM(PROGRESIVE_PROVISIONS)", _condition & " AND PROGRESIVE_PROVISIONS IS NOT NULL")
        End If
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        ' NO_PROGRESIVES
        _temp_obj = m_query_as_datatable.Compute("SUM(NO_PROGRESIVES)", _condition & " AND NO_PROGRESIVES IS NOT NULL")
        If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
          _temp_dec = 0
        Else
          _temp_dec = m_query_as_datatable.Compute("SUM(NO_PROGRESIVES)", _condition & " AND NO_PROGRESIVES IS NOT NULL")
        End If

        _temp_system_jackpots = _temp_system_jackpots + _temp_dec
        Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(_temp_system_jackpots, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _temp_bill = _temp_bill + _temp_dec

      End If

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        'Bills
        _temp_bill = 0
        For _index As Integer = SQL_FIRST_COLUMN_VARIABLE To m_query_as_datatable.Columns.Count - 1
          'For _index As Integer = SQL_FIRST_COLUMN_VARIABLE To 7
          _c_name = "[" & m_query_as_datatable.Columns(_index).ColumnName & "]"
          _temp_obj = m_query_as_datatable.Compute("SUM(" & _c_name & ")", _condition & " AND " & _c_name & " IS NOT NULL")

          If Not (_temp_obj Is Nothing OrElse IsDBNull(_temp_obj)) Then
            _temp_dec = m_query_as_datatable.Compute("SUM(" & _c_name & ")", _condition & " AND " & _c_name & " IS NOT NULL")
            _temp_bill += _temp_dec
          End If
        Next
      End If

      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        ' NETWIN_TOTAL
        _temp_obj = m_query_as_datatable.Compute("SUM(TOTAL_WIN_WITHOUT_BILLS)", _condition & " AND TOTAL_WIN_WITHOUT_BILLS IS NOT NULL")
        If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
          _temp_dec = 0
        Else
          _temp_dec = m_query_as_datatable.Compute("SUM(TOTAL_WIN_WITHOUT_BILLS)", _condition & " AND TOTAL_WIN_WITHOUT_BILLS IS NOT NULL")
        End If

        Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_bill - _temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_bill, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If


      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        ' PLAYED
        _temp_obj = m_query_as_datatable.Compute("SUM(TOTAL_COIN_IN_CREDITS)", _condition & " AND TOTAL_COIN_IN_CREDITS IS NOT NULL")
        If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
          _temp_dec = 0
        Else
          _temp_dec = m_query_as_datatable.Compute("SUM(TOTAL_COIN_IN_CREDITS)", _condition & " AND TOTAL_COIN_IN_CREDITS IS NOT NULL")
        End If
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        ' WON
        _temp_obj = m_query_as_datatable.Compute("SUM(TOTAL_COIN_OUT_CREDITS)", _condition & " AND TOTAL_COIN_OUT_CREDITS IS NOT NULL")
        If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
          _temp_dec = 0
        Else
          _temp_dec = m_query_as_datatable.Compute("SUM(TOTAL_COIN_OUT_CREDITS)", _condition & " AND TOTAL_COIN_OUT_CREDITS IS NOT NULL")
        End If
        Me.Grid.Cell(_idx_row, GRID_COLUMN_WON).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' JACKPOT_CREDITS
      _temp_obj = m_query_as_datatable.Compute("SUM(TOTAL_JACKPOT_CREDITS)", _condition & " AND TOTAL_JACKPOT_CREDITS IS NOT NULL")
      If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
        _temp_dec = 0
      Else
        _temp_dec = m_query_as_datatable.Compute("SUM(TOTAL_JACKPOT_CREDITS)", _condition & " AND TOTAL_JACKPOT_CREDITS IS NOT NULL")
      End If

      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _temp_meters_handpays = 0
      _temp_meters_handpays = _temp_meters_handpays + _temp_dec


      ' HAND_PAID_CANCELLED_CREDITS
      _temp_obj = m_query_as_datatable.Compute("SUM(TOTAL_HAND_PAID_CANCELLED_CREDITS)", _condition & " AND TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL")
      If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
        _temp_dec = 0
      Else
        _temp_dec = m_query_as_datatable.Compute("SUM(TOTAL_HAND_PAID_CANCELLED_CREDITS)", _condition & " AND TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL")
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _temp_meters_handpays = _temp_meters_handpays + _temp_dec


      ' NETWIN_COUNTERS
      If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
        _temp_dec = Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED).Value - Me.Grid.Cell(_idx_row, GRID_COLUMN_WON).Value - Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value
      Else
        _temp_dec = _temp_meters_handpays
      End If
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_COUNTERS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' DIFFERENCE
      _temp_dec = Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value - Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_COUNTERS).Value
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI)

  End Sub ' ProcesSubTotal

  Private Sub AddSubtotalWithoutValues(ByVal IdxTotalColumn As Integer)
    Dim _idx_row As Integer

    _idx_row = Me.Grid.AddRow()
    'SDS 28-10-2015
    GridColumnReIndex()
    ' Label - TOTAL
    Me.Grid.Cell(_idx_row, IdxTotalColumn).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then

      ' CREDIT_CANCEL
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' JACKPOTS: PROGRESIVES AND NOT PROGRESIVES
      Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End If

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
      ' PROGRESIVES
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVISIONED_JACKPOTS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' PROGRESIVE_PROVISIONS
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' NETWIN_TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
      ' PLAYED
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' WON
      Me.Grid.Cell(_idx_row, GRID_COLUMN_WON).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' JACKPOT_CREDITS
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_JACKPOT_CREDITS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' HAND_PAID_CANCELLED_CREDITS
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_COUNTERS
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_COUNTERS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' DIFFERENCE
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  End Sub

  ' PURPOSE: Set column order 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
    Private Sub GridColumnReIndex()

    Dim _column_offset As Integer
    _column_offset = 0

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    If m_query_as_datatable Is Nothing Then
      m_refresh_grid = True
    End If

    GRID_COLUMN_INDEX = 0
    GRID_INIT_TERMINAL_DATA = 1

    GRID_COLUMN_DENOMINATION = TERMINAL_DATA_COLUMNS + 1

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
      GRID_COLUMN_CREDIT_CANCEL = TERMINAL_DATA_COLUMNS + 2
      GRID_COLUMN_JACKPOTS = TERMINAL_DATA_COLUMNS + 3
      GRID_COLUMN_PROGRESSIVE_JACKPOT = TERMINAL_DATA_COLUMNS + 4
      GRID_COLUMN_NETWIN_TOTAL = TERMINAL_DATA_COLUMNS + 5
    Else
      GRID_COLUMN_PROGRESIVE_PROVISIONS = TERMINAL_DATA_COLUMNS + 2
      GRID_COLUMN_PROVISIONED_JACKPOTS = TERMINAL_DATA_COLUMNS + 3
      GRID_COLUMN_NETWIN_TOTAL = TERMINAL_DATA_COLUMNS + 4
    End If

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_NETWIN_REPORT Then
      GRID_COLUMN_PLAYED = TERMINAL_DATA_COLUMNS + 5
      GRID_COLUMN_WON = TERMINAL_DATA_COLUMNS + 6
    Else
      _column_offset = -2 'are decreased the columns, because, they are not necessary in the Manual pays balance report
    End If

    If m_tito_report_type = ENUM_TITO_REPORT_TYPE.TYPE_PAYS_BALANCE_REPORT Then
      GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS = TERMINAL_DATA_COLUMNS + 8 + _column_offset
      GRID_COLUMN_TOTAL_JACKPOT_CREDITS = TERMINAL_DATA_COLUMNS + 9 + _column_offset
      GRID_COLUMN_NETWIN_COUNTERS = TERMINAL_DATA_COLUMNS + 10 + _column_offset
      GRID_COLUMN_DIFFERENCE = TERMINAL_DATA_COLUMNS + 11 + _column_offset
      GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 12 + _column_offset
    Else
      GRID_COLUMN_TOTAL_JACKPOT_CREDITS = TERMINAL_DATA_COLUMNS + 7 + _column_offset
      GRID_COLUMN_TOTAL_HAND_PAID_CANCELLED_CREDITS = TERMINAL_DATA_COLUMNS + 8 + _column_offset
      GRID_COLUMN_NETWIN_COUNTERS = TERMINAL_DATA_COLUMNS + 9 + _column_offset
      GRID_COLUMN_DIFFERENCE = TERMINAL_DATA_COLUMNS + 10 + _column_offset
      GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 11 + _column_offset
    End If

  End Sub

#End Region   ' Private Functions

#Region " Events"

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region 'Events

End Class