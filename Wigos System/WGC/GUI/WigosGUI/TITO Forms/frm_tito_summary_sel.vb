'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_summary_sel
' DESCRIPTION:   TITO activity summary
' AUTHOR:        Adri�n Cuartas
' CREATION DATE: 13-DEC-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 13-DEC-2013 ACM    First Release
' 03-JAN-2014 ICS    Fixed sql query to use last action datetime for "ticket in" columns 
' 20-JAN-2014 DHA    Fixed Bug WIGOSTITO-999: Set format to the total row for columns type: currency, integer 
' 25-FEB-2014 DRV    Fixed Bug WIGOSTITO-1087: Counting tickets canceled wigos GUI
' 27-MAR-2014 LEM    Fixed Bug WIGOSTITO-1176: Wrong "redeemed in cashier" tickets info
' 23-APR-2014 ICS    Fixed Bug WIGOSTITO-1207: Handpay and Jackpot tickets aren't take in mind in tito summary form
' 12-JAN-2015 SGB    Fixed Bug WIG-1916: Format column date in export excel
' 12-FEB-2015 ANM    Fixed Bug WIG-1996: Tickets didn't match in activity report (function GUI_GetSqlCommand)
' 01-FEB-2016 DDS    Fixed Bug 8842:Ref. Ceuta - Resumen de actividad: se informa incorrectamente de los t�ckets creados en terminales de juego
' 04-JUL-2016 AMF    Bug 15123:CountR: En resumen de actividad no salen los tickets pagados por CountR
' 21-FEB-2017 LTC    PBI 22283:Winpot - Change Promotions NLS
' 22-MAR-2017 JBP    Bug 25289:Reportes contables muestran discrepancias en tickets TITO
' 22-MAR-2017 RLO    Bug 25289:Creaci�n sp con la consulta antigua
' 11-APR-2017 RAB    PBI 26539:MES10 Ticket validation - Prevent the use of dealer copy tickets (GUI)
' 26-JUL-2017 ETP    Fixed Bug WIGOS-3925 Differences in Activity summary when checking 'Date to' filter or not checking it with a future date.
' ----------  ------ -----------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_tito_summary_sel
  Inherits GUI_Controls.frm_base_sel

#Region "Constants"

  Private Const GRID_COLUMNS_COUNT As Integer = 18
  Private Const GRID_HEADERS_COUNT As Integer = 2

  ' Text format columns
  Private Const EXCEL_COLUMN_DATE As Integer = 0

  ' Index Columns
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  ' TI/TO for TITO_TICKET_TYPE
  Private Const GRID_COLUMN_TI_CASHABLE_QUANTITY As Integer = 2
  Private Const GRID_COLUMN_TI_CASHABLE_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY As Integer = 4
  Private Const GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY As Integer = 6
  Private Const GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_TOTAL_TICKET_IN As Integer = 8
  Private Const GRID_COLUMN_TO_CASHABLE_QUANTITY As Integer = 9
  Private Const GRID_COLUMN_TO_CASHABLE_AMOUNT As Integer = 10
  Private Const GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY As Integer = 11
  Private Const GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT As Integer = 12
  Private Const GRID_COLUMN_TOTAL_TICKET_OUT As Integer = 13
  Private Const GRID_COLUMN_PAID_QUANTITY As Integer = 14
  Private Const GRID_COLUMN_PAID_AMOUNT As Integer = 15
  Private Const GRID_COLUMN_GENERATED_QUANTITY As Integer = 16
  Private Const GRID_COLUMN_GENERATED_AMOUNT As Integer = 17

  ' Width
  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 1500
  Private Const GRID_WIDTH_QUANTIY_AMOUNT As Integer = 1800

  ' SQL Columns
  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_TI_CASHABLE_QUANTITY As Integer = 1
  Private Const SQL_COLUMN_TI_CASHABLE_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_TI_PROMO_REDEEM_QUANTITY As Integer = 3
  Private Const SQL_COLUMN_TI_PROMO_REDEEM_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_TI_PROMO_NONREDEEM_QUANTITY As Integer = 5
  Private Const SQL_COLUMN_TI_PROMO_NONREDEEM_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_TOTAL_TICKET_IN As Integer = 7
  Private Const SQL_COLUMN_TO_CASHABLE_QUANTITY As Integer = 8
  Private Const SQL_COLUMN_TO_CASHABLE_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_TO_PROMO_NONREDEEM_QUANTITY As Integer = 10
  Private Const SQL_COLUMN_TO_PROMO_NONREDEEM_AMOUNT As Integer = 11
  Private Const SQL_COLUMN_TOTAL_TICKET_OUT As Integer = 12
  Private Const SQL_COLUMN_PAID_QUANTITY As Integer = 13
  Private Const SQL_COLUMN_PAID_AMOUNT As Integer = 14
  Private Const SQL_COLUMN_GENERATED_QUANTITY As Integer = 15
  Private Const SQL_COLUMN_GENERATED_AMOUNT As Integer = 16

#End Region

#Region "Members"

  Private m_date_from As String
  Private m_date_to As String
  Private m_totals As Dictionary(Of Integer, Double)

#End Region

#Region "Properties"

#End Region

#Region "Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TITO_SUMMARY_SEL

    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1403)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date
    ds_from_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)

    Call SetDefaultDate()
    Call GUI_StyleSheet()

  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    ' Date
    Call SetDefaultDate()

  End Sub

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ds_from_to
  End Sub

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty

    'date from
    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'date to
    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

  End Sub

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Dates
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)

  End Sub

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _date_from As DateTime
    Dim _date_to As DateTime

    _date_from = IIf(ds_from_to.FromDateSelected, ds_from_to.FromDate, WGDB.MinDate)
    _date_to = IIf(ds_from_to.ToDateSelected, ds_from_to.ToDate, WGDB.Now.AddDays(1))

    Using _db_trx As New DB_TRX()

      Using _sql_cmd As New SqlCommand("GetTitoSummary", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

        _sql_cmd.CommandType = CommandType.StoredProcedure
        _sql_cmd.Parameters.Add("@pHour", SqlDbType.Int).Value = ds_from_to.ClosingTime
        _sql_cmd.Parameters.Add("@pCashable", SqlDbType.Int).Value = TITO_TICKET_TYPE.CASHABLE
        _sql_cmd.Parameters.Add("@pPromoRe", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_REDEEM
        _sql_cmd.Parameters.Add("@pPromoNR", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_NONREDEEM
        _sql_cmd.Parameters.Add("@pRedeemed", SqlDbType.Int).Value = TITO_TICKET_STATUS.REDEEMED
        _sql_cmd.Parameters.Add("@pPlayed", SqlDbType.Int).Value = TITO_TICKET_STATUS.CANCELED
        _sql_cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = TITO_TICKET_TYPE.HANDPAY
        _sql_cmd.Parameters.Add("@pJackpot", SqlDbType.Int).Value = TITO_TICKET_TYPE.JACKPOT
        _sql_cmd.Parameters.Add("@pChipsDealerCopy", SqlDbType.Int).Value = TITO_TICKET_TYPE.CHIPS_DEALER_COPY
        _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _date_from.ToUniversalTime()
        _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _date_to.ToUniversalTime()

        Using _sql_da As New SqlDataAdapter(_sql_cmd)

          Return _sql_cmd

        End Using

      End Using

    End Using

  End Function

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
    End Select

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

  End Sub

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If IsDBNull(DbRow.Value(SQL_COLUMN_DATE)) Then
      Return False
    End If

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    ' TICKET IN

    ' Cashable    
    ' Quantity    
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TI_CASHABLE_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TI_CASHABLE_QUANTITY))
    m_totals(GRID_COLUMN_TI_CASHABLE_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TI_CASHABLE_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TI_CASHABLE_QUANTITY))
    ' Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT))
      m_totals(GRID_COLUMN_TI_CASHABLE_AMOUNT) += DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    ' Promo Redeemable
    ' Quantity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_QUANTITY))
    m_totals(GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_QUANTITY))
    ' Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_AMOUNT))
      m_totals(GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT) += DbRow.Value(SQL_COLUMN_TI_PROMO_REDEEM_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    ' Promo NonRedeemable
    ' Quantity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_QUANTITY))
    m_totals(GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_QUANTITY))
    ' Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_AMOUNT))
      m_totals(GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT) += DbRow.Value(SQL_COLUMN_TI_PROMO_NONREDEEM_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    ' Total Ticket In
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TOTAL_TICKET_IN)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_TICKET_IN))
      m_totals(GRID_COLUMN_TOTAL_TICKET_IN) += DbRow.Value(SQL_COLUMN_TOTAL_TICKET_IN)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_IN).Value = GUI_FormatCurrency(0)
    End If

    ' TICKET OUT

    ' Cashable    
    ' Quantity    
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_CASHABLE_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TO_CASHABLE_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TO_CASHABLE_QUANTITY))
    m_totals(GRID_COLUMN_TO_CASHABLE_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TO_CASHABLE_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TO_CASHABLE_QUANTITY))
    ' Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TO_CASHABLE_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_CASHABLE_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TO_CASHABLE_AMOUNT))
      m_totals(GRID_COLUMN_TO_CASHABLE_AMOUNT) += DbRow.Value(SQL_COLUMN_TO_CASHABLE_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_CASHABLE_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    ' Promo NonRedeemable
    ' Quantity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_QUANTITY))
    m_totals(GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_QUANTITY))
    ' Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_AMOUNT))
      m_totals(GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT) += DbRow.Value(SQL_COLUMN_TO_PROMO_NONREDEEM_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    ' Total Ticket Out
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TOTAL_TICKET_OUT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_OUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_TICKET_OUT))
      m_totals(GRID_COLUMN_TOTAL_TICKET_OUT) += DbRow.Value(SQL_COLUMN_TOTAL_TICKET_OUT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_OUT).Value = GUI_FormatCurrency(0)
    End If

    ' CASHIER

    ' Paid Quantity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PAID_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_PAID_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_PAID_QUANTITY))
    m_totals(GRID_COLUMN_PAID_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_PAID_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_PAID_QUANTITY))
    ' Paid Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PAID_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAID_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PAID_AMOUNT))
      m_totals(GRID_COLUMN_PAID_AMOUNT) += DbRow.Value(SQL_COLUMN_PAID_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAID_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    ' Generated Quantity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GENERATED_QUANTITY).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GENERATED_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_GENERATED_QUANTITY))
    m_totals(GRID_COLUMN_GENERATED_QUANTITY) += IIf(IsDBNull(DbRow.Value(SQL_COLUMN_GENERATED_QUANTITY)), 0, DbRow.Value(SQL_COLUMN_GENERATED_QUANTITY))
    ' Generated Amount
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_GENERATED_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GENERATED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_GENERATED_AMOUNT))
      m_totals(GRID_COLUMN_GENERATED_AMOUNT) += DbRow.Value(SQL_COLUMN_GENERATED_AMOUNT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GENERATED_AMOUNT).Value = GUI_FormatCurrency(0)
    End If

    Return True

  End Function

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer

    _idx_row = Me.Grid.NumRows

    Me.Grid.AddRow()

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    'DHA 20-JAN-2014: Set format to the total row for columns type: currency, integer 
    ' Totals
    For Each _item As KeyValuePair(Of Integer, Double) In m_totals
      If (_item.Key = GRID_COLUMN_TI_CASHABLE_QUANTITY Or _
        _item.Key = GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY Or _
        _item.Key = GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY Or _
        _item.Key = GRID_COLUMN_TO_CASHABLE_QUANTITY Or _
        _item.Key = GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY Or _
        _item.Key = GRID_COLUMN_PAID_QUANTITY Or _
        _item.Key = GRID_COLUMN_GENERATED_QUANTITY) Then
        Me.Grid.Cell(_idx_row, _item.Key).Value = GUI_FormatNumber(_item.Value, 0)
      ElseIf (_item.Key = GRID_COLUMN_TI_CASHABLE_AMOUNT Or _
        _item.Key = GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT Or _
        _item.Key = GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT Or _
        _item.Key = GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT Or _
        _item.Key = GRID_COLUMN_GENERATED_AMOUNT Or _
        _item.Key = GRID_COLUMN_PAID_AMOUNT Or _
        _item.Key = GRID_COLUMN_TOTAL_TICKET_IN Or _
        _item.Key = GRID_COLUMN_TOTAL_TICKET_OUT Or _
        _item.Key = GRID_COLUMN_TO_CASHABLE_AMOUNT) Then
        Me.Grid.Cell(_idx_row, _item.Key).Value = GUI_FormatCurrency(_item.Value)
      Else
        Me.Grid.Cell(_idx_row, _item.Key).Value = GUI_FormatNumber(_item.Value)
      End If
    Next

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub

#End Region

#Region "Public Functions"

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub

#End Region

#Region "Private Functions"
  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultDate()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ds_from_to.FromDate = _today_opening
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening.AddDays(1)
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour

  End Sub ' SetDefaultDate

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Column(GRID_COLUMN_SELECT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TICKET IN

      ' Cashable
      .Column(GRID_COLUMN_TI_CASHABLE_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4711)
      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4711)
      ' Quantity
      .Column(GRID_COLUMN_TI_CASHABLE_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_TI_CASHABLE_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TI_CASHABLE_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Amount
      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Promo Redeemable
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4712)
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4712)
      ' Quantity
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TI_PROMO_REDEEM_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Promo NonRedeemable
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4713)
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4713)
      ' Quantity
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Amount
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TI_PROMO_NONREDEEM_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Ticket In
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3331)
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TOTAL_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TICKET OUT

      ' Cashable
      .Column(GRID_COLUMN_TO_CASHABLE_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4714)
      .Column(GRID_COLUMN_TO_CASHABLE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4714)
      ' Quantity
      .Column(GRID_COLUMN_TO_CASHABLE_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_TO_CASHABLE_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TO_CASHABLE_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Amount
      .Column(GRID_COLUMN_TO_CASHABLE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_TO_CASHABLE_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TO_CASHABLE_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Promo NonRedeemable
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4715)
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4715)
      ' Quantity
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Amount
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TO_PROMO_NONREDEEM_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Ticket Out
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3332)
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CASHIER
      ' Paid
      .Column(GRID_COLUMN_PAID_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3329)
      .Column(GRID_COLUMN_PAID_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3329)

      ' Paid Quantity
      .Column(GRID_COLUMN_PAID_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_PAID_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_PAID_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Paid Amount
      .Column(GRID_COLUMN_PAID_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_PAID_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_PAID_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Generated
      .Column(GRID_COLUMN_GENERATED_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3330)
      .Column(GRID_COLUMN_GENERATED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3330)
      ' Generated Quantity
      .Column(GRID_COLUMN_GENERATED_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_GENERATED_QUANTITY).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_GENERATED_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' Generated Amount
      .Column(GRID_COLUMN_GENERATED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_GENERATED_AMOUNT).Width = GRID_WIDTH_QUANTIY_AMOUNT
      .Column(GRID_COLUMN_GENERATED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

    ' Totals
    m_totals = New Dictionary(Of Integer, Double)
    For _index As Integer = 2 To GRID_COLUMNS_COUNT - 1 Step 1
      m_totals.Add(_index, 0)
    Next

  End Sub


#End Region

#Region "Events"

#End Region


End Class