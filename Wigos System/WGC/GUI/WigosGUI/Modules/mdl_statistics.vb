'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_statistics.vb
' DESCRIPTION:   Statistics module for Manage the Statistics
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 12-APR-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-APR-2007  MTM    Initial version.
' 30-DEC-2011  MPO    Change SELECT_USERS, exclude disabled users
' 05-FEB-2013  JCM    Change SELECT_USERS, replace deprecated GU_ENABLED by GU_BLOCK_REASON
' 28-MAR-2013  HBB    Added the constant GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
' 16-APR-2013  ICS    Added a new routine to set a combo with cashier aliases
' 30-APR-2013  DHA    Fixed Bug #757: The option list of control Cashier Terminals is not properly sorted 
' 10-MAY-2013  JCA    Added SELECT_GAMES_V2 query to use GAMES_V2 view
' 20-DEC-2013  JCA    Added SELECT_PHYSICAL_CASHIERS query to view only "Physical Cashiers", and modify SetComboCashierAlias()
' 18-FEB-2014  JBA    Added new parameter "ShowEmptyElement" to add empty element in the form's combos.
' 04-FEB-2015  ANM    Modified SetComboCashierAlias to accept an optional parameter with a SQL
' 28-JAN-2016  FOS    Product Backlog Item 7885: Add currency combo floor dual currency
'--------------------------------------------------------------------
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data
Imports WSI.Common

Public Module mdl_statistics

#Region " Constants "

  Public Const GRID_COLUMN_FIRST_WIDTH As Integer = 2600
  Public Const GRID_COLUMN_DATE_WIDTH As Integer = 1300
  Public Const GRID_COLUMN_AMOUNT_WIDTH As Integer = 1500
  Public Const GRID_COLUMN_PAYOUT_WIDTH As Integer = 1200
  Public Const GRID_COLUMN_THEORICAL_PAYOUT_WIDTH As Integer = 2000

  Public Const SELECT_SERVERS As String = "SELECT TE_TERMINAL_ID, TE_NAME FROM TERMINALS WHERE TE_TYPE='0' ORDER BY TE_NAME"
  Public Const SELECT_GAMES As String = "SELECT GM_GAME_ID, GM_NAME FROM GAMES_V ORDER BY GM_NAME"
  Public Const SELECT_GAMES_V2 As String = "SELECT GM_GAME_ID, GM_NAME FROM GAMES_V2 ORDER BY GM_NAME"
  Public Const SELECT_CASHIERS As String = "SELECT CT_CASHIER_ID, CT_NAME FROM CASHIER_TERMINALS ORDER BY CT_NAME"
  Public Const SELECT_PHYSICAL_CASHIERS As String = "SELECT CT_CASHIER_ID, CT_NAME FROM CASHIER_TERMINALS WHERE CT_TERMINAL_ID IS NULL AND CT_GAMING_TABLE_ID IS NULL ORDER BY CT_NAME"
  Public Const SELECT_CASHIERS_DUAL_CURRENCY As String = "SELECT CT_CASHIER_ID, CT_NAME FROM CASHIER_TERMINALS INNER JOIN TERMINALS ON TE_TERMINAL_ID = CT_TERMINAL_ID WHERE TE_ISO_CODE = {0}  ORDER BY CT_NAME"
  Public Const SELECT_PHYSICAL_CASHIERS_DUAL_CURRENCY As String = "SELECT CT_CASHIER_ID, CT_NAME FROM CASHIER_TERMINALS WHERE CT_TERMINAL_ID IS NULL AND CT_GAMING_TABLE_ID IS NULL  INNER JOIN TERMINALS ON TE_TERMINAL_ID = CT_TERMINAL_ID WHERE TE_ISO_CODE = {0} ORDER BY CT_NAME"
  Public Const SELECT_USERS As String = "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON=0 ORDER BY GU_USERNAME"
  Public Const SELECT_PROVIDERS As String = "SELECT DISTINCT TE_PROVIDER_ID FROM TERMINALS WHERE TE_TYPE='1' ORDER BY TE_PROVIDER_ID"
  Public Const SELECT_PROMOTION_NAMES As String = "SELECT PM_PROMOTION_ID, PM_NAME FROM PROMOTIONS ORDER BY PM_NAME"
  Public Const SELECT_MOBILE_BANKS As String = "SELECT MB_ACCOUNT_ID, CASE WHEN MB_HOLDER_NAME IS NULL THEN CAST(MB_ACCOUNT_ID as nvarchar) ELSE CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME END AS MB_NAME FROM MOBILE_BANKS ORDER BY MB_NAME"

  ' Account types stored in table accounts
  Public Const ACCOUNT_TYPE_WIN As Integer = 2

#End Region ' Constants

#Region " Members "

  Public m_select_terminals As String = "SELECT TE_TERMINAL_ID, TE_NAME FROM TERMINALS WHERE TE_TYPE='1' AND TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ") ORDER BY TE_NAME"

#End Region ' Members

#Region " Public Functions "


  ' PURPOSE : Set combo with SQL SELECT
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Public Sub SetCombo(ByVal combo As uc_combo, ByVal sql_select As String, Optional ByVal ShowEmptyElement As Boolean = False)
    Dim tabla As DataTable
    tabla = GUI_GetTableUsingSQL(sql_select, Integer.MaxValue)

    combo.Clear()

    If ShowEmptyElement Then
      combo.Add(-1, String.Empty)
    End If

    combo.Add(tabla)
  End Sub ' SetCombo

  ' PURPOSE : Set combo with game Unknown
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Public Sub SetComboGamesWithUnknown(ByVal combo As uc_combo, ByVal sql_select As String)
    Dim tabla As DataTable
    Dim _drow As DataRow()
    tabla = GUI_GetTableUsingSQL(sql_select, Integer.MaxValue)

    _drow = tabla.Select("GM_NAME = 'UNKNOWN'")
    If _drow.Length < 1 Then
      tabla.Rows.Add("-1", "UNKNOWN")
    End If
    combo.Clear()

    combo.Add(tabla)
  End Sub ' SetComboGamesWithUnknown

  ' PURPOSE : Set combo with cashier alias
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Public Sub SetComboCashierAlias(ByVal combo As uc_combo, Optional ByVal ShowOnlyPhysical As Boolean = False, _
                                  Optional ByVal ShowEmptyElement As Boolean = False, Optional ByVal psSql As String = "", Optional ByVal pIsoCode As String = "")
    Dim _table As DataTable
    Dim _new_table As DataTable
    Dim _query As String

    '3 different possible querys. 1 for parameter and the other 2 by local variable
    If psSql = "" Then
      If ShowOnlyPhysical Then
        If (WSI.Common.Misc.IsFloorDualCurrencyEnabled()) AndAlso Not String.IsNullOrEmpty(pIsoCode) Then
          _query = String.Format(SELECT_PHYSICAL_CASHIERS_DUAL_CURRENCY, pIsoCode)
        Else
          _query = SELECT_PHYSICAL_CASHIERS
        End If
      Else
        If (WSI.Common.Misc.IsFloorDualCurrencyEnabled()) AndAlso Not String.IsNullOrEmpty(pIsoCode) Then
          _query = String.Format(SELECT_CASHIERS_DUAL_CURRENCY, pIsoCode)

        Else
          _query = SELECT_CASHIERS
        End If

      End If
    Else
      _query = psSql
    End If

    _table = GUI_GetTableUsingSQL(_query, Integer.MaxValue)

    For _idx As Integer = 0 To _table.Rows.Count - 1
      _table.Rows(_idx)(1) = Computer.Alias(_table.Rows(_idx)(1))
    Next

    _new_table = _table.Clone()
    For Each _row As DataRow In _table.Select("", "CT_NAME")
      _new_table.ImportRow(_row)
    Next

    combo.Clear()

    If ShowEmptyElement Then
      combo.Add(-1, String.Empty)
    End If

    combo.Add(_new_table)
  End Sub ' SetCombo

  ' PURPOSE : Set closing hour combo
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Public Sub SetHourCombo(ByVal combo As uc_combo)
    combo.Clear()

    combo.Add(0, "0")
    combo.Add(1, "1")
    combo.Add(2, "2")
    combo.Add(3, "3")
    combo.Add(4, "4")
    combo.Add(5, "5")
    combo.Add(6, "6")
    combo.Add(7, "7")
    combo.Add(8, "8")
    combo.Add(9, "9")
    combo.Add(10, "10")
    combo.Add(11, "11")
    combo.Add(12, "12")
    combo.Add(13, "13")
    combo.Add(14, "14")
    combo.Add(15, "15")
    combo.Add(16, "16")
    combo.Add(17, "17")
    combo.Add(18, "18")
    combo.Add(19, "19")
    combo.Add(20, "20")
    combo.Add(21, "21")
    combo.Add(22, "22")
    combo.Add(23, "23")

  End Sub ' SetHourCombo


  ' PURPOSE : Return date for sql query
  '
  '  PARAMS :
  '     - INPUT :
  '           
  '     - OUTPUT :
  '
  ' RETURNS : Date

  Public Function SqlDate() As String
    Return "01-01-2007 00:00:00:000"
  End Function ' SqlDate

  Public Sub ResetCounters(ByVal PlayedAmount As Integer, ByVal WonAmount As Integer, ByVal PlayedCount As Integer, ByVal WonCount As Integer)

    PlayedAmount = 0
    WonAmount = 0
    PlayedCount = 0
    WonCount = 0

  End Sub ' ResetCounters

  ' PURPOSE: Get default closing time from table General_Params
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Function GetDefaultClosingTime() As Integer
    Return GeneralParam.GetInt32("WigosGUI", "ClosingTime", 8)
  End Function ' GetDefaultClosingTime

#End Region ' Public Functions

End Module
