'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME: mdl_globals.vb
'
'   DESCRIPTION: 
'
'        AUTHOR: Toni Jord�
'
' CREATION DATE: 14-FEB-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 14-FEB-2002 TJ    Initial version
' 10-OCT-2010 NMR   Added TITO audit code
' 14-DIC-2015 JRC   Print Terminals QR Codes
' 24-JUL-2017 ATB   PBI 28710: EGM Reserve � Settings
'-------------------------------------------------------------------

Imports GUI_CommonMisc

Module mdl_globals

  ' Audit names
  ' Must match with Audit constants on CommonDataAuditor.cpp (CommonData.dll)
  ' Please check also on mdl_globals on GUI_Controls and GUI_CommonMisc for names related to classes defined there.
  ' Changes on the current codes will make incompatible audit codes with older versions display.
  Public AUDIT_CODE_USERS As Integer = 7
  Public AUDIT_CODE_PROFILES As Integer = 8
  Public AUDIT_CODE_TERMINALS As Integer = 9
  Public AUDIT_CODE_GENERAL_PARAMS As Integer = 20
  Public AUDIT_CODE_DOWNLOAD As Integer = 23
  Public AUDIT_NAME_JACKPOT_PARAMETERS As Integer = 25
  Public AUDIT_NAME_CASHIER_CONFIGURATION As Integer = 26
  Public AUDIT_NAME_ACCOUNT As Integer = 27
  Public AUDIT_NAME_DRAWS As Integer = 28
  Public AUDIT_CODE_TERMINAL_GAME_RELATION As Integer = 29
  Public AUDIT_CODE_PLAYER_PROMOTIONS As Integer = 30
  Public AUDIT_CODE_GIFTS As Integer = 31
  Public AUDIT_NAME_SITE_JACKPOT_PARAMETERS As Integer = 32
  Public AUDIT_NAME_MAILING_PROGRAMMING As Integer = 33
  Public AUDIT_NAME_CARD_WRITER As Integer = 34
  Public AUDIT_NAME_SITES_OPERATORS As Integer = 35
  Public AUDIT_NAME_TERMINAL_RETIREMENT As Integer = 36
  Public AUDIT_NAME_ALARMS As Integer = 37
  Public AUDIT_CODE_ACCEPTORS As Integer = 38
  Public AUDIT_NAME_CASHIER_SESSIONS As Integer = 39
  Public AUDIT_CODE_MULTISITE As Integer = 40
  Public AUDIT_CODE_REPRINT_VOUCHERS As Integer = 41
  Public AUDIT_CODE_PROVIDERS As Integer = 42
  Public AUDIT_CODE_TITO_REPRINT_TICKET As Integer = 43
  Public AUDIT_CODE_USER_ACTIVITY As Integer = 44
  Public AUDIT_CODE_CAGE As Integer = 45
  Public AUDIT_CODE_GAMING_TABLES As Integer = 46
  Public AUDIT_CODE_BONUSES As Integer = 47
  Public AUDIT_CODE_PLAY_SESSIONS As Integer = 48
  Public AUDIT_CODE_PATTERNS As Integer = 49
  Public AUDIT_CODE_PROGRESSIVE_PROVISION As Integer = 50
  Public AUDIT_CODE_PROGRESSIVE As Integer = 51
  Public AUDIT_CODE_METERS As Integer = 52
  Public AUDIT_CODE_PRINT_QR_CODES As Integer = 53
  Public AUDIT_CODE_TOURNAMENTS As Integer = 54
  Public AUDIT_NAME_JACKPOT_CONFIG As Integer = 55
  Public AUDIT_NAME_JACKPOT_VIEWER_CONFIG As Integer = 56
  Public AUDIT_CODE_CREDIT_LINES As Integer = 57
  Public AUDIT_CODE_JUNKETS As Integer = 58
  Public AUDIT_TERMINALS_BOOKING As Integer = 59
  Public AUDIT_CUSTOMER_NOTICES As Integer = 60
  Public AUDIT_NAME_PROMOGAME As Integer = 61
  Public AUDIT_USERS_ASSIGNATION As Integer = 62


  Public AUDIT_EGM_METERS As Integer = 63
  Public AUDIT_BILLING As Integer = 64

  ' Variable declaration
  Public GLB_NLS_GUI_AUDITOR As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_AUDITOR)
  Public GLB_NLS_GUI_INVOICING As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_INVOICE_SYSTEM)
  Public GLB_NLS_GUI_CONFIGURATION As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONF)
  Public GLB_NLS_GUI_CONTROLS As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONTROLS)
  Public GLB_NLS_GUI_ALARMS As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_ALARMS)
  Public GLB_NLS_GUI_STATISTICS As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_STATISTICS)
  Public GLB_NLS_GUI_CLASS_II As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_CLASS_II)
  Public GLB_NLS_GUI_JACKPOT_MGR As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_JACKPOT_MGR)
  Public GLB_NLS_GUI_SW_DOWNLOAD As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_SW_DOWNLOAD)
  Public GLB_NLS_GUI_SYSTEM_MONITOR As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_SYSTEM_MONITOR)
  Public GLB_NLS_GUI_PLAYER_TRACKING As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_PLAYER_TRACKING)
  Public GLB_NLS_REPORT As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_REPORT)



  Public Enum MAGNETIC_CARD_TYPES
    CARD_TYPE_PLAYER = 0
  End Enum

End Module
