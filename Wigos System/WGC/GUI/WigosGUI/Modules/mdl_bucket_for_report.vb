﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_bucket_for_report.vb
' DESCRIPTION:   Report module for managing the reports
' AUTHOR:        Samuel González Berdugo
' CREATION DATE: 28-DEC-2015 
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-DEC-2015  SGB    Initial version.
' 25-FEB-2016  ETP    BUG 9565: Only show enabled Buckets
' 01-MAR-2016  ETP    Fixed BUG 8949: Needs to round or truncate values before add & incorrectly generated subtotals.

Imports WSI.Common
Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Data
Imports System.Text

Module mdl_bucket_for_report

#Region " Structures"
  Private Structure TYPE_BUCKET
    Dim BU_BUCKET_ID As Integer
    Dim BU_NAME As String
  End Structure

#End Region

#Region " MEMBERS "

  Dim Buckets_list As New List(Of TYPE_BUCKET)()

  ' totalizers
  Dim m_total_bucket As New List(Of Double)()
  Dim m_subtotal_bucket As New List(Of Double)()

  Dim m_pivot_columns As String '[1],[2],[3],[4],[5],[6],[7]'

  Public GRID_NUM_COLUMNS_BUCKETS As Integer

#End Region ' MEMBERS

#Region " CONSTANTS "

  'Identifies this particular report in order to know which buckets shoul be visible
  Private Const BUCKETREPORTID As Buckets.BucketVisibility = Buckets.BucketVisibility.Report_BucketsByClient '256

#End Region ' CONSTANTS

#Region " PUBLIC FUNCTIONS "

  ' PURPOSE: Define Grid Columns bucket data
  '
  '  PARAMS:
  '     - INPUT:
  '           - InitColumn : The last column before begins with holder data
  '           - Grid
  '           - ShowBucketColumn
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub StyleSheetBucketData(ByRef Grid As uc_grid, ByVal InitColumn As Integer, ByVal HeaderText As String, Optional ByVal ShowBuquetColumn As Boolean = False)

    Try
      With Grid
        For _idx As Integer = 0 To Buckets_list.Count() - 1

          Select Case Buckets_list(_idx).BU_BUCKET_ID
            Case Buckets.BucketType.RedemptionPoints
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 1700, 0)
            Case Buckets.BucketType.RankingLevelPoints
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 1700, 0)
            Case Buckets.BucketType.Credit_NR
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 1500, 0)
            Case Buckets.BucketType.Credit_RE
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 1500, 0)
            Case Buckets.BucketType.Comp1_RedemptionPoints
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 2200, 0)
            Case Buckets.BucketType.Comp2_Credit_NR
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 2000, 0)
            Case Buckets.BucketType.Comp3_Credit_RE
              .Column(InitColumn + _idx).Width = IIf(ShowBuquetColumn, 2000, 0)
          End Select

          .Column(InitColumn + _idx).Header(0).Text = HeaderText
          .Column(InitColumn + _idx).Header(1).Text = Buckets_list(_idx).BU_NAME

          .Column(InitColumn + _idx).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        Next
      End With
    Catch ex As Exception

      Log.Exception(ex)

    End Try

  End Sub ' StyleSheetBucketData

  ' PURPOSE : Select buckets for the next step take a pivot table parameters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Public Sub CountColumnsBuckets(Optional ByVal ShowBucketColumn As Boolean = False)
    Dim _sb As StringBuilder
    Dim _report_param As SqlParameter
    Dim _type_buckets As TYPE_BUCKET

    _report_param = New SqlParameter("@pReport", SqlDbType.Int)

    Buckets_list.Clear()
    m_total_bucket.Clear()
    m_subtotal_bucket.Clear()

    _sb = New StringBuilder()
    _sb.AppendLine("  SELECT   BU_BUCKET_ID                              ")
    _sb.AppendLine("         , ISNULL(BU_NAME,'')                        ")
    _sb.AppendLine("    FROM   BUCKETS                                   ")
    _sb.AppendLine("   WHERE   ((BU_VISIBLE_FLAGS & @pReport = @pReport  ")
    _sb.AppendLine("     AND   BU_ENABLED = 1)                           ")
    _sb.AppendLine("     OR    BU_BUCKET_ID = @p_bucket_pt)              ")
    _sb.AppendLine("     AND   BU_BUCKET_ID NOT IN (8,9)                 ")
    _sb.AppendLine("ORDER BY   BU_ORDER_ON_REPORTS                       ")


    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString())
          _cmd.Parameters.Add("@pReport", SqlDbType.Int).Value = BUCKETREPORTID
          _cmd.Parameters.Add("@p_bucket_pt", SqlDbType.Int).Value = Buckets.BucketId.RedemptionPoints
          Using _reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)
            While (_reader.Read())
              If Not _reader.IsDBNull(0) Then
                _type_buckets.BU_BUCKET_ID = _reader.GetInt64(0)
                _type_buckets.BU_NAME = _reader.GetString(1)
                Buckets_list.Add(_type_buckets)
                m_total_bucket.Add(0)
                m_subtotal_bucket.Add(0)
              End If
            End While
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    m_pivot_columns = ""
    For _i As Integer = 0 To Buckets_list.Count - 1
      m_pivot_columns = m_pivot_columns + "[" + Buckets_list(_i).BU_BUCKET_ID.ToString + "]"
      If (Buckets_list.Count > 1) And (_i < Buckets_list.Count - 1) Then
        m_pivot_columns = m_pivot_columns + ","
      End If
    Next

    If ShowBucketColumn Then
      GRID_NUM_COLUMNS_BUCKETS = Buckets_list.Count
    Else
      GRID_NUM_COLUMNS_BUCKETS = 0
    End If


  End Sub ' CountColumnsBuckets

  ' PURPOSE : Init subtotal values for reports.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Public Sub InitSubTotal()
    For _idx As Integer = 0 To Buckets_list.Count() - 1
      m_subtotal_bucket(_idx) = 0
    Next
  End Sub ' InitSubTotal


  ' PURPOSE : Select buckets and use in select
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Public Function BucketsFieldsSql(Optional ByVal ShowBuquetsColumn As Boolean = False) As String

    If String.IsNullOrEmpty(m_pivot_columns) Or Not ShowBuquetsColumn Then
      Return String.Empty
    Else
      Return "," & m_pivot_columns
    End If

  End Function

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '         - SelectSQL
  '         - ShowBuquetsColumn             
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Public Function BucketsJoinSql(Optional ByVal ShowBuquetsColumn As Boolean = False) As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    ' Get Select and from
    If ShowBuquetsColumn Then
      _sb.AppendLine("LEFT JOIN                                       ")
      _sb.AppendLine("           (                                    ")
      _sb.AppendLine("             SELECT   CB.CBU_CUSTOMER_ID        ")
      _sb.AppendLine("			              , CB.CBU_BUCKET_ID          ")
      _sb.AppendLine("					          , CBU_VALUE                 ")
      _sb.AppendLine("               FROM   CUSTOMER_BUCKET CB        ")
      _sb.AppendLine("           ) SRC                                ")
      _sb.AppendLine("           PIVOT                                ")
      _sb.AppendLine("           (                                    ")
      _sb.AppendLine("             SUM(CBU_VALUE)                     ")
      _sb.AppendLine("             FOR CBU_BUCKET_ID IN               ")
      _sb.AppendLine("             (                                  ")
      _sb.AppendLine(m_pivot_columns)
      _sb.AppendLine("             )                                  ")
      _sb.AppendLine("           ) PIV                                ")
      _sb.AppendLine("       ON   PIV.CBU_CUSTOMER_ID = AC_ACCOUNT_ID ")
    End If

    Return _sb.ToString()
  End Function ' BucketsJoinPivotSql

  ' PURPOSE : Sets the values of a row the older data
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow 
  '           - RowIndex
  '           - ColumnOfGrid 
  '           - ColumnOfSQL 
  '
  '     - OUTPUT :
  '           - Grid
  '
  ' RETURNS : 
  Public Function SetupRowBucketData(ByRef Grid As uc_grid, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, _
                                      ByVal RowIndex As Integer, ByVal GridColumn As Integer, ByVal SqlColumn As Integer) As Boolean

    For _idx As Integer = 0 To Buckets_list.Count() - 1

      If Not (DbRow.Value(SqlColumn + _idx) Is DBNull.Value) Then
        If (Buckets.GetBucketUnits(Buckets_list(_idx).BU_BUCKET_ID) = Buckets.BucketUnits.Money) Then
          ' format money
          Grid.Cell(RowIndex, GridColumn + _idx).Value = GUI_FormatCurrency(Math.Truncate(DbRow.Value(SqlColumn + _idx) * 100) / 100)
        End If

        If (Buckets.GetBucketUnits(Buckets_list(_idx).BU_BUCKET_ID) = Buckets.BucketUnits.Points) Then
          ' format points
          Grid.Cell(RowIndex, GridColumn + _idx).Value = GUI_FormatNumber(Math.Truncate(DbRow.Value(SqlColumn + _idx)), 0)
        End If
      End If

    Next

  End Function ' SetupRowBucketData

  ' PURPOSE : Set the Holder Columns, if are printable or not.
  '
  '  PARAMS :
  '     - INPUT :  
  '           - ColumnOfGrid : Column where it begin the holder data.
  '           - Printable : Value it say if is printabled or not
  '
  '     - OUTPUT :
  '           - Grid
  '
  ' RETURNS : 
  Public Sub BucketsColumnsPrintable(ByRef Grid As uc_grid, ByVal ColumnOfGrid As Integer, ByVal Printable As Boolean)

    For _idx_column As Integer = ColumnOfGrid To Grid.NumColumns - 1
      Grid.Column(_idx_column).IsColumnPrintable = Printable
    Next

  End Sub ' ColumnsHolderDataPrintable

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub BucketsBueforeFirstRow()
    InitTotals()
  End Sub

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub BucketsAfterLastRow(ByRef Grid As uc_grid, ByVal GridRow As Integer, ByVal GridColumn As Integer, ByVal IsTotal As Boolean)

    Dim _Total As List(Of Double)

    If (IsTotal) Then
      _Total = m_total_bucket
    Else
      _Total = m_subtotal_bucket
    End If

    For _idx As Integer = 0 To Buckets_list.Count() - 1

      If (_Total(_idx) <> 0) Then
        If (Buckets.GetBucketUnits(Buckets_list(_idx).BU_BUCKET_ID) = Buckets.BucketUnits.Money) Then
          ' format money
          Grid.Cell(GridRow, GridColumn + _idx).Value = GUI_FormatCurrency(_Total(_idx))
        End If

        If (Buckets.GetBucketUnits(Buckets_list(_idx).BU_BUCKET_ID) = Buckets.BucketUnits.Points) Then
          ' format points
          Grid.Cell(GridRow, GridColumn + _idx).Value = GUI_FormatNumber(_Total(_idx), 0)
        End If
      End If
    Next

  End Sub

  ' PURPOSE: Updates subtotals and totals variables
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub UpdateTotalizers(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, ByVal SqlColumn As Integer)

    Dim _sum_value As Double

    For _idx As Integer = 0 To Buckets_list.Count() - 1
      If Not (DbRow.Value(_idx + SqlColumn) Is DBNull.Value) Then

        If (Buckets.GetBucketUnits(Buckets_list(_idx).BU_BUCKET_ID) = Buckets.BucketUnits.Money) Then
          ' format money 
          _sum_value = Math.Truncate(DbRow.Value(_idx + SqlColumn) * 100) / 100
          m_total_bucket(_idx) += _sum_value
          m_subtotal_bucket(_idx) += _sum_value
        End If

        If (Buckets.GetBucketUnits(Buckets_list(_idx).BU_BUCKET_ID) = Buckets.BucketUnits.Points) Then
          ' format points
          _sum_value = Math.Truncate(DbRow.Value(_idx + SqlColumn))
          m_total_bucket(_idx) += _sum_value
          m_subtotal_bucket(_idx) += _sum_value
        End If

      End If
    Next

  End Sub ' UpdateTotalizers

#End Region ' PUBLIC FUNCTIONS

#Region " PRIVATE FUNCTIONS "

  ' PURPOSE: Initializes totals variables 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitTotals()
    For _idx As Integer = 0 To Buckets_list.Count() - 1
      m_total_bucket(_idx) = 0
      m_subtotal_bucket(_idx) = 0
    Next
  End Sub ' InitTotals

#End Region ' PRIVATE FUNCTIONS

End Module
