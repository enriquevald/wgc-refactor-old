'----------------------------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'----------------------------------------------------------------------------------------
'
' MODULE NAME : mdl_terminals.vb
'
' DESCRIPTION : Miscelaneous functions to manage common operations related to terminals 
'
' REVISION HISTORY:
'
' Date        Author Description
'----------------------------------------------------------------------------------------
' 04-MAR-2011        Initial version
' 08-JAN-2013        Added a function to get open play sessions of a terminal
' 01-FEB-2016  FOS   Product Backlog Item 8881: Protect terminal edition when terminal currency is changed
'----------------------------------------------------------------------------------------

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Module mdl_terminals

  '----------------------------------------------------------------------------
  ' PURPOSE : Counts the number of existing play sessions for a terminal from 
  '           a specific date
  '
  ' PARAMS :
  '     - INPUT :
  '         - TerminalId
  '         - FromDate
  '
  '     - OUTPUT :
  '         - MaxStarted
  '         - MaxFinished
  '
  ' RETURNS : Number of play sessions that were 
  '               - Not Closed 
  '               - Closed after FromDate
  '
  ' NOTES :

  Public Function CountPlaySessions(ByVal TerminalId As Int64, _
                                    ByVal FromDate As Date, _
                                    ByRef MaxStarted As Date, _
                                    ByRef MaxFinished As Date) As Integer
    Dim _str_sql As String
    Dim _data_table As DataTable
    Dim _num_play_sessions As Integer

    _num_play_sessions = 0

    ' PS_FINISHED may be NULL : in this case the comparison will return FALSE
    _str_sql = "SELECT COUNT(*) AS NUM_SESSIONS" _
                  & ", MAX (PS_STARTED) AS MAX_PS_STARTED" _
                  & ", MAX (PS_FINISHED) AS MAX_PS_FINISHED" _
              & " FROM PLAY_SESSIONS" _
             & " WHERE PS_TERMINAL_ID = " & TerminalId _
               & " AND (    ( PS_STARTED >= " & GUI_FormatDateDB(FromDate) & ")" _
                     & " OR ( PS_FINISHED >= " & GUI_FormatDateDB(FromDate) & ")" _
                    & ")"

    _data_table = GUI_GetTableUsingSQL(_str_sql, 5000)

    If Not IsNothing(_data_table) Then
      _num_play_sessions = _data_table.Rows(0).Item("NUM_SESSIONS")
      MaxStarted = IIf(IsDBNull(_data_table.Rows(0).Item("MAX_PS_STARTED")), Nothing, _data_table.Rows(0).Item("MAX_PS_STARTED"))
      MaxFinished = IIf(IsDBNull(_data_table.Rows(0).Item("MAX_PS_FINISHED")), Nothing, _data_table.Rows(0).Item("MAX_PS_FINISHED"))
    End If

    Return _num_play_sessions

  End Function ' CountPlaySessions

  '----------------------------------------------------------------------------
  ' PURPOSE : Counts the number of existing play sessions for a terminal 
  '
  ' PARAMS :
  '     - INPUT :
  '         - TerminalId
  '
  '     - OUTPUT :
  '         - MaxStarted
  '         - MaxFinished
  '
  ' RETURNS : Number of play sessions that were 
  '               - Not Closed 
  '               - Closed after FromDate
  '
  ' NOTES :

  Public Function MaxPlaySessions(ByVal TerminalId As Int64, _
                                  ByRef MaxStarted As Date, _
                                  ByRef MaxFinished As Date) As Integer
    Dim _str_sql As String
    Dim _data_table As DataTable
    Dim _num_play_sessions As Integer

    _num_play_sessions = 0

    ' PS_FINISHED may be NULL : in this case the comparison will return FALSE
    _str_sql = "SELECT COUNT(*) AS NUM_SESSIONS" _
                  + ", MAX (PS_STARTED) AS MAX_PS_STARTED" _
                  + ", MAX (PS_FINISHED) AS MAX_PS_FINISHED" _
              + " FROM PLAY_SESSIONS" _
             + " WHERE PS_TERMINAL_ID = " & TerminalId

    _data_table = GUI_GetTableUsingSQL(_str_sql, 5000)

    If Not IsNothing(_data_table) Then
      _num_play_sessions = _data_table.Rows(0).Item("NUM_SESSIONS")
      MaxStarted = IIf(IsDBNull(_data_table.Rows(0).Item("MAX_PS_STARTED")), Nothing, _data_table.Rows(0).Item("MAX_PS_STARTED"))
      MaxFinished = IIf(IsDBNull(_data_table.Rows(0).Item("MAX_PS_FINISHED")), Nothing, _data_table.Rows(0).Item("MAX_PS_FINISHED"))
    End If

    Return _num_play_sessions

  End Function ' MaxPlaySessions

  Public Function SuggestedRetirementDate(ByVal TerminalId As Int64) As Date

    Dim _max_started As Date
    Dim _max_finished As Date
    Dim _num_play_sessions As Integer
    Dim _retirement_date As Date

    _num_play_sessions = MaxPlaySessions(TerminalId, _
                                         _max_started, _
                                         _max_finished)

    If _num_play_sessions < 1 Then
      ' No play sessions => Current working day (truncated)
      _retirement_date = WSI.Common.Misc.TodayOpening
    Else
      If _max_started = Nothing Then
        _retirement_date = WSI.Common.Misc.TodayOpening
      Else
        If _max_finished = Nothing Then
          _retirement_date = _max_started
        Else
          If DateTime.Compare(WSI.Common.Misc.Opening(_max_started), _
                              WSI.Common.Misc.Opening(_max_finished)) > 0 Then
            _retirement_date = _max_started
          Else
            _retirement_date = _max_finished
          End If
        End If
      End If

      _retirement_date = WSI.Common.Misc.Opening(_retirement_date.AddDays(1))
    End If

    ' Truncate time
    Return CDate(_retirement_date.ToShortDateString())

  End Function    ' SuggestedRetirementDate

  ' PURPOSE: Trim all String values of the DataRows.
  '
  '  PARAMS:
  '     - INPUT :
  '           - Rows As DataRow()
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Sub TrimDataValues(ByVal Rows As DataRow(), Optional ByVal DoAcceptChanges As Boolean = False)
    Dim _str_value As String
    Dim _trim_value As String
    Dim _old_value As String
    Dim _value As Object
    Dim _is_different As Boolean

    For Each _row As DataRow In Rows
      _is_different = False

      For Each _col As DataColumn In _row.Table.Columns

        _value = _row(_col)

        If IsDBNull(_value) Then
          If _row.RowState = DataRowState.Modified AndAlso Not IsDBNull(_row(_col, DataRowVersion.Original)) Then
            _is_different = True
          End If
          Continue For
        End If

        If Not TypeOf _value Is String Then
          If _col.Caption = "TGT_PAYOUT_IDX" Then
            If IsDBNull(_row(_col, DataRowVersion.Original)) Then
              _is_different = True
            ElseIf _row.RowState = DataRowState.Modified And _value <> _row(_col, DataRowVersion.Original) Then
              _is_different = True
            End If
            Continue For
          Else
            Continue For
          End If
        End If

        _str_value = CType(_value, String)
        If Not String.IsNullOrEmpty(_str_value) Then
          _trim_value = _str_value.Trim()
          If Not _col.ReadOnly Then
            _row(_col) = _trim_value
          End If

          If _row.RowState = DataRowState.Modified AndAlso Not IsDBNull(_row(_col, DataRowVersion.Original)) Then
            _old_value = CType(_row(_col, DataRowVersion.Original), String).Trim()
          Else
            _old_value = Nothing
          End If
          If _old_value <> _trim_value Then
            _is_different = True
          End If
        End If
      Next

      If DoAcceptChanges And Not _is_different Then
        _row.AcceptChanges()
      End If
    Next

  End Sub ' TrimDataValues

  '----------------------------------------------------------------------------
  ' PURPOSE : Gets a list of existing open play sessions for a terminal
  '
  ' PARAMS :
  '     - INPUT :
  '         - TerminalId
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - List of open play sessions
  '
  Public Function GetOpenPlaySessions(ByVal TerminalId As Int64) As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)
    Dim _str_sql As String
    Dim _data_table As DataTable
    Dim _play_session As CLASS_PLAY_SESSION.TYPE_PLAY_SESSION
    Dim _play_session_list As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)

    _play_session_list = New List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)()

    _str_sql = "SELECT PS_ACCOUNT_ID " _
                  & ", PS_PLAY_SESSION_ID " _
                  & ", PS_FINAL_BALANCE " _
                  & ", TE_NAME " _
                  & ", TE_TERMINAL_TYPE " _
              & " FROM PLAY_SESSIONS" _
         & " LEFT JOIN TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID " _
             & " WHERE PS_TERMINAL_ID = " & TerminalId _
               & " AND (    ( PS_STATUS = " & WSI.Common.PlaySessionStatus.Opened & ")" _
                    & ")"

    _data_table = GUI_GetTableUsingSQL(_str_sql, 5000)

    For Each _row As DataRow In _data_table.Rows
      _play_session = New CLASS_PLAY_SESSION.TYPE_PLAY_SESSION
      _play_session.account_id = _row.Item("PS_ACCOUNT_ID")
      _play_session.session_id = _row.Item("PS_PLAY_SESSION_ID")
      _play_session.final_balance = _row.Item("PS_FINAL_BALANCE")
      _play_session.terminal_id = TerminalId
      _play_session.terminal_name = _row.Item("TE_NAME")
      _play_session.terminal_type = _row.Item("TE_TERMINAL_TYPE")

      _play_session_list.Add(_play_session)
    Next

    Return _play_session_list

  End Function 'GetOpenPlaySessions

  '----------------------------------------------------------------------------
  ' PURPOSE : Manual closing of open play sessions when terminal is retired
  '
  ' PARAMS :
  '     - INPUT :
  '         - List of play sessions to close
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '         - True: If it could close all sessions
  '         - False: Otherwise
  '
  Public Function ClosePlaySessions(ByVal PlaySessions As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)) As Integer
    Try
      For Each PlaySession As CLASS_PLAY_SESSION.TYPE_PLAY_SESSION In PlaySessions
        If WSI.Common.CardData.DB_PlaySessionCloseManually(PlaySession.account_id, _
                                                           PlaySession.session_id, _
                                                           PlaySession.final_balance, _
                                                           PlaySession.terminal_id, _
                                                           PlaySession.terminal_name, _
                                                           PlaySession.terminal_type, _
                                                           CurrentUser.Name + "@" + Environment.MachineName, _
                                                           False, _
                                                           0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4444)) Then

          Return True
        End If
      Next

    Catch ex As Exception
    End Try

    Return False

  End Function 'ClosePlaySessions

  '----------------------------------------------------------------------------
  ' PURPOSE : Indicates if the terminal has had activity
  '
  ' PARAMS :
  '     - INPUT :
  '         - TerminalId
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - Boolean
  '
  Public Function TerminalHadActivity(ByVal TerminalId As Integer) As Boolean
    Dim _sb As StringBuilder
    Dim _return As Boolean
    Dim _data As Object


    Using DbTrx As New DB_TRX()

      _sb = New StringBuilder()
      _return = False
      _data = Nothing

      _sb.AppendLine("     SELECT TOP 1 PS_TERMINAL_ID ")
      _sb.AppendLine("       FROM PLAY_SESSIONS")
      _sb.AppendLine(" INNER JOIN TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID")
      _sb.AppendLine("     WHERE PS_TERMINAL_ID = @pTerminalId")

      Try

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction)
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId
          _data = _cmd.ExecuteScalar()

          If Not IsNothing(_data) AndAlso _data IsNot DBNull.Value Then

            _return = True
          End If

        End Using
      Catch _ex As Exception

        WSI.Common.Log.Exception(_ex)
      End Try
    End Using

    Return _return

  End Function 'TerminalHadActivity

End Module
