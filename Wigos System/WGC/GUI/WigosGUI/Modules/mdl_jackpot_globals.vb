'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_globals.vb
' DESCRIPTION:   General definitions file for Gui Jackpot Manager
' AUTHOR:        Agust� Poch
' CREATION DATE: 29-NOV-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 29-NOV-2002  APB    Initial version
' 10-OCT-2005  JMP    Changes in ENUM_JACKPOT_STATUS
'-------------------------------------------------------------------

Imports GUI_CommonOperations
Imports System.Data.SqlClient


Module mdl_jackpot_globals


  ' Maximum number of rows accepted in the data grid
  Public Const MAX_NUM_ROWS As Integer = 1000

  Public Const MAX_JACKPOT_CONF As Integer = 9

  Public Const DEFAULT_MAX_DECIMALS As Integer = 2

  Public Const MAX_LIMIT_INITIAL_AMOUNT_VALUE As Double = 9999999999999.99
  Public Const MAX_LIMIT_ADD_JACKPOT_AMOUNT_VALUE As Double = 9999999999999.99
  Public Const MIN_LIMIT_ADD_JACKPOT_AMOUNT_VALUE As Double = 0.0
  Public Const MAX_LIMIT_CONTRIBUTION As Double = 100.0
  Public Const MAX_LIMIT_PCT_DIGIT As Integer = 5
  Public Const MAX_DAYS_DIGITS As Integer = 3
  Public Const MAX_AMOUNT_DIGITS As Integer = 16
  Public Const MIN_DIFF_MINUTES As Integer = 0
  Public Const MIN_DIFF_ASSIGN As Integer = 1
  Public Const MAX_DIGITS_TIMEOUTS As Integer = 3
  Public Const MAX_SECONDS_DIGITS As Integer = 3

  Public Const INITIAL_ADD_JACKPOT_AMOUNT_VALUE As Double = 0.0

  Public Const MIN_CONFIRMATION_TIMEOUT As Integer = 60
  Public Const MIN_AWARDING_TIMEOUT As Integer = 1
  Public Const MIN_BROADCAST_PERIOD As Integer = 1
  Public Const MIN_AMOUNT_MINIM_AWARDING As Integer = 0
  Public Const MIN_AMOUNT_MAXIM_AWARDING As Integer = 1

  Public Const SECONDS_X_DAY As Integer = 86400
  Public Const SECONDS_X_HOUR As Integer = 3600
  Public Const SECONDS_X_MINUTE As Integer = 60

  Public Const MAIN_JACKPOT_ROW As Integer = 0

  Public Enum ENUM_ORDER
    JACKPOT01 = 1
    JACKPOT02 = 2
    JACKPOT03 = 3
    JACKPOT04 = 4
    JACKPOT05 = 5
    JACKPOT06 = 6
    JACKPOT07 = 7
    JACKPOT08 = 8
    JACKPOT09 = 9
  End Enum

  Public Enum ENUM_DB_RC
    DB_RC_OK = 0
    DB_RC_ERROR = 1
    DB_RC_ERROR_CONTROL_BLOCK = 2
    DB_RC_DB_NOT_CONNECTED = 3
    DB_RC_ERROR_DB = 4
    DB_RC_ERROR_NOT_FOUND = 5
    DB_RC_ERROR_REGISTRY = 6
    DB_RC_ERROR_CONTEXT = 7
  End Enum

  Public Enum ENUM_DB_JACKPOT_STATUS
    'JACKPOT_ACTIVE = 0
    'JACKPOT_READY = 1
    'JACKPOT_ASSIGNED = 2
    'JACKPOT_NOTIFIED = 3
    'JACKPOT_CONCEDED = 4
    'JACKPOT_CONFIRMED = 5
    'JACKPOT_INDOUBT = 6
    CONFIRMED = 6
    INDOUBT = 7
  End Enum

  Public Enum ENUM_BOOLEAN
    BOOLEAN_NO = 0
    BOOLEAN_YES = 1
  End Enum

#Region " Public Functions "

  ' PURPOSE: Calculate last month played amount.
  '
  '  PARAMS:
  '     - INPUT:
  '           - OnlyRedeemable
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Public Function CalculateLastMonthPlayed(ByVal OnlyRedeemable As Boolean, _
                                           ByVal OnlyWSIGames As Boolean) As Double
    Dim sql_connection As SqlConnection
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim str_field_sql As String
    Dim value As Object
    Dim played As Double

    played = 0
    sql_connection = Nothing

    Try
      sql_connection = NewSQLConnection()

      If Not (sql_connection Is Nothing) Then
        ' Connected
        If sql_connection.State <> ConnectionState.Open Then
          ' Connecting ..
          sql_connection.Open()
        End If

        If Not sql_connection.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      If OnlyRedeemable Then
        str_field_sql = "PS_REDEEMABLE_PLAYED"
      Else
        str_field_sql = "PS_TOTAL_PLAYED"
      End If

      str_sql = "SELECT   SUM (" & str_field_sql & ")" & _
                "  FROM   PLAY_SESSIONS WITH (INDEX (IX_PS_FINISHED_STATUS) ) "

      If OnlyWSIGames Then
        str_sql = str_sql & " WHERE   PS_FINISHED >= DATEADD (DAY, -30, GETDATE()) " & _
                            " AND   PS_TERMINAL_ID IN ( " & _
                            "                          SELECT   DISTINCT TGT_TERMINAL_ID " & _
                            "                            FROM   TERMINAL_GAME_TRANSLATION " & _
                            "                                 , GAMES " & _
                            "                           WHERE   TGT_SOURCE_GAME_ID = GM_GAME_ID " & _
                            "                             AND   GM_NAME LIKE 'WSI - %'" & _
                            "                         ) " & _
                            " AND   PS_TERMINAL_ID IN ( " & _
                            "                          SELECT   TE_TERMINAL_ID " & _
                            "                            FROM   TERMINALS " & _
                            "                           WHERE   TE_TERMINAL_TYPE = " & WSI.Common.TerminalTypes.WIN & _
                            "                         ) "
      Else
        str_sql = str_sql & " INNER JOIN TERMINAL_GROUPS ON (TG_TERMINAL_ID = PS_TERMINAL_ID AND TG_ELEMENT_ID = 1 AND TG_ELEMENT_TYPE = 4) " & _
                            " WHERE   PS_FINISHED >= DATEADD (DAY, -30, GETDATE()) "
      End If

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = sql_connection

      value = sql_command.ExecuteScalar()
      If value IsNot Nothing And value IsNot DBNull.Value Then
        played = value
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      If sql_connection IsNot Nothing Then
        sql_connection.Close()
        sql_connection.Dispose()
      End If

    End Try

    Return played
  End Function ' CalculateLastMonthPlayed

  ' PURPOSE: Calculate the frequency of the jackpots.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Function CalculateFrequency(ByVal LastMonthPlayed As Decimal, _
                                     ByVal ContributionPct As Double, _
                                     ByVal InstanceContributionPct As Double, _
                                     ByVal InstanceAverage As Double) As String
    Dim individual_contrib As Double
    Dim period As Decimal
    Dim freq As Decimal
    Dim freq_str As String
    Dim freq_unit As String
    Dim weeks_per_month As Decimal

    freq = 0
    Try
      individual_contrib = ((1 * (ContributionPct / 100)) * (InstanceContributionPct / 100))
      If individual_contrib <> 0 Then
        ' Possible OverflowException when individual_contrib is very small
        period = InstanceAverage / individual_contrib
        If period <> 0 Then
          freq = LastMonthPlayed / period
        End If
      End If
    Catch ' OverflowException
    End Try

    weeks_per_month = 30 / 7

    If freq >= 30 Then
      freq_unit = GLB_NLS_GUI_JACKPOT_MGR.GetString(341)
      freq = freq / 30
    ElseIf freq >= weeks_per_month Then
      freq_unit = GLB_NLS_GUI_JACKPOT_MGR.GetString(342)
      freq = freq / weeks_per_month
    ElseIf freq < 1 Then
      freq_unit = GLB_NLS_GUI_JACKPOT_MGR.GetString(344)
      freq = freq * 12
    Else
      freq_unit = GLB_NLS_GUI_JACKPOT_MGR.GetString(343)
    End If

    freq_str = GUI_FormatNumber(freq, 2) & " " & freq_unit

    Return freq_str
  End Function ' CalculateFrequency


#End Region ' Public Functions

End Module
