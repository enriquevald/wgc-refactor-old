'----------------------------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'----------------------------------------------------------------------------------------
'
' MODULE NAME : mdl_cash_report.vb
'
' DESCRIPTION : Functions to manage common operations related to cash reports:
'               - Cuadratura de Estadisticas
'               - Estadisticas de Caja
'
' REVISION HISTORY:
'
' Date        Author Description
'----------------------------------------------------------------------------------------
' 29-MAY-2012 JML    Initial version
' 01-JUN-2012 RCI    Fixed Bug #311: Take in account the expired prizes in expired credits category ("G")
' 22-JUN-2012 JML    Fixed Bug #320: Problem with Date.MinValue 
' 25-JUN-2012 RCI    Changed routine CalculateTotalAssets(): Don't need to use BalanceParts(), access the (Not)Redeem parts directly.
' 07-JUL-2012 JAB    Get the value "result_cashier_only_a"
' 08-AUG-2013 ICS & RCI   Fixed Bug WIG-103: Imbalance due bank card commissions
' 11-OCT-2013 AJQ & DDM & RCI    UNR Taxes as Other Income
' 13-FEB-2014 DLL    Fixed Bug WIG-618: gambling tables not showed at providers activity
' 18-FEB-2014 MPO & DHA WIGOSTITO-1080: Open play sessions (D) show only Cash In.
' 26-MAR-2014 RCI    Fixed Bug WIGOSTITO-1174: Total Assets for TITO is not correclty calculated
' 28-MAR-2014 ICS    Fixed Bug WIGOSTITO-1177: Incongruence of data in the GUI for the expiration of tickets
' 24-APR-2014 DLL    Fixed Bug WIG-849: Error in ApplyExchange function, need one parameter more
' 16-MAY-2014 DLL    Fixed Bug WIG-924: Delete ApplyExchange function. Values save in CM_AUX_AMOUNT
' 23-OCT-2014 FJC    Fixed Bug WIG-1566: Add Provision to Progressive in statistics (column L)
' 27-OCT-2014 FJC    (ReOpen) Fixed Bug WIG-1566: Add Provision to Progressive in statistics (column L)
' 19-DEC-2014 RCI    Fixed Bug WIG-1860: Open PlaySessions (column D) still in SQL query (must remove in Cashless)
' 19-DEC-2014 RCI    Fixed Bug WIG-1862: Open PlaySessions (column D) still in SQL query (must remove in Cashless)
' 16-FEB-2015 DHA    Fixed Bug WIG-2051: Only the commissions of A are considered
' 19-JUN-2015 AMF    Backlog Item 2117
' 22-JAN-2016 ETP    Fixed Bug 8576: added ps_aux_ft_nr_cash_in to NR credits.
' 16-FEB-2015 FAV    Fixed Bug 9347 Added machine tickets voided 
' 28-APR-2016 FGB    Product Backlog Item 9758: Tables (Phase 1): Review table reports 
' 14-SEP-2016  FAV   Fixed Bug 17641 Timeout in request
' 09-SEP-2016 FAV    Product Backlog Item 16708: Liabilities in accounes and liabilities in machine.
' 26-ABR-2018 FOS    Bug: WIGOS-10147 Fallo: Actividad de Proveedores/Caja
'----------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common

Module mdl_cash_report

#Region "Enums"

  Public Enum ENUM_LIABILITIES_MODE
    APPLY_TO_ACCOUNT = 0
    APPLY_TO_MACHINE = 1
  End Enum
#End Region

#Region " Constants "

  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_PROVIDER_ACTIVITY_TOTAL As Integer = 1
  Private Const SQL_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE As Integer = 2
  Private Const SQL_COLUMN_PLAY_SESSIONS_OPEN As Integer = 3
  Private Const SQL_COLUMN_CREDITS_EXPIRED As Integer = 4
  Private Const SQL_COLUMN_HALL_PAYMENTS As Integer = 5
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION As Integer = 6
  Private Const SQL_COLUMN_PROGRESSIVE_RESERVE As Integer = 7
  Private Const SQL_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE As Integer = 8
  Private Const SQL_COLUMN_CASH_RESULTS As Integer = 9
  Private Const SQL_COLUMN_CARD_DEPOSITS As Integer = 10
  Private Const SQL_COLUMN_CONVERTED_CREDIT As Integer = 11
  Private Const SQL_COLUMN_REDEEMABLE_CIRCULATION_INCREASE As Integer = 12
  Private Const SQL_COLUMN_TOTAL_REDEEMABLE As Integer = 13
  Private Const SQL_COLUMN_TOTAL_NO_REDEEMABLE As Integer = 14

#End Region ' Constants

#Region " Structures "

  Public Structure TYPE_PROVIDER_CASH_ITEM
    Dim cash_date As String
    Dim provider_activity_total As Decimal
    Dim provider_activity_no_redeemable As Decimal
    Dim provider_activity_redeemable As Decimal
    Dim play_sessions_open As Decimal
    Dim cash_results As Decimal
    Dim other_income As Decimal
    Dim credits_expired As Decimal
    Dim converted_credit As Decimal
    Dim hall_payments As Decimal
    Dim progressive_provision As Decimal
    Dim progressive_reserve As Decimal
    Dim redeemable_circulation_increase As Decimal
    Dim gaming_table_circulation As Decimal
    Dim gaming_table_open As Decimal
    Dim machine_tickets_voided As Decimal

  End Structure

#End Region ' Structures

#Region " Private Functions "

  ' PURPOSE: Get Sql Query Daily data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String with SQL sentence
  '
  Private Function GetQueryData(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal BaseDate As String) As String
    Dim _str_sql As String

    If DateFrom = Date.MinValue Then
      DateFrom = New Date(2007, 1, 1)
    End If

    If DateTo = Date.MaxValue Then
      DateTo = WSI.Common.Misc.TodayOpening().AddDays(1)
    End If

    _str_sql = "SELECT   ISNULL(DATE_CASH, GETDATE())                                                     AS DATE_CASH " & _
               "       , SUM(A)                                                                           AS A    " & _
               "       , SUM(B)                                                                           AS B    " & _
               "       , SUM(D)                                                                           AS D    " & _
               "       , SUM(G)                                                                           AS G    " & _
               "       , SUM(I)                                                                           AS I    " & _
               "       , SUM(L)                                                                           AS L    " & _
               "       , SUM(M)                                                                           AS M    " & _
               "  FROM   ( "


    _str_sql = _str_sql & _
               "SELECT   " & GUI_FormatDateDB(DateFrom) & "                                               AS DATE_CASH " & _
               "       , 0                                                                                AS A    " & _
               "       , 0                                                                                AS B    " & _
               "       , 0                                                                                AS D    " & _
               "       , 0                                                                                AS G    " & _
               "       , 0                                                                                AS I    " & _
               "       , 0                                                                                AS L    " & _
               "       , 0                                                                                AS M    " & _
        "UNION ALL " & _
               "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24, '" & BaseDate & "') AS DATE_CASH " & _
               "       , SUM(ISNULL(PS_TOTAL_CASH_IN, 0)          - ISNULL(PS_TOTAL_CASH_OUT, 0))         AS A    " & _
               "       , SUM(ISNULL(PS_NON_REDEEMABLE_CASH_IN, 0) + ISNULL(PS_AUX_FT_NR_CASH_IN, 0) - ISNULL(PS_NON_REDEEMABLE_CASH_OUT,0)) AS B    "

    ' MPO & DHA 18-FEB-2014 Add "Promo not redeemable ticket out" and "Redeemable ticket out" for "open play sessions"(D), only show cash_in (RE, NR) amount in terminals
    _str_sql = _str_sql & _
               "       , 0                                                                                AS D    " & _
               "       , 0                                                                                AS G    " & _
               "       , 0                                                                                AS I    " & _
               "       , 0                                                                                AS L    " & _
               "       , 0                                                                                AS M    " & _
               "  FROM   PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status)), TERMINALS " & _
               " WHERE   TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID " & _
               "   AND   TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ")" & _
               "   AND   PS_FINISHED >= " & GUI_FormatDateDB(DateFrom) & " " & _
               "   AND   PS_FINISHED <  " & GUI_FormatDateDB(DateTo) & " " & _
               "   AND   PS_STATUS <> 0 " & _
               "   AND   PS_PROMO = 0 " & _
               " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24 "

    ' RCI 19-DEC-2014: In TITO mode, Open PlaySessions are shown. Not in Cashless.

    If WSI.Common.TITO.Utils.IsTitoMode() Then
      _str_sql = _str_sql & _
        "UNION ALL " & _
                 "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', PS_STARTED)/24, '" & BaseDate & "')  AS DATE_CASH " & _
                 "       , 0                                                                                AS A    " & _
                 "       , 0                                                                                AS B    " & _
                 "       , SUM(ISNULL(PS_TOTAL_CASH_IN, 0) - ISNULL(PS_TOTAL_CASH_OUT, 0) + ISNULL(PS_RE_TICKET_OUT, 0) + ISNULL(PS_PROMO_NR_TICKET_OUT, 0)) AS D    " & _
                 "       , 0                                                                                AS G    " & _
                 "       , 0                                                                                AS I    " & _
                 "       , 0                                                                                AS L    " & _
                 "       , 0                                                                                AS M    " & _
                 "  FROM   PLAY_SESSIONS WITH (INDEX(IX_ps_started)), TERMINALS " & _
                 " WHERE   TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID " & _
                 "   AND   TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ")" & _
                 "   AND   PS_STARTED >= " & GUI_FormatDateDB(DateFrom) & " " & _
                 "   AND   PS_STARTED <  " & GUI_FormatDateDB(DateTo) & " " & _
                 "   AND   PS_STATUS = 0 " & _
                 "   AND   PS_PROMO = 0 " & _
                 " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', PS_STARTED)/24"
    End If

    _str_sql = _str_sql & _
          "UNION ALL " & _
                 "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', AM_DATETIME)/24, '" & BaseDate & "') AS DATE_CASH " & _
                 "       , 0                                                                                AS A    " & _
                 "       , 0                                                                                AS B    " & _
                 "       , 0                                                                                AS D    " & _
                 "       , SUM(ISNULL(AM_SUB_AMOUNT, 0))                                                    AS G    " & _
                 "       , 0                                                                                AS I    " & _
                 "       , 0                                                                                AS L    " & _
                 "       , 0                                                                                AS M    " & _
                 "  FROM   ACCOUNT_MOVEMENTS " & _
                 " WHERE   AM_TYPE IN (" & MovementType.CreditsExpired & ", " & MovementType.PrizeExpired & ") " & _
                 "   AND   AM_DATETIME >= " & GUI_FormatDateDB(DateFrom) & " " & _
                 "   AND   AM_DATETIME <  " & GUI_FormatDateDB(DateTo) & " " & _
                 " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', AM_DATETIME)/24 " & _
          "UNION ALL " & _
                 "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', TI_LAST_ACTION_DATETIME)/24, '" & BaseDate & "')  AS DATE_CASH " & _
                 "       , 0                                                                                AS A    " & _
                 "       , 0                                                                                AS B    " & _
                 "       , 0                                                                                AS D    " & _
                 "       , SUM(ISNULL(TI_AMOUNT, 0))                                                        AS G    " & _
                 "       , 0                                                                                AS I    " & _
                 "       , 0                                                                                AS L    " & _
                 "       , 0                                                                                AS M    " & _
                 "  FROM   TICKETS " & _
                 " WHERE   TI_STATUS = " & TITO_TICKET_STATUS.EXPIRED & " " & _
                 "   AND   TI_LAST_ACTION_DATETIME >= " & GUI_FormatDateDB(DateFrom) & " " & _
                 "   AND   TI_LAST_ACTION_DATETIME <  " & GUI_FormatDateDB(DateTo) & " " & _
                 "   AND   TI_TYPE_ID NOT IN ( " & TITO_TICKET_TYPE.PROMO_NONREDEEM & " ) " & _
                 " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', TI_LAST_ACTION_DATETIME)/24 " & _
          "UNION ALL " & _
                 "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24, '" & BaseDate & "') AS DATE_CASH " & _
                 "       , 0                                                                                AS A    " & _
                 "       , 0                                                                                AS B    " & _
                 "       , 0                                                                                AS D    " & _
                 "       , 0                                                                                AS G    " & _
                 "       , SUM(ISNULL(PS_REDEEMABLE_CASH_OUT, 0) - ISNULL(PS_REDEEMABLE_CASH_IN, 0) )       AS I    " & _
                 "       , 0                                                                                AS L    " & _
                 "       , 0                                                                                AS M    " & _
                 "  FROM   PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status)), TERMINALS " & _
                 " WHERE   TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID " & _
                 "   AND   TE_TERMINAL_TYPE IN ( " & TerminalTypes.SITE & ", " & TerminalTypes.SITE_JACKPOT & ") " & _
                 "   AND   PS_FINISHED >= " & GUI_FormatDateDB(DateFrom) & " " & _
                 "   AND   PS_FINISHED <  " & GUI_FormatDateDB(DateTo) & " " & _
                 "   AND   PS_STATUS <> 0 " & _
                 "   AND   PS_PROMO = 0  " & _
                 " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24 " & _
          "UNION ALL " & _
                 "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24, '" & BaseDate & "') AS DATE_CASH " & _
                 "       , 0                                                                                AS A    " & _
                 "       , 0                                                                                AS B    " & _
                 "       , 0                                                                                AS D    " & _
                 "       , 0                                                                                AS G    " & _
                 "       , 0                                                                                AS I    " & _
                 "       , SUM(ISNULL(PS_FINAL_BALANCE, 0)-ISNULL(PS_INITIAL_BALANCE, 0))                   AS L    " & _
                 "       , 0                                                                                AS M    " & _
                 "  FROM   PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status)), TERMINALS " & _
                 " WHERE   TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID " & _
                 "   AND   TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ")" & _
                 "   AND   PS_FINISHED >= " & GUI_FormatDateDB(DateFrom) & " " & _
                 "   AND   PS_FINISHED <  " & GUI_FormatDateDB(DateTo) & " " & _
                 "   AND   PS_STATUS <> 0 " & _
                 "   AND   PS_TYPE = " & PlaySessionType.PROGRESSIVE_PROVISION & _
                 "   AND   PS_PROMO = 0  " & _
                 " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24  " & _
      "UNION ALL " & _
                 "SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24, '" & BaseDate & "') AS DATE_CASH " & _
                 "       , 0                                                                                AS A    " & _
                 "       , 0                                                                                AS B    " & _
                 "       , 0                                                                                AS D    " & _
                 "       , 0                                                                                AS G    " & _
                 "       , 0                                                                                AS I    " & _
                 "       , 0                                                                                AS L    " & _
                 "       , SUM(ISNULL(PS_INITIAL_BALANCE, 0)-ISNULL(PS_FINAL_BALANCE, 0))                   AS M    " & _
                 "  FROM   PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status)), TERMINALS " & _
                 " WHERE   TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID " & _
                 "   AND   TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ")" & _
                 "   AND   PS_FINISHED >= " & GUI_FormatDateDB(DateFrom) & " " & _
                 "   AND   PS_FINISHED <  " & GUI_FormatDateDB(DateTo) & " " & _
                 "   AND   PS_STATUS <> 0 " & _
                 "   AND   PS_TYPE = " & PlaySessionType.PROGRESSIVE_RESERVE & _
                 "   AND   PS_PROMO = 0  " & _
                 " GROUP BY DATEDIFF(HOUR, '" & BaseDate & "', PS_FINISHED)/24  "

    _str_sql = _str_sql & _
               "         ) AS DATA_CASH " & _
               " GROUP BY ISNULL(DATE_CASH, GETDATE()) " & _
               " ORDER BY ISNULL(DATE_CASH, GETDATE()) " & _
               " ; "

    Return _str_sql
  End Function ' GetQueryData

  Private Function GetAmountAndChipsQuery(ByVal DateFrom As Date, ByVal DateTo As Date) As String
    Dim _sb As System.Text.StringBuilder
    Dim _filter_chips As String
    Dim _filter_amount As String
    Dim _list_currency_iso_type As List(Of CurrencyIsoType)
    Dim _currency_iso_type As CurrencyIsoType
    Dim _national_currency As String

    _sb = New System.Text.StringBuilder()

    If DateFrom = Date.MinValue Then
      DateFrom = New Date(2007, 1, 1)
    End If

    If DateTo = Date.MaxValue Then
      DateTo = WSI.Common.Misc.TodayOpening().AddDays(1)
    End If

    'FGB 2016-APR-22 We change the filter so it includes the local currency redeemable chips
    _national_currency = CurrencyExchange.GetNationalCurrency()
    _list_currency_iso_type = New List(Of CurrencyIsoType)
    _currency_iso_type = New CurrencyIsoType(_national_currency, CurrencyExchangeType.CASINO_CHIP_RE)
    _list_currency_iso_type.Add(_currency_iso_type)

    _filter_chips = FeatureChips.GetSqlFilterForChipsType(_list_currency_iso_type)
    _filter_amount = FeatureChips.GetSqlFilterForAmount()

    _sb.AppendLine(" SELECT   0 ")
    _sb.AppendLine("        , CS_STATUS ")
    _sb.AppendLine("        , GT_HAS_INTEGRATED_CASHIER ")
    _sb.AppendLine("        , CHIPS_IN  - CHIPS_OUT ")
    _sb.AppendLine("        , AMOUNT_IN - AMOUNT_OUT ")
    _sb.AppendLine("   FROM (  ")
    _sb.AppendLine("         SELECT   CS_STATUS, GT_HAS_INTEGRATED_CASHIER ")
    _sb.AppendLine("                , SUM(CASE WHEN " & _filter_chips & " THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & " THEN CM_INITIAL_BALANCE ")
    _sb.AppendLine("                           ELSE 0   END  ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_IN  ")
    _sb.AppendLine("                , SUM(CASE WHEN " & _filter_chips & " THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_OPEN_SESSION & " THEN CM_FINAL_BALANCE ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_OUT ")
    _sb.AppendLine("                , SUM(CASE WHEN " & _filter_amount & " THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT & " THEN ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT) ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & "   THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & " THEN ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE) ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS AMOUNT_IN  ")
    _sb.AppendLine("                , SUM(CASE WHEN " & _filter_amount & " THEN ")
    _sb.AppendLine("                           CASE CM_TYPE ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN & "   THEN ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT) ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                           WHEN " & CASHIER_MOVEMENT.CAGE_OPEN_SESSION & " THEN CM_FINAL_BALANCE ")
    _sb.AppendLine("                           ELSE 0   END ")
    _sb.AppendLine("                      ELSE 0   END) AS AMOUNT_OUT ")
    _sb.AppendLine("           FROM   CASHIER_MOVEMENTS ")
    _sb.AppendLine("          INNER   JOIN GAMING_TABLES_SESSIONS ON CM_SESSION_ID = GTS_CASHIER_SESSION_ID  ")
    _sb.AppendLine("          INNER   JOIN GAMING_TABLES          ON GT_CASHIER_ID = CM_CASHIER_ID ")
    _sb.AppendLine("          INNER   JOIN CASHIER_SESSIONS       ON CS_SESSION_ID = CM_SESSION_ID ")
    _sb.AppendLine("          WHERE   CM_TYPE IN (" & CASHIER_MOVEMENT.FILLER_IN & ", " & CASHIER_MOVEMENT.FILLER_OUT & ", " & CASHIER_MOVEMENT.CAGE_OPEN_SESSION & ", " & CASHIER_MOVEMENT.CAGE_CLOSE_SESSION & ", " & CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK & ", " & CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK & ") ")
    _sb.AppendLine("            AND   CM_DATE >= " & GUI_FormatDateDB(DateFrom) & " ")
    _sb.AppendLine("            AND   CM_DATE <= " & GUI_FormatDateDB(DateTo) & " ")
    _sb.AppendLine("       GROUP BY   CS_STATUS, GT_HAS_INTEGRATED_CASHIER ")
    _sb.AppendLine("         ) x ")
    _sb.AppendLine(" UNION ALL ")
    _sb.AppendLine(" SELECT   1 ")
    _sb.AppendLine("        , CS_STATUS ")
    _sb.AppendLine("        , CAST(GT_HAS_INTEGRATED_CASHIER AS BIT) ")
    _sb.AppendLine("        , ISNULL(CHIPS_SALE, 0) - ISNULL(CHIPS_PURCHASE, 0) ")
    _sb.AppendLine("        , 0 ")
    _sb.AppendLine("   FROM (  ")
    _sb.AppendLine("         SELECT   CS_STATUS, CASE WHEN GTS_CASHIER_SESSION_ID IS NULL THEN 0 ELSE 1 END AS GT_HAS_INTEGRATED_CASHIER ")
    _sb.AppendLine("                , SUM(CASE CM_TYPE ")
    _sb.AppendLine("                      WHEN " & CASHIER_MOVEMENT.CHIPS_SALE_TOTAL & " THEN CM_SUB_AMOUNT ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_SALE  ")
    _sb.AppendLine("                , SUM(CASE CM_TYPE ")
    _sb.AppendLine("                      WHEN " & CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL & " THEN CM_ADD_AMOUNT ")
    _sb.AppendLine("                      ELSE 0   END) AS CHIPS_PURCHASE ")
    _sb.AppendLine("           FROM   CASHIER_MOVEMENTS ")
    _sb.AppendLine("          INNER   JOIN CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID ")
    _sb.AppendLine("           LEFT   JOIN GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID ")
    'We want the chips operations that are from cashiers that are not gambling tables
    _sb.AppendLine("          WHERE   CM_TYPE IN (" & CASHIER_MOVEMENT.CHIPS_SALE_TOTAL & ", " & CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL & ") ")
    _sb.AppendLine("            AND   " & _filter_chips & " ")
    _sb.AppendLine("            AND   CM_DATE >= " & GUI_FormatDateDB(DateFrom) & " ")
    _sb.AppendLine("            AND   CM_DATE <  " & GUI_FormatDateDB(DateTo) & " ")
    _sb.AppendLine("       GROUP BY   CS_STATUS, CASE WHEN GTS_CASHIER_SESSION_ID IS NULL THEN 0 ELSE 1 END ")
    _sb.AppendLine("         ) x ")

    Return _sb.ToString()
  End Function ' GetAmountAndChipsQuery

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Get Prov/Cash activity
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub GetProvCashData(ByVal DateFrom As Date, _
                              ByVal DateTo As Date, _
                              ByVal ClosingTime As String, _
                              ByVal SqlTrans As SqlTransaction, _
                              ByRef CashDaysData As TYPE_PROVIDER_CASH_ITEM)

    Dim _session_data As New WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _sum As Decimal
    Dim _base_date As String

    CashDaysData.provider_activity_total = 0
    CashDaysData.provider_activity_no_redeemable = 0
    CashDaysData.provider_activity_redeemable = 0
    CashDaysData.play_sessions_open = 0
    CashDaysData.cash_results = 0
    CashDaysData.other_income = 0
    CashDaysData.credits_expired = 0
    CashDaysData.converted_credit = 0
    CashDaysData.hall_payments = 0
    CashDaysData.progressive_provision = 0
    CashDaysData.progressive_reserve = 0
    CashDaysData.redeemable_circulation_increase = 0
    CashDaysData.gaming_table_circulation = 0
    CashDaysData.gaming_table_open = 0
    CashDaysData.machine_tickets_voided = 0

    _base_date = "01/01/2007 " & ClosingTime & ":00:00"

    Try
      WSI.Common.Cashier.ReadCashierSessionData(SqlTrans, DateFrom, DateTo, _session_data)

      '- CType(_session_data.b_total_in, Decimal) + CType(_session_data.b_total_dev, Decimal)
      ' CashDaysData.cash_results = CType(_session_data.result_cashier, Decimal) - CType(_session_data.b_total_in, Decimal) - CType(_session_data.b_total_dev, Decimal)

      ' JAB 07-JUL-2012: get the value "result_cashier_only_a"
      CashDaysData.cash_results = CType(_session_data.result_cashier_only_a, Decimal)
      ' RRB 02-OCT-2012: substract decimal rounding from cash results empresa A
      CashDaysData.cash_results -= _session_data.decimal_rounding

      CashDaysData.other_income = CType(_session_data.cards_usage_a, Decimal) - CType(_session_data.a_refund_cards_usage, Decimal)
      'ICS 08-AUG-2013: Commissions are not playable
      ' DHA 16-FEB-2015: Only the commissions of A are considered
      CashDaysData.other_income += CType(_session_data.total_commission_a, Decimal)
      ' AJQ & DDM & RCI 11-OCT-2013, UNR's
      CashDaysData.other_income -= (CType(_session_data.unr_k2_tax1, Decimal) + CType(_session_data.unr_k2_tax2, Decimal))

      ' TODO: Add the 3 balances of an account.
      CashDaysData.converted_credit = CType(_session_data.promo_nr_to_re, Decimal) + CType(_session_data.promo_re, Decimal) - CType(_session_data.cancel_promo_re, Decimal)

      Using _sql_cmd As SqlCommand = New SqlCommand(GetQueryData(DateFrom, DateTo, _base_date), SqlTrans.Connection, SqlTrans)
        _sql_cmd.CommandTimeout = 60
        Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

          While _reader.Read()
            CashDaysData.provider_activity_total += IIf(_reader.IsDBNull(SQL_COLUMN_PROVIDER_ACTIVITY_TOTAL), 0, _reader(SQL_COLUMN_PROVIDER_ACTIVITY_TOTAL))
            CashDaysData.provider_activity_no_redeemable += IIf(_reader.IsDBNull(SQL_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE), 0, _reader(SQL_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE))
            CashDaysData.play_sessions_open += IIf(_reader.IsDBNull(SQL_COLUMN_PLAY_SESSIONS_OPEN), 0, _reader(SQL_COLUMN_PLAY_SESSIONS_OPEN))
            CashDaysData.credits_expired += IIf(_reader.IsDBNull(SQL_COLUMN_CREDITS_EXPIRED), 0, _reader(SQL_COLUMN_CREDITS_EXPIRED))
            CashDaysData.hall_payments += IIf(_reader.IsDBNull(SQL_COLUMN_HALL_PAYMENTS), 0, _reader(SQL_COLUMN_HALL_PAYMENTS))
            CashDaysData.progressive_provision += IIf(_reader.IsDBNull(SQL_COLUMN_PROGRESSIVE_PROVISION), 0, _reader(SQL_COLUMN_PROGRESSIVE_PROVISION))
            CashDaysData.progressive_reserve += IIf(_reader.IsDBNull(SQL_COLUMN_PROGRESSIVE_RESERVE), 0, _reader(SQL_COLUMN_PROGRESSIVE_RESERVE))
          End While

        End Using
      End Using

      If GamingTableBusinessLogic.IsGamingTablesEnabled() Then
        Using _sql_cmd As SqlCommand = New SqlCommand(GetAmountAndChipsQuery(DateFrom, DateTo), SqlTrans.Connection, SqlTrans)
          Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()
            Dim _type As Integer
            Dim _status As Integer
            Dim _cashier_integrated As Boolean
            Dim _chips_value As Decimal
            Dim _amount_value As Decimal

            While _reader.Read()
              _type = _reader.GetInt32(0)
              _status = _reader.GetInt32(1)
              _cashier_integrated = _reader.GetBoolean(2)
              _chips_value = _reader.GetDecimal(3)
              _amount_value = _reader.GetDecimal(4)

              ' _type = 0 --> _xxx_value refers to FillOut - FillIn
              ' _type = 1 --> _xxx_value refers to Sales - Purchases

              Select Case _status
                Case 0, 2  ' Cashier session Open
                  If _type = 0 And Not _cashier_integrated Then
                    CashDaysData.gaming_table_open += -_chips_value
                    CashDaysData.gaming_table_circulation += -_chips_value
                  ElseIf _type = 0 And _cashier_integrated Then
                    ' Do nothing
                    ' When the session is open, gaming table with integrated cashier take only in account the sales/purchases (not fill in/out)
                  ElseIf _type = 1 Then
                    ' For conventional cashier and gaming table with integrated cashier
                    CashDaysData.gaming_table_circulation += _chips_value
                  End If

                Case 1, 3 ' Cashier session Closed
                  If _type = 0 And Not _cashier_integrated Then
                    CashDaysData.provider_activity_total += _chips_value
                    CashDaysData.gaming_table_circulation += _amount_value
                    CashDaysData.cash_results += _amount_value
                  ElseIf _type = 0 And _cashier_integrated Then
                    CashDaysData.provider_activity_total += _chips_value
                    CashDaysData.gaming_table_circulation -= _amount_value
                  ElseIf _type = 1 And Not _cashier_integrated Then
                    CashDaysData.provider_activity_total += _amount_value
                    CashDaysData.gaming_table_circulation += _chips_value
                  ElseIf _type = 1 And _cashier_integrated Then
                    CashDaysData.provider_activity_total += _amount_value
                    CashDaysData.gaming_table_circulation -= _chips_value
                  End If

                Case Else
              End Select
            End While

          End Using
        End Using
      End If

      'CashDaysData.gaming_table_profit = _session_data.chips_sale_total - _session_data.chips_purchase_total

      ' ICS & JML 12-FEB-2014 take in mind the tickets offline.
      CashDaysData.provider_activity_total += _session_data.tickets_offline_amount

      CashDaysData.provider_activity_redeemable = CashDaysData.provider_activity_total - CashDaysData.provider_activity_no_redeemable

      ' AJQ 05-SEP-2012, Include into 'Pagos de Sala' the TaxReturningAmount
      CashDaysData.hall_payments += _session_data.tax_returning_on_prize_coupon
      ' RRB 13-SEP-2012, Include into 'Pagos de Sala' the DecimalRounding
      CashDaysData.hall_payments += _session_data.decimal_rounding
      ' DHA 2-JUN-2014, Include into 'Pagos de Sala' the PointsAsCashinPrize
      CashDaysData.hall_payments += _session_data.points_as_cashin_prize
      ' RRB Service charge goes to empresa b, does not affect hall payments.
      ' CashDaysData.hall_payments -= _session_data.service_charge

      ' FAV 16-02-2016
      CashDaysData.machine_tickets_voided += _session_data.total_machine_ticket_voided

      _sum = 0
      _sum -= CashDaysData.provider_activity_redeemable
      _sum -= CashDaysData.play_sessions_open
      _sum += CashDaysData.cash_results
      _sum -= CashDaysData.other_income
      _sum -= CashDaysData.credits_expired
      _sum += CashDaysData.converted_credit
      _sum += CashDaysData.hall_payments
      _sum += CashDaysData.gaming_table_open
      _sum -= CashDaysData.gaming_table_circulation
      _sum -= CashDaysData.machine_tickets_voided

      'Progressive provision reserve & Progressive provision
      CashDaysData.progressive_provision -= CashDaysData.progressive_reserve
      _sum -= CashDaysData.progressive_provision

      CashDaysData.redeemable_circulation_increase += _sum

    Catch _ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "frm_cash_report", "GetProvCashData", _ex.Message)

    End Try

  End Sub ' GetProvCashData

  ' PURPOSE: Calculate Total Assets: Redeemable / Not Redeemable
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrans
  '
  '     - OUTPUT:
  '           - TotalRedeemable
  '           - TotalNotRedeemable
  '
  ' RETURNS:
  '
  Public Sub CalculateTotalAssets(ByRef TotalRedeemable As Decimal, _
                                  ByRef TotalNotRedeemable As Decimal, _
                                  ByVal SqlTrans As SqlTransaction)
    Dim _str_sql As String
    Dim _interval_days As Integer
    Dim _liability_mode As ENUM_LIABILITIES_MODE

    TotalRedeemable = 0
    TotalNotRedeemable = 0
    ' Inteval Days =  MAX(CreditsExpireAfterDays, PrizesExpireAfterDays)  + 10 days
    _interval_days = Math.Max(GeneralParam.GetInt32("Cashier", "CreditsExpireAfterDays"), GeneralParam.GetInt32("Cashier", "PrizesExpireAfterDays")) + 10

    Try

      _liability_mode = CType(GeneralParam.GetInt32("Liabilities", "Mode", 1), ENUM_LIABILITIES_MODE)

      If WSI.Common.TITO.Utils.IsTitoMode() Then
        _str_sql = "   SELECT   ISNULL (SUM(CASE TI_TYPE_ID WHEN @pTicketTypeCashable         THEN TI_AMOUNT " & _
                   "                                        WHEN @pTicketTypeHandpay          THEN TI_AMOUNT " & _
                   "                                        WHEN @pTicketTypeJackpot          THEN TI_AMOUNT " & _
                   "                                        WHEN @pTicketTypePromoRedeem      THEN TI_AMOUNT " & _
                   "                                        ELSE 0 END), 0)                AS REDEEMABLE     " & _
                   "          , ISNULL (SUM(CASE TI_TYPE_ID WHEN @pTicketTypePromoNonRedeem   THEN TI_AMOUNT " & _
                   "                                        ELSE 0 END), 0)                AS NO_REDEEMABLE  " & _
                   "     FROM   TICKETS                                                " & _
                   "    WHERE   TI_STATUS = @pTicketStatusValid "
      Else

        If (_liability_mode = ENUM_LIABILITIES_MODE.APPLY_TO_MACHINE) Then
          _str_sql = " SELECT   ISNULL(SUM(AC_RE_BALANCE + AC_PROMO_RE_BALANCE) + " & _
                     "                 SUM(CASE WHEN ISNULL(AC_CURRENT_PLAY_SESSION_ID, 0) = 0 " & _
                     "                      THEN 0 " & _
                     "                      ELSE AC_IN_SESSION_RE_TO_GM + AC_IN_SESSION_PROMO_RE_TO_GM END), 0) AS REDEEMABLE " & _
                     "        , ISNULL(SUM(AC_PROMO_NR_BALANCE) + " & _
                     "                 SUM(CASE WHEN ISNULL(AC_CURRENT_PLAY_SESSION_ID, 0) = 0 " & _
                     "                      THEN 0 " & _
                       "                      ELSE AC_IN_SESSION_PROMO_NR_TO_GM END), 0)                          AS NO_REDEEMABLE "
        Else
          ' Liabilities (Apply to accounts)
          _str_sql = " SELECT   ISNULL(SUM(AC_RE_BALANCE + AC_PROMO_RE_BALANCE), 0) AS REDEEMABLE " & _
                     "        , ISNULL(SUM(AC_PROMO_NR_BALANCE), 0)                 AS NO_REDEEMABLE "
        End If

        _str_sql &= "   FROM   ACCOUNTS " & _
                   "  WHERE   AC_LAST_ACTIVITY >= DATEADD(DAY,-@pIntevalDays, GETDATE())"
      End If

      Using _sql_cmd As New SqlCommand(_str_sql, SqlTrans.Connection, SqlTrans)
        _sql_cmd.CommandTimeout = 60
        _sql_cmd.Parameters.Add("@pTicketTypeCashable", SqlDbType.Int).Value = TITO_TICKET_TYPE.CASHABLE
        _sql_cmd.Parameters.Add("@pTicketTypeHandpay", SqlDbType.Int).Value = TITO_TICKET_TYPE.HANDPAY
        _sql_cmd.Parameters.Add("@pTicketTypeJackpot", SqlDbType.Int).Value = TITO_TICKET_TYPE.JACKPOT
        _sql_cmd.Parameters.Add("@pTicketTypePromoRedeem", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_REDEEM
        _sql_cmd.Parameters.Add("@pTicketTypePromoNonRedeem", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_NONREDEEM
        _sql_cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID
        _sql_cmd.Parameters.Add("@pIntevalDays", SqlDbType.Int).Value = _interval_days

        Using _reader As SqlDataReader = _sql_cmd.ExecuteReader()

          If _reader.Read() Then

            TotalRedeemable = _reader.GetDecimal(0)
            TotalNotRedeemable = _reader.GetDecimal(1)

          End If

        End Using
      End Using

    Catch _ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "frm_cash_report", "GetProvCashData", _ex.Message)
    End Try

  End Sub ' CalculateTotalAssets

#End Region ' Public Functions


End Module
