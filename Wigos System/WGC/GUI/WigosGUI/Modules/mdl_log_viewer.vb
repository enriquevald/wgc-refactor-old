'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_commands
' DESCRIPTION:   WCP commands common
'                   
' AUTHOR:        Andreu Juli�
' CREATION DATE: 18-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-JUN-2010  AJQ    Initial version
' 21-JUN-2010  RCI    GetCommandData and TopMost routines
' 24-JUL-2013  JML    Changed GetCommandData (Jira defect: WIG-84)
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports System.Text
Imports GUI_CommonOperations
Imports WSI.Common
Imports GUI_Controls
Imports System.Data.SqlClient

Module mdl_log_viewer

  Private GLB_FrmLogViewer As frm_log_viewer = Nothing
  Private GLB_CmdId As UInt64 = 0

#Region " Constants "

  Private Const LOGGER_ADD_COLOR_STATUS_ACTIVE As Boolean = True

  Private Const LOGGER_IDX_CODE As Integer = 64
  Private Const LOGGER_LEN_CODE As Integer = 5

  Private Const LOGGER_IDX_SPECIAL_CODE As Integer = 70
  Private Const LOGGER_LEN_SPECIAL_CODE As Integer = 2
  Private Const LOGGER_LEN_SPECIAL_CODE_VALUE_00 As String = "->"
  Private Const LOGGER_LEN_SPECIAL_CODE_VALUE_01 As String = "**"

  ' Colors
  Private Const COLOR_LOGGER_ERROR As String = "red"
  Private Const COLOR_LOGGER_UNEXPECTED As String = "brown"
  Private Const COLOR_LOGGER_IMPORTANT As String = "green"

#End Region ' Constants

#Region " Public Functions "

  ' PURPOSE: Show Logger string of the command CmdId in a unique window.
  '          Create window, if it doesn't exist.
  '
  '  PARAMS:
  '     - INPUT:
  '           - CmdId As UInt64: Command ID
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  No error occurred and Logger has been showed.
  '     - False: An error has occurred. Window is left as it was.

  Public Function ShowLogger(ByVal CmdId As UInt64, ByVal MdiParent As System.Windows.Forms.IWin32Window) As Boolean
    Dim _str_terminal As String
    Dim _str_provider As String
    Dim _str_created As String
    Dim _zip_logger As String
    Dim _str_logger As String
    Dim _just_created As Boolean

    _str_terminal = ""
    _str_provider = ""
    _str_created = ""
    _zip_logger = ""
    _str_logger = ""
    _just_created = False

    If CmdId <> GLB_CmdId Then

      Try
        ' Get Compressed Logger
        GetCommandData(CmdId, _str_terminal, _str_provider, _str_created, _zip_logger)

        ' Expand Logger
        WSI.Common.Misc.InflateString(_zip_logger, _str_logger)
        If LOGGER_ADD_COLOR_STATUS_ACTIVE Then
          _str_logger = SetColorStatus(_str_logger)
        End If

      Catch ex As Exception
        Return False
      End Try

      GLB_CmdId = CmdId

      If (GLB_FrmLogViewer Is Nothing) Then
        GLB_FrmLogViewer = New frm_log_viewer
        GLB_FrmLogViewer.MdiParent = MdiParent

        ' Allow some time to the form creation before inserting the texts to be displayed
        _just_created = True
      Else
        GLB_FrmLogViewer.Text = _str_created & " " & _str_provider & " " & _str_terminal & " - " & GLB_NLS_GUI_CONTROLS.GetString(423)
        GLB_FrmLogViewer.DisplayText = "<html><body><pre>" & _str_logger & "</pre></body></html>"
      End If
    End If

    GLB_FrmLogViewer.WindowState = FormWindowState.Normal
    GLB_FrmLogViewer.Show()
    'GLB_FrmLogViewer.TopMost = True
    GLB_FrmLogViewer.BringToFront()

    If _just_created Then
      ' Allow some time to the form creation before inserting the texts to be displayed
      GLB_FrmLogViewer.Text = _str_created & " " & _str_provider & " " & _str_terminal & " - " & GLB_NLS_GUI_CONTROLS.GetString(423)
      GLB_FrmLogViewer.DisplayText = "<html><body><pre>" & _str_logger & "</pre></body></html>"
    End If

    'LJM 20-NOV-2013: Audit the access to the log
    Call AuditForm(AUDIT_FLAGS.ACCESS, GLB_FrmLogViewer.Text)

    Return True
  End Function ' ShowLogger

  ' PURPOSE: Hide Logger window.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub HideLogger()
    Try
      GLB_FrmLogViewer.Close()
    Catch ex As Exception
    End Try

    GLB_FrmLogViewer = Nothing
    GLB_CmdId = 0
  End Sub ' HideLogger

  ' PURPOSE: Set Property TopMost of the log_viewer Form.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TopMost As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub LoggerSetTopMost(ByVal TopMost As Boolean)
    If Not GLB_FrmLogViewer Is Nothing Then
      GLB_FrmLogViewer.TopMost = TopMost
    End If
  End Sub ' LoggerSetTopMost

  ' PURPOSE: Get Property TopMost of the log_viewer Form.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean with the TopMost value of the form.

  Public Function LoggerGetTopMost() As Boolean
    Dim top_most As Boolean

    top_most = False
    If Not GLB_FrmLogViewer Is Nothing Then
      top_most = GLB_FrmLogViewer.TopMost
    End If

    Return top_most
  End Function ' LoggerGetTopMost

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Get Command Data including the Encoded Logger string.
  '
  '  PARAMS:
  '     - INPUT:
  '           - CmdId As UInt64
  '     - OUTPUT:
  '           - Terminal As String
  '           - Provider As String
  '           - Created As String
  '           - ZipLogger As String
  '
  ' RETURNS:
  '     - None
  Private Sub GetCommandData(ByVal CmdId As UInt64, ByRef Terminal As String, ByRef Provider As String, _
                             ByRef Created As String, ByRef ZipLogger As String)
    Dim _str_sql As String

    Try

      _str_sql = "SELECT TE_NAME " & _
                    " , TE_PROVIDER_ID " & _
                    " , CMD_CREATED " & _
                    " , CMD_RESPONSE " & _
                " FROM WCP_COMMANDS, TERMINALS " & _
                " WHERE CMD_TERMINAL_ID = TE_TERMINAL_ID " & _
                "   AND CMD_ID = @cmd_id"

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_command As New SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_command.Parameters.Add("@cmd_id", SqlDbType.Int, 0).Value = CmdId

          Using _sql_reader As SqlDataReader = _sql_command.ExecuteReader()

            If _sql_reader.Read Then

              Terminal = _sql_reader("TE_NAME")
              Provider = _sql_reader("TE_PROVIDER_ID")
              Created = _sql_reader("CMD_CREATED")

              If _sql_reader.IsDBNull(2) Then
                ZipLogger = ""
              Else
                ZipLogger = _sql_reader("CMD_RESPONSE")
              End If
            End If

          End Using

        End Using
      End Using

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

  End Sub ' GetCommandData

  ' PURPOSE: Format StrLogger lines setting color status depending of the code of the line.
  '
  '  PARAMS:
  '     - INPUT:
  '           - StrLogger As String
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function SetColorStatus(ByVal StrLogger As String) As String

    Dim _sep(1) As Char
    Dim _lines() As String
    Dim _code As Integer
    Dim _special_code As String
    Dim _str_out As StringBuilder
    Dim _color As String
    Dim _formated_line As String

    _sep(0) = Chr(10)
    _lines = StrLogger.Split(_sep)
    _str_out = New StringBuilder()

    For Each _line As String In _lines

      If _line.Length < LOGGER_IDX_SPECIAL_CODE + LOGGER_LEN_SPECIAL_CODE Then
        Continue For
      End If

      If Not Integer.TryParse(_line.Substring(LOGGER_IDX_CODE, LOGGER_LEN_CODE), _code) Then
        _code = 0
      End If

      _color = ""
      Select Case _code
        Case 1
          _special_code = _line.Substring(LOGGER_IDX_SPECIAL_CODE, LOGGER_LEN_SPECIAL_CODE)
          Select Case _special_code
            Case LOGGER_LEN_SPECIAL_CODE_VALUE_00, _
                 LOGGER_LEN_SPECIAL_CODE_VALUE_01
              _color = COLOR_LOGGER_IMPORTANT
          End Select
        Case 4, 21
          _color = COLOR_LOGGER_ERROR
        Case 31, 32
          _color = COLOR_LOGGER_UNEXPECTED
      End Select

      If _color <> "" Then
        _formated_line = "<font color =" + _color + ">" + _line + "</font>"
      Else
        _formated_line = _line
      End If

      _str_out.AppendLine(_formated_line)

    Next

    Return _str_out.ToString()
  End Function ' SetColorStatus

#End Region ' Private Functions

End Module ' mdl_log_viewer
