'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_misc.vb
' DESCRIPTION:   Miscelaneous functions for the Jackpot Manager system 
' AUTHOR:        Agust� Poch
' CREATION DATE: 29-NOV-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 29-NOV-2002 APB    Initial version
' 06-OCT-2005 JMP    Added functinonalities
' 01-FEB-2012 RCI & MPO    Added routine TimeSpanToString()
' 22-MAY-2012 ACC    Add functions for Acceptors funcionality: CreateCashierMovement and UpdateSessionBalance
' 14-FEB-2013 JCM    Add function DeleteTempFiles and Fixed Bug: infinite loop when app from third lock files
' 24-MAY-2013 HBB    The function LevelFilterText was brought from cls_promotion to make it accesible from other sites
' 22-JUL-2013 JBC    Now the tooltip shows the alarm description
' 21-NOV-2014 SGB    Fill alarm group control with hexadecimal code and description alarm
'-------------------------------------------------------------------

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Alarm

Module mdl_misc

  ' PURPOSE: The string associated to a Jackpot status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status Identifier
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - String describing the status

  'Public Function JackpotStatusIdToText(ByVal Status As ENUM_SERIES_STATUS) As String

  '  Select Case Status
  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_NOT_ACTIVE
  '      Return GLB_NLS.GetString(279)

  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_READY
  '      Return GLB_NLS.GetString(280)

  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_PARTIALLY_DISTRIBUTED
  '      Return GLB_NLS.GetString(281)

  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_COMPLETELY_DISTRIBUTED
  '      Return GLB_NLS.GetString(282)

  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_SOLD
  '      Return GLB_NLS.GetString(283)

  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_BEING_RETIRED
  '      Return GLB_NLS.GetString(284)

  '    Case ENUM_SERIES_STATUS.SERIES_STATUS_RETIRED
  '      Return GLB_NLS.GetString(285)
  '  End Select

  'End Function 'JackpotStatusIdToText


  ' PURPOSE: Calculate a gross amount from a net amount
  '
  '  PARAMS:
  '     - INPUT:
  '           - Net Amount
  '           - Percentage of taxes applied
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Gross amount

  Public Function NetToGrossAmount(ByVal NetAmount As Double, ByVal TaxesPct As Double) As Double

    If TaxesPct < 0 Or TaxesPct > 100 Then
      Return 0.0
    End If

    Return (100 * NetAmount) / (100 - TaxesPct)
  End Function 'NetToGrossAmount

  '' PURPOSE: Converts a boolean value (0 or 1) to text (Yes/No)
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''           - Value to convert
  ''     - OUTPUT:
  ''           - none
  ''
  '' RETURNS:
  ''     - Boolean string

  'Public Function BoolToText(ByVal BoolValue As Integer) As String

  '  Select Case BoolValue
  '    Case 0
  '      Return GLB_NLS.GetString(342)
  '    Case 1
  '      Return GLB_NLS.GetString(341)
  '    Case Else
  '      Return ""
  '  End Select
  'End Function 'BoolToText

  ' PURPOSE: Convert Seconds in Days, Hours, Minutes And Seconds 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Seconds to convert
  '     - OUTPUT:
  '           - Days
  '           - Hours
  '           - Minutes
  '           - Seconds
  '
  ' RETURNS:
  '     - none

  Public Sub Format_SecondsToDdHhMmSs(ByVal Seconds As Integer, _
                                      ByRef Dd As Integer, _
                                      ByRef Hh As Integer, _
                                      ByRef Mm As Integer, _
                                      ByRef Ss As Integer)
    Dim temp As Integer

    Dd = Seconds \ SECONDS_X_DAY
    temp = Seconds Mod SECONDS_X_DAY
    Hh = temp \ SECONDS_X_HOUR
    temp = temp Mod SECONDS_X_HOUR
    Mm = temp \ SECONDS_X_MINUTE
    Ss = temp Mod SECONDS_X_MINUTE

  End Sub

  ' PURPOSE: Convert Time value to Seconds 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Time to Convert
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - value in 
  Public Function Format_HhMmSsToSecond(ByVal Time As Date) As Integer
    Dim hour As Integer
    Dim minute As Integer

    hour = Time.Hour * SECONDS_X_HOUR
    minute = Time.Minute * SECONDS_X_MINUTE
    Return hour + minute + Time.Second
  End Function

  ' PURPOSE: Obtain the string representation of the TimeSpan in format 99 d hh:mm:ss.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Time
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Public Function TimeSpanToString(ByVal Time As TimeSpan) As String

    Dim _str_time As String = ""

    _str_time = New TimeSpan(Time.Hours, Time.Minutes, Time.Seconds).ToString()
    If Time.Days > 0 Then
      _str_time = Time.Days & " d " & _str_time
    End If

    Return _str_time
  End Function ' TimeSpanToString

  ' PURPOSE: mobile bank get cashier information
  '
  '  PARAMS:
  '      - INPUT:
  '          - CashierSessionId
  '
  '      - OUTPUT:
  '          - UserId
  '          - UserName
  '          - TerminalId
  '          - TerminalName
  '          - CashierBalance 
  '
  ' RETURNS: 
  '
  '   NOTES:
  '         
  Public Sub GetCashierInfo(ByVal SessionId As Int64, ByVal Trx As SqlTransaction, ByRef UserId As Int32, ByRef UserName As String, _
                            ByRef TerminalId As Int32, ByRef TerminalName As String, ByRef CashierBalance As Currency)

    Dim _sql_str As String

    Try
      _sql_str = " SELECT   TOP 1                               " & _
                 "          CS_USER_ID                          " & _
                 "        , GU_USERNAME                         " & _
                 "        , CS_CASHIER_ID                       " & _
                 "        , CT_NAME                             " & _
                 "        , CS_BALANCE                          " & _
                 "   FROM   CASHIER_SESSIONS                    " & _
                 "        , GUI_USERS                           " & _
                 "        , CASHIER_TERMINALS                   " & _
                 "  WHERE   CS_SESSION_ID = @pCashierSessionID  " & _
                 "    AND   CS_CASHIER_ID = CT_CASHIER_ID       " & _
                 "    AND   CS_USER_ID    = GU_USER_ID          "

      Using _sql_command As SqlCommand = New SqlCommand(_sql_str, Trx.Connection, Trx)
        _sql_command.Parameters.Add("@pCashierSessionID", SqlDbType.BigInt).Value = SessionId
        Using _reader As SqlDataReader = _sql_command.ExecuteReader()

          If _reader.Read() Then
            UserId = _reader.GetInt32(0)
            UserName = _reader.GetString(1)
            TerminalId = _reader.GetInt32(2)
            TerminalName = _reader.GetString(3)
            CashierBalance = _reader.GetSqlMoney(4)
          End If

        End Using
      End Using

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "MBGetCashierInfo", _ex.Message)
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "notes_register", "NACardCashDeposit", "Exception throw: Error reading Cashier Session. cs_session_id: " + SessionId.ToString())

    End Try
  End Sub ' MBGetCashierInfo

  ' PURPOSE: Update Session Balance
  ' 
  '  PARAMS:
  '      - INPUT:
  '          - CurrentSessionId
  '          - Increment
  '          - SqlTrx
  '
  '      - OUTPUT:
  '
  ' RETURNS:
  '      - Balance (Currency)
  ' 
  '   NOTES:
  '    
  Public Function UpdateSessionBalance(ByVal CurrentSessionId As Int64, ByVal Increment As Currency, ByVal SqlTrx As SqlTransaction) As Currency

    Dim _sql_str As String
    Dim _obj As Object
    Dim _balance As Currency

    _balance = 0

    _sql_str = "UPDATE   CASHIER_SESSIONS                       " & _
               "   SET   CS_BALANCE    = CS_BALANCE + @pBalance " & _
               " WHERE   CS_SESSION_ID = @pSessionId            " & _
               "   AND   CS_STATUS     = @pStatusOpen           "


    Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)
      _sql_cmd.Parameters.Add("@pBalance", SqlDbType.Money).Value = Increment.SqlMoney
      _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CurrentSessionId
      _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN

      If _sql_cmd.ExecuteNonQuery() <> 1 Then
        Throw New Exception("UpdateSessionBalance: Can't update Cashier Session Id " & Convert.ToString(CurrentSessionId) & ". It is not open.")
      End If
    End Using ' using _sql_cmd

    _sql_str = " SELECT   CS_BALANCE                   " & _
                "  FROM   CASHIER_SESSIONS             " & _
                " WHERE   CS_SESSION_ID  = @pSessionId "

    Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)
      _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CurrentSessionId
      _obj = _sql_cmd.ExecuteScalar()
      _balance = CDec(_obj)
    End Using

    Return _balance
  End Function ' UpdateSessionBalance

  ' PURPOSE: Insert Cashier Movement
  ' 
  '  PARAMS:
  '      - INPUT:
  '          - OperationId
  '          - MovementType
  '          - Amount
  '          - SessionId
  '          - CardTrackData
  '          - CardId
  '          - SqlTrx
  '
  '      - OUTPUT:
  '          - MovementId
  '
  ' RETURNS:
  '      - NONE
  ' 
  '   NOTES:
  '    
  Public Function CreateCashierMovement(ByVal OperationId As Int64, ByVal MovementType As CASHIER_MOVEMENT, ByVal Amount As Currency, ByVal SessionId As Int64, _
                                        ByVal CardTrackData As String, ByVal CardId As Int64, ByVal SqlTrx As SqlTransaction, ByRef MovementId As Int64) As Boolean

    Dim _sql_param As SqlParameter
    Dim _sql_str As String
    Dim _current_terminal_id As Int64
    Dim _final_movement_balance As Currency
    Dim _balance_increment As Currency
    Dim _add_amount As Currency
    Dim _sub_amount As Currency
    Dim _card_track_data As [String]
    Dim _aux As Currency
    Dim _obj As Object

    _current_terminal_id = CommonCashierInformation.TerminalId

    _sql_param = New SqlParameter()

    _card_track_data = ""

    _add_amount = Amount
    _sub_amount = 0
    _card_track_data = CardTrackData
    _balance_increment = CDec(_add_amount) - CDec(_sub_amount)

    ' Insert the movement in the database
    ' Check how to handle cashier movements ( per user / per cashier )      
    Try

      If CDec(_balance_increment) > 0 Then
        _final_movement_balance = UpdateSessionBalance(SessionId, _balance_increment, SqlTrx)
      Else
        _sql_str = " SELECT   CS_BALANCE                   " & _
                    "  FROM   CASHIER_SESSIONS             " & _
                    " WHERE   CS_SESSION_ID  = @pSessionId "

        Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId
          _obj = _sql_cmd.ExecuteScalar()
          _final_movement_balance = CDec(_obj)
        End Using
      End If

      _sql_str = "INSERT INTO   CASHIER_MOVEMENTS      " & _
                 "            ( CM_SESSION_ID          " & _
                 "            , CM_CASHIER_ID          " & _
                 "            , CM_USER_ID             " & _
                 "            , CM_TYPE                " & _
                 "            , CM_INITIAL_BALANCE     " & _
                 "            , CM_ADD_AMOUNT          " & _
                 "            , CM_SUB_AMOUNT          " & _
                 "            , CM_FINAL_BALANCE       " & _
                 "            , CM_USER_NAME           " & _
                 "            , CM_CASHIER_NAME        " & _
                 "            , CM_CARD_TRACK_DATA     " & _
                 "            , CM_ACCOUNT_ID          " & _
                 "            , CM_ACCOUNT_MOVEMENT_ID " & _
                 "            , CM_OPERATION_ID        " & _
                 "            )                        " & _
                 "   VALUES   ( @pSessionId            " & _
                 "            , @pTerminalId           " & _
                 "            , @pUserId               " & _
                 "            , @pMovementType         " & _
                 "            , @pInitialBalance       " & _
                 "            , @pAddAmount            " & _
                 "            , @pSubAmount            " & _
                 "            , @pFinalBalance         " & _
                 "            , @pUserName             " & _
                 "            , @pTerminalName         " & _
                 "            , @pTrackData            " & _
                 "            , @pAccountId            " & _
                 "            , @pAccountMovement      " & _
                 "            , @pOperationId          " & _
                 "            )                        "

      Using _sql_cmd = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

        _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId
        _sql_cmd.Parameters.Add("@pMovementType", SqlDbType.Int).Value = MovementType

        _aux = CDec(_final_movement_balance) - CDec(_balance_increment)

        _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = _aux.SqlMoney
        _sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).Value = _add_amount.SqlMoney
        _sql_cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).Value = _sub_amount.SqlMoney
        _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = _final_movement_balance.SqlMoney
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = CommonCashierInformation.TerminalId
        _sql_cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = CommonCashierInformation.UserId 'AuthorizedByUserId

        _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = CommonCashierInformation.UserName 'AuthorizedByUserName
        _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = CommonCashierInformation.TerminalName

        Dim sql_param As SqlParameter
        Dim sql_param1 As SqlParameter
        Dim sql_param2 As SqlParameter

        sql_param = _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar)
        sql_param.IsNullable = True
        sql_param.SqlValue = System.Data.SqlTypes.SqlString.Null

        sql_param1 = _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt)
        sql_param1.IsNullable = True
        sql_param1.SqlValue = System.Data.SqlTypes.SqlString.Null

        sql_param2 = _sql_cmd.Parameters.Add("@pAccountMovement", SqlDbType.BigInt)
        sql_param2.IsNullable = True
        sql_param2.SqlValue = System.Data.SqlTypes.SqlString.Null

        If _card_track_data <> "" Then
          sql_param.Value = _card_track_data
          sql_param1.Value = CardId
          sql_param2.Value = MovementId
        End If

        If OperationId = 0 Then
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = DBNull.Value
        Else
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId
        End If

        _sql_cmd.ExecuteNonQuery()

      End Using ' Using _sql_cmd

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "CreateCashierMovement", _ex.Message)

      Return False
    End Try

    Return True
  End Function ' CreateCashierMovement

  '----------------------------------------------------------------------------
  ' PURPOSE : Load an image of wkt resources
  '
  ' PARAMS :
  '     - INPUT :
  '         - ResourceId
  '         - SqlTrx
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Image
  '
  ' NOTES :
  Public Sub SaveImage(ByRef ResourceId As Nullable(Of Long), ByVal ResourceImage As Image, ByVal SqlTrx As SqlClient.SqlTransaction)
    Dim _resource_id As Long

    If IsNothing(ResourceImage) Then

      If ResourceId.HasValue Then
        If WSI.Common.WKTResources.Delete(ResourceId.Value, SqlTrx) Then
          ResourceId = Nothing
        End If
      End If
      Exit Sub
    End If

    If ResourceId.HasValue Then
      _resource_id = ResourceId.Value
    End If

    If Not WSI.Common.WKTResources.Save(_resource_id, ResourceImage, SqlTrx) Then

      ResourceId = Nothing
      Exit Sub
    End If

    ResourceId = _resource_id

  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE : Load an image of lcd resources
  '
  ' PARAMS :
  '     - INPUT :
  '         - ResourceId
  '         - SqlTrx
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Image
  '
  ' NOTES :
  '
  Public Sub SaveImageLCD(ByRef ResourceId As Nullable(Of Long), ByVal ResourceImage As Image, ByVal SqlTrx As SqlClient.SqlTransaction)
    Dim _resource_id As Long

    If IsNothing(ResourceImage) Then

      If ResourceId.HasValue Then
        If WSI.Common.LCDResources.Delete(ResourceId.Value, SqlTrx) Then
          ResourceId = Nothing
        End If
      End If
      Exit Sub
    End If

    If ResourceId.HasValue Then
      _resource_id = ResourceId.Value
    End If

    If Not WSI.Common.LCDResources.Save(_resource_id, ResourceImage, SqlTrx) Then

      ResourceId = Nothing
      Exit Sub
    End If

    ResourceId = _resource_id

  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE : Load an image of lcd resources
  '
  ' PARAMS :
  '     - INPUT :
  '         - ResourceId
  '         - SqlTrx
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Image
  '
  ' NOTES :
  '
  Public Sub SaveVideoLCD(ByRef ResourceId As Nullable(Of Long), ByVal ResourceVideo As System.IO.MemoryStream, ByVal Extension As String, ByVal SqlTrx As SqlClient.SqlTransaction)
    Dim _resource_id As Long

    If IsNothing(ResourceVideo) Then

      If ResourceId.HasValue Then
        If WSI.Common.LCDResources.Delete(ResourceId.Value, SqlTrx) Then
          ResourceId = Nothing
        End If
      End If
      Exit Sub
    End If

    If ResourceId.HasValue Then
      _resource_id = ResourceId.Value
    End If

    If Not WSI.Common.LCDResources.Save(_resource_id, ResourceVideo, Extension, SqlTrx) Then

      ResourceId = Nothing
      Exit Sub
    End If

    ResourceId = _resource_id

  End Sub ' SaveVideoLCD


  '----------------------------------------------------------------------------
  ' PURPOSE : Save an image into wkt resources
  '
  ' PARAMS :
  '     - INPUT :
  '         - ResourceId: If there is null is because it is wrong
  '         - SqlTrx
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Image
  '
  ' NOTES :
  Public Function LoadImage(ByVal ResourceId As Long, ByVal SqlTrx As SqlClient.SqlTransaction) As Image
    Dim _image As Image

    _image = Nothing

    If WSI.Common.WKTResources.Load(ResourceId, _image, SqlTrx) Then

      Return _image
    End If

    Return _image
  End Function

  '----------------------------------------------------------------------------
  ' PURPOSE : Get a unique string for the image
  '
  ' PARAMS :
  '     - INPUT :
  '         - ResourceImage: The image
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - Image
  '
  ' NOTES :
  Public Function GetInfoCRC(ByVal ResourceImage As Image) As String
    Dim _image_info As String
    Dim _image_hash As String

    If IsNothing(ResourceImage) Then Return "--"

    _image_hash = ""
    Using _mm = New IO.MemoryStream()
      ResourceImage.Save(_mm, ResourceImage.RawFormat)
      Using _sha1 As Security.Cryptography.SHA1 = Security.Cryptography.SHA1.Create()
        _image_hash = Convert.ToBase64String(_sha1.ComputeHash(_mm.GetBuffer()))
      End Using
    End Using

    If String.IsNullOrEmpty(_image_hash) Then
      _image_info = "--"
    Else
      _image_info = "CRC => {" & _image_hash & "}"
    End If

    Return _image_info
  End Function


  ' PURPOSE: Temp directory clean up 
  '         
  ' PARAMS:
  '    - INPUT:
  '       TempFiles
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Public Sub DeleteTempFiles(ByVal TempFolder As String)
    Dim _temp_files() As String
    Dim _temp_file As String
    Dim _error_found As Boolean
    Dim _max_retry_attemps As Int32
    Dim _retry_attemps As Int32

    Try
      _max_retry_attemps = 3
      _retry_attemps = 1

      'First, we check if there is a temp folder
      If TempFolder <> "" Then

        Do
          _error_found = False

          'look up for all the files on the temp folder
          _temp_files = System.IO.Directory.GetFiles(TempFolder)

          For Each _temp_file In _temp_files
            'try to delete all the previously generated temp files
            Try
              System.IO.File.Delete(_temp_file)

            Catch ex As Exception
              'if any delete fails...
              _error_found = True

              Call Trace.WriteLine(ex.ToString())
            End Try
          Next

          ' Fixed Bug: infinite loop when app from third lock files
          If _retry_attemps >= _max_retry_attemps Then
            Return
          End If

          '...we will try it again a bit later
          If _error_found Then
            _retry_attemps += 1
            System.Threading.Thread.Sleep(100)
          End If

        Loop While _error_found 'we'll loop until ensure all the temp files are deleted

        System.IO.Directory.Delete(TempFolder)
      End If

    Catch _ex As Exception

      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      "Document Viewer ", _
                      "DeleteTempFiles", _
                      _ex.Message)

    End Try

  End Sub ' DeleteTempFiles


  Public Function LevelFilterText(ByVal LevelFilter As Integer) As String

    Dim str_binary As String
    Dim bit_array As Char()
    Dim str_levels As String
    Dim level As String

    str_binary = Convert.ToString(LevelFilter, 2)
    str_binary = New String("0", 5 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    str_levels = ""
    If (bit_array(4) = "1") Then
      str_levels = str_levels & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)   ' Anonymous Player
    End If
    If (bit_array(3) = "1") Then
      level = GetCashierPlayerTrackingData("Level01.Name")
      If level = "" Then
        level = GLB_NLS_GUI_CONFIGURATION.GetString(289)
      End If
      str_levels = str_levels & ", " & level                                        ' Level 01
    End If
    If (bit_array(2) = "1") Then
      level = GetCashierPlayerTrackingData("Level02.Name")
      If level = "" Then
        level = GLB_NLS_GUI_CONFIGURATION.GetString(290)
      End If
      str_levels = str_levels & ", " & level                                        ' Level 02
    End If
    If (bit_array(1) = "1") Then
      level = GetCashierPlayerTrackingData("Level03.Name")
      If level = "" Then
        level = GLB_NLS_GUI_CONFIGURATION.GetString(291)
      End If
      str_levels = str_levels & ", " & level                                        ' Level 03
    End If
    If (bit_array(0) = "1") Then
      level = GetCashierPlayerTrackingData("Level04.Name")
      If level = "" Then
        level = GLB_NLS_GUI_CONFIGURATION.GetString(332)
      End If
      str_levels = str_levels & ", " & level                                        ' Level 04
    End If

    If str_levels.Length > 0 Then
      str_levels = str_levels.Substring(2)
    End If

    Return "[" & LevelFilter & "] " & str_levels

  End Function

  Public Sub FillAlarmsGroupsControl(ByVal AlarmsDic As Dictionary(Of Integer, ALARMS_CATALOG), ByRef AlarmsCheckedList As GUI_Controls.uc_checked_list)
    Dim _group As String = ""
    Dim _category As String = ""
    Dim _alarms_dic As ALARMS_CATALOG = Nothing
    Dim _name_id As String

    _name_id = ""

    Try

      ' Fill CheckedList control with groups
      Call AlarmsCheckedList.Clear()
      Call AlarmsCheckedList.ReDraw(False)

      For Each _alarms_dic In AlarmsDic.Values

        If _alarms_dic.GroupName <> _group Then
          AlarmsCheckedList.Add(1, 0, _alarms_dic.GroupName)
          _group = _alarms_dic.GroupName
        End If
        If _alarms_dic.CategoryName <> _category Then
          AlarmsCheckedList.Add(2, 0, _alarms_dic.CategoryName)
          _category = _alarms_dic.CategoryName
        End If

        _name_id = Hex(_alarms_dic.AlarmCode.ToString()) & " - " & _alarms_dic.AlarmName

        AlarmsCheckedList.Add(3, _alarms_dic.AlarmCode, _name_id, _alarms_dic.AlarmDescription)

      Next

      If AlarmsCheckedList.Count > 0 Then
        Call AlarmsCheckedList.CurrentRow(0)
        Call AlarmsCheckedList.ReDraw(True)
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillAlarmsGroupsControl

  '----------------------------------------------------------------------------
  ' PURPOSE : Return the image type extension of the image 
  '
  ' PARAMS :
  '     - INPUT :
  '         None
  '
  '     - OUTPUT :
  '         String
  '
  ' RETURNS :
  '     - string
  '
  ' NOTES :
  Public Function GetImageType(img As Image) As String
    Dim _result As String

    _result = ""
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg)) Then
      _result = ".jpg"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp)) Then
      _result = ".bmp"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png)) Then
      _result = ".png"
    End If
    If (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)) Then
      _result = ".gif"
    End If

    Return _result
  End Function

  Public Function GetMimeType(ByVal FileName As String) As String

    Dim mimeType As String = "application/unknown"
    Dim ext As String = System.IO.Path.GetExtension(FileName).ToLower()
    Dim regKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext)

    If Not regKey Is Nothing AndAlso Not regKey.GetValue("Content Type") Is Nothing Then
      mimeType = regKey.GetValue("Content Type").ToString()
    End If

    Return mimeType

  End Function
  ''' <summary>
  ''' Get filters string
  ''' </summary>
  ''' <param name="FilterList"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetFiltersString(ByVal FilterList As List(Of String)) As String
    Dim _index As Int32 = 0
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    For Each _filter As String In FilterList
      If (_index = 0) Then
        _sb.Append(" WHERE " + _filter)
      Else
        _sb.Append(" AND " + _filter)
      End If

      _index += 1
    Next

    Return _sb.ToString()
  End Function

End Module
