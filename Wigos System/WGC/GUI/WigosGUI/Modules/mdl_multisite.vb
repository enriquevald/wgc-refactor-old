'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_multisite.vb
' DESCRIPTION:   Miscelaneous functions for the Multisite system 
' AUTHOR:        Javier Molina
' CREATION DATE: 05-MAR-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 05-MAR-2013 JMM    Initial version
' 18-MAR-2013 ANG    Add TranslateMultiSiteProcessIDToNlsId.
' 06-MAY-2013 ANG    Move GetTaskFieldProperties.
' 14-SEP-2013 ANG    Add TYPE_MULTISITE_TASK_FIELD_PROPERTIES.RealTimeInterval property
' 16-SEP-2013 ANG    Refactored GetTaskFieldProperties() to make it more readable
' 20-MAY-2014 JBC    Added DownloadPatterns type
' 05-DIC-2014 DCS    Added Task Download Lcd Messages
' 14-JAN-2015 DCS    Added Task Upload Handpays
' 12-MAR-2015 ANM    Added Task Sells and pays
' 30-MAR-2015 ANM    Fixed bug #891. Not able to set the interval less than the defect value
' 31-MAR-2017 MS     WIGOS-411 Creditlines
'-------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Module mdl_multisite

#Region "Structures"

  Public Structure TYPE_MULTISITE_TASK_FIELD_PROPERTIES
    Public MinValue As Integer
    Public MaxValue As Integer
    Public Enabled As Boolean
    Public Editable As Boolean

    '' Allow interval values lower than config tasks
    Public RealTimeInterval As Boolean

  End Structure
#End Region

#Region " Constants "


  Public TaskOrderSiteDownload As TYPE_MULTISITE_SITE_TASKS() = {TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadProviders, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages, _
                                                      TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig, _
                                                      TYPE_MULTISITE_SITE_TASKS.Unknown}


  Public ReSyncTasks As TYPE_MULTISITE_SITE_TASKS() = {TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadProviders, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadProviders, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadBanks, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadAreas, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadTerminals, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadProvidersGames, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadAlarms, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadHandpays, _
                                                       TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadCreditLines, _
                                                       TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements}

  '' Must match the columns of the grid
  Private Const GRID_TASKS_NUM_COLUMNS As Byte = 6
  Public Const PROPERTY_TASKS_TASK_ID As Byte = 0
  Public Const PROPERTY_TASKS_TASK_NAME As Byte = 1
  Public Const PROPERTY_TASKS_ENABLED As Byte = 2
  Public Const PROPERTY_TASKS_INTERVAL_SECONDS As Byte = 3
  Private Const PROPERTY_TASKS_START_HOUR As Byte = 4             '' Currently not in use!
  Public Const PROPERTY_TASKS_MAX_ROWS2UPLOAD As Byte = 5

  Private Const TASKS_INTERVAL_MAX_DIGITS As Byte = 5
  Private Const TASKS_START_HOUR_MAX_DIGITS As Byte = 2

  Private m_task_sort_ds As DataSet
#End Region

#Region " Public Functions "

  ' PURPOSE : Allow sort tasks datatable by TaskOrder TYPE_MULTISITE_SITE_TASKS array.
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - Sorted datatable
  '
  Public Function SortTaskDataTable(ByRef TaskTable As DataTable, ByRef TaskOrderArray As TYPE_MULTISITE_SITE_TASKS()) As Boolean

    Dim _idx As Byte

    Try

      If m_task_sort_ds Is Nothing Then

        m_task_sort_ds = New DataSet()
        m_task_sort_ds.Tables.Add(New DataTable("SortOrder"))

        m_task_sort_ds.Tables("SortOrder").Columns.Add("TASK", GetType(Int32))
        m_task_sort_ds.Tables("SortOrder").Columns.Add("ORDER", GetType(Int32))

        For _idx = 0 To TaskOrderArray.Length - 1
          m_task_sort_ds.Tables("SortOrder").Rows.Add(TaskOrderArray(_idx), _idx)
        Next

      End If

      m_task_sort_ds.Tables.Add(TaskTable)

      m_task_sort_ds.Relations.Add("TaskOrder" _
                                  , m_task_sort_ds.Tables("SortOrder").Columns("TASK") _
                                  , TaskTable.Columns("ST_TASK_ID"))


      m_task_sort_ds.Tables(1).Columns.Add("SHOW_ORDER", GetType(Int32), "Parent.ORDER")

      Using _sorted_view = m_task_sort_ds.Tables(1).DefaultView
        _sorted_view.Sort = "SHOW_ORDER asc"
        TaskTable = Nothing
        TaskTable = _sorted_view.ToTable()
      End Using

      m_task_sort_ds.Tables(1).Constraints.Remove("TaskOrder")
      m_task_sort_ds.Relations.Remove("TaskOrder")

      m_task_sort_ds.Tables.Remove(m_task_sort_ds.Tables(1))

      Return True
    Catch ex As Exception

      Return False
    End Try

  End Function

  ' PURPOSE : Translate the multisite process id to a readbale string
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - Movement type description
  '
  Public Function TranslateMultiSiteProcessID(ByVal Process As TYPE_MULTISITE_SITE_TASKS, Optional ByVal Details As Boolean = True) As String
    Dim _nls_id As Integer
    Dim _param As String
    Dim _task_name As String

    _nls_id = TranslateMultiSiteProcessIDToNlsId(Process)
    _task_name = System.Enum.GetName(GetType(TYPE_MULTISITE_SITE_TASKS), Process)
    _param = String.Empty

    If String.IsNullOrEmpty(_task_name) Then

      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(786) '' Unknown
    End If

    If Details Then
      If _task_name.ToUpper.Contains("UPLOAD") Then
        _param = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1757) & ")" ' 1757 "From Site to MultiSite"
      ElseIf _task_name.ToUpper.Contains("DOWNLOAD") Then
        _param = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1758) & ")" ' 1758 "From MultiSite to Site"
      End If
    End If

    Return NLS_GetString(_nls_id, _param)

  End Function


  ' PURPOSE : Translate the multisite process id to a readbale string
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - Movement type description
  '
  Public Function TranslateMultiSiteProcessIDToNlsId(ByVal Process As TYPE_MULTISITE_SITE_TASKS) As Integer
    Dim _nls_id As Integer = 0

    Select Case Process
      Case TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1779) '1779 "Configuration"

      Case TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1765) '1765 "Points %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1767) '1767 "Accounts %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1766) '1766 "Points %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1768) '1768 "Accounts  %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1770) '1770 "Account Info Read %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1840) '1840 "Statistics %1"      

      Case TYPE_MULTISITE_SITE_TASKS.UploadTerminals
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1843) '1843 "Terminals %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadAreas
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1844) '1844 "Areas %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadBanks
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1845) '1845 "Banks %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadProviders
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1846) '1846 "Providers %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1847) '1847 "Play Sessions %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1938) '1938 "Terminals Connected %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1980) ' 1980 "Games by providers %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadProvidersGames
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1981) ' 1981 "Games by providers %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadProviders
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1982) ' 1982 "Providers %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1983) ' 1983 "Play Sessions by Game %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(2086) ' 1983 "Masters Profiles %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(2358)  ' 2358 "Common Users"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, _
           TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(2244)  ' 2244 "Account Documents %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadLastActivity
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(2505)  ' 2505 "Last account activity %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(4729)  ' 4729 "Cashier movements grouped by hour %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadAlarms
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(4858)  ' 4729 "Alarms %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(4971)  ' 4729 "Patterns %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(5785)  ' 5770 "LCD messages %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadHandpays
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(5826)  ' 5826 "Handpays %1"

      Case TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(6034)  ' 6034 "Sells and pays %1"

      Case TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(6997)  ' 6997 "Points and credit buckets configuration"

      Case TYPE_MULTISITE_SITE_TASKS.UploadCreditLines
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(7945)  ' 7945 "CreditLines"

      Case TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(7994)  ' 7994 "Creditline Movements"

      Case Else
        _nls_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(786) '786 "Unknown"

    End Select

    Return _nls_id
  End Function

  ' PURPOSE: Get a human-readable list contaning the resyncable tasks
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Public Function GetReSyncableTasksList() As String
    Dim _tasks_list As String
    Dim _tasks_array() As TYPE_MULTISITE_SITE_TASKS
    Dim _temp_array() As TYPE_MULTISITE_SITE_TASKS
    Dim _str_bld As StringBuilder
    Dim _sql_transaction As SqlTransaction
    Dim _task_id As Int32
    Dim _aux_tasks_list As String

    _tasks_list = ""
    _tasks_array = ReSyncTasks
    _temp_array = Nothing
    _sql_transaction = Nothing
    _aux_tasks_list = ""

    Try
      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "mdl_multisite", _
                              "GetReSyncableTasksList", _
                              "GUI_BeginSQLTransaction failed")

      End If

      For Each _task As TYPE_MULTISITE_SITE_TASKS In ReSyncTasks
        If _aux_tasks_list <> "" Then
          _aux_tasks_list &= ", "
        End If

        _aux_tasks_list &= _task
      Next

      _str_bld = New StringBuilder()

      _str_bld.AppendLine("  SELECT   ST_TASK_ID                              ")
      _str_bld.AppendLine("    FROM   MS_SITE_TASKS                           ")
      _str_bld.AppendLine("   WHERE   ST_ENABLED = 1                          ")
      _str_bld.AppendLine("     AND   ST_TASK_ID IN (" & _aux_tasks_list & ") ")

      If GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        _str_bld.AppendLine("   AND   ST_SITE_ID = 0                          ")
      End If

      _str_bld.AppendLine("ORDER BY   ST_TASK_ID                              ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)
        Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()

          Array.Resize(_temp_array, 0)

          While _sql_reader.Read()
            _task_id = _sql_reader.GetInt32(0)
            Array.Resize(_temp_array, _temp_array.Length + 1)
            _temp_array(_temp_array.Length - 1) = _task_id
          End While
        End Using
      End Using

      _tasks_array = _temp_array

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      "mdl_multisite", _
                      "GetReSyncableTasksList", _
                      _ex.Message)

    Finally
      For Each _task As TYPE_MULTISITE_SITE_TASKS In _tasks_array
        _tasks_list &= vbCrLf & " - " & mdl_multisite.TranslateMultiSiteProcessID(_task)
      Next

    End Try

    Return _tasks_list

  End Function ' GetReSyncableTasksList


  Public Function GetTaskFieldProperties() As Dictionary(Of TYPE_MULTISITE_SITE_TASKS, TYPE_MULTISITE_TASK_FIELD_PROPERTIES())
    Dim _task_properties As New Dictionary(Of TYPE_MULTISITE_SITE_TASKS, TYPE_MULTISITE_TASK_FIELD_PROPERTIES())

    '' TASK Unknown
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.Unknown, _task_properties, False, True, True, True, 0, Int32.MaxValue, False, True, True, 0, Int32.MaxValue)
    '' TASK DownloadConfiguration
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration, _task_properties, True, False, True, False, 0, UInt16.MaxValue, False, False, False, 0, 0)
    '' TASK UploadPointsMovements
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements, _task_properties, True, True, True, True, 0, 900, False, True, True, 1, 1000)
    '' TASK UploadPersonalInfo
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo, _task_properties, True, True, True, True, 0, 900, False, True, True, 1, 1000)
    '' TASK UploadAccountDocuments
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments, _task_properties, True, True, True, True, 0, 90000, False, True, False, 1, 1)
    '' TASK DownloadPoints_Site
    '' TODO :  Currently isn't sync.
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site, _task_properties, False, True, True, True, 0, 600, False, True, True, 100, 1000)
    '' TASK DownloadPoints_All
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, _task_properties, True, True, True, True, 0, 3600, False, True, True, 100, 1000)
    '' TASK DownloadPersonalInfo_All
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, _task_properties, True, True, True, True, 0, 3600, True, True, True, 1, 1000)
    '' TASK DownloadProvidersGames
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, _task_properties, True, True, True, True, 60, 7200, False, True, True, 100, 1000)
    '' TASK DownloadProviders
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadProviders, _task_properties, True, True, True, True, 60, 7200, False, True, True, 100, 1000)
    '' TASK DownloadAccountDocuments
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, _task_properties, True, True, True, True, 0, 90000, False, True, False, 1, 1)
    '' TASK UploadSalesPerHour
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour, _task_properties, True, True, True, True, 300, 7200, False, True, True, 1, 1000)
    '' TASK UploadPlaySessions
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions, _task_properties, True, True, True, True, 60, 7200, False, True, True, 1, 1000)
    '' TASK UploadBanks
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadBanks, _task_properties, True, True, True, True, 300, 7200, False, True, True, 1, 1000)
    '' TASK UploadAreas
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadAreas, _task_properties, True, True, True, True, 300, 7200, False, True, True, 1, 1000)
    '' TASK UploadTerminals
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadTerminals, _task_properties, True, True, True, True, 300, 7200, False, True, True, 1, 1000)
    '' TASK UploadTerminalsConnected
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected, _task_properties, True, True, True, True, 300, 7200, False, True, True, 1, 1000)
    '' TASK UploadGamePlaySessions
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions, _task_properties, True, True, True, True, 0, 7200, True, True, True, 1, 1000)
    '' TASK DownloadMasterProfiles
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, _task_properties, True, True, True, True, 300, 7200, False, True, False, 1, 1000)
    '' TASK DownloadCorporateUsers
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, _task_properties, True, True, True, True, 300, 7200, False, True, True, 1, 1000)
    '' TASK UploadLastActivity
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadLastActivity, _task_properties, True, True, True, True, 300, 7200, False, True, True, 100, 1000)
    '' TASK UploadCashierSesionsGrupedByHour
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour, _task_properties, True, True, True, True, 60, 7200, False, True, True, 100, 500)
    '' TASK UploadAlarms
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadAlarms, _task_properties, True, True, True, True, 300, 7200, False, True, True, 100, 1000)
    '' TASK DownloadAlarmPatterns
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, _task_properties, True, True, True, True, 60, 7200, False, True, True, 100, 1000)
    '' TASK DownloadLcdMessages
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages, _task_properties, True, True, True, True, 60, 7200, False, True, True, 1, 100)
    '' TASK UploadHandpays
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadHandpays, _task_properties, True, True, True, True, 60, 7200, False, True, True, 1, 1000)
    '' TASK UploadSellsAndBuys
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys, _task_properties, True, True, True, True, 60, 7200, False, True, True, 1, 100)
    '' TASK UploadCreditLines
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadCreditLines, _task_properties, True, True, True, True, 0, 900, False, True, True, 1, 1000)
    '' TASK UploadCreditLine Movements
    SetTaskProperty(TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements, _task_properties, True, True, True, True, 0, 900, False, True, True, 1, 1000)


    Return _task_properties

  End Function


  ' PURPOSE: SetTaskProperty
  '          Set field properties for a Task, like max value , is read onyl...
  ' PARAMS:
  '    - INPUT:
  '        - TaskId
  '        - TaskData																
  '        - IsTaskEnabled													
  '        - IsIntervalSecondsEnabled								
  '        - IsIntervalSecondsEditable							
  '        - IntervalSecondsMinValue								
  '        - IntervalSecondsMaxValue								
  '        - IsRealTimeTask													
  '        - IsMaxRowsEnabled												
  '        - IsMaxRowsEditable											
  '        - MaxRowsMinValue												
  '        - MaxRowsMaxValue					
  '        - Not implemented yet...
  '        - IsStartingHourEnabled									
  '        - IsStartingHourEditable									
  '        - StartingHourMinValue										
  '        - StartingHourMaxValue				
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetTaskProperty(ByVal TaskId As TYPE_MULTISITE_SITE_TASKS _
                                , ByRef TaskData As Dictionary(Of TYPE_MULTISITE_SITE_TASKS, TYPE_MULTISITE_TASK_FIELD_PROPERTIES()) _
                                , ByVal IsTaskEnabled As Boolean _
                                , ByVal IsTaskEnabledEditable As Boolean _
                                , ByVal IsIntervalSecondsEnabled As Boolean _
                                , ByVal IsIntervalSecondsEditable As Boolean _
                                , ByVal IntervalSecondsMinValue As Int32 _
                                , ByVal IntervalSecondsMaxValue As Int32 _
                                , ByVal IsRealTimeTask As Boolean _
                                , ByVal IsMaxRowsEnabled As Boolean _
                                , ByVal IsMaxRowsEditable As Boolean _
                                , ByVal MaxRowsMinValue As Int32 _
                                , ByVal MaxRowsMaxValue As Int32)

    Dim _column_properties As TYPE_MULTISITE_TASK_FIELD_PROPERTIES()

    _column_properties = New TYPE_MULTISITE_TASK_FIELD_PROPERTIES(GRID_TASKS_NUM_COLUMNS) {}
    '' Task Enabled
    _column_properties(PROPERTY_TASKS_ENABLED).Enabled = IsTaskEnabled
    _column_properties(PROPERTY_TASKS_ENABLED).Editable = IsTaskEnabledEditable
    '' Interval Seconds
    _column_properties(PROPERTY_TASKS_INTERVAL_SECONDS).Enabled = IsIntervalSecondsEnabled
    _column_properties(PROPERTY_TASKS_INTERVAL_SECONDS).Editable = IsIntervalSecondsEditable
    _column_properties(PROPERTY_TASKS_INTERVAL_SECONDS).MinValue = IntervalSecondsMinValue
    _column_properties(PROPERTY_TASKS_INTERVAL_SECONDS).MaxValue = IntervalSecondsMaxValue
    _column_properties(PROPERTY_TASKS_INTERVAL_SECONDS).RealTimeInterval = IsRealTimeTask
    '' Max rows
    _column_properties(PROPERTY_TASKS_MAX_ROWS2UPLOAD).Enabled = IsMaxRowsEnabled
    _column_properties(PROPERTY_TASKS_MAX_ROWS2UPLOAD).Editable = IsMaxRowsEditable
    _column_properties(PROPERTY_TASKS_MAX_ROWS2UPLOAD).MinValue = MaxRowsMinValue
    _column_properties(PROPERTY_TASKS_MAX_ROWS2UPLOAD).MaxValue = MaxRowsMaxValue

    ''' Start Hour ( Not implemented )
    ''_column_properties(PROPERTY_TASKS_START_HOUR).Enabled = IsStartingHourEnabled
    '_column_properties(PROPERTY_TASKS_START_HOUR).Editable = IsStartingHourEditable
    '_column_properties(PROPERTY_TASKS_START_HOUR).MinValue = StartingHourMinValue
    '_column_properties(PROPERTY_TASKS_START_HOUR).MaxValue = StartingHourMaxValue

    Call TaskData.Add(TaskId, _column_properties)

  End Sub

#End Region

End Module
