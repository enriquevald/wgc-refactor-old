'----------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'----------------------------------------------------------------------------------------
'
' MODULE NAME : mdl_print_account_info.vb
'
' DESCRIPTION : Functions to manage print account info
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 31-JUL-2013  FBA    Initial version
'---------------------------------------------------------------------------------------

Imports System.Data.SqlClient

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text

Module mdl_print_account_info

  ' PURPOSE: Prints the account info
  '
  ' PARAMS:
  '   - INPUT:
  '     - IncludeScannedDocs As Boolean
  '     - Card As CardData
  ' RETURNS:
  '     
  '
  Public Sub PrintPlayerData(ByVal IncludeScannedDocs As Boolean, ByVal Card As CardData)

    Dim _document_stream As IO.MemoryStream
    Dim _file_name As String
    Dim _path As String
    Dim _print_params As WSI.Common.GhostScriptParameter

    _document_stream = Nothing
    _file_name = String.Format("{0}{1}.PDF", System.IO.Path.GetTempFileName(), WSI.Common.WGDB.Now.ToString("MMddyyyyHHmmssfff"))

    _path = IO.Path.Combine(System.IO.Path.GetTempPath(), _file_name)

    Try
      'Create PDF file
      If (WSI.Common.CashierDocs.CreateDocumentAccountData(Card, IncludeScannedDocs, _document_stream)) Then
        IO.File.WriteAllBytes(_path, _document_stream.ToArray())
      End If

      If IO.File.Exists(_path) Then
        'Print 
        _print_params = New WSI.Common.GhostScriptParameter(_path)
        _print_params.PrinterDialog = True

        If Not WSI.Common.GSPrint.Print(_print_params) Then
          '564 the document was not printed
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(564), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        End If
        'Delete file
        IO.File.Delete(_path)
      End If

    Catch ex As Exception
      Log.Exception(ex)
    End Try

  End Sub ' PrintPlayerData

End Module
