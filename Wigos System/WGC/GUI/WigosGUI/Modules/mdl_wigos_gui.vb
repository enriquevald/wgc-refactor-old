'-------------------------------------------------------------------
' Copyright � 2002-2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_wigos_gui
'
' DESCRIPTION:   Constants, types, variables definitions related to
'                WigosGUI initialization.
'
' AUTHOR:        Carlos A. Costa
'
' CREATION DATE: 25-03-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-03-2002  CAC    Initial version
' 24-FEB-2012 MPO    Install Adobe Acrobat reader if necessary.
' 12-MAR-2012 MPO    Initialize resources (Main)
' 31-MAY-2012 JAB    Group component installation.
' 05-MAR-2013 MPO    Moved "add permission" to GUI_Controls.Misc.GUI_Init
' 28-AUG-2013 JCA    Added GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI
' 25-JUL-2017 RGR    Bug 28949:WIGOS-3834 Cage - The general report of cage is taking too long to open the report
' 07-JUL-2017 ETP    PBI WIGOS-4571 AGG Multisite entry mode.
'-------------------------------------------------------------------
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.IO
Imports WSI.Common.WGDB
Imports Unity
Imports Wigos.GUI.Adapter

Public Module mdl_wigos_gui

#Region "External Prototypes"

  Private Declare Function Common_ProcessReleaseCandidate Lib "CommonBase" (ByVal Path As String, ByVal ExeName As String) As Boolean

#End Region

#Region "Constants"

  Public Const TERMINAL_TYPE_AGENCY As Integer = 0
  Public Const TERMINAL_TYPE_ALL_LKTS As Integer = 999

#End Region

#Region "Structures"

#End Region

#Region "Members"

#End Region

#Region "Public Functions"


  ' PURPOSE: Main function. Initial actions:
  '           - Add forms data (Profiles)
  '           - Perform GUI Initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub Main()
    Dim _current_path As String
    Dim _app_name As String
    Dim _is_multisite As Boolean
    Dim _show_site_selector As Boolean
    Dim _is_multisite_selected As Boolean
    Dim _user_cancel_site_selection As Boolean
    Dim _dt_sites As DataTable
    Dim _initial_language_code As String
    Dim _language_id As ENUM_NLS_LANGUAGE
    Dim _ftp_config As WSI.Common.DbConfig
    Dim _site_config As WSI.Common.DbConfig
    Dim container As IUnityContainer


    _user_cancel_site_selection = False
    _is_multisite_selected = True
    _show_site_selector = False


    ' Create a container
    container = BuildUnityContainer()
    cls_global_params.Instance.GUIContainer = container
    ' 
    Log.AddListener(New SimpleLogger("GUI"))

    ' Apply any pending software-update
    _current_path = "."
    _app_name = Environment.CommandLine.Replace(Chr(34), Chr(32)).Trim
    Common_ProcessReleaseCandidate(_current_path, _app_name)

    ' 31-MAY-2012 JAB Group component installation.
    InstallExternalComponents()

    _is_multisite = False
    _dt_sites = New DataTable()
    _initial_language_code = "en-US"
    _language_id = ENUM_NLS_LANGUAGE.NLS_LANGUAGE_ENGLISH
    GLB_StartedInSelectSiteMode = False

    _ftp_config = WSI.Common.DbConfig.CreateFromFile("WigosGUI.cfg")
    If (_ftp_config.DbId < 0) Then
      MsgBox("Error reading application configuration settings. Application will stop")

      Environment.Exit(0)
    End If

    _site_config = WSI.Common.DbConfig.CreateFromFile("WigosGUI.cfg")

    WGDB.Init(_ftp_config.DbId, _ftp_config.Server1, _ftp_config.Server2)

    If Not WGDB.ReadSiteMultiSite(_is_multisite, _dt_sites, _initial_language_code, _show_site_selector) Then
      MsgBox("Error reading application configuration settings. Application will stop")

      Environment.Exit(0)
    End If

    If WGDB.IsEnableVisualStyles() Then
      Application.EnableVisualStyles()

    End If

    If _is_multisite Then

      GLB_GuiMode = ENUM_GUI.MULTISITE_GUI
      '' 
      ' Compare returned value with possible values:
      ' - en-US
      ' - es
      ' - fr
      ''
      If (String.Compare(_initial_language_code, "es", True) = 0) Then
        ' Set to spanish
        _language_id = ENUM_NLS_LANGUAGE.NLS_LANGUAGE_SPANISH
      End If
      If (String.Compare(_initial_language_code, "fr", True) = 0) Then
        ' Set to French
        _language_id = ENUM_NLS_LANGUAGE.NLS_LANGUAGE_FRENCH
      End If

      If (_show_site_selector) Then ' GP not AGG.Buffering.enabled == 1
        GLB_StartedInSelectSiteMode = True
        _user_cancel_site_selection = frm_sites_selector.ShowSelector(_language_id _
                                                                      , _dt_sites _
                                                                      , _is_multisite_selected _
                                                                      , _site_config)
      End If

      If Not _user_cancel_site_selection Then

        If _is_multisite_selected Then

          GLB_GuiMode = ENUM_GUI.MULTISITE_GUI
          Call GUI_Init(GLB_NLS_GUI_AUDITOR.Id(87), _
                        AddressOf GUI_MainMultiSite, _
                        _ftp_config, _site_config)
        Else
          GLB_GuiMode = ENUM_GUI.WIGOS_GUI
          Call GUI_Init(GLB_NLS_GUI_AUDITOR.Id(100), _
                        AddressOf GUI_Main, _
                        _ftp_config, _site_config)
        End If
      End If

    Else

      GLB_GuiMode = ENUM_GUI.WIGOS_GUI

      If WGDB.ReadCashDeskDraw() Then 'JCA CashDeskDraws

        GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI
        GLB_StartedInSelectSiteMode = False

        Call GUI_Init(GLB_NLS_GUI_AUDITOR.Id(475), _
                      AddressOf GUI_MainCashdesk_Draws, _
                      _ftp_config, _site_config)

      Else ' GUI: default mode 

        Call GUI_Init(GLB_NLS_GUI_AUDITOR.Id(100), _
                      AddressOf GUI_Main, _
                      _ftp_config, _site_config)

      End If

    End If

    Environment.Exit(0)

  End Sub ' Main

  ''' <summary>
  ''' Create the Unity container
  ''' </summary>
  ''' <param name="_container"></param>
  ''' <remarks></remarks>
  Private Function BuildUnityContainer() As IUnityContainer
    Dim container As IUnityContainer
    container = New UnityContainer

    ' Init adapter containet
    Bootstrapper.Initialize(container)

    Return container
  End Function


  ' PURPOSE: Group component installation.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InstallExternalComponents()
    Dim _ocx_file As String
    Dim _ocx_files() As String = {"vsFlex8.ocx", "mschrt20.ocx"}

    ' JAB 31-MAY-2012: ocx files register
    ' JML 28-NOV-2013: Call to common function in kernel
    For Each _ocx_file In _ocx_files
      WSI.Common.Misc.InstallExternalComponent(_ocx_file)
    Next

    ' 24-FEB-2012 MPO Install Adobe Acrobat reader if necessary.
    If Not WSI.Common.AdobeReader.Installed Then

      WSI.Common.AdobeReader.Install()

    End If

  End Sub

  ' PURPOSE: Main function (start application)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub GUI_MainCashdesk_Draws()
    Dim frm As frm_main_cashdesk_draws

    ' Create instance
    frm = New frm_main_cashdesk_draws

    Call frm.ShowMainWindow()
    Call frm.Dispose()

    frm = Nothing

  End Sub ' GUI_Main


  ' PURPOSE: Main function (start application)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub GUI_Main()
    Dim frm As frm_main

    ' LEM & RCI 02-OCT-2014: Init TerminalReport
    TerminalReport.Init()

    ' Create instance
    frm = New frm_main

    Call frm.ShowMainWindow()
    Call frm.Dispose()

    frm = Nothing

  End Sub ' GUI_Main

  ' PURPOSE: Main function (start application)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub GUI_MainMultiSite()
    Dim frm As frm_main_multisite

    ' Init Terminal Report 
    TerminalReport.Init()

    ' Create instance
    frm = New frm_main_multisite

    Call frm.ShowMainWindow()
    Call frm.Dispose()

    frm = Nothing

  End Sub ' GUI_Main

  ' PURPOSE: Main function (start application)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  'TITO : HBB
  'Public Sub GUI_MainTito()
  '  Dim frm As frm_main_tito

  '  ' Create instance
  '  frm = New frm_main_tito

  '  Call frm.ShowMainWindow()
  '  Call frm.Dispose()

  '  frm = Nothing

  'End Sub ' GUI_Main

#End Region

#Region "Private Functions"

#End Region

End Module