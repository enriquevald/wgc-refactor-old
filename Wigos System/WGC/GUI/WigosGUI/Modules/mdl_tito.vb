'----------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'----------------------------------------------------------------------------------------
'
' MODULE NAME : mdl_tito.vb
'
' DESCRIPTION : Miscelaneous functions to manage common operations related to stackers (TITO)
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 03-JUL-2013  NMR    Initial version
' 28-AUG-2013  NMR    Fixed error ocurred updating table money_collection
' 03-OCT-2013  LEM    WIGOSTITO-317:  
'                     Added: GetTerminalName, GetTerminalName, ExtractStackerCollection, UnlinkStackerCollection, LinkStackerCollection, ExistsDuplicateTerminalStacker, UpdateMoneyCollection
'                     Modified: ExistsPendingCollection
'                     Removed: UpdatePendingCollections
' 10-OCT-2013  LEM    Added NACardCashDeposit modified from CashierBussinessLogic for creation bills colecction movements
' 11-NOV-2013  HBB    InsertMoneyCollection calls the function DB_CreateMoneyCollection in the Kernel. Before It calls 
' 03-DEC-2013  AMF    Collection status as string
' 04-MAY-2014  YNM    Fixed Bug WIG-2207: Mismatch in the name of the assigned terminal.
'---------------------------------------------------------------------------------------

Imports System.Data.SqlClient

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text

Public Module mdl_tito

#Region " Members "

  Public m_minimal_date As DateTime = New DateTime(2007, 1, 1)

#End Region

#Region " Structs "

  Public Class SmartTerminal
    Public terminal_name As String
    Public terminal_id As Int64
    Public provider_id As Int64
    Public provider_name As String

    Public Sub New()
      terminal_id = 0
    End Sub

    Public Sub New(ByVal TerminalId As Int64)
      terminal_id = TerminalId
    End Sub
  End Class

#End Region ' Structs

#Region " Public functions and methods "

  ' PURPOSE: Return a string representation of ENUM_STACKER_STATE
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: ENUM_STACKER_STATE
  '
  ' RETURNS:
  '     - String
  '
  Public Function StackerStatusAsString(ByVal Status As TITO_STACKER_STATUS) As String

    Dim _status_str As String

    Select Case Status
      Case TITO_STACKER_STATUS.DEACTIVATED
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359)

      Case TITO_STACKER_STATUS.ACTIVE
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008)

      Case Else
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4567)
    End Select

    Return _status_str

  End Function

  ' PURPOSE: Return a string representation of TITO_TICKET_TYPE
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: TITO_TICKET_TYPE
  '
  ' RETURNS:
  '     - String
  '
  Public Function TicketTypeAsString(ByVal ticketType As String) As String
    Select Case ticketType
      Case TITO_TICKET_TYPE.CASHABLE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      Case TITO_TICKET_TYPE.PROMO_REDEEM
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))
      Case TITO_TICKET_TYPE.PROMO_NONREDEEM
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))
      Case TITO_TICKET_TYPE.HANDPAY
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)
      Case TITO_TICKET_TYPE.JACKPOT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)
      Case TITO_TICKET_TYPE.OFFLINE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)
      Case Else
        Return ""
    End Select
  End Function  'TicketTypeAsString



  ' PURPOSE: Return a string representation of TITO_TICKET_STATUS
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: TITO_TICKET_STATUS
  '
  ' RETURNS:
  '     - String
  '
  Public Function TicketStatusAsString(ByVal TicketStatus As String) As String
    Return TicketStatusAsString(TicketStatus, False)
  End Function

  Public Function TicketStatusAsString(ByVal TicketStatus As String, ByVal IsSpecial As Boolean) As String

    'ticket actual status
    Select Case (TicketStatus)
      Case WSI.Common.TITO_TICKET_STATUS.VALID
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(3010)
      Case WSI.Common.TITO_TICKET_STATUS.CANCELED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7671)
      Case WSI.Common.TITO_TICKET_STATUS.REDEEMED
        If IsSpecial Then
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
        Else
          Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138)
        End If
      Case WSI.Common.TITO_TICKET_STATUS.EXPIRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
      Case WSI.Common.TITO_TICKET_STATUS.DISCARDED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2989)
      Case WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7672)
      Case WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7648)
      Case WSI.Common.TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
      Case WSI.Common.TITO_TICKET_STATUS.SWAP_FOR_CHIPS
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7145) 'EOR 08-MAR-2016 Type State
      Case WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7278)
      Case WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(7279)
      Case Else
        Return ""
    End Select
  End Function 'TicketStatusAsString



  ' PURPOSE: Return a string representation of TITO_MONEY_COLLECTION_STATUS
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: TITO_MONEY_COLLECTION_STATUS
  '
  ' RETURNS:
  '     - String
  '

  Public Function CollectionStatusAsString(ByVal Status As TITO_MONEY_COLLECTION_STATUS) As String

    Dim _status_str As String

    Select Case Status
      Case TITO_MONEY_COLLECTION_STATUS.NONE
        _status_str = ""
      Case TITO_MONEY_COLLECTION_STATUS.OPEN
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2742)
      Case TITO_MONEY_COLLECTION_STATUS.PENDING
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)
      Case TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2233)
      Case TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2232)
      Case 4 'DRV: Status Active with activity (Only used in frm_tito_sacker_collections_sel
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4737)
      Case Else
        _status_str = ""
    End Select

    Return _status_str

  End Function

  ' PURPOSE: Check if in the passed Object exists; If not return 0
  '
  ' PARAMS:
  '   - INPUT:
  '     - Value: column read from BD
  '
  ' RETURNS:
  '     - 0: if value is Null
  '     - Value: otherwise
  Public Function GetValueForGrid(ByVal Value As Object) As Object

    Try
      If Not DBNull.Value.Equals(Value) Then
        Return Value
      End If
    Catch _ex As Exception
    End Try

    Return ""

  End Function
  ' PURPOSE: Check if in the passed Object exists; If not return 0
  '
  ' PARAMS:
  '   - INPUT:
  '     - Value: column read from BD
  '
  ' RETURNS:
  '     - 0: if value is Null
  '     - Value: otherwise
  Public Function GetDefaultNumber(ByVal Value As Object) As Object

    Try
      If Not DBNull.Value.Equals(Value) Then
        Return Value
      End If
    Catch _ex As Exception
    End Try

    Return -1

  End Function

  ' PURPOSE: Check if in the passed Object exists; If not return DateTime(0)
  '
  ' PARAMS:
  '   - INPUT:
  '     - Value: column read from BD or Object
  '
  ' RETURNS:
  '     - DateTime(0): if value is Null
  '     - Value: otherwise
  Public Function GetDefaultDate(ByVal Value As Object) As Object

    Try
      If Not DBNull.Value.Equals(Value) Then
        Return Value
      End If
    Catch _ex As Exception
    End Try

    Return New DateTime(0)

  End Function

  ' PURPOSE: Check if in the passed TYPE_DATE_TIME exists; If not return DateTime(0)
  '
  ' PARAMS:
  '   - INPUT:
  '     - Value: column read from BD or TYPE_DATE_TIME object
  '
  ' RETURNS:
  '     - DateTime(0): if value is Null
  '     - Value: otherwise
  Public Function GetDefaultDate(ByVal Value As TYPE_DATE_TIME) As DateTime

    Try
      If Not Value.IsNull Then
        Return Value.Value
      End If
    Catch _ex As Exception
    End Try

    Return New DateTime(0)

  End Function

  ' PURPOSE: Return a String with default formated date for Stackers functions purpouses
  '
  ' PARAMS:
  '   - INPUT:
  '     - Value: column read from BD or TYPE_DATE_TIME object
  '
  ' RETURNS:
  '     - yyyy/MM/dd HH:mm:ss: if value is Not Null
  '     - "": otherwise
  Public Function GetFormatedDate(ByVal Value As Object) As String

    Dim _date As DateTime

    If Not DBNull.Value.Equals(Value) Then
      Try
        _date = Value
        If _date > m_minimal_date Then
          Return GUI_FormatDate(Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
      Catch _ex As Exception
      End Try
    End If

    Return ""

  End Function

  ' PURPOSE: Return a String with default formated date for Stackers functions purpouses
  '
  ' PARAMS:
  '   - INPUT:
  '     - Value: column read from BD or TYPE_DATE_TIME object
  '
  ' RETURNS:
  '     - yyyy/MM/dd HH:mm:ss: if value is Not Null
  '     - "": otherwise
  Public Function GetFormatedCurrency(ByVal Value As Object) As String

    Try
      If Not DBNull.Value.Equals(Value) Then
        Return GUI_FormatCurrency(Value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    Catch _ex As Exception
    End Try

    Return ""

  End Function

  ' PURPOSE: Read MobileBank info and save movements for deposit cash operation
  ' 
  '  PARAMS:
  '      - INPUT:
  '          - CashierSessionId
  '          - UserType
  '          - AmountToDeposit
  '          - TerminalId
  '          - Trx
  '      - OUTPUT:
  '
  ' RETURNS:
  '      - True: operation completed successfully
  '      - False: operation failed
  '
  Public Function NACardCashDeposit(ByVal CashierSessionId As Int64, _
                                    ByVal UserType As GU_USER_TYPE, _
                                    ByVal AmountToDeposit As Currency, _
                                    ByVal TerminalId As Long, _
                                    ByVal Trx As SqlTransaction) As Boolean
    Dim _movement_id As Long
    Dim _cashier_user_id As Int32
    Dim _cashier_user_name As String
    Dim _cashier_terminal_id As Int32
    Dim _cashier_terminal_name As String
    Dim _cashier_balance As Currency
    Dim _session_info As CashierSessionInfo
    Dim _cashier_movements As CashierMovementsTable
    Dim _terminal_name As String
    Dim _mb_user_type As MB_USER_TYPE
    Dim _mb_account_id As Int64
    Dim _mb_track_data As String
    Dim _dummy_mb_cashier_session_id As Int64
    Dim _mb_movement_Info As MBMovementData
    Dim _terminal_info As WSI.Common.Terminal.TerminalInfo

    _movement_id = 0
    _cashier_user_id = 0
    _cashier_user_name = ""
    _cashier_terminal_id = 0
    _cashier_terminal_name = ""
    _cashier_balance = 0
    _terminal_name = ""
    _mb_track_data = ""
    _terminal_info = Nothing

    Try
      If TerminalId > 0 Then
        If Not WSI.Common.Misc.GetTerminalName(TerminalId, _terminal_name, Trx) Then
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "mdl_tito", "NACardCashDeposit", "NACardCashDeposit Error. GetTerminalName")

          Return False
        End If
      End If

      Select Case UserType
        Case GU_USER_TYPE.SYS_ACCEPTOR
          _mb_user_type = MB_USER_TYPE.SYS_ACCEPTOR
        Case GU_USER_TYPE.SYS_TITO
          _mb_user_type = MB_USER_TYPE.SYS_TITO
        Case Else
          _mb_user_type = MB_USER_TYPE.SYS_PROMOBOX
      End Select

      If Not WSI.Common.Cashier.ReadMobileBankNoteUser(Trx, _mb_user_type, _mb_account_id, _mb_track_data, _dummy_mb_cashier_session_id, _terminal_name) Then
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "mdl_tito", "NACardCashDeposit", "NACardCashDeposit Error. ReadMobileBankNoteUser")

        Return False
      End If

      GetCashierInfo(CashierSessionId, Trx, _cashier_user_id, _cashier_user_name, _cashier_terminal_id, _cashier_terminal_name, _cashier_balance)

      _mb_movement_Info = New MBMovementData()
      _mb_movement_Info.CashierId = _cashier_terminal_id
      _mb_movement_Info.CardId = _mb_account_id
      _mb_movement_Info.Type = MBMovementType.DepositCash
      _mb_movement_Info.CashierSessionId = CashierSessionId
      _mb_movement_Info.InitialBalance = 0
      _mb_movement_Info.SubAmount = 0
      _mb_movement_Info.AddAmount = AmountToDeposit
      _mb_movement_Info.FinalBalance = AmountToDeposit
      _mb_movement_Info.CashierName = _terminal_name

      MobileBank.DB_InsertMBCardMovement(_mb_movement_Info, _movement_id, Trx)

      _session_info = New CashierSessionInfo()
      _session_info.CashierSessionId = CashierSessionId
      _session_info.TerminalId = _cashier_terminal_id
      _session_info.UserId = _cashier_user_id
      _session_info.TerminalName = _cashier_terminal_name
      _session_info.UserName = _cashier_user_name
      _session_info.AuthorizedByUserId = _session_info.UserId
      _session_info.AuthorizedByUserName = _session_info.UserName

      _cashier_movements = New CashierMovementsTable(_session_info)

      ' DHA 01-FEB-2016: on Dual Currency, set Terminal ISO Code to movement
      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And UserType = GU_USER_TYPE.SYS_TITO Then
        WSI.Common.Terminal.Trx_GetTerminalInfo(CInt(TerminalId), _terminal_info, Trx)
        If (String.IsNullOrEmpty(_terminal_info.CurrencyCode)) Then
          _terminal_info.CurrencyCode = CurrencyExchange.GetNationalCurrency()
        End If
        _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_CASH_IN, AmountToDeposit, _mb_account_id, _mb_track_data, "", _terminal_info.CurrencyCode)
      Else
        _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_CASH_IN, AmountToDeposit, _mb_account_id, _mb_track_data)
      End If

      If Not _cashier_movements.Save(Trx) Then

        Return False
      End If

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "mdl_tito", "NACardCashDeposit", "Exception throw: Error in Mobile Bank card deposit operation. Account type: System.")
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "mdl_tito", "NACardCashDeposit", _ex.Message)

      Return False
    End Try

    Return True
  End Function  ' NACardCashDeposit


  ' PURPOSE: Return a string representation of ENUM_TITO_REDEEM_TICKET_TRANS_STATUS
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: ENUM_TITO_REDEEM_TICKET_TRANS_STATUS
  '
  '     former RedeemTicketTransactionStatusAsString
  '     frm_ticket_audit_base
  '     Table 15.12d Gaming machine Status Codes - IGT Slot Accounting system Version 6.02
  ' RETURNS:
  '     - String
  '
  Public Function SasHostRejectReasonAsString(ByVal Status As ENUM_TITO_REDEEM_TICKET_TRANS_STATUS) As String

    Dim _status_str As String

    Select Case Status
      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_CASHABLE_REDEEMED                        '0x00                 0d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7709)                                                       '"Cashable ticket redeemed"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_RESTRICTED_PROMOTIONAL_REDEEMED          '0x01                 1d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7710)                                                       '"Restricted promotional ticket redeemed"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_NONRESTRICTED_PROMOTIONAL_REDEEMED       '0x02                 2d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7711)                                                       '"Nonrestricted promotional ticket redeemed"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_WAITING_POOL                             '0x20                32d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7712)                                                       '"Waiting for long poll"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_PENDING                                  '0x40                64d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7713)                                                       '"Ticket redemption pending (not complete)"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_BY_HOST                         '0x80               128d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7714)                                                       '"Ticket rejected by host, or unknown"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_VALIDATION_NUMBER_NOT_MATCH              '0x81               129d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7715)                                                       '"Validation number does not match"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_NOT_VALID_TRANSFER_FUNCTION              '0x82               130d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7716)                                                       '"Not a valid transfer function"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_NOT_VALID_TRANSFER_AMOUNT                '0x83               131d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7717)                                                       '"Not a valid transfer amount (non-BCD)"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_AMOUNT_EXCEEDED_EGM_LIMIT                '0x84               132d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7718)                                                       '"Transfer amount exceeded the gaming machine credit limit"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_AMOUNT_NOT_MULTIPLE_DENOM                '0x85               133d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7719)                                                       '"Transfer amount not an even multiple of gaming machine denomination"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_AMOUNT_NOT_MATCH_TICKET_AMOUNT           '0x86               134d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7720)                                                       '"Transfer amount does not match ticket amount"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_MACHINE_UNABLE_TO_ACCEPT_THIS_TRANSFER   '0x87               135d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7721)                                                       '"Gaming machine unable to accept transfer at this time"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_DUE_TIMEOUT                     '0x88               136d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7722)                                                       '"Ticket rejected due to timeout"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_DUE_COMM_LINK_DOWN              '0x89               137d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7723)                                                       '"Ticket rejected due to comm link down"              

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_REDEMPTION_DISABLED                      '0x8A               138d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7724)                                                       '"Ticket redemption disabled"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_REJECTED_DUE_VALIDATOR_FAILURE           '0x8B               139d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7725)                                                       '"Ticket rejected due to validatior failure"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_NOT_COMPATIBLE_WITH_CURRENT_REDEMPTION_CYCLE '0xC0           192d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7726)                                                       '"Not compatible with the current redemption cycle (ignored)"

      Case ENUM_TITO_REDEEM_TICKET_TRANS_STATUS.SAS_REDEEM_TICKET_TRANS_STATUS_NO_VALIDATION_INFO_AVAILABLE             '0xFF               255d
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7727)                                                       '"No validation information available"

      Case Else
        _status_str = "Unknown"
    End Select

    Return _status_str

  End Function


  ' PURPOSE: Return a string representation of ENUM_TITO_IS_VALID_TICKET_MSG
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: ENUM_TITO_IS_VALID_TICKET_MSG
  '
  '     frm_ticket_audit_base
  '     Table 15.12c Ticket Transfer codes Codes - IGT Slot Accounting system Version 6.02
  ' RETURNS:
  '     - String
  '
  Public Function WcpRejectReasonAsString(ByVal Status As ENUM_TITO_IS_VALID_TICKET_MSG) As String
    Dim _status_str As String

    Select Case Status
      Case ENUM_TITO_IS_VALID_TICKET_MSG.VALID_CASHABLE                                ' = 0x0,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7733)                      ' "Valid Cashable ticket"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.VALID_RESTRICTED_PROMOTIONAL_TICKET           ' = 0x1,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7734)                      ' "Valid restricted promotional ticket"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.VALID_NONRESTRICTED_PROMOTIONAL_TICKET        ' = 0x2,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7735)                      ' "Valid nonrestricted promotional tickets"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.UNABLE_TO_VALIDATE                            ' = 0x80, // In Case ValidatedState does not meet any of the known conditions, defaults to this
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7736)                      ' "Unable to validate (no reason given / other)"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.NOT_A_VALID_VALIDATION_NUMBER                 ' = 0x81,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7737)                      ' "Not a valid validation number"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.VALIDATION_NUMBER_NOT_IN_SYSTEM               ' = 0x82, //
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7738)                      ' "Validation number not in the system"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_MARKED_PENDING_IN_SYSTEM               ' = 0x83,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7739)                      ' "Ticket marked pending in the system"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_ALREADY_REDEEMED                       ' = 0x84,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7740)                      ' "Ticket already redeemed"


      Case ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_EXPIRED                                ' = 0x85,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7741)                      ' "Ticket expired"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.VALIDATION_INFO_NOT_AVAILABLE                 ' = 0x86,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7742)                      ' "Validation information not available"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_AMOUNT_DOES_NOT_MATCH_SYSTEM_AMOUNT    ' = 0x87
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7743)                      ' "Ticket amount does not match system amount"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_AMOUNT_EXCEEDS_AUTO_REDEMPTION_LIMIT   ' = 0x88,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7744)                      ' "Ticket amount exceeds auto redepmtion limit"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.TERMINAL_DOES_NOT_ALLOW_REDEMPTION            ' = 0x89,
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7745)                      ' "Terminal does not allow redemption"

      Case ENUM_TITO_IS_VALID_TICKET_MSG.REQUEST_FOR_CURRENT_TICKET_STATUS             ' = 0xFF
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7759)                      ' "Request for current ticket status"




        'ENUM_TITO_IS_VALID_TICKET_MSG.VALID_HANDPAY = 0x03,
        'ENUM_TITO_IS_VALID_TICKET_MSG.VALID_JACKPOT = 0x04,
        'ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_OFFLINE = 0x90,


      Case Else
        _status_str = "Unknown"
    End Select

    Return _status_str

  End Function


#End Region

#Region " Database funtions "
  ' PURPOSE: From BD, return related ID from Providers, whose name is received as parameter
  '
  ' PARAMS:
  '   - INPUT:
  '     - Name: Name/Code of terminal
  '
  ' RETURNS:
  '     ID: if Terminal with name exists
  '     0 : otherwise
  Public Function GetProviderId(ByVal Name As String) As Int64

    Dim _table As DataTable
    Dim _sql_query As String
    Dim _result As Int64

    _result = -1

    If Not String.IsNullOrEmpty(Name) Then
      _sql_query = "SELECT   PV_ID " _
                  + " FROM   PROVIDERS" _
                 + " WHERE   PV_NAME = '" & Name.Replace("'", "''") & "'"
      _table = GUI_GetTableUsingSQL(_sql_query, 2)
      If _table.Rows.Count() > 0 Then
        _result = _table.Rows.Item(0).Item(0)
      End If
    End If
    _table = Nothing

    Return _result

  End Function

  ' PURPOSE: From BD, return related Name from Providers, whose Id is received as parameter
  '
  ' PARAMS:
  '   - INPUT:
  '     - ProvId: Id or provider
  '
  ' RETURNS:
  '     String: name of provider
  '     "" : otherwise
  Public Function GetProviderName(ByVal ProvId As Int64) As String
    Dim _table As DataTable
    Dim _sql_query As String
    Dim _result As String

    _result = ""

    If ProvId >= 0 Then
      _sql_query = " SELECT   PV_NAME   " _
                 + "   FROM   PROVIDERS " _
                 + "  WHERE   PV_ID =   " & ProvId
      _table = GUI_GetTableUsingSQL(_sql_query, 2)
      If _table.Rows.Count() > 0 Then
        _result = _table.Rows.Item(0).Item(0)
      End If
      _table = Nothing
    End If

    Return _result
  End Function

  ' PURPOSE: Return related ID Provider for terminal, whose name is received as parameter
  '
  ' PARAMS:
  '   - INPUT:
  '     - Name: Name/Code of terminal
  '
  ' RETURNS:
  '     ID Provider: if Terminal with name exists
  '     0 : otherwise
  Public Function GetSmartTerminal(ByVal Name As String) As SmartTerminal

    Dim _table As DataTable
    Dim _sql_query As String
    Dim _result As SmartTerminal

    _result = New SmartTerminal
    _result.terminal_id = 0

    If Not String.IsNullOrEmpty(Name) Then
      _sql_query = "SELECT   TE_TERMINAL_ID " _
                       + " , TE_NAME" _
                       + " , TE_PROV_ID" _
                       + " , TE_PROVIDER_ID" _
                  + " FROM   TERMINALS" _
                 + " WHERE   TE_NAME = '" & Name.Replace("'", "''") & "'"
      _table = GUI_GetTableUsingSQL(_sql_query, 2)
      If _table.Rows.Count() > 0 Then
        With _table.Rows.Item(0)
          _result.terminal_id = .Item(0)
          _result.terminal_name = .Item(1)
          _result.provider_id = .Item(2)
          _result.provider_name = .Item(3)
        End With
      End If
      _table = Nothing
    End If

    Return _result

  End Function

  ' PURPOSE: Return related Name Terminal, whose Id is received as parameter
  '
  ' PARAMS:
  '   - INPUT:
  '     - TermId: Id of terminal
  '
  ' RETURNS:
  '     String: Name of terminal
  '     "" : otherwise
  Public Function GetTerminalName(ByVal TermId As Int64) As String
    Dim _table As DataTable
    Dim _sql_query As String
    Dim _result As String

    _result = ""

    If TermId > 0 Then
      _sql_query = " SELECT   TE_NAME          " _
                 + "   FROM   TERMINALS        " _
                 + "  WHERE   TE_TERMINAL_ID = " & TermId
      _table = GUI_GetTableUsingSQL(_sql_query, 2)
      If _table.Rows.Count() > 0 Then
        _result = _table.Rows.Item(0).Item(0)
      End If
      _table = Nothing
    End If

    Return _result
  End Function

  Public Function GetTerminalNameFromMoneyCollection(ByVal StackerId As Int64) As String
    Dim _table As DataTable
    Dim _sb As StringBuilder
    Dim _terminal_name As String

    _sb = New StringBuilder()

    _terminal_name = ""

    If (StackerId = 0) Then
      Return _terminal_name
    End If

    _sb.AppendLine("SELECT  TOP 1 TE_NAME                                   ")
    _sb.AppendLine("  FROM   MONEY_COLLECTIONS                         ")
    _sb.AppendLine("  LEFT   JOIN TERMINALS                            ")
    _sb.AppendLine("    ON   TERMINALS.TE_TERMINAL_ID = MC_TERMINAL_ID ")
    _sb.AppendLine(" WHERE   MC_STACKER_ID =    " + StackerId.ToString())
    _sb.AppendLine("   AND   MC_STATUS IN(0, 1)                        ")
    _sb.AppendLine(" ORDER   BY MC_DATETIME DESC                       ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), 1)

    If _table.Rows.Count() > 0 Then
      _terminal_name = _table.Rows.Item(0).Item(0)
    End If

    _table = Nothing

    Return _terminal_name
  End Function

  ' PURPOSE: Verifies if exists pending collection in terminal
  '
  ' PARAMS:
  '   - INPUT:
  '     - TerminalId: Terminal identifier
  '
  ' RETURNS:
  '     ID: If something exists
  '     0: otherwise
  '
  Public Function ExistsPendingCollection(ByVal Stacker As CLASS_STACKER, ByVal ThisStacker As Boolean, Optional ByVal ThisTerminal As Boolean = True) As Int64
    Dim _table As DataTable
    Dim _sb_query As StringBuilder
    Dim _result As Integer

    _sb_query = New StringBuilder()
    _result = 0

    _sb_query.AppendLine(" SELECT   MC_COLLECTION_ID  ")
    _sb_query.AppendLine("   FROM   MONEY_COLLECTIONS ")

    If Not ThisTerminal Then
      _sb_query.AppendLine(" WHERE   MC_TERMINAL_ID <> " & Stacker.InsertedTerminal)
    Else
      _sb_query.AppendLine(" WHERE   MC_TERMINAL_ID = " & Stacker.InsertedTerminal)
    End If

    If Not ThisStacker Then
      _sb_query.AppendLine(" AND   ( MC_STACKER_ID <> " & Stacker.StackerId & " OR MC_STACKER_ID IS NULL )")
    Else
      _sb_query.AppendLine(" AND   MC_STACKER_ID = " & Stacker.StackerId)
    End If

    _sb_query.AppendLine(" AND   MC_COLLECTION_DATETIME IS NULL ")

    _table = GUI_GetTableUsingSQL(_sb_query.ToString(), 2)
    If _table.Rows.Count() > 0 Then
      If Not DBNull.Value.Equals(_table.Rows.Item(0).Item(0)) Then
        _result = _table.Rows.Item(0).Item(0)
      End If
    End If

    _table = Nothing

    Return _result

  End Function

  ' PURPOSE: Set money collection extraction date 
  '          Only set extraction datetime if asosiated stacker status is collection pending
  '
  ' PARAMS:
  '   - INPUT:
  '     - CollectionId: 
  '
  ' RETURNS:
  '     Boolean: true if updated, false otherwise
  Public Function ExtractStackerCollection(ByVal CollectionId As Int64) As Boolean
    Dim _sql_query As String

    Try
      If CollectionId <= 0 Then

        Return False
      End If

      _sql_query = " UPDATE   MONEY_COLLECTIONS                  " _
                 + "    SET   MC_EXTRACTION_DATETIME = GETDATE() " _
                 + "  WHERE   MC_COLLECTION_ID = @pCollectionId  " _
                 + "    AND   MC_EXTRACTION_DATETIME IS NULL     "

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sql_query)

          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId
          If _db_trx.ExecuteNonQuery(_sql_cmd) <> 1 Then

            Return False
          End If
          _db_trx.Commit()

        End Using
      End Using

    Catch _ex As Exception

      Return False
    End Try

    Return True
  End Function

  Public Function UnlinkStackerCollection(ByVal CollectionId As Int64) As Boolean
    Dim _sql_query As String

    Try
      If CollectionId <= 0 Then

        Return False
      End If

      _sql_query = " UPDATE   MONEY_COLLECTIONS                                                      " _
                 + "    SET   MC_STACKER_ID = NULL                                                   " _
                 + " OUTPUT   DELETED.MC_STACKER_ID                                                  " _
                 + "  WHERE   MC_COLLECTION_ID = @pCollectionId                                      "

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sql_query)

          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId
          If _db_trx.ExecuteNonQuery(_sql_cmd) <> 1 Then

            Return False
          End If
          _db_trx.Commit()

        End Using
      End Using

    Catch _ex As Exception

      Return False
    End Try

    Return True
  End Function

  Public Function LinkStackerCollection(ByVal CollectionId As Int64, ByVal Stacker As CLASS_STACKER, ByRef StackerUnlinked As Int64) As Boolean
    Dim _sql_query As String
    Dim _reader As SqlDataReader

    StackerUnlinked = 0

    Try
      If CollectionId <= 0 Then

        Return False
      End If

      _sql_query = " UPDATE   MONEY_COLLECTIONS                 " _
                 + "    SET   MC_STACKER_ID = @pStackerId       " _
                 + " OUTPUT   DELETED.MC_EXTRACTION_DATETIME    " _
                 + "        , DELETED.MC_STACKER_ID             " _
                 + "  WHERE   MC_COLLECTION_ID = @pCollectionId "


      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sql_query)

          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId
          _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = Stacker.StackerId

          _reader = _db_trx.ExecuteReader(_sql_cmd)
          If Not _reader.Read() Then

            Return False
          End If

          If Not _reader.IsDBNull(1) Then
            StackerUnlinked = _reader.GetInt64(1)
          End If

          _reader.Close()
          _db_trx.Commit()

        End Using
      End Using

    Catch _ex As Exception

      Return False
    End Try

    Return True
  End Function

  Public Function ExistsDuplicateTerminalStacker(ByVal StackerId As Int64, ByVal TerminalId As Int64) As Boolean
    Dim _sb As StringBuilder
    Dim _count As Int32

    _sb = New StringBuilder()
    _count = -1

    _sb.AppendLine(" SELECT   COUNT(ST_STACKER_ID)          ")
    _sb.AppendLine("   FROM   STACKERS                      ")
    _sb.AppendLine("  WHERE   ST_INSERTED = @pTerminalId    ")
    _sb.AppendLine("    AND   ST_STACKER_ID <> @pStackerId  ")
    _sb.AppendLine("    AND   ST_STATUS = @pActive ")

    Try

      Using _sql_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString)
          _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = StackerId
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId
          _sql_cmd.Parameters.Add("@pActive", SqlDbType.Int).Value = TITO_STACKER_STATUS.ACTIVE

          _count = CType(_sql_trx.ExecuteScalar(_sql_cmd), Int32)
        End Using
      End Using

    Catch _ex As Exception
      _count = -1
    End Try

    Return (_count <> 0)
  End Function

#End Region

#Region " GUI methods "

  ' PURPOSE: Fill Combo text with Values in Table
  '
  ' PARAMS:
  '   - INPUT:
  '     - SelectedId: ID of objct (terminal or provider)
  '     - Combo: Combo to Fill
  '     - Table: table with selected data
  '
  '   - OUTPUT: None
  Public Sub FillComboFromTable(ByVal SelectedId As Int64, ByRef Combo As ComboBox, ByRef Table As DataTable)

    Dim _idx_selected As Int32
    Dim _idx_start As Int32
    Dim _arr() As Object

    _idx_selected = 0
    _idx_start = 0

    Combo.Items.Clear()

    _arr = New Object() {-1, ""}
    Dim dr As DataRow = Table.NewRow()
    dr.ItemArray() = _arr
    Table.Rows.InsertAt(dr, 0)

    Combo.DataSource = Table
    Combo.ValueMember = "ID"
    Combo.DisplayMember = "NAME"
    Combo.SelectedValue = SelectedId

  End Sub

#End Region

End Module
