'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_account_for_report.vb
' DESCRIPTION:   Report module for managing the reports
' AUTHOR:        Daniel Dom�nguez Mart�n
' CREATION DATE: 29-MAY-2012 
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 29-MAY-2012  DDM    Initial version.
' 13-FEB-2013  ANG    Expand to support AC_HOLDER_WEDDING_DATE
' 28-MAR-2013  LEM    Expand to support AC_HOLDER_TWITTER_ACCOUNT
' 12-JUN-2013  ACM    Expand to support - AC_HOLDER: OCCUPATION, ID, ID_TYPE, SCAN   - BENEFICIARY: NAME,  ID_TYPE, SCAN.
' 04-JUL-2013  QMP    Expand to support Money Laundering columns
' 14-AUG-2013  LEM    Expand to support AC_HOLDER_ID1 and AC_HOLDER_ID2 columns 
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 29-APR-2014  LEM    Fixed Bug WIG-874: DB error on searching
' 09-JUL-2014  AMF    Added funcionality for account visible fields
' 12-AGO-2015  SGB    Backlog Item 3353: Add address country in account
' 21-AGO-2015  FJC    Product BackLog Item: 3702
' 10-DEC-2015  GMV    Fixed Bug WIG-7258: Holder Country column Showing even if not requested
' 21-JUN-2016  DLL    Fixed Bug 14748:Reporte Resumen de cuentas - Pa�s del Titular no informa correctamente
' 13-FEB-2018  EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
'--------------------------------------------------------------------
Imports WSI.Common
Imports GUI_Controls
Imports GUI_CommonOperations

Module mdl_account_for_report

#Region " MEMBERS "

  Private m_account_visible_field_document As Boolean
  Private m_account_visible_field_doc_scan As Boolean
  Private m_account_visible_field_birth_date As Boolean
  Private m_account_visible_field_occupation As Boolean
  Private m_account_visible_field_gender As Boolean
  Private m_account_visible_field_nationality As Boolean
  Private m_account_visible_field_birth_country As Boolean
  Private m_account_visible_field_wedding_date As Boolean
  Private m_account_visible_field_phone_1 As Boolean
  Private m_account_visible_field_phone_2 As Boolean
  Private m_account_visible_field_email_1 As Boolean
  Private m_account_visible_field_address As Boolean
  Private m_account_visible_field_ext_num As Boolean
  Private m_account_visible_field_zip As Boolean
  Private m_account_visible_field_address_02 As Boolean
  Private m_account_visible_field_address_03 As Boolean
  Private m_account_visible_field_city As Boolean
  Private m_account_visible_field_fed_entity As Boolean
  Private m_account_visible_field_country As Boolean
  Private m_account_visible_field_RFC As Boolean
  Private m_account_visible_field_CURP As Boolean
  Private m_ben_visible As Boolean

#End Region ' MEMBERS

#Region " CONSTANTS "

  ' SQL COLUMNS
  ' Holder data
  Dim SQL_COLUMN_HOLDER_GENDER As Integer
  Dim SQL_COLUMN_HOLDER_BIRTHDATE As Integer
  Dim SQL_COLUMN_HOLDER_WEDDING_DATE As Integer
  Dim SQL_COLUMN_HOLDER_STREET As Integer
  Dim SQL_COLUMN_HOLDER_ZIP As Integer
  Dim SQL_COLUMN_HOLDER_COLONY As Integer
  Dim SQL_COLUMN_HOLDER_DELEGATION As Integer
  Dim SQL_COLUMN_HOLDER_CITY As Integer
  Dim SQL_COLUMN_HOLDER_COUNTRY As Integer
  Dim SQL_COLUMN_HOLDER_EMAIL_1 As Integer
  Dim SQL_COLUMN_HOLDER_EMAIL_2 As Integer
  Dim SQL_COLUMN_HOLDER_TWITTER As Integer
  Dim SQL_COLUMN_HOLDER_PHONE_1 As Integer
  Dim SQL_COLUMN_HOLDER_PHONE_2 As Integer
  Dim SQL_COLUMN_HOLDER_OCCUPATION_ID As Integer
  Dim SQL_COLUMN_HOLDER_ID_TYPE As Integer
  Dim SQL_COLUMN_HOLDER_ID As Integer
  Dim SQL_COLUMN_HOLDER_ID3_TYPE As Integer
  Dim SQL_COLUMN_HOLDER_NATIONALITY As Integer
  Dim SQL_COLUMN_HOLDER_BIRTH_COUNTRY As Integer
  Dim SQL_COLUMN_HOLDER_FED_ENTITY As Integer
  Dim SQL_COLUMN_HOLDER_EXT_NUM As Integer
  Dim SQL_COLUMN_HOLDER_RFC As Integer
  Dim SQL_COLUMN_HOLDER_CURP As Integer
  Dim SQL_COLUMN_HOLDER_IS_VIP As Integer
  'Remember to update SQL_NUM_COLUMNS_HOLDER_DATA

  ' Beneficiary data
  Dim SQL_COLUMN_BEN_NAME As Integer
  Dim SQL_COLUMN_BEN_BIRTHDATE As Integer
  Dim SQL_COLUMN_BEN_GENDER As Integer
  Dim SQL_COLUMN_BEN_ID_1 As Integer
  Dim SQL_COLUMN_BEN_ID_2 As Integer
  Dim SQL_COLUMN_BEN_OCCUPATION_ID As Integer
  Dim SQL_COLUMN_BEN_ID3_TYPE As Integer
  Dim SQL_COLUMN_HOLDER_HAS_BEN As Integer

  ' Account user type
  Dim SQL_COLUMN_ACCOUNT_USER_TYPE As Integer

  ' Totals--Important to update if SQL columns quantity changed--
  Public Const SQL_NUM_COLUMNS_HOLDER_DATA As Integer = 34

  ' GRID COLUMNS
  ' Holder data
  Dim GRID_COLUMN_HOLDER_GENDER As Integer
  Dim GRID_COLUMN_HOLDER_AGE As Integer
  Dim GRID_COLUMN_HOLDER_BIRTHDATE As Integer
  Dim GRID_COLUMN_HOLDER_WEDDING_DATE As Integer
  Dim GRID_COLUMN_HOLDER_STREET As Integer
  Dim GRID_COLUMN_HOLDER_EXT_NUM As Integer
  Dim GRID_COLUMN_HOLDER_ZIP As Integer
  Dim GRID_COLUMN_HOLDER_COLONY As Integer
  Dim GRID_COLUMN_HOLDER_DELEGATION As Integer
  Dim GRID_COLUMN_HOLDER_CITY As Integer
  Dim GRID_COLUMN_HOLDER_FED_ENTITY As Integer
  Dim GRID_COLUMN_HOLDER_COUNTRY As Integer
  Dim GRID_COLUMN_HOLDER_EMAIL As Integer
  Dim GRID_COLUMN_HOLDER_PHONE_1 As Integer
  Dim GRID_COLUMN_HOLDER_PHONE_2 As Integer
  Dim GRID_COLUMN_HOLDER_NATIONALITY As Integer
  Dim GRID_COLUMN_HOLDER_ISO2_NATIONALITY As Integer
  Dim GRID_COLUMN_HOLDER_BIRTH_COUNTRY As Integer
  Dim GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY As Integer
  Dim GRID_COLUMN_HOLDER_OCCUPATION_CODE As Integer
  Dim GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION As Integer
  Dim GRID_COLUMN_HOLDER_DOC As Integer
  Dim GRID_COLUMN_HOLDER_SCANNED_DOC As Integer
  Dim GRID_COLUMN_HOLDER_RFC As Integer
  Dim GRID_COLUMN_HOLDER_CURP As Integer
  Dim GRID_COLUMN_HOLDER_IS_VIP As Integer
  'Remember to update GRID_NUM_COLUMNS_HOLDER_DATA

  'Beneficiary data
  Dim GRID_COLUMN_BEN_NAME As Integer
  Dim GRID_COLUMN_BEN_GENDER As Integer
  Dim GRID_COLUMN_BEN_AGE As Integer
  Dim GRID_COLUMN_BEN_BIRTHDATE As Integer
  Dim GRID_COLUMN_BEN_OCCUPATION_CODE As Integer
  Dim GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION As Integer
  Dim GRID_COLUMN_BEN_DOC_RFC As Integer
  Dim GRID_COLUMN_BEN_DOC_CURP As Integer
  Dim GRID_COLUMN_BEN_SCANNED_DOC As Integer

  ' Totals--Important to update if GRID columns quantity changed--
  Public Const GRID_NUM_COLUMNS_HOLDER_DATA As Integer = 43

  'secondary address
  Dim GRID_COLUMN_HOLDER_STREET_2 As Integer
  Dim GRID_COLUMN_HOLDER_EXT_NUM_2 As Integer
  Dim GRID_COLUMN_HOLDER_ZIP_2 As Integer
  Dim GRID_COLUMN_HOLDER_COLONY_2 As Integer
  Dim GRID_COLUMN_HOLDER_DELEGATION_2 As Integer
  Dim GRID_COLUMN_HOLDER_CITY_2 As Integer
  Dim GRID_COLUMN_HOLDER_FED_ENTITY_2 As Integer
  Dim GRID_COLUMN_HOLDER_COUNTRY_2 As Integer

  ' GRID COLUMN WIDTHS
  ' Holder
  Private Const GRID_WIDTH_HOLDER_GENDER As Integer = 850
  Private Const GRID_WIDTH_HOLDER_AGE As Integer = 550
  Private Const GRID_WIDTH_HOLDER_BIRTH_DATE As Integer = 1700
  Private Const GRID_WIDTH_HOLDER_WEDDING_DATE As Integer = 1700
  Private Const GRID_WIDTH_HOLDER_STREET As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_EXT_NUM As Integer = 1650
  Private Const GRID_WIDTH_HOLDER_ZIP As Integer = 900
  Private Const GRID_WIDTH_HOLDER_COLONY As Integer = 1500
  Private Const GRID_WIDTH_HOLDER_DELEGATION As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_CITY As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_FED_ENTITY As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_COUNTRY As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_EMAIL As Integer = 2600
  Private Const GRID_WIDTH_HOLDER_PHONE_1 As Integer = 1450
  Private Const GRID_WIDTH_HOLDER_PHONE_2 As Integer = 1450
  Private Const GRID_WIDTH_HOLDER_NATIONALITY As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_ISO2_NATIONALITY As Integer = 500
  Private Const GRID_WIDTH_HOLDER_BIRTH_COUNTRY As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_ISO2_BIRTH_COUNTRY As Integer = 500
  Private Const GRID_WIDTH_HOLDER_OCCUPATION_ID As Integer = 900
  Private Const GRID_WIDTH_HOLDER_OCCUPATION_DESCRIPTION As Integer = 3000
  Private Const GRID_WIDTH_HOLDER_DOC As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_SCANNED_DOC As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_IS_VIP As Integer = 800
  Private Const GRID_WIDTH_HOLDER_RFC As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_CURP As Integer = 2000

  ' Beneficiary
  Private Const GRID_WIDTH_BEN_NAME As Integer = 3100
  Private Const GRID_WIDTH_BEN_GENDER As Integer = 850
  Private Const GRID_WIDTH_BEN_AGE As Integer = 550
  Private Const GRID_WIDTH_BEN_BIRTHDATE As Integer = 1700
  Private Const GRID_WIDTH_BEN_OCCUPATION_ID As Integer = 900
  Private Const GRID_WIDTH_BEN_OCCUPATION_DESCRIPTION As Integer = 3000
  Private Const GRID_WIDTH_BEN_DOC_RFC As Integer = 1500
  Private Const GRID_WIDTH_BEN_DOC_CURP As Integer = 1500
  Private Const GRID_WIDTH_BEN_SCANNED_DOC As Integer = 2000

  Private Const OCCUPATION_CODE_DEFAULT As String = "9999999"
#End Region ' CONSTANTS

#Region " PUBLIC FUNCTIONS "

  ' PURPOSE : Get level names
  '
  '  PARAMS :
  '     - INPUT:
  '           
  '     - OUTPUT:
  '            - None
  '
  '  RETURNS :
  '       - Dictionary with code and name of level
  Public Function GetLevelNames() As Dictionary(Of Integer, String)

    Dim _dt_level_names As DataTable
    Dim _dc_level_names As Dictionary(Of Integer, String)

    _dt_level_names = New DataTable()
    _dc_level_names = New Dictionary(Of Integer, String)

    Using _db_trx As New DB_TRX()
      _dt_level_names = Reports.GetLevelNames(_db_trx.SqlTransaction)
    End Using

    'add anonimous.
    _dc_level_names.Add(0, GLB_NLS_GUI_STATISTICS.GetString(226))
    For Each _drow_level As DataRow In _dt_level_names.Rows
      _dc_level_names.Add(_drow_level(0), _drow_level(1))
    Next

    Return _dc_level_names

  End Function ' GetLevelNames

  ' PURPOSE: Define Grid Columns holder data
  '
  '  PARAMS:
  '     - INPUT:
  '           - LastColumn : The last column before begins with holder data
  '           - Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub StyleSheetHolderData(ByRef Grid As uc_grid, ByVal InitColumn As Integer, Optional ByVal ShowCodes As Boolean = False, Optional ByVal ShowHolderData As Boolean = False, Optional ByVal ShowSecondaryAddress As Boolean = False)

    Call ReadGeneralParams()

    GRID_COLUMN_HOLDER_GENDER = InitColumn
    GRID_COLUMN_HOLDER_AGE = InitColumn + 1
    GRID_COLUMN_HOLDER_BIRTHDATE = InitColumn + 2
    GRID_COLUMN_HOLDER_WEDDING_DATE = InitColumn + 3
    GRID_COLUMN_HOLDER_STREET = InitColumn + 4
    GRID_COLUMN_HOLDER_EXT_NUM = InitColumn + 5
    GRID_COLUMN_HOLDER_ZIP = InitColumn + 6
    GRID_COLUMN_HOLDER_COLONY = InitColumn + 7
    GRID_COLUMN_HOLDER_DELEGATION = InitColumn + 8
    GRID_COLUMN_HOLDER_CITY = InitColumn + 9
    GRID_COLUMN_HOLDER_FED_ENTITY = InitColumn + 10
    GRID_COLUMN_HOLDER_COUNTRY = InitColumn + 11
    GRID_COLUMN_HOLDER_EMAIL = InitColumn + 12
    GRID_COLUMN_HOLDER_PHONE_1 = InitColumn + 13
    GRID_COLUMN_HOLDER_PHONE_2 = InitColumn + 14
    GRID_COLUMN_HOLDER_ISO2_NATIONALITY = InitColumn + 15
    GRID_COLUMN_HOLDER_NATIONALITY = InitColumn + 16



    GRID_COLUMN_HOLDER_STREET_2 = InitColumn + 17
    GRID_COLUMN_HOLDER_EXT_NUM_2 = InitColumn + 18
    GRID_COLUMN_HOLDER_ZIP_2 = InitColumn + 19
    GRID_COLUMN_HOLDER_COLONY_2 = InitColumn + 20
    GRID_COLUMN_HOLDER_DELEGATION_2 = InitColumn + 21
    GRID_COLUMN_HOLDER_CITY_2 = InitColumn + 22
    GRID_COLUMN_HOLDER_FED_ENTITY_2 = InitColumn + 23
    GRID_COLUMN_HOLDER_COUNTRY_2 = InitColumn + 24


    GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY = InitColumn + 25
    GRID_COLUMN_HOLDER_BIRTH_COUNTRY = InitColumn + 26
    GRID_COLUMN_HOLDER_OCCUPATION_CODE = InitColumn + 27
    GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION = InitColumn + 28
    GRID_COLUMN_HOLDER_DOC = InitColumn + 29
    GRID_COLUMN_HOLDER_RFC = InitColumn + 30
    GRID_COLUMN_HOLDER_CURP = InitColumn + 31
    GRID_COLUMN_HOLDER_SCANNED_DOC = InitColumn + 32
    GRID_COLUMN_HOLDER_IS_VIP = InitColumn + 33

    GRID_COLUMN_BEN_NAME = InitColumn + 34
    GRID_COLUMN_BEN_GENDER = InitColumn + 35
    GRID_COLUMN_BEN_AGE = InitColumn + 36
    GRID_COLUMN_BEN_BIRTHDATE = InitColumn + 37
    GRID_COLUMN_BEN_OCCUPATION_CODE = InitColumn + 38
    GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION = InitColumn + 39
    GRID_COLUMN_BEN_DOC_RFC = InitColumn + 40
    GRID_COLUMN_BEN_DOC_CURP = InitColumn + 41
    GRID_COLUMN_BEN_SCANNED_DOC = InitColumn + 42

    With Grid
      ' Holder - Gender
      .Column(GRID_COLUMN_HOLDER_GENDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_GENDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_HOLDER_GENDER).Width = IIf(m_account_visible_field_gender AndAlso ShowHolderData, GRID_WIDTH_HOLDER_GENDER, 0)
      .Column(GRID_COLUMN_HOLDER_GENDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Age
      .Column(GRID_COLUMN_HOLDER_AGE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_AGE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(415)
      .Column(GRID_COLUMN_HOLDER_AGE).Width = IIf(m_account_visible_field_birth_date AndAlso ShowHolderData, GRID_WIDTH_HOLDER_AGE, 0)
      .Column(GRID_COLUMN_HOLDER_AGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder - Birth Date
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(428)
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Width = IIf(m_account_visible_field_birth_date AndAlso ShowHolderData, GRID_WIDTH_HOLDER_BIRTH_DATE, 0)
      .Column(GRID_COLUMN_HOLDER_BIRTHDATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Wedding Date
      .Column(GRID_COLUMN_HOLDER_WEDDING_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_WEDDING_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1652) ' Wedding Date
      .Column(GRID_COLUMN_HOLDER_WEDDING_DATE).Width = IIf(m_account_visible_field_wedding_date AndAlso ShowHolderData, GRID_WIDTH_HOLDER_WEDDING_DATE, 0)
      .Column(GRID_COLUMN_HOLDER_WEDDING_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Street
      .Column(GRID_COLUMN_HOLDER_STREET).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_STREET).Header(1).Text = AccountFields.GetAddressName() 'GLB_NLS_GUI_INVOICING.GetString(407)
      .Column(GRID_COLUMN_HOLDER_STREET).Width = IIf(m_account_visible_field_address AndAlso ShowHolderData, GRID_WIDTH_HOLDER_STREET, 0)
      .Column(GRID_COLUMN_HOLDER_STREET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Ext Num
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Header(1).Text = AccountFields.GetNumExtName() 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2091)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Width = IIf(m_account_visible_field_ext_num AndAlso ShowHolderData, GRID_WIDTH_HOLDER_EXT_NUM, 0)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder - Zip
      .Column(GRID_COLUMN_HOLDER_ZIP).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_ZIP).Header(1).Text = AccountFields.GetZipName() 'GLB_NLS_GUI_INVOICING.GetString(411)
      .Column(GRID_COLUMN_HOLDER_ZIP).Width = IIf(m_account_visible_field_zip AndAlso ShowHolderData, GRID_WIDTH_HOLDER_ZIP, 0)
      .Column(GRID_COLUMN_HOLDER_ZIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Colony
      .Column(GRID_COLUMN_HOLDER_COLONY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_COLONY).Header(1).Text = AccountFields.GetColonyName() 'GLB_NLS_GUI_INVOICING.GetString(408)
      .Column(GRID_COLUMN_HOLDER_COLONY).Width = IIf(m_account_visible_field_address_02 AndAlso ShowHolderData, GRID_WIDTH_HOLDER_COLONY, 0)
      .Column(GRID_COLUMN_HOLDER_COLONY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Delegation
      .Column(GRID_COLUMN_HOLDER_DELEGATION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_DELEGATION).Header(1).Text = AccountFields.GetDelegationName() 'GLB_NLS_GUI_INVOICING.GetString(409)
      .Column(GRID_COLUMN_HOLDER_DELEGATION).Width = IIf(m_account_visible_field_address_03 AndAlso ShowHolderData, GRID_WIDTH_HOLDER_DELEGATION, 0)
      .Column(GRID_COLUMN_HOLDER_DELEGATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - City
      .Column(GRID_COLUMN_HOLDER_CITY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_CITY).Header(1).Text = AccountFields.GetTownshipName() 'GLB_NLS_GUI_INVOICING.GetString(410)
      .Column(GRID_COLUMN_HOLDER_CITY).Width = IIf(m_account_visible_field_city AndAlso ShowHolderData, GRID_WIDTH_HOLDER_CITY, 0)
      .Column(GRID_COLUMN_HOLDER_CITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Zip
      '.Column(GRID_COLUMN_HOLDER_ZIP).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      '.Column(GRID_COLUMN_HOLDER_ZIP).Header(1).Text = AccountFields.GetZipName() 'GLB_NLS_GUI_INVOICING.GetString(411)
      '.Column(GRID_COLUMN_HOLDER_ZIP).Width = IIf(m_account_visible_field_zip AndAlso ShowHolderData, GRID_WIDTH_HOLDER_ZIP, 0)
      '.Column(GRID_COLUMN_HOLDER_ZIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Federal Entity
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Header(1).Text = AccountFields.GetStateName()
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Width = IIf(m_account_visible_field_fed_entity AndAlso ShowHolderData, GRID_WIDTH_HOLDER_FED_ENTITY, 0)
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Country
      .Column(GRID_COLUMN_HOLDER_COUNTRY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_COUNTRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090)
      .Column(GRID_COLUMN_HOLDER_COUNTRY).Width = IIf(m_account_visible_field_country AndAlso ShowHolderData, GRID_WIDTH_HOLDER_COUNTRY, 0)
      .Column(GRID_COLUMN_HOLDER_COUNTRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - E-mail
      .Column(GRID_COLUMN_HOLDER_EMAIL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_EMAIL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(406)
      .Column(GRID_COLUMN_HOLDER_EMAIL).Width = IIf(m_account_visible_field_email_1 AndAlso ShowHolderData, GRID_WIDTH_HOLDER_EMAIL, 0)
      .Column(GRID_COLUMN_HOLDER_EMAIL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Phone 1
      .Column(GRID_COLUMN_HOLDER_PHONE_1).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_PHONE_1).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(556)
      .Column(GRID_COLUMN_HOLDER_PHONE_1).Width = IIf(m_account_visible_field_phone_1 AndAlso ShowHolderData, GRID_WIDTH_HOLDER_PHONE_1, 0)
      .Column(GRID_COLUMN_HOLDER_PHONE_1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Phone 2
      .Column(GRID_COLUMN_HOLDER_PHONE_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_PHONE_2).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(556)
      .Column(GRID_COLUMN_HOLDER_PHONE_2).Width = IIf(m_account_visible_field_phone_2 AndAlso ShowHolderData, GRID_WIDTH_HOLDER_PHONE_2, 0)
      .Column(GRID_COLUMN_HOLDER_PHONE_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Nationality
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2089)
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Width = IIf(m_account_visible_field_nationality AndAlso ShowHolderData, GRID_WIDTH_HOLDER_NATIONALITY, 0)
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      'SECONDARY ADDRESS -------------------------------------------------------------------------------------------------

      ' Holder - Street 2
      .Column(GRID_COLUMN_HOLDER_STREET_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_STREET_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetAddressName(), "2")
      .Column(GRID_COLUMN_HOLDER_STREET_2).Width = IIf(m_account_visible_field_address AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_STREET, 0)
      .Column(GRID_COLUMN_HOLDER_STREET_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Ext Num 2
      .Column(GRID_COLUMN_HOLDER_EXT_NUM_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetNumExtName(), "2")
      .Column(GRID_COLUMN_HOLDER_EXT_NUM_2).Width = IIf(m_account_visible_field_ext_num AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_EXT_NUM, 0)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder - Zip 2
      .Column(GRID_COLUMN_HOLDER_ZIP_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_ZIP_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetZipName(), "2")
      .Column(GRID_COLUMN_HOLDER_ZIP_2).Width = IIf(m_account_visible_field_zip AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_ZIP + 25, 0)
      .Column(GRID_COLUMN_HOLDER_ZIP_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Colony 2
      .Column(GRID_COLUMN_HOLDER_COLONY_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_COLONY_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetColonyName(), "2")
      .Column(GRID_COLUMN_HOLDER_COLONY_2).Width = IIf(m_account_visible_field_address_02 AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_COLONY, 0)
      .Column(GRID_COLUMN_HOLDER_COLONY_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Delegation 2
      .Column(GRID_COLUMN_HOLDER_DELEGATION_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_DELEGATION_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetDelegationName(), "2")
      .Column(GRID_COLUMN_HOLDER_DELEGATION_2).Width = IIf(m_account_visible_field_address_03 AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_DELEGATION, 0)
      .Column(GRID_COLUMN_HOLDER_DELEGATION_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - City 2
      .Column(GRID_COLUMN_HOLDER_CITY_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_CITY_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetTownshipName(), "2")
      .Column(GRID_COLUMN_HOLDER_CITY_2).Width = IIf(m_account_visible_field_city AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_CITY, 0)
      .Column(GRID_COLUMN_HOLDER_CITY_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Federal Entity 2
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY_2).Header(1).Text = String.Format("{0} {1}", AccountFields.GetStateName(), "2")
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY_2).Width = IIf(m_account_visible_field_fed_entity AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_FED_ENTITY, 0)
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Country 2
      .Column(GRID_COLUMN_HOLDER_COUNTRY_2).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_COUNTRY_2).Header(1).Text = String.Format("{0} {1}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090), "2")
      .Column(GRID_COLUMN_HOLDER_COUNTRY_2).Width = IIf(m_account_visible_field_country AndAlso ShowHolderData AndAlso ShowSecondaryAddress, GRID_WIDTH_HOLDER_COUNTRY, 0)
      .Column(GRID_COLUMN_HOLDER_COUNTRY_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'END SECONDARY ADDRESS -------------------------------------------------------------------------------------------------

      ' Holder - Iso2 Nationality
      .Column(GRID_COLUMN_HOLDER_ISO2_NATIONALITY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_ISO2_NATIONALITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2089)
      .Column(GRID_COLUMN_HOLDER_ISO2_NATIONALITY).Width = IIf(ShowHolderData AndAlso ShowCodes And m_account_visible_field_nationality, GRID_WIDTH_HOLDER_ISO2_NATIONALITY, 0)
      .Column(GRID_COLUMN_HOLDER_ISO2_NATIONALITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Birth Country
      .Column(GRID_COLUMN_HOLDER_BIRTH_COUNTRY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_BIRTH_COUNTRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2088)
      .Column(GRID_COLUMN_HOLDER_BIRTH_COUNTRY).Width = IIf(m_account_visible_field_birth_country AndAlso ShowHolderData, GRID_WIDTH_HOLDER_BIRTH_COUNTRY, 0)
      .Column(GRID_COLUMN_HOLDER_BIRTH_COUNTRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Iso2 Birth Country
      .Column(GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2088)
      .Column(GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY).Width = IIf(ShowHolderData AndAlso ShowCodes And m_account_visible_field_birth_country, GRID_WIDTH_HOLDER_ISO2_BIRTH_COUNTRY, 0)
      .Column(GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder - Occupation Id
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_CODE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_CODE).Width = IIf(ShowHolderData AndAlso ShowCodes And m_account_visible_field_occupation, GRID_WIDTH_HOLDER_OCCUPATION_ID, 0)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Occupation Description
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION).Width = IIf(ShowHolderData AndAlso m_account_visible_field_occupation, GRID_WIDTH_HOLDER_OCCUPATION_DESCRIPTION, 0)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Doc
      .Column(GRID_COLUMN_HOLDER_DOC).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_DOC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1722)
      .Column(GRID_COLUMN_HOLDER_DOC).Width = IIf(m_account_visible_field_document AndAlso ShowHolderData, GRID_WIDTH_HOLDER_DOC, 0)
      .Column(GRID_COLUMN_HOLDER_DOC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - RFC
      .Column(GRID_COLUMN_HOLDER_RFC).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_RFC).Header(1).Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
      .Column(GRID_COLUMN_HOLDER_RFC).Width = IIf(m_account_visible_field_RFC AndAlso ShowHolderData, GRID_WIDTH_HOLDER_RFC, 0)
      .Column(GRID_COLUMN_HOLDER_RFC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - CURP
      .Column(GRID_COLUMN_HOLDER_CURP).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_CURP).Header(1).Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)
      .Column(GRID_COLUMN_HOLDER_CURP).Width = IIf(m_account_visible_field_CURP AndAlso ShowHolderData, GRID_WIDTH_HOLDER_CURP, 0)
      .Column(GRID_COLUMN_HOLDER_CURP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Scanned Doc
      .Column(GRID_COLUMN_HOLDER_SCANNED_DOC).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_SCANNED_DOC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2065)
      .Column(GRID_COLUMN_HOLDER_SCANNED_DOC).Width = IIf(m_account_visible_field_doc_scan AndAlso ShowHolderData, GRID_WIDTH_HOLDER_SCANNED_DOC, 0)
      .Column(GRID_COLUMN_HOLDER_SCANNED_DOC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder - Vip
      .Column(GRID_COLUMN_HOLDER_IS_VIP).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(420)
      .Column(GRID_COLUMN_HOLDER_IS_VIP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4805)
      .Column(GRID_COLUMN_HOLDER_IS_VIP).Width = IIf(ShowHolderData, GRID_WIDTH_HOLDER_IS_VIP, 0)
      .Column(GRID_COLUMN_HOLDER_IS_VIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Beneficiary - Name
      .Column(GRID_COLUMN_BEN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(758)
      .Column(GRID_COLUMN_BEN_NAME).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_NAME, 0)
      .Column(GRID_COLUMN_BEN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Beneficiary - Gender
      .Column(GRID_COLUMN_BEN_GENDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_GENDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_BEN_GENDER).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_GENDER, 0)
      .Column(GRID_COLUMN_BEN_GENDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Beneficiary - Age
      .Column(GRID_COLUMN_BEN_AGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_AGE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(415)
      .Column(GRID_COLUMN_BEN_AGE).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_AGE, 0)
      .Column(GRID_COLUMN_BEN_AGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Beneficiary - Birthdate
      .Column(GRID_COLUMN_BEN_BIRTHDATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_BIRTHDATE).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_BIRTHDATE, 0)
      .Column(GRID_COLUMN_BEN_BIRTHDATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(428)
      .Column(GRID_COLUMN_BEN_BIRTHDATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Beneficiary - Occupation Id
      .Column(GRID_COLUMN_BEN_OCCUPATION_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_OCCUPATION_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
      .Column(GRID_COLUMN_BEN_OCCUPATION_CODE).Width = IIf(ShowCodes And m_ben_visible, GRID_WIDTH_BEN_OCCUPATION_ID, 0)
      .Column(GRID_COLUMN_BEN_OCCUPATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Beneficiary - Occupation Description
      .Column(GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
      .Column(GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_OCCUPATION_DESCRIPTION, 0)
      .Column(GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Beneficiary - Doc RFC
      .Column(GRID_COLUMN_BEN_DOC_RFC).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_DOC_RFC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(764)
      .Column(GRID_COLUMN_BEN_DOC_RFC).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_DOC_RFC, 0)
      .Column(GRID_COLUMN_BEN_DOC_RFC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Beneficiary - Doc CURP
      .Column(GRID_COLUMN_BEN_DOC_CURP).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_DOC_CURP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(765)
      .Column(GRID_COLUMN_BEN_DOC_CURP).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_DOC_CURP, 0)
      .Column(GRID_COLUMN_BEN_DOC_CURP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Beneficiary - Scanned Doc
      .Column(GRID_COLUMN_BEN_SCANNED_DOC).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2060)
      .Column(GRID_COLUMN_BEN_SCANNED_DOC).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2065)
      .Column(GRID_COLUMN_BEN_SCANNED_DOC).Width = IIf(m_ben_visible, GRID_WIDTH_BEN_SCANNED_DOC, 0)
      .Column(GRID_COLUMN_BEN_SCANNED_DOC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' StyleSheetHolderData

  ' PURPOSE : Sets the values of a row the older data
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow 
  '           - RowIndex
  '           - ColumnOfGrid 
  '           - ColumnOfSQL 
  '
  '     - OUTPUT :
  '           - Grid
  '
  ' RETURNS : 
  Public Function SetupRowHolderData(ByRef Grid As uc_grid, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, _
                                      ByVal RowIndex As Integer, ByVal GridColumn As Integer, ByVal SqlColumn As Integer, Optional ByVal Address2 As WSI.Common.Entrances.Entities.CustomerAddress = Nothing) As Boolean

    Dim _gender As String
    Dim _ben_gender As String
    Dim _fed_entity As String
    Dim _country As String
    'Dim _country_id As Integer
    Dim _email As String
    Dim _email_aux As String
    Dim _twitter As String
    Dim _nationality As String
    Dim _birth_country As String
    Dim _occupation As String
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    SQL_COLUMN_HOLDER_GENDER = SqlColumn
    SQL_COLUMN_HOLDER_BIRTHDATE = SqlColumn + 1
    SQL_COLUMN_HOLDER_WEDDING_DATE = SqlColumn + 2
    SQL_COLUMN_HOLDER_STREET = SqlColumn + 3
    SQL_COLUMN_HOLDER_COLONY = SqlColumn + 4
    SQL_COLUMN_HOLDER_DELEGATION = SqlColumn + 5
    SQL_COLUMN_HOLDER_ZIP = SqlColumn + 6
    SQL_COLUMN_HOLDER_CITY = SqlColumn + 7
    SQL_COLUMN_HOLDER_COUNTRY = SqlColumn + 8
    SQL_COLUMN_HOLDER_EMAIL_1 = SqlColumn + 9
    SQL_COLUMN_HOLDER_EMAIL_2 = SqlColumn + 10
    SQL_COLUMN_HOLDER_TWITTER = SqlColumn + 11
    SQL_COLUMN_HOLDER_PHONE_1 = SqlColumn + 12
    SQL_COLUMN_HOLDER_PHONE_2 = SqlColumn + 13
    SQL_COLUMN_HOLDER_OCCUPATION_ID = SqlColumn + 14
    SQL_COLUMN_HOLDER_ID_TYPE = SqlColumn + 15
    SQL_COLUMN_HOLDER_ID = SqlColumn + 16
    SQL_COLUMN_HOLDER_ID3_TYPE = SqlColumn + 17
    SQL_COLUMN_HOLDER_NATIONALITY = SqlColumn + 18
    SQL_COLUMN_HOLDER_BIRTH_COUNTRY = SqlColumn + 19
    SQL_COLUMN_HOLDER_FED_ENTITY = SqlColumn + 20
    SQL_COLUMN_HOLDER_EXT_NUM = SqlColumn + 21
    SQL_COLUMN_HOLDER_RFC = SqlColumn + 22
    SQL_COLUMN_HOLDER_CURP = SqlColumn + 23
    SQL_COLUMN_BEN_NAME = SqlColumn + 24
    SQL_COLUMN_BEN_BIRTHDATE = SqlColumn + 25
    SQL_COLUMN_BEN_GENDER = SqlColumn + 26
    SQL_COLUMN_BEN_ID_1 = SqlColumn + 27
    SQL_COLUMN_BEN_ID_2 = SqlColumn + 28
    SQL_COLUMN_BEN_OCCUPATION_ID = SqlColumn + 29
    SQL_COLUMN_BEN_ID3_TYPE = SqlColumn + 30
    SQL_COLUMN_HOLDER_HAS_BEN = SqlColumn + 31
    SQL_COLUMN_ACCOUNT_USER_TYPE = SqlColumn + 32
    SQL_COLUMN_HOLDER_IS_VIP = SqlColumn + 33

    GRID_COLUMN_HOLDER_GENDER = GridColumn
    GRID_COLUMN_HOLDER_AGE = GridColumn + 1
    GRID_COLUMN_HOLDER_BIRTHDATE = GridColumn + 2
    GRID_COLUMN_HOLDER_WEDDING_DATE = GridColumn + 3
    GRID_COLUMN_HOLDER_STREET = GridColumn + 4
    GRID_COLUMN_HOLDER_EXT_NUM = GridColumn + 5
    GRID_COLUMN_HOLDER_ZIP = GridColumn + 6
    GRID_COLUMN_HOLDER_COLONY = GridColumn + 7
    GRID_COLUMN_HOLDER_DELEGATION = GridColumn + 8
    GRID_COLUMN_HOLDER_CITY = GridColumn + 9
    GRID_COLUMN_HOLDER_FED_ENTITY = GridColumn + 10
    GRID_COLUMN_HOLDER_COUNTRY = GridColumn + 11
    GRID_COLUMN_HOLDER_EMAIL = GridColumn + 12
    GRID_COLUMN_HOLDER_PHONE_1 = GridColumn + 13
    GRID_COLUMN_HOLDER_PHONE_2 = GridColumn + 14
    GRID_COLUMN_HOLDER_ISO2_NATIONALITY = GridColumn + 15
    GRID_COLUMN_HOLDER_NATIONALITY = GridColumn + 16


    GRID_COLUMN_HOLDER_STREET_2 = GridColumn + 17
    GRID_COLUMN_HOLDER_EXT_NUM_2 = GridColumn + 18
    GRID_COLUMN_HOLDER_ZIP_2 = GridColumn + 19
    GRID_COLUMN_HOLDER_COLONY_2 = GridColumn + 20
    GRID_COLUMN_HOLDER_DELEGATION_2 = GridColumn + 21
    GRID_COLUMN_HOLDER_CITY_2 = GridColumn + 22
    GRID_COLUMN_HOLDER_FED_ENTITY_2 = GridColumn + 23
    GRID_COLUMN_HOLDER_COUNTRY_2 = GridColumn + 24


    GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY = GridColumn + 25
    GRID_COLUMN_HOLDER_BIRTH_COUNTRY = GridColumn + 26
    GRID_COLUMN_HOLDER_OCCUPATION_CODE = GridColumn + 27
    GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION = GridColumn + 28
    GRID_COLUMN_HOLDER_DOC = GridColumn + 29
    GRID_COLUMN_HOLDER_RFC = GridColumn + 30
    GRID_COLUMN_HOLDER_CURP = GridColumn + 31
    GRID_COLUMN_HOLDER_SCANNED_DOC = GridColumn + 32
    GRID_COLUMN_HOLDER_IS_VIP = GridColumn + 33

    GRID_COLUMN_BEN_NAME = GridColumn + 34
    GRID_COLUMN_BEN_GENDER = GridColumn + 35
    GRID_COLUMN_BEN_AGE = GridColumn + 36
    GRID_COLUMN_BEN_BIRTHDATE = GridColumn + 37
    GRID_COLUMN_BEN_OCCUPATION_CODE = GridColumn + 38
    GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION = GridColumn + 39
    GRID_COLUMN_BEN_DOC_RFC = GridColumn + 40
    GRID_COLUMN_BEN_DOC_CURP = GridColumn + 41
    GRID_COLUMN_BEN_SCANNED_DOC = GridColumn + 42

    _gender = String.Empty
    _ben_gender = String.Empty
    _email = String.Empty

    ' Only process personalized accounts
    If DbRow.IsNull(SQL_COLUMN_ACCOUNT_USER_TYPE) OrElse DbRow.Value(SQL_COLUMN_ACCOUNT_USER_TYPE) = ACCOUNT_USER_TYPE.ANONYMOUS Then
      Return True
    End If

    ' Holder - Gender
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_GENDER) Then
      _gender = CardData.GenderString(DbRow.Value(SQL_COLUMN_HOLDER_GENDER))
    End If

    Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_GENDER).Value = _gender

    ' Holder - Age & Birthdate
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_BIRTHDATE) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_AGE).Value = WSI.Common.Misc.CalculateAge(DbRow.Value(SQL_COLUMN_HOLDER_BIRTHDATE))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_BIRTHDATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_HOLDER_BIRTHDATE), _
                                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                       ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    ' Holder - Wedding Date
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_WEDDING_DATE) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_WEDDING_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_HOLDER_WEDDING_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    ' Holder - Street
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_STREET) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_STREET).Value = DbRow.Value(SQL_COLUMN_HOLDER_STREET)
    End If

    ' Holder - Ext Num
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_EXT_NUM) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_EXT_NUM).Value = DbRow.Value(SQL_COLUMN_HOLDER_EXT_NUM)
    End If

    ' Holder - Zip Code
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ZIP) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ZIP).Value = DbRow.Value(SQL_COLUMN_HOLDER_ZIP)
    End If

    ' Holder - Colony
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_COLONY) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_COLONY).Value = DbRow.Value(SQL_COLUMN_HOLDER_COLONY)
    End If

    ' Holder - Delegation
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_DELEGATION) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_DELEGATION).Value = DbRow.Value(SQL_COLUMN_HOLDER_DELEGATION)
    End If

    ' Holder - City
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_CITY) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_CITY).Value = DbRow.Value(SQL_COLUMN_HOLDER_CITY)
    End If

    ' Holder - Federal Entity
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_FED_ENTITY) Then
      _fed_entity = CardData.FedEntitiesString(DbRow.Value(SQL_COLUMN_HOLDER_FED_ENTITY))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_FED_ENTITY).Value = _fed_entity
    End If

    ' Holder - Country
    _country = ""

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_COUNTRY) Then
      Countries.GetCountryInfo(DbRow.Value(SQL_COLUMN_HOLDER_COUNTRY), _country, "", "")
    End If
    Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_COUNTRY).Value = _country

    ' Holder - E-mail
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_EMAIL_1) Then
      _email_aux = DbRow.Value(SQL_COLUMN_HOLDER_EMAIL_1)
      _email = _email_aux.Trim()
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_EMAIL_2) Then
      _email_aux = DbRow.Value(SQL_COLUMN_HOLDER_EMAIL_2)
      _email_aux = _email_aux.Trim()

      If Not String.IsNullOrEmpty(_email_aux) Then
        If Not String.IsNullOrEmpty(_email) Then
          _email = _email & "; "
        End If
        _email = _email & _email_aux
      End If

    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_TWITTER) Then
      _twitter = DbRow.Value(SQL_COLUMN_HOLDER_TWITTER)

      If Not String.IsNullOrEmpty(_twitter) Then
        _twitter = "twitter: " & _twitter.Trim()

        If Not String.IsNullOrEmpty(_email) Then
          _email = _email & ", "
        End If
        _email = _email & _twitter

      End If
    End If

    If Not String.IsNullOrEmpty(_email) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_EMAIL).Value = _email
    End If

    ' Holder - Phone number 1
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_PHONE_1) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_PHONE_1).Value = DbRow.Value(SQL_COLUMN_HOLDER_PHONE_1)
    End If

    ' Holder - Phone number 2
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_PHONE_2) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_PHONE_2).Value = DbRow.Value(SQL_COLUMN_HOLDER_PHONE_2)
    End If

    ' Holder - Nationality
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NATIONALITY) Then
      _nationality = CardData.NationalitiesString(DbRow.Value(SQL_COLUMN_HOLDER_NATIONALITY))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NATIONALITY).Value = _nationality
    End If

    ' Holder - Iso2 Nationality
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NATIONALITY) Then
      _nationality = CardData.Iso2String(DbRow.Value(SQL_COLUMN_HOLDER_NATIONALITY))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ISO2_NATIONALITY).Value = _nationality
    End If

    ' Holder - Birth Country
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_BIRTH_COUNTRY) Then
      _birth_country = CardData.CountriesString(DbRow.Value(SQL_COLUMN_HOLDER_BIRTH_COUNTRY))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_BIRTH_COUNTRY).Value = _birth_country
    End If


    '' Second Address
    ' Holder - Street
    If Not Address2 Is Nothing Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_STREET_2).Value = Address2.Street
    End If

    ' Holder - Ext Num
    If Not Address2 Is Nothing Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_EXT_NUM_2).Value = Address2.Number
    End If

    ' Holder - Zip Code
    If Not Address2 Is Nothing Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ZIP_2).Value = Address2.Cp
    End If

    ' Holder - Colony
    If Not Address2 Is Nothing Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_COLONY_2).Value = Address2.Colony
    End If

    ' Holder - Delegation
    If Not Address2 Is Nothing Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_DELEGATION_2).Value = Address2.Delegation
    End If

    ' Holder - City
    If Not Address2 Is Nothing Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_CITY_2).Value = Address2.City
    End If

    ' Holder - Federal Entity
    If Not Address2 Is Nothing Then
      _fed_entity = CardData.FedEntitiesString(Address2.FederalEntity)
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_FED_ENTITY_2).Value = _fed_entity
    End If

    ' Holder - Country
    _country = ""

    If Not Address2 Is Nothing Then
      Countries.GetCountryInfo(Address2.CountryAddress, _country, "", "")
    End If
    Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_COUNTRY_2).Value = _country
    'secondary address


    ' Holder - Iso2 Birth Country
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_BIRTH_COUNTRY) Then
      _birth_country = CardData.Iso2String(DbRow.Value(SQL_COLUMN_HOLDER_BIRTH_COUNTRY))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ISO2_BIRTH_COUNTRY).Value = _birth_country
    End If

    ' Holder - Occupation Id
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_OCCUPATION_ID) Then
      _occupation = CardData.OccupationsCodeString(DbRow.Value(SQL_COLUMN_HOLDER_OCCUPATION_ID))
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_OCCUPATION_CODE).Value = _occupation
    Else
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_OCCUPATION_CODE).Value = OCCUPATION_CODE_DEFAULT
    End If

    ' Holder - Occupation Description
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_OCCUPATION_ID) Then
      _occupation = CardData.OccupationsDescriptionString(DbRow.Value(SQL_COLUMN_HOLDER_OCCUPATION_ID))
    Else
      _occupation = CardData.OccupationsDescriptionStringByCode(OCCUPATION_CODE_DEFAULT)
    End If
    Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_OCCUPATION_DESCRIPTION).Value = _occupation

    ' Holder - Doc
    If DbRow.Value(SQL_COLUMN_HOLDER_ID_TYPE) <> ACCOUNT_HOLDER_ID_TYPE.RFC _
       AndAlso DbRow.Value(SQL_COLUMN_HOLDER_ID_TYPE) <> ACCOUNT_HOLDER_ID_TYPE.CURP Then
      If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ID) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_HOLDER_ID)) Then
        Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_DOC).Value = String.Format("{0} - {1}", IdentificationTypes.DocIdTypeString(DbRow.Value(SQL_COLUMN_HOLDER_ID_TYPE)), DbRow.Value(SQL_COLUMN_HOLDER_ID))
      End If
    End If

    ' Holder - Scanned Doc
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ID3_TYPE) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_SCANNED_DOC).Value = IdentificationTypes.DocIdTypeStringList(DbRow.Value(SQL_COLUMN_HOLDER_ID3_TYPE), True)
    End If

    ' Holder - RFC
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_RFC) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_RFC).Value = DbRow.Value(SQL_COLUMN_HOLDER_RFC)
    End If

    ' Holder - CURP
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_CURP) Then
      Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_CURP).Value = DbRow.Value(SQL_COLUMN_HOLDER_CURP)
    End If

    ' Holder - Is VIP
    Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_IS_VIP).Value = _no_text

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_IS_VIP)) Then
      If DbRow.Value(SQL_COLUMN_HOLDER_IS_VIP) Then
        Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_IS_VIP).Value = _yes_text
      End If
    End If

    ' Holder Has Beneficiary
    If DbRow.IsNull(SQL_COLUMN_HOLDER_HAS_BEN) OrElse DbRow.Value(SQL_COLUMN_HOLDER_HAS_BEN) = False Then
      Return True
    End If

    ' Beneficiary - Name
    If Not DbRow.IsNull(SQL_COLUMN_BEN_NAME) Then
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_NAME).Value = DbRow.Value(SQL_COLUMN_BEN_NAME)
    End If

    ' Beneficiary - Gender
    If Not DbRow.IsNull(SQL_COLUMN_BEN_GENDER) Then
      _ben_gender = CardData.GenderString(DbRow.Value(SQL_COLUMN_BEN_GENDER))
    End If

    Grid.Cell(RowIndex, GRID_COLUMN_BEN_GENDER).Value = _ben_gender

    ' Beneficiary - Age & Birthdate
    If Not DbRow.IsNull(SQL_COLUMN_BEN_BIRTHDATE) Then
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_AGE).Value = WSI.Common.Misc.CalculateAge(DbRow.Value(SQL_COLUMN_BEN_BIRTHDATE))
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_BIRTHDATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_BEN_BIRTHDATE), _
                                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                       ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    ' Beneficiary - Occupation Id
    If Not DbRow.IsNull(SQL_COLUMN_BEN_OCCUPATION_ID) Then
      _occupation = CardData.OccupationsCodeString(DbRow.Value(SQL_COLUMN_BEN_OCCUPATION_ID))
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_OCCUPATION_CODE).Value = _occupation
    Else
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_OCCUPATION_CODE).Value = OCCUPATION_CODE_DEFAULT
    End If

    ' Beneficiary - Occupation Description
    If Not DbRow.IsNull(SQL_COLUMN_BEN_OCCUPATION_ID) Then
      _occupation = CardData.OccupationsDescriptionString(DbRow.Value(SQL_COLUMN_BEN_OCCUPATION_ID))
    Else
      _occupation = CardData.OccupationsDescriptionStringByCode(OCCUPATION_CODE_DEFAULT)
    End If
    Grid.Cell(RowIndex, GRID_COLUMN_BEN_OCCUPATION_DESCRIPTION).Value = _occupation

    ' Beneficiary - RFC
    If Not DbRow.IsNull(SQL_COLUMN_BEN_ID_1) Then
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_DOC_RFC).Value = DbRow.Value(SQL_COLUMN_BEN_ID_1)
    End If

    ' Beneficiary - CURP
    If Not DbRow.IsNull(SQL_COLUMN_BEN_ID_2) Then
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_DOC_CURP).Value = DbRow.Value(SQL_COLUMN_BEN_ID_2)
    End If

    ' Beneficiary - Scanned Doc
    If Not DbRow.IsNull(SQL_COLUMN_BEN_ID3_TYPE) Then
      Grid.Cell(RowIndex, GRID_COLUMN_BEN_SCANNED_DOC).Value = IdentificationTypes.DocIdTypeStringList(DbRow.Value(SQL_COLUMN_BEN_ID3_TYPE), True)
    End If





  End Function ' SetupRowHolderData

  ' PURPOSE : Set the Holder Columns, if are printable or not.
  '
  '  PARAMS :
  '     - INPUT :  
  '           - ColumnOfGrid : Column where it begin the holder data.
  '           - Printable : Value it say if is printabled or not
  '
  '     - OUTPUT :
  '           - Grid
  '
  ' RETURNS : 
  Public Sub HolderColumnsPrintable(ByRef Grid As uc_grid, ByVal ColumnOfGrid As Integer, ByVal Printable As Boolean)

    For _idx_column As Integer = ColumnOfGrid To Grid.NumColumns - 1
      Grid.Column(_idx_column).IsColumnPrintable = Printable
    Next

  End Sub ' ColumnsHolderDataPrintable

  ' PURPOSE : Account Fields SQL
  '
  '  PARAMS :
  '     - INPUT :  
  '           - Show Sql Column Names
  '           - Show Sql Column Alias
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '           - String with SQL Acount fields.
  Public Function AccountFieldsSql(Optional ByVal ShowSqlColumnNames As Boolean = True, Optional ByVal ShowColumnAlias As Boolean = True) As String

    Dim _str_sql As System.Text.StringBuilder
    Dim _fields(,) As String

    _str_sql = New System.Text.StringBuilder()

    ' SQL COLUMN - SQL ALIAS
    _fields = New String(,) {{"AC_HOLDER_GENDER", "[HOLDERGENDER]"}, _
                              {"AC_HOLDER_BIRTH_DATE", "[HOLDERBIRTHDATE]"}, _
                              {"AC_HOLDER_WEDDING_DATE", "[WEDDINGDATE]"}, _
                              {"AC_HOLDER_ADDRESS_01", "[HOLDERADDRESS01]"}, _
                              {"AC_HOLDER_ADDRESS_02", "[HOLDERADDRESS02]"}, _
                              {"AC_HOLDER_ADDRESS_03", "[HOLDERADDRESS03]"}, _
                              {"AC_HOLDER_ZIP", "[HOLDERZIP]"}, _
                              {"AC_HOLDER_CITY", "[HOLDERCITY]"}, _
                              {"AC_HOLDER_ADDRESS_COUNTRY", "[HOLDERADDRESSCOUNTRY]"}, _
                              {"AC_HOLDER_EMAIL_01", "[HOLDEREMAIL01]"}, _
                              {"AC_HOLDER_EMAIL_02", "[HOLDEREMAIL02]"}, _
                              {"AC_HOLDER_TWITTER_ACCOUNT", "[HOLDERTWITTER]"}, _
                              {"AC_HOLDER_PHONE_NUMBER_01", "[PHONE1]"}, _
                              {"AC_HOLDER_PHONE_NUMBER_02", "[PHONE2]"}, _
                              {"AC_HOLDER_OCCUPATION_ID", "[HOLDEROCCUPATIONID]"}, _
                              {"AC_HOLDER_ID_TYPE", "[HOLDERIDTYPE]"}, _
                              {"AC_HOLDER_ID", "[HOLDERID]"}, _
                              {"AC_HOLDER_ID3_TYPE", "[HOLDERID3TYPE]"}, _
                              {"AC_HOLDER_NATIONALITY", "[HOLDERNATIONALITY]"}, _
                              {"AC_HOLDER_BIRTH_COUNTRY", "[HOLDERBIRTHCOUNTRY]"}, _
                              {"AC_HOLDER_FED_ENTITY", "[ACHOLDERFEDENTITY]"}, _
                              {"AC_HOLDER_EXT_NUM", "[ACHOLDEREXTNUM]"}, _
                              {"AC_HOLDER_ID1", "[ACHOLDERRFC]"}, _
                              {"AC_HOLDER_ID2", "[ACHOLDERCURP]"}, _
                              {"AC_BENEFICIARY_NAME", "[BENEFICIARYNAME]"}, _
                              {"AC_BENEFICIARY_BIRTH_DATE", "[BENEFICIARYBIRTHDATE]"}, _
                              {"AC_BENEFICIARY_GENDER", "[BENEFICIARYGENDER]"}, _
                              {"AC_BENEFICIARY_ID1", "[BENEFICIARYRFC]"}, _
                              {"AC_BENEFICIARY_ID2", "[BENEFICIARYCURP]"}, _
                              {"AC_BENEFICIARY_OCCUPATION_ID", "[BENEFICIARYOCCUPATIONID]"}, _
                              {"AC_BENEFICIARY_ID3_TYPE", "[BENEFICIARYID3TYPE]"}, _
                              {"AC_HOLDER_HAS_BENEFICIARY", "[HOLDERHASBENEFICIARY]"}, _
                              {"AC_USER_TYPE", "[ACCOUNTUSERTYPE]"}, _
                              {"AC_HOLDER_IS_VIP", "[HOLDERISVIP]"}}

    For _idx As Byte = 0 To _fields.GetLength(0) - 1

      _str_sql.AppendFormat(" , {0} {1}", _
                    IIf(ShowSqlColumnNames, _fields(_idx, 0), String.Empty), _
                    IIf(ShowColumnAlias, _fields(_idx, 1), String.Empty)).AppendLine()
    Next

    Return _str_sql.ToString()

  End Function

#End Region ' PUBLIC FUNCTIONS

#Region " PRIVATE FUNCTIONS "

  ' PURPOSE : Read General Params Account Visible Fields
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub ReadGeneralParams()

    m_account_visible_field_document = GeneralParam.GetBoolean("Account.VisibleField", "Document", True)
    m_account_visible_field_doc_scan = GeneralParam.GetBoolean("Account.VisibleField", "DocScan", True)
    m_account_visible_field_birth_date = GeneralParam.GetBoolean("Account.VisibleField", "BirthDate", True)
    m_account_visible_field_occupation = GeneralParam.GetBoolean("Account.VisibleField", "Occupation", True)
    m_account_visible_field_gender = GeneralParam.GetBoolean("Account.VisibleField", "Gender", True)
    m_account_visible_field_nationality = GeneralParam.GetBoolean("Account.VisibleField", "Nationality", True)
    m_account_visible_field_birth_country = GeneralParam.GetBoolean("Account.VisibleField", "BirthCountry", True)
    m_account_visible_field_wedding_date = GeneralParam.GetBoolean("Account.VisibleField", "WeddingDate", True)
    m_account_visible_field_phone_1 = GeneralParam.GetBoolean("Account.VisibleField", "Phone1", True)
    m_account_visible_field_phone_2 = GeneralParam.GetBoolean("Account.VisibleField", "Phone2", True)
    m_account_visible_field_email_1 = GeneralParam.GetBoolean("Account.VisibleField", "Email1", True)
    m_account_visible_field_address = GeneralParam.GetBoolean("Account.VisibleField", "Address", True)
    m_account_visible_field_address_02 = GeneralParam.GetBoolean("Account.VisibleField", "Address02", True)
    m_account_visible_field_address_03 = GeneralParam.GetBoolean("Account.VisibleField", "Address03", True)
    m_account_visible_field_ext_num = GeneralParam.GetBoolean("Account.VisibleField", "ExtNum", True)
    m_account_visible_field_zip = GeneralParam.GetBoolean("Account.VisibleField", "Zip", True)
    m_account_visible_field_city = GeneralParam.GetBoolean("Account.VisibleField", "City", True)
    m_account_visible_field_fed_entity = GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", True)
    m_account_visible_field_country = GeneralParam.GetBoolean("Account.VisibleField", "AddressCountry", True)
    m_account_visible_field_RFC = WSI.Common.IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.RFC)
    m_account_visible_field_CURP = WSI.Common.IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.CURP)

    m_ben_visible = WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl()

  End Sub ' ReadGeneralParams

#End Region ' PRIVATE FUNCTIONS

End Module
