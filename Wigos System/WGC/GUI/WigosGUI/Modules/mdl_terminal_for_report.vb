'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_terminal_for_report.vb
' DESCRIPTION:   Report module for managing the terminal reports
' AUTHOR:        Luis Mesa
' CREATION DATE: 14-AUG-2014 
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-AUG-2014  LEM    Initial version.
'--------------------------------------------------------------------

Imports WSI.Common
Imports GUI_Controls
Imports GUI_CommonOperations

Module mdl_terminal_for_report

#Region "Constants"
  Private Const MAIN_DATA_COLUMNS = 2
#End Region

#Region "Initialize"
  Private m_terminal_fields = New String(,) {{"TE_PROVIDER_ID", "[TERMINALPROVIDER]"}, _
                                             {"TE_NAME", "[TERMINALNAME]"}, _
                                             {"(SELECT AR_NAME FROM AREAS LEFT JOIN BANKS ON AR_AREA_ID = BK_AREA_ID WHERE BK_BANK_ID = TE_BANK_ID)", "[TERMINALAREA]"}, _
                                             {"(SELECT BK_NAME FROM BANKS WHERE BK_BANK_ID = TE_BANK_ID)", "[TERMINALBANK]"}, _
                                             {"TE_POSITION", "[TERMINALPOSITION]"}}
#End Region

#Region "Publics"

  Public Function NumColumns(Optional ByVal IncludeExtraData As Boolean = True) As Int32

    Return IIf(IncludeExtraData, m_terminal_fields.GetLength(0), MAIN_DATA_COLUMNS)
  End Function

  Public Function SqlFields(Optional ByVal ShowColumnNames As Boolean = True, Optional ByVal ShowColumnAlias As Boolean = True, _
                            Optional ByVal IncludeExtraData As Boolean = True) As String

    Dim _str_sql As System.Text.StringBuilder
    Dim _num_columns As Int32

    _str_sql = New System.Text.StringBuilder()
    _num_columns = NumColumns(IncludeExtraData)

    If ShowColumnNames Or ShowColumnAlias Then
      For _idx As Int32 = 0 To _num_columns - 1

        _str_sql.AppendFormat(" , {0} {1}", _
                      IIf(ShowColumnNames, m_terminal_fields(_idx, 0), String.Empty), _
                      IIf(ShowColumnAlias, m_terminal_fields(_idx, 1), String.Empty)).AppendLine()
      Next
    End If

    Return _str_sql.ToString()
  End Function

  Public Sub StyleSheetData(ByRef Grid As uc_grid, ByVal InitColumn As Integer, Optional ByVal HeaderTitle As String = "", _
                            Optional ByVal IncludeExtraData As Boolean = True)

    Dim _one_header As Boolean
    Dim _current_column As Int32

    _one_header = String.IsNullOrEmpty(HeaderTitle)
    _current_column = InitColumn

    With Grid

      ' Provider
      With .Column(_current_column)
        If _one_header Then
          .Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(341)
        Else
          .Header(0).Text = HeaderTitle
          .Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(341)
        End If
        .Width = 2000
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With
      _current_column += 1

      ' Terminal
      With .Column(_current_column)
        If _one_header Then
          .Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal
        Else
          .Header(0).Text = HeaderTitle
          .Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(330) ' Name
        End If
        .Width = 2200
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With
      _current_column += 1

      If IncludeExtraData Then
        ' Area
        With .Column(_current_column)
          If _one_header Then
            .Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)
          Else
            .Header(0).Text = HeaderTitle
            .Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)
          End If
          .Width = 1900
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        End With
        _current_column += 1

        ' Bank
        With .Column(_current_column)
          If _one_header Then
            .Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)
          Else
            .Header(0).Text = HeaderTitle
            .Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)
          End If
          .Width = 1400
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        End With
        _current_column += 1

        ' Position
        With .Column(_current_column)
          If _one_header Then
            .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222)
          Else
            .Header(0).Text = HeaderTitle
            .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222)
          End If
          .Width = 1000
          .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        End With
        _current_column += 1
      End If

    End With

  End Sub

  Public Function SetupRowData(ByRef Grid As uc_grid, ByVal DbRow As frm_base_sel.CLASS_DB_ROW, ByVal RowIndex As Int32, ByVal InitGridColumn As Int32, _
                               ByVal InitSqlColumn As Int32, Optional ByVal IncludeExtraData As Boolean = True) As Boolean

    Dim _current_sql_column
    Dim _current_grid_column

    _current_sql_column = InitSqlColumn
    _current_grid_column = InitGridColumn

    'Provider
    If Not DbRow.IsNull(_current_sql_column) Then
      Grid.Cell(RowIndex, _current_grid_column).Value = DbRow.Value(_current_sql_column)
    End If
    _current_sql_column += 1
    _current_grid_column += 1

    'Terminal
    If Not DbRow.IsNull(_current_sql_column) Then
      Grid.Cell(RowIndex, _current_grid_column).Value = DbRow.Value(_current_sql_column)
    End If
    _current_sql_column += 1
    _current_grid_column += 1

    If IncludeExtraData Then
      'Area
      If Not DbRow.IsNull(_current_sql_column) Then
        Grid.Cell(RowIndex, _current_grid_column).Value = DbRow.Value(_current_sql_column)
      End If
      _current_sql_column += 1
      _current_grid_column += 1

      'Bank
      If Not DbRow.IsNull(_current_sql_column) Then
        Grid.Cell(RowIndex, _current_grid_column).Value = DbRow.Value(_current_sql_column)
      End If
      _current_sql_column += 1
      _current_grid_column += 1

      'Position
      If Not DbRow.IsNull(_current_sql_column) Then
        Grid.Cell(RowIndex, _current_grid_column).Value = DbRow.Value(_current_sql_column)
      End If
      _current_sql_column += 1
      _current_grid_column += 1
    End If

  End Function

#End Region

End Module
