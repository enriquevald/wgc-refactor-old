'-------------------------------------------------------------------
' Copyright © 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_event_history.vb
' DESCRIPTION:   Event History module for Mannage the event History definitions from the Common Data
' AUTHOR:        Luis Rodríguez
' CREATION DATE: 12-MAY-2004
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-MAY-2004  LRS    Initial version.
'
'--------------------------------------------------------------------
Option Explicit On 

Imports System.Runtime.InteropServices
Imports GUI_CommonMisc

Public Module mdl_event_history

#Region " Constants "

  Public OPERATION_CODE_SHUTDOWN As Integer = 2

  Public Enum ENUM_SHUTDOWN_TYPE

    SHUTDOWN_AUTOMATIC = 0
    SHUTDOWN_MANUAL = 1
    SHUTDOWN_POWER_FAIL = 2

  End Enum

  Public Enum ENUM_TERMINAL_TYPE
    LKAS = 0
    LKT_ONE = 1
    LKT_ALL = 2
  End Enum

#End Region

#Region " Members "

  ' Structure that contains all General Params Definitions info
  Public GLB_OperationsDef() As CLASS_OPERATION_DEFINITION

#End Region ' Members


#Region " CommonData.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class CLASS_OPERATION_DEFINITION
    Public control_block As Integer
    Public operation_code As Integer
    Public operation_nls_id As Integer
    Public operation_type As Integer

    Public Sub New()
      control_block = Marshal.SizeOf(Me)
    End Sub
  End Class

#End Region

#Region "Prototypes "

  Declare Function Common_DeviceDefNum Lib "CommonData" () As Integer
  Declare Function Common_DeviceGetCode Lib "CommonData" (ByVal DeviceIndex As Integer) As Integer
  Declare Function Common_DeviceGetType Lib "CommonData" (ByVal DeviceIndex As Integer) As Integer

  Declare Function Common_OperationDefNum Lib "CommonData" () As Integer
  Declare Function Common_OperationGet Lib "CommonData" (ByVal OperationIndex As Integer, _
                                                         <[In](), [Out]()> _
                                                         ByVal OperationDefinition As CLASS_OPERATION_DEFINITION) As Boolean
#End Region

#End Region

#Region " Public Functions "

  ' PURPOSE: Get Terminal Type from Terminal Code
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Terminal Type

  Public Function GetTerminalType(ByVal TerminalCode As Integer) As ENUM_TERMINAL_TYPE
    Dim TerminalType As ENUM_TERMINAL_TYPE

    Select Case TerminalCode
      Case TERMINAL_TYPE_AGENCY
        TerminalType = ENUM_TERMINAL_TYPE.LKAS
      Case TERMINAL_TYPE_ALL_LKTS
        TerminalType = ENUM_TERMINAL_TYPE.LKT_ALL
      Case Else
        TerminalType = ENUM_TERMINAL_TYPE.LKT_ONE
    End Select

    Return TerminalType
  End Function

  ' PURPOSE: Get Nls String from terminal type id ( LKAS or LKT %1)
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalType
  '           - TerminalId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Nls String
  Public Function GetNlsTerminalType(ByVal TerminalType As ENUM_TERMINAL_TYPE, _
                                     ByVal TerminalId As Integer) As String
    Select Case TerminalType
      Case ENUM_TERMINAL_TYPE.LKAS
        Return GLB_NLS_GUI_AUDITOR.GetString(311)
      Case ENUM_TERMINAL_TYPE.LKT_ONE
        Return GLB_NLS_GUI_AUDITOR.GetString(273) & " " & TerminalId
      Case ENUM_TERMINAL_TYPE.LKT_ALL
        Return GLB_NLS_GUI_AUDITOR.GetString(273) & " " & GLB_NLS_GUI_AUDITOR.GetString(251)
      Case Else
        Return " "
    End Select

  End Function

#End Region

End Module