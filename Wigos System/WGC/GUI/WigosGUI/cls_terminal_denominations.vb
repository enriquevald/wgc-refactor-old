'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_terminal_denominations.vb
'
' DESCRIPTION : terminal denominations config class
'              
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-AGO-2014  LEM    Initial version.
' 26-SEP-2014  LEM    Fixed Bug #WIG-1291: Data updating time is too long
'-------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System.Text

Public Class CLASS_TERMINAL_DENOMINATIONS
  Inherits CLASS_BASE

#Region "Members"
  Private m_denominations As List(Of String)
  Private m_single_denominations As List(Of String)
  Private m_multi_denominations As List(Of String)
#End Region

#Region "Constants"
  Private Const EGM_DENOMINATIONS = "0.0005;0.0010;0.0020;0.0025;0.0050;0.01;0.02;0.03;0.05;0.10;0.15;0.20;0.25;0.40;0.50;1;10;100;1000;2;2.50;20;200;2000;25;250;2500;5;50;500;5000;"
  Public Const DENOMINATIONS_SEP = ";"
  Public Const MULTI_SEP = "-"
#End Region

#Region "Publics"

  Public Sub New()
    m_denominations = New List(Of String)()
    m_single_denominations = New List(Of String)()
    m_multi_denominations = New List(Of String)()
  End Sub

  Public ReadOnly Property MultiDenominations() As List(Of String)
    Get
      Return m_multi_denominations
    End Get
  End Property    ' MultiDenominations 

  Public ReadOnly Property SingleDenominations() As List(Of String)
    Get
      Return m_single_denominations
    End Get
  End Property   ' SingleDenominations

  Public ReadOnly Property Denominations() As List(Of String)
    Get
      Return m_denominations
    End Get
  End Property   ' SingleDenominations

  Public Shared Function SortDenominations(ByVal DenominationsList As List(Of String), ByVal Multi As Boolean) As List(Of String)
    Dim _dec_list As List(Of Decimal)
    Dim _sorted_list As List(Of String)
    Dim _list As String
    Dim _multi_list() As String
    Dim _value As Decimal

    _sorted_list = New List(Of String)()

    Try
      If Not Multi Then

        _dec_list = New List(Of Decimal)()
        For Each _item As String In DenominationsList
          _value = GUI_ParseNumber(_item.Trim())
          _dec_list.Add(_value)
        Next
        _dec_list.Sort()
        For Each _dec As Decimal In _dec_list
          _sorted_list.Add(_dec.ToString())
        Next

      Else

        For Each _multi_item As String In DenominationsList
          _multi_list = _multi_item.Split(MULTI_SEP)

          _dec_list = New List(Of Decimal)()
          _list = ""
          For Each _item As String In _multi_list
            _value = GUI_ParseNumber(_item.Trim())
            _dec_list.Add(_value)
          Next
          _dec_list.Sort()
          For Each _dec As Decimal In _dec_list
            _list += _dec.ToString() + MULTI_SEP
          Next

          _list = _list.Trim(MULTI_SEP)
          _sorted_list.Add(_list)
        Next

        _sorted_list.Sort()

      End If

      Return _sorted_list

    Catch _ex As Exception

      Return DenominationsList
    End Try
  End Function       ' SortDenominations

  Public Shared Function NumberFormatDenomination(ByVal Denomination As String, Optional ByVal FromDB As Boolean = False, Optional ByVal ToDB As Boolean = False) As String
    Dim _dec_list As List(Of Decimal)
    Dim _value As Decimal
    Dim _multi_list() As String
    Dim _list As String

    Try
      _dec_list = New List(Of Decimal)()
      _list = ""

      _multi_list = Denomination.Split(MULTI_SEP)

      For Each _item As String In _multi_list
        If FromDB Then
          _value = GUI_ParseNumber(GUI_DBNumberToLocalNumber(_item.Trim()))
        Else
          _value = GUI_ParseNumber(_item.Trim())
        End If
        _dec_list.Add(_value)
      Next

      _dec_list.Sort()

      For Each _dec As Decimal In _dec_list
        If ToDB Then
          _list += GUI_LocalNumberToDBNumber(_dec.ToString()) + MULTI_SEP
        Else
          _list += ScreenFormatDenomination(_dec) + MULTI_SEP
        End If
      Next

      _list = _list.Trim(MULTI_SEP)

      Return _list

    Catch _ex As Exception

      Return Denomination
    End Try

  End Function        ' NumberFormatDenomination

  Private Shared Function DB_DenominationsToList(ByVal Denominations As String) As List(Of String)
    Dim _list As List(Of String)

    _list = New List(Of String)()

    For Each _item As String In Denominations.Split(New String() {DENOMINATIONS_SEP}, StringSplitOptions.RemoveEmptyEntries)

      _list.Add(NumberFormatDenomination(_item, True))

    Next

    Return _list
  End Function     ' DB_DenominationsToList

  Public Shared Function DenominationsToList(ByVal Denominations As String) As List(Of String)
    Dim _list As List(Of String)

    _list = New List(Of String)()
    For Each _item As String In Denominations.Split(New String() {DENOMINATIONS_SEP}, StringSplitOptions.RemoveEmptyEntries)
      _list.Add(_item)
    Next

    Return _list
  End Function     ' DenominationsToList

  Private Shared Function DB_ListToDenominations(ByVal List As List(Of String)) As String
    Dim _result As List(Of String)

    _result = New List(Of String)()

    For Each _item As String In List

      _result.Add(NumberFormatDenomination(_item, False, True))

    Next

    Return ListToDenominations(_result, False)
  End Function     ' DB_ListToDenominations

  Public Shared Function ListToDenominations(ByVal List As List(Of String), Optional ByVal Multi As Boolean = False) As String
    Return String.Join(IIf(Multi, MULTI_SEP, DENOMINATIONS_SEP), List.ToArray())
  End Function     ' ListToDenominations

  Public Shared Function ScreenFormatDenomination(ByVal Value As Decimal) As String
    Dim _format As String

    _format = "{0}"

    Select Case Value

      Case Is < 0.01
        _format = "{0:0.0000#}"

      Case Is < 1
        _format = "{0:0.00###}"

      Case Else
        _format = "{0:0.###}"

    End Select

    Return String.Format(_format, Value)
  End Function    ' FormatedDenomination

#End Region

#Region "Overrides"

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA
    Dim _auditor As CLASS_AUDITOR_DATA

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5226), "")

    Try

      _auditor.SetField(GLB_NLS_GUI_CONFIGURATION.Id(420), IIf(m_single_denominations.Count = 0, AUDIT_NONE_STRING, ListToDenominations(m_single_denominations)))
      _auditor.SetField(GLB_NLS_GUI_CONFIGURATION.Id(421), IIf(m_multi_denominations.Count = 0, AUDIT_NONE_STRING, ListToDenominations(m_multi_denominations)))

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _auditor
  End Function    ' AuditorData

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS

    Return ReadTerminalDenominations()

  End Function        ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim _result As ENUM_STATUS

    Using _db_trx As New DB_TRX()

      _result = UpdateTerminalDenominations(_db_trx.SqlTransaction)

      If _result = ENUM_STATUS.STATUS_OK Then
        _db_trx.Commit()
      End If

    End Using

    Return _result
  End Function      ' DB_Update

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE
    Dim _clone As CLASS_TERMINAL_DENOMINATIONS

    _clone = New CLASS_TERMINAL_DENOMINATIONS()

    _clone.m_single_denominations = New List(Of String)(m_single_denominations.ToArray())
    _clone.m_multi_denominations = New List(Of String)(m_multi_denominations.ToArray())

    Return _clone
  End Function      ' Duplicate

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function      ' DB_Delete

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS
    Return ENUM_STATUS.STATUS_ERROR
  End Function      ' DB_Insert

#End Region

#Region "Privates"

  Private Function RemoveDuplicatesValues(ByVal DenominationsList As List(Of String)) As List(Of String)
    Dim _list As List(Of String)

    _list = New List(Of String)()
    For Each _item As String In DenominationsList
      If Not _list.Contains(_item) Then
        _list.Add(_item)
      End If
    Next

    Return _list
  End Function

  Public Function ReadTerminalDenominations() As ENUM_STATUS
    Dim _general_params As GeneralParam.Dictionary

    _general_params = GeneralParam.Dictionary.Create()

    If _general_params Is Nothing Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    m_denominations = RemoveDuplicatesValues(SortDenominations(DB_DenominationsToList(_general_params.GetString("EGM", "Denominations", EGM_DENOMINATIONS)), False))
    m_single_denominations = RemoveDuplicatesValues(SortDenominations(DB_DenominationsToList(_general_params.GetString("EGM", "SingleDenominations")), False))
    m_multi_denominations = RemoveDuplicatesValues(SortDenominations(DB_DenominationsToList(_general_params.GetString("EGM", "MultiDenominations")), True))

    Return ENUM_STATUS.STATUS_OK
  End Function      ' ReadTerminalDenominations

  Private Function UpdateTerminalDenominations(ByVal Trx As SqlTransaction) As ENUM_STATUS

    If Not GUI_Controls.Misc.DB_GeneralParam_Update("EGM", "SingleDenominations", DB_ListToDenominations(m_single_denominations), Trx) Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    If Not GUI_Controls.Misc.DB_GeneralParam_Update("EGM", "MultiDenominations", DB_ListToDenominations(m_multi_denominations), Trx) Then
      Return ENUM_STATUS.STATUS_ERROR
    End If

    Return ENUM_STATUS.STATUS_OK
  End Function   ' UpdateTerminalDenominations

#End Region

End Class
