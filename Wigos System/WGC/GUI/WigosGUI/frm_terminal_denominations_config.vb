'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_terminal_denominations_config.vb
'
' DESCRIPTION : Terminal denominations list
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-AGO-2014  LEM    Initial version.   
' 26-SEP-2014  LEM    Fixed Bug #WIG-1293: Wrong format for decimal values greater than 1
' 26-SEP-2014  LEM    Fixed Bug #WIG-1291: Data updating time is too long
' 09-DEC-2014  JBC    Fixed Bug #WIG-1814: Delete permissions don't work
'--------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_terminal_denominations_config
  Inherits frm_base_edit

#Region "Constants"
  Private Const GRID_COLUMN_DENOMINATION_ADDED = 0
  Private Const GRID_COLUMN_DENOMINATION_VALUE = 1

  Private Const MAX_MULTI_ITEMS = 8
#End Region

#Region "Members"
  Dim m_editing As Boolean
  Dim m_selected_count As Int32
  Dim m_current As ListView
  Dim m_width_offset As Int32
  Dim m_height_offset As Int32
#End Region

#Region "Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_DENOMINATIONS_CONFIG

    Call MyBase.GUI_SetFormId()

  End Sub               ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_width_offset = Me.Width - flp_main.Width
    m_height_offset = Me.Height - flp_main.Height

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5226)

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.btn_create.Enabled = Me.Permissions.Write
    Me.btn_remove.Enabled = Me.Permissions.Delete

    Me.btn_create.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_remove.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    Me.gb_SAS.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5227)
    Me.lbl_single.Text = GLB_NLS_GUI_CONFIGURATION.GetString(420)
    Me.lbl_multi.Text = GLB_NLS_GUI_CONFIGURATION.GetString(421)

    Call InitLists()

  End Sub         ' GUI_InitControls

  Protected Overrides Sub GUI_SetInitialFocus()
  End Sub      ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_GetScreenData()
    Dim _edit_object As CLASS_TERMINAL_DENOMINATIONS

    _edit_object = DbEditedObject

    _edit_object.SingleDenominations.Clear()
    _edit_object.MultiDenominations.Clear()

    For Each _item As ListViewItem In lv_single.Items
      _edit_object.SingleDenominations.Add(_item.Text)
    Next

    For Each _item As ListViewItem In lv_multi.Items
      _edit_object.MultiDenominations.Add(_item.Text)
    Next
  End Sub        ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _edit_object As CLASS_TERMINAL_DENOMINATIONS

    _edit_object = DbEditedObject

    Call LoadSASDenominations(_edit_object)

    For Each _value As String In _edit_object.SingleDenominations
      lv_single.Items.Add(New ListViewItem(_value))
    Next

    For Each _value As String In _edit_object.MultiDenominations
      lv_multi.Items.Add(New ListViewItem(_value))
    Next
  End Sub        ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_TERMINAL_DENOMINATIONS()

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub         ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Return

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub          ' GUI_ButtonClick

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return True
  End Function  ' GUI_IsScreenDataOk

#End Region

#Region "Privates"

  Private Sub LoadSASDenominations(ByVal TerminalDenominations As CLASS_TERMINAL_DENOMINATIONS)
    Dim _denominations As List(Of String)
    Dim _new_chk As CheckBox
    Dim _value As Decimal
    Dim _log As Int32
    Dim _row As Int32
    Dim _pos As Int32
    Dim _row_count As Int32
    Dim _column_count As Int32
    Dim _positions As Dictionary(Of String, Point)
    Dim _location As Point

    tlp_SAS.Controls.Clear()
    tlp_SAS.RowCount = 0
    tlp_SAS.ColumnCount = 0

    _denominations = TerminalDenominations.Denominations

    _log = Int32.MinValue
    _row = -1
    _row_count = 11
    _column_count = 0
    _positions = New Dictionary(Of String, Point)()

    For Each _d As String In _denominations

      _value = GUI_ParseNumber(_d)
      If _value > 0 Then
        _pos = IIf(Math.Floor(Math.Log10(_d)) Mod 2 = 0, Math.Floor(Math.Log10(_d)), Math.Floor(Math.Log10(_d)) - 1)
        If _log <> _pos Then
          _row = 0
          _log = _pos
          _column_count += 1
        Else
          _row += 1
        End If

        _positions.Add(_d, New Point(_column_count - 1, _row))

      End If

    Next

    tlp_SAS.ColumnCount = _column_count
    tlp_SAS.RowCount = _row_count
    tlp_SAS.ColumnStyles.Clear()
    tlp_SAS.RowStyles.Clear()

    For _idx As Int32 = 1 To _column_count
      tlp_SAS.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize, 0))
    Next
    For _idx As Int32 = 1 To _row_count
      tlp_SAS.RowStyles.Add(New RowStyle(SizeType.Absolute, 30))
    Next

    For Each _d As String In _denominations

      _value = GUI_ParseNumber(_d)
      If _value > 0 Then

        _new_chk = New CheckBox()
        _new_chk.Text = CLASS_TERMINAL_DENOMINATIONS.ScreenFormatDenomination(_value)
        _new_chk.Tag = _value
        _new_chk.AutoSize = True
        _new_chk.MinimumSize = New Size(80, 0)
        _new_chk.Checked = False

        AddHandler _new_chk.CheckedChanged, AddressOf CheckEvent

        _location = _positions(_d)

        tlp_SAS.Controls.Add(_new_chk, _location.X, _location.Y)
      End If

    Next

    m_selected_count = 0
  End Sub   ' LoadSASDenominations

  Private Sub InitLists()
    lv_single.Items.Clear()
    lv_multi.Items.Clear()
  End Sub              ' InitLists

  Private Sub ResetHighLighting()

    tmr_highlighting.Stop()

    For Each _item As ListViewItem In lv_single.Items
      _item.BackColor = SystemColors.Window
      _item.ForeColor = SystemColors.WindowText
    Next

    For Each _item As ListViewItem In lv_multi.Items
      _item.BackColor = SystemColors.Window
      _item.ForeColor = SystemColors.WindowText
    Next

  End Sub

  Private Sub HighLighting(ByVal Item As ListViewItem)
    Item.BackColor = Color.Blue
    Item.ForeColor = Color.White

    tmr_highlighting.Enabled = True
    tmr_highlighting.Interval = 1000
    tmr_highlighting.Start()
  End Sub

#End Region

#Region "Events"

  Private Sub RemoveEvent() Handles btn_remove.ClickEvent
    If Not m_current Is Nothing Then
      If m_current.Name = lv_single.Name And lv_single.SelectedItems.Count > 0 Then
        For Each _item As ListViewItem In lv_single.SelectedItems
          lv_single.Items.Remove(_item)
        Next
      End If
      If m_current.Name = lv_multi.Name And lv_multi.SelectedItems.Count > 0 Then
        For Each _item As ListViewItem In lv_multi.SelectedItems
          lv_multi.Items.Remove(_item)
        Next
      End If
    End If
  End Sub    ' RemoveEvent

  Private Sub CheckEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    If CType(sender, CheckBox).Checked Then
      m_selected_count += 1
    Else
      m_selected_count -= 1
    End If

    If m_selected_count > MAX_MULTI_ITEMS Then
      CType(sender, CheckBox).Checked = False
    End If
  End Sub     ' CheckEvent

  Private Sub AddEvent() Handles btn_create.ClickEvent
    Dim _list As List(Of String)
    Dim _added As Boolean
    Dim _value_to_add As Decimal
    Dim _string_to_add As String
    Dim _current_value As Decimal

    _list = New List(Of String)()

    For Each _ctrl As Control In tlp_SAS.Controls
      If TypeOf (_ctrl) Is CheckBox AndAlso CType(_ctrl, CheckBox).Checked Then
        _list.Add(_ctrl.Tag.ToString())
        CType(_ctrl, CheckBox).Checked = False
      End If
    Next

    _added = False

    If _list.Count = 1 Then

      _value_to_add = GUI_ParseNumber(_list(0).Trim())

      For Each _item As ListViewItem In lv_single.Items

        _current_value = GUI_ParseNumber(_item.Text.Trim())

        If _current_value = _value_to_add Then
          HighLighting(_item)
          _added = True
          Exit For
        End If

        If _current_value > _value_to_add Then
          lv_single.Items.Insert(_item.Index, New ListViewItem(CLASS_TERMINAL_DENOMINATIONS.ScreenFormatDenomination(_value_to_add)))
          _added = True
          Exit For
        End If
      Next

      If Not _added Then
        lv_single.Items.Add(New ListViewItem(CLASS_TERMINAL_DENOMINATIONS.ScreenFormatDenomination(_value_to_add)))
      End If

    End If

    If _list.Count > 1 Then

      _string_to_add = CLASS_TERMINAL_DENOMINATIONS.ListToDenominations(_list, True)
      _string_to_add = CLASS_TERMINAL_DENOMINATIONS.NumberFormatDenomination(_string_to_add, False, False)

      For Each _item As ListViewItem In lv_multi.Items

        If _item.Text = _string_to_add Then
          HighLighting(_item)
          _added = True
          Exit For
        End If

        If _item.Text > _string_to_add Then
          lv_multi.Items.Insert(_item.Index, New ListViewItem(_string_to_add))
          _added = True
          Exit For
        End If

      Next

      If Not _added Then
        lv_multi.Items.Add(New ListViewItem(_string_to_add))
      End If

    End If

    m_selected_count = 0
  End Sub       ' AddEvent

  Private Sub SelectEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles lv_single.GotFocus, lv_multi.GotFocus
    m_current = sender
  End Sub    ' SelectEvent

  Private Sub flp_main_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles flp_main.Resize
    Me.Width = flp_main.Width + m_width_offset
    Me.Height = flp_main.Height + m_height_offset
  End Sub

  Private Sub tmr_highlighting_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_highlighting.Tick
    Call ResetHighLighting()
  End Sub

#End Region

End Class