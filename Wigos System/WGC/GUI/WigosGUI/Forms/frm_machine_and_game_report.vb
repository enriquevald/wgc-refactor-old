'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_machine_and_game_report
' DESCRIPTION:   
' AUTHOR:        Jordi Masachs
' CREATION DATE: 23-DIC-2014
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 23-DIC-2014 JMV    First Release
' 12-FEB-2015 FOS    Defect WIG-2047 Select DateTo problem when isn't Enabled
' 12-FEB-2015 FOS    Defect WIG-2053 Implement print page Filters for Excel and printer.
' 24-FEB-2015 FOS    Defect WIG-2047 Select DateFrom problem when isn't Enabled
' 02-FEB-2016 DDS    Bug 8860: When ToDate is not selected shows yesterday's data
'-------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region "Imports"
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
#End Region

Public Class frm_machine_and_game_report
  Inherits frm_base_sel

#Region " Members "
  Private m_date_from As String
  Private m_date_to As String
#End Region

#Region " Constants "

  Private Const SQL_COLUMN_TYPE As Integer = 0
  Private Const SQL_COLUMN_GROUP As Integer = 1
  Private Const SQL_COLUMN_TEXT As Integer = 2
  Private Const SQL_COLUMN_CASH_IN As Integer = 3
  Private Const SQL_COLUMN_CASH_OUT As Integer = 4
  Private Const SQL_COLUMN_HANDPAY As Integer = 5
  Private Const SQL_COLUMN_NETWIN As Integer = 6

  Private GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COLUMN_GROUP As Integer = 1
  Private GRID_COLUMN_TEXT As Integer = 2
  Private GRID_COLUMN_CASH_IN As Integer = 3
  Private GRID_COLUMN_CASH_OUT As Integer = 4
  Private GRID_COLUMN_HANDPAY As Integer = 5
  Private GRID_COLUMN_NETWIN As Integer = 6

  Private GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region

#Region " Enums "


#End Region

#Region " OVERRIDES "

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MACHINE_AND_GAME_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5820)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

    ' 2 minutes timeout
    Call GUI_SetQueryTimeout(300)

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Return True
  End Function

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - 
  Protected Overrides Sub GUI_AfterLastRow()

    ' Draw totals
    Call DrawRow(True)

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    With Me.Grid

      Dim _group As String

      Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).Value = ""

      _group = ""

      Select Case DbRow.Value(SQL_COLUMN_GROUP)
        Case 1 : _group = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5822)
        Case 2 : _group = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5823)
        Case 3 : _group = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5824)
      End Select

      Me.Grid.Cell(RowIndex, GRID_COLUMN_GROUP).Value = _group

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TEXT).Value = DbRow.Value(SQL_COLUMN_TEXT).ToString()

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_IN))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_OUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_OUT))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAY).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_HANDPAY))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN))


    End With

    Return True
  End Function

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_cmd As SqlCommand
    Dim _str_sql As StringBuilder
    Dim _date_from As Date
    Dim _date_to As Date

    _str_sql = New StringBuilder()
    _str_sql.AppendLine("EXEC MachineAndGameReport @pDateFrom, @pDateTo")

    _sql_cmd = New SqlCommand(_str_sql.ToString())

    'FOS 24-FEB-2015
    If Me.uc_dsl.FromDateSelected Then
      _date_from = Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    Else
      _date_from = New Date(2007, 1, 1, 0, 0, 0, 0)
    End If

    'FOS 12-FEB-2015
    If Me.uc_dsl.ToDateSelected Then
      _date_to = Me.uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime)
    Else
      'DDS 02-FEB-2016
      _date_to = Date.MaxValue
    End If

    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _date_from
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _date_to

    Return _sql_cmd

  End Function ' GUI_GetSqlCommand

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter("", "", True)

    PrintData.FilterValueWidth(1) = 5000


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _date As Date

    m_date_from = ""
    m_date_to = ""

    ' Date
    If Me.uc_dsl.FromDateSelected Then
      _date = GetFromDate()
      m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      _date = GetToDate()
      m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index - 0
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Group - 1
      .Column(GRID_COLUMN_GROUP).Header(0).Text = ""
      .Column(GRID_COLUMN_GROUP).Width = 3000
      .Column(GRID_COLUMN_GROUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_GROUP).HighLightWhenSelected = False
      .Column(GRID_COLUMN_GROUP).IsMerged = True

      ' Text - 2
      .Column(GRID_COLUMN_TEXT).Header(0).Text = ""
      .Column(GRID_COLUMN_TEXT).Width = 4000
      .Column(GRID_COLUMN_TEXT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' Cash in - 3
      .Column(GRID_COLUMN_CASH_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(379)
      .Column(GRID_COLUMN_CASH_IN).Width = 2000
      .Column(GRID_COLUMN_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' Cash out - 4
      .Column(GRID_COLUMN_CASH_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(380)
      .Column(GRID_COLUMN_CASH_OUT).Width = 2000
      .Column(GRID_COLUMN_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' Handpay - 5
      .Column(GRID_COLUMN_HANDPAY).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(407)
      .Column(GRID_COLUMN_HANDPAY).Width = 2000
      .Column(GRID_COLUMN_HANDPAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' Netwin - 6
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NETWIN).Width = 2000
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

  End Sub ' SetDefaultValues


  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '          - LastTotal: Indicates the kind of total
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - True
  Private Function DrawRow(ByVal LastTotal As Boolean, Optional ByVal IsTableTypeSubTotal As Boolean = False) As Boolean
    Dim _idx_row As Integer

    _idx_row = Me.Grid.NumRows - 1

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Return True
  End Function ' DrawRow

  ' PURPOSE: Return TO date of filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Date
  Private Function GetToDate() As Date

    Dim _to_time As Date = New Date(Me.uc_dsl.ToDate.Year, Me.uc_dsl.ToDate.Month, Me.uc_dsl.ToDate.Day, Me.uc_dsl.ClosingTime, 0, 0)
    Dim _now As Date = WSI.Common.Misc.TodayOpening().AddDays(1)

    If Me.uc_dsl.ToDateSelected Then
      If _to_time > _now Then
        _to_time = _now
      End If
    Else
      _to_time = _now
    End If

    Return _to_time

  End Function

  ' PURPOSE: Return from date of filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Date
  Private Function GetFromDate() As Date

    Return New Date(Me.uc_dsl.FromDate.Year, Me.uc_dsl.FromDate.Month, Me.uc_dsl.FromDate.Day, Me.uc_dsl.ClosingTime, 0, 0)

  End Function

#End Region

#Region " Events "



#End Region
End Class