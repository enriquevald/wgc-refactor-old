'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_money_pending_transfer
' DESCRIPTION:   List Money Pending to be transferred.
' AUTHOR:        Susana Sams�
' CREATION DATE: 08-FEB-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-FEB-2012  SSC    Initial version.
' 16-MAY-2012  DDM    Modifications: - Sql query for to be Notes History
'                                    - Add column "Not transferred".
'                                    - The filter date: The hour by defect. Changed literal "Reported" by "Date"
' 22-MAY-2012  ACC    Add code and functions for Acceptors funcionality. Change accounts filter.
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 11-APR-2013  RBG    #707: Wrong align in the column card data.
' 17-JUN-2013  DHA    Fixed Bug #848: Wrong date format on field First Ticket from Excel
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_money_pending_transfer
  Inherits frm_base_sel

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private GRID_COLUMN_SENTINEL As Integer
  Private GRID_COLUMN_REPORTED As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_TERMINAL As Integer
  Private GRID_COLUMN_AMOUNT As Integer
  Private GRID_COLUMN_TRACKDATA As Integer
  Private GRID_COLUMN_ACCOUNT As Integer
  Private GRID_COLUMN_HOLDER_NAME As Integer
  Private GRID_COLUMN_NOT_TRANSFERRED As Integer

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private GRID_COLUMNS As Integer

  Private Const GRID_WIDTH_PROVIDER As Integer = 1800
  Private Const GRID_WIDTH_TERMINAL As Integer = 2300

  ' Sql Columns
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_TRANSACTION As Integer = 1
  Private Const SQL_COLUMN_REPORTED As Integer = 2
  Private Const SQL_COLUMN_PROVIDER As Integer = 3
  Private Const SQL_COLUMN_TERMINAL As Integer = 4
  Private Const SQL_COLUMN_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_TRACKDATA As Integer = 6
  Private Const SQL_COLUMN_ACCOUNT As Integer = 7
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 8
  Private Const SQL_COLUMN_NOT_TRANSFERRED As Integer = 9

  'DDM 16-05-2012: Constant to know if is transferred or not.
  Private Const NOTES_NOT_TRANSFERRED As Integer = 0

#End Region ' Constants

#Region " Structures "

#End Region ' Structures

#Region " Enums "

#End Region ' Enums

#Region " Members "

  'REPORT FILTER
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_report_type As String
  Private m_track_data As String
  Private m_account As String
  Private m_no_account As Boolean
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_account_filter_type As String

  'TOTAL
  Dim m_sum_transf As Decimal

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MONEY_PENDING_TRANSFER

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Money_Pending to Tansfer
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(35)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Transfer button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_INVOICING.GetString(36)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201) 'GLB_NLS_GUI_ALARMS.GetString(441)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'Accounts
    Me.gb_account.Text = ""
    Me.opt_trackdata.Text = GLB_NLS_GUI_INVOICING.GetString(97)
    Me.opt_transfer_to.Text = GLB_NLS_GUI_INVOICING.GetString(98)
    Me.opt_all_accounts.Text = GLB_NLS_GUI_INVOICING.GetString(254)
    Me.uc_account_sel1.Anon = False
    'Me.uc_account_sel1.ShowAnonFilter()

    Call Me.ef_trackdata.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AcceptorTerminalTypeList())

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_from

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder
    Dim str_where_account As String = ""
    Dim _str_aux As String = ""

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" DECLARE @STARTED AS DATETIME ")
    _str_sql.AppendLine(" DECLARE @FINISHED AS DATETIME ")

    _str_sql.AppendLine(" SET @STARTED = " & GUI_FormatDateDB(dtp_from.Value))
    _str_sql.AppendLine(" SET @FINISHED = " & GUI_FormatDateDB(dtp_to.Value))

    _str_sql.AppendLine(" SELECT   ")
    _str_sql.AppendLine("          TM_TERMINAL_ID PK0	   ")
    _str_sql.AppendLine("        , TM_TRANSACTION_ID PK1 ")
    _str_sql.AppendLine("        , TM_REPORTED	         ")
    _str_sql.AppendLine("        , TE_PROVIDER_ID	       ")
    _str_sql.AppendLine("        , TE_NAME               ")
    _str_sql.AppendLine("        , TM_AMOUNT             ")
    _str_sql.AppendLine("        , TM_TRACKDATA	         ")
    _str_sql.AppendLine("        , AC_ACCOUNT_ID	       ")
    _str_sql.AppendLine("        , AC_HOLDER_NAME	       ")
    _str_sql.AppendLine("        , CASE WHEN TM_TRANSFERRED IS NULL THEN 0 ELSE 1 END TM_NOT_TRANSFERRED")
    _str_sql.AppendLine("   FROM   ")
    _str_sql.AppendLine("  	       TERMINAL_MONEY INNER JOIN TERMINALS ON TE_TERMINAL_ID = TM_TERMINAL_ID ")
    _str_sql.AppendLine("  	       LEFT  JOIN ACCOUNTS  ON AC_ACCOUNT_ID = TM_TRANSFERRED_TO_ACCOUNT_ID ")
    _str_sql.AppendLine("  WHERE TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())
    _str_sql.AppendLine("    AND TM_TERMINAL_ID = TE_TERMINAL_ID ")

    If Me.dtp_to.Checked Then
      _str_sql.AppendLine("  AND TM_REPORTED < @FINISHED ")
    End If

    If Me.dtp_from.Checked Then
      _str_sql.AppendLine("  AND TM_REPORTED >= @STARTED ")
    End If

    If opt_transfer_to.Checked Then
      str_where_account = Me.uc_account_sel1.GetFilterSQL()
      If str_where_account <> "" Then
        _str_sql.AppendLine("  AND " & str_where_account)
      End If
    ElseIf opt_trackdata.Checked And ef_trackdata.TextValue <> "" Then
      _str_aux = Me.ef_trackdata.TextValue.Replace("'", "''")
      _str_sql.AppendLine("  AND TM_TRACKDATA LIKE '%" & _str_aux & "%' ")
    End If

    _str_sql.AppendLine("    ORDER BY TM_REPORTED DESC ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery 


  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_REPORTED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_REPORTED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TRACKDATA)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACKDATA).Value = DbRow.Value(SQL_COLUMN_TRACKDATA)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ACCOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_ACCOUNT)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    'DDM 16-05-2012: Value to the column "not transferred"
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_NOT_TRANSFERRED)) Then
      If DbRow.Value(SQL_COLUMN_NOT_TRANSFERRED) = NOTES_NOT_TRANSFERRED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NOT_TRANSFERRED).Value = GLB_NLS_GUI_INVOICING.GetString(446)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NOT_TRANSFERRED).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NOT_TRANSFERRED).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NOT_TRANSFERRED).Value = ""
      End If
    End If

    m_sum_transf += DbRow.Value(SQL_COLUMN_AMOUNT)

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_sum_transf = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    Me.Grid.AddRow()
    _row_index = Me.Grid.NumRows - 1

    Me.Grid.Cell(_row_index, GRID_COLUMN_TERMINAL).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    Me.Grid.Cell(_row_index, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(m_sum_transf, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Row(_row_index).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT
        Return

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        GUI_TransferMoney()
        Return

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'First column
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)

    'Second column
    If m_account_filter_type = opt_transfer_to.Text Then
      If m_account <> "" Then
        PrintData.SetFilter(m_account_filter_type & " " & GLB_NLS_GUI_INVOICING.GetString(230), m_account)
      End If
      If m_track_data <> "" Then
        PrintData.SetFilter(m_account_filter_type & " " & GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
      End If
      If m_holder <> "" Then
        PrintData.SetFilter(m_account_filter_type & " " & GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
      End If
      If m_holder_is_vip <> "" Then
        PrintData.SetFilter(m_account_filter_type & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
      End If
    ElseIf m_account_filter_type = opt_trackdata.Text Then
      If m_track_data <> "" Then
        PrintData.SetFilter(m_account_filter_type, m_track_data)
      End If
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    m_account = ""
    m_track_data = ""
    m_holder = ""
    m_holder_is_vip = ""
    m_account_filter_type = opt_all_accounts.Text

    If opt_transfer_to.Checked Then
      m_account_filter_type = opt_transfer_to.Text
      m_account = Me.uc_account_sel1.Account()
      m_no_account = Me.uc_account_sel1.Anon
      m_track_data = Me.uc_account_sel1.TrackData()
      m_holder = Me.uc_account_sel1.Holder()
      m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()
      If m_no_account Then
        m_holder = ""
        m_holder_is_vip = ""
        m_track_data = ""
        m_account = GLB_NLS_GUI_STATISTICS.GetString(314)
      End If
    ElseIf opt_trackdata.Checked And ef_trackdata.TextValue <> "" Then
      m_account_filter_type = opt_trackdata.Text
      m_track_data = Me.ef_trackdata.TextValue
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' REPORTED
      .Column(GRID_COLUMN_REPORTED).Header(0).Text = " "
      .Column(GRID_COLUMN_REPORTED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(250)
      .Column(GRID_COLUMN_REPORTED).Width = 2000
      .Column(GRID_COLUMN_REPORTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = " "
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = " "
      .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(443)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TARGET NUMBER (TRACKDATA)
      .Column(GRID_COLUMN_TRACKDATA).Header(0).Text = " "
      .Column(GRID_COLUMN_TRACKDATA).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(308)
      .Column(GRID_COLUMN_TRACKDATA).Width = 2260
      .Column(GRID_COLUMN_TRACKDATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(98)
      .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT).Width = 1150
      .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' HOLDER NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(98)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = 2735
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      'DDM 16/05/2012: Add column NOT_TRANSFERRED
      ' NOT TRANSFERRED
      .Column(GRID_COLUMN_NOT_TRANSFERRED).Header(0).Text = " "
      .Column(GRID_COLUMN_NOT_TRANSFERRED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(446)
      'RRR 21-11-2013: Not shown in TITO mode
      If WSI.Common.TITO.Utils.IsTitoMode Then
        .Column(GRID_COLUMN_NOT_TRANSFERRED).Width = 0
      Else
        .Column(GRID_COLUMN_NOT_TRANSFERRED).Width = 1600
      End If
      .Column(GRID_COLUMN_NOT_TRANSFERRED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()
    Dim _initial_time As Date

    ' Terminals
    Call Me.uc_pr_list.SetDefaultValues()

    'Dates
    _initial_time = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Value = _initial_time
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)

    'Account
    Me.opt_trackdata.Checked = False
    Me.opt_transfer_to.Checked = False
    Me.opt_all_accounts.Checked = True
    Me.ef_trackdata.TextValue = ""
    Me.ef_trackdata.Enabled = False
    Me.uc_account_sel1.Clear()
    Me.uc_account_sel1.Enabled = False

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues  

  ' PURPOSE: Transfer money to account
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_TransferMoney()

  End Sub ' GUI_TransferMoney

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SENTINEL = 0
    GRID_COLUMN_REPORTED = 1
    GRID_INIT_TERMINAL_DATA = 2
    GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_AMOUNT = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_TRACKDATA = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_ACCOUNT = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_HOLDER_NAME = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_NOT_TRANSFERRED = TERMINAL_DATA_COLUMNS + 6

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 7
  End Sub

#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  Private Sub opt_all_accounts_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_accounts.CheckedChanged
    ef_trackdata.Enabled = False
    uc_account_sel1.Enabled = False
  End Sub

  Private Sub opt_transfer_to_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_transfer_to.CheckedChanged
    ef_trackdata.Enabled = False
    uc_account_sel1.Enabled = True
  End Sub

  Private Sub opt_trackdata_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_trackdata.CheckedChanged
    ef_trackdata.Enabled = True
    uc_account_sel1.Enabled = False
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

End Class 'frm_money_pending_transfer