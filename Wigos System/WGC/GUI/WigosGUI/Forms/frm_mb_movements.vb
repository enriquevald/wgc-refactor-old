'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_mb_movements
' DESCRIPTION:   This screen allows to view mobile bank movements:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all cashier sessions
' AUTHOR:        Agust� Poch
' CREATION DATE: 24-AUG-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-AUG-2010  APB    Initial version
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 19-APR-2013  DMR    Fixed Bug #735: Movements of Mobile Banks: Columns A and B
' 22-JUL-2013  LEM    Overloaded ShowForEdit to filter by date.
'                     Replaced uc_dsl by dtp_from and dtp_to to get correctly initial dates from ShowForEdit
' 01-AGU-2013  DHA    Added filters bank mobile types
'                     Added movements: limit change, session closed, automatic limit change
' 04-MAR-2014  DHA & DLL    Fixed Bug WIG-692: incorrect total amount in column pending
' 02-APR-2014  LEM    Fixed Bug WIG-789: Grid shows empty rows 
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 05-NOV-2014  JBC    Fixed Bug WIG-1642: Cash excess total
' 24-NOV-2014  JPJ    Fixed Bug WIG-1741: "Pending" is accumulated as ShortFall when session hasn't finished
' 12-JAN-2015  OPC    Work Item 143: Added new filter to show or not all users.
' 21-JAN-2015  JPJ    New mobile bank deposit movements (cancellation)
' 28-JAN-2015  SGB    Fixed Bug WIG-1967: Change location (lbl_company_a_name, lbl_company_b_name) if not fit on form
' 30-JAN-2017  RAB    Bug 23490: Mobile bank movements - Error to export to Excel.
' 09-MAR-2017  XGJ    Bug 25519: Mobile bank movements: if not check show movements, the totals are 0
' 11-MAY-2017  LTC    Bug 27277: Mobile Bank Movements Report: Total Pending is not showed correctly
' 31-MAY-2018  DMT    PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12454 Users shown in the combo 'User' in 'Mobile bank movements' form are the incorrect ones.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text

Public Class frm_mb_movements
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents tf_mb_cash_desk_session As GUI_Controls.uc_text_field
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_mobile_bank As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_mb As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_mbs As System.Windows.Forms.RadioButton
  Friend WithEvents chk_details As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_company_a_name As System.Windows.Forms.Label
  Friend WithEvents lbl_company_b_name As System.Windows.Forms.Label
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_mobile_bank_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_mb_type_limit_change As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_type_deposit As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_type_recharge As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_type_close_session As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_type_auto_change_limit As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_type_excess As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_type_shortfall As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mb_show_all_users As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_mb As GUI_Controls.uc_combo
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_mb_movements))
    Me.tf_mb_cash_desk_session = New GUI_Controls.uc_text_field()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_mobile_bank = New System.Windows.Forms.GroupBox()
    Me.chk_mb_show_all_users = New System.Windows.Forms.CheckBox()
    Me.opt_one_mb = New System.Windows.Forms.RadioButton()
    Me.opt_all_mbs = New System.Windows.Forms.RadioButton()
    Me.cmb_mb = New GUI_Controls.uc_combo()
    Me.chk_details = New System.Windows.Forms.CheckBox()
    Me.lbl_company_a_name = New System.Windows.Forms.Label()
    Me.lbl_company_b_name = New System.Windows.Forms.Label()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_mobile_bank_type = New System.Windows.Forms.GroupBox()
    Me.chk_mb_type_shortfall = New System.Windows.Forms.CheckBox()
    Me.chk_mb_type_excess = New System.Windows.Forms.CheckBox()
    Me.chk_mb_type_auto_change_limit = New System.Windows.Forms.CheckBox()
    Me.chk_mb_type_close_session = New System.Windows.Forms.CheckBox()
    Me.chk_mb_type_recharge = New System.Windows.Forms.CheckBox()
    Me.chk_mb_type_deposit = New System.Windows.Forms.CheckBox()
    Me.chk_mb_type_limit_change = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_mobile_bank.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_mobile_bank_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_mobile_bank_type)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.lbl_company_b_name)
    Me.panel_filter.Controls.Add(Me.lbl_company_a_name)
    Me.panel_filter.Controls.Add(Me.chk_details)
    Me.panel_filter.Controls.Add(Me.gb_mobile_bank)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.tf_mb_cash_desk_session)
    Me.panel_filter.Size = New System.Drawing.Size(1211, 203)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_mb_cash_desk_session, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mobile_bank, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_details, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_company_a_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_company_b_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mobile_bank_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 207)
    Me.panel_data.Size = New System.Drawing.Size(1211, 438)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1205, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1205, 4)
    '
    'tf_mb_cash_desk_session
    '
    Me.tf_mb_cash_desk_session.IsReadOnly = True
    Me.tf_mb_cash_desk_session.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_mb_cash_desk_session.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_mb_cash_desk_session.Location = New System.Drawing.Point(6, 143)
    Me.tf_mb_cash_desk_session.Name = "tf_mb_cash_desk_session"
    Me.tf_mb_cash_desk_session.Size = New System.Drawing.Size(524, 24)
    Me.tf_mb_cash_desk_session.SufixText = "Sufix Text"
    Me.tf_mb_cash_desk_session.SufixTextVisible = True
    Me.tf_mb_cash_desk_session.TabIndex = 5
    Me.tf_mb_cash_desk_session.TabStop = False
    Me.tf_mb_cash_desk_session.TextWidth = 60
    Me.tf_mb_cash_desk_session.Value = "xCash Desk Session"
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(749, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 4
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_mobile_bank
    '
    Me.gb_mobile_bank.Controls.Add(Me.chk_mb_show_all_users)
    Me.gb_mobile_bank.Controls.Add(Me.opt_one_mb)
    Me.gb_mobile_bank.Controls.Add(Me.opt_all_mbs)
    Me.gb_mobile_bank.Controls.Add(Me.cmb_mb)
    Me.gb_mobile_bank.Location = New System.Drawing.Point(268, 6)
    Me.gb_mobile_bank.Name = "gb_mobile_bank"
    Me.gb_mobile_bank.Size = New System.Drawing.Size(272, 80)
    Me.gb_mobile_bank.TabIndex = 2
    Me.gb_mobile_bank.TabStop = False
    Me.gb_mobile_bank.Text = "xMobile Bank"
    '
    'chk_mb_show_all_users
    '
    Me.chk_mb_show_all_users.AutoSize = True
    Me.chk_mb_show_all_users.Location = New System.Drawing.Point(80, 49)
    Me.chk_mb_show_all_users.Name = "chk_mb_show_all_users"
    Me.chk_mb_show_all_users.Size = New System.Drawing.Size(78, 17)
    Me.chk_mb_show_all_users.TabIndex = 3
    Me.chk_mb_show_all_users.Text = "xShowAll"
    Me.chk_mb_show_all_users.UseVisualStyleBackColor = True
    '
    'opt_one_mb
    '
    Me.opt_one_mb.Location = New System.Drawing.Point(8, 18)
    Me.opt_one_mb.Name = "opt_one_mb"
    Me.opt_one_mb.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_mb.TabIndex = 0
    Me.opt_one_mb.Text = "xOne"
    '
    'opt_all_mbs
    '
    Me.opt_all_mbs.Location = New System.Drawing.Point(8, 44)
    Me.opt_all_mbs.Name = "opt_all_mbs"
    Me.opt_all_mbs.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_mbs.TabIndex = 2
    Me.opt_all_mbs.Text = "xAll"
    '
    'cmb_mb
    '
    Me.cmb_mb.AllowUnlistedValues = False
    Me.cmb_mb.AutoCompleteMode = False
    Me.cmb_mb.IsReadOnly = False
    Me.cmb_mb.Location = New System.Drawing.Point(80, 18)
    Me.cmb_mb.Name = "cmb_mb"
    Me.cmb_mb.SelectedIndex = -1
    Me.cmb_mb.Size = New System.Drawing.Size(184, 24)
    Me.cmb_mb.SufixText = "Sufix Text"
    Me.cmb_mb.SufixTextVisible = True
    Me.cmb_mb.TabIndex = 1
    Me.cmb_mb.TextCombo = Nothing
    Me.cmb_mb.TextVisible = False
    Me.cmb_mb.TextWidth = 0
    '
    'chk_details
    '
    Me.chk_details.AutoSize = True
    Me.chk_details.Location = New System.Drawing.Point(6, 92)
    Me.chk_details.Name = "chk_details"
    Me.chk_details.Size = New System.Drawing.Size(72, 17)
    Me.chk_details.TabIndex = 1
    Me.chk_details.Text = "xDetails"
    Me.chk_details.UseVisualStyleBackColor = True
    '
    'lbl_company_a_name
    '
    Me.lbl_company_a_name.AutoSize = True
    Me.lbl_company_a_name.Location = New System.Drawing.Point(549, 160)
    Me.lbl_company_a_name.Name = "lbl_company_a_name"
    Me.lbl_company_a_name.Size = New System.Drawing.Size(110, 13)
    Me.lbl_company_a_name.TabIndex = 6
    Me.lbl_company_a_name.Text = "xCompanyAName"
    '
    'lbl_company_b_name
    '
    Me.lbl_company_b_name.AutoSize = True
    Me.lbl_company_b_name.Location = New System.Drawing.Point(549, 178)
    Me.lbl_company_b_name.Name = "lbl_company_b_name"
    Me.lbl_company_b_name.Size = New System.Drawing.Size(110, 13)
    Me.lbl_company_b_name.TabIndex = 7
    Me.lbl_company_b_name.Text = "xCompanyBName"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(256, 80)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 17)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_mobile_bank_type
    '
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_shortfall)
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_excess)
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_auto_change_limit)
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_close_session)
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_recharge)
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_deposit)
    Me.gb_mobile_bank_type.Controls.Add(Me.chk_mb_type_limit_change)
    Me.gb_mobile_bank_type.Location = New System.Drawing.Point(546, 6)
    Me.gb_mobile_bank_type.Name = "gb_mobile_bank_type"
    Me.gb_mobile_bank_type.Size = New System.Drawing.Size(200, 151)
    Me.gb_mobile_bank_type.TabIndex = 3
    Me.gb_mobile_bank_type.TabStop = False
    Me.gb_mobile_bank_type.Text = "xMobile Bank Type"
    '
    'chk_mb_type_shortfall
    '
    Me.chk_mb_type_shortfall.AutoSize = True
    Me.chk_mb_type_shortfall.Location = New System.Drawing.Point(6, 73)
    Me.chk_mb_type_shortfall.Name = "chk_mb_type_shortfall"
    Me.chk_mb_type_shortfall.Size = New System.Drawing.Size(81, 17)
    Me.chk_mb_type_shortfall.TabIndex = 3
    Me.chk_mb_type_shortfall.Text = "xShortfall"
    Me.chk_mb_type_shortfall.UseVisualStyleBackColor = True
    '
    'chk_mb_type_excess
    '
    Me.chk_mb_type_excess.AutoSize = True
    Me.chk_mb_type_excess.Location = New System.Drawing.Point(6, 54)
    Me.chk_mb_type_excess.Name = "chk_mb_type_excess"
    Me.chk_mb_type_excess.Size = New System.Drawing.Size(72, 17)
    Me.chk_mb_type_excess.TabIndex = 2
    Me.chk_mb_type_excess.Text = "xExcess"
    Me.chk_mb_type_excess.UseVisualStyleBackColor = True
    '
    'chk_mb_type_auto_change_limit
    '
    Me.chk_mb_type_auto_change_limit.AutoSize = True
    Me.chk_mb_type_auto_change_limit.Location = New System.Drawing.Point(6, 110)
    Me.chk_mb_type_auto_change_limit.Name = "chk_mb_type_auto_change_limit"
    Me.chk_mb_type_auto_change_limit.Size = New System.Drawing.Size(169, 17)
    Me.chk_mb_type_auto_change_limit.TabIndex = 5
    Me.chk_mb_type_auto_change_limit.Text = "xAutomatic Change Limit"
    Me.chk_mb_type_auto_change_limit.UseVisualStyleBackColor = True
    '
    'chk_mb_type_close_session
    '
    Me.chk_mb_type_close_session.AutoSize = True
    Me.chk_mb_type_close_session.Location = New System.Drawing.Point(6, 128)
    Me.chk_mb_type_close_session.Name = "chk_mb_type_close_session"
    Me.chk_mb_type_close_session.Size = New System.Drawing.Size(113, 17)
    Me.chk_mb_type_close_session.TabIndex = 6
    Me.chk_mb_type_close_session.Text = "xClose Session"
    Me.chk_mb_type_close_session.UseVisualStyleBackColor = True
    '
    'chk_mb_type_recharge
    '
    Me.chk_mb_type_recharge.AutoSize = True
    Me.chk_mb_type_recharge.Location = New System.Drawing.Point(6, 17)
    Me.chk_mb_type_recharge.Name = "chk_mb_type_recharge"
    Me.chk_mb_type_recharge.Size = New System.Drawing.Size(87, 17)
    Me.chk_mb_type_recharge.TabIndex = 0
    Me.chk_mb_type_recharge.Text = "xRecharge"
    Me.chk_mb_type_recharge.UseVisualStyleBackColor = True
    '
    'chk_mb_type_deposit
    '
    Me.chk_mb_type_deposit.AutoSize = True
    Me.chk_mb_type_deposit.Location = New System.Drawing.Point(6, 35)
    Me.chk_mb_type_deposit.Name = "chk_mb_type_deposit"
    Me.chk_mb_type_deposit.Size = New System.Drawing.Size(76, 17)
    Me.chk_mb_type_deposit.TabIndex = 1
    Me.chk_mb_type_deposit.Text = "xDeposit"
    Me.chk_mb_type_deposit.UseVisualStyleBackColor = True
    '
    'chk_mb_type_limit_change
    '
    Me.chk_mb_type_limit_change.AutoSize = True
    Me.chk_mb_type_limit_change.Location = New System.Drawing.Point(6, 92)
    Me.chk_mb_type_limit_change.Name = "chk_mb_type_limit_change"
    Me.chk_mb_type_limit_change.Size = New System.Drawing.Size(108, 17)
    Me.chk_mb_type_limit_change.TabIndex = 4
    Me.chk_mb_type_limit_change.Text = "xLimit Change"
    Me.chk_mb_type_limit_change.UseVisualStyleBackColor = True
    '
    'frm_mb_movements
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1219, 649)
    Me.Name = "frm_mb_movements"
    Me.Text = "frm_mb_movements"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_mobile_bank.ResumeLayout(False)
    Me.gb_mobile_bank.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_mobile_bank_type.ResumeLayout(False)
    Me.gb_mobile_bank_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_SESSION_NAME As Integer = 1
  Private Const SQL_COLUMN_MB_ID As Integer = 2
  Private Const SQL_COLUMN_MB_NAME As Integer = 3
  Private Const SQL_COLUMN_MB_USER_ID As Integer = 4
  Private Const SQL_COLUMN_MBM_TYPE As Integer = 5
  Private Const SQL_COLUMN_DATETIME As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 7
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 8
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 9
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 10
  Private Const SQL_COLUMN_RECHARGES As Integer = 11
  Private Const SQL_COLUMN_DEPOSITS As Integer = 12
  Private Const SQL_COLUMN_CASH_EXCESS As Integer = 13
  Private Const SQL_COLUMN_PENDING As Integer = 14
  Private Const SQL_COLUMN_LOST As Integer = 15
  Private Const SQL_COLUMN_LIMIT As Integer = 16
  Private Const SQL_COLUMN_MB_SPLIT1 As Integer = 17
  Private Const SQL_COLUMN_MB_SPLIT2 As Integer = 18
  Private Const SQL_COLUMN_CASH_SHORTFALL As Integer = 19
  Private Const SQL_COLUMN_UNDO_STATUS As Integer = 20

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SESSION_ID As Integer = 1
  Private Const GRID_COLUMN_SESSION_NAME As Integer = 2
  Private Const GRID_COLUMN_DATETIME As Integer = 3
  Private Const GRID_COLUMN_MB_ID_NAME As Integer = 4
  Private Const GRID_COLUMN_MB_TYPE As Integer = 5
  Private Const GRID_COLUMN_LIMIT As Integer = 6
  Private Const GRID_COLUMN_SPLIT1 As Integer = 7
  Private Const GRID_COLUMN_SPLIT2 As Integer = 8
  Private Const GRID_COLUMN_RECHARGES As Integer = 9
  Private Const GRID_COLUMN_DEPOSITS As Integer = 10
  Private Const GRID_COLUMN_PENDING As Integer = 11
  Private Const GRID_COLUMN_EXCESS_CASH As Integer = 12
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 13
  Private Const GRID_COLUMN_ACCOUNT_ID_NAME As Integer = 14

  Private Const GRID_COLUMNS As Integer = 15
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TERMINAL As Integer = 2600
  Private Const GRID_WIDTH_MOBILE_BANK As Integer = 2900
  Private Const GRID_WIDTH_ACCOUNT As Integer = 3200
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1400
  Private Const GRID_WIDTH_SESSION_NAME As Integer = 3000
  Private Const GRID_WIDTH_MOVEMENT As Integer = 2800

#End Region ' Constants

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_mobile_banks As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_mobile_banks_types As String
  ' Used to signal that Cashier Session filter is being applied: form called from another screen
  Private bl_sessions As Boolean = False
  Private m_session_id As Integer
  Private m_session_name As String
  ' LEM 19-JUL-2013: Initial dates movements filter as DATE
  Private m_init_date_from As Date
  Private m_init_date_to As Date

  ' For Transfer Credit Total row
  Dim total_transfer_credit As Double
  ' RCI 20-DEC-2010: For Deposit and Pending Total row
  Dim total_deposit As Double
  Dim total_pending As Double
  Dim total_lost As Double
  ' RRB 11-OCT-2012: For Empresa A and Empresa B row
  Dim total_split1 As Double
  Dim total_split2 As Double

  ' JBC 23-OCT-2014
  Dim total_excess_cash As Double

  ' SubTotals Mobile Bank
  Dim m_sb_mb_id As String
  Dim m_sb_mb_sesion As String
  Dim m_sb_mb_name As String
  Dim m_sb_mb_limit As Double
  Dim m_sb_mb_pending As Double
  Dim m_sb_mb_lost As Double
  Dim m_sb_mb_deposit As Double
  Dim m_sb_mb_recharge As Double
  Dim m_sb_mb_A As Double
  Dim m_sb_mb_B As Double
  Dim m_sb_mb_excess_cash As Double
  Dim m_sb_mb_shortfall As Double

  ' SubTotals Cashier
  Dim m_sb_cs_id As String
  Dim m_sb_cs_sesion As String
  Dim m_sb_cs_limit As Double
  Dim m_sb_cs_pending As Double
  Dim m_sb_cs_excess_cash As Double
  Dim m_sb_cs_lost As Double
  Dim m_sb_cs_deposit As Double
  Dim m_sb_cs_recharge As Double
  Dim m_sb_cs_A As Double
  Dim m_sb_cs_B As Double

  Dim m_mb_money_is_lost As Boolean
  Dim m_cs_money_is_lost As Boolean

  Dim m_company_b_enabled As Boolean
  Dim m_company_b_has_data As Boolean

  Dim m_selected_mov_types As List(Of Int32)

  Dim m_is_tito_mode As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MOBILE_BANK_MOVEMENTS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_InitControls()
    'Dim _select_mobile_banks_str As String
    Dim _sb_select_mobile_banks_cb As StringBuilder
    Dim _mb_device_active As Boolean

    _sb_select_mobile_banks_cb = New StringBuilder

    _mb_device_active = WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice

    ' = GeneralParam.GetBoolean("Movibank", "Device", False)

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(313)

    Me.IgnoreRowHeightFilterInExcelReport = True

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Mobile Banks
    Me.gb_mobile_bank.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_mb.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_mbs.Text = GLB_NLS_GUI_INVOICING.GetString(219)

    ' Mobile Banks Types
    Me.gb_mobile_bank_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460)
    Me.chk_mb_type_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2462)
    Me.chk_mb_type_deposit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2463)
    Me.chk_mb_type_excess.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670)
    Me.chk_mb_type_limit_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2461)
    Me.chk_mb_type_auto_change_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2465)
    Me.chk_mb_type_close_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2464)
    Me.chk_mb_type_shortfall.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5669)

    m_company_b_enabled = GeneralParam.GetBoolean("Cashier", "Split.B.Enabled")

    m_is_tito_mode = TITO.Utils.IsTitoMode()

    m_selected_mov_types = New List(Of Int32)
    m_selected_mov_types.Capacity = 7 ' update if add new checkbox type 

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

    '31-MAY-2018  DMT    PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12454 Users shown in the combo 'User' in 'Mobile bank movements' form are the incorrect ones.
    _sb_select_mobile_banks_cb.AppendLine("     SELECT DISTINCT MB_ACCOUNT_ID,                                      ")
    _sb_select_mobile_banks_cb.AppendLine("                     CASE")
    _sb_select_mobile_banks_cb.AppendLine("                       WHEN MB_USER_ID IS NULL AND (MB_HOLDER_NAME IS NULL OR MB_HOLDER_NAME = '') ")
    _sb_select_mobile_banks_cb.AppendLine("                     THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - **** ' ")
    _sb_select_mobile_banks_cb.AppendLine("                       WHEN MB_USER_ID IS NULL AND NOT MB_HOLDER_NAME = '' ")
    _sb_select_mobile_banks_cb.AppendLine("                     THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME ")
    _sb_select_mobile_banks_cb.AppendLine("                       ELSE (GU_USERNAME) ")
    _sb_select_mobile_banks_cb.AppendLine("                     END AS MB_NAME    ")
    _sb_select_mobile_banks_cb.AppendLine("     FROM GUI_USERS                                                   ")
    _sb_select_mobile_banks_cb.AppendLine("     LEFT OUTER JOIN   MOBILE_BANKS  ON  MB_USER_ID  =  GU_USER_ID    ")
    _sb_select_mobile_banks_cb.AppendLine("     WHERE GU_BLOCK_REASON = 0 And MB_BLOCKED = 0                     ")
    _sb_select_mobile_banks_cb.AppendLine("     AND MB_USER_ID IS NOT NULL                                       ")
    _sb_select_mobile_banks_cb.AppendLine("     AND   EXISTS (                                                   ")
    _sb_select_mobile_banks_cb.AppendLine("                   SELECT   *                                         ")
    _sb_select_mobile_banks_cb.AppendLine("                   FROM GUI_PROFILE_FORMS                             ")
    _sb_select_mobile_banks_cb.AppendLine("                   WHERE GPF_PROFILE_ID = GU_PROFILE_ID               ")
    _sb_select_mobile_banks_cb.AppendLine("                   AND   GPF_GUI_ID      = " & Convert.ToInt16(ENUM_GUI.WIGOS_GUI) & "  ")
    _sb_select_mobile_banks_cb.AppendLine("                   AND   GPF_FORM_ID     = " & Convert.ToInt16(ENUM_FORM.FORM_MOBIBANK_USER_CONFIG) & " ")
    _sb_select_mobile_banks_cb.AppendLine("                   AND   GPF_READ_PERM   = 1                          ")
    _sb_select_mobile_banks_cb.AppendLine("                  )                                                   ")
    _sb_select_mobile_banks_cb.AppendLine("     AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR & "")
    _sb_select_mobile_banks_cb.AppendLine("     AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX & "")
    _sb_select_mobile_banks_cb.AppendLine("     ORDER BY MB_NAME DESC ")

    Call SetCombo(Me.cmb_mb, _sb_select_mobile_banks_cb.ToString())

    Me.cmb_mb.Enabled = False

    ' Details checkbox
    Me.chk_details.Text = GLB_NLS_GUI_INVOICING.GetString(316)
    Me.chk_details.Checked = True

    ' Show all mobile banks users
    Me.chk_mb_show_all_users.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)
    Me.chk_mb_show_all_users.Enabled = Me.cmb_mb.Enabled

    ' Session Name
    Me.tf_mb_cash_desk_session.Text = GLB_NLS_GUI_INVOICING.GetString(208)

    ' Splits labels
    Me.lbl_company_a_name.Text = "A: " & GeneralParam.GetString("Cashier", "Split.A.CompanyName") & " / " & GeneralParam.GetString("Cashier", "Split.A.Name")
    Me.lbl_company_a_name.Visible = True
    Me.lbl_company_b_name.Text = "B: " & GeneralParam.GetString("Cashier", "Split.B.CompanyName") & " / " & GeneralParam.GetString("Cashier", "Split.B.Name")
    Me.lbl_company_b_name.Visible = m_company_b_enabled

    ' SGB 28/01/2015 Fixed bug 1967: Change location if not fit on form
    If Me.lbl_company_a_name.Size.Width > 500 Or Me.lbl_company_b_name.Size.Width > 500 Then
      Me.lbl_company_a_name.Location = New Point(6, Me.lbl_company_a_name.Location.Y)
      Me.lbl_company_b_name.Location = New Point(6, Me.lbl_company_b_name.Location.Y)
    End If

    uc_account_sel1.Visible = Not m_is_tito_mode

    If MobileBank.GP_GetRegisterShortfallOnDeposit() = False Then
      chk_mb_type_close_session.Location = chk_mb_type_auto_change_limit.Location
      chk_mb_type_auto_change_limit.Location = chk_mb_type_limit_change.Location
      chk_mb_type_limit_change.Location = chk_mb_type_shortfall.Location
      gb_mobile_bank_type.Height -= chk_mb_type_limit_change.Height
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()

    total_transfer_credit = 0
    total_deposit = 0
    total_pending = 0
    total_lost = 0

    total_split1 = 0
    total_split2 = 0

    total_excess_cash = 0

    'm_cashier_pending_total = 0

    If chk_details.Checked Then
      Me.Grid.Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Else
      Me.Grid.Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(348)
    End If

    m_mb_money_is_lost = False

    m_sb_mb_id = -1
    ResetSubTotalMobileBank()

    m_sb_cs_id = -1
    ResetSubTotalCashier()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_from.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder
    Dim _date_from As String = ""
    Dim _date_to As String = ""
    Dim _where_account As String
    Dim _mb_device_active As Boolean

    If Me.dtp_from.Checked Then
      _date_from = GUI_FormatDateDB(dtp_from.Value)
    End If
    If Me.dtp_to.Checked Then
      _date_to = GUI_FormatDateDB(dtp_to.Value)
    End If

    _mb_device_active = WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice

    _where_account = GetSqlWhereAccount()

    _sb = New StringBuilder()

    _sb.AppendLine("   SELECT   A.MBM_CASHIER_SESSION_ID AS SESSION_ID ")
    _sb.AppendLine("          , A.CS_NAME AS SESSION_NAME ")
    _sb.AppendLine("          , A.MBM_MB_ID AS MB_ID ")

    If _mb_device_active Then
      _sb.AppendLine("          , ISNULL(G.GU_USERNAME, C.MB_HOLDER_NAME) AS MB_NAME ")
    Else
      _sb.AppendLine("          , C.MB_HOLDER_NAME AS MB_NAME ")
    End If

    _sb.AppendLine("          , C.MB_USER_ID ")
    _sb.AppendLine("          , A.MBM_TYPE ")
    _sb.AppendLine("          , A.MBM_DATETIME AS MBM_DATETIME ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN A.MBM_TERMINAL_ID ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS TERMINAL_ID ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN A.MBM_TERMINAL_NAME ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS TERMINAL_NAME ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN A.MBM_PLAYER_ACCT_ID ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS ACCOUNT_ID ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN B.AC_HOLDER_NAME ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS ACCOUNT_NAME ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN A.MBM_SUB_AMOUNT ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS RECHARGES ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 2 THEN A.MBM_ADD_AMOUNT ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS DEPOSITS ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 9 THEN A.MBM_ADD_AMOUNT ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS CASH_EXCESS ")
    _sb.AppendLine("          , C.MB_PENDING_CASH AS PENDING ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 8 THEN A.MBM_SUB_AMOUNT ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS LOST ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 0 THEN A.MBM_FINAL_BALANCE ")
    _sb.AppendLine("               WHEN 7 THEN A.MBM_FINAL_BALANCE ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS LIMIT ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN A.MBM_AMOUNT_01 ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS MB_SPLIT1 ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 1 THEN A.MBM_AMOUNT_02 ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS MB_SPLIT2 ")
    _sb.AppendLine("          , CASE A.MBM_TYPE ")
    _sb.AppendLine("               WHEN 11 THEN A.MBM_SUB_AMOUNT ")
    _sb.AppendLine("               ELSE NULL ")
    _sb.AppendLine("            END AS CASH_SHORTFALL ")
    _sb.AppendLine("          , A.MBM_UNDO_STATUS ")
    _sb.AppendLine("     FROM   (   SELECT   MBM_CASHIER_SESSION_ID ")
    _sb.AppendLine("                       , CS_NAME ")
    _sb.AppendLine("                       , MBM_MB_ID ")
    _sb.AppendLine("                       , MBM_DATETIME ")
    _sb.AppendLine("                       , MBM_TERMINAL_ID ")
    _sb.AppendLine("                       , MBM_TERMINAL_NAME ")
    _sb.AppendLine("                       , MBM_PLAYER_ACCT_ID ")
    _sb.AppendLine("                       , MBM_ADD_AMOUNT ")
    _sb.AppendLine("                       , MBM_SUB_AMOUNT ")
    _sb.AppendLine("                       , MBM_AMOUNT_01 ")
    _sb.AppendLine("                       , MBM_AMOUNT_02 ")
    _sb.AppendLine("                       , MBM_FINAL_BALANCE ")
    _sb.AppendLine("                       , MBM_TYPE ")
    _sb.AppendLine("                       , MBM_UNDO_STATUS ")
    _sb.AppendLine("                  FROM   MB_MOVEMENTS ")
    _sb.AppendLine("                       , CASHIER_SESSIONS ")
    _sb.AppendLine("                 WHERE   MBM_TYPE IN ( " & GetSqlWhereMobileBankType() & " ) ")
    _sb.AppendLine("                         AND CS_SESSION_ID = MBM_CASHIER_SESSION_ID ")
    _sb.AppendLine("                         " & GetSqlWhere())
    _sb.AppendLine("             ) A ")
    _sb.AppendLine("         LEFT OUTER JOIN ACCOUNTS B     ON A.MBM_PLAYER_ACCT_ID = B.AC_ACCOUNT_ID ")

    If _mb_device_active Then
      _sb.AppendLine("         LEFT OUTER JOIN MOBILE_BANKS C ON A.MBM_MB_ID          = C.MB_ACCOUNT_ID ")
      _sb.AppendLine("         LEFT JOIN GUI_USERS G ON C.MB_USER_ID = GU_USER_ID")
    Else
      _sb.AppendLine("         INNER JOIN MOBILE_BANKS C ON A.MBM_MB_ID          = C.MB_ACCOUNT_ID")
      '_sb.AppendLine("         AND C.MB_USER_ID IS NULL")
    End If

    If Not String.IsNullOrEmpty(_where_account) Then
      ' account filter only has effect over TransferCredit movements
      _sb.AppendLine("  WHERE (" & _where_account & " AND A.MBM_TYPE = " & MBMovementType.TransferCredit & ")")
      '_sb.AppendLine("     OR A.MBM_TYPE <> " & MBMovementType.TransferCredit)
    End If
    _sb.AppendLine("ORDER BY SESSION_ID, MB_ID, MBM_DATETIME ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Checks out the DB row before adding it to the grid and process counters (& totals rows)
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns>True (the data row can be added) or False (the data row can not be added)</returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim m_add_row As Boolean
    m_add_row = False

    If m_sb_mb_id <> DbRow.Value(SQL_COLUMN_MB_ID) And m_sb_mb_id <> -1 Or _
      m_sb_cs_id <> DbRow.Value(SQL_COLUMN_SESSION_ID) And m_sb_cs_id <> -1 Then
      m_add_row = True
    End If

    If m_sb_cs_id <> DbRow.Value(SQL_COLUMN_SESSION_ID) And m_sb_cs_id <> -1 Then
      m_add_row = True
    End If

    ' XGJ 09-MAR-2017
    If (m_selected_mov_types.Count = 0 OrElse m_selected_mov_types.Contains(DbRow.Value(SQL_COLUMN_MBM_TYPE))) Then
      m_add_row = True
    End If

    ' Not print close session
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 3 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_close_session.Checked) Then
      Return m_add_row
    End If

    ' Not print recharge
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 1 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_recharge.Checked) Then
      Return m_add_row
    End If

    ' Not print deposit
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 2 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_deposit.Checked) Then
      Return m_add_row
    End If

    ' Not print excess
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 9 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_excess.Checked) Then
      Return m_add_row
    End If

    ' Not print shortfall
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 11 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_shortfall.Checked) Then
      Return m_add_row
    End If

    Return m_add_row
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim m_add_row As Boolean
    Dim _current_row_index As Int32

    _current_row_index = RowIndex

    m_add_row = False

    If m_sb_mb_id <> DbRow.Value(SQL_COLUMN_MB_ID) And m_sb_mb_id <> -1 Or _
      m_sb_cs_id <> DbRow.Value(SQL_COLUMN_SESSION_ID) And m_sb_cs_id <> -1 Then
      AddSubTotalMobileBankRow(RowIndex)
      ResetSubTotalMobileBank()
      m_add_row = True
    End If

    If m_sb_cs_id <> DbRow.Value(SQL_COLUMN_SESSION_ID) And m_sb_cs_id <> -1 Then
      If m_add_row Then
        Me.Grid.AddRow()
        RowIndex += 1
      End If
      AddSubTotalCashierRow(RowIndex)
      m_add_row = True
    End If

    ' MB session is close --> Imbalance (pending money) is lost
    ' Lost is not a visible row
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 3 OrElse DbRow.Value(SQL_COLUMN_MBM_TYPE) = 11 Then
      m_cs_money_is_lost = True
      m_mb_money_is_lost = True
    End If

    AcumulateTotals(DbRow)
    AcumulateSubTotalMobileBankRow(DbRow)
    AcumulateSubTotalCashierRow(DbRow)

    If chk_details.Checked AndAlso (m_selected_mov_types.Count = 0 OrElse m_selected_mov_types.Contains(DbRow.Value(SQL_COLUMN_MBM_TYPE))) Then
      If m_add_row Then
        Me.Grid.AddRow()
        RowIndex += 1
      End If
      m_add_row = True
    Else

      Return m_add_row Or _current_row_index <> RowIndex
    End If

    ' Not print close session
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 3 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_close_session.Checked) Then
      Return m_add_row
    End If

    ' Not print recharge
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 1 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_recharge.Checked) Then
      Return m_add_row
    End If

    ' Not print deposit
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 2 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_deposit.Checked) Then
      Return m_add_row
    End If

    ' Not print excess
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 9 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_excess.Checked) Then
      Return m_add_row
    End If

    ' Not print shortfall
    If DbRow.Value(SQL_COLUMN_MBM_TYPE) = 11 And _
        Not ((Not chk_mb_type_limit_change.Checked And _
        Not chk_mb_type_recharge.Checked And _
        Not chk_mb_type_deposit.Checked And _
        Not chk_mb_type_close_session.Checked And _
        Not chk_mb_type_auto_change_limit.Checked) Or _
        chk_mb_type_shortfall.Checked) Then
      Return m_add_row
    End If

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

    ' Session Name
    If Not DbRow.IsNull(SQL_COLUMN_SESSION_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = DbRow.Value(SQL_COLUMN_SESSION_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = ""
    End If

    ' DateTime
    If Not DbRow.IsNull(SQL_COLUMN_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Description
    Select Case DbRow.Value(SQL_COLUMN_MBM_TYPE)
      Case WSI.Common.MBMovementType.ManualChangeLimit
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2461)
      Case WSI.Common.MBMovementType.TransferCredit
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2462)
      Case WSI.Common.MBMovementType.DepositCash
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2463)
      Case WSI.Common.MBMovementType.CloseSession
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2464)
      Case WSI.Common.MBMovementType.AutomaticChangeLimit
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2465)
      Case WSI.Common.MBMovementType.CloseSessionWithCashLost
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2468)
      Case WSI.Common.MBMovementType.CashExcess
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670)
      Case WSI.Common.MBMovementType.CashShortFall
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1343) 'Shorfall
      Case Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = ""
    End Select

    ' Update movement type name if movement operation is undone
    If Not DbRow.IsNull(SQL_COLUMN_UNDO_STATUS) AndAlso DbRow.Value(SQL_COLUMN_UNDO_STATUS) = UndoStatus.UndoOperation Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_TYPE).Value
    End If

    ' MB Id + Name
    If DbRow.IsNull(SQL_COLUMN_MB_USER_ID) Then

      If Not DbRow.IsNull(SQL_COLUMN_MB_ID) Then
        If Not DbRow.IsNull(SQL_COLUMN_MB_NAME) And Not DbRow.Value(SQL_COLUMN_MB_NAME).Equals("") Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID_NAME).Value = DbRow.Value(SQL_COLUMN_MB_ID) & " - " & DbRow.Value(SQL_COLUMN_MB_NAME)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID_NAME).Value = DbRow.Value(SQL_COLUMN_MB_ID) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7773)
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID_NAME).Value = ""
      End If

    Else

      If Not DbRow.IsNull(SQL_COLUMN_MB_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID_NAME).Value = DbRow.Value(SQL_COLUMN_MB_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID_NAME).Value = ""
      End If

    End If
    ' Terminal Name
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = ""
    End If

    ' Account Id + Name
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID) & " - " & DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = ""
    End If

    ' Limit
    If Not DbRow.IsNull(SQL_COLUMN_LIMIT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LIMIT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_LIMIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LIMIT).Value = ""
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_RECHARGES).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT1).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT2).Value = ""

    If Not DbRow.IsNull(SQL_COLUMN_RECHARGES) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RECHARGES).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RECHARGES), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_RECHARGES) And Not DbRow.IsNull(SQL_COLUMN_MB_SPLIT1) And Not DbRow.IsNull(SQL_COLUMN_MB_SPLIT2) Then
      If DbRow.Value(SQL_COLUMN_RECHARGES) = (DbRow.Value(SQL_COLUMN_MB_SPLIT1) + DbRow.Value(SQL_COLUMN_MB_SPLIT2)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT1).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_SPLIT1), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT2).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_SPLIT2), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        If Not m_company_b_enabled AndAlso CDbl(DbRow.Value(SQL_COLUMN_MB_SPLIT2)) > 0 Then
          m_company_b_has_data = True
        End If
      End If
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DEPOSITS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DEPOSITS), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Cash Excess
    If Not DbRow.IsNull(SQL_COLUMN_CASH_EXCESS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EXCESS_CASH).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_EXCESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EXCESS_CASH).Value = ""
    End If

    ' Cash Shorfall
    If Not DbRow.IsNull(SQL_COLUMN_CASH_SHORTFALL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PENDING).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_SHORTFALL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PENDING).Value = ""
    End If

    Return m_add_row

  End Function ' GUI_SetupRow

  ' PURPOSE : Performs some actions after all rows have been painted
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Protected Overrides Sub GUI_AfterLastRow()

    Dim idx_row As Integer

    Call StyleSheetSplitB()
    m_company_b_has_data = False

    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1

    AddSubTotalMobileBankRow(idx_row)

    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1

    AddSubTotalCashierRow(idx_row)

    ' "TOTAL TRANSFER CREDIT" label
    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' "xTotal" 
    ' Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_RECHARGES).Value = GUI_FormatCurrency(total_transfer_credit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Pending
    Me.Grid.Cell(idx_row, GRID_COLUMN_PENDING).Value = GUI_FormatCurrency(total_pending, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Deposit
    Me.Grid.Cell(idx_row, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(total_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    ' Excess
    Me.Grid.Cell(idx_row, GRID_COLUMN_EXCESS_CASH).Value = GUI_FormatCurrency(total_excess_cash, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If total_transfer_credit = Math.Round(total_split1, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) + Math.Round(total_split2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_SPLIT1).Value = GUI_FormatCurrency(total_split1, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_SPLIT2).Value = GUI_FormatCurrency(total_split2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If
    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    If bl_sessions Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(208) & " ", m_session_name)
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_mobile_banks)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460), m_mobile_banks_types)

    If uc_account_sel1.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    End If

    If lbl_company_a_name.Visible Then
      PrintData.SetFilter(Me.lbl_company_a_name.Text.Substring(0, 1), Me.lbl_company_a_name.Text.Substring(3))
    Else
      PrintData.SetFilter("", "", True)
    End If
    If lbl_company_b_name.Visible Then
      PrintData.SetFilter(Me.lbl_company_b_name.Text.Substring(0, 1), Me.lbl_company_b_name.Text.Substring(3))
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.FilterHeaderWidth(2) = 1600
    PrintData.FilterValueWidth(2) = 2000
    PrintData.FilterHeaderWidth(4) = 500
    PrintData.FilterValueWidth(4) = 1800

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line
    PrintData.Settings.Columns(GRID_COLUMN_DATETIME).IsWidthFixed = True
    PrintData.Settings.Columns(GRID_COLUMN_SESSION_NAME).IsWidthFixed = True

    With Me.Grid
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).IsColumnPrintable = False
      .Column(GRID_COLUMN_TERMINAL_NAME).IsColumnPrintable = False
    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(313)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).IsColumnPrintable = True
      .Column(GRID_COLUMN_TERMINAL_NAME).IsColumnPrintable = True
    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_mobile_banks = ""
    m_account = ""
    m_track_data = ""
    m_holder = ""
    m_holder_is_vip = ""
    m_mobile_banks_types = ""

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Mobile Banks
    If Me.opt_all_mbs.Checked Then
      m_mobile_banks = Me.opt_all_mbs.Text
    Else
      m_mobile_banks = Me.cmb_mb.TextValue
    End If

    ' Mobile Banks Types
    If gb_mobile_bank_type.Enabled Then
      If Me.chk_mb_type_recharge.Checked And Me.chk_mb_type_deposit.Checked And _
        Me.chk_mb_type_limit_change.Checked And Me.chk_mb_type_auto_change_limit.Checked And _
        Me.chk_mb_type_close_session.Checked Or _
        Not Me.chk_mb_type_recharge.Checked And Not Me.chk_mb_type_deposit.Checked And _
        Not Me.chk_mb_type_limit_change.Checked And Not Me.chk_mb_type_auto_change_limit.Checked And _
        Not Me.chk_mb_type_close_session.Checked Then
        m_mobile_banks_types = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1671)
      Else
        If Me.chk_mb_type_recharge.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2462)
          m_selected_mov_types.Add(MBMovementType.TransferCredit)
        End If

        If Me.chk_mb_type_deposit.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2463)
          m_selected_mov_types.Add(MBMovementType.DepositCash)
        End If

        If Me.chk_mb_type_limit_change.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2461)
          m_selected_mov_types.Add(MBMovementType.ManualChangeLimit)
        End If

        If Me.chk_mb_type_auto_change_limit.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2465)
          m_selected_mov_types.Add(MBMovementType.AutomaticChangeLimit)
        End If

        If Me.chk_mb_type_close_session.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2464)
          m_selected_mov_types.Add(MBMovementType.CloseSession)
        End If

        If Me.chk_mb_type_excess.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670)
          m_selected_mov_types.Add(MBMovementType.CashExcess)
        End If

        If Me.chk_mb_type_shortfall.Checked Then
          m_mobile_banks_types += ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1343)
          m_selected_mov_types.Add(MBMovementType.CashShortFall)
        End If

        m_mobile_banks_types = m_mobile_banks_types.Substring(2)
      End If
    End If

    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    bl_sessions = False
    m_init_date_from = Nothing
    m_init_date_to = Nothing

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionId:
  '           - SessionName:
  '           - DateFrom: Movements date from
  '           - DateTo: Movements date to
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal SessionId As Integer, _
                         ByVal SessionName As String, _
                         Optional ByVal DateFrom As Date = Nothing, _
                         Optional ByVal DateTo As Date = Nothing)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    bl_sessions = True
    m_session_id = SessionId
    m_session_name = SessionName

    m_init_date_from = DateFrom
    m_init_date_to = DateTo

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  Private Sub StyleSheetSplitB()
    With Me.Grid
      .Column(GRID_COLUMN_SPLIT2).Header(0).Text = "B" ' "xEmpresaB" 
      If (m_company_b_enabled OrElse m_company_b_has_data) AndAlso (Not chk_details.Checked Or chk_mb_type_recharge.Checked Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked)) Then
        .Column(GRID_COLUMN_SPLIT2).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_SPLIT2).Width = 1
      End If
      .Column(GRID_COLUMN_SPLIT2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub

  Private Sub MovementsTypesFilterInit()
    Dim _offset As Int32
    Dim _convert_pending_to_shortfall As Boolean

    AddHandler Me.chk_mb_type_limit_change.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)
    AddHandler Me.chk_mb_type_recharge.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)
    AddHandler Me.chk_mb_type_deposit.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)
    AddHandler Me.chk_mb_type_close_session.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)
    AddHandler Me.chk_mb_type_auto_change_limit.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)
    AddHandler Me.chk_mb_type_excess.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)
    AddHandler Me.chk_mb_type_shortfall.CheckedChanged, New EventHandler(AddressOf chk_mb_type_CheckedChanged)

    If m_is_tito_mode Then
      Me.chk_mb_type_recharge.Checked = True
      Me.chk_mb_type_deposit.Checked = True
      Me.chk_mb_type_limit_change.Checked = False
      Me.chk_mb_type_close_session.Checked = False
      Me.chk_mb_type_auto_change_limit.Checked = False
      Me.chk_mb_type_excess.Checked = False
      Me.chk_mb_type_shortfall.Checked = False

      Me.chk_mb_type_limit_change.Visible = False
      Me.chk_mb_type_close_session.Visible = False
      Me.chk_mb_type_auto_change_limit.Visible = False

      'resize/relocate controls
      _offset = Me.gb_mobile_bank_type.Height - Me.gb_mobile_bank.Height
      Me.gb_mobile_bank_type.Height = Me.gb_mobile_bank.Height
      Me.lbl_company_a_name.Top -= _offset
      Me.lbl_company_b_name.Top -= _offset
      Me.tf_mb_cash_desk_session.Top = chk_details.Bottom
      Me.panel_filter.Height -= _offset
    Else
      Me.chk_mb_type_limit_change.Checked = True
      Me.chk_mb_type_recharge.Checked = True
      Me.chk_mb_type_deposit.Checked = True
      Me.chk_mb_type_close_session.Checked = True
      Me.chk_mb_type_auto_change_limit.Checked = False
      Me.chk_mb_type_excess.Checked = True
      _convert_pending_to_shortfall = MobileBank.GP_GetRegisterShortfallOnDeposit()
      Me.chk_mb_type_shortfall.Visible = _convert_pending_to_shortfall
      Me.chk_mb_type_shortfall.Checked = _convert_pending_to_shortfall
    End If

  End Sub


  Private Sub UpdateSelectedMovTypesList()

    m_selected_mov_types.Clear()

    If Not chk_details.Checked Then
      Return
    End If

    If Me.chk_mb_type_recharge.Checked Then
      m_selected_mov_types.Add(MBMovementType.TransferCredit)
    End If
    If Me.chk_mb_type_deposit.Checked Then
      m_selected_mov_types.Add(MBMovementType.DepositCash)
    End If
    If Me.chk_mb_type_limit_change.Checked Then
      m_selected_mov_types.Add(MBMovementType.ManualChangeLimit)
    End If
    If Me.chk_mb_type_auto_change_limit.Checked Then
      m_selected_mov_types.Add(MBMovementType.AutomaticChangeLimit)
    End If
    If Me.chk_mb_type_close_session.Checked Then
      m_selected_mov_types.Add(MBMovementType.CloseSession)
    End If
    If Me.chk_mb_type_excess.Checked Then
      m_selected_mov_types.Add(MBMovementType.CashExcess)
    End If
    If Me.chk_mb_type_shortfall.Checked Then
      m_selected_mov_types.Add(MBMovementType.CashShortFall)
    End If

    If m_selected_mov_types.Count = m_selected_mov_types.Capacity Then
      m_selected_mov_types.Clear()
    End If

  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_SESSION_ID).Width = 0
      .Column(GRID_COLUMN_SESSION_ID).IsColumnPrintable = False

      ' Session Name
      .Column(GRID_COLUMN_SESSION_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(208)
      .Column(GRID_COLUMN_SESSION_NAME).Width = GRID_WIDTH_SESSION_NAME
      .Column(GRID_COLUMN_SESSION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Concept/Date: Depends on if the movements are shown. It is changed in GUI_BeforeFirstRow().
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Mobile Bank
      .Column(GRID_COLUMN_MB_ID_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(220)
      .Column(GRID_COLUMN_MB_ID_NAME).Width = GRID_WIDTH_MOBILE_BANK
      .Column(GRID_COLUMN_MB_ID_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Mobile Bank Type
      .Column(GRID_COLUMN_MB_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460) ' "xTipo de Movimiento'
      If chk_details.Checked Then
        .Column(GRID_COLUMN_MB_TYPE).Width = GRID_WIDTH_MOVEMENT
      Else
        .Column(GRID_COLUMN_MB_TYPE).Width = 0
      End If
      .Column(GRID_COLUMN_MB_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Deposits
      .Column(GRID_COLUMN_DEPOSITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1342) ' "xDep�sitos" 
      If Not chk_details.Checked Or chk_mb_type_deposit.Checked Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_DEPOSITS).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_DEPOSITS).Width = 0
      End If
      .Column(GRID_COLUMN_DEPOSITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Limit
      .Column(GRID_COLUMN_LIMIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2467) ' "xLimite" 
      If chk_details.Checked And (chk_mb_type_limit_change.Checked Or chk_mb_type_auto_change_limit.Checked) Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_LIMIT).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_LIMIT).Width = 0
      End If
      .Column(GRID_COLUMN_LIMIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Pending
      .Column(GRID_COLUMN_PENDING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1343) ' "xPendientes" 
      If Not chk_details.Checked Or IIf(MobileBank.GP_GetRegisterShortfallOnDeposit, chk_mb_type_shortfall.Checked, chk_mb_type_deposit.Checked) Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_PENDING).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_PENDING).Width = 0
      End If
      .Column(GRID_COLUMN_PENDING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Excess Cash
      .Column(GRID_COLUMN_EXCESS_CASH).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670) ' "Cash excess" 
      If Not chk_details.Checked Or chk_mb_type_excess.Checked Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
         Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
         Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_EXCESS_CASH).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_EXCESS_CASH).Width = 0
      End If
      .Column(GRID_COLUMN_EXCESS_CASH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Recharges
      .Column(GRID_COLUMN_RECHARGES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1341) ' "xRecargas" 
      If Not chk_details.Checked Or chk_mb_type_recharge.Checked Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_RECHARGES).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_RECHARGES).Width = 0
      End If
      .Column(GRID_COLUMN_RECHARGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Split 1
      .Column(GRID_COLUMN_SPLIT1).Header(0).Text = "A" ' "xEmpresaA"
      If Not chk_details.Checked Or chk_mb_type_recharge.Checked Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_SPLIT1).Width = GRID_WIDTH_AMOUNT_SHORT
      Else
        .Column(GRID_COLUMN_SPLIT1).Width = 0
      End If
      .Column(GRID_COLUMN_SPLIT1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      Call StyleSheetSplitB()

      ' Terminal Name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(253)
      If chk_details.Checked And chk_mb_type_recharge.Checked Or _
        (Not chk_mb_type_limit_change.Checked And Not chk_mb_type_recharge.Checked And _
          Not chk_mb_type_deposit.Checked And Not chk_mb_type_close_session.Checked And _
          Not chk_mb_type_auto_change_limit.Checked And chk_details.Checked) Then
        .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_TERMINAL
      Else
        .Column(GRID_COLUMN_TERMINAL_NAME).Width = 0
      End If
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      If Not m_is_tito_mode AndAlso chk_details.Checked And (m_selected_mov_types.Count = 0 OrElse m_selected_mov_types.Contains(MBMovementType.TransferCredit)) Then
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Width = GRID_WIDTH_ACCOUNT
      Else
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Width = 0
      End If
      .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Sortable = False
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    If Me.bl_sessions Then
      Me.tf_mb_cash_desk_session.Visible = True
      Me.tf_mb_cash_desk_session.Value = m_session_name
      If m_init_date_from <> Nothing Then
        dtp_from.Value = m_init_date_from
        dtp_from.Checked = True
      Else
        dtp_from.Value = _initial_time
        dtp_from.Checked = False
      End If
      If m_init_date_to <> Nothing Then
        dtp_to.Value = m_init_date_to
        dtp_to.Checked = True
      Else
        dtp_to.Value = _initial_time.AddDays(1)
        dtp_to.Checked = False
      End If
    Else
      dtp_from.Value = _initial_time
      dtp_from.Checked = True
      dtp_to.Value = _initial_time.AddDays(1)
      dtp_to.Checked = False
      Me.tf_mb_cash_desk_session.Visible = False
      Me.tf_mb_cash_desk_session.Value = ""
    End If

    Me.opt_all_mbs.Checked = True
    Me.cmb_mb.Enabled = False

    Me.chk_details.Checked = True
    Me.chk_mb_show_all_users.Checked = False

    Call MovementsTypesFilterInit()

    Me.uc_account_sel1.InitExtendedQuery(m_is_tito_mode)
        Me.uc_account_sel1.Clear() '  07-JUN-2018  DMT
  End Sub ' SetDefaultValues

  Public Sub AcumulateSubTotalMobileBankRow(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    m_sb_mb_id = DbRow.Value(SQL_COLUMN_MB_ID)
    m_sb_mb_sesion = DbRow.Value(SQL_COLUMN_SESSION_NAME)

    If DbRow.IsNull(SQL_COLUMN_MB_USER_ID) Then
      If Not DbRow.IsNull(SQL_COLUMN_MB_ID) Then
        If Not DbRow.IsNull(SQL_COLUMN_MB_NAME) And Not DbRow.Value(SQL_COLUMN_MB_NAME).Equals("") Then
          m_sb_mb_name = DbRow.Value(SQL_COLUMN_MB_ID) & " - " & DbRow.Value(SQL_COLUMN_MB_NAME)
        Else
          m_sb_mb_name = DbRow.Value(SQL_COLUMN_MB_ID) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7773)
        End If
      Else
        m_sb_mb_name = ""
      End If
    Else
      If Not DbRow.IsNull(SQL_COLUMN_MB_NAME) Then
        m_sb_mb_name = DbRow.Value(SQL_COLUMN_MB_NAME)
      Else
        m_sb_mb_name = ""
      End If
    End If

    m_sb_mb_limit += IIf(DbRow.Value(SQL_COLUMN_LIMIT) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_LIMIT))
    m_sb_mb_shortfall += IIf(DbRow.Value(SQL_COLUMN_CASH_SHORTFALL) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_CASH_SHORTFALL))
    m_sb_mb_lost += IIf(DbRow.Value(SQL_COLUMN_LOST) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_LOST))
    m_sb_mb_deposit += IIf(DbRow.Value(SQL_COLUMN_DEPOSITS) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_DEPOSITS))
    m_sb_mb_recharge += IIf(DbRow.Value(SQL_COLUMN_RECHARGES) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_RECHARGES))
    m_sb_mb_A += IIf(DbRow.Value(SQL_COLUMN_MB_SPLIT1) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_MB_SPLIT1))
    m_sb_mb_B += IIf(DbRow.Value(SQL_COLUMN_MB_SPLIT2) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_MB_SPLIT2))
    m_sb_mb_excess_cash += IIf(DbRow.Value(SQL_COLUMN_CASH_EXCESS) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_CASH_EXCESS))

    ' LTC 2017-MAY-11
    'Pending calculated when reharge is selected
    If m_sb_mb_recharge > 0 Then
      m_sb_mb_pending = m_sb_mb_recharge - m_sb_mb_deposit + m_sb_mb_excess_cash
    End If

    'Shortfall is taken if recharge column is not selected 
    If m_sb_mb_pending <= 0 Then
      If m_sb_mb_shortfall > 0 Then
        m_sb_mb_pending = m_sb_mb_shortfall
      Else
        m_sb_mb_pending = 0
      End If
    Else
      If m_sb_mb_shortfall > m_sb_mb_pending Then
        m_sb_mb_pending = m_sb_mb_shortfall
      End If
    End If

  End Sub

  Public Sub AcumulateTotals(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    If Not DbRow.IsNull(SQL_COLUMN_RECHARGES) Then
      total_transfer_credit += DbRow.Value(SQL_COLUMN_RECHARGES)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_SPLIT1) Then
      total_split1 += DbRow.Value(SQL_COLUMN_MB_SPLIT1)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_MB_SPLIT2) Then
      total_split2 += DbRow.Value(SQL_COLUMN_MB_SPLIT2)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DEPOSITS) Then
      total_deposit += DbRow.Value(SQL_COLUMN_DEPOSITS)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_LOST) Then
      total_lost += DbRow.Value(SQL_COLUMN_LOST)
    End If

  End Sub

  Public Sub ResetSubTotalMobileBank()
    m_sb_mb_sesion = ""
    m_sb_mb_name = ""
    m_sb_mb_limit = 0
    m_sb_mb_pending = 0
    m_sb_mb_lost = 0
    m_sb_mb_deposit = 0
    m_sb_mb_recharge = 0
    m_sb_mb_A = 0
    m_sb_mb_B = 0
    m_sb_mb_excess_cash = 0
    m_sb_mb_shortfall = 0
    m_mb_money_is_lost = False
  End Sub


  ' PURPOSE: Add subtotal row mobile bank
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String
  Private Sub AddSubTotalMobileBankRow(ByVal RowIndex As Integer)

    ' Set color
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = m_sb_mb_id

    ' Session Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = m_sb_mb_sesion

    ' DateTime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1345)

    ' MB Id + Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_ID_NAME).Value = m_sb_mb_name

    ' Pending
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PENDING).Value = GUI_FormatCurrency(m_sb_mb_pending, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Deposits
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(m_sb_mb_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'End If

    ' Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RECHARGES).Value = GUI_FormatCurrency(m_sb_mb_recharge, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Slip 1 - A
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT1).Value = GUI_FormatCurrency(m_sb_mb_A, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Slip 2 - B
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT2).Value = GUI_FormatCurrency(m_sb_mb_B, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Excess cash
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXCESS_CASH).Value = GUI_FormatCurrency(m_sb_mb_excess_cash, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    m_sb_cs_pending += m_sb_mb_pending
    total_excess_cash += m_sb_mb_excess_cash
    total_pending += m_sb_mb_pending
  End Sub

  Public Sub AcumulateSubTotalCashierRow(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    ' Set the new subtotal
    If m_sb_cs_id <> DbRow.Value(SQL_COLUMN_SESSION_ID) And m_sb_cs_id <> -1 Then

      ResetSubTotalCashier()
    End If

    m_sb_cs_id = DbRow.Value(SQL_COLUMN_SESSION_ID)
    m_sb_cs_sesion = DbRow.Value(SQL_COLUMN_SESSION_NAME)
    m_sb_cs_limit += IIf(DbRow.Value(SQL_COLUMN_LIMIT) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_LIMIT))
    m_sb_cs_lost += IIf(DbRow.Value(SQL_COLUMN_LOST) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_LOST))
    m_sb_cs_deposit += IIf(DbRow.Value(SQL_COLUMN_DEPOSITS) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_DEPOSITS))
    m_sb_cs_recharge += IIf(DbRow.Value(SQL_COLUMN_RECHARGES) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_RECHARGES))
    m_sb_cs_A += IIf(DbRow.Value(SQL_COLUMN_MB_SPLIT1) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_MB_SPLIT1))
    m_sb_cs_B += IIf(DbRow.Value(SQL_COLUMN_MB_SPLIT2) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_MB_SPLIT2))
    m_sb_cs_excess_cash += IIf(DbRow.Value(SQL_COLUMN_CASH_EXCESS) Is DBNull.Value, 0, DbRow.Value(SQL_COLUMN_CASH_EXCESS))

  End Sub

  Public Sub ResetSubTotalCashier()
    m_sb_cs_sesion = ""
    m_sb_cs_limit = 0
    m_sb_cs_pending = 0
    m_sb_cs_excess_cash = 0
    m_sb_cs_lost = 0
    m_sb_cs_deposit = 0
    m_sb_cs_recharge = 0
    m_sb_cs_A = 0
    m_sb_cs_B = 0
    m_cs_money_is_lost = False
  End Sub


  Private Sub AddSubTotalCashierRow(ByVal RowIndex As Integer)

    ' Set color
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = m_sb_cs_id

    ' Session Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = m_sb_cs_sesion

    ' DateTime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1339)

    ' Pending
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PENDING).Value = GUI_FormatCurrency(m_sb_cs_pending, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Deposits
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(m_sb_cs_deposit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'End If

    ' Recharges
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RECHARGES).Value = GUI_FormatCurrency(m_sb_cs_recharge, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Slip 1 - A
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT1).Value = GUI_FormatCurrency(m_sb_cs_A, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Slip 2 - B
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPLIT2).Value = GUI_FormatCurrency(m_sb_cs_B, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Excess Cash
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXCESS_CASH).Value = GUI_FormatCurrency(m_sb_cs_excess_cash, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

  End Sub

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  ' NOTES:
  '     - Can't filter by uc_account_sel1 when IsCashInOperation is FALSE
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _str_where_account As String = ""
    Dim _date_from As String = ""
    Dim _date_to As String = ""

    If Me.dtp_from.Checked Then
      _date_from = GUI_FormatDateDB(dtp_from.Value)
    End If
    If Me.dtp_to.Checked Then
      _date_to = GUI_FormatDateDB(dtp_to.Value)
    End If

    ' Filter Dates
    _str_where = WSI.Common.Misc.DateSQLFilter("MBM_DATETIME", _date_from, _date_to, False)

    ' Filter Session Id
    If Me.bl_sessions = True Then
      _str_where = _str_where & " AND (MBM_CASHIER_SESSION_ID = " & m_session_id & ") "
    End If

    ' Filter Mobile Bank
    If Me.opt_one_mb.Checked = True Then
      _str_where = _str_where & " AND (MBM_MB_ID = " & Me.cmb_mb.Value & ") "
    End If

    _str_where = _str_where & " AND (MBM_MB_ID IN (SELECT MB_ACCOUNT_ID FROM MOBILE_BANKS WHERE MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR & " AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX & ") ) "

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Build the variable part of the WHERE clause for filter account
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  ' NOTES:
  '     - Can't filter by uc_account_sel1 when IsCashInOperation is FALSE
  Private Function GetSqlWhereAccount() As String
    Dim _str_where_account As String

    _str_where_account = ""

    If Not m_is_tito_mode Then
      ' Avoid account filter on TITOMode
      _str_where_account &= Me.uc_account_sel1.GetFilterSQL()
    End If

    Return _str_where_account
  End Function ' GetSqlWhereAccount

  ' PURPOSE: Build the variable part of the WHERE clause for filter mobile bank type
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  ' NOTES:
  '     - Can't filter by uc_account_sel1 when IsCashInOperation is FALSE
  Private Function GetSqlWhereMobileBankType() As String

    Dim _str_where As String

    _str_where = String.Format(", {0}, {1}, {2}, {3}, {4}, {5}", Convert.ToInt32(WSI.Common.MBMovementType.TransferCredit), _
                                                                 Convert.ToInt32(WSI.Common.MBMovementType.DepositCash), _
                                                                 Convert.ToInt32(WSI.Common.MBMovementType.CloseSession), _
                                                                 Convert.ToInt32(WSI.Common.MBMovementType.CashExcess), _
                                                                 Convert.ToInt32(WSI.Common.MBMovementType.CashShortFall), _
                                                                 Convert.ToInt32(WSI.Common.MBMovementType.CloseSessionWithCashLost))

    If Not chk_mb_type_limit_change.Checked And _
      Not chk_mb_type_recharge.Checked And _
      Not chk_mb_type_deposit.Checked And _
      Not chk_mb_type_close_session.Checked And _
      Not chk_mb_type_auto_change_limit.Checked Then

      _str_where += String.Format(", {0}, {1}", Convert.ToInt32(WSI.Common.MBMovementType.ManualChangeLimit), _
                                                Convert.ToInt32(WSI.Common.MBMovementType.AutomaticChangeLimit))
      Return _str_where.Remove(0, 1)

    End If

    If chk_mb_type_limit_change.Checked Then
      _str_where += ", " & Convert.ToInt32(WSI.Common.MBMovementType.ManualChangeLimit)
    End If

    If chk_mb_type_auto_change_limit.Checked Then
      _str_where += ", " & Convert.ToInt32(WSI.Common.MBMovementType.AutomaticChangeLimit)
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = _str_where.Remove(0, 1)
    End If
    Return _str_where
  End Function ' GetSqlWhereMobileBankType

#End Region ' Private Functions

#Region " Events "

  Private Sub opt_one_mb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_mb.Click
    Me.cmb_mb.Enabled = True
    Me.chk_mb_show_all_users.Enabled = Me.cmb_mb.Enabled
  End Sub

  Private Sub opt_all_mbs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_mbs.Click
    Me.cmb_mb.Enabled = False
    Me.chk_mb_show_all_users.Enabled = Me.cmb_mb.Enabled
  End Sub

  Private Sub chk_mb_show_all_users_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_mb_show_all_users.CheckedChanged
    Dim _sb_filter_users As StringBuilder

    _sb_filter_users = New StringBuilder()

    '31-MAY-2018  DMT    PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12454 Users shown in the combo 'User' in 'Mobile bank movements' form are the incorrect ones.
    If Me.chk_mb_show_all_users.Checked Then

      _sb_filter_users.AppendLine("     SELECT DISTINCT MB_ACCOUNT_ID,                                                                ")
      _sb_filter_users.AppendLine("                     CASE                                                                          ")
      _sb_filter_users.AppendLine("                       WHEN MB_USER_ID IS NULL AND (MB_HOLDER_NAME IS NULL OR MB_HOLDER_NAME = '') ")
      _sb_filter_users.AppendLine("                     THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - **** '                             ")
      _sb_filter_users.AppendLine("                       WHEN MB_USER_ID IS NULL AND NOT MB_HOLDER_NAME = ''                         ")
      _sb_filter_users.AppendLine("                     THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME                 ")
      _sb_filter_users.AppendLine("                       ELSE (GU_USERNAME)                                                          ")
      _sb_filter_users.AppendLine("                     END AS MB_NAME                                                                ")
      _sb_filter_users.AppendLine("     FROM GUI_USERS                                                                                ")
      _sb_filter_users.AppendLine("       LEFT OUTER JOIN   MOBILE_BANKS  ON  MB_USER_ID  =  GU_USER_ID                               ")
      _sb_filter_users.AppendLine("     WHERE                                                                                         ")
      _sb_filter_users.AppendLine("        EXISTS (                                                                                   ")
      _sb_filter_users.AppendLine("                   SELECT   *                                                                      ")
      _sb_filter_users.AppendLine("                   FROM GUI_PROFILE_FORMS                                                          ")
      _sb_filter_users.AppendLine("                   WHERE GPF_PROFILE_ID = GU_PROFILE_ID                                            ")
      _sb_filter_users.AppendLine("                   AND   GPF_GUI_ID      = " & Convert.ToInt16(ENUM_GUI.WIGOS_GUI) & "             ")
      _sb_filter_users.AppendLine("                   AND   GPF_FORM_ID     = " & Convert.ToInt16(ENUM_FORM.FORM_MOBIBANK_USER_CONFIG) & "  ")
      _sb_filter_users.AppendLine("                   AND   GPF_READ_PERM   = 1                                                             ")
      _sb_filter_users.AppendLine("                  )                                                                                      ")
      _sb_filter_users.AppendLine("     AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR & "                                 ")
      _sb_filter_users.AppendLine("     AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX & "                                 ")
      _sb_filter_users.AppendLine("     ORDER BY MB_NAME DESC                                                                               ")

      Call SetCombo(Me.cmb_mb, _sb_filter_users.ToString())

      Dim _sb_filter As StringBuilder
      _sb_filter = New StringBuilder()
      _sb_filter.AppendLine("       SELECT MB_ACCOUNT_ID,                                                               ")
      _sb_filter.AppendLine("           CASE                                                                            ")
      _sb_filter.AppendLine("             WHEN MB_USER_ID IS NULL AND (MB_HOLDER_NAME IS NULL OR MB_HOLDER_NAME = '')   ")
      _sb_filter.AppendLine("           THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - **** '                               ")
      _sb_filter.AppendLine("             WHEN MB_USER_ID IS NULL AND NOT MB_HOLDER_NAME = ''                           ")
      _sb_filter.AppendLine("           THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME                   ")
      _sb_filter.AppendLine("             ELSE (                                                                        ")
      _sb_filter.AppendLine("               GU_USERNAME                                                                 ")
      _sb_filter.AppendLine("                  )                                                                        ")
      _sb_filter.AppendLine("           END AS MB_NAME                                                                  ")
      _sb_filter.AppendLine("       FROM MOBILE_BANKS                                                                   ")
      _sb_filter.AppendLine("         LEFT JOIN GUI_USERS ON GU_USER_ID = MB_USER_ID                                    ")
      _sb_filter.AppendLine("       WHERE MB_USER_ID IS NULL                                                            ")
      _sb_filter.AppendLine("       AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR & "                 ")
      _sb_filter.AppendLine("       AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX & "                 ")
      _sb_filter.AppendLine(" ORDER BY MB_NAME DESC                                                                     ")

      Dim tabla As DataTable
      tabla = GUI_GetTableUsingSQL(_sb_filter.ToString(), Integer.MaxValue)

      If Me.cmb_mb.Count <= 0 Then
        Me.cmb_mb.Add(-1, String.Empty)
      End If

      Me.cmb_mb.Add(tabla)

    Else
      _sb_filter_users.AppendLine("     SELECT DISTINCT MB_ACCOUNT_ID,                                      ")
      _sb_filter_users.AppendLine("                     CASE")
      _sb_filter_users.AppendLine("                       WHEN MB_USER_ID IS NULL AND (MB_HOLDER_NAME IS NULL OR MB_HOLDER_NAME = '') ")
      _sb_filter_users.AppendLine("                     THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - **** ' ")
      _sb_filter_users.AppendLine("                       WHEN MB_USER_ID IS NULL AND NOT MB_HOLDER_NAME = '' ")
      _sb_filter_users.AppendLine("                     THEN CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME ")
      _sb_filter_users.AppendLine("                       ELSE (GU_USERNAME) ")
      _sb_filter_users.AppendLine("                     END AS MB_NAME    ")
      _sb_filter_users.AppendLine("     FROM GUI_USERS                                                   ")
      _sb_filter_users.AppendLine("     LEFT OUTER JOIN   MOBILE_BANKS  ON  MB_USER_ID  =  GU_USER_ID    ")
      _sb_filter_users.AppendLine("     WHERE GU_BLOCK_REASON = 0 And MB_BLOCKED = 0                     ")
      _sb_filter_users.AppendLine("     AND MB_USER_ID IS NOT NULL                                       ")
      _sb_filter_users.AppendLine("     AND   EXISTS (                                                   ")
      _sb_filter_users.AppendLine("                   SELECT   *                                         ")
      _sb_filter_users.AppendLine("                   FROM GUI_PROFILE_FORMS                             ")
      _sb_filter_users.AppendLine("                   WHERE GPF_PROFILE_ID = GU_PROFILE_ID               ")
      _sb_filter_users.AppendLine("                   AND   GPF_GUI_ID      = " & Convert.ToInt16(ENUM_GUI.WIGOS_GUI) & "  ")
      _sb_filter_users.AppendLine("                   AND   GPF_FORM_ID     = " & Convert.ToInt16(ENUM_FORM.FORM_MOBIBANK_USER_CONFIG) & " ")
      _sb_filter_users.AppendLine("                   AND   GPF_READ_PERM   = 1                          ")
      _sb_filter_users.AppendLine("                  )                                                   ")
      _sb_filter_users.AppendLine("     AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR & "")
      _sb_filter_users.AppendLine("     AND MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX & "")
      _sb_filter_users.AppendLine("     ORDER BY MB_NAME DESC ")

      Call SetCombo(Me.cmb_mb, _sb_filter_users.ToString())

    End If

    For Each _row As DataRow In Me.cmb_mb.Table.Rows
      If _row.ItemArray(1).ToString.Contains("****") Then
        _row(1) = _row(1).ToString.Replace("****", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7773))
      End If
    Next

  End Sub ' chk_mb_show_all_users_CheckedChanged

#End Region ' Events

  Private Sub chk_mb_type_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Call UpdateSelectedMovTypesList()

    uc_account_sel1.Enabled = Not chk_details.Checked OrElse chk_mb_type_recharge.Checked OrElse m_selected_mov_types.Count = 0

  End Sub

  Private Sub chk_details_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_details.CheckedChanged
    gb_mobile_bank_type.Enabled = chk_details.Checked

    If Not chk_details.Checked Then
      m_selected_mov_types.Clear()
    Else
      Call UpdateSelectedMovTypesList()
    End If

    uc_account_sel1.Enabled = Not chk_details.Checked OrElse chk_mb_type_recharge.Checked OrElse m_selected_mov_types.Count = 0

  End Sub

End Class
