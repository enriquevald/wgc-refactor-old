Option Explicit On
Imports GUI_Controls
Imports GUI_CommonMisc
Imports System.Security.Cryptography
Imports System.Text

Public Class frm_charts

#Region "Members"

  Private m_uri As String

#End Region

#Region "Private Functions"

  Private Sub InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5717)

    If Not String.IsNullOrEmpty(Me.m_uri) Then
      Me.web_browser.Refresh(WebBrowserRefreshOption.Completely)
      Me.web_browser.Navigate(m_uri)
    End If

  End Sub

  Private Function GetEncrypt(ByVal ValToEncrypt As String, ByVal KeyIndex As Int32) As String

    Dim _text_to_encrypt As String
    Dim _secretKey As String
    _secretKey = "0014587569WSIlayout54654787513213200054"

    _text_to_encrypt = ValToEncrypt & _secretKey

    Dim _hasher As New MD5CryptoServiceProvider()
    Dim _encoder As New UTF8Encoding()

    Dim _contentToSignData As Byte() = _encoder.GetBytes(_text_to_encrypt)
    Dim _signatureData As Byte() = _hasher.ComputeHash(_contentToSignData)
    Dim _signatureString As String = Convert.ToBase64String(_signatureData)

    Return _signatureString

  End Function

#End Region

#Region "Handlers"

  Private Sub frm_charts_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged

    Me.web_browser.Size = New Size(Me.Width - 16, Me.Height - 38)

  End Sub

  Private Sub frm_charts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    If String.IsNullOrEmpty(Me.m_uri) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5729), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Me.Close()
    End If

  End Sub

#End Region

#Region "Constructors"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  Public Sub New(ByVal Uri As String, ByVal LoginNeeded As Boolean)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    Me.m_uri = Uri

    If Not String.IsNullOrEmpty(Me.m_uri) Then

      If LoginNeeded Then
        Me.m_uri = String.Format(Me.m_uri + "/Login.aspx?Username={0}&Signature={1}", CurrentUser.Name, Me.GetEncrypt(CurrentUser.Name, 1))
      End If

    End If

    Me.InitControls()

  End Sub

#End Region

End Class