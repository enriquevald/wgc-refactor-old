<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_amounts
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub


  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_currencies = New System.Windows.Forms.GroupBox()
    Me.dg_currencies = New GUI_Controls.uc_grid()
    Me.gb_bills = New System.Windows.Forms.GroupBox()
    Me.btn_add = New GUI_Controls.uc_button()
    Me.btn_del = New GUI_Controls.uc_button()
    Me.dg_bills = New GUI_Controls.uc_grid()
    Me.Uc_grid1 = New GUI_Controls.uc_grid()
    Me.Uc_button1 = New GUI_Controls.uc_button()
    Me.Uc_button2 = New GUI_Controls.uc_button()
    Me.Uc_grid2 = New GUI_Controls.uc_grid()
    Me.Uc_button3 = New GUI_Controls.uc_button()
    Me.Uc_button4 = New GUI_Controls.uc_button()
    Me.Uc_grid3 = New GUI_Controls.uc_grid()
    Me.panel_data.SuspendLayout()
    Me.gb_currencies.SuspendLayout()
    Me.gb_bills.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.gb_bills)
    Me.panel_data.Controls.Add(Me.gb_currencies)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Size = New System.Drawing.Size(398, 419)
    '
    'gb_currencies
    '
    Me.gb_currencies.Controls.Add(Me.dg_currencies)
    Me.gb_currencies.Location = New System.Drawing.Point(16, 3)
    Me.gb_currencies.Name = "gb_currencies"
    Me.gb_currencies.Size = New System.Drawing.Size(369, 134)
    Me.gb_currencies.TabIndex = 0
    Me.gb_currencies.TabStop = False
    Me.gb_currencies.Text = "Divisas"
    '
    'dg_currencies
    '
    Me.dg_currencies.CurrentCol = -1
    Me.dg_currencies.CurrentRow = -1
    Me.dg_currencies.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_currencies.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_currencies.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_currencies.Location = New System.Drawing.Point(15, 20)
    Me.dg_currencies.Name = "dg_currencies"
    Me.dg_currencies.PanelRightVisible = False
    Me.dg_currencies.Redraw = True
    Me.dg_currencies.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_currencies.Size = New System.Drawing.Size(343, 101)
    Me.dg_currencies.Sortable = False
    Me.dg_currencies.SortAscending = True
    Me.dg_currencies.SortByCol = 0
    Me.dg_currencies.TabIndex = 1
    Me.dg_currencies.ToolTipped = True
    Me.dg_currencies.TopRow = -2
    '
    'gb_bills
    '
    Me.gb_bills.Controls.Add(Me.btn_add)
    Me.gb_bills.Controls.Add(Me.btn_del)
    Me.gb_bills.Controls.Add(Me.dg_bills)
    Me.gb_bills.Location = New System.Drawing.Point(16, 143)
    Me.gb_bills.Name = "gb_bills"
    Me.gb_bills.Size = New System.Drawing.Size(369, 263)
    Me.gb_bills.TabIndex = 0
    Me.gb_bills.TabStop = False
    Me.gb_bills.Text = "xBills"
    '
    'btn_add
    '
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(46, 236)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 3
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_del
    '
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(273, 236)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 4
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'dg_bills
    '
    Me.dg_bills.CurrentCol = -1
    Me.dg_bills.CurrentRow = -1
    Me.dg_bills.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_bills.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_bills.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_bills.Location = New System.Drawing.Point(15, 20)
    Me.dg_bills.Name = "dg_bills"
    Me.dg_bills.PanelRightVisible = False
    Me.dg_bills.Redraw = True
    Me.dg_bills.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_bills.Size = New System.Drawing.Size(343, 210)
    Me.dg_bills.Sortable = False
    Me.dg_bills.SortAscending = True
    Me.dg_bills.SortByCol = 0
    Me.dg_bills.TabIndex = 2
    Me.dg_bills.ToolTipped = True
    Me.dg_bills.TopRow = -2
    '
    'Uc_grid1
    '
    Me.Uc_grid1.CurrentCol = -1
    Me.Uc_grid1.CurrentRow = -1
    Me.Uc_grid1.EditableCellBackColor = System.Drawing.Color.Empty
    Me.Uc_grid1.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.Uc_grid1.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.Uc_grid1.Location = New System.Drawing.Point(2, 13)
    Me.Uc_grid1.Name = "Uc_grid1"
    Me.Uc_grid1.PanelRightVisible = False
    Me.Uc_grid1.Redraw = True
    Me.Uc_grid1.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.Uc_grid1.Size = New System.Drawing.Size(340, 87)
    Me.Uc_grid1.Sortable = False
    Me.Uc_grid1.SortAscending = True
    Me.Uc_grid1.SortByCol = 0
    Me.Uc_grid1.TabIndex = 0
    Me.Uc_grid1.TabStop = False
    Me.Uc_grid1.ToolTipped = True
    Me.Uc_grid1.TopRow = -2
    '
    'Uc_button1
    '
    Me.Uc_button1.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button1.Location = New System.Drawing.Point(65, 203)
    Me.Uc_button1.Name = "Uc_button1"
    Me.Uc_button1.Size = New System.Drawing.Size(50, 21)
    Me.Uc_button1.TabIndex = 0
    Me.Uc_button1.TabStop = False
    Me.Uc_button1.ToolTipped = False
    Me.Uc_button1.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'Uc_button2
    '
    Me.Uc_button2.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button2.Location = New System.Drawing.Point(231, 203)
    Me.Uc_button2.Name = "Uc_button2"
    Me.Uc_button2.Size = New System.Drawing.Size(50, 21)
    Me.Uc_button2.TabIndex = 0
    Me.Uc_button2.TabStop = False
    Me.Uc_button2.ToolTipped = False
    Me.Uc_button2.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'Uc_grid2
    '
    Me.Uc_grid2.CurrentCol = -1
    Me.Uc_grid2.CurrentRow = -1
    Me.Uc_grid2.Dock = System.Windows.Forms.DockStyle.Top
    Me.Uc_grid2.EditableCellBackColor = System.Drawing.Color.Empty
    Me.Uc_grid2.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.Uc_grid2.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.Uc_grid2.Location = New System.Drawing.Point(3, 17)
    Me.Uc_grid2.Name = "Uc_grid2"
    Me.Uc_grid2.PanelRightVisible = False
    Me.Uc_grid2.Redraw = True
    Me.Uc_grid2.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.Uc_grid2.Size = New System.Drawing.Size(340, 169)
    Me.Uc_grid2.Sortable = False
    Me.Uc_grid2.SortAscending = True
    Me.Uc_grid2.SortByCol = 0
    Me.Uc_grid2.TabIndex = 0
    Me.Uc_grid2.TabStop = False
    Me.Uc_grid2.ToolTipped = True
    Me.Uc_grid2.TopRow = -2
    '
    'Uc_button3
    '
    Me.Uc_button3.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button3.Location = New System.Drawing.Point(65, 203)
    Me.Uc_button3.Name = "Uc_button3"
    Me.Uc_button3.Size = New System.Drawing.Size(50, 21)
    Me.Uc_button3.TabIndex = 0
    Me.Uc_button3.TabStop = False
    Me.Uc_button3.ToolTipped = False
    Me.Uc_button3.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'Uc_button4
    '
    Me.Uc_button4.DialogResult = System.Windows.Forms.DialogResult.None
    Me.Uc_button4.Location = New System.Drawing.Point(231, 203)
    Me.Uc_button4.Name = "Uc_button4"
    Me.Uc_button4.Size = New System.Drawing.Size(50, 21)
    Me.Uc_button4.TabIndex = 0
    Me.Uc_button4.TabStop = False
    Me.Uc_button4.ToolTipped = False
    Me.Uc_button4.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'Uc_grid3
    '
    Me.Uc_grid3.CurrentCol = -1
    Me.Uc_grid3.CurrentRow = -1
    Me.Uc_grid3.Dock = System.Windows.Forms.DockStyle.Top
    Me.Uc_grid3.EditableCellBackColor = System.Drawing.Color.Empty
    Me.Uc_grid3.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.Uc_grid3.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.Uc_grid3.Location = New System.Drawing.Point(3, 16)
    Me.Uc_grid3.Name = "Uc_grid3"
    Me.Uc_grid3.PanelRightVisible = False
    Me.Uc_grid3.Redraw = True
    Me.Uc_grid3.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.Uc_grid3.Size = New System.Drawing.Size(340, 169)
    Me.Uc_grid3.Sortable = False
    Me.Uc_grid3.SortAscending = True
    Me.Uc_grid3.SortByCol = 0
    Me.Uc_grid3.TabIndex = 0
    Me.Uc_grid3.TabStop = False
    Me.Uc_grid3.ToolTipped = True
    Me.Uc_grid3.TopRow = -2
    '
    'frm_cage_amounts
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(494, 427)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximumSize = New System.Drawing.Size(600, 500)
    Me.MinimumSize = New System.Drawing.Size(477, 445)
    Me.Name = "frm_cage_amounts"
    Me.Text = "frm_CAGE_amounts"
    Me.panel_data.ResumeLayout(False)
    Me.gb_currencies.ResumeLayout(False)
    Me.gb_bills.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_currencies As System.Windows.Forms.GroupBox
  Friend WithEvents dg_currencies As GUI_Controls.uc_grid
  Friend WithEvents gb_bills As System.Windows.Forms.GroupBox
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents dg_bills As GUI_Controls.uc_grid
  Friend WithEvents Uc_grid1 As GUI_Controls.uc_grid
  Friend WithEvents Uc_button1 As GUI_Controls.uc_button
  Friend WithEvents Uc_button2 As GUI_Controls.uc_button
  Friend WithEvents Uc_grid2 As GUI_Controls.uc_grid
  Friend WithEvents Uc_button3 As GUI_Controls.uc_button
  Friend WithEvents Uc_button4 As GUI_Controls.uc_button
  Friend WithEvents Uc_grid3 As GUI_Controls.uc_grid
End Class
