'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statistics_hour_group
' DESCRIPTION:   This screen allows to view the statistics.
' AUTHOR:        Alberto Cuesta
' CREATION DATE: 12-JUL-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-JUL-2007  ACC    Initial version
' 03-DEC-2012  RRB    Add Average Bet column.
' 28-MAR-2013  RCI & HBB    Added column SPH_THEORETICAL_WON_AMOUNT in table SALES_PER_HOUR. Needed to calculate the theoretical payout %. Also added a column to the grid with the theoretical payout %
' 25-MAY-2013  JCA    Change to use new table providers_games
' 18-AUG-2014  AMF    Progressive Jackpot
' 17-OCT-2014  JMV    Fixed WIG-1431
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 14-APR-2015  FJC    BackLog Item 966
' 13-MAY-2015  FOS    WIG 2336:Error filter by Terminal
' 10-JUN-2015  DHA    Error when bigger numbers of money type
' 17-JUN-2015  FAV    WIG-2461: Errors in the log
' 06-OCT-2015  FAV    Fixed Bug WIG-4888: Statistics grouped by hour: rows are incorrectly displayed without activity
' 11-NOV-2015  FOS    Product Backlog Item 5839:Garcia River change NLS
' 25-NOV-2015  MPO    Fixed Bug TFS-6961
' 15-JUN-2018 GDA    Bug 33063 - WIGOS-12243 - [Ticket #14517] Fallo – Reporte de Estadística agrupada por fecha, proveedor, terminal- Jugadas Version V03.08.0001
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports WSI.Common
Imports System.Data

Public Class frm_statistics_hours_group
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents cb_rows As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents lbl_netwin As System.Windows.Forms.Label
  Friend WithEvents Uc_combo_games As GUI_Controls.uc_combo_games
  Friend WithEvents lbl_payout As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.cb_rows = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.lbl_netwin = New System.Windows.Forms.Label
    Me.lbl_payout = New System.Windows.Forms.Label
    Me.Uc_combo_games = New GUI_Controls.uc_combo_games
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.Uc_combo_games)
    Me.panel_filter.Controls.Add(Me.lbl_payout)
    Me.panel_filter.Controls.Add(Me.lbl_netwin)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.cb_rows)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_rows, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_netwin, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_payout, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.Uc_combo_games, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(1226, 514)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'cb_rows
    '
    Me.cb_rows.AutoSize = True
    Me.cb_rows.Location = New System.Drawing.Point(616, 111)
    Me.cb_rows.Name = "cb_rows"
    Me.cb_rows.Size = New System.Drawing.Size(63, 17)
    Me.cb_rows.TabIndex = 4
    Me.cb_rows.Text = "xRows"
    Me.cb_rows.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 1
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(270, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 2
    '
    'lbl_netwin
    '
    Me.lbl_netwin.AutoSize = True
    Me.lbl_netwin.ForeColor = System.Drawing.Color.Navy
    Me.lbl_netwin.Location = New System.Drawing.Point(610, 166)
    Me.lbl_netwin.Name = "lbl_netwin"
    Me.lbl_netwin.Size = New System.Drawing.Size(52, 13)
    Me.lbl_netwin.TabIndex = 14
    Me.lbl_netwin.Text = "xNetwin"
    '
    'lbl_payout
    '
    Me.lbl_payout.AutoSize = True
    Me.lbl_payout.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payout.Location = New System.Drawing.Point(610, 147)
    Me.lbl_payout.Name = "lbl_payout"
    Me.lbl_payout.Size = New System.Drawing.Size(53, 13)
    Me.lbl_payout.TabIndex = 15
    Me.lbl_payout.Text = "xPayout"
    '
    'Uc_combo_games
    '
    Me.Uc_combo_games.Location = New System.Drawing.Point(599, 5)
    Me.Uc_combo_games.Name = "Uc_combo_games"
    Me.Uc_combo_games.Size = New System.Drawing.Size(272, 72)
    Me.Uc_combo_games.TabIndex = 16
    '
    'frm_statistics_hours_group
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1234, 712)
    Me.Name = "frm_statistics_hours_group"
    Me.Text = "frm_statistics_hours_group"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_SINCE As Integer = 0
  Private Const SQL_COLUMN_UNTIL As Integer = 1
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_AMOUNT_PER_CENT As Integer = 4
  Private Const SQL_COLUMN_NETWIN As Integer = 5
  Private Const SQL_COLUMN_NETWIN_PER_CENT As Integer = 6
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 7
  Private Const SQL_COLUMN_WON_COUNT As Integer = 8
  Private Const SQL_COLUMN_COUNT_PER_CENT As Integer = 9
  Private Const SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT As Integer = 10
  Private Const SQL_COLUMN_THEORICAL_AMOUNT As Integer = 11
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer = 12
  Private Const SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 13
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 14
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 As Integer = 15

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SINCE As Integer = 1
  Private Const GRID_COLUMN_UNTIL As Integer = 2
  Private Const GRID_COLUMN_PLAYED_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_WON_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_AMOUNT_PER_CENT As Integer = 8
  Private Const GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT As Integer = 9
  Private Const GRID_COLUMN_NETWIN As Integer = 10
  Private Const GRID_COLUMN_NETWIN_PER_CENT As Integer = 11
  Private Const GRID_COLUMN_PLAYED_COUNT As Integer = 12
  Private Const GRID_COLUMN_WON_COUNT As Integer = 13
  Private Const GRID_COLUMN_COUNT_PER_CENT As Integer = 14
  Private Const GRID_COLUMN_AVERAGE_BET As Integer = 15

  Private Const GRID_COLUMNS As Integer = 16
  Private Const GRID_HEADER_ROWS As Integer = 2

  'Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_game As String
  Private m_terminals As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_rows As String


  ' For amount grid
  Dim total_played_amount As Decimal
  Dim total_won_amount As Decimal
  Dim total_played_count As Decimal
  Dim total_won_count As Decimal
  Dim total_theoretical_won_amount As Decimal
  Dim total_progressive_provision_amount As Decimal
  Dim total_non_progressive_jackpot_amount As Decimal
  Dim total_progressive_jackpot_amount As Decimal
  Dim total_progressive_jackpot_amount_0 As Decimal

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STATISTICS_HOURS_GROUP

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(201) + " " + GLB_NLS_GUI_STATISTICS.GetString(365) ' Estadísticas Agrupadas por Hora

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_STATISTICS.Id(202))

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Progressives
    Me.lbl_payout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5400)
    Me.lbl_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5349)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Games
    'Call SetCombo(Me.cmb_game, SELECT_GAMES)
    'Call SetCombo(Me.cmb_game, SELECT_GAMES_V2)
    'FJC
    'Call SetComboGamesWithUnknown(Me.cmb_game, SELECT_GAMES_V2)
    'Me.cmb_game.Enabled = False
    Call Me.Uc_combo_games.Init()

    ' Checkbox rows
    Me.cb_rows.Text = GLB_NLS_GUI_STATISTICS.GetString(370)

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    total_played_amount = 0
    total_won_amount = 0
    total_played_count = 0
    total_won_count = 0
    total_theoretical_won_amount = 0
    total_progressive_provision_amount = 0
    total_non_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount_0 = 0
  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

    If Me.cb_rows.Checked = False Then
      AddDaysGrid()
      Me.Grid.SortGrid(GRID_COLUMN_SINCE)
    End If

    Me.Grid.AddRow()

    Dim idx_row As Integer = Me.Grid.NumRows - 1
    Dim amount_per_cent As Decimal
    Dim netwin_per_cent As Decimal
    Dim netwin As Decimal = total_played_amount - (total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount_0)
    Dim count_per_cent As Decimal
    Dim _total_average_bet As Decimal
    Dim _theoretical_amount_per_cent As Decimal

    _total_average_bet = 0

    ' Dates
    Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GLB_NLS_GUI_INVOICING.GetString(205)  '"TOTAL: "

    Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = ""

    ' Played Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(total_played_amount, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Won Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(total_won_amount, _
                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Amount Per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    Else
      amount_per_cent = ((total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount_0) / total_played_amount) * 100


      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(amount_per_cent, _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Theoretical Amount Per cent
    If total_theoretical_won_amount = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = ""
    Else
      If total_played_amount = 0 Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = ""
      Else
        _theoretical_amount_per_cent = (total_theoretical_won_amount / total_played_amount) * 100
        Me.Grid.Cell(idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_theoretical_amount_per_cent, 2) & "%"
      End If
    End If

    ' Non progressive Jackpot amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(total_non_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive Jackpot amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(total_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive provision amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(total_progressive_provision_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = ""
    Else
      netwin_per_cent = netwin * 100 / total_played_amount

      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(netwin_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Played Count
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(total_played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Won Count
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(total_won_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Count per cent
    If total_played_count = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    Else
      count_per_cent = (total_won_count / total_played_count) * 100
      Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(count_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Average bet
    If total_played_count = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
    Else
      _total_average_bet = Math.Round(total_played_amount / total_played_count, 2, MidpointRounding.AwayFromZero)

      If _total_average_bet = 0 Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    End If

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = "select " & _
                   "x.SINCE," & _
                   "x.UNTIL," & _
                   "x.PlayedAmount," & _
                   "x.WonAmount," & _
                   "CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)*100/x.PlayedAmount) END as AmountPc," & _
                   "PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) as Netwin," & _
                   "CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))*100/x.PlayedAmount) END as NetwinPc," & _
                   "x.PlayedCount," & _
                   "x.WonCount," & _
                   "CASE x.PlayedCount WHEN 0 THEN 0 ELSE((x.WonCount* 100.0)/x.PlayedCount) END as CountPc, " & _
                   "CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc, " & _
                   "TheoreticalWonAmount, " & _
                   "ProgressiveProvisionAmount, " & _
                   "NonProgressiveJackpotAmount, " & _
                   "ProgressiveJackpotAmount, " & _
                   "ProgressiveJackpotAmount0 " & _
               "from" & _
               "(SELECT "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY X.SINCE ASC"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    'Checks if the row doesn't have data if "Exclude rows without activity" is checked.
    If cb_rows.Checked Then
      For i As Integer = 2 To 14
        If Not DbRow.IsNull(i) AndAlso DbRow.Value(i) <> 0 Then
          Return True
        End If
      Next

      Return False
    End If

    Return True
  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _average_bet As Decimal

    _average_bet = 0

    ' Since DateTime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SINCE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_SINCE), , ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Until DateTime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNTIL).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_UNTIL), , ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' PlayedAmount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT), _
                                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    total_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

    ' WonAmount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT))
    total_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)

    ' AmountPerCent
    If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AMOUNT_PER_CENT), _
                                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    'TheoricalAmount
    total_theoretical_won_amount += DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT)

    ' TheoreticalAmountPerCent
    Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT), _
                                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"

    ' Non progressive Jackpot amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT))
    total_non_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT)

    ' Progressive Jackpot amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
    total_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT)
    total_progressive_jackpot_amount_0 += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0)

    ' Progressive provision amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT))
    total_progressive_provision_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT)

    ' Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN))

    ' Netwin PerCent
    If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NETWIN_PER_CENT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' PlayedCount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_COUNT), _
                                                                              ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    total_played_count += DbRow.Value(SQL_COLUMN_PLAYED_COUNT)

    ' WonCount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WON_COUNT), _
                                                                           ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    total_won_count += DbRow.Value(SQL_COLUMN_WON_COUNT)

    ' CountPerCent
    If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COUNT_PER_CENT), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Average bet
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT) And Not DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      If DbRow.Value(SQL_COLUMN_PLAYED_COUNT) = 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        _average_bet = Math.Round(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / DbRow.Value(SQL_COLUMN_PLAYED_COUNT), 2, MidpointRounding.AwayFromZero)

        If _average_bet = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(215), m_game)
    PrintData.SetFilter(cb_rows.Text, m_rows)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000
    PrintData.FilterHeaderWidth(3) = 3000
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(366)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_terminals = ""
    m_game = ""
    m_date_from = ""
    m_date_to = ""
    m_rows = ""

    If cb_rows.Checked = True Then
      m_rows = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_rows = GLB_NLS_GUI_INVOICING.GetString(480)
    End If


    ' Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Game
    If Me.Uc_combo_games.GetSelectedId() = String.Empty Then
      m_game = Me.Uc_combo_games.opt_all_game.Text
    Else
      m_game = Me.Uc_combo_games.GetSelectedValue
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Since date
      .Column(GRID_COLUMN_SINCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_SINCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(309)
      .Column(GRID_COLUMN_SINCE).Width = 1750 'GRID_COLUMN_DATE_WIDTH
      .Column(GRID_COLUMN_SINCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Until date
      .Column(GRID_COLUMN_UNTIL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_UNTIL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(310)
      .Column(GRID_COLUMN_UNTIL).Width = 1750 ' GRID_COLUMN_DATE_WIDTH
      .Column(GRID_COLUMN_UNTIL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6275) 'Coin In
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(302)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5391)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = 1950
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non progressive Jackpot amount
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5293)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1600
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive Jackpot amount
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5294)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive provision amount
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5292)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount Per Cent
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Width = GRID_COLUMN_PAYOUT_WIDTH
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical Ammount Per Cent
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1809)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Width = GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '  Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin Pay out
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(312)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Width = GRID_COLUMN_PAYOUT_WIDTH
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count Per Cent
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(306)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Width = GRID_COLUMN_PAYOUT_WIDTH
      .Column(GRID_COLUMN_COUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average bet
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = 1400
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim closing_time As Integer
    Dim final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    closing_time = GetDefaultClosingTime()
    final_time = New DateTime(_now.Year, _now.Month, _now.Day, closing_time, 0, 0)
    If final_time > _now Then
      final_time = final_time.AddDays(-1)
    End If

    final_time = final_time.Date
    Me.uc_dsl.ToDate = final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = closing_time

    Me.Uc_combo_games.SetDefaultValues()

    Me.cb_rows.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_select As String = ""
    Dim init_hour As Integer

    Dim str_date As String = SqlDate()

    init_hour = Me.uc_dsl.ClosingTime
    str_date = "01-01-2007 " + init_hour.ToString("00") + ":00:00:000"

    ' Filter Grouped date

    str_select += "DATEADD(HOUR," & _
                    "DATEDIFF(HOUR,'" & str_date & "',SPH_BASE_HOUR)," & _
                    "'" & str_date & "') as SINCE," & _
                  "UNTIL=DATEADD(hour,1," & _
                        "DATEADD(HOUR,DATEDIFF(HOUR,'" & str_date & "',SPH_BASE_HOUR),'" & str_date & "')),"

    str_select += "sum(CAST(SPH_played_amount AS DECIMAL(22,4)))as PlayedAmount," & _
                  "sum(CAST(SPH_won_amount AS DECIMAL(22,4))) as WonAmount," & _
                  "CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END as PlayedCount," & _
                  "sum(SPH_won_count) as WonCount, " & _
                  "sum(SPH_theoretical_won_amount) AS TheoreticalWonAmount, " & _
                  "sum(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount, " & _
                  "sum(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount, " & _
                  "sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount, " & _
                  "sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0 " & _
                  "FROM SALES_PER_HOUR_V2 "

    ' Filter Dates
    str_where = Me.uc_dsl.GetSqlFilterCondition("SPH_BASE_HOUR")
    If str_where.Length > 0 Then
      str_where = " AND " & str_where
    End If

    'FOS 13-MAY-2015: WIG-2336
    ' Filter Providers - Terminals
    str_where = str_where & " AND SPH_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Filter Game
    If Me.Uc_combo_games.GetSelectedId <> String.Empty Then
      str_where = str_where & " AND SPH_GAME_ID = " & Me.Uc_combo_games.GetSelectedId
    End If

    If str_where.Length > 0 Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    ' Filter Grouped date GROUP BY
    str_where = str_where & " GROUP BY DATEDIFF(HOUR,'" & str_date & "',SPH_BASE_HOUR)) x"

    Return str_select & str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Add grid days without activity
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub AddDaysGrid()

    Dim idx_hour As Integer
    Dim idx_row As Integer
    Dim date_add As Date
    Dim date_grid As Date
    Dim interval_hours As Integer
    Dim hour_exists As Boolean
    Dim filter_from As Date
    Dim filter_to As Date

    filter_from = Me.uc_dsl.FromDate
    filter_to = Me.uc_dsl.ToDate
    If (Me.uc_dsl.FromDateSelected = False) Then
      If (Me.Grid.NumRows > 0) Then
        filter_from = Me.Grid.Cell(0, GRID_COLUMN_SINCE).Value
      Else
        filter_from = WSI.Common.WGDB.Now
      End If
    Else
      filter_from = Me.uc_dsl.FromDate
    End If
    filter_from = filter_from.AddHours(Me.uc_dsl.ClosingTime)

    If (Me.uc_dsl.ToDateSelected = False) Then
      filter_to = WSI.Common.WGDB.Now
      filter_to = filter_to.AddHours(1)
    Else
      filter_to = filter_to.AddHours(Me.uc_dsl.ClosingTime)
    End If

    hour_exists = False
    interval_hours = DateDiff(DateInterval.Hour, filter_from, filter_to) - 1

    date_add = filter_from

    For idx_hour = 0 To interval_hours
      For idx_row = 0 To Me.Grid.NumRows - 1
        date_grid = Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value
        If (date_add = date_grid Or date_add < filter_from Or date_add > filter_to) Then
          hour_exists = True
        End If
      Next

      If hour_exists = False Then
        Call AddRowNullGrid(idx_row, date_add, date_add)

      End If

      hour_exists = False
      date_add = DateAdd(DateInterval.Hour, 1, date_add)
    Next

  End Sub ' AddDaysGrid

  ' PURPOSE: Add grid rows without activity 
  '
  '  PARAMS:
  '     - INPUT:
  '           - idx_row
  '           - date1
  '           - date2
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddRowNullGrid(ByVal idx_row As Integer, ByVal date_since As Date, ByVal date_until As Date)

    Me.Grid.AddRow()

    Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GUI_FormatDate(date_since, , ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Until DateTime
    date_until = date_until.AddHours(1)
    Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GUI_FormatDate(date_until, , ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' PlayedAmount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Playedwont
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Played Per Cent
    Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""

    ' Netwin
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin Per cent
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = ""

    ' PlayedCount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = 0

    ' Playedwon
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = 0

    ' Played per cent
    Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""

    ' Average bet
    Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""

  End Sub 'AddRowNullGrid

#End Region  ' Private Functions

#Region "Events"


#End Region

End Class
