'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_safe_keeping_edit
' DESCRIPTION:   edit counts of safe keeping
' AUTHOR:        Samuel Gonz�lez
' CREATION DATE: 09-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-APR-2015  SGB    Initial version. Backlog Item 709
' 22-APR-2015  SGB    Change text entry files in upper case.
' 07-JUL-2015  SGB    Fixed bug WIG-2537: Bloqued new user and not print correct balance.

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.IO
Imports System.Drawing.Printing
Imports WSI.Common

Public Class frm_cage_safe_keeping_edit
  Inherits frm_base_edit

#Region " Members "

  Private m_input_owner_name As String

#End Region 'Members

#Region "Overrides"

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6114)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' User
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1718)
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.ef_middle_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5087)
    Me.ef_middle_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.ef_surname_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1719)
    Me.ef_surname_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.ef_surname_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1720)
    Me.ef_surname_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.ef_document_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1722)
    Me.ef_document_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 20)

    'Document Id Type
    If GeneralParam.GetString("RegionalOptions", "CurrencyISOCode").Equals("MXN") Then
      cmb_document_type.Add(IdentificationTypes.GetIdentificationTypes(False))
    Else
      cmb_document_type.Add(IdentificationTypes.GetIdentificationTypes(True))
    End If

    Me.cmb_document_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1721)

    Me.chk_block_reason.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6125)

    Me.ef_balance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6126)
    Me.ef_balance.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)
    Me.ef_balance.IsReadOnly = True

    Me.chk_max_balance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6127)

    Me.ef_max_balance.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.chk_max_balance.Checked = False
    Me.ef_max_balance.Enabled = False

    lbl_blocked.Text = ""
    lbl_blocked.Hide()

    Call VisibleData()

    Call ResizeAndRelocate()

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _cage_safe_keeping As CLASS_CAGE_SAFE_KEEPING

    _cage_safe_keeping = DbReadObject()

    If Not CheckRequiredEmptyData() Then

      Return False
    End If

    'check document id exists
    If Not String.IsNullOrEmpty(ef_document_id.TextValue) Then
      If CLASS_CAGE_SAFE_KEEPING.CheckIdAlreadyExists(ef_document_id.TextValue, cmb_document_type.Value(cmb_document_type.SelectedIndex), _cage_safe_keeping.safe_keeping_id) Then
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_HOLDER_ID_ALREADY_EXISTS"), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        Return False
      End If
    End If

    If chk_max_balance.Checked = True AndAlso String.IsNullOrEmpty(ef_max_balance.TextValue) Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , chk_max_balance.Text)
      Call ef_max_balance.Focus()

      Return False
    End If

    ' check if bloqued account when balance is more than 0
    If chk_block_reason.Checked And ef_balance.Value > 0 Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6142), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Call UpdateBalance()
        Call chk_block_reason.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _cage_safe_keeping As CLASS_CAGE_SAFE_KEEPING

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _cage_safe_keeping = DbReadObject

    If _cage_safe_keeping.max_balance > 0 Then
      Me.chk_max_balance.Checked = True
      Me.ef_max_balance.TextValue = GUI_FormatCurrency(_cage_safe_keeping.max_balance)
    Else
      Me.chk_max_balance.Checked = False
      Me.ef_max_balance.TextValue = ""
    End If
    Me.ef_balance.TextValue = GUI_FormatCurrency(_cage_safe_keeping.balance)
    Me.chk_block_reason.Checked = _cage_safe_keeping.block_reason
    Me.cmb_document_type.TextValue = IdentificationTypes.DocIdTypeString(_cage_safe_keeping.owner_document_type)
    Me.ef_document_id.TextValue = _cage_safe_keeping.owner_document_id
    Me.ef_surname_1.TextValue = _cage_safe_keeping.owner_name1
    Me.ef_surname_2.TextValue = _cage_safe_keeping.owner_name2
    Me.ef_name.TextValue = _cage_safe_keeping.owner_name3
    Me.ef_middle_name.TextValue = _cage_safe_keeping.owner_name4

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.cmb_document_type.TextValue = IdentificationTypes.DocIdTypeString(_cage_safe_keeping.owner_document_type)
    Else
      cmb_document_type.TextValue = IdentificationTypes.DocIdTypeString(GeneralParam.GetInt32("Account.DefaultValues", "DocumentType"))
      _cage_safe_keeping.owner_document_type = GeneralParam.GetInt32("Account.DefaultValues", "DocumentType")
    End If

  End Sub 'GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim _cage_safe_keeping As CLASS_CAGE_SAFE_KEEPING

    _cage_safe_keeping = DbEditedObject

    _cage_safe_keeping.block_reason = Me.chk_block_reason.Checked
    _cage_safe_keeping.owner_document_type = IdentificationTypes.GetIdentificationID(Me.cmb_document_type.TextValue)
    _cage_safe_keeping.owner_document_id = Me.ef_document_id.TextValue
    _cage_safe_keeping.owner_name1 = Me.ef_surname_1.TextValue
    _cage_safe_keeping.owner_name2 = Me.ef_surname_2.TextValue
    _cage_safe_keeping.owner_name3 = Me.ef_name.TextValue
    _cage_safe_keeping.owner_name4 = Me.ef_middle_name.TextValue
    _cage_safe_keeping.balance = Me.ef_balance.TextValue
    If Me.chk_max_balance.Checked And Not String.IsNullOrEmpty(Me.ef_max_balance.TextValue) Then
      _cage_safe_keeping.max_balance = Me.ef_max_balance.TextValue
    Else
      _cage_safe_keeping.max_balance = -1
    End If

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _cage_safe_keeping As CLASS_CAGE_SAFE_KEEPING
    Dim _aux_nls As String
    'Dim _rc As mdl_NLS.ENUM_MB_RESULT

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Min database version popup alerdy showed. Don't show any message again
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_CAGE_SAFE_KEEPING
        _cage_safe_keeping = DbEditedObject
        _cage_safe_keeping.safe_keeping_id = -1
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 6130 "Se ha producido un error al leer la custodia %1."
          _cage_safe_keeping = Me.DbEditedObject
          _aux_nls = m_input_owner_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6130), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 6131 "Ya existe una custodia con el nombre %1."
          _cage_safe_keeping = Me.DbEditedObject
          _aux_nls = _cage_safe_keeping.owner_name3
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6131), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_surname_1.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 6132 "Se ha producido un error al a�adir la custodia %1."
          _cage_safe_keeping = Me.DbEditedObject
          _aux_nls = _cage_safe_keeping.owner_name3
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6132), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 6131 "Ya existe una custodia con el nombre %1."
          _cage_safe_keeping = Me.DbEditedObject
          _aux_nls = _cage_safe_keeping.owner_name3
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6131), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 6133 "Se ha producido un error al modificar la custodia %1."
          _cage_safe_keeping = Me.DbEditedObject
          _aux_nls = _cage_safe_keeping.owner_name3
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6133), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal SafeKeepingId As Integer, ByVal OwnerName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = SafeKeepingId
    Me.m_input_owner_name = OwnerName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SAFE_KEEPING_EDIT

    ' Call Base Form procedure
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

#End Region 'Overrides

#Region "Private Functions"

  ' PURPOSE: Resize form and relocate controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub ResizeAndRelocate()

    If Not Me.ef_middle_name.Visible Then
      ' Relocate
      Me.lbl_blocked.Location = New System.Drawing.Point(Me.lbl_blocked.Location.X, Me.chk_block_reason.Location.Y)
      Me.chk_block_reason.Location = Me.chk_max_balance.Location
      Me.chk_max_balance.Location = New System.Drawing.Point(Me.chk_max_balance.Location.X, Me.ef_balance.Location.Y + 2)
      Me.ef_max_balance.Location = New System.Drawing.Point(Me.ef_max_balance.Location.X, Me.ef_balance.Location.Y)
      Me.ef_balance.Location = Me.ef_document_id.Location
      Me.ef_document_id.Location = Me.cmb_document_type.Location
      Me.cmb_document_type.Location = Me.ef_surname_2.Location
      Me.ef_surname_2.Location = Me.ef_surname_1.Location
      Me.ef_surname_1.Location = Me.ef_middle_name.Location

      ' Resize
      Me.Size = New System.Drawing.Size(Me.Size.Width, Me.Size.Height - 25)
    End If

    If Not Me.ef_surname_2.Visible Then
      ' Relocate
      Me.lbl_blocked.Location = New System.Drawing.Point(Me.lbl_blocked.Location.X, Me.chk_block_reason.Location.Y)
      Me.chk_block_reason.Location = Me.chk_max_balance.Location
      Me.chk_max_balance.Location = New System.Drawing.Point(Me.chk_max_balance.Location.X, Me.ef_balance.Location.Y + 2)
      Me.ef_max_balance.Location = New System.Drawing.Point(Me.ef_max_balance.Location.X, Me.ef_balance.Location.Y)
      Me.ef_balance.Location = Me.ef_document_id.Location
      Me.ef_document_id.Location = Me.cmb_document_type.Location
      Me.cmb_document_type.Location = Me.ef_surname_2.Location

      ' Resize
      Me.Size = New System.Drawing.Size(Me.Size.Width, Me.Size.Height - 25)
    End If

  End Sub ' ResizeAndRelocate

  ' PURPOSE: Visible data hide or show
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub VisibleData()

    Dim _visible_data As VisibleData

    _visible_data = New VisibleData()
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, ToList(ef_name))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, ToList(ef_middle_name))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, ToList(ef_surname_1))
    _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, ToList(ef_surname_2))

    _visible_data.HideControls()

    If ef_surname_1.Visible And Not ef_surname_2.Visible Then
      ef_surname_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5099)
    End If

  End Sub ' VisibleData

  ' PURPOSE: Control to list
  '
  '  PARAMS:
  '     - INPUT:
  '           - ParamArray
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Control
  Private Function ToList(ByVal ParamArray Controls() As Control) As Control()
    Return Controls
  End Function ' ToList

  ' PURPOSE: Check required fields
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True if not empty fields
  Public Function CheckRequiredEmptyData() As Boolean

    Dim _controls_to_check As RequiredData
    Dim _focus_control As ENUM_CARDDATA_FIELD
    Dim _label_control As String

    _controls_to_check = New RequiredData(PLAYER_DATA_TO_CHECK.ACCOUNT)
    _label_control = String.Empty

    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME3, ef_name.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME4, ef_middle_name.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME1, ef_surname_1.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME2, ef_surname_2.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID, ef_document_id.TextValue)
    _controls_to_check.Add(ENUM_CARDDATA_FIELD.DOC_TYPE, IIf(cmb_document_type.SelectedIndex >= 0, cmb_document_type.Value(cmb_document_type.SelectedIndex), ""))

    If _controls_to_check.IsAnyEmpty(False, _focus_control) Then
      Call SetControlFocus(_focus_control, _label_control)
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _label_control), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True

  End Function ' CheckRequiredEmptyData

  ' PURPOSE: Set focus on control
  '
  '  PARAMS:
  '     - INPUT:
  '         - Field
  '
  '     - OUTPUT:
  '         - Label
  '
  ' RETURNS:
  Private Sub SetControlFocus(ByVal Field As ENUM_CARDDATA_FIELD, ByRef Label As String)

    Select Case Field

      Case ENUM_CARDDATA_FIELD.NAME1
        ef_surname_1.Focus()
        Label = ef_surname_1.Text

      Case ENUM_CARDDATA_FIELD.NAME2
        ef_surname_2.Focus()
        Label = ef_surname_2.Text

      Case ENUM_CARDDATA_FIELD.NAME3
        ef_name.Focus()
        Label = ef_name.Text

      Case ENUM_CARDDATA_FIELD.NAME4
        ef_middle_name.Focus()
        Label = ef_middle_name.Text

      Case ENUM_CARDDATA_FIELD.ID
        ef_document_id.Focus()
        Label = ef_document_id.Text

      Case ENUM_CARDDATA_FIELD.ID1
        ef_document_id.Focus()
        Label = ef_document_id.Text

      Case ENUM_CARDDATA_FIELD.DOC_TYPE
        cmb_document_type.Focus()
        Label = cmb_document_type.Text

      Case Else
        Label = ""

    End Select

  End Sub ' SetControlFocus

  ' PURPOSE: Update label with current status
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateStatusLabel()

    lbl_blocked.Hide()
    If chk_block_reason.Checked Then
      lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558).ToUpper()
      lbl_blocked.Show()
    End If

  End Sub ' UpdateStatusLabel

  ' PURPOSE: Update entry file of balance
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateBalance()
    Dim _cage_safe_keeping As CLASS_CAGE_SAFE_KEEPING

    _cage_safe_keeping = DbReadObject()
    _cage_safe_keeping.UpdateBalance()
    ef_balance.TextValue = GUI_FormatCurrency(_cage_safe_keeping.balance)

  End Sub ' UpdateBalance

#End Region 'Private Functions

#Region " Events "

  Private Sub chk_max_monto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_max_balance.CheckedChanged
    If chk_max_balance.Checked Then
      ef_max_balance.Enabled = True
    Else
      ef_max_balance.Enabled = False
    End If
  End Sub ' chk_max_monto_CheckedChanged

  Private Sub chk_bloqued_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_block_reason.CheckedChanged
    If chk_block_reason.Checked And Not Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      UpdateBalance()
    End If
    UpdateStatusLabel()
  End Sub 'chk_bloqued_CheckedChanged

#End Region 'Events

End Class