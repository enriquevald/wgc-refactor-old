'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bank_transaction_data_sel
' DESCRIPTION:  
' AUTHOR:        Jaume Barn�s
' CREATION DATE: 15-SEP-2014
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 15-SEP-2014  JBC   First Release
' 16-OCT-2014  JBC   Fixed Bug WIG-1515: Error format commentary in excel
' 16-OCT-2014  JBC   Fixed Bug WIG-1484: Hidding card numbers
' 15-JUL-2015  DHA   Added new movements types on filter
' 11-AUG-2017  AMF   Bug 29309:[WIGOS-3802] Missing VIP Player selection criterion in Bank Data Report
'-------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_bank_transaction_data_sel
  Inherits GUI_Controls.frm_base_sel

  'Public Enum CurrencyExchangeSubType
  '{
  '  NONE = 0,
  '  CHECK = 1,
  '  BANK_CARD = 2,
  '  CREDIT_CARD = 3,
  '  DEBIT_CARD = 4
  '}

  'Public Enum TransactionType
  '{
  '  CASH_ADVANCE = 0,
  '  RECHARGE = 1
  '}

#Region " Constants "

  Private Const GRID_FIXED_COLUMNS As Integer = 21

  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_CASHIER_NAME As Integer = 2
  Private Const GRID_COLUMN_TRANSACTION_TYPE As Integer = 3
  Private Const GRID_COLUMN_AMOUNT As Integer = 4

  'Card Par
  Private Const GRID_COLUMN_CARD_DOCUMENT_NUMBER As Integer = 5
  Private Const GRID_COLUMN_CARD_HOLDER_NAME As Integer = 6
  Private Const GRID_COLUMN_CARD_EXPIRATION_DATE As Integer = 7
  Private Const GRID_COLUMN_CARD_TYPE As Integer = 8
  Private Const GRID_COLUMN_MANUAL_ENTRY As Integer = 9

  'Check Part
  Private Const GRID_COLUMN_CHECK_DOCUMENT_NUMBER As Integer = 10
  Private Const GRID_COLUMN_CHECK_DATE As Integer = 11
  Private Const GRID_COLUMN_CHECK_HOLDER_NAME As Integer = 12
  Private Const GRID_COLUMN_CHECK_ACCOUNT_NUMBER As Integer = 13
  Private Const GRID_COLUMN_CHECK_ROUTING_NUMBER As Integer = 14

  'Otros
  Private Const GRID_COLUMN_NAME As Integer = 15
  Private Const GRID_COLUMN_COUNTRY As Integer = 16
  Private Const GRID_COLUMN_COMMENTS As Integer = 17

  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 18
  Private Const GRID_COLUMN_CARD_TRACK_DATA As Integer = 19
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 20

  Private Const GRID_WIDTH_ACCOUNT As Integer = 1000

  Private Const GRID_WIDTH_ID As Integer = 150
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1150
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_HOLDER_NAME As Integer = 3100
  Private Const GRID_WIDTH_DATE As Integer = 1950
  Private Const GRID_WIDTH_TRANSACTION_TYPE As Integer = 2000
  Private Const GRID_WIDTH_CASHIER_NAME As Integer = 2000
  Private Const GRID_WIDTH_AMOUNT As Integer = 1150

  'Card Part
  Private Const GRID_WIDTH_CARD_DOCUMENT_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_CARD_TYPE As Integer = 900
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 3100
  Private Const GRID_WIDTH_CARD_EXPIRATION_DATE As Integer = 2000
  Private Const GRID_WIDTH_CARD_BANK_NAME As Integer = 2000
  Private Const GRID_WIDTH_CARD_BANK_COUNTRY As Integer = 2000
  Private Const GRID_WIDTH_CARD_COMMENTS As Integer = 3100
  Private Const GRID_WIDTH_MANUAL_ENTRY As Integer = 1500

  'Check Part
  Private Const GRID_WIDTH_CHECK_DOCUMENT_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_CHECK_DATE As Integer = 1650
  Private Const GRID_WIDTH_CHECK_HOLDER_NAME As Integer = 3100
  Private Const GRID_WIDTH_CHECK_ACCOUNT_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_CHECK_ROUTING_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_CHECK_BANK_NAME As Integer = 2000
  Private Const GRID_WIDTH_CHECK_BANK_COUNTRY As Integer = 2000
  Private Const GRID_WIDTH_CHECK_COMMENTS As Integer = 3100

  ''SQL COLUMNS
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_CARD_TRACK_DATA As Integer = 1
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 2
  Private Const SQL_COLUMN_DATE As Integer = 3
  Private Const SQL_COLUMN_TRANSACTION_TYPE As Integer = 4
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 5
  Private Const SQL_COLUMN_AMOUNT As Integer = 6

  'Card Part
  Private Const SQL_COLUMN_CARD_DOCUMENT_NUMBER As Integer = 7
  Private Const SQL_COLUMN_CARD_TYPE As Integer = 8
  Private Const SQL_COLUMN_CARD_HOLDER_NAME As Integer = 9
  Private Const SQL_COLUMN_CARD_EXPIRATION_DATE As Integer = 10
  Private Const SQL_COLUMN_MANUAL_ENTRY As Integer = 11

  'Check Part
  Private Const SQL_COLUMN_CHECK_DOCUMENT_NUMBER As Integer = 12
  Private Const SQL_COLUMN_CHECK_DATE As Integer = 13
  Private Const SQL_COLUMN_CHECK_HOLDER_NAME As Integer = 14
  Private Const SQL_COLUMN_CHECK_ACCOUNT_NUMBER As Integer = 15
  Private Const SQL_COLUMN_CHECK_ROUTING_NUMBER As Integer = 16

  Private Const SQL_COLUMN_NAME As Integer = 17
  Private Const SQL_COLUMN_COUNTRY As Integer = 18
  Private Const SQL_COLUMN_COMMENTS As Integer = 19

#End Region

#Region " Attributes "
  Private m_date_from As String
  Private m_date_to As String
  Private m_cashiers As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_movement_type As List(Of String)
  Private m_payment_method As List(Of String)
  Private m_subtotals As Dictionary(Of WSI.Common.CurrencyExchangeSubType, Decimal)
  Private m_card_number_visible_chars As Integer
#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "
  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub SetDefaultValues()
    Dim initial_time As Date

    initial_time = WSI.Common.Misc.TodayOpening()

    Me.dtp_from.Value = initial_time
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.opt_type_card.Checked = True
    Me.opt_type_check.Checked = True
    Me.opt_all_users.Checked = True
    Me.chk_show_all.Checked = False
    Me.cmb_user.Enabled = False

    Me.chk_cash_advance.Checked = True
    Me.chk_recharge.Checked = True
    Me.chk_card_payment.Checked = True
    Me.chk_card_replacement.Checked = True

    Me.uc_account_sel1.Clear()

  End Sub ' SetDefaultValues

  ' PURPOSE: GetSqlWhere
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As StringBuilder
    Dim _account_filter As String
    Dim _where_list As List(Of String)
    Dim _transaction_types As New List(Of String)

    _where_list = New List(Of String)

    _account_filter = Me.uc_account_sel1.GetFilterSQL()

    _str_where = New StringBuilder()
    _transaction_types = New List(Of String)()

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      _where_list.Add("(AO_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ")")
    End If

    If Me.dtp_to.Checked = True Then
      _where_list.Add("(AO_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ")")
    End If

    ' Filter User
    If Me.opt_one_user.Checked = True Then
      _where_list.Add("(BT_USER_ID = " & Me.cmb_user.Value & ")")
    End If

    If Me.opt_type_card.Checked = Me.opt_type_check.Checked Then
      _where_list.Add("BT_TYPE IN ( " & WSI.Common.CurrencyExchangeSubType.BANK_CARD & _
                            " , " & WSI.Common.CurrencyExchangeSubType.CREDIT_CARD & _
                            " , " & WSI.Common.CurrencyExchangeSubType.CHECK & _
                            " , " & WSI.Common.CurrencyExchangeSubType.DEBIT_CARD & " )")
    Else
      If Me.opt_type_check.Checked Then
        _where_list.Add("BT_TYPE IN ( " & WSI.Common.CurrencyExchangeSubType.CHECK & ")")
      Else
        _where_list.Add("BT_TYPE IN ( " & WSI.Common.CurrencyExchangeSubType.BANK_CARD & _
                              " , " & WSI.Common.CurrencyExchangeSubType.CREDIT_CARD & _
                              " , " & WSI.Common.CurrencyExchangeSubType.DEBIT_CARD & " )")
      End If
    End If

    If Not (Me.chk_cash_advance.Checked And Me.chk_recharge.Checked And Me.chk_card_payment.Checked And Me.chk_card_replacement.Checked Or _
      Not Me.chk_cash_advance.Checked And Not Me.chk_recharge.Checked And Not Me.chk_card_payment.Checked And Not Me.chk_card_replacement.Checked) Then

      If Me.chk_recharge.Checked Then
        _transaction_types.Add(WSI.Common.TransactionType.RECHARGE)
      End If
      If Me.chk_cash_advance.Checked Then
        _transaction_types.Add(WSI.Common.TransactionType.CASH_ADVANCE)
      End If
      If Me.chk_card_payment.Checked Then
        _transaction_types.Add(WSI.Common.TransactionType.RECHARGE_CARD_CREATION)
      End If
      If Me.chk_card_replacement.Checked Then
        _transaction_types.Add(WSI.Common.TransactionType.CARD_REPLACEMENT)
      End If

      _where_list.Add("BT_TRANSACTION_TYPE IN (" & String.Join(", ", _transaction_types.ToArray()) & ")")

    End If

    If Not String.IsNullOrEmpty(_account_filter) Then
      _where_list.Add(_account_filter)
    End If

    Return CreateSQLCondition(_where_list.ToArray())
  End Function

  ' PURPOSE: Hide the number of digits passed throught parameter
  '
  '  PARAMS:
  '     - INPUT:
  '           - String CardNumber
  '           - Integer NumCharsToHide
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function HideCardNumbers(ByVal CardNumber As String, ByVal NumCharsToHide As Integer) As String
    Dim _visible_numbers As String
    Dim _hidden_numbers As String

    _visible_numbers = String.Empty
    _hidden_numbers = String.Empty

    Try

      If Not String.IsNullOrEmpty(CardNumber) And Not (CardNumber.Length <= NumCharsToHide) Then

        _visible_numbers = CardNumber.Substring(CardNumber.Length - NumCharsToHide, NumCharsToHide)

        For _idx_ As Integer = 0 To CardNumber.Length - NumCharsToHide
          _hidden_numbers &= "*"
        Next

        Return _hidden_numbers & _visible_numbers
      End If

    Catch _ex As Exception
      Log.Message("HideCardNumbers: " & _ex.Message())

    End Try

    Return CardNumber

  End Function

  ' PURPOSE: CreateSQLCondition with a List of String
  '
  '  PARAMS:
  '     - INPUT:
  '           - String() Params
  '           - Optional: String Joiner
  '           - Optional: String WhereValue
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String _result
  Public Function CreateSQLCondition(ByVal Params As String(), Optional ByVal Joiner As String = "AND", Optional ByVal WhereValue As String = "WHERE") As String
    Dim _result As String = String.Join(" " + Joiner + " ", Params)

    If _result <> String.Empty Then
      Return " " + WhereValue + " " + _result
    End If

    Return String.Empty
  End Function


#End Region

#Region " Overrides "
  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BANK_TRANSACTION_DATA

    'Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer

    Me.Grid.Clear()

    _total_columns += GRID_FIXED_COLUMNS

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      '  Account
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' AccountID
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1579) '"N�mero" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '  Account
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' TrackData
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) '"Tarjeta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Account
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' HolderName
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794) '"Titular" 
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Movement
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466) '"Movimiento" 'GLB_NLS_GUI_INVOICING.GetString(230)
      ' Date
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) '"Fecha" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Movement
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      ' Type
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254) 'TIPO
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Width = GRID_WIDTH_TRANSACTION_TYPE
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Movement
      .Column(GRID_COLUMN_CASHIER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      ' CashierUser
      .Column(GRID_COLUMN_CASHIER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1270) '"Usuario/Maquina" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CASHIER_NAME).Width = GRID_WIDTH_CASHIER_NAME
      .Column(GRID_COLUMN_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Movement
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
      ' Amount
      .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372) '"Monto" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      '  Card
      .Column(GRID_COLUMN_CARD_DOCUMENT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) '"Tarjeta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      ' Document Number
      .Column(GRID_COLUMN_CARD_DOCUMENT_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1579) '"N�mero" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_DOCUMENT_NUMBER).Width = GRID_WIDTH_CARD_DOCUMENT_NUMBER
      .Column(GRID_COLUMN_CARD_DOCUMENT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Card
      .Column(GRID_COLUMN_CARD_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) '"Tarjeta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      ' Card Type
      .Column(GRID_COLUMN_CARD_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254) '"Tipo" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_TYPE).Width = GRID_WIDTH_CARD_TYPE
      .Column(GRID_COLUMN_CARD_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Card
      .Column(GRID_COLUMN_CARD_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) '"Tarjeta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      ' Holder Name
      .Column(GRID_COLUMN_CARD_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794) '"Titular" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_CARD_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Card
      .Column(GRID_COLUMN_CARD_EXPIRATION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) '"Tarjeta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      ' Expiration Date
      .Column(GRID_COLUMN_CARD_EXPIRATION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5501) '"Fecha de Expiracion" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_EXPIRATION_DATE).Width = GRID_WIDTH_CARD_EXPIRATION_DATE
      .Column(GRID_COLUMN_CARD_EXPIRATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Card
      .Column(GRID_COLUMN_MANUAL_ENTRY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) '"Tarjeta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      ' Manual Entry
      .Column(GRID_COLUMN_MANUAL_ENTRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4920) '"Entrada Manual" 
      .Column(GRID_COLUMN_MANUAL_ENTRY).Width = GRID_WIDTH_MANUAL_ENTRY
      .Column(GRID_COLUMN_MANUAL_ENTRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Check
      .Column(GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
      ' Check Number
      .Column(GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1579) '"N�mero"
      .Column(GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Width = GRID_WIDTH_CHECK_DOCUMENT_NUMBER
      .Column(GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Check
      .Column(GRID_COLUMN_CHECK_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
      ' Check Date
      .Column(GRID_COLUMN_CHECK_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) '"Fecha" 
      .Column(GRID_COLUMN_CHECK_DATE).Width = GRID_WIDTH_CHECK_DATE
      .Column(GRID_COLUMN_CHECK_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Check
      .Column(GRID_COLUMN_CHECK_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
      ' Holder Name
      .Column(GRID_COLUMN_CHECK_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567) '"Beneficiario" 
      .Column(GRID_COLUMN_CHECK_HOLDER_NAME).Width = GRID_WIDTH_CHECK_HOLDER_NAME
      .Column(GRID_COLUMN_CHECK_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Check
      .Column(GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
      ' Account Number
      .Column(GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2749) ' "N�mero de Cuenta" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Width = GRID_WIDTH_CHECK_ACCOUNT_NUMBER
      .Column(GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Check
      .Column(GRID_COLUMN_CHECK_ROUTING_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
      ' Routing Number
      .Column(GRID_COLUMN_CHECK_ROUTING_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5498) '"N�mero de Ruta" 
      .Column(GRID_COLUMN_CHECK_ROUTING_NUMBER).Width = GRID_WIDTH_CHECK_ROUTING_NUMBER
      .Column(GRID_COLUMN_CHECK_ROUTING_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Check
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346)
      ' Bank Name
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554) '"Nombre del Banco" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_CHECK_BANK_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Check
      .Column(GRID_COLUMN_COUNTRY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346)
      ' Bank Country
      .Column(GRID_COLUMN_COUNTRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090) '"Pa�s" 'GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_COUNTRY).Width = GRID_WIDTH_CHECK_BANK_COUNTRY
      .Column(GRID_COLUMN_COUNTRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Check
      .Column(GRID_COLUMN_COMMENTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346)
      ' Check Comments
      .Column(GRID_COLUMN_COMMENTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1716)  'COMENTARIOS
      .Column(GRID_COLUMN_COMMENTS).Width = GRID_WIDTH_CHECK_COMMENTS
      .Column(GRID_COLUMN_COMMENTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      If Me.opt_type_card.Checked <> Me.opt_type_check.Checked Then
        If Me.opt_type_card.Checked Then
          .Column(GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Width = 0
          .Column(GRID_COLUMN_CHECK_DATE).Width = 0
          .Column(GRID_COLUMN_CHECK_HOLDER_NAME).Width = 0
          .Column(GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Width = 0
          .Column(GRID_COLUMN_CHECK_ROUTING_NUMBER).Width = 0
        Else
          .Column(GRID_COLUMN_CARD_DOCUMENT_NUMBER).Width = 0
          .Column(GRID_COLUMN_CARD_HOLDER_NAME).Width = 0
          .Column(GRID_COLUMN_CARD_EXPIRATION_DATE).Width = 0
          .Column(GRID_COLUMN_CARD_TYPE).Width = 0
          .Column(GRID_COLUMN_MANUAL_ENTRY).Width = 0
        End If

      End If

    End With



  End Sub ' GUI_StyleSheet

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    If (Me.opt_type_card.Checked And Me.opt_type_check.Checked) Or (Not Me.opt_type_card.Checked And Not Me.opt_type_check.Checked) Then
      ExcelData.SetColumnFormat(4, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' card 
      ExcelData.SetColumnFormat(6, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' card 
      ExcelData.SetColumnFormat(9, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' card 
      ExcelData.SetColumnFormat(10, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
      ExcelData.SetColumnFormat(11, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
      ExcelData.SetColumnFormat(12, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
      ExcelData.SetColumnFormat(16, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
      ExcelData.SetColumnFormat(17, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
      ExcelData.SetColumnFormat(18, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
    Else
      If Me.opt_type_card.Checked Then
        ExcelData.SetColumnFormat(4, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' card 
        ExcelData.SetColumnFormat(6, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' card
        ExcelData.SetColumnFormat(9, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' card
      Else
        ExcelData.SetColumnFormat(4, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
        ExcelData.SetColumnFormat(5, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
        ExcelData.SetColumnFormat(7, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
        ExcelData.SetColumnFormat(8, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' check 
      End If

      ExcelData.SetColumnFormat(11, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Account-ID
      ExcelData.SetColumnFormat(12, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Account-ID
      ExcelData.SetColumnFormat(13, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) ' Account-ID
    End If

  End Sub ' GUI_ReportParams

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _columns As New Dictionary(Of String, String)

    Call MyBase.GUI_InitControls()

    uc_account_sel1.InitExtendedQuery(WSI.Common.TITO.Utils.IsTitoMode())

    _columns.Add("AC_ACCOUNT_ID", "AO_ACCOUNT_ID")
    _columns.Add("AC_HOLDER_NAME", "BT_ACCOUNT_HOLDER_NAME")
    _columns.Add("AC_TRACK_DATA", "BT_ACCOUNT_TRACK_DATA")
    uc_account_sel1.InitColumns(_columns)
    uc_account_sel1.SearchTrackDataAsInternal = False

    m_card_number_visible_chars = GeneralParam.GetInt64("Cashier.PaymentMethod", "BankCard.VisibleChars", 0)

    ' Account Summary
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5490)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False

    ' Creation Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2287)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    'MovementType
    Me.gb_movement_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460) '"Tipo de Movimiento"
    Me.chk_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233) '"Recarga"
    Me.chk_cash_advance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5497) '"Adelanto de Efectivo"
    Me.chk_card_payment.Text = WSI.Common.Misc.GetCardPlayerConceptName()
    Me.chk_card_replacement.Text = GLB_NLS_GUI_INVOICING.GetString(70) '" Sustituci�n de tarjeta

    'PaymentType
    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1595) '"Forma de Pago"
    Me.opt_type_card.Text = GLB_NLS_GUI_INVOICING.GetString(212) '"Tarjeta"
    Me.opt_type_check.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1569) '"Cheque"

    ' Set filter default values
    Call SetDefaultValues()

    '' Set combo with Users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " _
                  & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " AND GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")

    Me.cmb_user.Enabled = False

    ' Grid
    Call GUI_StyleSheet()

    'CCG 16-SEP-2013: Show Massive Search
    Me.uc_account_sel1.ShowMassiveSearch = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    If Not Me.uc_account_sel1.ValidateFormat() Then

      Return False
    End If

    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(" SELECT    AO_ACCOUNT_ID                                                           ")
    _str_sql.AppendLine("         , BT_ACCOUNT_TRACK_DATA                                                   ")
    _str_sql.AppendLine("         , BT_ACCOUNT_HOLDER_NAME                                                  ")
    _str_sql.AppendLine("         , AO_DATETIME                                                             ")
    _str_sql.AppendLine("         , BT_TRANSACTION_TYPE                                                     ")
    _str_sql.AppendLine("         , BT_USER_NAME + '@' + BT_CASHIER_NAME CM_NAME                            ")
    _str_sql.AppendLine("         , BT_AMOUNT                                                               ")
    _str_sql.AppendLine("         , BT_DOCUMENT_NUMBER                                                      ")
    _str_sql.AppendLine("         , BT_TYPE                                                                 ")
    _str_sql.AppendLine("         , BT_HOLDER_NAME                                                          ")
    _str_sql.AppendLine("         , BT_CARD_EXPIRATION_DATE                                                 ")
    _str_sql.AppendLine("         , BT_EDITED                                                               ")
    _str_sql.AppendLine("         , BT_DOCUMENT_NUMBER                                                      ")
    _str_sql.AppendLine("         , BT_CHECK_DATE                                                           ")
    _str_sql.AppendLine("         , BT_HOLDER_NAME                                                          ")
    _str_sql.AppendLine("         , BT_CHECK_ACCOUNT_NUMBER                                                 ")
    _str_sql.AppendLine("         , BT_CHECK_ROUTING_NUMBER                                                 ")
    _str_sql.AppendLine("         , BT_BANK_NAME                                                            ")
    _str_sql.AppendLine("         , BT_BANK_COUNTRY                                                         ")
    _str_sql.AppendLine("         , BT_COMMENTS                                                             ")
    _str_sql.AppendLine("   FROM    BANK_TRANSACTIONS                                                       ")
    _str_sql.AppendLine("INNER JOIN ACCOUNT_OPERATIONS ON BT_OPERATION_ID = AO_OPERATION_ID                 ")

    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine("     ORDER BY AO_DATETIME DESC                                                     ")

    Return _str_sql.ToString()
  End Function


  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()
    m_subtotals = New Dictionary(Of WSI.Common.CurrencyExchangeSubType, Decimal)

  End Sub

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _payment_method As String
    Dim _movement_type As String

    Dim _external_track_data As String
    Dim _bool_rc As Boolean

    ' Account ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)

    _external_track_data = ""

    _bool_rc = CardNumber.TrackDataToExternal(_external_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK_DATA).ToString(), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER)
    If _bool_rc Then
      ' Account Card External TrackData
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK_DATA).Value = _external_track_data
    End If
    

    ' Account Holder Name
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_HOLDER_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = String.Empty
    End If

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), _
                                                                                ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Select Case (DbRow.Value(SQL_COLUMN_TRANSACTION_TYPE))
      Case WSI.Common.TransactionType.CASH_ADVANCE
        _movement_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5497) '"Adelanto de Efectivo"
      Case WSI.Common.TransactionType.RECHARGE
        _movement_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233) '"Recarga"
      Case WSI.Common.TransactionType.RECHARGE_CARD_CREATION
        _movement_type = WSI.Common.Misc.GetCardPlayerConceptName()
      Case WSI.Common.TransactionType.CARD_REPLACEMENT
        _movement_type = GLB_NLS_GUI_INVOICING.GetString(70) '"Sustituci�n de tarjeta"
      Case Else
        _movement_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346) '"Otros"
    End Select

    ' Transaction Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSACTION_TYPE).Value = _movement_type

    ' Cashier Terminal Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CASHIER_NAME)

    ' Amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT))

    If DbRow.Value(SQL_COLUMN_CARD_TYPE) <> WSI.Common.CurrencyExchangeSubType.CHECK Then

      ' Document Number
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CARD_DOCUMENT_NUMBER)) Then
        If m_card_number_visible_chars > 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_DOCUMENT_NUMBER).Value = HideCardNumbers(MagneticStripeBankCard.DecorateCardNumber(DbRow.Value(SQL_COLUMN_CARD_DOCUMENT_NUMBER)), m_card_number_visible_chars) 'MagneticStripeBankCard.DecorateCardNumber(DbRow.Value(SQL_COLUMN_CARD_DOCUMENT_NUMBER)))
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_DOCUMENT_NUMBER).Value = MagneticStripeBankCard.DecorateCardNumber(DbRow.Value(SQL_COLUMN_CARD_DOCUMENT_NUMBER))
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_DOCUMENT_NUMBER).Value = String.Empty
      End If

      ' Card Type
      Select Case DbRow.Value(SQL_COLUMN_CARD_TYPE)
        Case WSI.Common.CurrencyExchangeSubType.BANK_CARD
          _payment_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5521) '"Tarjeta Bancaria"
        Case WSI.Common.CurrencyExchangeSubType.DEBIT_CARD
          _payment_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5569) '"Tarjeta de D�bito"
        Case WSI.Common.CurrencyExchangeSubType.CREDIT_CARD
          _payment_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5570) '"Tarjeta de Cr�dito"
        Case Else
          _payment_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5521) '"Tarjeta Bancaria"
      End Select

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TYPE).Value = _payment_method


      ' Holder Name
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CARD_HOLDER_NAME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_CARD_HOLDER_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_HOLDER_NAME).Value = String.Empty
      End If

      ' Expiration Date
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE)) Then
        If DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE) = "/" Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_EXPIRATION_DATE).Value = String.Empty
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_EXPIRATION_DATE).Value = DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE)
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_EXPIRATION_DATE).Value = String.Empty
      End If

      ' Manual entry
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MANUAL_ENTRY).Value = IIf(DbRow.Value(SQL_COLUMN_MANUAL_ENTRY), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5500), String.Empty)
    Else
      ' Document Number
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CHECK_DOCUMENT_NUMBER)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Value = DbRow.Value(SQL_COLUMN_CHECK_DOCUMENT_NUMBER)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_DOCUMENT_NUMBER).Value = String.Empty
      End If

      ' Date
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CHECK_DATE)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_DATE).Value = DbRow.Value(SQL_COLUMN_CHECK_DATE)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_DATE).Value = String.Empty
      End If

      ' Holder Name
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CHECK_HOLDER_NAME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_CHECK_HOLDER_NAME)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_HOLDER_NAME).Value = String.Empty
      End If

      ' Account Number
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CHECK_ACCOUNT_NUMBER)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Value = DbRow.Value(SQL_COLUMN_CHECK_ACCOUNT_NUMBER)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_ACCOUNT_NUMBER).Value = String.Empty
      End If

      ' Routing Number
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CHECK_ROUTING_NUMBER)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_ROUTING_NUMBER).Value = DbRow.Value(SQL_COLUMN_CHECK_ROUTING_NUMBER)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHECK_ROUTING_NUMBER).Value = String.Empty
      End If
    End If

    ' Bank Name
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = String.Empty
    End If

    ' Bank Country
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COUNTRY)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNTRY).Value = CardData.NationalitiesString(DbRow.Value(SQL_COLUMN_COUNTRY))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNTRY).Value = String.Empty
    End If

    ' Comments
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COMMENTS)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMENTS).Value = DbRow.Value(SQL_COLUMN_COMMENTS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMENTS).Value = String.Empty
    End If

    Return True
  End Function

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_movement_type = New List(Of String)
    m_payment_method = New List(Of String)
    m_date_from = String.Empty
    m_date_to = String.Empty
    m_cashiers = String.Empty
    m_account = String.Empty
    m_track_data = String.Empty
    m_holder = String.Empty

    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Users
    If Me.opt_all_users.Checked Then
      m_cashiers = Me.opt_all_users.Text
    Else
      m_cashiers = Me.cmb_user.TextValue
    End If

    ' Movement Type
    If Me.chk_recharge.Checked Then
      m_movement_type.Add(Me.chk_recharge.Text)
    End If

    If Me.chk_cash_advance.Checked Then
      m_movement_type.Add(Me.chk_cash_advance.Text)
    End If

    If Me.chk_card_payment.Checked Then
      m_movement_type.Add(Me.chk_card_payment.Text)
    End If

    If Me.chk_card_replacement.Checked Then
      m_movement_type.Add(Me.chk_card_replacement.Text)
    End If

    If m_movement_type.Count = 0 Then
      m_movement_type.Add(Me.chk_recharge.Text)
      m_movement_type.Add(Me.chk_cash_advance.Text)
    End If

    ' Filter Type
    If Me.opt_type_card.Checked Then
      m_payment_method.Add(Me.opt_type_card.Text)
    End If

    If Me.opt_type_check.Checked Then
      m_payment_method.Add(Me.opt_type_check.Text)
    End If

    If m_payment_method.Count = 0 Then
      m_payment_method.Add(Me.opt_type_card.Text)
      m_payment_method.Add(Me.opt_type_check.Text)
    End If

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_cashiers)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1595), String.Join(",", m_payment_method.ToArray))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460), String.Join(",", m_movement_type.ToArray))
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)

    ' With
    PrintData.FilterHeaderWidth(1) = 1000
    PrintData.FilterValueWidth(1) = 1600
    PrintData.FilterHeaderWidth(2) = 800
    PrintData.FilterValueWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 500
    PrintData.FilterValueWidth(3) = 8750

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Fill dictionary with all the payment types
  '
  '  PARAMS:
  '     - INPUT:
  '           - DbRow
  ' RETURNS:
  '     - Boolean
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _type As WSI.Common.CurrencyExchangeSubType
    Dim _amount As Decimal

    _type = DbRow.Value(SQL_COLUMN_CARD_TYPE)
    _amount = DbRow.Value(SQL_COLUMN_AMOUNT)

    If m_subtotals.ContainsKey(_type) Then
      m_subtotals(_type) += _amount
    Else
      m_subtotals.Add(_type, _amount)
    End If

    Return True
  End Function

  ' PURPOSE: Subtotal
  '
  '  PARAMS:
  ' 
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _ordered_list As List(Of WSI.Common.CurrencyExchangeSubType)
    Dim _total_amount As Decimal
    Dim _idx_row As Integer
    Dim _card_differentiation As Boolean
    Dim _first_time As Boolean


    If Me.Grid.NumRows > 0 Then

      _first_time = True
      _card_differentiation = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType")
      _ordered_list = New List(Of WSI.Common.CurrencyExchangeSubType)(m_subtotals.Keys)
      _total_amount = 0

      'Sorting to be:
      ' - DEBIT
      ' - CREDIT
      ' - OTHERS
      ' - CHECKS
      _ordered_list.Sort()
      _ordered_list.Reverse()

      For Each _type As Integer In _ordered_list

        Me.Grid.AddRow()
        _idx_row = Me.Grid.NumRows - 1
        Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

        If _first_time Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE).Value = GLB_NLS_GUI_STATISTICS.GetString(203) '"TOTAL: "
          _first_time = False
        End If

        If _type = WSI.Common.CurrencyExchangeSubType.CHECK Then
          If _total_amount <> 0 Then

            Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5598) '"Total tarjetas"
            Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_total_amount)
            Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

            Me.Grid.AddRow()

            _idx_row = Me.Grid.NumRows - 1
            Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

          End If

          Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5599) '"Total cheque"
          Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(m_subtotals(_type))

        Else
          Select Case _type
            Case WSI.Common.CurrencyExchangeSubType.CREDIT_CARD
              Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5595) '"Tarjeta de Cr�dito"
              Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

            Case WSI.Common.CurrencyExchangeSubType.DEBIT_CARD
              Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5596) '"Tarjeta de D�bito"
              Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)


            Case WSI.Common.CurrencyExchangeSubType.BANK_CARD
              If m_subtotals.Count = 1 AndAlso Not _card_differentiation Then
                Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5598) '"Total tarjetas"
                Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
              Else
                Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5597) '"Otras tarjeta"
                Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
              End If
          End Select

          _total_amount += m_subtotals(_type)

          Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(m_subtotals(_type))

        End If
      Next

      If Not m_subtotals.ContainsKey(CurrencyExchangeSubType.CHECK) Then

        If m_subtotals.Count > 1 Then
          Me.Grid.AddRow()
          Me.Grid.Row(Me.Grid.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5598) '"Total tarjetas"
          Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_total_amount)
        ElseIf Not m_subtotals.ContainsKey(CurrencyExchangeSubType.BANK_CARD) AndAlso _card_differentiation Then
          Me.Grid.AddRow()
          Me.Grid.Row(Me.Grid.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COLUMN_TRANSACTION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5598) '"Total tarjetas"
          Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_total_amount)

        End If

      End If
    End If

  End Sub

#End Region

#Region " Events "

  Private Sub opt_one_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
  End Sub

  Private Sub opt_all_users_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
  End Sub

  ' XCD 16-Aug-2012 Fill combo users
  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " AND GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")
    End If
  End Sub 'chk_show_all
#End Region

End Class