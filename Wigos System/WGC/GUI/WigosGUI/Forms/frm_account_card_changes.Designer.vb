<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_account_card_changes
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_movements = New System.Windows.Forms.GroupBox()
    Me.chk_include_movement_changes = New System.Windows.Forms.CheckBox()
    Me.chk_include_movement_created = New System.Windows.Forms.CheckBox()
    Me.chk_include_movement_recycled = New System.Windows.Forms.CheckBox()
    Me.chk_show_movements = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_movements.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_show_movements)
    Me.panel_filter.Controls.Add(Me.gb_movements)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(953, 137)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_movements, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_movements, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 141)
    Me.panel_data.Size = New System.Drawing.Size(953, 435)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(947, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(947, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(260, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 1
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(3, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(255, 101)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(7, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(7, 16)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_movements
    '
    Me.gb_movements.Controls.Add(Me.chk_include_movement_changes)
    Me.gb_movements.Controls.Add(Me.chk_include_movement_created)
    Me.gb_movements.Controls.Add(Me.chk_include_movement_recycled)
    Me.gb_movements.Location = New System.Drawing.Point(572, 6)
    Me.gb_movements.Name = "gb_movements"
    Me.gb_movements.Size = New System.Drawing.Size(212, 86)
    Me.gb_movements.TabIndex = 3
    Me.gb_movements.TabStop = False
    Me.gb_movements.Text = "xMovements"
    '
    'chk_include_movement_changes
    '
    Me.chk_include_movement_changes.AutoSize = True
    Me.chk_include_movement_changes.Location = New System.Drawing.Point(6, 39)
    Me.chk_include_movement_changes.Name = "chk_include_movement_changes"
    Me.chk_include_movement_changes.Size = New System.Drawing.Size(83, 17)
    Me.chk_include_movement_changes.TabIndex = 1
    Me.chk_include_movement_changes.Text = "xChanges"
    Me.chk_include_movement_changes.UseVisualStyleBackColor = True
    '
    'chk_include_movement_created
    '
    Me.chk_include_movement_created.AutoSize = True
    Me.chk_include_movement_created.Location = New System.Drawing.Point(6, 16)
    Me.chk_include_movement_created.Name = "chk_include_movement_created"
    Me.chk_include_movement_created.Size = New System.Drawing.Size(79, 17)
    Me.chk_include_movement_created.TabIndex = 0
    Me.chk_include_movement_created.Text = "xCreated"
    Me.chk_include_movement_created.UseVisualStyleBackColor = True
    '
    'chk_include_movement_recycled
    '
    Me.chk_include_movement_recycled.AutoSize = True
    Me.chk_include_movement_recycled.Location = New System.Drawing.Point(6, 62)
    Me.chk_include_movement_recycled.Name = "chk_include_movement_recycled"
    Me.chk_include_movement_recycled.Size = New System.Drawing.Size(84, 17)
    Me.chk_include_movement_recycled.TabIndex = 2
    Me.chk_include_movement_recycled.Text = "xRecycled"
    Me.chk_include_movement_recycled.UseVisualStyleBackColor = True
    '
    'chk_show_movements
    '
    Me.chk_show_movements.AutoSize = True
    Me.chk_show_movements.Location = New System.Drawing.Point(578, 98)
    Me.chk_show_movements.Name = "chk_show_movements"
    Me.chk_show_movements.Size = New System.Drawing.Size(129, 17)
    Me.chk_show_movements.TabIndex = 2
    Me.chk_show_movements.Text = "xShowMovements"
    Me.chk_show_movements.UseVisualStyleBackColor = True
    '
    'frm_account_card_changes
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(963, 580)
    Me.Name = "frm_account_card_changes"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_account_card_changes"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_movements.ResumeLayout(False)
    Me.gb_movements.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_movements As System.Windows.Forms.GroupBox
  Friend WithEvents chk_include_movement_changes As System.Windows.Forms.CheckBox
  Friend WithEvents chk_include_movement_created As System.Windows.Forms.CheckBox
  Friend WithEvents chk_include_movement_recycled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_movements As System.Windows.Forms.CheckBox
End Class
