<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_play_session_close
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_account_header = New System.Windows.Forms.Label
    Me.lbl_close_session_description = New System.Windows.Forms.Label
    Me.txt_reason = New System.Windows.Forms.TextBox
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_account_header)
    Me.panel_data.Controls.Add(Me.lbl_close_session_description)
    Me.panel_data.Controls.Add(Me.txt_reason)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(553, 226)
    '
    'lbl_account_header
    '
    Me.lbl_account_header.Cursor = System.Windows.Forms.Cursors.Default
    Me.lbl_account_header.Font = New System.Drawing.Font("Verdana", 9.25!)
    Me.lbl_account_header.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_account_header.Location = New System.Drawing.Point(12, 0)
    Me.lbl_account_header.Name = "lbl_account_header"
    Me.lbl_account_header.Size = New System.Drawing.Size(529, 39)
    Me.lbl_account_header.TabIndex = 5
    Me.lbl_account_header.Text = "xAccountHeader"
    Me.lbl_account_header.UseMnemonic = False
    '
    'lbl_close_session_description
    '
    Me.lbl_close_session_description.AutoSize = True
    Me.lbl_close_session_description.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
    Me.lbl_close_session_description.Location = New System.Drawing.Point(12, 39)
    Me.lbl_close_session_description.Name = "lbl_close_session_description"
    Me.lbl_close_session_description.Size = New System.Drawing.Size(223, 13)
    Me.lbl_close_session_description.TabIndex = 8
    Me.lbl_close_session_description.Text = "xDescriptionBlockUnblockReason"
    '
    'txt_reason
    '
    Me.txt_reason.Location = New System.Drawing.Point(15, 58)
    Me.txt_reason.MaxLength = 255
    Me.txt_reason.Multiline = True
    Me.txt_reason.Name = "txt_reason"
    Me.txt_reason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_reason.Size = New System.Drawing.Size(526, 159)
    Me.txt_reason.TabIndex = 9
    '
    'frm_play_session_close
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(657, 237)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_play_session_close"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_play_session_close"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Private WithEvents lbl_account_header As System.Windows.Forms.Label
  Private WithEvents lbl_close_session_description As System.Windows.Forms.Label
  Friend WithEvents txt_reason As System.Windows.Forms.TextBox
End Class
