<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mobile_bank_edit_device
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_mobile_bank_edit_device))
    Me.ef_pin = New GUI_Controls.uc_entry_field()
    Me.ef_limit_by_session = New GUI_Controls.uc_entry_field()
    Me.ef_limit_recharge = New GUI_Controls.uc_entry_field()
    Me.ef_limit_num_recharges = New GUI_Controls.uc_entry_field()
    Me.gb_edit_mb = New System.Windows.Forms.GroupBox()
    Me.gb_limits = New System.Windows.Forms.GroupBox()
    Me.chk_limit_num_recharges = New System.Windows.Forms.CheckBox()
    Me.chk_limit_recharge = New System.Windows.Forms.CheckBox()
    Me.chk_limit_by_session = New System.Windows.Forms.CheckBox()
    Me.ef_card_mb = New GUI_Controls.uc_entry_field()
    Me.chk_lock_mobile_bank = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_edit_mb.SuspendLayout()
    Me.gb_limits.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_lock_mobile_bank)
    Me.panel_data.Controls.Add(Me.gb_edit_mb)
    Me.panel_data.Size = New System.Drawing.Size(350, 260)
    '
    'ef_pin
    '
    Me.ef_pin.DoubleValue = 0.0R
    Me.ef_pin.IntegerValue = 0
    Me.ef_pin.IsReadOnly = False
    Me.ef_pin.Location = New System.Drawing.Point(33, 20)
    Me.ef_pin.Name = "ef_pin"
    Me.ef_pin.PlaceHolder = Nothing
    Me.ef_pin.ShortcutsEnabled = True
    Me.ef_pin.Size = New System.Drawing.Size(297, 24)
    Me.ef_pin.SufixText = "Sufix Text"
    Me.ef_pin.SufixTextVisible = True
    Me.ef_pin.TabIndex = 0
    Me.ef_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_pin.TextValue = ""
    Me.ef_pin.TextWidth = 140
    Me.ef_pin.Value = ""
    Me.ef_pin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_limit_by_session
    '
    Me.ef_limit_by_session.DoubleValue = 0.0R
    Me.ef_limit_by_session.IntegerValue = 0
    Me.ef_limit_by_session.IsReadOnly = False
    Me.ef_limit_by_session.Location = New System.Drawing.Point(167, 19)
    Me.ef_limit_by_session.Name = "ef_limit_by_session"
    Me.ef_limit_by_session.PlaceHolder = Nothing
    Me.ef_limit_by_session.ShortcutsEnabled = True
    Me.ef_limit_by_session.Size = New System.Drawing.Size(157, 25)
    Me.ef_limit_by_session.SufixText = "Sufix Text"
    Me.ef_limit_by_session.SufixTextVisible = True
    Me.ef_limit_by_session.TabIndex = 1
    Me.ef_limit_by_session.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_by_session.TextValue = ""
    Me.ef_limit_by_session.TextWidth = 0
    Me.ef_limit_by_session.Value = ""
    Me.ef_limit_by_session.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_limit_recharge
    '
    Me.ef_limit_recharge.DoubleValue = 0.0R
    Me.ef_limit_recharge.IntegerValue = 0
    Me.ef_limit_recharge.IsReadOnly = False
    Me.ef_limit_recharge.Location = New System.Drawing.Point(167, 52)
    Me.ef_limit_recharge.Name = "ef_limit_recharge"
    Me.ef_limit_recharge.PlaceHolder = Nothing
    Me.ef_limit_recharge.ShortcutsEnabled = True
    Me.ef_limit_recharge.Size = New System.Drawing.Size(157, 25)
    Me.ef_limit_recharge.SufixText = "Sufix Text"
    Me.ef_limit_recharge.SufixTextVisible = True
    Me.ef_limit_recharge.TabIndex = 3
    Me.ef_limit_recharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_recharge.TextValue = ""
    Me.ef_limit_recharge.TextWidth = 0
    Me.ef_limit_recharge.Value = ""
    Me.ef_limit_recharge.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_limit_num_recharges
    '
    Me.ef_limit_num_recharges.DoubleValue = 0.0R
    Me.ef_limit_num_recharges.IntegerValue = 0
    Me.ef_limit_num_recharges.IsReadOnly = False
    Me.ef_limit_num_recharges.Location = New System.Drawing.Point(167, 85)
    Me.ef_limit_num_recharges.Name = "ef_limit_num_recharges"
    Me.ef_limit_num_recharges.PlaceHolder = Nothing
    Me.ef_limit_num_recharges.ShortcutsEnabled = True
    Me.ef_limit_num_recharges.Size = New System.Drawing.Size(157, 25)
    Me.ef_limit_num_recharges.SufixText = "Sufix Text"
    Me.ef_limit_num_recharges.SufixTextVisible = True
    Me.ef_limit_num_recharges.TabIndex = 5
    Me.ef_limit_num_recharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_num_recharges.TextValue = ""
    Me.ef_limit_num_recharges.TextWidth = 0
    Me.ef_limit_num_recharges.Value = ""
    Me.ef_limit_num_recharges.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_edit_mb
    '
    Me.gb_edit_mb.Controls.Add(Me.ef_card_mb)
    Me.gb_edit_mb.Controls.Add(Me.gb_limits)
    Me.gb_edit_mb.Controls.Add(Me.ef_pin)
    Me.gb_edit_mb.Location = New System.Drawing.Point(3, 3)
    Me.gb_edit_mb.Name = "gb_edit_mb"
    Me.gb_edit_mb.Size = New System.Drawing.Size(342, 217)
    Me.gb_edit_mb.TabIndex = 0
    Me.gb_edit_mb.TabStop = False
    '
    'ef_card_mb
    '
    Me.ef_card_mb.DoubleValue = 0.0R
    Me.ef_card_mb.IntegerValue = 0
    Me.ef_card_mb.IsReadOnly = False
    Me.ef_card_mb.Location = New System.Drawing.Point(121, 184)
    Me.ef_card_mb.Name = "ef_card_mb"
    Me.ef_card_mb.PlaceHolder = Nothing
    Me.ef_card_mb.ShortcutsEnabled = True
    Me.ef_card_mb.Size = New System.Drawing.Size(209, 24)
    Me.ef_card_mb.SufixText = "Sufix Text"
    Me.ef_card_mb.SufixTextVisible = True
    Me.ef_card_mb.TabIndex = 19
    Me.ef_card_mb.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_mb.TextValue = ""
    Me.ef_card_mb.TextWidth = 50
    Me.ef_card_mb.Value = ""
    Me.ef_card_mb.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_limits
    '
    Me.gb_limits.Controls.Add(Me.chk_limit_num_recharges)
    Me.gb_limits.Controls.Add(Me.chk_limit_recharge)
    Me.gb_limits.Controls.Add(Me.chk_limit_by_session)
    Me.gb_limits.Controls.Add(Me.ef_limit_recharge)
    Me.gb_limits.Controls.Add(Me.ef_limit_num_recharges)
    Me.gb_limits.Controls.Add(Me.ef_limit_by_session)
    Me.gb_limits.Location = New System.Drawing.Point(6, 54)
    Me.gb_limits.Name = "gb_limits"
    Me.gb_limits.Size = New System.Drawing.Size(330, 123)
    Me.gb_limits.TabIndex = 1
    Me.gb_limits.TabStop = False
    Me.gb_limits.Text = "xLimits"
    '
    'chk_limit_num_recharges
    '
    Me.chk_limit_num_recharges.AutoSize = True
    Me.chk_limit_num_recharges.Location = New System.Drawing.Point(7, 89)
    Me.chk_limit_num_recharges.Name = "chk_limit_num_recharges"
    Me.chk_limit_num_recharges.Size = New System.Drawing.Size(166, 17)
    Me.chk_limit_num_recharges.TabIndex = 4
    Me.chk_limit_num_recharges.Text = "xchkLimitNumRecharges"
    Me.chk_limit_num_recharges.UseVisualStyleBackColor = True
    '
    'chk_limit_recharge
    '
    Me.chk_limit_recharge.AutoSize = True
    Me.chk_limit_recharge.Location = New System.Drawing.Point(7, 56)
    Me.chk_limit_recharge.Name = "chk_limit_recharge"
    Me.chk_limit_recharge.Size = New System.Drawing.Size(134, 17)
    Me.chk_limit_recharge.TabIndex = 2
    Me.chk_limit_recharge.Text = "xchkLimitRecharge"
    Me.chk_limit_recharge.UseVisualStyleBackColor = True
    '
    'chk_limit_by_session
    '
    Me.chk_limit_by_session.AutoSize = True
    Me.chk_limit_by_session.Location = New System.Drawing.Point(7, 23)
    Me.chk_limit_by_session.Name = "chk_limit_by_session"
    Me.chk_limit_by_session.Size = New System.Drawing.Size(139, 17)
    Me.chk_limit_by_session.TabIndex = 0
    Me.chk_limit_by_session.Text = "xchkLimitBySession"
    Me.chk_limit_by_session.UseVisualStyleBackColor = True
    '
    'chk_lock_mobile_bank
    '
    Me.chk_lock_mobile_bank.AutoSize = True
    Me.chk_lock_mobile_bank.Location = New System.Drawing.Point(16, 229)
    Me.chk_lock_mobile_bank.Name = "chk_lock_mobile_bank"
    Me.chk_lock_mobile_bank.Size = New System.Drawing.Size(123, 17)
    Me.chk_lock_mobile_bank.TabIndex = 1
    Me.chk_lock_mobile_bank.Text = "xLockMobilebank"
    Me.chk_lock_mobile_bank.UseVisualStyleBackColor = True
    '
    'frm_mobile_bank_edit_device
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(454, 268)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_mobile_bank_edit_device"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_mobile_bank_edit_device"
    Me.panel_data.ResumeLayout(False)
    Me.gb_edit_mb.ResumeLayout(False)
    Me.gb_limits.ResumeLayout(False)
    Me.gb_limits.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_pin As GUI_Controls.uc_entry_field
  Friend WithEvents ef_limit_by_session As GUI_Controls.uc_entry_field
  Friend WithEvents ef_limit_recharge As GUI_Controls.uc_entry_field
  Friend WithEvents ef_limit_num_recharges As GUI_Controls.uc_entry_field
  Friend WithEvents gb_edit_mb As System.Windows.Forms.GroupBox
  Friend WithEvents gb_limits As System.Windows.Forms.GroupBox
  Friend WithEvents chk_limit_num_recharges As System.Windows.Forms.CheckBox
  Friend WithEvents chk_limit_recharge As System.Windows.Forms.CheckBox
  Friend WithEvents chk_limit_by_session As System.Windows.Forms.CheckBox
  Friend WithEvents ef_card_mb As GUI_Controls.uc_entry_field
  Friend WithEvents chk_lock_mobile_bank As System.Windows.Forms.CheckBox
End Class
