<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_movements
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.chk_status_retired = New System.Windows.Forms.CheckBox
    Me.chk_status_out_of_order = New System.Windows.Forms.CheckBox
    Me.chk_status_active = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1236, 192)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 196)
    Me.panel_data.Size = New System.Drawing.Size(1236, 447)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1230, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1230, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(237, 72)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xOpen Date"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(249, 6)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(331, 183)
    Me.uc_pr_list.TabIndex = 11
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_status_retired)
    Me.gb_status.Controls.Add(Me.chk_status_out_of_order)
    Me.gb_status.Controls.Add(Me.chk_status_active)
    Me.gb_status.Location = New System.Drawing.Point(586, 10)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(317, 42)
    Me.gb_status.TabIndex = 12
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'chk_status_retired
    '
    Me.chk_status_retired.AutoSize = True
    Me.chk_status_retired.Location = New System.Drawing.Point(211, 20)
    Me.chk_status_retired.Name = "chk_status_retired"
    Me.chk_status_retired.Size = New System.Drawing.Size(74, 17)
    Me.chk_status_retired.TabIndex = 2
    Me.chk_status_retired.Text = "xRetired"
    '
    'chk_status_out_of_order
    '
    Me.chk_status_out_of_order.AutoSize = True
    Me.chk_status_out_of_order.Location = New System.Drawing.Point(88, 20)
    Me.chk_status_out_of_order.Name = "chk_status_out_of_order"
    Me.chk_status_out_of_order.Size = New System.Drawing.Size(117, 17)
    Me.chk_status_out_of_order.TabIndex = 1
    Me.chk_status_out_of_order.Text = "xOut Of Service"
    '
    'chk_status_active
    '
    Me.chk_status_active.AutoSize = True
    Me.chk_status_active.Location = New System.Drawing.Point(14, 20)
    Me.chk_status_active.Name = "chk_status_active"
    Me.chk_status_active.Size = New System.Drawing.Size(68, 17)
    Me.chk_status_active.TabIndex = 0
    Me.chk_status_active.Text = "xActive"
    '
    'frm_terminal_movements
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1246, 647)
    Me.Name = "frm_terminal_movements"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_terminal_movements"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_status_retired As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_out_of_order As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_active As System.Windows.Forms.CheckBox
End Class
