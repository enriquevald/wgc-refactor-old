<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customer_segmentation
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_gender = New System.Windows.Forms.GroupBox
    Me.chk_gender_female = New System.Windows.Forms.CheckBox
    Me.chk_gender_male = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.gb_level = New System.Windows.Forms.GroupBox
    Me.chk_level_04 = New System.Windows.Forms.CheckBox
    Me.chk_level_03 = New System.Windows.Forms.CheckBox
    Me.chk_level_02 = New System.Windows.Forms.CheckBox
    Me.chk_level_01 = New System.Windows.Forms.CheckBox
    Me.lbl_info = New System.Windows.Forms.Label
    Me.chk_only_anonymous = New System.Windows.Forms.CheckBox
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_gender.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.chk_only_anonymous)
    Me.panel_filter.Controls.Add(Me.lbl_info)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_gender)
    Me.panel_filter.Size = New System.Drawing.Size(1053, 258)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gender, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_info, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_anonymous, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 262)
    Me.panel_data.Size = New System.Drawing.Size(1053, 458)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1047, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1047, 4)
    '
    'gb_gender
    '
    Me.gb_gender.Controls.Add(Me.chk_gender_female)
    Me.gb_gender.Controls.Add(Me.chk_gender_male)
    Me.gb_gender.Location = New System.Drawing.Point(314, 99)
    Me.gb_gender.Name = "gb_gender"
    Me.gb_gender.Size = New System.Drawing.Size(170, 57)
    Me.gb_gender.TabIndex = 3
    Me.gb_gender.TabStop = False
    Me.gb_gender.Text = "xGender"
    '
    'chk_gender_female
    '
    Me.chk_gender_female.Location = New System.Drawing.Point(11, 35)
    Me.chk_gender_female.Name = "chk_gender_female"
    Me.chk_gender_female.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_female.TabIndex = 1
    Me.chk_gender_female.Text = "xWomen"
    '
    'chk_gender_male
    '
    Me.chk_gender_male.Location = New System.Drawing.Point(11, 18)
    Me.chk_gender_male.Name = "chk_gender_male"
    Me.chk_gender_male.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_male.TabIndex = 0
    Me.chk_gender_male.Text = "xMen"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(3, 3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(305, 116)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(314, 3)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(170, 93)
    Me.gb_level.TabIndex = 2
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(11, 16)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 0
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(11, 34)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 1
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(11, 53)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 2
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(11, 72)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 3
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'lbl_info
    '
    Me.lbl_info.AutoSize = True
    Me.lbl_info.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_info.Location = New System.Drawing.Point(322, 194)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.Size = New System.Drawing.Size(205, 13)
    Me.lbl_info.TabIndex = 5
    Me.lbl_info.Text = "xShow customer that have activity"
    '
    'chk_only_anonymous
    '
    Me.chk_only_anonymous.AutoSize = True
    Me.chk_only_anonymous.Location = New System.Drawing.Point(325, 163)
    Me.chk_only_anonymous.Name = "chk_only_anonymous"
    Me.chk_only_anonymous.Size = New System.Drawing.Size(130, 17)
    Me.chk_only_anonymous.TabIndex = 4
    Me.chk_only_anonymous.Text = "xOnly Anonymous"
    Me.chk_only_anonymous.UseVisualStyleBackColor = True
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(1, 120)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 1
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_customer_segmentation
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1061, 724)
    Me.Name = "frm_customer_segmentation"
    Me.Text = "frm_customer_segmentation"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_gender.ResumeLayout(False)
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_gender As System.Windows.Forms.GroupBox
  Friend WithEvents chk_gender_female As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_male As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents chk_only_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel


End Class
