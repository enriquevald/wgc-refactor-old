'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_flash_report.vb
' DESCRIPTION:   Flash Report Form
' AUTHOR:        Omar P�rez
' CREATION DATE: 22-JAN-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-JAN-2015  OPC    Initial version
' 09-APR-2015  RMS    Gaming Day
' 15-JUN-2015  FAV    WIG-2381: QTT column changed by Terminals_Connected column and added Terminals_Played column
' 08-AUG-2015  YNM    TFS-2089: Changes in Flash Report look&feel and some formulas. 
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_flash_report
    Inherits GUI_Controls.frm_base_sel

#Region " Constants "

    ' GRID
    Private Const GRID_COLUMN_ELEMENTS As Integer = 0
    Private Const GRID_COLUMN_DETAILS As Integer = 1

    ' FAV 15-JUN-2015
    'Private Const GRID_COLUMN_QTT As Integer = 2
    Private Const GRID_COLUMN_TERMINALS_CONNECTED As Integer = 2
    Private Const GRID_COLUMN_TERMINALS_PLAYED As Integer = 3

    Private Const GRID_COLUMN_CASH_IN As Integer = 4
    Private Const GRID_COLUMN_CASH_OUT As Integer = 5
    Private Const GRID_COLUMN_HOLD As Integer = 6
    Private Const GRID_COLUMN_ID As Integer = 7
    Private Const GRID_COLUMN_AMOUNT As Integer = 8

    ' SQL
    Private Const SQL_COLUMN_HEADER As Integer = 0
    Private Const SQL_COLUMN_COLOR As Integer = 1
    Private Const SQL_COLUMN_ELEMENTS As Integer = 2
    Private Const SQL_COLUMN_DETAILS As Integer = 3

    ' FAV 15-JUN-2015
    'Private Const SQL_COLUMN_QTT As Integer = 4
    Private Const SQL_COLUMN_TERMINALS_CONNECTED As Integer = 4
    Private Const SQL_COLUMN_TERMINALS_PLAYED As Integer = 5

    Private Const SQL_COLUMN_CASH_IN As Integer = 6
    Private Const SQL_COLUMN_CASH_OUT As Integer = 7
    Private Const SQL_COLUMN_HOLD As Integer = 8
    Private Const SQL_COLUMN_ID As Integer = 9
    Private Const SQL_COLUMN_AMOUNT As Integer = 10

    ' COLUMNS
    Private Const GRID_COLUMNS As Integer = 9
    Private Const GRID_HEADER_ROWS As Integer = 1

#End Region

#Region " Members"

    ' For Report
    Private m_date_from As String
    Private m_opening_time As String

#End Region

#Region " Overridable GUI function "

    Public Overrides Sub GUI_SetFormId()
        Me.FormId = ENUM_FORM.FORM_FLASH_REPORT
        Call MyBase.GUI_SetFormId()
    End Sub ' GUI_SetFormId

    Protected Overrides Sub GUI_InitControls()
        ' Required by the class
        Call MyBase.GUI_InitControls()

        ' Form Text
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5863)

        ' set date
        Me.dt_date_from.Text = GLB_NLS_GUI_INVOICING.GetString(401)
        Me.dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, 0)
        Me.gb_date.Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574)

        ' Custom Buttons
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

        Call SetDefaultValues()
        Call GUI_StyleView()
    End Sub ' GUI_InitControls

    Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
        Return ENUM_QUERY_TYPE.CUSTOM_DATATABLE
    End Function ' GUI_GetQueryType

    Protected Overrides Function GUI_GetCustomDataTable() As System.Data.DataTable
        'Return CSReports.FlashReport.GetFlashReport(Me.dt_date_from.Value.AddHours(WSI.Common.Misc.TodayOpening().Hour))
        Dim _date_from As Long = Me.dt_date_from.Value.Date.Ticks + WSI.Common.Misc.TodayOpening().TimeOfDay.Ticks
        Return CSReports.FlashReport.GetFlashReport(_date_from)
    End Function ' GUI_GetCustomDataTable

    Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
        Call MyBase.GUI_SetupRow(RowIndex, DbRow)

        ' Row Color
        If (DbRow.Value(SQL_COLUMN_COLOR) <> CSReports.FlashReport.RowColor.NONE) Then
            For idx_col As Integer = 0 To GRID_COLUMNS - 1
                Select Case (DbRow.Value(SQL_COLUMN_COLOR))
                    Case CSReports.FlashReport.RowColor.ORANGE
                        Me.Grid.Cell(RowIndex, idx_col).BackColor = Color.Orange
                    Case CSReports.FlashReport.RowColor.YELLOW
                        Me.Grid.Cell(RowIndex, idx_col).BackColor = Color.Yellow
                End Select
                If (idx_col = 0) Then
                  Me.Grid.Cell(RowIndex, idx_col).Alignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_CENTER
                End If
            Next
        End If

        ' Name headers
        If (Not DbRow.Value(SQL_COLUMN_HEADER) Is DBNull.Value) Then
            Dim headers_list As List(Of String) = New List(Of String)

            headers_list = DbRow.Value(SQL_COLUMN_HEADER)

            For idx_col As Integer = 0 To GRID_COLUMNS - 1
              Me.Grid.Cell(RowIndex, idx_col).Value = headers_list.Item(idx_col)
              Me.Grid.Cell(RowIndex, idx_col).Alignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_CENTER
            Next
        End If

        ' Format cells
        If Not DbRow.Value(SQL_COLUMN_CASH_IN) Is DBNull.Value AndAlso IsNumeric(Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value, 2)
        End If

        If Not DbRow.Value(SQL_COLUMN_CASH_OUT) Is DBNull.Value AndAlso IsNumeric(Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_OUT).Value) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_OUT).Value = GUI_FormatCurrency(Grid.Cell(RowIndex, GRID_COLUMN_CASH_OUT).Value, 2)
        End If

        If Not DbRow.Value(SQL_COLUMN_HOLD) Is DBNull.Value AndAlso IsNumeric(Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLD).Value) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLD).Value = GUI_FormatNumber(Grid.Cell(RowIndex, GRID_COLUMN_HOLD).Value, 2) + "%"
        End If

        If Not DbRow.Value(SQL_COLUMN_AMOUNT) Is DBNull.Value AndAlso IsNumeric(Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value, 2)
        End If

        Return True
    End Function ' GUI_SetupRow

    Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)
        Select Case ButtonId
            Case ENUM_BUTTON.BUTTON_FILTER_RESET
                Call SetDefaultValues()
            Case Else
                MyBase.GUI_ButtonClick(ButtonId)
        End Select
    End Sub ' GUI_ButtonClick

#End Region

#Region " GUI Reports"

    Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
        PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(401), m_date_from)
    End Sub ' GUI_ReportFilter

    Protected Overrides Sub GUI_ReportUpdateFilters()
        m_date_from = dt_date_from.Value & " " & m_opening_time
    End Sub ' GUI_ReportUpdateFilters

    Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, Optional ByVal FirstColIndex As Integer = 0)
        Call MyBase.GUI_ReportParams(PrintData)
        PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT
    End Sub ' GUI_ReportParams

#End Region

#Region " Private Methods/Functions"

    Private Sub SetDefaultValues()
        Dim _today_opening As Date

        _today_opening = WSI.Common.Misc.TodayOpening()
        dt_date_from.Value = _today_opening.Date.AddDays(-1)
        m_opening_time = GUI_FormatTime(_today_opening, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End Sub ' SetDefaultValues

    Private Sub GUI_StyleView()

        Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

        ' ELEMENTS
        Me.Grid.Column(GRID_COLUMN_ELEMENTS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5472)
        Me.Grid.Column(GRID_COLUMN_ELEMENTS).Width = 3000
        Me.Grid.Column(GRID_COLUMN_ELEMENTS).Mapping = SQL_COLUMN_ELEMENTS

        ' DETAILS
        Me.Grid.Column(GRID_COLUMN_DETAILS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2307)
        Me.Grid.Column(GRID_COLUMN_DETAILS).Width = 4000
        Me.Grid.Column(GRID_COLUMN_DETAILS).Mapping = SQL_COLUMN_DETAILS
        Me.Grid.Column(GRID_COLUMN_DETAILS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        '' QTT
        'Me.Grid.Column(GRID_COLUMN_QTT).Header.Text = String.Empty
        'Me.Grid.Column(GRID_COLUMN_QTT).Width = 1260
        'Me.Grid.Column(GRID_COLUMN_QTT).Mapping = SQL_COLUMN_QTT
        'Me.Grid.Column(GRID_COLUMN_QTT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' FAV 15-JUN-2015
        ' TERMINALS_CONNECTED
        Me.Grid.Column(GRID_COLUMN_TERMINALS_CONNECTED).Header.Text = String.Empty
        Me.Grid.Column(GRID_COLUMN_TERMINALS_CONNECTED).Width = 1155
        Me.Grid.Column(GRID_COLUMN_TERMINALS_CONNECTED).Mapping = SQL_COLUMN_TERMINALS_CONNECTED
        Me.Grid.Column(GRID_COLUMN_TERMINALS_CONNECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' FAV 15-JUN-2015
        ' TERMINALS_PLAYED
        Me.Grid.Column(GRID_COLUMN_TERMINALS_PLAYED).Header.Text = String.Empty
        Me.Grid.Column(GRID_COLUMN_TERMINALS_PLAYED).Width = 1155
        Me.Grid.Column(GRID_COLUMN_TERMINALS_PLAYED).Mapping = SQL_COLUMN_TERMINALS_PLAYED
        Me.Grid.Column(GRID_COLUMN_TERMINALS_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' CASH IN
        Me.Grid.Column(GRID_COLUMN_CASH_IN).Header.Text = String.Empty
        Me.Grid.Column(GRID_COLUMN_CASH_IN).Width = 1155
        Me.Grid.Column(GRID_COLUMN_CASH_IN).Mapping = SQL_COLUMN_CASH_IN
        Me.Grid.Column(GRID_COLUMN_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' CASH OUT
        Me.Grid.Column(GRID_COLUMN_CASH_OUT).Header.Text = String.Empty
        Me.Grid.Column(GRID_COLUMN_CASH_OUT).Width = 1155
        Me.Grid.Column(GRID_COLUMN_CASH_OUT).Mapping = SQL_COLUMN_CASH_OUT
        Me.Grid.Column(GRID_COLUMN_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' HOLD
        Me.Grid.Column(GRID_COLUMN_HOLD).Header.Text = String.Empty
        Me.Grid.Column(GRID_COLUMN_HOLD).Width = 1000
        Me.Grid.Column(GRID_COLUMN_HOLD).Mapping = SQL_COLUMN_HOLD
        Me.Grid.Column(GRID_COLUMN_HOLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' ID
        Me.Grid.Column(GRID_COLUMN_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5707)
        Me.Grid.Column(GRID_COLUMN_ID).Width = 500
        Me.Grid.Column(GRID_COLUMN_ID).Mapping = SQL_COLUMN_ID
        Me.Grid.Column(GRID_COLUMN_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' AMOUNT
        Me.Grid.Column(GRID_COLUMN_AMOUNT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
        Me.Grid.Column(GRID_COLUMN_AMOUNT).Width = 1800
        Me.Grid.Column(GRID_COLUMN_AMOUNT).Mapping = SQL_COLUMN_AMOUNT
        Me.Grid.Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End Sub ' GUI_StyleView

#End Region

#Region " Public Methods/Functions "

    Public Sub ShowForEdit(ByVal MidiParent As System.Windows.Forms.IWin32Window)

        Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
        Me.MdiParent = MidiParent
        Me.Display(False)

    End Sub ' ShowForEdit

#End Region

End Class