'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_ads_configuration.vb
' DESCRIPTION:   Advertisement configuration
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 17-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-MAY-2012  MPO    Initial version
' 06-AGO-2012  ANG    Add Field Type column
' 02-OCT-2012  JMM    Release's additional changes
' 03-OCT-2012  JMM    Backcolor error fixed on player fields sort
' 03-OCT-2012  JMM    More release's changes & fixes
' 03-OCT-2012  JMM    Funcionalities grouping
' 09-OCT-2012  JMM    Messages tab added
' 18-OCT-2012  JMM    Schedule tab added
' 27-NOV-2012  JMM    Level images & gift split added 
' 14-OCT-2013  JMM    Added SCHEDULE_PROMOTIONS functionality
' 17-JUL-2015  YNM    Fixed Bug WIG-2330: Promobox collection error 
' 06-AUG-2015  ETP    Fixed BUG 3595 Added logic for show messages by mode.
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_ads_configuration
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  ' GRID IMAGES DEFINITIONS
  Private Const GRID_NUM_COL_IMAGES As Integer = 4
  Private Const GRID_COLUMN_IMAGE_ID As Integer = 0
  Private Const GRID_COLUMN_IMAGE_NAME As Integer = 1
  Private Const GRID_COLUMN_IMAGE_RESOURCE_ID As Integer = 2
  Private Const GRID_COLUMN_IMAGE_CUSTOMIZED As Integer = 3


  ' GRID FIELDS DEFINITION
  Private Const GRID_NUM_COL_FIELD As Integer = 10 ' NUMBER OF COLUMNS
  Private Const GRID_COLUMN_COLOR_CODE As Integer = 0
  Private Const GRID_COLUMN_FIELD_ID As Integer = 1
  Private Const GRID_COLUMN_FIELD_NAME As Integer = 2
  Private Const GRID_COLUMN_FIELD_MIN_LENGTH As Integer = 3
  Private Const GRID_COLUMN_FIELD_MAX_LENGTH As Integer = 4
  Private Const GRID_COLUMN_FIELD_SYSTEM_READ_ONLY As Integer = 5
  Private Const GRID_COLUMN_FIELD_SHOWN As Integer = 6
  Private Const GRID_COLUMN_FIELD_EDITABLE As Integer = 7
  Private Const GRID_COLUMN_FIELD_TYPE As Integer = 8
  Private Const GRID_COLUMN_FIELD_ORDER As Integer = 9

  ' GRID FUNCTIONALITITES DEFINITIONS
  Private Const GRID_NUM_COL_FUNC As Integer = 3 ' NUMER OF COLUMNS
  Private Const GRID_COLUMN_FUNC_ID As Integer = 0
  Private Const GRID_COLUMN_FUNC_NAME As Integer = 1
  Private Const GRID_COLUMN_FUNC_ENABLED As Integer = 2

  ' GRID MESSAGES DEFINITIONS
  Private Const GRID_NUM_COL_MSG As Integer = 3 ' NUMER OF COLUMNS
  Private Const GRID_COLUMN_MSG_ID As Integer = 0
  Private Const GRID_COLUMN_MSG_NAME As Integer = 1
  Private Const GRID_COLUMN_MSG_TEXT As Integer = 2

  Private Const GRID_CHECKED_COLUMN_WIDTH As Integer = 1250

  Enum ENUM_MOVE_ROW_DIRECTION
    UP
    DOWN
  End Enum
#End Region

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ADS_CONFIGURATION

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId
  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1283)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Call Me.GUI_StyleImages()
    Call Me.GUI_StyleFields()
    Call Me.GUI_StyleFunctionalities()
    Call Me.GUI_StyleMessages()

    Me.tab_gifts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(464)                      '464 Gifts
    Me.tab_fields.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(753)                     '753 Player Info"

    Me.tab_functionalities.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(736)            '736 Functionalities
    Me.tab_Image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(742)                      '742 Pictures

    Me.gb_next_gifts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(738)                  '738 Next Gifts
    Me.ef_percent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)

    Me.chk_split_gifts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1445)               '1445 Categories gift split
    Me.lbl_category_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1449)             '1449 Category name
    Me.lbl_category_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1551)           '1551 Button
    Me.ef_category_1_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1446)            '1446 Category 1
    Me.ef_category_1_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 30)
    Me.ef_category_1_short.Text = ""
    Me.ef_category_1_short.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_category_2_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1447)            '1447 Category 2
    Me.ef_category_2_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 30)
    Me.ef_category_2_short.Text = ""
    Me.ef_category_2_short.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_category_3_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1448)            '1448 Category 3
    Me.ef_category_3_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 30)
    Me.ef_category_3_short.Text = ""
    Me.ef_category_3_short.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.lbl_split_caption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1552)             '1552 - Category 1 includes Redeemable and Non-redeemable gifts.\n - Category 2 includes Services gifts.\n - Category 3 includes Objects and Draw Tickets gifts.

    Call chk_split_gifts_CheckedChanged(Me, Nothing)

    Me.ef_min_gifts.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 1)
    Me.ef_unit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)

    Call img_preview.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)

    Me.lb_functionality_description.Text = ""
    Me.lb_img_description.Text = ""


    ' CONFIGURE SAVE BUTTON

    With Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0)
      .Visible = True
      .Enabled = Permissions.Write
      .Text = GLB_NLS_GUI_CONTROLS.GetString(13) ' Label button save
    End With

    ' Up&Down buttons
    With Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1)
      .Visible = True
      .Enabled = True
      .Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1204) ' Move down
    End With

    With Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2)
      .Visible = True
      .Enabled = True
      .Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1203) ' Move Up
    End With

    Me.lbl_percentage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(799) ' Percentage
    Me.lbl_min_2_show.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(800) ' Minium items to show
    Me.lbl_max_2_show.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(801) ' Max items to show

    Me.lbl_read_only.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.lbl_read_only.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.lbl_read_only_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1386) ' System no editable fields

    Me.gb_schedule.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1429) '1429 Schedule
    Me.dtp_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
    Me.dtp_time_from.ShowUpDown = True
    Me.dtp_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.dtp_time_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)
    Me.dtp_time_to.ShowUpDown = True
    Me.dtp_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    'Messages Tab
    Me.tab_messages.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1422) '1422 Messages

    'Voucher Tab
    Me.gb_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1433)      ' 1433 "Ticket de recarga total"
    Me.tab_voucher_header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(228) ' 228  "Cabecera"
    Me.tab_voucher_footer.Text = GLB_NLS_GUI_CONFIGURATION.GetString(229) ' 229  "Pie"

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _promobox_config As CLS_ADS_CONFIGURATION
    Dim _idx_row As Integer
    Dim _nls_name As String
    Dim _group_nls As String
    Dim _group_id As Integer
    Dim _num_groups As Integer
    Dim _num_items_in_group As Integer
    Dim _groups As Collection

    _promobox_config = DbReadObject

    _groups = New Collection()
    _num_groups = 0

    ' Main screen functionalities
    _num_groups += 1
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.DRAW_NUMBERS).ToString)
    ' Upper screen functionalities
    _num_groups += 1
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.DRAWS).ToString)
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.PROMOTIONS).ToString)
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.ADVERTISEMENTS).ToString)
    ' Activity functionalities
    _num_groups += 1
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.ACTIVITY_ONLY_POINTS).ToString)
    ' Schedule functionalities
    _num_groups += 1
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.SCHEDULE_GIFT_REQUEST).ToString)
    If Not TITO.Utils.IsTitoMode() Then
      _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.SCHEDULE_RECHARGES).ToString)
    End If
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.SCHEDULE_DRAW_NUMBERS).ToString)
    _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.SCHEDULE_PROMOTIONS).ToString)
    ' Printing functionalities    
    If Not TITO.Utils.IsTitoMode() Then
      _num_groups += 1
      _groups.Add(_num_groups, Convert.ToInt32(PromoBOX.Functionality.PRINT_RECHARGES_TICKET).ToString)
    End If
    ' Make sure to add only those functionalities  
    For Each _functionality As CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY In _promobox_config.Functionalities
      If _groups.Contains(Convert.ToInt32(_functionality.function_id).ToString) Then
        _functionality.group_id = _groups.Item(Convert.ToInt32(_functionality.function_id).ToString)
      End If
    Next

    For _group_id = 1 To _num_groups
      Try
        ' Group Header
        _idx_row = Me.dg_functionalities.AddRow()

        With Me.dg_functionalities
          .Row(_idx_row).IsMerged = True

          _group_nls = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2020 - 1 + _group_id)

          .Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value = 0
          .Cell(_idx_row, GRID_COLUMN_FUNC_NAME).Value() = _group_nls
          .Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value() = _group_nls

          .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        End With

        ' Group Functionalities
        _num_items_in_group = 0

        For Each _functionality As CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY In _promobox_config.Functionalities
          If _group_id = _functionality.group_id Then
            _idx_row = Me.dg_functionalities.AddRow()

            _nls_name = CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY.GetName(_functionality.function_id)

            With Me.dg_functionalities
              .Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value = _functionality.function_id
              .Cell(_idx_row, GRID_COLUMN_FUNC_NAME).Value = _nls_name
              .Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value = BoolToGridValue(_functionality.enabled)
            End With

            _num_items_in_group += 1

          End If
        Next

        ' Remove the group name in case it contains no entries
        If _num_items_in_group = 0 Then
          With Me.dg_functionalities
            .DeleteRow(.CurrentRow)
          End With
        End If

      Catch ex As Exception

      End Try
    Next

    If Me.dg_functionalities.NumRows > 1 Then
      ' JMM 03-OCT-2012: Select, just loaded, the first non group item to show its help text
      Me.dg_functionalities.IsSelected(1) = True
      Me.dg_functionalities_RowSelectedEvent(1)
    End If

    ' FIELDS
    For Each _field As CLS_ADS_CONFIGURATION.TYPE_FIELD_INFO In _promobox_config.Fields

      _idx_row = Me.dg_fields.AddRow()
      _nls_name = CLS_ADS_CONFIGURATION.TYPE_FIELD_INFO.GetName(_field.field_id)

      With Me.dg_fields
        .Cell(_idx_row, GRID_COLUMN_FIELD_ID).Value = _field.field_id
        .Cell(_idx_row, GRID_COLUMN_FIELD_NAME).Value = _nls_name
        .Cell(_idx_row, GRID_COLUMN_FIELD_MIN_LENGTH).Value = _field.min_length
        .Cell(_idx_row, GRID_COLUMN_FIELD_MAX_LENGTH).Value = _field.max_length
        .Cell(_idx_row, GRID_COLUMN_FIELD_SHOWN).Value = BoolToGridValue(_field.shown)
        .Cell(_idx_row, GRID_COLUMN_FIELD_SYSTEM_READ_ONLY).Value = Me.BoolToGridValue(Not _field.system_editable)
        .Cell(_idx_row, GRID_COLUMN_FIELD_TYPE).Value = _field.type
        .Cell(_idx_row, GRID_COLUMN_FIELD_ORDER).Value = _field.order

        If _field.system_editable Then
          .Cell(_idx_row, GRID_COLUMN_FIELD_EDITABLE).Value = EnableCheck(_field.user_editable, _field.shown)
        Else
          .Cell(_idx_row, GRID_COLUMN_FIELD_EDITABLE).Value = EnableCheck(.Cell(_idx_row, GRID_COLUMN_FIELD_EDITABLE), False)
          .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
        End If
      End With
    Next

    ' IMAGES
    For Each _image As CLS_ADS_CONFIGURATION.WKT_Image In _promobox_config.Images

      _idx_row = Me.dg_images.AddRow()

      _nls_name = CLS_ADS_CONFIGURATION.WKT_Image.GetImageName(_image.image_id)

      With Me.dg_images
        .Cell(_idx_row, GRID_COLUMN_IMAGE_ID).Value = _image.image_id
        .Cell(_idx_row, GRID_COLUMN_IMAGE_NAME).Value = _nls_name
        .Cell(_idx_row, GRID_COLUMN_IMAGE_RESOURCE_ID).Value = _image.resource_id

        If _image.resource_id <> 0 Then
          .Cell(_idx_row, GRID_COLUMN_IMAGE_CUSTOMIZED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278) '278 "S�"
        Else
          .Cell(_idx_row, GRID_COLUMN_IMAGE_CUSTOMIZED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279) '279 "No"
        End If

      End With

    Next

    If (dg_images.NumRows > 0) Then
      Call dg_images.SelectFirstRow(True)
      Call dg_images_RowSelectedEvent(0)
      Me.m_form_loading = False
      dg_images.CurrentRow = 0
    End If

    ' GIFTS
    If _promobox_config.Gifts.next_gifts_config.percentage.HasValue AndAlso _promobox_config.Gifts.next_gifts_config.percentage.Value > 0 Then
      Me.ef_percent.Value = _promobox_config.Gifts.next_gifts_config.percentage
      Me.chk_Percent.Checked = True
    Else
      Me.ef_percent.Value = Nothing
      Me.ef_min_gifts.Enabled = False
      Me.chk_Percent.Checked = False
    End If

    If _promobox_config.Gifts.next_gifts_config.min_to_show.HasValue _
       AndAlso IsNumeric(_promobox_config.Gifts.next_gifts_config.min_to_show) Then
      Me.ef_min_gifts.Value = _promobox_config.Gifts.next_gifts_config.min_to_show
    Else
      Me.ef_min_gifts.Value = Nothing
    End If

    If _promobox_config.Gifts.next_gifts_config.max_to_show.HasValue AndAlso _promobox_config.Gifts.next_gifts_config.max_to_show.Value > 0 Then
      Me.ef_unit.Value = _promobox_config.Gifts.next_gifts_config.max_to_show
      Me.chk_Max.Checked = True
    Else
      Me.chk_Max.Checked = False
      Me.ef_unit.Value = Nothing
    End If

    Me.chk_split_gifts.Checked = _promobox_config.Gifts.gifts_split.enabled
    Me.ef_category_1_name.Value = _promobox_config.Gifts.gifts_split.category_1_name
    Me.ef_category_2_name.Value = _promobox_config.Gifts.gifts_split.category_2_name
    Me.ef_category_3_name.Value = _promobox_config.Gifts.gifts_split.category_3_name
    Me.ef_category_1_short.Value = _promobox_config.Gifts.gifts_split.category_1_short
    Me.ef_category_2_short.Value = _promobox_config.Gifts.gifts_split.category_2_short
    Me.ef_category_3_short.Value = _promobox_config.Gifts.gifts_split.category_3_short

    Call ef_Gifts_EntryFieldValueChanged()

    ' MESSAGES
    For Each _message As CLS_ADS_CONFIGURATION.TYPE_MESSAGE In _promobox_config.Messages
      With Me.dg_messages
        If Not IsMessageShowed(_message.msg_id) Then
          Continue For
        End If

        _idx_row = .AddRow()

        .Cell(_idx_row, GRID_COLUMN_MSG_ID).Value = _message.msg_id
        .Cell(_idx_row, GRID_COLUMN_MSG_NAME).Value = CLS_ADS_CONFIGURATION.TYPE_MESSAGE.GetMessageName(_message.msg_id)
        .Cell(_idx_row, GRID_COLUMN_MSG_TEXT).Value = _message.text

      End With
    Next

    If Me.dg_messages.NumRows > 0 Then
      Me.dg_messages.CurrentRow = 0
      Me.dg_messages.CurrentCol = 0
      Me.dg_messages_RowSelectedEvent(0)
      Call dg_messages.SelectFirstRow(True)
    Else
      Me.lbl_message_help.Text = ""
    End If

    ' SCHEDULE
    Try
      Me.dtp_time_from.Value = GUI_FormatDate(DateTime.Today) + " " + _promobox_config.Schedule.from_time

    Catch ex As Exception

    End Try

    Try
      Me.dtp_time_to.Value = GUI_FormatDate(DateTime.Today) + " " + _promobox_config.Schedule.to_time

    Catch ex As Exception

    End Try

    ' RECHARGES VOUCHER CONF
    Me.txt_voucher_header.Text = _promobox_config.RechargesVoucherConf.header
    Me.txt_voucher_footer.Text = _promobox_config.RechargesVoucherConf.footer

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _promobox_config As CLS_ADS_CONFIGURATION
    Dim _idx_row As Integer
    Dim _field As CLS_ADS_CONFIGURATION.TYPE_FIELD_INFO
    Dim _functionality As CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY
    Dim _message As CLS_ADS_CONFIGURATION.TYPE_MESSAGE
    Dim _id_list As Integer

    _promobox_config = Me.DbEditedObject

    Try

      For _idx_row = 0 To Me.dg_fields.NumRows - 1
        _field = New CLS_ADS_CONFIGURATION.TYPE_FIELD_INFO()
        With _field
          .field_id = Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_ID).Value
          .shown = GridValueToBool(Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_SHOWN).Value)
          .user_editable = GridValueToBool(Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_EDITABLE).Value)
          .min_length = Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_MIN_LENGTH).Value
          .max_length = Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_MAX_LENGTH).Value
          .type = Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_TYPE).Value
          .order = Me.dg_fields.Cell(_idx_row, GRID_COLUMN_FIELD_ORDER).Value
        End With

        _id_list = _promobox_config.Fields.IndexOf(_field)
        If _id_list >= 0 Then
          _promobox_config.Fields(_id_list) = _field
        Else
          _promobox_config.Fields.Add(_field)
        End If
      Next

      For _idx_row = 0 To Me.dg_functionalities.NumRows - 1

        If Me.dg_functionalities.Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value <> 0 Then

          _functionality = New CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY
          With _functionality
            .function_id = Me.dg_functionalities.Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value
            .enabled = GridValueToBool(Me.dg_functionalities.Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value)
          End With

          _id_list = _promobox_config.Functionalities.IndexOf(_functionality)
          If _id_list >= 0 Then
            _promobox_config.Functionalities(_id_list) = _functionality
          Else
            _promobox_config.Functionalities.Add(_functionality)
          End If

        End If
      Next


      ' IMAGES
      ' Changes in the images are stored directly in DbEditedObject.Images list
      ' ( Screen only shows one picture from list )
      ' See cls_ads_configuration

      ' GIFTS
      If Me.chk_Percent.Checked Then
        _promobox_config.Gifts.next_gifts_config.percentage = CInt(ef_percent.Value)
      Else
        _promobox_config.Gifts.next_gifts_config.percentage = 0
      End If

      If String.IsNullOrEmpty(ef_min_gifts.Value) Then
        _promobox_config.Gifts.next_gifts_config.min_to_show = 0
      Else
        _promobox_config.Gifts.next_gifts_config.min_to_show = CInt(ef_min_gifts.Value)
      End If

      If Me.chk_Max.Checked Then
        _promobox_config.Gifts.next_gifts_config.max_to_show = CInt(Me.ef_unit.Value)
      Else
        _promobox_config.Gifts.next_gifts_config.max_to_show = 0
      End If

      _promobox_config.Gifts.gifts_split.enabled = Me.chk_split_gifts.Checked
      _promobox_config.Gifts.gifts_split.category_1_name = Me.ef_category_1_name.Value
      _promobox_config.Gifts.gifts_split.category_2_name = Me.ef_category_2_name.Value
      _promobox_config.Gifts.gifts_split.category_3_name = Me.ef_category_3_name.Value
      _promobox_config.Gifts.gifts_split.category_1_short = Me.ef_category_1_short.Value
      _promobox_config.Gifts.gifts_split.category_2_short = Me.ef_category_2_short.Value
      _promobox_config.Gifts.gifts_split.category_3_short = Me.ef_category_3_short.Value

      ' MESSAGES
      For _idx_row = 0 To Me.dg_messages.NumRows - 1
        _message = New CLS_ADS_CONFIGURATION.TYPE_MESSAGE

        _message.msg_id = Me.dg_messages.Cell(_idx_row, GRID_COLUMN_MSG_ID).Value
        _message.text = Me.dg_messages.Cell(_idx_row, GRID_COLUMN_MSG_TEXT).Value

        _id_list = _promobox_config.Messages.IndexOf(_message)
        If _id_list >= 0 Then
          _promobox_config.Messages(_id_list) = _message
        Else
          _promobox_config.Messages.Add(_message)
        End If
      Next

      ' SCHEDULE
      _promobox_config.Schedule.from_time = GUI_FormatTime(Me.dtp_time_from.Value, ENUM_FORMAT_TIME.FORMAT_HHMM)
      _promobox_config.Schedule.to_time = GUI_FormatTime(Me.dtp_time_to.Value, ENUM_FORMAT_TIME.FORMAT_HHMM)

      ' RECHARGES VOUCHER CONF
      _promobox_config.RechargesVoucherConf.header = Me.txt_voucher_header.Text
      _promobox_config.RechargesVoucherConf.footer = Me.txt_voucher_footer.Text

      Me.DbEditedObject = _promobox_config

    Catch ex As Exception

    End Try

  End Sub 'GUI_GetScreenData
  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        Me.DbEditedObject = New CLS_ADS_CONFIGURATION
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub 'GUI_DB_Operation
  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Me.chk_Percent.Checked AndAlso GUI_ParseNumber(Me.ef_percent.Value) = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1396), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.ef_percent.Focus()

      Return False
    End If

    If Me.chk_Max.Checked AndAlso IsNotNullAndNotZero(Me.ef_unit.Value) _
      And Me.chk_Percent.Checked AndAlso IsNotNullAndNotZero(Me.ef_min_gifts.Value) _
      And GUI_ParseNumber(Me.ef_unit.Value) < GUI_ParseNumber(Me.ef_min_gifts.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1389), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.ef_unit.Focus()

      Return False
    End If

    If Me.chk_Max.Checked AndAlso GUI_ParseNumber(Me.ef_unit.Value) = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1395), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.ef_unit.Focus()

      Return False
    End If

    Return True
  End Function 'GUI_IsScreenDataOk()
  ' PURPOSE: Handle Form Key events
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: True / False 
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Return True  ' Handled true , and don't trigger default behavior
  End Function 'GUI_KeyPress
  ' PURPOSE: Handle Form buttons cliks
  '
  '  PARAMS:
  '     - INPUT: Clicked button object
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0

        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

        ' Trick!
        ' Restart images status
        ' Necessary to detect new changes in images and to audit image changes  
        For Each _db_obj As CLS_ADS_CONFIGURATION In New List(Of CLS_ADS_CONFIGURATION)(New CLS_ADS_CONFIGURATION() {DbReadObject, DbEditedObject})
          For Each _image As CLS_ADS_CONFIGURATION.WKT_Image In _db_obj.Images
            _image.image_cached = Nothing
            _image.image_deleted = False
            _image.image_has_changes = False
          Next
        Next

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 ' Button DOWN

        If Not Me.dg_fields.SelectedRows Is Nothing Then
          Call MoveSelectedRows(ENUM_MOVE_ROW_DIRECTION.DOWN)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_2 ' Button UP

        If Not Me.dg_fields.SelectedRows Is Nothing Then
          Call MoveSelectedRows(ENUM_MOVE_ROW_DIRECTION.UP)
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick
  ' PURPOSE: Perform any clean-up , release used memory in image cache
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_Exit()
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

#End Region

#Region " Members "

  Private m_cell_cache_value As Object
  Private m_internal_image_loading As Boolean

  ' Avoids loading all images at boot
  Private m_form_loading As Boolean = True

#End Region

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE:  Set the columns from Images Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleImages()

    With Me.dg_images
      Call .Init(GRID_NUM_COL_IMAGES, 1)

      ' ID
      .Column(GRID_COLUMN_IMAGE_ID).Width = 0 ' Not visible

      ' Image
      .Column(GRID_COLUMN_IMAGE_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(752) ' Picture
      .Column(GRID_COLUMN_IMAGE_NAME).Width = 2900 ' 2520

      'GRID_COLUMN_IMAGE_RESOURCE_ID
      .Column(GRID_COLUMN_IMAGE_RESOURCE_ID).Width = 0

      'GRID_COLUMN_IMAGE_HAS_CHANGES
      With .Column(GRID_COLUMN_IMAGE_CUSTOMIZED)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(757) ' Enabled
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        .Width = GRID_CHECKED_COLUMN_WIDTH
        .Editable = False
      End With

    End With

  End Sub 'GUI_StyleImages
  ' PURPOSE:  Set the columns from Fields Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleFields()

    With Me.dg_fields
      Call .Init(GRID_NUM_COL_FIELD, 1, True)

      ' Color code
      With .Column(GRID_COLUMN_COLOR_CODE)
        .Header.Text = ""
        .Width = 200
        .HighLightWhenSelected = False
      End With

      ' Field
      With .Column(GRID_COLUMN_FIELD_ID)
        .Width = 0
      End With

      With .Column(GRID_COLUMN_FIELD_NAME)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(737) ' Field
        .Width = 7660
      End With

      ' Only read
      With .Column(GRID_COLUMN_FIELD_SYSTEM_READ_ONLY)
        .Width = 0
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(832) ' Read Only
        .Editable = False
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      ' Show
      With .Column(GRID_COLUMN_FIELD_SHOWN)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(754) ' Show
        .Width = GRID_CHECKED_COLUMN_WIDTH
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      ' Editable
      With .Column(GRID_COLUMN_FIELD_EDITABLE)
        .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(755) ' Edit
        .Width = GRID_CHECKED_COLUMN_WIDTH
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      ' Min length
      With .Column(GRID_COLUMN_FIELD_MIN_LENGTH)
        .Width = 0              ' HIDDE

      End With
      ' Max length
      With .Column(GRID_COLUMN_FIELD_MAX_LENGTH)
        .Width = 0            ' HIDDE 
      End With

      ' Type
      With .Column(GRID_COLUMN_FIELD_TYPE)
        .Width = 0            ' HIDDE
      End With

      ' Order
      With .Column(GRID_COLUMN_FIELD_ORDER)
        .Width = 0            ' HIDDE
      End With
    End With

  End Sub 'GUI_StyleFields()
  ' PURPOSE:  Set the columns from Functionalities Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleFunctionalities()

    With Me.dg_functionalities
      Call .Init(GRID_NUM_COL_FUNC, 1, True)

      .Column(GRID_COLUMN_FUNC_ID).Width = 0

      ' Functionalities
      .Column(GRID_COLUMN_FUNC_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(756) ' Functionality
      .Column(GRID_COLUMN_FUNC_NAME).Width = 2900

      ' Enabled
      .Column(GRID_COLUMN_FUNC_ENABLED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(757) ' Enabled
      .Column(GRID_COLUMN_FUNC_ENABLED).Width = GRID_CHECKED_COLUMN_WIDTH
      .Column(GRID_COLUMN_FUNC_ENABLED).Editable = True
      .Column(GRID_COLUMN_FUNC_ENABLED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

    End With

  End Sub 'GUI_StyleFunctionalities()

  ' PURPOSE:  Set the columns from Messages Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleMessages()

    With Me.dg_messages
      Call .Init(GRID_NUM_COL_MSG, 1, True)

      'Id
      .Column(GRID_COLUMN_MSG_ID).Width = 0

      ' Name
      .Column(GRID_COLUMN_MSG_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1427) '1427 "Case"
      .Column(GRID_COLUMN_MSG_NAME).Width = 2200
      .Column(GRID_COLUMN_MSG_NAME).HighLightWhenSelected = True

      ' Text
      .Column(GRID_COLUMN_MSG_TEXT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428) '1428 "Message"
      .Column(GRID_COLUMN_MSG_TEXT).Width = 4800
      .Column(GRID_COLUMN_MSG_TEXT).Editable = True
      .Column(GRID_COLUMN_MSG_TEXT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_MSG_TEXT).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MSG_TEXT).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLS_ADS_CONFIGURATION.TYPE_MESSAGE.MSG_MAX_LENGTH)

    End With

  End Sub 'GUI_StyleMessages()


  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue
  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function 'GridValueToBool
  ' PURPOSE: Enable - Disable check control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function EnableCheck(ByVal cell As uc_grid.CLASS_CELL_DATA, ByVal enabled As Boolean) As String

    Dim _return As String
    _return = ""

    Select Case cell.Value
      Case uc_grid.GRID_CHK_CHECKED
        _return = IIf(enabled, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_CHECKED_DISABLED)

      Case uc_grid.GRID_CHK_CHECKED_DISABLED
        _return = IIf(enabled, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_CHECKED_DISABLED)

      Case uc_grid.GRID_CHK_UNCHECKED
        _return = IIf(enabled, uc_grid.GRID_CHK_UNCHECKED, uc_grid.GRID_CHK_UNCHECKED_DISABLED)

      Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
        _return = IIf(enabled, uc_grid.GRID_CHK_UNCHECKED, uc_grid.GRID_CHK_UNCHECKED_DISABLED)

    End Select

    Return _return

  End Function 'EnableCheck
  ' PURPOSE: Enable - Disable check control 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function EnableCheck(ByVal Checked As Boolean, ByVal Enable As Boolean) As String

    Dim _ret_code As String
    _ret_code = Nothing


    If Checked Then
      _ret_code = IIf(Enable, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_CHECKED_DISABLED)
    ElseIf Not Checked Then
      _ret_code = IIf(Enable, uc_grid.GRID_CHK_UNCHECKED, uc_grid.GRID_CHK_UNCHECKED_DISABLED)

    End If

    Return _ret_code

  End Function 'EnableCheck
  ' PURPOSE:  Get image instance from Resource ID
  '
  '  PARAMS:
  '     - INPUT:
  '           - Integer : ResourceId 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Image object
  Private Function GetImage(ByVal ResourceId As Integer) As Image

    Dim _sql_transaction As SqlClient.SqlTransaction
    Dim _image As Image
    _sql_transaction = Nothing
    _image = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _image = Nothing
      End If

      If Not WSI.Common.WKTResources.Load(ResourceId, _image, _sql_transaction) Then
        _image = Nothing
      End If

    Catch ex As Exception
      _image = Nothing
    End Try

    Return _image

  End Function 'GetImage

  ' PURPOSE: Returns true is string is not null and not 0
  '
  '  PARAMS:
  '     - INPUT:
  '           - Integer : 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Image object
  Private Function IsNotNullAndNotZero(ByVal str As String) As Boolean

    If String.IsNullOrEmpty(str) Then
      Return False
    ElseIf IsNumeric(str) AndAlso CInt(str) <> 0 Then
      Return True
    Else
      Return False
    End If

  End Function
  ' PURPOSE: Move Row Up or Down 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Direction to move row 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Image object
  Private Sub MoveSelectedRows(ByVal Direction As ENUM_MOVE_ROW_DIRECTION)
    Dim _grid As uc_grid
    Dim _rows_to_move() As Integer
    Dim _idx_row_to As Integer
    Dim _idx_current_row As Integer
    Dim _idx As Integer

    Dim _initial As Integer
    Dim _end_for As Integer
    Dim _step As Integer

    _grid = Me.dg_fields
    _rows_to_move = _grid.SelectedRows

    If (_rows_to_move Is Nothing) Then
      Return
    End If

    Select Case Direction
      Case ENUM_MOVE_ROW_DIRECTION.DOWN
        _end_for = 0
        _initial = _rows_to_move.Length - 1
        _step = -1
      Case ENUM_MOVE_ROW_DIRECTION.UP
        _end_for = _rows_to_move.Length - 1
        _initial = 0
        _step = 1
    End Select

    For _idx = _initial To _end_for Step _step

      _idx_current_row = _rows_to_move(_idx)

      If (_idx_current_row = 0 And Direction = ENUM_MOVE_ROW_DIRECTION.UP) Then
        Exit For
      End If

      If (_idx_current_row = (_grid.NumRows - 1) And Direction = ENUM_MOVE_ROW_DIRECTION.DOWN) Then
        Exit For
      End If

      _idx_row_to = IIf(Direction = ENUM_MOVE_ROW_DIRECTION.DOWN, _idx_current_row + 1, _idx_current_row - 1)

      Call SwapRow(_grid, _idx_current_row, _idx_row_to)

      dg_fields.Row(_idx_current_row).IsSelected = False
      Call dg_fields.RepaintRow(_idx_current_row)
      dg_fields.Row(_idx_row_to).IsSelected = True
      Call dg_fields.RepaintRow(_idx_row_to)

    Next

    Me.dg_fields.Redraw = True

  End Sub

  ' PURPOSE: Swap data between two rows 
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRowFrom ( Row to be moved ) , idxRowTo ( Row to be replaced )
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     NOTE : Swap all data between two rows , EXCEPT PIF_ORDER ;)
  Private Sub SwapRow(ByRef _grid As uc_grid, ByVal IdxRowFrom As Integer, ByVal IdxRowTo As Integer)

    Dim _tmp_data(_grid.NumColumns - 1) As Object
    Dim _idx As Integer
    Dim _from_row_color As Color

    For _idx = 0 To _grid.NumColumns - 1
      ' Ignore pif_order field
      If (_idx = GRID_COLUMN_FIELD_ORDER) Then
        Continue For
      End If
      _tmp_data(_idx) = _grid.Cell(IdxRowTo, _idx).Value
      _grid.Cell(IdxRowTo, _idx).Value = _grid.Cell(IdxRowFrom, _idx).Value
      _grid.Cell(IdxRowFrom, _idx).Value = _tmp_data(_idx)
    Next

    _from_row_color = _grid.Row(IdxRowFrom).BackColor
    _grid.Row(IdxRowFrom).BackColor = _grid.Row(IdxRowTo).BackColor
    _grid.Row(IdxRowTo).BackColor = _from_row_color

  End Sub
  ' PURPOSE: Move No Visible Row To Bottom  NOT USED!
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub MoveNoVisibleToBottom()

    Dim _current_index As Integer
    _current_index = Me.dg_fields.SelectedRows(0)
    _current_index += 1

    While (GridValueToBool(Me.dg_fields.Cell(_current_index, GRID_COLUMN_FIELD_SHOWN).Value))
      MoveSelectedRows(ENUM_MOVE_ROW_DIRECTION.DOWN)
      _current_index += 1
    End While

  End Sub

  ' PURPOSE: Move No Visible Row To Top   NOT USED!
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub MoveVisibleToTop()

    Dim _current_index As Integer
    _current_index = Me.dg_fields.SelectedRows(0)
    _current_index -= 1

    While (Not GridValueToBool(Me.dg_fields.Cell(_current_index, GRID_COLUMN_FIELD_SHOWN).Value))

      MoveSelectedRows(ENUM_MOVE_ROW_DIRECTION.UP)
      _current_index -= 1

    End While


  End Sub

  ' PURPOSE: Gets Schedule From Time & Schedule To Time and builds the shcedule help text
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub BuildScheduleHelpText()

    Dim _from_string_param As String
    Dim _to_string_param As String
    Dim _msg_id As Integer

    _from_string_param = GUI_FormatTime(Me.dtp_time_from.Value, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _to_string_param = GUI_FormatTime(Me.dtp_time_to.Value, ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.dtp_time_from.Value.TimeOfDay.TotalSeconds = Me.dtp_time_to.Value.TimeOfDay.TotalSeconds Then
      '1432 "Scheduled functionalities will be available all time."  
      _msg_id = 1432
    ElseIf Me.dtp_time_from.Value.TimeOfDay.TotalSeconds > Me.dtp_time_to.Value.TimeOfDay.TotalSeconds Then
      '1431 "Scheduled functionalities will be available only from %1 to %2 the next day."
      _msg_id = 1431
    Else
      '1430 "Scheduled functionalities will be available only from %1 to %2."
      _msg_id = 1430
    End If

    Me.lbl_schedule_help.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_msg_id, _from_string_param, _to_string_param)

  End Sub ' BuildScheduleHelpText

  ' PURPOSE: Gets Schedule From Time & Schedule To Time and builds the shcedule help text
  '
  '  PARAMS:
  '     - INPUT: 
  '         Msg_id: Id of the message to show
  '     - OUTPUT:
  '
  ' RETURNS: 
  '       TRUE if the message id has to be show in the current mode.

  Private Function IsMessageShowed(ByVal Msg_id As Integer)

    Dim _bln_show_message As Boolean

    Select Case (Msg_id)
      Case 1000 To 1002
        ' Messages: "Anonymous card", "Recharge not available", "Warning on recharge"
        _bln_show_message = Not WSI.Common.TITO.Utils.IsTitoMode()
      Case Else
        _bln_show_message = True

    End Select

    Return _bln_show_message

  End Function ' IsMessageShowed

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE:  Handle RowSelectedEvent event
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_images_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_images.RowSelectedEvent

    Dim _image_resource_id As Integer
    Dim _current As CLS_ADS_CONFIGURATION
    Dim _image_to_load As Image

    If m_form_loading Then
      Return
    End If

    _image_to_load = Nothing
    _current = Me.DbEditedObject

    If (dg_images.NumRows < 0) Then
      Exit Sub
    End If

    _image_resource_id = Me.dg_images.Cell(SelectedRow, GRID_COLUMN_IMAGE_RESOURCE_ID).Value

    If IsNothing(_current.Images(SelectedRow).image_cached) Then

      If _image_resource_id <= 0 Then       ' It's a default picture
        _image_to_load = Nothing ' No picture
      Else
        _image_to_load = GetImage(_image_resource_id)
        _current.Images(SelectedRow).image_cached = _image_to_load
      End If
    Else
      _image_to_load = _current.Images(SelectedRow).image_cached
    End If

    m_internal_image_loading = True
    Me.img_preview.Image = _image_to_load
    m_internal_image_loading = False

    Me.lb_img_description.Text = CLS_ADS_CONFIGURATION.WKT_Image.GetDescription(SelectedRow)



  End Sub 'dg_images_RowSelectedEvent

  ' PURPOSE:  Handle OnKeydown Event
  ' Accept changes in grid when user press Intro
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub Form_OnKeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown


    Select Case e.KeyCode

      Case Keys.Enter

        If Me.ActiveControl.Equals(Me.dg_fields) Then
          Me.dg_fields.Validate()
        End If

      Case Keys.Escape
        Me.Close()


    End Select

  End Sub

  ' PURPOSE: img_preview_ImageChanged
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:  
  '           -  OldImage As System.Drawing.Image
  '           -  NewImage As System.Drawing.Image 
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Private Sub img_preview_ImageChanged(ByVal OldImage As System.Drawing.Image, ByVal NewImage As System.Drawing.Image) Handles img_preview.ImageChanged

    Dim _current_config As CLS_ADS_CONFIGURATION
    Dim _selected_row As Integer
    Dim _current_image As CLS_ADS_CONFIGURATION.WKT_Image

    _current_config = Me.DbEditedObject
    _selected_row = Me.dg_images.CurrentRow
    '_row_selected = Me.dg_images.SelectedRows(0)

    ' Ignore when the change is not done by the user
    If m_internal_image_loading Then
      Exit Sub
    End If

    If _selected_row < 0 Then
      Exit Sub
    End If


    _current_image = _current_config.Images(_selected_row)

    If NewImage Is Nothing Then

      '
      ' Image Deleted
      '

      _current_image.image_has_changes = True
      _current_image.image_cached = Nothing
      _current_image.image_deleted = True
      _current_image.resource_id = 0

      dg_images.Cell(_selected_row, GRID_COLUMN_IMAGE_RESOURCE_ID).Value = 0
      dg_images.Cell(_selected_row, GRID_COLUMN_IMAGE_CUSTOMIZED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279) '279 "No"

    Else

      '
      ' Image Load or Updated
      '
      _current_image.image_cached = NewImage
      _current_image.image_has_changes = True
      _current_image.image_deleted = False
      dg_images.Cell(_selected_row, GRID_COLUMN_IMAGE_CUSTOMIZED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278) '278 "S�"

    End If


    'End If

  End Sub  'img_preview_ImageDeleted


  ' PURPOSE:  Handle cell data changes to validate data
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_fields_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_fields.CellDataChangedEvent


    Dim _changed_cell As uc_grid.CLASS_CELL_DATA
    Dim _aux_cell As uc_grid.CLASS_CELL_DATA
    Dim _is_editable As Boolean
    Dim _checked As Boolean

    _changed_cell = Me.dg_fields.Cell(Row, Column)
    _checked = GridValueToBool(_changed_cell.Value)
    _is_editable = Not Me.GridValueToBool(dg_fields.Cell(Row, GRID_COLUMN_FIELD_SYSTEM_READ_ONLY).Value)

    Select Case Column

      Case GRID_COLUMN_FIELD_SHOWN

        _aux_cell = Me.dg_fields.Cell(Row, GRID_COLUMN_FIELD_EDITABLE)

        ' Move fields to bottom or top when are enabled / disabled
        'If (_checked) Then
        '  Call MoveVisibleToTop()
        'Else
        '  Call MoveNoVisibleToBottom()
        'End If


        If _is_editable Then
          _aux_cell.Value = Me.EnableCheck(_aux_cell, GridValueToBool(_changed_cell.Value))
        End If


    End Select


  End Sub 'dg_fields_CellDataChangedEvent

  ' PURPOSE: Before start editing grid 
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Private Sub dg_fields_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_fields.BeforeStartEditionEvent

    Dim _changed_cell As uc_grid.CLASS_CELL_DATA
    Dim _aux_cell As uc_grid.CLASS_CELL_DATA
    Dim _is_Editable As Boolean = False
    _changed_cell = Me.dg_fields.Cell(Row, Column)


    _is_Editable = Not Me.GridValueToBool(dg_fields.Cell(Row, GRID_COLUMN_FIELD_SYSTEM_READ_ONLY).Value)


    Select Case Column

      Case GRID_COLUMN_FIELD_EDITABLE ', GRID_COLUMN_FIELD_MIN_LENGTH
        _aux_cell = Me.dg_fields.Cell(Row, GRID_COLUMN_FIELD_SHOWN)
        If Not _is_Editable Then
          EditionCanceled = True
        End If
        If Not GridValueToBool(_aux_cell.Value) Then
          EditionCanceled = True ' Yo can't edit a disabled cell ;)
        End If

    End Select

    Me.m_cell_cache_value = Me.dg_fields.Cell(Row, Column).Value

  End Sub 'dg_fields_BeforeStartEditionEvent

  ' PURPOSE: chkGifts_CheckedChanged
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           -  sender As System.Object,  e As System.EventArgs, 
  '     - OUTPUT:    
  '           - 
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Private Sub chkGifts_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_Percent.CheckedChanged, chk_Max.CheckedChanged


    Dim _check_control As CheckBox
    Dim _control As Control
    _check_control = CType(sender, CheckBox)
    _control = Nothing

    Select Case _check_control.Name

      Case Me.chk_Max.Name
        _control = Me.ef_unit
        Call Me.ef_Gifts_EntryFieldValueChanged()
      Case Me.chk_Percent.Name
        Call Me.ef_Gifts_EntryFieldValueChanged()
        _control = Me.ef_percent

        ef_min_gifts.Enabled = chk_Percent.Checked AndAlso IsNotNullAndNotZero(Me.ef_percent.Value)
    End Select

    _control.Enabled = _check_control.Checked


  End Sub 'chkGifts_CheckedChanged


  ' PURPOSE: ef_Gifts_EntryFieldValueChanged
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - None
  '     - OUTPUT:    
  '           - None
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Private Sub ef_Gifts_EntryFieldValueChanged() Handles ef_percent.EntryFieldValueChanged, ef_unit.EntryFieldValueChanged, ef_min_gifts.EntryFieldValueChanged

    Dim _str_sample As System.Text.StringBuilder
    Dim _str_sample_text As String
    Dim _show_all_gifts As Boolean = True

    Dim _initial_points As WSI.Common.Points
    Dim _final_points As WSI.Common.Points
    Dim _aux As Decimal

    If GUI_ParseNumber(Me.ef_unit.Value) > 90 Then
      Me.ef_unit.Value = 90
    End If

    If Me.chk_Max.Checked And Me.chk_Percent.Checked _
      And GUI_ParseNumber(Me.ef_unit.Value) < GUI_ParseNumber(Me.ef_min_gifts.Value) Then
      Me.ef_min_gifts.Value = GUI_ParseNumber(Me.ef_unit.Value)
    End If

    _str_sample = New System.Text.StringBuilder()

    _initial_points = 10000

    ef_min_gifts.Enabled = chk_Percent.Checked AndAlso IsNotNullAndNotZero(Me.ef_percent.Value)

    '_str_sample.Append("En un jugador con 100 puntos, se mostrar�n,")
    _str_sample.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(805, _initial_points.ToString())) ' Para un jugador con un saldo de 100 puntos de mostrar�

    If Me.chk_Percent.Checked AndAlso IsNotNullAndNotZero(Me.ef_percent.Value) Then

      If IsNotNullAndNotZero(Me.ef_percent.Value) Then
        ' Regalos con valor entre %1 y %2
        _aux = _initial_points
        _aux = _aux * (1.0 + CInt(Me.ef_percent.Value) / 100)
        _final_points = _aux
        _str_sample.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(804, _final_points.ToString())) ' con un valor hasta
      End If


      ' S�lo mostrar si maximo numero de regalos es mayor que m�nimo de regalos a mostrar
      If IsNotNullAndNotZero(Me.ef_min_gifts.Value) Then

        If Me.chk_Max.Checked AndAlso IsNotNullAndNotZero(Me.ef_unit.Value) Then
          If (CInt(Me.ef_unit.Value) >= (CInt(Me.ef_min_gifts.Value))) Then
            _str_sample.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(803, Me.ef_min_gifts.Value)) ' Se intentar� mostrar un m�nimo de %1 regalos
          End If
        Else
          _str_sample.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(803, Me.ef_min_gifts.Value)) ' Se intentar� mostrar un m�nimo de %1 regalos
        End If

      End If

    End If


    If Me.chk_Max.Checked AndAlso IsNotNullAndNotZero(Me.ef_unit.Value) Then
      _str_sample.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(826, Me.ef_unit.Value)) ' Hasta un m�ximo 
    End If

    If (Not Me.chk_Percent.Checked Or (Not IsNotNullAndNotZero(Me.ef_percent.Value) And (Not IsNotNullAndNotZero(Me.ef_min_gifts.Value)))) _
            AndAlso (Not Me.chk_Max.Checked Or Not IsNotNullAndNotZero(Me.ef_unit.Value)) _
            Or (Me.chk_Percent.Checked And Not IsNotNullAndNotZero(Me.ef_percent.Value) And Not IsNotNullAndNotZero(Me.ef_unit.Value)) Then
      _str_sample.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(825)) ' Todos Los regalos
    End If

    _str_sample_text = _str_sample.ToString()
    Me.lbl_demo.Text = _str_sample_text

  End Sub 'ef_Gifts_EntryFieldValueChanged

  ' PURPOSE: Insert new line every X chars without split words
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           -  chars As Short, 
  '     - OUTPUT:    
  '           -  str As String, 
  '                  
  ' RETURNS:         
  '     -None
  '                  
  Private Sub InsertNewLine(ByRef str As String, ByVal chars As Short)

    Dim _idx As Integer
    Dim _idx_char As Integer = 0
    Dim _offset As Integer = 0

    For _idx = 0 To str.Length
      If _idx_char = chars Then
        If str.IndexOf(Chr(32), _idx) >= 0 Then
          _idx = str.IndexOf(Chr(32), _idx)
          If _idx < str.Length Then
            str = str.Substring(0, _idx) & System.Environment.NewLine & str.Substring(_idx + 1, str.Length - _idx - 1)
          End If
        End If
        _idx_char = 0
      Else
        _idx_char = _idx_char + 1
      End If
    Next

  End Sub 'InsertCr


  Private Sub dg_functionalities_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_functionalities.RowSelectedEvent

    Dim _str_description As String
    Dim _idx_functionality As Short

    If Short.TryParse(Me.dg_functionalities.Cell(SelectedRow, GRID_COLUMN_FUNC_ID).Value, _idx_functionality) Then
      _str_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(CLS_ADS_CONFIGURATION.TYPE_FUNCTIONALITY.GetDescription(_idx_functionality))
      Me.lb_functionality_description.Text = _str_description
    End If



  End Sub

  Private Sub tab_config_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_config.SelectedIndexChanged

    Dim _show_up_down_buttons As Boolean
    Dim _tb As TabControl

    _tb = sender

    _show_up_down_buttons = _tb.SelectedTab.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(753)  ' Player Info"
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = _show_up_down_buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = _show_up_down_buttons

  End Sub

  Private Sub dg_messages_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_messages.RowSelectedEvent
    Dim _str_description As String
    Dim _idx_message As Short

    If Short.TryParse(Me.dg_messages.Cell(SelectedRow, GRID_COLUMN_MSG_ID).Value, _idx_message) Then
      _str_description = CLS_ADS_CONFIGURATION.TYPE_MESSAGE.GetMessageHelp(_idx_message)
      Me.lbl_message_help.Text = _str_description
    End If

  End Sub

  Private Sub dtp_time_from_DatePickerValueChanged() Handles dtp_time_from.DatePickerValueChanged
    Call BuildScheduleHelpText()
  End Sub

  Private Sub dtp_time_to_DatePickerValueChanged() Handles dtp_time_to.DatePickerValueChanged
    Call BuildScheduleHelpText()
  End Sub

  Private Sub chk_split_gifts_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_split_gifts.CheckedChanged
    Me.gb_gifts_split.Enabled = Me.chk_split_gifts.Checked
  End Sub

#End Region ' Events

End Class