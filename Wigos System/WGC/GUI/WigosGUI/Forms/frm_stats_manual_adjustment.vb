'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_stats_manual_adjustment
' DESCRIPTION:   
' AUTHOR:        Humberto Braojos & Joaquim Cid
' CREATION DATE: 27-SEP-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 02-AUG-2012 HBB & JC    First Release
' 12-SEP-2013 RMS & ANG   Fixed Bug WIG-202 Implemented GUI_Closing method
' 19-NOV-2013 LJM    Changes for auditing forms
' 20-JAN-2015 FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 08-ABR-2015 FOS    TASK 973: Add terminal name
' 17-MAR-2016 SDS    PBI 10610: Add button export
' 15-MAY-2017 FGB    Bug 27301: Ajuste Manual de Estadísticas no muestra datos de Sesiones cuando se ha cambiado de nombre el terminal
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.frm_base_sel
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common.Misc

Public Class frm_stats_manual_adjustment
  Inherits frm_base_print

#Region "Constants"
  Dim SQL_TERMINAL_NAME As Int16 = 0
  Dim SQL_BASE_DAY As Int16 = 1
  Dim SQL_BASE_HOUR As Int16 = 2
  Dim SQL_GAME_ID As Int16 = 3
  Dim SQL_GAME_NAME As Int16 = 4
  Dim SQL_NUM_PLAYED As Int16 = 5
  Dim SQL_PLAYED As Int16 = 6
  Dim SQL_NUM_WON As Int16 = 7
  Dim SQL_WON As Int16 = 8
  Dim SQL_NETWIN As Int16 = 9
  Dim SQL_PS_NUM_PLAYED As Int16 = 10
  Dim SQL_PS_PLAYED As Int16 = 11
  Dim SQL_PS_NUM_WON As Int16 = 12
  Dim SQL_PS_WON As Int16 = 13
  Dim SQL_PS_CASH_IN As Int16 = 14
  Dim SQL_PS_CASH_OUT As Int16 = 15
  Dim SQL_PS_NETWIN_PL_WO As Int16 = 16
  Dim SQL_PS_NETWIN_CI_CO As Int16 = 17

  Dim SQL_COLUMN As Int16 = 18        ' TOTAL COLUMNS

  Dim GRID_TERMINAL_NAME As Int16 = 0
  Dim GRID_BASE_DAY As Int16 = 1
  Dim GRID_BASE_HOUR As Int16 = 2
  Dim GRID_GAME_ID As Int16 = 3
  Dim GRID_PLAYED As Int16 = 4
  Dim GRID_WON As Int16 = 5
  Dim GRID_NETWIN As Int16 = 6
  Dim GRID_NUM_PLAYED As Int16 = 7
  Dim GRID_NUM_WON As Int16 = 8
  Dim GRID_GAME_NAME As Int16 = 9

  Dim GRID_PS_CASH_IN As Int16 = 10          ' Only used on GRID1 GROUPED BY DAY, MAYBE ON GRID2 GROUPED BY HOUR
  Dim GRID_PS_CASH_OUT As Int16 = 11
  Dim GRID_PS_NETWIN_CI_CO As Int16 = 12
  Dim GRID_PS_PLAYED As Int16 = 13
  Dim GRID_PS_WON As Int16 = 14
  Dim GRID_PS_NETWIN_PL_WO As Int16 = 15
  Dim GRID_PS_NUM_PLAYED As Int16 = 16
  Dim GRID_PS_NUM_WON As Int16 = 17

  Dim GRID_COLUMNS As Int16 = 10              ' TOTAL COLUMNS
  Dim GRID_COLUMNS_INCLUDED_PS As Int16 = 18  ' TOTAL COLUMNS WITH PS
  Dim GRID_HEADERS As Int16 = 2               ' NUM OF TOTAL HEADERS
#End Region

  ' Variables
  Dim m_date_from As DateTime
  Dim m_date_to As DateTime
  Dim m_terminal_id As Int32
  Dim m_terminal_name As String = ""
  Dim m_date_to_filter As String = ""

  Dim m_dt_spd = New DataTable          ' DataTabel For Grid Hour
  Dim m_dt_sph = New DataTable          ' DataTable For Grid for Hours
  Dim m_dt_sph_game = New DataTable     ' DataTable For Grid Game

  Dim m_dt_sph_ps = New DataTable       ' DataTable For Play Sessions Values ReadOnly
  Dim m_dt_spd_ps = New DataTable       ' DataTable For Play Sessions Values ReadOnly

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STATS_MANUAL_ADJUSTMENT
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1363)
    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList(), uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL)

    'Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_CONTROLS.GetString(31)      'APPLY
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write

    'SDS 17-03-2016: PBI 10610 add button export
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)

    uc_dsl.Init(0, True, False)

    GUI_StyleSheet()

    dg_days.PanelRightVisible = False
    dg_hour.PanelRightVisible = False
    dg_games.PanelRightVisible = False

    GUI_FilterReset()

    lbl_NetWin1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1391)
    lbl_NetWin2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1392)

    uc_terminal_name.Value = "---"
    uc_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1394)
    uc_terminal_name.TextVisible = True

  End Sub

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call Button_Search()

        _print_data = New GUI_Reports.CLASS_PRINT_DATA
        Call GUI_ReportFilter(_print_data)
        Call MyBase.AuditFormClick(ButtonId, _print_data)

      Case ENUM_BUTTON.BUTTON_NEW
        SaveChanges()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_games.ContainsFocus Then
      Return Me.dg_games.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  'PURPOSE: Executed just before closing form.
  '         
  ' PARAMS:
  '    - INPUT: 
  '         - CloseCanceled: Send user decision to caller process
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    Dim _rows_modified() As DataRow

    _rows_modified = Nothing
    If (TestChanged(m_dt_sph_game, True, _rows_modified)) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), _
        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
        ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        CloseCanceled = True
      End If
    End If

  End Sub 'GUI_Closing

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to_filter)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1394), m_terminal_name)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  'SDS 17-03-2016: PBI 10610 add button export
  Protected Overrides Sub GUI_ExcelResultList()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition
    Dim m_print_datetime As DateTime

    _prev_cursor = Me.Cursor
    Cursor = Cursors.WaitCursor

    Try
      m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime

        'grid 1
        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = Me.FormTitle
        _sheet_layout.Name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7188)
        _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
        UpdateGridDays()
        _uc_grid_def.UcGrid = dg_days
        _uc_grid_def.Title = uc_terminal_name.Value 'revisarrr
        _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        .ListOfSheets.Add(_sheet_layout)

        'grid2
        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = Me.FormTitle
        _sheet_layout.Name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7189)
        _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
        UpdateGrid(dg_hour, m_dt_sph.Select(" GAME_NAME IS NOT NULL "), GRID_COLUMNS_INCLUDED_PS)
        _uc_grid_def.UcGrid = dg_hour
        _uc_grid_def.Title = uc_terminal_name.Value 'revisarrr
        _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        .ListOfSheets.Add(_sheet_layout)

        'grid3
        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = Me.FormTitle
        _sheet_layout.Name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7190)
        _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
        UpdateGrid(dg_games, m_dt_sph_game.Select(" GAME_NAME IS NOT NULL "), GRID_COLUMNS)
        _uc_grid_def.UcGrid = dg_games
        _uc_grid_def.Title = uc_terminal_name.Value 'revisarrr
        _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        .ListOfSheets.Add(_sheet_layout)
      End With
      'goto inital conditions

      'export
      _report_layout.GUI_GenerateExcel()
      uc_grid_days_RowSelectedEvent(0)
    Finally
      Cursor = _prev_cursor
    End Try
  End Sub ' GUI_ExcelResultList

#End Region

#Region "Public"
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal TerminalId As Integer, ByVal DateFrom As Date, ByVal DateTo As Date)

    Me.MdiParent = MdiParent
    Me.Display(False)

    If Not Me.Permissions.Read Then

      Return
    End If
    SetFilter(TerminalId, DateFrom, DateTo)

    DoSearch()

  End Sub ' ShowForEdit

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit
#End Region

#Region "Private"

  ' '' PURPOSE: Load grids of tabs
  ' ''
  ' ''  PARAMS:
  ' ''     - INPUT:
  ' ''           - None 
  ' ''     - OUTPUT:
  ' ''           - None
  ' ''
  ' '' RETURNS:
  ' ''     - None
  ''Private Sub LoadGrids()

  ''  For _i As Integer = 0 To Me.tab_segmentation.TabPages.Count - 1
  ''    Call ShowTable(_i)
  ''  Next

  ''End Sub ' LoadGrids

  ' '' PURPOSE: Show the grid according with tabpage selected
  ' ''
  ' ''  PARAMS:
  ' ''     - INPUT:
  ' ''           - IndexTable Index the tabpage selected
  ' ''     - OUTPUT:
  ' ''           - None
  ' ''
  ' '' RETURNS:
  ' ''     - None
  ''Private Sub ShowTable(ByVal IndexTable As Integer)
  ''  Dim _table As DataTable
  ''  Dim _column As Integer
  ''  _table = Nothing
  ''  ' Load style
  ''  Call GUI_StyleSheet()

  ''  TableToGrid(_column, Me.tab_segmentation.TabPages(IndexTable).Controls(IDX_GRID_TABPAGE))
  ''  ' Update the array (tables shown)
  ''  m_loaded_tables(IndexTable) = True
  ''  End If

  ''  End If
  ''End Sub ' ShowTable

  ' '' PURPOSE: Set values of grid
  ' ''
  ' ''  PARAMS:
  ' ''     - INPUT:
  ' ''           - Index: Index from tabpage selected
  ' ''           - DgSegmentation: DataGrid from Tabpage
  ' ''     - OUTPUT:
  ' ''           - None
  ' ''
  ' '' RETURNS:
  ' ''     - None
  ''Private Sub TableToGrid(ByVal Index As Integer, _
  ''                        ByRef DgSegmentation As GUI_Controls.uc_grid)

  ''  Dim _idx_row As Integer
  ''  Dim _idx_column As Integer
  ''  Dim _idx_column_table As String
  ''  Dim _redraw As Boolean
  ''  Dim _total As Decimal
  ''  Dim _al_media As ArrayList
  ''  Dim _total_adt As Decimal 'total netwin teórico / visita.
  ''  Dim _found_level_rows As Boolean
  ''  Dim _count_month As Integer

  ''  _al_media = New ArrayList()
  ''  _total = 0
  ''  _idx_column_table = ""
  ''  _total_adt = 0

  ''  Select Case Index
  ''    Case 2
  ''      _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO
  ''    Case 3
  ''      _idx_column_table = Reports.SR_SQL_COLUMN_TD_THEORETICAL_HOLD
  ''    Case 4
  ''      _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS
  ''    Case 5
  ''      _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT
  ''  End Select

  ''  _redraw = DgSegmentation.Redraw
  ''  DgSegmentation.Redraw = False
  ''  _found_level_rows = False

  ''  Try
  ''    _idx_column = GRID_COLUMN_MONTH_1
  ''    For Each _dt_month_values As DataTable In m_ds.Tables
  ''      _idx_row = 0
  ''      For Each _dr_agroup_month_values As DataRow In _dt_month_values.Rows

  ''        ' Add new row
  ''        If _idx_column = GRID_COLUMN_MONTH_1 Then
  ''          DgSegmentation.AddRow()
  ''          _idx_row = DgSegmentation.NumRows - 1
  ''        End If

  ''        ' Agroup (level, game type, gender,etc..)
  ''        If _dr_agroup_month_values.Table.Columns.Contains(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) Then
  ''          If IsDBNull(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME)) Then
  ''            If IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = "---"
  ''            End If
  ''          Else
  ''            '4 posibility row: Total, empty line, Title and normal line
  ''            If _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "TOTAL" Then
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(423)  'SUBTOTAL:  
  ''              DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  ''            ElseIf _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "EMPTY" Then
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = ""
  ''              DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
  ''            ElseIf Me.m_ht_names_subtables.ContainsKey(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME)) Then
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = Me.m_ht_names_subtables(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME))
  ''              DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
  ''            Else
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME)
  ''              DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
  ''            End If
  ''          End If
  ''        End If

  ''        ' Months (13)
  ''        If _dr_agroup_month_values.Table.Columns.Contains(_idx_column_table) Then
  ''          If IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
  ''            'TOTAL values
  ''            If _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "TOTAL" Then

  ''              If _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO OrElse _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS Then
  ''                DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatNumber(_total, 0)
  ''              Else
  ''                If _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT Then
  ''                  _total_adt = CalculateTotalADT(_dt_month_values, _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_TYPE))
  ''                  DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatCurrency(_total_adt, 2)
  ''                Else
  ''                  DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatCurrency(_total, 2)
  ''                End If

  ''              End If

  ''            Else
  ''              DgSegmentation.Cell(_idx_row, _idx_column).Value = ""
  ''            End If

  ''          Else
  ''            'Months values
  ''            If _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO OrElse _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS Then
  ''              DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatNumber(_dr_agroup_month_values(_idx_column_table), 0)
  ''            Else
  ''              DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatCurrency(_dr_agroup_month_values(_idx_column_table), 2)
  ''            End If
  ''            _total += _dr_agroup_month_values(_idx_column_table)
  ''          End If
  ''        End If

  ''        ' Calculate media
  ''        If _idx_column = GRID_COLUMN_MONTH_1 Then
  ''          If Not IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
  ''            _al_media.Add(CType(_dr_agroup_month_values(_idx_column_table), Decimal))
  ''          ElseIf _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "TOTAL" Then
  ''            If _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT Then
  ''              _al_media.Add(_total_adt)
  ''            Else
  ''              _al_media.Add(_total)
  ''            End If
  ''            _total = 0
  ''          Else
  ''            _al_media.Add(0)
  ''          End If

  ''        Else
  ''          If Not IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
  ''            _al_media(_idx_row) += _dr_agroup_month_values(_idx_column_table)
  ''          Else
  ''            If _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT Then
  ''              _al_media(_idx_row) += _total_adt
  ''            Else
  ''              _al_media(_idx_row) += _total
  ''            End If

  ''            _total = 0
  ''          End If

  ''        End If

  ''        ' Set media
  ''        If _idx_column = GRID_COLUMN_MONTH_13 Then
  ''          '  rows: Total and normal line.
  ''          If Not IsDBNull(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_TYPE)) Then

  ''            _count_month = 0

  ''            For _count_current_mounth As Integer = GRID_COLUMN_MONTH_1 To GRID_COLUMN_MONTH_13
  ''              If DgSegmentation.Cell(_idx_row, _count_current_mounth).Value > 0 Then
  ''                _count_month += 1
  ''              End If
  ''            Next

  ''            If _count_month = 0 Then
  ''              _count_month = MONTHS_NUMBER
  ''            End If

  ''            If _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO OrElse _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS Then
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_PROMEDIO).Value = GUI_FormatNumber(_al_media(_idx_row) / _count_month, 0)
  ''            Else
  ''              DgSegmentation.Cell(_idx_row, GRID_COLUMN_PROMEDIO).Value = GUI_FormatCurrency(_al_media(_idx_row) / _count_month, 2)
  ''            End If

  ''          Else
  ''            DgSegmentation.Cell(_idx_row, GRID_COLUMN_PROMEDIO).Value = ""
  ''          End If
  ''        End If

  ''        If _idx_column <> GRID_COLUMN_MONTH_1 Then
  ''          _idx_row += 1
  ''        End If
  ''      Next
  ''      _idx_column += 1

  ''    Next

  ''  Catch ex As Exception

  ''  Finally
  ''    DgSegmentation.Redraw = _redraw
  ''  End Try

  ''End Sub ' TableToGrid



  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date

    Me.uc_dsl.ToDate = _final_time
    ' Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    'Me.uc_dsl.FromDateSelected = False
    Me.uc_dsl.ClosingTime = _closing_time
    Me.uc_dsl.ClosingTimeEnabled = False

    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  Private Sub GUI_StyleSheet(ByRef Grid As uc_grid)

    With Grid
      .Column(GRID_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)      'Estadísticas
      .Column(GRID_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1387)     'Jugado
      .Column(GRID_PLAYED).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_WON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)         'Estadísticas
      .Column(GRID_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1388)        'Ganado
      .Column(GRID_WON).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_NUM_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)  'Estadísticas
      .Column(GRID_NUM_PLAYED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)        'Jugadas
      .Column(GRID_NUM_PLAYED).Width = GRID_COLUMN_AMOUNT_WIDTH - 500
      .Column(GRID_NUM_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_NUM_WON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)     'Estadísticas
      .Column(GRID_NUM_WON).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(262)           'Ganadas
      .Column(GRID_NUM_WON).Width = GRID_COLUMN_AMOUNT_WIDTH - 500
      .Column(GRID_NUM_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(549)      'Estadísticas
      .Column(GRID_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)            'Net Win
      .Column(GRID_NETWIN).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub

  Private Sub GUI_StyleSheet()

    With Me.dg_days
      .Clear()
      .Init(GRID_COLUMNS_INCLUDED_PS, GRID_HEADERS, True)

      .Column(GRID_TERMINAL_NAME).Header(0).Text = ""
      .Column(GRID_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)  'Terminal
      .Column(GRID_TERMINAL_NAME).Width = GRID_COLUMN_FIRST_WIDTH - 700
      .Column(GRID_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      .Column(GRID_BASE_DAY).Header(0).Text = ""
      .Column(GRID_BASE_DAY).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(302)               'xFecha xFechaSinHora
      .Column(GRID_BASE_DAY).Width = GRID_COLUMN_FIRST_WIDTH - 700
      .Column(GRID_BASE_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_BASE_HOUR).Header(0).Text = ""
      .Column(GRID_BASE_HOUR).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(302)              'Fecha
      .Column(GRID_BASE_HOUR).Width = 0
      .Column(GRID_BASE_HOUR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_GAME_ID).Header(0).Text = ""
      .Column(GRID_GAME_ID).Header(1).Text = ""                                                 'IdJuego
      .Column(GRID_GAME_ID).Width = 0
      .Column(GRID_GAME_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_GAME_NAME).Header(0).Text = ""
      .Column(GRID_GAME_NAME).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(211)              'Juego
      .Column(GRID_GAME_NAME).Width = 0
      .Column(GRID_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PLAYED).Editable = False
      .Column(GRID_PLAYED).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_PLAYED).EditionControl.EntryField.IsReadOnly = True
      .Column(GRID_PLAYED).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 20, 2)

      .Column(GRID_WON).Editable = False
      .Column(GRID_WON).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_WON).EditionControl.EntryField.IsReadOnly = True
      .Column(GRID_WON).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 20, 2)

      'PLAY SESSIONS
      .Column(GRID_PS_NUM_PLAYED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)        'PS
      .Column(GRID_PS_NUM_PLAYED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)         'Jugadas
      .Column(GRID_PS_NUM_PLAYED).Width = GRID_COLUMN_AMOUNT_WIDTH - 500
      .Column(GRID_PS_NUM_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_PLAYED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)            'PS
      .Column(GRID_PS_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1387)      'Jugado
      .Column(GRID_PS_PLAYED).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_NUM_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)           'PS
      .Column(GRID_PS_NUM_WON).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(262)            'Ganadas
      .Column(GRID_PS_NUM_WON).Width = GRID_COLUMN_AMOUNT_WIDTH - 500
      .Column(GRID_PS_NUM_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)               'PS
      .Column(GRID_PS_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1388)         'Ganado
      .Column(GRID_PS_WON).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_CASH_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)           'PS
      .Column(GRID_PS_CASH_IN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(289)            'CashIn
      .Column(GRID_PS_CASH_IN).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_CASH_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)          'PS
      .Column(GRID_PS_CASH_OUT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(290)           'CashOut
      .Column(GRID_PS_CASH_OUT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_NETWIN_PL_WO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)      'PS
      .Column(GRID_PS_NETWIN_PL_WO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)       'Net Win
      .Column(GRID_PS_NETWIN_PL_WO).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_NETWIN_PL_WO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_NETWIN_CI_CO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)      'PS
      .Column(GRID_PS_NETWIN_CI_CO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259) & "1" 'Net Win
      .Column(GRID_PS_NETWIN_CI_CO).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_NETWIN_CI_CO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

    With Me.dg_hour
      .Clear()
      .Init(GRID_COLUMNS_INCLUDED_PS, GRID_HEADERS, True)

      .Column(GRID_BASE_HOUR).Header(0).Text = ""

      .Column(GRID_TERMINAL_NAME).Width = 0

      .Column(GRID_BASE_DAY).Header(0).Text = ""
      .Column(GRID_BASE_DAY).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(302)    ' "xFecha" ' "xFechaSinHora"
      .Column(GRID_BASE_DAY).Width = 0
      .Column(GRID_BASE_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_BASE_HOUR).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(405)    ' "xFecha"
      .Column(GRID_BASE_HOUR).Width = GRID_COLUMN_FIRST_WIDTH - 700
      .Column(GRID_BASE_HOUR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_GAME_ID).Header(0).Text = ""
      .Column(GRID_GAME_ID).Header(1).Text = "" ' "xIdJuego"
      .Column(GRID_GAME_ID).Width = 0
      .Column(GRID_GAME_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_GAME_NAME).Header(0).Text = ""
      .Column(GRID_GAME_NAME).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(211)    ' "xJuego"
      .Column(GRID_GAME_NAME).Width = 0
      .Column(GRID_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PLAYED).Editable = False
      .Column(GRID_PLAYED).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_PLAYED).EditionControl.EntryField.IsReadOnly = True
      .Column(GRID_PLAYED).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 20, 2)

      .Column(GRID_WON).Editable = False
      .Column(GRID_WON).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_WON).EditionControl.EntryField.IsReadOnly = True
      .Column(GRID_WON).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 20, 2)

      ' PLAY SESSIONS
      .Column(GRID_PS_NUM_PLAYED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_NUM_PLAYED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)  ' "xJugadas"
      .Column(GRID_PS_NUM_PLAYED).Width = GRID_COLUMN_AMOUNT_WIDTH - 500
      .Column(GRID_PS_NUM_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_PLAYED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1387) ' GLB_NLS_GUI_INVOICING.GetString(256)      ' "xJugado"
      .Column(GRID_PS_PLAYED).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_NUM_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_NUM_WON).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(262)     ' "xGanadas"
      .Column(GRID_PS_NUM_WON).Width = GRID_COLUMN_AMOUNT_WIDTH - 500
      .Column(GRID_PS_NUM_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_WON).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1388) ' GLB_NLS_GUI_INVOICING.GetString(257)         ' "xGanado"
      .Column(GRID_PS_WON).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_CASH_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_CASH_IN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(289) ' "xCASHIN"
      .Column(GRID_PS_CASH_IN).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_CASH_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_CASH_OUT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(290) ' "xCASHOUT"
      .Column(GRID_PS_CASH_OUT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_NETWIN_PL_WO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_NETWIN_PL_WO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)     ' "xNet Win"
      .Column(GRID_PS_NETWIN_PL_WO).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_NETWIN_PL_WO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_PS_NETWIN_CI_CO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258) '"xPS"
      .Column(GRID_PS_NETWIN_CI_CO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259) & "1"     ' "xNet Win"
      .Column(GRID_PS_NETWIN_CI_CO).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_PS_NETWIN_CI_CO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

    With Me.dg_games

      .Clear()
      .Init(GRID_COLUMNS, GRID_HEADERS, True)

      .Column(GRID_TERMINAL_NAME).Width = 0

      .Column(GRID_BASE_DAY).Header(0).Text = ""
      .Column(GRID_BASE_DAY).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(302)    ' "xFecha" ' "xFechaSinHora"
      .Column(GRID_BASE_DAY).Width = 0
      .Column(GRID_BASE_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_BASE_HOUR).Header(0).Text = ""
      .Column(GRID_BASE_HOUR).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(302)    ' "xFecha"
      .Column(GRID_BASE_HOUR).Width = GRID_COLUMN_FIRST_WIDTH - 700
      .Column(GRID_BASE_HOUR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_GAME_ID).Header(0).Text = ""
      .Column(GRID_GAME_ID).Header(1).Text = "" ' "xIdJuego"
      .Column(GRID_GAME_ID).Width = 0
      .Column(GRID_GAME_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_GAME_NAME).Header(0).Text = ""
      .Column(GRID_GAME_NAME).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(211)    ' "xJuego"
      .Column(GRID_GAME_NAME).Width = GRID_COLUMN_FIRST_WIDTH
      .Column(GRID_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Editables
      .Column(GRID_PLAYED).Editable = True
      .Column(GRID_PLAYED).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_PLAYED).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_PLAYED).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 20, 2)

      .Column(GRID_WON).Editable = True
      .Column(GRID_WON).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_WON).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_WON).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 20, 2)

      .Column(GRID_NUM_PLAYED).Editable = True
      .Column(GRID_NUM_PLAYED).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_NUM_PLAYED).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_NUM_PLAYED).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 10)

      .Column(GRID_NUM_WON).Editable = True
      .Column(GRID_NUM_WON).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_NUM_WON).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_NUM_WON).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 10)

    End With

    ' Common Columns
    GUI_StyleSheet(Me.dg_days)
    GUI_StyleSheet(Me.dg_hour)
    GUI_StyleSheet(Me.dg_games)

  End Sub ' GUI_StyleSheet


  Private Function GetFilter()
    Dim _terminal_id As Long()
    Dim _today_opening As Date

    m_date_to_filter = ""

    _today_opening = WSI.Common.Misc.TodayOpening()

    m_date_from = IIf(uc_dsl.FromDateSelected, uc_dsl.FromDate, DateTime.MinValue.AddYears(2007))
    m_date_from = New DateTime(m_date_from.Year, m_date_from.Month, m_date_from.Day, _today_opening.Hour, _today_opening.Minute, 0)

    m_date_to = IIf(uc_dsl.ToDateSelected, uc_dsl.ToDate, _today_opening.AddDays(1))
    m_date_to = New DateTime(m_date_to.Year, m_date_to.Month, m_date_to.Day, _today_opening.Hour, _today_opening.Minute, 0)

    If uc_dsl.ToDateSelected Then
      m_date_to_filter = m_date_to.ToString()
    End If

    _terminal_id = uc_pr_list.GetTerminalIdListSelected()

    If Not uc_pr_list.TerminalListHasValues Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1411), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    If m_date_from > m_date_to Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(136), _
                    mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    m_terminal_id = _terminal_id.GetValue(0)
    m_terminal_name = Me.uc_pr_list.GetTerminalName(m_terminal_id)

    Return True
  End Function


  Private Sub SetFilter(ByVal TerminalId As Integer, ByVal DateFrom As Date, ByVal DateTo As Date)
    Dim _terminals_id(0) As Long

    m_date_from = DateFrom
    m_date_to = DateTo
    m_terminal_id = TerminalId

    If m_date_from > m_date_to Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(136), _
                    mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    uc_dsl.FromDate = m_date_from
    uc_dsl.ToDate = m_date_to
    _terminals_id(0) = TerminalId

    ' TODO Update Controls with these values
    uc_pr_list.SetTerminalIdListSelected(_terminals_id)

  End Sub


  Private Sub DoSearch()
    dg_days.Clear()
    dg_hour.Clear()
    dg_games.Clear()

    m_dt_spd = New DataTable()
    m_dt_sph = New DataTable()
    m_dt_sph_game = New DataTable()

    m_dt_sph_ps = New DataTable()
    m_dt_spd_ps = New DataTable()

    Using _db_trx As New WSI.Common.DB_TRX()
      m_terminal_name = uc_pr_list.GetTerminalName(m_terminal_id)
    End Using

    ' Get Data Table With All Data
    If Not GetStaticsGame() Then

      Return
    End If

    If CInt(m_dt_sph_game.Select(" GAME_NAME IS NOT NULL ").Length) = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1412), _
                      ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    uc_terminal_name.Value = m_terminal_name

    ' Get Data Set With All Grouped By Hour
    If Not GetStaticsHour() Then

      Return
    End If

    ' Get Data Set With All Grouped By Day
    If Not GetStaticsDay() Then

      Return
    End If

    UpdateGridDays()

    'SDS 17-03-2016: PBI 10610 add button export
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = True

    If (dg_days.NumRows > 0) Then
      dg_days.Redraw = False
      dg_days.CurrentRow = 0
      dg_days.IsSelected(0) = True
      dg_days.Redraw = True
      uc_grid_days_RowSelectedEvent(0)
    End If

  End Sub

  Private Sub Button_Search()
    Dim _rows_modified() As DataRow

    Me.uc_terminal_name.Value = "---"

    _rows_modified = Nothing
    If (TestChanged(m_dt_sph_game, True, _rows_modified)) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), _
            mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
            ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Return
      End If
    End If

    If Not GetFilter() Then
      Return
    End If

    DoSearch()

  End Sub

  Private Function UpdateSalesPerHour() As Boolean

    Try

      Using _db_trx As New WSI.Common.DB_TRX()

        If WSI.Common.Stats.UpdateTerminalStats(m_terminal_id, m_dt_sph_game, _db_trx.SqlTransaction) Then

          _db_trx.Commit()

          Return True
        End If

      End Using

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try

    Return False

  End Function

  Private Sub Cancel()
    Dim _rows_modified() As DataRow

    _rows_modified = Nothing

    If (TestChanged(m_dt_sph_game, True, _rows_modified)) Then
      ' Changes 
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), _
          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
          ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Return
      End If
    End If

    Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

  Private Sub SaveChanges()
    Dim _rows_modified() As DataRow

    _rows_modified = Nothing

    If (Not TestChanged(m_dt_sph_game, False, _rows_modified)) Then
      ' No Changes --> Success
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1393), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

      Return
    End If

    ' Audit
    If Not AuditChanges(_rows_modified) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(103), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return
    End If

    ' Save
    If Not UpdateSalesPerHour() Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(103), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return
    End If

    ' AcceptChanges
    m_dt_sph_game.AcceptChanges()

    Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

  End Sub

  Private Function GetStaticsGame() As Boolean
    Try
      If Not WSI.Common.Stats.GetStatsGroupByGame(m_terminal_id, m_date_from, m_date_to, m_dt_sph_game) Then

        Return False
      End If

      Return True

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try
  End Function

  Private Function GetStaticsHour() As Boolean
    Try
      If Not WSI.Common.Stats.GetStatsGroupByHours(m_dt_sph_game, m_dt_sph) Then

        Return False
      End If

      If Not WSI.Common.Stats.GetPlaySessionsGroupByHours(m_terminal_id, m_date_from, m_date_to, m_dt_sph_ps) Then

        Return False
      End If

      ' Set the primary key column.
      m_dt_sph.PrimaryKey = New DataColumn() {m_dt_sph.Columns(SQL_TERMINAL_NAME), m_dt_sph.Columns(SQL_BASE_HOUR)}
      m_dt_sph_ps.PrimaryKey = New DataColumn() {m_dt_sph_ps.Columns(SQL_TERMINAL_NAME), m_dt_sph_ps.Columns(SQL_BASE_HOUR)}
      m_dt_sph.Merge(m_dt_sph_ps, False, MissingSchemaAction.Add)

      Return True
    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try

    Return False

  End Function

  Private Function GetStaticsDay() As Boolean
    Try
      If Not WSI.Common.Stats.GetStatsGroupByDays(m_dt_sph_game, m_dt_spd) Then

        Return False
      End If

      If Not WSI.Common.Stats.GetPlaySessionsGroupByDays(m_dt_sph_ps, m_dt_spd_ps) Then

        Return False
      End If

      ' Set the primary key column.
      m_dt_spd.PrimaryKey = New DataColumn() {m_dt_spd.Columns(SQL_TERMINAL_NAME), m_dt_spd.Columns(SQL_BASE_DAY)}
      m_dt_spd_ps.PrimaryKey = New DataColumn() {m_dt_spd_ps.Columns(SQL_TERMINAL_NAME), m_dt_spd_ps.Columns(SQL_BASE_DAY)}
      m_dt_spd.Merge(m_dt_spd_ps, False, MissingSchemaAction.Add)

      Return True

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try

    Return False
  End Function

  Private Sub UpdateGrid(ByRef Grid As uc_grid, ByRef DtRows As DataRow(), ByVal NumCols As Int16)
    Try
      If DtRows.Length = 0 Then
        Grid.Clear()
        Return
      End If

      Grid.SuspendLayout()

      For _idx_r As Integer = 0 To DtRows.Length - 1

        ' TODO V2 SI QUE S'AFEGIRANT
        If DtRows(_idx_r).IsNull(SQL_GAME_ID) Then
          Continue For
        End If

        If (Grid.NumRows) <= _idx_r Then
          Grid.AddRow()
        End If

        If Not DtRows(_idx_r).IsNull(SQL_TERMINAL_NAME) Then
          Grid.Cell(_idx_r, GRID_TERMINAL_NAME).Value = DtRows(_idx_r).Item(SQL_TERMINAL_NAME)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_BASE_DAY) Then
          Grid.Cell(_idx_r, GRID_BASE_DAY).Value = GUI_FormatDate(DtRows(_idx_r).Item(SQL_BASE_DAY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_BASE_HOUR) Then
          Grid.Cell(_idx_r, GRID_BASE_HOUR).Value = GUI_FormatDate(DtRows(_idx_r).Item(SQL_BASE_HOUR), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_GAME_ID) Then
          Grid.Cell(_idx_r, GRID_GAME_ID).Value = DtRows(_idx_r).Item(SQL_GAME_ID)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_GAME_NAME) Then
          Grid.Cell(_idx_r, GRID_GAME_NAME).Value = DtRows(_idx_r).Item(SQL_GAME_NAME)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_NUM_PLAYED) Then
          Grid.Cell(_idx_r, GRID_NUM_PLAYED).Value = GUI_FormatNumber(DtRows(_idx_r).Item(SQL_NUM_PLAYED), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_BASE_DAY) Then
          Grid.Cell(_idx_r, GRID_PLAYED).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PLAYED))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_NUM_WON) Then
          Grid.Cell(_idx_r, GRID_NUM_WON).Value = GUI_FormatNumber(DtRows(_idx_r).Item(SQL_NUM_WON), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_WON) Then
          Grid.Cell(_idx_r, GRID_WON).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_WON))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_NETWIN) Then
          Grid.Cell(_idx_r, GRID_NETWIN).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_NETWIN))
        End If

        If NumCols = GRID_COLUMNS Then
          Continue For
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_PLAYED) Then
          Grid.Cell(_idx_r, GRID_PS_PLAYED).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PS_PLAYED))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_WON) Then
          Grid.Cell(_idx_r, GRID_PS_WON).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PS_WON))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_NUM_PLAYED) Then
          Grid.Cell(_idx_r, GRID_PS_NUM_PLAYED).Value = GUI_FormatNumber(DtRows(_idx_r).Item(SQL_PS_NUM_PLAYED), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_NUM_WON) Then
          Grid.Cell(_idx_r, GRID_PS_NUM_WON).Value = GUI_FormatNumber(DtRows(_idx_r).Item(SQL_PS_NUM_WON), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_CASH_IN) Then
          Grid.Cell(_idx_r, GRID_PS_CASH_IN).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PS_CASH_IN))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_CASH_OUT) Then
          Grid.Cell(_idx_r, GRID_PS_CASH_OUT).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PS_CASH_OUT))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_NETWIN_PL_WO) Then
          Grid.Cell(_idx_r, GRID_PS_NETWIN_PL_WO).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PS_NETWIN_PL_WO))
        End If

        If Not DtRows(_idx_r).IsNull(SQL_PS_NETWIN_CI_CO) Then
          Grid.Cell(_idx_r, GRID_PS_NETWIN_CI_CO).Value = GUI_FormatCurrency(DtRows(_idx_r).Item(SQL_PS_NETWIN_CI_CO))
        End If

      Next

      While Grid.NumRows > DtRows.Length
        Grid.DeleteRowFast((Grid.NumRows - 1))
      End While

      Grid.ResumeLayout()

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try

  End Sub

  Private Sub UpdateGridDays()
    Try
      Dim _dt As New DataTable

      UpdateGrid(dg_days, m_dt_spd.Select(" GAME_NAME IS NOT NULL "), GRID_COLUMNS_INCLUDED_PS)
    Catch ex As Exception

    End Try
  End Sub

  Private Sub UpdateGridHour(ByVal Day As DateTime)
    Dim _filter As String

    _filter = " BASE_HOUR >= " & FormatDateToStringDataTableFilter(Day)
    _filter = _filter & " AND BASE_HOUR < " & FormatDateToStringDataTableFilter(Day.AddDays(1))
    _filter = _filter & " AND GAME_NAME IS NOT NULL "

    UpdateGrid(dg_hour, m_dt_sph.Select(_filter), GRID_COLUMNS_INCLUDED_PS)

    If dg_hour.NumRows = 0 Then
      dg_games.Clear()
    End If

  End Sub

  Private Sub UpdateGridGame(ByVal Day As DateTime)
    Dim _filter As String

    _filter = " BASE_HOUR >= " & FormatDateToStringDataTableFilter(Day)
    _filter = _filter & " AND BASE_HOUR < " & FormatDateToStringDataTableFilter(Day.AddHours(1))

    UpdateGrid(dg_games, m_dt_sph_game.Select(_filter), GRID_COLUMNS)

  End Sub

  Private Function TestChanged(ByVal Datas As DataTable, ByVal TestOnly As Boolean, ByRef ModifiedRows As DataRow()) As Boolean

    Dim _modified_rows() As DataRow
    Dim _any_changed As Boolean
    Dim _unchanged_found As Boolean

    ModifiedRows = Nothing

    _modified_rows = Datas.Select("", "", DataViewRowState.ModifiedCurrent)

    _any_changed = False
    _unchanged_found = False

    For Each _row As DataRow In _modified_rows
      If _row.Item(SQL_PLAYED) <> _row.Item(SQL_PLAYED, DataRowVersion.Original) _
        Or _row.Item(SQL_WON) <> _row.Item(SQL_WON, DataRowVersion.Original) _
        Or _row.Item(SQL_NUM_PLAYED) <> _row.Item(SQL_NUM_PLAYED, DataRowVersion.Original) _
        Or _row.Item(SQL_NUM_WON) <> _row.Item(SQL_NUM_WON, DataRowVersion.Original) Then
        ' Changed
        _any_changed = True
        If (TestOnly) Then
          Return True
        End If
      Else
        ' Not Changed
        _unchanged_found = True
        _row.RejectChanges()
      End If
    Next

    If (TestOnly) Then
      Return False
    End If

    If (_unchanged_found) Then
      ModifiedRows = Datas.Select("", "", DataViewRowState.ModifiedCurrent)
    Else
      ModifiedRows = _modified_rows
    End If

    Return _any_changed

  End Function

  Private Function AuditChanges(ByVal ModifiedRows() As DataRow) As Boolean
    Dim _old_value As CLASS_AUDITOR_DATA
    Dim _new_value As CLASS_AUDITOR_DATA
    Dim _sb As StringBuilder
    Dim _min_date As Date
    Dim _max_date As Date

    _old_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _new_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _sb = New StringBuilder()

    _min_date = GUI_FormatDate(ModifiedRows(0).Item(1))
    _max_date = GUI_FormatDate(ModifiedRows(0).Item(1))

    For Each row As DataRow In ModifiedRows
      If _min_date > GUI_FormatDate(row.Item(1)) Then
        _min_date = GUI_FormatDate(row.Item(1))
      ElseIf _max_date < GUI_FormatDate(row.Item(1)) Then
        _max_date = GUI_FormatDate(row.Item(1))
      End If

      CreateAudit(row, DataRowVersion.Original, _old_value)
      CreateAudit(row, DataRowVersion.Current, _new_value)
    Next

    'making the text of audit
    _sb.Append(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1384))
    _sb.Append(" ")
    _sb.Append(m_terminal_name)
    _sb.Append(" ")
    _sb.Append(GUI_FormatDate(_min_date))

    If GUI_FormatDate(_min_date) <> GUI_FormatDate(_max_date) Then
      _sb.Append("-")
      _sb.Append(GUI_FormatDate(_max_date))
    End If

    _old_value.SetName(0, "", _sb.ToString())
    _new_value.SetName(0, "", _sb.ToString())

    If Not _new_value.Notify(GLB_CurrentUser.GuiId, _
                                 GLB_CurrentUser.Id, _
                                 GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                 0, _
                                 _old_value) Then

      ' Error!!
      Return False
    End If

    Return True
  End Function

  Private Sub CreateAudit(ByVal Row As DataRow, ByVal RowVersion As DataRowVersion, ByVal AuditorData As CLASS_AUDITOR_DATA)

    Dim _played As String
    Dim _won As String
    Dim _netwin As String
    Dim _num_played As String
    Dim _num_won As String
    Dim _audit_header As StringBuilder

    _audit_header = New StringBuilder()

    _audit_header.Append(GUI_FormatDate(Row.Item(SQL_BASE_DAY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))
    _audit_header.Append(" ")
    _audit_header.Append(GUI_FormatTime(Row.Item(SQL_BASE_HOUR), ENUM_FORMAT_TIME.FORMAT_HHMM))
    _audit_header.Append(" ")
    _audit_header.Append(Row.Item(3).ToString())
    _audit_header.Append(" ")

    _played = GUI_FormatCurrency(Row.Item(SQL_PLAYED, RowVersion).ToString())
    _won = GUI_FormatCurrency(Row.Item(SQL_WON, RowVersion).ToString().ToString())
    _netwin = GUI_FormatCurrency(Row.Item(SQL_NETWIN, RowVersion).ToString())
    _num_played = Row.Item(SQL_NUM_PLAYED, RowVersion).ToString()
    _num_won = Row.Item(SQL_NUM_WON, RowVersion).ToString()

    AuditorData.SetField(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1390, _played, _won, _netwin, _num_played, _num_won))

  End Sub

  Private Sub UcGridDaySelected(ByVal SelectedRow As System.Int32)
    Dim _filter As DateTime

    Try
      If (SelectedRow < 0 Or dg_days.NumRows <= 0) Then
        Return
      End If

      _filter = dg_days.Cell(SelectedRow, GRID_BASE_DAY).Value

      UpdateGridHour(_filter)

      If (dg_hour.NumRows > 0) Then
        dg_hour.Redraw = False
        dg_hour.ClearSelection()
        dg_hour.CurrentRow = 0
        dg_hour.IsSelected(0) = True
        dg_hour.Redraw = True
        uc_grid_hour_RowSelectedEvent(0)
      End If

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try
  End Sub

  Private Sub UcGridHourRowSelected(ByVal SelectedRow As Int32)

    Try
      If (SelectedRow < 0 Or dg_hour.NumRows <= 0 Or dg_hour.Cell(SelectedRow, GRID_BASE_HOUR).Value = "") Then
        Return
      End If

      UpdateGridGame(dg_hour.Cell(SelectedRow, GRID_BASE_HOUR).Value)

      If dg_games.NumRows > 0 Then
        dg_games.ClearSelection()
        dg_games.CurrentRow = 0
        dg_games.IsSelected(0) = True
      End If

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try
  End Sub

  Private Function FormatDateToStringDataTableFilter(ByVal Day As DateTime) As String
    Return String.Format(Global.System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, "#{0}#", Day)
  End Function

#End Region

#Region "Events"

  Private Sub uc_grid_days_RowSelectedEvent(ByVal SelectedRow As Int32) Handles dg_days.RowSelectedEvent

    If Not dg_days.Redraw Or SelectedRow <> dg_days.CurrentRow Then
      Exit Sub
    End If

    UcGridDaySelected(SelectedRow)
  End Sub

  Private Sub uc_grid_hour_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_hour.RowSelectedEvent
    If Not dg_hour.Redraw Or SelectedRow <> dg_hour.CurrentRow Then
      Exit Sub
    End If
    UcGridHourRowSelected(SelectedRow)
  End Sub

  Private Sub uc_grid_games_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_games.CellDataChangedEvent
    Dim _base_hour As DateTime
    Dim _game_id As Int64
    Dim _new_value As Object
    Dim _filter As String
    Dim _grid_hour_select As Int32
    Dim _grid_day_select As Int32

    If (Row < 0 Or dg_games.NumRows < 0 Or dg_games.Cell(Row, GRID_BASE_HOUR).Value = "") Then
      Return
    End If

    _base_hour = dg_games.Cell(Row, GRID_BASE_HOUR).Value
    _game_id = dg_games.Cell(Row, GRID_GAME_ID).Value

    If String.IsNullOrEmpty(dg_games.Cell(Row, Column).Value) Then
      _new_value = 0
    Else
      _new_value = dg_games.Cell(Row, Column).Value
    End If

    _filter = " BASE_HOUR = " & FormatDateToStringDataTableFilter(_base_hour)
    _filter = _filter & " AND GAME_ID = " & _game_id

    _grid_day_select = dg_days.CurrentRow
    _grid_hour_select = dg_hour.CurrentRow

    If (Column = GRID_PLAYED Or Column = GRID_WON) Then
      m_dt_sph_game.Select(_filter)(0).Item(IIf(Column = GRID_PLAYED, SQL_PLAYED, SQL_WON)) = CDec(_new_value)
    Else
      m_dt_sph_game.Select(_filter)(0).Item(IIf(Column = GRID_NUM_PLAYED, SQL_NUM_PLAYED, SQL_NUM_WON)) = CLng(_new_value)
    End If

    If Not GetStaticsHour() Then
      Return
    End If

    If Not GetStaticsDay() Then
      Return
    End If

    UpdateGridDays()

    UpdateGridHour(dg_days.Cell(dg_days.CurrentRow, GRID_BASE_DAY).Value)

    UpdateGridGame(dg_hour.Cell(dg_hour.CurrentRow, GRID_BASE_HOUR).Value)

  End Sub

#End Region

End Class