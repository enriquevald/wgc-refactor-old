'-------------------------------------------------------------------
' Copyright � 2002-2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminals_edit1.vb
'
' DESCRIPTION:   Change status dialog
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 05-NOV-2010 MBF    First version
' 18-MAR-2010 TJG    Retirement Date
' 16-NOV-2011 MPO    Add new fields: Denomination, MultipleDenomination, Program, Payout theorical.
' 01-DEC-2011 JMM    Bug solving: An error occurred selecting Denomination without input on its value
' 23-APR-2012 DDM    Fixed Bug #280: Validating terminal name and provider id
' 02-AUG-2012 MPO    Added field "sas_flags" --> extended_meter, credits_play_mode_sas
' 06-SEP-2012 RCI    ef_tax_registration is mandatory only when ExternalProtocol is Enabled.
' 30-MAY-2013 RBG    Fixed Bug #786: Error when selecting a provider with character: '
' 05-JUN-2013 JCA    Fixed Bug #821: Error when terminal has an invalid provider o pv_hide = 1
' 23-OCT-2013 DRV    Add TITO Configuration for each terminal
' 09-JAN-2014 ICS    Close open play sessions when terminal is retired
' 16-JAN-2014 ICS    Fixed Bug WIGOSTITO-985: Error message when clicked on "cancel" button in terminals edition form
' 28-MAY-2014 LEM    Fixed Bug WIG-961: Vendor is editable for 3GS terminals.
' 11-JUL-2014 XIT    Added Multilanguage Support
' 13-AUG-2014 LEM    Added new fields "MachineId", "Position", "Top Award", "Max Bet", "Number Lines"
'                    Convert "Denomination" and "Multidenomination" into ComboBox
' 30-SEP-2014  FJC   Add SAS_FLAG: SPECIAL_PROGRESSIVE_METER 
' 19-NOV-2014  SGB   Fixed Bug WIG-1712: Changes FloorId lenght
' 02-DEC-2014  DLL   Added new column GameTheme
' 20-JAN-2015  FJC   Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 24-MAR-2015  AMF   Added menu item 'Meter Delta'
' 13-APR-2015  FJC   BackLog Item 940
' 04-MAY-2015  FJC   Fixed Bug WIG-2270: 
' 05-MAY-2015  DLL   Fixed Bug WIG-2281: Incorrect text for reported and configurated for Asset Number and Serial Number
' 14-MAY-2015  ANM   Fixed Bug WIG-2345: Incorrect message in terminal validation
' 29-MAY-2015  FAV   Added Communication Type group box (Communication type combo and smib configuration combo for pulses type)
' 05-JUN-2015  AMF   Product Backlog Item 1936
' 11-JUN-2015  FAV   Added a validation for check the smib configuration mandatory when the user selects a pulse communication.
' 29-JUN-2015  SGB   Backlog item 2161: Added check for enabled machines
' 07-JUL-2015  SGB   Backlog item 2171: Undo last changes: "Added check for enabled machines"
' 25-AUG-2015  SGB   Fixed Bug 3982: Don't see label blocked
' 08-SEP-2015  FOS   Backlog Item 3709
' 17-SEP-2015  FAV   Fixed Bug 4520: Error with the max length in the terminal name field
' 30-SEP-2015  FOS   Backlog Item 4458:Fixed Bug new check is over others fields.
' 20-NOV-2015  RGR   Product Backlog Item 6451:Garcia River-03 add field Equity (%).
' 16-DEC-2015  ETP   Backlog Item 7728: New comunication type: SAS IGT POKER
' 05-JAN-2016  DDS   Product Backlog Item 8145:Floor Dual Currency: Definici�n de moneda en m�quina
' 29-JAN-2016  ETP   Fixed Bug  Gaming Hall no se podia modificar terminal.
' 01-FEB-2016  FOS   Product Backlog Item 8881: Protect terminal edition when terminal currency is changed
' 04-FEB-2016  FOS   Fixed Bug 8979:Floor Dual Currency: Only check if currency is changed when the combo of currencies is used
' 09-MAR-2016  FAV   Fixed Bug 9552: Validate that the money collection is not open when the Terminal is going to be to retired
' 10-MAR-2016  JPJ   Fixed Bug 10076: On an offline terminal iso_code is not
' 16-MAR-2016  FAV   Fixed Bug 9557: The cashier session for the pending money collection should change to pending closing
' 01-FEB-2016  RAB   Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
' 23-MAR-2016  DDS   Fixed Bug 10917: 3GS terminals cannot edit serial number
' 06-MAY-2016  RAB   PBI 12627:AFIP Client: Recovery accounting denomination
' 24-MAY-2016  RAB   PBI 13462:Review Sprint 24 (Accounting Denomination)
' 30-MAY-2016  FAV   Fixed Bug 13778: Shows chk_coin_collection only for TITO mode 
' 04-AUG-2016  ETP   FIXED Bug 15571: Resizing window for tito and cashless
' 17-AUG-2016  FAV   Product Backlog Item 16835: Add SAS Host Id
' 25-AUG-2016  ESE   Bug 15495, 14462: 4 decimal format. Accommodate checkbox
' 30-AGO-2016  ESE   Bug 15471: Convert Tax registration into numeric field
' 01-SEP-2016  JMV   Bug 17243:Edici�n de Terminales: "Contribuci�n al progresivo" debe permitir 4 decimales (rollback Bug 15495)
' 02-SEP-2016  LTC   Bug 15471:Error on modifying tax registering
' 06-OCT-2016  FAV   Fixed Bug 17967: The "TITO" box is showing incomplete.
' 18-OCT-2016  FAV   PBI 18240: eBox- 'Min. denomination' field added for TITO mode
' 19-OCT-2016  PDM   PBI 18245:eBox - Ticket Amount Not Multiple of Machine Denomination: GP - monto m�nimo aceptado por la m�quina (GUI y BD)
' 26-OCT-2016  FAV   PBI 19597:eBox - Ticket Amount Not Multiple of Machine Denomination: Review
' 22-NOV-2016  PDM   PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
' 23-ENE-2017  DPC   Bug 20657:Imposible introducir letras en el campo "Registro Fiscal" de edici�n de terminales
' 16-FEB-2016  FAV   PBI 24711: Machine on reserve.
' 09-MAY-2017  ATB   PBI 27209:Terminals Editor screen - Add new dates
' 05-SEP-2017  DHA   Bug 29553:WIGOS-4161 The terminal editor screen of a retired terminal still displaying the button "Retire"
' 08-JAN-2018  LTC   PBI 31209:WIGOS-7488 AGG - EGM manufacturing date
' 12-FEB-2018  RGR   Bug 31496:WIGOS-8108 There is a label in Terminal Editor that splits a word in two parts, in English language 
' 04-DEC-2017  FGB   WIGOS-6913: AGG - Terminal grouping for multi seat EGM - Update
' 08-JAN-2018  LTC   PBI 31209:WIGOS-7488 AGG - EGM manufacturing date
' 12-FEB-2018  RGR   Bug 31496:WIGOS-8108 There is a label in Terminal Editor that splits a word in two parts, in English language 
' 28-MAR-2018  MGG    PBI WIGOS-9037 Codere_Argentina_1428 - Edit SAS version, AFT and bill accepting for terminals
' 03-APR-2018  JML   Fixed Bug 32155:WIGOS-9628 Terminal edition screen is overlapping some fields
'-------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common
Imports WSI.Common.Misc

Public Class frm_terminals_edition
  Inherits GUI_Controls.frm_base_edit

#Region " Members "

  Private m_input_terminal_name As String
  Private m_provider_id As String
  'Private DeleteOperation As Boolean
  Private m_play_sessions_to_close As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)
  Private m_play_sessions_retirement_action As CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS
  Private m_system_mode As SYSTEM_MODE

  Private m_form_default_size As Size = New Size(890, 745)
  Private m_form_tito_size As Size = New Size(900, 780)
  Private m_form_wass_size As Size = New Size(890, 750)

  Private m_offline_terminal As Boolean

  Private m_national_currency As String

  Private m_terminal_to_pending As Int64

#End Region   ' Members

#Region " Constants "

  Private Const LEN_TERMINAL_NAME As Integer = 40

  Private Const IDX_CMB_STATUS_ACTIVE As Integer = 0
  Private Const IDX_CMB_STATUS_OUT_OF_SERVICE As Integer = 1
  Private Const IDX_CMB_STATUS_RETIRED As Integer = 2

  Private Const MASK_MACHINE_BLOCKED_NO_LICENSE As Integer = &H1
  Private Const MASK_MACHINE_BLOCKED_BY_SIGNATURE As Integer = &H2
  Private Const MASK_MACHINE_BLOCKED_MANUALLY As Integer = &H4
  Private Const MASK_MACHINE_BLOCKED_AUTOMATICALLY As Integer = &H8
  Private Const MASK_MACHINE_ALWAYS_BLOCKED As Integer = &H10
  Private Const MASK_MACHINE_ALWAYS_UNBLOCKED As Integer = &H20
  Private Const FORM_DB_MIN_VERSION As Short = 264

  Private Const AUDIT_NONE_STRING As String = "---"
  Private Const _regular_expression_tax_registration_default As String = "^\d{8}$"

  Private _regular_expression As String

#End Region ' Constants

#Region " Properties"
  Dim chk_met_homologated As CheckBox
  Dim ef_brand_code As uc_entry_field
  Dim ef_model As uc_entry_field
  Dim ef_manufacture_year As uc_entry_field

  Private Property StatusSelection() As String
    Get

      Select Case Me.cmb_status.SelectedIndex
        Case IDX_CMB_STATUS_ACTIVE
          Return WSI.Common.TerminalStatus.ACTIVE

        Case IDX_CMB_STATUS_OUT_OF_SERVICE
          Return WSI.Common.TerminalStatus.OUT_OF_SERVICE

        Case IDX_CMB_STATUS_RETIRED
          Return WSI.Common.TerminalStatus.RETIRED

        Case Else
          Return WSI.Common.TerminalStatus.ACTIVE

      End Select

    End Get

    Set(ByVal value As String)

      Select Case value
        Case WSI.Common.TerminalStatus.ACTIVE
          Me.cmb_status.SelectedIndex = IDX_CMB_STATUS_ACTIVE

        Case WSI.Common.TerminalStatus.OUT_OF_SERVICE
          Me.cmb_status.SelectedIndex = IDX_CMB_STATUS_OUT_OF_SERVICE

        Case WSI.Common.TerminalStatus.RETIRED
          Me.cmb_status.SelectedIndex = IDX_CMB_STATUS_RETIRED

        Case Else
          Me.cmb_status.SelectedIndex = -1
          Me.cmb_status.ResetLastIndex()
      End Select

      If value = WSI.Common.TerminalStatus.RETIRED Then

        Dim _read_item As CLASS_TERMINAL
        _read_item = DbReadObject

        dtp_retirement_date.Visible = True
        ef_retirement_date.Visible = False

        If _read_item.Status <> WSI.Common.TerminalStatus.RETIRED Then
          ' Suggest the most accurate retirement date
          dtp_retirement_date.Value = SuggestedRetirementDate(_read_item.TerminalId)
        End If

        If _read_item.RetirementRequested <> Nothing Then
          ef_retirement_requested.Value = GUI_FormatDate(_read_item.RetirementRequested, _
                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                         ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
        Else
          ef_retirement_requested.Value = "---"
        End If
      Else
        ' Not Retired
        dtp_retirement_date.Visible = False
        ef_retirement_date.Visible = True
        ef_retirement_requested.Value = "---"
      End If

    End Set

  End Property

#End Region ' Properties

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_InitControls()

    Dim _read_item As CLASS_TERMINAL
    Dim _meter_delta_table As DataTable
    Dim _offset_Y As Int32

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    _read_item = DbReadObject
    m_offline_terminal = _read_item.IsNewOffline Or (_read_item.Type = TerminalTypes.OFFLINE)

    ' Read System mode
    m_system_mode = WSI.Common.Misc.SystemMode()

    If m_offline_terminal Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6918)           ' 6918 "Alta de terminal offline"
    Else
      Me.Text = GLB_NLS_GUI_AUDITOR.GetString(365)                    ' 365 "Edici�n de Terminales"
    End If


    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = Me.ScreenMode <> ENUM_SCREEN_MODE.MODE_NEW

    ' RCI 17-JUN-2011: Needs Write AND Delete permissions to retire terminals.
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write And Me.Permissions.Delete

    ef_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(470)                 ' 470 "Terminal"
    ef_external_id.Text = GLB_NLS_GUI_AUDITOR.GetString(331)                ' 331 "ID Protocolo"
    ef_provider_id.Text = GLB_NLS_GUI_AUDITOR.GetString(341)                ' 341 "Proveedor"
    cmb_provider.Text = GLB_NLS_GUI_AUDITOR.GetString(341)                  ' 341 "Proveedor"
    ef_vendor_id.Text = GLB_NLS_GUI_AUDITOR.GetString(344)                  ' 344 "Vendedor"
    chk_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4896)                   ' 332 "Bloqueado"
    ef_terminal_type_build.Text = GLB_NLS_GUI_CONFIGURATION.GetString(434)  ' 434 "Tipo / Versi�n"
    cmb_status.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259)              ' 259 "Estado"
    'gb_dates.Text = GLB_NLS_GUI_CONFIGURATION.GetString(433)                ' 433 "Fechas"
    dtp_retirement_date.Text = GLB_NLS_GUI_CONFIGURATION.GetString(455)     ' 455 "Fecha de Retirada"
    ef_retirement_date.Text = GLB_NLS_GUI_CONFIGURATION.GetString(455)      ' 455 "Fecha de Retirada"
    ef_retirement_requested.Text = GLB_NLS_GUI_CONFIGURATION.GetString(456) ' 456 "Orden de Retirada"
    ef_program.Text = GLB_NLS_GUI_CONFIGURATION.GetString(418)              ' 418 "Programa"
    ef_tax_registration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1100)  ' 1100 "Registro fiscal"

    'LTC 2018-JAN-08
    uc_ds_manufacturing_date.Name = String.Empty
    lbl_date_manufacturing.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8970)  ' 8970 "Manufacturing Date"

    ef_theoretical_payout.Text = GLB_NLS_GUI_CONFIGURATION.GetString(419)   ' 419 "Payout te�rico (Porcentaje)"
    opt_single.Text = GLB_NLS_GUI_CONFIGURATION.GetString(420)              ' 420 "Denominaci�n"
    opt_multi.Text = GLB_NLS_GUI_CONFIGURATION.GetString(421)               ' 421 "Multidenominaci�n"

    'SSC 14-FEB-2012
    cmb_game_type.Text = GLB_NLS_GUI_CONFIGURATION.GetString(427)           ' 427 "Tipo de juego"
    ef_activation_date.Text = GLB_NLS_GUI_CONFIGURATION.GetString(428)      ' 428 "Fecha de entrada"
    ef_creation_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8279)     ' 8279 "Fecha de creaci�n"
    ef_replacement_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8280)  ' 8280 "Fecha de reemplazo"
    ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)              ' 430 "Zona"
    cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                ' 431 "Isla"
    ef_floor_id.Text = GLB_NLS_GUI_CONFIGURATION.GetString(432)             ' 432 "Id en planta"
    cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                ' 435 "Area"
    ef_cabinet_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1640)
    ef_jackpot_contribution_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1641)
    ef_position.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222)
    ef_top_award.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5223)
    ef_max_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5224)
    ef_number_lines.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5225)

    ef_machine_id.Text = GeneralParam.GetString("EGM", "MachineId.Name")
    ef_machine_id.Visible = Not String.IsNullOrEmpty(ef_machine_id.Text)

    ef_game_theme.Text = GeneralParam.GetString("EGM", "GameTheme.Name")
    ef_game_theme.Visible = Not String.IsNullOrEmpty(ef_game_theme.Text)

    ' Serial Number
    gb_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1639)
    ef_reported_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6048) ' "Reportado"
    ef_configured_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6049) ' "Configurado"
    btn_reset_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6117) ' "Set"

    ' 3GS

    gb_3GS.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920) ' 3GS

    ' Asset Number
    gb_asset_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6143)
    ef_reported_asset_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6048) ' "Reportado"
    ef_configured_asset_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6049) ' "Configurado"
    ef_reported_asset_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 19)
    ef_configured_asset_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 19)
    btn_reset_asset_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6117) ' "Set"

    'Account denomination
    ef_accounting_denomination.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7302) ' "Denominaci�n contable"
    ef_accounting_denomination.TextAlign = HorizontalAlignment.Right
    ef_accounting_denomination.IsReadOnly = True


    ef_min_ticket_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7676) '
    chk_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7677) ' 
    lbl_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7678) ' 

    Me.chk_equity_percentage.Location = New Point(58, 322)

    ''''' COLJUEGOS '''''
    If IsColJuegosEnabled() Then
      'Rename fields
      ' NUID
      ef_external_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6384)          ' 6384 "NUID"
      ' NUC
      ef_tax_registration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6385)     ' 6385 "NUC"
      ' Serial
      gb_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6386)        ' 6386 "Serial"

      'New fields
      ' Brand code
      ef_brand_code.Visible = True
      ef_brand_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6387)           ' 6387 "C�digo marca"
      ' Model
      ef_model.Visible = True
      ef_model.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6388)                ' 6388 "Modelo"
      ' Manufacture year
      ef_manufacture_year.Visible = True
      ef_manufacture_year.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6389)     ' 6389 "A�o de fabricaci�n"
      ' Bet code
      ef_bet_code.Visible = True
      ef_bet_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6391)             ' 6391 "C�digo de apuesta"
      ' MET Homologated
      chk_met_homologated.Visible = True
      chk_met_homologated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6392)     ' 6392 "MET homologada"

      ' Reposition if MachineId isn't visible
      If Not ef_machine_id.Visible Then
        chk_met_homologated.Location = New Point(Me.chk_met_homologated.Location.X, Me.ef_bet_code.Location.Y)
        ef_bet_code.Location = New Point(Me.ef_bet_code.Location.X, Me.ef_manufacture_year.Location.Y)
        ef_manufacture_year.Location = New Point(Me.ef_manufacture_year.Location.X, Me.ef_model.Location.Y)
        ef_model.Location = New Point(Me.ef_model.Location.X, Me.ef_brand_code.Location.Y)
        ef_brand_code.Location = New Point(Me.ef_brand_code.Location.X, Me.ef_machine_id.Location.Y)

        gb_contract.Location = New Point(Me.gb_contract.Location.X, Me.gb_contract.Location.Y - 28)
        gb_dates.Location = New Point(Me.gb_dates.Location.X, Me.gb_dates.Location.Y - 28)
      End If

      'LTC 2018-JAN-08
      uc_ds_manufacturing_date.Visible = False
      lbl_date_manufacturing.Visible = False

    Else
      ' Reposition GroupBox
      If String.IsNullOrEmpty(GeneralParam.GetString("EGM", "MachineId.Name")) Then
        _offset_Y = 150
      Else
        _offset_Y = 130
      End If
    End If

    If (_offset_Y <> 0) Then
      gb_contract.Location = New Point(Me.gb_contract.Location.X, Me.gb_contract.Location.Y - _offset_Y)
      gb_dates.Location = New Point(Me.gb_dates.Location.X, Me.gb_dates.Location.Y - _offset_Y)
    End If

    If Not ef_game_theme.Visible Then
      Me.m_form_default_size.Height -= 25

      Me.ef_equity_percentage.Location = Me.opt_multi.Location

      Me.opt_multi.Location = Me.opt_single.Location
      Me.cmb_multi_denomination.Location = Me.cmb_single_denomination.Location

      Me.opt_single.Location = New Point(Me.opt_single.Location.X, Me.cmb_currency.Location.Y)
      Me.cmb_single_denomination.Location = New Point(Me.cmb_single_denomination.Location.X, Me.cmb_currency.Location.Y)

      Me.cmb_currency.Location = New Point(Me.cmb_currency.Location.X, Me.ef_number_lines.Location.Y)

      Me.ef_number_lines.Location = Me.ef_game_theme.Location

      Me.gb_denomination.Size = New Size(Me.gb_denomination.Size.Width, Me.gb_denomination.Size.Height - 5)

      gb_SAS.Location = New Point(gb_denomination.Left, gb_denomination.Bottom + 2)

    End If

    ' JCOR 02-JUL-2013
    gb_contract.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2338)
    cmb_contract_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2339)
    ef_contract_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2340)
    ef_order_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2341)
    ef_contract_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_order_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Call StatusComboFill()

    Call CurrencyComboFill()

    ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TERMINAL_NAME)
    ef_external_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TERMINAL_NAME)
    ef_provider_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TERMINAL_NAME)
    ef_vendor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TERMINAL_NAME)
    ef_terminal_type_build.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TERMINAL_NAME)
    dtp_retirement_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, 0)
    dtp_retirement_date.ShowCheckBox = False
    ef_program.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 30)

    If IsColJuegosEnabled() Then
      ef_tax_registration.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 10, 0) ' NUC (Colombia)
    Else
      ef_tax_registration.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
      _regular_expression = WSI.Common.GeneralParam.GetString("Terminal", "TaxRegistration.Mask", _regular_expression_tax_registration_default)
    End If

    ef_tax_registration.TextAlign = HorizontalAlignment.Left

    ef_theoretical_payout.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    ef_reported_serial_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_reported_3gs_serial_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_cabinet_type.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_position.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4, 0)
    ef_position.TextAlign = HorizontalAlignment.Left
    ef_machine_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ef_top_award.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 15, 2)
    ef_max_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    ef_number_lines.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 10)
    ef_jackpot_contribution_pct.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7, 4)

    ef_game_theme.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ' Ini ColJuegos
    ef_brand_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_model.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ef_manufacture_year.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 4, 0)
    ef_bet_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    ' End ColJuegos

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_AUDITOR.GetString(466)   ' 466 "Retirar"

    ef_terminal_type_build.IsReadOnly = True
    ef_terminal_type_build.TabStop = False
    ef_external_id.IsReadOnly = Not (_read_item.IsNewOffline Or (_read_item.Type = TerminalTypes.OFFLINE))
    ef_external_id.TabStop = False
    ef_retirement_date.IsReadOnly = True
    ef_retirement_date.TabStop = False
    ef_retirement_requested.IsReadOnly = True
    ef_retirement_requested.TabStop = False
    ef_smoking.IsReadOnly = True
    ef_smoking.TabStop = False
    ef_configured_serial_number.IsReadOnly = True
    ef_configured_asset_number.IsReadOnly = True

    gb_SAS.Enabled = False
    gb_3GS.Visible = False

    gb_SAS.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096)
    gb_dates.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1165)
    gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)
    gb_denomination.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164)

    Me.lbl_meter_delta.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6061)
    _meter_delta_table = GUI_GetTableUsingSQL("SELECT TMD_ID, TMD_DESCRIPTION FROM TERMINAL_METER_DELTA ORDER BY TMD_DESCRIPTION", Integer.MaxValue)
    Me.cmb_meter_delta.Clear()
    Me.cmb_meter_delta.Add(_meter_delta_table)
    Me.cmb_meter_delta.SelectedIndex = -1

    'SSC 14-FEB-2012
    ef_floor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 6, 0)
    ef_activation_date.IsReadOnly = True
    ef_activation_date.TabStop = False

    'ATB 09-MAY-2017
    ef_creation_date.IsReadOnly = True
    ef_creation_date.TabStop = False
    ef_replacement_date.IsReadOnly = True
    ef_replacement_date.TabStop = False

    'DHA 01-APR-2014
    gb_signature_validation.Enabled = False

    'FAV 29-MAY-2015
    gb_communication_type.Visible = True
    gb_communication_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6379)
    cmb_communication_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2777)
    Call CommunicationTypeComboFill(_read_item.Type, _read_item.CommunicationType)

    'FAV 29-MAY-2015
    cmb_smib_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2775)
    Call SmibConfigurationComboFill()

    ' DDS 23-MAR-2016

    ' 3GS Serial Number
    ef_reported_3gs_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1639) ' 1639 N�mero de serie


    'DDS 05-JAN-2016
    cmb_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2076)

    ' DDS 23-MAR-2016

    ' FAV 17-AUG-2016
    ef_sas_host_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7511) ' "Host ID"
    ef_sas_host_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 4, 0)

    ' FAV 18-OCT-2016
    Me.ef_min_denomination.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7679)
    Me.ef_min_denomination.SetFilter(FORMAT_MONEY, 8, 2)

    Select Case m_system_mode
      Case SYSTEM_MODE.TITO
        Me.Size = Me.m_form_tito_size

        gb_ticket_configuration.Location = New Point(gb_dates.Left, gb_dates.Bottom + 2)

        gb_ticket_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2828)                ' 2828 "Ticket Configuration"
        opt_personalized.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2776)                       ' 2776 "Personalized"
        opt_default.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2778)                            ' 2778 "Default"
        chk_allow_redemption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2717)                   ' 2717 "Allow ticket in"
        chk_allow_cashable_emission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2718) & "*"      ' 2718 "Allow cashable ticket out*"
        chk_allow_promotional_emission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2719) & "*"   ' 2719 "Allow promotional ticket out*"
        ef_max_ticket_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2293)
        ef_max_ticket_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
        ef_max_ticket_out.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2864)
        ef_max_ticket_out.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
        lbl_ticket_out_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2865)
        gb_signature_validation.Visible = False

        ef_min_ticket_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7676)
        ef_min_ticket_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
        chk_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7677) ' 
        lbl_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7678) ' 

        'FAV 29-MAY-2015
        gb_communication_type.Location = New Point(gb_ticket_configuration.Left, gb_ticket_configuration.Bottom + 2)

        'SGB 07-JUL-2015
        lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558).ToUpper()

        'FOS 08-SEP-2015
        gb_denomination.Size = New Size(gb_denomination.Width, gb_denomination.Height + 35)

        chk_coin_collection.Location = New Point(57, cmb_multi_denomination.Location.Y + cmb_multi_denomination.Height + 35)
        chk_coin_collection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6706)

        'RGR 20-NOV-2015
        Me.ef_equity_percentage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6950)
        Me.ef_equity_percentage.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

        If Not ef_game_theme.Visible Then
          'RGR 05-JAN-2015
          Me.ef_equity_percentage.Location = New Point(14, 316)
        Else
          Me.chk_equity_percentage.Location = New Point(58, 354)
          gb_denomination.Size = New Size(gb_denomination.Size.Width, gb_denomination.Size.Height + 25)
        End If

        gb_SAS.Location = New Point(gb_SAS.Location.X, gb_denomination.Location.Y + gb_denomination.Height + 5)

        ' FAV 30-MAY-2016 Only for TITO mode
        chk_coin_collection.Visible = True
        ef_equity_percentage.Visible = True


      Case SYSTEM_MODE.WASS
        'DHA 01-APR-2014
        Me.Size = Me.m_form_wass_size
        gb_ticket_configuration.Visible = False

        gb_dates.Size = New Drawing.Size(375, 200)
        chk_machine_blocked.Visible = True
        lbl_machine_blocked_reason.Visible = True
        chk_machine_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4782)
        lbl_machine_blocked_reason.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4783, "")

        gb_signature_validation.Location = New Point(gb_dates.Left, gb_dates.Bottom + 2)
        gb_signature_validation.Visible = True
        gb_signature_validation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4778)
        lbl_authentication_method.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4768, "")
        lbl_authentication_seed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4769, "")
        lbl_authentication_signature.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4770, "")
        lbl_authentication_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4779)
        lbl_authentication_last_checked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4781, "")

        'FAV 29-MAY-2015
        gb_communication_type.Location = New Point(gb_signature_validation.Left, gb_signature_validation.Bottom + 2)

      Case SYSTEM_MODE.MICO2
        Me.Size = Me.m_form_tito_size

        gb_ticket_configuration.Location = New Point(gb_dates.Left, gb_dates.Bottom + 2)

        gb_ticket_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2828)                ' 2828 "Ticket Configuration"
        opt_personalized.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2776)                       ' 2776 "Personalized"
        opt_default.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2778)                            ' 2778 "Default"
        chk_allow_redemption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2717)                   ' 2717 "Allow ticket in"
        chk_allow_cashable_emission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2718) & "*"      ' 2718 "Allow cashable ticket out*"
        chk_allow_promotional_emission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2719) & "*"   ' 2719 "Allow promotional ticket out*"
        ef_max_ticket_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2293)
        ef_max_ticket_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
        ef_max_ticket_out.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2864)
        ef_max_ticket_out.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
        lbl_ticket_out_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2865)
        gb_signature_validation.Visible = False

        ef_min_ticket_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7676)
        ef_min_ticket_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
        chk_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7677) ' 
        lbl_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7678) ' 

        'FAV 29-MAY-2015
        gb_communication_type.Location = New Point(gb_ticket_configuration.Left, gb_ticket_configuration.Bottom + 2)

        'SGB 07-JUL-2015
        lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558).ToUpper()

        'FOS 08-SEP-2015
        gb_denomination.Size = New Size(gb_denomination.Width, gb_denomination.Height + 35)
        gb_SAS.Location = New Point(gb_SAS.Location.X, gb_denomination.Location.Y + gb_denomination.Height + 5)
        gb_SAS.Size = New Size(gb_SAS.Width, gb_SAS.Height + 20)

        chk_coin_collection.Location = New Point(57, cmb_multi_denomination.Location.Y + cmb_multi_denomination.Height + 35)
        chk_coin_collection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6706)

        'RGR 20-NOV-2015
        Me.ef_equity_percentage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6950)
        Me.ef_equity_percentage.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

        'RGR 05-JAN-2015
        Me.ef_equity_percentage.Location = New Point(14, 316)

        ' FAV 30-MAY-2016 Only for TITO mode
        chk_coin_collection.Visible = True
        ef_equity_percentage.Visible = True

      Case Else
        'FJC 30/09/2014
        Me.Size = Me.m_form_default_size
        gb_ticket_configuration.Visible = False
        gb_signature_validation.Visible = False
        If ef_game_theme.Visible Then
          gb_denomination.Size = New Size(gb_denomination.Size.Width, gb_denomination.Size.Height + 5)
        Else
          gb_denomination.Size = New Size(gb_denomination.Size.Width, gb_denomination.Size.Height - 15)
        End If

        gb_SAS.Location = New Point(gb_SAS.Location.X, gb_denomination.Location.Y + gb_denomination.Height + 5)

        'FAV 29-MAY-2015
        gb_communication_type.Location = New Point(gb_dates.Left, gb_dates.Bottom + 2)

    End Select

    'Call set form features resolutions when size's form exceeds desktop area
    Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)

    Call GameTypeComboFill()

    Call AreaComboFill()

    Call BankComboFill()

    Call AddHandles()

    Call ProvidersComboFill()

    Call TypeContractComboFill()

    'SAS Flags
    Me.uc_sas_flags.Init(GUI_Controls.uc_sas_flags.ENUM_SAS_FLAGS_TYPE.TERMINAL_EDIT)

    If m_system_mode = SYSTEM_MODE.TITO Then
      Me.uc_sas_flags.IsReadOnly(GUI_Controls.uc_sas_flags.ENUM_SAS_FLAGS_COMBOS.CMD1B_PROMOTIONAL_CREDITS)
    End If

    If Not GeneralParam.GetBoolean("Progressives", "Enabled") Then
      Me.uc_sas_flags.IsReadOnly(GUI_Controls.uc_sas_flags.ENUM_SAS_FLAGS_COMBOS.PROGRESSIVE_METERS)
    End If

    chk_equity_percentage.Visible = (m_system_mode = SYSTEM_MODE.TITO Or m_system_mode = SYSTEM_MODE.MICO2)

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _read_item As CLASS_TERMINAL
    Dim _num_play_sessions As Integer
    Dim _aux_date As Date
    Dim _max_started As Date
    Dim _max_finished As Date
    Dim _external_protocol_enabled As Int32
    Dim _terminal_open_play_sessions As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)
    Dim _equity_percentage As Decimal
    Dim nls_param1 As String
    Dim nls_param2 As String

    _aux_date = WSI.Common.Misc.TodayOpening.AddDays(1)
    _read_item = Me.DbReadObject
    m_terminal_to_pending = 0

    If StatusSelection = WSI.Common.TerminalStatus.RETIRED Then
      ' Selected retirement date can not be later than tomorrow's working day
      If dtp_retirement_date.Value > _aux_date Then
        ' 198 "La fecha de retirada seleccionada no puede ser posterior a la jornada de ma�ana."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(198), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

        Return False
      End If



      If _read_item.Status = WSI.Common.TerminalStatus.RETIRED _
     And _read_item.RetirementDate <> Nothing _
     And dtp_retirement_date.Value > _read_item.RetirementDate Then
        ' 199 "La fecha de retirada seleccionada debe ser anterior a la fecha de retirada (%2) del terminal %1."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(199), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        _read_item.Name, _
                        _read_item.RetirementDate)

        Return False
      End If

      ' The terminal can not have play sessions after the working day related to the selected retirement date
      _num_play_sessions = CountPlaySessions(_read_item.TerminalId, _
                                             WSI.Common.Misc.Opening(dtp_retirement_date.Value.AddDays(1)), _
                                             _max_started, _
                                             _max_finished)
      If _num_play_sessions > 0 Then
        ' 200 "La fecha de retirada seleccionada debe ser posterior a la �ltima sesi�n de juego del terminal %1."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(200), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        _read_item.Name)

        Return False
      End If

      m_play_sessions_to_close = New List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)()

      ' Check if there are still open play sessions
      _terminal_open_play_sessions = GetOpenPlaySessions(_read_item.TerminalId)
      If _terminal_open_play_sessions.Count > 0 Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4441), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      _read_item.Name) = ENUM_MB_RESULT.MB_RESULT_YES Then

          m_play_sessions_to_close.AddRange(_terminal_open_play_sessions)
          m_play_sessions_retirement_action = CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.CLOSE
        Else
          m_play_sessions_retirement_action = CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.REMAIN_OPEN
        End If
      End If

      'FAV 09-MAR-2016 It checks if there is a money_collection in Open status for the Terminal
      Dim _current_money_collection As WSI.Common.TITO.MoneyCollection
      Using _db_trx As New WSI.Common.DB_TRX()
        _current_money_collection = WSI.Common.TITO.MoneyCollection.DB_ReadCurrentMoneyCollection(_read_item.TerminalId, _db_trx.SqlTransaction)

        If (Not _current_money_collection Is Nothing) Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7180), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      _current_money_collection.StackerId, _
                      _read_item.Name) = ENUM_MB_RESULT.MB_RESULT_YES Then

            m_terminal_to_pending = _read_item.TerminalId
          Else
            Return False
          End If
        End If
      End Using

    End If

    ' PAYOUT TEORICO
    If Me.ef_theoretical_payout.Value <> "" Then
      Dim _d As Double = 0

      If Double.TryParse(Me.ef_theoretical_payout.Value, _d) AndAlso (_d < 0 Or _d > 100) Then
        ' 131 "El campo %1 debe de estar entre el valor %2 y %3."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        ef_theoretical_payout.Text, 0, 100)

        Call ef_theoretical_payout.Focus()

        Return False
      End If

    End If

    ' JACKPOT CONTRIBUTION
    If Me.ef_jackpot_contribution_pct.Value <> "" Then
      Dim _d As Double = 0

      If Double.TryParse(Me.ef_jackpot_contribution_pct.Value, _d) AndAlso (_d < 0 Or _d > 100) Then
        ' 131 "El campo %1 debe de estar entre el valor %2 y %3."
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        ef_jackpot_contribution_pct.Text, 0, 100)

        Call ef_jackpot_contribution_pct.Focus()

        Return False
      End If

    End If

    'Validating Terminal name
    If Me.ef_name.Value.Trim().Length = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      ef_name.Text)

      Return False
    End If

    'Validating Provider id
    If Me.ef_provider_id.Value.Trim().Length = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_provider_id.Text)

      Return False
    End If

    'Validating tax registration
    If Me.ef_tax_registration.Value.Trim().Length = 0 Then

      If Not Int32.TryParse(WSI.Common.GeneralParam.Value("ExternalProtocol", "Enabled"), _external_protocol_enabled) Then
        _external_protocol_enabled = 0
      End If

      If _external_protocol_enabled = 1 Then

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1101), _
                             ENUM_MB_TYPE.MB_TYPE_WARNING, _
                             ENUM_MB_BTN.MB_BTN_YES_NO, _
                             ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                             ef_tax_registration.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Me.ef_tax_registration.Focus()
          Return False
        End If

      End If
    End If

    'If change the provider
    If m_provider_id <> "-1" And Me.cmb_provider.TextValue <> m_provider_id Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1917), _
                                   ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                   ENUM_MB_BTN.MB_BTN_YES_NO, _
                                   ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                                   ef_tax_registration.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Me.cmb_provider.Focus()
        Return False
      End If
    End If

    'Position
    If Not String.IsNullOrEmpty(ef_position.Value) AndAlso Not ef_position.ValidateFormat() Then
      'The format of the field %1 is not correct.
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_position.Text)
      Me.ef_position.Focus()

      Return False
    End If

    'Top Award
    If Not String.IsNullOrEmpty(ef_top_award.Value) AndAlso Not ef_top_award.ValidateFormat() Then
      'The format of the field %1 is not correct.
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_top_award.Text)
      Me.ef_top_award.Focus()

      Return False
    End If

    'Max Bet
    If Not String.IsNullOrEmpty(ef_max_bet.Value) AndAlso Not ef_max_bet.ValidateFormat() Then
      'The format of the field %1 is not correct.
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_max_bet.Text)
      Me.ef_max_bet.Focus()

      Return False
    End If

    'Tito configuration
    If m_system_mode = SYSTEM_MODE.TITO Then
      If opt_personalized.Checked And GUI_ParseCurrency(ef_max_ticket_in.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_max_ticket_in.Text)
        Me.ef_max_ticket_in.Focus()

        Return False
      End If
      If opt_personalized.Checked And GUI_ParseCurrency(ef_max_ticket_out.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_max_ticket_out.Text)
        Me.ef_max_ticket_out.Focus()

        Return False
      End If

      If opt_personalized.Checked And GUI_ParseCurrency(ef_min_ticket_in.Value) < 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), _
                           ENUM_MB_TYPE.MB_TYPE_WARNING, _
                           ENUM_MB_BTN.MB_BTN_OK, _
                           ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                           ef_min_ticket_in.Text)
        Me.ef_min_ticket_in.Focus()

        Return False
      End If

    End If

    'FAV 11-JUN-2015
    'Validating Pulse and Configuration selection
    If cmb_communication_type.Value = SMIB_COMMUNICATION_TYPE.PULSES And cmb_smib_configuration.SelectedIndex = -1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(6380))

      Me.cmb_smib_configuration.Focus()
      Return False
    End If

    'RGR 20-NOV-2015
    'Validating 0 < equity_percentage < 100 
    If m_system_mode = SYSTEM_MODE.TITO Or m_system_mode = SYSTEM_MODE.MICO2 Then

      If chk_equity_percentage.Checked Then

        If String.IsNullOrEmpty(Me.ef_equity_percentage.Value) Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_OK, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                         ef_equity_percentage.Text, 0, 100)
          Return False

        End If

        _equity_percentage = Decimal.Parse(Me.ef_equity_percentage.Value)

        If _equity_percentage < 0 OrElse _equity_percentage > 100 Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        ef_equity_percentage.Text, 0, 100)
          Return False

        End If

      End If

      If WSI.Common.Misc.IsFloorDualCurrencyEnabled() Then
        If Not _read_item.CurrencyIsoCode Is Nothing Then
          If Not (_read_item.CurrencyIsoCode.Equals(cmb_currency.TextValue)) Then
            If TerminalHadActivity(_read_item.TerminalId) Then

              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7086), _
                            ENUM_MB_TYPE.MB_TYPE_WARNING, _
                            ENUM_MB_BTN.MB_BTN_OK, _
                            ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

              Return False
            End If
          End If
        End If
      End If
    End If

    'RGR 20-NOV-2015
    'Validating 0 < equity_percentage < 100 
    If TITO.Utils.IsTitoMode And Not WSI.Common.Misc.IsMico2Mode() Then

      If chk_equity_percentage.Checked Then

        If String.IsNullOrEmpty(Me.ef_equity_percentage.Value) Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_OK, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                         ef_equity_percentage.Text, 0, 100)
          Return False

        End If

        _equity_percentage = Decimal.Parse(Me.ef_equity_percentage.Value)

        If _equity_percentage < 0 OrElse _equity_percentage > 100 Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(131), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_OK, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        ef_equity_percentage.Text, 0, 100)
          Return False

        End If

      End If

    End If

    If Not (String.IsNullOrEmpty(Me.ef_sas_host_id.Value)) AndAlso Int32.Parse(Me.ef_sas_host_id.Value) > 0 _
      AndAlso Not (_read_item.SASHostId.Equals(Int32.Parse(Me.ef_sas_host_id.Value))) Then
      Dim _terminal_id As Int32 = 0

      Using _db_trx As New WSI.Common.DB_TRX()
        Terminal.Trx_GetActiveTerminalByHostId(Int32.Parse(Me.ef_sas_host_id.Value), _db_trx.SqlTransaction, _terminal_id)

        If (_terminal_id > 0) Then
          Dim _terminal_info As Terminal.TerminalInfo = Nothing

          Terminal.Trx_GetTerminalInfo(_terminal_id, _terminal_info, _db_trx.SqlTransaction)

          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7511)
          nls_param2 = _terminal_info.Name

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7512), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, nls_param2)
          Return False
        End If
      End Using
    End If

    'Validation data tax regsitration
    If Not IsColJuegosEnabled() Then
      If Not WSI.Common.ValidateFormat.CheckRegularExpression(ef_tax_registration.TextValue, _regular_expression) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , Me.ef_tax_registration.Text)
        Call ef_tax_registration.Focus()

        Return False
      End If
    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _edit_item As CLASS_TERMINAL
    Dim _read_item As CLASS_TERMINAL
    Dim table As DataTable

    _edit_item = DbEditedObject

    _edit_item.Name = ef_name.Value.Trim() & IIf(_edit_item.Change = 0, String.Empty, ef_name.SufixText)
    _edit_item.BaseName = ef_name.Value.Trim()
    _edit_item.Status = StatusSelection
    _edit_item.ExternalId = ef_external_id.Value
    _edit_item.ProviderId = ef_provider_id.Value.Trim()
    _edit_item.VendorId = ef_vendor_id.Value
    _edit_item.Blocked = chk_blocked.Checked
    _edit_item.RetirementPlaySessions = m_play_sessions_retirement_action

    If _edit_item.Status = WSI.Common.TerminalStatus.RETIRED Then
      _edit_item.RetirementDate = dtp_retirement_date.Value

      _read_item = DbReadObject
      If _read_item.RetirementDate <> _edit_item.RetirementDate Then
        _edit_item.RetirementRequested = Nothing
      End If

    ElseIf _edit_item.Status = WSI.Common.TerminalStatus.ACTIVE Then
      _edit_item.RetirementDate = Nothing
      _edit_item.RetirementRequested = Nothing
    End If

    If opt_single.Checked And cmb_single_denomination.SelectedIndex >= 0 Then
      _edit_item.Denomination = cmb_single_denomination.TextValue(cmb_single_denomination.SelectedIndex)
    End If
    If opt_multi.Checked And cmb_multi_denomination.SelectedIndex >= 0 Then
      _edit_item.Denomination = cmb_multi_denomination.TextValue(cmb_multi_denomination.SelectedIndex)
    End If

    _edit_item.Program = Me.ef_program.Value
    _edit_item.TaxRegistration = Me.ef_tax_registration.Value

    'LTC 2018-JAN-08
    _edit_item.ManufactureYear = IIf(Me.uc_ds_manufacturing_date.Year = "", 0, Me.uc_ds_manufacturing_date.Year)
    _edit_item.ManufacturingMonth = IIf(Me.uc_ds_manufacturing_date.Month = "", 0, Me.uc_ds_manufacturing_date.Month)
    _edit_item.ManufacturingDay = IIf(Me.uc_ds_manufacturing_date.Day = "", 0, Me.uc_ds_manufacturing_date.Day)

    If Not String.IsNullOrEmpty(Me.ef_theoretical_payout.Value) Then
      _edit_item.TheoreticalPayout = Convert.ToDecimal(Me.ef_theoretical_payout.Value) / 100
    Else
      _edit_item.TheoreticalPayout = -1D
    End If

    _edit_item.FloorId = ef_floor_id.Value

    If Not String.IsNullOrEmpty(ef_position.Value) Then
      _edit_item.Position = Int32.Parse(ef_position.Value)
    Else
      _edit_item.Position = 0
    End If

    'Bank
    table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID FROM BANKS WHERE BK_NAME = '" & cmb_bank.TextValue & "'", Integer.MaxValue)

    If Not IsNothing(table) Then
      If table.Rows.Count() > 0 Then
        _edit_item.BankName = cmb_bank.TextValue
        _edit_item.BankId = table.Rows.Item(0).Item("BK_BANK_ID")
        _edit_item.AreaName = cmb_area.TextValue
      Else
        _edit_item.BankName = ""
        _edit_item.BankId = -1I
      End If
    End If

    'Provider
    If IsNothing(cmb_provider.TextValue) Then
      cmb_provider.TextValue = "UNKNOWN"
    End If
    If Not IsNothing(cmb_provider.TextValue) Then
      table = GUI_GetTableUsingSQL("SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = '" & cmb_provider.TextValue.Replace("'", "''") & "'", Integer.MaxValue)

      If Not IsNothing(table) Then
        If table.Rows.Count() > 0 Then
          _edit_item.ProviderId = cmb_provider.TextValue
        Else
          _edit_item.ProviderId = "UNKNOWN"
        End If
      End If
      ef_provider_id.Value = cmb_provider.TextValue
    End If


    'Game Type
    table = GUI_GetTableUsingSQL("SELECT GT_GAME_TYPE FROM GAME_TYPES WHERE GT_NAME = '" & cmb_game_type.TextValue & "' AND GT_LANGUAGE_ID = " & Resource.LanguageId, Integer.MaxValue)

    If Not IsNothing(table) Then
      If table.Rows.Count() > 0 Then
        _edit_item.GameType = table.Rows.Item(0).Item("GT_GAME_TYPE")
        _edit_item.GameTypeName = cmb_game_type.TextValue
      Else
        _edit_item.GameType = -1I
        _edit_item.GameTypeName = "---"
      End If
    End If

    If CType(_edit_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.SAS_HOST Then
      _edit_item.ExtendedMeter = Me.uc_sas_flags.ExtendedMeters
      _edit_item.CreditsPlayModeSAS = Me.uc_sas_flags.PromotionalCredits
      _edit_item.UseCmd0x1bHandpays = Me.uc_sas_flags.Cmd1BHandPays
      _edit_item.ProgressiveMeter = Me.uc_sas_flags.ProgressiveMeters
      _edit_item.BCD4 = Me.uc_sas_flags.BCD4
      _edit_item.BCD5 = Me.uc_sas_flags.BCD5
      _edit_item.No_RTE = Me.uc_sas_flags.No_RTE
      _edit_item.Single_Byte = Me.uc_sas_flags.Single_byte
      _edit_item.Ignore_no_bet_plays = Me.uc_sas_flags.Ignore_no_bet_plays
      _edit_item.AFT_although_lock_0 = Me.uc_sas_flags.AFT_although_lock_0
      _edit_item.EnableDisableNoteAcceptor = Me.uc_sas_flags.EnableDisableNoteAcceptor

      _read_item = DbReadObject
      _edit_item.ReserveTerminal = _read_item.ReserveTerminal

      _edit_item.LockEgmOnReserve = Me.uc_sas_flags.LockEgmOnReserve
    End If

    If Not String.IsNullOrEmpty(Me.ef_jackpot_contribution_pct.Value) Then
      _edit_item.JackpotContributionPct = Convert.ToDecimal(Me.ef_jackpot_contribution_pct.Value)
    Else
      _edit_item.JackpotContributionPct = -1D
    End If

    If CType(_edit_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.SAS_HOST Then
      _edit_item.SerialNumber = ef_reported_serial_number.Value
    ElseIf CType(_edit_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.T3GS Then
      _edit_item.SerialNumber = ef_reported_3gs_serial_number.Value
    End If

    _edit_item.MachineSerialNumber = ef_configured_serial_number.Value

    If Not String.IsNullOrEmpty(ef_reported_asset_number.Value) Then
      _edit_item.AssetNumber = ef_reported_asset_number.Value
    Else
      _edit_item.AssetNumber = -1
    End If

    If Not String.IsNullOrEmpty(ef_configured_asset_number.Value) Then
      _edit_item.MachineAssetNumber = ef_configured_asset_number.Value
    Else
      _edit_item.MachineAssetNumber = -1
    End If
    _edit_item.MachineId = ef_machine_id.Value
    _edit_item.CabinetType = ef_cabinet_type.Value
    ' 01-APR-2016  LA
    _edit_item.ContractType = cmb_contract_type.Value
    _edit_item.ContractId = ef_contract_id.Value
    _edit_item.OrderNumber = ef_order_number.Value
    _edit_item.MeterDelta = cmb_meter_delta.Value
    _edit_item.MeterDeltaDesc = cmb_meter_delta.TextValue
    ' ColJuegos
    If IsColJuegosEnabled() Then
      _edit_item.BrandCode = ef_brand_code.Value
      _edit_item.Model = ef_model.Value
      _edit_item.ManufactureYear = IIf(ef_manufacture_year.Value = String.Empty, 0, ef_manufacture_year.Value)
      _edit_item.METHomologated = chk_met_homologated.Checked
      _edit_item.BetCode = ef_bet_code.Value
    End If

    If Not String.IsNullOrEmpty(ef_top_award.Value) Then
      _edit_item.TopAward = GUI_ParseCurrency(ef_top_award.Value)
    Else
      _edit_item.TopAward = 0
    End If

    If Not String.IsNullOrEmpty(ef_max_bet.Value) Then
      _edit_item.MaxBet = GUI_ParseCurrency(ef_max_bet.Value)
    Else
      _edit_item.MaxBet = 0
    End If

    _edit_item.NumberLines = ef_number_lines.Value

    _edit_item.GameTheme = ef_game_theme.Value

    If m_system_mode = SYSTEM_MODE.TITO Then
      _edit_item.TitoConfigurationType = opt_personalized.Checked
      _edit_item.TitoAllowedPromotionalEmission = chk_allow_promotional_emission.Checked
      _edit_item.TitoAllowedRedemption = chk_allow_redemption.Checked
      _edit_item.TitoAllowedCashableEmission = chk_allow_cashable_emission.Checked
      _edit_item.TitoMaxAllowedTicketIn = GUI_ParseCurrency(ef_max_ticket_in.Value)
      _edit_item.TitoMinAllowedTicketIn = GUI_ParseCurrency(ef_min_ticket_in.Value)
      _edit_item.TitoAllowTruncate = chk_allow_truncate.Checked
      _edit_item.TitoMaxAllowedTicketOut = GUI_ParseCurrency(ef_max_ticket_out.Value)
      _edit_item.TitoMinDenomination = GUI_ParseCurrency(ef_min_denomination.Value)
    End If

    'DHA 01-APR-2014
    If m_system_mode = SYSTEM_MODE.WASS Then
      If Me.chk_machine_blocked.Checked Then
        _edit_item.MachineStatus = _edit_item.MachineStatus Or MASK_MACHINE_BLOCKED_MANUALLY
      Else
        _edit_item.MachineStatus = _edit_item.MachineStatus And (Not MASK_MACHINE_BLOCKED_MANUALLY)
      End If
    End If

    'FAV 29-MAY-2015
    _edit_item.CommunicationType = cmb_communication_type.Value

    'FAV 29-MAY-2015: Smib Configuration for communication type equal to 'Pulses' 
    _edit_item.SmibConfigurationId = -1
    _edit_item.SmibConfigurationName = Nothing
    If cmb_communication_type.Value = SMIB_COMMUNICATION_TYPE.PULSES And cmb_smib_configuration.SelectedIndex <> -1 Then
      _edit_item.SmibConfigurationId = cmb_smib_configuration.Value
      _edit_item.SmibConfigurationName = cmb_smib_configuration.TextValue
    End If



    _edit_item.IsCoinCollection = chk_coin_collection.Checked


    'DDS 05-JAN-2016

    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() OrElse String.IsNullOrEmpty(_edit_item.CurrencyIsoCode) Then
      _edit_item.CurrencyIsoCode = cmb_currency.TextValue
    End If

    '22-NOV-2016 PDM

    If m_system_mode = SYSTEM_MODE.TITO Or m_system_mode = SYSTEM_MODE.MICO2 Then

      _edit_item.ChkEquityPercentage = chk_equity_percentage.Checked

      If chk_equity_percentage.Checked Then
        _edit_item.EquityPercentage = IIf(String.IsNullOrEmpty(Me.ef_equity_percentage.Value), 0, Me.ef_equity_percentage.Value)
      Else
        _edit_item.EquityPercentage = 0
      End If

    Else
      _edit_item.ChkEquityPercentage = False
      _edit_item.EquityPercentage = 0
    End If

    ''RGR 20-NOV-2015
    '_edit_item.EquityPercentage = IIf(String.IsNullOrEmpty(Me.ef_equity_percentage.Value), 0, Me.ef_equity_percentage.Value)


    _edit_item.SASHostId = ef_sas_host_id.Value

    Me.ef_accounting_denomination.Value = IIf(Me.ef_accounting_denomination.Value = Nothing, AUDIT_NONE_STRING, String.Format("{0:N2}", Me.ef_accounting_denomination.Value))

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _read_item As CLASS_TERMINAL
    Dim _retirement_date As Date
    Dim _game_type As WSI.Common.TerminalTypes
    Dim _authentication_status As String
    Dim _algorithm_method As String
    Dim _machine_block_reason As String

    _read_item = DbReadObject

    ef_name.Value = _read_item.BaseName
    If _read_item.Change <> 0 Then
      ef_name.SufixText = "-" + _read_item.Change.ToString
      ef_name.SufixTextVisible = True
      ef_name.SufixTextWidth = 100
    End If
    _machine_block_reason = ""

    m_play_sessions_retirement_action = CLASS_TERMINAL.ENUM_RETIREMENT_PLAY_SESSIONS_STATUS.NONE
    _read_item.RetirementPlaySessions = m_play_sessions_retirement_action

    ' Terminals that were retired before this sw version may have not RETIREMENT date
    If _read_item.Status = WSI.Common.TerminalStatus.RETIRED Then
      If _read_item.RetirementDate = Nothing Then
        _retirement_date = SuggestedRetirementDate(_read_item.TerminalId)
      Else
        _retirement_date = CDate(_read_item.RetirementDate.ToShortDateString())
      End If

      Me.cmb_status.Enabled = False

      ' Make sure that the date is just the date, truncate the time (if any)
      dtp_retirement_date.Value = _retirement_date
    End If

    ef_retirement_date.Value = "---"

    If _read_item.Status = WSI.Common.TerminalStatus.RETIRED _
   And _read_item.RetirementRequested <> Nothing Then
      ef_retirement_requested.Value = GUI_FormatDate(_read_item.RetirementRequested, _
                                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                     ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    Else
      ef_retirement_requested.Value = "---"
    End If

    StatusSelection = _read_item.Status

    ef_external_id.Value = _read_item.ExternalId
    ef_provider_id.Value = IIf(_read_item.ProviderId Is Nothing, cmb_provider.TextValue, _read_item.ProviderId)
    ef_vendor_id.Value = _read_item.VendorId
    chk_blocked.Checked = _read_item.Blocked

    ef_vendor_id.IsReadOnly = (_read_item.Type = WSI.Common.TerminalTypes.T3GS)

    ef_terminal_type_build.Value = _read_item.TypeName + " / " + _read_item.ClientId.ToString("00") + "." + _read_item.BuildId.ToString("000")

    Call DenominationsCombosFill(_read_item.Denomination, True)

    Me.ef_program.Value = _read_item.Program
    Me.ef_tax_registration.Value = _read_item.TaxRegistration

    'LTC 2018-JAN-08
    Me.uc_ds_manufacturing_date.Year = IIf(_read_item.ManufactureYear = 0, "", _read_item.ManufactureYear)
    Me.uc_ds_manufacturing_date.Month = IIf(_read_item.ManufacturingMonth = 0, "", _read_item.ManufacturingMonth)
    Me.uc_ds_manufacturing_date.Day = IIf(_read_item.ManufacturingDay = 0, "", _read_item.ManufacturingDay)

    If _read_item.TheoreticalPayout >= 0 Then
      Me.ef_theoretical_payout.Value = _read_item.TheoreticalPayout * 100
    Else
      Me.ef_theoretical_payout.Value = ""
    End If

    If Not m_system_mode = SYSTEM_MODE.WASS AndAlso Not (_read_item.MachineStatus And MASK_MACHINE_ALWAYS_UNBLOCKED) = MASK_MACHINE_ALWAYS_UNBLOCKED Then
      If (_read_item.MachineStatus And MASK_MACHINE_BLOCKED_MANUALLY) = MASK_MACHINE_BLOCKED_MANUALLY OrElse _
         (_read_item.MachineStatus And MASK_MACHINE_BLOCKED_AUTOMATICALLY) = MASK_MACHINE_BLOCKED_AUTOMATICALLY OrElse _
         (_read_item.MachineStatus And MASK_MACHINE_ALWAYS_BLOCKED) = MASK_MACHINE_ALWAYS_BLOCKED Then
        lbl_blocked.Visible = True
      End If
    End If

    ef_program.Enabled = False
    ef_theoretical_payout.Enabled = False
    gb_denomination.Enabled = False

    For Each _game_type In WSI.Common.Misc.GamingTerminalTypeList()

      If _game_type = _read_item.Type Then
        ef_program.Enabled = True
        ef_theoretical_payout.Enabled = True
        gb_denomination.Enabled = True

        Exit For
      End If

    Next

    ' SSC 14-FEB-2012 
    'Bank and area
    ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If _read_item.AreaName <> "" Then
      cmb_area.TextValue = _read_item.AreaName
    Else
      cmb_area.SelectedIndex = 0
    End If

    If _read_item.AreaName <> "" Then
      cmb_bank.TextValue = _read_item.BankName
    Else
      cmb_bank.SelectedIndex = 0
    End If

    'Provider
    If _read_item.ProviderId <> "" Then
      cmb_provider.TextValue = _read_item.ProviderId
      m_provider_id = _read_item.ProviderId
      If IsNothing(cmb_provider.TextValue) Then
        cmb_provider.TextValue = "UNKNOWN"
      End If

    Else
      cmb_provider.SelectedIndex = 0
      m_provider_id = "-1"
    End If

    ef_floor_id.Value = _read_item.FloorId

    'Game Type
    If _read_item.GameTypeName <> "" Then
      cmb_game_type.TextValue = _read_item.GameTypeName
    Else
      cmb_game_type.SelectedIndex = 0
    End If

    If _read_item.ActivationDate <> Nothing Then
      ef_activation_date.Value = GUI_FormatDate(_read_item.ActivationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      ef_activation_date.Value = "---"
    End If

    'ATB 09-MAY-2017
    If _read_item.CreationDate <> Nothing Then
      ef_creation_date.Value = GUI_FormatDate(_read_item.CreationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      ef_creation_date.Value = "---"
    End If
    If _read_item.ReplacementDate <> Nothing Then
      ef_replacement_date.Value = GUI_FormatDate(_read_item.ReplacementDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      ef_replacement_date.Value = "---"
    End If

    gb_SAS.Enabled = False
    gb_ticket_configuration.Enabled = False

    'DDS 23-MAR-2016
    gb_3GS.Visible = False
    If CType(_read_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.T3GS Then
      gb_SAS.Visible = False
      gb_3GS.Visible = True
      'DDS 23-MAR-2016
      gb_3GS.Location = gb_SAS.Location
      gb_3GS.Width = gb_SAS.Width
      Me.ef_reported_3gs_serial_number.Location = New Point(Me.ef_floor_id.Location.X, Me.ef_reported_3gs_serial_number.Location.Y)
    End If

    If CType(_read_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.SAS_HOST Then
      gb_SAS.Enabled = True

      Me.uc_sas_flags.ExtendedMeters = _read_item.ExtendedMeter
      Me.uc_sas_flags.PromotionalCredits = _read_item.CreditsPlayModeSAS
      Me.uc_sas_flags.Cmd1BHandPays = _read_item.UseCmd0x1bHandpays
      Me.uc_sas_flags.ProgressiveMeters = _read_item.ProgressiveMeter

      ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
      Me.uc_sas_flags.BCD4 = _read_item.BCD4
      Me.uc_sas_flags.BCD5 = _read_item.BCD5
      Me.uc_sas_flags.No_RTE = _read_item.No_RTE
      Me.uc_sas_flags.Single_byte = _read_item.Single_Byte
      Me.uc_sas_flags.Ignore_no_bet_plays = _read_item.Ignore_no_bet_plays
      Me.uc_sas_flags.AFT_although_lock_0 = _read_item.AFT_although_lock_0
      Me.uc_sas_flags.EnableDisableNoteAcceptor = _read_item.EnableDisableNoteAcceptor
      Me.uc_sas_flags.LockEgmOnReserve = _read_item.LockEgmOnReserve
      If m_system_mode = SYSTEM_MODE.TITO Then
        gb_ticket_configuration.Enabled = True
        opt_personalized.Checked = _read_item.TitoConfigurationType
        opt_default.Checked = Not _read_item.TitoConfigurationType
        chk_allow_cashable_emission.Checked = _read_item.TitoAllowedCashableEmission
        chk_allow_redemption.Checked = _read_item.TitoAllowedRedemption
        chk_allow_promotional_emission.Checked = _read_item.TitoAllowedPromotionalEmission
        ef_max_ticket_in.Value = _read_item.TitoMaxAllowedTicketIn
        ef_max_ticket_out.Value = _read_item.TitoMaxAllowedTicketOut
        ef_min_denomination.Value = _read_item.TitoMinDenomination
        ef_min_ticket_in.Value = _read_item.TitoMinAllowedTicketIn
        chk_allow_truncate.Checked = _read_item.TitoAllowTruncate
      End If
    End If

    'DHA 01-APR-2014
    If m_system_mode = SYSTEM_MODE.WASS Then
      gb_signature_validation.Enabled = True

      If _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.CRC16 Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4772) 'CRC 16
      ElseIf _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.CRC32 Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4773) 'CRC 32
      ElseIf _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.MD5 Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4774) 'MD5
      ElseIf _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.KOBETRON_I Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4775) 'Kobetron I
      ElseIf _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.KOBETRON_II Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4776) 'Kobetron II
      ElseIf _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.SHA1 Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4777) 'SHA1
      ElseIf _read_item.AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.ROMCRC16 Then
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4780) 'ROM CRC 16
      Else
        _algorithm_method = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4771) 'None
      End If

      lbl_authentication_method.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4768, _algorithm_method)
      lbl_authentication_seed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4769, _read_item.AuthenticationSeed)
      lbl_authentication_signature.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4770, _read_item.AuthenticationSignature)

      ' Authentication last checked
      If _read_item.AuthenticationLastChecked = DateTime.MinValue Then
        Me.lbl_authentication_last_checked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4781, "")
      Else
        Me.lbl_authentication_last_checked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4781, GUI_FormatDate(_read_item.AuthenticationLastChecked, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
      End If

      ' Authentication status
      lbl_authentication_status_detail.ForeColor = Color.Navy
      Select Case (_read_item.AuthenticationStatus)
        Case ENUM_AUTHENTICATION_STATUS.ERROR
          lbl_authentication_status_detail.ForeColor = Color.Red
          _authentication_status = GLB_NLS_GUI_CONTROLS.GetString(405)
        Case ENUM_AUTHENTICATION_STATUS.OK
          _authentication_status = GLB_NLS_GUI_CONTROLS.GetString(404)
        Case Else
          _authentication_status = GLB_NLS_GUI_CONTROLS.GetString(408)
      End Select
      lbl_authentication_status_detail.Text = _authentication_status

      ' Machine Status
      If (_read_item.MachineStatus And MASK_MACHINE_BLOCKED_MANUALLY) = MASK_MACHINE_BLOCKED_MANUALLY Then
        Me.chk_machine_blocked.Checked = True
      Else
        Me.chk_machine_blocked.Checked = False
      End If

      lbl_machine_blocked_reason.Visible = True
      If (_read_item.MachineStatus And MASK_MACHINE_BLOCKED_NO_LICENSE) = MASK_MACHINE_BLOCKED_NO_LICENSE Then
        _machine_block_reason &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4789)
      End If
      If (_read_item.MachineStatus And MASK_MACHINE_BLOCKED_BY_SIGNATURE) = MASK_MACHINE_BLOCKED_BY_SIGNATURE Then
        _machine_block_reason &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4790)
      End If
      If (_read_item.MachineStatus And MASK_MACHINE_BLOCKED_MANUALLY) = MASK_MACHINE_BLOCKED_MANUALLY Then
        _machine_block_reason &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4791)
      End If
      If _machine_block_reason <> String.Empty Then
        _machine_block_reason = _machine_block_reason.Remove(0, 2)
      Else
        lbl_machine_blocked_reason.Visible = False
        _machine_block_reason = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4792)
      End If
      lbl_machine_blocked_reason.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4783, _machine_block_reason)
    End If

    If _read_item.JackpotContributionPct >= 0 Then
      Me.ef_jackpot_contribution_pct.Value = _read_item.JackpotContributionPct
    Else
      Me.ef_jackpot_contribution_pct.Value = ""
    End If

    Me.ef_cabinet_type.Value = _read_item.CabinetType
    Me.ef_position.Value = IIf(_read_item.Position > 0, _read_item.Position, "")
    Me.ef_machine_id.Value = _read_item.MachineId
    Me.ef_top_award.Value = IIf(_read_item.TopAward > 0, _read_item.TopAward, "")
    Me.ef_max_bet.Value = IIf(_read_item.MaxBet > 0, _read_item.MaxBet, "")
    Me.ef_number_lines.Value = _read_item.NumberLines

    ' ColJuegos
    If IsColJuegosEnabled() Then
      Me.ef_brand_code.Value = _read_item.BrandCode
      Me.ef_model.Value = _read_item.Model
      Me.ef_manufacture_year.Value = IIf(_read_item.ManufactureYear > 0, _read_item.ManufactureYear, "")
      Me.chk_met_homologated.Checked = _read_item.METHomologated
      Me.ef_bet_code.Value = _read_item.BetCode
    End If

    ' 01-APR-2016  LA
    If _read_item.ContractType <> "" Then
      cmb_contract_type.Value = _read_item.ContractType
    Else
      cmb_contract_type.Value = 0
    End If

    If _read_item.MeterDelta > -1 Then
      cmb_meter_delta.TextValue = _read_item.MeterDeltaDesc
    Else
      cmb_meter_delta.SelectedIndex = -1
    End If

    ef_contract_id.Value = _read_item.ContractId
    ef_order_number.Value = _read_item.OrderNumber

    ef_game_theme.Value = _read_item.GameTheme

    If CType(_read_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.SAS_HOST Then
      Me.ef_reported_serial_number.Value = _read_item.SerialNumber
    ElseIf CType(_read_item.Type, WSI.Common.TerminalTypes) = WSI.Common.TerminalTypes.T3GS Then
      Me.ef_reported_3gs_serial_number.Value = _read_item.SerialNumber
    End If

    Me.ef_configured_serial_number.Value = _read_item.MachineSerialNumber
    If _read_item.AssetNumber >= 0 Then
      Me.ef_reported_asset_number.Value = _read_item.AssetNumber
    End If
    If _read_item.MachineAssetNumber >= 0 Then
      Me.ef_configured_asset_number.Value = _read_item.MachineAssetNumber
    End If
    Call CheckSerialNumberDifference()
    Call CheckAssetNumberDifference()

    'FAV 29-MAY-2015
    cmb_smib_configuration.Visible = (cmb_communication_type.Value = SMIB_COMMUNICATION_TYPE.PULSES)

    'FAV 29-MAY-2015
    If cmb_communication_type.Value = SMIB_COMMUNICATION_TYPE.PULSES Then
      cmb_smib_configuration.Value = _read_item.SmibConfigurationId
    Else
      cmb_smib_configuration.SelectedIndex = -1
    End If

    chk_coin_collection.Checked = _read_item.IsCoinCollection

    'DDS 05-JAN-2016
    If WSI.Common.Misc.IsFloorDualCurrencyEnabled() And Not String.IsNullOrEmpty(_read_item.CurrencyIsoCode) Then
      Me.cmb_currency.TextValue = _read_item.CurrencyIsoCode
    Else
      Me.cmb_currency.TextValue = m_national_currency
    End If


    'RGR 20-NOV-2015
    If m_system_mode = SYSTEM_MODE.TITO Or m_system_mode = SYSTEM_MODE.MICO2 Then

      If _read_item.ChkEquityPercentage Then
        Me.ef_equity_percentage.Value = _read_item.EquityPercentage
      Else
        Me.ef_equity_percentage.Value = CLASS_TERMINAL.DefaultEquityPercentage()
      End If

      Me.chk_equity_percentage.Checked = _read_item.ChkEquityPercentage

      Me.ef_equity_percentage.Enabled = Me.chk_equity_percentage.Checked

    End If

    Me.ef_accounting_denomination.Value = IIf(_read_item.AccountingDenomination = Nothing OrElse _read_item.AccountingDenomination <= 0,
                                              AUDIT_NONE_STRING, String.Format("{0:N2}", _read_item.AccountingDenomination) & " " & _read_item.CurrencyIsoCode)

    Me.ef_sas_host_id.Value = _read_item.SASHostId

    If Me.cmb_status.SelectedIndex = WSI.Common.TerminalStatus.RETIRED Then
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    End If

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _edit_item As CLASS_TERMINAL
    Dim nls_param1 As String
    Dim nls_param2 As String
    'Dim rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TERMINAL
        _edit_item = DbEditedObject
        _edit_item.TerminalId = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _edit_item = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
          nls_param2 = m_input_terminal_name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _edit_item = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
          nls_param2 = _edit_item.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          ' Call ef_draw_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _edit_item = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
          nls_param2 = _edit_item.Name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _edit_item = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
          nls_param2 = _edit_item.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          '  Call ef_draw_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _edit_item = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
          nls_param2 = _edit_item.Name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          ' Call ef_draw_name.Focus()
        Else
          ' Close the open sessions if necessary
          If Not m_play_sessions_to_close Is Nothing AndAlso m_play_sessions_to_close.Count > 0 Then
            ClosePlaySessions(m_play_sessions_to_close)
          End If

          'FAV 09-MAR-2016 It changes status to Pending
          If (m_terminal_to_pending > 0) Then
            Using _db_trx As New WSI.Common.DB_TRX()
              If (WSI.Common.TITO.TITO_ChangeStacker.ChangeStackerInTitoMode(m_terminal_to_pending, Nothing, 0, False, _db_trx.SqlTransaction) _
                  And Cashier.WCPCashierSessionPendingClosing(m_terminal_to_pending, _db_trx.SqlTransaction) <> MB_CASHIER_SESSION_CLOSE_STATUS.ERROR) Then
                _db_trx.Commit()
              Else
                Log.Error(String.Format("The operation to change the Stacker to Pending to collect has returned an error. TerminalId [{0}]", m_terminal_to_pending))
              End If
            End Using
          End If

        End If

        'Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        '  ' TODO: DEPENDENCIES depends of cls_draw.vb.
        '  ' TODO: Change message 146 if DEPENDENCIES are aplicable in Draws.
        '  If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
        '    _edit_item = Me.DbEditedObject
        '    nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
        '    nls_param2 = _edit_item.Name
        '    Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(146), _
        '                    ENUM_MB_TYPE.MB_TYPE_ERROR, _
        '                    ENUM_MB_BTN.MB_BTN_OK, _
        '                    ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

        '  ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
        '    _edit_item = Me.DbEditedObject
        '    nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
        '    nls_param2 = _edit_item.Name
        '    Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(115), _
        '                    ENUM_MB_TYPE.MB_TYPE_ERROR, _
        '                    ENUM_MB_BTN.MB_BTN_OK, _
        '                    ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
        '                    nls_param1, _
        '                    nls_param2)
        '  End If

        'Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        '  DeleteOperation = False
        '  _edit_item = Me.DbEditedObject
        '  nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(470)
        '  nls_param2 = _edit_item.Name
        '  rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(111), _
        '                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
        '                  ENUM_MB_BTN.MB_BTN_YES_NO, _
        '                  ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
        '                  nls_param1, _
        '                  nls_param2)
        '  If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
        '    DeleteOperation = True
        '  End If

        'Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        '  If DeleteOperation Then
        '    Call MyBase.GUI_DB_Operation(DbOperation)
        '  Else
        '    DbStatus = ENUM_STATUS.STATUS_ERROR
        '  End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub 'GUI_SetInitialFocus

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _is_BCD_incorrect As Boolean = False
    Dim _error_message As String = ""

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call DisconnectTerminal()

        If Me.cmb_status.SelectedIndex = WSI.Common.TerminalStatus.RETIRED Then
          GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
        End If

      Case Else
        ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificaci�n de la pantalla de edici�n de terminales
        If (ButtonId = ENUM_BUTTON.BUTTON_OK) Then
          If (Me.uc_sas_flags.BCD4 < 4 Or Me.uc_sas_flags.BCD4 > 8) Then
            Me.uc_sas_flags.BCD4 = 4
            _error_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7085)
            _is_BCD_incorrect = True
          End If

          If (Me.uc_sas_flags.BCD5 < 5 Or Me.uc_sas_flags.BCD5 > 10) Then
            Me.uc_sas_flags.BCD5 = 5
            If (_is_BCD_incorrect) Then
              _error_message = "- " & _error_message & vbCrLf & "- " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7087)
            Else
              _error_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7087)
              _is_BCD_incorrect = True
            End If
          End If

          If (_is_BCD_incorrect) Then
            MessageBox.Show(_error_message)
            Exit Sub
          End If
        End If
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    Dim _terminal As CLASS_TERMINAL
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    _terminal = DbEditedObject
    _terminal.IsNewOffline = True
    _terminal.Type = TerminalTypes.OFFLINE
    m_offline_terminal = True

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)


    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - DrawId
  '       - DrawName
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal TerminalId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = TerminalId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub 'ShowEditItem

#End Region ' Overrides

#Region " Public Functions "

#End Region

#Region " Private functions "

  ' PURPOSE: Checks Serial number differens and sets color and button properties
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CheckSerialNumberDifference()

    'Serial Number
    If ef_configured_serial_number.Value <> ef_reported_serial_number.Value Then
      ef_configured_serial_number.ValueForeColor = Color.Red
      ef_reported_serial_number.ValueForeColor = Color.Red
      btn_reset_serial_number.Enabled = True
    Else
      ef_configured_serial_number.ValueForeColor = Color.Blue
      ef_reported_serial_number.ValueForeColor = Color.Blue
      btn_reset_serial_number.Enabled = False
    End If

  End Sub

  ' PURPOSE: Checks Asset number differens and sets color and button properties
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CheckAssetNumberDifference()

    'Asset Number
    If ef_configured_asset_number.Value <> ef_reported_asset_number.Value Then
      ef_configured_asset_number.ValueForeColor = Color.Red
      ef_reported_asset_number.ValueForeColor = Color.Red
      btn_reset_asset_number.Enabled = True
    Else
      ef_configured_asset_number.ValueForeColor = Color.Blue
      ef_reported_asset_number.ValueForeColor = Color.Blue
      btn_reset_asset_number.Enabled = False
    End If

  End Sub

  ' PURPOSE: Add Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent
    AddHandler cmb_communication_type.ValueChangedEvent, AddressOf cmb_communication_ValueChangedEvent

  End Sub ' AddHandles

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub cmb_area_ValueChangedEvent()

    Dim table As DataTable

    'Zone (smoking - no smoking)
    ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_NAME = '" & cmb_area.TextValue.Replace("'", "''") & "'", Integer.MaxValue)

    If Not IsNothing(table) Then
      If table.Rows.Count() > 0 Then
        If table.Rows.Item(0).Item("AR_SMOKING") Then
          ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    'Banks
    Call BankComboFill()

  End Sub

  ' PURPOSE: Status selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub cmb_status_ValueChangedEvent() Handles cmb_status.ValueChangedEvent

    Dim new_status As Integer

    ' Force the selection refresh 
    new_status = StatusSelection()
    StatusSelection = new_status

  End Sub 'cmb_status_ValueChangedEvent

  ' PURPOSE: Communication selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub cmb_communication_ValueChangedEvent() Handles cmb_status.ValueChangedEvent

    'FAV 29-MAY-2015
    cmb_smib_configuration.Visible = (cmb_communication_type.Value = SMIB_COMMUNICATION_TYPE.PULSES)

  End Sub 'cmb_communication_ValueChangedEvent

  ' PURPOSE: Disconnect EBOX from system.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DisconnectTerminal()

    Me.ef_external_id.Value = ""
    Me.ef_configured_serial_number.Value = String.Empty
    Me.ef_reported_serial_number.Value = String.Empty
    ' DDS 23-MAR-2016 Some customers use this field for other purposes. Don't clear it
    'Me.ef_reported_3gs_serial_number.Value = String.Empty
    Me.ef_configured_asset_number.Value = String.Empty
    Me.ef_reported_asset_number.Value = String.Empty

    StatusSelection = WSI.Common.TerminalStatus.RETIRED

  End Sub ' DisconnectTerminal


  ' PURPOSE: Init type contract combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub TypeContractComboFill()
    ' 01-APR-2016  LA
    Dim catalog As New Catalog()
    Dim _item_catalog_list As New List(Of WSI.Common.Catalog.Catalog_Item)
    _item_catalog_list = catalog.GetCatalogItems(catalog.CatalogSystemType.ContractType)

    cmb_contract_type.Add(0, "")

    For Each cat As WSI.Common.Catalog.Catalog_Item In _item_catalog_list
      cmb_contract_type.Add(Convert.ToInt32(cat.CatalogItemId), cat.Name)
    Next

    cmb_contract_type.Value = 0

  End Sub     ' TypeContractComboFill


  ' PURPOSE: Init status combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub StatusComboFill()

    Me.cmb_status.Add(IDX_CMB_STATUS_ACTIVE, CLASS_TERMINAL.StatusName(WSI.Common.TerminalStatus.ACTIVE))
    Me.cmb_status.Add(IDX_CMB_STATUS_OUT_OF_SERVICE, CLASS_TERMINAL.StatusName(WSI.Common.TerminalStatus.OUT_OF_SERVICE))
    Me.cmb_status.Add(IDX_CMB_STATUS_RETIRED, CLASS_TERMINAL.StatusName(WSI.Common.TerminalStatus.RETIRED))

  End Sub     ' StatusComboFill

  ' PURPOSE: Init game types combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GameTypeComboFill()
    Dim idx As Integer
    Dim table As DataTable

    table = GUI_GetTableUsingSQL("SELECT DISTINCT GT_NAME FROM GAME_TYPES WHERE GT_LANGUAGE_ID = " & Resource.LanguageId & " ORDER BY GT_NAME", Integer.MaxValue)

    For idx = 0 To table.Rows.Count - 1
      Me.cmb_game_type.Add(table.Rows.Item(idx).Item("GT_NAME"))
    Next

  End Sub     ' GameTypeComboFill

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AreaComboFill()
    Dim idx As Integer
    Dim table As DataTable

    table = GUI_GetTableUsingSQL("SELECT DISTINCT AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID ORDER BY AR_NAME", Integer.MaxValue)

    For idx = 0 To table.Rows.Count - 1
      Me.cmb_area.Add(table.Rows.Item(idx).Item("AR_NAME"))
    Next

  End Sub     ' AreaComboFill

  ' PURPOSE: Init BANKS combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub BankComboFill()
    Dim idx As Integer
    Dim table As DataTable

    Me.cmb_bank.Clear()

    If cmb_area.SelectedIndex <> -1 Then
      table = GUI_GetTableUsingSQL("SELECT DISTINCT BK_NAME FROM BANKS INNER JOIN AREAS ON AR_AREA_ID = BK_AREA_ID WHERE AR_NAME = '" & cmb_area.TextValue.Replace("'", "''") & "' ORDER BY BK_NAME", Integer.MaxValue)

      If Not IsNothing(table) Then
        If table.Rows.Count() > 0 Then
          For idx = 0 To table.Rows.Count - 1
            Me.cmb_bank.Add(table.Rows.Item(idx).Item("BK_NAME"))
          Next
        End If
      End If

    End If


  End Sub     ' BankComboFill

  ' PURPOSE: Init providers combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ProvidersComboFill()
    Dim idx As Integer
    Dim table As DataTable

    table = GUI_GetTableUsingSQL("SELECT DISTINCT PV_NAME FROM PROVIDERS WHERE PV_HIDE = 0 OR PV_NAME = 'UNKNOWN' ORDER BY PV_NAME", Integer.MaxValue)

    For idx = 0 To table.Rows.Count - 1
      Me.cmb_provider.Add(table.Rows.Item(idx).Item("PV_NAME"))
    Next

  End Sub     ' ProvidersComboFill


  ' PURPOSE: Init currencies combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CurrencyComboFill()
    Dim table As DataTable
    Dim idx As Integer
    table = Nothing

    WSI.Common.CurrencyExchange.GetFloorDualCurrenciesForCombo(table)
    m_national_currency = table.Rows(0)("CE_CURRENCY_ISO_CODE")

    For idx = 0 To table.Rows.Count - 1
      Me.cmb_currency.Add(table.Rows.Item(idx).Item("CE_CURRENCY_ISO_CODE"))
    Next

  End Sub     ' CurrencyComboFill

  ' PURPOSE: Ticket Configuration selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub opt_ticket_configuration_changed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_personalized.CheckedChanged, opt_default.CheckedChanged
    If opt_personalized.Checked Then
      chk_allow_cashable_emission.Enabled = True
      chk_allow_redemption.Enabled = True
      chk_allow_promotional_emission.Enabled = True
      ef_max_ticket_in.Enabled = True
      ef_max_ticket_out.Enabled = True
      ef_min_ticket_in.Enabled = True
      chk_allow_truncate.Enabled = True
      ef_min_denomination.Enabled = Not chk_allow_truncate.Checked
    Else
      chk_allow_cashable_emission.Enabled = False
      chk_allow_cashable_emission.Checked = False
      chk_allow_redemption.Enabled = False
      chk_allow_redemption.Checked = False
      chk_allow_promotional_emission.Enabled = False
      chk_allow_promotional_emission.Checked = False
      ef_max_ticket_in.Enabled = False
      ef_max_ticket_out.Enabled = False
      ef_min_ticket_in.Enabled = False
      chk_allow_truncate.Enabled = False
      chk_allow_truncate.Checked = False
      ef_min_denomination.Enabled = False
    End If
  End Sub

  ' PURPOSE: Load Terminal denomination data from GP and set terminal data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DenominationsCombosFill(ByVal Denomination As String, ByVal Multi As Boolean)
    Dim _terminal_denominations As CLASS_TERMINAL_DENOMINATIONS
    Dim _single_denominations As List(Of String)
    Dim _multi_denominations As List(Of String)
    Dim _select_idx As Int32
    Dim _dec_value As Decimal

    _terminal_denominations = New CLASS_TERMINAL_DENOMINATIONS()
    _terminal_denominations.ReadTerminalDenominations()

    cmb_multi_denomination.Clear()
    cmb_single_denomination.Clear()
    cmb_multi_denomination.SelectedIndex = -1
    cmb_single_denomination.SelectedIndex = -1
    opt_single.Checked = False
    opt_multi.Checked = False

    _multi_denominations = _terminal_denominations.MultiDenominations
    _single_denominations = _terminal_denominations.SingleDenominations

    'Denomination is not active
    If Not String.IsNullOrEmpty(Denomination) Then
      _dec_value = GUI_ParseNumber(Denomination)
      If _dec_value > 0 Then
        If Not _single_denominations.Contains(Denomination) Then
          _single_denominations.Add(Denomination)
          _single_denominations = CLASS_TERMINAL_DENOMINATIONS.SortDenominations(_single_denominations, False)
        End If
      Else
        If Not _multi_denominations.Contains(Denomination) Then
          _multi_denominations.Add(Denomination)

          _multi_denominations = CLASS_TERMINAL_DENOMINATIONS.SortDenominations(_multi_denominations, True)
        End If
      End If
    End If

    'Multi
    _select_idx = -1
    For _idx As Int32 = 0 To _multi_denominations.Count - 1
      cmb_multi_denomination.Add(_multi_denominations(_idx))
      If _multi_denominations(_idx) = Denomination Then
        _select_idx = _idx
      End If
    Next

    cmb_multi_denomination.SelectedIndex = _select_idx
    If _select_idx >= 0 Then
      opt_multi.Checked = True
    End If

    'Single
    _select_idx = -1
    For _idx As Int32 = 0 To _single_denominations.Count - 1
      cmb_single_denomination.Add(_single_denominations(_idx))
      If _single_denominations(_idx) = Denomination Then
        _select_idx = _idx
      End If
    Next

    cmb_single_denomination.SelectedIndex = _select_idx
    If _select_idx >= 0 Then
      opt_single.Checked = True
    End If

  End Sub

  ' PURPOSE: Terminal denomination selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub opt_denominations_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_single.CheckedChanged, opt_multi.CheckedChanged
    cmb_single_denomination.Enabled = opt_single.Checked
    cmb_multi_denomination.Enabled = opt_multi.Checked
  End Sub

  Private Sub btn_reset_serial_number_ClickEvent() Handles btn_reset_serial_number.ClickEvent
    ef_reported_serial_number.Value = ef_configured_serial_number.Value
    Call CheckSerialNumberDifference()
  End Sub

  Private Sub btn_reset_asset_number_ClickEvent() Handles btn_reset_asset_number.ClickEvent
    ef_reported_asset_number.Value = ef_configured_asset_number.Value
    Call CheckAssetNumberDifference()
  End Sub

  ' FAV 29-MAY-2015
  ' PURPOSE: Init game types combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SmibConfigurationComboFill()
    Dim _pcd_configuration As CLASS_PCD_CONFIGURATION
    Dim _table As DataTable

    _pcd_configuration = New CLASS_PCD_CONFIGURATION()
    _table = _pcd_configuration.GetPCDConfigurationsByCommunicationType(SMIB_COMMUNICATION_TYPE.PULSES)

    Me.cmb_smib_configuration.Clear()
    Me.cmb_smib_configuration.Add(_table)
    Me.cmb_smib_configuration.SelectedIndex = -1

    If (Not _table Is Nothing And _table.Rows.Count = 0) Then
      Me.cmb_smib_configuration.Enabled = False
    End If

  End Sub     ' SmibConfigurationComboFill

  ' FAV 29-MAY-2015
  ' PURPOSE: CommunicationType List by TerminalType
  '
  '  PARAMS:
  '     - INPUT: Terminal type
  '
  '     - OUTPUT:
  '
  ' RETURNS: Communication type List
  Private Function CommunicationTypeListByTerminalType(ByVal TerminalType As Integer) As List(Of SMIB_COMMUNICATION_TYPE)
    Dim _communicationList As New List(Of SMIB_COMMUNICATION_TYPE)
    Select Case TerminalType
      Case WSI.Common.TerminalTypes.WIN
        _communicationList.Add(SMIB_COMMUNICATION_TYPE.WCP)
      Case WSI.Common.TerminalTypes.SAS_HOST
        _communicationList.Add(SMIB_COMMUNICATION_TYPE.SAS)
        _communicationList.Add(SMIB_COMMUNICATION_TYPE.PULSES)
        _communicationList.Add(SMIB_COMMUNICATION_TYPE.SAS_IGT_POKER)

      Case Else
        _communicationList.Add(SMIB_COMMUNICATION_TYPE.NONE)
    End Select
    Return _communicationList
  End Function

  ' PURPOSE: Init Communication types combo box
  '
  '  PARAMS:
  '     - INPUT: Terminal type
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  'FAV 29-MAY-2015
  Private Sub CommunicationTypeComboFill(ByVal TerminalType As Integer, ByVal CommunicationType As Integer)
    Dim _communicationList As List(Of SMIB_COMMUNICATION_TYPE)
    _communicationList = CommunicationTypeListByTerminalType(TerminalType)

    For Each _item As SMIB_COMMUNICATION_TYPE In _communicationList

      Select Case _item

        Case SMIB_COMMUNICATION_TYPE.NONE
          cmb_communication_type.Add(_item, "---")

        Case SMIB_COMMUNICATION_TYPE.WCP
          cmb_communication_type.Add(_item, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5894))

        Case SMIB_COMMUNICATION_TYPE.SAS
          cmb_communication_type.Add(_item, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096))

        Case SMIB_COMMUNICATION_TYPE.PULSES
          cmb_communication_type.Add(_item, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6378))

        Case SMIB_COMMUNICATION_TYPE.SAS_IGT_POKER
          cmb_communication_type.Add(_item, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7027))

      End Select

    Next

    ' Configure interface according to the Terminal type
    If (CommunicationType = -1) Then
      cmb_communication_type.Value = WSI.Common.Terminal.GetDefaultCommunicationTypeByTerminalType(TerminalType)
    Else
      cmb_communication_type.Value = CommunicationType
    End If

    gb_communication_type.Enabled = (TerminalType = WSI.Common.TerminalTypes.SAS_HOST)
  End Sub

  ' PURPOSE: Allow trancate CheckedChange event
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  'LTC 02-SEP-2016
  Private Sub chk_allow_truncate_CheckedChanged(sender As Object, e As EventArgs) Handles chk_allow_truncate.CheckedChanged
    ef_min_denomination.Enabled = Not chk_allow_truncate.Checked
  End Sub


  ' PURPOSE: CheckedChange event
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  ' PDM 22-NOV-2016
  Private Sub chk_equity_percentage_CheckedChanged(sender As Object, e As EventArgs) Handles chk_equity_percentage.CheckedChanged
    ef_equity_percentage.Enabled = chk_equity_percentage.Checked
  End Sub

#End Region

#Region "Constructor"
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub
#End Region

End Class