﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_representatives_sel.vb
' DESCRIPTION:   Displays list of representatives of junkets
' AUTHOR:        Alberto Marcos
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  AMF     Initial version
' -------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc

Public Class frm_junkets_sel
  Inherits frm_base_sel

#Region " Members "
  Private m_junket As CLASS_JUNKETS

  Private m_type As String
  Private m_init_date_from As String
  Private m_init_date_to As String
  Private m_filter_code_junket As String
  Private m_filter_name_junket As String
  Private m_filter_code_representative As String
  Private m_filter_name_representative As String

#End Region

#Region "Constants"

  ' SQL Columns
  Private Const SQL_COLUMN_CODE As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_CODE_REPRESENTATIVE As Integer = 2
  Private Const SQL_COLUMN_NAME_REPRESENTATIVE As Integer = 3
  Private Const SQL_COLUMN_DATE_FROM As Integer = 4
  Private Const SQL_COLUMN_DATE_TO As Integer = 5
  Private Const SQL_COLUMN_ID As Integer = 6

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE_FROM As Integer = 1
  Private Const GRID_COLUMN_DATE_TO As Integer = 2
  Private Const GRID_COLUMN_CODE As Integer = 3
  Private Const GRID_COLUMN_NAME As Integer = 4
  Private Const GRID_COLUMN_CODE_REPRESENTATIVE As Integer = 5
  Private Const GRID_COLUMN_NAME_REPRESENTATIVE As Integer = 6
  Private Const GRID_COLUMN_ID As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 250
  Private Const GRID_WIDTH_CODE As Integer = 1500
  Private Const GRID_WIDTH_NAME As Integer = 3500
  Private Const GRID_WIDTH_DATE As Integer = 1750
  Private Const GRID_WIDTH_ID As Integer = 0

#End Region

#Region "Public"

  ''' <summary>
  ''' Sets the Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKETS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show the Form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Overrides"

  ''' <summary>
  ''' Initialise all the form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7997)

    ' Init Date
    Me.gb_init_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
    Me.rb_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2424)
    Me.rb_date_ini.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3355)
    Me.dtp_init_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_init_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_init_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_init_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' Junket
    Me.gb_junket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
    Me.ef_code_junket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
    Me.ef_code_junket.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_name_junket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)
    Me.ef_name_junket.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Representative
    Me.gb_junket_representative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
    Me.ef_code_representative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
    Me.ef_code_representative.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_name_representative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)
    Me.ef_name_representative.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.m_junket = New CLASS_JUNKETS

    Call SetDefaultValues()

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Sets initial focus
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.rb_active
  End Sub ' GUI_SetInitialFocus

  ''' <summary>
  ''' Set te Form Filters to their Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'GUI_FilterReset

  ''' <summary>
  ''' Perform non-default actions for Button Clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_NEW

        Call GUI_ShowNewJunket()

      Case ENUM_BUTTON.BUTTON_SELECT

        Call GUI_EditSelectedItem()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Finds the first selected grid item and calls the edit form
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()
    Dim _idx_row As Int32
    Dim _frm As frm_junkets_edit

    ' Create instance
    _frm = New frm_junkets_edit()

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' If no row is selected
    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm.ShowEditItem(Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value)

    Windows.Forms.Cursor.Current = Cursors.Default

    Call Me.Grid.Focus()
  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Calls the Junkets class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _init_from As String
    Dim _init_to As String
    Dim _type As CLASS_JUNKETS.ReportType

    _init_from = String.Empty
    _init_to = String.Empty
    _type = IIf(Me.rb_active.Checked, CLASS_JUNKETS.ReportType.Active, CLASS_JUNKETS.ReportType.StartDate)

    _init_from = GUI_FormatDateDB(dtp_init_from.Value)

    If Me.dtp_init_to.Checked = True Then
      _init_to = GUI_FormatDateDB(dtp_init_to.Value)
    End If

    Return m_junket.GetJunkets(ef_code_junket.Value.Trim(), ef_name_junket.Value.Trim(),
                               ef_code_representative.Value.Trim(), ef_name_representative.Value.Trim(),
                               _type, _init_from, _init_to)

  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Validates the Filter selection where needed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_init_from.Checked And Me.dtp_init_to.Checked And Me.dtp_init_from.Enabled And Me.dtp_init_to.Enabled Then
      If Me.dtp_init_from.Value > Me.dtp_init_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_init_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If Not DbRow.IsNull(SQL_COLUMN_CODE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CODE).Value = DbRow.Value(SQL_COLUMN_CODE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CODE_REPRESENTATIVE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CODE_REPRESENTATIVE).Value = DbRow.Value(SQL_COLUMN_CODE_REPRESENTATIVE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_NAME_REPRESENTATIVE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME_REPRESENTATIVE).Value = DbRow.Value(SQL_COLUMN_NAME_REPRESENTATIVE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DATE_FROM) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_FROM), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DATE_TO) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_TO), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Update filters for reports (Excel and print)
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_type = String.Empty
    m_init_date_from = String.Empty
    m_init_date_to = String.Empty
    m_filter_code_junket = String.Empty
    m_filter_name_junket = String.Empty
    m_filter_code_representative = String.Empty
    m_filter_name_representative = String.Empty

    ' Type
    m_type = IIf(Me.rb_active.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2424), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3355))

    ' Init Date From
    m_init_date_from = GUI_FormatDate(Me.dtp_init_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Init Date To
    If Me.dtp_init_to.Checked Then
      m_init_date_to = GUI_FormatDate(Me.dtp_init_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Junket
    m_filter_code_junket = ef_code_junket.Value
    m_filter_name_junket = ef_name_junket.Value

    ' Representative
    m_filter_code_representative = ef_code_representative.Value
    m_filter_name_representative = ef_name_representative.Value

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(m_type & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
    PrintData.SetFilter(m_type & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014), m_filter_code_junket)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014), m_filter_name_junket)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015), m_filter_code_representative)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015), m_filter_name_representative)

  End Sub ' GUI_ReportFilter

#End Region

#Region "Private"

  ''' <summary>
  ''' Displayes the Junket Edit Form for a New Junket
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_ShowNewJunket()

    Dim _frm As frm_junkets_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_junkets_edit()

    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub ' GUI_ShowNewJunket

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim closing_time As Integer
    Dim _str_closing_time As String

    _str_closing_time = New DateTime(TimeSpan.FromHours(GetDefaultClosingTime()).Ticks).ToString("HH:mm")

    Me.rb_active.Checked = True
    closing_time = GetDefaultClosingTime()
    Me.dtp_init_from.Value = New DateTime(Now.Year, Now.Month, 1, closing_time, 0, 0)
    Me.dtp_init_from.Checked = False
    Me.dtp_init_to.Value = Me.dtp_init_from.Value.AddMonths(1)
    Me.dtp_init_to.Checked = False

    Me.ef_code_junket.Value = String.Empty
    Me.ef_name_junket.Value = String.Empty
    Me.ef_code_representative.Value = String.Empty
    Me.ef_name_representative.Value = String.Empty

    lbl_from_hour.Text = _str_closing_time
    lbl_to_hour.Text = _str_closing_time

  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Style Sheet for the look of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Enable sortable in Main Grid Columns
      .Sortable = True

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' From
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(269)
      .Column(GRID_COLUMN_DATE_FROM).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' To
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(270)
      .Column(GRID_COLUMN_DATE_TO).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Code
      .Column(GRID_COLUMN_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
      .Column(GRID_COLUMN_CODE).Width = GRID_WIDTH_CODE
      .Column(GRID_COLUMN_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Name
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Code
      .Column(GRID_COLUMN_CODE_REPRESENTATIVE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_CODE_REPRESENTATIVE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
      .Column(GRID_COLUMN_CODE_REPRESENTATIVE).Width = GRID_WIDTH_CODE
      .Column(GRID_COLUMN_CODE_REPRESENTATIVE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Name
      .Column(GRID_COLUMN_NAME_REPRESENTATIVE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_NAME_REPRESENTATIVE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)
      .Column(GRID_COLUMN_NAME_REPRESENTATIVE).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME_REPRESENTATIVE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ID
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID

    End With
  End Sub ' GUI_StyleSheet

#End Region

End Class
