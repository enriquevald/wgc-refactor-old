'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_player_edit
' DESCRIPTION:   Windows form for player data edition
' AUTHOR:        Luis Ernesto Mesa
' CREATION DATE: 28-FEB-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-FEB-2013  LEM    Initial version
' 15-MAY-2013  JMM    Added account basic info visible from any tab
' 16-MAY-2013  LEM    Fixed Bug #785: Fields Age and Birthdate shows wrong format with anonymous account on the top of screen
' 05-JUN-2013  JMM    Fixed Bug #820: Birthdate shows wrong format on ELP imported accounts
' 06-NOV-2013  ACM    Logic of "Print" Button
' 07-APR-2014  JBP    Added VIP Client property.
' 17-MAR-2017  DPC    Product Backlog Item 25787:CreditLine - Create a CreditLine
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports WSI.Common
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common.Entrances.Entities

Public Class frm_player_edit
  Inherits frm_base_edit

#Region " Members "

#End Region ' Members


#Region "Constants"

  Private Const FORM_DB_MIN_VERSION As Short = 218

#End Region

#Region " Overrides "

  ' PURPOSE: Check for screen data.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return uc_player_data.IsControlDataOk()

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _player As CLASS_PLAYER_DATA

    _player = DbEditedObject
    _player = uc_player_data.GetData()

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _player As CLASS_PLAYER_DATA

    _player = DbEditedObject

    Call SetHeaderInformation(_player)

    If _player.CardData.Blocked Then
      Me.lbl_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(238) ' "Locked"
    Else
      Me.lbl_blocked.Text = ""
    End If

    Call uc_player_data.SetData(_player)

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_InitControls()
    Dim _player As CLASS_PLAYER_DATA
    _player = DbEditedObject
#If DEBUG Then
    Call WSI.Common.GeneralParam.ReloadGP()
#End If

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1711)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(230) 'Print

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = GeneralParam.GetBoolean("CreditLine", "Enabled", False)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947) 'Credit Line
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Height += 20

    Me.ef_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1910) '1910 "Account"
    Me.ef_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1565) '1565 "Document"
    Me.ef_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723) '1723 "Gender"
    Me.ef_age.Text = GLB_NLS_GUI_INVOICING.GetString(415) '415 "Age"
    Me.ef_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725) '1725 "Date of Birth"

    ' It looks like redundant code, but it's necessary.
    Me.ef_account.IsReadOnly = True
    Me.ef_document.IsReadOnly = True
    Me.ef_gender.IsReadOnly = True
    Me.ef_age.IsReadOnly = True
    Me.ef_birth_date.IsReadOnly = True

    Me.uc_player_data.Init()

  End Sub ' GUI_InitControls

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _player As CLASS_PLAYER_DATA
    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PLAYER_DATA
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

        ' Status = 0 is the same as 7 (UNSPECIFIED)
        _player = DbEditedObject
        If _player.CardData.PlayerTracking.HolderMaritalStatus = 0 Then
          _player.CardData.PlayerTracking.HolderMaritalStatus = 7
        End If
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAYER_EDIT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Ignore [ENTER] key when editing comments
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) And Me.uc_player_data.txt_comments.ContainsFocus Then
      Return True
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  ' PURPOSE: Print button
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS: none
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0

        Dim _close As Boolean
        Dim _player As CLASS_PLAYER_DATA

        _player = DbEditedObject

        ' Look for changes or anonymous account
        Call GUI_GetScreenData()
        If String.IsNullOrEmpty(_player.CardData.PlayerTracking.HolderName) Or Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

          ' Ask to save changes
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(834), _
                         ENUM_MB_TYPE.MB_TYPE_INFO, _
                         ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

            Return
          End If

          If Not GUI_IsScreenDataOk() Then

            Return
          End If

          ' Save changes
          _close = CloseOnOkClick
          CloseOnOkClick = False
          Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
          CloseOnOkClick = _close

          ' Set header info
          Call SetHeaderInformation(_player)

        End If

        'Print
        Dim _frm As frm_print_account_info
        Dim _print As Boolean
        Dim _include_scanned_documents As Boolean

        _frm = New frm_print_account_info(ef_account.TextValue)
        Call _frm.Show(_print, _include_scanned_documents)

        If Not _print Then
          Return
        End If

        Call PrintPlayerData(_include_scanned_documents, _player.CardData)

      Case frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1 ' Credit Line

        Dim _frm As frm_creditline_edit
        Dim _player As CLASS_PLAYER_DATA
        Dim _account As New cls_account

        _player = DbEditedObject

        If _player.CardData.PlayerTracking.CardLevel = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7946), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

          Return
        End If

        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        _frm = New frm_creditline_edit()

        _account.holder_name = _player.CardData.PlayerTracking.HolderName
        _account.id_account = _player.CardData.AccountId

        Call _frm.ShowEditItem(_account)

        Windows.Forms.Cursor.Current = Cursors.Default

      Case Else

        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

#End Region ' Overrides

#Region "Private Functions"

  Private Sub SetHeaderInformation(ByVal Player As CLASS_PLAYER_DATA)

    If IsNothing(Player) Then

      Return
    End If

    ' Account - Name
    If Not String.IsNullOrEmpty(Player.CardData.PlayerTracking.HolderName) Then
      Me.ef_account.Value = Player.CardData.AccountId & " - " & Player.CardData.PlayerTracking.HolderName
    Else
      Me.ef_account.Value = Player.CardData.AccountId & " - " & WSI.Common.Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS")
    End If
    ' DocumentType: Document
    Me.ef_document.Text = CardData.GetLabelDocument(Player.CardData.PlayerTracking.HolderIdType).Replace(":", "")
    Me.ef_document.Value = Player.CardData.PlayerTracking.HolderId
    ' Gender
    Me.ef_gender.Value = WSI.Common.CardData.GenderString(Player.CardData.PlayerTracking.HolderGender)
    ' BirthDate
    If Player.CardData.PlayerTracking.HolderBirthDate <> Nothing Then
      Me.ef_age.Value = WSI.Common.Misc.CalculateAge(Player.CardData.PlayerTracking.HolderBirthDate)
      Me.ef_birth_date.Value = GUI_FormatDate(Player.CardData.PlayerTracking.HolderBirthDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

  End Sub

#End Region

End Class