'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statement_print
' DESCRIPTION:   print document statement
' AUTHOR:        Samuel González
' CREATION DATE: 27-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-APR-2015  SGB    Initial version. Backlog Item 1131
'--------------------------------------------------------------------

#Region " Imports "

Imports GUI_Controls
Imports WSI.Common
Imports WSI.Common.WinLossStatement
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Text
Imports System.Threading
Imports System.Drawing.Printing

#End Region ' Imports

Public Class frm_statement_print
  Inherits frm_base_edit

#Region " Enums "

  Enum PRINT_MODE
    PENDING_PRINT = 0
    LAST_PRINT = 1
  End Enum ' PRINT_MODE

#End Region ' Enums

#Region " Constants "

  ' GRID PENDING PRINT
  Private Const GRID_COLUMNS_PRINT As Integer = 5
  Private Const GRID_HEADER_ROWS_PRINT As Integer = 1

  ' DB Columns
  Private Const SQL_COLUMN_PENDING_YEAR As Integer = 0
  Private Const SQL_COLUMN_PENDING_PROCESS As Integer = 1
  Private Const SQL_COLUMN_PENDING_PRINT As Integer = 2

  ' Grid columns
  Private Const GRID_COLUMN_PENDING_INDEX As Integer = 0
  Private Const GRID_COLUMN_PENDING_CHECK As Integer = 1
  Private Const GRID_COLUMN_PENDING_YEAR As Integer = 2
  Private Const GRID_COLUMN_PENDING_PROCESS As Integer = 3
  Private Const GRID_COLUMN_PENDING_PRINT As Integer = 4

  ' Width
  Private Const GRID_WIDTH_PENDING_INDEX As Integer = 200
  Private Const GRID_WIDTH_PENDING_CHECK As Integer = 600
  Private Const GRID_WIDTH_PENDING_YEAR As Integer = 700
  Private Const GRID_WIDTH_PENDING_PROCESS As Integer = 2100
  Private Const GRID_WIDTH_PENDING_PRINT As Integer = 2100


  ' GRID LAST PRINT
  Private Const GRID_COLUMNS_LAST As Integer = 7
  Private Const GRID_HEADER_ROWS_LAST As Integer = 1

  ' DB Columns
  Private Const SQL_COLUMN_LAST_PRINT_DATE As Integer = 0
  Private Const SQL_COLUMN_LAST_ACCOUNT_ID As Integer = 1
  Private Const SQL_COLUMN_LAST_HOLDER_NAME As Integer = 2
  Private Const SQL_COLUMN_LAST_YEAR_STATEMENT As Integer = 3
  Private Const SQL_COLUMN_LAST_DATE_FROM As Integer = 4
  Private Const SQL_COLUMN_LAST_DATE_TO As Integer = 5

  ' Grid columns

  Private Const GRID_COLUMN_LAST_INDEX As Integer = 0
  Private Const GRID_COLUMN_LAST_PRINT_DATE As Integer = 1
  Private Const GRID_COLUMN_LAST_ACCOUNT_ID As Integer = 2
  Private Const GRID_COLUMN_LAST_HOLDER_NAME As Integer = 3
  Private Const GRID_COLUMN_LAST_YEAR_STATEMENT As Integer = 4
  Private Const GRID_COLUMN_LAST_DATE_FROM As Integer = 5
  Private Const GRID_COLUMN_LAST_DATE_TO As Integer = 6

  ' Width
  Private Const GRID_WIDTH_LAST_INDEX As Integer = 200
  Private Const GRID_WIDTH_LAST_PRINT_DATE As Integer = 2000
  Private Const GRID_WIDTH_LAST_ACCOUNT_ID As Integer = 1400
  Private Const GRID_WIDTH_LAST_HOLDER_NAME As Integer = 3100
  Private Const GRID_WIDTH_LAST_YEAR_STATEMENT As Integer = 700
  Private Const GRID_WIDTH_LAST_DATE_FROM As Integer = 0
  Private Const GRID_WIDTH_LAST_DATE_TO As Integer = 0

#End Region ' Constants

#Region " Members "

  Private m_print_parameters As WSI.Common.PrintParameters
  Private m_print_mode As PRINT_MODE
  Private m_thread As Thread
  Private m_years As New List(Of String)
  Private m_win_loss_statements As New List(Of WIN_LOSS_STATEMENT)
  Private m_wls_insert_in_grid As WIN_LOSS_STATEMENT
  Private m_wls_inserted_in_grid As Boolean
  Private m_pending_print_has_selected As Boolean
#End Region ' Members

#Region " Properties "

#End Region ' Properties

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()
    'RAB 11-JAN-2015: Change ENUM_FORM
    Me.FormId = ENUM_FORM.FORM_STATEMENT_PRINT
    ' Call Base Form procedure
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.btn_last_print

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6255)

    ' Enable / Disable buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False ' Do not show at moment
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6268)

    ' Print pending
    Me.gb_pending_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6256)
    Me.btn_last_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6277)
    Call GUI_StyleSheet_Print_Pending()

    ' Print progress
    m_print_parameters = New PrintParameters()

    tmr_status_printing.Enabled = True
    Me.lbl_print_status.Text = ""
    Me.btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7)   ' 7   "Pausa"
    Me.btn_print_cancel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)     ' 6   "Cancelar"
    Me.gb_print_progress.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(649)  ' 649 "Progreso de impresión"
    gb_print_progress.Enabled = False

    ' Print last
    Me.gb_last_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6263)
    Call GUI_StyleSheet_Print_Last()
    m_wls_inserted_in_grid = True

    ' Grids info
    Call RefreshGrids()

  End Sub 'GUI_InitControls

  ' PURPOSE: Process clicks on select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_PRINT
        m_print_mode = PRINT_MODE.PENDING_PRINT

        If GUI_IsScreenDataOk() Then
          m_win_loss_statements = New List(Of WIN_LOSS_STATEMENT)
          Call PrintDocuments()
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Check if screen data is ok before saving
  '
  '  PARAMS :
  '     -  INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  '   NOTES :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _fiscal_year_start As String

    Select Case m_print_mode

      Case PRINT_MODE.PENDING_PRINT

        _fiscal_year_start = GeneralParam.GetString("WinLossStatement", "FiscalYearStart.MMDDHH", "010100")
        m_years.Clear()
        m_print_parameters.m_total = 0

        'fill array _years to years selected in dg_print_pending
        For _row As Integer = 0 To Me.dg_print_pending.NumRows() - 1
          If Me.dg_print_pending.Cell(_row, GRID_COLUMN_PENDING_CHECK).Value = uc_grid.GRID_CHK_CHECKED AndAlso _
             Me.dg_print_pending.Cell(_row, GRID_COLUMN_PENDING_PRINT).Value > 0 Then

            m_print_parameters.m_total += Me.dg_print_pending.Cell(_row, GRID_COLUMN_PENDING_PRINT).Value

            m_years.Add(GUI_FormatDate(New DateTime(Me.dg_print_pending.Cell(_row, GRID_COLUMN_PENDING_YEAR).Value, _
                                             Integer.Parse(_fiscal_year_start.Substring(0, 2)), _
                                             Integer.Parse(_fiscal_year_start.Substring(2, 2)), _
                                             Integer.Parse(_fiscal_year_start.Substring(4, 2)), 0, 0), _
                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))
          End If
        Next

        ' Don't select any year
        If m_years.Count < 1 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6372), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If

        Return True

      Case PRINT_MODE.LAST_PRINT
        If Me.dg_print_last.SelectedRows Is Nothing Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6373), ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        Else
          m_print_parameters.m_total = Me.dg_print_last.SelectedRows.Length
          m_print_parameters.m_current = 0
        End If

      Case Else
        Return False

    End Select

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Cancel print.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If m_print_parameters Is Nothing Then
      Return
    End If

    If Not m_print_parameters.m_printing Then
      Return
    End If

    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(155), _
                  mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then
      CloseCanceled = True
    Else
      m_print_parameters.m_cancel = True
    End If

  End Sub ' GUI_Closing

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns of Pending prints
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet_Print_Pending()

    With Me.dg_print_pending

      Call .Init(GRID_COLUMNS_PRINT, GRID_HEADER_ROWS_PRINT, True)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_PENDING_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_PENDING_INDEX).Width = GRID_WIDTH_PENDING_INDEX
      .Column(GRID_COLUMN_PENDING_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_PENDING_INDEX).IsColumnPrintable = False

      ' Check
      .Column(GRID_COLUMN_PENDING_CHECK).Header(0).Text = ""
      .Column(GRID_COLUMN_PENDING_CHECK).WidthFixed = GRID_WIDTH_PENDING_CHECK
      .Column(GRID_COLUMN_PENDING_CHECK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PENDING_CHECK).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_PENDING_CHECK).Fixed = True
      .Column(GRID_COLUMN_PENDING_CHECK).Editable = True
      .Column(GRID_COLUMN_PENDING_CHECK).HighLightWhenSelected = False

      ' Year
      .Column(GRID_COLUMN_PENDING_YEAR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6257)
      .Column(GRID_COLUMN_PENDING_YEAR).Width = GRID_WIDTH_PENDING_YEAR
      .Column(GRID_COLUMN_PENDING_YEAR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PENDING_YEAR).HighLightWhenSelected = False

      ' Process
      .Column(GRID_COLUMN_PENDING_PROCESS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6258)
      .Column(GRID_COLUMN_PENDING_PROCESS).Width = GRID_WIDTH_PENDING_PROCESS
      .Column(GRID_COLUMN_PENDING_PROCESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PENDING_PROCESS).HighLightWhenSelected = False

      ' Print
      .Column(GRID_COLUMN_PENDING_PRINT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6259)
      .Column(GRID_COLUMN_PENDING_PRINT).Width = GRID_WIDTH_PENDING_PRINT
      .Column(GRID_COLUMN_PENDING_PRINT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PENDING_PRINT).HighLightWhenSelected = False

    End With

  End Sub ' GUI_StyleSheet_Print_Pending

  ' PURPOSE: Define all Main Grid Columns of Last Print
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet_Print_Last()

    With Me.dg_print_last

      Call .Init(GRID_COLUMNS_LAST, GRID_HEADER_ROWS_LAST)
      '.Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE

      ' Index
      .Column(GRID_COLUMN_LAST_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_LAST_INDEX).Width = GRID_WIDTH_LAST_INDEX
      .Column(GRID_COLUMN_LAST_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_LAST_INDEX).IsColumnPrintable = False

      ' Print date
      .Column(GRID_COLUMN_LAST_PRINT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6264)
      .Column(GRID_COLUMN_LAST_PRINT_DATE).Width = GRID_WIDTH_LAST_PRINT_DATE
      .Column(GRID_COLUMN_LAST_PRINT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account Id
      .Column(GRID_COLUMN_LAST_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6265)
      .Column(GRID_COLUMN_LAST_ACCOUNT_ID).Width = GRID_WIDTH_LAST_ACCOUNT_ID
      .Column(GRID_COLUMN_LAST_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder name
      .Column(GRID_COLUMN_LAST_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6266)
      .Column(GRID_COLUMN_LAST_HOLDER_NAME).Width = GRID_WIDTH_LAST_HOLDER_NAME
      .Column(GRID_COLUMN_LAST_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Year statement
      .Column(GRID_COLUMN_LAST_YEAR_STATEMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6267)
      .Column(GRID_COLUMN_LAST_YEAR_STATEMENT).Width = GRID_WIDTH_LAST_YEAR_STATEMENT
      .Column(GRID_COLUMN_LAST_YEAR_STATEMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date from
      .Column(GRID_COLUMN_LAST_DATE_FROM).Header(0).Text = ""
      .Column(GRID_COLUMN_LAST_DATE_FROM).Width = GRID_WIDTH_LAST_DATE_FROM

      ' Date to
      .Column(GRID_COLUMN_LAST_DATE_TO).Header(0).Text = ""
      .Column(GRID_COLUMN_LAST_DATE_TO).Width = GRID_WIDTH_LAST_DATE_TO

    End With

  End Sub ' GUI_StyleSheet_Print_Last

  ' PURPOSE: Set datagrid last print
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenLastPrint()
    Dim _idx_row As Integer

    Try

      Me.dg_print_last.Clear()

      For Each _documents As DataRow In PrintLastStatement.Rows()

        _idx_row = Me.dg_print_last.AddRow()

        If Not _documents(SQL_COLUMN_LAST_PRINT_DATE) Is DBNull.Value Then
          dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_PRINT_DATE).Value = GUI_FormatDate(_documents(SQL_COLUMN_LAST_PRINT_DATE) _
                                                                                                    , ENUM_FORMAT_DATE.FORMAT_DATE_SHORT _
                                                                                                    , ENUM_FORMAT_TIME.FORMAT_HHMM)
        End If

        dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_ACCOUNT_ID).Value = _documents(SQL_COLUMN_LAST_ACCOUNT_ID)
        dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_HOLDER_NAME).Value = _documents(SQL_COLUMN_LAST_HOLDER_NAME)
        dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_YEAR_STATEMENT).Value = _documents(SQL_COLUMN_LAST_YEAR_STATEMENT)
        dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_DATE_FROM).Value = _documents(SQL_COLUMN_LAST_DATE_FROM)
        dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_DATE_TO).Value = _documents(SQL_COLUMN_LAST_DATE_TO)
      Next

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "SetScreenLastPrint", _ex.Message)
      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _ex.Message)

    Finally

      dg_print_last.Redraw = True
      If Me.dg_print_last.NumRows > 0 Then
        For x As Integer = 0 To dg_print_last.NumRows - 1
          Me.dg_print_last.IsSelected(x) = False
        Next x
        Me.dg_print_last.IsSelected(0) = True
      End If

    End Try

  End Sub ' SetScreenLastPrint

  ' PURPOSE: Set datagrid pending print
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenPendingPrint()
    Dim _idx_row As Integer
    Dim _need_addrow As Boolean

    Try

      'Me.dg_print_pending.Clear()
      _need_addrow = False
      _idx_row = 0

      For Each _documents As DataRow In PrintPendingStatement.Rows()

        If Me.dg_print_pending.NumRows <= 0 Then
          _need_addrow = True
        End If

        If _need_addrow Then
          Me.dg_print_pending.AddRow()
        End If

        dg_print_pending.Cell(_idx_row, GRID_COLUMN_PENDING_YEAR).Value = _documents(SQL_COLUMN_PENDING_YEAR)
        dg_print_pending.Cell(_idx_row, GRID_COLUMN_PENDING_PROCESS).Value = IIf(_documents(SQL_COLUMN_PENDING_PROCESS) Is DBNull.Value, 0, _documents(SQL_COLUMN_PENDING_PROCESS))
        dg_print_pending.Cell(_idx_row, GRID_COLUMN_PENDING_PRINT).Value = IIf(_documents(SQL_COLUMN_PENDING_PRINT) Is DBNull.Value, 0, _documents(SQL_COLUMN_PENDING_PRINT))

        _idx_row += 1

      Next

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "SetScreenPendingPrint", _ex.Message)
      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _ex.Message)

    Finally

      dg_print_pending.Redraw = True
      If Me.dg_print_pending.NumRows > 0 Then
        For x As Integer = 0 To dg_print_pending.NumRows - 1
          Me.dg_print_pending.IsSelected(x) = False
        Next x
        Me.dg_print_pending.IsSelected(0) = True
      End If

    End Try

  End Sub ' SetScreenPendingPrint

  ' PURPOSE: Take last prints in SQL query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  Private Function PrintLastStatement() As DataTable
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _table = New DataTable()
    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   WLS.WLS_PRINT_DATE                                   ")
    _sb.AppendLine("           , AC.AC_ACCOUNT_ID                                     ")
    _sb.AppendLine("           , AC.AC_HOLDER_NAME                                    ")
    _sb.AppendLine("	         , YEAR(WLS.WLS_DATE_FROM) AS YEAR_STATEMENTS           ")
    _sb.AppendLine("	         , WLS.WLS_DATE_FROM                                    ")
    _sb.AppendLine("	         , WLS.WLS_DATE_TO                                      ")
    _sb.AppendLine("	    FROM   WIN_LOSS_STATEMENTS WLS                              ")
    _sb.AppendLine("INNER JOIN   ACCOUNTS AC ON WLS.WLS_ACCOUNT_ID = AC.AC_ACCOUNT_ID ")
    _sb.AppendLine("     WHERE  (WLS.WLS_PRINT = 1)                                   ")
    _sb.AppendLine("	ORDER BY   WLS_PRINT_DATE DESC                                  ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Return _table

  End Function ' PrintLastStatement

  ' PURPOSE: Take pending print in SQL query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  Private Function PrintPendingStatement() As DataTable
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _year_Sql As Integer

    _table = New DataTable()
    _sb = New StringBuilder()
    _year_Sql = Today.Year - GeneralParam.GetInt32("WinLossStatement", "NumberOfPreviousYear")

    _sb.AppendLine("DECLARE @pYear AS INT                                                                                   ")
    _sb.AppendLine("SET @pYear = " & _year_Sql & ";                                                                         ")
    _sb.AppendLine("WITH YEARLIST AS                                                                                        ")
    _sb.AppendLine("(                                                                                                       ")
    _sb.AppendLine("    SELECT @pYear AS YEAR                                                                               ")
    _sb.AppendLine("    UNION ALL                                                                                           ")
    _sb.AppendLine("    SELECT YL.YEAR + 1 AS YEAR                                                                          ")
    _sb.AppendLine("    FROM YEARLIST YL                                                                                    ")
    _sb.AppendLine("    WHERE YL.YEAR + 1 < YEAR(GETDATE())                                                                 ")
    _sb.AppendLine(")                                                                                                       ")
    _sb.AppendLine("                                                                                                        ")
    _sb.AppendLine("SELECT    TYEARS.YEAR_WL                                                                                ")
    _sb.AppendLine("        , TVALUES.PROCESS_PENDING                                                                       ")
    _sb.AppendLine("        , TVALUES.PRINT_PENDING                                                                         ")
    _sb.AppendLine("  FROM                                                                                                  ")
    _sb.AppendLine("	      ( SELECT   YEAR AS YEAR_WL FROM   YEARLIST) AS TYEARS                                           ")
    _sb.AppendLine("LEFT JOIN                                                                                               ")
    _sb.AppendLine("          (                                                                                             ")
    _sb.AppendLine("            SELECT   YEAR(WLS_DATE_FROM) AS YEAR_WL                                                     ")
    _sb.AppendLine("			       , SUM(CASE WHEN WLS_STATUS IN (0,1) THEN 1 ELSE 0 END) AS PROCESS_PENDING                  ")
    _sb.AppendLine("                   , SUM(CASE WHEN WLS_STATUS = 2 AND WLS_PRINT = 0 THEN 1 ELSE 0 END) AS PRINT_PENDING ")
    _sb.AppendLine("              FROM   WIN_LOSS_STATEMENTS                                                                ")
    _sb.AppendLine("             WHERE   WLS_ACCOUNT_ID <> 0                                                                ")
    _sb.AppendLine("          GROUP BY   YEAR(WLS_DATE_FROM)                                                                ")
    _sb.AppendLine("          ) AS TVALUES                                                                                  ")
    _sb.AppendLine("      ON TYEARS.YEAR_WL = TVALUES.YEAR_WL                                                               ")
    _sb.AppendLine("ORDER BY TYEARS.YEAR_WL DESC                                                                            ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), 5000)

    Return _table

  End Function ' PrintPendingStatement

  ' PURPOSE: refresh grid info
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshGrids()

    Call SetScreenPendingPrint()
    'Call SetScreenLastPrint()
    Call EnableButtonPrint()

  End Sub ' RefreshGrids

  ' PURPOSE: Print documents and update in DB
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PrintDocuments()

    Try
      ' Get printer name
      Call EnableControls(True)
      Me.btn_last_print.Enabled = False

      Call InicializePrintParameters()

      m_print_parameters.m_printer_name = Printer.GetPrinterName(1)

      If Not String.IsNullOrEmpty(m_print_parameters.m_printer_name) Then
        If GetWinLossStatementForPrint(m_years, m_win_loss_statements) Then
          m_thread = New Thread(AddressOf PrintStart)
          m_thread.Start()
        End If
      Else
        Call EnableControls(False)
        Me.btn_last_print.Enabled = Me.dg_print_last.SelectedRows.Length > 0
      End If

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "PrintDocuments", _ex.Message)
    Finally

    End Try

  End Sub ' PrintDocuments

  ' PURPOSE: Use this function of thread
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PrintStart()

    Dim _win_loss_statements() As WIN_LOSS_STATEMENT

    m_print_parameters.m_current = 0
    _win_loss_statements = Nothing

    Try

      m_print_parameters.m_printing = True

      For Each _win_loss_statement As WIN_LOSS_STATEMENT In m_win_loss_statements
        ' Pause treatment
        While m_print_parameters.m_pause

          Thread.Sleep(10)
          Application.DoEvents()

          ' Cancel while in pause mode
          If m_print_parameters.m_cancel Then
            m_thread.Abort()
          End If

        End While

        ' Print
        _win_loss_statement.PrintDate = WGDB.Now()
        _win_loss_statement.PrintUserId = CurrentUser.Id
        _win_loss_statement.StatusPrint = WinLossStatementStatusPrint.Printed
        RequestPrintWinLossStatementPdfDocument(_win_loss_statement, m_print_parameters.m_printer_name)

        ' Insert in grid
        m_wls_inserted_in_grid = False
        m_wls_insert_in_grid = _win_loss_statement

        m_print_parameters.m_current += 1

      Next

      m_print_parameters.m_printing = False
      m_print_parameters.m_print_end = True

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "PrintStart", _ex.Message)
    End Try

  End Sub ' PrintStart

  Private Sub InsertPrintStatementInGrid(ByVal PrintStatement As WIN_LOSS_STATEMENT)
    Dim _idx_row As Int32

    _idx_row = Me.dg_print_last.AddRow()

    ' Last print date
    dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_PRINT_DATE).Value = _
        GUI_FormatDate(PrintStatement.PrintDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    ' Account Id
    dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_ACCOUNT_ID).Value = PrintStatement.AccountId
    ' Holder name
    dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_HOLDER_NAME).Value = PrintStatement.HolderName
    ' Year statement
    dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_YEAR_STATEMENT).Value = PrintStatement.RequestYear
    ' Date from
    dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_DATE_FROM).Value = PrintStatement.DateFrom
    ' Date to
    dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_DATE_TO).Value = PrintStatement.DateTo

  End Sub 'InsertPrintStatementInGrid

  ' PURPOSE: Selected documents in print last update to not print
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ReprintDocuments()

    Dim _statement_reprint As WIN_LOSS_STATEMENT
    Dim _idx_wls As Int32

    m_win_loss_statements = New List(Of WIN_LOSS_STATEMENT)
    m_print_parameters.m_printing = True
    _idx_wls = 0

    Try
      For Each _idx_row As Int32 In Me.dg_print_last.SelectedRows

        _statement_reprint = New WIN_LOSS_STATEMENT
        _statement_reprint.AccountId = dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_ACCOUNT_ID).Value
        _statement_reprint.DateFrom = dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_DATE_FROM).Value
        _statement_reprint.DateTo = dg_print_last.Cell(_idx_row, GRID_COLUMN_LAST_DATE_TO).Value
        _statement_reprint.StatusPrint = WinLossStatementStatusPrint.NotPrinted
        _statement_reprint.StatusPrintPrev = WinLossStatementStatusPrint.Printed
        Using _db_trx As New DB_TRX()
          If GetWinLossStatement(_statement_reprint, _db_trx.SqlTransaction) Then
            m_win_loss_statements.Add(_statement_reprint)
            UpdateWinLossStatementStatusPrint(m_win_loss_statements(_idx_wls))
            _idx_wls += 1
          End If
        End Using

      Next


      ' Get printer name
      Call EnableControls(True)

      Call InicializePrintParameters()
      Me.btn_last_print.Enabled = False

      m_print_parameters.m_printer_name = Printer.GetPrinterName(1)

      If Not String.IsNullOrEmpty(m_print_parameters.m_printer_name) Then
        m_thread = New Thread(AddressOf ReprintStart)
        m_thread.Start()
        Call DeselectRowsPrintLast()
      Else
        Call EnableControls(False)
        Me.btn_last_print.Enabled = True
      End If

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "ReprintDocuments", _ex.Message)

    End Try

  End Sub ' ReprintDocuments

  ' PURPOSE: Deselec rows in grid print last
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DeselectRowsPrintLast()

    For Each _idx_row As Int32 In Me.dg_print_last.SelectedRows
      dg_print_last.Row(_idx_row).IsSelected = False
      dg_print_last.RepaintRow(_idx_row)
    Next

  End Sub ' DeselectRowsPrintLast

  ' PURPOSE: Use this function of thread
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ReprintStart()

    Dim _win_loss_statements() As WIN_LOSS_STATEMENT

    m_print_parameters.m_current = 0
    _win_loss_statements = Nothing

    Try

      m_print_parameters.m_printing = True

      For Each _win_loss_statement As WIN_LOSS_STATEMENT In m_win_loss_statements
        ' Pause treatment
        While m_print_parameters.m_pause

          Thread.Sleep(10)
          Application.DoEvents()

          ' Cancel while in pause mode
          If m_print_parameters.m_cancel Then
            m_thread.Abort()
          End If

        End While

        ' Print
        _win_loss_statement.PrintDate = WGDB.Now()
        _win_loss_statement.PrintUserId = CurrentUser.Id
        _win_loss_statement.StatusPrint = WinLossStatementStatusPrint.Printed
        RequestPrintWinLossStatementPdfDocument(_win_loss_statement, m_print_parameters.m_printer_name)

        m_print_parameters.m_current += 1

      Next

      m_print_parameters.m_printing = False
      m_print_parameters.m_print_end = True

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "ReprintStart", _ex.Message)
    End Try

  End Sub ' PrintStart

  ' PURPOSE: Controls on or off during the printing.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Enabled: Booblean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub EnableControls(ByVal Enabled As Boolean)

    Me.btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7)   ' 7   "Pausa"

    Me.lbl_print_status.Text = ""

    Me.gb_print_progress.Enabled = Enabled

    m_print_parameters.m_printing = Enabled

    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = Not Enabled And m_pending_print_has_selected

  End Sub ' EnableControls

  ' PURPOSE: Enabled button Print if is not necessary.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub EnableButtonPrint()
    Dim _idx As Integer
    Dim _has_selected As Boolean

    'enabled button pending print
    _has_selected = False

    For _idx = 0 To Me.dg_print_pending.NumRows - 1
      If (Me.dg_print_pending.Cell(_idx, GRID_COLUMN_PENDING_CHECK).Value = uc_grid.GRID_CHK_CHECKED) Then
        _has_selected = True
        Exit For
      End If
    Next

    If dg_print_pending.NumRows = 0 OrElse Not _has_selected Then
      m_pending_print_has_selected = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    Else
      m_pending_print_has_selected = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = Not m_print_parameters.m_printing
    End If

  End Sub ' EnableButtonPrintReprint

  Private Sub InicializePrintParameters()

    m_print_parameters.m_printer_name = String.Empty
    m_print_parameters.m_pause = False
    m_print_parameters.m_cancel = False

  End Sub ' InicializePrintParameters

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions 

#Region " Events "


  Private Sub dg_print_pending_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_print_pending.CellDataChangedEvent

    Call EnableButtonPrint()

  End Sub 'dg_print_pending_CellDataChangedEvent

  Private Sub dg_print_last_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_print_last.RowSelectedEvent

    btn_last_print.Enabled = SelectedRow > -1 And Not m_print_parameters.m_printing

  End Sub 'dg_print_last_RowSelectedEvent

  Private Sub btn_last_print_ClickEvent() Handles btn_last_print.ClickEvent

    m_print_mode = PRINT_MODE.LAST_PRINT

    If GUI_IsScreenDataOk() Then
      'SE CAMBIA REPRINT POR OTRA FUNCIONALIDAD
      Call ReprintDocuments()
      Call RefreshGrids()
    End If

  End Sub ' btn_last_print_ClickEvent

  Private Sub btn_pause_continue_ClickEvent() Handles btn_pause_continue.ClickEvent

    m_print_parameters.m_pause = Not m_print_parameters.m_pause

    If m_print_parameters.m_pause Then
      btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8) ' 8 "Continuar"
    Else
      btn_pause_continue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7) ' 7 "Pause"
    End If

  End Sub ' btn_pause_continue_ClickEvent

  Private Sub btn_print_cancel_ClickEvent() Handles btn_print_cancel.ClickEvent

    If Not m_print_parameters.m_printing Then

      Return
    End If

    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(154), _
                  mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then


      m_print_parameters.m_pause = True
      m_print_parameters.m_cancel = True
      m_print_parameters.m_printing = False
      m_print_parameters.m_current = 0
      Call EnableControls(False)
    End If

    Call RefreshGrids()

  End Sub ' btn_print_cancel_ClickEvent

  Private Sub tmr_status_printing_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_status_printing.Tick

    ' refresh grid pending print
    'If m_print_parameters.m_print_end Then
    Call SetScreenPendingPrint()
    'm_print_parameters.m_print_end = False
    'End If

    ' No't printing and all print inserted in grid
    If Not m_print_parameters.m_printing AndAlso m_wls_inserted_in_grid Then
      lbl_print_status.Text = String.Empty
      Call EnableControls(False)

      Return
    End If

    ' Process one print, insert in grid
    If Not m_wls_inserted_in_grid Then
      InsertPrintStatementInGrid(m_wls_insert_in_grid)
      m_wls_inserted_in_grid = True
    End If

    ' Refresh Label Value
    If m_print_parameters.m_current > 0 Then
      If m_print_parameters.m_pause Then
        ' Printing paused.
        lbl_print_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6375, GUI_FormatNumber(m_print_parameters.m_total - m_print_parameters.m_current, 0))

        Return
      End If

      lbl_print_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6374, GUI_FormatNumber(m_print_parameters.m_total - m_print_parameters.m_current, 0))
    Else
      If m_print_parameters.m_pause Then
        ' Printing paused.
        lbl_print_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6375, GUI_FormatNumber(m_print_parameters.m_total, 0))

        Return
      End If

      lbl_print_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6374, GUI_FormatNumber(m_print_parameters.m_total, 0))
    End If

  End Sub ' tmr_status_printing_Tick

#End Region ' Events

End Class