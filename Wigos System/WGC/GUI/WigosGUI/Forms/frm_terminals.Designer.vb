<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_terminals))
    Me.uc_type_list = New GUI_Controls.uc_terminal_type()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.chk_status_retired = New System.Windows.Forms.CheckBox()
    Me.chk_status_out_of_order = New System.Windows.Forms.CheckBox()
    Me.chk_status_active = New System.Windows.Forms.CheckBox()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.fra_terminal = New System.Windows.Forms.GroupBox()
    Me.ef_terminal_id = New GUI_Controls.uc_entry_field()
    Me.fra_terminal_blocked = New System.Windows.Forms.GroupBox()
    Me.chk_terminal_no = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_yes = New System.Windows.Forms.CheckBox()
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field()
    Me.chk_serial_number = New System.Windows.Forms.CheckBox()
    Me.chk_asset_number = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.fra_terminal.SuspendLayout()
    Me.fra_terminal_blocked.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_asset_number)
    Me.panel_filter.Controls.Add(Me.chk_serial_number)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.fra_terminal)
    Me.panel_filter.Controls.Add(Me.uc_type_list)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 205)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_type_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_serial_number, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_asset_number, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 209)
    Me.panel_data.Size = New System.Drawing.Size(1226, 509)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 19)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'uc_type_list
    '
    Me.uc_type_list.Location = New System.Drawing.Point(744, 6)
    Me.uc_type_list.Name = "uc_type_list"
    Me.uc_type_list.Size = New System.Drawing.Size(317, 159)
    Me.uc_type_list.TabIndex = 3
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_status_retired)
    Me.gb_status.Controls.Add(Me.chk_status_out_of_order)
    Me.gb_status.Controls.Add(Me.chk_status_active)
    Me.gb_status.Location = New System.Drawing.Point(8, 96)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(393, 48)
    Me.gb_status.TabIndex = 1
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'chk_status_retired
    '
    Me.chk_status_retired.AutoSize = True
    Me.chk_status_retired.Location = New System.Drawing.Point(285, 20)
    Me.chk_status_retired.Name = "chk_status_retired"
    Me.chk_status_retired.Size = New System.Drawing.Size(74, 17)
    Me.chk_status_retired.TabIndex = 2
    Me.chk_status_retired.Text = "xRetired"
    '
    'chk_status_out_of_order
    '
    Me.chk_status_out_of_order.AutoSize = True
    Me.chk_status_out_of_order.Location = New System.Drawing.Point(139, 20)
    Me.chk_status_out_of_order.Name = "chk_status_out_of_order"
    Me.chk_status_out_of_order.Size = New System.Drawing.Size(117, 17)
    Me.chk_status_out_of_order.TabIndex = 1
    Me.chk_status_out_of_order.Text = "xOut Of Service"
    '
    'chk_status_active
    '
    Me.chk_status_active.AutoSize = True
    Me.chk_status_active.Location = New System.Drawing.Point(37, 20)
    Me.chk_status_active.Name = "chk_status_active"
    Me.chk_status_active.Size = New System.Drawing.Size(68, 17)
    Me.chk_status_active.TabIndex = 0
    Me.chk_status_active.Text = "xActive"
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(407, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 200)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'fra_terminal
    '
    Me.fra_terminal.Controls.Add(Me.ef_terminal_id)
    Me.fra_terminal.Controls.Add(Me.fra_terminal_blocked)
    Me.fra_terminal.Controls.Add(Me.ef_terminal_name)
    Me.fra_terminal.Location = New System.Drawing.Point(8, 7)
    Me.fra_terminal.Name = "fra_terminal"
    Me.fra_terminal.Size = New System.Drawing.Size(393, 83)
    Me.fra_terminal.TabIndex = 0
    Me.fra_terminal.TabStop = False
    Me.fra_terminal.Text = "xTerminal"
    '
    'ef_terminal_id
    '
    Me.ef_terminal_id.DoubleValue = 0.0R
    Me.ef_terminal_id.IntegerValue = 0
    Me.ef_terminal_id.IsReadOnly = False
    Me.ef_terminal_id.Location = New System.Drawing.Point(6, 44)
    Me.ef_terminal_id.Name = "ef_terminal_id"
    Me.ef_terminal_id.PlaceHolder = Nothing
    Me.ef_terminal_id.ShortcutsEnabled = True
    Me.ef_terminal_id.Size = New System.Drawing.Size(242, 24)
    Me.ef_terminal_id.SufixText = "Sufix Text"
    Me.ef_terminal_id.SufixTextVisible = True
    Me.ef_terminal_id.TabIndex = 1
    Me.ef_terminal_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_id.TextValue = ""
    Me.ef_terminal_id.Value = ""
    Me.ef_terminal_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'fra_terminal_blocked
    '
    Me.fra_terminal_blocked.Controls.Add(Me.chk_terminal_no)
    Me.fra_terminal_blocked.Controls.Add(Me.chk_terminal_yes)
    Me.fra_terminal_blocked.Location = New System.Drawing.Point(259, 12)
    Me.fra_terminal_blocked.Name = "fra_terminal_blocked"
    Me.fra_terminal_blocked.Size = New System.Drawing.Size(123, 64)
    Me.fra_terminal_blocked.TabIndex = 2
    Me.fra_terminal_blocked.TabStop = False
    Me.fra_terminal_blocked.Text = "xBlocked"
    '
    'chk_terminal_no
    '
    Me.chk_terminal_no.Location = New System.Drawing.Point(28, 42)
    Me.chk_terminal_no.Name = "chk_terminal_no"
    Me.chk_terminal_no.Size = New System.Drawing.Size(72, 16)
    Me.chk_terminal_no.TabIndex = 1
    Me.chk_terminal_no.Text = "No"
    '
    'chk_terminal_yes
    '
    Me.chk_terminal_yes.Location = New System.Drawing.Point(28, 22)
    Me.chk_terminal_yes.Name = "chk_terminal_yes"
    Me.chk_terminal_yes.Size = New System.Drawing.Size(72, 16)
    Me.chk_terminal_yes.TabIndex = 0
    Me.chk_terminal_yes.Text = "Yes"
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0.0R
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(16, 18)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.PlaceHolder = Nothing
    Me.ef_terminal_name.ShortcutsEnabled = True
    Me.ef_terminal_name.Size = New System.Drawing.Size(232, 24)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 0
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 70
    Me.ef_terminal_name.Value = ""
    Me.ef_terminal_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_serial_number
    '
    Me.chk_serial_number.AutoSize = True
    Me.chk_serial_number.Location = New System.Drawing.Point(45, 157)
    Me.chk_serial_number.Name = "chk_serial_number"
    Me.chk_serial_number.Size = New System.Drawing.Size(114, 17)
    Me.chk_serial_number.TabIndex = 11
    Me.chk_serial_number.Text = "xSerial number"
    Me.chk_serial_number.UseVisualStyleBackColor = True
    '
    'chk_asset_number
    '
    Me.chk_asset_number.AutoSize = True
    Me.chk_asset_number.Location = New System.Drawing.Point(45, 180)
    Me.chk_asset_number.Name = "chk_asset_number"
    Me.chk_asset_number.Size = New System.Drawing.Size(112, 17)
    Me.chk_asset_number.TabIndex = 12
    Me.chk_asset_number.Text = "xAsset number"
    Me.chk_asset_number.UseVisualStyleBackColor = True
    '
    'frm_terminals
    '
    Me.ClientSize = New System.Drawing.Size(1234, 722)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_terminals"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.fra_terminal.ResumeLayout(False)
    Me.fra_terminal_blocked.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_type_list As GUI_Controls.uc_terminal_type
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_status_retired As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_out_of_order As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_active As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents fra_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents ef_terminal_id As GUI_Controls.uc_entry_field
  Friend WithEvents fra_terminal_blocked As System.Windows.Forms.GroupBox
  Friend WithEvents chk_terminal_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_yes As System.Windows.Forms.CheckBox
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_serial_number As System.Windows.Forms.CheckBox
  Friend WithEvents chk_asset_number As System.Windows.Forms.CheckBox

End Class
