﻿'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_creditline_sel.vb
' DESCRIPTION:   Displays list of Clients with credit lines
' AUTHOR:        Mark Stansfield
' CREATION DATE: 14-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-MAR-2017  MS     Initial version
' 25-JUL-2017  DPC    [WIGOS-3929]: Error message when having Credit line with limit = 0
' 25-JUL-2017  DPC    WIGOS-3935: Credit line creation date - missing time
' 26-SEP-2017  ETP    Fixed Bug 29921:[WIGOS-4564] Multisite: wrong site number in grids
' -------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Currency

Public Class frm_creditline_sel
  Inherits frm_base_sel

#Region " Members "
  Private m_init_date_from As Date
  Private m_credit_line As CLASS_CREDITLINE
  Private m_dic_states As Dictionary(Of Integer, String)
  Private m_totals_data_table As DataTable

  Private m_filter_account As String
  Private m_filter_card As String
  Private m_filter_holder As String
  Private m_filter_vip As String
  Private m_filter_status As String
  Private m_filter_expiration_date As String
  Private m_filter_with_debt As String
  Private m_filter_withaout_debt As String
  Private m_filter_greather_than As String
  Private m_filter_site As String

  Private m_is_center As Boolean

#End Region ' Members

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 1
  Private Const SQL_COLUMN_CREDIT_LIMIT As Integer = 2
  Private Const SQL_COLUMN_TTO As Integer = 3
  Private Const SQL_COLUMN_LENT As Integer = 4
  Private Const SQL_COLUMN_STATUS As Integer = 5
  Private Const SQL_COLUMN_DATE_CREATED As Integer = 6
  Private Const SQL_COLUMN_DATE_UPDATED As Integer = 7
  Private Const SQL_COLUMN_DATE_EXPIRATION As Integer = 8
  Private Const SQL_COLUMN_DATE_LAST_PAYBACK As Integer = 9
  Private Const SQL_COLUMN_SITE_ID As Integer = 10

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE_ID As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 2
  Private Const GRID_COLUMN_ACCOUNT_NAME As Integer = 3
  Private Const GRID_COLUMN_CREDIT_LIMIT As Integer = 4
  Private Const GRID_COLUMN_TTO As Integer = 5
  Private Const GRID_COLUMN_LENT As Integer = 6
  Private Const GRID_COLUMN_LENT_PCT As Integer = 7
  Private Const GRID_COLUMN_STATUS As Integer = 8
  Private Const GRID_COLUMN_DATE_CREATED As Integer = 9
  Private Const GRID_COLUMN_DATE_UPDATED As Integer = 10
  Private Const GRID_COLUMN_DATE_EXPIRATION As Integer = 11
  Private Const GRID_COLUMN_DATE_LAST_PAYBACK As Integer = 12

  Private Const GRID_COLUMNS As Integer = 13
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_HOLDER As Integer = 3000
  Private Const GRID_WIDTH_MONEY As Integer = 1800
  Private Const GRID_WIDTH_STATUS As Integer = 2000
  Private Const GRID_WIDTH_IBAN As Integer = 3000
  Private Const GRID_WIDTH_APPROVERS As Integer = 2800
  Private Const GRID_WIDTH_DATES As Integer = 2000
  Private Const GRID_WIDTH_DATE_LAST As Integer = 2400
  Private Const GRID_WIDTH_DEFAULT As Integer = 1000

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0


#End Region ' Constants

#Region " Constructor "
  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

  End Sub
#End Region 'Constructor

#Region " Public Methods "

  ''' <summary>
  ''' Sets the Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CREDITLINE_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show the Form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    m_init_date_from = Nothing

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Methods

#Region " Overrides "

  ''' <summary>
  ''' Initialise all the form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7945)

    ' Date Range
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7950)

    Me.uc_dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.uc_dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.uc_dtp_from.Value = GUI_GetDate().AddHours(GetDefaultClosingTime())
    Me.uc_dtp_from.Checked = False

    ' Credit Line Status
    Me.uc_creditline_status.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7955)
    Me.uc_creditline_status.multiChoice = True
    Me.uc_creditline_status.ResizeGrid()

    Me.chkWithDebt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7981)
    Me.chkWithoutDebt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7982)
    Me.chkDebtGreaterThan.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7983)
    Me.gb_debt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7984)

    Call Me.ef_greater_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.m_credit_line = New CLASS_CREDITLINE

    Call FillStatusGrid()

    Me.uc_sites_sel.Visible = m_is_center
    If (m_is_center) Then
      InitializeMultiSiteControls()
    End If

    Call SetDefaultValues()

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    Call InitTotalsDataTable()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set te Form Filters to their Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ''' <summary>
  ''' Perform non-default actions for Button Clicks
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_NEW

        Call GUI_ShowNewCreditLine()

      Case ENUM_BUTTON.BUTTON_SELECT

        Call GUI_EditSelectedItem()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_totals_data_table.Rows.Clear()
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Finds the first selected grid item and calls the edit form
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()
    Dim _idx_row As Int32
    Dim _frm As frm_creditline_edit

    ' Create instance
    _frm = New frm_creditline_edit()

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' If no row is selected
    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Dim _account As New cls_account
    _account.holder_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_NAME).Value
    _account.id_account = Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value
    _frm.ShowEditItem(_account)

    Windows.Forms.Cursor.Current = Cursors.Default
    Call Me.Grid.Focus()
  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Calls the Creditline class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim str_sql As String

    str_sql = ""
    str_sql = m_credit_line.getCreditLines(uc_dtp_from, uc_creditline_status, uc_account_sel1, chkWithDebt.Checked, chkWithoutDebt.Checked, IIf(ef_greater_than.Value = String.Empty, 0, GUI_ParseCurrency(ef_greater_than.Value)), uc_sites_sel)

    Return str_sql
  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Validates the Filter selection where needed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Account selection
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    If m_is_center Then
      ' Multisite: Sites selection
      If Not Me.uc_sites_sel.FilterCheckSites() Then

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _lent_pct As Double

    _lent_pct = 0.0

    If m_is_center AndAlso Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE_ID))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREDIT_LIMIT) AndAlso Not DbRow.IsNull(SQL_COLUMN_LENT) And DbRow.Value(SQL_COLUMN_CREDIT_LIMIT) <> 0 Then
      _lent_pct = (DbRow.Value(SQL_COLUMN_LENT) / DbRow.Value(SQL_COLUMN_CREDIT_LIMIT)) * 100
      _lent_pct = Math.Round(_lent_pct, 2)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CREDIT_LIMIT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_LIMIT).Value = _
        GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CREDIT_LIMIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TTO) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TTO).Value = _
      GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TTO), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_LENT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LENT).Value = _
      GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_LENT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_LENT_PCT).Value = IIf(GUI_FormatNumber(_lent_pct, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) <> 0.0, _
                                                          String.Format("{0}%", GUI_FormatNumber(_lent_pct, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)), _
                                                          String.Empty)

    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = m_dic_states.Item(DbRow.Value(SQL_COLUMN_STATUS))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DATE_CREATED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_CREATED), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DATE_UPDATED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_UPDATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_UPDATED), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DATE_EXPIRATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_EXPIRATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_EXPIRATION), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DATE_LAST_PAYBACK) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_LAST_PAYBACK).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_LAST_PAYBACK), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    Call SetTotalsDataTable(DbRow)

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Report filters.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_account = String.Empty
    m_filter_card = String.Empty
    m_filter_holder = String.Empty
    m_filter_status = String.Empty
    m_filter_expiration_date = String.Empty
    m_filter_greather_than = String.Empty
    m_filter_status = String.Empty
    m_filter_with_debt = String.Empty
    m_filter_withaout_debt = String.Empty

    If Not String.IsNullOrEmpty(uc_account_sel1.Account) Then
      m_filter_account = uc_account_sel1.Account
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.TrackData) Then
      m_filter_card = uc_account_sel1.TrackData
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.Holder) Then
      m_filter_holder = uc_account_sel1.Holder
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.HolderIsVip) Then
      m_filter_vip = uc_account_sel1.HolderIsVip
    End If

    If uc_dtp_from.Checked Then
      m_filter_expiration_date = uc_dtp_from.Value
    End If

    m_filter_with_debt = IIf(chkWithDebt.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    m_filter_withaout_debt = IIf(chkWithDebt.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))

    If chkDebtGreaterThan.Checked Then
      m_filter_greather_than = ef_greater_than.Value
    End If

    m_filter_status = String.Join(", ", Me.uc_creditline_status.SelectedValuesArray)

    If m_is_center Then
      Me.uc_sites_sel.GetSitesIdListSelectedAll()
      m_filter_site = Me.uc_sites_sel.GetSitesIdListSelectedToPrint()
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_filter_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_filter_card)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_filter_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_filter_vip)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7955), m_filter_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7950), m_filter_expiration_date)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7981), m_filter_with_debt)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7982), m_filter_withaout_debt)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7983), m_filter_greather_than)
    If (m_is_center) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_filter_site)
    End If
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

  ''' <summary>
  ''' Set the total row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer
    Dim _row_totals As DataRow

    If Me.Grid.NumRows > 0 Then
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      _row_totals = m_totals_data_table.Rows(0)

      Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_LIMIT).Value = GUI_FormatCurrency(_row_totals("LIMIT"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TTO).Value = GUI_FormatCurrency(_row_totals("TTO"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_LENT).Value = GUI_FormatCurrency(_row_totals("LENT"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_LENT_PCT).Value = IIf(GUI_FormatNumber(_row_totals("LENT_PCT"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) <> 0.0, _
                                                          String.Format("{0}%", GUI_FormatNumber(_row_totals("LENT_PCT"), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)), _
                                                          "0%")

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If
  End Sub ' GUI_AfterLastRow

#End Region ' Overrides

#Region " Private Methods"

  ''' <summary>
  ''' Displayes the Creditline Edit Form for a New Creditline
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_ShowNewCreditLine()

    Dim _frm As frm_creditline_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_creditline_edit()

    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub ' GUI_ShowNewCreditLine

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    ' Dates From and To
    _now = WGDB.Now

    Me.uc_dtp_from.Checked = False

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    If m_init_date_from <> Nothing Then
      Me.uc_dtp_from.Value = m_init_date_from
      Me.uc_dtp_from.Checked = True
    Else
      Me.uc_dtp_from.Value = _initial_time
      Me.uc_dtp_from.Checked = False
    End If

    ' Clear Account, Card and Name
    Me.uc_account_sel1.Clear()

    ' Reset Status Grid
    Me.uc_creditline_status.Clear()

    chkWithDebt.Checked = False
    chkWithoutDebt.Checked = False
    chkDebtGreaterThan.Checked = False

    ef_greater_than.Value = 0

    Call FillStatusGrid()

  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Populate the Combo Grid for Statuses
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FillStatusGrid()
    Call Me.uc_creditline_status.Clear()
    Call Me.uc_creditline_status.ReDraw(False)

    m_dic_states = m_credit_line.getListStatus(CreditLines.StatusDefinition.All)

    Me.uc_creditline_status.Add(m_dic_states)

  End Sub ' FillStatusGrid

  ''' <summary>
  ''' Style Sheet for the look of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Site Id
      ' Site
      If m_is_center Then
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
        .Column(GRID_COLUMN_SITE_ID).Width = 850
        .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Width = 0
        .Column(GRID_COLUMN_SITE_ID).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SITE_ID).IsColumnPrintable = False
      End If

      ' Account Number
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account Holder
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = GRID_WIDTH_HOLDER
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Credit Limit
      .Column(GRID_COLUMN_CREDIT_LIMIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_CREDIT_LIMIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7948)
      .Column(GRID_COLUMN_CREDIT_LIMIT).Width = GRID_WIDTH_MONEY
      .Column(GRID_COLUMN_CREDIT_LIMIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TTO
      .Column(GRID_COLUMN_TTO).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_TTO).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7949)
      .Column(GRID_COLUMN_TTO).Width = GRID_WIDTH_MONEY
      .Column(GRID_COLUMN_TTO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Credit Spent
      .Column(GRID_COLUMN_LENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_LENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7961)
      .Column(GRID_COLUMN_LENT).Width = GRID_WIDTH_MONEY
      .Column(GRID_COLUMN_LENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Lent %
      .Column(GRID_COLUMN_LENT_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_LENT_PCT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7962)
      .Column(GRID_COLUMN_LENT_PCT).Width = GRID_WIDTH_MONEY
      .Column(GRID_COLUMN_LENT_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Credit Line Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7955)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date Created 
      .Column(GRID_COLUMN_DATE_CREATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_DATE_CREATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7985)
      .Column(GRID_COLUMN_DATE_CREATED).Width = GRID_WIDTH_DATES
      .Column(GRID_COLUMN_DATE_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' date Updated
      .Column(GRID_COLUMN_DATE_UPDATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_DATE_UPDATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7986)
      .Column(GRID_COLUMN_DATE_UPDATED).Width = GRID_WIDTH_DATE_LAST
      .Column(GRID_COLUMN_DATE_UPDATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date Expires
      .Column(GRID_COLUMN_DATE_EXPIRATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_DATE_EXPIRATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7950)
      .Column(GRID_COLUMN_DATE_EXPIRATION).Width = GRID_WIDTH_DATES
      .Column(GRID_COLUMN_DATE_EXPIRATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date Last Paidback
      .Column(GRID_COLUMN_DATE_LAST_PAYBACK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      .Column(GRID_COLUMN_DATE_LAST_PAYBACK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7956)
      .Column(GRID_COLUMN_DATE_LAST_PAYBACK).Width = GRID_WIDTH_DATE_LAST
      .Column(GRID_COLUMN_DATE_LAST_PAYBACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With
  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Fill total datatable
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <remarks></remarks>
  Private Sub SetTotalsDataTable(ByRef Row As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _row_totals As DataRow

    If m_totals_data_table.Rows.Count = 0 Then
      _row_totals = m_totals_data_table.NewRow()
      _row_totals.Item("LIMIT") = 0
      _row_totals.Item("TTO") = 0
      _row_totals.Item("LENT") = 0
      _row_totals.Item("LENT_PCT") = 0
      m_totals_data_table.Rows.Add(_row_totals)
    End If

    _row_totals = m_totals_data_table.Rows(0)

    _row_totals.Item("LIMIT") = _row_totals.Item("LIMIT") + Row.Value(SQL_COLUMN_CREDIT_LIMIT)
    _row_totals.Item("TTO") = _row_totals.Item("TTO") + Row.Value(SQL_COLUMN_TTO)
    _row_totals.Item("LENT") = _row_totals.Item("LENT") + Row.Value(SQL_COLUMN_LENT)

    If _row_totals.Item("LIMIT") = 0 Then
      _row_totals.Item("LENT_PCT") = 0
    Else
      _row_totals.Item("LENT_PCT") = (_row_totals.Item("LENT") / _row_totals.Item("LIMIT")) * 100
    End If

  End Sub ' SetTotalsDataTable

  ''' <summary>
  ''' Create total datatable 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitTotalsDataTable()

    Dim _column As DataColumn

    If m_totals_data_table IsNot Nothing Then
      m_totals_data_table.Dispose()
      m_totals_data_table = Nothing
    End If

    m_totals_data_table = New DataTable("TOTAL")

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "LIMIT"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "TTO"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "LENT"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "LENT_PCT"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

  End Sub ' InitTotalsDataTable

  ''' <summary>
  ''' Initialize MultiSite Controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeMultiSiteControls()
    'Resize Controls
    gb_debt.Width = 322
    gb_date.Width = 305
    panel_filter.Height = 246

    'Allocate controls:
    gb_date.Location = New System.Drawing.Point(6, 159)
    gb_debt.Location = New System.Drawing.Point(319, 159)
    uc_sites_sel.Location = New System.Drawing.Point(646, 6)

    'TabOrder:
    Me.gb_date.TabIndex = 1
    Me.gb_debt.TabIndex = 3
    Me.uc_creditline_status.TabIndex = 2

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.uc_sites_sel.ShowMultisiteRow = False
    Me.uc_sites_sel.Init()
    Me.uc_sites_sel.SetDefaultValues(True)
  End Sub ' InitializeMultiSiteControls

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chkWithDebt_CheckedChanged(sender As Object, e As EventArgs) Handles chkWithDebt.CheckedChanged

    chkDebtGreaterThan.Enabled = chkWithDebt.Checked
    ef_greater_than.Enabled = chkWithDebt.Checked
    ef_greater_than.Enabled = chkDebtGreaterThan.Checked

    If Not chkDebtGreaterThan.Enabled Then
      chkDebtGreaterThan.Checked = False
      ef_greater_than.Value = String.Empty
    End If

  End Sub ' chkWithDebt_CheckedChanged

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chkDebtGreaterThan_CheckedChanged(sender As Object, e As EventArgs) Handles chkDebtGreaterThan.CheckedChanged

    ef_greater_than.Enabled = chkDebtGreaterThan.Checked

    If Not chkDebtGreaterThan.Checked Then
      ef_greater_than.Value = String.Empty
    End If

    If chkDebtGreaterThan.Checked AndAlso ef_greater_than.Value = String.Empty Then
      ef_greater_than.Value = 0
    End If

  End Sub ' chkDebtGreaterThan_CheckedChanged

#End Region ' Private Methods

End Class
