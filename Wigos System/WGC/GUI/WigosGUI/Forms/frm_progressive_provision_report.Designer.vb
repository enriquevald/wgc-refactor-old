<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_progressive_provision_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_provision_date = New System.Windows.Forms.RadioButton()
    Me.opt_period_date = New System.Windows.Forms.RadioButton()
    Me.gb_group = New System.Windows.Forms.GroupBox()
    Me.opt_group_provision = New System.Windows.Forms.RadioButton()
    Me.opt_group_progressive = New System.Windows.Forms.RadioButton()
    Me.uc_checked_list_progressive_catalog = New GUI_Controls.uc_checked_list()
    Me.gb_detail = New System.Windows.Forms.GroupBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.opt_detail_terminal = New System.Windows.Forms.RadioButton()
    Me.opt_detail_level = New System.Windows.Forms.RadioButton()
    Me.opt_detail_total = New System.Windows.Forms.RadioButton()
    Me.chk_showcanceledprovisions = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_group.SuspendLayout()
    Me.gb_detail.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_showcanceledprovisions)
    Me.panel_filter.Controls.Add(Me.gb_detail)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_progressive_catalog)
    Me.panel_filter.Controls.Add(Me.gb_group)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1122, 196)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_progressive_catalog, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_detail, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_showcanceledprovisions, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 200)
    Me.panel_data.Size = New System.Drawing.Size(1122, 370)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1116, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1116, 4)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 103)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 73)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_provision_date)
    Me.gb_date.Controls.Add(Me.opt_period_date)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 2)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(253, 133)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xEndingDate"
    '
    'opt_provision_date
    '
    Me.opt_provision_date.Location = New System.Drawing.Point(21, 20)
    Me.opt_provision_date.Name = "opt_provision_date"
    Me.opt_provision_date.Size = New System.Drawing.Size(210, 24)
    Me.opt_provision_date.TabIndex = 0
    Me.opt_provision_date.Text = "xSearchByOpeningDate"
    '
    'opt_period_date
    '
    Me.opt_period_date.Location = New System.Drawing.Point(21, 43)
    Me.opt_period_date.Name = "opt_period_date"
    Me.opt_period_date.Size = New System.Drawing.Size(210, 24)
    Me.opt_period_date.TabIndex = 1
    Me.opt_period_date.Text = "xSearchByClosingDate"
    '
    'gb_group
    '
    Me.gb_group.Controls.Add(Me.opt_group_provision)
    Me.gb_group.Controls.Add(Me.opt_group_progressive)
    Me.gb_group.Location = New System.Drawing.Point(265, 2)
    Me.gb_group.Name = "gb_group"
    Me.gb_group.Size = New System.Drawing.Size(244, 67)
    Me.gb_group.TabIndex = 2
    Me.gb_group.TabStop = False
    Me.gb_group.Text = "x_Group"
    '
    'opt_group_provision
    '
    Me.opt_group_provision.AutoSize = True
    Me.opt_group_provision.Location = New System.Drawing.Point(24, 43)
    Me.opt_group_provision.Name = "opt_group_provision"
    Me.opt_group_provision.Size = New System.Drawing.Size(91, 17)
    Me.opt_group_provision.TabIndex = 1
    Me.opt_group_provision.TabStop = True
    Me.opt_group_provision.Text = "x_Provision"
    Me.opt_group_provision.UseVisualStyleBackColor = True
    '
    'opt_group_progressive
    '
    Me.opt_group_progressive.AutoSize = True
    Me.opt_group_progressive.Location = New System.Drawing.Point(24, 20)
    Me.opt_group_progressive.Name = "opt_group_progressive"
    Me.opt_group_progressive.Size = New System.Drawing.Size(106, 17)
    Me.opt_group_progressive.TabIndex = 0
    Me.opt_group_progressive.TabStop = True
    Me.opt_group_progressive.Text = "x_Progressive"
    Me.opt_group_progressive.UseVisualStyleBackColor = True
    '
    'uc_checked_list_progressive_catalog
    '
    Me.uc_checked_list_progressive_catalog.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_progressive_catalog.Location = New System.Drawing.Point(514, 2)
    Me.uc_checked_list_progressive_catalog.m_resize_width = 359
    Me.uc_checked_list_progressive_catalog.multiChoice = True
    Me.uc_checked_list_progressive_catalog.Name = "uc_checked_list_progressive_catalog"
    Me.uc_checked_list_progressive_catalog.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_progressive_catalog.SelectedIndexesList = ""
    Me.uc_checked_list_progressive_catalog.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_progressive_catalog.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_progressive_catalog.SelectedValuesList = ""
    Me.uc_checked_list_progressive_catalog.SetLevels = 2
    Me.uc_checked_list_progressive_catalog.Size = New System.Drawing.Size(359, 188)
    Me.uc_checked_list_progressive_catalog.TabIndex = 4
    Me.uc_checked_list_progressive_catalog.ValuesArray = New String(-1) {}
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.chk_terminal_location)
    Me.gb_detail.Controls.Add(Me.opt_detail_terminal)
    Me.gb_detail.Controls.Add(Me.opt_detail_level)
    Me.gb_detail.Controls.Add(Me.opt_detail_total)
    Me.gb_detail.Location = New System.Drawing.Point(265, 75)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Size = New System.Drawing.Size(244, 115)
    Me.gb_detail.TabIndex = 3
    Me.gb_detail.TabStop = False
    Me.gb_detail.Text = "x_Detail"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(39, 89)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(199, 16)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'opt_detail_terminal
    '
    Me.opt_detail_terminal.AutoSize = True
    Me.opt_detail_terminal.Location = New System.Drawing.Point(24, 66)
    Me.opt_detail_terminal.Name = "opt_detail_terminal"
    Me.opt_detail_terminal.Size = New System.Drawing.Size(88, 17)
    Me.opt_detail_terminal.TabIndex = 2
    Me.opt_detail_terminal.TabStop = True
    Me.opt_detail_terminal.Text = "x_Terminal"
    Me.opt_detail_terminal.UseVisualStyleBackColor = True
    '
    'opt_detail_level
    '
    Me.opt_detail_level.AutoSize = True
    Me.opt_detail_level.Location = New System.Drawing.Point(24, 43)
    Me.opt_detail_level.Name = "opt_detail_level"
    Me.opt_detail_level.Size = New System.Drawing.Size(67, 17)
    Me.opt_detail_level.TabIndex = 1
    Me.opt_detail_level.TabStop = True
    Me.opt_detail_level.Text = "x_Nivel"
    Me.opt_detail_level.UseVisualStyleBackColor = True
    '
    'opt_detail_total
    '
    Me.opt_detail_total.AutoSize = True
    Me.opt_detail_total.Location = New System.Drawing.Point(24, 20)
    Me.opt_detail_total.Name = "opt_detail_total"
    Me.opt_detail_total.Size = New System.Drawing.Size(66, 17)
    Me.opt_detail_total.TabIndex = 0
    Me.opt_detail_total.TabStop = True
    Me.opt_detail_total.Text = "x_Total"
    Me.opt_detail_total.UseVisualStyleBackColor = True
    '
    'chk_showcanceledprovisions
    '
    Me.chk_showcanceledprovisions.AutoSize = True
    Me.chk_showcanceledprovisions.Location = New System.Drawing.Point(12, 154)
    Me.chk_showcanceledprovisions.Name = "chk_showcanceledprovisions"
    Me.chk_showcanceledprovisions.Size = New System.Drawing.Size(182, 17)
    Me.chk_showcanceledprovisions.TabIndex = 1
    Me.chk_showcanceledprovisions.Text = "x_ShowCanceledProvisions"
    Me.chk_showcanceledprovisions.UseVisualStyleBackColor = True
    '
    'frm_progressive_provision_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1132, 574)
    Me.Name = "frm_progressive_provision_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_progressive_provision_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_group.ResumeLayout(False)
    Me.gb_group.PerformLayout()
    Me.gb_detail.ResumeLayout(False)
    Me.gb_detail.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_group As System.Windows.Forms.GroupBox
  Friend WithEvents opt_group_provision As System.Windows.Forms.RadioButton
  Friend WithEvents opt_group_progressive As System.Windows.Forms.RadioButton
  Friend WithEvents opt_provision_date As System.Windows.Forms.RadioButton
  Friend WithEvents opt_period_date As System.Windows.Forms.RadioButton
  Friend WithEvents uc_checked_list_progressive_catalog As GUI_Controls.uc_checked_list
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Friend WithEvents opt_detail_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_detail_level As System.Windows.Forms.RadioButton
  Friend WithEvents opt_detail_total As System.Windows.Forms.RadioButton
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_showcanceledprovisions As System.Windows.Forms.CheckBox
End Class
