﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_fbm_log
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_fbm_session_status_cont = New System.Windows.Forms.Label()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.gb_machines = New System.Windows.Forms.GroupBox()
    Me.lbl_fbm_machines_status_cont = New System.Windows.Forms.Label()
    Me.lbl_gui_machines_status_cont = New System.Windows.Forms.Label()
    Me.lbl_fbm_machines_status = New System.Windows.Forms.Label()
    Me.lbl_gui_machines_status = New System.Windows.Forms.Label()
    Me.gb_session = New System.Windows.Forms.GroupBox()
    Me.lbl_gui_session_status_cont = New System.Windows.Forms.Label()
    Me.lbl_fbm_session_status = New System.Windows.Forms.Label()
    Me.lbl_gui_session_status = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_machines.SuspendLayout()
    Me.gb_session.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(777, 144)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 148)
    Me.panel_data.Size = New System.Drawing.Size(777, 210)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(771, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(771, 4)
    '
    'lbl_fbm_session_status_cont
    '
    Me.lbl_fbm_session_status_cont.AutoSize = True
    Me.lbl_fbm_session_status_cont.BackColor = System.Drawing.Color.Red
    Me.lbl_fbm_session_status_cont.Location = New System.Drawing.Point(71, 33)
    Me.lbl_fbm_session_status_cont.Name = "lbl_fbm_session_status_cont"
    Me.lbl_fbm_session_status_cont.Size = New System.Drawing.Size(81, 13)
    Me.lbl_fbm_session_status_cont.TabIndex = 11
    Me.lbl_fbm_session_status_cont.Text = "xOpen/Close"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.gb_machines)
    Me.gb_status.Controls.Add(Me.gb_session)
    Me.gb_status.Location = New System.Drawing.Point(6, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(406, 132)
    Me.gb_status.TabIndex = 12
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'gb_machines
    '
    Me.gb_machines.Controls.Add(Me.lbl_fbm_machines_status_cont)
    Me.gb_machines.Controls.Add(Me.lbl_gui_machines_status_cont)
    Me.gb_machines.Controls.Add(Me.lbl_fbm_machines_status)
    Me.gb_machines.Controls.Add(Me.lbl_gui_machines_status)
    Me.gb_machines.Location = New System.Drawing.Point(202, 23)
    Me.gb_machines.Name = "gb_machines"
    Me.gb_machines.Size = New System.Drawing.Size(177, 90)
    Me.gb_machines.TabIndex = 14
    Me.gb_machines.TabStop = False
    Me.gb_machines.Text = "xMachines"
    '
    'lbl_fbm_machines_status_cont
    '
    Me.lbl_fbm_machines_status_cont.AutoSize = True
    Me.lbl_fbm_machines_status_cont.BackColor = System.Drawing.Color.Red
    Me.lbl_fbm_machines_status_cont.Location = New System.Drawing.Point(72, 33)
    Me.lbl_fbm_machines_status_cont.Name = "lbl_fbm_machines_status_cont"
    Me.lbl_fbm_machines_status_cont.Size = New System.Drawing.Size(81, 13)
    Me.lbl_fbm_machines_status_cont.TabIndex = 11
    Me.lbl_fbm_machines_status_cont.Text = "xOpen/Close"
    '
    'lbl_gui_machines_status_cont
    '
    Me.lbl_gui_machines_status_cont.AutoSize = True
    Me.lbl_gui_machines_status_cont.BackColor = System.Drawing.Color.Chartreuse
    Me.lbl_gui_machines_status_cont.Location = New System.Drawing.Point(72, 56)
    Me.lbl_gui_machines_status_cont.Name = "lbl_gui_machines_status_cont"
    Me.lbl_gui_machines_status_cont.Size = New System.Drawing.Size(81, 13)
    Me.lbl_gui_machines_status_cont.TabIndex = 8
    Me.lbl_gui_machines_status_cont.Text = "xOpen/Close"
    '
    'lbl_fbm_machines_status
    '
    Me.lbl_fbm_machines_status.AutoSize = True
    Me.lbl_fbm_machines_status.Location = New System.Drawing.Point(12, 33)
    Me.lbl_fbm_machines_status.Name = "lbl_fbm_machines_status"
    Me.lbl_fbm_machines_status.Size = New System.Drawing.Size(42, 13)
    Me.lbl_fbm_machines_status.TabIndex = 5
    Me.lbl_fbm_machines_status.Text = "xFBM:"
    '
    'lbl_gui_machines_status
    '
    Me.lbl_gui_machines_status.AutoSize = True
    Me.lbl_gui_machines_status.Location = New System.Drawing.Point(12, 56)
    Me.lbl_gui_machines_status.Name = "lbl_gui_machines_status"
    Me.lbl_gui_machines_status.Size = New System.Drawing.Size(61, 13)
    Me.lbl_gui_machines_status.TabIndex = 6
    Me.lbl_gui_machines_status.Text = "xWIGOS:"
    '
    'gb_session
    '
    Me.gb_session.Controls.Add(Me.lbl_fbm_session_status_cont)
    Me.gb_session.Controls.Add(Me.lbl_gui_session_status_cont)
    Me.gb_session.Controls.Add(Me.lbl_fbm_session_status)
    Me.gb_session.Controls.Add(Me.lbl_gui_session_status)
    Me.gb_session.Location = New System.Drawing.Point(19, 23)
    Me.gb_session.Name = "gb_session"
    Me.gb_session.Size = New System.Drawing.Size(177, 90)
    Me.gb_session.TabIndex = 13
    Me.gb_session.TabStop = False
    Me.gb_session.Text = "xSession"
    '
    'lbl_gui_session_status_cont
    '
    Me.lbl_gui_session_status_cont.AutoSize = True
    Me.lbl_gui_session_status_cont.BackColor = System.Drawing.Color.Chartreuse
    Me.lbl_gui_session_status_cont.Location = New System.Drawing.Point(71, 56)
    Me.lbl_gui_session_status_cont.Name = "lbl_gui_session_status_cont"
    Me.lbl_gui_session_status_cont.Size = New System.Drawing.Size(81, 13)
    Me.lbl_gui_session_status_cont.TabIndex = 8
    Me.lbl_gui_session_status_cont.Text = "xOpen/Close"
    '
    'lbl_fbm_session_status
    '
    Me.lbl_fbm_session_status.AutoSize = True
    Me.lbl_fbm_session_status.Location = New System.Drawing.Point(11, 33)
    Me.lbl_fbm_session_status.Name = "lbl_fbm_session_status"
    Me.lbl_fbm_session_status.Size = New System.Drawing.Size(42, 13)
    Me.lbl_fbm_session_status.TabIndex = 5
    Me.lbl_fbm_session_status.Text = "xFBM:"
    '
    'lbl_gui_session_status
    '
    Me.lbl_gui_session_status.AutoSize = True
    Me.lbl_gui_session_status.Location = New System.Drawing.Point(11, 56)
    Me.lbl_gui_session_status.Name = "lbl_gui_session_status"
    Me.lbl_gui_session_status.Size = New System.Drawing.Size(61, 13)
    Me.lbl_gui_session_status.TabIndex = 6
    Me.lbl_gui_session_status.Text = "xWIGOS:"
    '
    'frm_fbm_log
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(787, 362)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_fbm_log"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_fbm_log"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_machines.ResumeLayout(False)
    Me.gb_machines.PerformLayout()
    Me.gb_session.ResumeLayout(False)
    Me.gb_session.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_fbm_session_status_cont As System.Windows.Forms.Label
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_gui_session_status_cont As System.Windows.Forms.Label
  Friend WithEvents lbl_gui_session_status As System.Windows.Forms.Label
  Friend WithEvents lbl_fbm_session_status As System.Windows.Forms.Label
  Friend WithEvents gb_machines As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_fbm_machines_status_cont As System.Windows.Forms.Label
  Friend WithEvents lbl_gui_machines_status_cont As System.Windows.Forms.Label
  Friend WithEvents lbl_fbm_machines_status As System.Windows.Forms.Label
  Friend WithEvents lbl_gui_machines_status As System.Windows.Forms.Label
  Friend WithEvents gb_session As System.Windows.Forms.GroupBox
End Class
