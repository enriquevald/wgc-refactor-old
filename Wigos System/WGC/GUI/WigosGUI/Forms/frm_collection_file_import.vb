'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_collection_file_import
' DESCRIPTION:   import the collection files.
' AUTHOR:        Samuel Gonz�lez
' CREATION DATE: 04-MAY-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-MAY-2015  SGB    Initial version. Backlog Item 1314
' 10-MAY-2015  SGB    BUG 1895: Show in line error in spanish "Line".
' 10-MAY-2015  SGB    BUG 1897: Change result log.
' 22-OCT-2015  SGB    Backlog Item 5422: Control error if not exist GP type.
'--------------------------------------------------------------------
#Region " Imports "

Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Text
Imports WSI.Common.CollectionImport

#End Region ' Imports
Public Delegate Sub ProcessImportMessageDelegate(ByVal ErrorReceived As IImported)

Public Class frm_collection_file_import
  Inherits frm_base_edit

#Region " Enums "

#End Region ' Enums

#Region " Constants "


#End Region ' Constants

#Region " Members "
  Dim m_import_manager As ImportManager
  Dim m_status_saved As Boolean
  Dim m_has_errors As Boolean = False
#End Region ' Members

#Region " Properties "

#End Region ' Properties

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COLLECTION_FILE_IMPORT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.btn_open_file_1_dialog

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _initial_time As Date
    Dim _str_message As String

    _initial_time = WSI.Common.Misc.TodayOpening()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6489)

    ' Enable / Disable buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Text = GLB_NLS_GUI_CLASS_II.GetString(427)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CLASS_II.GetString(561)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Imported
    Me.gb_imported.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6300)

    Me.ef_file_1_path.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6295)
    Me.ef_file_2_path.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6296)
    Me.dtp_imported.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6298)
    Me.dtp_imported.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_imported.Value = _initial_time
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6297)
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    _str_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6359, GUI_FormatDate(Me.dtp_imported.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    Me.ef_name.Value = _str_message

    'Me.ef_file_1_path.Enabled = False
    'Me.ef_file_2_path.Enabled = False

    'Formating Screen
    Me.gb_output.Text = GLB_NLS_GUI_CLASS_II.GetString(425)

    Me.tb_output.Enabled = True
    Me.tb_output.ReadOnly = True

  End Sub 'GUI_InitControls

  ' PURPOSE: Process clicks on select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_PRINT
        m_import_manager.StopImporting()
        Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
        Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = True
        Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False

      Case ENUM_BUTTON.BUTTON_DELETE
        If ef_file_1_path.Value = "" And ef_file_2_path.Value = "" Then
          ' 6301 "Debe seleccionar por lo menos un archivo."
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6301), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call ef_name.Focus()

          Return
        End If
        Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = True
        Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        Me.ef_file_1_path.Enabled = False
        Me.ef_file_2_path.Enabled = False
        Me.btn_open_file_1_dialog.Enabled = False
        Me.btn_open_file_2_dialog.Enabled = False
        Me.tb_output.Clear()
        Call ValidateFiles()

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_GetScreenData()
    Dim _soft_count As CLASS_COLLECTION_FILE_IMPORT

    _soft_count = DbEditedObject

    _soft_count.Name = ef_name.TextValue
    _soft_count.WorkingDay = dtp_imported.Value
    _soft_count.SoftCounts.m_import_user_id = GLB_CurrentUser.Id
    _soft_count.SoftCounts.m_import_datetime = WGDB.Now

  End Sub


  Private Sub ValidateFiles()
    Dim _import_info As ImportInfo
    Dim _edited As CLASS_COLLECTION_FILE_IMPORT

    _edited = DbEditedObject

    _import_info = New ImportInfo()
    _import_info.File1 = ef_file_1_path.TextValue
    _edited.File1Name = _import_info.File1
    _import_info.File2 = ef_file_2_path.TextValue
    _edited.File2Name = _import_info.File2

    Try

      m_import_manager = New ImportManager(_import_info)
      AddHandler m_import_manager.ImportedReceivedEvent, AddressOf ProcessImportMessage
      m_import_manager.StartImporting()

    Catch ex As Exception
      ' TODO falta NLS
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6849) _
                  , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR _
                  , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End Try
    

  End Sub

  ' PURPOSE : Check if screen data is ok before saving
  '
  '  PARAMS :
  '     -  INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '
  '   NOTES :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(ef_name.Value) Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_name.Text)
      Call ef_name.Focus()

      Return False
    End If

   
    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _edited As CLASS_COLLECTION_FILE_IMPORT


    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        _edited = New CLASS_COLLECTION_FILE_IMPORT()
        _edited.WorkingDay = WSI.Common.Misc.TodayOpening()
        DbEditedObject = _edited
      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        m_status_saved = False
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853) _
                  , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR _
                  , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          m_status_saved = True
        End If
      Case Else
        MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub
  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    Dim _parameters As SofCountData.SOFT_COUNT_PARAMETERS
    Dim _soft_count As CLASS_COLLECTION_FILE_IMPORT
    Dim _frm As frm_collection_file

    If Me.m_status_saved Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6367), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
      mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

        'Call Collection Form        
        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        _soft_count = DbEditedObject
        _frm = New frm_collection_file()

        _parameters = New SofCountData.SOFT_COUNT_PARAMETERS
        _parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT
        _parameters.Xml = _soft_count.XmlSoftCount
        _parameters.SessionDay = Me.dtp_imported.Value
        _parameters.SoftCountId = _soft_count.SoftCountID
        _parameters.SoftCountName = _soft_count.Name

        Windows.Forms.Cursor.Current = Cursors.Default
        _frm.ShowNewItem(_parameters)

      End If
    End If

  End Sub ' GUI_Closing

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE : Retorn un entry field value the path name & file name selected
  '
  '  PARAMS :
  '     -  INPUT :
  '         - ef_path
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  '   NOTES :
  Private Sub FilePathName(ByVal ef_path As uc_entry_field)
    Dim _file_dialog As OpenFileDialog
    _file_dialog = New OpenFileDialog()
    _file_dialog.Multiselect = False
    _file_dialog.Filter = "DAT(*.DAT)|*.DAT"
    _file_dialog.ShowDialog()
    ef_path.Value = _file_dialog.FileName.ToString()
  End Sub


  Private Function GetEventText(ByVal _event As ImportedEvent) As String
    Dim _str As String
    Select Case _event.ImportEvent
      Case ImportEvent.File1NotFound
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6317)
      Case ImportEvent.File2NotFound
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6318)
      Case ImportEvent.ImportationFinishedFile1
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6319) & System.Environment.NewLine
      Case ImportEvent.ImportationFinishedFile2
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6320) & System.Environment.NewLine
      Case ImportEvent.ValidationFinishedFile1
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6321) & System.Environment.NewLine
      Case ImportEvent.ValidationFinishedFile2
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6322) & System.Environment.NewLine
      Case ImportEvent.ImportationStartedFile1
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6492)
      Case ImportEvent.ImportationStartedFile2
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6493)
      Case ImportEvent.ValidationStartedFile1
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6490)
      Case ImportEvent.ValidationStartedFile2
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6491)
      Case Else
        _str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6323)
    End Select

    Return _str
  End Function
#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Form entry point from menu selection.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit()

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.MdiParent = MdiParent

    DbObjectId = Nothing
    DbEditedObject = New CLASS_COLLECTION_FILE_IMPORT()

    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    Call Me.Display(True)

  End Sub ' ShowEditItem

#End Region ' Public Functions 

#Region " Events "

  ' PURPOSE: opens a file dialog to chose the document to import
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_open_file_1_dialog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_open_file_1_dialog.Click
    FilePathName(ef_file_1_path)
  End Sub

  ' PURPOSE: opens a file dialog to chose the document to import
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_open_file_2_dialog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_open_file_2_dialog.Click
    FilePathName(ef_file_2_path)
  End Sub

  Private Sub ProcessImportMessage(ByVal Message As IImported)
    Dim _event As ImportedEvent
    Dim _error As ImportedError
    Dim _soft_count As CLASS_COLLECTION_FILE_IMPORT

    If Me.InvokeRequired Then
      Me.Invoke(New ProcessImportMessageDelegate(AddressOf ProcessImportMessage), Message)
    Else
      Select Case Message.GetImportedType()
        Case ImportedType.Event
          _event = Message
          tb_output.AppendText(GetEventText(Message) & System.Environment.NewLine)
        Case ImportedType.Error
          m_has_errors = True
          _error = Message
          tb_output.AppendText(_error.Error & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6486) & _error.Line & System.Environment.NewLine)
        Case ImportedType.SoftCount

          If m_has_errors Then
            tb_output.AppendText(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6495)) 'Proceso finalizado con errores
          Else
            tb_output.AppendText(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6494)) 'Proceso finalizado correctamente
          End If

          _soft_count = DbEditedObject
          _soft_count.SoftCounts = Message

                    'If there's no SoftCounts, file or files are not correct,
                    ' so we can't generate importation in DB
                    If _soft_count.SoftCounts.m_soft_count.Count <= 0 Then

                        Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
                        Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False

                        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6496), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_file_1_path.Text, ef_file_2_path.Text)
                    Else

                        Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
                        Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
                    End If

            End Select
    End If
  End Sub


#End Region ' Events

  Private Sub dtp_imported_DatePickerValueChanged() Handles dtp_imported.DatePickerValueChanged
    Dim _str_message As String

    If Me.ef_name.Value.StartsWith(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6359)) Then
      _str_message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6359, GUI_FormatDate(Me.dtp_imported.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
      Me.ef_name.Value = _str_message
    End If

  End Sub
End Class