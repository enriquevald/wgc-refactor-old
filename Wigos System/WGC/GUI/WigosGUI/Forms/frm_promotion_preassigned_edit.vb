'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotion_preassigned_edit.vb
' DESCRIPTION:   Preassigned promotion edit
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 16-AUG-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-AUG-2012  MPO    Initial version
' 27-AUG-2012  MPO    ContainAccountAnonymous --> Check holder level to know if is anonymous
' 12-SEP-2012  HBB    Added a textbox to specify a ticket footer
' 25-SEP-2012  QMP    Added Promotion Category
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty 
' 11-JUN-2013  RRR    Copy the data from a selected promotion to a new one
' 19-JUN-2013  RRR    Fixed Bug #878: Error adding images to copied promotions
' 14-OCT-2013  ANG    Fix bug WIG-64
' 23-OCT-2013  JMM    Added image to show on PromoBOX and Visible On PromoBOX check
' 05-NOV-2013  RMS    Added Description text and Applicable on PromoBOX checkbox
' 18-NOV-2013  QMP    Hide small image (icon) and validate large image
' 12-JUN-2014  XCD    Show Dialog message with comments if there are changes
' 04-NOV-2015  GMV    PBI 5836 - Garcia River - Discernir promos visibles en PromoBOX y Display Touch
' 08-MAY-2018  AGS    Bug 32603: WIGOS-10727 Missing image configuration for Preassigned promotions appearing in LCD InTouch
' 15-MAY-2018  GDA    Bug 32701 -- 11736 -- Unable to set an image for a preassigned promotion on the LCD InTouch if it is not checked 'Show in PromoBOX upper screen' checkbox.
' -------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.IO
Imports GUI_Reports

Public Class frm_promotion_preassigned_edit
  Inherits frm_base_edit

#Region " Members "

  Private m_promotion_copied_id As Long
  Private m_promotion_copy_operation As Boolean
  Private m_current_directory As String
  Private m_dt_acc As DataTable
  Private m_dt_error As DataTable
  ' Progress bar
  Private m_progress As frm_excel_progress = Nothing

#End Region

#Region " Constants "

  Private Const LEN_PROMO_NAME As Integer = 20
  Private Const LEN_PROMO_DESC As Integer = 50
  Private Const FORM_MIN_DB_VERSION As Short = 158

#End Region ' Constants

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTION_PREASSIGNED_EDIT

    Call GUI_SetMinDbVersion(FORM_MIN_DB_VERSION)
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim _closing_time As Integer

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    _closing_time = GetDefaultClosingTime()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1251)

    ' Hide the delete button if ScreenMode is New
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      btn_import.Enabled = True
      gb_credit_type.Enabled = True
      ef_expiration_days.Enabled = True
    Else
      btn_import.Enabled = False
      gb_credit_type.Enabled = False
      ef_expiration_days.Enabled = False
    End If

    ' Name
    ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Credit Type
    gb_credit_type.Text = GLB_NLS_GUI_INVOICING.GetString(336)
    opt_credit_non_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(338)
    opt_credit_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    opt_point.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)

    ' Enabled control
    gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)
    opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Promobox tab
    tab_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6928) ' 6928 "Options"
    chk_visible_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2868) '2868 "Mostrar en monitor superior"
    lbl_apply_on.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2869) '2869 "Allow request"
    chk_award_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(722) ' 722 "PromoBOX"
    chk_award_on_InTouch.Text = GeneralParam.GetString("DisplayTouch", "ProductName")

    ef_text_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2870)   '2870 "Display text"
    ef_text_on_PromoBOX.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_DESC)
    ef_text_on_PromoBOX.TextVisible = True
    lbl_no_text_on_promobox_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2872)   '2872 "Sin descripci�n s�lo se mostrar� la cantidad"
    Call UpdatePromoBOXTabState()

    ' PromoBOX Icon
    Call img_icon.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)
    lbl_icon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(719)
    lbl_icon_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "200", "150")
    img_icon.ImageInfoFont = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

    ' PromoBOX Image
    Call img_image.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)
    lbl_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(687)
    lbl_image_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "1280", "1024")
    img_image.Enabled = chk_visible_on_PromoBOX.Checked
    img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

    ' TicketFooter Tab
    tab_ticketfooter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1284)

    ' uc_chedule control
    uc_promo_schedule.SetDefaults()

    ' gb_account
    gb_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1237)
    btn_import.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1238)
    opt_account_summary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1239)
    opt_all_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1240)

    ' Expiration
    ef_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(448) ' 448 "Expires after"
    ef_expiration_days.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(444)
    ef_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    '
    m_dt_acc = Nothing
    m_dt_error = Nothing
    '
    AddHandler opt_account_summary.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler opt_all_account.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler opt_credit_non_redeemable.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler opt_credit_redeemable.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler opt_point.CheckedChanged, AddressOf opt_CheckedChanged

    ' Promotion Categories
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
    Me.cmb_categories.Add(PromotionCategories.Categories())

    ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
    'If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) Then
    '  ' Initialize Promogames combo
    '  Call InitAwardWithGameCombobox()

    '  Me.chk_award_with_game.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8613)
    'Else
    Me.cmb_award_with_game.Visible = False
    Me.chk_award_with_game.Visible = False
    'End If


  End Sub ' GUI_InitControls

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If m_progress IsNot Nothing Then
      m_progress.Close()
      m_progress.Dispose()
      m_progress = Nothing
    End If
    MyBase.GUI_Closing(CloseCanceled)
  End Sub ' GUI_Closing

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _promo_class As CLASS_PROMOTION
    Dim _aux_date As DateTime

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROMOTION
        _promo_class = DbEditedObject
        _promo_class.PromotionId = 0
        _promo_class.Enabled = True
        _promo_class.SpecialPermission = CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
        _promo_class.Type = Promotion.PROMOTION_TYPE.PREASSIGNED
        _promo_class.CreditType = ACCOUNT_PROMO_CREDIT_TYPE.NR1
        _promo_class.MinCashIn = 0
        _promo_class.MinCashInReward = 0
        _promo_class.MinPlayed = 0
        _promo_class.MinPlayedReward = 0
        _promo_class.MinSpent = 0
        _promo_class.MinSpentReward = 0
        _aux_date = Now.AddMonths(1)
        _promo_class.DateStart = New DateTime(_aux_date.Year, _aux_date.Month, 1, GetDefaultClosingTime(), 0, 0)
        _promo_class.DateFinish = _promo_class.DateStart
        _promo_class.DateFinish = _promo_class.DateStart.AddMonths(1)
        _promo_class.ScheduleWeekday = uc_promo_schedule.AllWeekDaysSelected()     ' All days
        _promo_class.Schedule1TimeFrom = 0     ' 00:00:00
        _promo_class.Schedule1TimeTo = 0       ' 00:00:00
        _promo_class.Schedule2Enabled = False
        _promo_class.Schedule2TimeFrom = 0     ' 00:00:00
        _promo_class.Schedule2TimeTo = 0       ' 00:00:00
        _promo_class.GenderFilter = CLASS_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
        _promo_class.BirthdayFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        _promo_class.LevelFilter = 0
        _promo_class.ExpirationType = CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            _promo_class.PromoScreenMode = CLASS_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            _promo_class.PromoScreenMode = CLASS_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_EDIT
        End Select

        ' RMS 05-NOV-2013   Change default values for promobox
        _promo_class.VisibleOnPromoBOX = False
        _promo_class.AwardOnPromoBOX = 0
        _promo_class.TextOnPromoBOX = String.Empty

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Dim _comments As frm_comments

        _promo_class = Me.DbEditedObject

        _comments = New frm_comments(NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(495)), _
                                     NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(496)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(1)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(2)), _
                                     False) ' Comments

        Try
          If _comments.ShowGPDialog() = Windows.Forms.DialogResult.OK Then
            _promo_class.Comments = _comments.Comments
            Me.DbStatus = ENUM_STATUS.STATUS_OK
          Else
            _promo_class.Comments = ""
            Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND
          End If
        Finally
          _comments.Dispose()
        End Try

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          _promo_class = Me.DbEditedObject
          _promo_class.Comments = ""
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _promo_item As CLASS_PROMOTION
    Dim _report_mode As WSI.Common.AccountsImport.REPORT_MODE

    _promo_item = DbReadObject

    ef_promo_name.Value = _promo_item.Name

    If _promo_item.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    ef_text_on_PromoBOX.Value = _promo_item.TextOnPromoBOX

    uc_promo_schedule.DateFrom = _promo_item.DateStart
    uc_promo_schedule.DateTo = _promo_item.DateFinish
    uc_promo_schedule.Weekday = _promo_item.ScheduleWeekday
    uc_promo_schedule.TimeFrom = _promo_item.Schedule1TimeFrom
    uc_promo_schedule.TimeTo = _promo_item.Schedule1TimeTo
    uc_promo_schedule.SecondTime = _promo_item.Schedule2Enabled
    uc_promo_schedule.SecondTimeFrom = _promo_item.Schedule2TimeFrom
    uc_promo_schedule.SecondTimeTo = _promo_item.Schedule2TimeTo

    ' Expiration type and value
    Select Case _promo_item.ExpirationType

      Case CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        If _promo_item.ExpirationValue > 0 Then
          ef_expiration_days.Value = _promo_item.ExpirationValue
        End If

      Case Else ' All periodical promotions managed as ENUM_EXPIRATION_TYPE.DAYS
        If _promo_item.ExpirationValue > 0 Then
          ef_expiration_days.Value = _promo_item.ExpirationValue
        End If

    End Select

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW) Then

      opt_credit_non_redeemable.Checked = True

    End If

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT) Or (Me.m_promotion_copy_operation) Then

      Select Case _promo_item.CreditType
        Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
          opt_credit_non_redeemable.Checked = True
        Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
          opt_point.Checked = True
        Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
          opt_credit_redeemable.Checked = True
        Case Else
          opt_credit_non_redeemable.Checked = True
      End Select

      If _promo_item.Type = Promotion.PROMOTION_TYPE.PREASSIGNED Then

        If opt_account_summary.Checked Then
          _report_mode = AccountsImport.REPORT_MODE.SUMMARY
        ElseIf opt_all_account.Checked Then
          _report_mode = AccountsImport.REPORT_MODE.DETAILS
        End If

        Call ShowReport(_promo_item.AccountsPreassigned, New DataTable(), _report_mode)
        m_dt_acc = _promo_item.AccountsPreassigned

        m_dt_error = New Data.DataTable()

      End If

    End If

    'HBB: Initialize ticket_footer field
    tb_ticket_footer_prasigned_promotion.Text = _promo_item.TicketFooter

    cmb_categories.Value = _promo_item.CategoryId

    ' Visible On Promo
    chk_visible_on_PromoBOX.Checked = _promo_item.VisibleOnPromoBOX

    ' Applicable en PromoBox
    chk_award_on_PromoBOX.Checked = (_promo_item.AwardOnPromoBOX And PROMOTION_AWARD_TARGET.PROMOBOX) = PROMOTION_AWARD_TARGET.PROMOBOX
    chk_award_on_InTouch.Checked = (_promo_item.AwardOnPromoBOX And PROMOTION_AWARD_TARGET.INTOUCH) = PROMOTION_AWARD_TARGET.INTOUCH


    ' PromoBOX Icon
    img_icon.Image = _promo_item.Icon

    ' PromoBOX Image
    img_image.Image = _promo_item.Image


    ' Set promogame value
    If _promo_item.Promogame > 0 Then
      Me.chk_award_with_game.Checked = True
      Me.cmb_award_with_game.SelectedValue = _promo_item.Promogame
      Me.cmb_award_with_game.Enabled = Not _promo_item.IsPromotionWithPromoGamePendingPlayed(_promo_item.PromotionId)
      Me.chk_award_with_game.Enabled = Me.cmb_award_with_game.Enabled
    Else
      Me.chk_award_with_game.Checked = False
      Me.cmb_award_with_game.SelectedIndex = -1
      Me.cmb_award_with_game.Enabled = False
    End If

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String
    Dim _promo_item As CLASS_PROMOTION

    _promo_item = DbReadObject

    ' Name
    If ef_promo_name.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_promo_name.Focus()

      Return False
    End If

    ' Expiration dates range
    If uc_promo_schedule.DateFrom > uc_promo_schedule.DateTo Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_promo_schedule.SetFocus(1)

      Return False
    End If

    ' No Week days selected
    If uc_promo_schedule.Weekday = uc_promo_schedule.NoWeekDaysSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_promo_schedule.SetFocus(4)

      Return False
    End If

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      If IsNothing(m_dt_acc) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1246), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        btn_import.Focus()

        Return False
      End If
    End If

    ' Expiration 
    If GUI_ParseNumber(ef_expiration_days.Value) <= 0 Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_expiration_days.Focus()

      Return False
    End If

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW) Then
      If Me.opt_point.Checked And ContainAccountAnonymous(m_dt_acc) Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1265), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Return False
        End If
      End If
    End If

    ' QMP 18-NOV-2013: Validate large image
    If chk_visible_on_PromoBOX.Checked And Me.img_image.Image Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1435), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call img_image.Focus()

      Return False
    End If

    ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
    'If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) Then
    '  If Me.chk_award_with_game.Checked AndAlso Me.cmb_award_with_game.SelectedIndex = -1 Then
    '    Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8677), ENUM_MB_TYPE.MB_TYPE_WARNING)
    '    Call Me.cmb_award_with_game.Focus()

    '    Return False
    '  End If
    'End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _promo_item As CLASS_PROMOTION

    _promo_item = DbEditedObject

    _promo_item.Name = ef_promo_name.Value
    _promo_item.Enabled = opt_enabled.Checked
    _promo_item.TextOnPromoBOX = ef_text_on_PromoBOX.Value

    _promo_item.DateStart = uc_promo_schedule.DateFrom
    _promo_item.DateFinish = uc_promo_schedule.DateTo

    _promo_item.ScheduleWeekday = uc_promo_schedule.Weekday
    _promo_item.Schedule1TimeFrom = uc_promo_schedule.TimeFrom
    _promo_item.Schedule1TimeTo = uc_promo_schedule.TimeTo
    _promo_item.Schedule2Enabled = uc_promo_schedule.SecondTime
    If _promo_item.Schedule2Enabled Then
      _promo_item.Schedule2TimeFrom = uc_promo_schedule.SecondTimeFrom
      _promo_item.Schedule2TimeTo = uc_promo_schedule.SecondTimeTo
    Else
      _promo_item.Schedule2TimeFrom = 0
      _promo_item.Schedule2TimeTo = 0
    End If

    _promo_item.ExpirationType = CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
    _promo_item.ExpirationValue = GUI_ParseNumber(ef_expiration_days.Value)

    If opt_credit_non_redeemable.Checked Then
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
    ElseIf opt_credit_redeemable.Checked Then
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    ElseIf opt_point.Checked Then
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
    Else
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
    End If

    _promo_item.WonLockEnabled = False
    _promo_item.WonLock = 0

    _promo_item.AccountsPreassigned = m_dt_acc

    'getting the ticket footer text
    _promo_item.TicketFooter = tb_ticket_footer_prasigned_promotion.Text

    _promo_item.CategoryId = cmb_categories.Value

    ' Visible On Promo
    _promo_item.VisibleOnPromoBOX = chk_visible_on_PromoBOX.Checked

    ' Applicable on PromoBox
    ' GMV 05-NOV-2015
    _promo_item.AwardOnPromoBOX = If(Me.chk_award_on_PromoBOX.Checked, PROMOTION_AWARD_TARGET.PROMOBOX, 0) + If(Me.chk_award_on_InTouch.Checked, PROMOTION_AWARD_TARGET.INTOUCH, 0)

    ' PromoBOX Icon
    _promo_item.Icon = img_icon.Image

    ' PromoBOX Image
    _promo_item.Image = img_image.Image

    If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) Then

      If Me.chk_award_with_game.Checked Then
        _promo_item.Promogame = Me.cmb_award_with_game.SelectedValue
      Else
        _promo_item.Promogame = -1
      End If
    End If


  End Sub ' GUI_GetScreenData

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.tb_ticket_footer_prasigned_promotion.ContainsFocus Then

          Return True
        End If

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)
    End Select

    ' The key Enter event is done in tb_ticket_footer_prasigned_promotion

  End Function ' GUI_KeyPress

#End Region

#Region " Public "

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal PromotionCopiedId As Long = 0)
    Dim _promo As CLASS_PROMOTION

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = PromotionCopiedId

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    If PromotionCopiedId > 0 Then
      _promo = DbEditedObject

      DbObjectId = PromotionCopiedId
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      DbObjectId = Nothing
      Me.m_promotion_copy_operation = True

      Call _promo.ResetIdValues()

    End If
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal PromoId As Integer, ByVal PromoName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = 0

    Me.DbObjectId = PromoId
    '    Me.m_input_promo_name = PromoName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> ENUM_STATUS.STATUS_OK Then

      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowEditItem

  ' PURPOSE: Update the progress bar.
  '
  '    - INPUT:
  '         - Count As Int32
  '         - Max As Int32
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub UpdateProgressBar(ByVal Count As Int32, ByVal Max As Int32)
    If m_progress IsNot Nothing Then
      m_progress.SetValues(Count, Max)
    End If
  End Sub ' UpdateProgressBar

#End Region

#Region " Private "

  ' PURPOSE: Update controls on PromoBOX tab
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdatePromoBOXTabState()
    ef_text_on_PromoBOX.Enabled = chk_award_on_PromoBOX.Checked Or chk_award_on_InTouch.Checked
    lbl_no_text_on_promobox_info.Visible = (ef_text_on_PromoBOX.Enabled And String.IsNullOrEmpty(ef_text_on_PromoBOX.Value))
  End Sub

  ' PURPOSE: Show report in richtextbox
  '
  '  PARAMS:
  '     - INPUT:
  '           - DtAccount:
  '           - DtErrors:
  '           - ReportMode:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub ShowReport(ByVal DtAccount As DataTable, ByVal DtErrors As DataTable, ByVal ReportMode As WSI.Common.AccountsImport.REPORT_MODE)

    Dim _report As System.Text.StringBuilder
    Dim _credit_type As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE

    _report = New System.Text.StringBuilder()

    If opt_credit_redeemable.Checked Then
      _credit_type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    ElseIf opt_point.Checked Then
      _credit_type = ACCOUNT_PROMO_CREDIT_TYPE.POINT
    Else
      _credit_type = ACCOUNT_PROMO_CREDIT_TYPE.NR1
    End If

    Call WSI.Common.AccountsImport.ReportForPreassignedPromotion(DtAccount, DtErrors, _credit_type, ReportMode, _report)

    rtb_report.Text = _report.ToString()

  End Sub

  ' PURPOSE: Enable/Disable the buttons for edit promotion
  '
  '  PARAMS:
  '     - INPUT:
  '           - Enable:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EnableButtons(ByVal Enable As Boolean)

    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Enable
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Enabled = Enable
    gb_account.Enabled = Enable

  End Sub

  ' PURPOSE: Check if contain an anonymous account
  '
  '  PARAMS:
  '     - INPUT:
  '           - Dt:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Function ContainAccountAnonymous(ByVal Dt As DataTable) As Boolean

    Try

      For Each _dr As DataRow In Dt.Rows

        If _dr(2) = 0 Then ' column holder level
          Return True
        End If

      Next

      Return False

    Catch ex As Exception

    End Try

    Return False

  End Function

  Private Sub InitAwardWithGameCombobox()
    Dim _promo As CLASS_PROMOTION
    Dim _game_type_list As List(Of PromoGame.GameType)

    _game_type_list = New List(Of PromoGame.GameType)

    _promo = DbReadObject
    _game_type_list.Add(PromoGame.GameType.PlayReward)

    cmb_award_with_game.DataSource = New PromoGame().GetByGameTypeListAndPromotionId(_game_type_list, _promo.PromotionId)
    cmb_award_with_game.DisplayMember = "Name"
    cmb_award_with_game.ValueMember = "Id"
  End Sub

#End Region

#Region " Event "

  Private Sub btn_import_ClickEvent() Handles btn_import.ClickEvent

    Dim _filename As String
    Dim _open_file As System.Windows.Forms.OpenFileDialog
    Dim _report_mode As WSI.Common.AccountsImport.REPORT_MODE

    _open_file = New System.Windows.Forms.OpenFileDialog

    If Not IsNothing(m_dt_acc) Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1245), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Return
      End If
    End If

    If Not String.IsNullOrEmpty(m_current_directory) Then
      _open_file.InitialDirectory = m_current_directory
    End If

    _open_file.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1241)
    _open_file.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1242)
    If _open_file.ShowDialog() <> Windows.Forms.DialogResult.OK Then

      Return
    End If

    _filename = _open_file.FileName
    m_current_directory = Path.GetDirectoryName(_filename)

    m_progress = New frm_excel_progress()
    m_progress.Show()

    Cursor = Cursors.WaitCursor
    Call EnableButtons(False)

    m_dt_acc = Nothing
    m_dt_error = Nothing
    If Not WSI.Common.AccountsImport.ImportForPreassignedPromotion(_filename, m_dt_acc, m_dt_error, AddressOf UpdateProgressBar) Then
      _report_mode = AccountsImport.REPORT_MODE.ONLY_ERRORS
    End If
    If opt_account_summary.Checked Then
      _report_mode = AccountsImport.REPORT_MODE.SUMMARY
    ElseIf opt_all_account.Checked Then
      _report_mode = AccountsImport.REPORT_MODE.DETAILS
    End If

    Call EnableButtons(True)
    Cursor = Cursors.Default

    m_progress.Close()
    m_progress.Dispose()
    m_progress = Nothing

    If m_dt_acc Is Nothing Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1268), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
    ElseIf m_dt_acc.Rows.Count = 0 Then
      m_dt_acc = Nothing
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1268), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Call ShowReport(m_dt_acc, m_dt_error, _report_mode)
    End If

  End Sub

  ' PURPOSE: Generic event
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub opt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _report_mode As WSI.Common.AccountsImport.REPORT_MODE

    If Not CType(sender, RadioButton).Checked Then Exit Sub

    If IsNothing(m_dt_acc) Then Exit Sub

    If opt_account_summary.Checked Then
      _report_mode = AccountsImport.REPORT_MODE.SUMMARY
    ElseIf opt_all_account.Checked Then
      _report_mode = AccountsImport.REPORT_MODE.DETAILS
    End If

    Call ShowReport(m_dt_acc, m_dt_error, _report_mode)

  End Sub

  Private Sub chk_visible_on_PromoBOX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_visible_on_PromoBOX.CheckedChanged
    img_image.Enabled = chk_visible_on_PromoBOX.Checked
  End Sub

  ' PURPOSE: Event on change checked state on AwardOnPromoBox
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub chk_award_on_PromoBOX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_award_on_PromoBOX.CheckedChanged, chk_award_on_InTouch.CheckedChanged
    chk_visible_on_PromoBOX.Enabled = chk_award_on_PromoBOX.Checked
    If Not chk_award_on_PromoBOX.Checked Then
      chk_visible_on_PromoBOX.Checked = False
    End If
    img_icon.Enabled = chk_award_on_PromoBOX.Checked Or chk_award_on_InTouch.Checked
    Call UpdatePromoBOXTabState()
  End Sub

  ' PURPOSE: Event to control the TextOnPromoBOX field value and update the PromoBOX tab state
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub ef_text_on_PromoBOX_EntryFieldValueChanged() Handles ef_text_on_PromoBOX.EntryFieldValueChanged
    Call UpdatePromoBOXTabState()
  End Sub

  Private Sub chk_award_with_game_CheckedChanged(sender As Object, e As EventArgs) Handles chk_award_with_game.CheckedChanged
    Me.cmb_award_with_game.Enabled = Me.chk_award_with_game.Checked

    If Not Me.chk_award_with_game.Checked Then
      Me.cmb_award_with_game.SelectedIndex = -1
    End If

  End Sub

#End Region

Private Sub frm_promotion_preassigned_edit_Load(sender As Object, e As EventArgs) Handles Me.Load
    chk_visible_on_PromoBOX.Enabled = Me.chk_award_on_PromoBOX.Checked
    If Not chk_award_on_PromoBOX.Checked Then
        chk_visible_on_PromoBOX.Enabled = False
    End If
    img_icon.Enabled = chk_award_on_PromoBOX.Checked Or chk_award_on_InTouch.Checked
End Sub
End Class