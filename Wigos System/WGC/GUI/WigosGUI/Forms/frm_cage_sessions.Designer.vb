<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_sessions
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cage_sessions))
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.opt_opening_user = New System.Windows.Forms.RadioButton()
    Me.opt_closing_user = New System.Windows.Forms.RadioButton()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.fra_session_state = New System.Windows.Forms.GroupBox()
    Me.chk_session_closed = New System.Windows.Forms.CheckBox()
    Me.chk_session_open = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_working_day = New System.Windows.Forms.RadioButton()
    Me.opt_opening_date = New System.Windows.Forms.RadioButton()
    Me.opt_closing_date = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.fra_session_state.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.fra_session_state)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1168, 147)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_session_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 151)
    Me.panel_data.Size = New System.Drawing.Size(1168, 542)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1162, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1162, 4)
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.GroupBox1)
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(266, 6)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(272, 134)
    Me.gb_user.TabIndex = 2
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.opt_opening_user)
    Me.GroupBox1.Controls.Add(Me.opt_closing_user)
    Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.GroupBox1.Location = New System.Drawing.Point(6, 12)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(254, 60)
    Me.GroupBox1.TabIndex = 0
    Me.GroupBox1.TabStop = False
    '
    'opt_opening_user
    '
    Me.opt_opening_user.Location = New System.Drawing.Point(6, 10)
    Me.opt_opening_user.Name = "opt_opening_user"
    Me.opt_opening_user.Size = New System.Drawing.Size(210, 24)
    Me.opt_opening_user.TabIndex = 0
    Me.opt_opening_user.Text = "xSearchByOpeningUser"
    '
    'opt_closing_user
    '
    Me.opt_closing_user.Location = New System.Drawing.Point(6, 33)
    Me.opt_closing_user.Name = "opt_closing_user"
    Me.opt_closing_user.Size = New System.Drawing.Size(210, 24)
    Me.opt_closing_user.TabIndex = 1
    Me.opt_closing_user.Text = "xSearchByClosingUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(81, 108)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 4
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(6, 80)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 1
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(6, 104)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 80)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 3
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'fra_session_state
    '
    Me.fra_session_state.Controls.Add(Me.chk_session_closed)
    Me.fra_session_state.Controls.Add(Me.chk_session_open)
    Me.fra_session_state.Location = New System.Drawing.Point(544, 6)
    Me.fra_session_state.Name = "fra_session_state"
    Me.fra_session_state.Size = New System.Drawing.Size(128, 72)
    Me.fra_session_state.TabIndex = 3
    Me.fra_session_state.TabStop = False
    Me.fra_session_state.Text = "xState"
    '
    'chk_session_closed
    '
    Me.chk_session_closed.Location = New System.Drawing.Point(6, 41)
    Me.chk_session_closed.Name = "chk_session_closed"
    Me.chk_session_closed.Size = New System.Drawing.Size(116, 16)
    Me.chk_session_closed.TabIndex = 1
    Me.chk_session_closed.Text = "xClosed"
    '
    'chk_session_open
    '
    Me.chk_session_open.Location = New System.Drawing.Point(6, 19)
    Me.chk_session_open.Name = "chk_session_open"
    Me.chk_session_open.Size = New System.Drawing.Size(116, 16)
    Me.chk_session_open.TabIndex = 0
    Me.chk_session_open.Text = "xOpen"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_working_day)
    Me.gb_date.Controls.Add(Me.opt_opening_date)
    Me.gb_date.Controls.Add(Me.opt_closing_date)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(254, 134)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_working_day
    '
    Me.opt_working_day.Location = New System.Drawing.Point(6, 54)
    Me.opt_working_day.Name = "opt_working_day"
    Me.opt_working_day.Size = New System.Drawing.Size(210, 24)
    Me.opt_working_day.TabIndex = 2
    Me.opt_working_day.Text = "xSearchByWorkingDay"
    '
    'opt_opening_date
    '
    Me.opt_opening_date.Location = New System.Drawing.Point(6, 14)
    Me.opt_opening_date.Name = "opt_opening_date"
    Me.opt_opening_date.Size = New System.Drawing.Size(210, 24)
    Me.opt_opening_date.TabIndex = 0
    Me.opt_opening_date.Text = "xSearchByOpeningDate"
    '
    'opt_closing_date
    '
    Me.opt_closing_date.Location = New System.Drawing.Point(6, 34)
    Me.opt_closing_date.Name = "opt_closing_date"
    Me.opt_closing_date.Size = New System.Drawing.Size(210, 24)
    Me.opt_closing_date.TabIndex = 1
    Me.opt_closing_date.Text = "xSearchByClosingDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 104)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 4
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 80)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 3
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_cage_sessions
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1178, 697)
    Me.Name = "frm_cage_sessions"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.fra_session_state.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents fra_session_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_session_closed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_session_open As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_opening_date As System.Windows.Forms.RadioButton
  Friend WithEvents opt_closing_date As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents opt_closing_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_opening_user As System.Windows.Forms.RadioButton
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents opt_working_day As System.Windows.Forms.RadioButton
End Class
