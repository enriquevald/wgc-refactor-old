'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_sites_operators_sel.vb
' DESCRIPTION:   Sites and Operators selection form
' AUTHOR:        Agust� Poch
' CREATION DATE: 03-FEB-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-FEB-2011  APB    Initial version.
'
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On 

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls

#End Region ' Imports

Public Class frm_sites_operators_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_results As System.Windows.Forms.GroupBox
  Friend WithEvents chk_site As System.Windows.Forms.CheckBox
  Friend WithEvents chk_operator As System.Windows.Forms.CheckBox
  Friend WithEvents ef_operator As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_color_site As System.Windows.Forms.Label
  Friend WithEvents lbl_color_operator As System.Windows.Forms.Label
  Friend WithEvents ef_site As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_results = New System.Windows.Forms.GroupBox
    Me.lbl_color_site = New System.Windows.Forms.Label
    Me.lbl_color_operator = New System.Windows.Forms.Label
    Me.chk_site = New System.Windows.Forms.CheckBox
    Me.chk_operator = New System.Windows.Forms.CheckBox
    Me.ef_operator = New GUI_Controls.uc_entry_field
    Me.ef_site = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_results.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_site)
    Me.panel_filter.Controls.Add(Me.ef_operator)
    Me.panel_filter.Controls.Add(Me.gb_results)
    Me.panel_filter.Size = New System.Drawing.Size(626, 84)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_results, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_operator, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_site, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 88)
    Me.panel_data.Size = New System.Drawing.Size(626, 345)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(620, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(620, 4)
    '
    'gb_results
    '
    Me.gb_results.Controls.Add(Me.lbl_color_site)
    Me.gb_results.Controls.Add(Me.lbl_color_operator)
    Me.gb_results.Controls.Add(Me.chk_site)
    Me.gb_results.Controls.Add(Me.chk_operator)
    Me.gb_results.Location = New System.Drawing.Point(275, 10)
    Me.gb_results.Name = "gb_results"
    Me.gb_results.Size = New System.Drawing.Size(146, 72)
    Me.gb_results.TabIndex = 3
    Me.gb_results.TabStop = False
    Me.gb_results.Text = "xResultados"
    '
    'lbl_color_site
    '
    Me.lbl_color_site.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_color_site.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_site.Location = New System.Drawing.Point(120, 44)
    Me.lbl_color_site.Name = "lbl_color_site"
    Me.lbl_color_site.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_site.TabIndex = 106
    '
    'lbl_color_operator
    '
    Me.lbl_color_operator.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.lbl_color_operator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_operator.Location = New System.Drawing.Point(120, 22)
    Me.lbl_color_operator.Name = "lbl_color_operator"
    Me.lbl_color_operator.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_operator.TabIndex = 105
    '
    'chk_site
    '
    Me.chk_site.Location = New System.Drawing.Point(13, 45)
    Me.chk_site.Name = "chk_site"
    Me.chk_site.Size = New System.Drawing.Size(99, 16)
    Me.chk_site.TabIndex = 1
    Me.chk_site.Text = "xSala"
    '
    'chk_operator
    '
    Me.chk_operator.Location = New System.Drawing.Point(13, 23)
    Me.chk_operator.Name = "chk_operator"
    Me.chk_operator.Size = New System.Drawing.Size(99, 16)
    Me.chk_operator.TabIndex = 0
    Me.chk_operator.Text = "xOperador"
    '
    'ef_operator
    '
    Me.ef_operator.DoubleValue = 0
    Me.ef_operator.IntegerValue = 0
    Me.ef_operator.IsReadOnly = False
    Me.ef_operator.Location = New System.Drawing.Point(8, 15)
    Me.ef_operator.Name = "ef_operator"
    Me.ef_operator.Size = New System.Drawing.Size(250, 24)
    Me.ef_operator.SufixText = "Sufix Text"
    Me.ef_operator.SufixTextVisible = True
    Me.ef_operator.TabIndex = 1
    Me.ef_operator.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_operator.TextValue = ""
    Me.ef_operator.TextWidth = 60
    Me.ef_operator.Value = ""
    '
    'ef_site
    '
    Me.ef_site.DoubleValue = 0
    Me.ef_site.IntegerValue = 0
    Me.ef_site.IsReadOnly = False
    Me.ef_site.Location = New System.Drawing.Point(8, 47)
    Me.ef_site.Name = "ef_site"
    Me.ef_site.Size = New System.Drawing.Size(250, 24)
    Me.ef_site.SufixText = "Sufix Text"
    Me.ef_site.SufixTextVisible = True
    Me.ef_site.TabIndex = 2
    Me.ef_site.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site.TextValue = ""
    Me.ef_site.TextWidth = 60
    Me.ef_site.Value = ""
    '
    'frm_sites_operators_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(634, 437)
    Me.Name = "frm_sites_operators_sel"
    Me.ShowInTaskbar = False
    Me.Text = "xfrm_sites_operators_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_results.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const LEN_OPERATOR_NAME As Integer = 30
  Private Const LEN_SITE_NAME As Integer = 30

  ' Row type
  Private Const ROW_TYPE_OPERATOR As Integer = 0
  Private Const ROW_TYPE_SITE As Integer = 1

  Private Const SQL_COLUMN_ROW_TYPE As Integer = 0
  Private Const SQL_COLUMN_OPERATOR_ID As Integer = 1
  Private Const SQL_COLUMN_OPERATOR_NAME As Integer = 2
  Private Const SQL_COLUMN_SITE_ID As Integer = 3
  Private Const SQL_COLUMN_SITE_NAME As Integer = 4

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ROW_TYPE As Integer = 1
  Private Const GRID_COLUMN_OPERATOR_ID As Integer = 2
  Private Const GRID_COLUMN_OPERATOR_NAME As Integer = 3
  Private Const GRID_COLUMN_SITE_ID As Integer = 4
  Private Const GRID_COLUMN_SITE_NAME As Integer = 5
  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Result type
  Public Enum ENUM_RESULT_TYPE
    RT_OPERATOR = 0
    RT_SITE = 1
  End Enum

#End Region

#Region " Members "

  ' Input parameters
  Private ResultType As ENUM_RESULT_TYPE

  ' For returning items selected in selection mode
  Private SitesSelected() As Integer

  ' For report filters 
  Private m_operator_name As String
  Private m_site_name As String
  Private m_results As String

#End Region ' Members

#Region " Properties "

#End Region

#Region " Overridable GUI functions "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITES_OPERATORS_SEL
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Buttons control. Replace New button by 2 custom buttons
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION Then
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    End If

    ' Fix and disable result type when in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      If ResultType = ENUM_RESULT_TYPE.RT_OPERATOR Then
        chk_operator.Checked = True
        chk_site.Enabled = False
      End If
      If ResultType = ENUM_RESULT_TYPE.RT_SITE Then
        chk_site.Checked = True
        chk_operator.Enabled = False
      End If
      'Else
      '  chk_site.Checked = True
    End If

    ' Assign NLS id's
    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(258)
    gb_results.Text = GLB_NLS_GUI_CONFIGURATION.GetString(379)
    chk_operator.Text = GLB_NLS_GUI_CONFIGURATION.GetString(381)
    chk_site.Text = GLB_NLS_GUI_CONFIGURATION.GetString(380)
    ef_operator.Text = GLB_NLS_GUI_CONFIGURATION.GetString(381)
    ef_site.Text = GLB_NLS_GUI_CONFIGURATION.GetString(380)

    ' New Site
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(4)
    ' New Operator
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(5)

    ' Filters
    Call ef_operator.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_OPERATOR_NAME)
    Call ef_site.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SITE_NAME)

    ' Colors
    Me.lbl_color_site.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    Me.lbl_color_operator.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)

    Call GUI_FilterReset()

    Call GUI_StyleView()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

    Dim super_center_flag As String

    ' Protect against access when not running in SuperCenter Mode
    super_center_flag = GetSuperCenterData("Enabled")
    If super_center_flag = "1" Then
      AndPerm.Write = True
      AndPerm.Delete = True
      AndPerm.Read = True
    Else
      AndPerm.Write = False
      AndPerm.Delete = False
      AndPerm.Read = False
    End If

  End Sub ' GUI_Permissions

  Protected Overrides Sub GUI_GetSelectedItems()

    Dim result_column As Integer
    Dim site_id As Integer

    Erase SitesSelected

    SitesSelected = Me.Grid.SelectedRows
    If IsNothing(SitesSelected) Then
      Exit Sub
    End If

    ' When in selection mode, only one type of result is selected
    ' We must get the ID of this data type looking for the correct column.
    If ResultType = ENUM_RESULT_TYPE.RT_OPERATOR Then
      result_column = GRID_COLUMN_OPERATOR_ID
    Else
      result_column = GRID_COLUMN_SITE_ID
    End If

    site_id = Me.Grid.Cell(SitesSelected(0), result_column).Value
    SitesSelected(0) = site_id

  End Sub ' GUI_GetSelectedItems

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim row_type As Integer
    Dim item_id As Integer
    Dim item_name As String
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the selected row's type
    row_type = Me.Grid.Cell(idx_row, GRID_COLUMN_ROW_TYPE).Value

    ' Get the site or operator and launch the proper editor depending on row type
    If row_type = ROW_TYPE_OPERATOR Then
      item_id = Me.Grid.Cell(idx_row, GRID_COLUMN_OPERATOR_ID).Value
      item_name = Me.Grid.Cell(idx_row, GRID_COLUMN_OPERATOR_NAME).Value
      frm_edit = New frm_site_operator_edit
    Else
      item_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SITE_ID).Value
      item_name = Me.Grid.Cell(idx_row, GRID_COLUMN_SITE_NAME).Value
      frm_edit = New frm_site_edit

    End If

    Call frm_edit.ShowEditItem(item_id, item_name)

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Any data is ok in filter fields
    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim filter_operators As String
    Dim filter_sites As String
    Dim query_operators As String
    Dim query_sites As String
    Dim query As String

    query = ""

    ' Query for operators when operator is checked
    ' and when none is checked (If none is checked it works as if both were checked)
    If chk_operator.Checked Or Not chk_site.Checked Then
      query_operators = GetSqlOperators()
      filter_operators = GetFilterOperators()
      If filter_operators.Length > 0 Then
        If InStr(query_operators, "WHERE") = 0 Then
          query = query_operators & " WHERE " & filter_operators
        Else
          query = query_operators & " AND " & filter_operators
        End If
      Else
        query = query_operators
      End If
    End If

    ' If both are checked or if both are unchecked (If only one is checked, no union is required)
    If chk_operator.Checked = chk_site.Checked Then
      query = query & " UNION "
    End If

    ' Query for sites when site is checked
    ' and when none is checked (If none is checked it works as if both were checked)
    If chk_site.Checked Or Not chk_operator.Checked Then
      query_sites = GetSqlSites()
      filter_sites = GetFilterSites()
      query = query & query_sites
      If filter_sites <> "" Then
        query = query & filter_sites
      End If

    End If

    query = query & " ORDER BY   SOP_OPERATOR_ID " & _
                             " , ST_SITE_ID "

    Return query

  End Function ' GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_FilterReset()

    ef_operator.Value = ""
    ef_site.Value = ""

    ' Do not reset result type in edition Mode because it is locked
    If Not ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_operator.Checked = False
      chk_site.Checked = False
    End If

  End Sub ' GUI_FilterReset

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim row_color As System.Drawing.Color

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Set color according to the row type
    If DbRow.Value(SQL_COLUMN_ROW_TYPE) = ROW_TYPE_OPERATOR Then
      row_color = lbl_color_operator.BackColor
    ElseIf DbRow.Value(SQL_COLUMN_ROW_TYPE) = ROW_TYPE_SITE Then
      row_color = lbl_color_site.BackColor
    End If

    ' Set row color
    Me.Grid.Row(RowIndex).BackColor = row_color

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call EditNewSite()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call EditNewOperator()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_operator
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(381), m_operator_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(380), m_site_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(379), m_results)
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_CONFIGURATION.GetString(258)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_operator_name = ""
    m_site_name = ""
    m_results = ""

    ' Operator name
    If Me.ef_operator.Value.Length > 0 Then
      m_operator_name = Me.ef_operator.Value
    End If

    ' Site name
    If Me.ef_site.Value.Length > 0 Then
      m_site_name = Me.ef_site.Value
    End If

    ' Results
    If Me.chk_operator.Checked Then
      m_results = Me.chk_operator.Text
    End If

    If Me.chk_site.Checked Then
      If m_results.Length > 0 Then
        m_results = m_results & ", " & Me.chk_site.Text
      Else
        m_results = Me.chk_site.Text
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overridable GUI functions

#Region " Private Functions "

  ' PURPOSE: Open site edit form to create New Site
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewSite()

    Dim idx_row As Int32
    Dim operator_id As Integer
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next
    frm_edit = New frm_site_edit

    If idx_row = Me.Grid.NumRows Then
      ' If no row is selected create a new site without preselected operator 
      Call frm_edit.ShowNewItem()
    Else
      ' If a row is selected create a new site with the same preselected operator
      operator_id = Me.Grid.Cell(idx_row, GRID_COLUMN_OPERATOR_ID).Value
      Call frm_edit.ShowNewItem(operator_id)
    End If

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' EditNewSite

  ' PURPOSE: Open operator edit form to create New Operator
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewOperator()

    Dim frm_edit As Object

    frm_edit = New frm_site_operator_edit

    Call frm_edit.ShowNewItem()

    frm_edit = Nothing
    Call Grid.Focus()

  End Sub ' EditNewOperator

  ' PURPOSE: Create the filter to query for operators
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with conditions based on dialog items state
  '
  Private Function GetFilterOperators() As String

    Dim filter_string As String

    filter_string = ""

    ' Restrict to operators with names matching the pattern
    If ef_operator.Value <> "" Then
      filter_string = GUI_FilterField("SOP_NAME", ef_operator.Value)
    End If

    ' Restrict to operators that have sites with names matching the pattern 
    If ef_site.Value <> "" Then
      If filter_string.Length > 0 Then
        filter_string = filter_string & " AND "
      End If
      filter_string = filter_string & " SOP_OPERATOR_ID IN ( SELECT ST_OPERATOR_ID " & _
                                                            "FROM SITES " & _
                                                            "WHERE " & GUI_FilterField("ST_NAME", ef_site.Value) & " ) "
    End If

    Return filter_string

  End Function ' GetFilterOperators

  ' PURPOSE: Create the filter to query for sites
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with conditions based on dialog items state
  '
  Private Function GetFilterSites() As String

    Dim filter_string As String

    filter_string = ""

    ' Restrict to sites with names that match the pattern
    If ef_site.Value <> "" Then
      filter_string = filter_string & " AND" & GUI_FilterField("ST_NAME", ef_site.Value)
    End If

    ' Restrict to sites with operators matching the pattern
    If ef_operator.Value <> "" Then
      filter_string = filter_string & " AND" & GUI_FilterField("SOP_NAME", ef_operator.Value)
    End If

    Return filter_string

  End Function ' GetFilterSites

  ' PURPOSE: Create query statement without filter to get site operators
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with query statment
  '
  ' NOTES:
  '     - The number of fields and its type must be equal
  '       than these on the the query for sites because 
  '       we want to make a UNION betwen these two queries
  '
  Private Function GetSqlOperators() As String

    Dim str_sql As String

    str_sql = "SELECT   0 as TIPO " & _
                    " , SOP_OPERATOR_ID " & _
                    " , SOP_NAME " & _
                    " , -1 as ST_SITE_ID " & _
                    " , NULL as ST_NAME " & _
                "FROM   SITE_OPERATORS "

    Return str_sql

  End Function ' GetSqlOperators

  ' PURPOSE: Create query statment without filter to get sites
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with query statment
  '
  ' NOTES:
  '     - The number of fields and its type must be equal
  '       than these on the the query for operators because 
  '       we want to make a UNION betwen these two queries
  '
  Private Function GetSqlSites() As String

    Dim str_sql As String

    str_sql = "SELECT   1 as TIPO " & _
                    " , SOP_OPERATOR_ID " & _
                    " , SOP_NAME " & _
                    " , ST_SITE_ID " & _
                    " , ST_NAME " & _
                "FROM   SITE_OPERATORS " & _
                    " , SITES " & _
               "WHERE   SOP_OPERATOR_ID = ST_OPERATOR_ID "

    Return str_sql

  End Function ' GetSqlSites

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    ' ROW_TYPE
    Me.Grid.Column(GRID_COLUMN_ROW_TYPE).Width = 0
    Me.Grid.Column(GRID_COLUMN_ROW_TYPE).Mapping = SQL_COLUMN_ROW_TYPE

    ' INDEX
    Me.Grid.Column(GRID_COLUMN_INDEX).Header.Text = " "
    Me.Grid.Column(GRID_COLUMN_INDEX).Width = 200
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    ' SOP_OPERATOR_ID
    Me.Grid.Column(GRID_COLUMN_OPERATOR_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_OPERATOR_ID).Mapping = SQL_COLUMN_OPERATOR_ID

    ' SOP_NAME
    Me.Grid.Column(GRID_COLUMN_OPERATOR_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(381)
    Me.Grid.Column(GRID_COLUMN_OPERATOR_NAME).Width = 3200
    Me.Grid.Column(GRID_COLUMN_OPERATOR_NAME).Mapping = SQL_COLUMN_OPERATOR_NAME
    Me.Grid.Column(GRID_COLUMN_OPERATOR_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_OPERATOR_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' ST_SITE_ID
    Me.Grid.Column(GRID_COLUMN_SITE_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_SITE_ID).Mapping = SQL_COLUMN_SITE_ID

    ' ST_NAME
    Me.Grid.Column(GRID_COLUMN_SITE_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(380)
    Me.Grid.Column(GRID_COLUMN_SITE_NAME).Width = 3200
    Me.Grid.Column(GRID_COLUMN_SITE_NAME).Mapping = SQL_COLUMN_SITE_NAME
    Me.Grid.Column(GRID_COLUMN_SITE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_SITE_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

  End Sub ' GUI_StyleView

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings in select mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelResType: Fix filter to this type of results
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - List of selected Operator Id's or Site Id's 
  Public Function ShowForSelect(ByVal SelResType As ENUM_RESULT_TYPE) As Integer()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    ResultType = SelResType

    Me.Display(True)

    Return SitesSelected

  End Function ' ShowForSelect

  ' PURPOSE: Opens dialog with default settings in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub chk_operator_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_operator.Click

    ' User can't change this check in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_operator.Checked = Not chk_operator.Checked
    End If

  End Sub ' chk_operator_Click

  Private Sub chk_site_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_site.Click

    ' User can't change this check in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_site.Checked = Not chk_site.Checked
    End If

  End Sub ' chk_site_Click

#End Region ' Events

End Class
