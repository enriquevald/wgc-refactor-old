'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_provider_data.vb
'
' DESCRIPTION : Provider data form
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-JUN-2012  JML    Initial version
' 14-JUN-2012  JML    Only redeemable per provider maintenance
' 04-SEP-2012  JML    Change for modal window
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_provider_data
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PROVIDER_ID As Integer = 1
  Private Const GRID_COLUMN_PROVIDER_NAME As Integer = 2
  Private Const GRID_COLUMN_PV_ONLY_REDEEMABLE As Integer = 3

  Private Const GRID_COLUMNS As Integer = 4
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_PROVIDER_NAME As Integer = 2950
  Private Const GRID_WIDTH_PV_ONLY_REDEEMABLE As Integer = 1500

#End Region ' Constants

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROVIDER_DATA

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub               ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(461) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1054)

    Me.lbl_nota.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1108)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Player Tracking
    Call GUI_StyleSheet()

  End Sub         ' GUI_InitControls

  Protected Overrides Sub GUI_GetScreenData()

    Dim provider_config_edit As CLASS_PROVIDER_DATA

    Try
      provider_config_edit = Me.DbEditedObject

      GetScreenProviders(provider_config_edit.DataPerProvider)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    Finally

    End Try

  End Sub        ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim provider_data As CLASS_PROVIDER_DATA

    Try
      provider_data = Me.DbReadObject

      SetScreenProviders(provider_data.DataPerProvider)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub        ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim provider_data As CLASS_PROVIDER_DATA
    ' add here new provider class objects, once per control

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROVIDER_DATA
        provider_data = DbEditedObject
        ' add here new provider data, once per control

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub         ' GUI_DB_Operation

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return True

  End Function  ' GUI_IsScreenDataOk

#End Region   ' Overrides

#Region "Private Functions"

  ' PURPOSE: Define all Grid Points Multiplier Per Provider Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.dg_providers
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Provider Id
      .Column(GRID_COLUMN_PROVIDER_ID).Header.Text = ""
      .Column(GRID_COLUMN_PROVIDER_ID).Width = 0

      ' Provider Name
      .Column(GRID_COLUMN_PROVIDER_NAME).Header.Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER_NAME).Width = GRID_WIDTH_PROVIDER_NAME
      .Column(GRID_COLUMN_PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Provider only redeemable
      .Column(GRID_COLUMN_PV_ONLY_REDEEMABLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1053)
      .Column(GRID_COLUMN_PV_ONLY_REDEEMABLE).Width = GRID_WIDTH_PV_ONLY_REDEEMABLE
      .Column(GRID_COLUMN_PV_ONLY_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_PV_ONLY_REDEEMABLE).IsMerged = False
      .Column(GRID_COLUMN_PV_ONLY_REDEEMABLE).Editable = True
      .Column(GRID_COLUMN_PV_ONLY_REDEEMABLE).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    End With

  End Sub       ' GUI_StyleSheet

  ' PURPOSE: Update the Providers DataTable with the multiplier value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Providers As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenProviders(ByVal Providers As DataTable)
    Dim _providers() As DataRow

    For _idx_row As Integer = 0 To Me.dg_providers.NumRows - 1
      _providers = Providers.Select("PV_ID = " & Me.dg_providers.Cell(_idx_row, GRID_COLUMN_PROVIDER_ID).Value)
      If _providers.Length = 1 Then
        _providers(0)(CLASS_PROVIDER_DATA.SQL_COLUMN_PV_ONLY_REDEEMABLE) = GridValueToBool(Me.dg_providers.Cell(_idx_row, GRID_COLUMN_PV_ONLY_REDEEMABLE).Value)

      End If
    Next
  End Sub   ' GetScreenProviders

  ' PURPOSE: Set the uc_grid from the screen data with the values from the Providers DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Providers As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenProviders(ByVal Providers As DataTable)
    Dim _idx_row As Integer

    dg_providers.Redraw = False
    dg_providers.Clear()
    dg_providers.Redraw = True

    dg_providers.Redraw = False

    Try
      For Each _provider As DataRow In Providers.Rows
        dg_providers.AddRow()
        _idx_row = dg_providers.NumRows - 1

        dg_providers.Cell(_idx_row, GRID_COLUMN_PROVIDER_ID).Value = _provider(CLASS_PROVIDER_DATA.SQL_COLUMN_PROVIDER_ID)
        dg_providers.Cell(_idx_row, GRID_COLUMN_PROVIDER_NAME).Value = _provider(CLASS_PROVIDER_DATA.SQL_COLUMN_PROVIDER_NAME)
        dg_providers.Cell(_idx_row, GRID_COLUMN_PV_ONLY_REDEEMABLE).Value = BoolToGridValue(_provider(CLASS_PROVIDER_DATA.SQL_COLUMN_PV_ONLY_REDEEMABLE))
      Next

    Finally
      dg_providers.Redraw = True
    End Try

  End Sub   ' SetScreenProviders

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function ' BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function ' GridValueToBool

#End Region ' Private Functions

#Region "Public Functions"

#End Region  ' Public Functions

#Region "Events"

#End Region      ' Events

End Class
