'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_categories_sel
' DESCRIPTION:   Categories Selection
' AUTHOR:        Sergio Soria
' CREATION DATE: 22-AUG-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-AUG-2016  SDS    Initial version

'-------------------------------------------------------------------- 
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_games_categories_sel
  Inherits frm_base_sel

#Region " Constants "

  Private Const LEN_CATEGORY_TITLE As Integer = 50

  ' DB Columns
  Private Const SQL_COLUMN_CAT_ID As Integer = 0
  Private Const SQL_COLUMN_TITLE As Integer = 1
  Private Const SQL_COLUMN_DESCRIPTION As Integer = 2
  Private Const SQL_COLUMN_ENABLED As Integer = 3

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CAT_ID As Integer = 1
  Private Const GRID_COLUMN_TITLE As Integer = 2
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 3
  Private Const GRID_COLUMN_ENABLED As Integer = 4

  Private Const GRID_COLUMNS As Integer = 5
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_CAT_ID As Integer = 500
  Private Const GRID_WIDTH_TITLE As Integer = 4600
  Private Const GRID_WIDTH_DESCRIPTION As Integer = 8000
  Private Const GRID_WIDTH_ENABLED As Integer = 1020


  ' Database codes
  Private Const CATEGORY_STATUS_ENABLED As Integer = 1
  Private Const CATEGORY_STATUS_DISABLED As Integer = 0


  Private Const FORM_DB_MIN_VERSION As Short = 204

#End Region ' Constants

#Region " Members "

  ' For report filters
  Private m_category_title As String
  Private m_category_status As String


#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAME_CATEGORIES_SEL


    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7533)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)

    'Category Title
    Me.lbl_category_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)

    ' Category Status Values
    Me.gb_category_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7523)
    Me.chk_status_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008)
    Me.chk_status_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359)

    ' filters
    Me.ef_category_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATEGORY_TITLE)


    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim category_id As Integer
    Dim category_title As String
    Dim frm_edit As Object
    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    category_id = Me.Grid.Cell(idx_row, GRID_COLUMN_CAT_ID).Value
    category_title = Me.Grid.Cell(idx_row, GRID_COLUMN_TITLE).Value

    frm_edit = New frm_games_categories_edit

    Call frm_edit.ShowEditItem(category_id)

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT " & _
              "          GEC_GAME_EXTERNAL_CATEGORIES_ID " & _
              "         ,GEC_TITLE " & _
              "         ,GEC_DESCRIPTION " & _
              "         ,GEC_ACTIVE " & _
              " FROM MAPP_GAMES_EXTERNAL_CATEGORIES "
    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY GEC_GAME_EXTERNAL_CATEGORIES_ID ASC"
    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _cat_status As Integer
    Dim _str_status As String = ""




    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Status
    _cat_status = IIf(DbRow.Value(SQL_COLUMN_ENABLED) = True, 1, 0)

    ' Status names
    Select Case _cat_status
      Case CATEGORY_STATUS_ENABLED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008)

      Case CATEGORY_STATUS_DISABLED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359)

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED).Value = _str_status

    ' Title
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITLE).Value = DbRow.Value(SQL_COLUMN_TITLE)

    ' Description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_DESCRIPTION)

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_category_title
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call GUI_ShowNewCategoryForm()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7523), m_category_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522), m_category_title)

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_category_title = ""
    m_category_status = ""


    ' Filter Category STATUS
    ' STATUS Enabled
    If Me.chk_status_enabled.Checked Then
      m_category_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7534)
    End If

    ' STATUS Disabled
    If Me.chk_status_disabled.Checked Then
      If m_category_status.Length > 0 Then
        m_category_status = m_category_status & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7521)
      Else
        m_category_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7521)
      End If
    End If

   

    ' Category Title
    m_category_title = ef_category_title.Value

  End Sub 'GUI_ReportUpdateFilters

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "



  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX 
      .Column(GRID_COLUMN_INDEX).Header.Text = ""
      .Column(GRID_COLUMN_INDEX).WidthFixed = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' CATEGORY Id
      .Column(GRID_COLUMN_CAT_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7524)
      .Column(GRID_COLUMN_CAT_ID).Width = GRID_COLUMN_CAT_ID
      .Column(GRID_COLUMN_CAT_ID).Mapping = SQL_COLUMN_CAT_ID

      ' CATEGORY Title
      .Column(GRID_COLUMN_TITLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
      .Column(GRID_COLUMN_TITLE).Width = GRID_WIDTH_TITLE
      .Column(GRID_COLUMN_TITLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CATEGORY Description
      .Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7525)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CATEGORY Status
      .Column(GRID_COLUMN_ENABLED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7523)
      .Column(GRID_COLUMN_ENABLED).Width = GRID_WIDTH_ENABLED
      .Column(GRID_COLUMN_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT




    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_status_enabled.Checked = True
    Me.chk_status_disabled.Checked = False

    Me.ef_category_title.Value = ""

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_status As String = ""
    Dim str_type As String = ""
    Dim str_where_account As String = ""
    Dim where_added As Boolean

    where_added = False

    ' Filter Category Title
    If Me.ef_category_title.Value <> "" Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & GUI_FilterField("GEC_TITLE", Me.ef_category_title.Value, False, False, True)
    End If


    ' Filter Category STATUS
    str_status = ""
    ' STATUS Enabled
    If Me.chk_status_enabled.Checked Then
      str_status = str_status & " OR GEC_ACTIVE = 1 "
    End If

    ' STATUS Disabled
    If Me.chk_status_disabled.Checked Then
      str_status = str_status & " OR GEC_ACTIVE = 0 "
    End If

    If str_status.Length > 0 Then
      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If
      str_status = Strings.Right(str_status, Len(str_status) - 4)
      str_where = str_where & " ( " & str_status & " ) "
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new draw to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_ShowNewCategoryForm()

    Dim frm_edit As frm_games_categories_edit

    frm_edit = New frm_games_categories_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_ShowNewCategoriesForm

#End Region ' Private Functions

#Region "Events"

#End Region ' Events

End Class ' frm_categories_sel
