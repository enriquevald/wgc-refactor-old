<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_types_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.chk_no = New System.Windows.Forms.CheckBox
    Me.chk_yes = New System.Windows.Forms.CheckBox
    Me.txt_name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.txt_name)
    Me.panel_filter.Size = New System.Drawing.Size(440, 82)
    Me.panel_filter.Controls.SetChildIndex(Me.txt_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 86)
    Me.panel_data.Size = New System.Drawing.Size(440, 304)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(434, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(434, 4)
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_no)
    Me.gb_enabled.Controls.Add(Me.chk_yes)
    Me.gb_enabled.Location = New System.Drawing.Point(205, 6)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(78, 71)
    Me.gb_enabled.TabIndex = 1
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'chk_no
    '
    Me.chk_no.AutoSize = True
    Me.chk_no.Location = New System.Drawing.Point(11, 47)
    Me.chk_no.Name = "chk_no"
    Me.chk_no.Size = New System.Drawing.Size(48, 17)
    Me.chk_no.TabIndex = 1
    Me.chk_no.Text = "xNo"
    Me.chk_no.UseVisualStyleBackColor = True
    '
    'chk_yes
    '
    Me.chk_yes.AutoSize = True
    Me.chk_yes.Location = New System.Drawing.Point(11, 24)
    Me.chk_yes.Name = "chk_yes"
    Me.chk_yes.Size = New System.Drawing.Size(53, 17)
    Me.chk_yes.TabIndex = 0
    Me.chk_yes.Text = "xYes"
    Me.chk_yes.UseVisualStyleBackColor = True
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(-55, 52)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.Size = New System.Drawing.Size(254, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 0
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 120
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_gaming_tables_types_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(448, 394)
    Me.Name = "frm_gaming_tables_types_sel"
    Me.Text = "frm_gaming_tables_types_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_no As System.Windows.Forms.CheckBox
  Friend WithEvents chk_yes As System.Windows.Forms.CheckBox
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
End Class
