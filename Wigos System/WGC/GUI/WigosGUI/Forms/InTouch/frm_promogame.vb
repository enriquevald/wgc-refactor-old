'-------------------------------------------------------------------
' Copyright � 2017 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promogame
' DESCRIPTION:   New or Edit PromoGame item
' AUTHOR:        Rub�n Lama Ord��ez
' CREATION DATE: 21-JUL-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-JUL-2017  RLO    Initial version.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports WSI.Common.PromoGame

Public Class frm_promogame
  Inherits frm_base_edit

#Region " Constants "
  Private Const LEN_PROMOGAME_NAME As Integer = 50
  Private Const LEN_PROMOGAME_GAMEURL As Integer = 250

#End Region ' Constants

#Region " Members "

  Private m_input_promogame_name As String
  Private m_delete_operation As Boolean

#End Region ' Members

#Region " Overrides "


  ''' <summary>
  ''' Initializes the form id.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOGAME_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Form controls initialization.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8509)                      ' 8509 "Edici�n de Promogame"

    ' Delete button is only available whe editing, not while creating
    'GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False '(Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)

    Me.ef_promogame_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8510)              ' 8510 "Game Name"
    Me.ef_promogame_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMOGAME_NAME)

    Me.lbl_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8511)              ' 8511 Type

    Me.uc_ef_price.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8542)              ' 8512 Price
    Me.uc_ef_price.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    Me.lbl_cost.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8512)              ' 8513 Cost
    Me.lbl_units.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8513)              ' 8514 Units
    Me.chk_return_cost.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8514)               ' 8515 Return Cost
    Me.lbl_Return_Units.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8515)              ' 8516 Return Units
    Me.lbl_return_per.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8516)              ' 8517 Return %
    Me.uc_ef_return.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.uc_ef_gameURL.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8517)              ' 8518 URL
    Me.uc_ef_gameURL.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMOGAME_GAMEURL)
    Me.chk_mandatory_ic.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8518)              ' 8519 The game MUST ...
    Me.chk_show_buy_dialog.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8519)              ' 8520 Show the buy dialog
    Me.chk_show_cancel_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8520)              ' 8521 Show the cancel button
    Me.uc_ef_buy_cancel_timeout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8521)              ' 8522 Buy cancel timeout
    Me.uc_ef_buy_cancel_timeout.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.uc_ef_go_to_transfer_timeout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8522)              ' 8523 go to transfer timeout
    Me.uc_ef_go_to_transfer_timeout.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.chk_transfer_after_play.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8523)              ' 8524 go to transfer after game play


    Call LoadComboType()
    Call LoadComboUnits()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Validate the data presented on the screen.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' The following values must be entered
    '   - Name : Mandator
    '   - Price : Mandator
    '   - Return : Mandator
    '   - URL : Mandator
    '   - BuyCancelDialog : Mandator
    '   - GotoTransferTimeout : Mandator

    If String.IsNullOrEmpty(Me.ef_promogame_name.Value) Then
      ' 123 "Debe introducir un valor."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo Promo Game Name no puede estar vac�o
      Call ef_promogame_name.Focus()
      Return False

    End If

    ' Empty Price
    If String.IsNullOrEmpty(Me.uc_ef_price.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo Price no puede estar vac�o
      Call Me.uc_ef_price.Focus()

      Return False
    End If

    'Return
    If String.IsNullOrEmpty(Me.uc_ef_return.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo REturn no puede estar vac�o
      Call Me.uc_ef_return.Focus()

      Return False
    End If

    'Buy cancel timeout
    If String.IsNullOrEmpty(Me.uc_ef_buy_cancel_timeout.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo Buy cancel timeout no puede estar vac�o
      Call Me.uc_ef_buy_cancel_timeout.Focus()

      Return False
    End If

    'GOto Transfer Timeout
    If String.IsNullOrEmpty(Me.uc_ef_go_to_transfer_timeout.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo Go to Transfer no puede estar vac�o
      Call Me.uc_ef_go_to_transfer_timeout.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' Set initial data on the screen.
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _promogame As CLASS_PROMOGAME
    _promogame = DbReadObject

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = (Me.ScreenMode <> ENUM_SCREEN_MODE.MODE_NEW)

    Me.ef_promogame_name.Value = _promogame.PromoGame.Name
    Me.uc_combo_type.Value = _promogame.PromoGame.Type
    Me.uc_ef_price.Value = _promogame.PromoGame.Price
    Me.uc_combo_units.Value = _promogame.PromoGame.PriceUnits
    Me.chk_return_cost.Checked = _promogame.PromoGame.ReturnPrice
    Me.Uc_combo_return_units.Value = _promogame.PromoGame.ReturnUnits
    Me.uc_ef_return.Value = _promogame.PromoGame.PercentatgeCostReturn
    Me.uc_ef_gameURL.Value = _promogame.PromoGame.GameURL
    Me.chk_mandatory_ic.Checked = _promogame.PromoGame.MandatoryIC
    Me.chk_show_buy_dialog.Checked = _promogame.PromoGame.ShowBuyDialog
    Me.chk_show_cancel_button.Checked = _promogame.PromoGame.PlayerCanCancel
    Me.uc_ef_buy_cancel_timeout.Value = _promogame.PromoGame.AutoCancel
    Me.uc_ef_go_to_transfer_timeout.Value = _promogame.PromoGame.TransferTimeOut
    Me.chk_transfer_after_play.Checked = _promogame.PromoGame.TransferScreen

    If (Me.chk_mandatory_ic.Checked = False) Then
      CheckButtonsGroupDialog()
    End If

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Get data from the screen.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    Dim _promogame As CLASS_PROMOGAME

    _promogame = DbEditedObject

    _promogame.PromoGame.Name = Me.ef_promogame_name.Value
    _promogame.PromoGame.Type = Me.uc_combo_type.Value
    _promogame.PromoGame.Price = Me.uc_ef_price.Value
    _promogame.PromoGame.PriceUnits = Me.uc_combo_units.Value
    _promogame.PromoGame.ReturnPrice = Me.chk_return_cost.Checked
    _promogame.PromoGame.ReturnUnits = Me.Uc_combo_return_units.Value
    _promogame.PromoGame.PercentatgeCostReturn = Me.uc_ef_return.Value
    _promogame.PromoGame.GameURL = Me.uc_ef_gameURL.Value
    _promogame.PromoGame.MandatoryIC = Me.chk_mandatory_ic.Checked
    _promogame.PromoGame.ShowBuyDialog = Me.chk_show_buy_dialog.Checked
    _promogame.PromoGame.PlayerCanCancel = Me.chk_show_cancel_button.Checked
    _promogame.PromoGame.AutoCancel = Me.uc_ef_buy_cancel_timeout.Value
    _promogame.PromoGame.TransferScreen = Me.chk_transfer_after_play.Checked
    _promogame.PromoGame.TransferTimeOut = Me.uc_ef_go_to_transfer_timeout.Value

    _promogame.PromoGame.LastModification = WGDB.Now

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' Database overridable operations. 
  ''' Define specific DB operation for this form.
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _promogame As CLASS_PROMOGAME
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT
    Dim _existPromotionWithPromoGame As Boolean

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROMOGAME
        _promogame = DbEditedObject
        _promogame.PromoGame.Id = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 143 "Se ha producido un error al leer el area %1."
          _promogame = Me.DbEditedObject
          _aux_nls = m_input_promogame_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(143), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 144 "Ya existe una area con el nombre %1."
          _promogame = Me.DbEditedObject
          _aux_nls = m_input_promogame_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(144), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_promogame_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 145 "Se ha producido un error al a�adir el area %1."
          _promogame = Me.DbEditedObject
          _aux_nls = m_input_promogame_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(145), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 144 "Ya existe un area con el nombre %1."
          _promogame = Me.DbEditedObject
          _aux_nls = m_input_promogame_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(144), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_promogame_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 146 "Se ha producido un error al modificar el area %1."
          _promogame = Me.DbEditedObject
          _aux_nls = m_input_promogame_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(146), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE


        _promogame = DbReadObject
        _existPromotionWithPromoGame = CheckPromoGameInPromotion(_promogame)
        _aux_nls = _promogame.PromoGame.Name

        'Exist a promotion with this promogame,  
        If _existPromotionWithPromoGame = True Then

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8742), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        Else

          _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8543), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _aux_nls)

          If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
            m_delete_operation = True
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 147 "Se ha producido un error al borrar el area %1."
          _promogame = Me.DbEditedObject
          _aux_nls = m_input_promogame_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(147), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' Database overridable operations. 
  ''' Define specific DB operation for this form.
  ''' </summary>
  ''' <param name="AndPerm"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ''' <summary>
  ''' Define the control which have the focus when the form is initially shown.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_promogame_name

  End Sub ' GUI_SetInitialFocus


  ''' <summary>
  '''  Init form in new mode
  ''' </summary>
  ''' <param name="AreaId"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowNewItem(ByVal AreaId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    'DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ''' <summary>
  ''' Init form in edit mode
  ''' </summary>
  ''' <param name="AreaId"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowEditItem(ByVal PromoGameId As Integer)

    Dim _table As DataTable
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = PromoGameId

    _table = GUI_GetTableUsingSQL("SELECT PG_NAME FROM PROMOGAMES WHERE PG_ID = " & PromoGameId, Integer.MaxValue)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        Me.m_input_promogame_name = _table.Rows.Item(0).Item("PG_NAME")
      End If
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Private functions "

  ''' <summary>
  ''' Function to load combo Type
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadComboType()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)

    _str_text = String.Empty

    Me.uc_combo_type.Clear()
    For Each item As Object In [Enum].GetValues(GetType(GameType))
      Select Case item
        Case GameType.PlayCash
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8550)
        Case GameType.PlayReward
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8549)
        Case GameType.PlayPoint
          _str_text = String.Empty
        Case Else
          _str_text = String.Empty

      End Select

      If Not String.IsNullOrEmpty(_str_text) Then
        dict.Add((CInt(item)), _str_text)
      End If

    Next

    Me.uc_combo_type.Add(dict)


  End Sub

  ''' <summary>
  ''' Function to load combo Units
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadComboUnits()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)

    _str_text = String.Empty

    Me.uc_combo_units.Clear()
    Me.Uc_combo_return_units.Clear()

    For Each item As Object In [Enum].GetValues(GetType(PrizeType))
      Select Case item

        Case PrizeType.NoRedeemable
          _str_text = GLB_NLS_GUI_INVOICING.GetString(338)
        Case PrizeType.Redeemable
          _str_text = GLB_NLS_GUI_INVOICING.GetString(337)

      End Select

      If Not _str_text.Equals(String.Empty) Then
        dict.Add((CInt(item)), _str_text)
      End If

      _str_text = String.Empty
    Next

    Me.uc_combo_units.Add(dict)
    Me.Uc_combo_return_units.Add(dict)

  End Sub

  Private Function CheckPromoGameInPromotion(Promogame As CLASS_PROMOGAME) As Boolean

    'If exist a promotion with this Promogame
    If (Promogame.PromoGame.CheckPromotionWithPromogame()) Then
      Return True
    End If

    Return False

  End Function

#End Region 'Private functions

#Region " Events "

  ''' <summary>
  ''' checked changed
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_mandatory_ic_CheckedChanged(sender As Object, e As EventArgs) Handles chk_mandatory_ic.CheckedChanged
    CheckButtonsGroupDialog()
  End Sub 'chk_mandatory_ic_CheckedChanged

  ''' <summary>
  ''' Check content from inside groupbox in case than it's disabled
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckButtonsGroupDialog() As Boolean
    If chk_mandatory_ic.Checked = False Then
      chk_show_buy_dialog.Enabled = False
      chk_show_buy_dialog.Checked = False
      chk_show_cancel_button.Enabled = False
      chk_show_cancel_button.Checked = False
    Else
      chk_show_buy_dialog.Enabled = True
      chk_show_cancel_button.Enabled = True
    End If
  End Function 'CheckButtonsGroupDialog

#End Region



End Class