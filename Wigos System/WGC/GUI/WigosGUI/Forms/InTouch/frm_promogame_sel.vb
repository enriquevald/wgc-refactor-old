'-------------------------------------------------------------------
' Copyright � 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promogame_sel
' DESCRIPTION:   This screen allows to view the PromoGames.
' AUTHOR:        Ferran Ortner
' CREATION DATE: 25-JUL-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-JUL-2017  FOS    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports WSI.Common.PromoGame


Public Class frm_promogame_sel
  Inherits frm_base_sel

#Region "Constants"

  Private Const GRID_COLUMNS_COUNT As Integer = 9

  ' DB Columns
  Private Const SQL_COLUMN_PROMO_GAME_ID As Integer = 0
  Private Const SQL_COLUMN_PROMO_GAME_NAME As Integer = 1
  Private Const SQL_COLUMN_PROMO_GAME_TYPE As Integer = 2
  Private Const SQL_COLUMN_PROMO_GAME_PRICE As Integer = 3
  Private Const SQL_COLUMN_PROMO_GAME_UNITS As Integer = 4
  Private Const SQL_COLUMN_PROMO_GAME_RETURN As Integer = 5
  Private Const SQL_COLUMN_PROMO_GAME_PERCENTAGE_RETURN As Integer = 6
  Private Const SQL_COLUMN_PROMO_GAME_RETURN_UNITS As Integer = 7
  Private Const SQL_COLUMN_PROMO_GAME_URL As Integer = 8


  'Grid Columns
  Private Const GRID_COLUMN_PROMO_DEFAULT As Integer = 0
  Private Const GRID_COLUMN_PROMO_GAME_ID As Integer = 1
  Private Const GRID_COLUMN_PROMO_GAME_NAME As Integer = 2
  Private Const GRID_COLUMN_PROMO_GAME_TYPE As Integer = 3
  Private Const GRID_COLUMN_PROMO_GAME_PRICE As Integer = 4
  Private Const GRID_COLUMN_PROMO_GAME_UNITS As Integer = 5
  Private Const GRID_COLUMN_PROMO_GAME_PERCENTAGE_RETURN As Integer = 6
  Private Const GRID_COLUMN_PROMO_GAME_RETURN_UNITS As Integer = 7
  Private Const GRID_COLUMN_PROMO_GAME_URL As Integer = 8

  'Width
  Private Const SQL_WIDTH_PROMO_DEFAULT As Integer = 200
  Private Const SQL_WIDTH_PROMO_GAME_ID As Integer = 0
  Private Const SQL_WIDTH_PROMO_GAME_NAME As Integer = 3600
  Private Const SQL_WIDTH_PROMO_GAME_TYPE As Integer = 1500
  Private Const SQL_WIDTH_PROMO_GAME_PRICE As Integer = 1000
  Private Const SQL_WIDTH_PROMO_GAME_UNITS As Integer = 1600
  Private Const SQL_WIDTH_PROMO_GAME_PERCENTAGE_RETURN As Integer = 1200
  Private Const SQL_WIDTH_PROMO_GAME_RETURN_UNITS As Integer = 1600
  Private Const SQL_WIDTH_PROMO_GAME_URL As Integer = 4300

#End Region

#Region " Overrides "

  ''' <summary>
  ''' Initializes the form id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOGAME

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Build a SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns>SQL query text ready to send to the database</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.Append(" SELECT   PG_ID                       ")
    _sb.Append("        , PG_NAME                     ")
    _sb.Append("        , PG_TYPE                     ")
    _sb.Append("        , PG_PRICE                    ")
    _sb.Append("        , PG_PRICE_UNITS              ")
    _sb.Append("        , PG_RETURN_PRICE             ")
    _sb.Append("        , PG_PERCENTATGE_COST_RETURN  ")
    _sb.Append("        , PG_RETURN_UNITS             ")
    _sb.Append("        , PG_GAME_URL                 ")
    _sb.Append("   FROM   PROMOGAMES                  ")
    _sb.AppendLine(GetSqlWhere())

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Initialize all form filters with their default values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call LoadComboType()

  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8541)

    Call GUI_StyleSheet()

    Call LoadComboType()

    Me.uc_combo_types.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8511)

  End Sub ' GUI_InitControls

  ''' <summary>
  '''  Fills the rows
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns> True (the row should be added) or False (the row can not be added)</returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                        ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_ID).Value = DbRow.Value(SQL_COLUMN_PROMO_GAME_ID)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_PROMO_GAME_NAME)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_TYPE).Value = TypeNameValue(DbRow.Value(SQL_COLUMN_PROMO_GAME_TYPE))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_PRICE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROMO_GAME_PRICE))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_UNITS).Value = UnitsNameValue(DbRow.Value(SQL_COLUMN_PROMO_GAME_UNITS))

    If DbRow.Value(SQL_COLUMN_PROMO_GAME_RETURN) = True Then

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_PERCENTAGE_RETURN).Value = DbRow.Value(SQL_COLUMN_PROMO_GAME_PERCENTAGE_RETURN)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_RETURN_UNITS).Value = UnitsNameValue(DbRow.Value(SQL_COLUMN_PROMO_GAME_RETURN_UNITS))
    Else

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_PERCENTAGE_RETURN).Value = String.Empty

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_RETURN_UNITS).Value = String.Empty
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_GAME_URL).Value = DbRow.Value(SQL_COLUMN_PROMO_GAME_URL)

    Return True
  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Process button actions
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call GUI_NewPromoGame()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Activated when a row of the grid is double clicked or selected, so it can be edited
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()
    Dim _idx_row As Short
    Dim _promo_game_id As Integer
    Dim _frm_edit As frm_promogame

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion game ID 
    _promo_game_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_PROMO_GAME_ID).Value
    _frm_edit = New frm_promogame()

    Call _frm_edit.ShowSelectedItem(_promo_game_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()
  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  '''  Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8511), TypeNameValue(Me.uc_combo_types.Value))

  End Sub ' GUI_ReportFilter
#End Region

#Region "Public Functions"

  ''' <summary>
  ''' Opens dialog with default settings for edit mode
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Private Functions"
  ''' <summary>
  ''' Define layout of all Main Grid Columns 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT)

      ' DEFAULT
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_DEFAULT, String.Empty, SQL_WIDTH_PROMO_DEFAULT, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' ID
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_ID, String.Empty, SQL_WIDTH_PROMO_GAME_ID, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' NAME
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(253), SQL_WIDTH_PROMO_GAME_NAME, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT)

      ' TYPE
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_TYPE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8511), SQL_WIDTH_PROMO_GAME_TYPE, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)

      ' PRICE
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_PRICE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8542), SQL_WIDTH_PROMO_GAME_PRICE, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' UNITS
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_UNITS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8695), SQL_WIDTH_PROMO_GAME_UNITS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)


      ' PERCENTAGE RETURN
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_PERCENTAGE_RETURN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8516), SQL_WIDTH_PROMO_GAME_PERCENTAGE_RETURN, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT)

      ' UNITS
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_RETURN_UNITS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8696), SQL_WIDTH_PROMO_GAME_RETURN_UNITS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)

      ' URL
      GUI_AddColumn(Me.Grid, GRID_COLUMN_PROMO_GAME_URL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8517), SQL_WIDTH_PROMO_GAME_URL, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT)

    End With
  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Create a new Promotion Game
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_NewPromoGame()
    Dim _frm_edit As frm_promogame

    _frm_edit = New frm_promogame()

    Call _frm_edit.ShowNewItem()

  End Sub ' GUI_NewPromoGame

  ''' <summary>
  ''' Get the sql 'where' statement
  ''' </summary>
  ''' <returns>a string with the 'where' statement</returns>
  ''' <remarks></remarks>
  Private Function GetSqlWhere() As String
    Dim _sb_where As StringBuilder

    _sb_where = New StringBuilder()

    _sb_where.Append(" WHERE PG_TYPE > 0 ")
    If Not Me.uc_combo_types.Value = GameType.All Then

      _sb_where.Append("  AND  PG_TYPE = ")
      _sb_where.Append(Me.uc_combo_types.Value)
    End If

    Return _sb_where.ToString()
  End Function ' GetSqlWhere

  ''' <summary>
  ''' Function that return Type string value
  ''' </summary>
  ''' <param name="TypeFromDB"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function TypeNameValue(TypeFromDB As Object) As String
    Dim _str_text As String

    _str_text = String.Empty

    Select Case TypeFromDB
      Case GameType.All
        _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
      Case GameType.PlayCash
        _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8550)
      Case GameType.PlayReward
        _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8549)
      Case GameType.PlayPoint
        _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8679)

    End Select

    Return _str_text
  End Function ' TypeNameValue

  ''' <summary>
  ''' Function that returns the Units String value
  ''' </summary>
  ''' <param name="UnitsFromDB"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function UnitsNameValue(UnitsFromDB As Object) As String
    Dim _str_text As String

    _str_text = String.Empty

    Select Case UnitsFromDB
      Case PrizeType.NoRedeemable
        _str_text = GLB_NLS_GUI_INVOICING.GetString(338)
      Case PrizeType.Redeemable
        _str_text = GLB_NLS_GUI_INVOICING.GetString(337)

    End Select

    Return _str_text
  End Function ' UnitsNameValue

  ''' <summary>
  ''' Function that sets the combo types values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadComboType()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)

    _str_text = String.Empty

    Me.uc_combo_types.Clear()
    For Each item As Object In [Enum].GetValues(GetType(GameType))
      Select Case item
        Case GameType.All
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
        Case GameType.PlayCash
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8550)
        Case GameType.PlayReward
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8549)
        Case GameType.PlayPoint
          _str_text = String.Empty
        Case Else
          _str_text = String.Empty

      End Select

      If Not String.IsNullOrEmpty(_str_text) Then
        dict.Add((CInt(item)), _str_text)
      End If

    Next

    Me.uc_combo_types.Add(dict)
    Me.uc_combo_types.Value = GameType.All

  End Sub ' LoadComboType

#End Region

End Class