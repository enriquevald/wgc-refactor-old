﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_promogame_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_promogame_sel))
    Me.uc_combo_types = New GUI_Controls.uc_combo()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_combo_types)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1188, 65)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_combo_types, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 69)
    Me.panel_data.Size = New System.Drawing.Size(1188, 477)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1182, 23)
    Me.pn_separator_line.TabIndex = 2
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1182, 4)
    '
    'uc_combo_types
    '
    Me.uc_combo_types.AllowUnlistedValues = False
    Me.uc_combo_types.AutoCompleteMode = False
    Me.uc_combo_types.IsReadOnly = False
    Me.uc_combo_types.Location = New System.Drawing.Point(3, 35)
    Me.uc_combo_types.Name = "uc_combo_types"
    Me.uc_combo_types.SelectedIndex = -1
    Me.uc_combo_types.Size = New System.Drawing.Size(223, 24)
    Me.uc_combo_types.SufixText = "Sufix Text"
    Me.uc_combo_types.SufixTextVisible = True
    Me.uc_combo_types.TabIndex = 0
    Me.uc_combo_types.TextCombo = Nothing
    '
    'frm_promogame_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1198, 550)
    Me.MinimumSize = New System.Drawing.Size(1214, 588)
    Me.Name = "frm_promogame_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_promogame_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_combo_types As GUI_Controls.uc_combo
End Class
