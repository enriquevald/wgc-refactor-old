<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_promogame
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_promogame))
    Me.ef_promogame_name = New GUI_Controls.uc_entry_field()
    Me.chk_return_cost = New System.Windows.Forms.CheckBox()
    Me.uc_ef_price = New GUI_Controls.uc_entry_field()
    Me.uc_ef_return = New GUI_Controls.uc_entry_field()
    Me.uc_ef_gameURL = New GUI_Controls.uc_entry_field()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.chk_show_cancel_button = New System.Windows.Forms.CheckBox()
    Me.chk_show_buy_dialog = New System.Windows.Forms.CheckBox()
    Me.chk_mandatory_ic = New System.Windows.Forms.CheckBox()
    Me.uc_ef_buy_cancel_timeout = New GUI_Controls.uc_entry_field()
    Me.uc_ef_go_to_transfer_timeout = New GUI_Controls.uc_entry_field()
    Me.chk_transfer_after_play = New System.Windows.Forms.CheckBox()
    Me.lbl_cost = New System.Windows.Forms.Label()
    Me.lbl_units = New System.Windows.Forms.Label()
    Me.lbl_Return_Units = New System.Windows.Forms.Label()
    Me.lbl_return_per = New System.Windows.Forms.Label()
    Me.uc_combo_units = New GUI_Controls.uc_combo()
    Me.Uc_combo_return_units = New GUI_Controls.uc_combo()
    Me.uc_combo_type = New GUI_Controls.uc_combo()
    Me.lbl_type = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.uc_combo_type)
    Me.panel_data.Controls.Add(Me.Uc_combo_return_units)
    Me.panel_data.Controls.Add(Me.uc_combo_units)
    Me.panel_data.Controls.Add(Me.lbl_return_per)
    Me.panel_data.Controls.Add(Me.lbl_type)
    Me.panel_data.Controls.Add(Me.lbl_Return_Units)
    Me.panel_data.Controls.Add(Me.lbl_units)
    Me.panel_data.Controls.Add(Me.lbl_cost)
    Me.panel_data.Controls.Add(Me.chk_transfer_after_play)
    Me.panel_data.Controls.Add(Me.uc_ef_go_to_transfer_timeout)
    Me.panel_data.Controls.Add(Me.uc_ef_buy_cancel_timeout)
    Me.panel_data.Controls.Add(Me.GroupBox1)
    Me.panel_data.Controls.Add(Me.uc_ef_return)
    Me.panel_data.Controls.Add(Me.uc_ef_price)
    Me.panel_data.Controls.Add(Me.chk_return_cost)
    Me.panel_data.Controls.Add(Me.uc_ef_gameURL)
    Me.panel_data.Controls.Add(Me.ef_promogame_name)
    Me.panel_data.Size = New System.Drawing.Size(707, 354)
    '
    'ef_promogame_name
    '
    Me.ef_promogame_name.DoubleValue = 0.0R
    Me.ef_promogame_name.IntegerValue = 0
    Me.ef_promogame_name.IsReadOnly = False
    Me.ef_promogame_name.Location = New System.Drawing.Point(3, 19)
    Me.ef_promogame_name.Name = "ef_promogame_name"
    Me.ef_promogame_name.PlaceHolder = Nothing
    Me.ef_promogame_name.Size = New System.Drawing.Size(494, 24)
    Me.ef_promogame_name.SufixText = "Sufix Text"
    Me.ef_promogame_name.SufixTextVisible = True
    Me.ef_promogame_name.TabIndex = 0
    Me.ef_promogame_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promogame_name.TextValue = ""
    Me.ef_promogame_name.TextWidth = 90
    Me.ef_promogame_name.Value = ""
    Me.ef_promogame_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_return_cost
    '
    Me.chk_return_cost.Location = New System.Drawing.Point(320, 72)
    Me.chk_return_cost.Name = "chk_return_cost"
    Me.chk_return_cost.Size = New System.Drawing.Size(109, 16)
    Me.chk_return_cost.TabIndex = 4
    Me.chk_return_cost.Text = "xReturnCost"
    '
    'uc_ef_price
    '
    Me.uc_ef_price.DoubleValue = 0.0R
    Me.uc_ef_price.IntegerValue = 0
    Me.uc_ef_price.IsReadOnly = False
    Me.uc_ef_price.Location = New System.Drawing.Point(3, 67)
    Me.uc_ef_price.Name = "uc_ef_price"
    Me.uc_ef_price.PlaceHolder = Nothing
    Me.uc_ef_price.Size = New System.Drawing.Size(162, 24)
    Me.uc_ef_price.SufixText = "Sufix Text"
    Me.uc_ef_price.SufixTextVisible = True
    Me.uc_ef_price.TabIndex = 2
    Me.uc_ef_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_ef_price.TextValue = ""
    Me.uc_ef_price.TextWidth = 90
    Me.uc_ef_price.Value = ""
    Me.uc_ef_price.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_ef_return
    '
    Me.uc_ef_return.DoubleValue = 0.0R
    Me.uc_ef_return.IntegerValue = 0
    Me.uc_ef_return.IsReadOnly = False
    Me.uc_ef_return.Location = New System.Drawing.Point(604, 67)
    Me.uc_ef_return.Name = "uc_ef_return"
    Me.uc_ef_return.PlaceHolder = Nothing
    Me.uc_ef_return.Size = New System.Drawing.Size(99, 24)
    Me.uc_ef_return.SufixText = "Sufix Text"
    Me.uc_ef_return.SufixTextVisible = True
    Me.uc_ef_return.TabIndex = 6
    Me.uc_ef_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_ef_return.TextValue = ""
    Me.uc_ef_return.TextWidth = 0
    Me.uc_ef_return.Value = ""
    Me.uc_ef_return.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_ef_gameURL
    '
    Me.uc_ef_gameURL.DoubleValue = 0.0R
    Me.uc_ef_gameURL.IntegerValue = 0
    Me.uc_ef_gameURL.IsReadOnly = False
    Me.uc_ef_gameURL.Location = New System.Drawing.Point(3, 108)
    Me.uc_ef_gameURL.Name = "uc_ef_gameURL"
    Me.uc_ef_gameURL.PlaceHolder = Nothing
    Me.uc_ef_gameURL.Size = New System.Drawing.Size(700, 24)
    Me.uc_ef_gameURL.SufixText = "Sufix Text"
    Me.uc_ef_gameURL.SufixTextVisible = True
    Me.uc_ef_gameURL.TabIndex = 7
    Me.uc_ef_gameURL.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_ef_gameURL.TextValue = ""
    Me.uc_ef_gameURL.TextWidth = 90
    Me.uc_ef_gameURL.Value = ""
    Me.uc_ef_gameURL.ValueForeColor = System.Drawing.Color.Blue
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.chk_show_cancel_button)
    Me.GroupBox1.Controls.Add(Me.chk_show_buy_dialog)
    Me.GroupBox1.Controls.Add(Me.chk_mandatory_ic)
    Me.GroupBox1.Location = New System.Drawing.Point(16, 166)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(687, 88)
    Me.GroupBox1.TabIndex = 8
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'chk_show_cancel_button
    '
    Me.chk_show_cancel_button.AutoSize = True
    Me.chk_show_cancel_button.Location = New System.Drawing.Point(24, 55)
    Me.chk_show_cancel_button.Name = "chk_show_cancel_button"
    Me.chk_show_cancel_button.Size = New System.Drawing.Size(140, 17)
    Me.chk_show_cancel_button.TabIndex = 11
    Me.chk_show_cancel_button.Text = "xShowCancelButton"
    '
    'chk_show_buy_dialog
    '
    Me.chk_show_buy_dialog.AutoSize = True
    Me.chk_show_buy_dialog.Location = New System.Drawing.Point(24, 30)
    Me.chk_show_buy_dialog.Name = "chk_show_buy_dialog"
    Me.chk_show_buy_dialog.Size = New System.Drawing.Size(122, 17)
    Me.chk_show_buy_dialog.TabIndex = 10
    Me.chk_show_buy_dialog.Text = "xShowBuyDialog"
    '
    'chk_mandatory_ic
    '
    Me.chk_mandatory_ic.AutoSize = True
    Me.chk_mandatory_ic.Location = New System.Drawing.Point(6, 0)
    Me.chk_mandatory_ic.Name = "chk_mandatory_ic"
    Me.chk_mandatory_ic.Size = New System.Drawing.Size(107, 17)
    Me.chk_mandatory_ic.TabIndex = 9
    Me.chk_mandatory_ic.Text = "xMandatoryIC"
    '
    'uc_ef_buy_cancel_timeout
    '
    Me.uc_ef_buy_cancel_timeout.DoubleValue = 0.0R
    Me.uc_ef_buy_cancel_timeout.IntegerValue = 0
    Me.uc_ef_buy_cancel_timeout.IsReadOnly = False
    Me.uc_ef_buy_cancel_timeout.Location = New System.Drawing.Point(40, 262)
    Me.uc_ef_buy_cancel_timeout.Name = "uc_ef_buy_cancel_timeout"
    Me.uc_ef_buy_cancel_timeout.PlaceHolder = Nothing
    Me.uc_ef_buy_cancel_timeout.Size = New System.Drawing.Size(592, 24)
    Me.uc_ef_buy_cancel_timeout.SufixText = "Sufix Text"
    Me.uc_ef_buy_cancel_timeout.SufixTextVisible = True
    Me.uc_ef_buy_cancel_timeout.TabIndex = 12
    Me.uc_ef_buy_cancel_timeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_ef_buy_cancel_timeout.TextValue = ""
    Me.uc_ef_buy_cancel_timeout.TextWidth = 300
    Me.uc_ef_buy_cancel_timeout.Value = ""
    Me.uc_ef_buy_cancel_timeout.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_ef_go_to_transfer_timeout
    '
    Me.uc_ef_go_to_transfer_timeout.DoubleValue = 0.0R
    Me.uc_ef_go_to_transfer_timeout.IntegerValue = 0
    Me.uc_ef_go_to_transfer_timeout.IsReadOnly = False
    Me.uc_ef_go_to_transfer_timeout.Location = New System.Drawing.Point(40, 291)
    Me.uc_ef_go_to_transfer_timeout.Name = "uc_ef_go_to_transfer_timeout"
    Me.uc_ef_go_to_transfer_timeout.PlaceHolder = Nothing
    Me.uc_ef_go_to_transfer_timeout.Size = New System.Drawing.Size(592, 24)
    Me.uc_ef_go_to_transfer_timeout.SufixText = "Sufix Text"
    Me.uc_ef_go_to_transfer_timeout.SufixTextVisible = True
    Me.uc_ef_go_to_transfer_timeout.TabIndex = 13
    Me.uc_ef_go_to_transfer_timeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_ef_go_to_transfer_timeout.TextValue = ""
    Me.uc_ef_go_to_transfer_timeout.TextWidth = 300
    Me.uc_ef_go_to_transfer_timeout.Value = ""
    Me.uc_ef_go_to_transfer_timeout.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_transfer_after_play
    '
    Me.chk_transfer_after_play.AutoSize = True
    Me.chk_transfer_after_play.Location = New System.Drawing.Point(66, 334)
    Me.chk_transfer_after_play.Name = "chk_transfer_after_play"
    Me.chk_transfer_after_play.Size = New System.Drawing.Size(107, 17)
    Me.chk_transfer_after_play.TabIndex = 14
    Me.chk_transfer_after_play.Text = "xGotoTransfer"
    '
    'lbl_cost
    '
    Me.lbl_cost.AutoSize = True
    Me.lbl_cost.Location = New System.Drawing.Point(98, 51)
    Me.lbl_cost.Name = "lbl_cost"
    Me.lbl_cost.Size = New System.Drawing.Size(40, 13)
    Me.lbl_cost.TabIndex = 10
    Me.lbl_cost.Text = "xCost"
    '
    'lbl_units
    '
    Me.lbl_units.AutoSize = True
    Me.lbl_units.Location = New System.Drawing.Point(177, 51)
    Me.lbl_units.Name = "lbl_units"
    Me.lbl_units.Size = New System.Drawing.Size(42, 13)
    Me.lbl_units.TabIndex = 11
    Me.lbl_units.Text = "xUnits"
    '
    'lbl_Return_Units
    '
    Me.lbl_Return_Units.AutoSize = True
    Me.lbl_Return_Units.Location = New System.Drawing.Point(436, 50)
    Me.lbl_Return_Units.Name = "lbl_Return_Units"
    Me.lbl_Return_Units.Size = New System.Drawing.Size(80, 13)
    Me.lbl_Return_Units.TabIndex = 11
    Me.lbl_Return_Units.Text = "xReturnUnits"
    '
    'lbl_return_per
    '
    Me.lbl_return_per.AutoSize = True
    Me.lbl_return_per.Location = New System.Drawing.Point(608, 51)
    Me.lbl_return_per.Name = "lbl_return_per"
    Me.lbl_return_per.Size = New System.Drawing.Size(64, 13)
    Me.lbl_return_per.TabIndex = 11
    Me.lbl_return_per.Text = "xReturn%"
    '
    'uc_combo_units
    '
    Me.uc_combo_units.AllowUnlistedValues = False
    Me.uc_combo_units.AutoCompleteMode = False
    Me.uc_combo_units.IsReadOnly = False
    Me.uc_combo_units.Location = New System.Drawing.Point(173, 67)
    Me.uc_combo_units.Name = "uc_combo_units"
    Me.uc_combo_units.SelectedIndex = -1
    Me.uc_combo_units.Size = New System.Drawing.Size(141, 24)
    Me.uc_combo_units.SufixText = "Sufix Text"
    Me.uc_combo_units.SufixTextVisible = True
    Me.uc_combo_units.TabIndex = 3
    Me.uc_combo_units.TextCombo = Nothing
    Me.uc_combo_units.TextWidth = 0
    '
    'Uc_combo_return_units
    '
    Me.Uc_combo_return_units.AllowUnlistedValues = False
    Me.Uc_combo_return_units.AutoCompleteMode = False
    Me.Uc_combo_return_units.IsReadOnly = False
    Me.Uc_combo_return_units.Location = New System.Drawing.Point(431, 67)
    Me.Uc_combo_return_units.Name = "Uc_combo_return_units"
    Me.Uc_combo_return_units.SelectedIndex = -1
    Me.Uc_combo_return_units.Size = New System.Drawing.Size(160, 24)
    Me.Uc_combo_return_units.SufixText = "Sufix Text"
    Me.Uc_combo_return_units.SufixTextVisible = True
    Me.Uc_combo_return_units.TabIndex = 5
    Me.Uc_combo_return_units.TextCombo = Nothing
    Me.Uc_combo_return_units.TextWidth = 0
    '
    'uc_combo_type
    '
    Me.uc_combo_type.AllowUnlistedValues = False
    Me.uc_combo_type.AutoCompleteMode = False
    Me.uc_combo_type.IsReadOnly = False
    Me.uc_combo_type.Location = New System.Drawing.Point(593, 19)
    Me.uc_combo_type.Name = "uc_combo_type"
    Me.uc_combo_type.SelectedIndex = -1
    Me.uc_combo_type.Size = New System.Drawing.Size(110, 24)
    Me.uc_combo_type.SufixText = "Sufix Text"
    Me.uc_combo_type.SufixTextVisible = True
    Me.uc_combo_type.TabIndex = 1
    Me.uc_combo_type.TextCombo = Nothing
    Me.uc_combo_type.TextWidth = 0
    '
    'lbl_type
    '
    Me.lbl_type.AutoSize = True
    Me.lbl_type.Location = New System.Drawing.Point(543, 25)
    Me.lbl_type.Name = "lbl_type"
    Me.lbl_type.Size = New System.Drawing.Size(41, 13)
    Me.lbl_type.TabIndex = 11
    Me.lbl_type.Text = "xType"
    '
    'frm_promogame
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.ClientSize = New System.Drawing.Size(809, 363)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_promogame"
    Me.Text = "frm_island_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_promogame_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_return_cost As System.Windows.Forms.CheckBox
  Friend WithEvents uc_ef_price As GUI_Controls.uc_entry_field
  Friend WithEvents chk_transfer_after_play As System.Windows.Forms.CheckBox
  Friend WithEvents uc_ef_go_to_transfer_timeout As GUI_Controls.uc_entry_field
  Friend WithEvents uc_ef_buy_cancel_timeout As GUI_Controls.uc_entry_field
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_cancel_button As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_buy_dialog As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mandatory_ic As System.Windows.Forms.CheckBox
  Friend WithEvents uc_ef_return As GUI_Controls.uc_entry_field
  Friend WithEvents uc_ef_gameURL As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_return_per As System.Windows.Forms.Label
  Friend WithEvents lbl_Return_Units As System.Windows.Forms.Label
  Friend WithEvents lbl_units As System.Windows.Forms.Label
  Friend WithEvents lbl_cost As System.Windows.Forms.Label
  Friend WithEvents uc_combo_units As GUI_Controls.uc_combo
  Friend WithEvents Uc_combo_return_units As GUI_Controls.uc_combo
  Friend WithEvents uc_combo_type As GUI_Controls.uc_combo
  Friend WithEvents lbl_type As System.Windows.Forms.Label
End Class
