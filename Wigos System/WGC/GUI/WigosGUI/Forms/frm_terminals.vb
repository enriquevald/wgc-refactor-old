
'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminals
' DESCRIPTION:   Selection base class
' AUTHOR:        Joshwa Marcalle
' CREATION DATE: 10-JUN-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 10-JUN-2013 JRM    Initial work
' 05-JUL-2013 JCOR   New grid and excel columns.
' 29-JUL-2013 DMR    Fixed bug WIG-87: Modified GUI_ReportUpdateFilters to report all filters
' 09-AUG-2013 JAB    Not call directly to "GUI_PrintResultLis" function, call "MyBase.GUI_ButtonClick(ButtonId".
' 22-AUG-2013 CCG    Fixed Bug WIG-147: When user selects all terminals, the WHERE clause is made by using a list of all terminal ids.
' 11-JUL-2014 XIT    Added Multilanguage Support
' 13-AUG-2014 LEM    Added new columns "MachineId", "Position", "Top Award", "Max Bet", "Number Lines"
'                    Join "Denomination" and "Multidenomination" into only column "Denomination" 
' 06-OCT-2014  LEM   Call TerminalReport.ForceRefresh on InitControls.
' 02-DEC-2014  DLL   Added new column GameTheme
' 13-APR-2015  FJC   BackLog Item 940
' 14-APR-2015  DHA   Added terminal clone
' 13-MAY-2015  FOS   WIG 2336:Error filter by Terminal
' 20-MAY-2015  DCS   WIG-2369 Do not allow clone the terminal "SORTEADOR"
' 12-JUN-2015  YNM   WIG-2440 Terminal unhandled exception
' 23-JUN-2015  FOS   Send Command Restart when clone a terminal
' 13-JUL-2015  DCS   WIG-2573: Blinking button in multiselect
' 15-JUL-2015  SGB   Backlog Item 2672: Modify column blocked, and show is atomatily or for comunication
' 10-AUG-2015  AMF   Product Backlog Item 1936
' 16-MAR-2016  FAV   Fixed Bug 9552: Change terminal to pending to collect for Clone terminal
' 16-MAR-2016  FAV   Fixed Bug 9557: The cashier session for the pending money collection should change to pending closing
' 01-APR-2016  LA    Add join to catalog in the sql list
' 19-OCT-2016  LTC   Bug 13096:Ref.288D - Wrong format in machine serial number in excel exported file
' 05-JAN-2017  CCG   Bug 21898:GUI: Terminales pendientes - mensaje de error incorrecto
' 16-JAN-2017  RAB   Bug 22724: Field type of counters not shown the information correctly.
' 09-MAY-2017  ATB   PBI 27206: Terminals report - Date of Creation
' 19-DHA-2017  DHA   Bug 28214:[Ticket #6067] Error in the display of denomination in Terminal Report
' 01-MAR-2018  DPC   Bug 31769:[WIGOS-8193]: [Ticket #12514] Reporte de terminales No muestra el comando 1B activado V03.06.0035
' 04-JUN-2018  GDA   Bug 32873: WIGOS-12241 - [Ticket #14515] Fallo � Reporte de Terminales � denominaci�n formato hora Version V03.08.0001
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Misc

Public Class frm_terminals
  Inherits frm_base_sel

#Region " Constants "
  ' Grid Columns
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 1
  Private Const GRID_COLUMN_PROVIDER_ID As Integer = 2
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const GRID_COLUMN_CREATION_DATE As Integer = 4
  Private Const GRID_COLUMN_ACTIVATION_DATE As Integer = 5
  Private Const GRID_COLUMN_REPLACEMENT_DATE As Integer = 6
  Private Const GRID_COLUMN_TERMINAL_STATUS As Integer = 7
  Private Const GRID_COLUMN_RETIREMENT_DATE As Integer = 8
  Private Const GRID_COLUMN_RETIREMENT_REQUESTED As Integer = 9
  Private Const GRID_COLUMN_IS_BLOCKED As Integer = 10
  Private Const GRID_COLUMN_AREA As Integer = 11
  Private Const GRID_COLUMN_ZONE As Integer = 12
  Private Const GRID_COLUMN_BANK_ID As Integer = 13
  Private Const GRID_COLUMN_POSITION As Integer = 14
  Private Const GRID_COLUMN_LOCATION_LEVEL As Integer = 15
  Private Const GRID_COLUMN_TERMINAL_TYPE As Integer = 16
  Private Const GRID_COLUMN_VERSION As Integer = 17
  Private Const GRID_COLUMN_EXTERNAL_ID As Integer = 18
  Private Const GRID_COLUMN_VENDOR_ID As Integer = 19
  Private Const GRID_COLUMN_PROGRAM As Integer = 20
  Private Const GRID_COLUMN_REGISTRATION_CODE As Integer = 21
  Private Const GRID_COLUMN_MACHINE_ID As Integer = 22
  'Ini ColJuegos
  Private GRID_COLUMN_BRAND_CODE As Integer = 23
  Private GRID_COLUMN_MODEL As Integer = 24
  Private GRID_COLUMN_MANUFACTURE_YEAR As Integer = 25
  Private GRID_COLUMN_SCLM As Integer = 26
  Private GRID_COLUMN_MET_HOMOLOGATED As Integer = 27
  Private GRID_COLUMN_BET_CODE As Integer = 28
  ' Fin ColJuegos
  Private GRID_COLUMN_DENOMINATION As Integer = 29
  Private GRID_COLUMN_GAME_TYPE As Integer = 30
  Private GRID_COLUMN_CABINET_TYPE As Integer = 31
  Private GRID_COLUMN_TOP_AWARD As Integer = 32
  Private GRID_COLUMN_MAX_BET As Integer = 33
  Private GRID_COLUMN_TEORETHICAL_PAYOUT As Integer = 34
  Private GRID_COLUMN_PROGRESSIVE_CONTRIBUTION As Integer = 35
  Private GRID_COLUMN_GAME_THEME As Integer = 36
  Private GRID_COLUMN_NUMBER_LINES As Integer = 37
  Private GRID_COLUMN_SAS_FLAG As Integer = 38
  Private GRID_COLUMN_TERMINAL_METER_DELTA As Integer = 39
  Private GRID_COLUMN_SERIAL_NUMBER As Integer = 40
  Private GRID_COLUMN_ASSET_NUMBER As Integer = 41
  Private GRID_COLUMN_CONTRACT_TYPE As Integer = 42
  Private GRID_COLUMN_CONTRACT_ID As Integer = 43
  Private GRID_COLUMN_ORDER_NUMBER As Integer = 44
  Private GRID_COLUMN_MASTER_ID As Integer = 45


  ' DB Columns
  Private Const SQL_COLUMN_SELECT As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_PROVIDER_ID As Integer = 2
  Private Const SQL_COLUMN_EXTERNAL_ID As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_STATUS As Integer = 5
  Private Const SQL_COLUMN_PROGRAM As Integer = 6
  Private Const SQL_COLUMN_REGISTRATION_CODE As Integer = 7
  Private Const SQL_COLUMN_SERIAL_NUMBER As Integer = 8
  Private Const SQL_COLUMN_VENDOR_ID As Integer = 9
  Private Const SQL_COLUMN_RETIREMENT_DATE As Integer = 10
  Private Const SQL_COLUMN_RETIREMENT_REQUESTED As Integer = 11
  Private Const SQL_COLUMN_DENOMINATION As Integer = 12
  Private Const SQL_COLUMN_IS_BLOCKED As Integer = 13
  Private Const SQL_COLUMN_LOCATION_LEVEL As Integer = 14
  Private Const SQL_COLUMN_GAME_TYPE As Integer = 15
  Private Const SQL_COLUMN_CABINET_TYPE As Integer = 16
  Private Const SQL_COLUMN_TEORETHICAL_PAYOUT As Integer = 17
  Private Const SQL_COLUMN_PROGRESSIVE_CONTRIBUTION As Integer = 18
  Private Const SQL_COLUMN_CREATION_DATE As Integer = 19
  Private Const SQL_COLUMN_ACTIVATION_DATE As Integer = 20
  Private Const SQL_COLUMN_REPLACEMENT_DATE As Integer = 21
  Private Const SQL_COLUMN_BANK_ID As Integer = 22
  Private Const SQL_COLUMN_AREA As Integer = 23
  Private Const SQL_COLUMN_ZONE As Integer = 24
  Private Const SQL_COLUMN_VERSION As Integer = 25
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 26
  Private Const SQL_COLUMN_SAS_FLAG As Integer = 27
  Private Const SQL_COLUMN_CONTRACT_TYPE As Integer = 28
  Private Const SQL_COLUMN_CONTRACT_ID As Integer = 29
  Private Const SQL_COLUMN_ORDER_NUMBER As Integer = 30
  Private Const SQL_COLUMN_TT_TYPE As Integer = 31
  Private Const SQL_COLUMN_STATUS_ID As Integer = 32
  Private Const SQL_COLUMN_MACHINE_ID As Integer = 33
  Private Const SQL_COLUMN_POSITION As Integer = 34
  Private Const SQL_COLUMN_TOP_AWARD As Integer = 35
  Private Const SQL_COLUMN_MAX_BET As Integer = 36
  Private Const SQL_COLUMN_NUMBER_LINES As Integer = 37
  Private Const SQL_COLUMN_GAME_THEME As Integer = 38
  Private Const SQL_COLUMN_ASSET_NUMBER As Integer = 39
  Private Const SQL_COLUMN_TERMINAL_METER_DELTA As Integer = 40
  Private Const SQL_COLUMN_MASTER_ID As Integer = 41
  Private Const SQL_COLUMN_ATOMATICALLY_BLOCKED As Integer = 42
  Private Const SQL_COLUMN_SAS_FLAGS_USE_SITE_DEFAULT As Integer = 43
  'Ini ColJuegos
  Private Const SQL_COLUMN_BRAND_CODE As Integer = 44
  Private Const SQL_COLUMN_MODEL As Integer = 45
  Private Const SQL_COLUMN_MANUFACTURE_YEAR As Integer = 46
  Private Const SQL_COLUMN_MET_HOMOLOGATED As Integer = 47
  Private Const SQL_COLUMN_BET_CODE As Integer = 48
  ' Fin ColJuegos


  ' Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_TERMINAL_NAME As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_PROVIDER_ID As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_EXTERNAL_ID As Integer = 2300
  Private Const GRID_COLUMN_WIDTH_TERMINAL_TYPE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TERMINAL_STATUS As Integer = 1900
  Private Const GRID_COLUMN_WIDTH_PROGRAM As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_REGISTRATION_CODE As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_SERIAL_NUMBER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_ASSET_NUMBER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_VENDOR_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_RETIREMENT_DATE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_RETIREMENT_REQUESTED As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_DENOMINATION As Integer = 1500
  Private Const GRID_COLUMN_WIDTH_IS_BLOCKED As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_LOCATION_LEVEL As Integer = 1300
  Private Const GRID_COLUMN_WIDTH_GAME_TYPE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_CABINET_TYPE As Integer = 1500
  Private Const GRID_COLUMN_WIDTH_TEORETHICAL_PAYOUT As Integer = 2400
  Private Const GRID_COLUMN_WIDTH_PROGRESSIVE_CONTRIBUTION As Integer = 3400
  Private Const GRID_COLUMN_WIDTH_CREATION_DATE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_ACTIVATION_DATE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_REPLACEMENT_DATE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BANK_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_AREA As Integer = 1900
  Private Const GRID_COLUMN_WIDTH_ZONE As Integer = 1500
  Private Const GRID_COLUMN_WIDTH_VERSION As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_TERMINAL_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_SAS_FLAG As Integer = 2800
  Private Const GRID_COLUMN_WIDTH_CONTRACT_TYPE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_CONTRACT_ID As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_ORDER_NUMBER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_MACHINE_ID As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_POSITION As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_TOP_AWARD As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_MAX_BET As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_NUMBER_LINES As Integer = 1800
  Private Const GRID_COLUMN_WIDTH_GAME_THEME As Integer = 1800
  Private Const GRID_COLUMN_WIDTH_TERMINAL_METER_DELTA As Integer = 1800
  Private Const GRID_COLUMN_WIDTH_MASTER_ID As Integer = 0
  'Ini ColJuegos
  Private Const GRID_COLUMN_WIDTH_BRAND_CODE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_MODEL As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_MANUFACTURE_YEAR As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_SCLM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_MET_HOMOLOGATED As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BET_CODE As Integer = 2000
  ' Fin ColJuegos

  ' Grid configuration
  Private GRID_COLUMNS As Integer = 46
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_NUM_COLUMNS_COLJUEGOS As Integer = 6

  'null date value denomination
  Private Const NULL_DATE_VALUE As String = "01/01/1900 00:00"

  ' Excel Columns
  Private Const EXCEL_COLUMN_PROVIDER_ID As Integer = 1
  Private Const EXCEL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const EXCEL_COLUMN_BANK_ID As Integer = 10
  Private Const EXCEL_COLUMN_LOCATION_LEVEL As Integer = 12
  Private Const EXCEL_COLUMN_VERSION As Integer = 14
  Private Const EXCEL_COLUMN_EXTERNAL_ID As Integer = 15
  Private Const EXCEL_COLUMN_VENDOR_ID As Integer = 16
  Private Const EXCEL_COLUMN_PROGRAM As Integer = 17
  Private Const EXCEL_COLUMN_REGISTRATION_CODE As Integer = 18
  Private Const EXCEL_COLUMN_SERIAL_NUMBER As Integer = 19
  Private Const EXCEL_COLUMN_MACHINE_ID As Integer = 20
  Private Const EXCEL_COLUMN_DENOMINATION As Integer = 22
  Private Const EXCEL_COLUMN_CABINET_TYPE As Integer = 23
  Private Const EXCEL_COLUMN_NUMBER_LINES As Integer = 28
  Private Const EXCEL_COLUMN_CONTRACT_TYPE As Integer = 30
  Private Const EXCEL_COLUMN_CONTRACT_ID As Integer = 31
  Private Const EXCEL_COLUMN_ORDER_NUMBER As Integer = 32

  Private Const FORM_DB_MIN_VERSION As Short = 226 ' TODO: Real MIN_VERSION = 227

#End Region ' Constants 

#Region " Enums "

  Public Enum ENUM_TERMINALS_SELECTION_MODE
    NONE = 0
    ONE = 1
    MULTI = 2
  End Enum

#End Region

#Region " Members "
  ' Booleans
  Private m_terminal_is_blocked As Boolean
  Private m_terminal_is_not_blocked As Boolean

  'Strings
  Private m_terminal_name As String
  Private m_terminal_id_protocol As String
  Private m_terminal_status_string_list As String
  Private m_terminal_types_string_list As String
  Private m_common_sql_select_part As String
  Private m_report_terminals As String
  Private m_report_terminal_type As String
  Private m_report_terminal_blocked As String
  Private m_report_terminal_status As String
  Private m_serial_number As String
  Private m_asset_number As String

  'Integers
  Private m_timeout As Integer

  'Others types
  Private m_sel_params As TYPE_TERMINAL_SEL_PARAMS
  Private m_data_view_terminals As DataView

  ' For terminal selection  
  Private m_selection_mode As ENUM_TERMINALS_SELECTION_MODE
  Private m_selected_terminal_id As Integer
  Private m_selected_multi_terminals_id As New List(Of Integer)

#End Region ' Members

#Region " Overrides "
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINALS_SELECTION
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()
    Dim _terminal_types() As Integer

    ' - Form title
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(328)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_AUDITOR.GetString(466)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6059)
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, GLB_NLS_GUI_CONTROLS.GetString(25), GLB_NLS_GUI_CONTROLS.GetString(9))
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6558)

    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, True, False)
    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, True, False)
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, True, False)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE, True, False)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Height += 10
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    If GeneralParam.GetBoolean("EGM", "ExternalMachine.Enabled") Then
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = IIf(m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE And CurrentUser.Permissions(ENUM_FORM.FORM_NEW_OFFLINE_TERMINAL).Read, True, False)
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Type = uc_button.ENUM_BUTTON_TYPE.USER
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Height += 10
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Enabled = True
    End If

    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    _terminal_types = WSI.Common.Misc.AllTerminalTypes()

    Call Me.uc_pr_list.Init(_terminal_types)
    Call Me.uc_type_list.Init(_terminal_types)

    chk_terminal_yes.Text = GLB_NLS_GUI_AUDITOR.GetString(336)
    chk_terminal_no.Text = GLB_NLS_GUI_AUDITOR.GetString(337)
    fra_terminal_blocked.Text = GLB_NLS_GUI_AUDITOR.GetString(332)
    fra_terminal.Text = GLB_NLS_GUI_AUDITOR.GetString(339)
    ef_terminal_name.Text = GLB_NLS_GUI_AUDITOR.GetString(330)
    ef_terminal_id.Text = GLB_NLS_GUI_AUDITOR.GetString(331)
    gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
    chk_status_active.Text = CLASS_TERMINAL.StatusName(0)
    chk_status_out_of_order.Text = CLASS_TERMINAL.StatusName(1)
    chk_status_retired.Text = CLASS_TERMINAL.StatusName(2)
    chk_serial_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6056)
    chk_asset_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6116)
    chk_serial_number.Checked = False
    chk_asset_number.Checked = False


    Call Me.ef_terminal_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.ef_terminal_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)

    If Not m_sel_params Is Nothing Then
      Call GUI_FilterReset()
    End If

    ' Hide ColJuegos
    If Not IsColJuegosEnabled() Then
      Call HideGridColumnsColJuegos()
    End If

    Call GUI_StyleSheet()

    Call TerminalReport.ForceRefresh()

  End Sub ' GUI_InitControls

  ' PURPOSE: Build an SQL query from conditions set in the filters of this view
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _new_str_sql As System.Text.StringBuilder

    GUI_ReportUpdateFilters()

    _new_str_sql = New System.Text.StringBuilder()

    If (m_common_sql_select_part = Nothing) Then
      m_common_sql_select_part = GetCommonSqlSelectPart()
    End If

    _new_str_sql.AppendLine(m_common_sql_select_part)
    _new_str_sql.AppendLine(" WHERE   TE.TE_TYPE = 1 ")
    _new_str_sql.AppendLine("   AND   ISNULL(TE.TE_SERVER_ID, 0) =  0")
    _new_str_sql.AppendLine("   AND   " & GUI_FilterField("TE.TE_NAME", m_terminal_name, True, False, True))
    _new_str_sql.AppendLine("   AND   UPPER (ISNULL(TE.TE_EXTERNAL_ID, '')) LIKE " & IIf(m_terminal_id_protocol.Length > 0, "'%" & m_terminal_id_protocol & "%' ESCAPE '\'", "ISNULL(TE.TE_EXTERNAL_ID, '')"))
    _new_str_sql.AppendLine("   AND   TE.TE_BLOCKED = " & IIf(m_terminal_is_blocked = m_terminal_is_not_blocked, "TE.TE_BLOCKED", IIf(m_terminal_is_blocked, "1", "0")))
    _new_str_sql.AppendLine("   AND   TE.TE_TERMINAL_TYPE IN (" & IIf(m_terminal_types_string_list.Length > 0, m_terminal_types_string_list, " TE.TE_TERMINAL_TYPE") & ")")
    _new_str_sql.AppendLine("   AND   TE.TE_STATUS IN (" & m_terminal_status_string_list & ")")

    'SGB 20-MAR-2015: Use a new columns in DB
    If Me.chk_serial_number.Checked Then
      _new_str_sql.AppendLine("   AND   TE.TE_SERIAL_NUMBER <> TE.TE_MACHINE_SERIAL_NUMBER")
    End If

    'FJC 13-APR-2015: Use a new columns in DB
    If Me.chk_asset_number.Checked Then
      _new_str_sql.AppendLine("   AND   TE.TE_ASSET_NUMBER <> TE.TE_MACHINE_ASSET_NUMBER")
    End If

    'FOS 13-MAY-2015: WIG-2336
    _new_str_sql.AppendLine(" AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_TERMINAL_CLONE).Execute

    Return _new_str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: What to do when no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  '
  Protected Overrides Sub GUI_NoDataFound()
    Call MyBase.GUI_NoDataFound()
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (Me.Grid.NumRows > 0)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (Me.Grid.NumRows > 0)
  End Sub ' GUI_NoDataFound

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _replacement_date As String
    Dim _retirement_date As String
    Dim _retirement_request_date As String

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ID).Value = DbRow.Value(SQL_COLUMN_PROVIDER_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXTERNAL_ID).Value = DbRow.Value(SQL_COLUMN_EXTERNAL_ID).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_STATUS).Value = DbRow.Value(SQL_COLUMN_TERMINAL_STATUS)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRAM).Value = DbRow.Value(SQL_COLUMN_PROGRAM).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REGISTRATION_CODE).Value = DbRow.Value(SQL_COLUMN_REGISTRATION_CODE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SERIAL_NUMBER).Value = DbRow.Value(SQL_COLUMN_SERIAL_NUMBER)
    If Not DbRow.IsNull(SQL_COLUMN_ASSET_NUMBER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ASSET_NUMBER).Value = DbRow.Value(SQL_COLUMN_ASSET_NUMBER)
    End If
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_METER_DELTA) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_METER_DELTA).Value = DbRow.Value(SQL_COLUMN_TERMINAL_METER_DELTA)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VENDOR_ID).Value = DbRow.Value(SQL_COLUMN_VENDOR_ID)

    If Not DbRow.IsNull(SQL_COLUMN_ATOMATICALLY_BLOCKED) AndAlso _
      Not TerminalStatusFlags.IsFlagActived(DbRow.Value(SQL_COLUMN_ATOMATICALLY_BLOCKED), TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_UNBLOCKED) AndAlso _
      (TerminalStatusFlags.IsFlagActived(DbRow.Value(SQL_COLUMN_ATOMATICALLY_BLOCKED), TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_AUTOMATICALLY) OrElse _
       TerminalStatusFlags.IsFlagActived(DbRow.Value(SQL_COLUMN_ATOMATICALLY_BLOCKED), TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY) OrElse _
       TerminalStatusFlags.IsFlagActived(DbRow.Value(SQL_COLUMN_ATOMATICALLY_BLOCKED), TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_BLOCKED)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_IS_BLOCKED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6541)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_IS_BLOCKED).Value = IIf(DbRow.Value(SQL_COLUMN_IS_BLOCKED), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6540), "")
    End If


    If Not DbRow.IsNull(SQL_COLUMN_ATOMATICALLY_BLOCKED) AndAlso TerminalStatusFlags.IsFlagActived(DbRow.Value(SQL_COLUMN_ATOMATICALLY_BLOCKED), TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_INTRUSION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_IS_BLOCKED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7116)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_LOCATION_LEVEL).Value = DbRow.Value(SQL_COLUMN_LOCATION_LEVEL)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TYPE).Value = DbRow.Value(SQL_COLUMN_GAME_TYPE).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CABINET_TYPE).Value = DbRow.Value(SQL_COLUMN_CABINET_TYPE).ToString()
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TEORETHICAL_PAYOUT).Value = IIf(DbRow.Value(SQL_COLUMN_TEORETHICAL_PAYOUT) = 0, "", GUI_FormatNumber(DbRow.Value(SQL_COLUMN_TEORETHICAL_PAYOUT) * 100))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_CONTRIBUTION).Value = IIf(DbRow.Value(SQL_COLUMN_PROGRESSIVE_CONTRIBUTION) = 0, "", GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PROGRESSIVE_CONTRIBUTION)))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_ID).Value = DbRow.Value(SQL_COLUMN_BANK_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA).Value = DbRow.Value(SQL_COLUMN_AREA)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ZONE).Value = IIf(DbRow.Value(SQL_COLUMN_ZONE), GLB_NLS_GUI_CONFIGURATION.GetString(436), GLB_NLS_GUI_CONFIGURATION.GetString(437))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VERSION).Value = DbRow.Value(SQL_COLUMN_VERSION)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    If Not DbRow.IsNull(SQL_COLUMN_DENOMINATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = CLASS_TERMINAL_DENOMINATIONS.NumberFormatDenomination(DbRow.Value(SQL_COLUMN_DENOMINATION), False)
    End If

    If (DbRow.Value(SQL_COLUMN_TT_TYPE) = WSI.Common.TerminalTypes.SAS_HOST) Then
      Dim _sas_flags As Integer
      Dim _sas_flags_site_default As Integer
      Dim _enum_sas_flag As ENUM_SAS_FLAGS

      _sas_flags = DbRow.Value(SQL_COLUMN_SAS_FLAG)
      _sas_flags_site_default = DbRow.Value(SQL_COLUMN_SAS_FLAGS_USE_SITE_DEFAULT)
      _enum_sas_flag = CLASS_TERMINAL.Cmd0x1Bhandpays(_sas_flags_site_default, _sas_flags)

      Select Case _enum_sas_flag
        Case ENUM_SAS_FLAGS.SITE_DEFINED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_FLAG).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432) 'Site defined
        Case ENUM_SAS_FLAGS.ACTIVATED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_FLAG).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7081) 'Extended
        Case ENUM_SAS_FLAGS.NOT_ACTIVATED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_FLAG).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7082) 'Non extended
        Case ENUM_SAS_FLAGS.NON_EXTENDED_METERS_LENGHT
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_FLAG).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7084) 'Non extended-Customized
        Case ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY

      End Select

    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CREATION_DATE), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACTIVATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACTIVATION_DATE), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM)


    _replacement_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_REPLACEMENT_DATE), _
                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                    ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REPLACEMENT_DATE).Value = IIf(_replacement_date = NULL_DATE_VALUE, "", _replacement_date)

    ' If date value is null then show nothing
    _retirement_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_RETIREMENT_DATE), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)

    If DbRow.Value(SQL_COLUMN_STATUS_ID) = WSI.Common.TerminalStatus.RETIRED Then
      _retirement_date = IIf(_retirement_date = NULL_DATE_VALUE, "", _retirement_date)
      ' If date value is null then show nothing
      _retirement_request_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_RETIREMENT_REQUESTED), _
                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                             ENUM_FORMAT_TIME.FORMAT_HHMM)
      _retirement_request_date = IIf(_retirement_request_date = NULL_DATE_VALUE, "", _retirement_request_date)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_RETIREMENT_DATE).Value = _retirement_date
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RETIREMENT_REQUESTED).Value = _retirement_request_date
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RETIREMENT_DATE).Value = Nothing
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RETIREMENT_REQUESTED).Value = Nothing
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CONTRACT_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CONTRACT_TYPE).Value = DbRow.Value(SQL_COLUMN_CONTRACT_TYPE)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CONTRACT_ID).Value = DbRow.Value(SQL_COLUMN_CONTRACT_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORDER_NUMBER).Value = DbRow.Value(SQL_COLUMN_ORDER_NUMBER)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_MACHINE_ID).Value = DbRow.Value(SQL_COLUMN_MACHINE_ID)

    ' ColJuegos
    If IsColJuegosEnabled() Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BRAND_CODE).Value = DbRow.Value(SQL_COLUMN_BRAND_CODE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MODEL).Value = DbRow.Value(SQL_COLUMN_MODEL)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MANUFACTURE_YEAR).Value = IIf(DbRow.Value(SQL_COLUMN_MANUFACTURE_YEAR) > 0, DbRow.Value(SQL_COLUMN_MANUFACTURE_YEAR), "")
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SCLM).Value = IIf(DbRow.Value(SQL_COLUMN_STATUS_ID) = WSI.Common.TerminalStatus.ACTIVE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6394), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6393))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MET_HOMOLOGATED).Value = IIf(DbRow.Value(SQL_COLUMN_MET_HOMOLOGATED), GLB_NLS_GUI_AUDITOR.GetString(336), GLB_NLS_GUI_AUDITOR.GetString(337))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BET_CODE).Value = DbRow.Value(SQL_COLUMN_BET_CODE)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_POSITION).Value = IIf(DbRow.Value(SQL_COLUMN_POSITION) > 0, GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POSITION), 0), "")
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOP_AWARD).Value = IIf(DbRow.Value(SQL_COLUMN_TOP_AWARD) > 0, GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOP_AWARD), 2), "")
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MAX_BET).Value = IIf(DbRow.Value(SQL_COLUMN_MAX_BET) > 0, GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MAX_BET), 2), "")
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUMBER_LINES).Value = DbRow.Value(SQL_COLUMN_NUMBER_LINES)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_THEME).Value = DbRow.Value(SQL_COLUMN_GAME_THEME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MASTER_ID).Value = DbRow.Value(SQL_COLUMN_MASTER_ID)

    Return True

  End Function ' GUI_SetupRow 

  ' PURPOSE : Sets parameters for the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '           - Optional FirstColIndex As Integer
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)


  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                          Optional ByVal FirstColIndex As Integer = 0)

    Call EnablePrintColumns(False)

    Call MyBase.GUI_ReportParams(PrintData)


    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(212)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE : Sets parameters for the excel
  '
  '  PARAMS :
  '     - INPUT :
  '           - ExcelData As GUI_Reports.CLASS_EXCEL_DATA
  '           - Optional FirstColIndex As Integer
  '
  '     - OUTPUT :

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, _
                                        Optional ByVal FirstColIndex As Integer = 0)

    Call EnablePrintColumns(True)
    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_PROVIDER_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_TERMINAL_NAME, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_BANK_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_LOCATION_LEVEL, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_VERSION, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_EXTERNAL_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_VENDOR_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_PROGRAM, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_REGISTRATION_CODE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SERIAL_NUMBER, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_MACHINE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_DENOMINATION, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_CABINET_TYPE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_NUMBER_LINES, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_CONTRACT_TYPE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_CONTRACT_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
    ExcelData.SetColumnFormat(EXCEL_COLUMN_ORDER_NUMBER, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    If SelectedRow >= 0 And Grid.NumRows > 0 Then
      If Me.Grid.Cell(SelectedRow, GRID_COLUMN_TERMINAL_STATUS).Value = CLASS_TERMINAL.StatusName(0) _
         AndAlso Me.Grid.Cell(SelectedRow, GRID_COLUMN_TERMINAL_TYPE).Value.TrimEnd().Replace(" ", "_") <> WSI.Common.TerminalTypes.CASHDESK_DRAW.ToString() _
         AndAlso Me.Grid.Cell(SelectedRow, GRID_COLUMN_TERMINAL_TYPE).Value.TrimEnd().Replace(" ", "_") <> WSI.Common.TerminalTypes.GAMEGATEWAY.ToString() _
         AndAlso Me.Grid.Cell(SelectedRow, GRID_COLUMN_TERMINAL_TYPE).Value.TrimEnd().Replace(" ", "_") <> WSI.Common.TerminalTypes.PARIPLAY.ToString() Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True And CurrentUser.Permissions(ENUM_FORM.FORM_TERMINAL_CLONE).Execute
      End If
    End If
  End Sub

  ' PURPOSE : Handles button click events of the form
  '
  '  PARAMS :
  '     - INPUT :
  '           - ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        If m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE Then
          Call GUI_EditSelectedItem()
        Else
          Call GUI_SelectTerminal()
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        Call GUI_DisconnectTerminal()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2
        Call GUI_CloneTerminal()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_EXCEL
        Call EnablePrintColumns(True)
        ' LTC 19-OCT-2016
        Call SetFormateExcel()
        Call MyBase.GUI_ButtonClick(ButtonId)
        Call RemoveFormatExcel()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3
        Call NewOfflineTerminal()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()
    Dim _frm_edit As frm_terminals_edition
    Dim _idx_row As Int32
    Dim _terminal_id As Integer

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    _terminal_id = Val(Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value)
    _frm_edit = New frm_terminals_edition

    Call _frm_edit.ShowEditItem(_terminal_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()
    RefreshGridWithNoReload()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Update the member variables used for the printed report
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' Gather all values from the view
    Dim _selected_providers_array As String()
    Dim _selected_terminal_ids_array As Long()

    m_terminal_name = ef_terminal_name.Value
    m_terminal_id_protocol = ef_terminal_id.Value
    m_terminal_is_blocked = chk_terminal_yes.Checked
    m_terminal_is_not_blocked = chk_terminal_no.Checked

    _selected_terminal_ids_array = Me.uc_pr_list.GetTerminalIdListSelected()
    _selected_providers_array = Me.uc_pr_list.GetOnlyProviderIdListSelected()

    m_terminal_status_string_list = SetTerminalStatusFilter()
    m_terminal_types_string_list = Me.uc_type_list.GetTerminalTypesSelected(False)

    If m_terminal_is_blocked Then
      m_report_terminal_blocked = GLB_NLS_GUI_AUDITOR.GetString(336)
      If m_terminal_is_not_blocked Then
        m_report_terminal_blocked &= ", " & GLB_NLS_GUI_AUDITOR.GetString(337)
      End If
    ElseIf m_terminal_is_not_blocked Then
      m_report_terminal_blocked = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

    ' Status
    m_report_terminal_status = ""
    If chk_status_active.Checked Then
      m_report_terminal_status &= CLASS_TERMINAL.StatusName(0)
    End If

    If chk_status_out_of_order.Checked Then
      m_report_terminal_status &= IIf(m_report_terminal_status.Length > 0, ", ", "") & CLASS_TERMINAL.StatusName(1)
    End If

    If chk_status_retired.Checked Then
      m_report_terminal_status &= IIf(m_report_terminal_status.Length > 0, ", ", "") & CLASS_TERMINAL.StatusName(2)
    End If

    If chk_serial_number.Checked Then
      m_serial_number = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_serial_number = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

    If chk_asset_number.Checked Then
      m_asset_number = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_asset_number = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

    ' Providers - Terminals
    m_report_terminals = Me.uc_pr_list.GetTerminalReportText()

    If Me.uc_type_list.opt_all_types.Checked Then
      m_report_terminal_type = Me.uc_type_list.opt_all_types.Text
    Else
      m_report_terminal_type = Me.uc_type_list.opt_several_types.Text
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE :  Executed just before closing form
  '
  '  PARAMS :
  '     - INPUT : CloseCanceled
  '     - OUTPUT :
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    'If on Closing, the selected terminal id is still equals to 0, it means that we did not select any single terminal
    If m_selected_terminal_id = 0 Then
      'Set selected terminal at -1 to avoid checks in frm_terminals_pending
      m_selected_terminal_id = -1
    End If

    MyBase.GUI_Closing(CloseCanceled)
  End Sub

#End Region ' Overrides

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE : Opens dialog with default settings for select mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS: TerminalId selected

  Public Function ShowForSelect() As Integer

    m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.ONE

    Me.Display(True)

    m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE

    Return m_selected_terminal_id

  End Function


  Public Function ShowForSelect(ByVal SelParams As TYPE_TERMINAL_SEL_PARAMS) As Integer

    m_sel_params = SelParams

    Return ShowForSelect()

  End Function

  ' PURPOSE : Opens dialog with default settings for select mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS: List of TerminalIds selected

  Public Function ShowForMultiSelect(ByVal SelParams As TYPE_TERMINAL_SEL_PARAMS) As List(Of Integer)

    m_sel_params = SelParams

    m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.MULTI

    Me.Display(True)

    m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.NONE

    Return m_selected_multi_terminals_id

  End Function

#End Region ' Public functions

#Region " Private Functions "
  ' PURPOSE: Load rows of selected datagrid rows from DB and repaint the cells without refreshing the un-selected ones
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub RefreshGridWithNoReload()
    Dim _selected_terminal_ids_list As String
    Dim _new_str_sql As System.Text.StringBuilder
    Dim _refreshed_data_set_of_selected_rows As DataTable
    Dim _idx_row As Integer
    Dim _temp_row As frm_base_sel.CLASS_DB_ROW
    Dim selectedRows() As Integer

    ' Get a list of the currently seleceted rows
    selectedRows = Me.Grid.SelectedRows()

    ' If there are no selected rows, then there is nothing to do here
    If selectedRows.Length = 0 Then
      Return
    End If

    ' Init ids list string
    _selected_terminal_ids_list = ""

    ' fetch terminal ids from selectedrows array
    For _idx_row = 0 To selectedRows.Length - 1
      _selected_terminal_ids_list &= Me.Grid.Cell(selectedRows(_idx_row), GRID_COLUMN_MASTER_ID).Value & ","
    Next

    ' Take away trailling coma from terminal id list
    _selected_terminal_ids_list = _selected_terminal_ids_list.Substring(0, _selected_terminal_ids_list.Length - 1)

    ' Build the refresh DataTable Query
    _new_str_sql = New System.Text.StringBuilder()
    _new_str_sql.AppendLine(GetCommonSqlSelectPart())
    _new_str_sql.AppendLine(" WHERE   TE.TE_MASTER_ID in(" & _selected_terminal_ids_list & ")")

    _refreshed_data_set_of_selected_rows = GUI_GetTableUsingSQL(_new_str_sql.ToString(), Integer.MaxValue, , , m_timeout)

    ' Change and repaint only selected rows
    For _idx_row = 0 To selectedRows.Length - 1
      For Each row As DataRow In _refreshed_data_set_of_selected_rows.Rows
        If Me.Grid.Cell(selectedRows(_idx_row), GRID_COLUMN_MASTER_ID).Value = row.Item("TE_MASTER_ID") Then
          _temp_row = New frm_base_sel.CLASS_DB_ROW(_refreshed_data_set_of_selected_rows.Rows(_idx_row))
          GUI_SetupRow(selectedRows(_idx_row), _temp_row)
        End If
      Next row
    Next _idx_row

    ' Disable "Clone" terminal if selected terminal is not active
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    If Me.Grid.NumRows > 0 And Me.Grid.SelectedRows.Length >= 0 And Grid.NumRows > 0 Then
      If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_TERMINAL_STATUS).Value = CLASS_TERMINAL.StatusName(0) Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True And CurrentUser.Permissions(ENUM_FORM.FORM_TERMINAL_CLONE).Execute
      End If
    End If

  End Sub ' RefreshGridWithNoReload
  Private Sub EnablePrintColumns(ByVal isTrue As Boolean)
    With Me.Grid
      ' Creation date
      .Column(GRID_COLUMN_CREATION_DATE).IsColumnPrintable = isTrue

      ' Activation date
      .Column(GRID_COLUMN_ACTIVATION_DATE).IsColumnPrintable = isTrue

      ' Replacement date
      .Column(GRID_COLUMN_REPLACEMENT_DATE).IsColumnPrintable = isTrue

      ' Retirement date
      .Column(GRID_COLUMN_RETIREMENT_DATE).IsColumnPrintable = isTrue

      'Retirement requested
      .Column(GRID_COLUMN_RETIREMENT_REQUESTED).IsColumnPrintable = isTrue

      ' Location level
      .Column(GRID_COLUMN_LOCATION_LEVEL).IsColumnPrintable = isTrue

      ' location zone  
      .Column(GRID_COLUMN_ZONE).IsColumnPrintable = isTrue

      ' Terminal Program
      .Column(GRID_COLUMN_PROGRAM).IsColumnPrintable = isTrue

      ' Registration code
      .Column(GRID_COLUMN_REGISTRATION_CODE).IsColumnPrintable = isTrue

      ' Serial number
      .Column(GRID_COLUMN_SERIAL_NUMBER).IsColumnPrintable = isTrue

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION).IsColumnPrintable = isTrue

      ' Game type
      .Column(GRID_COLUMN_GAME_TYPE).IsColumnPrintable = isTrue

      ' Cabinet type
      .Column(GRID_COLUMN_CABINET_TYPE).IsColumnPrintable = isTrue

      ' Theorical payout 
      .Column(GRID_COLUMN_TEORETHICAL_PAYOUT).IsColumnPrintable = isTrue

      ' Progressive contribution
      .Column(GRID_COLUMN_PROGRESSIVE_CONTRIBUTION).IsColumnPrintable = isTrue

      ' SAS column
      .Column(GRID_COLUMN_SAS_FLAG).IsColumnPrintable = isTrue

      ' Contract type
      .Column(GRID_COLUMN_CONTRACT_TYPE).IsColumnPrintable = isTrue

      ' Contract ID
      .Column(GRID_COLUMN_CONTRACT_ID).IsColumnPrintable = isTrue

      ' Order number
      .Column(GRID_COLUMN_ORDER_NUMBER).IsColumnPrintable = isTrue

      .Column(GRID_COLUMN_MACHINE_ID).IsColumnPrintable = isTrue
      .Column(GRID_COLUMN_POSITION).IsColumnPrintable = isTrue
      .Column(GRID_COLUMN_TOP_AWARD).IsColumnPrintable = isTrue
      .Column(GRID_COLUMN_MAX_BET).IsColumnPrintable = isTrue
      .Column(GRID_COLUMN_GAME_THEME).IsColumnPrintable = isTrue
      .Column(GRID_COLUMN_NUMBER_LINES).IsColumnPrintable = isTrue

    End With
  End Sub
  ' PURPOSE: Disconnect Terminal from system.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub GUI_DisconnectTerminal()

    Dim _rc As Boolean

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Execute assign        
      _rc = DisconnectSelectedTerminals()

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (m_data_view_terminals.Count > 0 And Me.Permissions.Write)

    Catch ex As Exception
      Debug.WriteLine(ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' GUI_DisconnectTerminal

  ' PURPOSE: Clone Terminal
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub GUI_CloneTerminal()

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      CloneSelectedTerminal()

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (Me.Grid.NumRows > 0 And CurrentUser.Permissions(ENUM_FORM.FORM_TERMINAL_CLONE).Execute)

    Catch ex As Exception
      Debug.WriteLine(ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' GUI_DisconnectTerminal
  ' PURPOSE: Invokes "Disconnect terminals" form
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Function DisconnectSelectedTerminals() As Boolean
    Dim _idx_row As Integer
    Dim _select_idx As Collection
    Dim _has_errors As Boolean
    Dim _frm_term_ret As frm_terminals_retirement
    Dim _temp_data_table As DataTable
    Dim _temp_data_row As DataRow
    Dim _terminal_id_temp_column As DataColumn
    Dim _selected_rows As String

    _temp_data_table = New DataTable("TERMINALS")
    _terminal_id_temp_column = New DataColumn("TE_TERMINAL_ID", Type.GetType("System.Int32"))
    _temp_data_table.Columns.Add(_terminal_id_temp_column)
    _select_idx = New Collection()
    _has_errors = False

    ' If there is no row selected.
    If Me.Grid.SelectedRows.Length = 0 Then
      Return True
    End If

    Try
      m_data_view_terminals = New DataView()
      _selected_rows = Me.Grid.SelectedRows.Length

      For _idx_row = 0 To Me.Grid.SelectedRows.Length - 1
        _select_idx.Add(_idx_row)
        _temp_data_row = _temp_data_table.NewRow()
        _temp_data_row("TE_TERMINAL_ID") = Me.Grid.Cell(Me.Grid.SelectedRows(_idx_row), GRID_COLUMN_TERMINAL_ID).Value
        _temp_data_table.Rows.Add(_temp_data_row)
      Next

      ' No terminal selected
      If _select_idx.Count = 0 Then
        ' 118 "Debe seleccionar al menos un terminal."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return True
      End If

      m_data_view_terminals.Table = _temp_data_table
      _frm_term_ret = New frm_terminals_retirement()
      Call _frm_term_ret.ShowEditItem(_select_idx, m_data_view_terminals)
      _frm_term_ret = Nothing

      RefreshGridWithNoReload()

      Return True

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      _has_errors = True
      Return False

    Finally
      If _has_errors Then
        ' 119 "No se ha podido desconectar los terminales."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try

  End Function ' DisconnectSelectedTerminals

  ' PURPOSE: Clone terminal
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Function CloneSelectedTerminal()
    Dim _terminal As CLASS_TERMINAL
    Dim _terminal_id As Int32
    Dim _terminal_open_play_sessions As List(Of CLASS_PLAY_SESSION.TYPE_PLAY_SESSION)
    Dim _close_play_sessions As Boolean
    Dim _change_to_pending As Boolean


    _terminal = New CLASS_TERMINAL()
    _close_play_sessions = False
    _change_to_pending = False

    ' If there is no row selected ot there are more than 1 row
    If Me.Grid.SelectedRows.Length = 0 Or Me.Grid.SelectedRows.Length > 1 Then

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(102), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        mdl_NLS.ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        GLB_NLS_GUI_AUDITOR.GetString(339))
      Return True
    End If

    Try

      If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_TERMINAL_STATUS).Value <> CLASS_TERMINAL.StatusName(0) Then
        Return False
      End If

      ' Selected terminal will be cloned.\nDo you want to continue?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6057), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

        ' Get selected terminal id
        _terminal_id = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_TERMINAL_ID).Value


        'FAV 09-MAR-2016 It checks if there is a money_collection in Open status for the Terminal
        Dim _current_money_collection As WSI.Common.TITO.MoneyCollection
        Using _db_trx As New WSI.Common.DB_TRX()
          _current_money_collection = WSI.Common.TITO.MoneyCollection.DB_ReadCurrentMoneyCollection(_terminal_id, _db_trx.SqlTransaction)

          If (Not _current_money_collection Is Nothing) Then
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7180), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        _current_money_collection.StackerId, _
                        Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_TERMINAL_NAME).Value) = ENUM_MB_RESULT.MB_RESULT_YES Then

              _change_to_pending = True

            Else
              Return False
            End If
          End If
        End Using

        ' Check if there are still open play sessions
        _terminal_open_play_sessions = GetOpenPlaySessions(_terminal_id)

        If _terminal_open_play_sessions.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4441), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                        Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_TERMINAL_NAME).Value) = ENUM_MB_RESULT.MB_RESULT_YES Then

            ' Close play sessions
            ClosePlaySessions(_terminal_open_play_sessions)
          End If
        End If


        'FAV 09-MAR-2016 It changes status to Pending
        If (_change_to_pending) Then
          Using _db_trx As New WSI.Common.DB_TRX()

            If (WSI.Common.TITO.TITO_ChangeStacker.ChangeStackerInTitoMode(_terminal_id, Nothing, 0, False, _db_trx.SqlTransaction) And _
                Cashier.WCPCashierSessionPendingClosing(_terminal_id, _db_trx.SqlTransaction) <> MB_CASHIER_SESSION_CLOSE_STATUS.ERROR) Then
              _db_trx.Commit()
            Else
              Log.Error(String.Format("The operation to change the Stacker to Pending to collect has returned an error. TerminalId [{0}]", _terminal_id))
            End If
          End Using
        End If


        SendRestartCommand(_terminal_id)

        ' Execute clone        
        _terminal.TerminalId = _terminal_id
        If Not _terminal.CloneTerminal() Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853), _
                                ENUM_MB_TYPE.MB_TYPE_INFO, _
                                ENUM_MB_BTN.MB_BTN_OK)
          Return False
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6058), _
                                ENUM_MB_TYPE.MB_TYPE_INFO, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                                GLB_NLS_GUI_AUDITOR.GetString(365), _
                                GLB_NLS_GUI_CONFIGURATION.GetString(468))
        End If



        RefreshGridWithNoReload()

      End If

      Return True

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      Return False

    End Try

  End Function ' CloneSelectedTerminal

  ' PURPOSE: Send restart command to ebox
  '
  '  PARAMS:
  '     - INPUT: terminal_id: identifier of terminal that restart
  '           - 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SendRestartCommand(ByVal _terminal_id As Integer)

    Using _db_trx As New WSI.Common.DB_TRX()
      If WcpCommands.InsertWcpCommand(_terminal_id, WCP_CommandCode.Restart, String.Empty, _db_trx.SqlTransaction) Then
        _db_trx.Commit()
      Else
        _db_trx.Rollback()
        Log.Error(String.Format("Clone terminal - Send Restart Command ({0})", WSI.Common.WGDB.Now))
      End If
    End Using

  End Sub ' SendRestartCommand


  ' PURPOSE: Sets the default values for filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Protected Sub SetDefaultValues()

    Dim _default_filter As Boolean

    ef_terminal_name.Value = ""
    ef_terminal_id.Value = ""
    chk_terminal_yes.Checked = False
    chk_terminal_no.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

    _default_filter = True

    If Not m_sel_params Is Nothing Then
      _default_filter = m_sel_params.FilterTypeDefault
    End If

    If _default_filter Then
      Call uc_type_list.SetDefaultValues()
    Else
      Call uc_type_list.SetFilterValues(m_sel_params)
    End If

    chk_status_active.Checked = False
    chk_status_out_of_order.Checked = False
    chk_status_retired.Checked = False

    chk_serial_number.Checked = False
    chk_asset_number.Checked = False

  End Sub 'SetDefaultValues
  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      Select Case m_selection_mode
        Case ENUM_TERMINALS_SELECTION_MODE.MULTI, ENUM_TERMINALS_SELECTION_MODE.NONE
          Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE

        Case ENUM_TERMINALS_SELECTION_MODE.ONE
          Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      End Select

      ' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      ' Grid columns - Terminal id, 795
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal 
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795) ' N�mero
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_COLUMN_WIDTH_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Provider Id
      .Column(GRID_COLUMN_PROVIDER_ID).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal 
      .Column(GRID_COLUMN_PROVIDER_ID).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(341) ' Proveedor
      .Column(GRID_COLUMN_PROVIDER_ID).Width = GRID_COLUMN_WIDTH_PROVIDER_ID
      .Column(GRID_COLUMN_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333) ' Terminal
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(330) ' Nombre
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_COLUMN_WIDTH_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Creation date
      .Column(GRID_COLUMN_CREATION_DATE).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_CREATION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8279) ' Fecha de entrada
      .Column(GRID_COLUMN_CREATION_DATE).Width = GRID_COLUMN_WIDTH_CREATION_DATE
      .Column(GRID_COLUMN_CREATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Activation date
      .Column(GRID_COLUMN_ACTIVATION_DATE).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_ACTIVATION_DATE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(428) ' Fecha de entrada
      .Column(GRID_COLUMN_ACTIVATION_DATE).Width = GRID_COLUMN_WIDTH_ACTIVATION_DATE
      .Column(GRID_COLUMN_ACTIVATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Replacement date
      .Column(GRID_COLUMN_REPLACEMENT_DATE).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_REPLACEMENT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8280) ' Fecha de entrada
      .Column(GRID_COLUMN_REPLACEMENT_DATE).Width = GRID_COLUMN_WIDTH_REPLACEMENT_DATE
      .Column(GRID_COLUMN_REPLACEMENT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal status
      .Column(GRID_COLUMN_TERMINAL_STATUS).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_TERMINAL_STATUS).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_TERMINAL_STATUS).Width = GRID_COLUMN_WIDTH_TERMINAL_STATUS
      .Column(GRID_COLUMN_TERMINAL_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Retirement date
      .Column(GRID_COLUMN_RETIREMENT_DATE).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_RETIREMENT_DATE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(455) ' Fecha de retirada
      .Column(GRID_COLUMN_RETIREMENT_DATE).Width = GRID_COLUMN_WIDTH_RETIREMENT_DATE
      .Column(GRID_COLUMN_RETIREMENT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Retirement requested
      .Column(GRID_COLUMN_RETIREMENT_REQUESTED).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_RETIREMENT_REQUESTED).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(456) ' Orden de retirada
      .Column(GRID_COLUMN_RETIREMENT_REQUESTED).Width = GRID_COLUMN_WIDTH_RETIREMENT_REQUESTED
      .Column(GRID_COLUMN_RETIREMENT_REQUESTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Is blocked
      .Column(GRID_COLUMN_IS_BLOCKED).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Estado
      .Column(GRID_COLUMN_IS_BLOCKED).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(332) ' Bloqueado
      .Column(GRID_COLUMN_IS_BLOCKED).Width = GRID_COLUMN_WIDTH_IS_BLOCKED
      .Column(GRID_COLUMN_IS_BLOCKED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER_CENTER

      ' Location area  
      .Column(GRID_COLUMN_AREA).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166) ' Ubicaci�n
      .Column(GRID_COLUMN_AREA).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(435) ' Area
      .Column(GRID_COLUMN_AREA).Width = GRID_COLUMN_WIDTH_AREA
      .Column(GRID_COLUMN_AREA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Location level
      .Column(GRID_COLUMN_LOCATION_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166) ' Ubicaci�n
      .Column(GRID_COLUMN_LOCATION_LEVEL).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(432) ' Id de planta
      .Column(GRID_COLUMN_LOCATION_LEVEL).Width = GRID_COLUMN_WIDTH_LOCATION_LEVEL
      .Column(GRID_COLUMN_LOCATION_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank_id (Island)
      .Column(GRID_COLUMN_BANK_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166) ' Ubicaci�n
      .Column(GRID_COLUMN_BANK_ID).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(431) ' Isla
      .Column(GRID_COLUMN_BANK_ID).Width = GRID_COLUMN_WIDTH_BANK_ID
      .Column(GRID_COLUMN_BANK_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' location zone  
      .Column(GRID_COLUMN_ZONE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166) ' Ubicaci�n
      .Column(GRID_COLUMN_ZONE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(430) ' Zona
      .Column(GRID_COLUMN_ZONE).Width = GRID_COLUMN_WIDTH_ZONE
      .Column(GRID_COLUMN_ZONE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Position
      .Column(GRID_COLUMN_POSITION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166) ' Ubicaci�n
      .Column(GRID_COLUMN_POSITION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222) ' Posici�n
      .Column(GRID_COLUMN_POSITION).Width = GRID_COLUMN_WIDTH_POSITION
      .Column(GRID_COLUMN_POSITION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Vendor id
      .Column(GRID_COLUMN_VENDOR_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
      .Column(GRID_COLUMN_VENDOR_ID).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(344) ' Vendedor
      .Column(GRID_COLUMN_VENDOR_ID).Width = GRID_COLUMN_WIDTH_VENDOR_ID
      .Column(GRID_COLUMN_VENDOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254) ' Tipo
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = GRID_COLUMN_WIDTH_TERMINAL_TYPE
      .Column(GRID_COLUMN_TERMINAL_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Program
      .Column(GRID_COLUMN_PROGRAM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
      .Column(GRID_COLUMN_PROGRAM).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(418) ' Programa
      .Column(GRID_COLUMN_PROGRAM).Width = GRID_COLUMN_WIDTH_PROGRAM
      .Column(GRID_COLUMN_PROGRAM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Registration code
      .Column(GRID_COLUMN_REGISTRATION_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
      .Column(GRID_COLUMN_REGISTRATION_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(IsColJuegosEnabled, 6385, 1100)) ' ColJuegos: NUC // Registro fiscal
      .Column(GRID_COLUMN_REGISTRATION_CODE).Width = GRID_COLUMN_WIDTH_REGISTRATION_CODE
      .Column(GRID_COLUMN_REGISTRATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Serial number
      .Column(GRID_COLUMN_SERIAL_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096) ' SAS
      .Column(GRID_COLUMN_SERIAL_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(IsColJuegosEnabled, 6386, 1639)) ' ColJuegos: Serial // N�mero de serie
      .Column(GRID_COLUMN_SERIAL_NUMBER).Width = GRID_COLUMN_WIDTH_SERIAL_NUMBER
      .Column(GRID_COLUMN_SERIAL_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SERIAL_NUMBER).ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_STRINGC ' LTC 18-OCT-2016

      ' Asset number
      .Column(GRID_COLUMN_ASSET_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096) ' SAS
      .Column(GRID_COLUMN_ASSET_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6143) ' Asset Number
      .Column(GRID_COLUMN_ASSET_NUMBER).Width = GRID_COLUMN_WIDTH_ASSET_NUMBER
      .Column(GRID_COLUMN_ASSET_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Meter Delta
      .Column(GRID_COLUMN_TERMINAL_METER_DELTA).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096) ' SAS
      .Column(GRID_COLUMN_TERMINAL_METER_DELTA).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6085) ' Meter Delta
      .Column(GRID_COLUMN_TERMINAL_METER_DELTA).Width = GRID_COLUMN_WIDTH_TERMINAL_METER_DELTA
      .Column(GRID_COLUMN_TERMINAL_METER_DELTA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' MACHINE_ID
      .Column(GRID_COLUMN_MACHINE_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
      .Column(GRID_COLUMN_MACHINE_ID).Header(1).Text = GeneralParam.GetString("EGM", "MachineId.Name")
      .Column(GRID_COLUMN_MACHINE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MACHINE_ID).Width = IIf(String.IsNullOrEmpty(.Column(GRID_COLUMN_MACHINE_ID).Header(1).Text), 0, GRID_COLUMN_WIDTH_MACHINE_ID)

      '''''''''''''''''''''
      ''''' COLJUEGOS '''''
      '''''''''''''''''''''
      If IsColJuegosEnabled() Then
        ' Brand code
        .Column(GRID_COLUMN_BRAND_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
        .Column(GRID_COLUMN_BRAND_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6387) ' C�digo marca
        .Column(GRID_COLUMN_BRAND_CODE).Width = GRID_COLUMN_WIDTH_BRAND_CODE
        .Column(GRID_COLUMN_BRAND_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ' Model
        .Column(GRID_COLUMN_MODEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
        .Column(GRID_COLUMN_MODEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6388) ' Modelo
        .Column(GRID_COLUMN_MODEL).Width = GRID_COLUMN_WIDTH_MODEL
        .Column(GRID_COLUMN_MODEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ' Manufacture year
        .Column(GRID_COLUMN_MANUFACTURE_YEAR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
        .Column(GRID_COLUMN_MANUFACTURE_YEAR).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6389) ' A�o de fabricaci�n
        .Column(GRID_COLUMN_MANUFACTURE_YEAR).Width = GRID_COLUMN_WIDTH_MANUFACTURE_YEAR
        .Column(GRID_COLUMN_MANUFACTURE_YEAR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' SCLM
        .Column(GRID_COLUMN_SCLM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
        .Column(GRID_COLUMN_SCLM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6390) ' SCLM
        .Column(GRID_COLUMN_SCLM).Width = GRID_COLUMN_WIDTH_SCLM
        .Column(GRID_COLUMN_SCLM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' MET homologated
        .Column(GRID_COLUMN_MET_HOMOLOGATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
        .Column(GRID_COLUMN_MET_HOMOLOGATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6392) ' MET homologada
        .Column(GRID_COLUMN_MET_HOMOLOGATED).Width = GRID_COLUMN_WIDTH_MET_HOMOLOGATED
        .Column(GRID_COLUMN_MET_HOMOLOGATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        ' Bet code
        .Column(GRID_COLUMN_BET_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
        .Column(GRID_COLUMN_BET_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6391) ' C�digo de apuesta
        .Column(GRID_COLUMN_BET_CODE).Width = GRID_COLUMN_WIDTH_BET_CODE
        .Column(GRID_COLUMN_BET_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End If

      ' Version Number
      .Column(GRID_COLUMN_VERSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional
      .Column(GRID_COLUMN_VERSION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2068) ' Version
      .Column(GRID_COLUMN_VERSION).Width = GRID_COLUMN_WIDTH_VERSION
      .Column(GRID_COLUMN_VERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' External Id 
      .Column(GRID_COLUMN_EXTERNAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2066) ' Informaci�n adicional 
      .Column(GRID_COLUMN_EXTERNAL_ID).Header(1).Text = IIf(IsColJuegosEnabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6384), GLB_NLS_GUI_AUDITOR.GetString(331)) ' ColJuegos: NUID // ID Protocolo
      .Column(GRID_COLUMN_EXTERNAL_ID).Width = GRID_COLUMN_WIDTH_EXTERNAL_ID
      .Column(GRID_COLUMN_EXTERNAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(420) ' Denominaci�n
      .Column(GRID_COLUMN_DENOMINATION).Width = GRID_COLUMN_WIDTH_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Game type
      .Column(GRID_COLUMN_GAME_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_GAME_TYPE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(427) ' Tipo de juego
      .Column(GRID_COLUMN_GAME_TYPE).Width = GRID_COLUMN_WIDTH_GAME_TYPE
      .Column(GRID_COLUMN_GAME_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cabinet type
      .Column(GRID_COLUMN_CABINET_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_CABINET_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1640) ' Tipo de gabinete
      .Column(GRID_COLUMN_CABINET_TYPE).Width = GRID_COLUMN_WIDTH_CABINET_TYPE
      .Column(GRID_COLUMN_CABINET_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Top Award
      .Column(GRID_COLUMN_TOP_AWARD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_TOP_AWARD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5223) ' Premio m�ximo
      .Column(GRID_COLUMN_TOP_AWARD).Width = GRID_COLUMN_WIDTH_TOP_AWARD
      .Column(GRID_COLUMN_TOP_AWARD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Max Bet 
      .Column(GRID_COLUMN_MAX_BET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_MAX_BET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5224) ' Apuesta m�xima
      .Column(GRID_COLUMN_MAX_BET).Width = GRID_COLUMN_WIDTH_MAX_BET
      .Column(GRID_COLUMN_MAX_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical payout 
      .Column(GRID_COLUMN_TEORETHICAL_PAYOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_TEORETHICAL_PAYOUT).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(419) ' Payout te�rico (%)
      .Column(GRID_COLUMN_TEORETHICAL_PAYOUT).Width = GRID_COLUMN_WIDTH_TEORETHICAL_PAYOUT

      ' Progressive contribution
      .Column(GRID_COLUMN_PROGRESSIVE_CONTRIBUTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_PROGRESSIVE_CONTRIBUTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1641) ' Contribuci�n al progresivo (%)
      .Column(GRID_COLUMN_PROGRESSIVE_CONTRIBUTION).Width = GRID_COLUMN_WIDTH_PROGRESSIVE_CONTRIBUTION

      ' GAME_THEME
      .Column(GRID_COLUMN_GAME_THEME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Informaci�n adicional
      .Column(GRID_COLUMN_GAME_THEME).Header(1).Text = GeneralParam.GetString("EGM", "GameTheme.Name")
      .Column(GRID_COLUMN_GAME_THEME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_GAME_THEME).Width = IIf(String.IsNullOrEmpty(.Column(GRID_COLUMN_GAME_THEME).Header(1).Text), 0, GRID_COLUMN_WIDTH_GAME_THEME)

      ' Number Lines 
      .Column(GRID_COLUMN_NUMBER_LINES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1164) ' Caracteristicas
      .Column(GRID_COLUMN_NUMBER_LINES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5225) ' N�mero de lineas
      .Column(GRID_COLUMN_NUMBER_LINES).Width = GRID_COLUMN_WIDTH_NUMBER_LINES

      ' SAS Flag
      .Column(GRID_COLUMN_SAS_FLAG).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096) ' SAS
      .Column(GRID_COLUMN_SAS_FLAG).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2097) ' Tipo de contador 
      .Column(GRID_COLUMN_SAS_FLAG).Width = GRID_COLUMN_WIDTH_SAS_FLAG
      .Column(GRID_COLUMN_SAS_FLAG).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Contract type
      .Column(GRID_COLUMN_CONTRACT_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2247)   ' Contrato
      .Column(GRID_COLUMN_CONTRACT_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2248)   ' Tipo de contrato.
      .Column(GRID_COLUMN_CONTRACT_TYPE).Width = GRID_COLUMN_WIDTH_CONTRACT_TYPE
      .Column(GRID_COLUMN_CONTRACT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ID Contract
      .Column(GRID_COLUMN_CONTRACT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2247) ' Contrato.
      .Column(GRID_COLUMN_CONTRACT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2249) ' ID contrato.
      .Column(GRID_COLUMN_CONTRACT_ID).Width = GRID_COLUMN_WIDTH_CONTRACT_ID
      .Column(GRID_COLUMN_CONTRACT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Order number
      .Column(GRID_COLUMN_ORDER_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2247) ' Contrato.
      .Column(GRID_COLUMN_ORDER_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2250) ' N�mero de pedido.
      .Column(GRID_COLUMN_ORDER_NUMBER).Width = GRID_COLUMN_WIDTH_ORDER_NUMBER
      .Column(GRID_COLUMN_ORDER_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Master Id
      .Column(GRID_COLUMN_MASTER_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_MASTER_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_MASTER_ID).Width = GRID_COLUMN_WIDTH_MASTER_ID
      .Column(GRID_COLUMN_MASTER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet
  ' PURPOSE: Set filter related to terminal type
  '
  '  PARAMS:
  '     - INPUT :
  '           - SqlFilter
  '
  '     - OUTPUT :
  '
  ' RETURNS: Sql condition or report filter parameters
  Private Function SetTerminalTypeFilter(ByVal SqlFilter As Boolean, Optional ByVal AddSQL As Boolean = True) As String

    Dim _result_string As String

    If SqlFilter Then
      _result_string = uc_type_list.GetTerminalTypesSelected(AddSQL)
    Else
      _result_string = uc_type_list.GetTerminalTypesSelectedNames()
    End If

    Return _result_string
  End Function ' SetTerminalTypeFilter
  ' PURPOSE: Set filter related to terminal status
  '
  '  PARAMS:
  '     - INPUT :
  '           - SqlFilter
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function SetTerminalStatusFilter() As String
    Dim _result_string As String
    _result_string = ""

    If chk_status_active.Checked Then
      _result_string &= WSI.Common.TerminalStatus.ACTIVE
    End If

    If chk_status_out_of_order.Checked Then
      _result_string &= IIf(_result_string.Length > 0, ",", "") & WSI.Common.TerminalStatus.OUT_OF_SERVICE
    End If

    If chk_status_retired.Checked Then
      _result_string &= IIf(_result_string.Length > 0, ",", "") & WSI.Common.TerminalStatus.RETIRED
    End If

    _result_string = IIf(_result_string.Length = 0, WSI.Common.TerminalStatus.ACTIVE & "," & _
                                                    WSI.Common.TerminalStatus.OUT_OF_SERVICE & "," & _
                                                    WSI.Common.TerminalStatus.RETIRED _
                                                  , _result_string)

    Return _result_string

  End Function ' SetTerminalStatusFilter
  ' PURPOSE: Set filter related to terminal status
  '
  '  PARAMS:
  '     - INPUT :
  '           - SqlFilter
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetCommonSqlSelectPart() As String

    Dim _sql_select_str As System.Text.StringBuilder
    _sql_select_str = New System.Text.StringBuilder()

    _sql_select_str.AppendLine("SELECT   TLC.TLC_TERMINAL_ID                                                                            ")
    _sql_select_str.AppendLine("       , TE.TE_BASE_NAME                                                                                ")
    _sql_select_str.AppendLine("       , TE.TE_PROVIDER_ID                                                                              ")
    _sql_select_str.AppendLine("       , TE.TE_EXTERNAL_ID                                                                              ")
    _sql_select_str.AppendLine("       , CASE WHEN T3GS_TERMINAL_ID IS NULL THEN TT_NAME ELSE '3GS' END AS TT_NAME_UPDATED              ")
    _sql_select_str.AppendLine("       , CASE TE.TE_STATUS WHEN 0 THEN '" & CLASS_TERMINAL.StatusName(0) & "'                           ")
    _sql_select_str.AppendLine("                           WHEN 1 THEN '" & CLASS_TERMINAL.StatusName(1) & "'                           ")
    _sql_select_str.AppendLine("                           WHEN 2 THEN '" & CLASS_TERMINAL.StatusName(2) & "' END AS STATUS_NAME        ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_PROGRAM, '')                                                                      ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_REGISTRATION_CODE, '')                                                            ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_SERIAL_NUMBER, '')                                                                ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_VENDOR_ID, '')                                                                    ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_RETIREMENT_DATE, '')                                                              ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_RETIREMENT_REQUESTED, '')                                                         ")
    _sql_select_str.AppendLine("       , ISNULL(TE_MULTI_DENOMINATION, TE_DENOMINATION)                                                 ")
    _sql_select_str.AppendLine("       , TE.TE_BLOCKED                                                                                  ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_FLOOR_ID, '')                                                                     ")
    _sql_select_str.AppendLine("       , GT_NAME                                                                                        ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_CABINET_TYPE, '')                                                                 ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_THEORETICAL_PAYOUT, 0)                                                            ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_JACKPOT_CONTRIBUTION_PCT, 0)                                                      ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_CREATION_DATE, '')                                                                ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_ACTIVATION_DATE, '')                                                              ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_REPLACEMENT_DATE, '')                                                             ")
    _sql_select_str.AppendLine("       , ISNULL(BANKS.BK_NAME, '')                                                                      ")
    _sql_select_str.AppendLine("       , ISNULL(AR_NAME, '')                                                                            ")
    _sql_select_str.AppendLine("       , ISNULL(AR_SMOKING, '')                                                                         ")
    _sql_select_str.AppendLine("       , CASE WHEN TE.TE_CLIENT_ID + TE.TE_BUILD_ID > 0 THEN                                            ")
    _sql_select_str.AppendLine("                         ISNULL(CAST(TE.TE_CLIENT_ID AS VARCHAR(10)) + '.', '') +  ISNULL(RIGHT ('000'+ CAST (TE.TE_BUILD_ID AS varchar), 3), '')")
    _sql_select_str.AppendLine("                         ELSE '' END                                                                    ")
    _sql_select_str.AppendLine("       , TLC.TLC_TERMINAL_ID                                                                            ")
    _sql_select_str.AppendLine("       , TE.TE_SAS_FLAGS                                                                                ")
    _sql_select_str.AppendLine("       , CI.CAI_NAME                                                                                    ")
    ' _sql_select_str.AppendLine("       , CASE TE.TE_CONTRACT_TYPE WHEN 0 THEN '" & CLASS_TERMINAL.ContractTypeName(0) & "' ")
    ' _sql_select_str.AppendLine("                                  WHEN 1 THEN '" & CLASS_TERMINAL.ContractTypeName(1) & "' ")
    ' _sql_select_str.AppendLine("                                  WHEN 2 THEN '" & CLASS_TERMINAL.ContractTypeName(2) & "' ")
    ' _sql_select_str.AppendLine("                                  WHEN 3 THEN '" & CLASS_TERMINAL.ContractTypeName(3) & "' ")
    ' _sql_select_str.AppendLine("                                  WHEN 4 THEN '" & CLASS_TERMINAL.ContractTypeName(4) & "' END")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_CONTRACT_ID, '')                                                                  ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_ORDER_NUMBER, '')                                                                 ")
    _sql_select_str.AppendLine("       , TT_TYPE                                                                                        ")
    _sql_select_str.AppendLine("       , TE.TE_STATUS                                                                                   ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_MACHINE_ID, '')                                                                   ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_POSITION, 0)                                                                      ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_TOP_AWARD, 0)                                                                     ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_MAX_BET, 0)                                                                       ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_NUMBER_LINES, '')                                                                 ")
    _sql_select_str.AppendLine("       , ISNULL(TE.TE_GAME_THEME, '')                                                                   ")
    _sql_select_str.AppendLine("       , TE.TE_ASSET_NUMBER                                                                             ")
    _sql_select_str.AppendLine("       , TMD.TMD_DESCRIPTION                                                                            ")
    _sql_select_str.AppendLine("       , TE.TE_MASTER_ID                                                                                ")
    _sql_select_str.AppendLine("       , TS.TS_MACHINE_FLAGS                                                                            ")
    _sql_select_str.AppendLine("       , TE.TE_SAS_FLAGS_USE_SITE_DEFAULT                                                               ")
    ' ColJuegos
    If IsColJuegosEnabled() Then
      _sql_select_str.AppendLine("       , ISNULL(TE.TE_BRAND_CODE, '')                                                                 ")
      _sql_select_str.AppendLine("       , ISNULL(TE.TE_MODEL, '')                                                                      ")
      _sql_select_str.AppendLine("       , ISNULL(TE.TE_MANUFACTURE_YEAR, 0)                                                            ")
      _sql_select_str.AppendLine("       , ISNULL(TE.TE_MET_HOMOLOGATED, 0)                                                             ")
      _sql_select_str.AppendLine("       , ISNULL(TE.TE_BET_CODE, '')                                                                   ")
    End If
    _sql_select_str.AppendLine("  FROM   TERMINALS_LAST_CHANGED AS TLC ")
    _sql_select_str.AppendLine(" INNER   JOIN TERMINALS AS TE        ON TE.TE_TERMINAL_ID   = TLC.TLC_TERMINAL_ID                       ")
    _sql_select_str.AppendLine("  LEFT   JOIN TERMINAL_TYPES              ON TT_TYPE             = TE.TE_TERMINAL_TYPE                  ")
    _sql_select_str.AppendLine("  LEFT   JOIN GAME_TYPES                  ON GT_GAME_TYPE        = TE.TE_GAME_TYPE AND GT_LANGUAGE_ID = " & WSI.Common.Resource.LanguageId)
    _sql_select_str.AppendLine("  LEFT   JOIN BANKS                       ON BK_BANK_ID          = TE.TE_BANK_ID                        ")
    _sql_select_str.AppendLine("  LEFT   JOIN AREAS                       ON AR_AREA_ID          = BK_AREA_ID                           ")
    _sql_select_str.AppendLine("  LEFT   JOIN TERMINALS_3GS               ON T3GS_TERMINAL_ID    = TE.TE_TERMINAL_ID                    ")
    _sql_select_str.AppendLine("  LEFT   JOIN TERMINAL_METER_DELTA AS TMD ON TMD_ID              = TE.TE_METER_DELTA_ID                 ")
    _sql_select_str.AppendLine("  LEFT   JOIN TERMINAL_STATUS  AS TS      ON TS_TERMINAL_ID      = TE.TE_TERMINAL_ID                    ")
    _sql_select_str.AppendLine("  LEFT   JOIN CATALOG_ITEMS  AS CI     ON CAI_ID      = TE.TE_CONTRACT_TYPE                             ")

    Return _sql_select_str.ToString()

  End Function ' GetCommonSqlSelectPart

  ' PURPOSE: change value when hide grid columns ColJuegos
  '
  '  PARAMS:
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub HideGridColumnsColJuegos()

    GRID_COLUMNS -= GRID_NUM_COLUMNS_COLJUEGOS

    GRID_COLUMN_DENOMINATION -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_GAME_TYPE -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_CABINET_TYPE -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_TOP_AWARD -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_MAX_BET -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_TEORETHICAL_PAYOUT -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_PROGRESSIVE_CONTRIBUTION -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_GAME_THEME -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_NUMBER_LINES -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_SAS_FLAG -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_TERMINAL_METER_DELTA -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_SERIAL_NUMBER -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_ASSET_NUMBER -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_CONTRACT_TYPE -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_CONTRACT_ID -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_ORDER_NUMBER -= GRID_NUM_COLUMNS_COLJUEGOS
    GRID_COLUMN_MASTER_ID -= GRID_NUM_COLUMNS_COLJUEGOS

  End Sub ' HideGridColumnsColJuegos

  Private Sub NewOfflineTerminal()
    Dim _frm As frm_terminals_edition

    _frm = New frm_terminals_edition()
    _frm.ShowNewItem()

  End Sub

#End Region ' Private functions

#Region " GUI Reports "
  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(339), m_terminal_name)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(331), m_terminal_id_protocol)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(332), m_report_terminal_blocked)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_report_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(322), m_report_terminal_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(262), m_report_terminal_status)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6056), m_serial_number)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000
    PrintData.FilterValueWidth(3) = 3000

  End Sub ' GUI_ReportFilter
#End Region ' GUI reports

  ' PURPOSE: Select terminal from list and return
  '
  '  PARAMS:
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SelectTerminal()

    Dim _selected_rows() As Integer

    _selected_rows = Me.Grid.SelectedRows()

    m_selected_multi_terminals_id = New List(Of Integer)

    For Each _row As Int32 In _selected_rows
      If m_selection_mode = ENUM_TERMINALS_SELECTION_MODE.ONE Then
        m_selected_terminal_id = Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL_ID).Value

        Exit For
      End If
      m_selected_multi_terminals_id.Add(Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL_ID).Value)
    Next

    ' close screen (as in Cancel)
    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_CANCEL)

  End Sub ' GUI_SelectTerminal

  ' LTC 19-OCT-2016
  ' PURPOSE: Format values for exel file
  '
  '  PARAMS:
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetFormateExcel()
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      ' Serial Number
      If Me.Grid.Cell(_idx_row, GRID_COLUMN_SERIAL_NUMBER).Value <> "" Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_SERIAL_NUMBER).Value = "'" & Me.Grid.Cell(_idx_row, GRID_COLUMN_SERIAL_NUMBER).Value
      End If
    Next

  End Sub

  ' LTC 19-OCT-2016
  ' PURPOSE: Format values for exel file
  '
  '  PARAMS:
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RemoveFormatExcel()
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      ' Serial Number
      If Me.Grid.Cell(_idx_row, GRID_COLUMN_SERIAL_NUMBER).Value <> "" Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_SERIAL_NUMBER).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_SERIAL_NUMBER).Value.Replace("'", "")
      End If
    Next

  End Sub

End Class ' frm_terminals
