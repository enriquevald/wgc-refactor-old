<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_collection_by_machine_and_denomination_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.uc_denominations = New GUI_Controls.uc_checked_list
    Me.gb_show = New System.Windows.Forms.GroupBox
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.cb_total = New System.Windows.Forms.CheckBox
    Me.cb_detail = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_show.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_show)
    Me.panel_filter.Controls.Add(Me.uc_denominations)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1174, 183)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_denominations, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_show, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 187)
    Me.panel_data.Size = New System.Drawing.Size(1174, 483)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1168, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1168, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(269, 2)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 182)
    Me.uc_pr_list.TabIndex = 2
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(260, 81)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_denominations
    '
    Me.uc_denominations.GroupBoxText = "xCheckedList"
    Me.uc_denominations.Location = New System.Drawing.Point(599, 6)
    Me.uc_denominations.m_resize_width = 332
    Me.uc_denominations.multiChoice = True
    Me.uc_denominations.Name = "uc_denominations"
    Me.uc_denominations.SelectedIndexes = New Integer(-1) {}
    Me.uc_denominations.SelectedIndexesList = ""
    Me.uc_denominations.SelectedIndexesListLevel2 = ""
    Me.uc_denominations.SelectedValuesArray = New String(-1) {}
    Me.uc_denominations.SelectedValuesList = ""
    Me.uc_denominations.SetLevels = 2
    Me.uc_denominations.Size = New System.Drawing.Size(332, 173)
    Me.uc_denominations.TabIndex = 3
    Me.uc_denominations.ValuesArray = New String(-1) {}
    '
    'gb_show
    '
    Me.gb_show.Controls.Add(Me.chk_terminal_location)
    Me.gb_show.Controls.Add(Me.cb_total)
    Me.gb_show.Controls.Add(Me.cb_detail)
    Me.gb_show.Location = New System.Drawing.Point(6, 89)
    Me.gb_show.Name = "gb_show"
    Me.gb_show.Size = New System.Drawing.Size(260, 90)
    Me.gb_show.TabIndex = 1
    Me.gb_show.TabStop = False
    Me.gb_show.Text = "xShow"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(11, 20)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(238, 17)
    Me.chk_terminal_location.TabIndex = 0
    Me.chk_terminal_location.Text = "xTerminals location"
    '
    'cb_total
    '
    Me.cb_total.Location = New System.Drawing.Point(11, 64)
    Me.cb_total.Name = "cb_total"
    Me.cb_total.Size = New System.Drawing.Size(238, 17)
    Me.cb_total.TabIndex = 2
    Me.cb_total.Text = "xTotals"
    Me.cb_total.UseVisualStyleBackColor = True
    '
    'cb_detail
    '
    Me.cb_detail.Location = New System.Drawing.Point(11, 42)
    Me.cb_detail.Name = "cb_detail"
    Me.cb_detail.Size = New System.Drawing.Size(238, 17)
    Me.cb_detail.TabIndex = 1
    Me.cb_detail.Text = "xDetail"
    Me.cb_detail.UseVisualStyleBackColor = True
    '
    'frm_collection_by_machine_and_denomination_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1184, 674)
    Me.Name = "frm_collection_by_machine_and_denomination_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_collection_by_machine_and_denomination_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_show.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_denominations As GUI_Controls.uc_checked_list
  Friend WithEvents gb_show As System.Windows.Forms.GroupBox
  Friend WithEvents cb_total As System.Windows.Forms.CheckBox
  Friend WithEvents cb_detail As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
