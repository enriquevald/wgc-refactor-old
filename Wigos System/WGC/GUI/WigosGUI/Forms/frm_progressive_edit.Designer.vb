<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_progressive_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_progressive_edit))
    Me.ef_contribution = New GUI_Controls.uc_entry_field()
    Me.chk_status = New System.Windows.Forms.CheckBox()
    Me.ef_amount = New GUI_Controls.uc_entry_field()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.uc_terminals_group_filter = New GUI_Controls.uc_terminals_group_filter()
    Me.gb_levels = New System.Windows.Forms.GroupBox()
    Me.btn_update = New GUI_Controls.uc_button()
    Me.btn_del = New GUI_Controls.uc_button()
    Me.btn_add = New GUI_Controls.uc_button()
    Me.dg_levels = New GUI_Controls.uc_grid()
    Me.lbl_porcentage_text = New System.Windows.Forms.Label()
    Me.lbl_term_particip = New System.Windows.Forms.Label()
    Me.lbl_progres_otorgado = New System.Windows.Forms.Label()
    Me.ef_brand = New GUI_Controls.uc_entry_field()
    Me.ef_server = New GUI_Controls.uc_entry_field()
    Me.chk_multiseat = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_levels.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.chk_multiseat)
    Me.panel_data.Controls.Add(Me.ef_server)
    Me.panel_data.Controls.Add(Me.ef_brand)
    Me.panel_data.Controls.Add(Me.lbl_progres_otorgado)
    Me.panel_data.Controls.Add(Me.lbl_term_particip)
    Me.panel_data.Controls.Add(Me.lbl_porcentage_text)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_filter)
    Me.panel_data.Controls.Add(Me.ef_contribution)
    Me.panel_data.Controls.Add(Me.chk_status)
    Me.panel_data.Controls.Add(Me.ef_amount)
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Controls.Add(Me.gb_levels)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(559, 602)
    '
    'ef_contribution
    '
    Me.ef_contribution.DoubleValue = 0.0R
    Me.ef_contribution.IntegerValue = 0
    Me.ef_contribution.IsReadOnly = False
    Me.ef_contribution.Location = New System.Drawing.Point(133, 34)
    Me.ef_contribution.Name = "ef_contribution"
    Me.ef_contribution.PlaceHolder = Nothing
    Me.ef_contribution.ShortcutsEnabled = True
    Me.ef_contribution.Size = New System.Drawing.Size(189, 24)
    Me.ef_contribution.SufixText = "Sufix Text"
    Me.ef_contribution.SufixTextVisible = True
    Me.ef_contribution.TabIndex = 3
    Me.ef_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_contribution.TextValue = ""
    Me.ef_contribution.TextWidth = 108
    Me.ef_contribution.Value = ""
    Me.ef_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_status
    '
    Me.chk_status.Location = New System.Drawing.Point(2, 38)
    Me.chk_status.Name = "chk_status"
    Me.chk_status.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_status.Size = New System.Drawing.Size(125, 17)
    Me.chk_status.TabIndex = 2
    Me.chk_status.Text = "xStatus"
    Me.chk_status.UseVisualStyleBackColor = True
    '
    'ef_amount
    '
    Me.ef_amount.DoubleValue = 0.0R
    Me.ef_amount.IntegerValue = 0
    Me.ef_amount.IsReadOnly = True
    Me.ef_amount.Location = New System.Drawing.Point(362, 34)
    Me.ef_amount.Name = "ef_amount"
    Me.ef_amount.PlaceHolder = Nothing
    Me.ef_amount.ShortcutsEnabled = True
    Me.ef_amount.Size = New System.Drawing.Size(194, 24)
    Me.ef_amount.SufixText = "Sufix Text"
    Me.ef_amount.SufixTextVisible = True
    Me.ef_amount.TabIndex = 4
    Me.ef_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount.TextValue = ""
    Me.ef_amount.Value = ""
    Me.ef_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(3, 3)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.ShortcutsEnabled = True
    Me.ef_name.Size = New System.Drawing.Size(556, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 108
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_terminals_group_filter
    '
    Me.uc_terminals_group_filter.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_filter.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_filter.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_filter.HeightOnExpanded = 300
    Me.uc_terminals_group_filter.Ignoreclick = False
    Me.uc_terminals_group_filter.ListEnabled = False
    Me.uc_terminals_group_filter.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_filter.Location = New System.Drawing.Point(0, 223)
    Me.uc_terminals_group_filter.Name = "uc_terminals_group_filter"
    Me.uc_terminals_group_filter.SelectedIndex = 0
    Me.uc_terminals_group_filter.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_filter.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_filter.ShowGroupBoxLine = True
    Me.uc_terminals_group_filter.ShowGroups = Nothing
    Me.uc_terminals_group_filter.ShowNodeAll = False
    Me.uc_terminals_group_filter.Size = New System.Drawing.Size(387, 54)
    Me.uc_terminals_group_filter.TabIndex = 7
    Me.uc_terminals_group_filter.ThreeStateCheckMode = True
    '
    'gb_levels
    '
    Me.gb_levels.Controls.Add(Me.btn_update)
    Me.gb_levels.Controls.Add(Me.btn_del)
    Me.gb_levels.Controls.Add(Me.btn_add)
    Me.gb_levels.Controls.Add(Me.dg_levels)
    Me.gb_levels.Location = New System.Drawing.Point(0, 288)
    Me.gb_levels.Name = "gb_levels"
    Me.gb_levels.Size = New System.Drawing.Size(559, 314)
    Me.gb_levels.TabIndex = 0
    Me.gb_levels.TabStop = False
    Me.gb_levels.Text = "xLevels"
    '
    'btn_update
    '
    Me.btn_update.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_update.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_update.Location = New System.Drawing.Point(426, 284)
    Me.btn_update.Name = "btn_update"
    Me.btn_update.Size = New System.Drawing.Size(90, 30)
    Me.btn_update.TabIndex = 23
    Me.btn_update.ToolTipped = False
    Me.btn_update.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_del
    '
    Me.btn_del.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(264, 284)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(63, 21)
    Me.btn_del.TabIndex = 22
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'btn_add
    '
    Me.btn_add.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(97, 284)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(63, 21)
    Me.btn_add.TabIndex = 21
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'dg_levels
    '
    Me.dg_levels.CurrentCol = -1
    Me.dg_levels.CurrentRow = -1
    Me.dg_levels.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_levels.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_levels.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_levels.Location = New System.Drawing.Point(3, 17)
    Me.dg_levels.Name = "dg_levels"
    Me.dg_levels.PanelRightVisible = False
    Me.dg_levels.Redraw = True
    Me.dg_levels.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_levels.Size = New System.Drawing.Size(553, 261)
    Me.dg_levels.Sortable = False
    Me.dg_levels.SortAscending = True
    Me.dg_levels.SortByCol = 0
    Me.dg_levels.TabIndex = 8
    Me.dg_levels.ToolTipped = True
    Me.dg_levels.TopRow = -2
    Me.dg_levels.WordWrap = False
    '
    'lbl_porcentage_text
    '
    Me.lbl_porcentage_text.Location = New System.Drawing.Point(324, 34)
    Me.lbl_porcentage_text.Name = "lbl_porcentage_text"
    Me.lbl_porcentage_text.Size = New System.Drawing.Size(19, 25)
    Me.lbl_porcentage_text.TabIndex = 6
    Me.lbl_porcentage_text.Text = "%"
    Me.lbl_porcentage_text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_term_particip
    '
    Me.lbl_term_particip.AutoSize = True
    Me.lbl_term_particip.ForeColor = System.Drawing.Color.Navy
    Me.lbl_term_particip.Location = New System.Drawing.Point(3, 130)
    Me.lbl_term_particip.Name = "lbl_term_particip"
    Me.lbl_term_particip.Size = New System.Drawing.Size(121, 13)
    Me.lbl_term_particip.TabIndex = 7
    Me.lbl_term_particip.Text = "xTERMIN_PROGRES"
    '
    'lbl_progres_otorgado
    '
    Me.lbl_progres_otorgado.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progres_otorgado.Location = New System.Drawing.Point(3, 154)
    Me.lbl_progres_otorgado.Name = "lbl_progres_otorgado"
    Me.lbl_progres_otorgado.Size = New System.Drawing.Size(547, 53)
    Me.lbl_progres_otorgado.TabIndex = 8
    Me.lbl_progres_otorgado.Text = "xLAST_PROGRES_OTORG"
    '
    'ef_brand
    '
    Me.ef_brand.DoubleValue = 0.0R
    Me.ef_brand.IntegerValue = 0
    Me.ef_brand.IsReadOnly = False
    Me.ef_brand.Location = New System.Drawing.Point(3, 64)
    Me.ef_brand.Name = "ef_brand"
    Me.ef_brand.PlaceHolder = Nothing
    Me.ef_brand.ShortcutsEnabled = True
    Me.ef_brand.Size = New System.Drawing.Size(261, 24)
    Me.ef_brand.SufixText = "Sufix Text"
    Me.ef_brand.SufixTextVisible = True
    Me.ef_brand.TabIndex = 4
    Me.ef_brand.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_brand.TextValue = ""
    Me.ef_brand.TextWidth = 108
    Me.ef_brand.Value = ""
    Me.ef_brand.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_server
    '
    Me.ef_server.DoubleValue = 0.0R
    Me.ef_server.IntegerValue = 0
    Me.ef_server.IsReadOnly = False
    Me.ef_server.Location = New System.Drawing.Point(295, 65)
    Me.ef_server.Name = "ef_server"
    Me.ef_server.PlaceHolder = Nothing
    Me.ef_server.ShortcutsEnabled = True
    Me.ef_server.Size = New System.Drawing.Size(261, 24)
    Me.ef_server.SufixText = "Sufix Text"
    Me.ef_server.SufixTextVisible = True
    Me.ef_server.TabIndex = 5
    Me.ef_server.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_server.TextValue = ""
    Me.ef_server.TextWidth = 108
    Me.ef_server.Value = ""
    Me.ef_server.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_multiseat
    '
    Me.chk_multiseat.Location = New System.Drawing.Point(113, 89)
    Me.chk_multiseat.Name = "chk_multiseat"
    Me.chk_multiseat.Size = New System.Drawing.Size(437, 43)
    Me.chk_multiseat.TabIndex = 6
    Me.chk_multiseat.Text = "xMultiseat"
    '
    'frm_progressive_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(657, 610)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_progressive_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_progressive_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_levels.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents chk_status As System.Windows.Forms.CheckBox
  Friend WithEvents ef_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_terminals_group_filter As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents gb_levels As System.Windows.Forms.GroupBox
  Friend WithEvents dg_levels As GUI_Controls.uc_grid
  Friend WithEvents lbl_porcentage_text As System.Windows.Forms.Label
  Friend WithEvents lbl_term_particip As System.Windows.Forms.Label
  Friend WithEvents lbl_progres_otorgado As System.Windows.Forms.Label
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_update As GUI_Controls.uc_button
  Friend WithEvents ef_server As GUI_Controls.uc_entry_field
  Friend WithEvents ef_brand As GUI_Controls.uc_entry_field
  Friend WithEvents chk_multiseat As System.Windows.Forms.CheckBox
End Class
