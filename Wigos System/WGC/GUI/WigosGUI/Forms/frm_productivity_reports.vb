'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_productivity_reports
' DESCRIPTION:   Reports of productivity
' AUTHOR:        Daniel Dom�nguez
' CREATION DATE: 14-MAR-2012

'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 14-MAR-2012 DDM    Initial version.
' 04-APR-2012 JCM    Literal string "Multijuego" changed to NLS Multigame.
' 17-SEP-2012 JAB    Timeout Exception.
' 19-NOV-2013 LJM    Changes for auditing forms.
' 04-DEC-2013 LJM	   Changes to use the base function to fulfill tabs with grids.
' 29-MAY-2014 JAB    Added permissions: To buttons "Excel" and "Print".
' 11-JUL-2014 XIT    Added Multilanguage Support
' 26-MAR-2015 ANM    Calling GetProviderIdListSelected always returns a query
' 23-JUN-2015 DCS    Fixed Bug #WIG-2454: Statistics reports: Error displaying cloned terminals
' 16-MAR-2016 SDS    Item 10248: Vendor Filter Include
'-------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.frm_base_sel
Imports System.Data.SqlClient

Public Class frm_productivity_reports
  Inherits GUI_Controls.frm_base_print

#Region " Constants "

  Private Const SQL_COLUMN_DATE As String = "TD_DATE"
  Private Const SQL_COLUMN_TERMINAL_NAME As String = "TE_NAME"
  Private Const SQL_COLUMN_BANK As String = "BK_NAME"
  Private Const SQL_COLUMN_PROVIDER As String = "PV_NAME"
  Private Const SQL_COLUMN_TG_GAME As String = "TG_GAME"
  Private Const SQL_COLUMN_AR_NAME As String = "AR_NAME"
  Private Const SQL_COLUMN_AR_SMOKING As String = "AR_SMOKING"
  Private Const SQL_COLUMN_TD_TYPE As String = "TE_TYPE_NAME"
  Private Const SQL_COLUMN_TERMINALS_NUMBER As String = "TD_NUM_TERMS"
  Private Const SQL_COLUMN_COIN_IN As String = "TD_COIN_IN"
  Private Const SQL_COLUMN_COIN_OUT As String = "TD_COIN_OUT"
  Private Const SQL_COLUMN_NETWIN As String = "AD_NETWIN"
  Private Const SQL_COLUMN_PC_NETWIN As String = "AD_PC_NETWIN"
  Private Const SQL_COLUMN_COIN_IN_MAQ As String = "AD_COIN_IN_MAQ"
  Private Const SQL_COLUMN_COIN_OUT_MAQ As String = "AD_COIN_OUT_MAQ"
  Private Const SQL_COLUMN_NETWIN_MAQ As String = "AD_NETWIN_MAQ"
  Private Const SQL_COLUMN_COUPON_IN As String = "TD_COUPON_IN"
  Private Const SQL_COLUMN_COUPON_OUT As String = "TD_COUPON_OUT"
  Private Const SQL_COLUMN_CI_FLOOR_INDEX As String = "AD_CI_FLOOR_INDEX"
  Private Const SQL_COLUMN_WIN_FLOOR_INDEX As String = "AD_WIN_FLOOR_INDEX"
  Private Const SQL_COLUMN_GAME_INDEX As String = "AD_GAME_INDEX"
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As String = "TD_PROGRESSIVE_PROVISION_AMOUNT"
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 As String = "TD_PROGRESSIVE_JACKPOT_AMOUNT_0"
  Private Const SQL_COLUMN_MASTER_TERMINALS_NUMBER As String = "TD_NUM_MASTER_TERMS"

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_BANK As Integer = 2
  Private Const GRID_COLUMN_PROVIDER As Integer = 3
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 4
  Private Const GRID_COLUMN_TG_GAME As Integer = 5
  Private Const GRID_COLUMN_AR_NAME As Integer = 6
  Private Const GRID_COLUMN_AR_SMOKING As Integer = 7
  Private Const GRID_COLUMN_TD_TYPE As Integer = 8
  Private Const GRID_COLUMN_TERMINALS_NUMBER As Integer = 9
  Private Const GRID_COLUMN_COIN_IN As Integer = 10
  Private Const GRID_COLUMN_COIN_OUT As Integer = 11
  Private Const GRID_COLUMN_NETWIN As Integer = 12
  Private Const GRID_COLUMN_PC_NETWIN As Integer = 13
  Private Const GRID_COLUMN_COIN_IN_MAQ As Integer = 14
  Private Const GRID_COLUMN_COIN_OUT_MAQ As Integer = 15
  Private Const GRID_COLUMN_NETWIN_MAQ As Integer = 16
  Private Const GRID_COLUMN_COUPON_IN As Integer = 17
  Private Const GRID_COLUMN_COUPON_OUT As Integer = 18
  Private Const GRID_COLUMN_CI_FLOOR_INDEX As Integer = 19
  Private Const GRID_COLUMN_WIN_FLOOR_INDEX As Integer = 20
  Private Const GRID_COLUMN_CI_GAME_INDEX As Integer = 21

  Private Const GRID_COLUMNS As Integer = 22
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' dataset relations 
  Private Const DS_RELATION_BANK As String = "BANKS_BT"
  Private Const DS_RELATION_GAME As String = "GAMES_GT"

  ' table names
  Private Const TABLE_DAY As String = "DAY"
  Private Const TABLE_TERMINAL_BANK As String = "TERMINAL_BANK"
  Private Const TABLE_BANK As String = "BANK"
  Private Const TABLE_AREA As String = "AREA"
  Private Const TABLE_ZONE As String = "ZONE"
  Private Const TABLE_TERMINAL_TYPE As String = "TERMINAL_TYPE"
  Private Const TABLE_GAME_PROVIDER As String = "GAME_PROVIDER"
  Private Const TABLE_PROVIDER As String = "PROVIDER"

  'index grid in tabpage
  Private Const IDX_GRID_TABPAGE As Integer = 0
  Private Const IDX_LABEL_TABPAGE As Integer = 1

  'Vendor Filter length
  Private Const LEN_TERMINAL_NAME As Integer = 40

  ' dbnull value
  Private Const DBNULL_VALUE As String = "---"

  Private Const FORM_DB_MIN_VERSION As Short = 219

#End Region ' Constants

#Region " Members "  ' Members

  Private m_ds As DataSet

  ' array for loaded tables
  Private m_loaded_tables As Boolean()

  Private m_list_tablename_ds As String()
  Private m_order_and_show_tables As String(,)
  Private m_tables_show As New ArrayList()

  'relations DS
  Private m_relation As String
  'order table
  Private m_field_order As String

  'filter
  Private m_date_to As DateTime
  Private m_date_from As DateTime
  Private m_terminals As String
  Private m_visible_header As Hashtable

  Private m_print_datetime As Date

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PRODUCTIVITY_REPORTS
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    ' - Form title
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(270)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)

    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    'Vendor Filter
    Me.ef_vendor_id.Text = GLB_NLS_GUI_AUDITOR.GetString(344)                  ' 344 "Vendedor"
    Me.ef_vendor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_TERMINAL_NAME)

    Call SetDefaultMembers()

    ' Create TabPages, Grids and Labels.
    For _i As Int16 = 0 To (m_order_and_show_tables.Length() / 3) - 1
      Call CreateDataGridInTabControl(New GUI_Controls.uc_grid(), m_order_and_show_tables(_i, 1), _
                              m_order_and_show_tables(_i, 0), True, m_order_and_show_tables(_i, 2), tab_productivity)
    Next

    Call GUI_StyleSheet(0, Me.tab_productivity.SelectedTab.Controls(IDX_GRID_TABPAGE))

    '' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ExecuteQuery()

    Dim _sql_command As SqlCommand
    Dim _str_sql As String
    Dim _initial_time As Date
    Dim _closing_time As Integer
    Dim _where_terminals As String

    ' Check the filter 
    If Not GUI_FilterCheck() Then
      Return
    End If

    m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

    Call SetDefaultMembers()

    'FILTERS
    'provider/terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(2007, 1, 1, _closing_time, 0, 0)
    If (uc_pr_list.OneTerminalChecked) Then
      'SDS  16-Mar-2016:Vendor Filter 
      If Me.ef_vendor_id.Value.Trim() <> "" Then
        _where_terminals = " SELECT TE_TERMINAL_ID " & _
                           " FROM TERMINALS " & _
                           " WHERE TE_TERMINAL_ID IN " & uc_pr_list.GetProviderIdListSelectedWithVendor(Me.ef_vendor_id.Value.Trim())
      Else
        _where_terminals = " SELECT TE_TERMINAL_ID " & _
                           " FROM TERMINALS " & _
                           " WHERE TE_TERMINAL_ID IN " & uc_pr_list.GetProviderIdListSelected()
      End If
    Else
      If Me.ef_vendor_id.Value.Trim() <> "" Then
        _where_terminals = uc_pr_list.GetProviderIdListSelectedWithVendor(Me.ef_vendor_id.Value.Trim())
      Else
        _where_terminals = uc_pr_list.GetProviderIdListSelected()
      End If
    End If


    'date
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    'm_date_from = Me.uc_dsl.FromDate
    Me.m_date_to = GetToTime()

    _str_sql = SqlQuery.CommandText("ProductivityReport")
    _sql_command = New SqlCommand()

    ' Add parameters for SQL

    _sql_command.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = Me.m_date_from
    _sql_command.Parameters.Add("@pFinDate", SqlDbType.DateTime).Value = Me.m_date_to
    _sql_command.Parameters.Add("@pBaseDate", SqlDbType.DateTime).Value = _initial_time
    _sql_command.Parameters.Add("@pSql_txt", SqlDbType.NVarChar).Value = _where_terminals
    _sql_command.Parameters.Add("@pMultigame", SqlDbType.NVarChar).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634)
    _sql_command.Parameters.Add("@pGamingTerminalTypeList", SqlDbType.NVarChar).Value = WSI.Common.Misc.GamingTerminalTypeListToString()
    _sql_command.Parameters.Add("@pLanguageId", SqlDbType.Int).Value = WSI.Common.Resource.LanguageId

    _sql_command.CommandText = _str_sql

    Try

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_da As New SqlClient.SqlDataAdapter(_sql_command)
          ' JAB 17-SEP-2012: Timeout Exception.
          _sql_da.SelectCommand.CommandTimeout = 100
          _db_trx.Fill(_sql_da, m_ds)
        End Using
      End Using

      'add calculated columns
      For _i As Integer = 0 To m_ds.Tables.Count - 1
        m_ds.Tables(_i).Merge(AddReportColumns(m_ds.Tables(_i)))
      Next

      Call SetTablesNames()

      ' Relations en DataSet.
      Call DatasetRelations()

      Me.ShowTable(Me.tab_productivity.SelectedIndex)

      GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

    Catch _ex_sql As SqlException

      GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False

      ' JAB 17-SEP-2012: Timeout Exception.
      If _ex_sql.Number = -2 Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(144), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

    Catch ex As Exception
    End Try

  End Sub ' GUI_ExecuteQuery

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()
        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck


  ' PURPOSE: Filters and sets its value to display in the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(344), ef_vendor_id.Value)
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition

    _prev_cursor = Me.Cursor
    Cursor = Cursors.WaitCursor

    Try

      Call LoadGrids(True)

      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime

        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = GLB_NLS_GUI_STATISTICS.GetString(270)
        For Each _tab_page As TabPage In Me.tab_productivity.TabPages
          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.Title = _tab_page.Controls(IDX_LABEL_TABPAGE).Text
          _uc_grid_def.UcGrid = _tab_page.Controls(IDX_GRID_TABPAGE)
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        Next
        .ListOfSheets.Add(_sheet_layout)
      End With
      _report_layout.GUI_GenerateExcel()

    Finally
      Call LoadGrids(False)
      Cursor = _prev_cursor
    End Try
  End Sub ' GUI_ExcelResultList

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Ge to time from user control 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetToTime() As Date

    Dim _to_time As Date = New Date(Me.uc_dsl.ToDate.Year, Me.uc_dsl.ToDate.Month, Me.uc_dsl.ToDate.Day, Me.uc_dsl.ClosingTime, 0, 0)
    Dim _to_date_default As Date = WSI.Common.Misc.TodayOpening().AddDays(1)

    If Me.uc_dsl.ToDateSelected Then
      If _to_time > _to_date_default Then
        _to_time = _to_date_default
      End If
    Else
      _to_time = _to_date_default
    End If

    Return _to_time

  End Function ' GetToTime

  ' PURPOSE: Establishing tables names from dataset
  '
  '  PARAMS:
  '
  '
  ' RETURNS:
  '     - None
  Private Sub SetTablesNames()

    If m_ds.Tables.Count = m_list_tablename_ds.Length Then

      ' set tables names
      For _i As Int16 = 0 To m_ds.Tables.Count - 1
        m_ds.Tables(_i).TableName = m_list_tablename_ds(_i)
      Next

      ' save table pointers
      For _i As Int16 = 0 To (m_order_and_show_tables.Length() / 3) - 1
        For _j As Int16 = 0 To m_list_tablename_ds.Length() - 1
          If m_order_and_show_tables(_i, 0) = m_list_tablename_ds(_j) Then
            m_tables_show.Add(m_order_and_show_tables(_i, 0))
            Exit For
          End If
        Next
      Next

    End If

  End Sub ' setTablesNames

  ' PURPOSE: Establishing relations from dataset
  '
  '  PARAMS:
  '
  '
  ' RETURNS:
  '     - None
  Private Sub DatasetRelations()
    Dim _column_parent As DataColumn
    Dim _column_child As DataColumn
    Dim _data_relation As DataRelation

    If IsNothing(m_ds) Then
      Return
    Else
      'define: Relations from dataset

      m_ds.Relations.Clear()
      'Relation: BANK with TERMINAL
      If Not IsNothing(m_ds.Tables(TABLE_BANK)) AndAlso Not IsNothing(m_ds.Tables(TABLE_TERMINAL_BANK)) Then
        _column_parent = m_ds.Tables(TABLE_BANK).Columns(SQL_COLUMN_BANK)
        _column_child = m_ds.Tables(TABLE_TERMINAL_BANK).Columns(SQL_COLUMN_BANK)
        _data_relation = New DataRelation(DS_RELATION_BANK, _column_parent, _column_child, True)
        m_ds.Relations.Add(_data_relation)
      End If

      'Relation: GAMES with PROVIDER 
      If Not IsNothing(m_ds.Tables(TABLE_PROVIDER)) AndAlso Not IsNothing(m_ds.Tables(TABLE_GAME_PROVIDER)) Then
        _column_parent = m_ds.Tables(TABLE_PROVIDER).Columns(SQL_COLUMN_PROVIDER)
        _column_child = m_ds.Tables(TABLE_GAME_PROVIDER).Columns(SQL_COLUMN_PROVIDER)
        _data_relation = New DataRelation(DS_RELATION_GAME, _column_parent, _column_child, True)
        m_ds.Relations.Add(_data_relation)
      End If
    End If

  End Sub ' DatasetRelations

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date

    ' uc date control
    _final_time = WSI.Common.Misc.TodayOpening()
    _closing_time = _final_time.Hour

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.ef_vendor_id.Value = String.Empty

    ' uc provider-terminal
    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Set default values to Members
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultMembers()

    ' reset load tables
    m_loaded_tables = New Boolean() {False, False, False, False, False, False, False, False, False}

    ' reset dataset
    m_ds = New DataSet()

    ' filter
    m_date_from = New Date()
    m_date_to = New Date
    m_terminals = String.Empty

    ' Define: table name from dataset
    If IsNothing(m_list_tablename_ds) Then
      m_list_tablename_ds = New String() {TABLE_DAY, TABLE_TERMINAL_BANK, TABLE_BANK, TABLE_AREA, TABLE_ZONE, _
                                        TABLE_TERMINAL_TYPE, TABLE_PROVIDER, TABLE_GAME_PROVIDER}
    End If

    ' Defines: tabs to show (order, hide table, position,..)
    'position 0 = table name,
    'position 1 = Page text 
    'position 2 = Label text 
    If IsNothing(m_order_and_show_tables) Then
      m_order_and_show_tables = New String(,) _
                               {{TABLE_DAY, GLB_NLS_GUI_STATISTICS.GetString(281), GLB_NLS_GUI_STATISTICS.GetString(294)}, _
                                {TABLE_TERMINAL_BANK, GLB_NLS_GUI_STATISTICS.GetString(273), GLB_NLS_GUI_STATISTICS.GetString(287)}, _
                                {TABLE_GAME_PROVIDER, GLB_NLS_GUI_STATISTICS.GetString(282), GLB_NLS_GUI_STATISTICS.GetString(292)}, _
                                {TABLE_BANK, GLB_NLS_GUI_CONTROLS.GetString(427), GLB_NLS_GUI_STATISTICS.GetString(293)}, _
                                {TABLE_PROVIDER, GLB_NLS_GUI_STATISTICS.GetString(446), GLB_NLS_GUI_STATISTICS.GetString(288)}, _
                                {TABLE_ZONE, GLB_NLS_GUI_STATISTICS.GetString(274), GLB_NLS_GUI_STATISTICS.GetString(289)}, _
                                {TABLE_AREA, GLB_NLS_GUI_CONTROLS.GetString(426), GLB_NLS_GUI_STATISTICS.GetString(291)}, _
                                {TABLE_TERMINAL_TYPE, GLB_NLS_GUI_STATISTICS.GetString(271), GLB_NLS_GUI_STATISTICS.GetString(290)}}
    End If


    ' Defines:  not visible columns
    If IsNothing(Me.m_visible_header) Then
      Me.m_visible_header = New Hashtable()

      Me.m_visible_header.Add(TABLE_DAY, New Integer() {GRID_COLUMN_BANK, GRID_COLUMN_PROVIDER, GRID_COLUMN_TERMINAL_NAME, _
                                                  GRID_COLUMN_TG_GAME, GRID_COLUMN_AR_NAME, GRID_COLUMN_AR_SMOKING, _
                                                  GRID_COLUMN_TD_TYPE, GRID_COLUMN_CI_FLOOR_INDEX, _
                                                  GRID_COLUMN_WIN_FLOOR_INDEX, GRID_COLUMN_CI_GAME_INDEX})

      Me.m_visible_header.Add(TABLE_TERMINAL_BANK, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_AR_NAME, GRID_COLUMN_AR_SMOKING, _
                                                  GRID_COLUMN_TD_TYPE})

      Me.m_visible_header.Add(TABLE_GAME_PROVIDER, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_BANK, GRID_COLUMN_TERMINAL_NAME, _
                                                GRID_COLUMN_AR_NAME, GRID_COLUMN_AR_SMOKING, GRID_COLUMN_TD_TYPE})

      Me.m_visible_header.Add(TABLE_BANK, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_PROVIDER, GRID_COLUMN_TERMINAL_NAME, _
                                                GRID_COLUMN_TG_GAME, GRID_COLUMN_AR_NAME, GRID_COLUMN_AR_SMOKING, _
                                                GRID_COLUMN_TD_TYPE, GRID_COLUMN_CI_FLOOR_INDEX, _
                                                GRID_COLUMN_WIN_FLOOR_INDEX, GRID_COLUMN_CI_GAME_INDEX})

      Me.m_visible_header.Add(TABLE_PROVIDER, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_BANK, GRID_COLUMN_TERMINAL_NAME, _
                                                GRID_COLUMN_AR_NAME, GRID_COLUMN_AR_SMOKING, GRID_COLUMN_TD_TYPE, _
                                                GRID_COLUMN_TG_GAME, GRID_COLUMN_CI_FLOOR_INDEX, _
                                                GRID_COLUMN_WIN_FLOOR_INDEX, GRID_COLUMN_CI_GAME_INDEX})

      Me.m_visible_header.Add(TABLE_AREA, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_BANK, GRID_COLUMN_PROVIDER, _
                                                GRID_COLUMN_TERMINAL_NAME, GRID_COLUMN_TG_GAME, GRID_COLUMN_TD_TYPE, _
                                                 GRID_COLUMN_CI_FLOOR_INDEX, _
                                                GRID_COLUMN_WIN_FLOOR_INDEX, GRID_COLUMN_CI_GAME_INDEX})

      Me.m_visible_header.Add(TABLE_ZONE, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_BANK, GRID_COLUMN_PROVIDER, _
                                                GRID_COLUMN_TERMINAL_NAME, GRID_COLUMN_AR_NAME, GRID_COLUMN_TG_GAME, _
                                                GRID_COLUMN_TD_TYPE, _
                                                GRID_COLUMN_CI_FLOOR_INDEX, GRID_COLUMN_WIN_FLOOR_INDEX, GRID_COLUMN_CI_GAME_INDEX})

      Me.m_visible_header.Add(TABLE_TERMINAL_TYPE, New Integer() {GRID_COLUMN_DATE, GRID_COLUMN_BANK, GRID_COLUMN_PROVIDER, _
                                                GRID_COLUMN_TERMINAL_NAME, GRID_COLUMN_AR_NAME, GRID_COLUMN_TG_GAME, _
                                                GRID_COLUMN_AR_SMOKING, GRID_COLUMN_WIN_FLOOR_INDEX, _
                                                GRID_COLUMN_CI_GAME_INDEX, GRID_COLUMN_CI_FLOOR_INDEX})



    End If



  End Sub ' SetDefaultMembers

  ' PURPOSE: Show the grid according with tabpage selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - IndexTable Index the tabpage selected
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ShowTable(ByVal IndexTable As Integer)
    Dim _table As DataTable

    If (IsNothing(m_loaded_tables) Or (Me.m_loaded_tables(IndexTable))) Then
      Return
    Else
      _table = Nothing
      ' Load style
      Call GUI_StyleSheet(IndexTable, Me.tab_productivity.TabPages(IndexTable).Controls(IDX_GRID_TABPAGE))
      ' Load Order and Relation
      Call OrderAndRelation(IndexTable)
      If (m_ds.Tables.Count > 0) Then
        TableToGrid(m_ds.Tables(m_tables_show(IndexTable)), Me.tab_productivity.TabPages(IndexTable).Controls(IDX_GRID_TABPAGE))
        ' Update the array (tables shown)
        m_loaded_tables(IndexTable) = True
      End If

    End If
  End Sub ' ShowTable

  ' PURPOSE: Establishing members that define relations
  '
  '  PARAMS:
  '     - INPUT:
  '           - IndexTable Index the tabpage selected
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub OrderAndRelation(ByVal IndexTable As Integer)

    Me.m_field_order = String.Empty
    Me.m_relation = String.Empty
    Select Case m_order_and_show_tables(IndexTable, 0)

      Case TABLE_TERMINAL_BANK
        Me.m_field_order = SQL_COLUMN_BANK
        Me.m_relation = DS_RELATION_BANK

      Case TABLE_GAME_PROVIDER
        Me.m_field_order = SQL_COLUMN_PROVIDER
        Me.m_relation = DS_RELATION_GAME

    End Select

  End Sub ' OrderAndRelation

  ' PURPOSE: Add data rows to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Private Sub TableToGrid(ByVal Dt As DataTable, _
                          ByRef Dg As GUI_Controls.uc_grid)
    Dim _total_coin_in As Double
    Dim _total_coin_out As Double
    Dim _total_terminals As Double
    Dim _total_cupon_in As Double
    Dim _total_cupon_out As Double
    Dim _total_netwin As Double
    Dim _redraw As Boolean
    Dim _field_order As String
    Dim p_parent_row As DataRow
    Dim p_last_parent_row As DataRow
    Dim _data_type As String
    Dim p_child As DataRow()

    If (IsNothing(Dt)) Then
      Return
    Else

      _total_coin_in = 0
      _total_coin_out = 0
      _total_terminals = 0
      _total_cupon_in = 0
      _total_cupon_out = 0
      _total_netwin = 0
      p_parent_row = Nothing
      p_last_parent_row = Nothing
      _field_order = String.Empty
      p_child = Nothing
      _data_type = "ROWS"

      Dg.SuspendLayout()
      _redraw = Dg.Redraw
      Dg.Redraw = False
      Try
        For Each _dr As DataRow In Dt.Select(String.Empty, Me.m_field_order)

          ' child relation
          p_child = _dr.GetChildRows(m_relation)
          If p_child.Length > 0 Then
            For _i As Integer = 0 To p_child.Length() - 1
              Call AddDataRow(p_child(_i), Dg, "ROWS", _total_coin_in, _total_coin_out, _total_terminals, _
                              _total_cupon_in, _total_cupon_out, _total_netwin, _dr)
            Next
            Call AddDataRow(_dr, Dg, "SUBTOTAL")
          End If

          ' parent relation  (first subtotal row)
          p_parent_row = Nothing
          p_parent_row = _dr.GetParentRow(m_relation)
          If (IsNothing(p_last_parent_row)) Then
            p_last_parent_row = p_parent_row
          End If
          If (Not IsNothing(p_parent_row) AndAlso Not p_last_parent_row.Equals(p_parent_row)) Then
            Call AddDataRow(p_last_parent_row, Dg, "SUBTOTAL")
          End If
          p_last_parent_row = p_parent_row

          If p_child.Length = 0 Then
            Call AddDataRow(_dr, Dg, "ROWS", _total_coin_in, _total_coin_out, _total_terminals, _total_cupon_in, _total_cupon_out, _total_netwin, p_parent_row)
          End If

        Next

        ' Add last row Subtotal (when is parent)
        If Not IsNothing(p_parent_row) Then
          Call AddDataRow(p_last_parent_row, Dg, "SUBTOTAL")
        End If

        ' Add colum Total     
        Call AddTotalRow(Dg, _total_coin_in, _total_coin_out, _total_terminals, _total_cupon_in, _total_cupon_out, _total_netwin)

      Catch ex As Exception

      Finally
        Dg.ResumeLayout()
        Dg.Redraw = _redraw
      End Try



    End If
  End Sub ' TableToGrid

  ' PURPOSE: Add data row to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           Dr : Add row to datagrid
  '           Dg : Datagrig
  '           Datatype : Data type the row 
  '           Optional Total_coin_in : Variable total coin in
  '           Optional Total_coin_out :Variable total coin out
  '           Optional Total_terminals :Variable total coin terminal number
  '           Optional Total_cupon_in : Variable total copon in
  '           Optional Total_cupon_out : Variable total copon out
  '           Optional Dr_sub_tot : row from subtotal
  ''     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddDataRow(ByVal Dr As DataRow, _
                          ByRef Dg As GUI_Controls.uc_grid, _
                          ByVal Datatype As String, _
                          Optional ByRef Total_coin_in As Decimal = 0, _
                          Optional ByRef Total_coin_out As Decimal = 0, _
                          Optional ByRef Total_terminals As Decimal = 0, _
                          Optional ByRef Total_cupon_in As Decimal = 0, _
                          Optional ByRef Total_cupon_out As Decimal = 0, _
                          Optional ByRef Total_netwin As Decimal = 0, _
                          Optional ByVal Dr_sub_tot As DataRow = Nothing)

    Dim _subt_cupon_out_maq As Double
    Dim _idx_row As Integer
    Dim _value As Decimal

    If (IsNothing(Dg)) Then
      Return
    End If

    'add new row
    Dg.AddRow()
    _idx_row = Dg.NumRows - 1

    Select Case Datatype
      Case "ROWS"

        If IsNothing(Dr_sub_tot) OrElse IsDBNull(Dr_sub_tot(SQL_COLUMN_COIN_IN_MAQ)) Then
          _subt_cupon_out_maq = 0
        Else
          _subt_cupon_out_maq = Dr_sub_tot(SQL_COLUMN_COIN_IN_MAQ)
        End If

        'ROWS
        ' Date 
        If Dr.Table.Columns.Contains(SQL_COLUMN_DATE) Then
          If IsDBNull(Dr(SQL_COLUMN_DATE)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_DATE).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_DATE).Value = GUI_FormatDate(Dr(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_DATE).Value = ""
        End If

        ' Bank
        If Dr.Table.Columns.Contains(SQL_COLUMN_BANK) Then
          If IsDBNull(Dr(SQL_COLUMN_BANK)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_BANK).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_BANK).Value = Dr(SQL_COLUMN_BANK)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_BANK).Value = ""
        End If

        ' Provider
        If Dr.Table.Columns.Contains(SQL_COLUMN_PROVIDER) Then
          If IsDBNull(Dr(SQL_COLUMN_PROVIDER)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = Dr(SQL_COLUMN_PROVIDER)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = ""
        End If

        ' Terminal name
        If Dr.Table.Columns.Contains(SQL_COLUMN_TERMINAL_NAME) Then
          If IsDBNull(Dr(SQL_COLUMN_TERMINAL_NAME)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = Dr(SQL_COLUMN_TERMINAL_NAME)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = ""
        End If

        ' Game
        If Dr.Table.Columns.Contains(SQL_COLUMN_TG_GAME) Then
          If IsDBNull(Dr(SQL_COLUMN_TG_GAME)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_TG_GAME).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_TG_GAME).Value = Dr(SQL_COLUMN_TG_GAME)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_TG_GAME).Value = ""
        End If

        ' Area
        If Dr.Table.Columns.Contains(SQL_COLUMN_AR_NAME) Then
          If IsDBNull(Dr(SQL_COLUMN_AR_NAME)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_AR_NAME).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_AR_NAME).Value = Dr(SQL_COLUMN_AR_NAME)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_AR_NAME).Value = ""
        End If

        ' Smoking
        If Dr.Table.Columns.Contains(SQL_COLUMN_AR_SMOKING) Then
          If IsDBNull(Dr(SQL_COLUMN_AR_SMOKING)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_AR_SMOKING).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_AR_SMOKING).Value = IIf(Dr(SQL_COLUMN_AR_SMOKING), GLB_NLS_GUI_CONTROLS.GetString(428), _
                                                              GLB_NLS_GUI_CONTROLS.GetString(440))
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_AR_SMOKING).Value = ""
        End If

        ' Terminal type (clase)
        If Dr.Table.Columns.Contains(SQL_COLUMN_TD_TYPE) Then
          If IsDBNull(Dr(SQL_COLUMN_TD_TYPE)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_TD_TYPE).Value = DBNULL_VALUE
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_TD_TYPE).Value = Dr(SQL_COLUMN_TD_TYPE)
          End If
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_TD_TYPE).Value = ""
        End If

        ' Coin in game Index
        If Dg.Column(GRID_COLUMN_CI_GAME_INDEX).Width = 0 Then
          Dg.Cell(_idx_row, GRID_COLUMN_CI_GAME_INDEX).Value = String.Empty
        Else
          If IsDBNull(Dr(SQL_COLUMN_COIN_IN_MAQ)) OrElse (Dr(SQL_COLUMN_COIN_IN_MAQ) = 0) OrElse _
             (IsDBNull(_subt_cupon_out_maq) OrElse (_subt_cupon_out_maq = 0)) Then
            Dg.Cell(_idx_row, GRID_COLUMN_CI_GAME_INDEX).Value = GUI_FormatNumber(0, 2)
          Else
            Dg.Cell(_idx_row, GRID_COLUMN_CI_GAME_INDEX).Value = GUI_FormatNumber(Dr(SQL_COLUMN_COIN_IN_MAQ) / _subt_cupon_out_maq, 2)
          End If

        End If

        '' Bank
        'If Dr.Table.Columns.Contains(SQL_COLUMN_MASTER_TERMINALS_NUMBER) Then
        '  If IsDBNull(Dr(SQL_COLUMN_MASTER_TERMINALS_NUMBER)) Then
        '    Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = Dr(SQL_COLUMN_TERMINALS_NUMBER)
        '  Else
        '    Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = Dr(SQL_COLUMN_MASTER_TERMINALS_NUMBER)
        '  End If
        'Else
        '  Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = Dr(SQL_COLUMN_TERMINALS_NUMBER)
        'End If

      Case "SUBTOTAL"
        For _i As Int16 = 1 To GRID_COLUMN_TERMINALS_NUMBER
          If Dg.Column(_i).Width <> 0 Then
            Dg.Cell(_idx_row, _i).Value = GLB_NLS_GUI_INVOICING.GetString(375)  'SUBTOTAL: 
            Exit For
          End If
        Next
        Dg.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
    End Select

    ' Communes columns
    ' Terminals Number
    If Dg.Column(GRID_COLUMN_TERMINALS_NUMBER).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_TERMINALS_NUMBER)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = GUI_FormatNumber(0, 0)
      Else
        If Dr(SQL_COLUMN_TERMINALS_NUMBER) = 0 And Dr.Table.Columns.Contains(SQL_COLUMN_MASTER_TERMINALS_NUMBER) AndAlso Not IsDBNull(Dr(SQL_COLUMN_MASTER_TERMINALS_NUMBER)) Then
          Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = Dr(SQL_COLUMN_MASTER_TERMINALS_NUMBER)
        Else
          Dg.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = GUI_FormatNumber(Dr(SQL_COLUMN_TERMINALS_NUMBER), 0)
          Total_terminals += Dr(SQL_COLUMN_TERMINALS_NUMBER)
        End If
      End If
    End If

    ' Coin in
    If Dg.Column(GRID_COLUMN_COIN_IN).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_COIN_IN).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_COIN_IN)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_IN).Value = GUI_FormatCurrency(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_IN).Value = GUI_FormatCurrency(Dr(SQL_COLUMN_COIN_IN), 2)
        Total_coin_in += Dr(SQL_COLUMN_COIN_IN)
      End If
    End If

    ' Coin out
    If Dg.Column(GRID_COLUMN_COIN_OUT).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_COIN_OUT).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_COIN_OUT)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_OUT).Value = GUI_FormatCurrency(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_OUT).Value = GUI_FormatCurrency(Dr(SQL_COLUMN_COIN_OUT), 2)
        Total_coin_out += Dr(SQL_COLUMN_COIN_OUT)
      End If
    End If


    ' Netwin
    If Dg.Column(GRID_COLUMN_NETWIN).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_NETWIN)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(Dr(SQL_COLUMN_NETWIN), 2)
        Total_netwin += Dr(SQL_COLUMN_NETWIN)
      End If
    End If

    ' % Netwin
    If Dg.Column(GRID_COLUMN_PC_NETWIN).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_PC_NETWIN).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_NETWIN)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_PC_NETWIN).Value = GUI_FormatNumber(0, 2) & "%"
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_PC_NETWIN).Value = GUI_FormatNumber(Dr(SQL_COLUMN_PC_NETWIN), 2) & "%"
      End If
    End If

    ' coin in / term
    If Dg.Column(GRID_COLUMN_COIN_IN_MAQ).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_COIN_IN_MAQ).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_COIN_IN_MAQ)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_IN_MAQ).Value = GUI_FormatCurrency(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_IN_MAQ).Value = GUI_FormatCurrency(Dr(SQL_COLUMN_COIN_IN_MAQ), 2)
      End If
    End If

    ' coin out / term
    If Dg.Column(GRID_COLUMN_COIN_OUT_MAQ).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_COIN_OUT_MAQ).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_COIN_OUT_MAQ)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_OUT_MAQ).Value = GUI_FormatCurrency(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_COIN_OUT_MAQ).Value = GUI_FormatCurrency(Dr(SQL_COLUMN_COIN_OUT_MAQ), 2)
      End If
    End If

    ' netwin / term
    If Dg.Column(GRID_COLUMN_NETWIN_MAQ).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_NETWIN_MAQ).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_NETWIN_MAQ)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_NETWIN_MAQ).Value = GUI_FormatCurrency(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_NETWIN_MAQ).Value = GUI_FormatCurrency(Dr(SQL_COLUMN_NETWIN_MAQ), 2)
      End If
    End If

    ' cupon in
    If Dg.Column(GRID_COLUMN_COUPON_IN).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_COUPON_IN).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_COUPON_IN)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_COUPON_IN).Value = GUI_FormatCurrency(0, 2)
      Else
        _value = Math.Round(Dr(SQL_COLUMN_COUPON_IN), 2, MidpointRounding.AwayFromZero)
        Dg.Cell(_idx_row, GRID_COLUMN_COUPON_IN).Value = GUI_FormatCurrency(_value, 2)
        Total_cupon_in += _value
      End If
    End If

    ' Cupon out
    If Dg.Column(GRID_COLUMN_COUPON_OUT).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_COUPON_OUT).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_COUPON_OUT)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_COUPON_OUT).Value = GUI_FormatCurrency(0, 2)
      Else
        _value = Math.Round(Dr(SQL_COLUMN_COUPON_OUT), 2, MidpointRounding.AwayFromZero)
        Dg.Cell(_idx_row, GRID_COLUMN_COUPON_OUT).Value = GUI_FormatCurrency(_value, 2)
        Total_cupon_out += _value
      End If
    End If

    ' ci - floor index
    If Dg.Column(GRID_COLUMN_CI_FLOOR_INDEX).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_CI_FLOOR_INDEX).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_CI_FLOOR_INDEX)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_CI_FLOOR_INDEX).Value = GUI_FormatNumber(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_CI_FLOOR_INDEX).Value = GUI_FormatNumber(Dr(SQL_COLUMN_CI_FLOOR_INDEX), 2)
      End If
    End If

    ' win - floor index
    If Dg.Column(GRID_COLUMN_WIN_FLOOR_INDEX).Width = 0 Then
      Dg.Cell(_idx_row, GRID_COLUMN_WIN_FLOOR_INDEX).Value = String.Empty
    Else
      If IsDBNull(Dr(SQL_COLUMN_WIN_FLOOR_INDEX)) Then
        Dg.Cell(_idx_row, GRID_COLUMN_WIN_FLOOR_INDEX).Value = GUI_FormatNumber(0, 2)
      Else
        Dg.Cell(_idx_row, GRID_COLUMN_WIN_FLOOR_INDEX).Value = GUI_FormatNumber(Dr(SQL_COLUMN_WIN_FLOOR_INDEX), 2)
      End If
    End If


  End Sub ' AddDataRows

  '------------------------------------------------------------------------------
  ' PURPOSE : Return a DataTable with columns of report
  '
  '  PARAMS :
  '      - INPUT :
  '          - DataTable Table    
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  '      - DataTable
  '   
  Private Function AddReportColumns(ByVal Table As DataTable) As DataTable
    Try
      Dim _netwin_ter_sala As String
      Dim _coin_in_ter_sala As String
      Dim _column_terminals_number As String

      If Table.Columns.Contains(SQL_COLUMN_MASTER_TERMINALS_NUMBER) Then
        _column_terminals_number = SQL_COLUMN_MASTER_TERMINALS_NUMBER
      Else
        _column_terminals_number = SQL_COLUMN_TERMINALS_NUMBER
      End If

      ' PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) as Netwin
      _netwin_ter_sala = "((SUM( " & SQL_COLUMN_COIN_IN & " ) - (SUM( " & SQL_COLUMN_COIN_OUT & " ) + SUM(" & SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT & ") - SUM(" & SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 & ")))/SUM( " & _column_terminals_number & "))"
      _coin_in_ter_sala = "(SUM(" & SQL_COLUMN_COIN_IN & ") / (SUM(" & _column_terminals_number & ")))"

      ' Colum netwin. (coin in - (coin out +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))
      Table.Columns.Add(SQL_COLUMN_NETWIN, Type.GetType("System.Decimal"), _
                        SQL_COLUMN_COIN_IN & " - (" & SQL_COLUMN_COIN_OUT & " + " & SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT & " - " & SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 & ")")

      ' Column % netwin. (netwin/coin in)) * 100
      Table.Columns.Add(SQL_COLUMN_PC_NETWIN, Type.GetType("System.Decimal"), _
                  " IIF( " & SQL_COLUMN_COIN_IN & "  = 0, 0, ((( " & SQL_COLUMN_COIN_IN & " - (" & SQL_COLUMN_COIN_OUT & " + " & SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT & " - " & SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 & ")) /  " & _
                    SQL_COLUMN_COIN_IN & " ) * 100) ) ")

      ' Column coin in / terminals. 
      Table.Columns.Add(SQL_COLUMN_COIN_IN_MAQ, Type.GetType("System.Decimal"), _
                  " IIF( " & _column_terminals_number & "  = 0, 0,  " & SQL_COLUMN_COIN_IN & "  /  " & _column_terminals_number & "  ) ")

      ' Column coin out / terminals. 
      Table.Columns.Add(SQL_COLUMN_COIN_OUT_MAQ, Type.GetType("System.Decimal"), _
                  " IIF( " & _column_terminals_number & "  = 0, 0,  " & SQL_COLUMN_COIN_OUT & "  /  " & _column_terminals_number & "  ) ")

      ' Column netwin / terminals
      Table.Columns.Add(SQL_COLUMN_NETWIN_MAQ, Type.GetType("System.Decimal"), _
                  " IIF( " & _column_terminals_number & "  = 0 , 0 ,(( " & SQL_COLUMN_COIN_IN & " - (" & SQL_COLUMN_COIN_OUT & " + " & SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT & " - " & SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 & ")) /  " & _
                  _column_terminals_number & " ))")

      ' Column Coin in floor index. (( Coin in / terminal)j / ( sum(Coin in) / sum(terminals))
      Table.Columns.Add(SQL_COLUMN_CI_FLOOR_INDEX, Type.GetType("System.Decimal"), _
                  " IIF( " & SQL_COLUMN_COIN_IN_MAQ & "  = 0 Or SUM( " & _column_terminals_number & " ) = 0 OR SUM( " & SQL_COLUMN_COIN_IN & " ) = 0" & _
                  ", 0 ,(  " & SQL_COLUMN_COIN_IN_MAQ & "  /  " & _coin_in_ter_sala & ") )")

      ' Column netwin floor index. (( neetwin / terminal)j / ((sum(Coin in)-sum(Coin out)) / sum(terminals))
      Table.Columns.Add(SQL_COLUMN_WIN_FLOOR_INDEX, Type.GetType("System.Decimal"), _
                  " IIF(SUM( " & _column_terminals_number & " )= 0 OR " & _netwin_ter_sala & "= 0" & _
                  ", 0 ,(" & SQL_COLUMN_NETWIN_MAQ & ") / " & _netwin_ter_sala & ")")

    Catch ex As Exception

    End Try

    Return Table
  End Function ' AddReportColumns

  ' PURPOSE: Add data row GranTotal to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           Dg : Datagrig
  '           Total_coin_in : Variable total coin in
  '           Total_coin_out :Variable total coin out
  '           Total_terminals :Variable total coin terminal number
  '           Total_cupon_in : Variable total copon in
  '           Total_cupon_out : Variable total copon out
  ''     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddTotalRow(ByRef Grid As GUI_Controls.uc_grid, _
                          ByVal Total_coin_in As Decimal, _
                          ByVal Total_coin_out As Decimal, _
                          ByVal Total_terminals As Decimal, _
                          ByVal Total_cupon_in As Decimal, _
                          ByVal Total_cupon_out As Decimal, _
                          ByVal Total_netwin As Decimal)

    Dim _idx_row As Integer

    _idx_row = Grid.NumRows

    Grid.AddRow()
    For _i As Int16 = 1 To GRID_COLUMN_TERMINALS_NUMBER
      If Grid.Column(_i).Width <> 0 Then
        Grid.Cell(_idx_row, _i).Value = GLB_NLS_GUI_INVOICING.GetString(205)  '"TOTAL: "        
        Exit For
      End If
    Next
    Grid.Cell(_idx_row, GRID_COLUMN_TERMINALS_NUMBER).Value = GUI_FormatNumber(Total_terminals, 0)
    Grid.Cell(_idx_row, GRID_COLUMN_COIN_IN).Value = GUI_FormatCurrency(Total_coin_in, 2)
    Grid.Cell(_idx_row, GRID_COLUMN_COIN_OUT).Value = GUI_FormatCurrency(Total_coin_out, 2)
    Grid.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(Total_netwin, 2)

    ' Netwin %
    If Grid.Column(GRID_COLUMN_PC_NETWIN).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_PC_NETWIN).Value = String.Empty
    Else
      If Total_coin_in = 0 Then
        Grid.Cell(_idx_row, GRID_COLUMN_PC_NETWIN).Value = GUI_FormatNumber(0, 2) & "%"
      Else
        Grid.Cell(_idx_row, GRID_COLUMN_PC_NETWIN).Value = GUI_FormatNumber((Total_netwin / Total_coin_in) * 100, 2) & "%"
      End If
    End If

    ' coin in / terminal
    If Grid.Column(GRID_COLUMN_COIN_IN_MAQ).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_COIN_IN_MAQ).Value = String.Empty
    Else
      If (Total_terminals = 0) Then
        Grid.Cell(_idx_row, GRID_COLUMN_COIN_IN_MAQ).Value = GUI_FormatCurrency(0, 2)
      Else
        Grid.Cell(_idx_row, GRID_COLUMN_COIN_IN_MAQ).Value = GUI_FormatCurrency((Total_coin_in / Total_terminals), 2)
      End If
    End If

    ' coin out/ terminal
    If Grid.Column(GRID_COLUMN_COIN_OUT_MAQ).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_COIN_OUT_MAQ).Value = String.Empty
    Else
      If (Total_terminals = 0) Then
        Grid.Cell(_idx_row, GRID_COLUMN_COIN_OUT_MAQ).Value = GUI_FormatCurrency(0, 2)
      Else
        Grid.Cell(_idx_row, GRID_COLUMN_COIN_OUT_MAQ).Value = GUI_FormatCurrency((Total_coin_out / Total_terminals), 2)
      End If
    End If

    ' netwin maquina
    If Grid.Column(GRID_COLUMN_NETWIN_MAQ).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_MAQ).Value = String.Empty
    Else
      If (Total_terminals = 0) Then
        Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_MAQ).Value = GUI_FormatCurrency(0, 2)
      Else
        Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_MAQ).Value = GUI_FormatCurrency((Total_netwin / Total_terminals), 2)
      End If
    End If

    ' coupon in
    If Grid.Column(GRID_COLUMN_COUPON_IN).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_COUPON_IN).Value = String.Empty
    Else
      If (Total_terminals = 0) Then
        Grid.Cell(_idx_row, GRID_COLUMN_COUPON_IN).Value = GUI_FormatCurrency(0, 2)
      Else
        Grid.Cell(_idx_row, GRID_COLUMN_COUPON_IN).Value = GUI_FormatCurrency(Total_cupon_in, 2)
      End If
    End If

    ' coupon out
    If Grid.Column(GRID_COLUMN_COUPON_OUT).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_COUPON_OUT).Value = String.Empty
    Else
      If (Total_terminals = 0) Then
        Grid.Cell(_idx_row, GRID_COLUMN_COUPON_OUT).Value = GUI_FormatCurrency(0, 2)
      Else
        Grid.Cell(_idx_row, GRID_COLUMN_COUPON_OUT).Value = GUI_FormatCurrency(Total_cupon_out, 2)
      End If
    End If


    ' ci -floor index
    If Grid.Column(GRID_COLUMN_CI_FLOOR_INDEX).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_CI_FLOOR_INDEX).Value = String.Empty
    Else
      Grid.Cell(_idx_row, GRID_COLUMN_CI_FLOOR_INDEX).Value = GUI_FormatNumber(1, 2)
    End If

    ' win - floor index
    If Grid.Column(GRID_COLUMN_WIN_FLOOR_INDEX).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_WIN_FLOOR_INDEX).Value = String.Empty
    Else
      Grid.Cell(_idx_row, GRID_COLUMN_WIN_FLOOR_INDEX).Value = GUI_FormatNumber(1, 2)
    End If

    ' ci-game index
    If Grid.Column(GRID_COLUMN_CI_GAME_INDEX).Width = 0 Then
      Grid.Cell(_idx_row, GRID_COLUMN_CI_GAME_INDEX).Value = String.Empty
    Else
      Grid.Cell(_idx_row, GRID_COLUMN_CI_GAME_INDEX).Value = GUI_FormatNumber(1, 2)
    End If

    'style
    Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' TotalRow

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet(ByVal IndexTable As Integer, _
                             ByRef Dg As GUI_Controls.uc_grid)

    Dim _columns_not_visible As Integer()

    _columns_not_visible = Nothing
    With Dg
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(338)
      .Column(GRID_COLUMN_DATE).Width = 2200
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(253)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = 1700
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Provider name
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(268)
      .Column(GRID_COLUMN_PROVIDER).Width = 1700
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Game type
      .Column(GRID_COLUMN_TG_GAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(462)
      .Column(GRID_COLUMN_TG_GAME).Width = 1700
      .Column(GRID_COLUMN_TG_GAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank name
      .Column(GRID_COLUMN_BANK).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(427)
      .Column(GRID_COLUMN_BANK).Width = 1700
      .Column(GRID_COLUMN_BANK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Area name
      .Column(GRID_COLUMN_AR_NAME).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(426)
      .Column(GRID_COLUMN_AR_NAME).Width = 1700
      .Column(GRID_COLUMN_AR_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Zone name
      .Column(GRID_COLUMN_AR_SMOKING).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(439)
      .Column(GRID_COLUMN_AR_SMOKING).Width = 1700
      .Column(GRID_COLUMN_AR_SMOKING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Type
      .Column(GRID_COLUMN_TD_TYPE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(271)
      .Column(GRID_COLUMN_TD_TYPE).Width = 1700
      .Column(GRID_COLUMN_TD_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      ' Columns comuns
      ' Terminals number
      .Column(GRID_COLUMN_TERMINALS_NUMBER).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(461)
      .Column(GRID_COLUMN_TERMINALS_NUMBER).Width = 1700
      .Column(GRID_COLUMN_TERMINALS_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin in
      .Column(GRID_COLUMN_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_COIN_IN).Width = 1700
      .Column(GRID_COLUMN_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin out
      .Column(GRID_COLUMN_COIN_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(257)
      .Column(GRID_COLUMN_COIN_OUT).Width = 1700
      .Column(GRID_COLUMN_COIN_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(259)
      .Column(GRID_COLUMN_NETWIN).Width = 1700
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' % Netwin
      .Column(GRID_COLUMN_PC_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(260)
      .Column(GRID_COLUMN_PC_NETWIN).Width = 1700
      .Column(GRID_COLUMN_PC_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin in terminal
      .Column(GRID_COLUMN_COIN_IN_MAQ).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(276)
      .Column(GRID_COLUMN_COIN_IN_MAQ).Width = 1700
      .Column(GRID_COLUMN_COIN_IN_MAQ).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin out terminal
      .Column(GRID_COLUMN_COIN_OUT_MAQ).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(277)
      .Column(GRID_COLUMN_COIN_OUT_MAQ).Width = 1700
      .Column(GRID_COLUMN_COIN_OUT_MAQ).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin terminal
      .Column(GRID_COLUMN_NETWIN_MAQ).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(278)
      .Column(GRID_COLUMN_NETWIN_MAQ).Width = 1700
      .Column(GRID_COLUMN_NETWIN_MAQ).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cupon in
      .Column(GRID_COLUMN_COUPON_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(279)
      .Column(GRID_COLUMN_COUPON_IN).Width = 1700
      .Column(GRID_COLUMN_COUPON_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cupon out
      .Column(GRID_COLUMN_COUPON_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(280)
      .Column(GRID_COLUMN_COUPON_OUT).Width = 1700
      .Column(GRID_COLUMN_COUPON_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin in floor index
      .Column(GRID_COLUMN_CI_FLOOR_INDEX).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(283)
      .Column(GRID_COLUMN_CI_FLOOR_INDEX).Width = 1700
      .Column(GRID_COLUMN_CI_FLOOR_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Win floor index
      .Column(GRID_COLUMN_WIN_FLOOR_INDEX).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(284)
      .Column(GRID_COLUMN_WIN_FLOOR_INDEX).Width = 1700
      .Column(GRID_COLUMN_WIN_FLOOR_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CI - Game Index
      .Column(GRID_COLUMN_CI_GAME_INDEX).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(285)
      .Column(GRID_COLUMN_CI_GAME_INDEX).Width = 1700
      .Column(GRID_COLUMN_CI_GAME_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Columns not visible
      _columns_not_visible = Me.m_visible_header.Item(Me.tab_productivity.TabPages(IndexTable).Name)
      For _i As Int16 = 0 To _columns_not_visible.Length - 1
        Dg.Column(_columns_not_visible(_i)).Header(0).Text = String.Empty
        Dg.Column(_columns_not_visible(_i)).Width = 1
      Next

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Load grids of tabs
  '
  '  PARAMS:
  '     - INPUT:
  '           Visible: 
  ''     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub LoadGrids(ByVal Visible As Boolean)
    Dim _grid As GUI_Controls.uc_grid
    Dim _num_columns As Integer()

    For _i As Int16 = 0 To Me.tab_productivity.TabPages.Count - 1

      _grid = Me.tab_productivity.TabPages(_i).Controls(IDX_GRID_TABPAGE)

      If Visible Then
        Call ShowTable(_i)
        _grid.Redraw = False
      End If

      _num_columns = Me.m_visible_header.Item(Me.tab_productivity.TabPages(_i).Name)
      For _j As Int16 = 0 To _num_columns.Length - 1
        If Visible Then
          _grid.Column(_num_columns(_j)).Width = 100
        Else
          _grid.Column(_num_columns(_j)).Width = 1
        End If
        '_grid.Column(_num_columns(_j)).Width = IIf(Visible, 100, 1)
      Next

      If Not Visible Then
        _grid.Redraw = True
      End If

    Next

  End Sub

#End Region ' Private Functions 

#Region " Public Functions  "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit


#End Region ' Public Functions 

#Region " Events "

  Private Sub tab_productivity_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tab_productivity.Selected
    Me.ShowTable(Me.tab_productivity.SelectedIndex)
  End Sub

#End Region

End Class