﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_countr_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_countr = New System.Windows.Forms.GroupBox()
    Me.chk_show_log = New System.Windows.Forms.CheckBox()
    Me.txt_status = New GUI_Controls.uc_entry_field()
    Me.txt_last_connection = New GUI_Controls.uc_entry_field()
    Me.txt_ip_address = New GUI_Controls.uc_entry_field()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.txt_description = New System.Windows.Forms.TextBox()
    Me.txt_provider = New GUI_Controls.uc_entry_field()
    Me.txt_name = New GUI_Controls.uc_entry_field()
    Me.txt_code = New GUI_Controls.uc_entry_field()
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.ef_floor_id = New GUI_Controls.uc_entry_field()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.gb_payment = New System.Windows.Forms.GroupBox()
    Me.cmb_currency = New GUI_Controls.uc_combo()
    Me.txt_max_payment = New GUI_Controls.uc_entry_field()
    Me.txt_min_payment = New GUI_Controls.uc_entry_field()
    Me.gb_retired = New System.Windows.Forms.GroupBox()
    Me.txt_retirement_user = New GUI_Controls.uc_entry_field()
    Me.txt_retirement_date = New GUI_Controls.uc_entry_field()
    Me.chk_create_ticket = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_countr.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_payment.SuspendLayout()
    Me.gb_retired.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_retired)
    Me.panel_data.Controls.Add(Me.gb_payment)
    Me.panel_data.Controls.Add(Me.gb_area_island)
    Me.panel_data.Controls.Add(Me.gb_countr)
    Me.panel_data.Size = New System.Drawing.Size(622, 378)
    '
    'gb_countr
    '
    Me.gb_countr.Controls.Add(Me.chk_create_ticket)
    Me.gb_countr.Controls.Add(Me.chk_show_log)
    Me.gb_countr.Controls.Add(Me.txt_status)
    Me.gb_countr.Controls.Add(Me.txt_last_connection)
    Me.gb_countr.Controls.Add(Me.txt_ip_address)
    Me.gb_countr.Controls.Add(Me.chk_enabled)
    Me.gb_countr.Controls.Add(Me.lbl_description)
    Me.gb_countr.Controls.Add(Me.txt_description)
    Me.gb_countr.Controls.Add(Me.txt_provider)
    Me.gb_countr.Controls.Add(Me.txt_name)
    Me.gb_countr.Controls.Add(Me.txt_code)
    Me.gb_countr.Location = New System.Drawing.Point(13, 16)
    Me.gb_countr.Name = "gb_countr"
    Me.gb_countr.Size = New System.Drawing.Size(291, 357)
    Me.gb_countr.TabIndex = 0
    Me.gb_countr.TabStop = False
    Me.gb_countr.Text = "xCountR"
    '
    'chk_show_log
    '
    Me.chk_show_log.AutoSize = True
    Me.chk_show_log.Location = New System.Drawing.Point(9, 149)
    Me.chk_show_log.Name = "chk_show_log"
    Me.chk_show_log.Size = New System.Drawing.Size(84, 17)
    Me.chk_show_log.TabIndex = 4
    Me.chk_show_log.Text = "xShowLog"
    Me.chk_show_log.UseVisualStyleBackColor = True
    '
    'txt_status
    '
    Me.txt_status.DoubleValue = 0.0R
    Me.txt_status.IntegerValue = 0
    Me.txt_status.IsReadOnly = False
    Me.txt_status.Location = New System.Drawing.Point(6, 328)
    Me.txt_status.Name = "txt_status"
    Me.txt_status.PlaceHolder = Nothing
    Me.txt_status.Size = New System.Drawing.Size(279, 24)
    Me.txt_status.SufixText = "Sufix Text"
    Me.txt_status.SufixTextVisible = True
    Me.txt_status.TabIndex = 9
    Me.txt_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_status.TextValue = ""
    Me.txt_status.TextWidth = 110
    Me.txt_status.Value = ""
    Me.txt_status.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_last_connection
    '
    Me.txt_last_connection.DoubleValue = 0.0R
    Me.txt_last_connection.IntegerValue = 0
    Me.txt_last_connection.IsReadOnly = False
    Me.txt_last_connection.Location = New System.Drawing.Point(6, 298)
    Me.txt_last_connection.Name = "txt_last_connection"
    Me.txt_last_connection.PlaceHolder = Nothing
    Me.txt_last_connection.Size = New System.Drawing.Size(279, 24)
    Me.txt_last_connection.SufixText = "Sufix Text"
    Me.txt_last_connection.SufixTextVisible = True
    Me.txt_last_connection.TabIndex = 8
    Me.txt_last_connection.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_last_connection.TextValue = ""
    Me.txt_last_connection.TextWidth = 110
    Me.txt_last_connection.Value = ""
    Me.txt_last_connection.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_ip_address
    '
    Me.txt_ip_address.DoubleValue = 0.0R
    Me.txt_ip_address.IntegerValue = 0
    Me.txt_ip_address.IsReadOnly = False
    Me.txt_ip_address.Location = New System.Drawing.Point(6, 112)
    Me.txt_ip_address.Name = "txt_ip_address"
    Me.txt_ip_address.PlaceHolder = Nothing
    Me.txt_ip_address.Size = New System.Drawing.Size(279, 24)
    Me.txt_ip_address.SufixText = "Sufix Text"
    Me.txt_ip_address.SufixTextVisible = True
    Me.txt_ip_address.TabIndex = 3
    Me.txt_ip_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_ip_address.TextValue = ""
    Me.txt_ip_address.TextWidth = 110
    Me.txt_ip_address.Value = ""
    Me.txt_ip_address.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(207, 149)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 5
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'lbl_description
    '
    Me.lbl_description.Location = New System.Drawing.Point(6, 200)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(125, 16)
    Me.lbl_description.TabIndex = 6
    Me.lbl_description.Text = "xDescription"
    '
    'txt_description
    '
    Me.txt_description.Location = New System.Drawing.Point(6, 220)
    Me.txt_description.MaxLength = 250
    Me.txt_description.Multiline = True
    Me.txt_description.Name = "txt_description"
    Me.txt_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_description.Size = New System.Drawing.Size(279, 72)
    Me.txt_description.TabIndex = 7
    '
    'txt_provider
    '
    Me.txt_provider.DoubleValue = 0.0R
    Me.txt_provider.IntegerValue = 0
    Me.txt_provider.IsReadOnly = False
    Me.txt_provider.Location = New System.Drawing.Point(6, 82)
    Me.txt_provider.Name = "txt_provider"
    Me.txt_provider.PlaceHolder = Nothing
    Me.txt_provider.Size = New System.Drawing.Size(279, 24)
    Me.txt_provider.SufixText = "Sufix Text"
    Me.txt_provider.SufixTextVisible = True
    Me.txt_provider.TabIndex = 2
    Me.txt_provider.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_provider.TextValue = ""
    Me.txt_provider.TextWidth = 110
    Me.txt_provider.Value = ""
    Me.txt_provider.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0.0R
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(6, 51)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.PlaceHolder = Nothing
    Me.txt_name.Size = New System.Drawing.Size(279, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 1
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 110
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_code
    '
    Me.txt_code.DoubleValue = 0.0R
    Me.txt_code.IntegerValue = 0
    Me.txt_code.IsReadOnly = False
    Me.txt_code.Location = New System.Drawing.Point(6, 20)
    Me.txt_code.Name = "txt_code"
    Me.txt_code.PlaceHolder = Nothing
    Me.txt_code.Size = New System.Drawing.Size(279, 24)
    Me.txt_code.SufixText = "Sufix Text"
    Me.txt_code.SufixTextVisible = True
    Me.txt_code.TabIndex = 0
    Me.txt_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_code.TextValue = ""
    Me.txt_code.TextWidth = 110
    Me.txt_code.Value = ""
    Me.txt_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.ef_floor_id)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(313, 16)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(302, 131)
    Me.gb_area_island.TabIndex = 1
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(6, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(290, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 110
    '
    'ef_floor_id
    '
    Me.ef_floor_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_floor_id.DoubleValue = 0.0R
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(6, 100)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.PlaceHolder = Nothing
    Me.ef_floor_id.Size = New System.Drawing.Size(290, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 3
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 110
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(6, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(290, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 110
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(6, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.Size = New System.Drawing.Size(290, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 110
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_payment
    '
    Me.gb_payment.Controls.Add(Me.cmb_currency)
    Me.gb_payment.Controls.Add(Me.txt_max_payment)
    Me.gb_payment.Controls.Add(Me.txt_min_payment)
    Me.gb_payment.Location = New System.Drawing.Point(313, 153)
    Me.gb_payment.Name = "gb_payment"
    Me.gb_payment.Size = New System.Drawing.Size(302, 113)
    Me.gb_payment.TabIndex = 2
    Me.gb_payment.TabStop = False
    Me.gb_payment.Text = "xPaymentConfig"
    '
    'cmb_currency
    '
    Me.cmb_currency.AllowUnlistedValues = False
    Me.cmb_currency.AutoCompleteMode = False
    Me.cmb_currency.IsReadOnly = False
    Me.cmb_currency.Location = New System.Drawing.Point(6, 20)
    Me.cmb_currency.Name = "cmb_currency"
    Me.cmb_currency.SelectedIndex = -1
    Me.cmb_currency.Size = New System.Drawing.Size(290, 24)
    Me.cmb_currency.SufixText = "Sufix Text"
    Me.cmb_currency.SufixTextVisible = True
    Me.cmb_currency.TabIndex = 0
    Me.cmb_currency.TextCombo = Nothing
    Me.cmb_currency.TextWidth = 110
    '
    'txt_max_payment
    '
    Me.txt_max_payment.DoubleValue = 0.0R
    Me.txt_max_payment.IntegerValue = 0
    Me.txt_max_payment.IsReadOnly = False
    Me.txt_max_payment.Location = New System.Drawing.Point(6, 81)
    Me.txt_max_payment.Name = "txt_max_payment"
    Me.txt_max_payment.PlaceHolder = Nothing
    Me.txt_max_payment.Size = New System.Drawing.Size(290, 24)
    Me.txt_max_payment.SufixText = "Sufix Text"
    Me.txt_max_payment.SufixTextVisible = True
    Me.txt_max_payment.TabIndex = 2
    Me.txt_max_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_max_payment.TextValue = ""
    Me.txt_max_payment.TextWidth = 110
    Me.txt_max_payment.Value = ""
    Me.txt_max_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_min_payment
    '
    Me.txt_min_payment.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.txt_min_payment.DoubleValue = 0.0R
    Me.txt_min_payment.IntegerValue = 0
    Me.txt_min_payment.IsReadOnly = False
    Me.txt_min_payment.Location = New System.Drawing.Point(6, 50)
    Me.txt_min_payment.Name = "txt_min_payment"
    Me.txt_min_payment.PlaceHolder = Nothing
    Me.txt_min_payment.Size = New System.Drawing.Size(290, 24)
    Me.txt_min_payment.SufixText = "Sufix Text"
    Me.txt_min_payment.SufixTextVisible = True
    Me.txt_min_payment.TabIndex = 1
    Me.txt_min_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_min_payment.TextValue = ""
    Me.txt_min_payment.TextWidth = 110
    Me.txt_min_payment.Value = ""
    Me.txt_min_payment.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_retired
    '
    Me.gb_retired.Controls.Add(Me.txt_retirement_user)
    Me.gb_retired.Controls.Add(Me.txt_retirement_date)
    Me.gb_retired.Location = New System.Drawing.Point(313, 269)
    Me.gb_retired.Name = "gb_retired"
    Me.gb_retired.Size = New System.Drawing.Size(302, 84)
    Me.gb_retired.TabIndex = 3
    Me.gb_retired.TabStop = False
    Me.gb_retired.Text = "xRetiredInfo"
    '
    'txt_retirement_user
    '
    Me.txt_retirement_user.DoubleValue = 0.0R
    Me.txt_retirement_user.IntegerValue = 0
    Me.txt_retirement_user.IsReadOnly = False
    Me.txt_retirement_user.Location = New System.Drawing.Point(6, 50)
    Me.txt_retirement_user.Name = "txt_retirement_user"
    Me.txt_retirement_user.PlaceHolder = Nothing
    Me.txt_retirement_user.Size = New System.Drawing.Size(290, 24)
    Me.txt_retirement_user.SufixText = "Sufix Text"
    Me.txt_retirement_user.SufixTextVisible = True
    Me.txt_retirement_user.TabIndex = 2
    Me.txt_retirement_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_retirement_user.TextValue = ""
    Me.txt_retirement_user.TextWidth = 110
    Me.txt_retirement_user.Value = ""
    Me.txt_retirement_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_retirement_date
    '
    Me.txt_retirement_date.DoubleValue = 0.0R
    Me.txt_retirement_date.IntegerValue = 0
    Me.txt_retirement_date.IsReadOnly = False
    Me.txt_retirement_date.Location = New System.Drawing.Point(6, 20)
    Me.txt_retirement_date.Name = "txt_retirement_date"
    Me.txt_retirement_date.PlaceHolder = Nothing
    Me.txt_retirement_date.Size = New System.Drawing.Size(290, 24)
    Me.txt_retirement_date.SufixText = "Sufix Text"
    Me.txt_retirement_date.SufixTextVisible = True
    Me.txt_retirement_date.TabIndex = 1
    Me.txt_retirement_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_retirement_date.TextValue = ""
    Me.txt_retirement_date.TextWidth = 110
    Me.txt_retirement_date.Value = ""
    Me.txt_retirement_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_create_ticket
    '
    Me.chk_create_ticket.AutoSize = True
    Me.chk_create_ticket.Location = New System.Drawing.Point(9, 172)
    Me.chk_create_ticket.Name = "chk_create_ticket"
    Me.chk_create_ticket.Size = New System.Drawing.Size(106, 17)
    Me.chk_create_ticket.TabIndex = 10
    Me.chk_create_ticket.Text = "xCreateTicket"
    Me.chk_create_ticket.UseVisualStyleBackColor = True
    '
    'frm_countr_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(724, 387)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_countr_edit"
    Me.Text = "frm_countr_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_countr.ResumeLayout(False)
    Me.gb_countr.PerformLayout()
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_payment.ResumeLayout(False)
    Me.gb_retired.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_countr As System.Windows.Forms.GroupBox
  Friend WithEvents txt_provider As GUI_Controls.uc_entry_field
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
  Friend WithEvents txt_code As GUI_Controls.uc_entry_field
  Friend WithEvents txt_description As System.Windows.Forms.TextBox
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents gb_payment As System.Windows.Forms.GroupBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents txt_max_payment As GUI_Controls.uc_entry_field
  Friend WithEvents txt_min_payment As GUI_Controls.uc_entry_field
  Friend WithEvents gb_retired As System.Windows.Forms.GroupBox
  Friend WithEvents txt_retirement_user As GUI_Controls.uc_entry_field
  Friend WithEvents txt_retirement_date As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_currency As GUI_Controls.uc_combo
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_ip_address As GUI_Controls.uc_entry_field
  Friend WithEvents txt_status As GUI_Controls.uc_entry_field
  Friend WithEvents txt_last_connection As GUI_Controls.uc_entry_field
  Friend WithEvents chk_show_log As System.Windows.Forms.CheckBox
  Friend WithEvents chk_create_ticket As System.Windows.Forms.CheckBox

End Class
