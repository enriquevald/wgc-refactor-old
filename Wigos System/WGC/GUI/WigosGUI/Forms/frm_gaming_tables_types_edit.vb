'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_types_edit.vb
' DESCRIPTION:   Edit gaming tables types
' AUTHOR:        Javier Barea
' CREATION DATE: 17-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-DEC-2013  JBP    Initial version
' 24-FEB-2014  RRR    Added ValidateFormat
' 17-MAR-2017  FOS    Bug 25374: Change default afip type value
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports WSI.Common
Imports WigosGUI.CLASS_GAMING_TABLE_TYPE


Public Class frm_gaming_tables_types_edit
  Inherits frm_base_edit

#Region " Constants "

  ' Fields length
  Public Const MAX_NAME_LEN As Integer = 50

#End Region             'Constants

#Region " Members "
  Private m_gaming_table_type As CLASS_GAMING_TABLE_TYPE
  Private m_delete_operation As Boolean
#End Region               'Members

#Region "Properties"

#End Region              'Properties

#Region "Privates"

  ' PURPOSE: Validates controls content
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Validation result
  '
  Private Function ValidateFormat() As Boolean

    If Not Me.txt_name.ValidateFormat() Then
      Return False
    End If

    Return True

  End Function  ' ValidateFormat

#End Region 'Privates


#Region "Overrides"

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowEditItem(ByVal OperationId As Int64)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = OperationId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Gaming Tables Types
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3429)                     ' 3429 "Definici�n Tipos de Mesa de Juego"

    'Controls:

    ' Table Name
    Me.txt_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399)               ' 4399 "Nombre Tipo"
    Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_LEN, 0)

    ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4400)            ' 4400 "Habilitado"

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Enabled = False

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Enabled = True
    End If

    'Fill Combo from types of AFIP catalog.
    If (WSI.Common.AFIP_Common.AFIP_Common.IsEnabled) Then

      Me.lbl_afip_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7332)

      Call FillCatalogItems()

      If (m_gaming_table_type.AFIPType > 0) Then
        cb_AFIP_types.SelectedValue = m_gaming_table_type.AFIPType.ToString()
      End If
    Else
      Me.cb_AFIP_types.Visible = False
      Me.lbl_afip_type.Visible = False
    End If


  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim nls_param As String

    ' Empty Name
    If String.IsNullOrEmpty(Me.txt_name.Value) Then
      ' 3443 "El campo 'Nombre' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3443), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK)
      Call Me.txt_name.Focus()

      Return False
    End If

    ' Too long Name
    If Me.txt_name.Value.Length > MAX_NAME_LEN Then
      ' 3442 "El campo 'Nombre' debe tener un m�ximo de %1 car�cteres."
      nls_param = MAX_NAME_LEN.ToString()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3444), _
                                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                nls_param)
      Call Me.txt_name.Focus()

      Return False
    End If

    If Not ValidateFormat() Then
      Return False
    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    m_gaming_table_type = Me.DbEditedObject

    ' Name
    m_gaming_table_type.Name = Me.txt_name.Value

    ' Enabled
    m_gaming_table_type.Enabled = Me.chk_enabled.Checked

    'AFIP_Type
    m_gaming_table_type.AFIPType = Me.cb_AFIP_types.SelectedValue

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    m_gaming_table_type = Me.DbReadObject

    ' Name
    Me.txt_name.Value = m_gaming_table_type.Name

    ' Enabled
    Me.chk_enabled.Checked = m_gaming_table_type.Enabled

    'AFIP_Type
    Me.cb_AFIP_types.SelectedItem = m_gaming_table_type.AFIPType

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _rc As ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_GAMING_TABLE_TYPE
        m_gaming_table_type = Me.DbEditedObject
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4386), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4387), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4388), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4389), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4388), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4401), ENUM_MB_TYPE.MB_TYPE_ERROR)
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4386), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4390), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        m_delete_operation = False

        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4306), _
                                 ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                 ENUM_MB_BTN.MB_BTN_YES_NO, _
                                 ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED ' Avoid delete, no show error and no close form. 
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            ' Avoid delete, no show error and no close form. 
          ElseIf Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3452), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3453), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.txt_name

  End Sub 'GUI_SetInitialFocus

  Private Sub FillCatalogItems()
    Dim _item_catalog_list As List(Of WSI.Common.Catalog.Catalog_Item)
    Dim _catalog As Catalog
    Dim _dt_AFIP_type As DataTable
    Dim _dr_AFIP_type As DataRow
    Dim _afip_type As Integer

    _dt_AFIP_type = New DataTable()
    _dt_AFIP_type.Columns.Add("Key", Type.[GetType]("System.String"))
    _dt_AFIP_type.Columns.Add("Value", Type.[GetType]("System.Int64"))

    _item_catalog_list = New List(Of WSI.Common.Catalog.Catalog_Item)()
    _catalog = New Catalog()
    _item_catalog_list = _catalog.GetCatalogItems(Catalog.CatalogSystemType.AFIP_GamingTablesType)

    For i As Integer = 0 To _item_catalog_list.Count - 1
      _dr_AFIP_type = _dt_AFIP_type.NewRow()
      _dr_AFIP_type("Key") = _item_catalog_list(i).Name

      Select Case _item_catalog_list(i).Name

        Case "Ruleta"
          _afip_type = AFIP_TYPES.Ruleta

        Case "Naipes"
          _afip_type = AFIP_TYPES.Naipes

        Case "Dados"
          _afip_type = AFIP_TYPES.Dados

        Case "Torneo"
          _afip_type = AFIP_TYPES.Torneo

        Case "Otros"
          _afip_type = AFIP_TYPES.Otros

      End Select

      _dr_AFIP_type("Value") = _afip_type
      _dt_AFIP_type.Rows.Add(_dr_AFIP_type)
    Next

    Me.cb_AFIP_types.DataSource = _dt_AFIP_type
    Me.cb_AFIP_types.ValueMember = "Value"
    Me.cb_AFIP_types.DisplayMember = "Key"


  End Sub

#End Region               'Overrides

#Region " Events "

#End Region 'Events 

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

End Class