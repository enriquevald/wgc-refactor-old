﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_how_to_get_there
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_how_to_title = New GUI_Controls.uc_entry_field()
    Me.ef_how_to_address = New GUI_Controls.uc_entry_field()
    Me.btn_items_add = New GUI_Controls.uc_button()
    Me.btn_items_del = New GUI_Controls.uc_button()
    Me.dg_how_to_items = New GUI_Controls.uc_grid()
    Me.lblTitle = New System.Windows.Forms.Label()
    Me.lblAddress = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lblAddress)
    Me.panel_data.Controls.Add(Me.lblTitle)
    Me.panel_data.Controls.Add(Me.dg_how_to_items)
    Me.panel_data.Controls.Add(Me.btn_items_add)
    Me.panel_data.Controls.Add(Me.btn_items_del)
    Me.panel_data.Controls.Add(Me.ef_how_to_address)
    Me.panel_data.Controls.Add(Me.ef_how_to_title)
    Me.panel_data.Location = New System.Drawing.Point(6, 5)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(4)
    Me.panel_data.Size = New System.Drawing.Size(659, 337)
    '
    'ef_how_to_title
    '
    Me.ef_how_to_title.DoubleValue = 0.0R
    Me.ef_how_to_title.IntegerValue = 0
    Me.ef_how_to_title.IsReadOnly = False
    Me.ef_how_to_title.Location = New System.Drawing.Point(78, 9)
    Me.ef_how_to_title.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_how_to_title.Name = "ef_how_to_title"
    Me.ef_how_to_title.PlaceHolder = Nothing
    Me.ef_how_to_title.Size = New System.Drawing.Size(552, 26)
    Me.ef_how_to_title.SufixText = "Sufix Text"
    Me.ef_how_to_title.SufixTextVisible = True
    Me.ef_how_to_title.TabIndex = 0
    Me.ef_how_to_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_how_to_title.TextValue = ""
    Me.ef_how_to_title.TextWidth = 0
    Me.ef_how_to_title.Value = ""
    Me.ef_how_to_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_how_to_address
    '
    Me.ef_how_to_address.DoubleValue = 0.0R
    Me.ef_how_to_address.IntegerValue = 0
    Me.ef_how_to_address.IsReadOnly = False
    Me.ef_how_to_address.Location = New System.Drawing.Point(78, 43)
    Me.ef_how_to_address.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_how_to_address.Name = "ef_how_to_address"
    Me.ef_how_to_address.PlaceHolder = Nothing
    Me.ef_how_to_address.Size = New System.Drawing.Size(552, 26)
    Me.ef_how_to_address.SufixText = "Sufix Text"
    Me.ef_how_to_address.SufixTextVisible = True
    Me.ef_how_to_address.TabIndex = 4
    Me.ef_how_to_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_how_to_address.TextValue = ""
    Me.ef_how_to_address.TextWidth = 0
    Me.ef_how_to_address.Value = ""
    Me.ef_how_to_address.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_items_add
    '
    Me.btn_items_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_items_add.Location = New System.Drawing.Point(520, 314)
    Me.btn_items_add.Margin = New System.Windows.Forms.Padding(4)
    Me.btn_items_add.Name = "btn_items_add"
    Me.btn_items_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_items_add.TabIndex = 5
    Me.btn_items_add.ToolTipped = False
    Me.btn_items_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_items_del
    '
    Me.btn_items_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_items_del.Location = New System.Drawing.Point(578, 314)
    Me.btn_items_del.Margin = New System.Windows.Forms.Padding(4)
    Me.btn_items_del.Name = "btn_items_del"
    Me.btn_items_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_items_del.TabIndex = 6
    Me.btn_items_del.ToolTipped = False
    Me.btn_items_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'dg_how_to_items
    '
    Me.dg_how_to_items.CurrentCol = -1
    Me.dg_how_to_items.CurrentRow = -1
    Me.dg_how_to_items.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_how_to_items.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_how_to_items.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_how_to_items.Location = New System.Drawing.Point(79, 79)
    Me.dg_how_to_items.Margin = New System.Windows.Forms.Padding(4)
    Me.dg_how_to_items.Name = "dg_how_to_items"
    Me.dg_how_to_items.PanelRightVisible = False
    Me.dg_how_to_items.Redraw = True
    Me.dg_how_to_items.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_how_to_items.Size = New System.Drawing.Size(550, 218)
    Me.dg_how_to_items.Sortable = False
    Me.dg_how_to_items.SortAscending = True
    Me.dg_how_to_items.SortByCol = 0
    Me.dg_how_to_items.TabIndex = 7
    Me.dg_how_to_items.ToolTipped = True
    Me.dg_how_to_items.TopRow = -2
    Me.dg_how_to_items.WordWrap = False
    '
    'lblTitle
    '
    Me.lblTitle.AutoSize = True
    Me.lblTitle.Location = New System.Drawing.Point(6, 10)
    Me.lblTitle.Name = "lblTitle"
    Me.lblTitle.Size = New System.Drawing.Size(53, 17)
    Me.lblTitle.TabIndex = 8
    Me.lblTitle.Text = "Label1"
    '
    'lblAddress
    '
    Me.lblAddress.AutoSize = True
    Me.lblAddress.Location = New System.Drawing.Point(6, 47)
    Me.lblAddress.Name = "lblAddress"
    Me.lblAddress.Size = New System.Drawing.Size(53, 17)
    Me.lblAddress.TabIndex = 9
    Me.lblAddress.Text = "Label2"
    '
    'frm_how_to_get_there
    '
    'Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 17.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(766, 347)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Margin = New System.Windows.Forms.Padding(4)
    Me.Name = "frm_how_to_get_there"
    Me.Padding = New System.Windows.Forms.Padding(6, 5, 6, 5)
    Me.Text = " "
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_how_to_title As GUI_Controls.uc_entry_field
  Friend WithEvents ef_how_to_address As GUI_Controls.uc_entry_field
  Friend WithEvents dg_how_to_items As GUI_Controls.uc_grid
  Friend WithEvents btn_items_add As GUI_Controls.uc_button
  Friend WithEvents btn_items_del As GUI_Controls.uc_button
  Friend WithEvents lblAddress As System.Windows.Forms.Label
  Friend WithEvents lblTitle As System.Windows.Forms.Label
End Class
