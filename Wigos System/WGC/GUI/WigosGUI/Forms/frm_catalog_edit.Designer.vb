﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_catalog_edit
    Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_catalog_items = New System.Windows.Forms.GroupBox()
    Me.btn_catalog_filter = New GUI_Controls.uc_button()
    Me.btn_catalog_add = New GUI_Controls.uc_button()
    Me.btn_catalog_del = New GUI_Controls.uc_button()
    Me.ef_filter = New GUI_Controls.uc_entry_field()
    Me.dg_catalog_items = New GUI_Controls.uc_grid()
    Me.tb_description = New System.Windows.Forms.TextBox()
    Me.ef_catalog_name = New GUI_Controls.uc_entry_field()
    Me.lbl_catalog_description = New System.Windows.Forms.Label()
    Me.gb_catalog_state = New System.Windows.Forms.GroupBox()
    Me.rb_catalog_disabled = New System.Windows.Forms.RadioButton()
    Me.rb_catalog_enabled = New System.Windows.Forms.RadioButton()
    Me.panel_data.SuspendLayout()
    Me.gb_catalog_items.SuspendLayout()
    Me.gb_catalog_state.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_catalog_state)
    Me.panel_data.Controls.Add(Me.lbl_catalog_description)
    Me.panel_data.Controls.Add(Me.tb_description)
    Me.panel_data.Controls.Add(Me.ef_catalog_name)
    Me.panel_data.Controls.Add(Me.gb_catalog_items)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(474, 492)
    '
    'gb_catalog_items
    '
    Me.gb_catalog_items.Controls.Add(Me.btn_catalog_filter)
    Me.gb_catalog_items.Controls.Add(Me.btn_catalog_add)
    Me.gb_catalog_items.Controls.Add(Me.btn_catalog_del)
    Me.gb_catalog_items.Controls.Add(Me.ef_filter)
    Me.gb_catalog_items.Controls.Add(Me.dg_catalog_items)
    Me.gb_catalog_items.Location = New System.Drawing.Point(3, 214)
    Me.gb_catalog_items.Name = "gb_catalog_items"
    Me.gb_catalog_items.Size = New System.Drawing.Size(468, 263)
    Me.gb_catalog_items.TabIndex = 3
    Me.gb_catalog_items.TabStop = False
    Me.gb_catalog_items.Text = "xItems"
    '
    'btn_catalog_filter
    '
    Me.btn_catalog_filter.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_catalog_filter.Location = New System.Drawing.Point(234, 236)
    Me.btn_catalog_filter.Name = "btn_catalog_filter"
    Me.btn_catalog_filter.Size = New System.Drawing.Size(50, 21)
    Me.btn_catalog_filter.TabIndex = 2
    Me.btn_catalog_filter.ToolTipped = False
    Me.btn_catalog_filter.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_catalog_add
    '
    Me.btn_catalog_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_catalog_add.Location = New System.Drawing.Point(344, 236)
    Me.btn_catalog_add.Name = "btn_catalog_add"
    Me.btn_catalog_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_catalog_add.TabIndex = 3
    Me.btn_catalog_add.ToolTipped = False
    Me.btn_catalog_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_catalog_del
    '
    Me.btn_catalog_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_catalog_del.Location = New System.Drawing.Point(400, 236)
    Me.btn_catalog_del.Name = "btn_catalog_del"
    Me.btn_catalog_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_catalog_del.TabIndex = 4
    Me.btn_catalog_del.ToolTipped = False
    Me.btn_catalog_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'ef_filter
    '
    Me.ef_filter.DoubleValue = 0.0R
    Me.ef_filter.IntegerValue = 0
    Me.ef_filter.IsReadOnly = False
    Me.ef_filter.Location = New System.Drawing.Point(15, 234)
    Me.ef_filter.Name = "ef_filter"
    Me.ef_filter.PlaceHolder = Nothing
    Me.ef_filter.Size = New System.Drawing.Size(213, 24)
    Me.ef_filter.SufixText = "Sufix Text"
    Me.ef_filter.SufixTextVisible = True
    Me.ef_filter.TabIndex = 1
    Me.ef_filter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_filter.TextValue = ""
    Me.ef_filter.Value = ""
    Me.ef_filter.ValueForeColor = System.Drawing.Color.Blue
    '
    'dg_catalog_items
    '
    Me.dg_catalog_items.CurrentCol = -1
    Me.dg_catalog_items.CurrentRow = -1
    Me.dg_catalog_items.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_catalog_items.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_catalog_items.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_catalog_items.Location = New System.Drawing.Point(15, 20)
    Me.dg_catalog_items.Name = "dg_catalog_items"
    Me.dg_catalog_items.PanelRightVisible = False
    Me.dg_catalog_items.Redraw = True
    Me.dg_catalog_items.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_catalog_items.Size = New System.Drawing.Size(435, 210)
    Me.dg_catalog_items.Sortable = False
    Me.dg_catalog_items.SortAscending = True
    Me.dg_catalog_items.SortByCol = 0
    Me.dg_catalog_items.TabIndex = 0
    Me.dg_catalog_items.ToolTipped = True
    Me.dg_catalog_items.TopRow = -2
    Me.dg_catalog_items.WordWrap = False
    '
    'tb_description
    '
    Me.tb_description.Location = New System.Drawing.Point(114, 46)
    Me.tb_description.MaxLength = 200
    Me.tb_description.Multiline = True
    Me.tb_description.Name = "tb_description"
    Me.tb_description.Size = New System.Drawing.Size(332, 81)
    Me.tb_description.TabIndex = 1
    '
    'ef_catalog_name
    '
    Me.ef_catalog_name.DoubleValue = 0.0R
    Me.ef_catalog_name.IntegerValue = 0
    Me.ef_catalog_name.IsReadOnly = False
    Me.ef_catalog_name.Location = New System.Drawing.Point(32, 16)
    Me.ef_catalog_name.Name = "ef_catalog_name"
    Me.ef_catalog_name.PlaceHolder = Nothing
    Me.ef_catalog_name.Size = New System.Drawing.Size(271, 24)
    Me.ef_catalog_name.SufixText = "Sufix Text"
    Me.ef_catalog_name.SufixTextVisible = True
    Me.ef_catalog_name.TabIndex = 0
    Me.ef_catalog_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_catalog_name.TextValue = ""
    Me.ef_catalog_name.Value = ""
    Me.ef_catalog_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_catalog_description
    '
    Me.lbl_catalog_description.Location = New System.Drawing.Point(12, 49)
    Me.lbl_catalog_description.Margin = New System.Windows.Forms.Padding(0)
    Me.lbl_catalog_description.Name = "lbl_catalog_description"
    Me.lbl_catalog_description.Size = New System.Drawing.Size(99, 78)
    Me.lbl_catalog_description.TabIndex = 10
    Me.lbl_catalog_description.Text = "xDescripción"
    Me.lbl_catalog_description.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'gb_catalog_state
    '
    Me.gb_catalog_state.Controls.Add(Me.rb_catalog_disabled)
    Me.gb_catalog_state.Controls.Add(Me.rb_catalog_enabled)
    Me.gb_catalog_state.Location = New System.Drawing.Point(114, 133)
    Me.gb_catalog_state.Name = "gb_catalog_state"
    Me.gb_catalog_state.Size = New System.Drawing.Size(117, 75)
    Me.gb_catalog_state.TabIndex = 2
    Me.gb_catalog_state.TabStop = False
    Me.gb_catalog_state.Text = "Habilitado"
    '
    'rb_catalog_disabled
    '
    Me.rb_catalog_disabled.AutoSize = True
    Me.rb_catalog_disabled.Location = New System.Drawing.Point(7, 44)
    Me.rb_catalog_disabled.Name = "rb_catalog_disabled"
    Me.rb_catalog_disabled.Size = New System.Drawing.Size(40, 17)
    Me.rb_catalog_disabled.TabIndex = 2
    Me.rb_catalog_disabled.TabStop = True
    Me.rb_catalog_disabled.Text = "No"
    Me.rb_catalog_disabled.UseVisualStyleBackColor = True
    '
    'rb_catalog_enabled
    '
    Me.rb_catalog_enabled.AutoSize = True
    Me.rb_catalog_enabled.Location = New System.Drawing.Point(7, 21)
    Me.rb_catalog_enabled.Name = "rb_catalog_enabled"
    Me.rb_catalog_enabled.Size = New System.Drawing.Size(36, 17)
    Me.rb_catalog_enabled.TabIndex = 1
    Me.rb_catalog_enabled.TabStop = True
    Me.rb_catalog_enabled.Text = "Sí"
    Me.rb_catalog_enabled.UseVisualStyleBackColor = True
    '
    'frm_catalog_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(593, 500)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_catalog_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_catalog_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_catalog_items.ResumeLayout(False)
    Me.gb_catalog_state.ResumeLayout(False)
    Me.gb_catalog_state.PerformLayout()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents gb_catalog_items As System.Windows.Forms.GroupBox
    Friend WithEvents btn_catalog_add As GUI_Controls.uc_button
    Friend WithEvents btn_catalog_del As GUI_Controls.uc_button
    Friend WithEvents dg_catalog_items As GUI_Controls.uc_grid
    Friend WithEvents tb_description As System.Windows.Forms.TextBox
    Friend WithEvents ef_catalog_name As GUI_Controls.uc_entry_field
    Friend WithEvents lbl_catalog_description As System.Windows.Forms.Label
    Friend WithEvents gb_catalog_state As System.Windows.Forms.GroupBox
    Friend WithEvents rb_catalog_disabled As System.Windows.Forms.RadioButton
    Friend WithEvents rb_catalog_enabled As System.Windows.Forms.RadioButton
    Friend WithEvents btn_catalog_filter As GUI_Controls.uc_button
    Friend WithEvents ef_filter As GUI_Controls.uc_entry_field
End Class
