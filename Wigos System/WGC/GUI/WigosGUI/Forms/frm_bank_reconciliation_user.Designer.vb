<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bank_reconciliation_user
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_bank_reconciliation_user))
    Me.fra_user = New System.Windows.Forms.GroupBox()
    Me.btn_accept = New GUI_Controls.uc_button()
    Me.cmb_users = New GUI_Controls.uc_combo()
    Me.btn_exit = New GUI_Controls.uc_button()
    Me.fra_user.SuspendLayout()
    Me.SuspendLayout()
    '
    'fra_user
    '
    Me.fra_user.Controls.Add(Me.btn_accept)
    Me.fra_user.Controls.Add(Me.cmb_users)
    Me.fra_user.Location = New System.Drawing.Point(6, 2)
    Me.fra_user.Name = "fra_user"
    Me.fra_user.Size = New System.Drawing.Size(332, 120)
    Me.fra_user.TabIndex = 0
    Me.fra_user.TabStop = False
    Me.fra_user.Text = "fra_user"
    '
    'btn_accept
    '
    Me.btn_accept.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_accept.Location = New System.Drawing.Point(220, 72)
    Me.btn_accept.Name = "btn_accept"
    Me.btn_accept.Size = New System.Drawing.Size(90, 30)
    Me.btn_accept.TabIndex = 1
    Me.btn_accept.ToolTipped = False
    Me.btn_accept.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'cmb_users
    '
    Me.cmb_users.AllowUnlistedValues = False
    Me.cmb_users.AutoCompleteMode = False
    Me.cmb_users.IsReadOnly = False
    Me.cmb_users.Location = New System.Drawing.Point(19, 33)
    Me.cmb_users.Name = "cmb_users"
    Me.cmb_users.SelectedIndex = -1
    Me.cmb_users.Size = New System.Drawing.Size(293, 24)
    Me.cmb_users.SufixText = "Sufix Text"
    Me.cmb_users.SufixTextVisible = True
    Me.cmb_users.TabIndex = 0
    Me.cmb_users.TextCombo = Nothing
    Me.cmb_users.TextVisible = False
    Me.cmb_users.TextWidth = 0
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(248, 140)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 1
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_bank_reconciliation_user
    '
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(345, 186)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.fra_user)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_bank_reconciliation_user"
    Me.Text = "frm_bank_reconciliation_user"
    Me.fra_user.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Uc_combo1 As GUI_Controls.uc_combo
  Friend WithEvents fra_user As System.Windows.Forms.GroupBox
  Friend WithEvents btn_accept As GUI_Controls.uc_button
  Friend WithEvents cmb_users As GUI_Controls.uc_combo
  Friend WithEvents btn_exit As GUI_Controls.uc_button
End Class
