'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_movements
' DESCRIPTION:   Information about the movements of terminals 
' AUTHOR:        Raul Ruiz
' CREATION DATE: 31-JAN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-JAN-2013  RRB    Initial version.
' 11-JUL-2014  XIT    Added Multilanguage Support
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_terminal_movements
  Inherits frm_base_sel


#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
  End Sub
#End Region ' Windows Form Designer generated code

#Region " Enum "
  Public Enum TerminalsAudit
    ' Registration
    ActiveRegistration = &H0
    OutOfServiceRegistration = &H101
    RetiredRegistration = &H202

    ' Status Change
    ActiveToOutOfService = &H1
    ActiveToRetired = &H2
    OutOfServiceToActive = &H100
    OutOfServiceToRetired = &H102
    RetiredToActive = &H200
    RetiredToOutOfService = &H201
  End Enum

#End Region

#Region " Members "

  ' For report filters
  Private m_date_from As String
  Private m_date_to As String
  Private m_report_terminals As String
  Private m_report_terminal_status As String

#End Region ' Member

#Region " Constants "

  'TERMINAL_AUDIT_DATE, TERMINAL_ID, TERMINAL_NAME, PROVIDER_ID, TERMINAL_TYPE, GAME_TYPE, STATUS, NUM_ACTIVE_TERMS, NUM_OUT_OF_SERVICE_TERMS
  ' Index Columns of SQL
  Private Const SQL_COLUMN_TERMINAL_AUDIT_DATE As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const SQL_COLUMN_PROVIDER_ID As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 4
  Private Const SQL_COLUMN_GAME_TYPE As Integer = 5
  Private Const SQL_COLUMN_STATUS As Integer = 6
  Private Const SQL_COLUMN_NUM_ACTIVE_TERMS As Integer = 7
  Private Const SQL_COLUMN_NUM_OUT_OF_SERVICE_TERMS As Integer = 8

  ' Index Columns Grids
  Private Const GRID_COLUMN_TERMINAL_AUDIT_DATE As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 1
  Private Const GRID_COLUMN_PROVIDER_ID As Integer = 2
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const GRID_COLUMN_TERMINAL_TYPE As Integer = 4
  Private Const GRID_COLUMN_GAME_TYPE As Integer = 5
  Private Const GRID_COLUMN_STATUS As Integer = 6
  Private Const GRID_COLUMN_NUM_ACTIVE_TERMS As Integer = 7
  Private Const GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS As Integer = 8
  Private Const GRID_COLUMN_TOTAL_TERMS As Integer = 9

  ' Num columns
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 10

  ' Width columns
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TERMINAL_ID As Integer = 2000
  Private Const GRID_WIDTH_NAME As Integer = 2600
  Private Const GRID_WIDTH_TYPE As Integer = 1600
  Private Const GRID_WIDTH_STATUS As Integer = 3300
  Private Const GRID_WIDTH_NUM_TERMS As Integer = 1600
  Private Const FORM_DB_MIN_VERSION As Short = 219

#End Region ' Constants

#Region " Overrides "

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '				- None
  '
  '     - OUTPUT:
  '				- None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_from.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           	- RowIndex
  '           	- DbRow
  '
  '     - OUTPUT :
  '				- None
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    '  TERMINAL AUDIT DATE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_AUDIT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TERMINAL_AUDIT_DATE), _
                                                                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    '  TERMINAL ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    '  TERMINAL NAME
    If Not DbRow.IsNull(GRID_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = ""
    End If

    '  PROVIDER ID
    If Not DbRow.IsNull(GRID_COLUMN_PROVIDER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ID).Value = DbRow.Value(SQL_COLUMN_PROVIDER_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_ID).Value = ""
    End If

    '  TERMINAL TYPE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)

    '  GAME TYPE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_TYPE).Value = DbRow.Value(SQL_COLUMN_GAME_TYPE)

    '  STATUS
    Dim _status As TerminalsAudit
    _status = DbRow.Value(SQL_COLUMN_STATUS)

    Select Case _status
      Case TerminalsAudit.ActiveRegistration
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1635, CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE))

      Case TerminalsAudit.OutOfServiceRegistration
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1635, CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE))

      Case TerminalsAudit.RetiredRegistration
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1635, CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED))

      Case TerminalsAudit.ActiveToOutOfService
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE), CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE))

      Case TerminalsAudit.ActiveToRetired
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED), CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE))

      Case TerminalsAudit.OutOfServiceToActive
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE), CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE))

      Case TerminalsAudit.OutOfServiceToRetired
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED), CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE))

      Case TerminalsAudit.RetiredToActive
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE), CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED))

      Case TerminalsAudit.RetiredToOutOfService
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1636, CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE), CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED))

      Case Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = ""

    End Select

    '  NUM ACTIVE TERMS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_ACTIVE_TERMS).Value = DbRow.Value(SQL_COLUMN_NUM_ACTIVE_TERMS)

    '  NUM OUT OF SERVICE TERMS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS).Value = DbRow.Value(SQL_COLUMN_NUM_OUT_OF_SERVICE_TERMS)

    '  TOTAL TERMS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TERMS).Value = DbRow.Value(SQL_COLUMN_NUM_ACTIVE_TERMS) + DbRow.Value(SQL_COLUMN_NUM_OUT_OF_SERVICE_TERMS)

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '				- None
  '
  '     - OUTPUT:
  '				- None
  '
  ' RETURNS:
  '		- None
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINAL_MOVEMENTS
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '				- None
  '
  '     - OUTPUT:
  '				- None
  '
  ' RETURNS:
  '		- None
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    ' Title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1637)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date time filter
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminal types
    Dim _terminal_types() As Integer

    _terminal_types = WSI.Common.Misc.AllTerminalTypes()
    Call Me.uc_pr_list.Init(_terminal_types)

    ' Status
    gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
    chk_status_active.Text = CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE)
    chk_status_out_of_order.Text = CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE)
    chk_status_retired.Text = CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()
  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '				- None
  '
  '     - OUTPUT:
  '				- None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sql As New System.Text.StringBuilder

    'TERMINAL_AUDIT_DATE, TERMINAL_ID, TERMINAL_NAME, PROVIDER_ID, TERMINAL_TYPE, GAME_TYPE, STATUS, NUM_ACTIVE_TERMS, NUM_OUT_OF_SERVICE_TERMS
    _sql.AppendLine(" SELECT   TA_TERM_AUDIT_DATE ")
    _sql.AppendLine("        , TA_TERMINAL_ID     ")
    _sql.AppendLine("        , TA_TERMINAL_NAME   ")
    _sql.AppendLine("        , TA_PROVIDER_ID     ")
    _sql.AppendLine("        , (SELECT TT_NAME FROM TERMINAL_TYPES WHERE TT_TYPE = TA_TERMINAL_TYPE) AS TA_TERMINAL_TYPE ")
    _sql.AppendLine("        , (SELECT GT_NAME FROM GAME_TYPES WHERE GT_GAME_TYPE = TA_GAME_TYPE AND GT_LANGUAGE_ID = " & Resource.LanguageId & ") AS TA_GAME_TYPE")
    _sql.AppendLine("        , TA_STATUS          ")
    _sql.AppendLine("        , TA_NUM_ACTIVE_TERMS         ")
    _sql.AppendLine("        , TA_NUM_OUT_OF_SERVICE_TERMS ")
    _sql.AppendLine("   FROM   TERMINALS_AUDIT             ")
    _sql.AppendLine(GetSqlWhere())
    _sql.AppendLine("  ORDER   BY TA_TERM_AUDIT_DATE DESC  ")

    Return _sql.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Overridable routine to initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           	- None
  '     - OUTPUT:
  '           	- None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_FilterReset()
    SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT :
  '				- None
  '
  '     - OUTPUT :
  '				- None
  '
  ' RETURNS:
  '		- None
  '
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '				- None
  '
  '     - OUTPUT:
  '				- None
  '
  ' RETURNS:
  '		- None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)
  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE
  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    ' Date from
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    ' Date to
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    ' Terminal
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_report_terminals)
    ' Status
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(262), m_report_terminal_status)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_from = ""
    m_date_to = ""
    m_report_terminals = ""
    m_report_terminal_status = ""

    ' Activation Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_TIME_SHORT)
    End If

    ' Providers - Terminals
    m_report_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Status
    m_report_terminal_status = SetTerminalStatusFilter(False)
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Private "

  ' PURPOSE: Set filter related to terminal status
  '
  '  PARAMS:
  '     - INPUT :
  '           - SqlFilter
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - String
  '
  Private Function SetTerminalStatusFilter(ByVal SqlFilter As Boolean)
    Dim _num_terminals As Short
    Dim _str_filter As String

    _num_terminals = 0
    _str_filter = ""

    ' Terminal -> Active
    If chk_status_active.Checked Then
      If SqlFilter Then
        _str_filter = TerminalsAudit.ActiveRegistration & ", " & TerminalsAudit.OutOfServiceToActive & ", " & TerminalsAudit.RetiredToActive
      Else
        _str_filter = CLASS_TERMINAL.StatusName(TerminalStatus.ACTIVE)
      End If
      _num_terminals = _num_terminals + 1
    End If

    ' Terminal -> Out of service
    If chk_status_out_of_order.Checked Then
      If SqlFilter Then
        _str_filter = _str_filter & IIf(String.IsNullOrEmpty(_str_filter), _
                                        TerminalsAudit.ActiveToOutOfService & ", " & TerminalsAudit.OutOfServiceRegistration & ", " & TerminalsAudit.RetiredToOutOfService, _
                                        ", " & TerminalsAudit.ActiveToOutOfService & ", " & TerminalsAudit.OutOfServiceRegistration & ", " & TerminalsAudit.RetiredToOutOfService)
      Else
        _str_filter = _str_filter & IIf(String.IsNullOrEmpty(_str_filter), _
                                        CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE), _
                                        ", " & CLASS_TERMINAL.StatusName(TerminalStatus.OUT_OF_SERVICE))
      End If
      _num_terminals = _num_terminals + 1
    End If

    ' Terminal -> Retired
    If chk_status_retired.Checked Then
      If SqlFilter Then
        _str_filter = _str_filter & IIf(String.IsNullOrEmpty(_str_filter), _
                                        TerminalsAudit.ActiveToRetired & ", " & TerminalsAudit.OutOfServiceToRetired & ", " & TerminalsAudit.RetiredRegistration, _
                                        ", " & TerminalsAudit.ActiveToRetired & ", " & TerminalsAudit.OutOfServiceToRetired & ", " & TerminalsAudit.RetiredRegistration)
      Else
        _str_filter = _str_filter & IIf(String.IsNullOrEmpty(_str_filter), _
                                        CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED), _
                                        ", " & CLASS_TERMINAL.StatusName(TerminalStatus.RETIRED))
      End If
      _num_terminals = _num_terminals + 1
    End If

    If _num_terminals < 1 Or _num_terminals > 2 Then
      Return "" ' None terminal selected or all terminals selected
    End If

    If SqlFilter Then
      _str_filter = " TA_STATUS IN ( " & _str_filter & " )"
    End If

    Return _str_filter
  End Function ' SetTerminalStatusFilter

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      'TERMINAL_AUDIT_DATE, TERMINAL_ID, TERMINAL_NAME, PROVIDER_ID, TERMINAL_TYPE, GAME_TYPE, STATUS, NUM_ACTIVE_TERMS, NUM_OUT_OF_SERVICE_TERMS
      'GRID_COLUMN_TERMINAL_AUDIT_DATE
      .Column(GRID_COLUMN_TERMINAL_AUDIT_DATE).Header(0).Text = " "
      .Column(GRID_COLUMN_TERMINAL_AUDIT_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_TERMINAL_AUDIT_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_TERMINAL_AUDIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0 ' GRID_WIDTH_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_PROVIDER_ID 
      .Column(GRID_COLUMN_PROVIDER_ID).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_PROVIDER_ID).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(341)
      .Column(GRID_COLUMN_PROVIDER_ID).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_PROVIDER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(330)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_TERMINAL_TYPE 
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(322)
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TERMINAL_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_GAME_TYPE
      .Column(GRID_COLUMN_GAME_TYPE).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_GAME_TYPE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(427)
      .Column(GRID_COLUMN_GAME_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_GAME_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_STATUS
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(333)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_NUM_ACTIVE_TERMS
      .Column(GRID_COLUMN_NUM_ACTIVE_TERMS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1638)
      .Column(GRID_COLUMN_NUM_ACTIVE_TERMS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)
      .Column(GRID_COLUMN_NUM_ACTIVE_TERMS).Width = GRID_WIDTH_NUM_TERMS
      .Column(GRID_COLUMN_NUM_ACTIVE_TERMS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS
      .Column(GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1638)
      .Column(GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)
      .Column(GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS).Width = GRID_WIDTH_NUM_TERMS
      .Column(GRID_COLUMN_NUM_OUT_OF_SERVICE_TERMS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'GRID_COLUMN_TOTAL_TERMS
      .Column(GRID_COLUMN_TOTAL_TERMS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1638)
      .Column(GRID_COLUMN_TOTAL_TERMS).Header(1).Text = GLB_NLS_GUI_AUDITOR.GetString(423)
      .Column(GRID_COLUMN_TOTAL_TERMS).Width = GRID_WIDTH_NUM_TERMS
      .Column(GRID_COLUMN_TOTAL_TERMS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()
    ' Init dates
    dtp_from.Value = GUI_GetDate()
    dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_from.ShowCheckBox = True
    dtp_from.Checked = True

    dtp_to.Value = GUI_GetDate()
    dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_to.ShowCheckBox = True
    dtp_to.Checked = False

    ' Terminals
    Call Me.uc_pr_list.SetDefaultValues()

    ' Status
    chk_status_active.Checked = False
    chk_status_out_of_order.Checked = False
    chk_status_retired.Checked = False
  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function GetSqlWhere() As String
    Dim _str_where As String = ""
    Dim _str_filter As String = ""

    ' Add FROM date filter
    If dtp_from.Checked = True Then
      _str_filter = _str_filter & " TA_TERM_AUDIT_DATE >= " & GUI_FormatDateDB(dtp_from.Value)

    End If

    ' Add TO date filter
    If dtp_to.Checked = True Then
      If _str_filter.Length > 0 Then
        _str_filter = _str_filter & " AND "
      End If

      _str_filter = _str_filter & " TA_TERM_AUDIT_DATE <= " & GUI_FormatDateDB(dtp_to.Value)

    End If

    ' Add TERMINALS filter
    If _str_filter.Length > 0 Then
      _str_filter = _str_filter & " AND "
    End If
    _str_filter = _str_filter & " TA_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Add STATUS filter
    If _str_filter.Length > 0 And SetTerminalStatusFilter(True).Length > 0 Then
      _str_filter = _str_filter & " AND " & SetTerminalStatusFilter(True)
    Else
      _str_filter = _str_filter & SetTerminalStatusFilter(True)
    End If

    If _str_filter.Length > 0 Then
      _str_where = " WHERE " & _str_filter
    End If

    Return _str_where
  End Function ' GetSqlWhere

#End Region ' Private

#Region " Public "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region ' Public

#Region " Events "

#End Region ' Events

End Class
