<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_notes_register
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_num_notes = New System.Windows.Forms.GroupBox
    Me.dg_collect_list = New GUI_Controls.uc_grid
    Me.gb_terminal_options = New System.Windows.Forms.GroupBox
    Me.gb_num_notes.SuspendLayout()
    Me.gb_terminal_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.AutoSize = True
    Me.panel_data.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Top
    Me.panel_data.Size = New System.Drawing.Size(738, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(6, 15)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(310, 180)
    Me.uc_pr_list.TabIndex = 2
    '
    'gb_num_notes
    '
    Me.gb_num_notes.Controls.Add(Me.dg_collect_list)
    Me.gb_num_notes.Enabled = False
    Me.gb_num_notes.Location = New System.Drawing.Point(7, 6)
    Me.gb_num_notes.Name = "gb_num_notes"
    Me.gb_num_notes.Size = New System.Drawing.Size(378, 210)
    Me.gb_num_notes.TabIndex = 10
    Me.gb_num_notes.TabStop = False
    Me.gb_num_notes.Text = "xNumNotes"
    '
    'dg_collect_list
    '
    Me.dg_collect_list.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.dg_collect_list.CurrentCol = -1
    Me.dg_collect_list.CurrentRow = -1
    Me.dg_collect_list.Location = New System.Drawing.Point(11, 24)
    Me.dg_collect_list.Name = "dg_collect_list"
    Me.dg_collect_list.PanelRightVisible = False
    Me.dg_collect_list.Redraw = True
    Me.dg_collect_list.Size = New System.Drawing.Size(358, 119)
    Me.dg_collect_list.Sortable = False
    Me.dg_collect_list.SortAscending = True
    Me.dg_collect_list.SortByCol = 0
    Me.dg_collect_list.TabIndex = 11
    Me.dg_collect_list.ToolTipped = True
    Me.dg_collect_list.TopRow = -2
    '
    'gb_terminal_options
    '
    Me.gb_terminal_options.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.gb_terminal_options.Controls.Add(Me.uc_pr_list)
    Me.gb_terminal_options.Location = New System.Drawing.Point(393, 5)
    Me.gb_terminal_options.Name = "gb_terminal_options"
    Me.gb_terminal_options.Size = New System.Drawing.Size(343, 211)
    Me.gb_terminal_options.TabIndex = 11
    Me.gb_terminal_options.TabStop = False
    Me.gb_terminal_options.Text = "XTerminalOptions"
    '
    'frm_notes_register
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(834, 220)
    Me.Controls.Add(Me.gb_terminal_options)
    Me.Controls.Add(Me.gb_num_notes)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_notes_register"
    Me.Text = "frm_notes_register"
    Me.Controls.SetChildIndex(Me.gb_num_notes, 0)
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.Controls.SetChildIndex(Me.gb_terminal_options, 0)
    Me.gb_num_notes.ResumeLayout(False)
    Me.gb_terminal_options.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_num_notes As System.Windows.Forms.GroupBox
  Friend WithEvents gb_terminal_options As System.Windows.Forms.GroupBox
  Friend WithEvents dg_collect_list As GUI_Controls.uc_grid
End Class
