<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_capacity
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.gb_options = New System.Windows.Forms.GroupBox
    Me.lbl_color_average = New System.Windows.Forms.Label
    Me.lbl_color_site_capacity = New System.Windows.Forms.Label
    Me.chk_show_average = New System.Windows.Forms.CheckBox
    Me.opt_by_day = New System.Windows.Forms.RadioButton
    Me.opt_by_provider = New System.Windows.Forms.RadioButton
    Me.chk_show_providers_capacity = New System.Windows.Forms.CheckBox
    Me.chk_show_site_capacity = New System.Windows.Forms.CheckBox
    Me.chk_exclude_empty_providers = New System.Windows.Forms.CheckBox
    Me.lbl_num_days = New GUI_Controls.uc_text_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_num_days)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Size = New System.Drawing.Size(883, 310)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_num_days, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 314)
    Me.panel_data.Size = New System.Drawing.Size(883, 444)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(877, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(877, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(404, 91)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(386, 186)
    Me.uc_pr_list.TabIndex = 2
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.lbl_color_average)
    Me.gb_options.Controls.Add(Me.lbl_color_site_capacity)
    Me.gb_options.Controls.Add(Me.chk_show_average)
    Me.gb_options.Controls.Add(Me.opt_by_day)
    Me.gb_options.Controls.Add(Me.opt_by_provider)
    Me.gb_options.Controls.Add(Me.chk_show_providers_capacity)
    Me.gb_options.Controls.Add(Me.chk_show_site_capacity)
    Me.gb_options.Controls.Add(Me.chk_exclude_empty_providers)
    Me.gb_options.Location = New System.Drawing.Point(6, 92)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(392, 154)
    Me.gb_options.TabIndex = 1
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'lbl_color_average
    '
    Me.lbl_color_average.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_average.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.lbl_color_average.Location = New System.Drawing.Point(184, 124)
    Me.lbl_color_average.Name = "lbl_color_average"
    Me.lbl_color_average.Size = New System.Drawing.Size(31, 17)
    Me.lbl_color_average.TabIndex = 7
    '
    'lbl_color_site_capacity
    '
    Me.lbl_color_site_capacity.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_site_capacity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.lbl_color_site_capacity.Location = New System.Drawing.Point(184, 97)
    Me.lbl_color_site_capacity.Name = "lbl_color_site_capacity"
    Me.lbl_color_site_capacity.Size = New System.Drawing.Size(31, 17)
    Me.lbl_color_site_capacity.TabIndex = 6
    '
    'chk_show_average
    '
    Me.chk_show_average.AutoSize = True
    Me.chk_show_average.Location = New System.Drawing.Point(11, 123)
    Me.chk_show_average.Name = "chk_show_average"
    Me.chk_show_average.Size = New System.Drawing.Size(149, 17)
    Me.chk_show_average.TabIndex = 5
    Me.chk_show_average.Text = "xShow Daily Average"
    '
    'opt_by_day
    '
    Me.opt_by_day.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_by_day.Location = New System.Drawing.Point(30, 44)
    Me.opt_by_day.Name = "opt_by_day"
    Me.opt_by_day.Size = New System.Drawing.Size(185, 17)
    Me.opt_by_day.TabIndex = 1
    Me.opt_by_day.Text = "xBy Day"
    '
    'opt_by_provider
    '
    Me.opt_by_provider.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_by_provider.Location = New System.Drawing.Point(30, 67)
    Me.opt_by_provider.Name = "opt_by_provider"
    Me.opt_by_provider.Size = New System.Drawing.Size(185, 17)
    Me.opt_by_provider.TabIndex = 2
    Me.opt_by_provider.Text = "xBy Provider"
    '
    'chk_show_providers_capacity
    '
    Me.chk_show_providers_capacity.AutoSize = True
    Me.chk_show_providers_capacity.Location = New System.Drawing.Point(11, 21)
    Me.chk_show_providers_capacity.Name = "chk_show_providers_capacity"
    Me.chk_show_providers_capacity.Size = New System.Drawing.Size(176, 17)
    Me.chk_show_providers_capacity.TabIndex = 0
    Me.chk_show_providers_capacity.Text = "xShow Providers Capacity"
    '
    'chk_show_site_capacity
    '
    Me.chk_show_site_capacity.AutoSize = True
    Me.chk_show_site_capacity.Location = New System.Drawing.Point(11, 96)
    Me.chk_show_site_capacity.Name = "chk_show_site_capacity"
    Me.chk_show_site_capacity.Size = New System.Drawing.Size(144, 17)
    Me.chk_show_site_capacity.TabIndex = 4
    Me.chk_show_site_capacity.Text = "xShow Site Capacity"
    '
    'chk_exclude_empty_providers
    '
    Me.chk_exclude_empty_providers.CheckAlign = System.Drawing.ContentAlignment.TopLeft
    Me.chk_exclude_empty_providers.Location = New System.Drawing.Point(216, 44)
    Me.chk_exclude_empty_providers.Name = "chk_exclude_empty_providers"
    Me.chk_exclude_empty_providers.Size = New System.Drawing.Size(170, 40)
    Me.chk_exclude_empty_providers.TabIndex = 3
    Me.chk_exclude_empty_providers.Text = "xExclude empty providers"
    Me.chk_exclude_empty_providers.TextAlign = System.Drawing.ContentAlignment.TopLeft
    Me.chk_exclude_empty_providers.UseVisualStyleBackColor = True
    '
    'lbl_num_days
    '
    Me.lbl_num_days.AutoSize = True
    Me.lbl_num_days.IsReadOnly = True
    Me.lbl_num_days.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_num_days.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_num_days.Location = New System.Drawing.Point(277, 60)
    Me.lbl_num_days.Name = "lbl_num_days"
    Me.lbl_num_days.Size = New System.Drawing.Size(509, 24)
    Me.lbl_num_days.SufixText = "Sufix Text"
    Me.lbl_num_days.SufixTextVisible = True
    Me.lbl_num_days.TabIndex = 3
    Me.lbl_num_days.TabStop = False
    Me.lbl_num_days.TextWidth = 0
    Me.lbl_num_days.Value = "xNumber days: 999"
    '
    'frm_capacity
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(891, 762)
    Me.Name = "frm_capacity"
    Me.Text = "frm_capacity"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_providers_capacity As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_site_capacity As System.Windows.Forms.CheckBox
  Friend WithEvents chk_exclude_empty_providers As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_num_days As GUI_Controls.uc_text_field
  Friend WithEvents opt_by_day As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_provider As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_average As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_average As System.Windows.Forms.Label
  Friend WithEvents lbl_color_site_capacity As System.Windows.Forms.Label
End Class
