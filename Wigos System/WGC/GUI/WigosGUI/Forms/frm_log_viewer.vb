Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

Partial Public Class frm_log_viewer
Inherits frm_base

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Buttons
    btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)

  End Sub ' GUI_InitControls

  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As GUI_Controls.mdl_audit_form.AUDIT_FLAGS) As Boolean
    Select Case AuditType
      Case AUDIT_FLAGS.ACCESS
        Return False
      Case Else
        Return MyBase.GUI_HasToBeAudited(AuditType)
    End Select
  End Function
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent

  Public WriteOnly Property DisplayText() As String
    Set(ByVal Value As String)
      wb_log_viewer.DocumentText = Value
    End Set
  End Property

  Private Sub frm_log_viewer_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    HideLogger()
  End Sub ' frm_log_viewer_FormClosed

End Class