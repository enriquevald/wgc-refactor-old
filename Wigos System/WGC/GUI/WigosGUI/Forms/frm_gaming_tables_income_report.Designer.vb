<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_income_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_checked_list_table_type = New GUI_Controls.uc_checked_list()
    Me.gb_report_type = New System.Windows.Forms.GroupBox()
    Me.rb_grouped_type_detail = New System.Windows.Forms.RadioButton()
    Me.rb_groupped_type_total = New System.Windows.Forms.RadioButton()
    Me.gb_report_options = New System.Windows.Forms.GroupBox()
    Me.chb_table_type_totals = New System.Windows.Forms.CheckBox()
    Me.chb_only_with_activity = New System.Windows.Forms.CheckBox()
    Me.chb_order_by_name = New System.Windows.Forms.CheckBox()
    Me.chb_by_date = New System.Windows.Forms.CheckBox()
    Me.rb_interval_year = New System.Windows.Forms.RadioButton()
    Me.rb_interval_month = New System.Windows.Forms.RadioButton()
    Me.rb_interval_day = New System.Windows.Forms.RadioButton()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.lbl_note1 = New System.Windows.Forms.Label()
    Me.lbl_hold = New System.Windows.Forms.Label()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_report_type.SuspendLayout()
    Me.gb_report_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.lbl_hold)
    Me.panel_filter.Controls.Add(Me.lbl_note1)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_report_options)
    Me.panel_filter.Controls.Add(Me.gb_report_type)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_table_type)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1193, 211)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_table_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_report_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_report_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_note1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_hold, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 215)
    Me.panel_data.Size = New System.Drawing.Size(1193, 427)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1187, 21)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1187, 4)
    '
    'uc_checked_list_table_type
    '
    Me.uc_checked_list_table_type.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_table_type.Location = New System.Drawing.Point(556, 8)
    Me.uc_checked_list_table_type.m_resize_width = 359
    Me.uc_checked_list_table_type.multiChoice = True
    Me.uc_checked_list_table_type.Name = "uc_checked_list_table_type"
    Me.uc_checked_list_table_type.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_table_type.SelectedIndexesList = ""
    Me.uc_checked_list_table_type.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_table_type.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_table_type.SelectedValuesList = ""
    Me.uc_checked_list_table_type.SetLevels = 2
    Me.uc_checked_list_table_type.Size = New System.Drawing.Size(359, 173)
    Me.uc_checked_list_table_type.TabIndex = 3
    Me.uc_checked_list_table_type.ValuesArray = New String(-1) {}
    '
    'gb_report_type
    '
    Me.gb_report_type.Controls.Add(Me.rb_grouped_type_detail)
    Me.gb_report_type.Controls.Add(Me.rb_groupped_type_total)
    Me.gb_report_type.Location = New System.Drawing.Point(6, 91)
    Me.gb_report_type.Name = "gb_report_type"
    Me.gb_report_type.Size = New System.Drawing.Size(257, 90)
    Me.gb_report_type.TabIndex = 1
    Me.gb_report_type.TabStop = False
    Me.gb_report_type.Text = "xReportType"
    '
    'rb_grouped_type_detail
    '
    Me.rb_grouped_type_detail.AutoSize = True
    Me.rb_grouped_type_detail.Location = New System.Drawing.Point(20, 50)
    Me.rb_grouped_type_detail.Name = "rb_grouped_type_detail"
    Me.rb_grouped_type_detail.Size = New System.Drawing.Size(78, 17)
    Me.rb_grouped_type_detail.TabIndex = 1
    Me.rb_grouped_type_detail.TabStop = True
    Me.rb_grouped_type_detail.Text = "xByTable"
    Me.rb_grouped_type_detail.UseVisualStyleBackColor = True
    '
    'rb_groupped_type_total
    '
    Me.rb_groupped_type_total.AutoSize = True
    Me.rb_groupped_type_total.Location = New System.Drawing.Point(20, 27)
    Me.rb_groupped_type_total.Name = "rb_groupped_type_total"
    Me.rb_groupped_type_total.Size = New System.Drawing.Size(106, 17)
    Me.rb_groupped_type_total.TabIndex = 0
    Me.rb_groupped_type_total.TabStop = True
    Me.rb_groupped_type_total.Text = "xByTableType"
    Me.rb_groupped_type_total.UseVisualStyleBackColor = True
    '
    'gb_report_options
    '
    Me.gb_report_options.Controls.Add(Me.chb_table_type_totals)
    Me.gb_report_options.Controls.Add(Me.chb_only_with_activity)
    Me.gb_report_options.Controls.Add(Me.chb_order_by_name)
    Me.gb_report_options.Controls.Add(Me.chb_by_date)
    Me.gb_report_options.Controls.Add(Me.rb_interval_year)
    Me.gb_report_options.Controls.Add(Me.rb_interval_month)
    Me.gb_report_options.Controls.Add(Me.rb_interval_day)
    Me.gb_report_options.Location = New System.Drawing.Point(269, 7)
    Me.gb_report_options.Name = "gb_report_options"
    Me.gb_report_options.Size = New System.Drawing.Size(281, 174)
    Me.gb_report_options.TabIndex = 2
    Me.gb_report_options.TabStop = False
    Me.gb_report_options.Text = "xOptions"
    '
    'chb_table_type_totals
    '
    Me.chb_table_type_totals.AutoSize = True
    Me.chb_table_type_totals.Location = New System.Drawing.Point(19, 129)
    Me.chb_table_type_totals.Name = "chb_table_type_totals"
    Me.chb_table_type_totals.Size = New System.Drawing.Size(157, 17)
    Me.chb_table_type_totals.TabIndex = 5
    Me.chb_table_type_totals.Text = "xShowTableTypeTotals"
    Me.chb_table_type_totals.UseVisualStyleBackColor = True
    '
    'chb_only_with_activity
    '
    Me.chb_only_with_activity.AutoSize = True
    Me.chb_only_with_activity.Location = New System.Drawing.Point(19, 150)
    Me.chb_only_with_activity.Name = "chb_only_with_activity"
    Me.chb_only_with_activity.Size = New System.Drawing.Size(126, 17)
    Me.chb_only_with_activity.TabIndex = 6
    Me.chb_only_with_activity.Text = "xOnlyWithActivity"
    Me.chb_only_with_activity.UseVisualStyleBackColor = True
    '
    'chb_order_by_name
    '
    Me.chb_order_by_name.AutoSize = True
    Me.chb_order_by_name.Enabled = False
    Me.chb_order_by_name.Location = New System.Drawing.Point(19, 108)
    Me.chb_order_by_name.Name = "chb_order_by_name"
    Me.chb_order_by_name.Size = New System.Drawing.Size(187, 17)
    Me.chb_order_by_name.TabIndex = 4
    Me.chb_order_by_name.Text = "xGroupByTableOrTableType"
    Me.chb_order_by_name.UseVisualStyleBackColor = True
    '
    'chb_by_date
    '
    Me.chb_by_date.AutoSize = True
    Me.chb_by_date.Location = New System.Drawing.Point(19, 20)
    Me.chb_by_date.Name = "chb_by_date"
    Me.chb_by_date.Size = New System.Drawing.Size(110, 17)
    Me.chb_by_date.TabIndex = 0
    Me.chb_by_date.Text = "xGroupByDate"
    Me.chb_by_date.UseVisualStyleBackColor = True
    '
    'rb_interval_year
    '
    Me.rb_interval_year.AutoSize = True
    Me.rb_interval_year.Enabled = False
    Me.rb_interval_year.Location = New System.Drawing.Point(50, 84)
    Me.rb_interval_year.Name = "rb_interval_year"
    Me.rb_interval_year.Size = New System.Drawing.Size(73, 17)
    Me.rb_interval_year.TabIndex = 3
    Me.rb_interval_year.TabStop = True
    Me.rb_interval_year.Text = "xByYear"
    Me.rb_interval_year.UseVisualStyleBackColor = True
    '
    'rb_interval_month
    '
    Me.rb_interval_month.AutoSize = True
    Me.rb_interval_month.Enabled = False
    Me.rb_interval_month.Location = New System.Drawing.Point(50, 63)
    Me.rb_interval_month.Name = "rb_interval_month"
    Me.rb_interval_month.Size = New System.Drawing.Size(76, 17)
    Me.rb_interval_month.TabIndex = 2
    Me.rb_interval_month.TabStop = True
    Me.rb_interval_month.Text = "xMonthly"
    Me.rb_interval_month.UseVisualStyleBackColor = True
    '
    'rb_interval_day
    '
    Me.rb_interval_day.AutoSize = True
    Me.rb_interval_day.Enabled = False
    Me.rb_interval_day.Location = New System.Drawing.Point(50, 42)
    Me.rb_interval_day.Name = "rb_interval_day"
    Me.rb_interval_day.Size = New System.Drawing.Size(61, 17)
    Me.rb_interval_day.TabIndex = 1
    Me.rb_interval_day.TabStop = True
    Me.rb_interval_day.Text = "xDaily"
    Me.rb_interval_day.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 7)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 82)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'lbl_note1
    '
    Me.lbl_note1.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_note1.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_note1.ForeColor = System.Drawing.Color.Navy
    Me.lbl_note1.Location = New System.Drawing.Point(9, 184)
    Me.lbl_note1.Name = "lbl_note1"
    Me.lbl_note1.Size = New System.Drawing.Size(541, 27)
    Me.lbl_note1.TabIndex = 123
    Me.lbl_note1.Text = "xNote"
    '
    'lbl_hold
    '
    Me.lbl_hold.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_hold.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_hold.ForeColor = System.Drawing.Color.Navy
    Me.lbl_hold.Location = New System.Drawing.Point(556, 184)
    Me.lbl_hold.Name = "lbl_hold"
    Me.lbl_hold.Size = New System.Drawing.Size(359, 17)
    Me.lbl_hold.TabIndex = 128
    Me.lbl_hold.Text = "xHold"
    Me.lbl_hold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.HeightControl = 0
    Me.uc_multicurrency.Location = New System.Drawing.Point(919, 9)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.OpenModeControl = GUI_Controls.uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.SelectedCurrency = ""
    Me.uc_multicurrency.SelectedOption = GUI_Controls.uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode
    Me.uc_multicurrency.Size = New System.Drawing.Size(177, 78)
    Me.uc_multicurrency.TabIndex = 4
    Me.uc_multicurrency.WidthControl = 0
    '
    'frm_gaming_tables_income_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1203, 646)
    Me.Name = "frm_gaming_tables_income_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_table_gaming_income_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_report_type.ResumeLayout(False)
    Me.gb_report_type.PerformLayout()
    Me.gb_report_options.ResumeLayout(False)
    Me.gb_report_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_checked_list_table_type As GUI_Controls.uc_checked_list
  Friend WithEvents gb_report_type As System.Windows.Forms.GroupBox
  Friend WithEvents rb_grouped_type_detail As System.Windows.Forms.RadioButton
  Friend WithEvents rb_groupped_type_total As System.Windows.Forms.RadioButton
  Friend WithEvents gb_report_options As System.Windows.Forms.GroupBox
  Friend WithEvents rb_interval_year As System.Windows.Forms.RadioButton
  Friend WithEvents rb_interval_month As System.Windows.Forms.RadioButton
  Friend WithEvents rb_interval_day As System.Windows.Forms.RadioButton
  Friend WithEvents chb_by_date As System.Windows.Forms.CheckBox
  Friend WithEvents chb_only_with_activity As System.Windows.Forms.CheckBox
  Friend WithEvents chb_order_by_name As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chb_table_type_totals As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_note1 As System.Windows.Forms.Label
  Friend WithEvents lbl_hold As System.Windows.Forms.Label
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
End Class
