'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_reservation_monitor
' DESCRIPTION:   Terminal Reserve 
' AUTHOR:        Toni T�llez
' CREATION DATE: 26-JUL-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-JUL-2017  ATB    Initial version (PBI 28982: WIGOS-894: EGM Reserve - Phase 1 - Reserved monitor)
' 03-AUG-2017  ATB    PBI 28982: EGM Reserve - Phase 1 - Set free on GUI (WIGOS-4000)
' 14-AGU-2017  XGJ    Bug 29346: Monitor de Terminales - Is cutting up a Button "Liberar Todas"
' 21-AUG-2017  ATB    Bug 29409: Terminals - The exportation report is displaying the wrong date.
' 21-AUG-2017  ATB    Bug 29412: Terminals - A terminal selection is lost when the terminal is refreshing (WIGOS-4619)
' 22-AUG-2017  ATB    Bug 29412: Terminals - A terminal selection is lost when the terminal is refreshing (WIGOS-4619)
' 24-AUG-2017  ATB    Bug 29409: Terminals - The exportation report is displaying the wrong date (WIGOS-4676)
' 19-MAR-2018  AGS    Bug 31967: WIGOS-8910 Missing GUI information in Audit screen for Reservation monitor screen
' 20-APR-2018  RGR    Bug 32360: WIGOS-6401 The filters "Recents First" and "Oldest first" doent work as expected in the "Terminal Reservation Monitor" screen
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text
Imports WSI.Common.ReservedTerminal

Public Class frm_terminal_reservation_monitor
  Inherits frm_base_sel

#Region " Structures "

#End Region ' Structures

#Region " Constants "
  Private Const FORM_DATABASE_MIN_VERSION As Short = 203

  Private m_print_data As GUI_Reports.CLASS_PRINT_DATA
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA
  Private m_print_datetime As Date
  Private m_filter_order As String


  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private GRID_COLUMN_RESERVE_DATE As Int32 = 0
  Private GRID_COLUMN_ELAPSED_TIME As Int32 = 1
  Private GRID_COLUMN_REMAINING_TIME As Int32 = 2
  Private GRID_COLUMN_TERMINAL_ID As Int32 = 3
  Private GRID_COLUMN_PROVIDER As Int32 = 4
  Private GRID_COLUMN_TERMINAL As Int32 = 5
  Private GRID_COLUMN_ACCOUNT_ID As Int32 = 6
  Private GRID_COLUMN_PLAYER_CARD As Int32 = 7
  Private GRID_COLUMN_PLAYER_NAME As Int32 = 8
  Private GRID_COLUMN_LEVEL As Int32 = 9

  Private GRID_COLUMNS As Integer = 10

  Private Const GRID_COLUMN_FLAG_WIDTH = 2500

  Private Const SQL_COLUMN_ACCOUNT_ID = 0
  Private Const SQL_COLUMN_PLAYER_NAME = 1
  Private Const SQL_COLUMN_PLAYER_CARD = 2
  Private Const SQL_COLUMN_TERMINAL_ID = 3
  Private Const SQL_COLUMN_TERMINAL = 4
  Private Const SQL_COLUMN_PROVIDER = 5
  Private Const SQL_COLUMN_LEVEL = 6
  Private Const SQL_COLUMN_START_RESERVED = 7
  Private Const SQL_COLUMN_END_RESERVED = 8
  Private Const SQL_COLUMN_MAX_MINUTES = 9
  Private Const SQL_COLUMN_TOTAL_MINUTES = 10
  Private Const SQL_COLUMN_STATUS = 11

  Private Const GRID_MAX_COLUMNS As Integer = 10

#End Region ' Constants

#Region " Enums "

#End Region ' Enums

#Region " Members "

  Private m_first_time As Boolean = True
  Private m_grid_last_rows As Int32 = 0

  Private m_grid_columns As Integer

  Private m_timer_search As Boolean = False
  Private m_num_of_refreshes As Integer

  Private m_system_mode As SYSTEM_MODE

  Private m_refresh_grid As Boolean = False

  Private m_isOrderAsc As Boolean

  Private m_reserved_terminal_service As ReservedTerminalService

#End Region ' Members

#Region " Overrides "

  ''' <summary>
  ''' Establish Form Id, according to the enumeration in the application
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINAL_RESERVATION_MONITOR

    Call GUI_SetMinDbVersion(FORM_DATABASE_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    m_reserved_terminal_service = New ReservedTerminalService()
    Dim _filter_buttons As Control

    _filter_buttons = panel_filter.Controls.Find("panel_filter_buttons", True)(0)
    _filter_buttons.Visible = False

    ' Read System mode
    m_system_mode = WSI.Common.Misc.SystemMode()

    ' Set screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    ' Init grid columns
    m_grid_columns = GRID_COLUMNS

    ' Form Title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8534)

    ' Labels
    Me.gp_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8551)
    Me.rb_recent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8552)
    Me.rb_oldest.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8553)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(5)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8555) 'Release
    '14-AGU-2017 Bug 9346:Monitor de Terminales - Is cutting up a Button "Liberar Todas"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8556) 'Release All
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Size = New System.Drawing.Size(90, 45)
    '#If DEBUG Then
    '    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True
    '    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    '    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = "RESERVE*"
    '#End If


    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)

    Call GUI_StyleSheet()


    m_num_of_refreshes = 0

    ' Set filter default values
    Call SetDefaultValues()

    ' Set Timer (General Param)
    Me.tmr_monitor.Interval = GeneralParam.Get("Terminal", "Reservation.MonitorRefreshInterval", 5) * 1000
    Me.tmr_monitor.Enabled = True

    'MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    Me.rb_recent.Checked = True
    Me.rb_oldest.Checked = False

    ' Refresh grid for first time
    Call TimerRefreshGrid()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Manage buttons pressed
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Me.tmr_monitor.Stop()
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_num_of_refreshes = 0
        MyBase.GUI_ButtonClick(ButtonId)
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        If CurrentUser.Permissions(ENUM_FORM.FORM_TERMINAL_RESERVATION_MANAGE_TERMINALS).Execute Then
          ReleaseTerminal()
        Else
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        If CurrentUser.Permissions(ENUM_FORM.FORM_TERMINAL_RESERVATION_MANAGE_TERMINALS).Execute Then
          ReleaseAllTerminals()
        Else
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
        End If
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        ReserveTerminal()
      Case ENUM_BUTTON.BUTTON_CANCEL
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call GUI_ExecuteQuery()
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
    If ButtonId <> ENUM_BUTTON.BUTTON_CANCEL Then
      Me.tmr_monitor.Start()
    End If
  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Given a type, the function returns if it needs to be audited
  ''' </summary>
  ''' <param name="AuditType"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean
    Select Case AuditType
      Case AUDIT_FLAGS.SEARCH
        Return (Me.m_num_of_refreshes < 1)
      Case Else
        Return MyBase.GUI_HasToBeAudited(AuditType)
    End Select
  End Function ' GUI_HasToBeAudited

  ''' <summary>
  ''' Get the defined Query Type
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ''' <summary>
  ''' Build an SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("SELECT          RTT.RTT_ACCOUNT_ID                        ")
    _str_sql.AppendLine("              , AC.AC_HOLDER_NAME                         ")
    _str_sql.AppendLine("              , AC.AC_TRACK_DATA                          ")
    _str_sql.AppendLine("              , RTT.RTT_TERMINAL_ID                       ")
    _str_sql.AppendLine("              , T.TE_NAME                                 ")
    _str_sql.AppendLine("              , T.TE_PROVIDER_ID                          ")
    _str_sql.AppendLine("              , AC.AC_HOLDER_LEVEL                        ")
    _str_sql.AppendLine("              , RTT.RTT_START_RESERVED                    ")
    _str_sql.AppendLine("              , RTT.RTT_END_RESERVED                      ")
    _str_sql.AppendLine("              , RTT.RTT_MAX_MINUTES                       ")
    _str_sql.AppendLine("              , RTT.RTT_TOTAL_MINUTES                     ")
    _str_sql.AppendLine("              , RTT.RTT_STATUS                            ")
    _str_sql.AppendLine("FROM            RESERVED_TERMINAL_TRANSACTION RTT         ")
    _str_sql.AppendLine("INNER JOIN      ACCOUNTS AC                               ")
    _str_sql.AppendLine("        ON      RTT.RTT_ACCOUNT_ID = AC.AC_ACCOUNT_ID     ")
    _str_sql.AppendLine("INNER JOIN      TERMINALS T                               ")
    _str_sql.AppendLine("        ON      RTT.RTT_TERMINAL_ID = T.TE_TERMINAL_ID    ")
    _str_sql.AppendLine("WHERE           RTT.RTT_STATUS = 1                        ")
    '_str_sql.AppendLine("OR           RTT.RTT_STATUS = 3                          ") 'Only development!

    If m_isOrderAsc Then
      _str_sql.AppendLine(" ORDER BY RTT.RTT_START_RESERVED ASC                    ")
    Else
      _str_sql.AppendLine(" ORDER BY RTT.RTT_START_RESERVED DESC                   ")
    End If

    Return _str_sql.ToString

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Define the ExecuteQuery customized
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _str_sql As String
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer
    Dim _has_changed As Boolean

    Dim _terminal_id As Integer

    _table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"
    _has_changed = False
    _max_rows = GUI_MaxRows()

    Try

      Me.tmr_monitor.Stop()

      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then

        Return
      End If

      _table = GUI_GetTableUsingSQL(_str_sql, _max_rows)

      If _table Is Nothing Then

        Return
      End If

      Me.Grid.Clear()

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      'First, look for terminals and update data if exists in datagrid.
      While _idx_row < Me.Grid.NumRows

        _terminal_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value

        Dim _found_rows() As Data.DataRow

        ' Search for terminal
        _found_rows = _table.Select("rtt_terminal_id = " & _terminal_id & "")

        If _found_rows.Length > 0 Then

          _db_row = New CLASS_DB_ROW(_found_rows(0))
          Me.Grid.Redraw = False

          If GUI_SetupRow(_idx_row, _db_row) Then
            Me.Grid.Redraw = True
          End If

          ' mark row from datatable to be deleted
          _found_rows(0).Delete()

          _idx_row += 1
        Else
          ' Terminal not found, delete grid row!
          Me.Grid.Redraw = False
          Me.Grid.DeleteRow(_idx_row)
          Me.Grid.Redraw = True
        End If
      End While

      _table.AcceptChanges()

      'Insert new rows of new terminals
      For Each _dr As Data.DataRow In _table.Rows

        Me.Grid.Redraw = False
        Me.Grid.AddRow()

        _db_row = New CLASS_DB_ROW(_dr)

        If GUI_SetupRow(_idx_row, _db_row) Then
          _count += 1
          _idx_row += 1
        End If
        Me.Grid.Redraw = True
      Next


      Call GUI_AfterLastRow()

      m_grid_last_rows = _grid_rows

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If
      Me.tmr_monitor.Start()

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Terminal Status", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _has_changes As Boolean = True
    Dim _player_card As String
    Dim _format_time_difference As String
    Dim _time_span As TimeSpan
    _player_card = String.Empty
    Dim _level_names As Dictionary(Of Integer, String)
    _level_names = mdl_account_for_report.GetLevelNames()

    If Not DbRow.Value(SQL_COLUMN_START_RESERVED) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RESERVE_DATE).Value = DbRow.Value(SQL_COLUMN_START_RESERVED)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RESERVE_DATE).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_START_RESERVED) Is DBNull.Value Then
      _time_span = (DateTime.Now() - CType(DbRow.Value(SQL_COLUMN_START_RESERVED), DateTime))
      _format_time_difference = IIf(_time_span.Hours > 0, _time_span.Hours.ToString() + "h ", String.Empty) + _time_span.Minutes.ToString() + "' " + _time_span.Seconds.ToString() + "''"
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ELAPSED_TIME).Value = _format_time_difference
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ELAPSED_TIME).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_END_RESERVED) Is DBNull.Value Then
      _time_span = (CType(DbRow.Value(SQL_COLUMN_END_RESERVED), DateTime) - DateTime.Now())
      _format_time_difference = IIf(_time_span.Hours > 0, _time_span.Hours.ToString() + "h ", String.Empty) + _time_span.Minutes.ToString() + "'" + _time_span.Seconds.ToString() + "''"
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REMAINING_TIME).Value = _format_time_difference
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REMAINING_TIME).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_TERMINAL_ID) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_PROVIDER) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_TERMINAL) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_ACCOUNT_ID) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = "---"
    End If
    If Not DbRow.Value(SQL_COLUMN_PLAYER_CARD) Is DBNull.Value Then
      WSI.Common.CardNumber.TrackDataToExternal(_player_card, DbRow.Value(SQL_COLUMN_PLAYER_CARD), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYER_CARD).Value = _player_card
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYER_CARD).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_PLAYER_NAME) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYER_NAME).Value = DbRow.Value(SQL_COLUMN_PLAYER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYER_NAME).Value = "---"
    End If

    If Not DbRow.Value(SQL_COLUMN_LEVEL) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = _level_names(DbRow.Value(SQL_COLUMN_LEVEL))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = "---"
    End If

    Return _has_changes
  End Function ' GUI_SetupRow

#Region " GUI Reports "

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8551), m_filter_order)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' Set texts corresponding to the provided filter values for the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_filter_order = ""
    If Me.rb_recent.Checked Then
      m_isOrderAsc = False
      m_filter_order = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8552)
    Else
      m_isOrderAsc = True
      m_filter_order = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8553)
    End If
  End Sub ' GUI_ReportUpdateFilters
#End Region 'GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ''' <summary>
  ''' Opens dialog with default settings for edit mode
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ''' <summary>
  ''' Define all Main Grid Columns 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    Dim _idx_grid As Int32
    _idx_grid = 0

    With Me.Grid
      Call .Init(GRID_MAX_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' Reservation Time
      .Column(GRID_COLUMN_RESERVE_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8539) 'Reservation
      .Column(GRID_COLUMN_RESERVE_DATE).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_RESERVE_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8536) 'Reservation Time
      .Column(GRID_COLUMN_RESERVE_DATE).Width = 2000
      .Column(GRID_COLUMN_RESERVE_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Elapsed Time
      .Column(GRID_COLUMN_ELAPSED_TIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8539) 'Reservation
      .Column(GRID_COLUMN_ELAPSED_TIME).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_ELAPSED_TIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8537) 'Elapsed Time
      .Column(GRID_COLUMN_ELAPSED_TIME).Width = 1800
      .Column(GRID_COLUMN_ELAPSED_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Remaining Time
      .Column(GRID_COLUMN_REMAINING_TIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8539) 'Reservation
      .Column(GRID_COLUMN_REMAINING_TIME).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_REMAINING_TIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8538) 'Remaining Time
      .Column(GRID_COLUMN_REMAINING_TIME).Width = 1800
      .Column(GRID_COLUMN_REMAINING_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal ID
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8540) 'EGM
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795) 'Number
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 1200
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal Provider
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8540) 'EGM
      .Column(GRID_COLUMN_PROVIDER).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PROVIDER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469) 'Provider
      .Column(GRID_COLUMN_PROVIDER).Width = 1800
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Name
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8540) 'EGM
      .Column(GRID_COLUMN_TERMINAL).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) 'Name
      .Column(GRID_COLUMN_TERMINAL).Width = 1800
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) 'Account
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795) 'Number
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1200
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Card
      .Column(GRID_COLUMN_PLAYER_CARD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) 'Account
      .Column(GRID_COLUMN_PLAYER_CARD).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PLAYER_CARD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157) 'Card
      .Column(GRID_COLUMN_PLAYER_CARD).Width = 2300
      .Column(GRID_COLUMN_PLAYER_CARD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Player Name
      .Column(GRID_COLUMN_PLAYER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) 'Account
      .Column(GRID_COLUMN_PLAYER_NAME).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PLAYER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) 'Holder Name
      .Column(GRID_COLUMN_PLAYER_NAME).Width = 1800
      .Column(GRID_COLUMN_PLAYER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Player Level
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) 'Account
      .Column(GRID_COLUMN_LEVEL).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4808) 'Level
      .Column(GRID_COLUMN_LEVEL).Width = 1500
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()
    Dim _terminal_types() As Integer = Nothing

    ReDim Preserve _terminal_types(0 To 0)
    _terminal_types(0) = WSI.Common.TerminalTypes.SAS_HOST
  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Execute proper actions when programmed timer elapses
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub TimerRefreshGrid()
    m_timer_search = True
    m_num_of_refreshes += 1
    Call GUI_ExecuteQueryCustom()
    Call GUI_AfterLastRow()
    If Me.Grid.NumRows > 0 Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
    Else
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    End If
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = (Me.Grid.NumRows > 0)
    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    m_timer_search = False

    Me.Grid.Redraw = True
  End Sub ' TimerRefreshGrid

  ''' <summary>
  ''' Unlock the selected terminal from a reservation
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ReleaseTerminal()
    Dim _idx_row As Int32
    Dim _terminal_id As Integer
    Dim _account_id As Integer
    Dim _terminal_name As String
    Dim _selected_found As Boolean
    Dim _is_reserved As TerminalReservation.ReservedTerminalResponse
    Dim auditor_data As CLASS_AUDITOR_DATA



    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_TERMINALS_BOOKING)
    _is_reserved = New TerminalReservation.ReservedTerminalResponse()

    ' Resetting to take the connection again
    m_reserved_terminal_service = New ReservedTerminalService()

    ' Stops the monitor working while releasing the terminals
    Me.tmr_monitor.Stop()

    _terminal_id = 0
    _account_id = 0
    _terminal_name = String.Empty
    _selected_found = False

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        _selected_found = True
        Exit For
      End If
    Next

    ' If there's no terminal selected
    If Not _selected_found Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8558), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    If Not Integer.TryParse(Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value, _terminal_id) Then
      Return
    End If

    _terminal_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL).Value

    ' Getting the account id from the grid
    Integer.TryParse(Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value, _account_id)

    'Prompt the user if sure
    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8559), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _terminal_name) = ENUM_MB_RESULT.MB_RESULT_YES Then

      'Release the terminal
      _is_reserved = TerminalReservation.Release(_terminal_id)
      If _is_reserved.Success Then
        Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8562), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8562))
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8561), _terminal_name)
        auditor_data.Notify(GLB_CurrentUser.GuiId,
                   GLB_CurrentUser.Id, _
                   GLB_CurrentUser.Name, _
                   CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                   0)

      End If
      Call MsgBox(_is_reserved.Message, MsgBoxStyle.OkOnly, GLB_NLS_GUI_PLAYER_TRACKING.GetString("8535"))

      Call TimerRefreshGrid()
      Me.Grid.ClearSelection()
    End If
  End Sub

  ''' <summary>
  ''' Unlock all the reserved terminals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ReleaseAllTerminals()
    Dim _is_reserved As TerminalReservation.ReservedTerminalResponse
    Dim auditor_data As CLASS_AUDITOR_DATA

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_TERMINALS_BOOKING)
    _is_reserved = New TerminalReservation.ReservedTerminalResponse()

    ' Stops the monitor working while releasing the terminals
    Me.tmr_monitor.Stop()

    'Prompt the user if sure
    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8560), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
      ' Unlocking all the terminals
      _is_reserved = TerminalReservation.ReleaseAll()

      ' Setting the auditory fields
      Call auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8562), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8562))
      For Each term As String In _is_reserved.MessageExtended.Split("|")(0).Split(",")
        Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6529), term) 'OK
      Next

      'In case of terminals failed
      If _is_reserved.MessageExtended.Split("|").Length > 1 Then
        For Each term As String In _is_reserved.MessageExtended.Split("|")(1).Split(",")
          Call auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(8570), term) 'KO
        Next
      End If

      auditor_data.Notify(GLB_CurrentUser.GuiId, _
                 GLB_CurrentUser.Id, _
                 GLB_CurrentUser.Name, _
                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                 0)

      Call MsgBox(_is_reserved.Message, MsgBoxStyle.OkOnly, GLB_NLS_GUI_PLAYER_TRACKING.GetString("8535"))

      Call TimerRefreshGrid()
      Me.Grid.ClearSelection()
    End If


  End Sub

  ''' <summary>
  ''' Testing purposes, it won't be shown by default
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ReserveTerminal()
    Dim _idx_row As Int32
    Dim _terminal_id As Integer
    Dim _account_id As Integer
    Dim _terminal_name As String
    Dim _selected_found As Boolean
    Dim _reserved_terminal_reserved_request As ReservedTerminalReservedRequest
    _reserved_terminal_reserved_request = New ReservedTerminalReservedRequest()
    Dim isReserved As ReservedTerminalResponse(Of Boolean)

    ' Resetting to take the connection again
    m_reserved_terminal_service = New ReservedTerminalService()
    isReserved = New ReservedTerminalResponse(Of Boolean)()

    ' Stops the monitor working while releasing the terminals
    Me.tmr_monitor.Stop()

    _terminal_id = 0
    _account_id = 0
    _terminal_name = String.Empty
    _selected_found = False

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        _selected_found = True
        Exit For
      End If
    Next

    If Not _selected_found Then
      Return
    End If

    If Not Integer.TryParse(Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value, _terminal_id) Then
      Return
    End If
    Integer.TryParse(Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value, _account_id)
    _reserved_terminal_reserved_request.TerminalID = _terminal_id
    _reserved_terminal_reserved_request.AccountID = _account_id

    isReserved = m_reserved_terminal_service.Add(_reserved_terminal_reserved_request)

    Call MsgBox(isReserved.Message, MsgBoxStyle.OkOnly, GLB_NLS_GUI_PLAYER_TRACKING.GetString("8535"))
  End Sub
#End Region  ' Private Functions

#Region " Events "

  Private Sub tmr_monitor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_monitor.Tick
    Call TimerRefreshGrid()
  End Sub ' tmr_monitor_Tick

  Private Sub frm_terminal_reserve_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
    Me.Grid.Redraw = True
  End Sub

  Private Sub rb_recent_CheckedChanged(sender As Object, e As EventArgs) Handles rb_recent.CheckedChanged
    m_filter_order = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8552)
    m_isOrderAsc = False
    GUI_ExecuteQueryCustom()
  End Sub

  Private Sub rb_oldest_CheckedChanged(sender As Object, e As EventArgs) Handles rb_oldest.CheckedChanged   
    m_filter_order = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8553)
    m_isOrderAsc = True
    GUI_ExecuteQueryCustom()
  End Sub

#End Region ' Events

End Class ' frm_terminal_reserve
