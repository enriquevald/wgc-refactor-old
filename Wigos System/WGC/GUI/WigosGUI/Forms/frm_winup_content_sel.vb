﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_winup_notifications_sel
' DESCRIPTION:   
' AUTHOR:        Pablo Molina
' CREATION DATE: 24-FEB-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-FEB-2017  PDM    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class frm_winup_content_sel
  Inherits frm_base_sel



#Region "Members"

  Private m_sectionschematype As Integer

#End Region

#Region "Constants"

  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS_COUNT As Integer = 6

  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_TITLE As Integer = 2
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 3
  Private Const GRID_COLUMN_ORDER As Integer = 4
  Private Const GRID_COLUMN_STATUS As Integer = 5

  Private Const NULL_VALUE As String = "NULL"

#End Region

#Region "Constructor"

  Public Sub New()
    Me.New(0)
  End Sub

  Public Sub New(ByVal SectionSchemaType As Integer)
    m_sectionschematype = SectionSchemaType
    Me.InitializeComponent()
  End Sub

#End Region

#Region "Overrides"


  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_WINUP_CONTENT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()


  End Sub ' GUI_FilterReset

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetCustomDataTable() As DataTable
    Return ReadContents()
  End Function 'GUI_GetCustomDataTable

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = CLASS_WINUP_CONTENT.GetSectionSchemaNameById(m_sectionschematype)

    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ef_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
    chk_enable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    chk_disable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    gb_enable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)

    Me.ef_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 1000)

    Call Me.GUI_StyleSheet()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer

    Try
      _table = GUI_GetCustomDataTable()


      If _table Is Nothing Or _table.Rows.Count = 0 Then
        Return
      End If

      _grid_rows = _table.Rows.Count

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      _table.DefaultView.Sort = "[Order] ASC"
      _table = _table.DefaultView.ToTable()

      For _idx_row = 0 To _table.Rows.Count - 1

        If _idx_row >= Me.Grid.NumRows Then
          Me.Grid.AddRow()
        End If

        _db_row = New CLASS_DB_ROW(_table.Rows(_idx_row))

        Me.Grid.Redraw = False

        If GUI_SetupRow(_idx_row, _db_row) Then
          Me.Grid.Redraw = True
        End If

      Next


      While _idx_row < Me.Grid.NumRows
        Me.Grid.DeleteRow(_idx_row)
      End While

      Call GUI_AfterLastRow()

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cash Monitor", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)
      Me.Grid.Redraw = True

    End Try
  End Sub 'GUI_ExecuteQueryCustom

  ' PURPOSE : Fills the rows
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                        ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(6), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITLE).Value = DbRow.Value(1)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = DbRow.Value(2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORDER).Value = DbRow.Value(4)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = IIf(DbRow.Value(5), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))

    Return True

  End Function ''GUI_SetupRow


  ' PURPOSE: Process button actions 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        GUI_EditNewItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        'Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()
    'If Me.Permissions.Write Then

    Dim _idx_row As Short
    Dim _id As Integer
    Dim _frm_edit As frm_winup_content_edit

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    '' Get the  ID 
    _id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _frm_edit = New frm_winup_content_edit(m_sectionschematype)

    Call _frm_edit.ShowEditItem(_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

    ' End If
  End Sub ''GUI_EditSelectedItem

  Protected Overrides Sub GUI_EditNewItem()

    Dim _frm_edit As frm_winup_content_edit
    _frm_edit = New frm_winup_content_edit(m_sectionschematype)

    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ''GUI_EditNewItem

#End Region

#Region "Private Functions"

  Private Function ReadContents() As DataTable

    Dim _url As String
    Dim _api As CLS_API
    Dim _tbl As DataTable
    Dim _response, _responseSanitized As String
    Dim _jarray As JArray
    Dim _items() As String = {"dateUpdate", "listImageId", "detailImageId"}
    Dim _use_filters As Boolean
    Dim _title As String
    Dim _status As String
    Dim _datefrom As String
    Dim _dateto As String

    _use_filters = False
    _status = False
    _tbl = Nothing

    Try

      If Not String.IsNullOrEmpty(ef_title.Value) Then
        _title = ef_title.Value
        _use_filters = True
      Else
        _title = NULL_VALUE
      End If

      If Me.dtp_from.Checked Then
        _datefrom = dtp_from.Value.ToString("yyyyMMdd")
        _use_filters = True
      Else
        _datefrom = NULL_VALUE
      End If

      If Me.dtp_to.Checked Then
        _dateto = dtp_to.Value.ToString("yyyyMMdd")
        _use_filters = True
      Else
        _dateto = NULL_VALUE
      End If

      If (chk_enable.Checked Xor chk_disable.Checked) Then
        If chk_enable.Checked Then
          _status = "1"
        ElseIf chk_disable.Checked Then
          _status = "0"
        End If
        _use_filters = True
      Else
        _status = NULL_VALUE
      End If


      If _use_filters Then
        _url = GetUrlForApi(_title, _status, _datefrom, _dateto)
      Else
        _url = GetUrlForApi()
      End If


      _api = New CLS_API(_url)
      _response = _api.getData()

      _responseSanitized = _api.sanitizeData(_response)
      _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
      _tbl = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))
    End Try

    Return _tbl

  End Function

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT)

      '' ID
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = 0

      '' TITLE
      .Column(GRID_COLUMN_DATE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_COLUMN_DATE).Width = 1500
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '' TITLE
      .Column(GRID_COLUMN_TITLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
      .Column(GRID_COLUMN_TITLE).Width = 2500
      .Column(GRID_COLUMN_TITLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '' MESSAGE
      .Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7935)
      .Column(GRID_COLUMN_DESCRIPTION).Width = 4300
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '' MESSAGE
      .Column(GRID_COLUMN_ORDER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5778)
      .Column(GRID_COLUMN_ORDER).Width = 1200
      .Column(GRID_COLUMN_ORDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      .Column(GRID_COLUMN_STATUS).Width = 1200
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


    End With

  End Sub

  Public Function GetUrlForApi()

    Dim _url As String

    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/content/" + String.Format("{0:d}", m_sectionschematype)
    ''_url = "http://localhost:33224/api" + "/content/" + String.Format("{0:d}", m_sectionschematype)

    Return _url

  End Function ' GetUrlForApi

  Public Function GetUrlForApi(ByVal Title As String, ByVal Status As String, ByVal DateFrom As String, ByVal DateTo As String)

    Dim _url As String

    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/content/getbyfilters/" + String.Format("{0:d}", m_sectionschematype) + "/" + Title + "/" + Status + "/" + DateFrom + "/" + DateTo
    ''_url = "http://localhost:33224/api" + "/content/getbyfilters/" + String.Format("{0:d}", m_sectionschematype) + "/" + Title + "/" + Status + "/" + DateFrom + "/" + DateTo

    Return _url

  End Function ' GetUrlForApi


#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

End Class