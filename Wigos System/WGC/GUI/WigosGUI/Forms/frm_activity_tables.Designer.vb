﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_activity_tables
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dt_datetime = New GUI_Controls.uc_date_picker()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.dt_datetime)
    Me.panel_filter.Size = New System.Drawing.Size(1128, 80)
    Me.panel_filter.Controls.SetChildIndex(Me.dt_datetime, 0)
    '
    'panel_data
    '
    Me.panel_data.Size = New System.Drawing.Size(1128, 465)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1122, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1122, 4)
    '
    'dt_datetime
    '
    Me.dt_datetime.Checked = False
    Me.dt_datetime.IsReadOnly = False
    Me.dt_datetime.Location = New System.Drawing.Point(17, 16)
    Me.dt_datetime.Name = "dt_datetime"
    Me.dt_datetime.ShowCheckBox = False
    Me.dt_datetime.ShowUpDown = False
    Me.dt_datetime.Size = New System.Drawing.Size(256, 24)
    Me.dt_datetime.SufixText = "Sufix Text"
    Me.dt_datetime.SufixTextVisible = True
    Me.dt_datetime.TabIndex = 15
    Me.dt_datetime.Value = New Date(2016, 3, 24, 0, 0, 0, 0)
    '
    'frm_activity_tables
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1136, 553)
    Me.Name = "frm_activity_tables"
    Me.Text = "frm_activity_tables"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dt_datetime As GUI_Controls.uc_date_picker
End Class
