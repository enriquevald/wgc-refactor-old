'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_multisite_monitor.vb
' DESCRIPTION:   Multisite monitor
' AUTHOR:        Javier Molina Moral
' CREATION DATE: 01-MAR-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-MAR-2013  JMM    Initial version
' 18-APR-2013  JMM    Some code optimizing
' 06-JUN-2013  ANG    Fix bug #822
' 06-JUN-2013  JMM    Fix bug #828
' 20-MAY-2014  JBC    Added DownloadPatterns type
' 05-DIC-2014  DCS    Added Task Download Lcd Messages
' 14-JAN-2015  DCS    Added Task Upload Handpays
' 10-MAR-2015  ANM    Added Task Sells and pays
' 31-MAR-2017  MS     WIGOS-411 Creditlines
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_multisite_monitor
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  ' GRID UPLOAD DEFINITIONS
  Private Const GRID_NUM_COL_UPLOAD As Integer = 6
  Private Const GRID_COLUMN_UPLOAD_TASK_ID As Integer = 0
  Private Const GRID_COLUMN_UPLOAD_TASK_NAME As Integer = 1
  Private Const GRID_COLUMN_UPLOAD_STATUS As Integer = 2
  Private Const GRID_COLUMN_UPLOAD_PENDING_CHANGES As Integer = 3
  Private Const GRID_COLUMN_UPLOAD_LAST_SYNC As Integer = 4
  Private Const GRID_COLUMN_UPLOAD_NEXT_SYNC As Integer = 5

  ' GRID DOWNLOAD DEFINITIONS
  Private Const GRID_NUM_COL_DOWNLOAD As Integer = 7
  Private Const GRID_COLUMN_DOWNLOAD_TASK_ID As Integer = 0
  Private Const GRID_COLUMN_DOWNLOAD_TASK_NAME As Integer = 1
  Private Const GRID_COLUMN_DOWNLOAD_STATUS As Integer = 2
  Private Const GRID_COLUMN_DOWNLOAD_SEQ_ID As Integer = 3
  Private Const GRID_COLUMN_DOWNLOAD_PROGRESS As Integer = 4
  Private Const GRID_COLUMN_DOWNLOAD_PENDING_CHANGES As Integer = 5
  Private Const GRID_COLUMN_DOWNLOAD_LAST_SYNC As Integer = 6

  ' QUERY CONNECTION STATUS
  Private Const SQL_COLUMN_CONN_STATUS As Integer = 0
  Private Const SQL_COLUMN_CONN_LAST_CHANGE As Integer = 1
  Private Const SQL_COLUMN_CONN_LAST_MESSAGE_SENT As Integer = 2
  Private Const SQL_COLUMN_CONN_LAST_ADDRESS As Integer = 3

  ' QUERY UPLOAD PROCESSES
  Private Const SQL_COLUMN_UPLOAD_TASK_ID As Integer = 0
  Private Const SQL_COLUMN_UPLOAD_RUNNING As Integer = 1
  Private Const SQL_COLUMN_UPLOAD_LAST_SYNC As Integer = 2
  Private Const SQL_COLUMN_UPLOAD_SYNC_SINCE As Integer = 3
  Private Const SQL_COLUMN_UPLOAD_SYNC_INTERVAL As Integer = 4
  Private Const SQL_COLUMN_UPLOAD_NUM_PENDING As Integer = 5
  Private Const SQL_COLUMN_UPLOAD_STARTED_SYNC As Integer = 6

  ' QUERY DOWNLOAD PROCESSES
  Private Const SQL_COLUMN_DOWNLOAD_TASK_ID As Integer = 0
  Private Const SQL_COLUMN_DOWNLOAD_REPORTING As Integer = 1
  Private Const SQL_COLUMN_DOWNLOAD_LAST_SYNC As Integer = 2
  Private Const SQL_COLUMN_DOWNLOAD_NUM_PENDING As Integer = 3
  Private Const SQL_COLUMN_DOWNLOAD_LOCAL_SEQUENCE_ID As Integer = 4
  Private Const SQL_COLUMN_DOWNLOAD_REMOTE_SEQUENCE_ID As Integer = 5

  ' Timer interval to refresh multisite status (in milliseconds)
  Private Const REFRESH_INTERVAL = 5000

  Private Const UNDEFINED_STR = "---"

  Private Const COLUMN_DOWNLOAD_STATUS_WIDTH As Short = 2230
  Private Const COLUMN_UPLOAD_STATUS_WIDTH As Short = 5730

#End Region ' Constants

#Region " Structures "

#End Region ' Structures

#Region " Members "

  Private m_green_color As Color = Color.FromArgb(0, 192, 0)
  Private m_red_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
  Private m_pending_changes_perfil As Integer = 0
  Private m_pending_changes_user As Integer = 0

#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MULTISITE_MONITOR

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1787) '1787 "Connection with MultiSite"

    Call Me.GUI_StyleUploadGrid()
    Call Me.GUI_StyleDownloadGrid()

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Permissions.Execute
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1759) '1759 "Force Task"
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)

    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = UNDEFINED_STR

    Me.tf_current_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1747) '1747 "Status"
    Me.tf_current_status.Value = UNDEFINED_STR

    Me.tf_status_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1748) '1748 "Connection"
    Me.tf_status_change.Value = UNDEFINED_STR

    Me.tf_last_message_sent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1749) '1749 "Last Message Sent"
    Me.tf_last_message_sent.Value = UNDEFINED_STR

    Me.tf_last_address.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1750) '1750 "Center Address"
    Me.tf_last_address.Value = UNDEFINED_STR

    Me.lbl_uploading_processes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1757) '1757 "Upload"
    Me.lbl_downloading_processes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1758) '1758 "Download"

    Me.RefreshMonitor()

    Me.timer.Interval = REFRESH_INTERVAL
    Me.timer.Start()

  End Sub 'GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

  End Sub 'GUI_GetScreenData  

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Me.DbStatus = ENUM_STATUS.STATUS_OK
  End Sub 'GUI_DB_Operation

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return True
  End Function 'GUI_IsScreenDataOk()

  ' PURPOSE: Handle Form buttons cliks
  '
  '  PARAMS:
  '     - INPUT: Clicked button object
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call ForceReSync()

      Case ENUM_BUTTON.BUTTON_OK
        ' TRICK 
        ' Form not perform update operations not check updates and writting permision...
        ' Change button to 'Cancel' In order to avoid 'The user does not have permission..' 
        ' when user closes form  without writting permisions.
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_CANCEL)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform any clean-up , release used memory in image cache
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_Exit()
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    If MultiSiteConfigured() Then
      Call Me.Display(False)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  '----------------------------------------------------------------------------
  ' PURPOSE : Gets if the site is configured to a MultiSite system
  '
  ' PARAMS :
  '          - GroupKey
  '          - SubjectKey
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  Private Function MultiSiteConfigured() As Boolean
    Dim _sql_transaction As SqlTransaction
    Dim _multisite_configured_str As String
    Dim _multisite_configured As Int32

    _sql_transaction = Nothing
    _multisite_configured = 0

    If Not GUI_BeginSQLTransaction(_sql_transaction) Then
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_multisite_monitor", _
                            "ShowForEdit", _
                            "An error occurred calling GUI_BeginSQLTransaction")

      Return False
    End If

    'MultiSite Configured
    _multisite_configured_str = ReadGeneralParam("Site", "MultiSiteMember", _sql_transaction)

    If Not Int32.TryParse(_multisite_configured_str, _multisite_configured) Then
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_multisite_monitor", _
                            "ShowForEdit", _
                            "Wrong value for GeneralParam Site.MultiSiteMember")
      Return False
    End If

    If _multisite_configured <> 1 Then
      '1695 "This site is not attached to any MultiSite system."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1695), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True
  End Function ' MultiSiteConfigured

  '----------------------------------------------------------------------------
  ' PURPOSE : Read a General Param directly from db.
  '
  ' PARAMS :
  '          - GroupKey
  '          - SubjectKey
  '
  ' RETURNS :
  '     - ENUM_CONFIGURATION_RC
  '
  ' NOTES :
  Private Function ReadGeneralParam(ByVal GroupKey As String, ByVal SubjectKey As String, ByVal Trx As SqlTransaction) As String
    Dim _str_bld As StringBuilder
    Dim _value As String

    _value = ""

    _str_bld = New StringBuilder()
    _str_bld.AppendLine("SELECT   GP_KEY_VALUE                  ")
    _str_bld.AppendLine("  FROM   GENERAL_PARAMS                ")
    _str_bld.AppendLine(" WHERE   GP_GROUP_KEY = @pGroupKey     ")
    _str_bld.AppendLine("   AND   GP_SUBJECT_KEY = @pSubjectKey ")

    Using _sql_cmd As New SqlCommand(_str_bld.ToString())
      _sql_cmd.Connection = Trx.Connection
      _sql_cmd.Transaction = Trx

      _sql_cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar).Value = GroupKey
      _sql_cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar).Value = SubjectKey

      _value = _sql_cmd.ExecuteScalar()
    End Using

    Return _value
  End Function ' ReadGeneralParam

  ' PURPOSE:  Set the columns from Upload processes grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleUploadGrid()

    With Me.dg_upload_tasks
      Call .Init(GRID_NUM_COL_UPLOAD, 1)

      ' Task ID
      .Column(GRID_COLUMN_UPLOAD_TASK_ID).Header.Text = ""
      .Column(GRID_COLUMN_UPLOAD_TASK_ID).Width = 0
      .Column(GRID_COLUMN_UPLOAD_TASK_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Task
      .Column(GRID_COLUMN_UPLOAD_TASK_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1751) '1751 "Task"
      .Column(GRID_COLUMN_UPLOAD_TASK_NAME).Width = 4500
      .Column(GRID_COLUMN_UPLOAD_TASK_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_UPLOAD_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752) '1752 "Status"  
      .Column(GRID_COLUMN_UPLOAD_STATUS).Width = COLUMN_UPLOAD_STATUS_WIDTH
      .Column(GRID_COLUMN_UPLOAD_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Pending changes
      .Column(GRID_COLUMN_UPLOAD_PENDING_CHANGES).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1753) '1753 "PendingChanges"
      .Column(GRID_COLUMN_UPLOAD_PENDING_CHANGES).Width = 0
      .Column(GRID_COLUMN_UPLOAD_PENDING_CHANGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Last Update
      .Column(GRID_COLUMN_UPLOAD_LAST_SYNC).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1754) '1754 "Last Update"
      .Column(GRID_COLUMN_UPLOAD_LAST_SYNC).Width = 0
      .Column(GRID_COLUMN_UPLOAD_LAST_SYNC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Next Update
      .Column(GRID_COLUMN_UPLOAD_NEXT_SYNC).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1755) '1755 "Next Update"
      .Column(GRID_COLUMN_UPLOAD_NEXT_SYNC).Width = 2200
      .Column(GRID_COLUMN_UPLOAD_NEXT_SYNC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub 'GUI_StyleDownloadGrid

  ' PURPOSE:  Set the columns from Download processes grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleDownloadGrid()

    With Me.dg_download_tasks
      Call .Init(GRID_NUM_COL_DOWNLOAD, 1)

      ' Task ID
      .Column(GRID_COLUMN_DOWNLOAD_TASK_ID).Header.Text = ""
      .Column(GRID_COLUMN_DOWNLOAD_TASK_ID).Width = 0
      .Column(GRID_COLUMN_DOWNLOAD_TASK_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Task Name
      .Column(GRID_COLUMN_DOWNLOAD_TASK_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1751) '1751 "Task"
      .Column(GRID_COLUMN_DOWNLOAD_TASK_NAME).Width = 4500
      .Column(GRID_COLUMN_DOWNLOAD_TASK_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_DOWNLOAD_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752) '1752 "Status"  
      .Column(GRID_COLUMN_DOWNLOAD_STATUS).Width = COLUMN_DOWNLOAD_STATUS_WIDTH
      .Column(GRID_COLUMN_DOWNLOAD_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Progress
      .Column(GRID_COLUMN_DOWNLOAD_PROGRESS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1756) '1756 "Progress"
      .Column(GRID_COLUMN_DOWNLOAD_PROGRESS).Width = 1400
      .Column(GRID_COLUMN_DOWNLOAD_PROGRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Local Sequence ID
      .Column(GRID_COLUMN_DOWNLOAD_SEQ_ID).Header.Text = ""
      .Column(GRID_COLUMN_DOWNLOAD_SEQ_ID).Width = 0
      .Column(GRID_COLUMN_DOWNLOAD_SEQ_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Pending changes
      .Column(GRID_COLUMN_DOWNLOAD_PENDING_CHANGES).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1753) '1753 "PendingChanges"
      .Column(GRID_COLUMN_DOWNLOAD_PENDING_CHANGES).Width = 2100
      .Column(GRID_COLUMN_DOWNLOAD_PENDING_CHANGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Last Update
      .Column(GRID_COLUMN_DOWNLOAD_LAST_SYNC).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1754) '1754 "Last Update"
      .Column(GRID_COLUMN_DOWNLOAD_LAST_SYNC).Width = 2200
      .Column(GRID_COLUMN_DOWNLOAD_LAST_SYNC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub 'GUI_StyleDownloadGrid

  ' PURPOSE:  Reads the current WWP connection status from WWP_Status
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function ReadCurrentConnectionStatus() As Int32

    Dim _str_bld As StringBuilder
    Dim _sql_transaction As SqlTransaction
    Dim _status As Int32
    Dim _status_string As String
    Dim _status_changed As String
    Dim _last_message_sent As String
    Dim _last_address As String
    Dim _color As Color
    Dim _date_diff As TimeSpan

    _sql_transaction = Nothing

    _status = WWP_CONNECTION_STATUS.UNKNOWN
    _color = m_red_color

    _status_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(786) '786 "Unknown"
    _status_changed = UNDEFINED_STR
    _last_message_sent = UNDEFINED_STR
    _last_address = UNDEFINED_STR

    If GUI_BeginSQLTransaction(_sql_transaction) Then

      _str_bld = New StringBuilder()
      _str_bld.AppendLine("SELECT   WWP_STATUS         ")
      _str_bld.AppendLine("       , WWP_STATUS_CHANGED ")
      _str_bld.AppendLine("       , WWP_LAST_SENT_MSG  ")
      _str_bld.AppendLine("       , WWP_LAST_ADDRESS   ")
      _str_bld.AppendLine("  FROM   WWP_STATUS         ")
      _str_bld.AppendLine(" WHERE   WWP_TYPE = @pType  ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = WWP_CONNECTION_TYPE.MULTISITE

        Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()
          If _sql_reader.Read() Then
            _status = _sql_reader.GetInt32(SQL_COLUMN_CONN_STATUS)

            If Not _sql_reader.IsDBNull(SQL_COLUMN_CONN_LAST_CHANGE) Then
              _status_changed = GUI_FormatDate(_sql_reader.GetDateTime(SQL_COLUMN_CONN_LAST_CHANGE), _
                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                               ENUM_FORMAT_TIME.FORMAT_HHMMSS)
            End If

            If Not _sql_reader.IsDBNull(SQL_COLUMN_CONN_LAST_MESSAGE_SENT) Then
              _last_message_sent = GUI_FormatDate(_sql_reader.GetDateTime(SQL_COLUMN_CONN_LAST_MESSAGE_SENT), _
                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                  ENUM_FORMAT_TIME.FORMAT_HHMMSS)
            End If

            If Not _sql_reader.IsDBNull(SQL_COLUMN_CONN_LAST_ADDRESS) Then
              _last_address = _sql_reader.GetString(SQL_COLUMN_CONN_LAST_ADDRESS)
            End If
          End If
        End Using
      End Using

      If _status = WWP_CONNECTION_STATUS.CONNECTED Then
        'Check if last message is old enough to consider connection lost
        _date_diff = GUI_GetDBDateTime().Subtract(_last_message_sent)
        If _date_diff.TotalMilliseconds >= CommonMultiSite.WwpConnectionTimeout Then
          _status = WWP_CONNECTION_STATUS.DISCONNECTED
        End If
      End If

      Select Case _status
        Case WWP_CONNECTION_STATUS.CONNECTED
          _status_string = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
          _color = m_green_color

        Case WWP_CONNECTION_STATUS.DISCONNECTED
          _status_string = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)
          _color = m_red_color

          _status_changed = UNDEFINED_STR
          _last_message_sent = UNDEFINED_STR
          _last_address = UNDEFINED_STR

        Case Else
          _status_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(786) '786 "Unknown"
          _color = m_red_color

      End Select
    End If

    Me.tf_current_status.Value = _status_string
    Me.tf_current_status.LabelForeColor = _color
    Me.tf_status_change.Value = _status_changed
    Me.tf_last_message_sent.Value = _last_message_sent
    Me.tf_last_address.Value = _last_address

    Return _status
  End Function ' ReadCurrentConnectionStatus

  ' PURPOSE:  Reads the uploading multisite process information
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ReadUploadProcessesStatus(ByVal ConnectionStatus As Int32)
    Dim _str_bld As StringBuilder
    Dim _sql_transaction As SqlTransaction
    Dim _idx_row As Integer
    Dim _task_id As String
    Dim _found_rows() As Data.DataRow
    Dim _data_row As DataRow

    _sql_transaction = Nothing

    dg_upload_tasks.Redraw = False

    If GUI_BeginSQLTransaction(_sql_transaction) Then

      _str_bld = New StringBuilder()

      _str_bld.AppendLine("  SELECT   ST_TASK_ID                                                  ")
      _str_bld.AppendLine("         , ST_RUNNING                                                  ")
      _str_bld.AppendLine("         , ST_LAST_RUN                                                 ")
      _str_bld.AppendLine("         , DATEDIFF (second, ST_LAST_RUN, GETDATE())                   ")
      _str_bld.AppendLine("         , ST_INTERVAL_SECONDS                                         ")
      _str_bld.AppendLine("         , ST_NUM_PENDING                                              ")
      _str_bld.AppendLine("         , ST_STARTED                                                  ")
      _str_bld.AppendLine("    FROM   MS_SITE_TASKS                                               ")
      _str_bld.AppendLine("   WHERE   ST_TASK_ID IN (@pType1,  @pType2,  @pType3,  @pType4, @pType5  ")
      _str_bld.AppendLine("                        , @pType6,  @pType7,  @pType8,  @pType9, @pType10 ")
      _str_bld.AppendLine("                        , @pType11, @pType12, @pType13, @pType14, @pType15 ")
      _str_bld.AppendLine("                        , @pType16, @pType17, @pType18, @pType19) ")
      _str_bld.AppendLine("     AND   ST_ENABLED = 1                                              ")
      _str_bld.AppendLine("ORDER BY   ST_TASK_ID                                                  ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)
        _sql_cmd.Parameters.Add("@pType1", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements
        _sql_cmd.Parameters.Add("@pType2", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo
        _sql_cmd.Parameters.Add("@pType3", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour
        _sql_cmd.Parameters.Add("@pType4", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions
        _sql_cmd.Parameters.Add("@pType5", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadProviders
        _sql_cmd.Parameters.Add("@pType6", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadBanks
        _sql_cmd.Parameters.Add("@pType7", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadAreas
        _sql_cmd.Parameters.Add("@pType8", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadTerminals
        _sql_cmd.Parameters.Add("@pType9", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected
        _sql_cmd.Parameters.Add("@pType10", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadProvidersGames
        _sql_cmd.Parameters.Add("@pType11", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions
        _sql_cmd.Parameters.Add("@pType12", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments
        _sql_cmd.Parameters.Add("@pType13", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadLastActivity
        _sql_cmd.Parameters.Add("@pType14", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour
        _sql_cmd.Parameters.Add("@pType15", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadAlarms
        _sql_cmd.Parameters.Add("@pType16", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadHandpays
        _sql_cmd.Parameters.Add("@pType17", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys
        _sql_cmd.Parameters.Add("@pType18", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadCreditLines
        _sql_cmd.Parameters.Add("@pType19", SqlDbType.Int).Value = TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()

          _sql_adap.SelectCommand = _sql_cmd

          Using _data_set = New DataSet()
            _sql_adap.Fill(_data_set)

            _idx_row = 0

            ' update existing rows
            While _idx_row < dg_upload_tasks.NumRows

              _task_id = dg_upload_tasks.Cell(_idx_row, GRID_COLUMN_UPLOAD_TASK_ID).Value

              If _task_id <> "" Then
                ' Search for site
                _found_rows = _data_set.Tables(0).Select("ST_TASK_ID = " & _task_id)

                If _found_rows.Length > 0 Then
                  ' Site found: Update fields
                  UpdateUploadRow(_found_rows(0), _idx_row, ConnectionStatus)

                  ' mark row from datatable to be deleted
                  _found_rows(0).Delete()

                  _idx_row += 1
                Else
                  ' Task not found, delete grid row!
                  dg_upload_tasks.DeleteRow(_idx_row)
                End If
              End If
            End While

            _data_set.Tables(0).AcceptChanges()

            ' New rows to add
            For Each _data_row In _data_set.Tables(0).Rows
              _idx_row = dg_upload_tasks.AddRow()
              UpdateUploadRow(_data_row, _idx_row, ConnectionStatus)
            Next
          End Using
        End Using
      End Using
    End If

    dg_upload_tasks.Redraw = True
  End Sub ' ReadUploadProcessesStatus

  ' PURPOSE:  Format the data read from database to the upload grid cols
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateUploadRow(ByVal Row As DataRow, ByVal RowIndex As Int32, ByVal ConnectionStatus As Int32)
    Dim _running As Boolean
    Dim _sync_since As Int32
    Dim _last_sync As DateTime
    Dim _next_sync As DateTime
    Dim _date_diff As TimeSpan
    Dim _sync_started As DateTime
    Dim _task_id As Integer
    Dim _task_name As String
    Dim _task_status As String
    Dim _pending_changes_str As String
    Dim _pending_changes As Integer
    Dim _sync_since_str As String
    Dim _next_sync_str As String
    Dim _now As DateTime
    Dim _days_offline As Int32
    Dim _hours_offline As Int32
    Dim _mins_offline As Int32
    Dim _seconds_offline As Int32

    _task_id = -1
    _task_name = UNDEFINED_STR
    _task_status = UNDEFINED_STR
    _next_sync_str = UNDEFINED_STR
    _now = GUI_GetDBDateTime()
    _pending_changes_str = UNDEFINED_STR
    _sync_since_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1833) '1833 "Not updated"

    ' Task ID
    _task_id = Row.Item(SQL_COLUMN_UPLOAD_TASK_ID)

    ' Name
    _task_name = mdl_multisite.TranslateMultiSiteProcessID(_task_id, False)

    ' Last Sync
    If Not Row.IsNull(SQL_COLUMN_UPLOAD_SYNC_SINCE) Then
      _sync_since = Row.Item(SQL_COLUMN_UPLOAD_SYNC_SINCE)
      _date_diff = New TimeSpan(0, 0, _sync_since)
      _sync_since_str = ""
      _days_offline = Math.Max(Math.Truncate(_date_diff.TotalDays), 0)
      _hours_offline = Math.Max(_date_diff.Hours, 0)
      _mins_offline = Math.Max(_date_diff.Minutes, 0)
      _seconds_offline = Math.Max(_date_diff.Seconds, 0)

      If _days_offline > 0 Then
        _sync_since_str = _days_offline.ToString & "d " & _hours_offline.ToString() & "h " & _mins_offline.ToString("00") & "m " & _seconds_offline.ToString("00") & "s"
      Else
        If _hours_offline > 0 Then
          _sync_since_str = _hours_offline.ToString() & "h " & _mins_offline.ToString("00") & "m " & _seconds_offline.ToString("00") & "s"
        Else
          If _mins_offline > 0 Then
            _sync_since_str = _mins_offline.ToString() & "m " & _seconds_offline.ToString("00") & "s"
          Else
            _sync_since_str = _seconds_offline.ToString() & "s"
          End If
        End If
      End If

      '1832 "Updated %1 ago"
      _sync_since_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1832, _sync_since_str)
    End If

    If Not Row.IsNull(SQL_COLUMN_UPLOAD_LAST_SYNC) Then
      _last_sync = Row.Item(SQL_COLUMN_UPLOAD_LAST_SYNC)

      ' Next sync is calculated adding the sync interval to the last sync
      If Not Row.IsNull(SQL_COLUMN_UPLOAD_SYNC_INTERVAL) Then
        _next_sync = _last_sync.AddSeconds(Row.Item(SQL_COLUMN_UPLOAD_SYNC_INTERVAL))

        If _next_sync > _now Then
          _next_sync_str = GUI_FormatDate(_next_sync, _
                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
      End If
    End If

    ' Next Sync
    If Not Row.IsNull(SQL_COLUMN_UPLOAD_RUNNING) Then
      _running = Row.Item(SQL_COLUMN_UPLOAD_RUNNING)
    End If


    ' Pending Changes
    If Not Row.IsNull(SQL_COLUMN_UPLOAD_NUM_PENDING) Then
      _pending_changes = Row.Item(SQL_COLUMN_UPLOAD_NUM_PENDING)
      _pending_changes_str = _pending_changes
    End If

    ' Status
    If _running And ConnectionStatus = WWP_CONNECTION_STATUS.CONNECTED Then
      _task_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2006) ' 2006 "Updating"

      ' When process is currently running, will display the time that the process is running
      If Not Row.IsNull(SQL_COLUMN_UPLOAD_STARTED_SYNC) Then
        _sync_started = Row.Item(SQL_COLUMN_UPLOAD_STARTED_SYNC)
        _date_diff = _now.Subtract(_sync_started)
        _task_status = _task_status & "... " & String.Format("{0:HH:mm:ss}", New DateTime(_date_diff.Ticks))
        _next_sync_str = UNDEFINED_STR
      End If
    Else
      _task_status = _sync_since_str
    End If

    If _pending_changes > 0 Then
      '1776 "Pending Changes"
      _task_status = _task_status & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1776) & " (" & _pending_changes & ")"
    End If

    With dg_upload_tasks
      .Cell(RowIndex, GRID_COLUMN_UPLOAD_TASK_ID).Value = _task_id
      .Cell(RowIndex, GRID_COLUMN_UPLOAD_TASK_NAME).Value = _task_name
      .Cell(RowIndex, GRID_COLUMN_UPLOAD_STATUS).Value = _task_status
      .Cell(RowIndex, GRID_COLUMN_UPLOAD_PENDING_CHANGES).Value = _pending_changes_str
      .Cell(RowIndex, GRID_COLUMN_UPLOAD_LAST_SYNC).Value = _sync_since_str
      .Cell(RowIndex, GRID_COLUMN_UPLOAD_NEXT_SYNC).Value = _next_sync_str
    End With
  End Sub ' UpdateUploadRow

  ' PURPOSE:  Reads the downloading multisite process information
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ReadDownloadProcessesStatus(ByVal ConnectionStatus As Int32)
    Dim _str_bld As StringBuilder
    Dim _sql_transaction As SqlTransaction
    Dim _idx_row As Integer
    Dim _task_id As String
    Dim _data_row As DataRow
    Dim _found_rows() As Data.DataRow
    Dim _data_table As New DataTable()

    _sql_transaction = Nothing

    dg_download_tasks.Redraw = False


    If GUI_BeginSQLTransaction(_sql_transaction) Then

      _str_bld = New StringBuilder()

      _str_bld.AppendLine("  SELECT   ST_TASK_ID                                ")
      _str_bld.AppendLine("         , ST_RUNNING                                ")
      _str_bld.AppendLine("         , ST_LAST_RUN                               ")
      _str_bld.AppendLine("         , ST_NUM_PENDING                            ")
      _str_bld.AppendLine("         , ST_LOCAL_SEQUENCE_ID                      ")
      _str_bld.AppendLine("         , ST_REMOTE_SEQUENCE_ID                     ")
      _str_bld.AppendLine("    FROM   MS_SITE_TASKS                             ")
      _str_bld.AppendLine("   WHERE   ST_TASK_ID IN( @pDownloadConfiguration    ")
      _str_bld.AppendLine("                         ,@pDownloadPoints_Site      ")
      _str_bld.AppendLine("                         ,@pDownloadPoints_All        ")
      _str_bld.AppendLine("                         ,@pDownloadPersonalInfo_Card ")
      _str_bld.AppendLine("                         ,@pDownloadPersonalInfo_Site ")
      _str_bld.AppendLine("                         ,@pDownloadPersonalInfo_All  ")
      _str_bld.AppendLine("                         ,@pDownloadProvidersGames    ")
      _str_bld.AppendLine("                         ,@pDownloadProviders         ")
      _str_bld.AppendLine("                         ,@pDownloadMasterProfiles    ")
      _str_bld.AppendLine("                         ,@pDownloadAccountDocuments  ")
      _str_bld.AppendLine("                         ,@pDownloadCorporateUsers    ")
      _str_bld.AppendLine("                         ,@pDownloadAlarmPatterns     ")
      _str_bld.AppendLine("                         ,@pDownloadLcdMessages    ")
      _str_bld.AppendLine("                         ,@pDownloadBucketsConfig)    ")
      _str_bld.AppendLine("     AND   ST_ENABLED = 1                   ")
      _str_bld.AppendLine("ORDER BY   ST_TASK_ID                       ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)

        For Each _task_item As Byte In [Enum].GetValues(GetType(TYPE_MULTISITE_SITE_TASKS))
          _sql_cmd.Parameters.Add("@p" & [Enum].GetName(GetType(TYPE_MULTISITE_SITE_TASKS), _task_item), SqlDbType.Int).Value = _task_item
        Next

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()

          _sql_adap.SelectCommand = _sql_cmd

          _sql_adap.Fill(_data_table)

          _idx_row = 0

          Call mdl_multisite.SortTaskDataTable(_data_table, mdl_multisite.TaskOrderSiteDownload)

          ' update existing rows
          While _idx_row < dg_download_tasks.NumRows

            _task_id = dg_download_tasks.Cell(_idx_row, GRID_COLUMN_DOWNLOAD_TASK_ID).Value

            If _task_id <> "" Then
              ' Search for site
              _found_rows = _data_table.Select("ST_TASK_ID = " & _task_id)

              If _found_rows.Length > 0 Then

                UpdateDownloadRow(_found_rows(0), _idx_row, ConnectionStatus)

                ' mark row from datatable to be deleted
                _found_rows(0).Delete()

                _idx_row += 1
              Else
                ' Terminal not found, delete grid row!
                dg_download_tasks.DeleteRow(_idx_row)
              End If
            End If
          End While

          _data_table.AcceptChanges()

          ' New rows to add
          For Each _data_row In _data_table.Rows
            _idx_row = dg_download_tasks.AddRow()
            UpdateDownloadRow(_data_row, _idx_row, ConnectionStatus)
          Next

        End Using
      End Using
      _data_table.Dispose()
    End If

    dg_download_tasks.Redraw = True
  End Sub ' ReadDownloadProcessesStatus

  ' PURPOSE:  Gets the progress from a task
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetTaskProgress(ByVal RowIndex As Int32, ByVal LocalSequenceId As Int32, ByVal RemoteSequenceId As Int32) As String
    Dim _aux_local_seq_id As String
    Dim _last_local_seq_id As Int32
    Dim _current_progress As Int32

    If LocalSequenceId = 0 Then
      Me.dg_download_tasks.Cell(RowIndex, GRID_COLUMN_DOWNLOAD_SEQ_ID).Value = 0

      If RemoteSequenceId = 0 Then
        Return UNDEFINED_STR
      End If
    End If

    _aux_local_seq_id = Me.dg_download_tasks.Cell(RowIndex, GRID_COLUMN_DOWNLOAD_SEQ_ID).Value

    If _aux_local_seq_id = "" Then
      Me.dg_download_tasks.Cell(RowIndex, GRID_COLUMN_DOWNLOAD_SEQ_ID).Value = 0
      Return UNDEFINED_STR
    End If

    _last_local_seq_id = _aux_local_seq_id

    If LocalSequenceId = RemoteSequenceId Then
      Me.dg_download_tasks.Cell(RowIndex, GRID_COLUMN_DOWNLOAD_SEQ_ID).Value = LocalSequenceId
      Return UNDEFINED_STR
    End If

    If RemoteSequenceId = _last_local_seq_id Then
      Return UNDEFINED_STR
    End If

    If LocalSequenceId > RemoteSequenceId Then
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      "frm_multisite_monitor", _
                      "GetTaskProgress", _
                      "LocalSequenceId is higher than RemoteSequenceId")
    End If

    _current_progress = (LocalSequenceId - _last_local_seq_id) / (RemoteSequenceId - _last_local_seq_id) * 100
    _current_progress = Math.Min(_current_progress, 100) ' 100% is the hightest value possible
    _current_progress = Math.Max(0, _current_progress) ' 0% is the lowest possible value

    Return _current_progress & "%"

  End Function 'GetTaskProgress

  ' PURPOSE:  Format the data read from database to the download grid cols
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateDownloadRow(ByVal Row As DataRow, ByVal RowIndex As Int32, ByVal ConnectionStatus As Int32)
    Dim _task_id As Integer
    Dim _task_name As String
    Dim _running As Boolean
    Dim _task_status As String
    Dim _last_sync As String
    Dim _pending_changes As Integer
    Dim _pending_changes_str As String
    Dim _local_sequence_id As Integer
    Dim _remote_sequence_id As Integer
    Dim _progress As String
    Dim _pending_changes_true As Integer

    _task_status = UNDEFINED_STR
    _last_sync = UNDEFINED_STR
    _pending_changes_str = UNDEFINED_STR
    _progress = UNDEFINED_STR
    _local_sequence_id = 0
    _remote_sequence_id = 0
    _pending_changes_true = 0

    ' Task ID
    _task_id = Row.Item(SQL_COLUMN_DOWNLOAD_TASK_ID)

    ' Task Name
    _task_name = mdl_multisite.TranslateMultiSiteProcessID(_task_id, False)

    ' Status
    If Not Row.IsNull(SQL_COLUMN_DOWNLOAD_REPORTING) Then
      _running = Row.Item(SQL_COLUMN_DOWNLOAD_REPORTING)

      If _running And ConnectionStatus = WWP_CONNECTION_STATUS.CONNECTED Then
        _task_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2006) '2006 "Updating"
      Else
        _task_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(548) '548 "No activity"
      End If
    End If

    ' Last Sync
    If Not Row.IsNull(SQL_COLUMN_DOWNLOAD_LAST_SYNC) _
       And Not _running Then
      _last_sync = GUI_FormatDate(Row.Item(SQL_COLUMN_DOWNLOAD_LAST_SYNC), _
                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                  ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Pending Changes
    If Not Row.IsNull(SQL_COLUMN_DOWNLOAD_NUM_PENDING) _
      And Not Row.IsNull(SQL_COLUMN_DOWNLOAD_LOCAL_SEQUENCE_ID) Then
      _local_sequence_id = Row.Item(SQL_COLUMN_DOWNLOAD_LOCAL_SEQUENCE_ID)

      If Not Row.IsNull(SQL_COLUMN_DOWNLOAD_NUM_PENDING) Then
        _remote_sequence_id = Row.Item(SQL_COLUMN_DOWNLOAD_REMOTE_SEQUENCE_ID)
      End If

      _pending_changes_true = Row.Item(SQL_COLUMN_DOWNLOAD_NUM_PENDING)

      If _local_sequence_id > 0 Then
        _pending_changes = Row.Item(SQL_COLUMN_DOWNLOAD_NUM_PENDING)
        _pending_changes_str = _pending_changes
      End If
    End If

    If _task_id = TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles _
      Or _task_id = TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers Then

      Call getTimestampSequences(_task_id, _pending_changes_true, _pending_changes, _local_sequence_id, _remote_sequence_id)

    End If

    _progress = GetTaskProgress(RowIndex, _local_sequence_id, _remote_sequence_id)

    With dg_download_tasks
      .Cell(RowIndex, GRID_COLUMN_DOWNLOAD_TASK_ID).Value = _task_id
      .Cell(RowIndex, GRID_COLUMN_DOWNLOAD_TASK_NAME).Value = _task_name
      .Cell(RowIndex, GRID_COLUMN_DOWNLOAD_STATUS).Value = _task_status
      .Cell(RowIndex, GRID_COLUMN_DOWNLOAD_PROGRESS).Value = _progress
      .Cell(RowIndex, GRID_COLUMN_DOWNLOAD_LAST_SYNC).Value = _last_sync
      .Cell(RowIndex, GRID_COLUMN_DOWNLOAD_PENDING_CHANGES).Value = _pending_changes_str
    End With
  End Sub ' UpdateDownloadRow

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub getTimestampSequences(ByVal TaskId As Integer _
                                 , ByVal PendingChangesTrue As Integer _
                                 , ByVal PendingChanges As Integer _
                                 , ByRef LocalSequenceId As Integer _
                                 , ByRef RemoteSequenceId As Integer)

    Select Case TaskId
      Case TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles
        If PendingChangesTrue > m_pending_changes_perfil Then
          m_pending_changes_perfil = PendingChangesTrue
        ElseIf PendingChanges = 0 And PendingChangesTrue = 0 Then
          m_pending_changes_perfil = 0
        End If
        If Not (LocalSequenceId = 0 And RemoteSequenceId > 0) Then
          LocalSequenceId = m_pending_changes_perfil - PendingChangesTrue
          RemoteSequenceId = m_pending_changes_perfil
        End If

      Case TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers
        If PendingChangesTrue > m_pending_changes_user Then
          m_pending_changes_user = PendingChangesTrue
        ElseIf PendingChanges = 0 And PendingChangesTrue = 0 Then
          m_pending_changes_user = 0
        End If
        If Not (LocalSequenceId = 0 And RemoteSequenceId > 0) Then
          LocalSequenceId = m_pending_changes_user - PendingChangesTrue
          RemoteSequenceId = m_pending_changes_user
        End If

    End Select

  End Sub

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub RefreshMonitor()
    Dim _connection_status As Int32

    Try
      _connection_status = ReadCurrentConnectionStatus()
      ReadUploadProcessesStatus(_connection_status)
      ReadDownloadProcessesStatus(_connection_status)

      Call AdjustGridScrollbars()
    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_multisite_monitor", _
                            "RefreshGrid", _
                            ex.Message)

    Finally
      Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
      Me.tf_last_update.Value = GUI_FormatDate(GUI_GetDBDateTime, _
                                               ENUM_FORMAT_DATE.FORMAT_DATE_NONE, _
                                               ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End Try
  End Sub ' RefreshMonitor

  ' PURPOSE: Force the selected download task to totally resync
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Sub ForceReSync()
    Dim _selected_row As Integer
    Dim _task_id As TYPE_MULTISITE_SITE_TASKS
    Dim _resyncable_tasks() As TYPE_MULTISITE_SITE_TASKS
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _resyncable_description_tasks As String
    Dim _str_bld As StringBuilder
    Dim _sql_command As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _num_rows_updated As Integer
    Dim _commit As Boolean
    Dim _ctx As Integer
    Dim _resync_field As String
    Dim _grid As uc_grid

    _sql_transaction = Nothing
    _commit = False
    _resyncable_tasks = Nothing
    _resyncable_description_tasks = vbCrLf

    If Not MultiSiteConfigured() Then
      Return
    End If

    Try
      Array.Resize(_resyncable_tasks, 0)

      If Not Me.dg_upload_tasks.SelectedRows() Is Nothing Then
        For Each _selected_row In Me.dg_upload_tasks.SelectedRows
          _task_id = Me.dg_upload_tasks.Cell(_selected_row, GRID_COLUMN_DOWNLOAD_TASK_ID).Value()

          If Array.IndexOf(ReSyncTasks, _task_id) <> -1 Then

            Array.Resize(_resyncable_tasks, _resyncable_tasks.Length + 1)
            _resyncable_tasks(_resyncable_tasks.Length - 1) = _task_id

            _resyncable_description_tasks &= vbCrLf & " - " & mdl_multisite.TranslateMultiSiteProcessID(_task_id)

          End If
        Next
      End If

      If Not Me.dg_download_tasks.SelectedRows() Is Nothing Then
        For Each _selected_row In Me.dg_download_tasks.SelectedRows
          _task_id = Me.dg_download_tasks.Cell(_selected_row, GRID_COLUMN_DOWNLOAD_TASK_ID).Value()

          If Array.IndexOf(ReSyncTasks, _task_id) <> -1 Then

            Array.Resize(_resyncable_tasks, _resyncable_tasks.Length + 1)
            _resyncable_tasks(_resyncable_tasks.Length - 1) = _task_id

            _resyncable_description_tasks &= vbCrLf & " - " & mdl_multisite.TranslateMultiSiteProcessID(_task_id)

            If _task_id = TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers _
                AndAlso Array.IndexOf(_resyncable_tasks, TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles) = -1 Then

              Array.Resize(_resyncable_tasks, _resyncable_tasks.Length + 1)
              _resyncable_tasks(_resyncable_tasks.Length - 1) = TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles
              _resyncable_description_tasks &= vbCrLf & " - " & mdl_multisite.TranslateMultiSiteProcessID(TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles)

            End If
          End If
        Next
      End If

      If _resyncable_tasks.Length = 0 Then
        '1782 "You should select one of following tasks to force its update:\n%1"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1782), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                        , _
                        mdl_multisite.GetReSyncableTasksList())

        Return
      End If

      '1837 "Are you sure you want to force update of following tasks? %1"
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1837), _
                    ENUM_MB_TYPE.MB_TYPE_INFO, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    , _
                    _resyncable_description_tasks) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        Return
      End If

      For Each _task_id In _resyncable_tasks

        _str_bld = New StringBuilder()

        Select Case _task_id
          Case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site, TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, _
               TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site, TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, _
               TYPE_MULTISITE_SITE_TASKS.DownloadProviders, TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, _
               TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, _
               TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, _
               TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages, TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig

            _resync_field = "ST_LOCAL_SEQUENCE_ID"
            _grid = Me.dg_download_tasks

          Case TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour, TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions, _
               TYPE_MULTISITE_SITE_TASKS.UploadProviders, TYPE_MULTISITE_SITE_TASKS.UploadBanks, _
               TYPE_MULTISITE_SITE_TASKS.UploadAreas, TYPE_MULTISITE_SITE_TASKS.UploadTerminals, _
               TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected, TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions, _
               TYPE_MULTISITE_SITE_TASKS.UploadProvidersGames, TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour, _
               TYPE_MULTISITE_SITE_TASKS.UploadAlarms, TYPE_MULTISITE_SITE_TASKS.UploadHandpays, _
               TYPE_MULTISITE_SITE_TASKS.UploadCreditLines, TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements

            _resync_field = "ST_REMOTE_SEQUENCE_ID"
            _grid = Me.dg_upload_tasks

          Case Else

            Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  "frm_multisite_monitor", _
                                  "ForceReSync", _
                                  "Task " & mdl_multisite.TranslateMultiSiteProcessID(_task_id) & " isn't resyncable")

            Return

        End Select

        _str_bld.AppendLine("UPDATE MS_SITE_TASKS            ")
        _str_bld.AppendLine("   SET " & _resync_field & " = 0")
        _str_bld.AppendLine(" WHERE ST_TASK_ID = @pTaskId    ")

        _sql_command = New SqlCommand(_str_bld.ToString(), _sql_transaction.Connection, _sql_transaction)

        _sql_command.Parameters.Add("@pTaskId", SqlDbType.Int).Value = _task_id

        _num_rows_updated = _sql_command.ExecuteNonQuery()

        If _num_rows_updated <> 1 Then
          Return
        End If

        _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_MULTISITE)

        Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1697), _
                                   GLB_NLS_GUI_PLAYER_TRACKING.GetString(1783)) '1783 "Update started for task %1."

        Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1784), _
                                    mdl_multisite.TranslateMultiSiteProcessID(_task_id))

        If Not _auditor_data.Notify(CurrentUser.GuiId, _
                                    CurrentUser.Id, _
                                    CurrentUser.Name, _
                                    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                    _ctx) Then
          Return
        End If

        _grid.Cell(_selected_row, GRID_COLUMN_DOWNLOAD_PENDING_CHANGES).Value = UNDEFINED_STR
      Next

      '1785 "Update was forced for the following tasks: %1"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1785), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                      , _
                      _resyncable_description_tasks)

      _commit = True

    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_multisite_monitor", _
                            "ForceReSync", _
                            ex.Message)
    Finally
      GUI_EndSQLTransaction(_sql_transaction, _commit)
    End Try

  End Sub 'ForceReSync


  ' PURPOSE: Adjust grid width to fit vertical scrollbar without show horitzontal scroll
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Sub AdjustGridScrollbars()

    Call GUI_AdjustGridScrollbars(dg_upload_tasks, GRID_COLUMN_UPLOAD_STATUS, 1, COLUMN_UPLOAD_STATUS_WIDTH)

    Call GUI_AdjustGridScrollbars(dg_download_tasks, GRID_COLUMN_DOWNLOAD_STATUS, 1, COLUMN_DOWNLOAD_STATUS_WIDTH)

  End Sub
#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Execute proper actions when programmed timer elapses
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick
    Me.RefreshMonitor()
  End Sub

#End Region ' Events

End Class