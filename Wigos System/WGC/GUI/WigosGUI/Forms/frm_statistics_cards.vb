'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statistics_cards
' DESCRIPTION:   This screen allows to view the terminals statistics between:
'                           - two dates 
'                           - card id
'                           - card track data
'                           - card holder name
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 01-JUN-2007
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     -----------------------------------------------
' 01-JUN-2007  MTM        Initial version
' 12-FEB-2013  JMM        uc_account_sel.ValidateFormat added on FilterCheck
' 04-APR-2013  HBB        Check if the account type is not null
' 25-APR-2013  RBG        #746 It makes no sense to work in this form with multiselection
' 25-NOV-2013  RMS & QMP  DEPRECATED
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_statistics_cards
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1242, 118)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 122)
    Me.panel_data.Size = New System.Drawing.Size(1242, 490)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1236, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1236, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(244, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2003, 5, 20, 10, 37, 59, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(258, 5)
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.Size = New System.Drawing.Size(309, 108)
    Me.uc_account_sel1.TabIndex = 8
    Me.uc_account_sel1.TrackData = ""
    '
    'frm_statistics_cards
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1250, 616)
    Me.Name = "frm_statistics_cards"
    Me.Text = "frm_statistics_cards"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 1
  Private Const SQL_COLUMN_CARD_TRACK_DATA As Integer = 2
  Private Const SQL_COLUMN_CARD_PLAYER As Integer = 3
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_AMOUNT_PER_CENT As Integer = 6
  Private Const SQL_COLUMN_NETWIN As Integer = 7
  Private Const SQL_COLUMN_NETWIN_PER_CENT As Integer = 8
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 9
  Private Const SQL_COLUMN_WON_COUNT As Integer = 10
  Private Const SQL_COLUMN_COUNT_PER_CENT As Integer = 11

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE_FROM As Integer = 1
  Private Const GRID_COLUMN_DATE_TO As Integer = 2
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 3
  Private Const GRID_COLUMN_ACCOUNT_TYPE As Integer = 4
  Private Const GRID_COLUMN_CARD_TRACK_DATA As Integer = 5
  Private Const GRID_COLUMN_CARD_PLAYER As Integer = 6
  Private Const GRID_COLUMN_PLAYED_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_WON_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_AMOUNT_PER_CENT As Integer = 9
  Private Const GRID_COLUMN_NETWIN As Integer = 10
  Private Const GRID_COLUMN_NETWIN_PER_CENT As Integer = 11
  Private Const GRID_COLUMN_PLAYED_COUNT As Integer = 12
  Private Const GRID_COLUMN_WON_COUNT As Integer = 13
  Private Const GRID_COLUMN_COUNT_PER_CENT As Integer = 14

  Private Const GRID_COLUMNS As Integer = 15
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_account_id As String
  Private m_card_track_data As String
  Private m_card_holder_name As String
  Private m_card_anonym As Boolean
  Private m_date_from As String
  Private m_date_to As String

  ' For amount grid
  Dim total_played_amount As Double
  Dim total_won_amount As Double
  Dim total_played_count As Double
  Dim total_won_count As Double
  Dim aux_amount As Double

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STATISTICS_CARDS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(363)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Text = GLB_NLS_GUI_STATISTICS.GetString(3)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Enabled = False

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(202)
    Me.dtp_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    Me.dtp_to.Text = GLB_NLS_GUI_STATISTICS.GetString(310)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    Me.uc_account_sel1.ShowAnonFilter()

    ' 5 minutes timeout
    Call GUI_SetQueryTimeout(300)

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    total_played_amount = 0
    total_won_amount = 0
    total_played_count = 0
    total_won_count = 0

  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.SortGrid(GRID_COLUMN_ACCOUNT_ID)

    Me.Grid.AddRow()
    Dim idx_row As Integer = Me.Grid.NumRows - 1
    Dim amount_per_cent As Double
    Dim netwin_per_cent As Double
    Dim netwin As Double = total_played_amount - total_won_amount
    Dim count_per_cent As Double

    ' Card Id
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_FROM).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    ' Card Track Data
    Me.Grid.Cell(idx_row, GRID_COLUMN_CARD_TRACK_DATA).Value = " "

    ' Card Player
    Me.Grid.Cell(idx_row, GRID_COLUMN_CARD_PLAYER).Value = " "

    ' Played Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(total_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Won Amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(total_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Amount Per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    Else
      amount_per_cent = (total_won_amount / total_played_amount) * 100
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(amount_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Netwin
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = ""
    Else
      netwin_per_cent = netwin * 100 / total_played_amount
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(netwin_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Played Count
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(total_played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Won Count
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(total_won_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

    ' Count per cent
    If total_played_count = 0 Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    Else
      count_per_cent = (total_won_count / total_played_count) * 100
      Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(count_per_cent, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Color Row
    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_GRID_INFO
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Int32
    Dim frm_plays As frm_plays_plays
    Dim filter_account_id As String
    Dim filter_date_from As Date
    Dim filter_date_to As Date

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      Return
    End If

    ' Get the complete series ID and launch editor form
    filter_account_id = Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT_ID).Value

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    filter_date_from = Nothing
    filter_date_to = Nothing

    If dtp_from.Checked = True Then
      filter_date_from = dtp_from.Value
    End If

    If dtp_to.Checked = True Then
      filter_date_to = dtp_to.Value
    End If

    frm_plays = New frm_plays_plays
    frm_plays.ShowForEdit(Me.MdiParent, 0, 0, _
                          filter_account_id, _
                          filter_date_from, _
                          filter_date_to)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_ShowSelectedItem

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = "SELECT x.PL_ACCOUNT_ID, " & _
                     "AC_TYPE, " & _
                     "AC_TRACK_DATA, " & _
                     "AC_HOLDER_NAME, " & _
                     "x.PlayedAmount, " & _
                     "x.WonAmount, " & _
                     "CASE x.PlayedAmount WHEN 0 THEN 0 ELSE (x.WonAmount*100/x.PlayedAmount) END as AmountPerCent, " & _
                     "x.PlayedAmount-x.WonAmount as Netwin, " & _
                     "CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((x.PlayedAmount-x.WonAmount)*100/x.PlayedAmount) END as NetwinPc, " & _
                     "x.PlayedCount, " & _
                     "x.WonCount, " & _
                     "CASE x.PlayedCount WHEN 0 THEN 0 ELSE((x.WonCount* 100.0)/x.PlayedCount) END as CountPerCent " & _
               "FROM ACCOUNTS" & _
        " RIGHT OUTER JOIN " & _
                    "(SELECT PL_ACCOUNT_ID" & _
                          ", sum(PL_PLAYED_AMOUNT)as PlayedAmount" & _
                          ", sum(PL_WON_AMOUNT) AS WonAmount" & _
                          ", count(PL_PLAYED_AMOUNT) as PlayedCount" & _
                          ", sum (CASE PL_WON_AMOUNT WHEN 0.00 THEN 0 ELSE 1 END) AS WonCount " & _
                      "FROM PLAYS "

    str_sql = str_sql & GetSqlWhere()

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim track_data As String

    ' Date from
    If m_date_from <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(m_date_from, _
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = ""
    End If

    ' Date to
    If m_date_to <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(m_date_to, _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = ""
    End If

    ' Account Id

    If DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = GLB_NLS_GUI_STATISTICS.GetString(313)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    ' Account type
    If DbRow.IsNull(SQL_COLUMN_ACCOUNT_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE)
    End If

    ' Track data
    If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK_DATA) Then
      ' Screen must always show the encrypted card number
      ' Internal Identifier must be converted prior to showing it
      track_data = ""
      ' WIN accounts must follow encryption formats
      If Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = ACCOUNT_TYPE_WIN Then
        If WSI.Common.CardNumber.TrackDataToExternal(track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK_DATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK_DATA).Value = track_data
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK_DATA).Value = "* * * * * * * *"
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK_DATA).Value = DbRow.Value(SQL_COLUMN_CARD_TRACK_DATA)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK_DATA).Value = ""
    End If

    ' Player
    If Not DbRow.IsNull(SQL_COLUMN_CARD_PLAYER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_PLAYER).Value = DbRow.Value(SQL_COLUMN_CARD_PLAYER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_PLAYER).Value = ""
    End If

    ' PlayedAmount
    aux_amount = IIf(DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT), 0, DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(aux_amount)
    total_played_amount += aux_amount

    ' WonAmount
    aux_amount = IIf(DbRow.IsNull(SQL_COLUMN_WON_AMOUNT), 0, DbRow.Value(SQL_COLUMN_WON_AMOUNT))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(aux_amount)
    total_won_amount += aux_amount

    ' AmountPerCent
    If DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    ElseIf DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = "0.00" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AMOUNT_PER_CENT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Netwin
    aux_amount = IIf(DbRow.IsNull(SQL_COLUMN_NETWIN), 0, DbRow.Value(SQL_COLUMN_NETWIN))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(aux_amount)

    ' Netwin PerCent
    If DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = ""
    ElseIf DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = "0.00" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NETWIN_PER_CENT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' PlayedCount
    aux_amount = IIf(DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT), 0, DbRow.Value(SQL_COLUMN_PLAYED_COUNT))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(aux_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    total_played_count += aux_amount

    ' WonCount
    aux_amount = IIf(DbRow.IsNull(SQL_COLUMN_WON_COUNT), 0, DbRow.Value(SQL_COLUMN_WON_COUNT))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(aux_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    total_won_count += aux_amount

    ' CountPerCent
    If DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    ElseIf DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = "0.00" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COUNT_PER_CENT), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    If m_card_anonym Then
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(207), GLB_NLS_GUI_STATISTICS.GetString(313))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account_id)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_card_track_data)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_card_holder_name)
    End If

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(363)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_account_id = ""
    m_card_track_data = ""
    m_card_holder_name = ""
    m_date_from = ""
    m_date_to = ""

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    m_account_id = Me.uc_account_sel1.Account()
    m_card_track_data = Me.uc_account_sel1.TrackData()
    m_card_holder_name = Me.uc_account_sel1.Holder()
    m_card_anonym = Me.uc_account_sel1.Anon

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date from
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(309)
      .Column(GRID_COLUMN_DATE_FROM).Width = 1700
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date to
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(310)
      .Column(GRID_COLUMN_DATE_TO).Width = 1700
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Card Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(207)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(208)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1150
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account type
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(207)
      ' RCI 30-JUN-2010: If Width = 0, Header(1).Text must be "", else a strange char is showed in the column.
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(1).Text = "" ' GLB_NLS_GUI_INVOICING.GetString(208)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Width = 0

      ' Track Data Card
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(207)
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(209)
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Width = 2400
      .Column(GRID_COLUMN_CARD_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Card Holder Name
      ' Removed to free up some space
      .Column(GRID_COLUMN_CARD_PLAYER).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(207)
      .Column(GRID_COLUMN_CARD_PLAYER).Header(1).Text = ""  'GLB_NLS_GUI_STATISTICS.GetString(210)
      .Column(GRID_COLUMN_CARD_PLAYER).Width = 0 '1400
      .Column(GRID_COLUMN_CARD_PLAYER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(301)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(302)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount Per Cent
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Width = 1150
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NETWIN).Width = 1250
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin Per Cent
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(312)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Width = 1150
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = 950
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = 950
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count Per Cent
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(306)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Width = 1100
      .Column(GRID_COLUMN_COUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    Me.dtp_to.Value = _final_time
    Me.dtp_to.Checked = True

    Me.dtp_from.Value = _final_time.AddDays(-1)
    Me.dtp_from.Checked = True

    Me.uc_account_sel1.Clear()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_where_cards As String = ""
    Dim str_aux1 As String = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (PL_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (PL_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    If str_where <> "" Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    str_where = str_where + " GROUP BY PL_ACCOUNT_ID) x ON AC_ACCOUNT_ID = x.PL_ACCOUNT_ID"

    str_where_cards = Me.uc_account_sel1.GetFilterSQL()

    If str_where_cards <> "" Then
      str_where_cards = " WHERE " & str_where_cards
    End If

    str_where = str_where + str_where_cards

    Return str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    If IdxRow < 0 Or IdxRow >= (Me.Grid.NumRows - 1) Then
      Return False
    Else
      Return True
    End If

  End Function ' IsValidDataRow

#End Region ' Private Functions

End Class
