<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_games_admin_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_category_status = New System.Windows.Forms.GroupBox()
    Me.chk_status_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_status_enabled = New System.Windows.Forms.CheckBox()
    Me.lbl_title = New System.Windows.Forms.Label()
    Me.txt_name = New System.Windows.Forms.TextBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_checked_categories = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_category_status.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_checked_categories)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.txt_name)
    Me.panel_filter.Controls.Add(Me.lbl_title)
    Me.panel_filter.Controls.Add(Me.gb_category_status)
    Me.panel_filter.Location = New System.Drawing.Point(3, 3)
    Me.panel_filter.Margin = New System.Windows.Forms.Padding(2)
    Me.panel_filter.Padding = New System.Windows.Forms.Padding(2, 2, 0, 0)
    Me.panel_filter.Size = New System.Drawing.Size(949, 163)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_category_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_title, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.txt_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_categories, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(3, 166)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(2)
    Me.panel_data.Padding = New System.Windows.Forms.Padding(2)
    Me.panel_data.Size = New System.Drawing.Size(949, 397)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(2, 2)
    Me.pn_separator_line.Margin = New System.Windows.Forms.Padding(2)
    Me.pn_separator_line.Padding = New System.Windows.Forms.Padding(0, 5, 0, 5)
    Me.pn_separator_line.Size = New System.Drawing.Size(945, 20)
    '
    'pn_line
    '
    Me.pn_line.Location = New System.Drawing.Point(0, 5)
    Me.pn_line.Margin = New System.Windows.Forms.Padding(2)
    Me.pn_line.Size = New System.Drawing.Size(945, 4)
    '
    'gb_category_status
    '
    Me.gb_category_status.Controls.Add(Me.chk_status_disabled)
    Me.gb_category_status.Controls.Add(Me.chk_status_enabled)
    Me.gb_category_status.Location = New System.Drawing.Point(583, 1)
    Me.gb_category_status.Margin = New System.Windows.Forms.Padding(2)
    Me.gb_category_status.Name = "gb_category_status"
    Me.gb_category_status.Padding = New System.Windows.Forms.Padding(2)
    Me.gb_category_status.Size = New System.Drawing.Size(86, 85)
    Me.gb_category_status.TabIndex = 30
    Me.gb_category_status.TabStop = False
    Me.gb_category_status.Text = "xStatus"
    '
    'chk_status_disabled
    '
    Me.chk_status_disabled.AutoSize = True
    Me.chk_status_disabled.Location = New System.Drawing.Point(10, 49)
    Me.chk_status_disabled.Margin = New System.Windows.Forms.Padding(2)
    Me.chk_status_disabled.Name = "chk_status_disabled"
    Me.chk_status_disabled.Size = New System.Drawing.Size(65, 17)
    Me.chk_status_disabled.TabIndex = 2
    Me.chk_status_disabled.Text = "xDisab"
    Me.chk_status_disabled.UseVisualStyleBackColor = True
    '
    'chk_status_enabled
    '
    Me.chk_status_enabled.AutoSize = True
    Me.chk_status_enabled.Location = New System.Drawing.Point(10, 23)
    Me.chk_status_enabled.Margin = New System.Windows.Forms.Padding(2)
    Me.chk_status_enabled.Name = "chk_status_enabled"
    Me.chk_status_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_status_enabled.TabIndex = 1
    Me.chk_status_enabled.Text = "xEnabled"
    Me.chk_status_enabled.UseVisualStyleBackColor = True
    '
    'lbl_title
    '
    Me.lbl_title.AutoSize = True
    Me.lbl_title.Location = New System.Drawing.Point(8, 98)
    Me.lbl_title.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lbl_title.Name = "lbl_title"
    Me.lbl_title.Size = New System.Drawing.Size(38, 13)
    Me.lbl_title.TabIndex = 35
    Me.lbl_title.Text = "xTitle"
    '
    'txt_name
    '
    Me.txt_name.Location = New System.Drawing.Point(62, 92)
    Me.txt_name.Margin = New System.Windows.Forms.Padding(2)
    Me.txt_name.Multiline = True
    Me.txt_name.Name = "txt_name"
    Me.txt_name.Size = New System.Drawing.Size(198, 24)
    Me.txt_name.TabIndex = 36
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 1)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(254, 86)
    Me.gb_date.TabIndex = 37
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 49)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(242, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 21)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(242, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_checked_categories
    '
    Me.uc_checked_categories.GroupBoxText = "xCheckedList"
    Me.uc_checked_categories.Location = New System.Drawing.Point(268, 1)
    Me.uc_checked_categories.m_resize_width = 308
    Me.uc_checked_categories.multiChoice = True
    Me.uc_checked_categories.Name = "uc_checked_categories"
    Me.uc_checked_categories.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_categories.SelectedIndexesList = ""
    Me.uc_checked_categories.SelectedIndexesListLevel2 = ""
    Me.uc_checked_categories.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_categories.SelectedValuesList = ""
    Me.uc_checked_categories.SetLevels = 2
    Me.uc_checked_categories.Size = New System.Drawing.Size(308, 159)
    Me.uc_checked_categories.TabIndex = 38
    Me.uc_checked_categories.ValuesArray = New String(-1) {}
    '
    'frm_games_admin_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(955, 566)
    Me.Margin = New System.Windows.Forms.Padding(2)
    Me.Name = "frm_games_admin_sel"
    Me.Padding = New System.Windows.Forms.Padding(3)
    Me.Text = "frm_games_admin_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_category_status.ResumeLayout(False)
    Me.gb_category_status.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_category_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_status_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents txt_name As System.Windows.Forms.TextBox
  Friend WithEvents lbl_title As System.Windows.Forms.Label
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_checked_categories As GUI_Controls.uc_checked_list
End Class
