<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_promotion_auto_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_promotion_auto_edit))
    Me.gb_credit_type = New System.Windows.Forms.GroupBox()
    Me.opt_point = New System.Windows.Forms.RadioButton()
    Me.opt_credit_redeemable = New System.Windows.Forms.RadioButton()
    Me.opt_credit_non_redeemable = New System.Windows.Forms.RadioButton()
    Me.ef_promo_name = New GUI_Controls.uc_entry_field()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.opt_disabled = New System.Windows.Forms.RadioButton()
    Me.opt_enabled = New System.Windows.Forms.RadioButton()
    Me.gb_auto_promotion_type = New System.Windows.Forms.GroupBox()
    Me.opt_birthday = New System.Windows.Forms.RadioButton()
    Me.opt_min_played = New System.Windows.Forms.RadioButton()
    Me.opt_per_level = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_level_anonymous = New System.Windows.Forms.CheckBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.chk_only_vip = New System.Windows.Forms.CheckBox()
    Me.lbl_by_level = New GUI_Controls.uc_text_field()
    Me.ef_expiration_days = New GUI_Controls.uc_entry_field()
    Me.uc_provider_filter = New GUI_Controls.uc_provider_filter()
    Me.gb_applies = New System.Windows.Forms.GroupBox()
    Me.lbl_notes = New System.Windows.Forms.Label()
    Me.opt_pct = New System.Windows.Forms.RadioButton()
    Me.opt_multiplier = New System.Windows.Forms.RadioButton()
    Me.ef_percentage_reward = New GUI_Controls.uc_entry_field()
    Me.lbl_birthday = New GUI_Controls.uc_text_field()
    Me.ef_min_played_reward = New GUI_Controls.uc_entry_field()
    Me.ef_played_reward = New GUI_Controls.uc_entry_field()
    Me.ef_months_day = New GUI_Controls.uc_entry_field()
    Me.ef_min_played = New GUI_Controls.uc_entry_field()
    Me.ef_played = New GUI_Controls.uc_entry_field()
    Me.lbl_played_promotion = New GUI_Controls.uc_text_field()
    Me.lbl_min_played_promotion = New GUI_Controls.uc_text_field()
    Me.Timer = New System.Windows.Forms.Timer(Me.components)
    Me.gb_ticket_footer_automatic_promotion = New System.Windows.Forms.GroupBox()
    Me.tb_ticket_footer_automatic_promotion = New System.Windows.Forms.TextBox()
    Me.cmb_categories = New GUI_Controls.uc_combo()
    Me.cmb_award_with_game = New System.Windows.Forms.ComboBox()
    Me.chk_award_with_game = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_credit_type.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_auto_promotion_type.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_applies.SuspendLayout()
    Me.gb_ticket_footer_automatic_promotion.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_award_with_game)
    Me.panel_data.Controls.Add(Me.chk_award_with_game)
    Me.panel_data.Controls.Add(Me.cmb_categories)
    Me.panel_data.Controls.Add(Me.gb_ticket_footer_automatic_promotion)
    Me.panel_data.Controls.Add(Me.uc_provider_filter)
    Me.panel_data.Controls.Add(Me.gb_applies)
    Me.panel_data.Controls.Add(Me.gb_level)
    Me.panel_data.Controls.Add(Me.dtp_to)
    Me.panel_data.Controls.Add(Me.dtp_from)
    Me.panel_data.Controls.Add(Me.gb_auto_promotion_type)
    Me.panel_data.Controls.Add(Me.gb_credit_type)
    Me.panel_data.Controls.Add(Me.ef_promo_name)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(750, 576)
    '
    'gb_credit_type
    '
    Me.gb_credit_type.Controls.Add(Me.opt_point)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_redeemable)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_non_redeemable)
    Me.gb_credit_type.Location = New System.Drawing.Point(103, 58)
    Me.gb_credit_type.Name = "gb_credit_type"
    Me.gb_credit_type.Size = New System.Drawing.Size(178, 93)
    Me.gb_credit_type.TabIndex = 3
    Me.gb_credit_type.TabStop = False
    Me.gb_credit_type.Text = "xCreditType"
    '
    'opt_point
    '
    Me.opt_point.Location = New System.Drawing.Point(18, 68)
    Me.opt_point.Name = "opt_point"
    Me.opt_point.Size = New System.Drawing.Size(135, 16)
    Me.opt_point.TabIndex = 4
    Me.opt_point.Text = "xPoint"
    '
    'opt_credit_redeemable
    '
    Me.opt_credit_redeemable.Location = New System.Drawing.Point(18, 45)
    Me.opt_credit_redeemable.Name = "opt_credit_redeemable"
    Me.opt_credit_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_redeemable.TabIndex = 1
    Me.opt_credit_redeemable.Text = "xRedeemable"
    '
    'opt_credit_non_redeemable
    '
    Me.opt_credit_non_redeemable.Location = New System.Drawing.Point(18, 22)
    Me.opt_credit_non_redeemable.Name = "opt_credit_non_redeemable"
    Me.opt_credit_non_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_non_redeemable.TabIndex = 0
    Me.opt_credit_non_redeemable.Text = "xNon-Redeemable"
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0.0R
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(14, 3)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.PlaceHolder = Nothing
    Me.ef_promo_name.Size = New System.Drawing.Size(266, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 0
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 87
    Me.ef_promo_name.Value = ""
    Me.ef_promo_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(3, 58)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(94, 94)
    Me.gb_enabled.TabIndex = 2
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(19, 57)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(19, 31)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'gb_auto_promotion_type
    '
    Me.gb_auto_promotion_type.Controls.Add(Me.opt_birthday)
    Me.gb_auto_promotion_type.Controls.Add(Me.opt_min_played)
    Me.gb_auto_promotion_type.Controls.Add(Me.opt_per_level)
    Me.gb_auto_promotion_type.Location = New System.Drawing.Point(3, 158)
    Me.gb_auto_promotion_type.Name = "gb_auto_promotion_type"
    Me.gb_auto_promotion_type.Size = New System.Drawing.Size(278, 88)
    Me.gb_auto_promotion_type.TabIndex = 4
    Me.gb_auto_promotion_type.TabStop = False
    Me.gb_auto_promotion_type.Text = "xTypeAutomaticPromotion"
    '
    'opt_birthday
    '
    Me.opt_birthday.Location = New System.Drawing.Point(20, 66)
    Me.opt_birthday.Name = "opt_birthday"
    Me.opt_birthday.Size = New System.Drawing.Size(203, 16)
    Me.opt_birthday.TabIndex = 2
    Me.opt_birthday.Text = "xBirthday"
    '
    'opt_min_played
    '
    Me.opt_min_played.Location = New System.Drawing.Point(20, 42)
    Me.opt_min_played.Name = "opt_min_played"
    Me.opt_min_played.Size = New System.Drawing.Size(203, 16)
    Me.opt_min_played.TabIndex = 1
    Me.opt_min_played.Text = "xMinPlayed"
    '
    'opt_per_level
    '
    Me.opt_per_level.Location = New System.Drawing.Point(20, 18)
    Me.opt_per_level.Name = "opt_per_level"
    Me.opt_per_level.Size = New System.Drawing.Size(203, 16)
    Me.opt_per_level.TabIndex = 0
    Me.opt_per_level.Text = "xPerLevel"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(532, 28)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(157, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 10
    Me.dtp_to.TextWidth = 20
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(313, 28)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(220, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 9
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_level_anonymous
    '
    Me.chk_level_anonymous.AutoSize = True
    Me.chk_level_anonymous.Location = New System.Drawing.Point(48, 45)
    Me.chk_level_anonymous.Name = "chk_level_anonymous"
    Me.chk_level_anonymous.Size = New System.Drawing.Size(100, 17)
    Me.chk_level_anonymous.TabIndex = 1
    Me.chk_level_anonymous.Text = "xAnonymous"
    Me.chk_level_anonymous.UseVisualStyleBackColor = True
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(48, 141)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 5
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(48, 117)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 4
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(48, 93)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 3
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(48, 69)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 2
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_only_vip)
    Me.gb_level.Controls.Add(Me.lbl_by_level)
    Me.gb_level.Controls.Add(Me.chk_level_anonymous)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Location = New System.Drawing.Point(3, 252)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(278, 193)
    Me.gb_level.TabIndex = 5
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(11, 170)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(83, 17)
    Me.chk_only_vip.TabIndex = 11
    Me.chk_only_vip.Text = "xOnlyVips"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'lbl_by_level
    '
    Me.lbl_by_level.IsReadOnly = True
    Me.lbl_by_level.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_by_level.LabelForeColor = System.Drawing.Color.Black
    Me.lbl_by_level.Location = New System.Drawing.Point(6, 20)
    Me.lbl_by_level.Name = "lbl_by_level"
    Me.lbl_by_level.Size = New System.Drawing.Size(266, 24)
    Me.lbl_by_level.SufixText = "Sufix Text"
    Me.lbl_by_level.SufixTextVisible = True
    Me.lbl_by_level.TabIndex = 6
    Me.lbl_by_level.TextWidth = 0
    Me.lbl_by_level.Value = "xAppliesOnBirthdayOnly"
    '
    'ef_expiration_days
    '
    Me.ef_expiration_days.DoubleValue = 0.0R
    Me.ef_expiration_days.IntegerValue = 0
    Me.ef_expiration_days.IsReadOnly = False
    Me.ef_expiration_days.Location = New System.Drawing.Point(225, 276)
    Me.ef_expiration_days.Name = "ef_expiration_days"
    Me.ef_expiration_days.PlaceHolder = Nothing
    Me.ef_expiration_days.Size = New System.Drawing.Size(233, 24)
    Me.ef_expiration_days.SufixText = "xday(s)"
    Me.ef_expiration_days.SufixTextVisible = True
    Me.ef_expiration_days.SufixTextWidth = 80
    Me.ef_expiration_days.TabIndex = 11
    Me.ef_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_days.TextValue = ""
    Me.ef_expiration_days.TextWidth = 110
    Me.ef_expiration_days.Value = ""
    Me.ef_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_provider_filter
    '
    Me.uc_provider_filter.AllowCollapse = True
    Me.uc_provider_filter.Collapsed = True
    Me.uc_provider_filter.Location = New System.Drawing.Point(458, 70)
    Me.uc_provider_filter.Name = "uc_provider_filter"
    Me.uc_provider_filter.Size = New System.Drawing.Size(283, 48)
    Me.uc_provider_filter.TabIndex = 12
    '
    'gb_applies
    '
    Me.gb_applies.Controls.Add(Me.lbl_notes)
    Me.gb_applies.Controls.Add(Me.opt_pct)
    Me.gb_applies.Controls.Add(Me.opt_multiplier)
    Me.gb_applies.Controls.Add(Me.ef_percentage_reward)
    Me.gb_applies.Controls.Add(Me.ef_expiration_days)
    Me.gb_applies.Controls.Add(Me.lbl_birthday)
    Me.gb_applies.Controls.Add(Me.ef_min_played_reward)
    Me.gb_applies.Controls.Add(Me.ef_played_reward)
    Me.gb_applies.Controls.Add(Me.ef_months_day)
    Me.gb_applies.Controls.Add(Me.ef_min_played)
    Me.gb_applies.Controls.Add(Me.ef_played)
    Me.gb_applies.Controls.Add(Me.lbl_played_promotion)
    Me.gb_applies.Controls.Add(Me.lbl_min_played_promotion)
    Me.gb_applies.Location = New System.Drawing.Point(287, 58)
    Me.gb_applies.Name = "gb_applies"
    Me.gb_applies.Size = New System.Drawing.Size(462, 387)
    Me.gb_applies.TabIndex = 11
    Me.gb_applies.TabStop = False
    Me.gb_applies.Text = "xApplies"
    '
    'lbl_notes
    '
    Me.lbl_notes.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_notes.ForeColor = System.Drawing.Color.Navy
    Me.lbl_notes.Location = New System.Drawing.Point(12, 313)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(441, 46)
    Me.lbl_notes.TabIndex = 12
    Me.lbl_notes.Text = "xNOTE"
    '
    'opt_pct
    '
    Me.opt_pct.Location = New System.Drawing.Point(17, 216)
    Me.opt_pct.Name = "opt_pct"
    Me.opt_pct.Size = New System.Drawing.Size(129, 16)
    Me.opt_pct.TabIndex = 8
    Me.opt_pct.Text = "xOptPctj"
    Me.opt_pct.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'opt_multiplier
    '
    Me.opt_multiplier.Location = New System.Drawing.Point(17, 185)
    Me.opt_multiplier.Name = "opt_multiplier"
    Me.opt_multiplier.Size = New System.Drawing.Size(129, 16)
    Me.opt_multiplier.TabIndex = 5
    Me.opt_multiplier.Text = "xOptMultiplier"
    Me.opt_multiplier.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_percentage_reward
    '
    Me.ef_percentage_reward.DoubleValue = 0.0R
    Me.ef_percentage_reward.IntegerValue = 0
    Me.ef_percentage_reward.IsReadOnly = False
    Me.ef_percentage_reward.Location = New System.Drawing.Point(36, 211)
    Me.ef_percentage_reward.Name = "ef_percentage_reward"
    Me.ef_percentage_reward.PlaceHolder = Nothing
    Me.ef_percentage_reward.Size = New System.Drawing.Size(289, 24)
    Me.ef_percentage_reward.SufixText = "%"
    Me.ef_percentage_reward.SufixTextVisible = True
    Me.ef_percentage_reward.SufixTextWidth = 130
    Me.ef_percentage_reward.TabIndex = 9
    Me.ef_percentage_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_percentage_reward.TextValue = ""
    Me.ef_percentage_reward.TextWidth = 110
    Me.ef_percentage_reward.Value = ""
    Me.ef_percentage_reward.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_birthday
    '
    Me.lbl_birthday.IsReadOnly = True
    Me.lbl_birthday.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_birthday.LabelForeColor = System.Drawing.Color.Black
    Me.lbl_birthday.Location = New System.Drawing.Point(24, 69)
    Me.lbl_birthday.Name = "lbl_birthday"
    Me.lbl_birthday.Size = New System.Drawing.Size(421, 24)
    Me.lbl_birthday.SufixText = "Sufix Text"
    Me.lbl_birthday.SufixTextVisible = True
    Me.lbl_birthday.TabIndex = 1
    Me.lbl_birthday.TextWidth = 0
    Me.lbl_birthday.Value = "xAppliesOnBirthdayOnly"
    '
    'ef_min_played_reward
    '
    Me.ef_min_played_reward.DoubleValue = 0.0R
    Me.ef_min_played_reward.IntegerValue = 0
    Me.ef_min_played_reward.IsReadOnly = False
    Me.ef_min_played_reward.Location = New System.Drawing.Point(225, 103)
    Me.ef_min_played_reward.Name = "ef_min_played_reward"
    Me.ef_min_played_reward.PlaceHolder = Nothing
    Me.ef_min_played_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_played_reward.SufixText = "Sufix Text"
    Me.ef_min_played_reward.SufixTextVisible = True
    Me.ef_min_played_reward.TabIndex = 3
    Me.ef_min_played_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_played_reward.TextValue = ""
    Me.ef_min_played_reward.TextWidth = 110
    Me.ef_min_played_reward.Value = ""
    Me.ef_min_played_reward.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_played_reward
    '
    Me.ef_played_reward.DoubleValue = 0.0R
    Me.ef_played_reward.IntegerValue = 0
    Me.ef_played_reward.IsReadOnly = False
    Me.ef_played_reward.Location = New System.Drawing.Point(225, 181)
    Me.ef_played_reward.Name = "ef_played_reward"
    Me.ef_played_reward.PlaceHolder = Nothing
    Me.ef_played_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_played_reward.SufixText = "Sufix Text"
    Me.ef_played_reward.SufixTextVisible = True
    Me.ef_played_reward.TabIndex = 7
    Me.ef_played_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_played_reward.TextValue = ""
    Me.ef_played_reward.TextWidth = 110
    Me.ef_played_reward.Value = ""
    Me.ef_played_reward.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_months_day
    '
    Me.ef_months_day.DoubleValue = 0.0R
    Me.ef_months_day.IntegerValue = 0
    Me.ef_months_day.IsReadOnly = False
    Me.ef_months_day.Location = New System.Drawing.Point(6, 69)
    Me.ef_months_day.Name = "ef_months_day"
    Me.ef_months_day.OnlyUpperCase = True
    Me.ef_months_day.PlaceHolder = Nothing
    Me.ef_months_day.Size = New System.Drawing.Size(253, 24)
    Me.ef_months_day.SufixText = "xOfMonth"
    Me.ef_months_day.SufixTextVisible = True
    Me.ef_months_day.SufixTextWidth = 100
    Me.ef_months_day.TabIndex = 0
    Me.ef_months_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_months_day.TextValue = ""
    Me.ef_months_day.TextWidth = 110
    Me.ef_months_day.Value = ""
    Me.ef_months_day.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_min_played
    '
    Me.ef_min_played.DoubleValue = 0.0R
    Me.ef_min_played.IntegerValue = 0
    Me.ef_min_played.IsReadOnly = False
    Me.ef_min_played.Location = New System.Drawing.Point(6, 103)
    Me.ef_min_played.Name = "ef_min_played"
    Me.ef_min_played.PlaceHolder = Nothing
    Me.ef_min_played.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_played.SufixText = "Sufix Text"
    Me.ef_min_played.SufixTextVisible = True
    Me.ef_min_played.TabIndex = 2
    Me.ef_min_played.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_played.TextValue = ""
    Me.ef_min_played.TextWidth = 110
    Me.ef_min_played.Value = ""
    Me.ef_min_played.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_played
    '
    Me.ef_played.DoubleValue = 0.0R
    Me.ef_played.IntegerValue = 0
    Me.ef_played.IsReadOnly = False
    Me.ef_played.Location = New System.Drawing.Point(36, 180)
    Me.ef_played.Name = "ef_played"
    Me.ef_played.PlaceHolder = Nothing
    Me.ef_played.Size = New System.Drawing.Size(187, 24)
    Me.ef_played.SufixText = "Sufix Text"
    Me.ef_played.SufixTextVisible = True
    Me.ef_played.TabIndex = 6
    Me.ef_played.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_played.TextValue = ""
    Me.ef_played.TextWidth = 110
    Me.ef_played.Value = ""
    Me.ef_played.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_played_promotion
    '
    Me.lbl_played_promotion.AutoSize = True
    Me.lbl_played_promotion.IsReadOnly = True
    Me.lbl_played_promotion.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_played_promotion.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_played_promotion.Location = New System.Drawing.Point(24, 239)
    Me.lbl_played_promotion.Name = "lbl_played_promotion"
    Me.lbl_played_promotion.Size = New System.Drawing.Size(428, 24)
    Me.lbl_played_promotion.SufixText = "Sufix Text"
    Me.lbl_played_promotion.SufixTextVisible = True
    Me.lbl_played_promotion.TabIndex = 10
    Me.lbl_played_promotion.TabStop = False
    Me.lbl_played_promotion.TextVisible = False
    Me.lbl_played_promotion.TextWidth = 0
    Me.lbl_played_promotion.Value = "xUser gets [] non-redeemable for every [] spent"
    '
    'lbl_min_played_promotion
    '
    Me.lbl_min_played_promotion.AutoSize = True
    Me.lbl_min_played_promotion.IsReadOnly = True
    Me.lbl_min_played_promotion.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_min_played_promotion.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_min_played_promotion.Location = New System.Drawing.Point(24, 132)
    Me.lbl_min_played_promotion.Name = "lbl_min_played_promotion"
    Me.lbl_min_played_promotion.Size = New System.Drawing.Size(427, 24)
    Me.lbl_min_played_promotion.SufixText = "Sufix Text"
    Me.lbl_min_played_promotion.SufixTextVisible = True
    Me.lbl_min_played_promotion.TabIndex = 4
    Me.lbl_min_played_promotion.TabStop = False
    Me.lbl_min_played_promotion.TextVisible = False
    Me.lbl_min_played_promotion.TextWidth = 0
    Me.lbl_min_played_promotion.Value = "xUser gets [] non-redeemable for the minimum spent of []"
    '
    'Timer
    '
    '
    'gb_ticket_footer_automatic_promotion
    '
    Me.gb_ticket_footer_automatic_promotion.Controls.Add(Me.tb_ticket_footer_automatic_promotion)
    Me.gb_ticket_footer_automatic_promotion.Location = New System.Drawing.Point(3, 459)
    Me.gb_ticket_footer_automatic_promotion.Name = "gb_ticket_footer_automatic_promotion"
    Me.gb_ticket_footer_automatic_promotion.Size = New System.Drawing.Size(367, 110)
    Me.gb_ticket_footer_automatic_promotion.TabIndex = 6
    Me.gb_ticket_footer_automatic_promotion.TabStop = False
    Me.gb_ticket_footer_automatic_promotion.Text = "xTicket Footer"
    '
    'tb_ticket_footer_automatic_promotion
    '
    Me.tb_ticket_footer_automatic_promotion.Location = New System.Drawing.Point(14, 19)
    Me.tb_ticket_footer_automatic_promotion.MaxLength = 250
    Me.tb_ticket_footer_automatic_promotion.Multiline = True
    Me.tb_ticket_footer_automatic_promotion.Name = "tb_ticket_footer_automatic_promotion"
    Me.tb_ticket_footer_automatic_promotion.Size = New System.Drawing.Size(342, 85)
    Me.tb_ticket_footer_automatic_promotion.TabIndex = 1
    '
    'cmb_categories
    '
    Me.cmb_categories.AllowUnlistedValues = False
    Me.cmb_categories.AutoCompleteMode = False
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(14, 28)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(268, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 1
    Me.cmb_categories.TextCombo = Nothing
    Me.cmb_categories.TextWidth = 87
    '
    'cmb_award_with_game
    '
    Me.cmb_award_with_game.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_award_with_game.FormattingEnabled = True
    Me.cmb_award_with_game.Location = New System.Drawing.Point(464, 4)
    Me.cmb_award_with_game.Name = "cmb_award_with_game"
    Me.cmb_award_with_game.Size = New System.Drawing.Size(223, 21)
    Me.cmb_award_with_game.TabIndex = 8
    '
    'chk_award_with_game
    '
    Me.chk_award_with_game.AutoSize = True
    Me.chk_award_with_game.Location = New System.Drawing.Point(332, 6)
    Me.chk_award_with_game.Name = "chk_award_with_game"
    Me.chk_award_with_game.Size = New System.Drawing.Size(128, 17)
    Me.chk_award_with_game.TabIndex = 7
    Me.chk_award_with_game.Text = "xAwardWithGame"
    Me.chk_award_with_game.UseVisualStyleBackColor = True
    '
    'frm_promotion_auto_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(854, 577)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_promotion_auto_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_promotion_auto_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_credit_type.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_auto_promotion_type.ResumeLayout(False)
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_applies.ResumeLayout(False)
    Me.gb_applies.PerformLayout()
    Me.gb_ticket_footer_automatic_promotion.ResumeLayout(False)
    Me.gb_ticket_footer_automatic_promotion.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_credit_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_non_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents gb_auto_promotion_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_birthday As System.Windows.Forms.RadioButton
  Friend WithEvents opt_min_played As System.Windows.Forms.RadioButton
  Friend WithEvents opt_per_level As System.Windows.Forms.RadioButton
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents uc_provider_filter As GUI_Controls.uc_provider_filter
  Friend WithEvents gb_applies As System.Windows.Forms.GroupBox
  Friend WithEvents ef_months_day As GUI_Controls.uc_entry_field
  Friend WithEvents Timer As System.Windows.Forms.Timer
  Friend WithEvents ef_min_played_reward As GUI_Controls.uc_entry_field
  Friend WithEvents ef_played_reward As GUI_Controls.uc_entry_field
  Friend WithEvents ef_min_played As GUI_Controls.uc_entry_field
  Friend WithEvents ef_played As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_played_promotion As GUI_Controls.uc_text_field
  Friend WithEvents lbl_min_played_promotion As GUI_Controls.uc_text_field
  Friend WithEvents lbl_birthday As GUI_Controls.uc_text_field
  Friend WithEvents ef_percentage_reward As GUI_Controls.uc_entry_field
  Friend WithEvents opt_pct As System.Windows.Forms.RadioButton
  Friend WithEvents opt_multiplier As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_notes As System.Windows.Forms.Label
  Friend WithEvents lbl_by_level As GUI_Controls.uc_text_field
  Friend WithEvents opt_point As System.Windows.Forms.RadioButton
  Friend WithEvents gb_ticket_footer_automatic_promotion As System.Windows.Forms.GroupBox
  Friend WithEvents tb_ticket_footer_automatic_promotion As System.Windows.Forms.TextBox
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_award_with_game As System.Windows.Forms.ComboBox
  Friend WithEvents chk_award_with_game As System.Windows.Forms.CheckBox
End Class
