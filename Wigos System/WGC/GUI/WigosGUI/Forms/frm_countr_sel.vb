﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_countr_sel.vb
' DESCRIPTION:   List CountR Kiosks
' AUTHOR:        Yineth Nuñez
' CREATION DATE: 24-MAR-2016
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 24-MAR-2016   YNM     Initial version
' 31-MAR-2016   DDS     Task 11054:CountR - Cash It Out: GUI - CountR terminal selection
' 11-JUL-2016   JBP     Bug 15315:CountR: no se puede cerrar una sesión de caja de countr teniendo el kiosco deshabilitado
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text

Public Class frm_countr_sel
  Inherits frm_base_sel

#Region " Constants "

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 22

  ' DB Columns
  Private Const SQL_COLUMN_COUNTR_ID As Integer = 0
  Private Const SQL_COLUMN_CODE As Integer = 1
  Private Const SQL_COLUMN_NAME As Integer = 2
  Private Const SQL_COLUMN_DESCRIPTION As Integer = 3
  Private Const SQL_COLUMN_LASTCONNECTION As Integer = 4
  Private Const SQL_COLUMN_STATUS As Integer = 5
  Private Const SQL_COLUMN_IP_ADDRESS As Integer = 6
  Private Const SQL_COLUMN_PROVIDER As Integer = 7
  Private Const SQL_COLUMN_ENABLED As Integer = 8
  Private Const SQL_COLUMN_CREATED As Integer = 9
  Private Const SQL_COLUMN_LASTMODIFIED As Integer = 10
  Private Const SQL_COLUMN_AREAID As Integer = 11
  Private Const SQL_COLUMN_AREANAME As Integer = 12
  Private Const SQL_COLUMN_BANKID As Integer = 13
  Private Const SQL_COLUMN_BANKNAME As Integer = 14
  Private Const SQL_COLUMN_FLOORID As Integer = 15
  Private Const SQL_COLUMN_MAXPAYMENT As Integer = 16
  Private Const SQL_COLUMN_MINPAYMENT As Integer = 17
  Private Const SQL_COLUMN_ISOCODE As Integer = 18
  Private Const SQL_COLUMN_RETIREMENTDATE As Integer = 19
  Private Const SQL_COLUMN_RETIREMENTUSERID As Integer = 20
  Private Const SQL_COLUMN_RETIREMENTUSERNAME As Integer = 21


  ' Grid Columns
  Private Const GRID_COLUMN_COUNTR_ID As Integer = 0
  Private Const GRID_COLUMN_CODE As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_AREAID As Integer = 3
  Private Const GRID_COLUMN_AREANAME As Integer = 4
  Private Const GRID_COLUMN_BANKID As Integer = 5
  Private Const GRID_COLUMN_BANKNAME As Integer = 6
  Private Const GRID_COLUMN_FLOORID As Integer = 7
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 8
  Private Const GRID_COLUMN_LASTCONNECTION As Integer = 9
  Private Const GRID_COLUMN_STATUS As Integer = 10
  Private Const GRID_COLUMN_IP_ADRESS As Integer = 11
  Private Const GRID_COLUMN_PROVIDER As Integer = 12
  Private Const GRID_COLUMN_ENABLED As Integer = 13
  Private Const GRID_COLUMN_CREATED As Integer = 14
  Private Const GRID_COLUMN_LASTMODIFIED As Integer = 15
  Private Const GRID_COLUMN_MAXPAYMENT As Integer = 16
  Private Const GRID_COLUMN_MINPAYMENT As Integer = 17
  Private Const GRID_COLUMN_ISOCODE As Integer = 18
  Private Const GRID_COLUMN_RETIREMENTDATE As Integer = 19
  Private Const GRID_COLUMN_RETIREMENTUSERID As Integer = 20
  Private Const GRID_COLUMN_RETIREMENTUSERNAME As Integer = 21


  ' Width
  Private Const GRID_WIDTH_COUNTR_ID As Integer = 0
  Private Const GRID_WIDTH_CODE As Integer = 800
  Private Const GRID_WIDTH_NAME As Integer = 2000
  Private Const GRID_WIDTH_DESCRIPTION As Integer = 0
  Private Const GRID_WIDTH_LASTCONNECTION = 2200
  Private Const GRID_WIDTH_STATUS = 1400
  Private Const GRID_WIDTH_IP_ADRESS = 1550
  Private Const GRID_WIDTH_PROVIDER As Integer = 1500
  Private Const GRID_WIDTH_ENABLED As Integer = 1000
  Private Const GRID_WIDTH_CREATED As Integer = 2200
  Private Const GRID_WIDTH_LASTMODIFIED As Integer = 2200
  Private Const GRID_WIDTH_AREAID As Integer = 0
  Private Const GRID_WIDTH_AREANAME As Integer = 1200
  Private Const GRID_WIDTH_BANKID As Integer = 0
  Private Const GRID_WIDTH_BANKNAME As Integer = 1200
  Private Const GRID_WIDTH_FLOORID As Integer = 1200
  Private Const GRID_WIDTH_MAXPAYMENT As Integer = 1300
  Private Const GRID_WIDTH_MINPAYMENT As Integer = 1300
  Private Const GRID_WIDTH_ISOCODE As Integer = 800
  Private Const GRID_WIDTH_RETIREMENTDATE As Integer = 2200
  Private Const GRID_WIDTH_RETIREMENTUSERID As Integer = 0
  Private Const GRID_WIDTH_RETIREMENTUSERNAME As Integer = 1250

#End Region ' Constants

#Region " Enums"

#End Region ' Enums

#Region " Members "

  ' Filter
  Private m_report_name As String
  Private m_report_code As String
  Private m_report_enabled As String
  Private m_report_area_name As String
  Private m_report_bank_name As String
  Private m_report_floor_id As String
  Private m_report_zone_name As String
  Private m_report_countr_code As String
  Private m_report_countr_name As String
  Private m_report_status As String
  Private m_terminal_show_location As Boolean

#End Region ' Members

#Region " Overrides "

  ''' <summary>
  ''' Set Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COUNTR_SEL
    MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  '''  Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Labels
    txt_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7227)
    txt_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7228)
    gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7229)
    gb_CountR.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7225)
    cmb_area.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7230)
    ef_smoking.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7231)
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False
    cmb_bank.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7232)
    ef_floor_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6507)
    chk_online.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7299)
    chk_offline.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7300)

    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)
    Me.chk_yes.Text = GLB_NLS_GUI_INVOICING.GetString(479)
    Me.chk_no.Text = GLB_NLS_GUI_INVOICING.GetString(480)

    Call Me.txt_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.txt_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.ef_smoking.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.ef_floor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)
    Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    ' Initialize Combos
    Call AreaComboFill()
    Call BankComboFill()

    ' Style Sheet
    Call GUI_StyleSheet()

    Call AddHandles()

    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set Focus in a control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.txt_code
  End Sub 'GUI_SetInitialFocus

  ''' <summary>
  ''' Process clicks on data grid (double-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Dim _indx_sel As Integer

    _indx_sel = -1

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

        If Me.Grid.NumRows > 0 Then
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
        Else
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        If Not IsNothing(Me.Grid.SelectedRows) AndAlso Me.Grid.SelectedRows.Length > 0 Then
          _indx_sel = Me.Grid.SelectedRows(0)
        End If

        If _indx_sel <> -1 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7270), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            Call GUI_NewItemFromSelected()
          Else
            Call MyBase.GUI_ButtonClick(ButtonId)
          End If
        Else
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _result As Boolean

    _result = True

    With Me.Grid

      ' CR Id
      .Cell(RowIndex, GRID_COLUMN_COUNTR_ID).Value = DbRow.Value(SQL_COLUMN_COUNTR_ID)
      ' CR Code
      .Cell(RowIndex, GRID_COLUMN_CODE).Value = DbRow.Value(SQL_COLUMN_CODE)
      ' CR Name
      .Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)
      ' CR Description
      .Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_DESCRIPTION)
      'CR Last Connection
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_LASTCONNECTION)) Then
        .Cell(RowIndex, GRID_COLUMN_LASTCONNECTION).Value = DbRow.Value(SQL_COLUMN_LASTCONNECTION)
      End If
      'Cr Status

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_STATUS)) Then
        If DbRow.Value(SQL_COLUMN_STATUS) = "0" Then
          .Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7300)

        Else
          .Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7299)
        End If

      End If
      'CR IpAdress
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_IP_ADDRESS)) Then
        .Cell(RowIndex, GRID_COLUMN_IP_ADRESS).Value = DbRow.Value(SQL_COLUMN_IP_ADDRESS)
      End If
      ' CR Provider
      .Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)

      ' CR Area
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_AREAID)) Then
        .Cell(RowIndex, GRID_COLUMN_AREAID).Value = DbRow.Value(SQL_COLUMN_AREAID)
        .Cell(RowIndex, GRID_COLUMN_AREANAME).Value = DbRow.Value(SQL_COLUMN_AREANAME)
      End If

      ' CR Bank
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_BANKID)) Then
        .Cell(RowIndex, GRID_COLUMN_BANKID).Value = DbRow.Value(SQL_COLUMN_BANKID)
        .Cell(RowIndex, GRID_COLUMN_BANKNAME).Value = DbRow.Value(SQL_COLUMN_BANKNAME)
      End If
      ' CR Floor
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_FLOORID)) Then
        .Cell(RowIndex, GRID_COLUMN_FLOORID).Value = DbRow.Value(SQL_COLUMN_FLOORID)
      End If
      ' CR Payment
      .Cell(RowIndex, GRID_COLUMN_MAXPAYMENT).Value = GUI_FormatCurrency(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_MAXPAYMENT)), 0, DbRow.Value(SQL_COLUMN_MAXPAYMENT)))
      .Cell(RowIndex, GRID_COLUMN_MINPAYMENT).Value = GUI_FormatCurrency(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_MINPAYMENT)), 0, DbRow.Value(SQL_COLUMN_MINPAYMENT)))
      ' CR ISO Code
      .Cell(RowIndex, GRID_COLUMN_ISOCODE).Value = DbRow.Value(SQL_COLUMN_ISOCODE)
      ' CR Enabled
      .Cell(RowIndex, GRID_COLUMN_ENABLED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(DbRow.Value(SQL_COLUMN_ENABLED), 359, 360))
      ' CR Created
      .Cell(RowIndex, GRID_COLUMN_CREATED).Value = DbRow.Value(SQL_COLUMN_CREATED)
      ' CR Last Modified
      .Cell(RowIndex, GRID_COLUMN_LASTMODIFIED).Value = DbRow.Value(SQL_COLUMN_LASTMODIFIED)
      ' CR Retirement date
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_RETIREMENTDATE)) Then
        .Cell(RowIndex, GRID_COLUMN_RETIREMENTDATE).Value = DbRow.Value(SQL_COLUMN_RETIREMENTDATE)
      End If
      ' CR Retirement User 
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_RETIREMENTUSERID)) Then
        .Cell(RowIndex, GRID_COLUMN_RETIREMENTUSERID).Value = DbRow.Value(SQL_COLUMN_RETIREMENTUSERID)
        .Cell(RowIndex, GRID_COLUMN_RETIREMENTUSERNAME).Value = DbRow.Value(SQL_COLUMN_RETIREMENTUSERNAME)
      End If


    End With

    Return _result

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Get the defined Query Type
  ''' </summary>
  ''' <returns>ENUM_QUERY_TYPE</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE

    Return MyBase.GUI_GetQueryType()

  End Function ' GUI_GetQueryType

  ''' <summary>
  '''  SQL Command query ready to send to the database
  ''' </summary>
  ''' <returns>Build an SQL Command query from conditions set in the filters</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetSqlCommand() As SqlClient.SqlCommand

    Return MyBase.GUI_GetSqlCommand()

  End Function ' GUI_GetSqlCommand

  ''' <summary>
  ''' Show form to edit new Item
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditNewItem()
    Dim _frm As frm_countr_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_countr_edit
    _frm.ShowNewItem()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditNewItem

  ''' <summary>
  ''' Show form to update a item
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _frm As frm_countr_edit
    Dim _idx_row As Int64
    Dim _id As Int64

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' If no row is selected
    If _idx_row = Me.Grid.NumRows Then
      Return
    End If
    _id = Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNTR_ID).Value

    _frm = New frm_countr_edit
    _frm.ShowEditItem(_id)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Build an SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT    CR_COUNTR_ID                                           ")
    _str_sql.AppendLine("           , CR_CODE   				                                     ")
    _str_sql.AppendLine("           , CR_NAME   				                                     ")
    _str_sql.AppendLine("           , CR_DESCRIPTION                                         ")
    _str_sql.AppendLine("           , CR_LAST_CONNECTION                                     ")
    _str_sql.AppendLine("           , ISNULL(CR_STATUS,0)                                    ")
    _str_sql.AppendLine("           , CR_IP_ADDRESS                                          ")
    _str_sql.AppendLine("           , CR_PROVIDER      		                                   ")
    _str_sql.AppendLine("           , CR_ENABLED 	 			                                     ")
    _str_sql.AppendLine("           , CR_CREATED                                             ")
    _str_sql.AppendLine("           , CR_LAST_MODIFIED                                       ")
    _str_sql.AppendLine("           , CR_AREA_ID       		                                   ")
    _str_sql.AppendLine("           , AR_NAME AS CR_AREA_NAME                                ")
    _str_sql.AppendLine("           , CR_BANK_ID      		                                   ")
    _str_sql.AppendLine("           , BK_NAME AS CR_BANK_NAME                                ")
    _str_sql.AppendLine("           , CR_FLOOR_ID 		                                       ")
    _str_sql.AppendLine("           , CR_MAX_PAYMENT		                                     ")
    _str_sql.AppendLine("           , CR_MIN_PAYMENT		                                     ")
    _str_sql.AppendLine("           , CR_CURRENT_ISOCODE                                     ")
    _str_sql.AppendLine("           , CR_RETIREMENT_DATE                                     ")
    _str_sql.AppendLine("           , CR_RETIREMENT_USER_ID                                  ")
    _str_sql.AppendLine("           , GU_USERNAME AS CR_RETIREMENT_USER_NAME                 ")
    _str_sql.AppendLine("     FROM    COUNTR         			                                   ")
    _str_sql.AppendLine("     LEFT    JOIN AREAS      ON  CR_AREA_ID = AR_AREA_ID            ")
    _str_sql.AppendLine("     LEFT    JOIN BANKS      ON  CR_BANK_ID = BK_BANK_ID            ")
    _str_sql.AppendLine("     LEFT    JOIN GUI_USERS  ON  GU_USER_ID = CR_RETIREMENT_USER_ID ")

    ' Where 
    _str_sql.AppendLine(GetSqlWhere())

    ' Order
    _str_sql.AppendLine(" ORDER BY    CR_CODE, CR_NAME ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Set texts corresponding to the provided filter values for the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_report_enabled = String.Empty
    m_report_area_name = String.Empty
    m_report_bank_name = String.Empty
    m_report_zone_name = String.Empty
    m_report_countr_code = String.Empty
    m_report_countr_name = String.Empty
    m_report_floor_id = String.Empty
    m_report_status = String.Empty


    ' Enabled filter
    If Me.chk_yes.Checked AndAlso Not Me.chk_no.Checked Then
      m_report_enabled = GLB_NLS_GUI_INVOICING.GetString(479)        ' Enabled
    ElseIf Me.chk_no.Checked AndAlso Not Me.chk_yes.Checked Then
      m_report_enabled = GLB_NLS_GUI_INVOICING.GetString(480)        ' Disabled
    Else
      m_report_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    End If

    'Status filter
    If Me.chk_online.Checked AndAlso Not Me.chk_offline.Checked Then
      m_report_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7299) ' Online
    ElseIf Me.chk_offline.Checked AndAlso Not Me.chk_online.Checked Then
      m_report_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7300) ' Offline
    Else
      m_report_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    End If

    ' Location filter
    If Me.cmb_area.SelectedIndex > 0 Then
      m_report_area_name = Me.cmb_area.TextValue
      m_report_zone_name = Me.ef_smoking.Value
      m_report_bank_name = Me.cmb_bank.TextValue
    End If

    'Countr
    m_report_countr_code = Me.txt_code.Value
    m_report_countr_name = Me.txt_name.Value

    'FloorID
    m_report_floor_id = Me.ef_floor_id.Value
  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'Countr
    PrintData.SetFilter(String.Format("{0}{1}{2}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7227), " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)), m_report_countr_code)
    PrintData.SetFilter(String.Format("{0}{1}{2}", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7228), " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)), m_report_countr_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422), m_report_enabled)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6507), m_report_status)

    ' Location Filter
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), IIf(m_terminal_show_location,
                                                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304),
                                                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305)))
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(435), m_report_area_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(430), m_report_zone_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(431), m_report_bank_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233), m_report_floor_id)

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Previous check in filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True

  End Function 'GUI_FilterCheck

  ''' <summary>
  ''' cleans grid
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ClearGrid()

    MyBase.GUI_ClearGrid()

  End Sub ' GUI_ClearGrid

#End Region ' Overrides

#Region " Private Functions "

  ''' <summary>
  ''' Add Handles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

  ''' <summary>
  ''' Area selection
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub ' cmb_area_ValueChangedEvent

  ''' <summary>
  ''' Returns zone description
  ''' </summary>
  ''' <param name="AreaId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function ' GetZoneDescription

  ''' <summary>
  ''' Init areas combo box
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub ' AreaComboFill

  ''' <summary>
  ''' Init BANKS combo box
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub ' BankComboFill

  ''' <summary>
  ''' Set Style to the Grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    ' Grid StyleSheet
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' Id
      .Column(GRID_COLUMN_COUNTR_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_COUNTR_ID).Width = GRID_WIDTH_COUNTR_ID

      ' Code
      .Column(GRID_COLUMN_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7227)
      .Column(GRID_COLUMN_CODE).Width = GRID_WIDTH_CODE
      .Column(GRID_COLUMN_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Name
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7228)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Area 
      .Column(GRID_COLUMN_AREAID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_AREAID).Header(0).Text = ""
      .Column(GRID_COLUMN_AREAID).Width = GRID_WIDTH_AREAID

      .Column(GRID_COLUMN_AREANAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_AREANAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7230)
      .Column(GRID_COLUMN_AREANAME).Width = IIf(m_terminal_show_location, GRID_WIDTH_AREANAME, 0)
      .Column(GRID_COLUMN_AREANAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank 
      .Column(GRID_COLUMN_BANKID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_BANKID).Header(1).Text = ""
      .Column(GRID_COLUMN_BANKID).Width = GRID_WIDTH_BANKID

      .Column(GRID_COLUMN_BANKNAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_BANKNAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7232)
      .Column(GRID_COLUMN_BANKNAME).Width = IIf(m_terminal_show_location, GRID_WIDTH_BANKNAME, 0)
      .Column(GRID_COLUMN_BANKNAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Floor
      .Column(GRID_COLUMN_FLOORID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_FLOORID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233)
      .Column(GRID_COLUMN_FLOORID).Width = IIf(m_terminal_show_location, GRID_WIDTH_FLOORID, 0)
      .Column(GRID_COLUMN_FLOORID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(GRID_COLUMN_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7235)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      'Last connection 
      .Column(GRID_COLUMN_LASTCONNECTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_LASTCONNECTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7297)
      .Column(GRID_COLUMN_LASTCONNECTION).Width = GRID_WIDTH_LASTCONNECTION
      .Column(GRID_COLUMN_LASTCONNECTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6868)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Ip adress
      .Column(GRID_COLUMN_IP_ADRESS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_IP_ADRESS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1897)
      .Column(GRID_COLUMN_IP_ADRESS).Width = GRID_WIDTH_IP_ADRESS
      .Column(GRID_COLUMN_IP_ADRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Provider
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_PROVIDER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7236)
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Enabled
      .Column(GRID_COLUMN_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_ENABLED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7234)
      .Column(GRID_COLUMN_ENABLED).Width = GRID_WIDTH_ENABLED
      .Column(GRID_COLUMN_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Created Date
      .Column(GRID_COLUMN_CREATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_CREATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7262)
      .Column(GRID_COLUMN_CREATED).Width = GRID_WIDTH_CREATED
      .Column(GRID_COLUMN_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Last update date
      .Column(GRID_COLUMN_LASTMODIFIED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_LASTMODIFIED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7263)
      .Column(GRID_COLUMN_LASTMODIFIED).Width = GRID_WIDTH_LASTMODIFIED
      .Column(GRID_COLUMN_LASTMODIFIED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Max payment
      .Column(GRID_COLUMN_MAXPAYMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7238)
      .Column(GRID_COLUMN_MAXPAYMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7239)
      .Column(GRID_COLUMN_MAXPAYMENT).Width = GRID_WIDTH_MAXPAYMENT
      .Column(GRID_COLUMN_MAXPAYMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Min payment
      .Column(GRID_COLUMN_MINPAYMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7238)
      .Column(GRID_COLUMN_MINPAYMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7240)
      .Column(GRID_COLUMN_MINPAYMENT).Width = GRID_WIDTH_MINPAYMENT
      .Column(GRID_COLUMN_MINPAYMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Iso Code
      .Column(GRID_COLUMN_ISOCODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7238)
      .Column(GRID_COLUMN_ISOCODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7237)
      .Column(GRID_COLUMN_ISOCODE).Width = GRID_WIDTH_ISOCODE
      .Column(GRID_COLUMN_ISOCODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      ' Retirement date
      .Column(GRID_COLUMN_RETIREMENTDATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7241)
      .Column(GRID_COLUMN_RETIREMENTDATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7242)
      .Column(GRID_COLUMN_RETIREMENTDATE).Width = GRID_WIDTH_RETIREMENTDATE
      .Column(GRID_COLUMN_RETIREMENTDATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Retirement User
      .Column(GRID_COLUMN_RETIREMENTUSERID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7241)
      .Column(GRID_COLUMN_RETIREMENTUSERID).Header(1).Text = ""
      .Column(GRID_COLUMN_RETIREMENTUSERID).Width = GRID_WIDTH_RETIREMENTUSERID

      .Column(GRID_COLUMN_RETIREMENTUSERNAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7241)
      .Column(GRID_COLUMN_RETIREMENTUSERNAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7243)
      .Column(GRID_COLUMN_RETIREMENTUSERNAME).Width = GRID_WIDTH_RETIREMENTUSERNAME
      .Column(GRID_COLUMN_RETIREMENTUSERNAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Set Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    ' Fields
    txt_code.Value = String.Empty
    txt_name.Value = String.Empty
    ef_floor_id.Value = String.Empty
    ef_smoking.Value = String.Empty
    Me.chk_terminal_location.Checked = False

    ' Enabled
    chk_yes.Checked = True
    chk_no.Checked = False

    ' Status
    chk_online.Checked = False
    chk_offline.Checked = False

    ' Location
    cmb_area.SelectedIndex = 0
    cmb_bank.SelectedIndex = 0
    ef_floor_id.Value = String.Empty
    ef_smoking.Value = String.Empty

    m_report_enabled = String.Empty
    m_report_area_name = String.Empty
    m_report_bank_name = String.Empty
    m_report_zone_name = String.Empty
    m_report_countr_code = String.Empty
    m_report_countr_name = String.Empty
    m_report_floor_id = String.Empty
    m_report_status = String.Empty

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Validate if the indicated row is correct
  ''' </summary>
  ''' <param name="IdxRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    Return (Me.Grid.Cell(IdxRow, GRID_COLUMN_COUNTR_ID).Value <> "")

  End Function ' IsValidDataRow

  ''' <summary>
  '''  Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '''  main SQL Query.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlWhere() As String

    Dim _str_where As String = String.Empty

    ' -- Code Filter -- 
    If Not String.IsNullOrEmpty(Me.txt_code.Value) Then
      _str_where = _str_where & " AND (CR_CODE LIKE '%" & DataTable_FilterValue(Me.txt_code.Value) & "%') "
    End If

    ' -- Name Filter -- 
    If Not String.IsNullOrEmpty(Me.txt_name.Value) Then
      _str_where = _str_where & " AND (CR_NAME LIKE '%" & DataTable_FilterValue(Me.txt_name.Value) & "%') "
    End If

    ' -- Enabled Filter -- 
    If Me.chk_yes.Checked AndAlso Not Me.chk_no.Checked Then
      '  Only Enabled
      _str_where = _str_where & " AND (CR_ENABLED IN (1)) "
    ElseIf Me.chk_no.Checked AndAlso Not Me.chk_yes.Checked Then
      '  Only Disabled
      _str_where = _str_where & " AND (CR_ENABLED IN (0)) "
    Else
      '  Enabled and Disabled
      _str_where = _str_where & " AND (CR_ENABLED IN (0,1)) "
    End If

    ' -- Anyone area selected  -
    If Me.cmb_area.SelectedIndex > 0 Then
      _str_where = _str_where & " AND (CR_AREA_ID = " & Me.cmb_area.Value & ") "
      _str_where = _str_where & " AND (CR_BANK_ID = " & Me.cmb_bank.Value & ") "
    End If

    ' -- Floor Id --
    If Not String.IsNullOrEmpty(Me.ef_floor_id.Value) Then
      _str_where = _str_where & " AND " & GUI_FilterField("CR_FLOOR_ID", Me.ef_floor_id.Value, False, False, True)
    End If

    'Online
    If Me.chk_online.Checked AndAlso Not Me.chk_offline.Checked Then
      '  Only Online
      _str_where = _str_where & " AND (CR_STATUS IN (1)) "
    ElseIf Me.chk_offline.Checked AndAlso Not Me.chk_online.Checked Then
      '  Only Offline
      _str_where = _str_where & " AND (CR_STATUS IN (0) OR (CR_STATUS IS NULL) ) "
    Else
      '  All
      _str_where = _str_where & " AND (CR_STATUS IN (0, 1) OR (CR_STATUS IS NULL) ) "
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & _str_where.Remove(_str_where.IndexOf("AND"), 3)
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ''' <summary>
  ''' New Item From Selected
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_NewItemFromSelected()

    Dim idx_row As Int32
    Dim countr_id As Integer

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the GamingTable ID, and launch the editor
    countr_id = Me.Grid.Cell(idx_row, GRID_COLUMN_COUNTR_ID).Value

    Call GUI_ShowNewCountRForm(countr_id)

  End Sub ' GUI_NewItemFromSelected

  ''' <summary>
  ''' Open form to create new CountR
  ''' </summary>
  ''' <param name="CountRCopiedId"></param>
  ''' <remarks></remarks>
  Private Sub GUI_ShowNewCountRForm(Optional ByVal CountRCopiedId As Long = 0)

    Dim frm_edit As frm_countr_edit
    frm_edit = New frm_countr_edit()
    frm_edit.ShowNewItem(CountRCopiedId)
    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_ShowNewCountRForm

#End Region ' Private Functions

#Region " Public Functions "

  ''' <summary>
  ''' Open form for edit
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(sender As Object, e As EventArgs) Handles chk_terminal_location.CheckedChanged
    m_terminal_show_location = chk_terminal_location.Checked
  End Sub
#End Region ' Events


End Class
