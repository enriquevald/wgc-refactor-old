'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_export_cash_cage_count
' DESCRIPTION:   
' AUTHOR:        Humberto Braojos
' CREATION DATE: 01-Apr-2015
'
' REVISION HISTORY:
'
' Date        Author       Description 
' ----------  ------       -----------------------------------------
' 01-APR-2015 HBB          First Release.
' 08-APR-2015 SGB          Backlog Item 541 Task 951: Change format form

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_export_cash_cage_count
  Inherits frm_base

  Private m_print_selected As Boolean
  Private m_export_to_excel As Boolean
  Private m_accept_cash_cage_caount As Boolean

  Protected Overrides Sub GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4352)
    Me.chk_excel_option.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6089)
    Me.chk_print_option.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6088)
    Me.lb_confirmation_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4575)
    Me.lb_acceptornot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6090)
    Me.lb_confirmation_message.Visible = True
    Me.lb_acceptornot.Visible = True
    Me.btn_acept.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6091)
    Me.btn_cancel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6092)
    Me.Height = 194
    Me.Width = 448
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
    Me.AutoSize = False
  End Sub

  Public ReadOnly Property PrintSelected() As Boolean
    Get
      Return m_print_selected
    End Get
  End Property

  Public ReadOnly Property ExportToExcel()
    Get
      Return m_export_to_excel
    End Get
  End Property

  Public ReadOnly Property AcceptCashCageCount()
    Get
      Return m_accept_cash_cage_caount
    End Get
  End Property

  Private Sub chk_print_option_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_print_option.CheckedChanged

    'chk_excel_option.Checked = False
    m_export_to_excel = False

    If chk_print_option.Checked Then
      chk_excel_option.Checked = False
      m_print_selected = True
    Else
      m_print_selected = False
    End If

  End Sub

  Private Sub chk_excel_option_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_excel_option.CheckedChanged

    m_print_selected = False

    If chk_excel_option.Checked Then
      chk_print_option.Checked = False
      m_export_to_excel = True
    Else
      m_export_to_excel = False
    End If

  End Sub

  Private Sub btn_acept_ClickEvent() Handles btn_acept.ClickEvent
    m_accept_cash_cage_caount = True
    Me.Close()
  End Sub

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
    m_accept_cash_cage_caount = False
    Me.Close()
  End Sub
End Class