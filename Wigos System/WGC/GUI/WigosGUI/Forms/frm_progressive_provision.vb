'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_progressive_provision.vb
'
' DESCRIPTION : Progressive Jackpots Provissioning screen
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-AUG-2014  JMM    Initial version.
' 14-OCT-2014  JMM    Fixed bug WIG-1479: Windows dissapears on EDIT mode if clicking out
' 15-OCT-2014  JMV    Fixed bug WIG-1478: Empty values in both grids on edit mode
' 20-OCT-2014  JMM    Fixed bug WIG-1533: Provision doesn't insert with the proper To date.
' 21-OCT-2014  JMM    Fixed bug WIG-1551: Error on loading levels when there are no records on TERMINAL_SAS_METERS
' 29-OCT-2014  JMM    Fixed bug WIG-1568: Terminals total row becomes white on clicking on it
' 03-NOV-2014  JMM    Fixed bug WIG-1621: On Reading/Equally mode, theorical amounts don't get updated on period editing
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_progressive_provision
  Inherits frm_base_edit

#Region " Constants "

  Private Const AMOUNT_COL_WIDTH As Integer = 1550

  ' Provision Grid Columns
  Private Const GRID_LEVELS_COLUMNS As Integer = 10

  Private Const GRID_LEVELS_HEADER_ROWS As Integer = 2

  Private Const GRID_LEVELS_COLUMN_LEVEL_ID As Integer = 0
  Private Const GRID_LEVELS_COLUMN_NAME_LEVEL As Integer = 1
  Private Const GRID_LEVELS_COLUMN_CONTRIBUTION_PCT As Integer = 2
  Private Const GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT As Integer = 3
  Private Const GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT As Integer = 4
  Private Const GRID_LEVELS_COLUMN_DELTA_AMOUNT As Integer = 5
  Private Const GRID_LEVELS_COLUMN_PROVISION_AMOUNT As Integer = 6
  Private Const GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT As Integer = 7
  Private Const GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV As Integer = 8
  Private Const GRID_LEVELS_COLUMN_PCTJ As Integer = 9

  ' Provision to Termianls Grid Columns
  Private Const GRID_TERMINALS_COLUMNS As Integer = 8

  Private Const GRID_TERMINALS_HEADER_ROWS As Integer = 1

  Private Const GRID_TERMINALS_COLUMN_TERMINAL_ID As Integer = 0
  Private Const GRID_TERMINALS_COLUMN_PROVIDER As Integer = 1
  Private Const GRID_TERMINALS_COLUMN_TERMINAL As Integer = 2
  Private Const GRID_TERMINALS_COLUMN_PLAYED_AMOUNT As Integer = 3
  Private Const GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT As Integer = 4
  Private Const GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT As Integer = 5
  Private Const GRID_TERMINALS_COLUMN_PROVISION_AMOUNT As Integer = 6
  Private Const GRID_TERMINALS_COLUMN_PCTJ As Integer = 7

  Private Const MAX_LEVELS_ROWS As Integer = 10
  Private Const MAX_TERMINALS_ROWS As Integer = 20

  Private Const STR_INFINITE As String = "---"

#End Region ' Constants

#Region " Enumerates "

#End Region

#Region " Members "

  Private m_max_hour_to As DateTime
  Private m_default_hour_to As Object
  Private m_total_levels_amount As Double
  Private m_initialized As Boolean = False
  Private m_dt_progressives As DataTable
  Private m_insert_provision As Boolean
  Private m_total_terminals_provision As Double
  Private m_theoretical_amount As Double
  Private m_progressive_contribution As Double
  Private m_total_played As Double

  Private m_input_mode As Progressives.PROGRESSIVE_PROVISION_INPUT_MODE
  Private m_distribution_mode As Progressives.PROGRESSIVE_PROVISION_DISTRIBUTION_MODE
  Private m_provision_to_cage As Progressives.PROGRESSIVE_PROVISION_GOES_TO_CAGE

  Private _m_last_date As System.DateTime = Nothing
  Private _m_is_equal As Boolean = False

  Private m_provision_max_allowed As Integer

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROVISIONS_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form Text
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5265) ' 5245 "Provision details"
    Else
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5205) ' 5205 "New provision"
    End If

    ' - Buttons
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)  'EXIT
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4679) '4679 "Cancel"

    ' - Progressive Combo
    Me.cmb_progressive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5340) ' 5340 "Progressive jackpot"

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Call LoadComboProgressives()
    End If

    ' Canceled Provision lavel
    Me.lbl_canceled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5534) ' 5534 "Canceled provision"
    Me.lbl_canceled.Visible = False

    ' - Provisioning Dates
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) '830 "Date Range"

    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(311) '311 "Start date"
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(312) '312 "End date"

    If m_provision_max_allowed = 1 And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.dtp_from.SetFormat(GUI_CommonOperations.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, GUI_CommonOperations.ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_to.SetFormat(GUI_CommonOperations.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, GUI_CommonOperations.ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      Me.dtp_from.IsReadOnly = False
      Me.dtp_from.Width = 165
      Me.dtp_to.IsReadOnly = True
      Me.dtp_to.Width = 165
    Else
      Me.dtp_from.SetFormat(GUI_CommonOperations.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, GUI_CommonOperations.ENUM_FORMAT_TIME.FORMAT_HOURS)
      Me.dtp_to.SetFormat(GUI_CommonOperations.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, GUI_CommonOperations.ENUM_FORMAT_TIME.FORMAT_HOURS)
      Me.dtp_from.IsReadOnly = True
      Me.dtp_to.IsReadOnly = False
    End If

    ' - Provision
    Me.gb_levels.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) ' 5245 "Provision"

    ' - Increase to Provision button
    Me.btn_incr_2_prov.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5246) ' 5246 "By increment"

    ' - Theoretical to Provision button
    Me.btn_theo_2_prov.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5463) '5463 "By theoretical"

    ' - Provision to Terminals
    Me.gb_machine_provisions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5247) '5247 "Provision to terminals"

    ' - As Played button
    Me.btn_as_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5248) '5248 "By Played"

    ' - Equally button
    Me.btn_equally.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5250) '5250 "By machine"

    ' - Cage Provision
    Me.ef_cage_provision_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5252) '5252 "Cage Provision"
    Me.ef_cage_provision_amount.TextAlign = HorizontalAlignment.Right
    Me.ef_cage_provision_amount.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

    ' - Info Labels
    Me.lbl_progressive_info_010.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5251) ' 5251 "Theoretical Provision"
    Me.lbl_progressive_info_011.TextAlign = ContentAlignment.TopRight
    Me.lbl_progressive_info_011.Text = String.Empty
    Me.lbl_progressive_info_012.TextAlign = ContentAlignment.TopRight
    Me.lbl_progressive_info_012.Text = String.Empty

    Me.lbl_progressive_info_020.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) ' 5245 "Provisi�n"
    Me.lbl_progressive_info_021.TextAlign = ContentAlignment.TopRight
    Me.lbl_progressive_info_021.Text = String.Empty
    Me.lbl_progressive_info_022.TextAlign = ContentAlignment.TopRight
    Me.lbl_progressive_info_022.Text = String.Empty

    ' User name
    Me.ef_user_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603) '603 "User"
    Me.ef_user_name.IsReadOnly = True

    ' Configure Data Grid
    Call GUI_StyleView()

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.dtp_to.IsReadOnly = True
      Me.ef_cage_provision_amount.IsReadOnly = True
      Me.btn_incr_2_prov.Visible = False
      Me.btn_theo_2_prov.Visible = False
      Me.btn_as_played.Visible = False
      Me.btn_equally.Visible = False
      Me.cmb_progressive.IsReadOnly = True
      Me.gb_date.Width = 218
      Me.dtp_to.Width = 190
      Me.dtp_from.Width = 190

      Me.lbl_progressive_info_010.Visible = False
      Me.lbl_progressive_info_011.Visible = False
      Me.lbl_progressive_info_012.Visible = False
      Me.lbl_progressive_info_020.Visible = False
      Me.lbl_progressive_info_021.Visible = False
      Me.lbl_progressive_info_022.Visible = False

      Me.Width = 662
    Else
      Me.ef_user_name.Visible = False

      If Not Cage.IsCageEnabled Then
        Me.ef_cage_provision_amount.IsReadOnly = True
      End If
    End If

    ' Version 2 changes: Disable all the buttons
    Me.btn_incr_2_prov.Visible = False
    Me.btn_theo_2_prov.Visible = False
    Me.btn_as_played.Visible = False
    Me.btn_equally.Visible = False
    Me.ef_cage_provision_amount.Visible = False

    Call AdjustLevelsGridHeight()

    'Call set form features resolutions when size's form exceeds desktop area
    Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)

    Me.m_initialized = True

  End Sub 'GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    If (Me.dg_levels.Cell(Me.dg_levels.NumRows - 1, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = String.Empty _
    Or m_total_terminals_provision = 0) Then
      ' 5249 "Can't generate a zero amount provision"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5249), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Me.DbStatus = ENUM_STATUS.STATUS_ERROR

      Return False
    End If

    If Not CheckTotals() Then
      ' 5343 "Levels total provision and terminals total provision must be equals."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5343), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Me.DbStatus = ENUM_STATUS.STATUS_ERROR

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _idx_row As Integer
    Dim _row As DataRow

    Try
      _progressive_provision = DbEditedObject

      _progressive_provision.Amount = m_total_terminals_provision
      _progressive_provision.TheoreticalAmount = m_theoretical_amount
      _progressive_provision.TotalTerminalsProvision = m_total_terminals_provision
      _progressive_provision.TotalCageProvision = m_total_terminals_provision

      If Not _progressive_provision.Levels Is Nothing _
      And Me.dg_levels.NumRows > 0 Then
        For _idx_row = 0 To _progressive_provision.Levels.Rows.Count - 1
          _row = _progressive_provision.Levels.Select("LEVEL_ID = " + Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_LEVEL_ID).Value)(0)
          If Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value <> String.Empty Then
            _row("PROVISION_AMOUNT") = GUI_ParseCurrency(Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value)
          End If
        Next
      End If

      _progressive_provision.GuiUserId = GLB_CurrentUser.Id
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _dt_aux_progressives As DataTable

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Me.dtp_from.Enabled = False
      Me.dtp_from.Value = DateTime.MinValue
      Me.dtp_to.Enabled = False
      Me.dtp_to.Value = DateTime.MinValue

      _progressive_provision = DbEditedObject

      If _progressive_provision.ProgressiveId <> 0 Then

        ' - Cancel Provision button
        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
          If _progressive_provision.HourTo = _progressive_provision.ProgressiveLastProvisionedHour _
          And _progressive_provision.Status = Progressives.PROGRESSIVE_PROVISION_STATUS.ACTIVE Then
            Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
          End If

          Me.lbl_canceled.Visible = (_progressive_provision.Status = Progressives.PROGRESSIVE_PROVISION_STATUS.CANCELED)
        End If

        ' Provision dates
        Me.dtp_from.Enabled = True
        Me.dtp_from.Value = _progressive_provision.HourFrom
        Me.dtp_to.Enabled = True
        Me.dtp_to.Value = _progressive_provision.HourTo

        Call LoadLevelsGrid(_progressive_provision)

        Call AdjustLevelsGridHeight()

        'Call set form features resolutions when size's form exceeds desktop area
        Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)

        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
          m_theoretical_amount = _progressive_provision.TheoreticalAmount

          _dt_aux_progressives = New DataTable

          _dt_aux_progressives.Columns.Add("PGS_PROGRESSIVE_ID", Type.GetType("System.Int64"))
          _dt_aux_progressives.Columns.Add("PGS_NAME", Type.GetType("System.String"))

          _dt_aux_progressives.Rows.Add(_progressive_provision.ProgressiveId, _progressive_provision.ProgressiveName)

          Me.cmb_progressive.Clear()
          Me.cmb_progressive.Add(_dt_aux_progressives)
          Me.cmb_progressive.Value = _progressive_provision.ProgressiveId

          If _progressive_provision.GuiUserId <> 0 And _progressive_provision.GuiUserName <> String.Empty Then
            Me.ef_user_name.Value = _progressive_provision.GuiUserName
            Me.ef_user_name.Visible = True
          Else
            Me.ef_user_name.Visible = False
          End If
        End If

        ' Cage provision amount
        Me.ef_cage_provision_amount.Value = GUI_FormatCurrency(_progressive_provision.TotalCageProvision)

        Me.lbl_progressive_info_011.Text = GUI_FormatCurrency(m_theoretical_amount)
        Me.lbl_progressive_info_012.Text = "(" & GUI_FormatNumber(m_progressive_contribution, 4) & "%)"
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _res As ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        DbEditedObject = New CLASS_PROGRESSIVE_PROVISION

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        _progressive_provision = Me.DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_SUPPORTED Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          _progressive_provision = Me.DbEditedObject
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        _progressive_provision = Me.DbEditedObject

        If m_provision_max_allowed = 1 AndAlso Progressives.ExistsProvisionOnDate(_progressive_provision.ProgressiveId, Me.dtp_from.Value) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5871), ENUM_MB_TYPE.MB_TYPE_INFO, , , Me.dtp_from.Value.ToShortDateString())

          m_insert_provision = False
        Else
          _progressive_provision.CurrentAmount = Me.dg_levels.Cell(Me.dg_levels.NumRows - 1, GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Value

          If _progressive_provision.TotalCageProvision <> _progressive_provision.TotalTerminalsProvision Then
            '5339 "The following amounts will be provisioned:\n -	Cash cage provision: %1\n -	Terminal provision: %2\n\nDo you want to continue?"
            _res = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5339), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
                          , _
                          GUI_FormatCurrency(_progressive_provision.TotalCageProvision), _
                          GUI_FormatCurrency(_progressive_provision.TotalTerminalsProvision))
          Else
            '5243 "A %1 amount provision will be made.\n\nDo you want to continue?"  
            _res = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5243), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
                          , _
                          GUI_FormatCurrency(_progressive_provision.TotalCageProvision))
          End If

          If _res = ENUM_MB_RESULT.MB_RESULT_YES Then
            m_insert_provision = True
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT

        If m_insert_provision Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        Me.CloseOnOkClick = False

        If m_insert_provision Then

          Select Case Me.DbStatus
            Case ENUM_STATUS.STATUS_OK
              _progressive_provision = Me.DbEditedObject

              '5441 "A provision for progressive jackpot %1 was performed for period %2 to %3."  
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5441), _
                                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                                mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                _progressive_provision.ProgressiveName, _
                                _progressive_provision.HourFrom, _
                                _progressive_provision.HourTo)

              m_default_hour_to = _progressive_provision.HourTo

            Case ENUM_STATUS.STATUS_DEPENDENCIES

            Case Else
              If Me.DbStatus <> ENUM_STATUS.STATUS_DEPENDENCIES Then
                '5442 "An error occurred while inserting provision."
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5442), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
              End If
          End Select
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

        m_insert_provision = False

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation  

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _dr As DataRow()

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK

        If Me.ActiveControl.Equals(Me.dtp_to) Then

          Return
        End If

        Try
          If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
            Me.CloseOnOkClick = False

            MyBase.GUI_ButtonClick(ButtonId)

            If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
              _progressive_provision = Me.DbEditedObject

              _dr = m_dt_progressives.Select("PGS_PROGRESSIVE_ID = " & _progressive_provision.ProgressiveId)
              _dr(0).Delete()

              m_dt_progressives.AcceptChanges()

              Me.cmb_progressive.Clear()
              Me.cmb_progressive.Add(m_dt_progressives)
              Me.cmb_progressive.SelectedIndex = -1

              Me.lbl_progressive_info_011.Text = String.Empty
              Me.lbl_progressive_info_012.Text = String.Empty
              Me.lbl_progressive_info_021.Text = String.Empty
              Me.lbl_progressive_info_022.Text = String.Empty

              Call EnableButtons(False)

              Me.dg_levels.Clear()
              Me.dg_terminals.Clear()
              Me.gb_machine_provisions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5247)
              Me.ef_cage_provision_amount.Value = 0

              Me.dtp_from.Enabled = False
              Me.dtp_from.Value = DateTime.MinValue
              Me.dtp_to.Enabled = False
              Me.dtp_to.Value = DateTime.MinValue
            End If
          End If
        Catch _ex As Exception
          Log.Exception(_ex)
        End Try

      Case ENUM_BUTTON.BUTTON_CANCEL
        MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call CancelProvision()

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_levels.ContainsFocus Then
      Return Me.dg_levels.KeyPressed(sender, e)
    End If

    ' The keypress event is done in uc_grid control
    If Me.dg_terminals.ContainsFocus Then
      Return Me.dg_terminals.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Or Me.cmb_progressive.Value = 0 Then
      CloseCanceled = False
    Else
      '5440 "If you close the window, the provision entry will be canceled.\nDo you want to continue?"
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5440), _
         ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        CloseCanceled = True
      End If
    End If
  End Sub

#End Region ' Overrides

#Region " Private Functions"

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()
    ' Levels Grid
    With Me.dg_levels

      Call .Init(GRID_LEVELS_COLUMNS, GRID_LEVELS_HEADER_ROWS, True)

      ' Grid Columns

      ' Level #
      .Column(GRID_LEVELS_COLUMN_LEVEL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) ' 443 "Level"
      .Column(GRID_LEVELS_COLUMN_LEVEL_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) ' 443 "Level"
      .Column(GRID_LEVELS_COLUMN_LEVEL_ID).Width = 800
      .Column(GRID_LEVELS_COLUMN_LEVEL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_LEVEL_ID).HighLightWhenSelected = False

      ' Level Name
      .Column(GRID_LEVELS_COLUMN_NAME_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) ' 443 "Level"
      .Column(GRID_LEVELS_COLUMN_NAME_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253) ' 253 "Name"
      .Column(GRID_LEVELS_COLUMN_NAME_LEVEL).Width = 1775
      .Column(GRID_LEVELS_COLUMN_NAME_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_LEVELS_COLUMN_NAME_LEVEL).HighLightWhenSelected = False

      ' Contribution %
      .Column(GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) ' 443 "Level"
      .Column(GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).Header(1).Text = "%"
      .Column(GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).Width = 800
      .Column(GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).HighLightWhenSelected = False

      ' Previous amount
      If Me.m_input_mode = Progressives.PROGRESSIVE_PROVISION_INPUT_MODE.COUNTER_READING Then
        .Column(GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5244) ' 5244 "Progresive"
      Else
        .Column(GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Header(0).Text = String.Empty
      End If
      .Column(GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5341) ' 5341 "Previous Reading"
      .Column(GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).HighLightWhenSelected = False

      ' New Amount
      .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5244) ' 5244 "Progresive"
      .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5298) ' 5298 "Current Reading"
      .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If Me.m_input_mode = Progressives.PROGRESSIVE_PROVISION_INPUT_MODE.COUNTER_READING Then
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Editable = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW)
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).EditionControl.EntryField.IsReadOnly = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).HighLightWhenSelected = False
      Else
        .Column(GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Width = 0
      End If

      ' Increase Amount
      .Column(GRID_LEVELS_COLUMN_DELTA_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5244) ' 5244 "Progresive"
      .Column(GRID_LEVELS_COLUMN_DELTA_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1657) ' 1657 "Increase"
      .Column(GRID_LEVELS_COLUMN_DELTA_AMOUNT).Width = 0 'IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_LEVELS_COLUMN_DELTA_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_DELTA_AMOUNT).HighLightWhenSelected = False

      ' Theoretical Amount
      .Column(GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) ' 5245 "Provision"
      .Column(GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3435) ' 3435 "Theoretical"
      .Column(GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).HighLightWhenSelected = False

      ' Provision Amount
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) ' 5245 "Provision"
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) ' 5245 "Provision"
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Width = AMOUNT_COL_WIDTH
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Editable = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW)
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).EditionControl.EntryField.IsReadOnly = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)
      .Column(GRID_LEVELS_COLUMN_PROVISION_AMOUNT).HighLightWhenSelected = False

      ' Difference between theoretical and provision
      .Column(GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610) ' 2610 "Difference"
      .Column(GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610) ' 2610 "Difference"
      .Column(GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).HighLightWhenSelected = False

      ' %
      .Column(GRID_LEVELS_COLUMN_PCTJ).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610) ' 2610 "Difference"
      .Column(GRID_LEVELS_COLUMN_PCTJ).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042) ' 5042 "%"
      .Column(GRID_LEVELS_COLUMN_PCTJ).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_LEVELS_COLUMN_PCTJ).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_LEVELS_COLUMN_PCTJ).HighLightWhenSelected = False
    End With

    ' Terminals Grid
    With Me.dg_terminals

      Call .Init(GRID_TERMINALS_COLUMNS, GRID_TERMINALS_HEADER_ROWS, True)

      ' Grid Columns

      ' Terminal ID
      .Column(GRID_TERMINALS_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097) ' 1097 "Terminal"
      .Column(GRID_TERMINALS_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_TERMINALS_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TERMINALS_COLUMN_TERMINAL_ID).HighLightWhenSelected = False

      ' Provider
      .Column(GRID_TERMINALS_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469) ' 469 "Provider"
      .Column(GRID_TERMINALS_COLUMN_PROVIDER).Width = 2200
      .Column(GRID_TERMINALS_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_TERMINALS_COLUMN_PROVIDER).HighLightWhenSelected = False

      ' Terminal
      .Column(GRID_TERMINALS_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097) ' 1097 "Terminal"
      .Column(GRID_TERMINALS_COLUMN_TERMINAL).Width = 4000
      .Column(GRID_TERMINALS_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_TERMINALS_COLUMN_TERMINAL).HighLightWhenSelected = False

      ' Played Amount
      .Column(GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) ' 537 "Played"
      .Column(GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).HighLightWhenSelected = False

      ' As Played Amount
      .Column(GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5248) ' 5248 "Amount as Played"
      ' Version 2 changes: Hide as played column
      .Column(GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).Width = 0 'IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).HighLightWhenSelected = False

      ' Equally Amount
      .Column(GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5250) ' 5250 "By machine"
      .Column(GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).Width = 0 'IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).HighLightWhenSelected = False

      ' Provision Amount
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5245) ' 5245 "Provision"
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Width = AMOUNT_COL_WIDTH
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Editable = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW)
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).EditionControl.EntryField.IsReadOnly = (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)
      .Column(GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).HighLightWhenSelected = False

      ' %
      .Column(GRID_TERMINALS_COLUMN_PCTJ).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042) ' 5042 "%"
      .Column(GRID_TERMINALS_COLUMN_PCTJ).Width = IIf((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW), AMOUNT_COL_WIDTH, 0)
      .Column(GRID_TERMINALS_COLUMN_PCTJ).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TERMINALS_COLUMN_PCTJ).HighLightWhenSelected = False
    End With

  End Sub ' GUI_StyleView

  ' PURPOSE: Copies amounts from Diff. column to Provision column
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub Levels_CopyCol2Prov(ByVal Col As Integer)
    Dim _idx As Integer

    For _idx = 0 To Me.dg_levels.NumRows - 1
      Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = Me.dg_levels.Cell(_idx, Col).Value
      Call CalcCurrentReadAndDiff(_idx)
    Next

    Call UpdateLevelProvisionTotal()

  End Sub ' Levels_CopyCol2Prov

  ' PURPOSE: Updates provision total amount from levels provision amount
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub UpdateLevelProvisionTotal()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _idx_row As Integer
    Dim _total As Double
    Dim _diff_total As Double
    Dim _theoretical_total As Double
    Dim _reset_total As Boolean

    _progressive_provision = Me.DbEditedObject

    _total = 0
    _diff_total = 0
    _reset_total = False

    Try
      For _idx_row = 0 To _progressive_provision.Levels.Rows.Count - 1
        ' Check all levels are informed
        If Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = String.Empty Then
          _total = 0
          _diff_total = 0
          _reset_total = True
        End If

        If Not _reset_total Then
          _total += GUI_ParseCurrency(Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value)
          _diff_total += GUI_ParseCurrency(Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Value)
        End If
      Next

      Me.ef_cage_provision_amount.Value = String.Empty
      If _total <> 0 Then
        Me.ef_cage_provision_amount.Value = GUI_FormatCurrency(_total)
      End If

      If _progressive_provision.Levels.Rows.Count > 1 Then
        If _total = 0 Then
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = String.Empty
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Value = String.Empty
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PCTJ).Value = String.Empty
        Else
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = GUI_FormatCurrency(_total)
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Value = GUI_FormatCurrency(_diff_total)

          _theoretical_total = GUI_ParseCurrency(Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Value)

          If _theoretical_total > 0 Then
            Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PCTJ).Value = GUI_FormatNumber((_diff_total / _theoretical_total) * 100, 2)
          Else
            Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PCTJ).Value = STR_INFINITE
          End If
        End If
      End If

      m_total_levels_amount = Math.Round(_total, 2)

      Call UpdateTerminalsGrid()

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' UpdateLevelProvisionTotal

  ' PURPOSE: Calculates levels provision equally for each level
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub CalcLevelProvisions()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _total_provision As Double
    Dim _level_provision As Double
    Dim _num_levels As Integer
    Dim _level_contribution As Double
    Dim _remaining_ratio As Double

    Try
      _progressive_provision = Me.DbEditedObject

      _total_provision = Me.dg_levels.Cell(Me.dg_levels.NumRows - 1, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value
      _num_levels = _progressive_provision.Levels.Rows.Count

      _remaining_ratio = 100

      For Each _dr As DataRow In _progressive_provision.Levels.Rows
        _level_contribution = _dr("CONTRIBUTION_PCT")
        _level_provision = Math.Round(_total_provision * (_level_contribution / _remaining_ratio), 2)
        _dr("PROVISION_AMOUNT") = _level_provision
        _remaining_ratio = _remaining_ratio - _level_contribution
        _total_provision = _total_provision - _level_provision
      Next

      Call LoadLevelsGrid(_progressive_provision)

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub ' CalcLevelProvisions

  ' PURPOSE: Updates increment column from levels grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub UpdateLevelDeltaColumn()
    Dim _idx As Integer
    Dim _to_idx As Integer
    Dim _prev_amount As Double
    Dim _new_amount As Double
    Dim _total_amount As Double
    Dim _difference As Double
    Dim _total_difference As Double

    Try
      _total_amount = 0
      _total_difference = 0

      _to_idx = Me.dg_levels.NumRows - 2
      If Me.dg_levels.NumRows = 1 Then
        _to_idx = 0
      End If

      For _idx = 0 To _to_idx
        If Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Value <> String.Empty Then
          _prev_amount = Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Value

          _new_amount = Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Value
          _total_amount += _new_amount

          _difference = _new_amount - _prev_amount
          Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_DELTA_AMOUNT).Value = GUI_FormatCurrency(_difference)
          _total_difference += _difference
        Else

          Return
        End If
      Next

      If Me.dg_levels.NumRows > 1 Then
        Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Value = GUI_FormatCurrency(_total_amount)
        Me.dg_levels.Cell(_idx, GRID_LEVELS_COLUMN_DELTA_AMOUNT).Value = GUI_FormatCurrency(_total_difference)
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub ' UpdateLevelDeltaColumn

  ' PURPOSE: Updates provision total amount from terminals provision amount
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub UpdateTerminalsProvisionTotal()
    Dim _dr As DataRow
    Dim _value As Double
    Dim _total As Double
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _fore_color As System.Drawing.Color

    _progressive_provision = Me.DbEditedObject

    _fore_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    _total = 0

    For Each _dr In _progressive_provision.Terminals.Rows
      _value = _dr("PROVISION_AMOUNT")
      _total += _value
    Next

    m_total_terminals_provision = _total

    If m_total_terminals_provision > 0 Then
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value = GUI_FormatCurrency(m_total_terminals_provision)

      If m_total_played = 0 Then
        Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PCTJ).Value = STR_INFINITE
      Else
        Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PCTJ).Value = GUI_FormatNumber(m_total_terminals_provision / m_total_played * 100, 4)
      End If

      If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW And Math.Round(_total, 2) <> m_total_levels_amount Then
        _fore_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      End If

      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).ForeColor = _fore_color

      ' JMM 29-OCT-2014: Chapuza para evitar que al clicar se pinte la l�nea de totales de blanco
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_TERMINAL_ID).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PROVIDER).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_TERMINAL).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

    Me.lbl_progressive_info_021.Text = GUI_FormatCurrency(m_total_terminals_provision)
    If m_total_played > 0 Then
      Me.lbl_progressive_info_022.Text = "(" & GUI_FormatNumber(m_total_terminals_provision / m_total_played * 100, 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE) & "%)"
    Else
      Me.lbl_progressive_info_022.Text = String.Empty
    End If

  End Sub ' UpdateTerminalsProvisionTotal

  ' PURPOSE: Adjust level grid height depending on how many rows currently shows
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub AdjustLevelsGridHeight()
    Me.dg_levels.Height = (Math.Min(MAX_LEVELS_ROWS + 1, Math.Max(Me.dg_levels.NumRows, 1)) * 16) + 36

    Me.gb_levels.Height = Me.dg_levels.Height + 30

    Me.gb_machine_provisions.Top = Me.gb_levels.Bottom + 5

    Me.dg_terminals.Height = (Math.Min(MAX_TERMINALS_ROWS + 1, Math.Max(Me.dg_terminals.NumRows, 1)) * 16) + 20
    Me.gb_machine_provisions.Height = Me.dg_terminals.Height + 30
    Me.Height = Me.gb_date.Height + Me.gb_levels.Height + Me.gb_machine_provisions.Height + 50
  End Sub

  ' PURPOSE: Loads levels grid
  '
  '  PARAMS:
  '     - INPUT:
  '         - ProgressiveProvision
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub LoadLevelsGrid(ByVal ProgressiveProvision As CLASS_PROGRESSIVE_PROVISION)
    Dim _idx_row As Integer
    Dim _theoretical_amount As Double
    Dim _total_theoretical_amount As Double
    Dim _contribution As Double
    Dim _total_contribution As Double
    Dim _level_amount As Double
    Dim _progressive_amount As Double
    Dim _provision_amount As Double
    Dim _total_provision_amount As Double

    Try
      Me.dg_levels.Clear()

      _total_contribution = 0
      _progressive_amount = 0
      _total_theoretical_amount = 0
      _total_provision_amount = 0

      For Each _dr As DataRow In ProgressiveProvision.Levels.Rows
        _idx_row = Me.dg_levels.AddRow()

        ' Level ID
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_LEVEL_ID).Value = _dr.Item("LEVEL_ID")

        ' Level Name
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_NAME_LEVEL).Value = _dr.Item("LEVEL_NAME")

        ' Level Contribution %
        _contribution = _dr.Item("CONTRIBUTION_PCT")
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).Value = GUI_FormatNumber(_contribution)
        _total_contribution += _contribution

        ' Level Amount
        _level_amount = _dr.Item("LEVEL_AMOUNT")
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Value = GUI_FormatCurrency(_level_amount)
        _progressive_amount += _level_amount

        ' Theoretical Amount
        _theoretical_amount = 0
        If Not _dr.IsNull("THEORETICAL_AMOUNT") Then
          _theoretical_amount = _dr.Item("THEORETICAL_AMOUNT")
        End If

        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Value = GUI_FormatCurrency(_theoretical_amount)
        _total_theoretical_amount += _theoretical_amount

        ' Provision Amount
        If _dr.Item("PROVISION_AMOUNT") <> 0 Or Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
          _provision_amount = _dr.Item("PROVISION_AMOUNT")
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = GUI_FormatCurrency(_provision_amount)
          _total_provision_amount += _provision_amount

          Call CalcCurrentReadAndDiff(_idx_row)
        End If
      Next

      If ProgressiveProvision.Levels.Rows.Count > 1 Then
        _idx_row = Me.dg_levels.AddRow()
        Me.dg_levels.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_NAME_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_NAME_LEVEL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5297) '5297 "Total:"

        ' Level Contribution %
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_CONTRIBUTION_PCT).Value = GUI_FormatNumber(_total_contribution)

        ' Progressive Amount
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Value = GUI_FormatCurrency(_progressive_amount)

        ' Theoretical Amount
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Value = GUI_FormatCurrency(_total_theoretical_amount)

        ' Provision Amount
        If _total_provision_amount > 0 Then
          Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value = GUI_FormatCurrency(_total_provision_amount)
        End If
        Call CalcCurrentReadAndDiff(_idx_row)
      End If

      ''''''If Me.dg_levels.NumRows > MAX_LEVELS_ROWS + 1 Then
      ''''''  Me.dg_levels.Column(GRID_LEVELS_COLUMN_NAME_LEVEL).Width = 1550
      ''''''Else
      Me.dg_levels.Column(GRID_LEVELS_COLUMN_NAME_LEVEL).Width = 1775
      ''''''End If

      m_theoretical_amount = _total_theoretical_amount

      Call UpdateLevelProvisionTotal()

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' LoadLevelsGrid

  ' PURPOSE: Loads terminals grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub LoadTerminalsGrid()
    Dim _idx_row As Integer
    Dim _played As Double
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _as_played As Double
    Dim _total_as_played As Double
    Dim _provision_amount As Double
    Dim _total_provision_amount As Double

    Try
      _progressive_provision = Me.DbEditedObject

      Call Me.dg_terminals.Clear()
      Me.gb_machine_provisions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5247)

      m_total_played = 0
      _total_as_played = 0
      _total_provision_amount = 0

      For Each _dr As DataRow In _progressive_provision.Terminals.Rows
        _idx_row = Me.dg_terminals.AddRow()

        Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_TERMINAL_ID).Value = _dr("TERMINAL_ID")
        Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PROVIDER).Value = _dr("PROVIDER_NAME")
        Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_TERMINAL).Value = _dr("TERMINAL_NAME")

        If Not IsDBNull(_dr("TERMINAL_PLAYED_AMOUNT")) Then
          _played = _dr("TERMINAL_PLAYED_AMOUNT")
          m_total_played += _played
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_played)
        End If

        If m_total_levels_amount > 0 Or Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
          _as_played = _dr("AS_PLAYED_AMOUNT")
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_as_played)
          _total_as_played += _as_played

          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).Value = GUI_FormatCurrency(_dr("EQUALLY_AMOUNT"))

          If _dr("PROVISION_AMOUNT") > 0 Or Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
            _provision_amount = _dr("PROVISION_AMOUNT")
            Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value = GUI_FormatCurrency(_provision_amount)
            _total_provision_amount += _provision_amount

            If Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = 0 Then
              Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = STR_INFINITE
            Else
              Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = GUI_FormatNumber((_provision_amount / Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value) * 100, 4)
            End If
          Else
            Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value = String.Empty
            Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = String.Empty
          End If
        End If
      Next

      _idx_row = Me.dg_terminals.AddRow()
      Me.dg_terminals.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_TERMINAL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5297) '5297 "Total:"
      Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(m_total_played)
      Me.lbl_progressive_info_021.Text = GUI_FormatCurrency(m_total_played)

      If _total_as_played > 0 Then
        Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_total_as_played)
      End If

      If _total_provision_amount > 0 Then
        Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value = GUI_FormatCurrency(_total_provision_amount)

        If Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = 0 Then
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = STR_INFINITE
        Else
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = GUI_FormatNumber((_total_provision_amount / Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value) * 100, 4)
        End If
      End If

      '5247 "Provision to terminals"
      Me.gb_machine_provisions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5247) & ": " & GUI_FormatNumber(_progressive_provision.Terminals.Rows.Count, 0)

      'Disable As Played button when total played = 0
      Me.btn_as_played.Enabled = (m_total_played > 0)

      If Me.dg_terminals.NumRows > MAX_TERMINALS_ROWS + 1 Then
        Me.dg_terminals.Column(GRID_TERMINALS_COLUMN_TERMINAL).Width = 3775
      Else
        Me.dg_terminals.Column(GRID_TERMINALS_COLUMN_TERMINAL).Width = 4000
      End If

      Call UpdateTerminalsProvisionTotal()
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  ' PURPOSE: Updates terminals grid amounts
  '
  '  PARAMS:
  '     - INPUT:
  '         - ProvisionTotal
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub UpdateTerminalsGrid()
    Dim _idx_row As Integer
    Dim _updated As Boolean
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    _progressive_provision = Me.DbEditedObject

    _updated = False

    Try
      If m_total_levels_amount > 0 And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then

        If Not Progressives.ProvisionDistribute(m_total_levels_amount, _
                                                _progressive_provision.ProgressiveId, _
                                                Me.dg_levels.Cell(Me.dg_levels.NumRows - 1, GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Value, _
                                                _progressive_provision.Terminals, _
                                                _progressive_provision.TotalTerminalsPlayed) Then

          Return
        End If

        _updated = True
      End If

      Call LoadTerminalsGrid()
    Catch _ex As Exception
      Log.Exception(_ex)
    Finally

      If Not _updated And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
        For _idx_row = 0 To Me.dg_terminals.NumRows - 1
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT).Value = String.Empty
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT).Value = String.Empty
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value = String.Empty
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = String.Empty
        Next
      End If

    End Try

  End Sub

  ' PURPOSE: Updates provision total amount from levels provision amount
  '
  '  PARAMS:
  '     - INPUT:
  '         - SourceColumn
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub Terminals_CopyColumnToProvision(ByVal SourceColumn As Integer)
    Dim _error As Boolean
    Dim _dr As DataRow
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    _progressive_provision = Me.DbEditedObject

    Try
      For Each _dr In _progressive_provision.Terminals.Rows
        Select Case SourceColumn
          Case GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT
            _dr("PROVISION_AMOUNT") = _dr("AS_PLAYED_AMOUNT")
          Case GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT
            _dr("PROVISION_AMOUNT") = _dr("EQUALLY_AMOUNT")
          Case Else
            _error = True

            Return
        End Select
      Next

      Call LoadTerminalsGrid()
      Call UpdateTerminalsProvisionTotal()

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub ' Terminals_CopyColumnToProvision

  ' PURPOSE: Checks the level totals equals terminals totals
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Private Function CheckTotals() As Boolean
    Dim _grid_value As String

    _grid_value = Me.dg_levels.Cell(Me.dg_levels.NumRows - 1, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value

    Return GUI_ParseCurrency(_grid_value) = Math.Round(m_total_terminals_provision, 2)
  End Function

  ' PURPOSE: Checks the provision period dates
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Private Function CheckValidDates() As Boolean

    'Check introduced end date isn't smaller than the start date
    If Me.dtp_to.Value <= Me.dtp_from.Value Then
      '5283 "Date range error: start date must be before end date."
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5283), _
                 ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Me.dtp_to.Value = m_max_hour_to

      Return False
    End If

    'Check introduced end date isn't bigger than the max to hour (current time at 00 minutes)
    If Me.dtp_to.Value > m_max_hour_to Then
      '5240 "End date can't be later than %1"
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5240), _
                 ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , _
                 GUI_FormatDate(m_max_hour_to, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))

      Me.dtp_to.Value = m_max_hour_to

      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Updates theoretical amounts from levels & terminals grids
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateTheoreticalAmounts()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _idx_row As Int32
    Dim _row As DataRow
    Dim _level_id As String
    Dim _terminal_id As String
    Dim _provision_amount As Double
    Dim _total_provision_amount As Double
    Dim _theoretical_amount As Double
    Dim _total_theoretical_amount As Double
    Dim _played_amount As Double
    Dim _total_played_amount As Double

    _progressive_provision = DbEditedObject

    ' Levels grid
    _total_theoretical_amount = 0

    For _idx_row = 0 To Me.dg_levels.NumRows - 1
      _level_id = Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_LEVEL_ID).Value

      If _level_id <> String.Empty Then
        _row = _progressive_provision.Levels.Select("LEVEL_ID = " + _level_id)(0)

        _theoretical_amount = _row("THEORETICAL_AMOUNT")
        Me.dg_levels.Cell(_idx_row, GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Value = GUI_FormatCurrency(_theoretical_amount)
        _total_theoretical_amount += _theoretical_amount

        Call CalcCurrentReadAndDiff(_idx_row)
      End If
    Next

    ' Levels grid total row
    If Me.dg_levels.NumRows > 1 Then
      Me.dg_levels.Cell(Me.dg_levels.NumRows - 1, GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Value = GUI_FormatCurrency(_total_theoretical_amount)
      Call CalcCurrentReadAndDiff(Me.dg_levels.NumRows - 1)
    End If

    ' Terminals grid
    _total_played_amount = 0
    _total_provision_amount = 0

    For _idx_row = 0 To Me.dg_terminals.NumRows - 1
      _terminal_id = Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_TERMINAL_ID).Value

      If _terminal_id <> String.Empty Then
        _row = _progressive_provision.Terminals.Select("TERMINAL_ID = " + _terminal_id)(0)

        _played_amount = _row("TERMINAL_PLAYED_AMOUNT")
        _total_played_amount += _played_amount

        _provision_amount = GUI_ParseCurrency(Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value)
        _total_provision_amount += _provision_amount

        Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_played_amount)

        If _played_amount = 0 Then
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = STR_INFINITE
        Else
          Me.dg_terminals.Cell(_idx_row, GRID_TERMINALS_COLUMN_PCTJ).Value = GUI_FormatNumber((_provision_amount / _played_amount) * 100, 4)
        End If
      End If
    Next

    ' Terminals grid total row
    If Me.dg_terminals.NumRows > 1 Then
      Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_total_played_amount)

      If _total_played_amount = 0 Then
        Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PCTJ).Value = STR_INFINITE
      Else
        Me.dg_terminals.Cell(Me.dg_terminals.NumRows - 1, GRID_TERMINALS_COLUMN_PCTJ).Value = GUI_FormatNumber((_provision_amount / _total_played_amount) * 100, 4)
      End If

    End If
  End Sub 'UpdateTheoreticalAmounts

  ' PURPOSE: Loads the provision levels
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  Private Sub LoadAmounts()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      _progressive_provision = DbEditedObject

      If Not _progressive_provision Is Nothing Then
        If Not Progressives.GetProgressiveLevels(_progressive_provision.ProgressiveId, _
                                                 _progressive_provision.HourFrom, _
                                                 _progressive_provision.HourTo, _
                                                 _progressive_provision.Levels) Then

          Return
        End If

        If Not Progressives.GetPlayedFromProgressiveTerminals(_progressive_provision.ProgressiveId, _
                                                              _progressive_provision.HourFrom, _
                                                              _progressive_provision.HourTo, _
                                                              _progressive_provision.Terminals) Then

          Return
        End If

        If Progressives.GetProvisionInputMode = Progressives.PROGRESSIVE_PROVISION_INPUT_MODE.COUNTER_READING _
            And Progressives.GetProvisionDistributionMode = Progressives.PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.EQUALLY Then
          ' Counter reading & equally distribution, keep the amounts input by user and reload only theorically and played amounts
          Call UpdateTheoreticalAmounts()
        Else
          ' In any other mode, reload all the data
          Call GUI_SetScreenData(0)
        End If
      End If

    Catch _ex As Exception
      Log.Exception(_ex)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try
  End Sub ' LoadLevels

  ' PURPOSE: Calculates current read column & diff column
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub CalcCurrentReadAndDiff(ByVal Row As Int32)
    Dim _provision_amount As Double
    Dim _previous_amount As Double
    Dim _theoretical_amount As Double

    If Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value <> String.Empty _
    And Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Value <> String.Empty Then
      _provision_amount = Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_PROVISION_AMOUNT).Value
      _previous_amount = Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_PREVIOUS_AMOUNT).Value
      _theoretical_amount = Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT).Value

      Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT).Value = GUI_FormatCurrency(_provision_amount + _previous_amount)
      Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_DELTA_AMOUNT).Value = GUI_FormatCurrency(_provision_amount)

      Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_DIFF_BETWEEN_THEO_AND_PROV).Value = GUI_FormatCurrency(_provision_amount - _theoretical_amount)

      If _provision_amount = 0 Or _theoretical_amount = 0 Then
        Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_PCTJ).Value = STR_INFINITE
      Else
        Me.dg_levels.Cell(Row, GRID_LEVELS_COLUMN_PCTJ).Value = GUI_FormatNumber(((_provision_amount - _theoretical_amount) / _theoretical_amount) * 100, 2)
      End If
    End If
  End Sub ' CalcCurrentReadAndDiff

  ' PURPOSE: Loads the combo with the currently enabled progressives
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub LoadComboProgressives()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _sb As StringBuilder

    Try

      _progressive_provision = Me.DbEditedObject

      _sb = New StringBuilder

      m_dt_progressives = New DataTable

      _sb.AppendLine("  SELECT   PGS_PROGRESSIVE_ID                                             ")
      _sb.AppendLine("         , PGS_NAME                                                       ")
      _sb.AppendLine("    FROM   PROGRESSIVES                                                   ")
      _sb.AppendLine("   WHERE   PGS_STATUS = @pStatus                                          ")
      _sb.AppendLine("     AND   (    dateadd(MINUTE, 75, PGS_LAST_PROVISIONED_HOUR) < GetDate()")
      _sb.AppendLine("             OR PGS_LAST_PROVISIONED_HOUR IS NULL )                       ")
      _sb.AppendLine("ORDER BY   PGS_NAME                                                       ")

      Using _db_trx As DB_TRX = New DB_TRX
        Using _cmd_sql As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _cmd_sql.Parameters.Add("@pStatus", SqlDbType.Int).Value = Progressives.PROGRESSIVE_STATUS.ACTIVE

          Using _da As New SqlClient.SqlDataAdapter(_cmd_sql)
            _da.Fill(m_dt_progressives)
          End Using
        End Using
      End Using

      Me.cmb_progressive.Clear()
      Me.cmb_progressive.Add(m_dt_progressives)

      If _progressive_provision.ProgressiveId <> 0 Then
        Me.cmb_progressive.Value = _progressive_provision.ProgressiveId
      Else
        Me.cmb_progressive.SelectedIndex = -1
      End If

      Call EnableButtons(Me.cmb_progressive.SelectedIndex <> -1)

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub         ' LoadComboAccounts

  ' PURPOSE: Loads the progressive data to insert a provision instance
  '          
  ' PARAMS:
  '    - INPUT:
  '         -   ProgressiveId
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        Boolean
  Public Function GetProgressiveData(ByVal ProgressiveId As Int64) As Boolean
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _progressive As CLASS_PROGRESSIVE
    Dim _levels As DataTable
    Dim _terminals As DataTable
    Dim _hour_from As DateTime
    Dim _hour_to As DateTime
    Dim _error As Boolean

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    If ProgressiveId <> 0 Then

      Try
        _progressive_provision = DbEditedObject
        _levels = Nothing
        _terminals = Nothing
        _error = False

        Me.m_input_mode = Progressives.GetProvisionInputMode()
        Me.m_distribution_mode = Progressives.GetProvisionDistributionMode()
        Me.m_provision_to_cage = Progressives.ProvisionGoesToCage()

        Using _db_trx As DB_TRX = New DB_TRX
          _progressive_provision = Me.DbEditedObject
          _progressive_provision.ProgressiveId = ProgressiveId

          ' Progressive Name
          _progressive = New CLASS_PROGRESSIVE

          If _progressive.DB_Read(_progressive_provision.ProgressiveId, 0) <> ENUM_STATUS.STATUS_OK Then
            Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Return False
          End If

          _progressive_provision.ProgressiveName = _progressive.ProgressiveName
          _progressive_provision.ProgressiveLastProvisionedHour = _progressive.ProgressiveLastProvisionedHour
          m_progressive_contribution = _progressive.ProgressiveContributionPct

          ' Default period: from last period end to last o'clock hour
          If Not Progressives.GetDefaultPeriod(_progressive_provision.ProgressiveId, _hour_from, _hour_to, m_max_hour_to, _db_trx.SqlTransaction) Then
            Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Return False
          End If

          ' Period < 1h long.  Not allowed to provioning this progressive until next o'clock
          If _hour_from >= m_max_hour_to Then
            '5342 "Progressive jackpot %1 has been provisioned up to %2.\nYou cannot perform a new provision for this progressive jackpot until after 60 minutes from the end of the last provisioned period."
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5342), _
                            mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                            , _
                            _progressive_provision.ProgressiveName, _
                            GUI_FormatDate(_hour_from, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) + " " + GUI_FormatTime(_hour_from, ENUM_FORMAT_TIME.FORMAT_HHMM))

            Return False
          End If

          ' Read levels related to the progressive
          If Not Progressives.GetProgressiveLevels(_progressive_provision.ProgressiveId, _
                                                   _hour_from, _
                                                   _hour_to, _
                                                   _levels, _
                                                   _db_trx.SqlTransaction) Then
            Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Return False
          End If

          ' Terminals
          _terminals = New DataTable

          If Not Progressives.GetPlayedFromProgressiveTerminals(_progressive_provision.ProgressiveId, _
                                                                _hour_from, _
                                                                _hour_to, _
                                                                _terminals, _
                                                                _db_trx.SqlTransaction) Then
            Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Return False
          End If
        End Using

        '5348 "Unable to provision the progressive jackpot %1 due it has not any related level."
        If _levels.Rows.Count < 1 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5348), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Return False
        End If

        m_provision_max_allowed = WSI.Common.GeneralParam.GetInt32("Progressives", "Provision.MaxAllowedPerDay", 0)

        If m_provision_max_allowed = 1 Then
          _progressive_provision.HourFrom = New Date(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, GetDefaultClosingTime(), 0, 0)
          _progressive_provision.HourTo = _progressive_provision.HourFrom.AddDays(1)
        Else
          ' Hour From
          _progressive_provision.HourFrom = _hour_from
          ' Hour To
          _progressive_provision.HourTo = _hour_to
          If Not m_default_hour_to Is Nothing Then
            If m_default_hour_to <= m_max_hour_to And m_default_hour_to > _hour_from Then
              _progressive_provision.HourTo = m_default_hour_to
            End If
          End If
        End If

        ' Levels
        _progressive_provision.Levels = _levels

        ' Terminals
        _progressive_provision.Terminals = _terminals

        ' Cage Provision
        _progressive_provision.TotalCageProvision = 0

        Return True

      Finally
        Windows.Forms.Cursor.Current = Cursors.Default
      End Try
    End If

  End Function

  ' PURPOSE: Revokes a provision
  '          
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub CancelProvision()
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _errors_on_terminals As List(Of String)

    Try
      _progressive_provision = Me.DbReadObject
      _errors_on_terminals = New List(Of String)

      '5649 "Provision will be canceled.\n\nDo you want to continue?"
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5649), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Return
      End If

      Using _db_trx As New DB_TRX()
        If Not Progressives.CancelProvision(_progressive_provision.GetCommonProgressiveProvision, _db_trx.SqlTransaction, _errors_on_terminals) Then
          ' 5533 "An error occurred while canceling provision."
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5533), ENUM_MB_TYPE.MB_TYPE_ERROR)

          'An error occurred generating provision: rollback
          _db_trx.Rollback()

          Return
        End If

        _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PROGRESSIVE_PROVISION)

        _auditor_data = _progressive_provision.AuditorData()

        Call _auditor_data.SetName(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5573)) '5573 "Provision cancellation"

        If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                           GLB_CurrentUser.Id, _
                           GLB_CurrentUser.Name, _
                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                           0) Then
          'An error occurred on notifying: rollback
          _db_trx.Rollback()

          Return
        End If

        _db_trx.Commit()

        ' 5532 "Provision successfully canceled."
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5532), ENUM_MB_TYPE.MB_TYPE_INFO)

        Me.Close()
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  ' PURPOSE: Enables/Disables form buttons
  '          
  ' PARAMS:
  '    - INPUT:
  '         - Enabled
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '        None
  Private Sub EnableButtons(ByVal Enabled As Boolean)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Enabled
    Me.btn_incr_2_prov.Enabled = Enabled
    Me.btn_theo_2_prov.Enabled = Enabled
    Me.btn_as_played.Enabled = Enabled
    Me.btn_equally.Enabled = Enabled
  End Sub ' EnableButtons

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal ProvisionID As Int64, ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    _progressive_provision = Me.DbEditedObject
    _progressive_provision.ProvisionId = ProvisionID

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowForEdit

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal ProgressiveId As Int64)
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If Not Me.GetProgressiveData(ProgressiveId) Then
      Me.DbStatus = ENUM_STATUS.STATUS_ERROR

      Return
    End If

    m_insert_provision = False
    m_default_hour_to = Nothing

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem


  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    m_insert_provision = False
    m_default_hour_to = Nothing

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

#End Region ' Public Functions

#Region " Events "

  Private Sub dtp_from_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtp_from.Validating
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    _progressive_provision = DbEditedObject

    If Progressives.ExistsProvisionOnDate(_progressive_provision.ProgressiveId, Me.dtp_from.Value) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5871), ENUM_MB_TYPE.MB_TYPE_INFO, , , Me.dtp_from.Value.ToShortDateString())

      Me.dtp_from.Value = _progressive_provision.HourFrom
    Else
      dtp_to.Value = dtp_from.Value.AddDays(1)

      If Not _progressive_provision Is Nothing Then
        _progressive_provision.HourTo = Me.dtp_to.Value
        _progressive_provision.HourFrom = Me.dtp_from.Value

        Call LoadAmounts()
      End If
    End If
  End Sub

  Private Sub dtp_to_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtp_to.Validating
    Dim _time_span As TimeSpan
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    If Not _m_is_equal Then
      'Dismiss time part but hour
      _time_span = Me.dtp_to.Value.TimeOfDay
      Me.dtp_to.Value = Me.dtp_to.Value.Subtract(_time_span)
      Me.dtp_to.Value = Me.dtp_to.Value.AddHours(_time_span.Hours)

      Call CheckValidDates()

      _progressive_provision = DbEditedObject

      If Not _progressive_provision Is Nothing Then
        _progressive_provision.HourTo = Me.dtp_to.Value

        Call LoadAmounts()
      End If
    End If
  End Sub

  Private Sub btn_incr_2_prov_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_incr_2_prov.Click
    Call Levels_CopyCol2Prov(GRID_LEVELS_COLUMN_DELTA_AMOUNT)
  End Sub

  Private Sub btn_theo_2_prov_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_theo_2_prov.Click
    Call Levels_CopyCol2Prov(GRID_LEVELS_COLUMN_THEORETICAL_AMOUNT)
  End Sub

  Private Sub dg_levels_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_levels.CellDataChangedEvent
    Static Dim _editing = False

    If Not _editing Then
      _editing = True

      Select Case Column
        Case GRID_LEVELS_COLUMN_CURRENT_READ_AMOUNT ' Current read column changed
          Call UpdateLevelDeltaColumn()
          Call Levels_CopyCol2Prov(GRID_LEVELS_COLUMN_DELTA_AMOUNT)

        Case GRID_LEVELS_COLUMN_PROVISION_AMOUNT ' Provision column changed
          ' If total row is edited, calc the levels provision equally
          If Row = Me.dg_levels.NumRows - 1 Then
            Call CalcLevelProvisions()
          End If

          Call CalcCurrentReadAndDiff(Row)
          Call UpdateLevelProvisionTotal()
      End Select

      If Row < Me.dg_levels.NumRows - 2 Then
        Me.dg_levels.CurrentRow = Row + 1
      End If

      _editing = False
    End If
  End Sub

  Private Sub dg_levels_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_levels.BeforeStartEditionEvent
    If Row >= Me.dg_levels.NumRows - 1 _
    And Column <> GRID_LEVELS_COLUMN_PROVISION_AMOUNT _
    And Me.dg_levels.NumRows > 1 Then
      EditionCanceled = True
    End If
  End Sub

  Private Sub dg_terminals_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_terminals.CellDataChangedEvent
    Dim _terminal_id As Integer
    Dim _dr As DataRow
    Dim _progressive_provision As CLASS_PROGRESSIVE_PROVISION

    If m_total_levels_amount > 0 Then
      If Row < Me.dg_terminals.NumRows - 1 Then
        _progressive_provision = Me.DbEditedObject

        _terminal_id = Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_TERMINAL_ID).Value
        _dr = _progressive_provision.Terminals.Select("TERMINAL_ID = " + _terminal_id.ToString)(0)
        _dr("PROVISION_AMOUNT") = GUI_ParseCurrency(Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value)

        If Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value = 0 Then
          Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_PCTJ).Value = STR_INFINITE
        Else
          Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_PCTJ).Value = GUI_FormatNumber((Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_PROVISION_AMOUNT).Value / Me.dg_terminals.Cell(Row, GRID_TERMINALS_COLUMN_PLAYED_AMOUNT).Value) * 100, 4)
        End If

        ' If we aren't on the last row, move focus to the next row
        If Row < Me.dg_terminals.NumRows - 2 Then
          Me.dg_terminals.CurrentRow = Row + 1
        End If
      End If

      Call UpdateTerminalsProvisionTotal()
    End If
  End Sub

  Private Sub dg_terminals_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_terminals.BeforeStartEditionEvent
    ' Don't allow to edit the total row or if there is no levels provision
    If Row >= Me.dg_terminals.NumRows - 1 Or m_total_levels_amount = 0 Then
      EditionCanceled = True
    End If
  End Sub

  Private Sub btn_as_played_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_as_played.Click
    Call Terminals_CopyColumnToProvision(GRID_TERMINALS_COLUMN_AS_PLAYED_AMOUNT)
  End Sub

  Private Sub btn_equally_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_equally.Click
    Call Terminals_CopyColumnToProvision(GRID_TERMINALS_COLUMN_EQUALLY_AMOUNT)
  End Sub

  Private Sub cmb_progressive_ValueChangedEvent() Handles cmb_progressive.ValueChangedEvent
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW And m_initialized Then
      Me.dg_levels.Clear()
      Me.dg_terminals.Clear()

      Call EnableButtons(Me.cmb_progressive.Value <> 0)

      If Me.GetProgressiveData(Me.cmb_progressive.Value) Then
        Call Me.GUI_SetScreenData(0)
      End If
    End If
  End Sub

  Private Sub dg_levels_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_levels.Enter
    Me.CancelButton = Nothing
  End Sub

  Private Sub dg_levels_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_levels.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

  Private Sub dg_terminals_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals.Enter
    Me.CancelButton = Nothing
  End Sub

  Private Sub dg_terminals_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_terminals.Leave
    Me.CancelButton = GUI_Button(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

  Private Sub dtp_to_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_to.Leave
    If _m_last_date = dtp_to.Value Then
      _m_is_equal = True
    Else
      _m_is_equal = False
    End If
  End Sub ' dtp_to_Leave

  Private Sub dtp_to_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_to.Enter
    _m_last_date = dtp_to.Value
  End Sub ' dtp_to_Enter

#End Region ' Events

End Class