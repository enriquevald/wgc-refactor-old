'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_sessions_edit.vb
'
' DESCRIPTION:   Show full details for a cashier session 
'
' AUTHOR:        Sergi Mart�nez
'
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ---------  ---------------------------------------------------
' 16-DEC-2013  SMN        Initial Draft.

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Reports
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports WSI.Common

Public Class frm_gaming_tables_sessions_edit
  Inherits frm_base_edit

#Region "Members"
  Private m_load_complete As Boolean = False

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_SESSIONS_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub               ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.ef_client_visits.Text = "xClient Visits"
    Me.dtp_close_datetime.Text = "xxClose DateTime"
    Me.dtp_open_datetime.Text = "xOpen DateTime"
    Me.ef_collected_amount.Text = "xCollected Amount"
    Me.ef_initial_amount.Text = "xInitial Amount"
    Me.cmb_gaming_table.Text = "xGaming Table"
    Me.dtp_close_datetime.IsReadOnly = True
    Me.dtp_open_datetime.IsReadOnly = True

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.ef_client_visits.IsReadOnly = False
      Me.ef_collected_amount.IsReadOnly = False
      Me.ef_initial_amount.IsReadOnly = True
      Me.cmb_gaming_table.IsReadOnly = False
    Else
      Me.ef_client_visits.IsReadOnly = True
      Me.ef_collected_amount.IsReadOnly = True
      Me.ef_initial_amount.IsReadOnly = False
      Me.cmb_gaming_table.IsReadOnly = True

    End If

    ' Form
    Me.Text = "xGaming Sessions - Edition"

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)

    Call GamingTablesComboFill()

    'm_init_controls = True
    '''' Player Tracking
    'Call GUI_StyleSheet()
    'm_init_controls = False

  End Sub         ' GUI_InitControls

  Public Overloads Sub ShowEditItem(ByVal GtsUniqueId As Integer, ByVal GtsGamingName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = GtsUniqueId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _gaming_table_session As CLASS_GAMING_TABLES_SESSIONS
    Dim _idx As Integer

    m_load_complete = False
    _gaming_table_session = Me.DbEditedObject ' Me.DbReadObject

    ' GtsClientVisits
    ef_client_visits.Value = _gaming_table_session.GtsClientVisits

    ' GtsCloseDateTime
    If _gaming_table_session.GtsCloseDateTime = Date.MinValue Then
      dtp_close_datetime.Value = Nothing
    Else
      dtp_close_datetime.Value = _gaming_table_session.GtsCloseDateTime
    End If

    ' GtsCollectedAmount
    ef_collected_amount.Value = _gaming_table_session.GtsCollectedAmount

    ' GtsInitialAmount
    ef_initial_amount.Value = _gaming_table_session.GtsInitialAmount

    ' GtsOpenDateTime
    If _gaming_table_session.GtsOpenDateTime = Date.MinValue Then
      dtp_open_datetime.Value = Nothing
    Else
      dtp_open_datetime.Value = _gaming_table_session.GtsOpenDateTime
    End If

    ' GtsGamingTableId
    For _idx = 0 To cmb_gaming_table.Count - 1
      If cmb_gaming_table.Value(_idx) = _gaming_table_session.GtsGamingTableId Then
        cmb_gaming_table.SelectedIndex = _idx

        Exit For
      End If
    Next


    m_load_complete = True

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _gaming_table_session As CLASS_GAMING_TABLES_SESSIONS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_GAMING_TABLES_SESSIONS
        _gaming_table_session = DbEditedObject
        _gaming_table_session.GtsUniqueId = -1
        _gaming_table_session.GtsOpenDateTime = GUI_GetDateTime()
        _gaming_table_session.GtsInitialAmount = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)


    End Select ' DbOperation

  End Sub

#End Region ' Overrides

#Region " Private Functions "
  ' PURPOSE: Init credit type combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GamingTablesComboFill()

    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder()
    _sb.AppendLine(" SELECT     GT_UNIQUE_ID ")
    _sb.AppendLine("         ,  GT_NAME  ")
    _sb.AppendLine("   FROM     GAMING_TABLES ")
    _sb.AppendLine("  WHERE     GT_ENABLED = 1 ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Me.cmb_gaming_table.Add(_table)

  End Sub ' GamingTablesComboFill

#End Region ' Private Functions

End Class