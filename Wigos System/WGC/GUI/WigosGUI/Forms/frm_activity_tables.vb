﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_activity_tables.vb
'
' DESCRIPTION:   Shows Activity in Tables.
'
' AUTHOR:        Ervin Olvera
'
' CREATION DATE: 09-MAY-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-MAY-2016  EOR    Initial version
' 24-MAY-2016  EOR    Product Backlog Item 13672:Sprint Review 24 Winions - Mex
' --------------------------------------------------------------------


Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
#End Region ' Imports

Public Class frm_activity_tables
  Inherits frm_base_sel

#Region " Members"

  Private m_query_as_table As DataTable
  Private m_month_year As String
  Private m_month As String
  Private m_last_day_month As Integer

#End Region

#Region " Constants & Enums"
  ' SQL Columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_TABLE_TYPE As Integer = 1
  Private Const SQL_COLUMN_TABLE_NAME As Integer = 2
  Private Const SQL_COLUMN_MONTH As Integer = 3
  Private Const SQL_COLUMN_ENABLED_DAYS As Integer = 4
  Private Enum SQL_COLUMN_ENABLED As Integer
    DAY_1 = 5
    DAY_2 = 6
    DAY_3 = 7
    DAY_4 = 8
    DAY_5 = 9
    DAY_6 = 10
    DAY_7 = 11
    DAY_8 = 12
    DAY_9 = 13
    DAY_10 = 14
    DAY_11 = 15
    DAY_12 = 16
    DAY_13 = 17
    DAY_14 = 18
    DAY_15 = 19
    DAY_16 = 20
    DAY_17 = 21
    DAY_18 = 22
    DAY_19 = 23
    DAY_20 = 24
    DAY_21 = 25
    DAY_22 = 26
    DAY_23 = 27
    DAY_24 = 28
    DAY_25 = 29
    DAY_26 = 30
    DAY_27 = 31
    DAY_28 = 32
    DAY_29 = 33
    DAY_30 = 34
    DAY_31 = 35
  End Enum
  Private Const SQL_COLUMN_USE_DAYS As Integer = 36
  Private Enum SQL_COLUMN_USE As Integer
    DAY_1 = 37
    DAY_2 = 38
    DAY_3 = 39
    DAY_4 = 40
    DAY_5 = 41
    DAY_6 = 42
    DAY_7 = 43
    DAY_8 = 44
    DAY_9 = 45
    DAY_10 = 46
    DAY_11 = 47
    DAY_12 = 48
    DAY_13 = 49
    DAY_14 = 50
    DAY_15 = 51
    DAY_16 = 52
    DAY_17 = 53
    DAY_18 = 54
    DAY_19 = 55
    DAY_20 = 56
    DAY_21 = 57
    DAY_22 = 58
    DAY_23 = 59
    DAY_24 = 60
    DAY_25 = 61
    DAY_26 = 62
    DAY_27 = 63
    DAY_28 = 64
    DAY_29 = 65
    DAY_30 = 66
    DAY_31 = 67
  End Enum

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_TABLE_TYPE As Integer = 1
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 2
  Private Const GRID_COLUMN_MONTH As Integer = 3
  Private Const GRID_COLUMN_ENABLED_DAYS As Integer = 4
  Private Enum GRID_COLUMN_ENABLED As Integer
    DAY_1 = 5
    DAY_2 = 6
    DAY_3 = 7
    DAY_4 = 8
    DAY_5 = 9
    DAY_6 = 10
    DAY_7 = 11
    DAY_8 = 12
    DAY_9 = 13
    DAY_10 = 14
    DAY_11 = 15
    DAY_12 = 16
    DAY_13 = 17
    DAY_14 = 18
    DAY_15 = 19
    DAY_16 = 20
    DAY_17 = 21
    DAY_18 = 22
    DAY_19 = 23
    DAY_20 = 24
    DAY_21 = 25
    DAY_22 = 26
    DAY_23 = 27
    DAY_24 = 28
    DAY_25 = 29
    DAY_26 = 30
    DAY_27 = 31
    DAY_28 = 32
    DAY_29 = 33
    DAY_30 = 34
    DAY_31 = 35
  End Enum
  Private Const GRID_COLUMN_USE_DAYS As Integer = 36
  Private Enum GRID_COLUMN_USE As Integer
    DAY_1 = 37
    DAY_2 = 38
    DAY_3 = 39
    DAY_4 = 40
    DAY_5 = 41
    DAY_6 = 42
    DAY_7 = 43
    DAY_8 = 44
    DAY_9 = 45
    DAY_10 = 46
    DAY_11 = 47
    DAY_12 = 48
    DAY_13 = 49
    DAY_14 = 50
    DAY_15 = 51
    DAY_16 = 52
    DAY_17 = 53
    DAY_18 = 54
    DAY_19 = 55
    DAY_20 = 56
    DAY_21 = 57
    DAY_22 = 58
    DAY_23 = 59
    DAY_24 = 60
    DAY_25 = 61
    DAY_26 = 62
    DAY_27 = 63
    DAY_28 = 64
    DAY_29 = 65
    DAY_30 = 66
    DAY_31 = 67
  End Enum

  ' Width Columns
  Private Const GRID_WIDTH_ID As Integer = 200
  Private Const GRID_WIDTH_COLUMN_TABLE_TYPE As Integer = 1800
  Private Const GRID_WIDTH_COLUMN_TABLE_NAME As Integer = 2200
  Private Const GRID_WIDTH_MONTH_OR_DAYS As Integer = 800
  Private Const GRID_WIDTH_DAY As Integer = 800

  'Grid Setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 68

#End Region

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim GRID_COLUMN_DAY_ENABLED As Integer
    Dim GRID_COLUMN_DAY_USE As Integer
    Dim _counter As Integer

    With Me.Grid
      ' Initialize Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' GRID INDEX
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' TABLE TYPE
      .Column(GRID_COLUMN_TABLE_TYPE).Header(0).Text = " "
      .Column(GRID_COLUMN_TABLE_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3420)
      .Column(GRID_COLUMN_TABLE_TYPE).Width = GRID_WIDTH_COLUMN_TABLE_TYPE
      .Column(GRID_COLUMN_TABLE_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' TABLE NAME
      .Column(GRID_COLUMN_TABLE_NAME).Header(0).Text = "  "
      .Column(GRID_COLUMN_TABLE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)
      .Column(GRID_COLUMN_TABLE_NAME).Width = GRID_WIDTH_COLUMN_TABLE_NAME
      .Column(GRID_COLUMN_TABLE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' MONTH
      .Column(GRID_COLUMN_MONTH).Header(0).Text = "   "
      .Column(GRID_COLUMN_MONTH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7113)
      .Column(GRID_COLUMN_MONTH).Width = GRID_WIDTH_MONTH_OR_DAYS
      .Column(GRID_COLUMN_MONTH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' ENABLED DAYS
      .Column(GRID_COLUMN_ENABLED_DAYS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)
      .Column(GRID_COLUMN_ENABLED_DAYS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310)
      .Column(GRID_COLUMN_ENABLED_DAYS).Width = GRID_WIDTH_MONTH_OR_DAYS
      .Column(GRID_COLUMN_ENABLED_DAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' ENABLED BY DAY AND USE BY DAY
      For _counter = 1 To 31
        GRID_COLUMN_DAY_ENABLED = CInt([Enum].Parse(GetType(GRID_COLUMN_ENABLED), "DAY_" & _counter.ToString()))
        GRID_COLUMN_DAY_USE = CInt([Enum].Parse(GetType(GRID_COLUMN_USE), "DAY_" & _counter.ToString()))

        .Column(GRID_COLUMN_DAY_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)
        .Column(GRID_COLUMN_DAY_ENABLED).Header(1).Text = _counter.ToString().PadLeft(2, "0") & " " & m_month
        .Column(GRID_COLUMN_DAY_ENABLED).Width = GRID_WIDTH_DAY
        .Column(GRID_COLUMN_DAY_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

        .Column(GRID_COLUMN_DAY_USE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7309)
        .Column(GRID_COLUMN_DAY_USE).Header(1).Text = _counter.ToString().PadLeft(2, "0") & " " & m_month
        .Column(GRID_COLUMN_DAY_USE).Width = GRID_WIDTH_DAY
        .Column(GRID_COLUMN_DAY_USE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      Next

      ' USE DAYS
      .Column(GRID_COLUMN_USE_DAYS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7309)
      .Column(GRID_COLUMN_USE_DAYS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7310)
      .Column(GRID_COLUMN_USE_DAYS).Width = GRID_WIDTH_MONTH_OR_DAYS
      .Column(GRID_COLUMN_USE_DAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    End With
  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Me.dt_datetime.Value = WGDB.Now
  End Sub ' SetDefaultValues

  Private Sub CustomColumnsDaysHideGrid()
    Dim _counter As Integer
    Dim GRID_COLUMN_DAY_ENABLED As Integer
    Dim GRID_COLUMN_DAY_USE As Integer

    _counter = 31

    While (_counter > m_last_day_month)
      GRID_COLUMN_DAY_ENABLED = CInt([Enum].Parse(GetType(GRID_COLUMN_ENABLED), "DAY_" & _counter.ToString()))
      GRID_COLUMN_DAY_USE = CInt([Enum].Parse(GetType(GRID_COLUMN_USE), "DAY_" & _counter.ToString()))

      With Me.Grid
        .Column(GRID_COLUMN_DAY_ENABLED).Header.Text = String.Empty
        .Column(GRID_COLUMN_DAY_ENABLED).Width = 1
        .Column(GRID_COLUMN_DAY_ENABLED).IsColumnPrintable = False
        .Column(GRID_COLUMN_DAY_ENABLED).HighLightWhenSelected = False

        .Column(GRID_COLUMN_DAY_USE).Header.Text = String.Empty
        .Column(GRID_COLUMN_DAY_USE).Width = 1
        .Column(GRID_COLUMN_DAY_USE).IsColumnPrintable = False
        .Column(GRID_COLUMN_DAY_USE).HighLightWhenSelected = False
      End With

      _counter -= 1
    End While

  End Sub

  Private Sub SetRowTotalGrid()
    Dim _idx_row As Integer
    Dim _text_total As String
    Dim _total_days_enabled As Integer
    Dim _total_days_used As Integer
    Dim _total_month As Integer
    Dim _total_day As Integer
    Dim _counter As Integer
    Dim GRID_COLUMN_DAY_ENABLED As Integer
    Dim GRID_COLUMN_DAY_USE As Integer
    Dim _operation_sum As String

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1
    _text_total = GLB_NLS_GUI_INVOICING.GetString(205)

    'Label - TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TABLE_TYPE).Value = _text_total

    'Total Month
    _total_month = m_query_as_table.Compute("SUM(T_MONTH)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_MONTH).Value = _total_month

    'Total of Days Enabled
    _total_days_enabled = m_query_as_table.Compute("SUM(T_DAYS)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ENABLED_DAYS).Value = _total_days_enabled

    'Total Enabled & Used by Day
    For _counter = 1 To m_last_day_month
      GRID_COLUMN_DAY_ENABLED = CInt([Enum].Parse(GetType(GRID_COLUMN_ENABLED), "DAY_" & _counter.ToString()))
      GRID_COLUMN_DAY_USE = CInt([Enum].Parse(GetType(GRID_COLUMN_USE), "DAY_" & _counter.ToString()))

      _operation_sum = "SUM(D" & _counter.ToString() & ")"
      _total_day = m_query_as_table.Compute(_operation_sum, "")
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_ENABLED).Value = _total_day

      _operation_sum = "SUM(DU" & _counter.ToString() & ")"
      _total_day = m_query_as_table.Compute(_operation_sum, "")
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DAY_USE).Value = _total_day
    Next

    'Total of Days Used
    _total_days_used = m_query_as_table.Compute("SUM(T_DAYSUSE)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_USE_DAYS).Value = _total_days_used

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  End Sub


#End Region

#Region " Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region

#Region " Overrides"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    'Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7308)

    'Month
    Me.dt_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7113)
    Me.dt_datetime.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dt_datetime.SetRange(New Date(2013, 7, 1), WGDB.Now)
    Me.dt_datetime.ShowUpDown = True
    Me.dt_datetime.Value = WGDB.Now

    m_last_day_month = New Date(dt_datetime.Value.Year, dt_datetime.Value.Month, 1).AddMonths(1).AddDays(-1).Day
    m_month = dt_datetime.Value.ToString("MMM")

    ' Initialize Grid
    Call GUI_StyleSheet()
    Call CustomColumnsDaysHideGrid()

    ' Set filter default values
    Call SetDefaultValues()
  End Sub 'GUI_InitControls

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_ACTIVITY_TABLES
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Call function to reset Filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    MyBase.GUI_ButtonClick(ButtonId)
  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_command As SqlCommand = New SqlCommand()
    Dim db_row As CLASS_DB_ROW
    Dim _query_as_datatable As DataTable
    Dim _row As DataRow
    Dim idx_row As Integer

    Call GUI_StyleSheet()
    Call CustomColumnsDaysHideGrid()

    _sql_command.CommandText = "sp_GetTablesActivity"
    _sql_command.CommandType = CommandType.StoredProcedure
    _sql_command.Parameters.Add("@pDate", SqlDbType.DateTime).Value = dt_datetime.Value

    _query_as_datatable = GUI_GetTableUsingCommand(_sql_command, Integer.MaxValue)

    If _query_as_datatable.Rows.Count > 0 Then

      m_query_as_table = _query_as_datatable

      For Each _row In _query_as_datatable.Rows
        Try
          db_row = New CLASS_DB_ROW(_row)

          Me.Grid.AddRow()
          idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(idx_row, db_row) Then
            Me.Grid.DeleteRowFast(idx_row)
          End If

        Catch ex As OutOfMemoryException
          Throw
        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_activity_tables", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If
  End Sub

  ' PURPOSE : Sets the values of a row after show in datagrid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim SQL_COLUMN_DAY_ENABLED As Integer
    Dim SQL_COLUMN_DAY_USE As Integer
    Dim GRID_COLUMN_DAY_ENABLED As Integer
    Dim GRID_COLUMN_DAY_USE As Integer
    Dim _counter As Integer
    Dim _diff As Integer

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    'Table Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_TYPE).Value = IIf(DbRow.IsNull(SQL_COLUMN_TABLE_TYPE), 0, DbRow.Value(SQL_COLUMN_TABLE_TYPE))

    'Table Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TABLE_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_TABLE_NAME), 0, DbRow.Value(SQL_COLUMN_TABLE_NAME))

    'Month
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MONTH).Value = IIf(DbRow.IsNull(SQL_COLUMN_MONTH), "", DbRow.Value(SQL_COLUMN_MONTH))

    'Days Enabled
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED_DAYS).Value = IIf(DbRow.IsNull(SQL_COLUMN_ENABLED_DAYS), 0, DbRow.Value(SQL_COLUMN_ENABLED_DAYS))

    'ENABLED BY DAY AND USE BY DAY
    _diff = 31 - m_last_day_month

    For _counter = 1 To m_last_day_month
      GRID_COLUMN_DAY_ENABLED = CInt([Enum].Parse(GetType(GRID_COLUMN_ENABLED), "DAY_" & _counter.ToString()))
      GRID_COLUMN_DAY_USE = CInt([Enum].Parse(GetType(GRID_COLUMN_USE), "DAY_" & _counter.ToString()))

      SQL_COLUMN_DAY_ENABLED = CInt([Enum].Parse(GetType(SQL_COLUMN_ENABLED), "DAY_" & _counter.ToString()))
      SQL_COLUMN_DAY_USE = CInt([Enum].Parse(GetType(SQL_COLUMN_USE), "DAY_" & _counter.ToString())) - _diff

      Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_ENABLED).Value = IIf(DbRow.IsNull(SQL_COLUMN_DAY_ENABLED), 0, DbRow.Value(SQL_COLUMN_DAY_ENABLED))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DAY_USE).Value = IIf(DbRow.IsNull(SQL_COLUMN_DAY_USE), 0, DbRow.Value(SQL_COLUMN_DAY_USE))
    Next

    'USE DAYS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_USE_DAYS).Value = IIf(DbRow.IsNull(SQL_COLUMN_USE_DAYS - _diff), 0, DbRow.Value(SQL_COLUMN_USE_DAYS - _diff))

    Return True
  End Function 'GUI_SetupRow

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS:
  '     - none       
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dt_datetime
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Call SetRowTotalGrid()
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_month_year = dt_datetime.Value.ToString("MMMM yyyy")
    m_month = dt_datetime.Value.ToString("MMM")
    m_last_day_month = New Date(dt_datetime.Value.Year, dt_datetime.Value.Month, 1).AddMonths(1).AddDays(-1).Day
  End Sub 'GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7113), m_month_year)
    PrintData.FilterValueWidth(1) = 3000
  End Sub ' GUI_ReportFilter

#End Region

End Class