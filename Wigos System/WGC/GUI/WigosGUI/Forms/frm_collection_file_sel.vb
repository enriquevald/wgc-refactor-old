'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_soft_count_sel
'   DESCRIPTION : Selection Soft Counts movements
'        AUTHOR : Fernando Jim�nez
' CREATION DATE : 05-MAY-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-MAY-2015  FJC    Initial version. Backlog Item 1415
' 10-MAY-2015  SGB    BUG 1896: Change title for this form.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common.CollectionImport

Public Class frm_collection_file_sel
  Inherits frm_base_sel

#Region " Enums "



#End Region ' Enums

#Region " Constants "
  Private Const GRID_FIXED_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  'COLUMN
  'select
  Private Const GRID_COLUMN_SELECT As Integer = 0
  'Soft Counts
  Private Const GRID_COLUMN_SOFT_COUNTS_ID As Integer = 1
  Private Const GRID_COLUMN_SOFT_COUNTS_NAME As Integer = 2
  Private Const GRID_COLUMN_SOFT_COUNTS_STATUS As Integer = 3
  Private Const GRID_COLUMN_SOFT_COUNTS_STATUS_ID As Integer = 4
  Private Const GRID_COLUMN_SOFT_COUNTS_IMPORT_DATETIME As Integer = 5
  Private Const GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY As Integer = 6
  Private Const GRID_COLUMN_SOFT_COUNTS_IMPORT_USER As Integer = 7
  Private Const GRID_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME As Integer = 8
  Private Const GRID_COLUMN_SOFT_COUNTS_COLLECTION_USER As Integer = 9
  Private Const GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION As Integer = 10
  Private Const GRID_COLUMN_SOFT_COUNTS_TOTAL_BILLS As Integer = 11
  Private Const GRID_COLUMN_SOFT_COUNTS_TOTAL_TICKETS As Integer = 12
  Private Const GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID As Integer = 13

  'WIDTH
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
#If DEBUG Then
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_ID As Integer = 1000
#Else
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_ID As Integer = 0
#End If
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_NAME As Integer = 2400
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_IMPORT_DATETIME As Integer = 1700
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_IMPORT_USER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_COLLECTION_DATETIME As Integer = 1700
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_COLLECTION_USER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_STATUS As Integer = 2100
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_IMPORT_SESSION_DAY As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_CAGE_SESSION As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_CAGE_SESSION_ID As Integer = 0
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_TOTAL_BILLS As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_SOFT_COUNTS_TOTAL_TICKETS As Integer = 2000

  'SQL
  'Soft Count
  Private Const SQL_COLUMN_SOFT_COUNTS_ID As Integer = 0
  Private Const SQL_COLUMN_SOFT_COUNTS_NAME As Integer = 1
  Private Const SQL_COLUMN_SOFT_COUNTS_STATUS As Integer = 2
  Private Const SQL_COLUMN_SOFT_COUNTS_IMPORT_DATETIME As Integer = 3
  Private Const SQL_COLUMN_SOFT_COUNTS_IMPORT_USER As Integer = 4
  Private Const SQL_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME As Integer = 5
  Private Const SQL_COLUMN_SOFT_COUNTS_COLLECTION_USER As Integer = 6
  Private Const SQL_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY As Integer = 7
  Private Const SQL_COLUMN_SOFT_COUNTS_CAGE_SESSION As Integer = 8
  Private Const SQL_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID As Integer = 9
  Private Const SQL_COLUMN_SOFT_COUNTS_TOTAL_BILLS As Integer = 10
  Private Const SQL_COLUMN_SOFT_COUNTS_TOTAL_TICKETS As Integer = 11

  'COUNTERS
  Private Const COUNTER_PENDING_COLLECTION As Int32 = 1
  Private Const COUNTER_COLLECTED As Int32 = 2

#End Region 'Constants

#Region " Members "

  ' Report filters 
  Private m_soft_count_id As String
  Private m_soft_count_name As String
  Private m_soft_count_status As String
  Private m_soft_count_date_type As String
  Private m_soft_count_date_from As String
  Private m_soft_count_date_to As String
#End Region 'Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COLLECTION_FILE_SEL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6487)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_COLLECTION_FILE_IMPORT).Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6299) ' Import

    'Cancel
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)

    ' Name
    Me.ef_soft_count_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6297)
    Me.ef_soft_count_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50, 0)

    ' Status
    Me.gb_soft_count_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6304)
    Me.chk_soft_count_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)
    Me.chk_soft_count_collected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6306)

    ' Dates
    Me.gb_soft_count_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6199)      'Fechas
    Me.opt_imported.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6312)              'Fecha de Importaci�n
    Me.opt_collected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6313)             'Fecha de Recaudaci�n
    Me.opt_daily_imported.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6314)        'Jornada de Importaci�n
    Me.opt_daily_cage_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6170)    'Jornada de Sesi�n de B�veda

    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Colors
    Me.lbl_pending.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
    Me.lbl_collected.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREEN_00)

    Call GUI_StyleSheet()

    Call SetDefaultValues()

  End Sub

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.opt_imported

  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Dim _current_soft_count_register As Long
    Dim _current_name_soft_count_register As String
    Dim _soft_count_data As SofCountData
    Dim _xml As String

    _current_soft_count_register = 0
    _current_name_soft_count_register = String.Empty
    _xml = String.Empty
    _soft_count_data = New SofCountData()

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT
        'FJC & JCA 22/05/2015
        If _soft_count_data.GetSoftCountStatus(Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_ID).Value, _xml) = _
           Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Value Then

          Select Case Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Value
            Case SoftCountStatus.PendingCollection
              Call ShowCollection(_xml)

            Case SoftCountStatus.Collected
              Call ShowCancelCollection(_xml)
          End Select
        Else
          ' Status change out of window
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6366), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_3
        Call ShowImport()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine("    SELECT    SC.SC_ID                                                                          ")
    _sb.AppendLine("            , SC.SC_NAME                                                                        ")
    _sb.AppendLine("            , SC.SC_STATUS                                                                      ")
    _sb.AppendLine("            , SC.SC_IMPORT_DATETIME                                                             ")
    _sb.AppendLine("            , GU_IMPORT.GU_FULL_NAME AS 'GU_USERNAME_IMPORT'                                    ")
    _sb.AppendLine("            , SC.SC_COLLECTION_DATETIME                                                         ")
    _sb.AppendLine("            , GU_COLLECT.GU_FULL_NAME AS 'GU_USERNAME_COLLECT'                                  ")
    _sb.AppendLine("            , SC.SC_WORKING_DAY                                                                 ")
    _sb.AppendLine("            , CS.CGS_SESSION_NAME                                                               ")
    _sb.AppendLine("            , CS.CGS_CAGE_SESSION_ID                                                            ")
    _sb.AppendLine("            , SC.SC_TOTAL_BILLS                                                                 ")
    _sb.AppendLine("            , SC.SC_TOTAL_TICKETS                                                               ")
    _sb.AppendLine("      FROM    SOFT_COUNTS SC                                                                    ")
    _sb.AppendLine(" LEFT JOIN    GUI_USERS GU_IMPORT                                                               ")
    _sb.AppendLine("        ON    GU_IMPORT.GU_USER_ID = SC.SC_IMPORT_USER_ID                                       ")
    _sb.AppendLine(" LEFT JOIN    GUI_USERS GU_COLLECT                                                              ")
    _sb.AppendLine("        ON    GU_COLLECT.GU_USER_ID = SC.SC_COLLECTION_USER_ID                                  ")

    If Me.opt_daily_cage_session.Checked OrElse _
       Me.opt_collected.Checked Then

      _sb.AppendLine("  INNER JOIN CAGE_SESSIONS CS")
      _sb.AppendLine("            ON SC.SC_CAGE_SESSION_ID = CS.CGS_CAGE_SESSION_ID")
    Else

      _sb.AppendLine("  LEFT JOIN CAGE_SESSIONS CS")
      _sb.AppendLine("            ON SC.SC_CAGE_SESSION_ID = CS.CGS_CAGE_SESSION_ID")
    End If

    _sb.AppendLine(GetSqlWhere())

    ' Pdte JCA & RCI
    _sb.AppendLine("   ORDER BY  SC.SC_IMPORT_DATETIME  DESC  ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _color_status As System.Drawing.Color
    Dim _status As String

    _status = String.Empty
    _color_status = Nothing

    ' SOFT COUNT

    ' Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_ID).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_ID)

    ' Name
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_NAME).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_NAME)

    End If

    ' Status
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_STATUS)
      Me.GetNameColorStatus(DbRow.Value(SQL_COLUMN_SOFT_COUNTS_STATUS), _status, _color_status)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_STATUS).Value = _status
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_STATUS).BackColor = _color_status

    End If

    ' IMPORT
    ' Date
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_IMPORT_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_IMPORT_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_SOFT_COUNTS_IMPORT_DATETIME), _
                                                                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                     ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY), _
                                                                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    ' User
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_IMPORT_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_IMPORT_USER).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_IMPORT_USER)
    End If

    ' COLLECTION
    ' Date
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME), _
                                                                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                     ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' User
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_COLLECTION_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_COLLECTION_USER).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_COLLECTION_USER)
    End If

    ' Cage Session Day
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_CAGE_SESSION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_CAGE_SESSION)
    End If

    ' Total bills
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_TOTAL_BILLS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_TOTAL_BILLS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SOFT_COUNTS_TOTAL_BILLS))

    End If

    ' Total tickets
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_TOTAL_TICKETS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_TOTAL_TICKETS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SOFT_COUNTS_TOTAL_TICKETS))
    End If

    'Cage Session Id
    If Not DbRow.IsNull(SQL_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID)
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    If IsValidDataRow(SelectedRow) Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_COLLECTION_FILE_COLLECT).Write
    End If

  End Sub ' GUI_RowSelectedEvent


#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6303), Me.m_soft_count_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6304), Me.m_soft_count_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5683), Me.m_soft_count_date_type)
    PrintData.SetFilter("   " & LCase(GLB_NLS_GUI_INVOICING.GetString(202)), Me.m_soft_count_date_from)
    PrintData.SetFilter("   " & LCase(GLB_NLS_GUI_INVOICING.GetString(203)), Me.m_soft_count_date_to)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(7, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _status_aux As String

    _status_aux = String.Empty
    Me.m_soft_count_id = String.Empty
    Me.m_soft_count_name = String.Empty
    Me.m_soft_count_status = String.Empty
    Me.m_soft_count_date_type = String.Empty
    Me.m_soft_count_date_from = String.Empty
    Me.m_soft_count_date_to = String.Empty

    ' Name
    Me.m_soft_count_name = Me.ef_soft_count_name.Value

    'Status
    If Me.chk_soft_count_pending.Checked Then

      Me.GetNameColorStatus(SoftCountStatus.PendingCollection, _status_aux, Nothing)
      Me.m_soft_count_status &= _status_aux
    End If

    If Me.chk_soft_count_collected.Checked Then

      Me.m_soft_count_status &= IIf(Me.m_soft_count_status <> String.Empty, ",", String.Empty)
      Me.GetNameColorStatus(SoftCountStatus.Collected, _status_aux, Nothing)
      Me.m_soft_count_status &= _status_aux
    End If

    ' DATES
    ' Type Dates
    If Me.opt_imported.Checked Then

      Me.m_soft_count_date_type = Me.opt_imported.Text
    ElseIf Me.opt_daily_imported.Checked Then

      Me.m_soft_count_date_type = Me.opt_daily_imported.Text
    ElseIf Me.opt_collected.Checked Then

      Me.m_soft_count_date_type = Me.opt_collected.Text
    Else

      Me.m_soft_count_date_type = Me.opt_daily_cage_session.Text
    End If

    ' Dates
    If Me.dtp_from.Checked Then
      Me.m_soft_count_date_from = GUI_FormatDate(dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Me.dtp_to.Checked Then
      Me.m_soft_count_date_to = GUI_FormatDate(dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region 'OVERRIDES

#Region " Public Functions "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

    AddHandler opt_imported.CheckedChanged, AddressOf SetFormatFilterDate
    AddHandler opt_collected.CheckedChanged, AddressOf SetFormatFilterDate
    AddHandler opt_daily_imported.CheckedChanged, AddressOf SetFormatFilterDate
    AddHandler opt_daily_cage_session.CheckedChanged, AddressOf SetFormatFilterDate
  End Sub

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit
#End Region 'Public Functions

#Region " Private Functions "

  ' PURPOSE : Get Name (String) && Color from a status ID
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GetNameColorStatus(ByVal Status As SoftCountStatus, _
                                 ByRef StringStatus As String, _
                                 ByRef ColorStatus As System.Drawing.Color)
    Select Case Status
      Case SoftCountStatus.PendingCollection

        StringStatus = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)
        ColorStatus = Me.lbl_pending.BackColor
        Me.Grid.Counter(COUNTER_PENDING_COLLECTION).Value += 1

      Case SoftCountStatus.Collected

        StringStatus = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6306)
        ColorStatus = Me.lbl_collected.BackColor
        Me.Grid.Counter(COUNTER_COLLECTED).Value += 1

      Case Else

        ColorStatus = Me.lbl_pending.BackColor
    End Select
  End Sub ' GetNameColorStatus

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_FIXED_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_PENDING_COLLECTION).Visible = True
      .Counter(COUNTER_PENDING_COLLECTION).BackColor = Me.lbl_pending.BackColor
      .Counter(COUNTER_PENDING_COLLECTION).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_COLLECTED).Visible = True
      .Counter(COUNTER_COLLECTED).BackColor = Me.lbl_collected.BackColor
      .Counter(COUNTER_COLLECTED).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = True
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      ' Identifier
      .Column(GRID_COLUMN_SOFT_COUNTS_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SOFT_COUNTS_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6164)
      .Column(GRID_COLUMN_SOFT_COUNTS_ID).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_ID
      .Column(GRID_COLUMN_SOFT_COUNTS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Name
      .Column(GRID_COLUMN_SOFT_COUNTS_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_SOFT_COUNTS_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6303)
      .Column(GRID_COLUMN_SOFT_COUNTS_NAME).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_NAME
      .Column(GRID_COLUMN_SOFT_COUNTS_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6304)
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_STATUS
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS).HighLightWhenSelected = False

      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6304)
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_ID
      .Column(GRID_COLUMN_SOFT_COUNTS_STATUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' IMPORT DATA
      ' Date
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6300)
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5683)
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_DATETIME).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_IMPORT_DATETIME
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Session Day
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6300)
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_IMPORT_SESSION_DAY
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' User
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6300)
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6272)
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_USER).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_IMPORT_USER
      .Column(GRID_COLUMN_SOFT_COUNTS_IMPORT_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' COLLECTED DATA
      ' Date
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6308)
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5683)
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_COLLECTION_DATETIME
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' User
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6308)
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6272)
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_USER).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_COLLECTION_USER
      .Column(GRID_COLUMN_SOFT_COUNTS_COLLECTION_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cage Session
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6308)
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388) ' 5715 
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_CAGE_SESSION
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Total Bills
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6308)
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_BILLS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6357)
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_BILLS).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_TOTAL_BILLS
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Tickets
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6308)
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_TICKETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6358)
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_TICKETS).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_TOTAL_TICKETS
      .Column(GRID_COLUMN_SOFT_COUNTS_TOTAL_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cage Session Id
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID).Width = GRID_COLUMN_WIDTH_SOFT_COUNTS_CAGE_SESSION_ID
      .Column(GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' HightLight 
      For _count As Integer = 0 To GRID_FIXED_COLUMNS - 1
        .Column(_count).HighLightWhenSelected = True
      Next

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column GRID_COLUMN_SOFT_COUNTS_ID.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_SOFT_COUNTS_ID).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()

    ' Name
    Me.ef_soft_count_name.Value = String.Empty

    ' Dates operation
    Me.opt_imported.Checked = True
    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    ' Status
    Me.chk_soft_count_pending.Enabled = True
    Me.chk_soft_count_collected.Enabled = True

    Me.chk_soft_count_pending.Checked = True
    Me.chk_soft_count_collected.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _str_where_status As String
    Dim _str_where_dates As String
    Dim _str_aux1 As String
    Dim _str_from As String
    Dim _str_to As String

    _str_where = String.Empty
    _str_where_status = String.Empty
    _str_where_dates = String.Empty

    ' Name
    If Me.ef_soft_count_name.Value <> String.Empty Then

      _str_aux1 = UCase(Me.ef_soft_count_name.Value).Replace("'", "''")
      _str_where &= " WHERE UPPER(SC.SC_NAME) LIKE '%" & _str_aux1 & "%' "
    End If

    ' Status
    _str_where &= IIf(_str_where <> String.Empty, " AND ", " WHERE ")

    If Me.chk_soft_count_pending.Checked OrElse _
       Me.chk_soft_count_collected.Checked Then

      _str_where &= "SC_STATUS IN("

      ' Pending collection
      If Me.chk_soft_count_pending.Checked Then
        _str_where_status &= IIf(_str_where_status <> String.Empty, ",", String.Empty)
        _str_where_status &= SoftCountStatus.PendingCollection
      End If

      ' Collected
      If Me.chk_soft_count_collected.Checked Then
        _str_where_status &= IIf(_str_where_status <> String.Empty, ",", String.Empty)
        _str_where_status &= SoftCountStatus.Collected
      End If

      _str_where_status += ")"

    Else

      _str_where &= "SC_STATUS <> "
      _str_where_status = SoftCountStatus.Deleted

    End If
    _str_where += _str_where_status

    'Date
    If Me.dtp_from.Checked OrElse _
       Me.dtp_to.Checked Then

      _str_where &= IIf(_str_where <> String.Empty, " AND ", " WHERE ")

      ' Field Name
      If Me.opt_imported.Checked Then

        _str_aux1 = "SC_IMPORT_DATETIME"
      ElseIf Me.opt_collected.Checked Then

        _str_aux1 = "SC_COLLECTION_DATETIME"
      ElseIf Me.opt_daily_imported.Checked Then

        _str_aux1 = "SC_WORKING_DAY"
      Else

        _str_aux1 = "CGS_WORKING_DAY"
      End If

      ' Apply Format (with hours/minutes or not)
      If Me.opt_imported.Checked OrElse _
         Me.opt_collected.Checked Then

        _str_from = GUI_FormatDateDB(Me.dtp_from.Value)
        _str_to = GUI_FormatDateDB(Me.dtp_to.Value)
      Else

        _str_from = GUI_FormatDayDB(Me.dtp_from.Value)
        _str_to = GUI_FormatDayDB(Me.dtp_to.Value)
      End If

      'Apply date range
      If Me.dtp_from.Checked Then
        _str_where_dates &= _str_aux1 & " >= " & _str_from
      End If

      If Me.dtp_to.Checked Then
        If _str_where_dates <> String.Empty Then
          _str_where_dates &= " AND "
        End If
        _str_where_dates &= _str_aux1 & " < " & _str_to
      End If

      _str_where += _str_where_dates
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Show Import Form
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowImport()
    Dim _frm As frm_collection_file_import
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_collection_file_import()
    _frm.ShowForEdit()

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' ShowImport

  ' PURPOSE : Show Cancel Collection
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowCancelCollection(ByVal Xml As String)
    Dim _frm As frm_collection_file
    Dim _parameters As SofCountData.SOFT_COUNT_PARAMETERS
    'FJC & JCA 22/05/2015
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _parameters = New SofCountData.SOFT_COUNT_PARAMETERS
    _parameters.CollectionMode = COLLECTION_MODE.MODE_CANCEL
    _parameters.Xml = Xml
    _parameters.SessionDay = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Value
    _parameters.CageSessionId = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION_ID).Value
    _parameters.CageSessionName = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_CAGE_SESSION).Value
    _parameters.SoftCountId = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_ID).Value
    _parameters.SoftCountName = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_NAME).Value

    _frm = New frm_collection_file()

    _frm.ShowNewItem(_parameters)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' ShowCancelCollection

  ' PURPOSE : Show Form Collection
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowCollection(ByVal Xml As String)
    Dim _frm As frm_collection_file
    Dim _parameters As SofCountData.SOFT_COUNT_PARAMETERS
    'FJC & JCA 22/05/2015
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _parameters = New SofCountData.SOFT_COUNT_PARAMETERS
    _parameters.CollectionMode = COLLECTION_MODE.MODE_COLLECT
    _parameters.Xml = Xml
    _parameters.SessionDay = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_IMPORT_SESSION_DAY).Value
    _parameters.SoftCountId = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_ID).Value
    _parameters.SoftCountName = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SOFT_COUNTS_NAME).Value

    _frm = New frm_collection_file()

    _frm.ShowNewItem(_parameters)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' ShowCollection

  ' PURPOSE : Get Name (String) from a status ID
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub SetFormatFilterDate(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Select Case sender.name.toUpper()
      Case "OPT_IMPORTED", _
           "OPT_COLLECTED"

        Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
        Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
        Me.dtp_from.Width = 200
        Me.dtp_to.Width = 200

      Case "OPT_DAILY_IMPORTED", _
           "OPT_DAILY_CAGE_SESSION"

        Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        Me.dtp_from.Width = 164
        Me.dtp_to.Width = 164
    End Select

    Select Case sender.name.toUpper()
      Case "OPT_IMPORTED", _
           "OPT_DAILY_IMPORTED"

        Me.chk_soft_count_pending.Enabled = True
        Me.chk_soft_count_collected.Enabled = True

      Case "OPT_DAILY_CAGE_SESSION", _
           "OPT_COLLECTED"

        Me.chk_soft_count_pending.Enabled = False
        Me.chk_soft_count_pending.Checked = False
        Me.chk_soft_count_collected.Enabled = True
    End Select

  End Sub ' SetFormatFilterDate

#End Region ' Private Functions


End Class