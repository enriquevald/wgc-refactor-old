'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_pattern_edit.vb
'
' DESCRIPTION : New or Edit Pattern item 
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2014  JMM    Initial version
' 01-JUN-2014  JMM    Fixed bug WIG-976: Don't let patterns with an empty alarm description 
' 22-JUL-2013  JBC    We look up pattern recursivity
' 05-SEP-2014  JPJ    Mailing alarm functionality
' 26-SEP-2014  JPJ    Fixed Bug WIG-1319: We can create a pattern with no alarms.
' 03-OCT-2014  JBC    Fixed Bug WIG-1396: Terminal list control show correctly
' 08-OCT-2014  JPJ    Fixed Bug WIG-1442: When lifetime is left empty
' 09-OCT-2014  DRV    Fixed Bug WIG-1453: Alarm Filter idiom it's different from the configured language
' 23-OCT-2014  DLL    Fixed Bug WIG-1237: Hide columns GRID_COLUMN_DETECTIONS And GRID_COLUMN_LAST_FIND when is Multisite. Only show when is Site
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_pattern_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Short = 219

  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

  ' Grid columns
  Private Const PATTERN_ELEMENTS_GRID_COLUMNS As Integer = 2
  Private Const PATTERN_ELEMENTS_GRID_HEADER_ROWS As Integer = 0

  Private Const PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE = 0
  Private Const PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION = 1

  Enum GUI_MODE
    WITHOUT_MULTISITE = 1
    MULTISITE_CENTER = 2
    MULTISITE_SITE = 3
  End Enum

#End Region ' Constants

#Region " Members "

  Dim m_alarms_dic As Dictionary(Of Integer, Alarm.ALARMS_CATALOG)

  Private m_input_pattern_name As String
  Private m_delete_operation As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Control the enter key.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If txt_alarm_mailing_address_list.Focused() Or txt_alarm_description.Focused() Or txt_description.Focused() Then
      Return True
    Else
      Return False
    End If

  End Function ' GUI_KeyPress

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PATTERNS_EDIT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()


  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :

  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4926) ' 4926 "Edici�n de Patr�n"

    ' Delete button is only available when editing, not while creating on stand-alone sites
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = ((Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT) _
                                                 And Not GeneralParam.GetBoolean("Site", "MultiSiteMember", False) _
                                                 And Not GeneralParam.GetBoolean("MultiSite", "IsCenter", False))

    'Name
    Me.ef_name.TextVisible = True
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) '693 "Nombre"
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_PATTERN.MAX_NAME_LEN)

    ' Enabled control
    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008) '3008 "Habilitado"
    Me.opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Me.opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Schedule
    Me.gb_schedule.Text = ""
    Me.chk_schedule.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4953) ' 4953 "Validity period"

    Me.dtp_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(695) ' From
    Me.dtp_time_from.ShowUpDown = True
    Me.dtp_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.dtp_time_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(696) ' To
    Me.dtp_time_to.ShowUpDown = True
    Me.dtp_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.ef_last_find.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4961) '4961 "Last occurrence"
    Me.ef_last_find.IsReadOnly = True
    Me.ef_num_occurrencies.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4962) '4962 "Occurrences"
    Me.ef_num_occurrencies.IsReadOnly = True

    Me.ef_life_time.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4972)        '4972 "Life time"
    Me.ef_life_time.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.lbl_min.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1583)             '1583 "min."

    ' Type
    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1087)          '1087 "Type"
    Me.opt_consecutive.Text = CLASS_PATTERN.PatternTypeDescription(PATTERN_TYPE.CONSECUTIVE)
    Me.opt_no_consecutive_with_order.Text = CLASS_PATTERN.PatternTypeDescription(PATTERN_TYPE.NO_CONSECUTIVE_WITH_ORDER)
    Me.opt_with_no_order.Text = CLASS_PATTERN.PatternTypeDescription(PATTERN_TYPE.NO_ORDER)
    Me.opt_with_no_order_with_repetitions.Text = CLASS_PATTERN.PatternTypeDescription(PATTERN_TYPE.NO_ORDER_WITH_REPETITION)

    ' Description
    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5012)     ' 5012 "Pattern description"
    Me.txt_description.MaxLength = CLASS_PATTERN.MAX_DESCRIPTION_LEN

    ' Elements
    Me.gb_elements.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4963)         ' 4963 "Elements"
    Me.btn_clear.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4969)           ' 4969 "Clear all"

    ' Alarm
    Me.gb_alarm.Text = GLB_NLS_GUI_ALARMS.GetString(207)
    Me.ef_alarm_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)       ' 3418 "Code"
    Me.ef_alarm_code.IsReadOnly = True
    Me.ef_alarm_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.ef_alarm_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693)        ' 693 "Nombre"
    Me.ef_alarm_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_PATTERN.MAX_NAME_LEN)

    ' Severity
    Me.gb_severity.Text = GLB_NLS_GUI_ALARMS.GetString(416)
    Me.opt_info.Text = GLB_NLS_GUI_ALARMS.GetString(206)
    Me.opt_warning.Text = GLB_NLS_GUI_ALARMS.GetString(358)
    Me.opt_error.Text = GLB_NLS_GUI_ALARMS.GetString(357)

    Me.lbl_color_info.BackColor = GetColor(COLOR_INFO_BACK)
    Me.lbl_color_warning.BackColor = GetColor(COLOR_WARNING_BACK)
    Me.lbl_color_error.BackColor = GetColor(COLOR_ERROR_BACK)

    ' Alarm Description
    Me.lbl_alarm_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)      ' 688 "Description"
    Me.txt_alarm_description.MaxLength = CLASS_PATTERN.MAX_DESCRIPTION_LEN

    ' Alarms
    Me.checked_list_alarms.GroupBoxText = GLB_NLS_GUI_ALARMS.GetString(201)         ' 201 "Alarms"

    'Terminals list
    Dim _terminal_list As TerminalList

    _terminal_list = New TerminalList
    _terminal_list.SetListType(TerminalList.TerminalListType.All)

    Me.dg_providers.SetGroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(477)            ' Allowed terminals list

    Me.dg_providers.SetTerminalList(_terminal_list)

    ' Mailing
    chk_mailing.Text = GLB_NLS_GUI_AUDITOR.GetString(77)

    ' Subject
    lbl_subject.Text = GLB_NLS_GUI_STATISTICS.GetString(436)

    ' Address List
    lbl_address_list.Text = GLB_NLS_GUI_STATISTICS.GetString(428)

    Me.gb_mailing.Text = ""

    GUI_DG_ElementsStyleSheet()

    GUI_EnableDisableControls()

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_param1 As String
    Dim emails() As String
    Dim email As String

    ' The following values must be entered
    '   - Name : Mandatory

    '   - Name : Mandatory
    If ef_name.Value = "" Then
      ' 118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_name.Text)
      Call ef_name.Focus()

      Return False
    End If

    ' 08-OCT-2014  JPJ    Fixed Bug WIG-1442: When lifetime is left empty
    If String.IsNullOrEmpty(Me.ef_life_time.Value) Then
      '1190  "%1 field value must be between %2 and %3." 
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , _
                      Me.ef_life_time.Text, Patterns.PATTERN_LIFE_TIME_MIN, Patterns.PATTERN_LIFE_TIME_MAX)
      Call Me.ef_life_time.Focus()

      Return False
    End If

    If Me.ef_life_time.Value < Patterns.PATTERN_LIFE_TIME_MIN _
       Or Me.ef_life_time.Value > Patterns.PATTERN_LIFE_TIME_MAX Then
      '1190  "%1 field value must be between %2 and %3." 
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , _
                      Me.ef_life_time.Text, Patterns.PATTERN_LIFE_TIME_MIN, Patterns.PATTERN_LIFE_TIME_MAX)
      Call Me.ef_life_time.Focus()

      Return False
    End If

    If Me.ef_alarm_name.Value = "" Then
      '5013 "You must enter an alarm name"  
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5013), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      Call Me.ef_alarm_name.Focus()

      Return False
    End If

    If Me.txt_alarm_description.Text = "" Then
      '5015  "You must enter an alarm description"  
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5015), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      Call Me.txt_alarm_description.Focus()

      Return False
    End If

    If Me.dg_elements.NumRows < 1 Then
      '4966 "You must select at least one element to add to the pattern."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4966), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    If Me.chk_mailing.Checked Then
      If Me.txt_alarm_mailing_subject.Text = "" Then
        nls_param1 = GLB_NLS_GUI_STATISTICS.GetString(436)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        Call Me.txt_alarm_mailing_subject.Focus()

        Return False
      End If

      If Me.txt_alarm_mailing_address_list.Text = "" Then
        nls_param1 = GLB_NLS_GUI_STATISTICS.GetString(429)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        Call Me.txt_alarm_mailing_address_list.Focus()

        Return False
      End If

      ' Check email addresses
      Dim sep(0 To 1) As Char
      Dim tmp_address As String

      emails = txt_alarm_mailing_address_list.Text.Split()
      tmp_address = String.Join(";", emails)
      sep(0) = ";"
      sep(1) = ","
      emails = tmp_address.Split(sep, StringSplitOptions.RemoveEmptyEntries)

      For Each email In emails
        email = email.Trim()
        If email = "" Then
          Continue For
        End If

        If Not WSI.Common.ValidateFormat.Email(email) Then
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , email)
          Call txt_alarm_mailing_address_list.Focus()

          Return False
        End If
      Next
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _pattern As CLASS_PATTERN
    Dim _elapsed As TimeSpan

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _pattern = DbReadObject

    Me.ef_name.Value = _pattern.Name
    Me.opt_enabled.Checked = _pattern.Active
    Me.opt_disabled.Checked = Not _pattern.Active

    If _pattern.ScheduledTimeFrom = -1 Or _pattern.ScheduledTimeTo = -1 Then
      Me.chk_schedule.Checked = False
      _elapsed = TimeSpan.FromSeconds(0)
      Me.dtp_time_from.Value = DateTime.Today.Add(_elapsed)
      Me.dtp_time_to.Value = DateTime.Today.Add(_elapsed)
    Else
      Me.chk_schedule.Checked = True
      _elapsed = TimeSpan.FromSeconds(_pattern.ScheduledTimeFrom)
      Me.dtp_time_from.Value = DateTime.Today.Add(_elapsed)

      _elapsed = TimeSpan.FromSeconds(_pattern.ScheduledTimeTo)
      Me.dtp_time_to.Value = DateTime.Today.Add(_elapsed)
    End If
    Call Me.chk_schedule_CheckedChanged(Me, Nothing)

    If _pattern.LastOcurrence <> Date.MinValue Then
      Me.ef_last_find.Value = _pattern.LastOcurrence
    Else
      Me.ef_last_find.Value = "---"
    End If


    Me.ef_num_occurrencies.Value = _pattern.NumOccurrences
    If GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      Me.ef_num_occurrencies.Value = "---"
    End If

    Me.ef_life_time.Value = _pattern.LifeTime

    Select Case _pattern.Type
      Case PATTERN_TYPE.CONSECUTIVE
        Me.opt_consecutive.Checked = True

      Case PATTERN_TYPE.NO_CONSECUTIVE_WITH_ORDER
        Me.opt_no_consecutive_with_order.Checked = True

      Case PATTERN_TYPE.NO_ORDER
        Me.opt_with_no_order.Checked = True

      Case PATTERN_TYPE.NO_ORDER_WITH_REPETITION
        Me.opt_with_no_order_with_repetitions.Checked = True

      Case Else
        Me.opt_consecutive.Checked = True

    End Select

    Me.txt_description.Text = _pattern.Description

    Call FillAlarmsCheckedList(_pattern.AlarmCode)

    Me.ef_alarm_code.Value = _pattern.AlarmCode
    Me.ef_alarm_name.Value = _pattern.AlarmName
    Me.txt_alarm_description.Text = _pattern.AlarmDescription

    Select Case _pattern.AlarmSeverity
      Case AlarmSeverity.Error
        Me.opt_error.Checked = True
      Case AlarmSeverity.Info
        Me.opt_info.Checked = True
      Case AlarmSeverity.Warning
        Me.opt_warning.Checked = True
      Case Else
    End Select

    Me.dg_providers.SetTerminalList(_pattern.TerminalList)

    Me.chk_mailing.Checked = _pattern.MailingActive
    Call Me.chk_mailing_CheckedChanged(Me, Nothing)

    Me.txt_alarm_mailing_address_list.Text = _pattern.MailingAddressList
    Me.txt_alarm_mailing_subject.Text = _pattern.MailingSubject

    GUI_DG_ElementsSetPattern(_pattern.Pattern)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()

    Dim _pattern As CLASS_PATTERN

    _pattern = DbEditedObject

    _pattern.Name = Me.ef_name.Value
    _pattern.Active = Me.opt_enabled.Checked

    If Me.chk_schedule.Checked Then
      _pattern.ScheduledTimeFrom = Me.dtp_time_from.Value.TimeOfDay.TotalSeconds
      _pattern.ScheduledTimeTo = Me.dtp_time_to.Value.TimeOfDay.TotalSeconds
    Else
      _pattern.ScheduledTimeFrom = -1
      _pattern.ScheduledTimeTo = -1
    End If

    If Me.opt_consecutive.Checked Then
      _pattern.Type = PATTERN_TYPE.CONSECUTIVE
    ElseIf Me.opt_no_consecutive_with_order.Checked Then
      _pattern.Type = PATTERN_TYPE.NO_CONSECUTIVE_WITH_ORDER
    ElseIf Me.opt_with_no_order.Checked Then
      _pattern.Type = PATTERN_TYPE.NO_ORDER
    ElseIf Me.opt_with_no_order_with_repetitions.Checked Then
      _pattern.Type = PATTERN_TYPE.NO_ORDER_WITH_REPETITION
    End If

    ' 08-OCT-2014  JPJ    Fixed Bug WIG-1442: When lifetime is left empty
    If String.IsNullOrEmpty(Me.ef_life_time.Value) Then Me.ef_life_time.Value = 0
    _pattern.LifeTime = Me.ef_life_time.Value

    _pattern.Description = Me.txt_description.Text

    If ef_alarm_code.Value <> "" Then
      _pattern.AlarmCode = Me.ef_alarm_code.Value
    End If

    _pattern.AlarmName = Me.ef_alarm_name.Value
    _pattern.AlarmDescription = Me.txt_alarm_description.Text

    If Me.opt_error.Checked Then
      _pattern.AlarmSeverity = AlarmSeverity.Error
    ElseIf Me.opt_info.Checked Then
      _pattern.AlarmSeverity = AlarmSeverity.Info
    ElseIf Me.opt_warning.Checked Then
      _pattern.AlarmSeverity = AlarmSeverity.Warning
    End If

    Me.dg_providers.GetTerminalsFromControl(_pattern.TerminalList)

    _pattern.MailingActive = Me.chk_mailing.Checked
    _pattern.MailingAddressList = Me.txt_alarm_mailing_address_list.Text
    _pattern.MailingSubject = Me.txt_alarm_mailing_subject.Text

    GUI_DG_ElementsGetPattern(_pattern.Pattern)

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _pattern As CLASS_PATTERN
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Min database version popup alerdy showed. Don't show any message again
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PATTERN
        _pattern = DbEditedObject
        _pattern.PatternID = 0
        _pattern.Type = PatternList.PatternListType.Alarms
        _pattern.Active = True
        _pattern.AlarmSeverity = AlarmSeverity.Info
        _pattern.LifeTime = GeneralParam.GetInt32("Pattern", "Pattern.LifeTime", Patterns.PATTERN_LIFE_TIME, Patterns.PATTERN_LIFE_TIME_MIN, Patterns.PATTERN_LIFE_TIME_MAX)
        _pattern.Source = PatternList.PatternListType.Alarms ' JMM 27-MAY-2014: For now, only patterns of alarms

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 4935 "Se ha producido un error al leer el patr�n %1."
          _pattern = Me.DbEditedObject
          _aux_nls = m_input_pattern_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4935), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 4936 "Ya existe un patr�n con el nombre %1."
          _pattern = Me.DbEditedObject
          _aux_nls = _pattern.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4936), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 4937 "Se ha producido un error al a�adir el patr�n %1."
          _pattern = Me.DbEditedObject
          _aux_nls = _pattern.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4937), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 4936 "Ya existe un patr�n con el nombre %1."
          _pattern = Me.DbEditedObject
          _aux_nls = _pattern.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4936), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 4938 "Se ha producido un error al modificar el patr�n %1."
          _pattern = Me.DbEditedObject
          _aux_nls = _pattern.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4938), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        ' 4939 "�Est� seguro que quiere borrar el patr�n %1?"
        _pattern = Me.DbEditedObject
        _aux_nls = _pattern.Name
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4939), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _aux_nls)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 4940 "Se ha producido un error al borrar el patr�n %1."
          _pattern = Me.DbEditedObject
          _aux_nls = _pattern.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4940), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal PatternId As Integer, ByVal PatternName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = PatternId
    Me.m_input_pattern_name = PatternName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Private "

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillAlarmsCheckedList(ByVal AlarmCode As Int64)
    Dim _alarm_code As Object
    Dim _dt As DataTable

    Me.checked_list_alarms.SetLevels = 3
    Me.checked_list_alarms.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 5000)

    _dt = New DataTable()

    m_alarms_dic = New Dictionary(Of Integer, Alarm.ALARMS_CATALOG)

    Try

      If AlarmCode = 0 Then
        _alarm_code = Nothing
      Else
        _alarm_code = AlarmCode
      End If

      ' Load alarms excluding customized ones
      If Not Alarm.GetAlarmsCatalog(True, _alarm_code, m_alarms_dic, Language.GetWigosGUILanguageId()) Then
        Debug.WriteLine("Not all alarm codes have been retrieved")

        Return
      End If

      Call Patterns.GetForbiddenPatterns(AlarmCode, _dt)

      For Each _dr As DataRow In _dt.Rows
        If m_alarms_dic.ContainsKey(Convert.ToInt64(_dr(0))) Then
          m_alarms_dic.Remove(Convert.ToInt64(_dr(0)))
        End If
      Next

      Call FillAlarmsGroupsControl(m_alarms_dic, Me.checked_list_alarms)

      If checked_list_alarms.Count > 0 Then
        Me.checked_list_alarms.CurrentRow(0)
        Me.checked_list_alarms.ReDraw(True)
        Me.checked_list_alarms.CollapseAll()
        Me.checked_list_alarms.SetDefaultValue(False)
      End If

      Me.checked_list_alarms.ResizeGrid()

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillAlarmsCheckedList

  Private Sub AddAlarmToPattern(ByVal AlarmCode As Integer)
    Dim _idx_row As Integer

    Try
      If m_alarms_dic.ContainsKey(AlarmCode) Then
        _idx_row = Me.dg_elements.AddRow()

        Me.dg_elements.Cell(_idx_row, PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Value = AlarmCode
        Me.dg_elements.Cell(_idx_row, PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Value = m_alarms_dic(AlarmCode).AlarmName
      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' AddAlarmToPattern

  ' PURPOSE: Format appearance of the offers data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_DG_ElementsStyleSheet()

    With Me.dg_elements

      Call .Init(PATTERN_ELEMENTS_GRID_COLUMNS, PATTERN_ELEMENTS_GRID_HEADER_ROWS, False)

      .Sortable = False

      ' Key
      .Column(PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Width = 0
      .Column(PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).HighLightWhenSelected = True
      .Column(PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Width = 5550
      .Column(PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).HighLightWhenSelected = True
      .Column(PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_DG_ElementsStyleSheet

  ' PURPOSE: Format appearance of the offers data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub GUI_DG_ElementsGetPattern(ByRef Pattern As PatternList)
    Dim _idx_row As Integer

    Try

      Pattern.CleanList()

      For _idx_row = 0 To Me.dg_elements.NumRows - 1
        Pattern.AddElement(Me.dg_elements.Cell(_idx_row, PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Value, _
                           PatternList.PatternListType.Alarms)
      Next

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub

  Private Sub GUI_DG_ElementsSetPattern(ByVal Pattern As PatternList)

    Me.dg_elements.Clear()

    For Each _dr As DataRow In Pattern.Elements.Rows
      AddAlarmToPattern(_dr(PatternList.COLUMN_ID))
    Next

  End Sub

  Private Sub GUI_DG_ElementsSwitchRows(ByVal IdxRowSource, ByVal IdxRowTarget)
    Dim _id As Integer
    Dim _name As String

    Try
      _id = Me.dg_elements.Cell(IdxRowTarget, PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Value
      _name = Me.dg_elements.Cell(IdxRowTarget, PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Value

      Me.dg_elements.Cell(IdxRowTarget, PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Value = Me.dg_elements.Cell(IdxRowSource, PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Value
      Me.dg_elements.Cell(IdxRowTarget, PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Value = Me.dg_elements.Cell(IdxRowSource, PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Value

      Me.dg_elements.Cell(IdxRowSource, PATTERN_ELEMENTS_GRID_COLUMN_ALARM_CODE).Value = _id
      Me.dg_elements.Cell(IdxRowSource, PATTERN_ELEMENTS_GRID_COLUMN_DESCRIPTION).Value = _name

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub

  Private Sub GUI_EnableDisableControls()
    Dim _gui_mode As GUI_MODE

    ' Mulsisite Disabled - Standalone
    _gui_mode = GUI_MODE.WITHOUT_MULTISITE

    If GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      ' Multisite Enabled - Center
      _gui_mode = GUI_MODE.MULTISITE_CENTER
    ElseIf GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
      ' Multisite Enabled - Site
      _gui_mode = GUI_MODE.MULTISITE_SITE
    End If

    If _gui_mode = GUI_MODE.WITHOUT_MULTISITE Then
      'Stant Alone mode: All controls enabled.  No changes

      Return
    End If

    Me.ef_name.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.gb_enabled.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.chk_schedule.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.gb_schedule.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.ef_life_time.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.gb_type.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.txt_description.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.ef_alarm_name.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.gb_severity.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.txt_alarm_description.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.btn_add_element.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.btn_remove_element.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.btn_clear.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.btn_move_up.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)
    Me.btn_move_down.Enabled = (_gui_mode = GUI_MODE.MULTISITE_CENTER)

    Me.dg_providers.Enabled = (_gui_mode = GUI_MODE.MULTISITE_SITE)
    Me.gb_mailing.Enabled = (_gui_mode = GUI_MODE.MULTISITE_SITE)
    Me.chk_mailing.Enabled = (_gui_mode = GUI_MODE.MULTISITE_SITE)
  End Sub

#End Region ' Private

#Region " Events "

  Private Sub btn_clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_clear.Click
    Me.dg_elements.Clear()
  End Sub

  Private Sub btn_add_element_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_element.Click
    Dim _idx As Integer
    Dim _alarm_code As Integer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try

      For _idx = 0 To Me.checked_list_alarms.SelectedIndexes.Length - 1
        _alarm_code = Me.checked_list_alarms.SelectedIndexes(_idx)

        If _alarm_code <> 0 Then
          AddAlarmToPattern(_alarm_code)
        End If
      Next

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub

  Private Sub btn_remove_element_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_remove_element.Click
    Dim _idx_row As Integer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try

      While True
        If Me.dg_elements.SelectedRows() Is Nothing Then

          Return
        End If

        _idx_row = Me.dg_elements.SelectedRows(0)

        Me.dg_elements.Row(_idx_row).IsSelected = False
        Me.dg_elements.DeleteRowFast(_idx_row)
      End While

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub

  Private Sub btn_move_up_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_move_up.Click
    Dim _selected_rows As Array
    Dim _idx_row As Integer
    Dim _idx As Integer

    _selected_rows = Me.dg_elements.SelectedRows()

    If _selected_rows Is Nothing Then

      Return
    End If

    For _idx = 0 To _selected_rows.Length - 1
      _idx_row = _selected_rows(_idx)

      If (_idx_row > 0) Then
        If Me.dg_elements.Row(_idx_row - 1).IsSelected = False Then

          GUI_DG_ElementsSwitchRows(_idx_row, _idx_row - 1)
          Me.dg_elements.Row(_idx_row).IsSelected = False
          Call dg_elements.RepaintRow(_idx_row)
          Me.dg_elements.Row(_idx_row - 1).IsSelected = True
          Call dg_elements.RepaintRow(_idx_row - 1)
        End If
      End If
    Next
  End Sub

  Private Sub btn_move_down_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_move_down.Click
    Dim _selected_rows As Array
    Dim _idx_row As Integer
    Dim _idx As Integer

    _selected_rows = Me.dg_elements.SelectedRows()

    If _selected_rows Is Nothing Then

      Return
    End If

    For _idx = _selected_rows.Length - 1 To 0 Step -1
      _idx_row = _selected_rows(_idx)

      If (_idx_row < Me.dg_elements.NumRows - 1) Then
        If Me.dg_elements.Row(_idx_row + 1).IsSelected = False Then
          GUI_DG_ElementsSwitchRows(_idx_row, _idx_row + 1)
          Me.dg_elements.Row(_idx_row).IsSelected = False
          Call dg_elements.RepaintRow(_idx_row)
          Me.dg_elements.Row(_idx_row + 1).IsSelected = True
          Call dg_elements.RepaintRow(_idx_row + 1)
        End If
      End If

    Next

  End Sub

  Private Sub chk_schedule_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_schedule.CheckedChanged
    Me.gb_schedule.Enabled = Me.chk_schedule.Checked _
                             And (GeneralParam.GetBoolean("MultiSite", "IsCenter", False) _
                             Or Not GeneralParam.GetBoolean("Site", "MultiSiteMember", False))
  End Sub

  Private Sub chk_mailing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_mailing.CheckedChanged
    Me.gb_mailing.Enabled = Me.chk_mailing.Checked _
                            And Not (GeneralParam.GetBoolean("MultiSite", "IsCenter", False))
  End Sub

#End Region ' Events

End Class
