<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_template_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_template_name = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.ef_template_name)
    Me.panel_data.Size = New System.Drawing.Size(385, 60)
    '
    'ef_template_name
    '
    Me.ef_template_name.DoubleValue = 0
    Me.ef_template_name.IntegerValue = 0
    Me.ef_template_name.IsReadOnly = False
    Me.ef_template_name.Location = New System.Drawing.Point(3, 20)
    Me.ef_template_name.Name = "ef_template_name"
    Me.ef_template_name.Size = New System.Drawing.Size(316, 24)
    Me.ef_template_name.SufixText = "Sufix Text"
    Me.ef_template_name.SufixTextVisible = True
    Me.ef_template_name.TabIndex = 1
    Me.ef_template_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_template_name.TextValue = ""
    Me.ef_template_name.TextWidth = 90
    Me.ef_template_name.Value = ""
    '
    'frm_template_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(487, 69)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_template_edit"
    Me.ShowInTaskbar = True
    Me.Text = "frm_template_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_template_name As GUI_Controls.uc_entry_field
End Class
