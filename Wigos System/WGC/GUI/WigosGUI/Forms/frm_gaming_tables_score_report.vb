﻿'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_score_report
' DESCRIPTION:   Score report
' AUTHOR:        José Martínez López  
' CREATION DATE: 19-JUL-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-JUL-2017  JML    Initial version
' 09-AUG-2017  RAB    Bug 29293:WIGOS-4303 'Hourly score report' reports an incorrect Drop
' 17-AUG-2017  RAB    Bug 29332:WIGOS-4150 Score report - Close gaming table amounts are not considered on WinLoss "Final" column.
' 18-AUG-2017  RAB    Bug 29404:WIGOS-4613 Score report, columns Final and Total show values when session is not closed
' 27-SEP-2017  JML    Bug 29490:WIGOS-4775 [Ticket #8366] Issue: Reporte Score por Hora - Formulas V03.06.0023
' 18-OCT-2017  RAB    Bug 29490:WIGOS-4775 [Ticket #8366] Issue: Reporte Score por Hora - Formulas V03.06.0023
' 30-OCT-2017  RAB    Bug 30461:WIGOS-6244 [Ticket #9954] Reporte de Score Horario
' 19-DEC-2017  JML    Fixed Bug 29490:WIGOS-4775 [Ticket #8366] Issue: Reporte Score por Hora - Formulas V03.06.0023
' 18-JUN-2018  AGS    Bug 33143:WIGOS-13054 Score report screen fails to load data
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class frm_gaming_tables_score_report
  Inherits frm_base_sel


#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_TYPE_NAME As Integer = 0
  Private Const SQL_COLUMN_TABLE_NAME As Integer = 1
  Private Const SQL_COLUMN_OPENING_DATE As Integer = 2
  Private Const SQL_COLUMN_CLOSING_DATE As Integer = 3
  Private Const SQL_COLUMN_GAMING_TABLE_SESSION_ID As Integer = 4
  Private Const SQL_COLUMN_SESION_NAME As Integer = 5
  Private Const SQL_COLUMN_INITIAL_BALANCE As Integer = 6
  Private Const SQL_COLUMN_FINAL_BALANCE As Integer = 7
  Private Const SQL_COLUMN_NUM_SEATS As Integer = 8
  Private Const SQL_COLUMN_0000_DROP As Integer = 9
  Private Const SQL_COLUMN_0000_WIN_LOSS As Integer = 10
  Private Const SQL_COLUMN_0000_HOLD As Integer = 11
  Private Const SQL_COLUMN_0000_OCCUPATION As Integer = 12
  Private Const SQL_COLUMN_0100_DROP As Integer = 13
  Private Const SQL_COLUMN_0100_WIN_LOSS As Integer = 14
  Private Const SQL_COLUMN_0100_HOLD As Integer = 15
  Private Const SQL_COLUMN_0100_OCCUPATION As Integer = 16
  Private Const SQL_COLUMN_0200_DROP As Integer = 17
  Private Const SQL_COLUMN_0200_WIN_LOSS As Integer = 18
  Private Const SQL_COLUMN_0200_HOLD As Integer = 19
  Private Const SQL_COLUMN_0200_OCCUPATION As Integer = 20
  Private Const SQL_COLUMN_0300_DROP As Integer = 21
  Private Const SQL_COLUMN_0300_WIN_LOSS As Integer = 22
  Private Const SQL_COLUMN_0300_HOLD As Integer = 23
  Private Const SQL_COLUMN_0300_OCCUPATION As Integer = 24
  Private Const SQL_COLUMN_0400_DROP As Integer = 25
  Private Const SQL_COLUMN_0400_WIN_LOSS As Integer = 26
  Private Const SQL_COLUMN_0400_HOLD As Integer = 27
  Private Const SQL_COLUMN_0400_OCCUPATION As Integer = 28
  Private Const SQL_COLUMN_0500_DROP As Integer = 29
  Private Const SQL_COLUMN_0500_WIN_LOSS As Integer = 30
  Private Const SQL_COLUMN_0500_HOLD As Integer = 31
  Private Const SQL_COLUMN_0500_OCCUPATION As Integer = 32
  Private Const SQL_COLUMN_0600_DROP As Integer = 33
  Private Const SQL_COLUMN_0600_WIN_LOSS As Integer = 34
  Private Const SQL_COLUMN_0600_HOLD As Integer = 35
  Private Const SQL_COLUMN_0600_OCCUPATION As Integer = 36
  Private Const SQL_COLUMN_0700_DROP As Integer = 37
  Private Const SQL_COLUMN_0700_WIN_LOSS As Integer = 38
  Private Const SQL_COLUMN_0700_HOLD As Integer = 39
  Private Const SQL_COLUMN_0700_OCCUPATION As Integer = 40
  Private Const SQL_COLUMN_0800_DROP As Integer = 41
  Private Const SQL_COLUMN_0800_WIN_LOSS As Integer = 42
  Private Const SQL_COLUMN_0800_HOLD As Integer = 43
  Private Const SQL_COLUMN_0800_OCCUPATION As Integer = 44
  Private Const SQL_COLUMN_0900_DROP As Integer = 45
  Private Const SQL_COLUMN_0900_WIN_LOSS As Integer = 46
  Private Const SQL_COLUMN_0900_HOLD As Integer = 47
  Private Const SQL_COLUMN_0900_OCCUPATION As Integer = 48
  Private Const SQL_COLUMN_1000_DROP As Integer = 49
  Private Const SQL_COLUMN_1000_WIN_LOSS As Integer = 50
  Private Const SQL_COLUMN_1000_HOLD As Integer = 51
  Private Const SQL_COLUMN_1000_OCCUPATION As Integer = 52
  Private Const SQL_COLUMN_1100_DROP As Integer = 53
  Private Const SQL_COLUMN_1100_WIN_LOSS As Integer = 54
  Private Const SQL_COLUMN_1100_HOLD As Integer = 55
  Private Const SQL_COLUMN_1100_OCCUPATION As Integer = 56
  Private Const SQL_COLUMN_1200_DROP As Integer = 57
  Private Const SQL_COLUMN_1200_WIN_LOSS As Integer = 58
  Private Const SQL_COLUMN_1200_HOLD As Integer = 59
  Private Const SQL_COLUMN_1200_OCCUPATION As Integer = 60
  Private Const SQL_COLUMN_1300_DROP As Integer = 61
  Private Const SQL_COLUMN_1300_WIN_LOSS As Integer = 62
  Private Const SQL_COLUMN_1300_HOLD As Integer = 63
  Private Const SQL_COLUMN_1300_OCCUPATION As Integer = 64
  Private Const SQL_COLUMN_1400_DROP As Integer = 65
  Private Const SQL_COLUMN_1400_WIN_LOSS As Integer = 66
  Private Const SQL_COLUMN_1400_HOLD As Integer = 67
  Private Const SQL_COLUMN_1400_OCCUPATION As Integer = 68
  Private Const SQL_COLUMN_1500_DROP As Integer = 69
  Private Const SQL_COLUMN_1500_WIN_LOSS As Integer = 70
  Private Const SQL_COLUMN_1500_HOLD As Integer = 71
  Private Const SQL_COLUMN_1500_OCCUPATION As Integer = 72
  Private Const SQL_COLUMN_1600_DROP As Integer = 73
  Private Const SQL_COLUMN_1600_WIN_LOSS As Integer = 74
  Private Const SQL_COLUMN_1600_HOLD As Integer = 75
  Private Const SQL_COLUMN_1600_OCCUPATION As Integer = 76
  Private Const SQL_COLUMN_1700_DROP As Integer = 77
  Private Const SQL_COLUMN_1700_WIN_LOSS As Integer = 78
  Private Const SQL_COLUMN_1700_HOLD As Integer = 79
  Private Const SQL_COLUMN_1700_OCCUPATION As Integer = 80
  Private Const SQL_COLUMN_1800_DROP As Integer = 81
  Private Const SQL_COLUMN_1800_WIN_LOSS As Integer = 82
  Private Const SQL_COLUMN_1800_HOLD As Integer = 83
  Private Const SQL_COLUMN_1800_OCCUPATION As Integer = 84
  Private Const SQL_COLUMN_1900_DROP As Integer = 85
  Private Const SQL_COLUMN_1900_WIN_LOSS As Integer = 86
  Private Const SQL_COLUMN_1900_HOLD As Integer = 87
  Private Const SQL_COLUMN_1900_OCCUPATION As Integer = 88
  Private Const SQL_COLUMN_2000_DROP As Integer = 89
  Private Const SQL_COLUMN_2000_WIN_LOSS As Integer = 90
  Private Const SQL_COLUMN_2000_HOLD As Integer = 91
  Private Const SQL_COLUMN_2000_OCCUPATION As Integer = 92
  Private Const SQL_COLUMN_2100_DROP As Integer = 93
  Private Const SQL_COLUMN_2100_WIN_LOSS As Integer = 94
  Private Const SQL_COLUMN_2100_HOLD As Integer = 95
  Private Const SQL_COLUMN_2100_OCCUPATION As Integer = 96
  Private Const SQL_COLUMN_2200_DROP As Integer = 97
  Private Const SQL_COLUMN_2200_WIN_LOSS As Integer = 98
  Private Const SQL_COLUMN_2200_HOLD As Integer = 99
  Private Const SQL_COLUMN_2200_OCCUPATION As Integer = 100
  Private Const SQL_COLUMN_2300_DROP As Integer = 101
  Private Const SQL_COLUMN_2300_WIN_LOSS As Integer = 102
  Private Const SQL_COLUMN_2300_HOLD As Integer = 103
  Private Const SQL_COLUMN_2300_OCCUPATION As Integer = 104
  Private Const SQL_COLUMN_9999_DROP As Integer = 105
  Private Const SQL_COLUMN_9999_WIN_LOSS As Integer = 106
  Private Const SQL_COLUMN_9999_HOLD As Integer = 107
  Private Const SQL_COLUMN_9999_OCCUPATION As Integer = 108


  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_TYPE_NAME As Integer = 1
  Private Const GRID_COLUMN_SESION_NAME As Integer = 2
  Private Const GRID_COLUMN_INICIAL_DROP As Integer = 3
  Private Const GRID_COLUMN_INICIAL_WIN_LOSS As Integer = 4
  Private Const GRID_COLUMN_INICIAL_HOLD As Integer = 5
  Private Const GRID_COLUMN_INICIAL_OCCUPATION As Integer = 6
  Private Const GRID_COLUMN_0000_DROP As Integer = 7
  Private Const GRID_COLUMN_0000_WIN_LOSS As Integer = 8
  Private Const GRID_COLUMN_0000_HOLD As Integer = 9
  Private Const GRID_COLUMN_0000_OCCUPATION As Integer = 10
  Private Const GRID_COLUMN_0100_DROP As Integer = 11
  Private Const GRID_COLUMN_0100_WIN_LOSS As Integer = 12
  Private Const GRID_COLUMN_0100_HOLD As Integer = 13
  Private Const GRID_COLUMN_0100_OCCUPATION As Integer = 14
  Private Const GRID_COLUMN_0200_DROP As Integer = 15
  Private Const GRID_COLUMN_0200_WIN_LOSS As Integer = 16
  Private Const GRID_COLUMN_0200_HOLD As Integer = 17
  Private Const GRID_COLUMN_0200_OCCUPATION As Integer = 18
  Private Const GRID_COLUMN_0300_DROP As Integer = 19
  Private Const GRID_COLUMN_0300_WIN_LOSS As Integer = 20
  Private Const GRID_COLUMN_0300_HOLD As Integer = 21
  Private Const GRID_COLUMN_0300_OCCUPATION As Integer = 22
  Private Const GRID_COLUMN_0400_DROP As Integer = 23
  Private Const GRID_COLUMN_0400_WIN_LOSS As Integer = 24
  Private Const GRID_COLUMN_0400_HOLD As Integer = 25
  Private Const GRID_COLUMN_0400_OCCUPATION As Integer = 26
  Private Const GRID_COLUMN_0500_DROP As Integer = 27
  Private Const GRID_COLUMN_0500_WIN_LOSS As Integer = 28
  Private Const GRID_COLUMN_0500_HOLD As Integer = 29
  Private Const GRID_COLUMN_0500_OCCUPATION As Integer = 30
  Private Const GRID_COLUMN_0600_DROP As Integer = 31
  Private Const GRID_COLUMN_0600_WIN_LOSS As Integer = 32
  Private Const GRID_COLUMN_0600_HOLD As Integer = 33
  Private Const GRID_COLUMN_0600_OCCUPATION As Integer = 34
  Private Const GRID_COLUMN_0700_DROP As Integer = 35
  Private Const GRID_COLUMN_0700_WIN_LOSS As Integer = 36
  Private Const GRID_COLUMN_0700_HOLD As Integer = 37
  Private Const GRID_COLUMN_0700_OCCUPATION As Integer = 38
  Private Const GRID_COLUMN_0800_DROP As Integer = 39
  Private Const GRID_COLUMN_0800_WIN_LOSS As Integer = 40
  Private Const GRID_COLUMN_0800_HOLD As Integer = 41
  Private Const GRID_COLUMN_0800_OCCUPATION As Integer = 42
  Private Const GRID_COLUMN_0900_DROP As Integer = 43
  Private Const GRID_COLUMN_0900_WIN_LOSS As Integer = 44
  Private Const GRID_COLUMN_0900_HOLD As Integer = 45
  Private Const GRID_COLUMN_0900_OCCUPATION As Integer = 46
  Private Const GRID_COLUMN_1000_DROP As Integer = 47
  Private Const GRID_COLUMN_1000_WIN_LOSS As Integer = 48
  Private Const GRID_COLUMN_1000_HOLD As Integer = 49
  Private Const GRID_COLUMN_1000_OCCUPATION As Integer = 50
  Private Const GRID_COLUMN_1100_DROP As Integer = 51
  Private Const GRID_COLUMN_1100_WIN_LOSS As Integer = 52
  Private Const GRID_COLUMN_1100_HOLD As Integer = 53
  Private Const GRID_COLUMN_1100_OCCUPATION As Integer = 54
  Private Const GRID_COLUMN_1200_DROP As Integer = 55
  Private Const GRID_COLUMN_1200_WIN_LOSS As Integer = 56
  Private Const GRID_COLUMN_1200_HOLD As Integer = 57
  Private Const GRID_COLUMN_1200_OCCUPATION As Integer = 58
  Private Const GRID_COLUMN_1300_DROP As Integer = 59
  Private Const GRID_COLUMN_1300_WIN_LOSS As Integer = 60
  Private Const GRID_COLUMN_1300_HOLD As Integer = 61
  Private Const GRID_COLUMN_1300_OCCUPATION As Integer = 62
  Private Const GRID_COLUMN_1400_DROP As Integer = 63
  Private Const GRID_COLUMN_1400_WIN_LOSS As Integer = 64
  Private Const GRID_COLUMN_1400_HOLD As Integer = 65
  Private Const GRID_COLUMN_1400_OCCUPATION As Integer = 66
  Private Const GRID_COLUMN_1500_DROP As Integer = 67
  Private Const GRID_COLUMN_1500_WIN_LOSS As Integer = 68
  Private Const GRID_COLUMN_1500_HOLD As Integer = 69
  Private Const GRID_COLUMN_1500_OCCUPATION As Integer = 70
  Private Const GRID_COLUMN_1600_DROP As Integer = 71
  Private Const GRID_COLUMN_1600_WIN_LOSS As Integer = 72
  Private Const GRID_COLUMN_1600_HOLD As Integer = 73
  Private Const GRID_COLUMN_1600_OCCUPATION As Integer = 74
  Private Const GRID_COLUMN_1700_DROP As Integer = 75
  Private Const GRID_COLUMN_1700_WIN_LOSS As Integer = 76
  Private Const GRID_COLUMN_1700_HOLD As Integer = 77
  Private Const GRID_COLUMN_1700_OCCUPATION As Integer = 78
  Private Const GRID_COLUMN_1800_DROP As Integer = 79
  Private Const GRID_COLUMN_1800_WIN_LOSS As Integer = 80
  Private Const GRID_COLUMN_1800_HOLD As Integer = 81
  Private Const GRID_COLUMN_1800_OCCUPATION As Integer = 82
  Private Const GRID_COLUMN_1900_DROP As Integer = 83
  Private Const GRID_COLUMN_1900_WIN_LOSS As Integer = 84
  Private Const GRID_COLUMN_1900_HOLD As Integer = 85
  Private Const GRID_COLUMN_1900_OCCUPATION As Integer = 86
  Private Const GRID_COLUMN_2000_DROP As Integer = 87
  Private Const GRID_COLUMN_2000_WIN_LOSS As Integer = 88
  Private Const GRID_COLUMN_2000_HOLD As Integer = 89
  Private Const GRID_COLUMN_2000_OCCUPATION As Integer = 90
  Private Const GRID_COLUMN_2100_DROP As Integer = 91
  Private Const GRID_COLUMN_2100_WIN_LOSS As Integer = 92
  Private Const GRID_COLUMN_2100_HOLD As Integer = 93
  Private Const GRID_COLUMN_2100_OCCUPATION As Integer = 94
  Private Const GRID_COLUMN_2200_DROP As Integer = 95
  Private Const GRID_COLUMN_2200_WIN_LOSS As Integer = 96
  Private Const GRID_COLUMN_2200_HOLD As Integer = 97
  Private Const GRID_COLUMN_2200_OCCUPATION As Integer = 98
  Private Const GRID_COLUMN_2300_DROP As Integer = 99
  Private Const GRID_COLUMN_2300_WIN_LOSS As Integer = 100
  Private Const GRID_COLUMN_2300_HOLD As Integer = 101
  Private Const GRID_COLUMN_2300_OCCUPATION As Integer = 102
  Private Const GRID_COLUMN_FINAL_DROP As Integer = 103
  Private Const GRID_COLUMN_FINAL_WIN_LOSS As Integer = 104
  Private Const GRID_COLUMN_FINAL_HOLD As Integer = 105
  Private Const GRID_COLUMN_FINAL_OCCUPATION As Integer = 106
  Private Const GRID_COLUMN_TOTAL_DROP As Integer = 107
  Private Const GRID_COLUMN_TOTAL_WIN_LOSS As Integer = 108
  Private Const GRID_COLUMN_TOTAL_HOLD As Integer = 109
  Private Const GRID_COLUMN_TOTAL_OCCUPATION As Integer = 110

  Private Const GRID_COLUMNS As Integer = 111
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_TYPE_NAME As Integer = 2000
  Private Const GRID_WIDTH_SESION_NAME As Integer = 5000
  Private Const GRID_WIDTH_DROP As Integer = 1500
  Private Const GRID_WIDTH_WIN_LOSS As Integer = 1500
  Private Const GRID_WIDTH_HOLD As Integer = 1500
  Private Const GRID_WIDTH_OCCUPATION As Integer = 1500

#End Region ' Constants

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_closing_time_int As Integer
  Private m_closing_time_str As String
  Private m_opening_time As DateTime
  Private m_closing_time As DateTime
  Private m_initial_date As Date

  Dim m_total_drop As Decimal
  Dim m_total_win_loss As Decimal

  ' subtotals
  Dim m_sb_type As String
  Dim m_sb_cs_sesion As String
  Dim m_sb_num_seats As Decimal
  Dim m_sb_inicial_drop As Decimal
  Dim m_sb_inicial_win_loss As Decimal
  Dim m_sb_inicial_hold As Decimal
  Dim m_sb_inicial_occupation As Decimal
  Dim m_sb_0000_drop As Decimal
  Dim m_sb_0000_win_loss As Decimal
  Dim m_sb_0000_hold As Decimal
  Dim m_sb_0000_occupation As Decimal
  Dim m_sb_0100_drop As Decimal
  Dim m_sb_0100_win_loss As Decimal
  Dim m_sb_0100_hold As Decimal
  Dim m_sb_0100_occupation As Decimal
  Dim m_sb_0200_drop As Decimal
  Dim m_sb_0200_win_loss As Decimal
  Dim m_sb_0200_hold As Decimal
  Dim m_sb_0200_occupation As Decimal
  Dim m_sb_0300_drop As Decimal
  Dim m_sb_0300_win_loss As Decimal
  Dim m_sb_0300_hold As Decimal
  Dim m_sb_0300_occupation As Decimal
  Dim m_sb_0400_drop As Decimal
  Dim m_sb_0400_win_loss As Decimal
  Dim m_sb_0400_hold As Decimal
  Dim m_sb_0400_occupation As Decimal
  Dim m_sb_0500_drop As Decimal
  Dim m_sb_0500_win_loss As Decimal
  Dim m_sb_0500_hold As Decimal
  Dim m_sb_0500_occupation As Decimal
  Dim m_sb_0600_drop As Decimal
  Dim m_sb_0600_win_loss As Decimal
  Dim m_sb_0600_hold As Decimal
  Dim m_sb_0600_occupation As Decimal
  Dim m_sb_0700_drop As Decimal
  Dim m_sb_0700_win_loss As Decimal
  Dim m_sb_0700_hold As Decimal
  Dim m_sb_0700_occupation As Decimal
  Dim m_sb_0800_drop As Decimal
  Dim m_sb_0800_win_loss As Decimal
  Dim m_sb_0800_hold As Decimal
  Dim m_sb_0800_occupation As Decimal
  Dim m_sb_0900_drop As Decimal
  Dim m_sb_0900_win_loss As Decimal
  Dim m_sb_0900_hold As Decimal
  Dim m_sb_0900_occupation As Decimal
  Dim m_sb_1000_drop As Decimal
  Dim m_sb_1000_win_loss As Decimal
  Dim m_sb_1000_hold As Decimal
  Dim m_sb_1000_occupation As Decimal
  Dim m_sb_1100_drop As Decimal
  Dim m_sb_1100_win_loss As Decimal
  Dim m_sb_1100_hold As Decimal
  Dim m_sb_1100_occupation As Decimal
  Dim m_sb_1200_drop As Decimal
  Dim m_sb_1200_win_loss As Decimal
  Dim m_sb_1200_hold As Decimal
  Dim m_sb_1200_occupation As Decimal
  Dim m_sb_1300_drop As Decimal
  Dim m_sb_1300_win_loss As Decimal
  Dim m_sb_1300_hold As Decimal
  Dim m_sb_1300_occupation As Decimal
  Dim m_sb_1400_drop As Decimal
  Dim m_sb_1400_win_loss As Decimal
  Dim m_sb_1400_hold As Decimal
  Dim m_sb_1400_occupation As Decimal
  Dim m_sb_1500_drop As Decimal
  Dim m_sb_1500_win_loss As Decimal
  Dim m_sb_1500_hold As Decimal
  Dim m_sb_1500_occupation As Decimal
  Dim m_sb_1600_drop As Decimal
  Dim m_sb_1600_win_loss As Decimal
  Dim m_sb_1600_hold As Decimal
  Dim m_sb_1600_occupation As Decimal
  Dim m_sb_1700_drop As Decimal
  Dim m_sb_1700_win_loss As Decimal
  Dim m_sb_1700_hold As Decimal
  Dim m_sb_1700_occupation As Decimal
  Dim m_sb_1800_drop As Decimal
  Dim m_sb_1800_win_loss As Decimal
  Dim m_sb_1800_hold As Decimal
  Dim m_sb_1800_occupation As Decimal
  Dim m_sb_1900_drop As Decimal
  Dim m_sb_1900_win_loss As Decimal
  Dim m_sb_1900_hold As Decimal
  Dim m_sb_1900_occupation As Decimal
  Dim m_sb_2000_drop As Decimal
  Dim m_sb_2000_win_loss As Decimal
  Dim m_sb_2000_hold As Decimal
  Dim m_sb_2000_occupation As Decimal
  Dim m_sb_2100_drop As Decimal
  Dim m_sb_2100_win_loss As Decimal
  Dim m_sb_2100_hold As Decimal
  Dim m_sb_2100_occupation As Decimal
  Dim m_sb_2200_drop As Decimal
  Dim m_sb_2200_win_loss As Decimal
  Dim m_sb_2200_hold As Decimal
  Dim m_sb_2200_occupation As Decimal
  Dim m_sb_2300_drop As Decimal
  Dim m_sb_2300_win_loss As Decimal
  Dim m_sb_2300_hold As Decimal
  Dim m_sb_2300_occupation As Decimal
  Dim m_sb_final_drop As Decimal
  Dim m_sb_final_win_loss As Decimal
  Dim m_sb_final_hold As Decimal
  Dim m_sb_final_occupation As Decimal
  Dim m_sb_total_drop As Decimal
  Dim m_sb_total_win_loss As Decimal
  Dim m_sb_total_hold As Decimal
  Dim m_sb_total_occupation As Decimal

  ' Totals
  Dim m_tot_cs_sesion As String
  Dim m_tot_num_seats As Decimal
  Dim m_tot_inicial_drop As Decimal
  Dim m_tot_inicial_win_loss As Decimal
  Dim m_tot_inicial_hold As Decimal
  Dim m_tot_inicial_occupation As Decimal
  Dim m_tot_0000_drop As Decimal
  Dim m_tot_0000_win_loss As Decimal
  Dim m_tot_0000_hold As Decimal
  Dim m_tot_0000_occupation As Decimal
  Dim m_tot_0100_drop As Decimal
  Dim m_tot_0100_win_loss As Decimal
  Dim m_tot_0100_hold As Decimal
  Dim m_tot_0100_occupation As Decimal
  Dim m_tot_0200_drop As Decimal
  Dim m_tot_0200_win_loss As Decimal
  Dim m_tot_0200_hold As Decimal
  Dim m_tot_0200_occupation As Decimal
  Dim m_tot_0300_drop As Decimal
  Dim m_tot_0300_win_loss As Decimal
  Dim m_tot_0300_hold As Decimal
  Dim m_tot_0300_occupation As Decimal
  Dim m_tot_0400_drop As Decimal
  Dim m_tot_0400_win_loss As Decimal
  Dim m_tot_0400_hold As Decimal
  Dim m_tot_0400_occupation As Decimal
  Dim m_tot_0500_drop As Decimal
  Dim m_tot_0500_win_loss As Decimal
  Dim m_tot_0500_hold As Decimal
  Dim m_tot_0500_occupation As Decimal
  Dim m_tot_0600_drop As Decimal
  Dim m_tot_0600_win_loss As Decimal
  Dim m_tot_0600_hold As Decimal
  Dim m_tot_0600_occupation As Decimal
  Dim m_tot_0700_drop As Decimal
  Dim m_tot_0700_win_loss As Decimal
  Dim m_tot_0700_hold As Decimal
  Dim m_tot_0700_occupation As Decimal
  Dim m_tot_0800_drop As Decimal
  Dim m_tot_0800_win_loss As Decimal
  Dim m_tot_0800_hold As Decimal
  Dim m_tot_0800_occupation As Decimal
  Dim m_tot_0900_drop As Decimal
  Dim m_tot_0900_win_loss As Decimal
  Dim m_tot_0900_hold As Decimal
  Dim m_tot_0900_occupation As Decimal
  Dim m_tot_1000_drop As Decimal
  Dim m_tot_1000_win_loss As Decimal
  Dim m_tot_1000_hold As Decimal
  Dim m_tot_1000_occupation As Decimal
  Dim m_tot_1100_drop As Decimal
  Dim m_tot_1100_win_loss As Decimal
  Dim m_tot_1100_hold As Decimal
  Dim m_tot_1100_occupation As Decimal
  Dim m_tot_1200_drop As Decimal
  Dim m_tot_1200_win_loss As Decimal
  Dim m_tot_1200_hold As Decimal
  Dim m_tot_1200_occupation As Decimal
  Dim m_tot_1300_drop As Decimal
  Dim m_tot_1300_win_loss As Decimal
  Dim m_tot_1300_hold As Decimal
  Dim m_tot_1300_occupation As Decimal
  Dim m_tot_1400_drop As Decimal
  Dim m_tot_1400_win_loss As Decimal
  Dim m_tot_1400_hold As Decimal
  Dim m_tot_1400_occupation As Decimal
  Dim m_tot_1500_drop As Decimal
  Dim m_tot_1500_win_loss As Decimal
  Dim m_tot_1500_hold As Decimal
  Dim m_tot_1500_occupation As Decimal
  Dim m_tot_1600_drop As Decimal
  Dim m_tot_1600_win_loss As Decimal
  Dim m_tot_1600_hold As Decimal
  Dim m_tot_1600_occupation As Decimal
  Dim m_tot_1700_drop As Decimal
  Dim m_tot_1700_win_loss As Decimal
  Dim m_tot_1700_hold As Decimal
  Dim m_tot_1700_occupation As Decimal
  Dim m_tot_1800_drop As Decimal
  Dim m_tot_1800_win_loss As Decimal
  Dim m_tot_1800_hold As Decimal
  Dim m_tot_1800_occupation As Decimal
  Dim m_tot_1900_drop As Decimal
  Dim m_tot_1900_win_loss As Decimal
  Dim m_tot_1900_hold As Decimal
  Dim m_tot_1900_occupation As Decimal
  Dim m_tot_2000_drop As Decimal
  Dim m_tot_2000_win_loss As Decimal
  Dim m_tot_2000_hold As Decimal
  Dim m_tot_2000_occupation As Decimal
  Dim m_tot_2100_drop As Decimal
  Dim m_tot_2100_win_loss As Decimal
  Dim m_tot_2100_hold As Decimal
  Dim m_tot_2100_occupation As Decimal
  Dim m_tot_2200_drop As Decimal
  Dim m_tot_2200_win_loss As Decimal
  Dim m_tot_2200_hold As Decimal
  Dim m_tot_2200_occupation As Decimal
  Dim m_tot_2300_drop As Decimal
  Dim m_tot_2300_win_loss As Decimal
  Dim m_tot_2300_hold As Decimal
  Dim m_tot_2300_occupation As Decimal
  Dim m_tot_final_drop As Decimal
  Dim m_tot_final_win_loss As Decimal
  Dim m_tot_final_hold As Decimal
  Dim m_tot_final_occupation As Decimal
  Dim m_tot_total_drop As Decimal
  Dim m_tot_total_win_loss As Decimal
  Dim m_tot_total_hold As Decimal
  Dim m_tot_total_occupation As Decimal

  Dim m_gambling_rows_per_type As Integer
  Dim m_num_rows As Integer

  Dim m_first_time As DateTime
  Dim m_last_time As DateTime

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_SCORE_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8458)

    Me.IgnoreRowHeightFilterInExcelReport = True

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True, True)
    uc_dsl.ClosingTimeEnabled = True
    uc_dsl.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
    uc_dsl.FromDateText = String.Empty
    uc_dsl.ToDateText = String.Empty

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()

    Call ResetTotals()

    Call ResetSubTotalBySession()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    '' Date selection 
    'If Me.dtp_from.Checked And Me.dtp_to.Checked Then
    '  If Me.dtp_from.Value > Me.dtp_to.Value Then
    '    Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
    '    Call Me.dtp_from.Focus()

    '    Return False
    '  End If
    'End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _date_time_from As Date

    _date_time_from = New Date(uc_dsl.FromDate.Year, uc_dsl.FromDate.Month, uc_dsl.FromDate.Day, uc_dsl.ClosingTime, 0, 0)

    Call GUI_StyleSheet()

    _sql_cmd = New SqlCommand("ScoreReport")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _date_time_from
    _sql_cmd.Parameters.Add("@pCageFillerIn", SqlDbType.Int).Value = CType(CASHIER_MOVEMENT.CAGE_FILLER_IN, Integer)
    _sql_cmd.Parameters.Add("@pCageFillerOut", SqlDbType.Int).Value = CType(CASHIER_MOVEMENT.CAGE_FILLER_OUT, Integer)
    _sql_cmd.Parameters.Add("@pChipRE", SqlDbType.Int).Value = CType(CurrencyExchangeType.CASINO_CHIP_RE, Integer)
    _sql_cmd.Parameters.Add("@pSaleChips", SqlDbType.Int).Value = CType(PlayerTrackingMovementType.SaleChips, Integer)

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    If _table Is Nothing Then
      Return
    End If

    m_num_rows = _table.Rows.Count

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim idx_row As Integer

    If _table.Rows.Count > 0 Then
      For Each _row In _table.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_gaming_tables_score_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If

  End Sub


  ''' <summary>
  ''' Checks out the DB row before adding it to the grid and process counters (& totals rows)
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns>True (the data row can be added) or False (the data row can not be added)</returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _temp_number As Decimal
    Dim _number_seats As Decimal
    Dim _accumulate_occupation As Decimal
    Dim _reported_hours As Integer
    Dim _last_ocupation As Decimal
    Dim _date_time_from As Date

    _accumulate_occupation = 0
    _reported_hours = 0
    _last_ocupation = 0
    _date_time_from = New Date(uc_dsl.FromDate.Year, uc_dsl.FromDate.Month, uc_dsl.FromDate.Day, uc_dsl.ClosingTime, 0, 0)

    If (IsDBNull(DbRow.Value(SQL_COLUMN_CLOSING_DATE))) Then
      m_closing_time = New Date(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, WGDB.Now.Hour, WGDB.Now.Minute, WGDB.Now.Second)
      If m_closing_time > _date_time_from.AddDays(1) Then
        m_closing_time = _date_time_from.AddDays(1)
      End If
    Else
      m_closing_time = DbRow.Value(SQL_COLUMN_CLOSING_DATE)
    End If

    If (IsDBNull(DbRow.Value(SQL_COLUMN_OPENING_DATE))) Then
      m_opening_time = _date_time_from
    Else
      m_opening_time = DbRow.Value(SQL_COLUMN_OPENING_DATE)
    End If

    If (m_first_time > m_opening_time) Then
      m_first_time = New Date(m_opening_time.Year, m_opening_time.Month, m_opening_time.Day, m_opening_time.Hour, 0, 0)
    End If

    If (m_last_time < m_closing_time) Then
      m_last_time = New Date(m_closing_time.Year, m_closing_time.Month, m_closing_time.Day, m_closing_time.Hour, 0, 0)
    End If

    m_total_drop = DbRow.Value(SQL_COLUMN_0000_DROP) _
                 + DbRow.Value(SQL_COLUMN_0100_DROP) _
                 + DbRow.Value(SQL_COLUMN_0200_DROP) _
                 + DbRow.Value(SQL_COLUMN_0300_DROP) _
                 + DbRow.Value(SQL_COLUMN_0400_DROP) _
                 + DbRow.Value(SQL_COLUMN_0500_DROP) _
                 + DbRow.Value(SQL_COLUMN_0600_DROP) _
                 + DbRow.Value(SQL_COLUMN_0700_DROP) _
                 + DbRow.Value(SQL_COLUMN_0800_DROP) _
                 + DbRow.Value(SQL_COLUMN_0900_DROP) _
                 + DbRow.Value(SQL_COLUMN_1000_DROP) _
                 + DbRow.Value(SQL_COLUMN_1100_DROP) _
                 + DbRow.Value(SQL_COLUMN_1200_DROP) _
                 + DbRow.Value(SQL_COLUMN_1300_DROP) _
                 + DbRow.Value(SQL_COLUMN_1400_DROP) _
                 + DbRow.Value(SQL_COLUMN_1500_DROP) _
                 + DbRow.Value(SQL_COLUMN_1600_DROP) _
                 + DbRow.Value(SQL_COLUMN_1700_DROP) _
                 + DbRow.Value(SQL_COLUMN_1800_DROP) _
                 + DbRow.Value(SQL_COLUMN_1900_DROP) _
                 + DbRow.Value(SQL_COLUMN_2000_DROP) _
                 + DbRow.Value(SQL_COLUMN_2100_DROP) _
                 + DbRow.Value(SQL_COLUMN_2200_DROP) _
                 + DbRow.Value(SQL_COLUMN_2300_DROP)

    m_total_win_loss = DbRow.Value(SQL_COLUMN_0000_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0100_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0200_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0300_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0400_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0500_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0600_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0700_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0800_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_0900_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1000_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1100_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1200_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1300_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1400_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1500_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1600_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1700_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1800_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_1900_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_2000_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_2100_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_2200_WIN_LOSS) _
                     + DbRow.Value(SQL_COLUMN_2300_WIN_LOSS)

    If m_sb_type <> DbRow.Value(SQL_COLUMN_TYPE_NAME) And m_sb_type <> String.Empty Then
      AddSubTotalBySession(RowIndex)
      ResetSubTotalBySession()

      Me.Grid.AddRow()
      RowIndex += 1
    End If

    _number_seats = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NUM_SEATS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    If _number_seats = 0 Then
      _number_seats = 1
    End If

    ' Type_name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = DbRow.Value(SQL_COLUMN_TYPE_NAME)

    ' Session Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESION_NAME).Value = DbRow.Value(SQL_COLUMN_TABLE_NAME) & "-" & DbRow.Value(SQL_COLUMN_SESION_NAME)

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_DROP).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INITIAL_BALANCE), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_HOLD).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_OCCUPATION).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0000_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0000_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0000_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0000_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0000_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0100_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0100_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0100_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0100_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0100_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0200_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0200_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0200_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0200_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0200_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0300_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0300_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0300_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0300_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0300_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0400_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0400_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0400_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0400_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0400_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0500_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0500_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0500_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0500_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0500_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0600_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0600_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0600_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0600_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0600_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0700_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0700_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0700_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0700_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0700_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0800_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0800_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0800_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0800_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0800_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0900_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0900_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_0900_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_0900_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_0900_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1000_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1000_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1000_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1000_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1000_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1100_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1100_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1100_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1100_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1100_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1200_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1200_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1200_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1200_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1200_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1300_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1300_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1300_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1300_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1300_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1400_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1400_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1400_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1400_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1400_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1500_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1500_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1500_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1500_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1500_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1600_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1600_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1600_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1600_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1600_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1700_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1700_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1700_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1700_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1700_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1800_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1800_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1800_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1800_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1800_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1900_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1900_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_1900_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_1900_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_1900_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2000_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2000_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2000_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_2000_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_2000_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2100_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2100_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2100_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_2100_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_2100_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2200_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2200_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2200_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_2200_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_2200_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2300_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2300_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_2300_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = DbRow.Value(SQL_COLUMN_2300_OCCUPATION) / _number_seats
    If (_temp_number > 0) Then
      _reported_hours += 1
      _accumulate_occupation += _temp_number
      _last_ocupation = DbRow.Value(SQL_COLUMN_2300_OCCUPATION)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    If (m_closing_time = DateTime.MinValue) Then
      ' Drop
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_DROP).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' W/L
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_WIN_LOSS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'Hold
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_HOLD).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

      'Ocupation
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_OCCUPATION).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    Else
      ' Drop
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_DROP).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' W/L
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_WIN_LOSS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_9999_WIN_LOSS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'Hold
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_HOLD).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

      'Ocupation
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_OCCUPATION).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    End If

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_DROP).Value = GUI_FormatCurrency(m_total_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    _temp_number = m_total_win_loss
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_WIN_LOSS).Value = GUI_FormatCurrency(_temp_number, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_total_win_loss <> 0 Then
      m_tot_total_hold = m_total_drop / m_total_win_loss * 100
    Else
      m_tot_total_hold = 0
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HOLD).Value = GUI_FormatCurrency(m_tot_total_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = _accumulate_occupation
    If (_reported_hours = 0) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_OCCUPATION).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol
      AcumulateSubTotalBySession(DbRow, (_last_ocupation / _number_seats), 0)
      AcumulateTotals(DbRow, (_last_ocupation / _number_seats), 0)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / _reported_hours * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol
      AcumulateSubTotalBySession(DbRow, (_last_ocupation / _number_seats), (_accumulate_occupation / _reported_hours))
      AcumulateTotals(DbRow, (_last_ocupation / _number_seats), (_accumulate_occupation / _reported_hours))
    End If


    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE : Performs some actions after all rows have been painted
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    AddSubTotalBySession(_idx_row)

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    AddTotals(_idx_row)

    HideCeroHours()

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723), m_date_from)

    PrintData.FilterHeaderWidth(2) = 1600
    PrintData.FilterValueWidth(2) = 2000
    PrintData.FilterHeaderWidth(4) = 500
    PrintData.FilterValueWidth(4) = 1800

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8458)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _temp_date As Date
    m_date_from = ""
    m_date_to = ""

    'Date 
    _temp_date = New Date(uc_dsl.FromDate.Year, uc_dsl.FromDate.Month, uc_dsl.FromDate.Day, m_closing_time_int, 0, 0)

    m_date_from = GUI_FormatDate(_temp_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    _temp_date = New Date(uc_dsl.ToDate.Year, uc_dsl.ToDate.Month, uc_dsl.ToDate.Day, m_closing_time_int, 0, 0)

    m_date_to = GUI_FormatDate(_temp_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode with filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - SessionId:
  '           - SessionName:
  '           - DateFrom: Movements date from
  '           - DateTo: Movements date to
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal SessionId As Integer, _
                         ByVal SessionName As String, _
                         Optional ByVal DateFrom As Date = Nothing, _
                         Optional ByVal DateTo As Date = Nothing)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Sortable = False
    End With

    ' Index
    StyleColumn(GRID_COLUMN_INDEX, String.Empty, String.Empty, GRID_WIDTH_INDEX, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER)
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    StyleColumn(GRID_COLUMN_TYPE_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8483), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8484), GRID_WIDTH_TYPE_NAME, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER)
    StyleColumn(GRID_COLUMN_SESION_NAME, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8483), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8485), GRID_WIDTH_SESION_NAME, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER)
    StyleColumn(GRID_COLUMN_INICIAL_DROP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8490), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_INICIAL_WIN_LOSS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8490), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_INICIAL_HOLD, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8490), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_INICIAL_OCCUPATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8490), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)

    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0000_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0000_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0000_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0000_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0100_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0100_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0100_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0100_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0200_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0200_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0200_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0200_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0300_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0300_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0300_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0300_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0400_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0400_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0400_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0400_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0500_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0500_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0500_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0500_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0600_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0600_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0600_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0600_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0700_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0700_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0700_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0700_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0800_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0800_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0800_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0800_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_0900_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0900_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0900_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_0900_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1000_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1000_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1000_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1000_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1100_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1100_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1100_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1100_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1200_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1200_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1200_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1200_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1300_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1300_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1300_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1300_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1400_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1400_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1400_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1400_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1500_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1500_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1500_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1500_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1600_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1600_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1600_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1600_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1700_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1700_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1700_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1700_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1800_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1800_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1800_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1800_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_1900_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1900_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1900_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_1900_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_2000_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2000_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2000_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2000_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_2100_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2100_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2100_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2100_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_2200_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2200_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2200_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2200_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    StyleColumn(GRID_COLUMN_2300_DROP, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2300_WIN_LOSS, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2300_HOLD, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_2300_OCCUPATION, m_closing_time_str, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    m_initial_date = m_initial_date.AddHours(1)
    m_closing_time_str = GUI_FormatDate(m_initial_date, ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)

    StyleColumn(GRID_COLUMN_FINAL_DROP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8491), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_FINAL_WIN_LOSS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8491), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_FINAL_HOLD, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8491), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_FINAL_OCCUPATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8491), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_TOTAL_DROP, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8486), GRID_WIDTH_DROP, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_TOTAL_WIN_LOSS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8487), GRID_WIDTH_WIN_LOSS, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_TOTAL_HOLD, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8488), GRID_WIDTH_HOLD, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)
    StyleColumn(GRID_COLUMN_TOTAL_OCCUPATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8489), GRID_WIDTH_OCCUPATION, uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER)



  End Sub ' GUI_StyleSheet

  Private Sub StyleColumn(Column As Int32, Header0 As String, Header1 As String, Width As Int32, Alignement As uc_grid.CLASS_COL_DATA.ENUM_ALIGN)

    With Me.Grid
      .Column(Column).Header(0).Text = Header0
      .Column(Column).Header(1).Text = Header1
      .Column(Column).Width = Width
      .Column(Column).Alignment = Alignement
    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()
    Dim _now As Date

    _now = WGDB.Now

    m_closing_time_int = GetDefaultClosingTime()
    m_initial_date = New DateTime(_now.Year, _now.Month, _now.Day, m_closing_time_int, 0, 0)

    If m_initial_date > _now Then
      m_initial_date = m_initial_date.AddDays(-1)
    End If

    uc_dsl.FromDate = m_initial_date

    uc_dsl.ToDate = m_initial_date.AddDays(1)

    uc_dsl.ClosingTime = m_closing_time_int
    uc_dsl.ClosingTimeEnabled = False

  End Sub ' SetDefaultValues

  Public Sub AcumulateSubTotalBySession(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, ByVal FinalOccupation As Decimal, ByVal TotalOccupation As Decimal)
    Dim _hours_occupation_reported As Integer
    Dim _accumulate_occupation As Decimal

    m_gambling_rows_per_type += 1
    _accumulate_occupation = 0
    m_sb_num_seats = DbRow.Value(SQL_COLUMN_NUM_SEATS)

    ' Type
    m_sb_type = DbRow.Value(SQL_COLUMN_TYPE_NAME)

    ' Session Name
    m_sb_cs_sesion += DbRow.Value(SQL_COLUMN_TABLE_NAME) & DbRow.Value(SQL_COLUMN_SESION_NAME)

    ' Drop
    m_sb_inicial_drop = 0

    ' W/L
    m_sb_inicial_win_loss += DbRow.Value(SQL_COLUMN_INITIAL_BALANCE)

    'Hold
    m_sb_inicial_hold += 0

    'Ocupation
    m_sb_inicial_occupation += 0

    ' Drop
    m_sb_0000_drop += DbRow.Value(SQL_COLUMN_0000_DROP)

    ' W/L
    m_sb_0000_win_loss += DbRow.Value(SQL_COLUMN_0000_WIN_LOSS)

    'Hold
    m_sb_0000_hold += DbRow.Value(SQL_COLUMN_0000_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0000_occupation += DbRow.Value(SQL_COLUMN_0000_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0000_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0000_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0100_drop += DbRow.Value(SQL_COLUMN_0100_DROP)

    ' W/L
    m_sb_0100_win_loss += DbRow.Value(SQL_COLUMN_0100_WIN_LOSS)

    'Hold
    m_sb_0100_hold += DbRow.Value(SQL_COLUMN_0100_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0100_occupation += DbRow.Value(SQL_COLUMN_0100_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0100_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0100_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0200_drop += DbRow.Value(SQL_COLUMN_0200_DROP)

    ' W/L
    m_sb_0200_win_loss += DbRow.Value(SQL_COLUMN_0200_WIN_LOSS)

    'Hold
    m_sb_0200_hold += DbRow.Value(SQL_COLUMN_0200_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0200_occupation += DbRow.Value(SQL_COLUMN_0200_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0200_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0200_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0300_drop += DbRow.Value(SQL_COLUMN_0300_DROP)

    ' W/L
    m_sb_0300_win_loss += DbRow.Value(SQL_COLUMN_0300_WIN_LOSS)

    'Hold
    m_sb_0300_hold += DbRow.Value(SQL_COLUMN_0300_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0300_occupation += DbRow.Value(SQL_COLUMN_0300_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0300_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0300_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0400_drop += DbRow.Value(SQL_COLUMN_0400_DROP)

    ' W/L
    m_sb_0400_win_loss += DbRow.Value(SQL_COLUMN_0400_WIN_LOSS)

    'Hold
    m_sb_0400_hold += DbRow.Value(SQL_COLUMN_0400_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0400_occupation += DbRow.Value(SQL_COLUMN_0400_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0400_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0400_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0500_drop += DbRow.Value(SQL_COLUMN_0500_DROP)

    ' W/L
    m_sb_0500_win_loss += DbRow.Value(SQL_COLUMN_0500_WIN_LOSS)

    'Hold
    m_sb_0500_hold += DbRow.Value(SQL_COLUMN_0500_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0500_occupation += DbRow.Value(SQL_COLUMN_0500_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0500_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0500_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0600_drop += DbRow.Value(SQL_COLUMN_0600_DROP)

    ' W/L
    m_sb_0600_win_loss += DbRow.Value(SQL_COLUMN_0600_WIN_LOSS)

    'Hold
    m_sb_0600_hold += DbRow.Value(SQL_COLUMN_0600_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0600_occupation += DbRow.Value(SQL_COLUMN_0600_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0600_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0600_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0700_drop += DbRow.Value(SQL_COLUMN_0700_DROP)

    ' W/L
    m_sb_0700_win_loss += DbRow.Value(SQL_COLUMN_0700_WIN_LOSS)

    'Hold
    m_sb_0700_hold += DbRow.Value(SQL_COLUMN_0700_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0700_occupation += DbRow.Value(SQL_COLUMN_0700_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0700_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0700_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0800_drop += DbRow.Value(SQL_COLUMN_0800_DROP)

    ' W/L
    m_sb_0800_win_loss += DbRow.Value(SQL_COLUMN_0800_WIN_LOSS)

    'Hold
    m_sb_0800_hold += DbRow.Value(SQL_COLUMN_0800_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0800_occupation += DbRow.Value(SQL_COLUMN_0800_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0800_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0800_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_0900_drop += DbRow.Value(SQL_COLUMN_0900_DROP)

    ' W/L
    m_sb_0900_win_loss += DbRow.Value(SQL_COLUMN_0900_WIN_LOSS)

    'Hold
    m_sb_0900_hold += DbRow.Value(SQL_COLUMN_0900_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_0900_occupation += DbRow.Value(SQL_COLUMN_0900_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_0900_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_0900_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1000_drop += DbRow.Value(SQL_COLUMN_1000_DROP)

    ' W/L
    m_sb_1000_win_loss += DbRow.Value(SQL_COLUMN_1000_WIN_LOSS)

    'Hold
    m_sb_1000_hold += DbRow.Value(SQL_COLUMN_1000_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1000_occupation += DbRow.Value(SQL_COLUMN_1000_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1000_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1000_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1100_drop += DbRow.Value(SQL_COLUMN_1100_DROP)

    ' W/L
    m_sb_1100_win_loss += DbRow.Value(SQL_COLUMN_1100_WIN_LOSS)

    'Hold
    m_sb_1100_hold += DbRow.Value(SQL_COLUMN_1100_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1100_occupation += DbRow.Value(SQL_COLUMN_1100_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1100_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1100_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1200_drop += DbRow.Value(SQL_COLUMN_1200_DROP)

    ' W/L
    m_sb_1200_win_loss += DbRow.Value(SQL_COLUMN_1200_WIN_LOSS)

    'Hold
    m_sb_1200_hold += DbRow.Value(SQL_COLUMN_1200_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1200_occupation += DbRow.Value(SQL_COLUMN_1200_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1200_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1200_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1300_drop += DbRow.Value(SQL_COLUMN_1300_DROP)

    ' W/L
    m_sb_1300_win_loss += DbRow.Value(SQL_COLUMN_1300_WIN_LOSS)

    'Hold
    m_sb_1300_hold += DbRow.Value(SQL_COLUMN_1300_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1300_occupation += DbRow.Value(SQL_COLUMN_1300_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1300_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1300_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1400_drop += DbRow.Value(SQL_COLUMN_1400_DROP)

    ' W/L
    m_sb_1400_win_loss += DbRow.Value(SQL_COLUMN_1400_WIN_LOSS)

    'Hold
    m_sb_1400_hold += DbRow.Value(SQL_COLUMN_1400_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1400_occupation += DbRow.Value(SQL_COLUMN_1400_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1400_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1400_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1500_drop += DbRow.Value(SQL_COLUMN_1500_DROP)

    ' W/L
    m_sb_1500_win_loss += DbRow.Value(SQL_COLUMN_1500_WIN_LOSS)

    'Hold
    m_sb_1500_hold += DbRow.Value(SQL_COLUMN_1500_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1500_occupation += DbRow.Value(SQL_COLUMN_1500_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1500_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1500_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1600_drop += DbRow.Value(SQL_COLUMN_1600_DROP)

    ' W/L
    m_sb_1600_win_loss += DbRow.Value(SQL_COLUMN_1600_WIN_LOSS)

    'Hold
    m_sb_1600_hold += DbRow.Value(SQL_COLUMN_1600_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1600_occupation += DbRow.Value(SQL_COLUMN_1600_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1600_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1600_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1700_drop += DbRow.Value(SQL_COLUMN_1700_DROP)

    ' W/L
    m_sb_1700_win_loss += DbRow.Value(SQL_COLUMN_1700_WIN_LOSS)

    'Hold
    m_sb_1700_hold += DbRow.Value(SQL_COLUMN_1700_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1700_occupation += DbRow.Value(SQL_COLUMN_1700_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1700_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1700_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1800_drop += DbRow.Value(SQL_COLUMN_1800_DROP)

    ' W/L
    m_sb_1800_win_loss += DbRow.Value(SQL_COLUMN_1800_WIN_LOSS)

    'Hold
    m_sb_1800_hold += DbRow.Value(SQL_COLUMN_1800_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1800_occupation += DbRow.Value(SQL_COLUMN_1800_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1800_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1800_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_1900_drop += DbRow.Value(SQL_COLUMN_1900_DROP)

    ' W/L
    m_sb_1900_win_loss += DbRow.Value(SQL_COLUMN_1900_WIN_LOSS)

    'Hold
    m_sb_1900_hold += DbRow.Value(SQL_COLUMN_1900_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_1900_occupation += DbRow.Value(SQL_COLUMN_1900_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_1900_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_1900_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_2000_drop += DbRow.Value(SQL_COLUMN_2000_DROP)

    ' W/L
    m_sb_2000_win_loss += DbRow.Value(SQL_COLUMN_2000_WIN_LOSS)

    'Hold
    m_sb_2000_hold += DbRow.Value(SQL_COLUMN_2000_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_2000_occupation += DbRow.Value(SQL_COLUMN_2000_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_2000_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_2000_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_2100_drop += DbRow.Value(SQL_COLUMN_2100_DROP)

    ' W/L
    m_sb_2100_win_loss += DbRow.Value(SQL_COLUMN_2100_WIN_LOSS)

    'Hold
    m_sb_2100_hold += DbRow.Value(SQL_COLUMN_2100_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_2100_occupation += DbRow.Value(SQL_COLUMN_2100_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_2100_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_2100_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_2200_drop += DbRow.Value(SQL_COLUMN_2200_DROP)

    ' W/L
    m_sb_2200_win_loss += DbRow.Value(SQL_COLUMN_2200_WIN_LOSS)

    'Hold
    m_sb_2200_hold += DbRow.Value(SQL_COLUMN_2200_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_2200_occupation += DbRow.Value(SQL_COLUMN_2200_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_2200_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_2200_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    ' Drop
    m_sb_2300_drop += DbRow.Value(SQL_COLUMN_2300_DROP)

    ' W/L
    m_sb_2300_win_loss += DbRow.Value(SQL_COLUMN_2300_WIN_LOSS)

    'Hold
    m_sb_2300_hold += DbRow.Value(SQL_COLUMN_2300_HOLD)

    'Ocupation
    If m_sb_num_seats > 0 Then
      m_sb_2300_occupation += DbRow.Value(SQL_COLUMN_2300_OCCUPATION) / m_sb_num_seats
      If DbRow.Value(SQL_COLUMN_2300_OCCUPATION) > 0 Then
        _accumulate_occupation += DbRow.Value(SQL_COLUMN_2300_OCCUPATION) / m_sb_num_seats
        _hours_occupation_reported += 1
      End If
    End If

    If (m_closing_time = DateTime.MinValue) Then
      ' Drop
      m_sb_final_drop += 0

      ' W/L
      m_sb_final_win_loss += 0

      'Hold
      m_sb_final_hold += 0

      'Ocupation
      m_sb_final_occupation += 0

    Else
      m_sb_final_drop += 0

      ' W/L
      m_sb_final_win_loss += DbRow.Value(SQL_COLUMN_9999_WIN_LOSS)

      'Hold
      m_sb_final_hold += DbRow.Value(SQL_COLUMN_9999_HOLD)

      'Ocupation
      m_sb_final_occupation += 0
    End If

    ' Drop
    m_sb_total_drop += m_total_drop

    ' W/L
    m_sb_total_win_loss += m_total_win_loss

    'Hold
    m_sb_total_hold += DbRow.Value(SQL_COLUMN_9999_HOLD)

    'Ocupation
    If (_hours_occupation_reported = 0) Then
      m_sb_total_occupation += 0
    Else
      m_sb_total_occupation += _accumulate_occupation / _hours_occupation_reported
    End If


  End Sub

  Public Sub ResetSubTotalBySession()
    m_sb_cs_sesion = String.Empty
    m_sb_num_seats = 0
    m_sb_inicial_drop = 0
    m_sb_inicial_win_loss = 0
    m_sb_inicial_hold = 0
    m_sb_inicial_occupation = 0
    m_sb_0000_drop = 0
    m_sb_0000_win_loss = 0
    m_sb_0000_hold = 0
    m_sb_0000_occupation = 0
    m_sb_0100_drop = 0
    m_sb_0100_win_loss = 0
    m_sb_0100_hold = 0
    m_sb_0100_occupation = 0
    m_sb_0200_drop = 0
    m_sb_0200_win_loss = 0
    m_sb_0200_hold = 0
    m_sb_0200_occupation = 0
    m_sb_0300_drop = 0
    m_sb_0300_win_loss = 0
    m_sb_0300_hold = 0
    m_sb_0300_occupation = 0
    m_sb_0400_drop = 0
    m_sb_0400_win_loss = 0
    m_sb_0400_hold = 0
    m_sb_0400_occupation = 0
    m_sb_0500_drop = 0
    m_sb_0500_win_loss = 0
    m_sb_0500_hold = 0
    m_sb_0500_occupation = 0
    m_sb_0600_drop = 0
    m_sb_0600_win_loss = 0
    m_sb_0600_hold = 0
    m_sb_0600_occupation = 0
    m_sb_0700_drop = 0
    m_sb_0700_win_loss = 0
    m_sb_0700_hold = 0
    m_sb_0700_occupation = 0
    m_sb_0800_drop = 0
    m_sb_0800_win_loss = 0
    m_sb_0800_hold = 0
    m_sb_0800_occupation = 0
    m_sb_0900_drop = 0
    m_sb_0900_win_loss = 0
    m_sb_0900_hold = 0
    m_sb_0900_occupation = 0
    m_sb_1000_drop = 0
    m_sb_1000_win_loss = 0
    m_sb_1000_hold = 0
    m_sb_1000_occupation = 0
    m_sb_1100_drop = 0
    m_sb_1100_win_loss = 0
    m_sb_1100_hold = 0
    m_sb_1100_occupation = 0
    m_sb_1200_drop = 0
    m_sb_1200_win_loss = 0
    m_sb_1200_hold = 0
    m_sb_1200_occupation = 0
    m_sb_1300_drop = 0
    m_sb_1300_win_loss = 0
    m_sb_1300_hold = 0
    m_sb_1300_occupation = 0
    m_sb_1400_drop = 0
    m_sb_1400_win_loss = 0
    m_sb_1400_hold = 0
    m_sb_1400_occupation = 0
    m_sb_1500_drop = 0
    m_sb_1500_win_loss = 0
    m_sb_1500_hold = 0
    m_sb_1500_occupation = 0
    m_sb_1600_drop = 0
    m_sb_1600_win_loss = 0
    m_sb_1600_hold = 0
    m_sb_1600_occupation = 0
    m_sb_1700_drop = 0
    m_sb_1700_win_loss = 0
    m_sb_1700_hold = 0
    m_sb_1700_occupation = 0
    m_sb_1800_drop = 0
    m_sb_1800_win_loss = 0
    m_sb_1800_hold = 0
    m_sb_1800_occupation = 0
    m_sb_1900_drop = 0
    m_sb_1900_win_loss = 0
    m_sb_1900_hold = 0
    m_sb_1900_occupation = 0
    m_sb_2000_drop = 0
    m_sb_2000_win_loss = 0
    m_sb_2000_hold = 0
    m_sb_2000_occupation = 0
    m_sb_2100_drop = 0
    m_sb_2100_win_loss = 0
    m_sb_2100_hold = 0
    m_sb_2100_occupation = 0
    m_sb_2200_drop = 0
    m_sb_2200_win_loss = 0
    m_sb_2200_hold = 0
    m_sb_2200_occupation = 0
    m_sb_2300_drop = 0
    m_sb_2300_win_loss = 0
    m_sb_2300_hold = 0
    m_sb_2300_occupation = 0
    m_sb_final_drop = 0
    m_sb_final_win_loss = 0
    m_sb_final_hold = 0
    m_sb_final_occupation = 0
    m_sb_total_drop = 0
    m_sb_total_win_loss = 0
    m_sb_total_hold = 0
    m_sb_total_occupation = 0
  End Sub

  Private Sub AddSubTotalBySession(ByVal RowIndex As Integer)
    Dim _temp_number As Decimal
    Dim _acumulate_occupation As Decimal
    _acumulate_occupation = 0

    ' Set color
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    ' Session Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESION_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8492) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8484) & " " & m_sb_type

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_DROP).Value = GUI_FormatCurrency(m_sb_inicial_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_inicial_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_HOLD).Value = GUI_FormatCurrency(m_sb_inicial_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_OCCUPATION).Value = GUI_FormatCurrency(m_sb_inicial_occupation, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_DROP).Value = GUI_FormatCurrency(m_sb_0000_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0000_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0000_win_loss <> 0 Then
      m_sb_0000_hold = m_sb_0000_drop / m_sb_0000_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_HOLD).Value = GUI_FormatCurrency(m_sb_0000_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0000_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_DROP).Value = GUI_FormatCurrency(m_sb_0100_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0100_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0100_win_loss <> 0 Then
      m_sb_0100_hold = m_sb_0100_drop / m_sb_0100_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_HOLD).Value = GUI_FormatCurrency(m_sb_0100_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0100_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_DROP).Value = GUI_FormatCurrency(m_sb_0200_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0200_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0200_win_loss <> 0 Then
      m_sb_0200_hold = m_sb_0200_drop / m_sb_0200_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_HOLD).Value = GUI_FormatCurrency(m_sb_0200_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0200_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_DROP).Value = GUI_FormatCurrency(m_sb_0300_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0300_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0300_win_loss <> 0 Then
      m_sb_0300_hold = m_sb_0300_drop / m_sb_0300_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_HOLD).Value = GUI_FormatCurrency(m_sb_0300_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0300_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_DROP).Value = GUI_FormatCurrency(m_sb_0400_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0400_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0400_win_loss <> 0 Then
      m_sb_0400_hold = m_sb_0400_drop / m_sb_0400_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_HOLD).Value = GUI_FormatCurrency(m_sb_0400_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0400_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_DROP).Value = GUI_FormatCurrency(m_sb_0500_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0500_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0500_win_loss <> 0 Then
      m_sb_0500_hold = m_sb_0500_drop / m_sb_0500_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_HOLD).Value = GUI_FormatCurrency(m_sb_0500_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0500_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_DROP).Value = GUI_FormatCurrency(m_sb_0600_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0600_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0600_win_loss <> 0 Then
      m_sb_0600_hold = m_sb_0600_drop / m_sb_0600_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_HOLD).Value = GUI_FormatCurrency(m_sb_0600_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0600_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_DROP).Value = GUI_FormatCurrency(m_sb_0700_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0700_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0700_win_loss <> 0 Then
      m_sb_0700_hold = m_sb_0700_drop / m_sb_0700_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_HOLD).Value = GUI_FormatCurrency(m_sb_0700_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0700_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_DROP).Value = GUI_FormatCurrency(m_sb_0800_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0800_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0800_win_loss <> 0 Then
      m_sb_0800_hold = m_sb_0800_drop / m_sb_0800_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_HOLD).Value = GUI_FormatCurrency(m_sb_0800_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0800_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_DROP).Value = GUI_FormatCurrency(m_sb_0900_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_0900_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_0900_win_loss <> 0 Then
      m_sb_0900_hold = m_sb_0900_drop / m_sb_0900_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_HOLD).Value = GUI_FormatCurrency(m_sb_0900_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_0900_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_DROP).Value = GUI_FormatCurrency(m_sb_1000_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1000_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1000_win_loss <> 0 Then
      m_sb_1000_hold = m_sb_1000_drop / m_sb_1000_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_HOLD).Value = GUI_FormatCurrency(m_sb_1000_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1000_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_DROP).Value = GUI_FormatCurrency(m_sb_1100_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1100_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1100_win_loss <> 0 Then
      m_sb_1100_hold = m_sb_1100_drop / m_sb_1100_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_HOLD).Value = GUI_FormatCurrency(m_sb_1100_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1100_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_DROP).Value = GUI_FormatCurrency(m_sb_1200_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1200_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1200_win_loss <> 0 Then
      m_sb_1200_hold = m_sb_1200_drop / m_sb_1200_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_HOLD).Value = GUI_FormatCurrency(m_sb_1200_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1200_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_DROP).Value = GUI_FormatCurrency(m_sb_1300_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1300_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1300_win_loss <> 0 Then
      m_sb_1300_hold = m_sb_1300_drop / m_sb_1300_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_HOLD).Value = GUI_FormatCurrency(m_sb_1300_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1300_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_DROP).Value = GUI_FormatCurrency(m_sb_1400_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1400_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1400_win_loss <> 0 Then
      m_sb_1400_hold = m_sb_1400_drop / m_sb_1400_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_HOLD).Value = GUI_FormatCurrency(m_sb_1400_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1400_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_DROP).Value = GUI_FormatCurrency(m_sb_1500_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1500_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1500_win_loss <> 0 Then
      m_sb_1500_hold = m_sb_1500_drop / m_sb_1500_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_HOLD).Value = GUI_FormatCurrency(m_sb_1500_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1500_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_DROP).Value = GUI_FormatCurrency(m_sb_1600_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1600_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1600_win_loss <> 0 Then
      m_sb_1600_hold = m_sb_1600_drop / m_sb_1600_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_HOLD).Value = GUI_FormatCurrency(m_sb_1600_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1600_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_DROP).Value = GUI_FormatCurrency(m_sb_1700_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1700_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1700_win_loss <> 0 Then
      m_sb_1700_hold = m_sb_1700_drop / m_sb_1700_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_HOLD).Value = GUI_FormatCurrency(m_sb_1700_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1700_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_DROP).Value = GUI_FormatCurrency(m_sb_1800_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1800_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1800_win_loss <> 0 Then
      m_sb_1800_hold = m_sb_1800_drop / m_sb_1800_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_HOLD).Value = GUI_FormatCurrency(m_sb_1800_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1800_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_DROP).Value = GUI_FormatCurrency(m_sb_1900_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_1900_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_1900_win_loss <> 0 Then
      m_sb_1900_hold = m_sb_1900_drop / m_sb_1900_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_HOLD).Value = GUI_FormatCurrency(m_sb_1900_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_1900_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_DROP).Value = GUI_FormatCurrency(m_sb_2000_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_2000_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_2000_win_loss <> 0 Then
      m_sb_2000_hold = m_sb_2000_drop / m_sb_2000_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_HOLD).Value = GUI_FormatCurrency(m_sb_2000_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_2000_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_DROP).Value = GUI_FormatCurrency(m_sb_2100_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_2100_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_2100_win_loss <> 0 Then
      m_sb_2100_hold = m_sb_2100_drop / m_sb_2100_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_HOLD).Value = GUI_FormatCurrency(m_sb_2100_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_2100_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_DROP).Value = GUI_FormatCurrency(m_sb_2200_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_2200_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_2200_win_loss <> 0 Then
      m_sb_2200_hold = m_sb_2200_drop / m_sb_2200_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_HOLD).Value = GUI_FormatCurrency(m_sb_2200_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_2200_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_DROP).Value = GUI_FormatCurrency(m_sb_2300_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_2300_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_2300_win_loss <> 0 Then
      m_sb_2300_hold = m_sb_2300_drop / m_sb_2300_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_HOLD).Value = GUI_FormatCurrency(m_sb_2300_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_2300_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_gambling_rows_per_type * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_DROP).Value = GUI_FormatCurrency(m_sb_final_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_final_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_final_win_loss <> 0 Then
      m_sb_final_hold = m_sb_final_drop / m_sb_final_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_HOLD).Value = GUI_FormatCurrency(m_sb_final_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_final_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_OCCUPATION).Value = GUI_FormatCurrency((_temp_number / m_gambling_rows_per_type) * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_DROP).Value = GUI_FormatCurrency(m_sb_total_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_WIN_LOSS).Value = GUI_FormatCurrency(m_sb_total_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_sb_total_win_loss <> 0 Then
      m_sb_total_hold = m_sb_total_drop / m_sb_total_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HOLD).Value = GUI_FormatCurrency(m_sb_total_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_sb_total_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_OCCUPATION).Value = GUI_FormatCurrency((_temp_number / m_gambling_rows_per_type) * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    m_gambling_rows_per_type = 0

  End Sub

  Public Sub AcumulateTotals(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, ByVal FinalOccupation As Decimal, ByVal TotalOccupation As Decimal)
    Dim _number_seats As Decimal

    _number_seats = DbRow.Value(SQL_COLUMN_NUM_SEATS)
    If _number_seats = 0 Then
      _number_seats = 1
    End If

    m_tot_num_seats += _number_seats

    ' Session Name
    m_tot_cs_sesion += DbRow.Value(SQL_COLUMN_TABLE_NAME) & DbRow.Value(SQL_COLUMN_SESION_NAME)

    ' Drop
    m_tot_inicial_drop = 0

    ' W/L
    m_tot_inicial_win_loss += DbRow.Value(SQL_COLUMN_INITIAL_BALANCE)

    'Hold
    m_tot_inicial_hold += 0

    'Ocupation
    m_tot_inicial_occupation += 0

    ' Drop
    m_tot_0000_drop += DbRow.Value(SQL_COLUMN_0000_DROP)

    ' W/L
    m_tot_0000_win_loss += DbRow.Value(SQL_COLUMN_0000_WIN_LOSS)

    'Hold
    m_tot_0000_hold += DbRow.Value(SQL_COLUMN_0000_HOLD)

    'Ocupation
    m_tot_0000_occupation += DbRow.Value(SQL_COLUMN_0000_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0100_drop += DbRow.Value(SQL_COLUMN_0100_DROP)

    ' W/L
    m_tot_0100_win_loss += DbRow.Value(SQL_COLUMN_0100_WIN_LOSS)

    'Hold
    m_tot_0100_hold += DbRow.Value(SQL_COLUMN_0100_HOLD)

    'Ocupation
    m_tot_0100_occupation += DbRow.Value(SQL_COLUMN_0100_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0200_drop += DbRow.Value(SQL_COLUMN_0200_DROP)

    ' W/L
    m_tot_0200_win_loss += DbRow.Value(SQL_COLUMN_0200_WIN_LOSS)

    'Hold
    m_tot_0200_hold += DbRow.Value(SQL_COLUMN_0200_HOLD)

    'Ocupation
    m_tot_0200_occupation += DbRow.Value(SQL_COLUMN_0200_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0300_drop += DbRow.Value(SQL_COLUMN_0300_DROP)

    ' W/L
    m_tot_0300_win_loss += DbRow.Value(SQL_COLUMN_0300_WIN_LOSS)

    'Hold
    m_tot_0300_hold += DbRow.Value(SQL_COLUMN_0300_HOLD)

    'Ocupation
    m_tot_0300_occupation += DbRow.Value(SQL_COLUMN_0300_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0400_drop += DbRow.Value(SQL_COLUMN_0400_DROP)

    ' W/L
    m_tot_0400_win_loss += DbRow.Value(SQL_COLUMN_0400_WIN_LOSS)

    'Hold
    m_tot_0400_hold += DbRow.Value(SQL_COLUMN_0400_HOLD)

    'Ocupation
    m_tot_0400_occupation += DbRow.Value(SQL_COLUMN_0400_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0500_drop += DbRow.Value(SQL_COLUMN_0500_DROP)

    ' W/L
    m_tot_0500_win_loss += DbRow.Value(SQL_COLUMN_0500_WIN_LOSS)

    'Hold
    m_tot_0500_hold += DbRow.Value(SQL_COLUMN_0500_HOLD)

    'Ocupation
    m_tot_0500_occupation += DbRow.Value(SQL_COLUMN_0500_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0600_drop += DbRow.Value(SQL_COLUMN_0600_DROP)

    ' W/L
    m_tot_0600_win_loss += DbRow.Value(SQL_COLUMN_0600_WIN_LOSS)

    'Hold
    m_tot_0600_hold += DbRow.Value(SQL_COLUMN_0600_HOLD)

    'Ocupation
    m_tot_0600_occupation += DbRow.Value(SQL_COLUMN_0600_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0700_drop += DbRow.Value(SQL_COLUMN_0700_DROP)

    ' W/L
    m_tot_0700_win_loss += DbRow.Value(SQL_COLUMN_0700_WIN_LOSS)

    'Hold
    m_tot_0700_hold += DbRow.Value(SQL_COLUMN_0700_HOLD)

    'Ocupation
    m_tot_0700_occupation += DbRow.Value(SQL_COLUMN_0700_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0800_drop += DbRow.Value(SQL_COLUMN_0800_DROP)

    ' W/L
    m_tot_0800_win_loss += DbRow.Value(SQL_COLUMN_0800_WIN_LOSS)

    'Hold
    m_tot_0800_hold += DbRow.Value(SQL_COLUMN_0800_HOLD)

    'Ocupation
    m_tot_0800_occupation += DbRow.Value(SQL_COLUMN_0800_OCCUPATION) / _number_seats

    ' Drop
    m_tot_0900_drop += DbRow.Value(SQL_COLUMN_0900_DROP)

    ' W/L
    m_tot_0900_win_loss += DbRow.Value(SQL_COLUMN_0900_WIN_LOSS)

    'Hold
    m_tot_0900_hold += DbRow.Value(SQL_COLUMN_0900_HOLD)

    'Ocupation
    m_tot_0900_occupation += DbRow.Value(SQL_COLUMN_0900_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1000_drop += DbRow.Value(SQL_COLUMN_1000_DROP)

    ' W/L
    m_tot_1000_win_loss += DbRow.Value(SQL_COLUMN_1000_WIN_LOSS)

    'Hold
    m_tot_1000_hold += DbRow.Value(SQL_COLUMN_1000_HOLD)

    'Ocupation
    m_tot_1000_occupation += DbRow.Value(SQL_COLUMN_1000_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1100_drop += DbRow.Value(SQL_COLUMN_1100_DROP)

    ' W/L
    m_tot_1100_win_loss += DbRow.Value(SQL_COLUMN_1100_WIN_LOSS)

    'Hold
    m_tot_1100_hold += DbRow.Value(SQL_COLUMN_1100_HOLD)

    'Ocupation
    m_tot_1100_occupation += DbRow.Value(SQL_COLUMN_1100_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1200_drop += DbRow.Value(SQL_COLUMN_1200_DROP)

    ' W/L
    m_tot_1200_win_loss += DbRow.Value(SQL_COLUMN_1200_WIN_LOSS)

    'Hold
    m_tot_1200_hold += DbRow.Value(SQL_COLUMN_1200_HOLD)

    'Ocupation
    m_tot_1200_occupation += DbRow.Value(SQL_COLUMN_1200_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1300_drop += DbRow.Value(SQL_COLUMN_1300_DROP)

    ' W/L
    m_tot_1300_win_loss += DbRow.Value(SQL_COLUMN_1300_WIN_LOSS)

    'Hold
    m_tot_1300_hold += DbRow.Value(SQL_COLUMN_1300_HOLD)

    'Ocupation
    m_tot_1300_occupation += DbRow.Value(SQL_COLUMN_1300_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1400_drop += DbRow.Value(SQL_COLUMN_1400_DROP)

    ' W/L
    m_tot_1400_win_loss += DbRow.Value(SQL_COLUMN_1400_WIN_LOSS)

    'Hold
    m_tot_1400_hold += DbRow.Value(SQL_COLUMN_1400_HOLD)

    'Ocupation
    m_tot_1400_occupation += DbRow.Value(SQL_COLUMN_1400_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1500_drop += DbRow.Value(SQL_COLUMN_1500_DROP)

    ' W/L
    m_tot_1500_win_loss += DbRow.Value(SQL_COLUMN_1500_WIN_LOSS)

    'Hold
    m_tot_1500_hold += DbRow.Value(SQL_COLUMN_1500_HOLD)

    'Ocupation
    m_tot_1500_occupation += DbRow.Value(SQL_COLUMN_1500_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1600_drop += DbRow.Value(SQL_COLUMN_1600_DROP)

    ' W/L
    m_tot_1600_win_loss += DbRow.Value(SQL_COLUMN_1600_WIN_LOSS)

    'Hold
    m_tot_1600_hold += DbRow.Value(SQL_COLUMN_1600_HOLD)

    'Ocupation
    m_tot_1600_occupation += DbRow.Value(SQL_COLUMN_1600_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1700_drop += DbRow.Value(SQL_COLUMN_1700_DROP)

    ' W/L
    m_tot_1700_win_loss += DbRow.Value(SQL_COLUMN_1700_WIN_LOSS)

    'Hold
    m_tot_1700_hold += DbRow.Value(SQL_COLUMN_1700_HOLD)

    'Ocupation
    m_tot_1700_occupation += DbRow.Value(SQL_COLUMN_1700_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1800_drop += DbRow.Value(SQL_COLUMN_1800_DROP)

    ' W/L
    m_tot_1800_win_loss += DbRow.Value(SQL_COLUMN_1800_WIN_LOSS)

    'Hold
    m_tot_1800_hold += DbRow.Value(SQL_COLUMN_1800_HOLD)

    'Ocupation
    m_tot_1800_occupation += DbRow.Value(SQL_COLUMN_1800_OCCUPATION) / _number_seats

    ' Drop
    m_tot_1900_drop += DbRow.Value(SQL_COLUMN_1900_DROP)

    ' W/L
    m_tot_1900_win_loss += DbRow.Value(SQL_COLUMN_1900_WIN_LOSS)

    'Hold
    m_tot_1900_hold += DbRow.Value(SQL_COLUMN_1900_HOLD)

    'Ocupation
    m_tot_1900_occupation += DbRow.Value(SQL_COLUMN_1900_OCCUPATION) / _number_seats

    ' Drop
    m_tot_2000_drop += DbRow.Value(SQL_COLUMN_2000_DROP)

    ' W/L
    m_tot_2000_win_loss += DbRow.Value(SQL_COLUMN_2000_WIN_LOSS)

    'Hold
    m_tot_2000_hold += DbRow.Value(SQL_COLUMN_2000_HOLD)

    'Ocupation
    m_tot_2000_occupation += DbRow.Value(SQL_COLUMN_2000_OCCUPATION) / _number_seats

    ' Drop
    m_tot_2100_drop += DbRow.Value(SQL_COLUMN_2100_DROP)

    ' W/L
    m_tot_2100_win_loss += DbRow.Value(SQL_COLUMN_2100_WIN_LOSS)

    'Hold
    m_tot_2100_hold += DbRow.Value(SQL_COLUMN_2100_HOLD)

    'Ocupation
    m_tot_2100_occupation += DbRow.Value(SQL_COLUMN_2100_OCCUPATION) / _number_seats

    ' Drop
    m_tot_2200_drop += DbRow.Value(SQL_COLUMN_2200_DROP)

    ' W/L
    m_tot_2200_win_loss += DbRow.Value(SQL_COLUMN_2200_WIN_LOSS)

    'Hold
    m_tot_2200_hold += DbRow.Value(SQL_COLUMN_2200_HOLD)

    'Ocupation
    m_tot_2200_occupation += DbRow.Value(SQL_COLUMN_2200_OCCUPATION) / _number_seats

    ' Drop
    m_tot_2300_drop += DbRow.Value(SQL_COLUMN_2300_DROP)

    ' W/L
    m_tot_2300_win_loss += DbRow.Value(SQL_COLUMN_2300_WIN_LOSS)

    'Hold
    m_tot_2300_hold += DbRow.Value(SQL_COLUMN_2300_HOLD)

    'Ocupation
    m_tot_2300_occupation += DbRow.Value(SQL_COLUMN_2300_OCCUPATION) / _number_seats

    If (m_closing_time = DateTime.MinValue) Then
      m_tot_final_drop += 0

      ' W/L
      m_tot_final_win_loss += 0

      'Hold
      m_tot_final_hold += 0

      'Ocupation
      m_tot_final_occupation += 0

    Else
      m_tot_final_drop += 0

      ' W/L
      m_tot_final_win_loss += DbRow.Value(SQL_COLUMN_9999_WIN_LOSS)

      'Hold
      m_tot_final_hold += DbRow.Value(SQL_COLUMN_9999_HOLD)

      'Ocupation
      m_tot_final_occupation += 0

    End If

    ' Drop
    m_tot_total_drop += m_total_drop

    ' W/L
    m_tot_total_win_loss += m_total_win_loss

    'Hold
    m_tot_total_hold += DbRow.Value(SQL_COLUMN_9999_HOLD)

    'Ocupation
    m_tot_total_occupation += TotalOccupation

  End Sub

  Public Sub ResetTotals()
    m_sb_type = String.Empty
    m_tot_num_seats = 0
    m_tot_inicial_drop = 0
    m_tot_inicial_win_loss = 0
    m_tot_inicial_hold = 0
    m_tot_inicial_occupation = 0
    m_tot_0000_drop = 0
    m_tot_0000_win_loss = 0
    m_tot_0000_hold = 0
    m_tot_0000_occupation = 0
    m_tot_0100_drop = 0
    m_tot_0100_win_loss = 0
    m_tot_0100_hold = 0
    m_tot_0100_occupation = 0
    m_tot_0200_drop = 0
    m_tot_0200_win_loss = 0
    m_tot_0200_hold = 0
    m_tot_0200_occupation = 0
    m_tot_0300_drop = 0
    m_tot_0300_win_loss = 0
    m_tot_0300_hold = 0
    m_tot_0300_occupation = 0
    m_tot_0400_drop = 0
    m_tot_0400_win_loss = 0
    m_tot_0400_hold = 0
    m_tot_0400_occupation = 0
    m_tot_0500_drop = 0
    m_tot_0500_win_loss = 0
    m_tot_0500_hold = 0
    m_tot_0500_occupation = 0
    m_tot_0600_drop = 0
    m_tot_0600_win_loss = 0
    m_tot_0600_hold = 0
    m_tot_0600_occupation = 0
    m_tot_0700_drop = 0
    m_tot_0700_win_loss = 0
    m_tot_0700_hold = 0
    m_tot_0700_occupation = 0
    m_tot_0800_drop = 0
    m_tot_0800_win_loss = 0
    m_tot_0800_hold = 0
    m_tot_0800_occupation = 0
    m_tot_0900_drop = 0
    m_tot_0900_win_loss = 0
    m_tot_0900_hold = 0
    m_tot_0900_occupation = 0
    m_tot_1000_drop = 0
    m_tot_1000_win_loss = 0
    m_tot_1000_hold = 0
    m_tot_1000_occupation = 0
    m_tot_1100_drop = 0
    m_tot_1100_win_loss = 0
    m_tot_1100_hold = 0
    m_tot_1100_occupation = 0
    m_tot_1200_drop = 0
    m_tot_1200_win_loss = 0
    m_tot_1200_hold = 0
    m_tot_1200_occupation = 0
    m_tot_1300_drop = 0
    m_tot_1300_win_loss = 0
    m_tot_1300_hold = 0
    m_tot_1300_occupation = 0
    m_tot_1400_drop = 0
    m_tot_1400_win_loss = 0
    m_tot_1400_hold = 0
    m_tot_1400_occupation = 0
    m_tot_1500_drop = 0
    m_tot_1500_win_loss = 0
    m_tot_1500_hold = 0
    m_tot_1500_occupation = 0
    m_tot_1600_drop = 0
    m_tot_1600_win_loss = 0
    m_tot_1600_hold = 0
    m_tot_1600_occupation = 0
    m_tot_1700_drop = 0
    m_tot_1700_win_loss = 0
    m_tot_1700_hold = 0
    m_tot_1700_occupation = 0
    m_tot_1800_drop = 0
    m_tot_1800_win_loss = 0
    m_tot_1800_hold = 0
    m_tot_1800_occupation = 0
    m_tot_1900_drop = 0
    m_tot_1900_win_loss = 0
    m_tot_1900_hold = 0
    m_tot_1900_occupation = 0
    m_tot_2000_drop = 0
    m_tot_2000_win_loss = 0
    m_tot_2000_hold = 0
    m_tot_2000_occupation = 0
    m_tot_2100_drop = 0
    m_tot_2100_win_loss = 0
    m_tot_2100_hold = 0
    m_tot_2100_occupation = 0
    m_tot_2200_drop = 0
    m_tot_2200_win_loss = 0
    m_tot_2200_hold = 0
    m_tot_2200_occupation = 0
    m_tot_2300_drop = 0
    m_tot_2300_win_loss = 0
    m_tot_2300_hold = 0
    m_tot_2300_occupation = 0
    m_tot_final_drop = 0
    m_tot_final_win_loss = 0
    m_tot_final_hold = 0
    m_tot_final_occupation = 0
    m_tot_total_drop = 0
    m_tot_total_win_loss = 0
    m_tot_total_hold = 0
    m_tot_total_occupation = 0
    m_first_time = DateTime.MaxValue
    m_last_time = DateTime.MinValue
  End Sub

  Private Sub AddTotals(ByVal RowIndex As Integer)
    Dim _temp_number As Decimal

    ' Set color
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    ' Session Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESION_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8492)

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_DROP).Value = GUI_FormatCurrency(m_tot_inicial_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_inicial_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_HOLD).Value = GUI_FormatCurrency(m_tot_inicial_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INICIAL_OCCUPATION).Value = GUI_FormatCurrency(m_tot_inicial_occupation, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_DROP).Value = GUI_FormatCurrency(m_tot_0000_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0000_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0000_win_loss <> 0 Then
      m_tot_0000_hold = m_tot_0000_drop / m_tot_0000_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_HOLD).Value = GUI_FormatCurrency(m_tot_0000_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0000_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_DROP).Value = GUI_FormatCurrency(m_tot_0100_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0100_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0100_win_loss <> 0 Then
      m_tot_0100_hold = m_tot_0100_drop / m_tot_0100_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_HOLD).Value = GUI_FormatCurrency(m_tot_0100_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0100_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_DROP).Value = GUI_FormatCurrency(m_tot_0200_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0200_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0200_win_loss <> 0 Then
      m_tot_0200_hold = m_tot_0200_drop / m_tot_0200_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_HOLD).Value = GUI_FormatCurrency(m_tot_0200_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0200_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_DROP).Value = GUI_FormatCurrency(m_tot_0300_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0300_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0300_win_loss <> 0 Then
      m_tot_0300_hold = m_tot_0300_drop / m_tot_0300_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_HOLD).Value = GUI_FormatCurrency(m_tot_0300_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0300_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_DROP).Value = GUI_FormatCurrency(m_tot_0400_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0400_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0400_win_loss <> 0 Then
      m_tot_0400_hold = m_tot_0400_drop / m_tot_0400_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_HOLD).Value = GUI_FormatCurrency(m_tot_0400_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0400_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0400_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_DROP).Value = GUI_FormatCurrency(m_tot_0500_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0500_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0500_win_loss <> 0 Then
      m_tot_0500_hold = m_tot_0500_drop / m_tot_0500_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_HOLD).Value = GUI_FormatCurrency(m_tot_0500_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0500_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0500_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_DROP).Value = GUI_FormatCurrency(m_tot_0600_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0600_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0600_win_loss <> 0 Then
      m_tot_0600_hold = m_tot_0600_drop / m_tot_0600_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_HOLD).Value = GUI_FormatCurrency(m_tot_0600_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0600_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0600_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_DROP).Value = GUI_FormatCurrency(m_tot_0700_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0700_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0700_win_loss <> 0 Then
      m_tot_0700_hold = m_tot_0700_drop / m_tot_0700_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_HOLD).Value = GUI_FormatCurrency(m_tot_0700_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0700_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0700_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_DROP).Value = GUI_FormatCurrency(m_tot_0800_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0800_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0800_win_loss <> 0 Then
      m_tot_0800_hold = m_tot_0800_drop / m_tot_0800_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_HOLD).Value = GUI_FormatCurrency(m_tot_0800_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0800_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0800_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_DROP).Value = GUI_FormatCurrency(m_tot_0900_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_0900_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_0900_win_loss <> 0 Then
      m_tot_0900_hold = m_tot_0900_drop / m_tot_0900_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_HOLD).Value = GUI_FormatCurrency(m_tot_0900_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_0900_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_0900_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_DROP).Value = GUI_FormatCurrency(m_tot_1000_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1000_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1000_win_loss <> 0 Then
      m_tot_1000_hold = m_tot_1000_drop / m_tot_1000_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_HOLD).Value = GUI_FormatCurrency(m_tot_1000_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1000_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_DROP).Value = GUI_FormatCurrency(m_tot_1100_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1100_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1100_win_loss <> 0 Then
      m_tot_1100_hold = m_tot_1100_drop / m_tot_1100_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_HOLD).Value = GUI_FormatCurrency(m_tot_1100_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1100_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_DROP).Value = GUI_FormatCurrency(m_tot_1200_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1200_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1200_win_loss <> 0 Then
      m_tot_1200_hold = m_tot_1200_drop / m_tot_1200_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_HOLD).Value = GUI_FormatCurrency(m_tot_1200_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1200_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_DROP).Value = GUI_FormatCurrency(m_tot_1300_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1300_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1300_win_loss <> 0 Then
      m_tot_1300_hold = m_tot_1300_drop / m_tot_1300_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_HOLD).Value = GUI_FormatCurrency(m_tot_1300_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1300_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_DROP).Value = GUI_FormatCurrency(m_tot_1400_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1400_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1400_win_loss <> 0 Then
      m_tot_1400_hold = m_tot_1400_drop / m_tot_1400_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_HOLD).Value = GUI_FormatCurrency(m_tot_1400_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1400_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1400_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_DROP).Value = GUI_FormatCurrency(m_tot_1500_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1500_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1500_win_loss <> 0 Then
      m_tot_1500_hold = m_tot_1500_drop / m_tot_1500_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_HOLD).Value = GUI_FormatCurrency(m_tot_1500_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1500_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1500_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_DROP).Value = GUI_FormatCurrency(m_tot_1600_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1600_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1600_win_loss <> 0 Then
      m_tot_1600_hold = m_tot_1600_drop / m_tot_1600_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_HOLD).Value = GUI_FormatCurrency(m_tot_1600_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1600_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1600_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_DROP).Value = GUI_FormatCurrency(m_tot_1700_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1700_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1700_win_loss <> 0 Then
      m_tot_1700_hold = m_tot_1700_drop / m_tot_1700_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_HOLD).Value = GUI_FormatCurrency(m_tot_1700_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1700_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1700_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_DROP).Value = GUI_FormatCurrency(m_tot_1800_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1800_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1800_win_loss <> 0 Then
      m_tot_1800_hold = m_tot_1800_drop / m_tot_1800_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_HOLD).Value = GUI_FormatCurrency(m_tot_1800_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1800_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1800_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_DROP).Value = GUI_FormatCurrency(m_tot_1900_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_1900_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_1900_win_loss <> 0 Then
      m_tot_1900_hold = m_tot_1900_drop / m_tot_1900_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_HOLD).Value = GUI_FormatCurrency(m_tot_1900_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_1900_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_1900_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_DROP).Value = GUI_FormatCurrency(m_tot_2000_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_2000_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_2000_win_loss <> 0 Then
      m_tot_2000_hold = m_tot_2000_drop / m_tot_2000_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_HOLD).Value = GUI_FormatCurrency(m_tot_2000_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_2000_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2000_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_DROP).Value = GUI_FormatCurrency(m_tot_2100_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_2100_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_2100_win_loss <> 0 Then
      m_tot_2100_hold = m_tot_2100_drop / m_tot_2100_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_HOLD).Value = GUI_FormatCurrency(m_tot_2100_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_2100_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2100_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_DROP).Value = GUI_FormatCurrency(m_tot_2200_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_2200_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_2200_win_loss <> 0 Then
      m_tot_2200_hold = m_tot_2200_drop / m_tot_2200_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_HOLD).Value = GUI_FormatCurrency(m_tot_2200_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_2200_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2200_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_DROP).Value = GUI_FormatCurrency(m_tot_2300_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_2300_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_2300_win_loss <> 0 Then
      m_tot_2300_hold = m_tot_2300_drop / m_tot_2300_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_HOLD).Value = GUI_FormatCurrency(m_tot_2300_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_2300_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_2300_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_DROP).Value = GUI_FormatCurrency(m_tot_final_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_final_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_final_win_loss <> 0 Then
      m_tot_final_hold = m_tot_final_drop / m_tot_final_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_HOLD).Value = GUI_FormatCurrency(m_tot_final_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_final_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    ' Drop
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_DROP).Value = GUI_FormatCurrency(m_tot_total_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' W/L
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_WIN_LOSS).Value = GUI_FormatCurrency(m_tot_total_win_loss, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Hold
    If m_tot_total_win_loss <> 0 Then
      m_tot_total_hold = m_tot_total_drop / m_tot_total_win_loss * 100
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_HOLD).Value = GUI_FormatCurrency(m_tot_total_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

    'Ocupation
    _temp_number = m_tot_total_occupation
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_OCCUPATION).Value = GUI_FormatCurrency(_temp_number / m_num_rows * 100, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , False) & Application.CurrentCulture.NumberFormat.PercentSymbol

  End Sub

  Private Sub HideCeroHours()
    Dim _date_time_from As Date

    _date_time_from = New Date(uc_dsl.FromDate.Year, uc_dsl.FromDate.Month, uc_dsl.FromDate.Day, uc_dsl.ClosingTime, 0, 0)

    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0000_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0000_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0000_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0000_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0100_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0100_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0100_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0100_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0200_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0200_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0200_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0200_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0300_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0300_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0300_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0300_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0400_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0400_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0400_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0400_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0500_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0500_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0500_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0500_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0600_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0600_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0600_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0600_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0700_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0700_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0700_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0700_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0800_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0800_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0800_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0800_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_0900_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_0900_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_0900_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_0900_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1000_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1000_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1000_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1000_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1100_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1100_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1100_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1100_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1200_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1200_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1200_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1200_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1300_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1300_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1300_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1300_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1400_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1400_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1400_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1400_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1500_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1500_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1500_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1500_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1600_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1600_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1600_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1600_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1700_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1700_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1700_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1700_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1800_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1800_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1800_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1800_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_1900_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_1900_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_1900_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_1900_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_2000_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_2000_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_2000_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_2000_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_2100_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_2100_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_2100_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_2100_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_2200_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_2200_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_2200_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_2200_OCCUPATION).Width = 0
    End If

    _date_time_from = _date_time_from.AddHours(1)
    If _date_time_from < m_first_time Or _date_time_from > m_last_time Then
      Me.Grid.Column(GRID_COLUMN_2300_DROP).Width = 0
      Me.Grid.Column(GRID_COLUMN_2300_WIN_LOSS).Width = 0
      Me.Grid.Column(GRID_COLUMN_2300_HOLD).Width = 0
      Me.Grid.Column(GRID_COLUMN_2300_OCCUPATION).Width = 0
    End If

  End Sub


#End Region ' Private Functions

#Region " Events "

  Private Sub uc_dsl_DatePickerValueChanged() Handles uc_dsl.DatePickerValueChanged

    Dim _now As Date

    _now = WGDB.Now

    If uc_dsl.FromDate > _now Then
      m_closing_time_int = GetDefaultClosingTime()
      m_initial_date = New DateTime(_now.Year, _now.Month, _now.Day, m_closing_time_int, 0, 0)

      If m_initial_date > _now Then
        m_initial_date = m_initial_date.AddDays(-1)
      End If

      uc_dsl.FromDate = m_initial_date

      uc_dsl.ClosingTime = m_closing_time_int
      uc_dsl.ClosingTimeEnabled = False
    End If

    uc_dsl.ToDate = uc_dsl.FromDate.AddDays(1)

  End Sub

#End Region ' Events

End Class