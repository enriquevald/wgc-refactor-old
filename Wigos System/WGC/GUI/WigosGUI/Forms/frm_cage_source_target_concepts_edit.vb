'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : 
'
' DESCRIPTION : 
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-SEP-2014  OPC    Initial version   
' 30-SEP-2014  OPC    Fixed Bug #1344 Show a message box when try to insert a duplicate item.
' 30-SEP-2014  OPC    Fixed Bug #1345 Set Enabled true.
' 08-JAN-2015  OPC    Fixed Bug #1913 Can't update an item with the name of other item.
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports WSI.Common

#End Region

Public Class frm_cage_source_target_concepts_edit
  Inherits frm_base_edit

#Region "Constants"

  Private Const DEFAULT_TYPE As Integer = 1

#End Region

#Region "Enums"

  Public Enum CONCEPT_TYPES As Integer
    SYSTEM = 0
    CUSTOM = 1
  End Enum

#End Region

#Region "Members"

  Public sw_custom As Boolean = False
  Private sw_new As Boolean = False

#End Region

#Region "Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_CONCEPTS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5477) ' Concepts

    ' Group Box 
    gb_Status.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) ' Status

    ' Entry Field 
    ef_Name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210) ' Name 
    ef_Name.Value = String.Empty
    Call ef_Name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, cls_cage_source_target_concepts.MAX_TITLE_LEN) ' Filter

    ef_UnitPrice.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5476) ' Unit price
    ef_UnitPrice.Value = String.Empty
    Call ef_UnitPrice.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' Text Box
    txt_Description.MaxLength = cls_cage_source_target_concepts.MAX_DESCRIPTION_LEN
    txt_Description.Text = String.Empty

    ' Label
    lbl_description.Text = GLB_NLS_GUI_CONFIGURATION.GetString(62)

    ' Type 
    tf_Type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)

    ' Radio Button
    rb_enabled.Checked = True
    rb_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(311)
    rb_disabled.Checked = False
    rb_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(328)

    ' CheckBox's
    chk_IsProvision.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5474)
    chk_ShowInReport.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5475)
    chk_IsProvision.Checked = False
    chk_ShowInReport.Checked = True ' 06-OCT-2014  SMN

  End Sub ' GUI_InitControls

  Public Overloads Sub ShowNewItem()

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    sw_custom = True
    sw_new = True

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  Public Overloads Sub ShowEditItem(ByVal Concept_Id As Integer, ByVal Custom_Concept As Boolean)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = Concept_Id
    Me.sw_custom = Custom_Concept

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _cls_cage_source_target_concepts As cls_cage_source_target_concepts

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New cls_cage_source_target_concepts
        _cls_cage_source_target_concepts = DbEditedObject
        _cls_cage_source_target_concepts.Concept_Id = -1
        _cls_cage_source_target_concepts.Description = String.Empty
        _cls_cage_source_target_concepts.Enabled = True
        _cls_cage_source_target_concepts.Is_Provision = False
        _cls_cage_source_target_concepts.Name = String.Empty
        _cls_cage_source_target_concepts.Show_In_Report = True ' 06-OCT-2014 SMN
        If Me.sw_custom Then
          _cls_cage_source_target_concepts.Type = CONCEPT_TYPES.CUSTOM
        Else
          _cls_cage_source_target_concepts.Type = CONCEPT_TYPES.SYSTEM
        End If
        _cls_cage_source_target_concepts.Unit_Price = GUI_ParseCurrency("0")

        DbStatus = ENUM_STATUS.STATUS_OK
      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_OK, , _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480).ToLower(), ef_Name.Value)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_OK, , _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480).ToLower(), ef_Name.Value)
        End If

      Case Else
        MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _cage_source_target_concepts As cls_cage_source_target_concepts

    _cage_source_target_concepts = DbReadObject

    ' Name
    ef_Name.Value = _cage_source_target_concepts.Name

    ' Description
    txt_Description.Text = _cage_source_target_concepts.Description

    ' Unit Price
    If _cage_source_target_concepts.Unit_Price.ToString <> 0 Then
      ef_UnitPrice.Value = _cage_source_target_concepts.Unit_Price.ToString
    Else
      ef_UnitPrice.Value = String.Empty
    End If

    ' Enabled
    If Me.sw_new Then
      rb_enabled.Checked = True
    Else
      If _cage_source_target_concepts.Enabled Then
        rb_enabled.Checked = True
      Else
        rb_disabled.Checked = True
      End If
    End If

    ' Is Provision
    chk_IsProvision.Checked = _cage_source_target_concepts.Is_Provision

    ' Show in Report
    chk_ShowInReport.Checked = _cage_source_target_concepts.Show_In_Report

    ' Type
    If _cage_source_target_concepts.Type = CONCEPT_TYPES.SYSTEM  Then
      tf_Type.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268) 'System
    Else
      tf_Type.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393) 'Customized
    End If

    ' 03-OCT-2014 SMN
    ' Enable / Disable
    If _cage_source_target_concepts.Type = CONCEPT_TYPES.SYSTEM _
          Or _cage_source_target_concepts.Concept_Id = CageMeters.CageConceptId.Global _
          Or _cage_source_target_concepts.Concept_Id = CageMeters.CageConceptId.Tips Then
      ef_UnitPrice.Enabled = False
      gb_Status.Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    End If

  End Sub 'GUI_SetScreenData

  Protected Overrides Sub GUI_GetScreenData()

    Dim _cage_source_target_concepts As cls_cage_source_target_concepts

    _cage_source_target_concepts = DbEditedObject
    _cage_source_target_concepts.Description = txt_Description.Text
    _cage_source_target_concepts.Enabled = IIf(rb_enabled.Checked, True, False)
    _cage_source_target_concepts.Is_Provision = chk_IsProvision.Checked
    _cage_source_target_concepts.Name = ef_Name.Value
    _cage_source_target_concepts.Show_In_Report = chk_ShowInReport.Checked
    If Me.sw_custom Then
      _cage_source_target_concepts.Type = CONCEPT_TYPES.CUSTOM
    Else
      _cage_source_target_concepts.Type = CONCEPT_TYPES.SYSTEM
    End If
    If Not String.IsNullOrEmpty(ef_UnitPrice.Value) Then
      _cage_source_target_concepts.Unit_Price = GUI_ParseCurrency(ef_UnitPrice.Value)
    Else
      _cage_source_target_concepts.Unit_Price = Nothing
    End If

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_Name

  End Sub ' GUI_SetInitialFocus

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' Empty name
    If String.IsNullOrEmpty(ef_Name.Value.Trim()) Then
      Call ef_Name.Focus()
      NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , ef_Name.Text)

      Return False
    End If

    ' Empty description
    If String.IsNullOrEmpty(txt_Description.Text.Trim()) Then
      Call txt_Description.Focus()
      NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , lbl_description.Text)

      Return False
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If Me.txt_Description.Focused Then
      Return True
    Else
      Return False
    End If

  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE
        If Delete_Concept() Then
          MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select ' GUI_ButtonClick

  End Sub ' GUI_ButtonClick

#End Region

#Region "Privates"

  ' PURPOSE: Get the confirmation to delete a concept.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function Delete_Concept() As Boolean
    Dim _delete As Boolean = True

    If Not Permissions.Delete Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      _delete = False
    End If

    Dim _cage_source_target_concepts As cls_cage_source_target_concepts
    _cage_source_target_concepts = DbReadObject

    If Not cls_cage_source_target_concepts.GetConceptRelation(_cage_source_target_concepts.Concept_Id) Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5504), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _cage_source_target_concepts.Name) = ENUM_MB_RESULT.MB_RESULT_NO Then
        _delete = False
      End If
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5507), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
      _delete = False
    End If

    Return _delete
  End Function ' Delete_Concept

#End Region

End Class