'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_money_laundering_report
' DESCRIPTION:  
' AUTHOR:        Artur Nebot
' CREATION DATE: 25-JUL-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-JUL-2013 ANG & LEM   First Release
' 22-AUG-2013 LEM    Fixed Bug WIG-145: 'To' date are not included on results
' 22-AUG-2013 LEM    Fixed Bug WIG-152: Wrong results if AML limits are changed while this window still open
' 26-AUG-2013 LEM    A new filter can Show/Hide holder name if Near Identification limit is selected
' 02-SEP-2013 ANG    Refactor and Remove some unused code.
' 05-SEP-2013 LEM    Fixed Bug WIG-190: Error on search with Spanish(es) culture info. Show message: "Not connected to the database." 
' 09-SEP-2013 LEM    Fixed Bug WIG-197: Wrong data are showed on daily detail opcion.
' 24-OCT-2013 JBC    Added GroupByMode feature
' 23-MAY-2014 JBC    Fixed Bug WIG-945: Added ORDER BY to query data.
' 07-JUL-2014 JBC    Fixed Bug WIG-1065: Now user can hide/show data below AML interval.
' 18-FEB-2015 ANM    Fixed Bug WIG-2081: Add xShowAccountDetails checkbox's caption and value to excel report
' 09-JUN-2015 RCI    Fixed Bug WIG-2423: Movements option doesn't work for Spain regional settings
' 13-AUG-2015 YNM    Fixed Bug WIG-2625/TFS-3719: AML: Total amounts in Movements search did't match with Daily and Montly search
' 21-AGO-2015 FJC    Product BackLog Item: 3702
' 09-SEP-2015 YNM    Fixed Bug TFS-3719: AML: Total amounts in Movements search did't match with Daily and Montly search
' 22-SEP-2015 YNM    Fixed Bug TFS-4536: AML: DB Error when customer data is added to query
' 14-DEC-2015 DLL    Fixed Bug TFS-7640: Error on search with filter Movements
' 20-SEP-2016 JMV    Fixed Bug 17640:Reporte anti-lavado da time-out
' 05-FEV-2018 AGS    Bug 31417:WIGOS-6919 [Ticket #10851] Error Consulta Reporte Logrand PLD version V03.06.0035
' 13-FEB-2018 EOR    Bug 31519: WIGOS-8080 [Ticket #10688] Release v03.005.0105 - Reporte Antilavado - Aparecen valores negativos en recargas 
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections

Public Class frm_money_laundering_report
  Inherits GUI_Controls.frm_base_sel

#Region "Members"

  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_date_range_from As String
  Private m_date_range_to As String
  Private m_detail As String
  Private m_show_not_identified_name As Boolean
  Private m_opt_limit As Byte

  Private m_report_mode As ENUM_MONEY_LAUNDERING_REPORT_MODE

  Private m_warning_identification_limit As Decimal
  Private m_identification_limit As Decimal
  Private m_warning_report_limit As Decimal
  Private m_report_limit As Decimal

  Private m_number_of_date_columns As Byte = 1 '' Min one column ;) '' Its really necessary? Or can be replaced by m_months_headers.Count?
  Private m_months_headers As New List(Of String)
  Private m_group_type As ENUM_GROUP_BY_MODE
  Private m_subtotal As TYPE_TOTAL_COUNTERS
  Private m_first_time As Boolean = True
  Private m_show_subtotals As Boolean

#End Region

#Region "Constants"

  Private Const GRID_COLUMNS As Byte = 9
  Private Const GRID_HEADER_ROWS As Byte = 2
  Private Const GRID_MAX_MONTH_COLUMNS As Int32 = 9

  Private Const GRID_COL_INDEX As Byte = 0
  Private Const GRID_COL_ACCOUNT_ID As Byte = 1
  Private Const GRID_COL_TRACK_DATA As Byte = 2
  Private Const GRID_COL_ACCOUNT_HOLDER_NAME1 As Byte = 3
  Private Const GRID_COL_ACCOUNT_HOLDER_NAME2 As Byte = 4
  Private Const GRID_COL_ACCOUNT_HOLDER_NAME3 As Byte = 5
  Private Const GRID_COL_MOVEMENT_DATE As Byte = 6
  Private Const GRID_COL_MOVEMENT_AMOUNT As Byte = 7
  Private Const GRID_COL_MOVEMENT_CLIENT_CARD As Byte = 8
  Private GRID_INIT_COLUMNS_AMOUNT As Byte = 9


  Private Const SQL_COL_ACCOUNT_ID As Byte = 0
  Private Const SQL_COL_TRACK_DATA As Byte = 1
  Private Const SQL_COL_ACCOUNT_HOLDER_NAME1 As Byte = 2
  Private Const SQL_COL_ACCOUNT_HOLDER_NAME2 As Byte = 3
  Private Const SQL_COL_ACCOUNT_HOLDER_NAME3 As Byte = 4
  Private Const SQL_COL_ACCOUNT_HOLDER_LEVEL As Byte = 5
  Private Const SQL_INIT_COLUMNS_AMOUNT As Byte = 6
  Private Const SQL_COL_MOVEMENT_AMOUNT As Byte = 6
  Private Const SQL_COL_MOVEMENT_DATE As Byte = 7
  Private Const SQL_COL_TYPE As Byte = 8
  Private Const SQL_COL_MOVEMENT_CLIENT_CARD As Byte = 1
  Private Const SQL_INIT_COLUMNS_AMOUNT_MOVEMENT As Byte = 9

  Private m_sql_init_columns_holder_data As Byte

  Private Const GRID_CURRENCY_COL_WIDTH As Short = 2000

  Private Const BOUNDARY_ROW_TYPE_HEADER = 0
  Private Const BOUNDARY_ROW_TYPE_DATA = 1

  Private Const DATE_MONTH_NAME_AND_YEAR As String = "MMM yyyy"

  Private Const FORM_DB_MIN_VERSION As Short = 160

#End Region

#Region "Structures"

  Public Enum ENUM_MONEY_LAUNDERING_REPORT_MODE
    PRIZE = 0
    RECHARGE = 1
  End Enum
  Public Enum ENUM_GROUP_BY_MODE
    MODE_0 = 0 '0 byAccountId, 
    MODE_1 = 1 '1 byAccountId+TrackData (Anonymous)
    MODE_3 = 3 '3 byAccountId+TrackData (Anonymous+Personal)
  End Enum
  '' Custom Key-Description 
  Class ENUM_LAUNDERING_REPORT_LIMIT

    Public Id As Byte
    Private m_description As String

#Region "Properties"
    Public ReadOnly Property BaseAmount() As Decimal
      Get
        Return GeneralParam.GetDecimal("AntiMoneyLaundering", "BaseAmount", 0)
      End Get
    End Property

    Public ReadOnly Property Multiplier(ByVal Type As ENUM_MONEY_LAUNDERING_REPORT_MODE) As Decimal
      Get

        Dim _gp_subject_key_bld As New StringBuilder()
        Dim _value As Decimal


        Select Case Type
          Case ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE
            _gp_subject_key_bld.Append("Prize.")
          Case ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE
            _gp_subject_key_bld.Append("Recharge.")
          Case Else
            Throw New Exception("Unrepresentable value")
        End Select

        Select Case Me
          Case IDENTIFICATION_WARNING
            _gp_subject_key_bld.Append("Identification.Warning.")
          Case IDENTIFICATION
            _gp_subject_key_bld.Append("Identification.")
          Case REPORT_WARNING
            _gp_subject_key_bld.Append("Report.Warning.")
          Case REPORT
            _gp_subject_key_bld.Append("Report.")
          Case Else
            Throw New Exception("Unrepresentable value")
        End Select

        _gp_subject_key_bld.Append("Limit")
        _value = GeneralParam.GetDecimal("AntiMoneyLaundering", _gp_subject_key_bld.ToString())

        GeneralParam.GetString("Cashier", "Language")
        Return _value

      End Get
    End Property

    Public ReadOnly Property LimitValue(ByVal Type As ENUM_MONEY_LAUNDERING_REPORT_MODE) As Decimal
      Get
        Return Me.BaseAmount * Me.Multiplier(Type)
      End Get
    End Property

    Public ReadOnly Property ShortDescription(ByVal Type As ENUM_MONEY_LAUNDERING_REPORT_MODE) As String
      Get

        Dim _str_smvdf As String
        _str_smvdf = GeneralParam.GetString("AntiMoneyLaundering", "BaseName")

        Return String.Format("{0,5} {1,-5} {2,-7}" _
                            , GUI_FormatNumber(Me.Multiplier(Type), 2) _
                            , _str_smvdf _
                            , GUI_FormatCurrency(Me.LimitValue(Type)))
      End Get
    End Property
#End Region

#Region "Enum Members"

    '' Warning Identification Limit
    Public Shared ReadOnly IDENTIFICATION_WARNING As ENUM_LAUNDERING_REPORT_LIMIT = New ENUM_LAUNDERING_REPORT_LIMIT(0, _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(2413))

    '' Identification 
    Public Shared ReadOnly IDENTIFICATION As ENUM_LAUNDERING_REPORT_LIMIT = New ENUM_LAUNDERING_REPORT_LIMIT(1, _
    GLB_NLS_GUI_PLAYER_TRACKING.GetString(2411))


    '' Warning Report Limit
    Public Shared ReadOnly REPORT_WARNING As ENUM_LAUNDERING_REPORT_LIMIT = New ENUM_LAUNDERING_REPORT_LIMIT(2, _
    GLB_NLS_GUI_PLAYER_TRACKING.GetString(2414))

    '' Report Limit
    Public Shared ReadOnly REPORT As ENUM_LAUNDERING_REPORT_LIMIT = New ENUM_LAUNDERING_REPORT_LIMIT(3, _
    GLB_NLS_GUI_PLAYER_TRACKING.GetString(2412))

#End Region

#Region "Operators"

    '' To allow compare enum members.
    Public Shared Operator <>(ByVal o1 As ENUM_LAUNDERING_REPORT_LIMIT, _
                             ByVal o2 As ENUM_LAUNDERING_REPORT_LIMIT) As Boolean
      Return o1.Id <> o2.Id

    End Operator

    '' To allow compare enum members
    Public Shared Operator =(ByVal o1 As ENUM_LAUNDERING_REPORT_LIMIT, _
                             ByVal o2 As ENUM_LAUNDERING_REPORT_LIMIT) As Boolean

      Return o1.Id = o2.Id

    End Operator

#End Region

#Region "Constructors"
    Private Sub New(ByVal Id As Byte, ByVal Description As String)
      Me.Id = Id
      Me.m_description = Description
    End Sub

#End Region

  End Class

  ''Save data from Totals
  Public Class TYPE_TOTAL_COUNTERS
    Private m_amounts As Dictionary(Of Integer, Decimal)
    Private m_draw_row As Boolean
    Private m_is_anonymous As Boolean
    Private m_print_subtotal As Boolean
    Private m_data_row As CLASS_DB_ROW
    'Constructor
    Public Sub New()
      m_amounts = New Dictionary(Of Integer, Decimal)
      m_draw_row = False
      m_is_anonymous = False
      m_print_subtotal = False
      m_data_row = Nothing
    End Sub
    Public Property Amounts() As Dictionary(Of Integer, Decimal)
      Get
        Return m_amounts
      End Get
      Set(ByVal value As Dictionary(Of Integer, Decimal))
        m_amounts = value
      End Set
    End Property
    Public Property DrawRow() As Boolean
      Get
        Return m_draw_row
      End Get
      Set(ByVal value As Boolean)
        m_draw_row = value
      End Set
    End Property
    Public Property IsAnonymous() As Boolean
      Get
        Return m_is_anonymous
      End Get
      Set(ByVal value As Boolean)
        m_is_anonymous = value
      End Set
    End Property
    Public Property PrintSubTotal() As Boolean
      Get
        Return m_print_subtotal
      End Get
      Set(ByVal value As Boolean)
        m_print_subtotal = value
      End Set
    End Property
    Public Property CurrentRow() As CLASS_DB_ROW
      Get
        Return m_data_row
      End Get
      Set(ByVal value As CLASS_DB_ROW)
        m_data_row = value
      End Set
    End Property
  End Class

#End Region

#Region "Constructors"

  Public Sub New()
    MyBase.New()

    ' RRB 27-AUG-2012 Set FormId
    Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE
    Me.m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    If FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE Then
      Me.m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE
    Else
      Me.m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE
    End If

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

#End Region

#Region "Overrides"

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()
    Call ResetTotalCounters()
    m_subtotal.PrintSubTotal = False
    m_first_time = True

  End Sub 'GUI_BeforeFirstRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _idx_month_column As Byte
    Dim _amount As Decimal

    If Me.opt_movements.Checked Then
      GUI_SetupRowsMovementDetail(RowIndex, DbRow)
      Return True
    End If

    ' Account Id
    Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_ACCOUNT_ID)

    ' Card Track Data
    If Not String.IsNullOrEmpty(DbRow.Value(SQL_COL_TRACK_DATA)) Then
      Me.Grid.Cell(RowIndex, GRID_COL_TRACK_DATA).Value = DbRow.Value(SQL_COL_TRACK_DATA)
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_TRACK_DATA).Value = String.Empty
    End If

    ' Amount columns
    For _idx_month_column = 0 To Math.Max(m_number_of_date_columns - 1, 0)
      If DbRow.IsNull(SQL_INIT_COLUMNS_AMOUNT + _idx_month_column) Then
        Grid.Cell(RowIndex, GRID_INIT_COLUMNS_AMOUNT + _idx_month_column).Value = String.Empty
      Else
        _amount = DbRow.Value(SQL_INIT_COLUMNS_AMOUNT + _idx_month_column)

        If Not IsInInterval(_amount) Then
          If Not Me.chk_show_amounts_below_limit.Checked Then
            Grid.Cell(RowIndex, GRID_INIT_COLUMNS_AMOUNT + _idx_month_column).Value = String.Empty
          Else
            Grid.Cell(RowIndex, GRID_INIT_COLUMNS_AMOUNT + _idx_month_column).Value = IIf(_amount > 0, GUI_FormatCurrency(_amount), String.Empty)
          End If
        Else
          Grid.Cell(RowIndex, GRID_INIT_COLUMNS_AMOUNT + _idx_month_column).Value = GUI_FormatCurrency(_amount)
          If Me.chk_show_amounts_below_limit.Checked Then
            Grid.Cell(RowIndex, GRID_INIT_COLUMNS_AMOUNT + _idx_month_column).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          End If
        End If
      End If
    Next

    ' Account Holder Name
    If m_show_not_identified_name Or m_opt_limit <> ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.Id Then
      If Not DbRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME1) Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME1).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME1)
      End If
      If Not DbRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME2) Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME2).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME2)
      End If
      If Not DbRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME3) Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME3).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME3)
      End If
    End If

    ' Holder data colums
    If Me.chk_show_account_details.Checked Then
      mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMNS + m_number_of_date_columns, m_sql_init_columns_holder_data)
    End If

    Return True

  End Function 'GUI_SetupRow

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    ' Leave this sub empty, as the FormId is set now in the Constructor of the class.
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)


  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Control captions
    If m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2949, Accounts.getAMLName())
    ElseIf m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2950, Accounts.getAMLName())
    End If

    'Me.chk_show_account_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2502)    ' Show identification data
    Me.chk_show_account_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(552)

    Me.chk_show_amounts_below_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5077)    ' Show identification data

    dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)                        ' From
    dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_from.SetRange(New Date(2013, 7, 1), WGDB.Now)                                 ' AML begun on 01/07/2013

    dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)                          ' To
    dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_to.SetRange(New Date(2013, 7, 1), WGDB.Now)                                   ' AML begun on 01/07/2013

    gb_detail.Text = GLB_NLS_GUI_INVOICING.GetString(7)                       ' Detail
    opt_daily.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2500)                      ' Daily
    opt_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2501)                      ' Monthly
    opt_movements.Text = GLB_NLS_GUI_INVOICING.GetString(2)                           ' Movements

    Me.lbl_color_info.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.lbl_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5078)

    gb_date_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830)                   ' Date Range

    With Me.cmb_group
      .Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651)
      .Add(ENUM_GROUP_BY_MODE.MODE_0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2830))
      .Add(ENUM_GROUP_BY_MODE.MODE_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2831))
      .Add(ENUM_GROUP_BY_MODE.MODE_3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2832))
    End With

    Me.chk_total_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2833)

    Call InitLimitsFilter()

    ' Buttons setup
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Set filter default values
    Call SetDefaultValues()

    ' Set columns in grid
    Call GUI_StyleSheet()

    'Init Subtotals
    m_subtotal = New TYPE_TOTAL_COUNTERS

  End Sub ' GUI_InitControls


  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AndPerm TYPE_PERMISSIONS
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    ' GroupBox Permissions
    Me.chk_show_account_details.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read


  End Sub ' GUI_Permissions

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If m_subtotal.PrintSubTotal Then
      m_subtotal.DrawRow = FilterAccountByAccountId()
      If m_subtotal.PrintSubTotal Then
        Call DrawSubTotalRow(Me.m_subtotal.DrawRow)
      End If
    End If
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If opt_movements.Checked Then
      Call ProcessCounters(DbRow)
      Return True
    End If

    Call ProcessCounters(DbRow)

    Return m_subtotal.DrawRow

  End Function

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _str_bld As System.Text.StringBuilder
    Dim _idx_date As Byte
    Dim _dates_bld As New StringBuilder()
    Dim _str_date As String
    Dim _first_day_of_month As DateTime
    Dim _account_filter As String
    Dim _col_month As String
    Dim _col_day As String
    Dim _from_date As DateTime
    Dim _to_date As DateTime

    _col_day = ""
    _col_month = ""

    _from_date = New DateTime(dtp_from.Value.Year, dtp_from.Value.Month, 1)
    _to_date = New DateTime(dtp_to.Value.Year, dtp_to.Value.Month, 1)

    m_show_subtotals = Me.chk_total_account.Checked

    'if Daily Detail are selected, must show only one month
    If Not dtp_to.Enabled Then
      _to_date = _from_date
    End If

    ' radio button "Movements" is checked, it means that the recharge/Awards of clients have to be shown
    If opt_movements.Checked Then
      _sql_command = GetSqlCommandMovements()
    Else

      'to include _to_date on results
      _to_date = New DateTime(_to_date.AddMonths(1).Year, _to_date.AddMonths(1).Month, 1)

      _str_bld = New System.Text.StringBuilder()
      m_months_headers.Clear()

      _account_filter = uc_account.GetFilterSQL()

      If m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
        _col_month = "AMM_SPLIT_A"
        _col_day = "AMD_SPLIT_A"
      ElseIf m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE Then
        _col_month = "AMM_PRIZE"
        _col_day = "AMD_PRIZE"
      End If

      _sql_command = New SqlCommand()

      ''******************''
      ''   MONTH MODE     ''
      ''******************''
      If opt_month.Checked Then
        For _idx_date = 0 To m_number_of_date_columns - 1
          _first_day_of_month = dtp_from.Value.AddDays(1 - dtp_from.Value.Day).AddMonths(_idx_date)
          _str_date = _first_day_of_month.ToString("yyyy-MM-dd")

          m_months_headers.Add(_first_day_of_month.ToString(DATE_MONTH_NAME_AND_YEAR).ToUpperInvariant())
          _dates_bld.AppendFormat("[{0}]", _str_date)          '' To Build Sql Pivot Columns and also where filters.
          If _idx_date < m_number_of_date_columns - 1 Then
            _dates_bld.Append(",")
          End If
        Next

        _str_bld.AppendLine("SELECT   AC_ACCOUNT_ID  ")
        _str_bld.AppendLine("       , AMM_TRACK_DATA  ")
        _str_bld.AppendLine("       , AC_HOLDER_NAME3 ")
        _str_bld.AppendLine("       , AC_HOLDER_NAME1 ")
        _str_bld.AppendLine("       , AC_HOLDER_NAME2 ")
        _str_bld.AppendLine("       , AC_HOLDER_LEVEL ")
        _str_bld.AppendFormat("     , {0}          ", _dates_bld.ToString())

        ' Show account details
        If chk_show_account_details.Checked Then
          _str_bld.AppendLine(mdl_account_for_report.AccountFieldsSql(False))
        End If
        _str_bld.AppendLine("  FROM   (            ")

        _str_bld.AppendLine("           SELECT   DATEADD(HOUR, -DATEPART(HOUR, AMM_MONTH), AMM_MONTH) AMM_MONTH      ")
        _str_bld.AppendLine("                  , AC_ACCOUNT_ID  ")
        _str_bld.AppendLine("                 , AC_HOLDER_NAME3 ")
        _str_bld.AppendLine("                 , AC_HOLDER_NAME1 ")
        _str_bld.AppendLine("                 , AC_HOLDER_NAME2 ")
        _str_bld.AppendLine("                 , AC_HOLDER_LEVEL ")
        _str_bld.AppendLine("                  , AMM_TRACK_DATA  ")
        _str_bld.AppendLine("                  , " & _col_month)

        ' Show account details
        If chk_show_account_details.Checked Then
          _str_bld.AppendLine(mdl_account_for_report.AccountFieldsSql())
        End If

        _str_bld.AppendLine("             FROM   AML_MONTHLY")

        _str_bld.AppendLine("       INNER JOIN   ACCOUNTS ON AMM_ACCOUNT_ID = AC_ACCOUNT_ID")

        If Not String.IsNullOrEmpty(_account_filter) Then
          _str_bld.AppendFormat(" AND {0}", _account_filter).AppendLine()
        End If

        _str_bld.AppendLine("    AND   DATEADD(HOUR, -DATEPART(HOUR, AMM_MONTH), AMM_MONTH) >= @pDateFrom ")
        _str_bld.AppendLine("    AND   DATEADD(HOUR, -DATEPART(HOUR, AMM_MONTH), AMM_MONTH) < @pDateTo    ")

        'Call WhereLimits(_str_bld, _sql_command, m_report_mode)

        _str_bld.AppendLine("          ) AS SourceTable")

        _str_bld.AppendLine("PIVOT                                         ")
        _str_bld.AppendLine("(                                             ")
        _str_bld.AppendFormat("	SUM(" & _col_month & ") FOR AMM_MONTH IN({0})               ", _dates_bld.ToString())
        _str_bld.AppendLine(") AS PivotTable															 ")
        _str_bld.AppendLine("  ORDER BY AC_ACCOUNT_ID,AMM_TRACK_DATA            ")

        ''******************''
        ''    DAILY MODE    ''
        ''******************''
      ElseIf opt_daily.Checked Then

        m_months_headers.Add(dtp_from.Value.ToString(DATE_MONTH_NAME_AND_YEAR).ToUpperInvariant())
        _first_day_of_month = dtp_from.Value.AddDays(1 - dtp_from.Value.Day)

        For _idx_date = 0 To m_number_of_date_columns - 1
          _str_date = _first_day_of_month.AddDays(_idx_date).ToString("yyyy-MM-dd")

          m_months_headers.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(706) & " " & _idx_date + 1)  '' (706) Day _idx_date
          _dates_bld.AppendFormat("[{0}]", _str_date)          '' To Build Sql Pivot Columns and also where filters.
          If _idx_date < m_number_of_date_columns - 1 Then
            _dates_bld.Append(",")
          End If
        Next

        m_number_of_date_columns = m_number_of_date_columns + 1 '' To add a column with total month amount

        _str_bld.AppendLine("SELECT   AC_ACCOUNT_ID  ")
        _str_bld.AppendLine("       , TRACK_DATA  ")
        _str_bld.AppendLine("       , AC_HOLDER_NAME3 ")
        _str_bld.AppendLine("       , AC_HOLDER_NAME1 ")
        _str_bld.AppendLine("       , AC_HOLDER_NAME2 ")
        _str_bld.AppendLine("       , AC_HOLDER_LEVEL ")
        _str_bld.AppendLine("       , MONTH_TOTAL   ")
        _str_bld.AppendFormat("     , {0}          ", _dates_bld.ToString()).AppendLine() '' List of columns with reported date range

        ' Show account details
        If chk_show_account_details.Checked Then
          _str_bld.AppendLine(mdl_account_for_report.AccountFieldsSql(False))
        End If
        _str_bld.AppendLine("  FROM   (            ")
        _str_bld.AppendLine("           SELECT   DATEADD(HOUR, -DATEPART(HOUR,  AMD_DAY),  AMD_DAY) AMD_DAY  ")
        _str_bld.AppendLine("                  , AC_ACCOUNT_ID  ")
        _str_bld.AppendLine("                  , AC_HOLDER_NAME3 ")
        _str_bld.AppendLine("                  , AC_HOLDER_NAME1 ")
        _str_bld.AppendLine("                  , AC_HOLDER_NAME2 ")
        _str_bld.AppendLine("                  , AML_DAILY.AMD_TRACK_DATA AS TRACK_DATA  ")
        _str_bld.AppendLine("                  , AC_HOLDER_LEVEL ")
        _str_bld.AppendLine("                  , " & _col_month & " AS MONTH_TOTAL")
        _str_bld.AppendLine("                  , " & _col_day & " AS DAILY_TOTAL")

        ' Show account details
        If chk_show_account_details.Checked Then
          _str_bld.AppendLine(mdl_account_for_report.AccountFieldsSql())
        End If
        _str_bld.AppendLine("             FROM   AML_DAILY ")
        _str_bld.AppendLine("       INNER JOIN   ACCOUNTS ON AMD_ACCOUNT_ID = AC_ACCOUNT_ID")

        If Not String.IsNullOrEmpty(_account_filter) Then
          _str_bld.AppendFormat("          AND   {0}", _account_filter).AppendLine()
        End If

        _str_bld.AppendLine("       INNER JOIN   AML_MONTHLY ")
        _str_bld.AppendLine("               ON   AMM_ACCOUNT_ID = AMD_ACCOUNT_ID AND AMM_TRACK_DATA=AMD_TRACK_DATA")
        _str_bld.AppendLine("              AND   CAST(MONTH(AMM_MONTH) AS VARCHAR(2)) + '-' + CAST(YEAR(AMM_MONTH) AS VARCHAR(4)) ") '' Match by month and year...
        _str_bld.AppendLine("                  = CAST(MONTH(AMD_DAY) AS VARCHAR(2)) + '-' + CAST(YEAR(AMD_DAY) AS VARCHAR(4)) ")
        _str_bld.AppendLine("            WHERE   DATEADD(HOUR, -DATEPART(HOUR,  AMD_DAY),  AMD_DAY) >= @pDateFrom ")
        _str_bld.AppendLine("              AND   DATEADD(HOUR, -DATEPART(HOUR,  AMD_DAY),  AMD_DAY)  < @pDateTo")

        'Call WhereLimits(_str_bld, _sql_command, m_report_mode)

        _str_bld.AppendLine("          ) AS SourceTable")

        _str_bld.AppendLine("PIVOT                                         ")
        _str_bld.AppendLine("(                                             ")
        _str_bld.AppendFormat("	SUM(DAILY_TOTAL) FOR AMD_DAY IN({0})               ", _dates_bld.ToString()).AppendLine()
        _str_bld.AppendLine(") AS PivotTable															  ")
        _str_bld.AppendLine("  ORDER BY AC_ACCOUNT_ID,TRACK_DATA            ")

      End If

      _sql_command.CommandText = _str_bld.ToString()

      ''' Report by 'natural' day
      _sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _from_date
      _sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _to_date

    End If

    Return _sql_command

  End Function

  Private Sub WhereLimits(ByVal StrBld As StringBuilder, _
                          ByVal SqlCmd As SqlCommand, _
                          ByVal ReportMode As ENUM_MONEY_LAUNDERING_REPORT_MODE)
    Dim _col_name As String

    _col_name = ""

    If ReportMode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
      _col_name = "AMM_SPLIT_A"
    ElseIf ReportMode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE Then
      _col_name = "AMM_PRIZE"
    End If

    If opt_next_to_identify.Enabled AndAlso opt_next_to_identify.Checked Then
      StrBld.AppendFormat("              AND   {0} >= @pWIdentifyLimit", _col_name).AppendLine()
      StrBld.AppendFormat("              AND   {0} < @pIdentifyLimit", _col_name).AppendLine()
      SqlCmd.Parameters.Add("@pWIdentifyLimit", SqlDbType.Money).Value = m_warning_identification_limit
      SqlCmd.Parameters.Add("@pIdentifyLimit", SqlDbType.Money).Value = m_identification_limit
    End If

    If opt_identified.Checked Then
      StrBld.AppendFormat("              AND   {0} >= @pIdentifyLimit", _col_name).AppendLine()
      SqlCmd.Parameters.Add("@pIdentifyLimit", SqlDbType.Money).Value = m_identification_limit
    End If

    If opt_next_to_report.Enabled AndAlso opt_next_to_report.Checked Then
      StrBld.AppendFormat("              AND   {0} >= @pWReportLimit", _col_name).AppendLine()
      StrBld.AppendFormat("              AND   {0} < @pReportLimit", _col_name).AppendLine()
      SqlCmd.Parameters.Add("@pWReportLimit", SqlDbType.Money).Value = m_warning_report_limit
      SqlCmd.Parameters.Add("@pReportLimit", SqlDbType.Money).Value = m_report_limit
    End If

    If opt_report_sat.Checked Then
      StrBld.AppendFormat("              AND   {0} >= @pReportLimit", _col_name).AppendLine()
      SqlCmd.Parameters.Add("@pReportLimit", SqlDbType.Money).Value = m_report_limit
    End If

  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _number_of_date_columns As Byte

    If Not Me.uc_account.ValidateFormat() Then
      Return False
    End If

    If dtp_to.Enabled AndAlso dtp_from.Enabled AndAlso dtp_to.Value < dtp_from.Value Then
      NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If opt_month.Checked Then
      _number_of_date_columns = Math.Abs((dtp_to.Value.Month - dtp_from.Value.Month) + 12 * (dtp_to.Value.Year - dtp_from.Value.Year)) + 1
      If _number_of_date_columns > GRID_MAX_MONTH_COLUMNS Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2513), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GRID_MAX_MONTH_COLUMNS)

        Return False
      End If
    ElseIf opt_daily.Checked Then
      _number_of_date_columns = DateTime.DaysInMonth(dtp_from.Value.Year, dtp_from.Value.Month)

    End If

    m_number_of_date_columns = _number_of_date_columns

    m_sql_init_columns_holder_data = IIf(Not Me.opt_movements.Checked, SQL_INIT_COLUMNS_AMOUNT, SQL_INIT_COLUMNS_AMOUNT_MOVEMENT) _
                                    + IIf(opt_daily.Checked, 1, 0) _
                                    + m_number_of_date_columns

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :
  Protected Overrides Sub GUI_EditSelectedItem()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2409, Accounts.getAMLName())
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(gb_detail.Text, m_detail) '' Detalle

    If Not String.IsNullOrEmpty(m_date_range_to) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), m_date_range_from) '' Period desde
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), m_date_range_to) '' Period hasta
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830), m_date_range_from) ' Period
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)      ' Cuenta
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)   ' Tarjeta
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)       ' Titular

    Select Case m_opt_limit
      Case ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.Id
        PrintData.SetFilter(gb_limits.Text, opt_next_to_identify.Text)
      Case ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION.Id
        PrintData.SetFilter(gb_limits.Text, opt_identified.Text)
      Case ENUM_LAUNDERING_REPORT_LIMIT.REPORT_WARNING.Id
        PrintData.SetFilter(gb_limits.Text, opt_next_to_report.Text)
      Case ENUM_LAUNDERING_REPORT_LIMIT.REPORT.Id
        PrintData.SetFilter(gb_limits.Text, opt_report_sat.Text)
      Case Else
    End Select

    If m_opt_limit = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.Id Then
      PrintData.SetFilter(chk_show_not_identified.Text, IIf(m_show_not_identified_name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))) '' Limites
    End If

    Select Case CType(Me.cmb_group.Value, ENUM_GROUP_BY_MODE)
      Case ENUM_GROUP_BY_MODE.MODE_0
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2830))
      Case ENUM_GROUP_BY_MODE.MODE_1
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2831))
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2833), IIf(m_show_subtotals, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
      Case ENUM_GROUP_BY_MODE.MODE_3
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2832))
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2833), IIf(m_show_subtotals, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
      Case Else
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2651), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2830))
    End Select
    If Me.chk_show_amounts_below_limit.Enabled Then
      If Me.chk_show_amounts_below_limit.Checked Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5077), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
      Else
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5077), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
      End If
    End If

    If chk_show_account_details.Enabled Then
      If chk_show_account_details.Checked Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2502), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
      Else
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2502), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
      End If
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_account = ""
    m_track_data = ""
    m_holder = ""
    m_date_range_from = ""
    m_date_range_to = ""
    m_detail = ""
    m_show_not_identified_name = False

    m_account = uc_account.Account()
    m_track_data = uc_account.TrackData()
    m_holder = uc_account.Holder()

    m_date_range_from = dtp_from.Value.ToString(DATE_MONTH_NAME_AND_YEAR).ToUpperInvariant()

    If dtp_to.Enabled Then
      m_date_range_to = dtp_to.Value.ToString(DATE_MONTH_NAME_AND_YEAR).ToUpperInvariant()
    End If

    If opt_month.Checked Then
      m_detail = opt_month.Text
    ElseIf opt_daily.Checked Then
      m_detail = opt_daily.Text
    Else
      m_detail = opt_movements.Text
    End If

    If opt_next_to_identify.Enabled AndAlso opt_next_to_identify.Checked Then
      m_opt_limit = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.Id
    End If

    If opt_identified.Checked Then
      m_opt_limit = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION.Id

    End If

    If opt_next_to_report.Enabled AndAlso opt_next_to_report.Checked Then
      m_opt_limit = ENUM_LAUNDERING_REPORT_LIMIT.REPORT_WARNING.Id

    End If

    If opt_report_sat.Checked Then
      m_opt_limit = ENUM_LAUNDERING_REPORT_LIMIT.REPORT.Id

    End If

    If chk_show_not_identified.Checked Then
      m_show_not_identified_name = True
    End If

    m_group_type = CType(Me.cmb_group.Value, ENUM_GROUP_BY_MODE)

    m_show_subtotals = Me.chk_total_account.Checked

  End Sub ' GUI_ReportUpdateFilters

#End Region
#End Region

#Region "Public Sub"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Private Sub"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _grid_num_columns As Integer
    Dim _idx_month_column As Byte
    Dim _report_mode_description As String

    _report_mode_description = String.Empty
    If opt_movements.Checked Then
      GRID_INIT_COLUMNS_AMOUNT = 7
      '_grid_num_columns = GRID_COLUMNS + 3
    Else
      GRID_INIT_COLUMNS_AMOUNT = 9
      '_grid_num_columns = GRID_COLUMNS + m_number_of_date_columns
    End If
    _grid_num_columns = GRID_COLUMNS + m_number_of_date_columns

    '' view account columns
    If Me.chk_show_account_details.Checked Then
      _grid_num_columns = _grid_num_columns + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA
    End If

    With Me.Grid
      Call .Init(_grid_num_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COL_INDEX).Header(0).Text = " "
      .Column(GRID_COL_INDEX).Header(1).Text = " "
      .Column(GRID_COL_INDEX).Width = 200
      .Column(GRID_COL_INDEX).HighLightWhenSelected = False
      .Column(GRID_COL_INDEX).IsColumnPrintable = False

      '' Account id
      With .Column(GRID_COL_ACCOUNT_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
        .Width = 1150
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      ''' TrackData
      With .Column(GRID_COL_TRACK_DATA)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
        .Width = IIf(Not opt_movements.Checked, 2300, 0)
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      '' Account Holder Name
      With .Column(GRID_COL_ACCOUNT_HOLDER_NAME1)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186)
        .Width = 1600
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      With .Column(GRID_COL_ACCOUNT_HOLDER_NAME2)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1188)
        .Width = 1600
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      With .Column(GRID_COL_ACCOUNT_HOLDER_NAME3)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1189)
        .Width = 1600
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      If m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE Then
        _report_mode_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1592)        '' "Winnings"
      ElseIf m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
        _report_mode_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1318)        '' "Recharge"
      End If

      'Columns for Daily Movements
      ' Date
      With .Column(GRID_COL_MOVEMENT_DATE)
        .Header(0).Text = _report_mode_description
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201) ' "Fecha" 
        .Width = IIf(Not opt_movements.Checked, 0, 2000)
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      ' Amount
      With .Column(GRID_COL_MOVEMENT_AMOUNT)
        .Header(0).Text = _report_mode_description
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(287) ' "Monto" 
        .Width = IIf(Not opt_movements.Checked, 0, 1600)
      End With

      With .Column(GRID_COL_MOVEMENT_CLIENT_CARD)
        .Header(0).Text = _report_mode_description 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
        .Width = IIf(Not opt_movements.Checked, 0, 2300)
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With


      '' Amount columns
      If Not opt_movements.Checked Then
        For _idx_month_column = 0 To Math.Max(m_number_of_date_columns - 1, 0)
          With .Column(GRID_INIT_COLUMNS_AMOUNT + _idx_month_column)
            If m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE Then
              _report_mode_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1592)        '' "Winnings"
            ElseIf m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
              _report_mode_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1318)        '' "Recharge"
            End If
            .Header(0).Text = _report_mode_description
            If _idx_month_column < m_months_headers.Count _
                AndAlso Not m_months_headers(_idx_month_column) Is Nothing Then
              .Header(1).Text = m_months_headers(_idx_month_column)
              'Else
              '  .Header(1).Text = "xMonth" & _idx_month_column + 1
            End If
            .Width = 1700
            .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
          End With
        Next
      End If
    End With

    '' Add account columns
    If Me.chk_show_account_details.Checked Then
      mdl_account_for_report.StyleSheetHolderData(Grid, GRID_COLUMNS + m_number_of_date_columns, True, chk_show_account_details.Checked)
    End If


  End Sub   ' GUI_StyleSheet
  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _initial_date As Date

    _initial_date = WSI.Common.Misc.TodayOpening()

    ' Show account data 
    Me.chk_show_account_details.Checked = False

    'Hide Amounts under limit
    Me.chk_show_amounts_below_limit.Checked = False

    '' Account Fields Filter
    Call uc_account.Clear()

    '' Time Range
    If _initial_date.Day < 17 Then
      _initial_date = _initial_date.AddMonths(-1)
    End If
    dtp_from.Value = _initial_date.AddDays(1 - _initial_date.Day)
    dtp_to.Value = dtp_from.Value
    m_months_headers.Add(dtp_from.Value.ToString(DATE_MONTH_NAME_AND_YEAR).ToUpperInvariant())

    opt_month.Checked = True
    opt_report_sat.Checked = True
    chk_show_not_identified.Checked = False

    Select Case CType(GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0, 0, 3), ENUM_GROUP_BY_MODE)
      Case ENUM_GROUP_BY_MODE.MODE_0
        Me.cmb_group.Value = ENUM_GROUP_BY_MODE.MODE_0
        Me.chk_total_account.Checked = False
        Me.chk_total_account.Enabled = False
      Case ENUM_GROUP_BY_MODE.MODE_1
        Me.cmb_group.Value = ENUM_GROUP_BY_MODE.MODE_1
        Me.chk_total_account.Checked = False
        Me.chk_total_account.Enabled = True
      Case ENUM_GROUP_BY_MODE.MODE_3
        Me.cmb_group.Value = ENUM_GROUP_BY_MODE.MODE_3
        Me.chk_total_account.Checked = False
        Me.chk_total_account.Enabled = True
      Case Else
        Me.cmb_group.Value = ENUM_GROUP_BY_MODE.MODE_0
        Me.chk_total_account.Checked = False
        Me.chk_total_account.Enabled = False
    End Select

  End Sub 'SetDefaultValues

  Private Sub InitLimitsFilter()

    Dim _form_mode_name As String

    _form_mode_name = String.Empty

    If m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.PRIZE Then
      _form_mode_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2522)           '' 2522   "Prizes"
    ElseIf m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
      _form_mode_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2521)           '' 2521   "Recharges"
    End If
    gb_limits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2516, _form_mode_name) ''  "[ Prizes | Recharges ] greater than or equal to"

    opt_next_to_identify.Text = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.ShortDescription(m_report_mode) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2517)    ' Next to identify
    opt_identified.Text = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION.ShortDescription(m_report_mode) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2518)                  ' Identified
    opt_next_to_report.Text = ENUM_LAUNDERING_REPORT_LIMIT.REPORT_WARNING.ShortDescription(m_report_mode) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2520)              ' Next to report
    opt_report_sat.Text = ENUM_LAUNDERING_REPORT_LIMIT.REPORT.ShortDescription(m_report_mode) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2519)                          ' To report to SAT
    chk_show_not_identified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2523)

    m_warning_identification_limit = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.LimitValue(m_report_mode)
    m_identification_limit = ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION.LimitValue(m_report_mode)
    m_warning_report_limit = ENUM_LAUNDERING_REPORT_LIMIT.REPORT_WARNING.LimitValue(m_report_mode)
    m_report_limit = ENUM_LAUNDERING_REPORT_LIMIT.REPORT.LimitValue(m_report_mode)

    chk_show_not_identified.Enabled = False

    If m_warning_identification_limit <= 0 Then
      opt_next_to_identify.Visible = False
      chk_show_not_identified.Visible = False
      gb_limits.Height = gb_limits.Height - (opt_identified.Top - opt_next_to_identify.Top)
      opt_report_sat.Location = opt_identified.Location
      opt_next_to_report.Location = New Point(opt_next_to_identify.Location.X, chk_show_not_identified.Location.Y)
      opt_identified.Location = opt_next_to_identify.Location
    End If

    If m_warning_report_limit <= 0 Then
      opt_next_to_report.Visible = False
      gb_limits.Height = gb_limits.Height - (opt_report_sat.Top - opt_next_to_report.Top)
      opt_report_sat.Location = opt_next_to_report.Location
    End If

  End Sub
  ' PURPOSE : Process new Rows.
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)

    If m_first_time Then
      m_subtotal.IsAnonymous = (DbRow.Value(SQL_COL_ACCOUNT_HOLDER_LEVEL) = 0)
      Call UpdatePrintRow()
    ElseIf Not m_subtotal.CurrentRow.Value(SQL_COL_ACCOUNT_ID).Equals(DbRow.Value(SQL_COL_ACCOUNT_ID)) Then
      m_subtotal.DrawRow = FilterAccountByAccountId() OrElse opt_movements.Checked 'jca
      If m_subtotal.PrintSubTotal Then
        Call DrawSubTotalRow(Me.m_subtotal.DrawRow)
      End If
      Call ResetTotalCounters()
      m_subtotal.IsAnonymous = (DbRow.Value(SQL_COL_ACCOUNT_HOLDER_LEVEL) = 0)
      Call UpdatePrintRow()
    End If

    m_subtotal.CurrentRow = DbRow

    Select Case m_group_type
      Case ENUM_GROUP_BY_MODE.MODE_0
        Call UpdateSubTotals(DbRow)
      Case ENUM_GROUP_BY_MODE.MODE_1
        If m_subtotal.IsAnonymous Then
          m_subtotal.DrawRow = FilterAccountByTrackData(DbRow)
          If m_subtotal.DrawRow Then
            Call UpdateSubTotals(DbRow)
          End If
        Else
          Call UpdateSubTotals(DbRow)
        End If
      Case ENUM_GROUP_BY_MODE.MODE_3
        m_subtotal.DrawRow = FilterAccountByTrackData(DbRow)
        If m_subtotal.DrawRow Then
          Call UpdateSubTotals(DbRow)
        End If
      Case Else
        m_subtotal.DrawRow = True
    End Select

    m_first_time = False

  End Sub 'ProcessCounters
  ' PURPOSE : Set m_total.add_row to know if we have to add rows or subtotals
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdatePrintRow()

    Select Case m_group_type
      Case ENUM_GROUP_BY_MODE.MODE_0
        ' Personal and Anonymous Grouped by Account Id
        If Me.opt_movements.Checked Then
          If Me.m_show_subtotals Then
            m_subtotal.DrawRow = True
            m_subtotal.PrintSubTotal = True
          Else
            m_subtotal.DrawRow = False
            m_subtotal.PrintSubTotal = False
          End If
        Else
          m_subtotal.DrawRow = False
          m_subtotal.PrintSubTotal = True
        End If
      Case ENUM_GROUP_BY_MODE.MODE_1
        ' Personal Grouped by Account Id and Anonymous Grouped by Account Id and TackData
        If Me.opt_movements.Checked Then
          If Me.m_show_subtotals Then
            m_subtotal.DrawRow = True
            m_subtotal.PrintSubTotal = True
          Else
            m_subtotal.DrawRow = False
            m_subtotal.PrintSubTotal = False
          End If
        Else
          m_subtotal.DrawRow = m_subtotal.IsAnonymous
          m_subtotal.PrintSubTotal = True
          If m_subtotal.IsAnonymous Then
            m_subtotal.PrintSubTotal = m_show_subtotals
          End If
        End If

      Case ENUM_GROUP_BY_MODE.MODE_3
        ' Personal and Anonymous Grouped by Account Id and TackData
        If Me.opt_movements.Checked Then
          If Me.m_show_subtotals Then
            m_subtotal.DrawRow = True
            m_subtotal.PrintSubTotal = True
          Else
            m_subtotal.DrawRow = False
            m_subtotal.PrintSubTotal = False
          End If
        Else
          m_subtotal.DrawRow = True
          m_subtotal.PrintSubTotal = m_show_subtotals
        End If
      Case Else
        m_subtotal.DrawRow = True
    End Select

  End Sub 'UpdatePrintRow
  ' PURPOSE : Update the total object with the values of row.
  '
  '  PARAMS :
  '     - INPUT : Current Row
  '
  '     - OUTPUT :
  '
  ' RETURNS : True if works fine
  '
  Private Sub UpdateSubTotals(ByVal DbRow As CLASS_DB_ROW)
    Dim _idx_month_column As Integer
    Dim _amount As Decimal

    For _idx_month_column = 0 To Math.Max(m_number_of_date_columns - 1, 0)

      If Not m_subtotal.Amounts.ContainsKey(_idx_month_column) Then
        m_subtotal.Amounts.Add(_idx_month_column, 0)
      End If

      If DbRow.IsNull(SQL_INIT_COLUMNS_AMOUNT + _idx_month_column) Then
        Continue For
      Else
        _amount = DbRow.Value(SQL_INIT_COLUMNS_AMOUNT + _idx_month_column)
      End If

      'YNM 13-AUG-2015
      If opt_movements.Checked Then
        Select Case DbRow.Value(SQL_COL_TYPE)
          Case WSI.Common.MovementType.Cancellation, _
               WSI.Common.MovementType.HIDDEN_RechargeNotDoneWithCash
            _amount = _amount * -1
        End Select
      End If

      m_subtotal.Amounts(_idx_month_column) += _amount

    Next

  End Sub 'UpdateSubTotals
  ' PURPOSE : Draw a row with subtotal values
  '
  '  PARAMS :
  '     - INPUT : Color
  '
  '     - OUTPUT :
  '
  ' RETURNS : True if works fine
  '
  Private Sub DrawSubTotalRow(ByVal Draw As Boolean)
    Dim _idx_row As Integer
    Dim _is_total As Boolean
    Dim _amount As Decimal

    If Not Draw Then
      Return
    End If

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1
    _is_total = False

    Select Case m_group_type
      Case ENUM_GROUP_BY_MODE.MODE_0
        If Me.opt_movements.Checked AndAlso m_show_subtotals Then
          Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          _is_total = True
        End If

      Case ENUM_GROUP_BY_MODE.MODE_1
        If (m_subtotal.IsAnonymous AndAlso m_show_subtotals) OrElse _
           (Me.opt_movements.Checked AndAlso m_show_subtotals) Then

          Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          _is_total = True
        End If
      Case ENUM_GROUP_BY_MODE.MODE_3
        Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        _is_total = True
      Case Else
    End Select

    ' Account Id
    Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_ID).Value = m_subtotal.CurrentRow.Value(SQL_COL_ACCOUNT_ID)

    If m_subtotal.CurrentRow.Value(SQL_COL_TRACK_DATA) IsNot DBNull.Value AndAlso Not String.IsNullOrEmpty(m_subtotal.CurrentRow.Value(SQL_COL_TRACK_DATA)) And Not _is_total Then
      Me.Grid.Cell(_idx_row, GRID_COL_TRACK_DATA).Value = m_subtotal.CurrentRow.Value(SQL_COL_TRACK_DATA)
    Else
      Me.Grid.Cell(_idx_row, GRID_COL_TRACK_DATA).Value = String.Empty
    End If

    ' _key is a month or day

    For Each _indx As Integer In m_subtotal.Amounts.Keys

      _amount = m_subtotal.Amounts(_indx)
      If Not IsInInterval(m_subtotal.Amounts(_indx)) Then
        If Not Me.chk_show_amounts_below_limit.Checked Then
          Grid.Cell(_idx_row, GRID_INIT_COLUMNS_AMOUNT + _indx).Value = String.Empty
        Else
          Grid.Cell(_idx_row, GRID_INIT_COLUMNS_AMOUNT + _indx).Value = IIf(_amount > 0, GUI_FormatCurrency(_amount), String.Empty)
        End If
      Else
        Grid.Cell(_idx_row, GRID_INIT_COLUMNS_AMOUNT + _indx).Value = GUI_FormatCurrency(_amount)
        If Me.chk_show_amounts_below_limit.Checked And Not _is_total Then
          Grid.Cell(_idx_row, GRID_INIT_COLUMNS_AMOUNT + _indx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
        End If
      End If

    Next

    ' Account Holder Name
    If m_show_not_identified_name Or m_opt_limit <> ENUM_LAUNDERING_REPORT_LIMIT.IDENTIFICATION_WARNING.Id Then
      If Not m_subtotal.CurrentRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME1) Then
        Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_HOLDER_NAME1).Value = m_subtotal.CurrentRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME1)
      End If
      If Not m_subtotal.CurrentRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME2) Then
        Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_HOLDER_NAME2).Value = m_subtotal.CurrentRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME2)
      End If
      If Not m_subtotal.CurrentRow.IsNull(SQL_COL_ACCOUNT_HOLDER_NAME3) Then
        Me.Grid.Cell(_idx_row, GRID_COL_ACCOUNT_HOLDER_NAME3).Value = m_subtotal.CurrentRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME3)
      End If
    End If

    ' Holder data colums
    If Not _is_total AndAlso Me.chk_show_account_details.Checked Then
      mdl_account_for_report.SetupRowHolderData(Me.Grid, m_subtotal.CurrentRow, _idx_row, GRID_COLUMNS + m_number_of_date_columns, m_sql_init_columns_holder_data)
    End If
    'End If
  End Sub 'DrawRow
  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters()
    m_subtotal.Amounts.Clear()
  End Sub ' ResetTotalCounters

  Private Function FilterAccountByAccountId() As Boolean
    Dim _amount As Decimal

    ' _key is a month or day
    For Each _indx As Integer In m_subtotal.Amounts.Keys

      _amount = m_subtotal.Amounts(_indx)

      If IsInInterval(_amount) Then
        Return True
      End If

    Next

    Return False

  End Function

  Private Function FilterAccountByTrackData(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _idx_month_column As Integer
    Dim _amount As Decimal

    For _idx_month_column = 0 To Math.Max(m_number_of_date_columns - 1, 0)

      If DbRow.IsNull(SQL_INIT_COLUMNS_AMOUNT + _idx_month_column) Then
        Continue For
      End If

      _amount = DbRow.Value(SQL_INIT_COLUMNS_AMOUNT + _idx_month_column)

      'if amount is belong to the interval or movements have to be shown
      If IsInInterval(_amount) OrElse opt_movements.Checked Then 'JCA
        Return True
      End If

    Next

    Return False

  End Function

  Private Function IsInInterval(ByVal Amount As Decimal) As Boolean

    If opt_next_to_identify.Enabled AndAlso opt_next_to_identify.Checked Then
      If Amount >= m_warning_identification_limit And Amount < m_identification_limit Then
        Return True
      End If
    End If

    If opt_identified.Checked Then
      If Amount >= m_identification_limit Then
        Return True
      End If
    End If

    If opt_next_to_report.Enabled AndAlso opt_next_to_report.Checked Then
      If Amount >= m_warning_report_limit And Amount < m_report_limit Then
        Return True
      End If
    End If

    If opt_report_sat.Checked Then
      If Amount >= m_report_limit Then
        Return True
      End If
    End If

    Return False

  End Function

  Private Function GetSqlCommandMovements() As SqlCommand
    Dim _sb As StringBuilder
    Dim _command As SqlCommand
    Dim _to_date As DateTime
    Dim _from_date As DateTime
    Dim _today_opening As DateTime
    Dim _account_filter As String
    Dim _amount As String

    'YNM 13-AUG-2015
    _today_opening = WSI.Common.Misc.TodayOpening()
    _from_date = New DateTime(dtp_from.Value.Year, dtp_from.Value.Month, 1, _today_opening.Hour, _today_opening.Minute, 0)

    If (_from_date.Month = WGDB.Now.Month) Then
      _to_date = WSI.Common.Misc.TodayOpening()
    Else
      _to_date = _from_date.AddMonths(1)
    End If

    _sb = New StringBuilder
    _command = New SqlCommand()
    _account_filter = uc_account.GetFilterSQL()
    _amount = GetAmountWhere()
    'If Me.m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
    '  _amount_type = "AMM_SPLIT_A"
    'Else
    '  _amount_type = "AMM_PRIZE"
    'End If

    _sb.AppendLine("    SELECT   AC_ACCOUNT_ID                                        ")
    _sb.AppendLine("           , AM_TRACK_DATA                                        ")
    _sb.AppendLine("           , AC_HOLDER_NAME3                                      ")
    _sb.AppendLine("           , AC_HOLDER_NAME1                                      ")
    _sb.AppendLine("           , AC_HOLDER_NAME2                                      ")
    _sb.AppendLine("           , AC_HOLDER_LEVEL                                      ")
    If Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE Then
      _sb.AppendLine("         , CASE WHEN (AM_TYPE IN (77, 1000)) THEN AM_ADD_AMOUNT + AM_SUB_AMOUNT ELSE AM_ADD_AMOUNT END ")
      '_sb.AppendLine("         , AM_ADD_AMOUNT                                        ")
    Else
      _sb.AppendLine("         , AM_SUB_AMOUNT                                        ")
    End If

    _sb.AppendLine("           , AM_DATETIME                                          ")
    _sb.AppendLine("           , AM_TYPE                                              ")
    If chk_show_account_details.Checked Then
      _sb.AppendLine(mdl_account_for_report.AccountFieldsSql(True))
    End If


    Select Case (m_group_type)
      Case ENUM_GROUP_BY_MODE.MODE_3
        ' Show account details
        If chk_show_account_details.Checked Then
          _sb.AppendLine(mdl_account_for_report.AccountFieldsSql(True, False))
        End If
        _sb.AppendLine("      FROM   ACCOUNTS          ")
        _sb.AppendLine("      INNER JOIN   ACCOUNT_MOVEMENTS ON AC_ACCOUNT_ID = AM_ACCOUNT_ID ")
        _sb.AppendLine("      INNER JOIN  AML_MONTHLY ON AC_ACCOUNT_ID = AMM_ACCOUNT_ID AND " & _amount)
        _sb.AppendLine("                  AND AMM_MONTH >= @pDateFrom AND AMM_MONTH < @pDateTo")
        _sb.AppendLine("                  AND AM_TRACK_DATA = AMM_TRACK_DATA ")
      Case Else
        _sb.AppendLine("      FROM   ACCOUNT_MOVEMENTS WITH (INDEX(IX_am_account_id_type_datetime))   ")
        _sb.AppendLine("INNER JOIN   (SELECT AC_ACCOUNT_ID      ")
        _sb.AppendLine("                  , AC_HOLDER_NAME3     ")
        _sb.AppendLine("                  , AC_HOLDER_NAME1     ")
        _sb.AppendLine("                  , AC_HOLDER_NAME2     ")
        _sb.AppendLine("                  , AC_HOLDER_LEVEL     ")
        _sb.AppendLine("                  , AC_TRACK_DATA       ")
        If chk_show_account_details.Checked Then
          _sb.AppendLine(mdl_account_for_report.AccountFieldsSql(True, False))
        End If
        _sb.AppendLine("                FROM ACCOUNTS WITH (INDEX (PK_accounts)) ")
        _sb.AppendLine("                    INNER JOIN          ")
        _sb.AppendLine("                               (SELECT AMM_ACCOUNT_ID, ")
        If Me.m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
          _sb.AppendLine("                                      SUM(AMM_SPLIT_A) AS AMM_SPLIT_A ")
        Else
          _sb.AppendLine("                                      SUM(AMM_PRIZE) AS AMM_PRIZE ")
        End If
        _sb.AppendLine("                                FROM AML_MONTHLY WITH (INDEX (IX_amm_account_month))")
        _sb.AppendLine("                                WHERE AMM_MONTH >= @pDateFrom              ")
        _sb.AppendLine("                                AND   AMM_MONTH < @pDateTo          ")
        _sb.AppendLine("                                GROUP BY AMM_ACCOUNT_ID           ")
        _sb.AppendLine("                              ) AS AMM ON AC_ACCOUNT_ID = AMM.AMM_ACCOUNT_ID AND " & _amount)
        _sb.AppendLine("              ) AS AC ON AM_ACCOUNT_ID = AC.AC_ACCOUNT_ID ")
    End Select

    _sb.AppendLine(" WHERE AM_ACCOUNT_ID > 0 ")
    _sb.AppendLine("    AND (AM_DATETIME >= @pDateFrom AND AM_DATETIME < @pDateTo)")
    If Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE Then
      _sb.AppendLine("     	 AND   AM_TYPE  IN(@pMovementType, @pMovementType1, @pMovementType2)")
    Else
      _sb.AppendLine("     	 AND   AM_TYPE  = @pMovementType")
    End If

        _sb.AppendLine("     	 AND (AM_UNDO_STATUS IS NULL OR AM_UNDO_STATUS <> 2)") 'EOR 13-FEB-2018

    If Not String.IsNullOrEmpty(_account_filter) Then
      _sb.AppendLine("     AND " & _account_filter)
    End If
    _sb.AppendLine("  ORDER BY  AC_ACCOUNT_ID, AM_DATETIME ASC                        ")


    _command.CommandText = _sb.ToString()
    _command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = _from_date
    _command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _to_date
    _command.Parameters.Add("@Amount", SqlDbType.Decimal).Value = m_report_limit
    If Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE Then
      _command.Parameters.Add("@pMovementType", SqlDbType.Int).Value = MovementType.CashIn
      _command.Parameters.Add("@pMovementType1", SqlDbType.Int).Value = MovementType.Cancellation
      _command.Parameters.Add("@pMovementType2", SqlDbType.Int).Value = MovementType.HIDDEN_RechargeNotDoneWithCash
    Else
      _command.Parameters.Add("@pMovementType", SqlDbType.Int).Value = MovementType.CashOut
    End If
    _command.CommandTimeout = 100

    Call GUI_StyleSheet()
    Return _command
  End Function

  Private Function GetAmountWhere() As String
    'Dim _sb As StringBuilder
    Dim _account_filter As String
    Dim _from_date As DateTime
    Dim _to_date As DateTime
    Dim _amount_type As String
    Dim _amount As String

    _from_date = New DateTime(dtp_from.Value.Year, dtp_from.Value.Month, 1)
    _to_date = _from_date.AddMonths(1)
    _account_filter = uc_account.GetFilterSQL()
    _amount = ""

    If Me.m_report_mode = ENUM_MONEY_LAUNDERING_REPORT_MODE.RECHARGE Then
      _amount_type = "AMM_SPLIT_A"
    Else
      _amount_type = "AMM_PRIZE"
    End If

    If opt_next_to_identify.Enabled AndAlso opt_next_to_identify.Checked Then
      _amount = _amount_type & " >= " & GUI_LocalNumberToDBNumber(m_warning_identification_limit.ToString()) & " AND " & _
                _amount_type & " <  " & GUI_LocalNumberToDBNumber(m_identification_limit.ToString())
    End If

    If opt_identified.Checked Then
      _amount = _amount_type & " >= " & GUI_LocalNumberToDBNumber(m_identification_limit.ToString())
    End If

    If opt_next_to_report.Enabled AndAlso opt_next_to_report.Checked Then
      _amount = _amount_type & " >= " & GUI_LocalNumberToDBNumber(m_warning_report_limit.ToString()) & " AND " & _
                _amount_type & " <  " & GUI_LocalNumberToDBNumber(m_report_limit.ToString())
    End If

    If opt_report_sat.Checked Then
      _amount = _amount_type & " >= " & GUI_LocalNumberToDBNumber(m_report_limit.ToString())
    End If

    '''_sb = New StringBuilder()

    '''_sb.AppendLine("            WHERE   AM_DATETIME >= @pDateFrom   ")
    '''_sb.AppendLine("              AND   AM_DATETIME  < @pDateTo     ")
    '''If Not String.IsNullOrEmpty(_account_filter) Then
    '''  _sb.AppendLine("              AND   " & _account_filter)
    '''End If
    '''If Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE Then
    '''  _sb.AppendLine("              AND   AM_TYPE  = @pCashIn   ")
    '''Else
    '''  _sb.AppendLine("              AND   AM_TYPE  = @pCashOut  ")
    '''End If

    Return _amount
  End Function

  Private Function GUI_SetupRowsMovementDetail(ByVal RowIndex As Integer, _
                                               ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _amount As Currency

    ' Account Id
    Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_ACCOUNT_ID)
    If Not opt_next_to_identify.Checked OrElse (opt_next_to_identify.Checked AndAlso chk_show_not_identified.Checked) Then
      If DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME3) IsNot DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME3).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME3)
      End If
      If DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME2) IsNot DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME2).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME2)
      End If
      If DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME1) IsNot DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME1).Value = DbRow.Value(SQL_COL_ACCOUNT_HOLDER_NAME1)
      End If
    End If
    If DbRow.Value(SQL_COL_MOVEMENT_DATE) IsNot DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COL_MOVEMENT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COL_MOVEMENT_DATE), _
                                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If DbRow.Value(SQL_COL_MOVEMENT_AMOUNT) IsNot DBNull.Value Then
      Select Case DbRow.Value(SQL_COL_TYPE)
        Case WSI.Common.MovementType.Cancellation, _
             WSI.Common.MovementType.HIDDEN_RechargeNotDoneWithCash

          _amount = CType(DbRow.Value(SQL_COL_MOVEMENT_AMOUNT), Decimal) * -1
          Me.Grid.Cell(RowIndex, GRID_COL_MOVEMENT_AMOUNT).Value = GUI_FormatCurrency(_amount)

        Case Else

          Me.Grid.Cell(RowIndex, GRID_COL_MOVEMENT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_MOVEMENT_AMOUNT))
      End Select
    End If

    If DbRow.Value(SQL_COL_MOVEMENT_CLIENT_CARD) IsNot DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COL_MOVEMENT_CLIENT_CARD).Value = DbRow.Value(SQL_COL_MOVEMENT_CLIENT_CARD)
    End If

    ' Holder data colums
    If Me.chk_show_account_details.Checked Then
      mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMNS + m_number_of_date_columns, m_sql_init_columns_holder_data)
    End If
    Return True
  End Function

#End Region


#Region "Events"

  Private Sub dtp_from_DateChanged() Handles dtp_from.DatePickerValueChanged
    If opt_daily.Checked Or opt_movements.Checked Then
      dtp_to.Value = dtp_from.Value
    End If
  End Sub

  Private Sub rb_month_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_month.CheckedChanged, opt_movements.CheckedChanged
    dtp_to.Value = dtp_from.Value
    dtp_to.Enabled = opt_month.Checked
    Me.chk_show_amounts_below_limit.Checked = False
    Me.chk_show_amounts_below_limit.Enabled = opt_daily.Checked
    Me.lbl_color_info.Enabled = opt_daily.Checked
    Me.lbl_info.Enabled = opt_daily.Checked
    If Me.opt_movements.Checked And Me.cmb_group.Value = ENUM_GROUP_BY_MODE.MODE_0 Then
      chk_total_account.Enabled = True
    Else
      If Me.opt_movements.Checked = False And Me.cmb_group.Value = ENUM_GROUP_BY_MODE.MODE_0 Then
        chk_total_account.Enabled = False
      End If
    End If

  End Sub

  Private Sub rb_limits_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_next_to_identify.CheckedChanged
    chk_show_not_identified.Enabled = opt_next_to_identify.Checked
  End Sub

  Private Sub cmb_group_ValueChangedEvent() Handles cmb_group.ValueChangedEvent
    Select Case Me.cmb_group.SelectedIndex
      Case ENUM_GROUP_BY_MODE.MODE_0
        If opt_movements.Checked Then
          Me.chk_total_account.Enabled = True
        Else
          Me.chk_total_account.Checked = False
          Me.chk_total_account.Enabled = False
        End If
      Case Else
        Me.chk_total_account.Enabled = True
    End Select

    ''If Me.cmb_group.SelectedIndex <> ENUM_GROUP_BY_MODE.MODE_0 Then
    ''  Me.chk_total_account.Enabled = True
    ''Else
    ''  Me.chk_total_account.Checked = False
    ''  Me.chk_total_account.Enabled = False
    ''End If
  End Sub

#End Region

End Class