<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_session_open
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_session_name = New GUI_Controls.uc_entry_field
    Me.dtp_working_day = New GUI_Controls.uc_date_picker
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.dtp_working_day)
    Me.panel_data.Controls.Add(Me.ef_session_name)
    Me.panel_data.Size = New System.Drawing.Size(385, 88)
    '
    'ef_session_name
    '
    Me.ef_session_name.DoubleValue = 0
    Me.ef_session_name.IntegerValue = 0
    Me.ef_session_name.IsReadOnly = False
    Me.ef_session_name.Location = New System.Drawing.Point(23, 20)
    Me.ef_session_name.Name = "ef_session_name"
    Me.ef_session_name.PlaceHolder = Nothing
    Me.ef_session_name.Size = New System.Drawing.Size(316, 24)
    Me.ef_session_name.SufixText = "Sufix Text"
    Me.ef_session_name.SufixTextVisible = True
    Me.ef_session_name.TabIndex = 0
    Me.ef_session_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_session_name.TextValue = ""
    Me.ef_session_name.TextWidth = 90
    Me.ef_session_name.Value = ""
    Me.ef_session_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_working_day
    '
    Me.dtp_working_day.Checked = True
    Me.dtp_working_day.IsReadOnly = False
    Me.dtp_working_day.Location = New System.Drawing.Point(3, 53)
    Me.dtp_working_day.Name = "dtp_working_day"
    Me.dtp_working_day.ShowCheckBox = False
    Me.dtp_working_day.ShowUpDown = False
    Me.dtp_working_day.Size = New System.Drawing.Size(206, 24)
    Me.dtp_working_day.SufixText = "Sufix Text"
    Me.dtp_working_day.SufixTextVisible = True
    Me.dtp_working_day.TabIndex = 1
    Me.dtp_working_day.TextWidth = 110
    Me.dtp_working_day.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_cage_session_open
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(487, 97)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_cage_session_open"
    Me.ShowInTaskbar = True
    Me.Text = "frm_template_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_session_name As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_working_day As GUI_Controls.uc_date_picker
End Class
