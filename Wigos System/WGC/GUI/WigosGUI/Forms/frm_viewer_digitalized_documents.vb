'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_viewer_digitalized_documents
' DESCRIPTION:   Viewer digitalized documents
' AUTHOR:        Jos� Mart�nez L�pez
' CREATION DATE: 24-JUL-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-JUL-2013  JML    Initial version
' 09-AUG-2013  FBA    Modifications to reload imagepreview whenever the selected item is changed (via mouselick or keypress)
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
'--------------------------------------------------------------------
'135520234
'09824755241498255548

Option Explicit On
Option Strict Off
Imports WSI.Common
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_viewer_digitalized_documents
  Inherits frm_base

#Region " Members "

  Dim m_player_data As CardData
  Dim m_docment_owner As ACCOUNT_SCANNED_OWNER
  Dim m_docs_list As DocumentList
  Dim m_player_doc As ImageList
  Dim m_document_image As Image


#End Region ' Members

#Region " Property "

  Public WriteOnly Property SetPlayerData() As CardData
    Set(ByVal Value As CardData)
      m_player_data = Value
    End Set
  End Property

  Public WriteOnly Property AccountScannedOwner() As ACCOUNT_SCANNED_OWNER
    Set(ByVal Value As ACCOUNT_SCANNED_OWNER)
      m_docment_owner = Value
    End Set
  End Property




#End Region  ' Property

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_VIEWER_DIGITALIZED_DOCUMENTS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2426)

    Me.lbl_digitized_documents.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2422)

    Me.btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    'If there is documents in DataBase
    If Me.m_player_data.LoadAccountDocuments() Then

      Select Case m_docment_owner

        'Holder Documents
        Case ACCOUNT_SCANNED_OWNER.HOLDER
          m_docs_list = m_player_data.PlayerTracking.HolderScannedDocs


        Case ACCOUNT_SCANNED_OWNER.BENEFICIARY
          m_docs_list = m_player_data.PlayerTracking.BeneficiaryScannedDocs
      End Select


      Call FillDocumentList()
    End If



  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region " Private "

  Private Sub FillDocumentList()

    If (m_docs_list.Count > 0) Then

      Dim _document_type As String

      _document_type = String.Empty

      ' Document name structure (example): XXX_Y_Random Number
      ' XXX: Scanned Document Type
      ' Y: Scanned Document Owner --> Holder, Beneficiary.
      For Each _document As IDocument In m_docs_list
        If CardData.GetInfoDocName(_document.Name, _document_type) Then
          _document_type = IdentificationTypes.DocIdTypeString(Convert.ToInt32(_document_type))
          lb_documents.Items.Add(GetDocumentsTypeNameCopies(_document_type))
        End If
      Next

      lb_documents.SelectedIndex = 0
      lb_documents_SelectedIndexChanged(lb_documents, System.EventArgs.Empty)
    End If

  End Sub

  Private Function PreviewImage(ByVal Image As Image)
    Try
      pnl_document_image.BackgroundImage = Image

      Return True
    Catch Ex As Exception
      Log.Exception(Ex)

    End Try

    Return False
  End Function

  '-----------------------------------------------------------------------------------------
  ' PURPOSE :  Get a String with the document type name from list Box control, if exists --> add a number of the copy. (RFC, RFC (2), RFC (3)).
  '
  '  PARAMS :
  '      - INPUT : String with document type name
  '
  '      - OUTPUT : 
  '
  ' RETURNS : String with document type + copy number
  '
  Private Function GetDocumentsTypeNameCopies(ByVal DocumentTypeName As String) As String

    If (Not String.IsNullOrEmpty(DocumentTypeName)) Then
      Dim _copy_number As Int16

      _copy_number = 1

      If (Not String.IsNullOrEmpty(DocumentTypeName) And lb_documents.Items.Count > 0) Then

        For Each _doc_type_name As String In lb_documents.Items
          If (_doc_type_name.Contains(DocumentTypeName)) Then
            _copy_number += 1
          End If
        Next

        If (_copy_number > 1) Then
          Return String.Format("{0} ({1})", DocumentTypeName, _copy_number.ToString())
        End If

      End If
    End If

    Return DocumentTypeName

  End Function



#End Region  ' Privates

#Region " Events "

  Private Sub pnl_document_image_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pnl_document_image.Click

    Call WSI.Common.ViewImage.ViewFullScreenImage(m_document_image)

  End Sub ' pnl_document_image_Click

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent

    Me.Close()

  End Sub ' btn_cancel_ClickEvent

  'FBA 09-AUG-2013 Reloads the imagepreview whenever the selected item is changed (via mouselick or keypress)
  Private Sub lb_documents_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lb_documents.SelectedIndexChanged
    Dim _doc_image As IDocument

    _doc_image = Nothing

    If (lb_documents.Items.Count > 0 And lb_documents.SelectedItems.Count > 0) Then

      If (m_docs_list.Find(m_docs_list(lb_documents.SelectedIndex).Name, _doc_image)) Then
        If Not IsNothing(m_document_image) Then
          m_document_image.Dispose()
        End If
        m_document_image = Image.FromStream(New System.IO.MemoryStream(_doc_image.Content))
        PreviewImage(m_document_image)

      End If
    End If
  End Sub

#End Region

End Class