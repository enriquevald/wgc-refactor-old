'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_money_laundering_config
' DESCRIPTION:   Configure accounts required fields and limits for money laundering actions
' AUTHOR:        Rubén ROdríguez
' CREATION DATE: 19-Jul-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 19-JUL-2013 RRR    First Release.
' 07-AGO-2013 ANG    Fix bug #WIG-99 Disallow changes when site belongs to Multisite.
' 14-AGO-2013 JRM    Fixed GUI_GetScreenData, numeric variables were not ready for empty string. Code revision.
' 01-OCT-2013 ACM    Added two new documents types to scan.
' 17-OCT-2013 JAB    Added "check box" to ask before print user information.
' 29-OCT-2013 DLL    Added two "combo box" to configure permission to exceed report limit.
' 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes
' 18-FEB-2014 FBA    Fixed Bug WIG-641
' 19-FEB-2014 AMF    Fixed Bug WIG-647: Control minimum version
' 21-FEB-2014 JBC    Fixed Bug WIG-657: Identification types control
' 12-MAR-2014 QMP    Fixed Bug WIG-715: "SMGVDF" label not showing
' 10-JUN-2015 DHA    Added edition mode for MultSite-MultiCurrency
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_money_laundering_config
  Inherits frm_base_edit

#Region " Members "

  Private m_base_name As String

#End Region ' Members

#Region " Enums "

  Public Enum CUSTOM_TICKET_PRINT_MODE
    DONT_PRINT = 0
    PRINT = 1
    ASK_BEFORE_PRINT = 2
  End Enum

#End Region

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Short = 183

#End Region

#Region " Overrides "

  ' PURPOSE : Override form permissions to disallow writting in MultiSite Member
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    MyBase.GUI_Permissions(AndPerm)

    Dim _is_multisite_member As Boolean
    Dim _is_multiste_multicurrency As Boolean

    _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember")
    _is_multiste_multicurrency = WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency()

    AndPerm.Write = Not _is_multisite_member Or _is_multisite_member And _is_multiste_multicurrency

  End Sub

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MONEY_LAUNDERING_CONFIG

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _account_holder_info As Dictionary(Of Int32, String)
    Dim _is_multisite_member As Boolean
    Dim _is_multiste_multicurrency As Boolean

    MyBase.GUI_InitControls()

    _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember")
    _is_multiste_multicurrency = WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency()

    'setting the window name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2948, Accounts.getAMLName()) ' NLS: Configuración antilavado de dinero
    Me.lbl_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2507, Accounts.getAMLName()) 'NLS: La Configuración antilavado de dinero solo puede editarse en el MultiSite
    Me.lbl_info.Visible = _is_multisite_member And Not _is_multiste_multicurrency
    Me.ef_aml_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367)

    ' DLL 29-OCT-2013
    Me.cmb_exceed_recharge.Add(ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Never, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1287))
    Me.cmb_exceed_recharge.Add(ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.OnlyWhenCross, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2849))
    Me.cmb_exceed_recharge.Add(ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Always, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2850))
    Me.lbl_rchg_exceed_report_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2852)

    Me.cmb_exceed_prize.Add(ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Never, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1287))
    Me.cmb_exceed_prize.Add(ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.OnlyWhenCross, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2849))
    Me.cmb_exceed_prize.Add(ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Always, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2850))
    Me.lbl_prize_exceed_report_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2852)

    Me.gb_prize_report_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2391)
    Me.gb_rchg_report_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2391)
    Me.gb_prize_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2387)
    Me.gb_rchg_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2387)
    Me.chk_rchg_maximum_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2882) 'NLS: Activar límite máximo
    Me.chk_prize_maximum_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2882) 'NLS: Activar límite máximo
    Me.gb_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(282) ' Límites

    _account_holder_info = IdentificationTypes.GetIdentificationTypes()

    'setting text to the labels
    Me.chk_active_aml.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2386, Accounts.getAMLName()) ' NLS: Activar control antilavado de dinero

    Me.tp_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(417) ' NLS: Recarga
    Me.tp_prize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1592) ' NLS: Premio
    Me.lbl_rchg_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288) ' NLS: Límite identificación
    Me.chk_rchg_ident_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2388) ' NLS: Activar aviso identificación
    Me.lbl_rchg_ident_warning_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2389) ' NLS: Límite aviso
    Me.lbl_rchg_ident_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.lbl_rchg_ident_warning_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.chk_ident_not_allow_mb.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2390) ' NLS: El aviso no permite recargas en BM
    Me.lbl_rchg_report_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288) ' NLS: Límite reporte
    Me.chk_rchg_report_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2392) ' NLS: Activar aviso reporte
    Me.lbl_rchg_report_warning_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2389) ' NLS: Límite aviso
    Me.lbl_rchg_report_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las 
    Me.lbl_rchg_report_warning_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las 
    Me.chk_report_not_allow_mb.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2390) ' NLS: El aviso no permite recargas en BM
    Me.chk_rchg_report_limit_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2715) ' NLS: Mostrar mensaje en el Cajero
    Me.chk_prize_report_limit_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2715) ' NLS: Mostrar mensaje en el Cajero
    Me.lbl_prize_ident_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288) ' NLS: Límite identificación
    Me.chk_prize_ident_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2388) ' NLS: Activar aviso identificación
    Me.lbl_prize_ident_warning_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2389) ' NLS: Límite aviso
    Me.lbl_prize_ident_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.lbl_prize_ident_warning_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.lbl_prize_report_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288) ' NLS: Límite reporte
    Me.chk_prize_report_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2392) ' NLS: Activar aviso reporte
    Me.lbl_prize_report_warning_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2389) ' NLS: Límite aviso
    Me.lbl_prize_report_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.lbl_prize_report_warning_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252) ' NLS: A las
    Me.lbl_rchg_maximum_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288)
    Me.lbl_rchg_maximum_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252)
    Me.lbl_prize_maxim_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288)
    Me.lbl_prize_maximum_msg.Text = GLB_NLS_GUI_AUDITOR.GetString(252)

    'setting properties to the entry fields: lenght
    Me.ef_minimum_wage.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 6, 2)
    Me.ef_rchg_ident_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_rchg_ident_warning_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_rchg_report_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_rchg_maximum_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_rchg_report_warning_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_prize_ident_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_prize_ident_warning_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_prize_report_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_prize_report_warning_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_prize_maximum_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_aml_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' making message fields editable
    Me.chk_rchg_ident_warning.Checked = True
    Me.chk_rchg_report_warning.Checked = True
    Me.chk_prize_ident_warning.Checked = True
    Me.chk_prize_report_warning.Checked = True
    Me.chk_rchg_maximum_limit.Checked = True
    Me.chk_prize_maximum_limit.Checked = True

    'making entry fields editable
    Me.ef_minimum_wage.IsReadOnly = False
    Me.ef_minimum_wage.TextVisible = True
    Me.ef_rchg_ident_limit.IsReadOnly = False
    Me.ef_rchg_ident_warning_limit.IsReadOnly = False
    Me.ef_rchg_report_limit.IsReadOnly = False
    Me.ef_rchg_report_warning_limit.IsReadOnly = False
    Me.ef_prize_ident_limit.IsReadOnly = False
    Me.ef_prize_ident_warning_limit.IsReadOnly = False
    Me.ef_prize_report_limit.IsReadOnly = False
    Me.ef_prize_report_warning_limit.IsReadOnly = False
    Me.ef_rchg_maximum_limit.IsReadOnly = False
    Me.ef_prize_maximum_limit.IsReadOnly = False
    Me.chk_has_beneficiary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5085)

    Me.ef_minimum_wage.Focus()

    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_MONEY_LAUNDERING_CONFIG

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2479), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Me.IsEmptyOrZero(Me.ef_minimum_wage.Value) Then
      Me.ef_minimum_wage.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.ef_minimum_wage.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Me.IsEmptyOrZero(Me.ef_rchg_ident_limit.Value) Then
      Me.tc_limits.SelectedTab = Me.tp_recharge
      Me.ef_rchg_ident_limit.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_rchg_ident_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.ef_aml_name.Value.Trim()) Then
      Me.tc_limits.SelectedTab = Me.tp_recharge
      Me.ef_aml_name.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.ef_aml_name.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.tb_rchg_ident_msg.Text.Trim()) Then
      Me.tc_limits.SelectedTab = Me.tp_recharge
      Me.tb_rchg_ident_msg.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_rchg_ident_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If
    '
    If Me.chk_rchg_maximum_limit.Checked Then

      If Me.IsEmptyOrZero(Me.ef_rchg_maximum_limit.Value) Then
        Me.tc_limits.SelectedTab = Me.tp_recharge
        Me.ef_rchg_maximum_limit.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_rchg_maximum_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If String.IsNullOrEmpty(Me.tb_rchg_maximum_msg.Text.Trim()) Then
        Me.tc_limits.SelectedTab = Me.tp_recharge
        Me.tb_rchg_maximum_msg.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_rchg_maximum_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    If Me.chk_rchg_ident_warning.Checked Then

      If Me.IsEmptyOrZero(Me.ef_rchg_ident_warning_limit.Value) Then
        Me.tc_limits.SelectedTab = Me.tp_recharge
        Me.ef_rchg_ident_warning_limit.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_rchg_ident_warning_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If String.IsNullOrEmpty(Me.tb_rchg_ident_warning_msg.Text) Then
        Me.tc_limits.SelectedTab = Me.tp_recharge
        Me.tb_rchg_ident_warning_msg.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_rchg_ident_warning_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    If Me.IsEmptyOrZero(Me.ef_rchg_report_limit.Value) Then
      Me.tc_limits.SelectedTab = Me.tp_recharge
      Me.ef_rchg_report_limit.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_rchg_report_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.tb_rchg_report_msg.Text.Trim()) Then
      Me.tc_limits.SelectedTab = Me.tp_recharge
      Me.tb_rchg_report_msg.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_rchg_report_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Me.chk_rchg_report_warning.Checked Then

      If Me.IsEmptyOrZero(Me.ef_rchg_report_warning_limit.Value) Then
        Me.tc_limits.SelectedTab = Me.tp_recharge
        Me.ef_rchg_report_warning_limit.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_rchg_report_warning_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If String.IsNullOrEmpty(Me.tb_rchg_report_warning_msg.Text) Then
        Me.tc_limits.SelectedTab = Me.tp_recharge
        Me.tb_rchg_report_warning_msg.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_rchg_report_warning_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    If Me.IsEmptyOrZero(Me.ef_prize_ident_limit.Value) Then
      Me.tc_limits.SelectedTab = Me.tp_prize
      Me.ef_prize_ident_limit.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_prize_ident_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.tb_prize_ident_msg.Text.Trim()) Then
      Me.tc_limits.SelectedTab = Me.tp_prize
      Me.tb_prize_ident_msg.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_prize_ident_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Me.chk_prize_maximum_limit.Checked Then
      If Me.IsEmptyOrZero(Me.ef_prize_maximum_limit.Value) Then
        Me.tc_limits.SelectedTab = Me.tp_prize
        Me.ef_prize_maximum_limit.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_prize_maximum_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If String.IsNullOrEmpty(Me.tb_prize_maximum_msg.Text.Trim()) Then
        Me.tc_limits.SelectedTab = Me.tp_prize
        Me.tb_prize_maximum_msg.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_prize_maximum_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If
    End If
    If Me.chk_prize_ident_warning.Checked Then

      If Me.IsEmptyOrZero(Me.ef_prize_ident_warning_limit.Value) Then
        Me.tc_limits.SelectedTab = Me.tp_prize
        Me.ef_prize_ident_warning_limit.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_prize_ident_warning_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If String.IsNullOrEmpty(Me.tb_prize_ident_warning_msg.Text) Then
        Me.tc_limits.SelectedTab = Me.tp_prize
        Me.tb_prize_ident_warning_msg.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_prize_ident_warning_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    If Me.IsEmptyOrZero(Me.ef_prize_report_limit.Value) Then
      Me.tc_limits.SelectedTab = Me.tp_prize
      Me.ef_prize_report_limit.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_prize_report_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If String.IsNullOrEmpty(Me.tb_prize_report_msg.Text.Trim()) Then
      Me.tc_limits.SelectedTab = Me.tp_prize
      Me.tb_prize_report_msg.Focus()
      ' NLS: Personalización de Cuenta
      NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                        Me.lbl_prize_report_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    If Me.chk_prize_report_warning.Checked Then

      If Me.IsEmptyOrZero(Me.ef_prize_report_warning_limit.Value) Then
        Me.tc_limits.SelectedTab = Me.tp_prize
        Me.ef_prize_report_warning_limit.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_prize_report_warning_limit.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

      If String.IsNullOrEmpty(Me.tb_prize_report_warning_msg.Text) Then
        Me.tc_limits.SelectedTab = Me.tp_prize
        Me.tb_prize_report_warning_msg.Focus()
        ' NLS: Personalización de Cuenta
        NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _
                          Me.lbl_prize_report_warning_msg.Text), ENUM_MB_TYPE.MB_TYPE_WARNING)

        Return False
      End If

    End If

    Return True

  End Function

  '  PURPOSE: Ignore [ENTER] key when editing comments
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) Then
      If Me.tb_rchg_ident_msg.ContainsFocus _
        Or Me.tb_rchg_ident_warning_msg.ContainsFocus _
        Or Me.tb_rchg_report_msg.ContainsFocus _
        Or Me.tb_rchg_report_warning_msg.ContainsFocus _
        Or Me.tb_prize_ident_msg.ContainsFocus _
        Or Me.tb_prize_ident_warning_msg.ContainsFocus _
        Or Me.tb_prize_report_msg.ContainsFocus _
        Or Me.tb_prize_report_warning_msg.ContainsFocus Then

        Return True
      End If
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function

  '  PURPOSE: Set the screen data based on Database object
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _ml_config As CLS_MONEY_LAUNDERING_CONFIG

    _ml_config = DbReadObject


    Me.ef_minimum_wage.Text = _ml_config.BaseName
    Me.ef_aml_name.Value = _ml_config.Name
    Me.lbl_minwage_desc.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2400) & " " & _ml_config.BaseName
    m_base_name = _ml_config.BaseName

    'Maximum Limits
    Me.ef_rchg_maximum_limit.Value = _ml_config.RechargeMaximum.Limit
    Me.tb_rchg_maximum_msg.Text = _ml_config.RechargeMaximum.Message

    ' If Limit is defined
    If Not Me.ef_rchg_maximum_limit.Value > 0 Then
      Me.chk_rchg_maximum_limit.Checked = False
    End If

    Me.chk_active_aml.Checked = _ml_config.Enabled
    Me.chk_ident_not_allow_mb.Checked = _ml_config.RechargeIdentification.DontAllowMB
    Me.chk_report_not_allow_mb.Checked = _ml_config.RechargeReport.DontAllowMB

    Me.chk_rchg_report_limit_message.Checked = _ml_config.RechargeReport.ShowReportMessage
    Me.chk_prize_report_limit_message.Checked = _ml_config.PrizeReport.ShowReportMessage

    Me.ef_minimum_wage.Value = _ml_config.BaseAmount

    Me.ef_rchg_ident_limit.Value = _ml_config.RechargeIdentification.Limit
    Me.ef_rchg_ident_warning_limit.Value = _ml_config.RechargeIdentification.WarningLimit
    Me.tb_rchg_ident_msg.Text = _ml_config.RechargeIdentification.Message

    ' If Limit is defined
    If Not Me.ef_rchg_ident_warning_limit.Value > 0 Then
      Me.chk_rchg_ident_warning.Checked = False
    End If

    Me.tb_rchg_ident_warning_msg.Text = _ml_config.RechargeIdentification.WarningMessage
    Me.ef_rchg_report_limit.Value = _ml_config.RechargeReport.Limit
    Me.ef_rchg_report_warning_limit.Value = _ml_config.RechargeReport.WarningLimit
    Me.tb_rchg_report_msg.Text = _ml_config.RechargeReport.Message

    ' If Limit is defined
    If Not Me.ef_rchg_report_warning_limit.Value > 0 Then
      Me.chk_rchg_report_warning.Checked = False
    End If

    Me.tb_rchg_report_warning_msg.Text = _ml_config.RechargeReport.WarningMessage
    Me.ef_prize_ident_limit.Value = _ml_config.PrizeIdentification.Limit
    Me.ef_prize_ident_warning_limit.Value = _ml_config.PrizeIdentification.WarningLimit
    Me.tb_prize_ident_msg.Text = _ml_config.PrizeIdentification.Message

    'Maximum Limits
    Me.ef_prize_maximum_limit.Value = _ml_config.PrizeMaximum.Limit
    Me.tb_prize_maximum_msg.Text = _ml_config.PrizeMaximum.Message

    ' If Limit is defined
    If Not Me.ef_prize_maximum_limit.Value > 0 Then
      Me.chk_prize_maximum_limit.Checked = False
    End If

    ' If Limit is defined
    If Not Me.ef_prize_ident_warning_limit.Value > 0 Then
      Me.chk_prize_ident_warning.Checked = False
    End If

    Me.tb_prize_ident_warning_msg.Text = _ml_config.PrizeIdentification.WarningMessage
    Me.ef_prize_report_limit.Value = _ml_config.PrizeReport.Limit
    Me.ef_prize_report_warning_limit.Value = _ml_config.PrizeReport.WarningLimit
    Me.tb_prize_report_msg.Text = _ml_config.PrizeReport.Message

    ' If Limit is defined
    If Not Me.ef_prize_report_warning_limit.Value > 0 Then
      Me.chk_prize_report_warning.Checked = False
    End If

    Me.cmb_exceed_recharge.SelectedIndex = _ml_config.RechargeReport.PermissionToExeedLimit
    Me.cmb_exceed_prize.SelectedIndex = _ml_config.PrizeReport.PermissionToExeedLimit

    Me.tb_prize_report_warning_msg.Text = _ml_config.PrizeReport.WarningMessage

    Me.chk_has_beneficiary.Checked = _ml_config.HasBeneficiary
    Me.SetAbsoluteValues()

  End Sub

  '  PURPOSE: Get data in the form
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _ml_config As CLS_MONEY_LAUNDERING_CONFIG

    _ml_config = DbEditedObject

    _ml_config.Name = Me.ef_aml_name.Value
    _ml_config.Enabled = Me.chk_active_aml.Checked
    _ml_config.BaseAmount = IIf(Me.IsEmptyOrZero(Me.ef_minimum_wage.Value), 0, Me.ef_minimum_wage.Value)
    _ml_config.RechargeIdentification.Limit = IIf(Me.IsEmptyOrZero(Me.ef_rchg_ident_limit.Value), 0, Me.ef_rchg_ident_limit.Value)
    _ml_config.RechargeIdentification.Message = Me.tb_rchg_ident_msg.Text

    'Maximum limit
    If Me.chk_rchg_maximum_limit.Checked Then
      _ml_config.RechargeMaximum.Limit = IIf(Me.IsEmptyOrZero(Me.ef_rchg_maximum_limit.Value), 0, Me.ef_rchg_maximum_limit.Value)
      _ml_config.RechargeMaximum.Message = Me.tb_rchg_maximum_msg.Text
    Else
      _ml_config.RechargeMaximum.Limit = 0
    End If

    If Me.chk_rchg_ident_warning.Checked Then
      _ml_config.RechargeIdentification.WarningLimit = IIf(Me.IsEmptyOrZero(Me.ef_rchg_ident_warning_limit.Value), 0, Me.ef_rchg_ident_warning_limit.Value)
      _ml_config.RechargeIdentification.WarningMessage = Me.tb_rchg_ident_warning_msg.Text
    Else
      _ml_config.RechargeIdentification.WarningLimit = 0
    End If

    _ml_config.RechargeIdentification.DontAllowMB = Me.chk_ident_not_allow_mb.Checked
    _ml_config.RechargeReport.Limit = IIf(Me.IsEmptyOrZero(Me.ef_rchg_report_limit.TextValue), 0, Me.ef_rchg_report_limit.TextValue)
    _ml_config.RechargeReport.Message = Me.tb_rchg_report_msg.Text

    If Me.chk_rchg_report_warning.Checked Then
      _ml_config.RechargeReport.WarningLimit = IIf(Me.IsEmptyOrZero(Me.ef_rchg_report_warning_limit.Value), 0, Me.ef_rchg_report_warning_limit.Value)
      _ml_config.RechargeReport.WarningMessage = Me.tb_rchg_report_warning_msg.Text
    Else
      _ml_config.RechargeReport.WarningLimit = 0
    End If

    _ml_config.RechargeReport.DontAllowMB = Me.chk_report_not_allow_mb.Checked
    _ml_config.PrizeIdentification.Limit = IIf(Me.IsEmptyOrZero(Me.ef_prize_ident_limit.Value), 0, Me.ef_prize_ident_limit.Value)
    _ml_config.PrizeIdentification.Message = Me.tb_prize_ident_msg.Text

    'Maximum limit
    If Me.chk_prize_maximum_limit.Checked Then
      _ml_config.PrizeMaximum.Limit = IIf(Me.IsEmptyOrZero(Me.ef_prize_maximum_limit.Value), 0, Me.ef_prize_maximum_limit.Value)
      _ml_config.PrizeMaximum.Message = Me.tb_prize_maximum_msg.Text
    Else
      _ml_config.PrizeMaximum.Limit = 0
    End If


    _ml_config.RechargeReport.ShowReportMessage = Me.chk_rchg_report_limit_message.Checked
    _ml_config.PrizeReport.ShowReportMessage = Me.chk_prize_report_limit_message.Checked

    If Me.chk_prize_ident_warning.Checked Then
      _ml_config.PrizeIdentification.WarningLimit = IIf(Me.IsEmptyOrZero(Me.ef_prize_ident_warning_limit.Value), 0, Me.ef_prize_ident_warning_limit.Value)
      _ml_config.PrizeIdentification.WarningMessage = Me.tb_prize_ident_warning_msg.Text
    Else
      _ml_config.PrizeIdentification.WarningLimit = 0
    End If

    _ml_config.PrizeReport.Limit = IIf(Me.IsEmptyOrZero(Me.ef_prize_report_limit.Value), 0, Me.ef_prize_report_limit.Value)
    _ml_config.PrizeReport.Message = Me.tb_prize_report_msg.Text

    If Me.chk_prize_report_warning.Checked Then
      _ml_config.PrizeReport.WarningLimit = IIf(Me.IsEmptyOrZero(Me.ef_prize_report_warning_limit.Value), 0, Me.ef_prize_report_warning_limit.Value)
      _ml_config.PrizeReport.WarningMessage = Me.tb_prize_report_warning_msg.Text
    Else
      _ml_config.PrizeReport.WarningLimit = 0
    End If

    _ml_config.RechargeReport.PermissionToExeedLimit = Me.cmb_exceed_recharge.SelectedIndex
    _ml_config.PrizeReport.PermissionToExeedLimit = Me.cmb_exceed_prize.SelectedIndex

    _ml_config.HasBeneficiary = Me.chk_has_beneficiary.Checked

  End Sub

#End Region ' Overrides

#Region "User Controls Handlers"

  ' PURPOSE : Controls check state change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub chk_rchg_ident_warning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_rchg_ident_warning.CheckedChanged

    If chk_rchg_ident_warning.Checked = True Then
      Me.gb_rchg_ident_warning.Enabled = True
    Else
      Me.gb_rchg_ident_warning.Enabled = False
    End If

  End Sub

  ' PURPOSE : Controls check state change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub chk_rchg_report_warning_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_rchg_report_warning.CheckStateChanged

    If chk_rchg_report_warning.Checked = True Then
      Me.gb_rchg_report_warning.Enabled = True
    Else
      Me.gb_rchg_report_warning.Enabled = False
    End If

  End Sub

  ' PURPOSE : Controls check state change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub chk_prize_ident_warning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_prize_ident_warning.CheckedChanged

    If chk_prize_ident_warning.Checked = True Then
      Me.gb_prize_ident_warning.Enabled = True
    Else
      Me.gb_prize_ident_warning.Enabled = False
    End If

  End Sub

  ' PURPOSE : Controls check state change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub chk_prize_report_warning_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_prize_report_warning.CheckStateChanged

    If chk_prize_report_warning.Checked = True Then
      Me.gb_prize_report_warning.Enabled = True
    Else
      Me.gb_prize_report_warning.Enabled = False
    End If

  End Sub

  ' PURPOSE : Controls entry fields value change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ef_EntryFieldValueChanged() Handles ef_minimum_wage.EntryFieldValueChanged, ef_rchg_report_limit.EntryFieldValueChanged, ef_rchg_report_warning_limit.EntryFieldValueChanged, ef_rchg_ident_warning_limit.EntryFieldValueChanged, ef_prize_report_warning_limit.EntryFieldValueChanged, ef_prize_report_limit.EntryFieldValueChanged, ef_prize_ident_warning_limit.EntryFieldValueChanged, ef_prize_ident_limit.EntryFieldValueChanged, ef_prize_maximum_limit.EntryFieldValueChanged, ef_rchg_maximum_limit.EntryFieldValueChanged, ef_rchg_ident_limit.EntryFieldValueChanged
    Me.SetAbsoluteValues()
  End Sub


  ' PURPOSE : Controls list check state change
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub chk_rchg_maximum_limit_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_rchg_maximum_limit.CheckStateChanged
    If chk_rchg_maximum_limit.Checked = True Then
      Me.gb_rchg_maximum_limit.Enabled = True
    Else
      Me.gb_rchg_maximum_limit.Enabled = False
    End If
  End Sub

#End Region ' User Controls Handlers

#Region "Private Functions"

  ' PURPOSE : Converts a bolean value into "1" or "0"
  '
  '  PARAMS :
  '     - INPUT : 
  '             - BoolValue
  '     - OUTPUT :
  '
  ' RETURNS : The result of the conversion
  Private Function Get_String_from_Bool(ByVal BoolValue As Boolean) As String
    If BoolValue Then
      Return "1"
    Else
      Return "0"
    End If
  End Function

  ' PURPOSE : Sets absolute values fields text
  '
  '  PARAMS :
  '     - INPUT : 
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetAbsoluteValues()
    Dim _base_amount As Decimal
    Dim _recharge_limit1_value As Decimal
    Dim _recharge_warning_limit1_value As Decimal
    Dim _recharge_limit2_value As Decimal
    Dim _recharge_warning_limit2_value As Decimal
    Dim _recharge_limit3_value As Decimal
    Dim _prize_limit1_value As Decimal
    Dim _prize_warning_limit1_value As Decimal
    Dim _prize_limit2_value As Decimal
    Dim _prize_warning_limit2_value As Decimal
    Dim _prize_limit3_value As Decimal

    _base_amount = 0
    _recharge_limit1_value = 0
    _recharge_warning_limit1_value = 0
    _recharge_limit2_value = 0
    _recharge_warning_limit2_value = 0
    _recharge_limit3_value = 0
    _prize_limit1_value = 0
    _prize_warning_limit1_value = 0
    _prize_limit2_value = 0
    _prize_warning_limit2_value = 0
    _prize_limit3_value = 0

    If Me.ef_minimum_wage.TextValue.Trim() <> String.Empty Then
      _base_amount = Convert.ToDecimal(Me.ef_minimum_wage.Value.Trim())
    End If

    If Me.ef_rchg_ident_limit.TextValue.Trim <> String.Empty Then
      _recharge_limit1_value = Convert.ToDecimal(Me.ef_rchg_ident_limit.Value.Trim())
    End If

    If Me.ef_rchg_ident_warning_limit.TextValue.Trim <> String.Empty Then
      _recharge_warning_limit1_value = Convert.ToDecimal(Me.ef_rchg_ident_warning_limit.Value.Trim())
    End If

    If Me.ef_rchg_report_limit.TextValue.Trim <> String.Empty Then
      _recharge_limit2_value = Convert.ToDecimal(Me.ef_rchg_report_limit.Value.Trim())
    End If

    If Me.ef_rchg_report_warning_limit.TextValue.Trim <> String.Empty Then
      _recharge_warning_limit2_value = Convert.ToDecimal(Me.ef_rchg_report_warning_limit.Value.Trim())
    End If

    If Me.ef_rchg_maximum_limit.TextValue.Trim <> String.Empty Then
      _recharge_limit3_value = Convert.ToDecimal(Me.ef_rchg_maximum_limit.Value.Trim())
    End If

    If Me.ef_prize_ident_limit.TextValue.Trim <> String.Empty Then
      _prize_limit1_value = Convert.ToDecimal(Me.ef_prize_ident_limit.Value.Trim())
    End If

    If Me.ef_prize_ident_warning_limit.TextValue.Trim <> String.Empty Then
      _prize_warning_limit1_value = Convert.ToDecimal(Me.ef_prize_ident_warning_limit.Value.Trim())
    End If

    If Me.ef_prize_report_limit.TextValue.Trim <> String.Empty Then
      _prize_limit2_value = Convert.ToDecimal(Me.ef_prize_report_limit.Value.Trim())
    End If

    If Me.ef_prize_report_warning_limit.TextValue.Trim <> String.Empty Then
      _prize_warning_limit2_value = Convert.ToDecimal(Me.ef_prize_report_warning_limit.Value.Trim())
    End If

    If Me.ef_prize_maximum_limit.TextValue.Trim <> String.Empty Then
      _prize_limit3_value = Convert.ToDecimal(Me.ef_prize_maximum_limit.Value.Trim())
    End If

    Me.lbl_rchg_ident_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _recharge_limit1_value) & ")"
    Me.lbl_rchg_ident_warning_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _recharge_warning_limit1_value) & ")"
    Me.lbl_rchg_report_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _recharge_limit2_value) & ")"
    Me.lbl_rchg_report_warning_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _recharge_warning_limit2_value) & ")"
    Me.lbl_rchg_maximum_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _recharge_limit3_value) & ")"
    Me.lbl_prize_ident_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _prize_limit1_value) & ")"
    Me.lbl_prize_ident_warning_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _prize_warning_limit1_value) & ")"
    Me.lbl_prize_report_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _prize_limit2_value) & ")"
    Me.lbl_prize_report_warning_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _prize_warning_limit2_value) & ")"
    Me.lbl_prize_maximum_absolute.Value = m_base_name & " (" & GUI_FormatCurrency(_base_amount * _prize_limit3_value) & ")"

  End Sub

  ' PURPOSE : Determines if a numeric istring is empty or 0
  '
  '  PARAMS :
  '     - INPUT : 
  '             - Value
  '     - OUTPUT :
  '
  ' RETURNS : TRUE if is empty or zero, FALSE otherwise
  Private Function IsEmptyOrZero(ByVal Value As String) As Boolean

    If String.IsNullOrEmpty(Value) Then
      Return True
    ElseIf Value = 0 Then
      Return True
    End If

  End Function

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE : Overloads ShowEditItem function to pass the 'show mode'
  '
  '  PARAMS :
  '     - INPUT : 
  '             - RequiredFieldsMode
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    MyBase.ShowEditItem(0)

  End Sub

#End Region ' Public Functions


End Class