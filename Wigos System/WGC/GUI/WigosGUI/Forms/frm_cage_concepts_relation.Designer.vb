<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_concepts_relation
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_relations = New GUI_Controls.uc_grid
    Me.gb_options = New System.Windows.Forms.GroupBox
    Me.btn_reset = New GUI_Controls.uc_button
    Me.btn_process = New GUI_Controls.uc_button
    Me.ef_price_factor = New GUI_Controls.uc_entry_field
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.chk_only_national_currency = New System.Windows.Forms.CheckBox
    Me.cmb_type = New GUI_Controls.uc_combo
    Me.cmb_cashier_action = New GUI_Controls.uc_combo
    Me.cmb_source_target = New GUI_Controls.uc_combo
    Me.cmb_concepts = New GUI_Controls.uc_combo
    Me.panel_data.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_options)
    Me.panel_data.Controls.Add(Me.dg_relations)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(890, 380)
    '
    'dg_relations
    '
    Me.dg_relations.BackColor = System.Drawing.SystemColors.Control
    Me.dg_relations.CurrentCol = -1
    Me.dg_relations.CurrentRow = -1
    Me.dg_relations.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_relations.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_relations.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_relations.Location = New System.Drawing.Point(12, 138)
    Me.dg_relations.Name = "dg_relations"
    Me.dg_relations.PanelRightVisible = False
    Me.dg_relations.Redraw = True
    Me.dg_relations.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_relations.Size = New System.Drawing.Size(870, 234)
    Me.dg_relations.Sortable = False
    Me.dg_relations.SortAscending = True
    Me.dg_relations.SortByCol = 0
    Me.dg_relations.TabIndex = 1
    Me.dg_relations.ToolTipped = True
    Me.dg_relations.TopRow = -2
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.btn_reset)
    Me.gb_options.Controls.Add(Me.btn_process)
    Me.gb_options.Controls.Add(Me.ef_price_factor)
    Me.gb_options.Controls.Add(Me.chk_enabled)
    Me.gb_options.Controls.Add(Me.chk_only_national_currency)
    Me.gb_options.Controls.Add(Me.cmb_type)
    Me.gb_options.Controls.Add(Me.cmb_cashier_action)
    Me.gb_options.Controls.Add(Me.cmb_source_target)
    Me.gb_options.Controls.Add(Me.cmb_concepts)
    Me.gb_options.Location = New System.Drawing.Point(12, 3)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(870, 129)
    Me.gb_options.TabIndex = 0
    Me.gb_options.TabStop = False
    '
    'btn_reset
    '
    Me.btn_reset.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_reset.Location = New System.Drawing.Point(668, 58)
    Me.btn_reset.Name = "btn_reset"
    Me.btn_reset.Size = New System.Drawing.Size(63, 21)
    Me.btn_reset.TabIndex = 7
    Me.btn_reset.ToolTipped = False
    Me.btn_reset.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'btn_process
    '
    Me.btn_process.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_process.Location = New System.Drawing.Point(668, 24)
    Me.btn_process.Name = "btn_process"
    Me.btn_process.Size = New System.Drawing.Size(63, 21)
    Me.btn_process.TabIndex = 8
    Me.btn_process.ToolTipped = False
    Me.btn_process.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'ef_price_factor
    '
    Me.ef_price_factor.DoubleValue = 0
    Me.ef_price_factor.IntegerValue = 0
    Me.ef_price_factor.IsReadOnly = False
    Me.ef_price_factor.Location = New System.Drawing.Point(525, 91)
    Me.ef_price_factor.Name = "ef_price_factor"
    Me.ef_price_factor.PlaceHolder = Nothing
    Me.ef_price_factor.Size = New System.Drawing.Size(208, 24)
    Me.ef_price_factor.SufixText = "Sufix Text"
    Me.ef_price_factor.SufixTextVisible = True
    Me.ef_price_factor.TabIndex = 6
    Me.ef_price_factor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_price_factor.TextValue = ""
    Me.ef_price_factor.TextWidth = 90
    Me.ef_price_factor.Value = ""
    Me.ef_price_factor.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(389, 95)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 5
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_only_national_currency
    '
    Me.chk_only_national_currency.AutoSize = True
    Me.chk_only_national_currency.Location = New System.Drawing.Point(98, 95)
    Me.chk_only_national_currency.Name = "chk_only_national_currency"
    Me.chk_only_national_currency.Size = New System.Drawing.Size(158, 17)
    Me.chk_only_national_currency.TabIndex = 2
    Me.chk_only_national_currency.Text = "xOnlyNationalCurrency"
    Me.chk_only_national_currency.UseVisualStyleBackColor = True
    '
    'cmb_type
    '
    Me.cmb_type.AllowUnlistedValues = False
    Me.cmb_type.AutoCompleteMode = False
    Me.cmb_type.IsReadOnly = False
    Me.cmb_type.Location = New System.Drawing.Point(296, 21)
    Me.cmb_type.Name = "cmb_type"
    Me.cmb_type.SelectedIndex = -1
    Me.cmb_type.Size = New System.Drawing.Size(250, 24)
    Me.cmb_type.SufixText = "Sufix Text"
    Me.cmb_type.SufixTextVisible = True
    Me.cmb_type.TabIndex = 3
    Me.cmb_type.TextWidth = 90
    '
    'cmb_cashier_action
    '
    Me.cmb_cashier_action.AllowUnlistedValues = False
    Me.cmb_cashier_action.AutoCompleteMode = False
    Me.cmb_cashier_action.IsReadOnly = False
    Me.cmb_cashier_action.Location = New System.Drawing.Point(296, 55)
    Me.cmb_cashier_action.Name = "cmb_cashier_action"
    Me.cmb_cashier_action.SelectedIndex = -1
    Me.cmb_cashier_action.Size = New System.Drawing.Size(250, 24)
    Me.cmb_cashier_action.SufixText = "Sufix Text"
    Me.cmb_cashier_action.SufixTextVisible = True
    Me.cmb_cashier_action.TabIndex = 4
    Me.cmb_cashier_action.TextWidth = 90
    '
    'cmb_source_target
    '
    Me.cmb_source_target.AllowUnlistedValues = False
    Me.cmb_source_target.AutoCompleteMode = False
    Me.cmb_source_target.IsReadOnly = False
    Me.cmb_source_target.Location = New System.Drawing.Point(6, 20)
    Me.cmb_source_target.Name = "cmb_source_target"
    Me.cmb_source_target.SelectedIndex = -1
    Me.cmb_source_target.Size = New System.Drawing.Size(250, 24)
    Me.cmb_source_target.SufixText = "Sufix Text"
    Me.cmb_source_target.SufixTextVisible = True
    Me.cmb_source_target.TabIndex = 0
    Me.cmb_source_target.TextWidth = 90
    '
    'cmb_concepts
    '
    Me.cmb_concepts.AllowUnlistedValues = False
    Me.cmb_concepts.AutoCompleteMode = False
    Me.cmb_concepts.IsReadOnly = False
    Me.cmb_concepts.Location = New System.Drawing.Point(6, 55)
    Me.cmb_concepts.Name = "cmb_concepts"
    Me.cmb_concepts.SelectedIndex = -1
    Me.cmb_concepts.Size = New System.Drawing.Size(250, 24)
    Me.cmb_concepts.SufixText = "Sufix Text"
    Me.cmb_concepts.SufixTextVisible = True
    Me.cmb_concepts.TabIndex = 1
    Me.cmb_concepts.TextWidth = 90
    '
    'frm_cage_concepts_relation
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1009, 391)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_concepts_relation"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_concepts_relation"
    Me.panel_data.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_relations As GUI_Controls.uc_grid
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents btn_reset As GUI_Controls.uc_button
  Friend WithEvents btn_process As GUI_Controls.uc_button
  Friend WithEvents ef_price_factor As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_national_currency As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_cashier_action As GUI_Controls.uc_combo
  Friend WithEvents cmb_source_target As GUI_Controls.uc_combo
  Friend WithEvents cmb_concepts As GUI_Controls.uc_combo
End Class
