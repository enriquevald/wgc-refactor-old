<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mailing_parameters
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_credentials = New System.Windows.Forms.GroupBox
    Me.ef_smtp_password = New GUI_Controls.uc_entry_field
    Me.ef_smtp_email_from = New GUI_Controls.uc_entry_field
    Me.ef_smtp_domain = New GUI_Controls.uc_entry_field
    Me.ef_smtp_username = New GUI_Controls.uc_entry_field
    Me.chk_smtp_enabled = New System.Windows.Forms.CheckBox
    Me.chk_mailing_enabled = New System.Windows.Forms.CheckBox
    Me.gb_smtp_config = New System.Windows.Forms.GroupBox
    Me.cmb_smtp_secured = New GUI_Controls.uc_combo
    Me.ef_smtp_server = New GUI_Controls.uc_entry_field
    Me.ef_smtp_port = New GUI_Controls.uc_entry_field
    Me.gb_email_test = New System.Windows.Forms.GroupBox
    Me.btn_email_test = New GUI_Controls.uc_button
    Me.ef_email_test_to = New GUI_Controls.uc_entry_field
    Me.gb_authorized_server_list = New System.Windows.Forms.GroupBox
    Me.clb_authorized_server_list = New System.Windows.Forms.CheckedListBox
    Me.panel_data.SuspendLayout()
    Me.gb_credentials.SuspendLayout()
    Me.gb_smtp_config.SuspendLayout()
    Me.gb_email_test.SuspendLayout()
    Me.gb_authorized_server_list.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_authorized_server_list)
    Me.panel_data.Controls.Add(Me.gb_email_test)
    Me.panel_data.Controls.Add(Me.gb_credentials)
    Me.panel_data.Controls.Add(Me.chk_smtp_enabled)
    Me.panel_data.Controls.Add(Me.chk_mailing_enabled)
    Me.panel_data.Controls.Add(Me.gb_smtp_config)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(398, 536)
    '
    'gb_credentials
    '
    Me.gb_credentials.Controls.Add(Me.ef_smtp_password)
    Me.gb_credentials.Controls.Add(Me.ef_smtp_email_from)
    Me.gb_credentials.Controls.Add(Me.ef_smtp_domain)
    Me.gb_credentials.Controls.Add(Me.ef_smtp_username)
    Me.gb_credentials.Location = New System.Drawing.Point(17, 280)
    Me.gb_credentials.Name = "gb_credentials"
    Me.gb_credentials.Size = New System.Drawing.Size(368, 150)
    Me.gb_credentials.TabIndex = 4
    Me.gb_credentials.TabStop = False
    Me.gb_credentials.Text = "xCredentials"
    '
    'ef_smtp_password
    '
    Me.ef_smtp_password.DoubleValue = 0
    Me.ef_smtp_password.IntegerValue = 0
    Me.ef_smtp_password.IsReadOnly = False
    Me.ef_smtp_password.Location = New System.Drawing.Point(11, 51)
    Me.ef_smtp_password.Name = "ef_smtp_password"
    Me.ef_smtp_password.Size = New System.Drawing.Size(351, 24)
    Me.ef_smtp_password.SufixText = "Sufix Text"
    Me.ef_smtp_password.SufixTextVisible = True
    Me.ef_smtp_password.TabIndex = 1
    Me.ef_smtp_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smtp_password.TextValue = ""
    Me.ef_smtp_password.TextWidth = 110
    Me.ef_smtp_password.Value = ""
    '
    'ef_smtp_email_from
    '
    Me.ef_smtp_email_from.DoubleValue = 0
    Me.ef_smtp_email_from.IntegerValue = 0
    Me.ef_smtp_email_from.IsReadOnly = False
    Me.ef_smtp_email_from.Location = New System.Drawing.Point(11, 111)
    Me.ef_smtp_email_from.Name = "ef_smtp_email_from"
    Me.ef_smtp_email_from.Size = New System.Drawing.Size(351, 24)
    Me.ef_smtp_email_from.SufixText = "Sufix Text"
    Me.ef_smtp_email_from.SufixTextVisible = True
    Me.ef_smtp_email_from.TabIndex = 3
    Me.ef_smtp_email_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smtp_email_from.TextValue = ""
    Me.ef_smtp_email_from.TextWidth = 110
    Me.ef_smtp_email_from.Value = ""
    '
    'ef_smtp_domain
    '
    Me.ef_smtp_domain.DoubleValue = 0
    Me.ef_smtp_domain.IntegerValue = 0
    Me.ef_smtp_domain.IsReadOnly = False
    Me.ef_smtp_domain.Location = New System.Drawing.Point(11, 81)
    Me.ef_smtp_domain.Name = "ef_smtp_domain"
    Me.ef_smtp_domain.Size = New System.Drawing.Size(351, 24)
    Me.ef_smtp_domain.SufixText = "Sufix Text"
    Me.ef_smtp_domain.SufixTextVisible = True
    Me.ef_smtp_domain.TabIndex = 2
    Me.ef_smtp_domain.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smtp_domain.TextValue = ""
    Me.ef_smtp_domain.TextWidth = 110
    Me.ef_smtp_domain.Value = ""
    '
    'ef_smtp_username
    '
    Me.ef_smtp_username.DoubleValue = 0
    Me.ef_smtp_username.IntegerValue = 0
    Me.ef_smtp_username.IsReadOnly = False
    Me.ef_smtp_username.Location = New System.Drawing.Point(11, 20)
    Me.ef_smtp_username.Name = "ef_smtp_username"
    Me.ef_smtp_username.Size = New System.Drawing.Size(351, 24)
    Me.ef_smtp_username.SufixText = "Sufix Text"
    Me.ef_smtp_username.SufixTextVisible = True
    Me.ef_smtp_username.TabIndex = 0
    Me.ef_smtp_username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smtp_username.TextValue = ""
    Me.ef_smtp_username.TextWidth = 110
    Me.ef_smtp_username.Value = ""
    '
    'chk_smtp_enabled
    '
    Me.chk_smtp_enabled.AutoSize = True
    Me.chk_smtp_enabled.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_smtp_enabled.Location = New System.Drawing.Point(17, 42)
    Me.chk_smtp_enabled.Name = "chk_smtp_enabled"
    Me.chk_smtp_enabled.Size = New System.Drawing.Size(113, 17)
    Me.chk_smtp_enabled.TabIndex = 1
    Me.chk_smtp_enabled.Text = "xSMTP Enabled"
    Me.chk_smtp_enabled.UseVisualStyleBackColor = True
    '
    'chk_mailing_enabled
    '
    Me.chk_mailing_enabled.AutoSize = True
    Me.chk_mailing_enabled.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_mailing_enabled.Location = New System.Drawing.Point(17, 14)
    Me.chk_mailing_enabled.Name = "chk_mailing_enabled"
    Me.chk_mailing_enabled.Size = New System.Drawing.Size(121, 17)
    Me.chk_mailing_enabled.TabIndex = 0
    Me.chk_mailing_enabled.Text = "xMailing Enabled"
    Me.chk_mailing_enabled.UseVisualStyleBackColor = True
    '
    'gb_smtp_config
    '
    Me.gb_smtp_config.Controls.Add(Me.cmb_smtp_secured)
    Me.gb_smtp_config.Controls.Add(Me.ef_smtp_server)
    Me.gb_smtp_config.Controls.Add(Me.ef_smtp_port)
    Me.gb_smtp_config.Location = New System.Drawing.Point(17, 152)
    Me.gb_smtp_config.Name = "gb_smtp_config"
    Me.gb_smtp_config.Size = New System.Drawing.Size(368, 122)
    Me.gb_smtp_config.TabIndex = 3
    Me.gb_smtp_config.TabStop = False
    Me.gb_smtp_config.Text = "xSMTP Configuration"
    '
    'cmb_smtp_secured
    '
    Me.cmb_smtp_secured.IsReadOnly = False
    Me.cmb_smtp_secured.Location = New System.Drawing.Point(11, 81)
    Me.cmb_smtp_secured.Name = "cmb_smtp_secured"
    Me.cmb_smtp_secured.SelectedIndex = -1
    Me.cmb_smtp_secured.Size = New System.Drawing.Size(174, 24)
    Me.cmb_smtp_secured.SufixText = "Sufix Text"
    Me.cmb_smtp_secured.SufixTextVisible = True
    Me.cmb_smtp_secured.TabIndex = 2
    Me.cmb_smtp_secured.TextWidth = 110
    '
    'ef_smtp_server
    '
    Me.ef_smtp_server.DoubleValue = 0
    Me.ef_smtp_server.IntegerValue = 0
    Me.ef_smtp_server.IsReadOnly = False
    Me.ef_smtp_server.Location = New System.Drawing.Point(11, 19)
    Me.ef_smtp_server.Name = "ef_smtp_server"
    Me.ef_smtp_server.Size = New System.Drawing.Size(351, 24)
    Me.ef_smtp_server.SufixText = "Sufix Text"
    Me.ef_smtp_server.SufixTextVisible = True
    Me.ef_smtp_server.TabIndex = 0
    Me.ef_smtp_server.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smtp_server.TextValue = ""
    Me.ef_smtp_server.TextWidth = 110
    Me.ef_smtp_server.Value = ""
    '
    'ef_smtp_port
    '
    Me.ef_smtp_port.DoubleValue = 0
    Me.ef_smtp_port.IntegerValue = 0
    Me.ef_smtp_port.IsReadOnly = False
    Me.ef_smtp_port.Location = New System.Drawing.Point(11, 50)
    Me.ef_smtp_port.Name = "ef_smtp_port"
    Me.ef_smtp_port.Size = New System.Drawing.Size(174, 24)
    Me.ef_smtp_port.SufixText = "Sufix Text"
    Me.ef_smtp_port.SufixTextVisible = True
    Me.ef_smtp_port.TabIndex = 1
    Me.ef_smtp_port.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smtp_port.TextValue = ""
    Me.ef_smtp_port.TextWidth = 110
    Me.ef_smtp_port.Value = ""
    '
    'gb_email_test
    '
    Me.gb_email_test.Controls.Add(Me.btn_email_test)
    Me.gb_email_test.Controls.Add(Me.ef_email_test_to)
    Me.gb_email_test.Location = New System.Drawing.Point(17, 436)
    Me.gb_email_test.Name = "gb_email_test"
    Me.gb_email_test.Size = New System.Drawing.Size(368, 92)
    Me.gb_email_test.TabIndex = 5
    Me.gb_email_test.TabStop = False
    Me.gb_email_test.Text = "xEmail Test"
    '
    'btn_email_test
    '
    Me.btn_email_test.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_email_test.Location = New System.Drawing.Point(122, 49)
    Me.btn_email_test.Name = "btn_email_test"
    Me.btn_email_test.Size = New System.Drawing.Size(90, 30)
    Me.btn_email_test.TabIndex = 1
    Me.btn_email_test.ToolTipped = False
    Me.btn_email_test.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_email_test_to
    '
    Me.ef_email_test_to.DoubleValue = 0
    Me.ef_email_test_to.IntegerValue = 0
    Me.ef_email_test_to.IsReadOnly = False
    Me.ef_email_test_to.Location = New System.Drawing.Point(11, 19)
    Me.ef_email_test_to.Name = "ef_email_test_to"
    Me.ef_email_test_to.Size = New System.Drawing.Size(351, 24)
    Me.ef_email_test_to.SufixText = "Sufix Text"
    Me.ef_email_test_to.SufixTextVisible = True
    Me.ef_email_test_to.TabIndex = 0
    Me.ef_email_test_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_email_test_to.TextValue = ""
    Me.ef_email_test_to.TextWidth = 110
    Me.ef_email_test_to.Value = ""
    '
    'gb_authorized_server_list
    '
    Me.gb_authorized_server_list.Controls.Add(Me.clb_authorized_server_list)
    Me.gb_authorized_server_list.Location = New System.Drawing.Point(17, 65)
    Me.gb_authorized_server_list.Name = "gb_authorized_server_list"
    Me.gb_authorized_server_list.Size = New System.Drawing.Size(368, 81)
    Me.gb_authorized_server_list.TabIndex = 2
    Me.gb_authorized_server_list.TabStop = False
    Me.gb_authorized_server_list.Text = "xAuthorizedServerList"
    '
    'clb_authorized_server_list
    '
    Me.clb_authorized_server_list.BackColor = System.Drawing.SystemColors.Control
    Me.clb_authorized_server_list.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.clb_authorized_server_list.CheckOnClick = True
    Me.clb_authorized_server_list.FormattingEnabled = True
    Me.clb_authorized_server_list.Location = New System.Drawing.Point(38, 20)
    Me.clb_authorized_server_list.Name = "clb_authorized_server_list"
    Me.clb_authorized_server_list.Size = New System.Drawing.Size(294, 50)
    Me.clb_authorized_server_list.TabIndex = 6
    '
    'frm_mailing_parameters
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(501, 546)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_mailing_parameters"
    Me.Text = "frm_mailing_parameters"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_credentials.ResumeLayout(False)
    Me.gb_smtp_config.ResumeLayout(False)
    Me.gb_email_test.ResumeLayout(False)
    Me.gb_authorized_server_list.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_credentials As System.Windows.Forms.GroupBox
  Friend WithEvents ef_smtp_password As GUI_Controls.uc_entry_field
  Friend WithEvents ef_smtp_email_from As GUI_Controls.uc_entry_field
  Friend WithEvents ef_smtp_domain As GUI_Controls.uc_entry_field
  Friend WithEvents ef_smtp_username As GUI_Controls.uc_entry_field
  Friend WithEvents chk_smtp_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_mailing_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_smtp_config As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_smtp_secured As GUI_Controls.uc_combo
  Friend WithEvents ef_smtp_server As GUI_Controls.uc_entry_field
  Friend WithEvents ef_smtp_port As GUI_Controls.uc_entry_field
  Friend WithEvents gb_email_test As System.Windows.Forms.GroupBox
  Friend WithEvents btn_email_test As GUI_Controls.uc_button
  Friend WithEvents ef_email_test_to As GUI_Controls.uc_entry_field
  Friend WithEvents gb_authorized_server_list As System.Windows.Forms.GroupBox
  Friend WithEvents clb_authorized_server_list As System.Windows.Forms.CheckedListBox
End Class
