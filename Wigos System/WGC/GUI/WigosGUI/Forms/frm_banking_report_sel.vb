﻿'--------------------------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_banking_report_sel.vb
'
'   DESCRIPTION :  Report banking
'
'        AUTHOR :  Rodrigo Gonzalez Rodriguez
'
' CREATION DATE :  14-JUL-2016
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 27-JUL-2016  RGR    Initial version - Product Backlog Item 15207: TPV Televisa: GUI, banking report
' 06-OCT-2016  FAV    Fixed Bug 18280:Televisa: It has added 'Canceled' status for TPV transactions
' 20-OCT-2016  ATB    Bug 19314:Recarga con "PinPad" - Televisa: se transfieren los créditos a la cuenta antes de la confirmación del banco
' 24-OCT-2016  ETP    Fixed Bug 19314 Pin Pad recharge refactoring
' 02-NOV-2016  ETP    Fixed Bug 119731 Pin Pad status initialized not working properly.
'----------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common.TITO
Imports WSI.Common
Imports WSI.Common.PinPad
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class frm_banking_report_sel
  Inherits frm_base_sel

#Region " Constants"

#Region " Central Grid"

  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 22

  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_STATUS As Integer = 2
  Private Const GRID_COLUMN_CASHIER_NAME As Integer = 3
  Private Const GRID_COLUMN_USER As Integer = 4
  Private Const GRID_COLUMN_CONTROL_NUMBER As Integer = 5
  Private Const GRID_COLUMN_PINPAD_ID As Integer = 6
  Private Const GRID_COLUMN_ACCOUNT_NUMBER As Integer = 7
  Private Const GRID_COLUMN_ACCOUNT_NAME As Integer = 8
  Private Const GRID_COLUMN_BANK_NAME As Integer = 9
  Private Const GRID_COLUMN_CARD_NUMBER As Integer = 10
  Private Const GRID_COLUMN_CARD_TYPE As Integer = 11
  Private Const GRID_COLUMN_CARD_HOLDER As Integer = 12
  Private Const GRID_COLUMN_TRANSACTION_AMOUNT As Integer = 13
  Private Const GRID_COLUMN_COMISSION As Integer = 14
  Private Const GRID_COLUMN_TOTAL_AMOUNT As Integer = 15

  Private Const GRID_COLUMN_MESSAGE_ERROR As Integer = 16
  Private Const GRID_COLUMN_REFERENCE As Integer = 17
  Private Const GRID_COLUMN_OPERATION_NUMBER As Integer = 18
  Private Const GRID_COLUMN_MERCHANT As Integer = 19
  Private Const GRID_COLUMN_AUTHORIZATION_CODE As Integer = 20
  Private Const GRID_COLUMN_DELIVERED As Integer = 21


  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 2300
  Private Const GRID_WIDTH_CONTROL_NUMBER As Integer = 3300
  Private Const GRID_WIDTH_USER As Integer = 2300
  Private Const GRID_WIDTH_ACCOUNT_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_ACCOUNT_NAME As Integer = 2300
  Private Const GRID_WIDTH_BANK_NAME As Integer = 2300
  Private Const GRID_WIDTH_CARD_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_CARD_TYPE As Integer = 2300
  Private Const GRID_WIDTH_CARD_HOLDER As Integer = 2300
  Private Const GRID_WIDTH_TRANSACTION_AMOUNT As Integer = 2300
  Private Const GRID_WIDTH_COMISSION As Integer = 2300
  Private Const GRID_WIDTH_TOTAL_AMOUNT As Integer = 2300
  Private Const GRID_WIDTH_STATUS As Integer = 2300
  Private Const GRID_WIDTH_MESSAGE_ERROR As Integer = 4600
  Private Const GRID_WIDTH_REFERENCE As Integer = 2300
  Private Const GRID_WIDTH_OPERATION_NUMBER As Integer = 2300
  Private Const GRID_WIDTH_MERCHANT As Integer = 2300
  Private Const GRID_WIDTH_AUTHORIZATION_CODE As Integer = 2300
  Private Const GRID_WIDTH_DELIVERED As Integer = 0
  Private Const GRID_WIDTH_CASHIER_NAME As Integer = 2300
  Private Const GRID_WIDTH_PINPAD_ID As Integer = 2300

  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_CONTROL_NUMBER As Integer = 1
  Private Const SQL_COLUMN_USER As Integer = 2
  Private Const SQL_COLUMN_ACCOUNT_NUMBER As Integer = 3
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 4
  Private Const SQL_COLUMN_BANK_NAME As Integer = 5
  Private Const SQL_COLUMN_CARD_NUMBER As Integer = 6
  Private Const SQL_COLUMN_CARD_TYPE As Integer = 7
  Private Const SQL_COLUMN_CARD_HOLDER As Integer = 8
  Private Const SQL_COLUMN_TRANSACTION_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_COMISSION As Integer = 10
  Private Const SQL_COLUMN_TOTAL_AMOUNT As Integer = 11
  Private Const SQL_COLUMN_STATUS As Integer = 12
  Private Const SQL_COLUMN_MESSAGE_ERROR As Integer = 13
  Private Const SQL_COLUMN_REFERENCE As Integer = 14
  Private Const SQL_COLUMN_OPERATION_NUMBER As Integer = 15
  Private Const SQL_COLUMN_MERCHANT As Integer = 16
  Private Const SQL_COLUMN_AUTHORIZATION_CODE As Integer = 17
  Private Const SQL_COLUMN_DELIVERED As Integer = 18
  Private Const SQL_COLUMN_IS_NATIONAL As Integer = 19
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 20
  Private Const SQL_COLUMN_PINPAD_ID As Integer = 21

#End Region

#End Region

#Region " Members"
  Private m_status_dictionary As IDictionary(Of Integer, String)
  Private m_card_types_dictionary As IDictionary(Of Integer, String)
  Private m_is_national_dictionary As IDictionary(Of Integer, String)

  Private m_from_date As Nullable(Of Date)
  Private m_to_date As Date
  Private m_select_status As IDictionary(Of Integer, String)
  Private m_select_card_types As IDictionary(Of Integer, String)
  Private m_select_is_national As IDictionary(Of Integer, String)
#End Region

#Region " Constructor"

  Public Sub New()

    InitializeComponent()

    Me.m_status_dictionary = New Dictionary(Of Integer, String)
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.NO_RESPONSE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4822)) 'Error
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.APPROVED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7427)) 'Approved
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.DECLINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6890)) 'Denied
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.REJECTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7428)) 'Rejected
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.CANCELED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1331)) 'Canceled
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.INITIALIZED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5448)) 'Initialized
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.PENDING_WIGOS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7683)) 'Pending Wigos
    Me.m_status_dictionary.Add(PinPadTransactions.STATUS.ERROR, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4658)) 'Error


    Me.m_card_types_dictionary = New Dictionary(Of Integer, String)
    Me.m_card_types_dictionary.Add(CARD_TYPE.NONE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7434)) 'Debit
    Me.m_card_types_dictionary.Add(CARD_TYPE.DEBIT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7434)) 'Debit
    Me.m_card_types_dictionary.Add(CARD_TYPE.CREDIT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7435)) 'Credit

    Me.m_is_national_dictionary = New Dictionary(Of Integer, String)
    Me.m_is_national_dictionary.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7436)) 'National
    Me.m_is_national_dictionary.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7437)) 'International

    Me.m_select_status = New Dictionary(Of Integer, String)
    Me.m_select_card_types = New Dictionary(Of Integer, String)
    Me.m_select_is_national = New Dictionary(Of Integer, String)

  End Sub

#End Region

#Region " Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BANKING_REPORT
    Call MyBase.GUI_SetFormId()

  End Sub

  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7408)
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1112)
    Me.chk_approved.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7427)
    Me.chk_denied.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7428)
    Me.chk_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4658)
    Me.chk_canceled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1331)
    Me.chk_Initialized.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5448)
    Me.chk_PendingWigos.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7683)
    Me.chk_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4658)
    Me.chk_Declined.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6890)
    Me.chk_NoResponse.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4822)


    Me.gb_card_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7433)
    Me.chk_debit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7434)
    Me.chk_credit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7435)
    Me.chk_national.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7436)
    Me.chk_linternational.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7437)

    Call Me.GUI_StyleSheet()
    Call Me.SetDefaultValues()

  End Sub

  Protected Overrides Sub GUI_FilterReset()

    Me.SetDefaultValues()

  End Sub

  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()
        Return False
      End If
    End If

    Return True

  End Function

  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _dateNow As Date

    _dateNow = WSI.Common.Misc.TodayOpening()
    Me.m_select_status.Clear()
    Me.m_select_card_types.Clear()
    Me.m_select_is_national.Clear()

    If Not Me.chk_NoResponse.Checked _
      AndAlso Not Me.chk_approved.Checked _
      AndAlso Not Me.chk_Declined.Checked _
      AndAlso Not Me.chk_denied.Checked _
      AndAlso Not Me.chk_canceled.Checked _
      AndAlso Not Me.chk_PendingWigos.Checked _
      AndAlso Not Me.chk_error.Checked _
      AndAlso Not Me.chk_Initialized.Checked Then

      For Each _item As KeyValuePair(Of Integer, String) In Me.m_status_dictionary
        Me.m_select_status.Add(_item)
      Next

    Else

      If Me.chk_approved.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.APPROVED, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.APPROVED))
      End If

      If Me.chk_denied.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.REJECTED, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.REJECTED))
      End If

      If Me.chk_Declined.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.DECLINED, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.DECLINED))
      End If

      If Me.chk_error.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.ERROR, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.ERROR))
      End If

      If Me.chk_canceled.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.CANCELED, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.CANCELED))
      End If

      If Me.chk_Initialized.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.INITIALIZED, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.INITIALIZED))
      End If

      If Me.chk_PendingWigos.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.PENDING_WIGOS, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.PENDING_WIGOS))
      End If

      If Me.chk_NoResponse.Checked Then
        Me.m_select_status.Add(PinPadTransactions.STATUS.NO_RESPONSE, Me.m_status_dictionary.Item(PinPadTransactions.STATUS.NO_RESPONSE))
      End If



    End If

    If Not Me.chk_debit.Checked AndAlso Not Me.chk_credit.Checked Then
      For Each _item As KeyValuePair(Of Integer, String) In Me.m_card_types_dictionary
        Me.m_select_card_types.Add(_item)
      Next
    Else
      If Me.chk_debit.Checked Then
        Me.m_select_card_types.Add(CARD_TYPE.NONE, Me.m_card_types_dictionary.Item(CARD_TYPE.NONE))
        Me.m_select_card_types.Add(CARD_TYPE.DEBIT, Me.m_card_types_dictionary.Item(CARD_TYPE.DEBIT))
      End If

      If Me.chk_credit.Checked Then
        Me.m_select_card_types.Add(CARD_TYPE.CREDIT, Me.m_card_types_dictionary.Item(CARD_TYPE.CREDIT))
      End If
    End If

    If Not Me.chk_national.Checked AndAlso Not Me.chk_linternational.Checked Then
      For Each _item As KeyValuePair(Of Integer, String) In Me.m_is_national_dictionary
        Me.m_select_is_national.Add(_item)
      Next
    Else
      If Me.chk_national.Checked Then
        Me.m_select_is_national.Add(1, Me.m_is_national_dictionary.Item(1))
      End If

      If Me.chk_linternational.Checked Then
        Me.m_select_is_national.Add(0, Me.m_is_national_dictionary.Item(0))
      End If
    End If

    If Me.dtp_from.Checked Then
      Me.m_from_date = Me.dtp_from.Value
    Else
      Me.m_from_date = Nothing
    End If

    If Me.dtp_to.Checked Then
      Me.m_to_date = Me.dtp_to.Value
    Else
      Me.m_to_date = _dateNow.AddDays(1)
    End If

  End Sub

  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _status As StringBuilder
    Dim _cartTypes As StringBuilder
    Dim _isNational As StringBuilder

    _status = Me.GetStringFilter(Me.m_select_status)
    _cartTypes = Me.GetStringFilter(Me.m_select_card_types)
    _isNational = Me.GetStringFilter(Me.m_select_is_national)

    _sql_command = New SqlCommand()
    _sql_command.CommandText = "GetReportBanking"
    _sql_command.CommandType = CommandType.StoredProcedure
    _sql_command.Parameters.Add("@pFrom", SqlDbType.DateTime).Value = IIf(IsNothing(Me.m_from_date), DBNull.Value, Me.m_from_date)
    _sql_command.Parameters.Add("@pTo", SqlDbType.DateTime).Value = Me.m_to_date
    _sql_command.Parameters.Add("@pStatus", SqlDbType.NVarChar).Value = _status.ToString()
    _sql_command.Parameters.Add("@pCardType", SqlDbType.NVarChar).Value = _cartTypes.ToString()
    _sql_command.Parameters.Add("@pNational", SqlDbType.NVarChar).Value = _isNational.ToString()

    Return _sql_command
  End Function

  Public Overrides Function GUI_SetupRow(RowIndex As Integer, DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _str_merchant As String

    With MyBase.Grid

      If Not DbRow.IsNull(SQL_COLUMN_DATE) Then
        .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CONTROL_NUMBER) Then
        .Cell(RowIndex, GRID_COLUMN_CONTROL_NUMBER).Value = DbRow.Value(SQL_COLUMN_CONTROL_NUMBER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_USER) Then
        .Cell(RowIndex, GRID_COLUMN_USER).Value = DbRow.Value(SQL_COLUMN_USER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NUMBER) Then
        .Cell(RowIndex, GRID_COLUMN_ACCOUNT_NUMBER).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NUMBER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
        .Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_BANK_NAME) Then
        .Cell(RowIndex, GRID_COLUMN_BANK_NAME).Value = DbRow.Value(SQL_COLUMN_BANK_NAME)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CARD_NUMBER) Then
        .Cell(RowIndex, GRID_COLUMN_CARD_NUMBER).Value = DbRow.Value(SQL_COLUMN_CARD_NUMBER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CARD_TYPE) AndAlso Not DbRow.IsNull(SQL_COLUMN_IS_NATIONAL) Then
        .Cell(RowIndex, GRID_COLUMN_CARD_TYPE).Value = Me.GetCardTypeString(DbRow.Value(SQL_COLUMN_CARD_TYPE), DbRow.Value(SQL_COLUMN_IS_NATIONAL))
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CARD_HOLDER) Then
        .Cell(RowIndex, GRID_COLUMN_CARD_HOLDER).Value = DbRow.Value(SQL_COLUMN_CARD_HOLDER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_TRANSACTION_AMOUNT) Then
        .Cell(RowIndex, GRID_COLUMN_TRANSACTION_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TRANSACTION_AMOUNT))
      End If

      If Not DbRow.IsNull(SQL_COLUMN_COMISSION) Then
        .Cell(RowIndex, GRID_COLUMN_COMISSION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMISSION))
      End If

      If Not DbRow.IsNull(SQL_COLUMN_TOTAL_AMOUNT) Then
        .Cell(RowIndex, GRID_COLUMN_TOTAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT))
      End If

      If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
        If Me.m_status_dictionary.ContainsKey(DbRow.Value(SQL_COLUMN_STATUS)) Then
          .Cell(RowIndex, GRID_COLUMN_STATUS).Value = Me.m_status_dictionary.Item(DbRow.Value(SQL_COLUMN_STATUS))
        Else
          .Cell(RowIndex, GRID_COLUMN_STATUS).Value = DbRow.Value(SQL_COLUMN_STATUS)
        End If
      End If

      If Not DbRow.IsNull(SQL_COLUMN_MESSAGE_ERROR) Then
        .Cell(RowIndex, GRID_COLUMN_MESSAGE_ERROR).Value = DbRow.Value(SQL_COLUMN_MESSAGE_ERROR)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_REFERENCE) Then
        .Cell(RowIndex, GRID_COLUMN_REFERENCE).Value = DbRow.Value(SQL_COLUMN_REFERENCE)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_OPERATION_NUMBER) Then
        .Cell(RowIndex, GRID_COLUMN_OPERATION_NUMBER).Value = DbRow.Value(SQL_COLUMN_OPERATION_NUMBER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_MERCHANT) Then
        _str_merchant = DbRow.Value(SQL_COLUMN_MERCHANT)
        If _str_merchant <> "0" Then
          .Cell(RowIndex, GRID_COLUMN_MERCHANT).Value = _str_merchant
        End If
      End If

      If Not DbRow.IsNull(SQL_COLUMN_AUTHORIZATION_CODE) Then
        .Cell(RowIndex, GRID_COLUMN_AUTHORIZATION_CODE).Value = DbRow.Value(SQL_COLUMN_AUTHORIZATION_CODE)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_DELIVERED) Then
        If DbRow.Value(SQL_COLUMN_DELIVERED) Then
          .Cell(RowIndex, GRID_COLUMN_DELIVERED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
        Else
          .Cell(RowIndex, GRID_COLUMN_DELIVERED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
        End If
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CASHIER_NAME) Then
        .Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CASHIER_NAME)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_PINPAD_ID) Then
        .Cell(RowIndex, GRID_COLUMN_PINPAD_ID).Value = DbRow.Value(SQL_COLUMN_PINPAD_ID)
      End If

    End With

    Return True
  End Function

  Protected Overrides Sub GUI_ReportParams(ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional FirstColIndex As Integer = 0)
    MyBase.GUI_ReportParams(ExcelData, FirstColIndex)

    ExcelData.SetColumnFormat(GRID_COLUMN_ACCOUNT_NUMBER - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
    ExcelData.SetColumnFormat(GRID_COLUMN_REFERENCE - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
    ExcelData.SetColumnFormat(GRID_COLUMN_OPERATION_NUMBER - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
    ExcelData.SetColumnFormat(GRID_COLUMN_MERCHANT - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
    ExcelData.SetColumnFormat(GRID_COLUMN_AUTHORIZATION_CODE - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)

  End Sub

  Protected Overrides Sub GUI_ReportFilter(PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _status As StringBuilder
    'for the time being disabled
    'Dim _cardType As StringBuilder
    'Dim _nationalType As StringBuilder

    If Me.CountLabels(Me.m_select_status) >= 3 Then
      _status = New StringBuilder()
      _status.Append(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))
    Else
      _status = Me.GetStringFilterLabels(Me.m_select_status)
    End If

    'for the time being disabled
    '_cardType = Me.GetStringFilterLabels(Me.m_select_card_types)
    '_nationalType = Me.GetStringFilterLabels(Me.m_select_is_national)

    MyBase.GUI_ReportFilter(PrintData)

    If IsNothing(Me.m_from_date) Then
      Call PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), String.Empty)
    Else
      Call PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(Me.m_from_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    End If

    Call PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(Me.m_to_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    Call PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1112), _status.ToString())

    'for the time being disabled
    'Call PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7433), String.Format("{0}, {1}", _cardType.ToString(), _nationalType.ToString()))

  End Sub

#End Region

#Region " Private Functions"

  Private Sub GUI_StyleSheet()

    With MyBase.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Column(GRID_COLUMN_SELECT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3353)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CONTROL_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7412)
      .Column(GRID_COLUMN_CONTROL_NUMBER).Width = GRID_WIDTH_CONTROL_NUMBER
      .Column(GRID_COLUMN_CONTROL_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
      .Column(GRID_COLUMN_USER).Width = GRID_WIDTH_USER
      .Column(GRID_COLUMN_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2749)
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Width = GRID_WIDTH_ACCOUNT_NUMBER
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7413)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = GRID_WIDTH_ACCOUNT_NAME
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_BANK_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554)
      .Column(GRID_COLUMN_BANK_NAME).Width = GRID_WIDTH_BANK_NAME
      .Column(GRID_COLUMN_BANK_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_CARD_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7414)
      .Column(GRID_COLUMN_CARD_NUMBER).Width = GRID_WIDTH_CARD_NUMBER
      .Column(GRID_COLUMN_CARD_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CARD_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7415)
      .Column(GRID_COLUMN_CARD_TYPE).Width = GRID_WIDTH_CARD_TYPE
      .Column(GRID_COLUMN_CARD_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CARD_HOLDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7416)
      .Column(GRID_COLUMN_CARD_HOLDER).Width = GRID_WIDTH_CARD_HOLDER
      .Column(GRID_COLUMN_CARD_HOLDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_TRANSACTION_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7417)
      .Column(GRID_COLUMN_TRANSACTION_AMOUNT).Width = GRID_WIDTH_TRANSACTION_AMOUNT
      .Column(GRID_COLUMN_TRANSACTION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_COMISSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7418)
      .Column(GRID_COLUMN_COMISSION).Width = GRID_WIDTH_COMISSION
      .Column(GRID_COLUMN_COMISSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7419)
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Width = GRID_WIDTH_TOTAL_AMOUNT
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1752)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_MESSAGE_ERROR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7420)
      .Column(GRID_COLUMN_MESSAGE_ERROR).Width = GRID_WIDTH_MESSAGE_ERROR
      .Column(GRID_COLUMN_MESSAGE_ERROR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_REFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1588)
      .Column(GRID_COLUMN_REFERENCE).Width = GRID_WIDTH_REFERENCE
      .Column(GRID_COLUMN_REFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_OPERATION_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7421)
      .Column(GRID_COLUMN_OPERATION_NUMBER).Width = GRID_WIDTH_OPERATION_NUMBER
      .Column(GRID_COLUMN_OPERATION_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_MERCHANT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7422)
      .Column(GRID_COLUMN_MERCHANT).Width = GRID_WIDTH_MERCHANT
      .Column(GRID_COLUMN_MERCHANT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7423)
      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Width = GRID_WIDTH_AUTHORIZATION_CODE
      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_DELIVERED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(436)
      .Column(GRID_COLUMN_DELIVERED).Width = GRID_WIDTH_DELIVERED
      .Column(GRID_COLUMN_DELIVERED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CASHIER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_CASHIER_NAME).Width = GRID_WIDTH_CASHIER_NAME
      .Column(GRID_COLUMN_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_PINPAD_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7685)
      .Column(GRID_COLUMN_PINPAD_ID).Width = GRID_WIDTH_PINPAD_ID
      .Column(GRID_COLUMN_PINPAD_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  Private Sub SetDefaultValues()

    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.chk_approved.Checked = False
    Me.chk_denied.Checked = False
    Me.chk_error.Checked = False
    Me.chk_canceled.Checked = False
    Me.chk_Initialized.Checked = False
    Me.chk_PendingWigos.Checked = False
    Me.chk_Declined.Checked = False
    Me.chk_NoResponse.Checked = False

    Me.chk_debit.Checked = False
    Me.chk_credit.Checked = False
    Me.chk_national.Checked = False
    Me.chk_linternational.Checked = False

  End Sub

  Private Function GetStringFilter(ByVal dictionary As Dictionary(Of Integer, String)) As StringBuilder
    Dim _filter As StringBuilder

    _filter = New StringBuilder()

    For Each _item As KeyValuePair(Of Integer, String) In dictionary
      If _filter.Length = 0 Then
        _filter.Append(_item.Key)
      Else
        _filter.Append(String.Format(",{0}", _item.Key))
      End If
    Next

    Return _filter
  End Function

  Private Function GetStringFilterLabels(ByVal dictionary As Dictionary(Of Integer, String)) As StringBuilder
    Dim _filter As StringBuilder

    _filter = New StringBuilder()

    For Each _item As KeyValuePair(Of Integer, String) In dictionary
      If _filter.Length = 0 Then
        _filter.Append(_item.Value)
      Else
        If Not _filter.ToString.Contains(_item.Value) Then
          _filter.Append(String.Format(", {0}", _item.Value))
        End If
      End If
    Next

    Return _filter
  End Function

  Private Function GetCardTypeString(ByVal cardType As CARD_TYPE, ByVal nationalType As Integer) As String
    Dim _label As String

    _label = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7446)

    'for the time being disabled
    'Select Case cardType
    '  Case CARD_TYPE.CREDIT
    '    If nationalType = 1 Then
    '      _label = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7430)
    '    Else
    '      _label = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7432)
    '    End If
    '  Case CARD_TYPE.DEBIT, CARD_TYPE.NONE
    '    If nationalType = 1 Then
    '      _label = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7429)
    '    Else
    '      _label = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7431)
    '    End If
    'End Select

    Return _label
  End Function

  Private Function CountLabels(ByVal dictionary As Dictionary(Of Integer, String)) As Integer
    Dim _list As IList(Of String)

    _list = New List(Of String)
    For Each _item As KeyValuePair(Of Integer, String) In dictionary
      If Not _list.Contains(_item.Value) Then
        _list.Add(_item.Value)
      End If
    Next

    Return _list.Count
  End Function

#End Region

#Region " Public Functions"

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    MyBase.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    MyBase.MdiParent = MdiParent
    MyBase.Display(False)
  End Sub

#End Region

End Class