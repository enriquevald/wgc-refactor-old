﻿'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_report
' DESCRIPTION:   This screen allows to view junket details between:
'                           - two dates 
'                           - details of junket Working Date
'                                        junket Customers
'                                        junket Representative
'                           - for all Junket Representatives or one
'                           - for all Junkets or one
'                           - for all Customers or one
' AUTHOR:        Mark Stansfield
' CREATION DATE: 26-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-APR-2017  MS     Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls

Public Class frm_junkets_report
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

    If m_junket Is Nothing Then
      m_junket = New CLASS_JUNKETS
    End If

  End Sub 'New

#End Region

#Region " Enums "

  Protected Enum ENUM_REPORT_TYPE
    WORKINGDAY = 0
    CUSTOMERS = 1
    REPRESENTATIVES = 2
  End Enum

#End Region ' Enums

#Region " Constants "

  ''' <summary>
  ''' Grid Constants
  ''' </summary>
  ''' <remarks>As the form has 3 reports these values can change</remarks>
  Private Const MODULE_NAME As String = "FRM_JUNKETS_REPORT"

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COLUMN_JUNKET_CODE As Integer = 1
  Private GRID_COLUMN_JUNKET_NAME As Integer = 2
  Private GRID_COLUMN_JUNKET_START As Integer = 3
  Private GRID_COLUMN_JUNKET_END As Integer = 4
  Private GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE As Integer = 5
  Private GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME As Integer = 6

  Private GRID_COLUMN_WORKING_DAY As Integer = 7 ' Overidden in specific report stylesheet
  Private GRID_COLUMN_CUSTOMER As Integer = 7 ' Overidden in specific report stylesheet
  Private GRID_COLUMN_CUSTOMER_NAME As Integer = 7 ' Overidden in specific report stylesheet

  Private GRID_COLUMN_COMMISION_CUSTOMER As Integer = 7
  Private GRID_COLUMN_COMMISION_VISIT As Integer = 8
  Private GRID_COLUMN_COMMISION_NEW_CUSTOMER As Integer = 9
  Private GRID_COLUMN_COMMISION_POINTS As Integer = 10
  Private GRID_COLUMN_COMMISION_COIN_IN As Integer = 11
  Private GRID_COLUMN_COMMISION_BUY_IN As Integer = 12
  Private GRID_COLUMN_COMMISION_NETWIN As Integer = 13
  Private GRID_COLUMN_COMMISION_TOTAL As Integer = 14

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private GRID_COLUMNS As Integer = 15

  Private GRID_WIDTH_INDEX As Integer = 200
  Private GRID_WIDTH_DEFAULT As Integer = 2000
  Private GRID_WIDTH_NAMES As Integer = 3000
  Private GRID_WIDTH_DATE As Integer = 1300

  ''' <summary>
  ''' SQL Columns
  ''' </summary>
  ''' <remarks>As the form has 3 reports these values can change</remarks>
  Private SQL_COLUMN_JUNKET_CODE As Integer = 1
  Private SQL_COLUMN_JUNKET_NAME As Integer = 2
  Private SQL_COLUMN_JUNKET_START As Integer = 3
  Private SQL_COLUMN_JUNKET_END As Integer = 4
  Private SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE As Integer = 5
  Private SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME As Integer = 6

  Private SQL_COLUMN_WORKING_DAY As Integer = 7
  Private SQL_COLUMN_CUSTOMER As Integer = 7
  Private SQL_COLUMN_CUSTOMER_NAME As Integer = 7


  Private SQL_COLUMN_COMMISION_CUSTOMER As Integer = 7
  Private SQL_COLUMN_COMMISION_VISIT As Integer = 8
  Private SQL_COLUMN_COMMISION_NEW_CUSTOMER As Integer = 9
  Private SQL_COLUMN_COMMISION_POINTS As Integer = 10
  Private SQL_COLUMN_COMMISION_COIN_IN As Integer = 11
  Private SQL_COLUMN_COMMISION_BUY_IN As Integer = 12
  Private SQL_COLUMN_COMMISION_NETWIN As Integer = 13
  Private SQL_COLUMN_COMMISION_TOTAL As Integer = 14

  ' Filter grid
  Private Const GRID_FILTER_COLUMN_ID As Integer = 0
  Private Const GRID_FILTER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_FILTER_COLUMN_DESC As Integer = 2

  Private Const GRID_FILTER_HEADER = 0
  Private Const GRID_FILTER_DATA = 1

  Private Const GRID_FILTER_COLUMNS As Integer = 3
  Private Const GRID_FILTER_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Members "

  Private m_is_tito_mode As Boolean
  Private m_report_type As ENUM_REPORT_TYPE
  Private m_junket As CLASS_JUNKETS
  Private m_str_junket_name As String = String.Empty
  Private m_filter_representatives As String
  Private m_filter_junkets As String
  Private m_subtotal_id As String
  Private m_subtotal_Commission_Customer As Decimal
  Private m_subtotal_Commission_Visit As Decimal
  Private m_subtotal_Commission_NewCustomer As Decimal
  Private m_subtotal_Commission_Points As Decimal
  Private m_subtotal_Commission_CoinIn As Decimal
  Private m_subtotal_Commission_BuyIn As Decimal
  Private m_subtotal_Commission_NetWin As Decimal
  Private m_subtotal_Commission_Totals As Decimal
  Private m_total_Commission_Customer As Decimal
  Private m_total_Commission_Visit As Decimal
  Private m_total_Commission_NewCustomer As Decimal
  Private m_total_Commission_Points As Decimal
  Private m_total_Commission_CoinIn As Decimal
  Private m_total_Commission_BuyIn As Decimal
  Private m_total_Commission_NetWin As Decimal
  Private m_total_Commission_Totals As Decimal
  Private m_filter_account As String
  Private m_filter_card As String
  Private m_filter_holder As String
  Private m_filter_date_from As String
  Private m_filter_date_to As String
  Private m_filter_vip As String
  Private m_filter_report_type As String
  Private m_filter_exclude_without_activity As String

#End Region ' Members

#Region " Overrides "

  ''' <summary>
  '''    GUI_SetFormId
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKET_REPORT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' GUI_InitControls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set Form Caption
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8268)

    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode()

    InitControlResourses()

    ' Style Sheets
    Call GUI_StyleSheetFilter(dg_filter_representatives)
    Call GUI_StyleSheetFilter(dg_filter_junkets)
    Call GUI_StyleSheet()

    ' Set Default Filter Values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' GUI_SetInitialFocus
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub 'GUI_SetInitialFocus

  ''' <summary>
  ''' GUI_ButtonClick
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    If ButtonId = ENUM_BUTTON.BUTTON_FILTER_APPLY Then
      Call GUI_StyleSheet()
    End If

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' GUI_FilterCheck
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    'Dates
    If (Me.dtp_from.Checked AndAlso Me.dtp_to.Checked AndAlso Me.dtp_from.Value > Me.dtp_to.Value) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1365), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    ' Account
    If Not Me.uc_account_sel.ValidateFormat() Then
      Return False
    End If

    Return True

  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' GUI_ReportUpdateFilters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_account = String.Empty
    m_filter_card = String.Empty
    m_filter_holder = String.Empty
    m_filter_date_from = String.Empty
    m_filter_date_to = String.Empty
    m_filter_representatives = String.Empty
    m_filter_junkets = String.Empty
    m_filter_report_type = String.Empty

    If Not String.IsNullOrEmpty(uc_account_sel.Account) Then
      m_filter_account = uc_account_sel.Account
    End If

    If Not String.IsNullOrEmpty(uc_account_sel.TrackData) Then
      m_filter_card = uc_account_sel.TrackData
    End If

    If Not String.IsNullOrEmpty(uc_account_sel.Holder) Then
      m_filter_holder = uc_account_sel.Holder
    End If

    If Not String.IsNullOrEmpty(uc_account_sel.HolderIsVip) Then
      m_filter_vip = uc_account_sel.HolderIsVip
    End If

    If dtp_from.Checked Then
      m_filter_date_from = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    If dtp_to.Checked Then
      m_filter_date_to = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    m_filter_representatives = GetDescListSelected(dg_filter_representatives, opt_all_representatives.Checked)
    If (String.IsNullOrEmpty(m_filter_representatives)) Then
      m_filter_representatives = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6774)
    End If

    m_filter_junkets = GetDescListSelected(dg_filter_junkets, opt_all_junkets.Checked)
    If (String.IsNullOrEmpty(m_filter_junkets)) Then
      m_filter_junkets = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6774)
    End If

    Select Case (m_report_type)
      Case ENUM_REPORT_TYPE.WORKINGDAY
        m_filter_report_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8250)
      Case ENUM_REPORT_TYPE.CUSTOMERS
        m_filter_report_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8251)
      Case ENUM_REPORT_TYPE.REPRESENTATIVES
        m_filter_report_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8252)
    End Select

    m_filter_exclude_without_activity = IIf(chk_show_without_activity.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' GUI_ReportFilter
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(311), m_filter_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(312), m_filter_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_filter_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_filter_card)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_filter_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_filter_vip)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8089), m_filter_representatives)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7997), m_filter_junkets)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8267), m_filter_report_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4738), m_filter_exclude_without_activity)

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' GUI_FilterGetSqlQuery
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    ' Get the query based on report selection

    Dim _str_sql As String = ""

    Select Case (m_report_type)
      Case ENUM_REPORT_TYPE.WORKINGDAY
        _str_sql = m_junket.WorkingDayQuery(Me.dtp_from, Me.dtp_to, Me.uc_account_sel, _
                                            GetGridFilter(dg_filter_representatives, opt_all_representatives.Checked), _
                                            GetGridFilter(dg_filter_junkets, opt_all_junkets.Checked), chk_show_without_activity.Checked)

      Case ENUM_REPORT_TYPE.CUSTOMERS
        _str_sql = m_junket.CustomerQuery(Me.dtp_from, Me.dtp_to, Me.uc_account_sel, _
                                          GetGridFilter(dg_filter_representatives, opt_all_representatives.Checked), _
                                          GetGridFilter(dg_filter_junkets, opt_all_junkets.Checked), chk_show_without_activity.Checked)

      Case ENUM_REPORT_TYPE.REPRESENTATIVES
        _str_sql = m_junket.RepresentativeQuery(Me.dtp_from, Me.dtp_to, Me.uc_account_sel, _
                                               GetGridFilter(dg_filter_representatives, opt_all_representatives.Checked), _
                                               GetGridFilter(dg_filter_junkets, opt_all_junkets.Checked), chk_show_without_activity.Checked)
    End Select

    Return _str_sql

  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' GUI_BeforeFirstRow
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeFirstRow()

    ' Set Subtotals to 0
    Call ResetSubtotals()

    Call ResetTotals()

    ' Set Subtotal Id to ""
    m_subtotal_id = ""

  End Sub ' GUI_BeforeFirstRow

  ''' <summary>
  ''' GUI_CheckOutRowBeforeAdd
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _BuyIn As Decimal
    Dim _CoinIn As Decimal
    Dim _Customer As Decimal
    Dim _Netwin As Decimal
    Dim _NewCustomer As Decimal
    Dim _Points As Decimal
    Dim _Visit As Decimal
    Dim _Totals As Decimal

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_BUY_IN) Then
      _BuyIn = DbRow.Value(SQL_COLUMN_COMMISION_BUY_IN)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_COIN_IN) Then
      _CoinIn = DbRow.Value(SQL_COLUMN_COMMISION_COIN_IN)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_CUSTOMER) Then
      _Customer = DbRow.Value(SQL_COLUMN_COMMISION_CUSTOMER)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NETWIN) Then
      _Netwin = DbRow.Value(SQL_COLUMN_COMMISION_NETWIN)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NEW_CUSTOMER) Then
      _NewCustomer = DbRow.Value(SQL_COLUMN_COMMISION_NEW_CUSTOMER)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_POINTS) Then
      _Points = DbRow.Value(SQL_COLUMN_COMMISION_POINTS)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_VISIT) Then
      _Visit = DbRow.Value(SQL_COLUMN_COMMISION_VISIT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_COMMISION_TOTAL) Then
      _Totals = DbRow.Value(SQL_COLUMN_COMMISION_TOTAL)
    End If

    Select Case (m_report_type)
      Case ENUM_REPORT_TYPE.CUSTOMERS
        If Not String.Equals(m_subtotal_id, DbRow.Value(SQL_COLUMN_JUNKET_CODE)) Then
          If Not String.IsNullOrEmpty(m_subtotal_id) Then
            DrawRow(False)
          End If
          m_subtotal_id = DbRow.Value(SQL_COLUMN_JUNKET_CODE)

          ResetSubtotals()
        End If
      Case ENUM_REPORT_TYPE.WORKINGDAY
        If Not String.Equals(m_subtotal_id, DbRow.Value(SQL_COLUMN_JUNKET_CODE)) Then
          If Not String.IsNullOrEmpty(m_subtotal_id) Then
            DrawRow(False)
          End If
          m_subtotal_id = DbRow.Value(SQL_COLUMN_JUNKET_CODE)

          ResetSubtotals()
        End If
      Case ENUM_REPORT_TYPE.REPRESENTATIVES

    End Select

    ' Update subtotals
    m_subtotal_Commission_BuyIn += _BuyIn
    m_subtotal_Commission_CoinIn += _CoinIn
    m_subtotal_Commission_Customer += _Customer
    m_subtotal_Commission_NetWin += _Netwin
    m_subtotal_Commission_NewCustomer += _NewCustomer
    m_subtotal_Commission_Points += _Points
    m_subtotal_Commission_Visit += _Visit
    m_subtotal_Commission_Totals += _Totals

    'Update totals
    m_total_Commission_BuyIn += _BuyIn
    m_total_Commission_CoinIn += _CoinIn
    m_total_Commission_Customer += _Customer
    m_total_Commission_NetWin += _Netwin
    m_total_Commission_NewCustomer += _NewCustomer
    m_total_Commission_Points += _Points
    m_total_Commission_Visit += _Visit
    m_total_Commission_Totals += _Totals

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ''' <summary>
  ''' GUI_SetupRow
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Select Case (m_report_type)

      Case ENUM_REPORT_TYPE.WORKINGDAY

        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_CODE) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_CODE).Value = DbRow.Value(SQL_COLUMN_JUNKET_CODE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_NAME).Value = DbRow.Value(SQL_COLUMN_JUNKET_NAME)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_START) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_START).Value = _
          GUI_FormatDate(DbRow.Value(SQL_COLUMN_JUNKET_START), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_END) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_END).Value = _
          GUI_FormatDate(DbRow.Value(SQL_COLUMN_JUNKET_END), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Value = DbRow.Value(SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Value = DbRow.Value(SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = _
            GUI_FormatDate(DbRow.Value(SQL_COLUMN_WORKING_DAY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_CUSTOMER).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_CUSTOMER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_VISIT) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_VISIT).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_VISIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NEW_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_NEW_CUSTOMER).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_NEW_CUSTOMER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_POINTS) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_POINTS).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_POINTS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_COIN_IN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_COIN_IN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_COIN_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_BUY_IN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_BUY_IN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_BUY_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NETWIN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_NETWIN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_NETWIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_TOTAL) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_TOTAL).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_TOTAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_TOTAL).Value = _
            GUI_FormatCurrency(0.0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      Case ENUM_REPORT_TYPE.CUSTOMERS
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_CODE) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_CODE).Value = DbRow.Value(SQL_COLUMN_JUNKET_CODE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_NAME).Value = DbRow.Value(SQL_COLUMN_JUNKET_NAME)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_START) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_START).Value = _
          GUI_FormatDate(DbRow.Value(SQL_COLUMN_JUNKET_START), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_END) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_END).Value = _
          GUI_FormatDate(DbRow.Value(SQL_COLUMN_JUNKET_END), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Value = DbRow.Value(SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Value = DbRow.Value(SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CUSTOMER).Value = DbRow.Value(SQL_COLUMN_CUSTOMER)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_CUSTOMER_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CUSTOMER_NAME).Value = DbRow.Value(SQL_COLUMN_CUSTOMER_NAME)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_CUSTOMER).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_CUSTOMER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_VISIT) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_VISIT).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_VISIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NEW_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_NEW_CUSTOMER).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_NEW_CUSTOMER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_POINTS) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_POINTS).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_POINTS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_COIN_IN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_COIN_IN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_COIN_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_BUY_IN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_BUY_IN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_BUY_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NETWIN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_NETWIN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_NETWIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_TOTAL) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_TOTAL).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_TOTAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_TOTAL).Value = _
            GUI_FormatCurrency(0.0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      Case ENUM_REPORT_TYPE.REPRESENTATIVES
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_CODE) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_CODE).Value = DbRow.Value(SQL_COLUMN_JUNKET_CODE)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_JUNKET_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_JUNKET_NAME).Value = DbRow.Value(SQL_COLUMN_JUNKET_NAME)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_CUSTOMER).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_CUSTOMER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_VISIT) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_VISIT).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_VISIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NEW_CUSTOMER) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_NEW_CUSTOMER).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_NEW_CUSTOMER), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_POINTS) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_POINTS).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_POINTS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_COIN_IN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_COIN_IN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_COIN_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_BUY_IN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_BUY_IN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_BUY_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_NETWIN) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_NETWIN).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_NETWIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
        If Not DbRow.IsNull(SQL_COLUMN_COMMISION_TOTAL) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_TOTAL).Value = _
            GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COMMISION_TOTAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_COMMISION_TOTAL).Value = _
            GUI_FormatCurrency(0.0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

    End Select

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' GUI_AfterLastRow
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    Select Case (m_report_type)

      Case ENUM_REPORT_TYPE.WORKINGDAY
        ' Draw Sub Total
        Call DrawRow(False)
      Case ENUM_REPORT_TYPE.CUSTOMERS
        ' Draw Sub Total
        Call DrawRow(False)

    End Select

    ' Draw totals
    Call DrawRow(True)

  End Sub ' GUI_AfterLastRow

  ''' <summary>
  ''' GUI_FilterReset
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'GUI_FilterReset

#End Region ' Overrides

#Region " Public methods "

  ''' <summary>
  ''' ShowForEdit
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public methods

#Region " Private methods "

  ''' <summary>
  ''' SetDefaultValues
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _ds_result As DataSet
    Dim _str_closing_time As String

    _ds_result = New DataSet
    _str_closing_time = New DateTime(TimeSpan.FromHours(GetDefaultClosingTime()).Ticks).ToString("HH:mm")

    ' Dates
    dtp_from.Value = GUI_GetDate().AddHours(GetDefaultClosingTime())
    dtp_from.Checked = True
    dtp_to.Value = GUI_GetDate().AddDays(1).AddHours(GetDefaultClosingTime())
    dtp_to.Checked = False

    ' Account
    uc_account_sel.Clear()

    opt_all_representatives.Checked = True
    opt_all_junkets.Checked = True

    opt_working_day.Checked = True

    If CLASS_JUNKETS_FLYER.GetDataForFilters(_ds_result) Then
      LoadFilterGrid(dg_filter_representatives, _ds_result.Tables("JUNKETS_REPRESENTATIVES"))
      LoadFilterGrid(dg_filter_junkets, _ds_result.Tables("JUNKETS"))
    End If

    lbl_from_hour.Text = _str_closing_time
    lbl_to_hour.Text = _str_closing_time

    chk_show_without_activity.Checked = True

  End Sub ' SetDefaultvalues

  ''' <summary>
  ''' GUI_StyleView
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    Select Case (m_report_type)
      Case ENUM_REPORT_TYPE.WORKINGDAY
        StyleSheetWorkingDays()
      Case ENUM_REPORT_TYPE.CUSTOMERS
        StyleSheetCustomers()
      Case ENUM_REPORT_TYPE.REPRESENTATIVES
        StyleSheetRepresentatives()
    End Select

  End Sub ' GUI_StyleSheet

  Private Sub GUI_StyleSheetFilter(ByRef Grid As uc_grid)

    With Grid
      Call .Init(GRID_FILTER_COLUMNS, GRID_FILTER_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_FILTER_COLUMN_ID).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_FILTER_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_FILTER_COLUMN_CHECKED).Fixed = True
      .Column(GRID_FILTER_COLUMN_CHECKED).Editable = True
      .Column(GRID_FILTER_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_FILTER_COLUMN_DESC).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_DESC).WidthFixed = 3000
      .Column(GRID_FILTER_COLUMN_DESC).Fixed = True
      .Column(GRID_FILTER_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheetFilter

  ''' <summary>
  ''' StyleSheetWorkingDays
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub StyleSheetWorkingDays()

    ResetGridColumns()

    With Me.Grid

      GRID_COLUMNS = 16

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Junket Code
      .Column(GRID_COLUMN_JUNKET_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8253)
      .Column(GRID_COLUMN_JUNKET_CODE).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_JUNKET_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Junket Name
      .Column(GRID_COLUMN_JUNKET_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8254)
      .Column(GRID_COLUMN_JUNKET_NAME).Width = GRID_WIDTH_NAMES
      .Column(GRID_COLUMN_JUNKET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Junket Start
      .Column(GRID_COLUMN_JUNKET_START).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_START).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8255)
      .Column(GRID_COLUMN_JUNKET_START).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_JUNKET_START).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Junket END
      .Column(GRID_COLUMN_JUNKET_END).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_END).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8256)
      .Column(GRID_COLUMN_JUNKET_END).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_JUNKET_END).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Representative Code
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8257)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Representative name
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8258)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Width = GRID_WIDTH_NAMES
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Working Day
      .Column(GRID_COLUMN_WORKING_DAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_WORKING_DAY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_COLUMN_WORKING_DAY).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_WORKING_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Customer
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Customer Visits
      .Column(GRID_COLUMN_COMMISION_VISIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_VISIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8261)
      .Column(GRID_COLUMN_COMMISION_VISIT).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_VISIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' New Customers
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8262)
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Points
      .Column(GRID_COLUMN_COMMISION_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205)
      .Column(GRID_COLUMN_COMMISION_POINTS).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin-In
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661)
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Buy-In
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Net Win
      .Column(GRID_COLUMN_COMMISION_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8263)
      .Column(GRID_COLUMN_COMMISION_NETWIN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total
      .Column(GRID_COLUMN_COMMISION_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)
      .Column(GRID_COLUMN_COMMISION_TOTAL).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

    ' Reset the SQL Columns for the Report

    SQL_COLUMN_JUNKET_CODE = 0
    SQL_COLUMN_JUNKET_NAME = 1
    SQL_COLUMN_JUNKET_START = 2
    SQL_COLUMN_JUNKET_END = 3
    SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE = 4
    SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME = 5
    SQL_COLUMN_WORKING_DAY = 6
    SQL_COLUMN_COMMISION_CUSTOMER = 7
    SQL_COLUMN_COMMISION_VISIT = 8
    SQL_COLUMN_COMMISION_NEW_CUSTOMER = 9
    SQL_COLUMN_COMMISION_POINTS = 10
    SQL_COLUMN_COMMISION_COIN_IN = 11
    SQL_COLUMN_COMMISION_BUY_IN = 12
    SQL_COLUMN_COMMISION_NETWIN = 13
    SQL_COLUMN_COMMISION_TOTAL = 14

  End Sub ' StyleSheetWorkingDays

  ''' <summary>
  ''' StyleSheetCustomers
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub StyleSheetCustomers()

    ResetGridColumns()

    With Me.Grid

      GRID_COLUMNS = 17

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Junket Code
      .Column(GRID_COLUMN_JUNKET_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8253)
      .Column(GRID_COLUMN_JUNKET_CODE).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_JUNKET_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Junket Name
      .Column(GRID_COLUMN_JUNKET_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8254)
      .Column(GRID_COLUMN_JUNKET_NAME).Width = GRID_WIDTH_NAMES
      .Column(GRID_COLUMN_JUNKET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Junket Start
      .Column(GRID_COLUMN_JUNKET_START).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_START).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8255)
      .Column(GRID_COLUMN_JUNKET_START).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_JUNKET_START).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Junket END
      .Column(GRID_COLUMN_JUNKET_END).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8014)
      .Column(GRID_COLUMN_JUNKET_END).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8256)
      .Column(GRID_COLUMN_JUNKET_END).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_JUNKET_END).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Representative Code
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8257)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Representative name
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8258)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Width = GRID_WIDTH_NAMES
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Customer
      .Column(GRID_COLUMN_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
      .Column(GRID_COLUMN_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Customer
      .Column(GRID_COLUMN_CUSTOMER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_CUSTOMER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
      .Column(GRID_COLUMN_CUSTOMER_NAME).Width = GRID_WIDTH_NAMES
      .Column(GRID_COLUMN_CUSTOMER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Customer Comission
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Customer Visits
      .Column(GRID_COLUMN_COMMISION_VISIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_VISIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8261)
      .Column(GRID_COLUMN_COMMISION_VISIT).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_VISIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' New Customers
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8262)
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Points
      .Column(GRID_COLUMN_COMMISION_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205)
      .Column(GRID_COLUMN_COMMISION_POINTS).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin-In
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661)
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Buy-In
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Net Win
      .Column(GRID_COLUMN_COMMISION_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8263)
      .Column(GRID_COLUMN_COMMISION_NETWIN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total
      .Column(GRID_COLUMN_COMMISION_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)
      .Column(GRID_COLUMN_COMMISION_TOTAL).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

    ' Reset the SQL Columns for the Report

    SQL_COLUMN_JUNKET_CODE = 0
    SQL_COLUMN_JUNKET_NAME = 1
    SQL_COLUMN_JUNKET_START = 2
    SQL_COLUMN_JUNKET_END = 3
    SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE = 4
    SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME = 5
    SQL_COLUMN_CUSTOMER = 6
    SQL_COLUMN_CUSTOMER_NAME = 7
    SQL_COLUMN_COMMISION_CUSTOMER = 8
    SQL_COLUMN_COMMISION_VISIT = 9
    SQL_COLUMN_COMMISION_NEW_CUSTOMER = 10
    SQL_COLUMN_COMMISION_POINTS = 11
    SQL_COLUMN_COMMISION_COIN_IN = 12
    SQL_COLUMN_COMMISION_BUY_IN = 13
    SQL_COLUMN_COMMISION_NETWIN = 14
    SQL_COLUMN_COMMISION_TOTAL = 15

  End Sub ' StyleSheetCustomers

  ''' <summary>
  ''' StyleSheetRepresentatives
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub StyleSheetRepresentatives()

    ResetGridColumns()

    With Me.Grid

      GRID_COLUMNS = 11

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Representative Code
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8257)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Representative name
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8015)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8258)
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Width = GRID_WIDTH_NAMES
      .Column(GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Customer
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6763)
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Customer Visits
      .Column(GRID_COLUMN_COMMISION_VISIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_VISIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8261)
      .Column(GRID_COLUMN_COMMISION_VISIT).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_VISIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' New Customers
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8262)
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_NEW_CUSTOMER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Points
      .Column(GRID_COLUMN_COMMISION_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205)
      .Column(GRID_COLUMN_COMMISION_POINTS).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin-In
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661)
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Buy-In
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_BUY_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Net Win
      .Column(GRID_COLUMN_COMMISION_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8263)
      .Column(GRID_COLUMN_COMMISION_NETWIN).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total
      .Column(GRID_COLUMN_COMMISION_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
      .Column(GRID_COLUMN_COMMISION_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)
      .Column(GRID_COLUMN_COMMISION_TOTAL).Width = GRID_WIDTH_DEFAULT
      .Column(GRID_COLUMN_COMMISION_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

    SQL_COLUMN_JUNKET_REPRESENTATIVE_CODE = 0
    SQL_COLUMN_JUNKET_REPRESENTATIVE_NAME = 1
    SQL_COLUMN_COMMISION_CUSTOMER = 2
    SQL_COLUMN_COMMISION_VISIT = 3
    SQL_COLUMN_COMMISION_NEW_CUSTOMER = 4
    SQL_COLUMN_COMMISION_POINTS = 5
    SQL_COLUMN_COMMISION_COIN_IN = 6
    SQL_COLUMN_COMMISION_BUY_IN = 7
    SQL_COLUMN_COMMISION_NETWIN = 8
    SQL_COLUMN_COMMISION_TOTAL = 9

  End Sub ' StyleSheetRepresentatives

  ''' <summary>
  ''' ResetGridColumns
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetGridColumns()

    Select Case (m_report_type)
      Case ENUM_REPORT_TYPE.WORKINGDAY
        GRID_COLUMN_JUNKET_CODE = 1
        GRID_COLUMN_JUNKET_NAME = 2
        GRID_COLUMN_JUNKET_START = 3
        GRID_COLUMN_JUNKET_END = 4
        GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE = 5
        GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME = 6
        GRID_COLUMN_WORKING_DAY = 7
        GRID_COLUMN_COMMISION_CUSTOMER = 8
        GRID_COLUMN_COMMISION_VISIT = 9
        GRID_COLUMN_COMMISION_NEW_CUSTOMER = 10
        GRID_COLUMN_COMMISION_POINTS = 11
        GRID_COLUMN_COMMISION_COIN_IN = 12
        GRID_COLUMN_COMMISION_BUY_IN = 13
        GRID_COLUMN_COMMISION_NETWIN = 14
        GRID_COLUMN_COMMISION_TOTAL = 15
      Case ENUM_REPORT_TYPE.CUSTOMERS
        GRID_COLUMN_JUNKET_CODE = 1
        GRID_COLUMN_JUNKET_NAME = 2
        GRID_COLUMN_JUNKET_START = 3
        GRID_COLUMN_JUNKET_END = 4
        GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE = 5
        GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME = 6
        GRID_COLUMN_CUSTOMER = 7
        GRID_COLUMN_CUSTOMER_NAME = 8
        GRID_COLUMN_COMMISION_CUSTOMER = 9
        GRID_COLUMN_COMMISION_VISIT = 10
        GRID_COLUMN_COMMISION_NEW_CUSTOMER = 11
        GRID_COLUMN_COMMISION_POINTS = 12
        GRID_COLUMN_COMMISION_COIN_IN = 13
        GRID_COLUMN_COMMISION_BUY_IN = 14
        GRID_COLUMN_COMMISION_NETWIN = 15
        GRID_COLUMN_COMMISION_TOTAL = 16
      Case ENUM_REPORT_TYPE.REPRESENTATIVES
        GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE = 1
        GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME = 2
        GRID_COLUMN_COMMISION_CUSTOMER = 3
        GRID_COLUMN_COMMISION_VISIT = 4
        GRID_COLUMN_COMMISION_NEW_CUSTOMER = 5
        GRID_COLUMN_COMMISION_POINTS = 6
        GRID_COLUMN_COMMISION_COIN_IN = 7
        GRID_COLUMN_COMMISION_BUY_IN = 8
        GRID_COLUMN_COMMISION_NETWIN = 9
        GRID_COLUMN_COMMISION_TOTAL = 10
    End Select

    Me.Grid.Refresh()
    Me.Grid.Redraw = True

  End Sub ' ResetGridColumns

  ''' <summary>
  ''' Get filter Description list
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetDescListSelected(ByRef Grid As uc_grid, GetAll As Boolean) As String

    Dim _idx_row As Integer
    Dim _list As List(Of String)

    _list = New List(Of String)

    For _idx_row = 0 To Grid.NumRows - 1
      If Not GetAll Then
        If (Grid.Cell(_idx_row, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED) Then
          _list.Add(Grid.Cell(_idx_row, GRID_FILTER_COLUMN_DESC).Value)
        End If
      End If
    Next

    Return String.Join(", ", _list.ToArray())

  End Function ' GetTypeDescListSelected

  ''' <summary>
  ''' Add row in grid
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <param name="Row"></param>
  ''' <remarks></remarks>
  Private Sub AddRowDataFilter(ByRef Grid As uc_grid, ByRef Row As DataRow)

    Dim prev_redraw As Boolean

    prev_redraw = Me.Grid.Redraw
    Grid.Redraw = False

    Grid.AddRow()

    Grid.Redraw = False

    Grid.Cell(Grid.NumRows - 1, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Grid.Cell(Grid.NumRows - 1, GRID_FILTER_COLUMN_ID).Value = Row("ID")
    Grid.Cell(Grid.NumRows - 1, GRID_FILTER_COLUMN_DESC).Value = Row("DESCRIPTION")

    Grid.Redraw = prev_redraw

  End Sub ' AddRowDataFilter

  ''' <summary>
  ''' Set results on grid
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <param name="Results"></param>
  ''' <remarks></remarks>
  Private Sub LoadFilterGrid(ByRef Grid As uc_grid, ByRef Results As DataTable)

    Grid.Clear()

    For Each _row As DataRow In Results.Rows
      AddRowDataFilter(Grid, _row)
    Next

  End Sub ' LoadFilterGrid

  ''' <summary>
  ''' Get selecteds rows in filter grids
  ''' </summary>
  ''' <param name="Grid"></param>
  ''' <param name="GetAll"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetGridFilter(ByRef Grid As uc_grid, GetAll As Boolean) As String

    Dim _list_id As List(Of String)

    _list_id = New List(Of String)

    If GetAll Then Return String.Empty

    For i As Int32 = 0 To Grid.NumRows - 1
      If Grid.Cell(i, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _list_id.Add(Grid.Cell(i, GRID_FILTER_COLUMN_ID).Value.ToString)
      End If
    Next

    Return String.Join(", ", _list_id.ToArray())

  End Function ' GetGridFilter

  ''' <summary>
  ''' Inicialize the subtotals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetSubtotals()

    m_subtotal_Commission_Customer = 0.0
    m_subtotal_Commission_Visit = 0.0
    m_subtotal_Commission_NewCustomer = 0.0
    m_subtotal_Commission_Points = 0.0
    m_subtotal_Commission_CoinIn = 0.0
    m_subtotal_Commission_BuyIn = 0.0
    m_subtotal_Commission_NetWin = 0.0
    m_subtotal_Commission_Totals = 0.0

  End Sub ' ResetSubtotals

  ''' <summary>
  ''' Inicialize the totals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetTotals()

    m_total_Commission_Customer = 0.0
    m_total_Commission_Visit = 0.0
    m_total_Commission_NewCustomer = 0.0
    m_total_Commission_Points = 0.0
    m_total_Commission_CoinIn = 0.0
    m_total_Commission_BuyIn = 0.0
    m_total_Commission_NetWin = 0.0
    m_total_Commission_Totals = 0.0

  End Sub ' ResetTotals

  ''' <summary>
  ''' Add row in results table
  ''' </summary>
  ''' <param name="IsTotal"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DrawRow(ByVal IsTotal As Boolean) As Boolean
    Dim _idx_row As Integer
    Dim _subtotal_title As String

    _subtotal_title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271) + " "

    With Me.Grid

      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      Select Case (m_report_type)

        Case ENUM_REPORT_TYPE.WORKINGDAY
          .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty
          If IsTotal Then
            .Cell(_idx_row, GRID_COLUMN_JUNKET_CODE).Value = _subtotal_title
          Else
            .Cell(_idx_row, GRID_COLUMN_JUNKET_CODE).Value = _subtotal_title + m_subtotal_id
          End If
          .Cell(_idx_row, GRID_COLUMN_JUNKET_NAME).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_JUNKET_NAME).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_WORKING_DAY).Value = String.Empty

        Case ENUM_REPORT_TYPE.CUSTOMERS
          .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty
          If IsTotal Then
            .Cell(_idx_row, GRID_COLUMN_JUNKET_CODE).Value = _subtotal_title
          Else
            .Cell(_idx_row, GRID_COLUMN_JUNKET_CODE).Value = _subtotal_title + m_subtotal_id
          End If
          .Cell(_idx_row, GRID_COLUMN_JUNKET_NAME).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_JUNKET_NAME).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_CUSTOMER).Value = String.Empty

        Case ENUM_REPORT_TYPE.REPRESENTATIVES
          .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty
          If IsTotal Then
            .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Value = _subtotal_title
          Else
            .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_CODE).Value = _subtotal_title + m_subtotal_id
          End If
          .Cell(_idx_row, GRID_COLUMN_JUNKET_REPRESENTATIVE_NAME).Value = String.Empty

      End Select

      If Not IsTotal Then
        .Cell(_idx_row, GRID_COLUMN_COMMISION_CUSTOMER).Value = GUI_FormatCurrency(m_subtotal_Commission_Customer, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_VISIT).Value = GUI_FormatCurrency(m_subtotal_Commission_Visit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_NEW_CUSTOMER).Value = GUI_FormatCurrency(m_subtotal_Commission_NewCustomer, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_POINTS).Value = GUI_FormatCurrency(m_subtotal_Commission_Points, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_COIN_IN).Value = GUI_FormatCurrency(m_subtotal_Commission_CoinIn, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_BUY_IN).Value = GUI_FormatCurrency(m_subtotal_Commission_BuyIn, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_NETWIN).Value = GUI_FormatCurrency(m_subtotal_Commission_NetWin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_TOTAL).Value = GUI_FormatCurrency(m_subtotal_Commission_Totals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      Else
        .Cell(_idx_row, GRID_COLUMN_COMMISION_CUSTOMER).Value = GUI_FormatCurrency(m_total_Commission_Customer, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_VISIT).Value = GUI_FormatCurrency(m_total_Commission_Visit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_NEW_CUSTOMER).Value = GUI_FormatCurrency(m_total_Commission_NewCustomer, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_POINTS).Value = GUI_FormatCurrency(m_total_Commission_Points, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_COIN_IN).Value = GUI_FormatCurrency(m_total_Commission_CoinIn, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_BUY_IN).Value = GUI_FormatCurrency(m_total_Commission_BuyIn, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_NETWIN).Value = GUI_FormatCurrency(m_total_Commission_NetWin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_idx_row, GRID_COLUMN_COMMISION_TOTAL).Value = GUI_FormatCurrency(m_total_Commission_Totals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If

    End With

    Return True
  End Function ' DrawRow

  ''' <summary>
  ''' Init resourses
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitControlResourses()
    'Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10) ' 10 "Salir"

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8266)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' Report Type
    Me.gb_report_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8267)
    Me.opt_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8250)
    Me.opt_customers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8251)
    Me.opt_representative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8252)

    ' Representatives
    Me.gb_representatives.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8089)
    Me.opt_several_representatives.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_representatives.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    ' Junkets
    Me.gb_junkets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7997)
    Me.opt_several_junkets.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_junkets.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    Me.chk_show_without_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4738)

  End Sub ' InitControlResourses

#End Region ' Private methods 

#Region " Events "

  Private Sub opt_working_day_Click(sender As Object, e As EventArgs) Handles opt_working_day.Click
    m_report_type = ENUM_REPORT_TYPE.WORKINGDAY
  End Sub ' opt_working_day_Click

  Private Sub opt_customers_Click(sender As Object, e As EventArgs) Handles opt_customers.Click
    m_report_type = ENUM_REPORT_TYPE.CUSTOMERS
  End Sub ' opt_customers_Click

  Private Sub opt_representative_Click(sender As Object, e As EventArgs) Handles opt_representative.Click
    m_report_type = ENUM_REPORT_TYPE.REPRESENTATIVES
  End Sub ' opt_representative_Click

  Private Sub dg_filter_representatives_CellDataChangedEvent(Row As Integer, Column As Integer) Handles dg_filter_representatives.CellDataChangedEvent
    ' Update radio buttons accordingly
    If Me.opt_several_representatives.Checked = False Then
      Me.opt_several_representatives.Checked = True
    End If
  End Sub ' dg_filter_representatives_CellDataChangedEvent

  Private Sub dg_filter_junkets_CellDataChangedEvent(Row As Integer, Column As Integer) Handles dg_filter_junkets.CellDataChangedEvent
    ' Update radio buttons accordingly
    If Me.opt_several_junkets.Checked = False Then
      Me.opt_several_junkets.Checked = True
    End If
  End Sub ' dg_filter_junkets_CellDataChangedEvent

  Private Sub opt_all_representatives_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_representatives.CheckedChanged
    Dim idx As Integer

    If opt_all_representatives.Checked Then
      For idx = 0 To dg_filter_representatives.NumRows - 1
        Me.dg_filter_representatives.Cell(idx, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
    End If
  End Sub ' opt_all_representatives_CheckedChanged

  Private Sub opt_all_junkets_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_junkets.CheckedChanged
    Dim idx As Integer

    If opt_all_junkets.Checked Then
      For idx = 0 To dg_filter_junkets.NumRows - 1
        Me.dg_filter_junkets.Cell(idx, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
    End If
  End Sub ' opt_all_junkets_CheckedChanged

#End Region ' Events

End Class
