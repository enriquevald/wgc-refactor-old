'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_source_target_concepts.vb
' DESCRIPTION:   Cage Source Target Concepts Form Selection
' AUTHOR:        Omar P�rez
' CREATION DATE: 16-SEP-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-SEP-2014  OPC    Initial version
' 02-OCT-2014  LRS    Enabled/Disable button "Relaciones" depending of grid rows
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"

Imports GUI_Controls
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports WSI.Common
#End Region

Public Class frm_cage_source_target_concepts
  Inherits GUI_Controls.frm_base_sel

#Region "Constants"

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CONCEPT_ID As Integer = 1
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 3
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_PROVISION As Integer = 4
  Private Const GRID_COLUMN_SHOW_REPORT As Integer = 5
  Private Const GRID_COLUMN_UNIT_PRICE As Integer = 6
  Private Const GRID_COLUMN_TYPE As Integer = 7
  Private Const GRID_COLUMN_ENABLED As Integer = 8
  Private Const GRID_COLUMN_ENABLED_BIT As Integer = 9

  Private Const SQL_COLUMN_CONCEPT_ID As Integer = 0
  Private Const SQL_COLUMN_DESCRIPTION As Integer = 1
  Private Const SQL_COLUMN_NAME As Integer = 2
  Private Const SQL_COLUMN_PROVISION As Integer = 3
  Private Const SQL_COLUMN_SHOW_REPORT As Integer = 4
  Private Const SQL_COLUMN_UNIT_PRICE As Integer = 5
  Private Const SQL_COLUMN_TYPE As Integer = 6
  Private Const SQL_COLUMN_ENABLED As Integer = 7

  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const DEFAULT_UNIT_PRICE As Double = 0
  Private Const MIN_CUSTOM As Integer = 1000

#End Region

#Region "Enums"

  Public Enum CONCEPT_TYPES As Integer
    SYSTEM = 0
    CUSTOM = 1
  End Enum

#End Region

#Region "Members"

  ' For excel report
  Private m_status As String
  Private m_concept_name As String
  Private m_show_system_concept As String

#End Region

#Region "Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SOURCE_TARGET_CONCEPTS
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5495) ' Change

    ' GUI Buttons - Visibility
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True

    ' Source
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5506)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Height += 10

    ' Relation
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5505)

    ' GUI Buttons - Enabled
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    ' GUI Buttons - Text
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Entry Field - Label
    Me.ef_Name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210) 'Name

    ' Entry Field - Input Mask 
    Call ef_Name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, cls_cage_source_target_concepts.MAX_DESCRIPTION_LEN) ' CREATE THE CLASS_CAGE_SOURCE...

    ' CheckBoxes - Text
    Me.chk_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(311) 'Enabled
    Me.chk_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(328) 'Disabled
    Me.chk_system.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5494) 'System

    ' Group Box
    gb_group.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259) 'Status

    ' Default Values
    SetDefaultValues()

    ' Grid Style
    Call GUI_StyleView()

  End Sub ' GUI_InitControls

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function ' GUI_GetQueryType

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_NEW
        Call EditNewTargetConcepts()
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call ShowSource()
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call ShowRelation()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As New System.Text.StringBuilder

    ' SELECT
    _str_sql.AppendLine("SELECT   cc_concept_id")
    _str_sql.AppendLine("       , cc_description")
    _str_sql.AppendLine("       , cc_name")
    _str_sql.AppendLine("       , cc_is_provision")
    _str_sql.AppendLine("       , cc_show_in_report")
    _str_sql.AppendLine("       , cc_unit_price")
    _str_sql.AppendLine("       , cc_type")
    _str_sql.AppendLine("       , cc_enabled")

    ' FROM
    _str_sql.AppendLine("  FROM   cage_concepts ")

    ' WHERE
    _str_sql.AppendLine(GetSqlWhere())

    ' ORDER BY
    _str_sql.AppendLine(" ORDER BY cc_concept_id ASC")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _str_sql As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _dataset As DataSet = Nothing
    Dim _datatable As DataTable

    Try

      _str_sql = GUI_FilterGetSqlQuery()
      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand(_str_sql))
      _sql_da.SelectCommand.CommandTimeout = 100
      _dataset = New DataSet()
      _db_trx.Fill(_sql_da, _dataset)
      _datatable = _dataset.Tables(0)

      Me.Grid.Redraw = False

      If Not ShowHugeNumberOfRows(_datatable.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      For Each _row As DataRow In _datatable.Rows

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        Me.Grid.AddRow()
        Call GUI_SetupRow(Me.Grid.NumRows - 1, New CLASS_DB_ROW(_row))
      Next

      _sql_da.Dispose()
      _db_trx.Dispose()

      ' enabled buttons if have results
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = Me.Grid.NumRows > 0

    Catch ex As OutOfMemoryException

    Catch ex As Exception

    Finally
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      If _dataset IsNot Nothing Then
        Call _dataset.Clear()
        Call _dataset.Dispose()
        _dataset = Nothing
      End If
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' PROVISION
    If DbRow.Value(SQL_COLUMN_PROVISION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVISION).Value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVISION).Value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    ' SHOW IN REPORTS
    If DbRow.Value(SQL_COLUMN_SHOW_REPORT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SHOW_REPORT).Value = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SHOW_REPORT).Value = GLB_NLS_GUI_CONFIGURATION.GetString(265)
    End If

    ' UNIT PRICE
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_UNIT_PRICE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_UNIT_PRICE).Value = GUI_FormatCurrency(Me.Grid.Cell(RowIndex, GRID_COLUMN_UNIT_PRICE).Value)
    End If

    ' TYPE
    ' 07-OCT-2014 SMN: IF ID < 1000 THEN System ELSE Custom
    If DbRow.Value(SQL_COLUMN_CONCEPT_ID) < 1000 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3393)
    End If

    ' ENABLED
    If DbRow.Value(SQL_COLUMN_ENABLED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED).Value = GLB_NLS_GUI_CONFIGURATION.GetString(311)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED_BIT).Value = 1 ' Enabled
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED).Value = GLB_NLS_GUI_CONFIGURATION.GetString(328)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED_BIT).Value = 0 ' Disabled
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub 'GUI_FilterReset()

  Protected Overrides Sub GUI_EditSelectedItem()

    If Not Me.Grid.SelectedRows Is Nothing Then
      Dim _idx_row As Short
      Dim _concept_id As Integer
      Dim _type As Boolean
      Dim _frm_concept_edit As Object

      _idx_row = Me.Grid.SelectedRows(0)

      _concept_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CONCEPT_ID).Value

      If Me.Grid.Cell(_idx_row, GRID_COLUMN_TYPE).Value.ToString = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268) Then
        _type = CONCEPT_TYPES.SYSTEM
      Else
        _type = CONCEPT_TYPES.CUSTOM
      End If

      _frm_concept_edit = New frm_cage_source_target_concepts_edit()

      Call _frm_concept_edit.ShowEditItem(_concept_id, _type)
      _frm_concept_edit = Nothing
      Call Me.Grid.Focus()

    End If

  End Sub 'GUI_EditSelectedItem

#Region "Reports"

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_status = String.Empty
    m_concept_name = String.Empty
    m_show_system_concept = String.Empty

    ' Status
    If Me.chk_enabled.Checked Then
      m_status = chk_enabled.Text()
    End If

    If Me.chk_disabled.Checked Then
      If String.IsNullOrEmpty(m_status) Then
        m_status = chk_disabled.Text()
      Else
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
      End If
    End If

    ' Name
    If Not String.IsNullOrEmpty(Me.ef_Name.Value) Then
      m_concept_name = Me.ef_Name.Value()
    End If

    ' Show
    If Me.chk_system.Checked Then
      m_show_system_concept = GLB_NLS_GUI_CONFIGURATION.GetString(311)
    Else
      m_show_system_concept = GLB_NLS_GUI_CONFIGURATION.GetString(328)
    End If

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(280), m_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5494), m_show_system_concept)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1296), m_concept_name)

  End Sub ' GUI_ReportFilter

#End Region

#End Region

#Region "Private"

  ' PURPOSE: Open target concept edit form to create New concept or edit.
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewTargetConcepts()

    Dim frm_Target_Concepts_Edit As Object

    frm_Target_Concepts_Edit = New frm_cage_source_target_concepts_edit

    Call frm_Target_Concepts_Edit.ShowNewItem()
    frm_Target_Concepts_Edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'EditNewTargetConcepts

  ' PURPOSE : Return the where clause.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS String:
  Private Function GetSqlWhere() As String

    Dim _str_where As New System.Text.StringBuilder

    ' ConceptId <> Global
    _str_where.AppendLine("WHERE cc_concept_id <> " & CageMeters.CageConceptId.Global)

    ' System
    If Not Me.chk_system.Checked Then
      _str_where.AppendLine(" AND cc_concept_id >= 1000")
    End If

    ' Description
    If Not String.IsNullOrEmpty(ef_Name.Value()) Then
      _str_where.AppendLine(" AND " & GUI_FilterField("cc_name", ef_Name.Value(), False, False))
    End If

    ' Enabled // Disabled
    If Not chk_enabled.Checked And chk_disabled.Checked Then
      _str_where.AppendLine(" AND cc_enabled = " & cls_cage_source_target_concepts.STATUS.DISABLED)
    End If
    If chk_enabled.Checked And Not chk_disabled.Checked Then
      _str_where.AppendLine(" AND cc_enabled = " & cls_cage_source_target_concepts.STATUS.ENABLED)
    End If

    Return _str_where.ToString()

  End Function 'GetSqlWhere

  ' PURPOSE : This method defines the style of the grid.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    If Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION Then
      Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    End If

    ' COLUMN_INDEX
    Me.Grid.Column(GRID_COLUMN_INDEX).Header.Text = "  "
    Me.Grid.Column(GRID_COLUMN_INDEX).Width = 200
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    ' CONCEPT_ID
    Me.Grid.Column(GRID_COLUMN_CONCEPT_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_CONCEPT_ID).Mapping = SQL_COLUMN_CONCEPT_ID

    ' NAME
    Me.Grid.Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210)  ' NAME
    Me.Grid.Column(GRID_COLUMN_NAME).Mapping = SQL_COLUMN_NAME
    Me.Grid.Column(GRID_COLUMN_NAME).Width = 2805
    Me.Grid.Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' DESCRIPTION
    Me.Grid.Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(62) ' Description
    Me.Grid.Column(GRID_COLUMN_DESCRIPTION).Mapping = SQL_COLUMN_DESCRIPTION
    Me.Grid.Column(GRID_COLUMN_DESCRIPTION).Width = 0 '4545
    Me.Grid.Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' PROVISION
    Me.Grid.Column(GRID_COLUMN_PROVISION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5474) ' Provision
    Me.Grid.Column(GRID_COLUMN_PROVISION).Mapping = SQL_COLUMN_PROVISION
    Me.Grid.Column(GRID_COLUMN_PROVISION).Width = 1500
    Me.Grid.Column(GRID_COLUMN_PROVISION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    ' VISIBILITY ON REPORTS
    Me.Grid.Column(GRID_COLUMN_SHOW_REPORT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5475)  ' Visible
    Me.Grid.Column(GRID_COLUMN_SHOW_REPORT).Mapping = SQL_COLUMN_SHOW_REPORT
    Me.Grid.Column(GRID_COLUMN_SHOW_REPORT).Width = 2100
    Me.Grid.Column(GRID_COLUMN_SHOW_REPORT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    ' PRICE
    Me.Grid.Column(GRID_COLUMN_UNIT_PRICE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5476)  ' Price
    Me.Grid.Column(GRID_COLUMN_UNIT_PRICE).Mapping = SQL_COLUMN_UNIT_PRICE
    Me.Grid.Column(GRID_COLUMN_UNIT_PRICE).Width = 1785
    Me.Grid.Column(GRID_COLUMN_UNIT_PRICE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    ' TYPE
    Me.Grid.Column(GRID_COLUMN_TYPE).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(379)  ' Type 
    Me.Grid.Column(GRID_COLUMN_TYPE).Mapping = SQL_COLUMN_TYPE
    Me.Grid.Column(GRID_COLUMN_TYPE).Width = 1620
    Me.Grid.Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    ' ENABLED
    Me.Grid.Column(GRID_COLUMN_ENABLED).Width = 2100
    Me.Grid.Column(GRID_COLUMN_ENABLED).Mapping = SQL_COLUMN_ENABLED
    Me.Grid.Column(GRID_COLUMN_ENABLED).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259)
    Me.Grid.Column(GRID_COLUMN_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    ' ENABLED BIT
    Me.Grid.Column(GRID_COLUMN_ENABLED_BIT).Width = 0
    Me.Grid.Column(GRID_COLUMN_ENABLED_BIT).Mapping = SQL_COLUMN_ENABLED
    Me.Grid.Column(GRID_COLUMN_ENABLED_BIT).Header.Text = String.Empty


  End Sub 'GUI_StyleView

  ' PURPOSE : Method to set Default Values.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()

    Me.chk_enabled.Checked = True
    Me.chk_disabled.Checked = False
    Me.chk_system.Checked = False
    Me.ef_Name.Value = String.Empty

  End Sub 'SetDefaultValues

  ' PURPOSE : Method to show the Cage Source Target form.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowSource()

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Dim _frm As frm_cage_source_target_edit

    _frm = New frm_cage_source_target_edit
    _frm.ShowEditItem(0)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub 'ShowSource

  ' PURPOSE : Method to show the Cage Concept Relation form.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub ShowRelation()

    ' Enabled
    If Not Me.Grid.SelectedRows Is Nothing AndAlso Me.Grid.SelectedRows.Length > 0 Then
      If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ENABLED_BIT).Value = 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5606), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        Return
      End If

      ' Custom
      If Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_CONCEPT_ID).Value < MIN_CUSTOM _
      And Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_CONCEPT_ID).Value <> CageMeters.CageConceptId.Tips Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5520), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        Return
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Dim _frm As frm_cage_concepts_relation

      _frm = New frm_cage_concepts_relation
      _frm.ShowEditItem(0, Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_CONCEPT_ID).Value)

      Windows.Forms.Cursor.Current = Cursors.Default


    End If

  End Sub ' ShowRelation

#End Region

#Region "Public"

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowNew()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Call Me.Display(True)

  End Sub ' ShowNew

#End Region

#Region "Events"

#End Region

End Class