'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_dialog_web_browser
' DESCRIPTION:   Open a html document
' AUTHOR:        Raul Cervera
' CREATION DATE: 11-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-JUN-2010  RCI    Initial version
' 07-FEB-2014  LJM    Fixed Bug WIG-517: The access to this form shouldn't be audited.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_Controls

Public Class frm_YesNo_web_browser
  Inherits frm_base

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = ""
    ' - Buttons

  End Sub ' GUI_InitControls


  Public Sub Init(ByVal MessageText As String, ByVal ButtonOkText As String, ByVal ButtonCancelText As String)
    Me.lbl_message.Text = MessageText
    btn_cancel.Text = ButtonCancelText
    btn_ok.Text = ButtonOkText
    btn_cancel.Visible = ((ButtonCancelText Is Nothing) Or (ButtonCancelText.Trim <> ""))
    btn_ok.Visible = ((ButtonOkText Is Nothing) Or (ButtonOkText.Trim <> ""))

  End Sub

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function


End Class ' frm_dialog_web_browser
