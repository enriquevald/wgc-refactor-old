'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_payment_orders_config
' DESCRIPTION:   Payment Order configuration form
' AUTHOR:        Luis Ernesto Mesa  
' CREATION DATE: 14-JAN-2013
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-JAN-2013  LEM    Initial version
' 08-FEB-2013  ICS    Limited the maximum tabs number that can be created.
' 17-JUN-2013  AMF    Fixed Defect #868: Exception when delete bank account
' 25-JUN-2014  JBC    Changed documents control to uc_grid
' 08-OCT-2014  LRS    Not allow two checks marked in the same row of documents
' 11-MAR-2015  FOS    TASK 496: Modify Form
' 25-MAR-2015  FOS    TASK 796: Defect with controls enabled/disabled
' 14-OCT-2016  RAB    PBI 18098: General params: Automatically add the GP to the system
' 06-FEB-2017  ETP    Fixed bug 24226: GP not saved.
' 02-JUL-2018  MS     Fixed bug 33434: Payment Order -- when the minium has some decimal number the minimum to create a check is readed without decimals.
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Drawing.Imaging
Imports System.Text

#End Region ' Imports

Public Class frm_payment_orders_config
  Inherits frm_base_edit

#Region " Members "

  Dim m_bank_accounts As DataTable
  Dim m_newtabs_counter As Integer
  Dim m_img_logo As Image
  Dim m_old_logo As Image
  Dim m_img_changed As Boolean = False
  Dim m_current_directory As String
  Dim m_params_order As PAYMENT_ORDER_CONFIGURATION
  Dim m_ignore_changes As Boolean
  Dim m_cropping_area As Boolean
  Dim m_moving_btn As Boolean = False
  Dim m_enter_panel_logo As Boolean = False
  Dim m_x As Integer
  Dim m_y As Integer
  Dim m_dx As Integer
  Dim m_dy As Integer
  Dim m_scale As Double
  Dim m_with As Integer
  Dim m_height As Integer

#End Region   ' Members

#Region " Constants "

  Const SCALE_CONSTANT1 As Double = 0.01
  Const SCALE_CONSTANT2 As Double = 0.05
  Const MAX_LENGHT_DOCUMENT_TYPE = 20
  Const SPLIT_CHAR_DOCUMENT_TYPE = ";"
  Const TIME_LIMIT_EDITABLE_ORDER = 45000

  Const MAX_TABS = 200

  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_PRINCIPAL As Integer = 3
  Private Const GRID_COLUMN_SECONDARY As Integer = 4

  Private Const GRID_COLUMN_WIDTH_SENTINEL As Integer = 200
  Private Const GRID_COLUMN_WIDTH_ID As Integer = 0
  Private Const GRID_COLUMN_WIDTH_NAME As Integer = 3200
  Private Const GRID_COLUMN_WIDTH_VISIBLE As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_REQUIRED As Integer = 1200

  ' IDENTIFICATION TYPES COLUMNS
  Private Const SQL_DT_COLUMN_ID As Integer = 0
  Private Const SQL_DT_COLUMN_ENABLED As Integer = 2
  Private Const SQL_DT_COLUMN_NAME As Integer = 1

#End Region ' Constants 

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PAYMENT_ORDER_CONFIG
    Call MyBase.GUI_SetFormId()

  End Sub              ' GUI_SetFormId

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    CloseCanceled = Not DiscardChanges()

  End Sub                ' GUI_Closing

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1559)

    Call InitControlsFormat()
    Call LoadParams()
    Call InitControlsText()
    Call InitTabPages()
    Call StyleSheetDocuments()
    Call PopulateIdentificationTypes(m_params_order)
    '11-MAR-2015 FOS
    Call CheckGenerateDocumentStatus()
    '25-MAR-2015 FOS
    Call CheckIsEnabledPaymentOrder()

  End Sub        ' GUI_InitControls

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

  End Sub       ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

  End Sub       ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_BANK_ACCOUNT()

        If Me.ScreenMode = ENUM_SCREEN_TYPE.MODE_NEW Then
          DbObjectId = 0
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

        If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND Then
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub        ' GUI_DB_Operation

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_OK
        Call OkButtonClick()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub         ' GUI_ButtonClick

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     TRUE: if everything goes ok
  '     FALSE: if fails
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _idf As Integer

    For _idf = 0 To tab_bank_accounts.TabPages.Count - 1
      If TabIsScreenDataOk(_idf, chk_enabled.Checked) = False Then
        Return False
      End If
    Next
    If chk_PaymentDocument.Checked Then
      If chk_enabled.Checked Then

        If Not ParamsIsScreenDataOk() Then
          Return False
        End If

        If DuplicatedAlias() Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1612), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Return False
        End If
      End If

      If Not ThereIsAccountEnabled() And chk_enabled.Checked Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1634), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Return False
        End If
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Privates "

  ' PURPOSE : Form controls text initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Private Sub InitControlsText()
    '11-MAR-2015 FOS
    Me.btn_deleteAccount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1607)
    Me.btn_addAccount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1594)
    chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1602)
    ef_min_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1603)
    ef_editable_minutes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1582)
    ef_editable_minutes.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1583)
    ef_authorit1_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613)
    ef_authorit1_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614)
    ef_authorit2_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613)
    ef_authorit2_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614)
    chk_authorit1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1616)
    chk_authorit2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1617)
    ef_doc_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1610)
    ef_reference.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1626)
    btn_select_img.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(659)
    btn_scale_up.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(660)
    btn_scale_down.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(661)
    lbl_logo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1611)
    lbl_min_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1622)
    '11-MAR-2015 FOS
    chk_PaymentDocument.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6035)
    gb_documents.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1565)
    tab_client.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(372)
    tab_docs_required.Text = GLB_NLS_GUI_AUDITOR.GetString(343)
    tab_autorize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1619)
    tab_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1553)
    lbl_required_documents.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1615)

  End Sub              ' InitControlsText

  ' PURPOSE : Form controls format initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Private Sub InitControlsFormat()
    '11-MAR-2015 FOS
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.btn_deleteAccount.Visible = True
    Me.btn_deleteAccount.Enabled = False
    Me.btn_deleteAccount.Type = uc_button.ENUM_BUTTON_TYPE.USER
    'Me.btn_deleteAccount.Size = New System.Drawing.Size(90, 45)

    Me.btn_addAccount.Visible = True
    Me.btn_addAccount.Enabled = Me.Permissions.Write
    Me.btn_addAccount.Type = uc_button.ENUM_BUTTON_TYPE.USER
    'Me.btn_addAccount.Size = New System.Drawing.Size(90, 45)

    Me.btn_select_img.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.btn_select_img.Size = New System.Drawing.Size(90, 35)
    Me.btn_scale_up.Enabled = False
    Me.btn_scale_down.Enabled = False
    Me.btn_select_img.Enabled = Me.Permissions.Write

    ef_min_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    ef_editable_minutes.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
    ef_authorit1_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ef_authorit1_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_authorit2_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, 50)
    ef_authorit2_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, 50)
    ef_doc_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 27) 'M�x TextWidth = 27 to fit space in pdf document template
    ef_reference.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)



  End Sub            ' InitControlsFormat

  ' PURPOSE: Initialize tabpage control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub InitTabPages()

    Dim _idf As Integer

    m_bank_accounts = CLASS_BANK_ACCOUNT.GetAllAccounts()

    If m_bank_accounts IsNot Nothing And m_bank_accounts.Rows.Count > 0 Then
      For _idf = 0 To m_bank_accounts.Rows.Count - 1
        Call CreateTabPage()
        Call InitTabStyle(_idf)
        Call DataRowToTab(_idf)
      Next
    End If

  End Sub                  ' InitTabPages

  '------------------------------------------------------------------------------
  ' PURPOSE : Create TabPage and Datagrids in Tabcontrol.
  '
  '  PARAMS :
  '      - INPUT :
  '           Grid Control uc_grid
  '           TabPage 
  '           TabPageText TabPage Text
  '           TabPageName TabPage Name
  '           ValuePanelRightVisible Boolean for view the right panel
  '           Title The Label
  '           TitleText The text to the label
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  '      - DataTable
  '    
  Private Sub CreateTabPage(Optional ByVal TabTitle As String = "")

    Dim _tab_page As TabPage
    Dim _enabled As CheckBox
    Dim _bank_name As uc_entry_field
    Dim _alias As uc_entry_field
    Dim _efective_days As uc_entry_field
    Dim _customer_number As uc_entry_field
    Dim _account_number As uc_entry_field
    Dim _payment_type As uc_entry_field
    Dim _id As Label
    Dim _alias_help As Label
    Dim _efective_days_help As Label
    Dim _left_margin As Integer = 10

    _tab_page = New TabPage()
    _enabled = New CheckBox()
    _bank_name = New uc_entry_field()
    _alias = New uc_entry_field()
    _efective_days = New uc_entry_field()
    _customer_number = New uc_entry_field()
    _account_number = New uc_entry_field()
    _payment_type = New uc_entry_field()
    _id = New Label()
    _alias_help = New Label()
    _efective_days_help = New Label()


    tab_bank_accounts.TabPages.Add(_tab_page)
    '
    ' TabPage
    '
    _tab_page.Controls.Add(_id)
    _tab_page.Controls.Add(_bank_name)
    _tab_page.Controls.Add(_alias)
    _tab_page.Controls.Add(_efective_days)
    _tab_page.Controls.Add(_customer_number)
    _tab_page.Controls.Add(_account_number)
    _tab_page.Controls.Add(_payment_type)
    _tab_page.Controls.Add(_enabled)
    _tab_page.Controls.Add(_alias_help)
    _tab_page.Controls.Add(_efective_days_help)
    _tab_page.Location = New System.Drawing.Point(4, 22)
    _tab_page.Name = "tbp_" & tab_bank_accounts.TabPages.Count - 1
    _tab_page.Size = New System.Drawing.Size(956, 268)
    _tab_page.Text = TabTitle
    _tab_page.UseVisualStyleBackColor = True
    '
    '_id
    '
    _id.Location = New System.Drawing.Point(0, 0)
    _id.Visible = False
    _id.Text = "0"
    '
    '_enabled
    '
    _enabled.Enabled = True
    _enabled.Location = New System.Drawing.Point(_left_margin, 10)
    _enabled.Name = "_enabled"
    _enabled.TabIndex = 0
    _enabled.Text = "xHabilitada"
    _enabled.Checked = False
    _enabled.Size = New System.Drawing.Size(130, 24)
    '
    '_alias
    '
    _alias.DoubleValue = 0
    _alias.IntegerValue = 0
    _alias.IsReadOnly = False
    _alias.Location = New System.Drawing.Point(_left_margin, 30)
    _alias.Name = "_account_info"
    _alias.Size = New System.Drawing.Size(270, 24)
    _alias.SufixText = "Sufix Text"
    _alias.SufixTextVisible = True
    _alias.TabIndex = 1
    _alias.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    _alias.TextValue = ""
    _alias.TextWidth = 115
    _alias.Value = ""
    '
    '_bank_name
    '
    _bank_name.DoubleValue = 0
    _bank_name.IntegerValue = 0
    _bank_name.IsReadOnly = False
    _bank_name.Location = New System.Drawing.Point(_left_margin, 60)
    _bank_name.Name = "_bank_name"
    _bank_name.Size = New System.Drawing.Size(270, 24)
    _bank_name.SufixText = "Sufix Text"
    _bank_name.SufixTextVisible = True
    _bank_name.TabIndex = 2
    _bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    _bank_name.TextValue = ""
    _bank_name.TextWidth = 115
    _bank_name.Value = ""
    '
    '_efective_days
    '
    _efective_days.DoubleValue = 0
    _efective_days.IntegerValue = 0
    _efective_days.IsReadOnly = False
    _efective_days.Location = New System.Drawing.Point(_left_margin, 90)
    _efective_days.Name = "_efective_days"
    _efective_days.Size = New System.Drawing.Size(270, 24)
    _efective_days.SufixText = "Sufix Text"
    _efective_days.SufixTextVisible = True
    _efective_days.TabIndex = 3
    _efective_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    _efective_days.TextValue = ""
    _efective_days.TextWidth = 115
    _efective_days.Value = ""
    '
    '_payment_type
    '
    _payment_type.DoubleValue = 0
    _payment_type.IntegerValue = 0
    _payment_type.IsReadOnly = False
    _payment_type.Location = New System.Drawing.Point(_left_margin, 120)
    _payment_type.Name = "_payment_type"
    _payment_type.Size = New System.Drawing.Size(270, 24)
    _payment_type.SufixText = "Sufix Text"
    _payment_type.SufixTextVisible = True
    _payment_type.TabIndex = 4
    _payment_type.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    _payment_type.TextValue = ""
    _payment_type.TextWidth = 115
    _payment_type.Value = ""
    '
    '_customer_number
    '
    _customer_number.DoubleValue = 0
    _customer_number.IntegerValue = 0
    _customer_number.IsReadOnly = False
    _customer_number.Location = New System.Drawing.Point(_left_margin, 150)
    _customer_number.Name = "_customer_number"
    _customer_number.Size = New System.Drawing.Size(270, 24)
    _customer_number.SufixText = "Sufix Text"
    _customer_number.SufixTextVisible = True
    _customer_number.TabIndex = 5
    _customer_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    _customer_number.TextValue = ""
    _customer_number.TextWidth = 115
    _customer_number.Value = ""
    '
    '_account_number
    '
    _account_number.DoubleValue = 0
    _account_number.IntegerValue = 0
    _account_number.IsReadOnly = False
    _account_number.Location = New System.Drawing.Point(_left_margin, 180)
    _account_number.Name = "_account_number"
    _account_number.Size = New System.Drawing.Size(340, 24)
    _account_number.SufixText = "Sufix Text"
    _account_number.SufixTextVisible = True
    _account_number.TabIndex = 6
    _account_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    _account_number.TextValue = ""
    _account_number.TextWidth = 115
    _account_number.Value = ""
    '
    '_alias_help
    '
    _alias_help.BackColor = System.Drawing.SystemColors.Control
    _alias_help.Font = New System.Drawing.Font("Courier New", 8.25!)
    _alias_help.ForeColor = System.Drawing.Color.Navy
    _alias_help.Location = New System.Drawing.Point(_left_margin + 275, _alias.Location.Y - 2)
    _alias_help.Name = "_alias_help"
    _alias_help.Size = New System.Drawing.Size(230, 30)
    _alias_help.TextAlign = ContentAlignment.TopLeft
    _alias_help.TabIndex = 20
    _alias_help.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1623)
    '
    '_efective_days_help
    '
    _efective_days_help.BackColor = System.Drawing.SystemColors.Control
    _efective_days_help.Font = New System.Drawing.Font("Courier New", 8.25!)
    _efective_days_help.ForeColor = System.Drawing.Color.Navy
    _efective_days_help.Location = New System.Drawing.Point(_left_margin + 275, _efective_days.Location.Y - 2)
    _efective_days_help.Name = "_alias_help"
    _efective_days_help.Size = New System.Drawing.Size(230, 30)
    _efective_days_help.TextAlign = ContentAlignment.TopLeft
    _efective_days_help.TabIndex = 21
    _efective_days_help.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1625)

  End Sub                 ' CreateTabPage

  ' PURPOSE: Set initial tabpage style
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub InitTabStyle(ByVal TabIndex As Integer)

    Dim _tab As TabPage
    Dim _bank_name As uc_entry_field
    Dim _alias As uc_entry_field
    Dim _efective_days As uc_entry_field
    Dim _customer_number As uc_entry_field
    Dim _account_number As uc_entry_field
    Dim _payment_type As uc_entry_field
    Dim _enabled As CheckBox
    Dim _id As Label

    _tab = tab_bank_accounts.TabPages(TabIndex)

    _id = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ID)
    _bank_name = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME)
    _alias = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)
    _efective_days = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS)
    _customer_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER)
    _account_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER)
    _payment_type = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE)
    _enabled = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)

    _bank_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554)
    _alias.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1561)
    _efective_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1556)
    _customer_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1557)
    _account_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1558)
    _payment_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1595)
    _enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1560)

    _bank_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    _alias.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    _efective_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    _customer_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    _account_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    _payment_type.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

  End Sub                  ' InitTabStyle

  ' PURPOSE: Transfer data from one row in m_bank_accounts to one tabpage account
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub DataRowToTab(ByVal TabIndex As Integer)

    Dim _row As frm_base_sel.CLASS_DB_ROW
    Dim _tab As TabPage
    Dim _bank_name As uc_entry_field
    Dim _alias As uc_entry_field
    Dim _efective_days As uc_entry_field
    Dim _customer_number As uc_entry_field
    Dim _account_number As uc_entry_field
    Dim _payment_type As uc_entry_field
    Dim _enabled As CheckBox
    Dim _id As Label

    _row = New frm_base_sel.CLASS_DB_ROW(m_bank_accounts.Rows(TabIndex))
    _tab = tab_bank_accounts.TabPages(TabIndex)

    _id = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ID)
    _bank_name = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME)
    _alias = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)
    _efective_days = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS)
    _customer_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER)
    _account_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER)
    _payment_type = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE)
    _enabled = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)

    _id.Text = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.ID)
    _bank_name.TextValue = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME)
    _alias.TextValue = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)
    _efective_days.TextValue = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS)
    _customer_number.TextValue = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER)
    _account_number.TextValue = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER)
    _payment_type.TextValue = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE)
    _enabled.Checked = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)

    _tab.Text = _alias.TextValue

  End Sub                  ' DataRowToTab

  ' PURPOSE: Execute when Button_Ok is clicked
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub OkButtonClick()

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to cahnge the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call IsChangedValues(True)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      If Me.CloseOnOkClick Then
        m_ignore_changes = True
        Call Me.Close()
      End If
    End If

  End Sub                 ' OkButtonClick


  ' PURPOSE : Add tabpage to create new account
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS : 
  Private Sub AddNewTab()

    ' ICS 08-FEB-2013: Check if it can create more tabs
    If tab_bank_accounts.TabPages.Count >= MAX_TABS Then
      Return
    End If

    m_newtabs_counter += 1
    Call CreateTabPage(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1594) & " " & m_newtabs_counter)
    Call InitTabStyle(tab_bank_accounts.TabPages.Count - 1)
    tab_bank_accounts.SelectedIndex = tab_bank_accounts.TabPages.Count - 1
    Call tab_bank_accounts.Refresh()
    '11-MAR-2015 FOS
    btn_deleteAccount.Enabled = True
  End Sub                     ' AddNewTab

  ' PURPOSE : Delete tabpage, only for not saved tabpage
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS : 
  Private Sub DeleteTab()

    Dim _tab_idx As Integer

    _tab_idx = tab_bank_accounts.SelectedIndex

    Call tab_bank_accounts.TabPages.RemoveAt(_tab_idx)
    If tab_bank_accounts.TabPages.Count > 0 And _tab_idx - 1 < tab_bank_accounts.TabPages.Count Then
      If _tab_idx > 0 Then
        Call tab_bank_accounts.SelectTab(_tab_idx - 1)
      Else
        Call tab_bank_accounts.SelectTab(0)
      End If
    End If

  End Sub                     ' DeleteTab

  ' PURPOSE : Set Payment Order config data to controls
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS : 
  Private Sub LoadParams()

    Call PAYMENT_ORDER_CONFIGURATION.LoadPaymentOrderConfig(m_params_order)

    chk_enabled.Checked = m_params_order.Enabled
    ef_min_amount.Value = m_params_order.MinAmount.ToString()
    ef_doc_title.TextValue = m_params_order.DocumentTitle
    ef_authorit1_title.TextValue = m_params_order.Authorization1Title
    ef_authorit1_name.TextValue = m_params_order.Authorization1Name
    ef_authorit2_title.TextValue = m_params_order.Authorization2Title
    ef_authorit2_name.TextValue = m_params_order.Authorization2Name
    chk_authorit1.Checked = m_params_order.Authorization1Required
    chk_authorit2.Checked = m_params_order.Authorization2Required
    ef_reference.TextValue = m_params_order.Reference
    ef_editable_minutes.TextValue = GUI_FormatNumber(m_params_order.EditableMinutes, 0)
    m_img_logo = m_params_order.Logo
    Me.pnl_logo.BackgroundImage = m_params_order.Logo
    Me.pnl_logo.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom
    '11-MAR-2015 FOS
    chk_PaymentDocument.Checked = m_params_order.IsPaymentDocument


  End Sub                    ' LoadParams

  ' PURPOSE : Begin croping logo image
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  Private Sub BeginningCropScale()

    m_x = 0
    m_y = 0
    m_dx = 0
    m_dy = 0
    m_scale = 1
    m_with = m_img_logo.Width
    m_height = m_img_logo.Height

    m_old_logo = pnl_logo.BackgroundImage
    pnl_logo.BackgroundImage = Nothing
    pnl_logo.Cursor = Windows.Forms.Cursors.NoMove2D
    btn_scale_up.Enabled = True
    btn_scale_down.Enabled = True
    m_cropping_area = True

    AddHandler pnl_logo.Paint, AddressOf pnl_logo_Paint

    AddHandler pnl_logo.MouseDown, AddressOf pnl_logo_MouseDown
    AddHandler pnl_logo.MouseMove, AddressOf pnl_logo_MouseMove
    AddHandler pnl_logo.MouseUp, AddressOf pnl_logo_MouseUp

  End Sub            ' BeginningCropScale

  ' PURPOSE : End croping logo image
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  Private Sub EndingCropScale()

    m_cropping_area = False
    btn_scale_up.Enabled = False
    btn_scale_down.Enabled = False

    pnl_logo.Cursor = Windows.Forms.Cursors.Default
    btn_select_img.Enabled = True

    RemoveHandler pnl_logo.Paint, AddressOf pnl_logo_Paint

    RemoveHandler pnl_logo.MouseDown, AddressOf pnl_logo_MouseDown
    RemoveHandler pnl_logo.MouseMove, AddressOf pnl_logo_MouseMove
    RemoveHandler pnl_logo.MouseUp, AddressOf pnl_logo_MouseUp

  End Sub               ' EndingCropScale

  ' PURPOSE : Set new logo property accord to image control dimensions
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  Private Sub AcceptLogo()

    Dim _target_img As Bitmap
    Dim _target_gr As Graphics
    Dim _mm As New IO.MemoryStream
    Dim _ratio As Double

    If IsNothing(m_img_logo) Then Return

    If m_cropping_area Then

      Call EndingCropScale()

    ElseIf Not (pnl_logo.Width < m_img_logo.Width Or pnl_logo.Height < m_img_logo.Height) Then

      m_x = (pnl_logo.Width - m_img_logo.Width) / 2
      m_y = (pnl_logo.Height - m_img_logo.Height) / 2
      m_scale = 1
      m_with = m_img_logo.Width
      m_height = m_img_logo.Height

    End If

    _ratio = 1

    _target_img = New Bitmap(pnl_logo.Width * _ratio, pnl_logo.Height * _ratio, m_img_logo.PixelFormat)
    _target_img.SetResolution(90, 90)
    _target_gr = Graphics.FromImage(_target_img)

    _target_gr.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
    _target_gr.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    _target_gr.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
    _target_gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

    _target_gr.Clear(Color.White)

    _target_gr.DrawImage(m_img_logo, _
                        Convert.ToInt32(m_x * _ratio), Convert.ToInt32(m_y * _ratio), _
                        Convert.ToInt32(m_with * m_scale * _ratio), Convert.ToInt32(m_height * m_scale * _ratio))

    Dim _info As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
    Dim _encoderParameters As Imaging.EncoderParameters
    Dim _encoder As ImageCodecInfo

    _encoderParameters = New Imaging.EncoderParameters(1)
    _encoderParameters.Param(0) = New Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L)

    For Each _encoder In ImageCodecInfo.GetImageEncoders()

      If _encoder.FormatID = ImageFormat.Png.Guid Then
        Exit For
      End If

    Next

    _target_img.Save(_mm, _info(4), _encoderParameters)
    _target_img.Dispose()
    _target_gr.Dispose()

    Me.pnl_logo.BackgroundImage = Image.FromStream(_mm)
    Me.pnl_logo.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom
    Me.pnl_logo.Refresh()

  End Sub                    ' AcceptLogo

  ' PURPOSE : Restore logo property to old logo
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  Private Sub CancelLogo()

    If m_cropping_area Then

      Call EndingCropScale()

      Me.pnl_logo.BackgroundImage = m_old_logo
      Call pnl_logo.Refresh()

    End If

  End Sub                    ' Cancel Logo

  ' PURPOSE : Validate the data presented on one tabpage account.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : Boolean
  Private Function TabIsScreenDataOk(ByVal TabIndex As Integer, ByVal PaymentOrdersEnabled As Boolean) As Boolean

    Dim _tab As TabPage
    Dim _bank_name As uc_entry_field
    Dim _alias As uc_entry_field
    Dim _efective_days As uc_entry_field
    Dim _customer_number As uc_entry_field
    Dim _account_number As uc_entry_field
    Dim _payment_type As uc_entry_field
    Dim _enabled_tab As CheckBox

    _tab = tab_bank_accounts.TabPages(TabIndex)

    _alias = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)

    If String.IsNullOrEmpty(_alias.TextValue) Then
      Call tab_bank_accounts.SelectTab(_tab)
      Call _alias.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _alias.Text)

      Return False
    End If

    _enabled_tab = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)

    If PaymentOrdersEnabled And _enabled_tab.Checked Then

      _bank_name = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME)
      _efective_days = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS)
      _customer_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER)
      _account_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER)
      _payment_type = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE)

      If String.IsNullOrEmpty(_bank_name.TextValue) Then
        Call tab_bank_accounts.SelectTab(_tab)
        Call _bank_name.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _bank_name.Text)

        Return False
      End If

      If String.IsNullOrEmpty(_efective_days.TextValue) Then
        Call tab_bank_accounts.SelectTab(_tab)
        Call _efective_days.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _efective_days.Text)

        Return False
      End If

      If String.IsNullOrEmpty(_customer_number.TextValue) Then
        Call tab_bank_accounts.SelectTab(_tab)
        Call _customer_number.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _customer_number.Text)

        Return False
      End If

      If String.IsNullOrEmpty(_account_number.TextValue) Then
        Call tab_bank_accounts.SelectTab(_tab)
        Call _account_number.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _account_number.Text)

        Return False
      End If

      If String.IsNullOrEmpty(_payment_type.TextValue) Then
        Call tab_bank_accounts.SelectTab(_tab)
        Call _payment_type.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _payment_type.Text)

        Return False
      End If

    End If

    Return True

  End Function        ' TabIsScreenDataOk

  ' PURPOSE : Validate if there is at least one enabled bank account.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : Boolean
  Private Function ThereIsAccountEnabled() As Boolean

    Dim _enabled_tab As CheckBox
    Dim _tab As TabPage
    Dim _idf As Integer

    For _idf = 0 To tab_bank_accounts.TabPages.Count - 1
      _tab = tab_bank_accounts.TabPages(_idf)
      _enabled_tab = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)
      If _enabled_tab.Checked Then

        Return True
      End If
    Next

    Return False

  End Function

  ' PURPOSE : Check allowed documents list and truncate the name if exceed MAX_LENGHT_DOCUMENT_TYPE characters
  '
  '  PARAMS :
  '     - INPUT : TypesList = list of values separated by SPLIT_CHAR_DOCUMENT_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : new allowed documents list
  Private Function TruncateAllowedDocument(ByVal TypesList As String) As String

    Dim _str_list As String = ""
    Dim _char As String = ""

    For Each _str As String In TypesList.Split(New String() {SPLIT_CHAR_DOCUMENT_TYPE}, StringSplitOptions.RemoveEmptyEntries)

      _str = _str.Trim()
      If _str.Length > 0 Then

        If _str.Length > MAX_LENGHT_DOCUMENT_TYPE Then
          _str = _str.Remove(MAX_LENGHT_DOCUMENT_TYPE)
        End If

        _str_list = String.Concat(_str_list, _char, _str)
        _char = SPLIT_CHAR_DOCUMENT_TYPE

      End If

    Next

    Return _str_list

  End Function  ' TruncateAllowedDocument

  ' PURPOSE : Check if changed and save values except groupbox accounts data
  '
  '  PARAMS :
  '     - INPUT : Save = indicate if must save changed data
  '
  '     - OUTPUT : 
  '
  ' RETURNS : Boolean
  Private Function IsChangedParams(ByVal Save As Boolean) As Boolean

    Dim _changed As Boolean
    Dim _edit_params As PAYMENT_ORDER_CONFIGURATION

    _changed = False



    _edit_params = New PAYMENT_ORDER_CONFIGURATION()
    _edit_params.Enabled = chk_enabled.Checked
    If Not Double.TryParse(GUI_ParseCurrency(ef_min_amount.Value), _edit_params.MinAmount) Then
      _edit_params.MinAmount = 0
    End If
    If Not Integer.TryParse(ef_editable_minutes.Value, _edit_params.EditableMinutes) Then
      _edit_params.EditableMinutes = 0
    End If
    _edit_params.DocumentTitle = ef_doc_title.TextValue
    _edit_params.Reference = ef_reference.TextValue
    _edit_params.Authorization1Title = ef_authorit1_title.TextValue
    _edit_params.Authorization1Name = ef_authorit1_name.TextValue
    _edit_params.Authorization1Required = chk_authorit1.Checked
    _edit_params.Authorization2Title = ef_authorit2_title.TextValue
    _edit_params.Authorization2Name = ef_authorit2_name.TextValue
    _edit_params.Authorization2Required = chk_authorit2.Checked

    Call GetDataFromDocuments(_edit_params)

    If m_img_changed Then
      Call Me.AcceptLogo()
      _edit_params.Logo = pnl_logo.BackgroundImage
    Else
      _edit_params.Logo = m_params_order.Logo
    End If
    '11-MAR-2015 FOS
    _edit_params.IsPaymentDocument = chk_PaymentDocument.Checked

    _changed = Not _edit_params.AuditorData.IsEqual(m_params_order.AuditorData)

    If _changed And Save Then
      PAYMENT_ORDER_CONFIGURATION.SavePaymentOrderConfig(_edit_params)
      _edit_params.AuditorData.Notify(CurrentUser.GuiId, _
                                      CurrentUser.Id, _
                                      CurrentUser.Name, _
                                      CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                      0, _
                                      m_params_order.AuditorData)
    End If

    Return _changed

  End Function          ' IsChangedParams

  ' PURPOSE : Check if changed and save of one tabpage account values
  '
  '  PARAMS :
  '     - INPUT :   TabIndex = index of tab to ckeck
  '                 Save = indicate if must save changed data
  '
  '     - OUTPUT : 
  '
  ' RETURNS : Boolean
  Private Function IsChangedTab(ByVal TabIndex As Integer, ByVal Save As Boolean) As Boolean

    Dim _read_ba As CLASS_BANK_ACCOUNT
    Dim _edit_ba As CLASS_BANK_ACCOUNT
    Dim _row As frm_base_sel.CLASS_DB_ROW
    Dim _tab As TabPage
    Dim _bank_name As uc_entry_field
    Dim _description As uc_entry_field
    Dim _efective_days As uc_entry_field
    Dim _customer_number As uc_entry_field
    Dim _account_number As uc_entry_field
    Dim _payment_type As uc_entry_field
    Dim _enabled As CheckBox
    Dim _id As Label
    Dim _changed As Boolean
    Dim _audit_op As CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS

    _tab = tab_bank_accounts.TabPages(TabIndex)
    _read_ba = New CLASS_BANK_ACCOUNT()
    _edit_ba = New CLASS_BANK_ACCOUNT()
    _changed = False

    _id = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ID)
    _bank_name = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME)
    _description = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)
    _efective_days = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS)
    _customer_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER)
    _account_number = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER)
    _payment_type = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE)
    _enabled = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)

    _edit_ba.Id = CInt(_id.Text)
    _edit_ba.BankName = _bank_name.TextValue
    _edit_ba.Description = _description.TextValue
    _edit_ba.EfectiveDays = _efective_days.TextValue
    _edit_ba.CustomerNumber = _customer_number.TextValue
    _edit_ba.AccountNumber = _account_number.TextValue
    _edit_ba.PaymentType = _payment_type.TextValue
    _edit_ba.Enabled = _enabled.Checked

    If _edit_ba.Id <> 0 Then

      _row = New frm_base_sel.CLASS_DB_ROW(m_bank_accounts.Rows(TabIndex))

      _read_ba.Id = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.ID)
      _read_ba.BankName = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME)
      _read_ba.Description = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)
      _read_ba.EfectiveDays = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS)
      _read_ba.CustomerNumber = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER)
      _read_ba.AccountNumber = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER)
      _read_ba.PaymentType = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE)
      _read_ba.Enabled = _row.Value(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED)

      _changed = Not _edit_ba.AuditorData.IsEqual(_read_ba.AuditorData)

      If _changed And Save Then
        _audit_op = CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE
      End If

    End If

    If _edit_ba.Id = 0 And Save Then
      _changed = True
      _audit_op = CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT
    End If

    If _changed And Save Then
      SaveTab(TabIndex, _edit_ba)
      _edit_ba.AuditorData.Notify(CurrentUser.GuiId, _
                                      CurrentUser.Id, _
                                      CurrentUser.Name, _
                                      _audit_op, _
                                      0, _
                                      _read_ba.AuditorData)
    End If

    Return _changed

  End Function             ' IsChangedTab

  ' PURPOSE : Check if changed and save values
  '
  '  PARAMS :
  '     - INPUT : Save = indicate if must save changed data
  '
  '     - OUTPUT : 
  '
  ' RETURNS : Boolean
  Private Function IsChangedValues(ByVal Save As Boolean) As Boolean

    Dim _idf As Integer
    Dim _changed As Boolean

    _changed = IsChangedParams(Save)

    For _idf = 0 To tab_bank_accounts.TabPages.Count - 1
      _changed = _changed Or IsChangedTab(_idf, Save)
    Next

    Return _changed

  End Function          ' IsChangedValues 

  '----------------------------------------------------------------------------
  ' PURPOSE: Inserts or updates an bank_account object from the database, 
  '          depending on the property Id of this same instance. 
  '
  ' PARAMS:
  '   - INPUT: Trx As SqlTransaction
  '
  '   - OUTPUT: None
  '
  ' RETURNS:
  '     - ENUM_STATUS
  '
  ' NOTES:
  Private Function SaveTab(ByVal TabIndex As Integer, ByRef BankAccount As CLASS_BANK_ACCOUNT) As ENUM_STATUS

    Dim _rc As ENUM_STATUS
    Dim _row As DataRow

    _rc = BankAccount.DB_Update(0)

    If TabIndex >= m_bank_accounts.Rows.Count Then
      _row = m_bank_accounts.NewRow()
      m_bank_accounts.Rows.Add(_row)
    Else
      _row = m_bank_accounts.Rows(TabIndex)
    End If

    If _rc = ENUM_STATUS.STATUS_OK Then

      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.ID) = BankAccount.Id
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.BANK_NAME) = BankAccount.BankName
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION) = BankAccount.Description
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.EFECTIVE_DAYS) = BankAccount.EfectiveDays
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.CUSTOMER_NUMBER) = BankAccount.CustomerNumber
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.ACCOUNT_NUMBER) = BankAccount.AccountNumber
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.PAYMENT_TYPE) = BankAccount.PaymentType
      _row(CLASS_BANK_ACCOUNT.BA_MEMBERS.ENABLED) = BankAccount.Enabled

    End If

    Return _rc

  End Function                  ' SaveTab

  ' PURPOSE : Discard changes on closing
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS : Boolean
  Private Function DiscardChanges() As Boolean

    If Not m_ignore_changes Then
      If IsChangedValues(False) Then
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return False
        End If
      End If
    End If

    Return True

  End Function           ' DiscardChanges

  ' PURPOSE : Validate the document types data.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '      TRUE : if doccument types are ok
  '      FALSE : if not
  Private Function DocTypesIsDataOk()

    Dim _has_error As Boolean

    _has_error = True


    With Me.dg_identification_documents
      For _idx_row As Integer = 0 To .NumRows - 1
        If GridValuetoBool(.Cell(_idx_row, GRID_COLUMN_PRINCIPAL).Value) Then
          _has_error = False
        End If
      Next
    End With

    If _has_error Then
      Call Me.dg_identification_documents.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5180), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If
    Return True

  End Function         ' DocTypesIsDataOk

  ' PURPOSE : Validate the data presented on the screen except accounts groupbox data.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '      TRUE : if all the params are ok
  '      FALSE : if not
  Private Function ParamsIsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(ef_min_amount.TextValue) Then
      Call ef_min_amount.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_min_amount.Text)
      Return False
    End If

    If GUI_ParseCurrency(ef_min_amount.TextValue) < 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_min_amount.Text)
      Return False
    End If

    If String.IsNullOrEmpty(ef_editable_minutes.TextValue) Then
      Call ef_editable_minutes.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_editable_minutes.Text)
      Return False
    End If

    If ef_editable_minutes.Value > TIME_LIMIT_EDITABLE_ORDER Then
      Call ef_editable_minutes.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(338), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _
                      Me.ef_editable_minutes.Text, _
                      GUI_FormatNumber(TIME_LIMIT_EDITABLE_ORDER, 0) _
                      )
      Return False
    End If

    If Not DocTypesIsDataOk() Then
      Return False
    End If

    If Not chk_authorit1.Checked And Not chk_authorit2.Checked Then
      Call chk_authorit1.Focus()
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1630), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If

    If chk_authorit1.Checked Then
      If String.IsNullOrEmpty(ef_authorit1_title.TextValue) Then
        Call ef_authorit1_title.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_authorit1_title.Text)
        Return False
      End If
      If String.IsNullOrEmpty(ef_authorit1_name.TextValue) Then
        Call ef_authorit1_name.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_authorit1_name.Text)
        Return False
      End If
    End If

    If chk_authorit2.Checked Then
      If String.IsNullOrEmpty(ef_authorit2_title.TextValue) Then
        Call ef_authorit2_title.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_authorit2_title.Text)
        Return False
      End If
      If String.IsNullOrEmpty(ef_authorit2_name.TextValue) Then
        Call ef_authorit2_name.Focus()
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_authorit2_name.Text)
        Return False
      End If
    End If

    Return ValidateFormat()

  End Function     ' ParamsIsScreenDataOk

  ' PURPOSE : Check if exist duplicated bank account alias
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS : Boolean
  Private Function DuplicatedAlias() As Boolean

    Dim _alias As uc_entry_field
    Dim _all_alias As List(Of String)

    _all_alias = New List(Of String)
    For Each _tab As TabPage In tab_bank_accounts.TabPages

      _alias = _tab.Controls(CLASS_BANK_ACCOUNT.BA_MEMBERS.DESCRIPTION)
      If _all_alias.Contains(_alias.TextValue) Then
        Call tab_bank_accounts.SelectTab(_tab)
        Call _alias.Focus()
        Return True
      Else
        _all_alias.Add(_alias.TextValue)
      End If

    Next

    Return False

  End Function          ' DuplicateAlias


  ' PURPOSE:  Grid configuration
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid: Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetDocuments()

    With Me.dg_identification_documents
      Call .Init(5, 1, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = GRID_COLUMN_WIDTH_SENTINEL
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False
      .Column(GRID_COLUMN_SENTINEL).Editable = False

      ' GRID ID
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      ' GRID NAME
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1565) '"Documento"
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Width = GRID_COLUMN_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Editable = False

      ' GRID VISIBLE
      .Column(GRID_COLUMN_PRINCIPAL).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(373) '"Primario"
      .Column(GRID_COLUMN_PRINCIPAL).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PRINCIPAL).Width = GRID_COLUMN_WIDTH_VISIBLE
      .Column(GRID_COLUMN_PRINCIPAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PRINCIPAL).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_PRINCIPAL).Editable = True

      ' GRID REQUIRED
      .Column(GRID_COLUMN_SECONDARY).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(374) '"Secundario"
      .Column(GRID_COLUMN_SECONDARY).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SECONDARY).Width = GRID_COLUMN_WIDTH_REQUIRED
      .Column(GRID_COLUMN_SECONDARY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SECONDARY).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_SECONDARY).Editable = True

    End With
  End Sub

  ' PURPOSE:  Fill Documents grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid: Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub PopulateIdentificationTypes(ByVal Data As PAYMENT_ORDER_CONFIGURATION)

    Dim _idx_row As Integer = 0
    Dim _string_code As Integer = 0

    For _idx_ As Integer = 0 To Data.IdentificationTypes.Rows.Count - 1
      Me.dg_identification_documents.AddRow()
    Next

    For Each _dr As DataRow In Data.IdentificationTypes.Rows
      With Me.dg_identification_documents

        .Cell(_idx_row, GRID_COLUMN_ID).Value = Convert.ToInt16(_dr(SQL_DT_COLUMN_ID)).ToString("000")
        .Cell(_idx_row, GRID_COLUMN_NAME).Value = _dr(SQL_DT_COLUMN_NAME)
        .Cell(_idx_row, GRID_COLUMN_PRINCIPAL).Value = BoolToGridValue(_dr(SQL_DT_COLUMN_ENABLED))

        _string_code = _dr(SQL_DT_COLUMN_ID).ToString()

        If Data.AllowedDocuments1.Contains(_string_code.ToString("000")) Then
          .Cell(_idx_row, GRID_COLUMN_PRINCIPAL).Value = BoolToGridValue(True)
        Else
          .Cell(_idx_row, GRID_COLUMN_PRINCIPAL).Value = BoolToGridValue(False)
        End If

        If Data.AllowedDocuments2.Contains(_string_code.ToString("000")) Then
          .Cell(_idx_row, GRID_COLUMN_SECONDARY).Value = BoolToGridValue(True)
        Else
          .Cell(_idx_row, GRID_COLUMN_SECONDARY).Value = BoolToGridValue(False)
        End If

      End With

      _idx_row += 1
    Next

  End Sub


  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue

  ' PURPOSE:  Cast from  uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValuetoBool(ByVal enabled As String) As Boolean
    If enabled = uc_grid.GRID_CHK_CHECKED Or enabled = uc_grid.GRID_CHK_CHECKED_DISABLED Then
      Return True
    Else
      Return False
    End If
  End Function 'BoolToGridValue


  ' PURPOSE: Get All the info from Documents grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetDataFromDocuments(ByRef Data As PAYMENT_ORDER_CONFIGURATION)
    Dim _types_principal As List(Of String)
    Dim _types_secondary As List(Of String)

    _types_principal = New List(Of String)
    _types_secondary = New List(Of String)

    With Me.dg_identification_documents
      For _idx_row As Integer = 0 To .NumRows - 1
        If GridValuetoBool(.Cell(_idx_row, GRID_COLUMN_PRINCIPAL).Value) Then

          _types_principal.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If

        'PRINCIPAL
        If GridValuetoBool(.Cell(_idx_row, GRID_COLUMN_SECONDARY).Value) Then

          _types_secondary.Add(.Cell(_idx_row, GRID_COLUMN_ID).Value)
        End If
      Next
    End With

    If _types_principal.Count > 0 Then
      Data.AllowedDocuments1 = String.Join(",", _types_principal.ToArray())
    Else
      Data.AllowedDocuments1 = String.Empty
    End If

    If _types_secondary.Count > 0 Then
      Data.AllowedDocuments2 = String.Join(",", _types_secondary.ToArray())
    Else
      Data.AllowedDocuments2 = String.Empty
    End If

  End Sub

  '11-MAR-2015 FOS
  ' PURPOSE: Enable controls if generate Document is Active
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableControls()

    gb_documents.Enabled = True
    btn_addAccount.Enabled = Me.Permissions.Write
    If Not m_bank_accounts Is Nothing Then
      Call CheckDeleteButtonStatus()
    End If

  End Sub '  EnableControls

  ' PURPOSE: Disable controls if generate Document is inactive
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DisableControls()

    gb_documents.Enabled = False
    btn_addAccount.Enabled = False
    btn_deleteAccount.Enabled = False

  End Sub ' DisableControls

#End Region  ' Privates

#Region " Publics "

  Public Function ValidateFormat() As Boolean

    If Not ef_authorit1_title.IsReadOnly And Not ef_authorit1_title.ValidateFormat() Then
      Call ef_authorit1_title.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_authorit1_title.Text)
      Return False
    End If

    If Not ef_authorit1_name.IsReadOnly And Not ef_authorit1_name.ValidateFormat() Then
      Call ef_authorit1_name.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_authorit1_name.Text)
      Return False
    End If

    If Not ef_authorit2_title.IsReadOnly And Not ef_authorit2_title.ValidateFormat() Then
      Call ef_authorit2_title.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_authorit2_title.Text)
      Return False
    End If

    If Not ef_authorit2_name.IsReadOnly And Not ef_authorit2_name.ValidateFormat() Then
      Call ef_authorit2_name.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_authorit2_name.Text)
      Return False
    End If

    Return True

  End Function




  ' PURPOSE : Display form to edit config params
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  Public Sub ShowForConfig()

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.Display(True)

  End Sub  ' ShowForConfig

  ' PURPOSE : Creation actions
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT : 
  '
  ' RETURNS :
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    m_ignore_changes = False
    m_newtabs_counter = 0

  End Sub            ' New

#End Region   ' Publics

#Region " Events "

  Private Sub frm_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseWheel
    If m_cropping_area AndAlso m_enter_panel_logo Then
      If e.Delta < 0 Then
        m_scale -= SCALE_CONSTANT1
        pnl_logo.Refresh()
      Else
        m_scale += SCALE_CONSTANT1
        pnl_logo.Refresh()
      End If
    End If
  End Sub


  Private Sub tab_bank_accounts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_bank_accounts.SelectedIndexChanged
    Call CheckDeleteButtonStatus()
  End Sub

  ' PURPOSE: Check status of  General param "generate document"
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CheckGenerateDocumentStatus()

    If Not chk_PaymentDocument.Checked Then
      Call DisableControls()
      Call CheckDeleteButtonStatus()
    End If

  End Sub  ' Check_Generate_Document

  ' PURPOSE: Check if Enabled Payment Order
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CheckIsEnabledPaymentOrder()
    If Not chk_enabled.Checked Then
      Call DisableControls()
      chk_PaymentDocument.Enabled = False
      ef_min_amount.Enabled = False
    Else
      If Not chk_PaymentDocument.Checked Then
        Call DisableControls()
      Else
        Call EnableControls()
      End If
      ef_min_amount.Enabled = True
      chk_PaymentDocument.Enabled = True
    End If
  End Sub 'Check_Enabled_Payment_Order

  Private Sub chk_doc2_require_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'ef_doc_types2.IsReadOnly = Not chk_doc2_require.Checked
    'If Not chk_doc2_require.Checked Then
    '  ef_doc_types2.TextValue = ""
    'End If
  End Sub


  Private Sub pnl_logo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
    m_moving_btn = True
    m_dx = e.X - m_x
    m_dy = e.Y - m_y
  End Sub

  Private Sub pnl_logo_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
    If m_moving_btn Then
      m_x = e.X - m_dx
      m_y = e.Y - m_dy
      pnl_logo.Refresh()
    End If
  End Sub

  Private Sub pnl_logo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
    m_moving_btn = False
    m_x = e.X - m_dx
    m_y = e.Y - m_dy
  End Sub

  Private Sub pnl_logo_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
    m_enter_panel_logo = True
  End Sub

  Private Sub pnl_logo_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
    m_enter_panel_logo = False
  End Sub

  Private Sub pnl_logo_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
    e.Graphics.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    e.Graphics.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
    e.Graphics.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

    e.Graphics.DrawImage(m_img_logo, m_x, m_y, Convert.ToInt32(m_with * m_scale), Convert.ToInt32(m_height * m_scale))

  End Sub

  Private Sub btn_scale_up_ClickEvent() Handles btn_scale_up.ClickEvent
    m_scale += SCALE_CONSTANT2
    pnl_logo.Refresh()
  End Sub

  Private Sub btn_scale_down_ClickEvent() Handles btn_scale_down.ClickEvent
    m_scale -= SCALE_CONSTANT2
    pnl_logo.Refresh()
  End Sub

  Private Sub chk_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enabled.CheckedChanged
    Call CheckIsEnabledPaymentOrder()
  End Sub

  Private Sub btn_custom_3_ClickEvent() Handles btn_deleteAccount.ClickEvent
    Call DeleteTab()
  End Sub

  Private Sub btn_custom_4_ClickEvent() Handles btn_addAccount.ClickEvent
    Call AddNewTab()
    Call CheckDeleteButtonStatus()
  End Sub

  Private Sub btn_select_img_ClickEvent() Handles btn_select_img.ClickEvent
    Call LoadImage()
  End Sub

  Private Sub chk_PaymentDocument_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_PaymentDocument.CheckedChanged
    If chk_PaymentDocument.Checked Then
      Call EnableControls()
    Else
      Call DisableControls()
    End If
  End Sub
#End Region
  ' lmrs 08/10/2014 when click over a checkbox change the state for the other check
  Private Sub dg_identification_documents_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

    If Column = GRID_COLUMN_PRINCIPAL Then

      If Not CBool(dg_identification_documents.Cell(Row, GRID_COLUMN_PRINCIPAL).Value) Then
        dg_identification_documents.Cell(Row, GRID_COLUMN_SECONDARY).Value = BoolToGridValue((dg_identification_documents.Cell(Row, GRID_COLUMN_PRINCIPAL).Value))
      End If

    ElseIf Column = GRID_COLUMN_SECONDARY Then

      If Not CBool(dg_identification_documents.Cell(Row, GRID_COLUMN_SECONDARY).Value) Then
        dg_identification_documents.Cell(Row, GRID_COLUMN_PRINCIPAL).Value = BoolToGridValue((dg_identification_documents.Cell(Row, GRID_COLUMN_SECONDARY).Value))
      End If

    End If


  End Sub    ' Events

  '----------------------------------------------------------------------------
  ' PURPOSE : Load Image at Form
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  ' NOTES :
  Private Sub LoadImage()
    Dim _filename As String
    Dim _open_file As System.Windows.Forms.OpenFileDialog
    Dim _mm As New IO.MemoryStream

    _open_file = New System.Windows.Forms.OpenFileDialog

    Try

      If Not String.IsNullOrEmpty(m_current_directory) Then
        _open_file.InitialDirectory = m_current_directory
      End If

      _open_file.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(662)
      _open_file.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(663)
      If _open_file.ShowDialog() <> Windows.Forms.DialogResult.OK Then
        Return
      End If

      _filename = _open_file.FileName
      m_current_directory = System.IO.Path.GetDirectoryName(_filename)
      m_img_changed = True

      Using _img_tmp1 As Image = System.Drawing.Image.FromFile(_filename)
        m_img_logo = New Bitmap(_img_tmp1)
      End Using

      If m_img_logo.Width > pnl_logo.Width Or m_img_logo.Height > pnl_logo.Height Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(664))
        Call BeginningCropScale()
      Else
        If Me.m_cropping_area Then
          Call EndingCropScale()
        End If

        Dim _info As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
        Dim _encoderParameters As Imaging.EncoderParameters
        Dim _encoder As ImageCodecInfo

        _encoderParameters = New Imaging.EncoderParameters(1)
        _encoderParameters.Param(0) = New Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L)

        For Each _encoder In ImageCodecInfo.GetImageEncoders()

          If _encoder.FormatID = ImageFormat.Png.Guid Then
            Exit For
          End If

        Next

        m_img_logo.Save(_mm, _info(4), _encoderParameters)

        Me.pnl_logo.BackgroundImage = Image.FromStream(_mm)
        Me.pnl_logo.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom


      End If
      pnl_logo.Refresh()

    Catch _ex As Exception
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1586), ENUM_MB_TYPE.MB_TYPE_ERROR)
      m_current_directory = ""
      m_img_changed = False
    End Try
  End Sub 'Load_Image

  '----------------------------------------------------------------------------
  ' PURPOSE : Disabled or Enabled Delete account button 
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  ' NOTES :
  Private Sub CheckDeleteButtonStatus()
    Dim _tab_idx As Integer

    _tab_idx = tab_bank_accounts.SelectedIndex

    If _tab_idx < 0 Or _tab_idx < m_bank_accounts.Rows.Count Then
      btn_deleteAccount.Enabled = False
    Else
      btn_deleteAccount.Enabled = True
    End If

  End Sub 'Delete_button_status

End Class

Public Class PAYMENT_ORDER_CONFIGURATION

#Region " Members "

  Public Enabled As Boolean
  Public DocumentTitle As String
  Public Reference As String
  Public MinAmount As Decimal
  Public EditableMinutes As Integer
  Public AllowedDocuments1 As String
  Public AllowedDocuments2 As String
  Public Authorization1Title As String
  Public Authorization1Name As String
  Public Authorization1Required As Boolean
  Public Authorization2Title As String
  Public Authorization2Name As String
  Public Authorization2Required As Boolean
  Public Logo As Image
  Public IdentificationTypes As DataTable
  Public IsPaymentDocument As Boolean

#End Region ' Members

#Region " Publics "
  Public Sub New()
    Me.IdentificationTypes = New DataTable()
  End Sub

  '----------------------------------------------------------------------------
  ' PURPOSE : Returns the related auditor data for the current object.
  '
  ' PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - The newly created audit object
  '
  ' NOTES :
  Public Function AuditorData() As CLASS_AUDITOR_DATA

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _document_type_list As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(1559), "")

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1602), GLB_NLS_GUI_AUDITOR.GetString(IIf(Me.Enabled, 336, 337)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1603), GUI_FormatCurrency(Me.MinAmount, 2, , True))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1582), Me.EditableMinutes & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1583))

    ' Types Visible
    _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(Me.AllowedDocuments1)
    If String.IsNullOrEmpty(_document_type_list) Then
      _document_type_list = AUDIT_NONE_STRING
    End If
    Call _auditor_data.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1604)) ' Visible

    ' Types Visible
    _document_type_list = WSI.Common.IdentificationTypes.DocIdTypeStringList(Me.AllowedDocuments2)
    If String.IsNullOrEmpty(_document_type_list) Then
      _document_type_list = AUDIT_NONE_STRING
    End If
    Call _auditor_data.SetField(0, _document_type_list, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1620)) ' Visible

    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1610), Me.DocumentTitle)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1626), Me.Reference)
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1611), GetInfoCRC(Me.Logo))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1616), GLB_NLS_GUI_AUDITOR.GetString(IIf(Me.Authorization1Required, 336, 337)))
    Call _auditor_data.SetField(0, Me.Authorization1Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1605, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613)))
    Call _auditor_data.SetField(0, Me.Authorization1Name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1605, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614)))
    Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(1617), GLB_NLS_GUI_AUDITOR.GetString(IIf(Me.Authorization2Required, 336, 337)))
    Call _auditor_data.SetField(0, Me.Authorization2Title, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1606, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1613)))
    Call _auditor_data.SetField(0, Me.Authorization2Name, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1606, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1614)))
    '11-MAR-2015 FOS
    If Me.IsPaymentDocument Then
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6035), "Habilitado")
    Else
      Call _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6035), "Desactivado")
    End If


    Return _auditor_data

  End Function                   ' AuditorData

  '----------------------------------------------------------------------------
  ' PURPOSE : Load Payment Order config from general params.
  '
  ' PARAMS :
  '     - INPUT :   None
  '     - OUTPUT :  OrderConfig = config info as PAYMENT_ORDER_CONFIGURATION class
  '
  ' RETURNS :
  '     - ENUM_STATUS
  '
  ' NOTES :
  Public Shared Function LoadPaymentOrderConfig(ByRef OrderConfig As PAYMENT_ORDER_CONFIGURATION) As ENUM_STATUS

    Dim _str_param As String
    Dim _general_params As WSI.Common.GeneralParam.Dictionary

    _str_param = ""
    OrderConfig = New PAYMENT_ORDER_CONFIGURATION()

    Try
      _general_params = WSI.Common.GeneralParam.Dictionary.Create()

      OrderConfig.Enabled = _general_params.GetBoolean("PaymentOrder", "Enabled", False)
      OrderConfig.MinAmount = _general_params.GetDecimal("PaymentOrder", "MinAmount", 0)
      OrderConfig.EditableMinutes = _general_params.GetInt32("PaymentOrder", "EditableMinutes", 0)
      OrderConfig.DocumentTitle = _general_params.GetString("PaymentOrder", "DocumentTitle")
      OrderConfig.Reference = _general_params.GetString("PaymentOrder", "DocReference")
      OrderConfig.AllowedDocuments1 = _general_params.GetString("PaymentOrder", "AllowedDocuments1")
      OrderConfig.AllowedDocuments2 = _general_params.GetString("PaymentOrder", "AllowedDocuments2")
      OrderConfig.Authorization1Title = _general_params.GetString("PaymentOrder", "Authorization1.DefaultTitle")
      OrderConfig.Authorization1Name = _general_params.GetString("PaymentOrder", "Authorization1.DefaultName")
      OrderConfig.Authorization1Required = _general_params.GetBoolean("PaymentOrder", "Authorization1.Required")
      OrderConfig.Authorization2Title = _general_params.GetString("PaymentOrder", "Authorization2.DefaultTitle")
      OrderConfig.Authorization2Name = _general_params.GetString("PaymentOrder", "Authorization2.DefaultName")
      OrderConfig.Authorization2Required = _general_params.GetBoolean("PaymentOrder", "Authorization2.Required")
      OrderConfig.IdentificationTypes = WSI.Common.IdentificationTypes.GetIdentificationTypes(False)
      '11-MAR-2015 FOS
      OrderConfig.IsPaymentDocument = _general_params.GetBoolean("PaymentOrder", "GenerateDocument", False)

      Using _trx As New DB_TRX()
        Call LoadPaymentOrderLogo(OrderConfig.Logo, _trx.SqlTransaction)
      End Using

      Return ENUM_STATUS.STATUS_OK

    Catch _ex As Exception

      Return ENUM_STATUS.STATUS_ERROR

    End Try

  End Function ' LoadPaymentOrderConfig

  '----------------------------------------------------------------------------
  ' PURPOSE : Save Payment Order config on general params.
  '
  ' PARAMS :
  '     - INPUT :   OrderConfig = config info as PAYMENT_ORDER_CONFIGURATION class
  '     - OUTPUT :  None
  '
  ' RETURNS :
  '     - ENUM_STATUS
  '
  ' NOTES :
  Public Shared Function SavePaymentOrderConfig(ByVal ParamsOrder As PAYMENT_ORDER_CONFIGURATION) As ENUM_STATUS

    Using _db_trx As New DB_TRX()
      Try
        Dim _gp_update_list As New List(Of String())

        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Enabled", IIf(ParamsOrder.Enabled, "1", "0")))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "MinAmount", GUI_LocalNumberToDBNumber(ParamsOrder.MinAmount.ToString())))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "EditableMinutes", ParamsOrder.EditableMinutes))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "DocumentTitle", ParamsOrder.DocumentTitle))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "DocReference", ParamsOrder.Reference))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "AllowedDocuments1", ParamsOrder.AllowedDocuments1))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "AllowedDocuments2", ParamsOrder.AllowedDocuments2))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Authorization1.DefaultTitle", ParamsOrder.Authorization1Title))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Authorization1.DefaultName", ParamsOrder.Authorization1Name))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Authorization1.Required", IIf(ParamsOrder.Authorization1Required, "1", "0")))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Authorization2.DefaultTitle", ParamsOrder.Authorization2Title))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Authorization2.DefaultName", ParamsOrder.Authorization2Name))
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "Authorization2.Required", IIf(ParamsOrder.Authorization2Required, "1", "0")))
        '11-MAR-2015 FOS
        _gp_update_list.Add(GeneralParam.AddToList("PaymentOrder", "GenerateDocument", IIf(ParamsOrder.IsPaymentDocument, "1", "0")))

        Call SavePaymentOrderLogo(ParamsOrder.Logo, _db_trx.SqlTransaction)

        For Each _general_param As String() In _gp_update_list
          If (Not GUI_Controls.DB_GeneralParam_Update(_general_param(GeneralParam.COLUMN_NAME.Group),
                                                      _general_param(GeneralParam.COLUMN_NAME.Subject),
                                                      _general_param(GeneralParam.COLUMN_NAME.Value),
                                                      _db_trx.SqlTransaction)) Then


            Return ENUM_STATUS.STATUS_ERROR
          End If
        Next

        _db_trx.SqlTransaction.Commit()
        Return ENUM_STATUS.STATUS_OK

      Catch _ex As Exception


        Return ENUM_STATUS.STATUS_ERROR
      End Try
    End Using

  End Function ' SavePaymentOrderConfig

  '----------------------------------------------------------------------------
  ' PURPOSE : Read one Payment Order record from general params.
  '
  ' PARAMS :
  '     - INPUT :   SubjectKey = Subject general param field related to Payment Order config info
  '                 Trx As SqlTransaction
  '     - OUTPUT :  ValueKey = Value for Subject
  '
  ' RETURNS :
  '     - ENUM_STATUS
  '
  ' NOTES :
  Public Shared Function ReadPaymentOrderParam(ByVal SubjectKey As String, _
                                     ByRef ValueKey As String, _
                                     ByVal Trx As SqlTransaction) As ENUM_STATUS
    Dim _str_sql As String
    Dim _group_key As String = "PaymentOrder"

    _str_sql = "SELECT   GP_KEY_VALUE               " & _
               "  FROM   GENERAL_PARAMS             " & _
               " WHERE   GP_GROUP_KEY   = @pGroup   " & _
               "   AND   GP_SUBJECT_KEY = @pSubject "

    Using _cmd As New SqlCommand(_str_sql, Trx.Connection, Trx)

      _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar, 50).Value = SubjectKey
      _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar, 50).Value = _group_key

      ValueKey = _cmd.ExecuteScalar()

      If ValueKey Is Nothing Or ValueKey Is DBNull.Value Then
        ValueKey = ""
        Return ENUM_STATUS.STATUS_ERROR
      Else
        Return ENUM_STATUS.STATUS_OK
      End If

    End Using

  End Function      ' ReadPaymentOrderParam

  '----------------------------------------------------------------------------
  ' PURPOSE : Load Payment Order logo image from documents table in database.
  '
  ' PARAMS :
  '     - INPUT :   Trx As SqlTransaction
  '     - OUTPUT :  ImageLogo = logo info as Image class
  '
  ' RETURNS :
  '     - Boolean
  '
  ' NOTES :
  Public Shared Function LoadPaymentOrderLogo(ByRef ImageLogo As Image, ByVal Trx As SqlTransaction) As Boolean

    Dim _doc_img As WSI.Common.DocumentList

    _doc_img = Nothing

    Try

      WSI.Common.TableDocuments.Load(WSI.Common.DOCUMENT_TYPE.PAYMENT_ORDER_LOGO, _doc_img, Trx)

      If IsNothing(_doc_img) Or _doc_img.Count = 0 Then
        Return False
      End If

      ImageLogo = Image.FromStream(New IO.MemoryStream(_doc_img.Item(0).Content))

      Return True

    Catch _ex As Exception
      ImageLogo = Nothing
      Return False

    End Try

  End Function   ' LoadPaymentOrderLogo

  '----------------------------------------------------------------------------
  ' PURPOSE : Save Payment Order logo image from documents table in database.
  '
  ' PARAMS :
  '     - INPUT :   ImageLogo = logo info as Image class
  '                 Trx As SqlTransaction
  '     - OUTPUT :  None
  '
  ' RETURNS :
  '     - Boolean
  '
  ' NOTES :
  Public Shared Function SavePaymentOrderLogo(ByVal Logo As Image, ByVal Trx As SqlTransaction) As Boolean

    Dim _doc_img As WSI.Common.DocumentList
    Dim _read_only_doc As WSI.Common.ReadOnlyDocument
    Dim _mm As IO.MemoryStream

    If IsNothing(Logo) Then
      Return False
    End If

    _doc_img = New WSI.Common.DocumentList
    _mm = New IO.MemoryStream()

    Try

      Logo.Save(_mm, Logo.RawFormat)

      _read_only_doc = New WSI.Common.ReadOnlyDocument("", _mm.GetBuffer())
      _doc_img.Add(_read_only_doc)

      WSI.Common.TableDocuments.Save(WSI.Common.DOCUMENT_TYPE.PAYMENT_ORDER_LOGO, _doc_img, Trx)

      Return True

    Catch _ex As Exception
      Return False

    End Try

  End Function   ' SavePaymentOrderLogo



#End Region ' Publics
End Class
