<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_collection_by_machine_and_date_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_show = New System.Windows.Forms.GroupBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.cb_bills_denominations = New System.Windows.Forms.CheckBox()
    Me.cb_total = New System.Windows.Forms.CheckBox()
    Me.cb_detail = New System.Windows.Forms.CheckBox()
    Me.uc_denominations = New GUI_Controls.uc_checked_list()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.lbl_remarks_1 = New System.Windows.Forms.Label()
    Me.lbl_remarks_2 = New System.Windows.Forms.Label()
    Me.lbl_remarks_3 = New System.Windows.Forms.Label()
    Me.chk_include_jackpot = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_show.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_remarks_3)
    Me.panel_filter.Controls.Add(Me.chk_include_jackpot)
    Me.panel_filter.Controls.Add(Me.lbl_remarks_2)
    Me.panel_filter.Controls.Add(Me.lbl_remarks_1)
    Me.panel_filter.Controls.Add(Me.gb_show)
    Me.panel_filter.Controls.Add(Me.uc_denominations)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1143, 240)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_denominations, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_show, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_remarks_1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_remarks_2, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_include_jackpot, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_remarks_3, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 244)
    Me.panel_data.Size = New System.Drawing.Size(1143, 462)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1137, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1137, 4)
    '
    'gb_show
    '
    Me.gb_show.Controls.Add(Me.chk_terminal_location)
    Me.gb_show.Controls.Add(Me.cb_bills_denominations)
    Me.gb_show.Controls.Add(Me.cb_total)
    Me.gb_show.Controls.Add(Me.cb_detail)
    Me.gb_show.Location = New System.Drawing.Point(6, 88)
    Me.gb_show.Name = "gb_show"
    Me.gb_show.Size = New System.Drawing.Size(260, 92)
    Me.gb_show.TabIndex = 1
    Me.gb_show.TabStop = False
    Me.gb_show.Text = "xShow"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(16, 14)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(238, 17)
    Me.chk_terminal_location.TabIndex = 0
    Me.chk_terminal_location.Text = "xTerminals location"
    '
    'cb_bills_denominations
    '
    Me.cb_bills_denominations.Location = New System.Drawing.Point(16, 33)
    Me.cb_bills_denominations.Name = "cb_bills_denominations"
    Me.cb_bills_denominations.Size = New System.Drawing.Size(238, 17)
    Me.cb_bills_denominations.TabIndex = 1
    Me.cb_bills_denominations.Text = "xBills_denominations"
    Me.cb_bills_denominations.UseVisualStyleBackColor = True
    '
    'cb_total
    '
    Me.cb_total.Location = New System.Drawing.Point(16, 71)
    Me.cb_total.Name = "cb_total"
    Me.cb_total.Size = New System.Drawing.Size(238, 17)
    Me.cb_total.TabIndex = 3
    Me.cb_total.Text = "xTotals"
    Me.cb_total.UseVisualStyleBackColor = True
    '
    'cb_detail
    '
    Me.cb_detail.Location = New System.Drawing.Point(16, 52)
    Me.cb_detail.Name = "cb_detail"
    Me.cb_detail.Size = New System.Drawing.Size(238, 17)
    Me.cb_detail.TabIndex = 2
    Me.cb_detail.Text = "xDetail"
    Me.cb_detail.UseVisualStyleBackColor = True
    '
    'uc_denominations
    '
    Me.uc_denominations.GroupBoxText = "xCheckedList"
    Me.uc_denominations.Location = New System.Drawing.Point(603, 6)
    Me.uc_denominations.m_resize_width = 387
    Me.uc_denominations.multiChoice = True
    Me.uc_denominations.Name = "uc_denominations"
    Me.uc_denominations.SelectedIndexes = New Integer(-1) {}
    Me.uc_denominations.SelectedIndexesList = ""
    Me.uc_denominations.SelectedIndexesListLevel2 = ""
    Me.uc_denominations.SelectedValuesArray = New String(-1) {}
    Me.uc_denominations.SelectedValuesList = ""
    Me.uc_denominations.SetLevels = 2
    Me.uc_denominations.Size = New System.Drawing.Size(387, 173)
    Me.uc_denominations.TabIndex = 3
    Me.uc_denominations.ValuesArray = New String(-1) {}
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(272, 2)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(318, 178)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(260, 81)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'lbl_remarks_1
    '
    Me.lbl_remarks_1.AutoSize = True
    Me.lbl_remarks_1.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_remarks_1.Location = New System.Drawing.Point(269, 186)
    Me.lbl_remarks_1.Name = "lbl_remarks_1"
    Me.lbl_remarks_1.Size = New System.Drawing.Size(102, 13)
    Me.lbl_remarks_1.TabIndex = 4
    Me.lbl_remarks_1.Text = "xLabelRemarks1"
    '
    'lbl_remarks_2
    '
    Me.lbl_remarks_2.AutoSize = True
    Me.lbl_remarks_2.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_remarks_2.Location = New System.Drawing.Point(269, 204)
    Me.lbl_remarks_2.Name = "lbl_remarks_2"
    Me.lbl_remarks_2.Size = New System.Drawing.Size(102, 13)
    Me.lbl_remarks_2.TabIndex = 11
    Me.lbl_remarks_2.Text = "xLabelRemarks2"
    '
    'lbl_remarks_3
    '
    Me.lbl_remarks_3.AutoSize = True
    Me.lbl_remarks_3.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_remarks_3.Location = New System.Drawing.Point(269, 222)
    Me.lbl_remarks_3.Name = "lbl_remarks_3"
    Me.lbl_remarks_3.Size = New System.Drawing.Size(102, 13)
    Me.lbl_remarks_3.TabIndex = 12
    Me.lbl_remarks_3.Text = "xLabelRemarks3"
    '
    'chk_include_jackpot
    '
    Me.chk_include_jackpot.Location = New System.Drawing.Point(22, 182)
    Me.chk_include_jackpot.Name = "chk_include_jackpot"
    Me.chk_include_jackpot.Size = New System.Drawing.Size(238, 17)
    Me.chk_include_jackpot.TabIndex = 4
    Me.chk_include_jackpot.Text = "x_include_jackpot"
    '
    'frm_collection_by_machine_and_date_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1153, 710)
    Me.Name = "frm_collection_by_machine_and_date_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_collection_by_machine_and_date_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_show.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_show As System.Windows.Forms.GroupBox
  Friend WithEvents cb_total As System.Windows.Forms.CheckBox
  Friend WithEvents cb_detail As System.Windows.Forms.CheckBox
  Friend WithEvents uc_denominations As GUI_Controls.uc_checked_list
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents cb_bills_denominations As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_remarks_1 As System.Windows.Forms.Label
  Friend WithEvents lbl_remarks_2 As System.Windows.Forms.Label
  Friend WithEvents lbl_remarks_3 As System.Windows.Forms.Label
  Friend WithEvents chk_include_jackpot As System.Windows.Forms.CheckBox
End Class
