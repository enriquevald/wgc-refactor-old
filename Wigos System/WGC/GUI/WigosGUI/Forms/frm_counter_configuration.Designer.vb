<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_counter_configuration
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_denomination = New GUI_Controls.uc_grid
    Me.gb_table_denomination = New System.Windows.Forms.GroupBox
    Me.btn_del = New GUI_Controls.uc_button
    Me.btn_add = New GUI_Controls.uc_button
    Me.cmb_model = New GUI_Controls.uc_combo
    Me.cmb_communication = New GUI_Controls.uc_combo
    Me.cmb_currency = New GUI_Controls.uc_combo
    Me.gb_area_conection_serial = New System.Windows.Forms.GroupBox
    Me.cmb_parity = New GUI_Controls.uc_combo
    Me.cmb_databits = New GUI_Controls.uc_combo
    Me.cmb_stopbits = New GUI_Controls.uc_combo
    Me.cmb_baudrate = New GUI_Controls.uc_combo
    Me.cmb_port = New GUI_Controls.uc_combo
    Me.ef_machine = New GUI_Controls.uc_entry_field
    Me.gb_counter = New System.Windows.Forms.GroupBox
    Me.panel_data.SuspendLayout()
    Me.gb_table_denomination.SuspendLayout()
    Me.gb_area_conection_serial.SuspendLayout()
    Me.gb_counter.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_counter)
    Me.panel_data.Controls.Add(Me.gb_area_conection_serial)
    Me.panel_data.Controls.Add(Me.gb_table_denomination)
    Me.panel_data.Size = New System.Drawing.Size(571, 272)
    '
    'dg_denomination
    '
    Me.dg_denomination.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.dg_denomination.CurrentCol = -1
    Me.dg_denomination.CurrentRow = -1
    Me.dg_denomination.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_denomination.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_denomination.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_denomination.Location = New System.Drawing.Point(6, 20)
    Me.dg_denomination.Name = "dg_denomination"
    Me.dg_denomination.PanelRightVisible = False
    Me.dg_denomination.Redraw = True
    Me.dg_denomination.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_denomination.Size = New System.Drawing.Size(291, 132)
    Me.dg_denomination.Sortable = False
    Me.dg_denomination.SortAscending = True
    Me.dg_denomination.SortByCol = 0
    Me.dg_denomination.TabIndex = 0
    Me.dg_denomination.ToolTipped = True
    Me.dg_denomination.TopRow = -2
    '
    'gb_table_denomination
    '
    Me.gb_table_denomination.Controls.Add(Me.btn_del)
    Me.gb_table_denomination.Controls.Add(Me.btn_add)
    Me.gb_table_denomination.Controls.Add(Me.dg_denomination)
    Me.gb_table_denomination.Location = New System.Drawing.Point(3, 80)
    Me.gb_table_denomination.Name = "gb_table_denomination"
    Me.gb_table_denomination.Size = New System.Drawing.Size(305, 189)
    Me.gb_table_denomination.TabIndex = 2
    Me.gb_table_denomination.TabStop = False
    '
    'btn_del
    '
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(214, 160)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 2
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_add
    '
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(33, 160)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 1
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'cmb_model
    '
    Me.cmb_model.AllowUnlistedValues = False
    Me.cmb_model.AutoCompleteMode = False
    Me.cmb_model.IsReadOnly = False
    Me.cmb_model.Location = New System.Drawing.Point(24, 46)
    Me.cmb_model.Name = "cmb_model"
    Me.cmb_model.SelectedIndex = -1
    Me.cmb_model.Size = New System.Drawing.Size(217, 24)
    Me.cmb_model.SufixText = "Sufix Text"
    Me.cmb_model.SufixTextVisible = True
    Me.cmb_model.TabIndex = 1
    '
    'cmb_communication
    '
    Me.cmb_communication.AllowUnlistedValues = False
    Me.cmb_communication.AutoCompleteMode = False
    Me.cmb_communication.IsReadOnly = False
    Me.cmb_communication.Location = New System.Drawing.Point(301, 46)
    Me.cmb_communication.Name = "cmb_communication"
    Me.cmb_communication.SelectedIndex = -1
    Me.cmb_communication.Size = New System.Drawing.Size(250, 24)
    Me.cmb_communication.SufixText = "Sufix Text"
    Me.cmb_communication.SufixTextVisible = True
    Me.cmb_communication.TabIndex = 3
    Me.cmb_communication.TextWidth = 130
    '
    'cmb_currency
    '
    Me.cmb_currency.AllowUnlistedValues = False
    Me.cmb_currency.AutoCompleteMode = False
    Me.cmb_currency.IsReadOnly = False
    Me.cmb_currency.Location = New System.Drawing.Point(301, 17)
    Me.cmb_currency.Name = "cmb_currency"
    Me.cmb_currency.SelectedIndex = -1
    Me.cmb_currency.Size = New System.Drawing.Size(250, 24)
    Me.cmb_currency.SufixText = "Sufix Text"
    Me.cmb_currency.SufixTextVisible = True
    Me.cmb_currency.TabIndex = 2
    Me.cmb_currency.TextWidth = 130
    '
    'gb_area_conection_serial
    '
    Me.gb_area_conection_serial.Controls.Add(Me.cmb_parity)
    Me.gb_area_conection_serial.Controls.Add(Me.cmb_databits)
    Me.gb_area_conection_serial.Controls.Add(Me.cmb_stopbits)
    Me.gb_area_conection_serial.Controls.Add(Me.cmb_baudrate)
    Me.gb_area_conection_serial.Controls.Add(Me.cmb_port)
    Me.gb_area_conection_serial.Location = New System.Drawing.Point(314, 81)
    Me.gb_area_conection_serial.Name = "gb_area_conection_serial"
    Me.gb_area_conection_serial.Size = New System.Drawing.Size(251, 188)
    Me.gb_area_conection_serial.TabIndex = 1
    Me.gb_area_conection_serial.TabStop = False
    '
    'cmb_parity
    '
    Me.cmb_parity.AllowUnlistedValues = False
    Me.cmb_parity.AutoCompleteMode = False
    Me.cmb_parity.IsReadOnly = False
    Me.cmb_parity.Location = New System.Drawing.Point(6, 140)
    Me.cmb_parity.Name = "cmb_parity"
    Me.cmb_parity.SelectedIndex = -1
    Me.cmb_parity.Size = New System.Drawing.Size(234, 24)
    Me.cmb_parity.SufixText = "Sufix Text"
    Me.cmb_parity.SufixTextVisible = True
    Me.cmb_parity.TabIndex = 4
    Me.cmb_parity.TextWidth = 130
    '
    'cmb_databits
    '
    Me.cmb_databits.AllowUnlistedValues = False
    Me.cmb_databits.AutoCompleteMode = False
    Me.cmb_databits.IsReadOnly = False
    Me.cmb_databits.Location = New System.Drawing.Point(6, 110)
    Me.cmb_databits.Name = "cmb_databits"
    Me.cmb_databits.SelectedIndex = -1
    Me.cmb_databits.Size = New System.Drawing.Size(234, 24)
    Me.cmb_databits.SufixText = "Sufix Text"
    Me.cmb_databits.SufixTextVisible = True
    Me.cmb_databits.TabIndex = 3
    Me.cmb_databits.TextWidth = 130
    '
    'cmb_stopbits
    '
    Me.cmb_stopbits.AllowUnlistedValues = False
    Me.cmb_stopbits.AutoCompleteMode = False
    Me.cmb_stopbits.IsReadOnly = False
    Me.cmb_stopbits.Location = New System.Drawing.Point(6, 80)
    Me.cmb_stopbits.Name = "cmb_stopbits"
    Me.cmb_stopbits.SelectedIndex = -1
    Me.cmb_stopbits.Size = New System.Drawing.Size(234, 24)
    Me.cmb_stopbits.SufixText = "Sufix Text"
    Me.cmb_stopbits.SufixTextVisible = True
    Me.cmb_stopbits.TabIndex = 2
    Me.cmb_stopbits.TextWidth = 130
    '
    'cmb_baudrate
    '
    Me.cmb_baudrate.AllowUnlistedValues = False
    Me.cmb_baudrate.AutoCompleteMode = False
    Me.cmb_baudrate.IsReadOnly = False
    Me.cmb_baudrate.Location = New System.Drawing.Point(6, 50)
    Me.cmb_baudrate.Name = "cmb_baudrate"
    Me.cmb_baudrate.SelectedIndex = -1
    Me.cmb_baudrate.Size = New System.Drawing.Size(234, 24)
    Me.cmb_baudrate.SufixText = "Sufix Text"
    Me.cmb_baudrate.SufixTextVisible = True
    Me.cmb_baudrate.TabIndex = 1
    Me.cmb_baudrate.TextWidth = 130
    '
    'cmb_port
    '
    Me.cmb_port.AllowUnlistedValues = False
    Me.cmb_port.AutoCompleteMode = False
    Me.cmb_port.IsReadOnly = False
    Me.cmb_port.Location = New System.Drawing.Point(6, 20)
    Me.cmb_port.Name = "cmb_port"
    Me.cmb_port.SelectedIndex = -1
    Me.cmb_port.Size = New System.Drawing.Size(234, 24)
    Me.cmb_port.SufixText = "Sufix Text"
    Me.cmb_port.SufixTextVisible = True
    Me.cmb_port.TabIndex = 0
    Me.cmb_port.TextWidth = 130
    '
    'ef_machine
    '
    Me.ef_machine.DoubleValue = 0
    Me.ef_machine.IntegerValue = 0
    Me.ef_machine.IsReadOnly = True
    Me.ef_machine.Location = New System.Drawing.Point(24, 17)
    Me.ef_machine.Name = "ef_machine"
    Me.ef_machine.Size = New System.Drawing.Size(217, 24)
    Me.ef_machine.SufixText = "Sufix Text"
    Me.ef_machine.SufixTextVisible = True
    Me.ef_machine.TabIndex = 0
    Me.ef_machine.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_machine.TextValue = ""
    Me.ef_machine.Value = ""
    Me.ef_machine.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_counter
    '
    Me.gb_counter.Controls.Add(Me.cmb_currency)
    Me.gb_counter.Controls.Add(Me.ef_machine)
    Me.gb_counter.Controls.Add(Me.cmb_model)
    Me.gb_counter.Controls.Add(Me.cmb_communication)
    Me.gb_counter.Location = New System.Drawing.Point(3, 3)
    Me.gb_counter.Name = "gb_counter"
    Me.gb_counter.Size = New System.Drawing.Size(562, 77)
    Me.gb_counter.TabIndex = 0
    Me.gb_counter.TabStop = False
    '
    'frm_counter_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(673, 281)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_counter_configuration"
    Me.Text = "frm_counter_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.gb_table_denomination.ResumeLayout(False)
    Me.gb_area_conection_serial.ResumeLayout(False)
    Me.gb_counter.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_denomination As GUI_Controls.uc_grid
  Friend WithEvents gb_table_denomination As System.Windows.Forms.GroupBox
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents gb_area_conection_serial As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_currency As GUI_Controls.uc_combo
  Friend WithEvents cmb_communication As GUI_Controls.uc_combo
  Friend WithEvents cmb_model As GUI_Controls.uc_combo
  Friend WithEvents cmb_parity As GUI_Controls.uc_combo
  Friend WithEvents cmb_databits As GUI_Controls.uc_combo
  Friend WithEvents cmb_stopbits As GUI_Controls.uc_combo
  Friend WithEvents cmb_baudrate As GUI_Controls.uc_combo
  Friend WithEvents cmb_port As GUI_Controls.uc_combo
  Friend WithEvents ef_machine As GUI_Controls.uc_entry_field
  Friend WithEvents gb_counter As System.Windows.Forms.GroupBox
End Class
