'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_alarms
' DESCRIPTION:   Alarms 
' AUTHOR:        Raul Cervera
' CREATION DATE: 15-JUL-2011
'
' REVISION HISTORY: 
' 
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-JUL-2011  RCI    Initial version
' 18-OCT-2011  JML    Add select a filter for all source alarms.
' 01-JUN-2012  JAB    Check if disposed.
' 08-JUN-2012  JAB    DoEvents run each second.
' 12-JUN-2012  JAB    DoEvents run each second (use common routine).
' 24-AUG-2012  JAB    Control to show huge amounts of records.
' 28-NOV-2012  ANG    Added new filter entry for PSAClient alarm.
' 04-DES-2012  ANG    Fix bug #417. 
' 18-APR-2013  ICS    Changed the cashier name by its alias.
' 21-MAY-2013  AMF    Fixed Bug #796: Remove Terminals Filter from the impresion when is MultiSite
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 15-NOV-2013  LJM    Changes for auditing form
' 07-FEB-2014  AMF    Fixed Bug WIG-497: Don't reset checkbox sources
' 19-FEB-2014  JPJ    Defect WIG-485, WIG-518: Searches can't be done
' 02-MAY-2014  LEM    Fixed Bug WIG-870: Unhandled exception by Mode filter
' 02-MAY-2014  LEM    Date filter are disabled on Monitoring Mode, in this case the search period is set to last MONITOR_PERIOD_DAYS days.
' 07-MAY-2014  JML    Add filter "Alarms catalog", "Inquiry source alarm" and "Execute patterns"
' 04-JUN-2014  JBC    Fixed Bug WIG-995: Now uc_checked_list_alarms_catalog doesn't reload data from DB on reset click
' 22-JUL-2014  JBC    Fixed Bug WIG-1021: Now filters all game alarms
' 26-AUG-2014  JBC    Fixed Bug WIG-1212: Now filters all game alarms correctly
' 02-OCT-2014  JPJ    Fixed Bug WIG-1378: Changing from monitor to historic doesn't enable some buttons
' 09-OCT-2014  DRV    Fixed Bug WIG-1453: Alarm Filter idiom it's different from the configured language
' 11-NOV-2014  MPO    WIG-1671: Pattern source is related to a terminal
' 21-NOV-2014  SGB    Change order columns in gr_list, show in column code: hexadecimal and description alarm, move controls in design.
' 22-DEC-2014  DRV    Fixed Bug: WIG-1806: Some patterns are not shown in the alarms form
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 25-JUN-2015  SGB    Backlog Item 1429: New filter of technician card
' 16-SEP-2015  FJC    Fixed Bug 4513: Add column al_technician_id on Table 'alarms' (multisite) 
'                                     Add column sa_technician_id on Table 'sitealarms' (multisite)
' 17-NOV-2015 FAV     Fixed Bug 6650: Query in Alarm report returns all elements (Mode group box) and Query too large with all codes selected
' 14-MAR-2016 RAB     Bug 9835:Alarms: "Technical" filter does not include blocked users
' 26-APR-2016 ETP     Bug 10647: Alarms: Monitor Mode --> order by not needed makes query to slow.
' 06-FEB-2017 MS      Bug 6650:Reporte de Alarmas - Filtro devuelve todos los registros y hace select innecesario
' 26-SEP-2017 ETP     Fixed Bug 29921:[WIGOS-4564] Multisite: wrong site number in grids
' 20-NOV-2017 OMC     PBI 30857: Enable Monitor mode for multisite.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports WSI.Common.Alarm
Imports System.Text
Imports System.Text.RegularExpressions

Public Class frm_alarms
  Inherits frm_base_sel

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Short = 219

  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_ALARM_ID As Integer = 1
  Private Const SQL_COLUMN_SOURCE_CODE As Integer = 2
  Private Const SQL_COLUMN_SOURCE_ID As Integer = 3
  Private Const SQL_COLUMN_SOURCE_NAME As Integer = 4
  Private Const SQL_COLUMN_ALARM_CODE As Integer = 5
  Private Const SQL_COLUMN_ALARM_CODE_DESCRIPTION As Integer = 6
  Private Const SQL_COLUMN_ALARM_NAME As Integer = 7
  Private Const SQL_COLUMN_ALARM_DESCRIPTION As Integer = 8
  Private Const SQL_COLUMN_SEVERITY As Integer = 9
  Private Const SQL_COLUMN_REPORTED As Integer = 10
  Private Const SQL_COLUMN_DATETIME As Integer = 11
  Private Const SQL_COLUMN_ACK_DATETIME As Integer = 12
  Private Const SQL_COLUMN_ACK_USER_ID As Integer = 13
  Private Const SQL_COLUMN_ACK_USER_NAME As Integer = 14
  Private Const SQL_COLUMN_TIMESTAMP As Integer = 15
  Private Const SQL_COLUMN_TECHNICIAN_ID As Integer = 16
  Private Const SQL_COLUMN_TECHNICIAN_NAME As Integer = 17

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_TIMESTAMP As Integer = 1
  Private Const GRID_COLUMN_SITE_ID As Integer = 2
  Private Const GRID_COLUMN_DATETIME As Integer = 3
  Private Const GRID_COLUMN_ALARM_ID As Integer = 4
  Private Const GRID_COLUMN_SEVERITY As Integer = 5
  Private Const GRID_COLUMN_ALARM_GROUP As Integer = 6
  Private Const GRID_COLUMN_ALARM_CATEGORY As Integer = 7
  Private Const GRID_COLUMN_ALARM_CODE_DESCRIPTION As Integer = 8
  Private Const GRID_COLUMN_SOURCE_NAME As Integer = 9
  Private Const GRID_COLUMN_ALARM_DESCRIPTION As Integer = 10
  Private Const GRID_COLUMN_ACK_DATETIME As Integer = 11
  Private Const GRID_COLUMN_ACK_USER_NAME As Integer = 12
  Private Const GRID_COLUMN_SOURCE_CODE As Integer = 13
  Private Const GRID_COLUMN_TECHNICIAN_ID As Integer = 14
  Private Const GRID_COLUMN_TECHNICIAN_NAME As Integer = 15

  Private Const GRID_COLUMNS As Integer = 16
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Grid for alarm types
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2

  Private Const GRID_2_COLUMNS As Integer = 3
  Private Const GRID_2_HEADER_ROWS As Integer = 0

  Private Const GRID_2_TAB As String = "    "

  ' Timer interval to refresh alarms (in seconds)
  Private ALARM_REFRESH_TIME As Integer = 5

  Private Const MAX_ROWS_IN_MONITOR_MODE As Integer = 30

  'Counters
  Private Const COUNTER_INFO As Integer = 1
  Private Const COUNTER_WARNING As Integer = 2
  Private Const COUNTER_ERROR As Integer = 3

  Private Const MAX_COUNTERS As Integer = 3

  Private Const COUNTER_ALL_VISIBLE As Boolean = True

  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

  Private Const MAX_FILTER_REPORT = 4

  ' Source Event 

  Private Const GRID_3_COLUMN_CODE As Integer = 0
  Private Const GRID_3_COLUMN_CHECKED As Integer = 1
  Private Const GRID_3_COLUMN_DESC As Integer = 2
  Private Const GRID_3_COLUMN_TYPE As Integer = 3
  Private Const GRID_3_COLUMNS As Integer = 4

  Private Const GRID_3_HEADER_ROWS As Integer = 0
  Private Const GRID_3_TAB As String = "    "

  ' Source Event level
  Const WCP_SOURCE_EVENT_LEVEL_1 As Integer = 1
  Const WCP_SOURCE_EVENT_LEVEL_2 As Integer = 2

  Private Const MONITOR_PERIOD_DAYS As Integer = 7

  Private Const START_RANGE_GAME_ALARMS As String = "135169"
  Private Const END_RANGE_GAME_ALARMS As String = "139263"

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String = ""
  Private m_date_to As String = ""
  Private m_severity As String = ""
  Private m_terminals As String = ""
  Private m_sites As String = ""
  Private m_alarm_types As String = ""
  Private m_monitoring As String = ""
  Private m_users As String = ""
  Private m_monitor_enabled As Boolean = False

  Private m_first_time As Boolean = True
  Private m_init As Boolean = True

  Private m_counter_list(MAX_COUNTERS) As Integer

  Private m_selected_ids As List(Of Integer) = Nothing

  Private m_enable_pr_list As Boolean = False
  Private m_source_list_terminal As String
  Private m_source_list_no_terminal As String
  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)
  Private m_is_cashdeskdraw As Boolean = GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw", False)
  Private m_is_sites_alarms As Boolean = False

  ' source event
  Private m_code As String = ""
  Private m_refreshing_grid As Boolean
  Private m_alarm_desc As String = ""
  Private m_alarm_id As String = ""

  Private m_num_of_refreshes As Integer = 0

  ' JML 5-MAY-2014
  Dim m_alarms_dic As Dictionary(Of Integer, ALARMS_CATALOG)

  Dim m_table As DataTable

  Private m_is_task_patterns_enable As Boolean = GeneralParam.GetBoolean("Pattern", "Enabled", False)

  Private m_selected_alarms As String

  Public Structure SOURCE_EVENT
    Dim source_type As Integer
    Dim source_code As Integer
    Dim source_nls_id As Integer

    Public Sub SOURCE_EVENT(ByVal T As Integer, ByVal ID As Integer)

    End Sub
  End Structure

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    'Me.FormId = ENUM_FORM.FORM_ALARMS
    'Call MyBase.GUI_SetFormId()
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Select Case Me.FormId
      Case ENUM_FORM.FORM_ALARMS
        ' Alarms
        Me.Text = GLB_NLS_GUI_ALARMS.GetString(201)

      Case ENUM_FORM.FORM_SITES_ALARMS
        ' Sites Alarms
        Me.Text = GLB_NLS_GUI_ALARMS.GetString(461)
        m_is_sites_alarms = True

      Case Else
        ' Alarms
        Me.Text = GLB_NLS_GUI_ALARMS.GetString(201)

    End Select


    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    ' Ack button
    'If Not m_is_sites_alarms Then
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_ALARMS.GetString(1)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    'End If

    ' Source inquiry
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_ALARMS.GetString(464)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Height += 10

    ' Run patterns
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_ALARMS.GetString(465)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Height += 10

    ' Source inquiry & Run patterns buttons are visible?
    If m_is_cashdeskdraw Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    ElseIf m_is_center And Not m_is_sites_alarms Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    ElseIf m_is_sites_alarms Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    End If


    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_ALARMS.GetString(443)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Alarms
    Me.gb_sources.Text = GLB_NLS_GUI_ALARMS.GetString(202)


    ' Mode
    Me.gb_mode.Text = GLB_NLS_GUI_ALARMS.GetString(203)
    Me.opt_monitor_mode.Text = GLB_NLS_GUI_ALARMS.GetString(204)
    Me.opt_history_mode.Text = GLB_NLS_GUI_ALARMS.GetString(205)

    Me.lbl_latest_alarms.Value = GLB_NLS_GUI_ALARMS.GetString(210)
    Me.lbl_no_data.Value = GLB_NLS_GUI_ALARMS.GetString(211)

    ' Game Providers - Terminals
    If m_is_center OrElse m_is_cashdeskdraw OrElse m_is_sites_alarms Then
      uc_pr_list.Visible = False
    Else
      Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes())
    End If

    If m_is_cashdeskdraw Or GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      Me.uc_checked_list_alarms_catalog.Visible = False
    End If

    If m_is_sites_alarms Then
      uc_site_select.Visible = True
      uc_site_select.ShowMultisiteRow = False
      uc_site_select.MultiSelect = False
      uc_site_select.Init()
      uc_site_select.SetDefaultValues(False)
      gb_mode.Visible = False
    Else
      uc_site_select.Visible = False
    End If

    ' AGG - Site Alarms - Add Terminals filter
    If WSI.Common.Misc.IsAGGEnabled() AndAlso m_is_sites_alarms Then
      uc_pr_list.Visible = True
      uc_checked_list_alarms_catalog.Visible = True
      Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes(), , , True)
      gb_sources.Visible = False
      gb_mode.Visible = True
    End If


    If WSI.Common.Misc.IsAGGEnabled() AndAlso m_is_center Then
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    End If


    ' Severity
    Me.gb_severity.Text = GLB_NLS_GUI_ALARMS.GetString(416)
    Me.chk_info.Text = GLB_NLS_GUI_ALARMS.GetString(206)
    Me.chk_warning.Text = GLB_NLS_GUI_ALARMS.GetString(358)
    Me.chk_error.Text = GLB_NLS_GUI_ALARMS.GetString(357)

    Me.lbl_color_info.BackColor = GetColor(COLOR_INFO_BACK)
    Me.lbl_color_warning.BackColor = GetColor(COLOR_WARNING_BACK)
    Me.lbl_color_error.BackColor = GetColor(COLOR_ERROR_BACK)

    Me.ef_alarm_desc.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5711)
    Me.ef_alarm_desc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_alarm_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5707)
    Me.ef_alarm_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 20)
    Me.ef_alarm_id.IsReadOnly = False

    ' Users 
    Me.chk_user_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6522)
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.opt_none.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6521)

    ' Fill source message
    btn_check_all_sources.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all_sources.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    ' If task is not enable or is cashdeskdraw: Filter by alarm categories, & buttons about patterns are not vilible
    If Not m_is_task_patterns_enable Or m_is_cashdeskdraw Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    End If

    ' Update info
    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)
    Me.tf_last_update.Visible = False
    Me.tf_last_update.Location = New Point(Me.tf_last_update.Location.X + 355, Me.tf_last_update.Location.Y)

    If Not m_is_sites_alarms Then
      Me.gb_date.Location = New Point(Me.gb_date.Location.X - 290, Me.gb_date.Location.Y)
      Me.lbl_latest_alarms.Location = New Point(Me.lbl_latest_alarms.Location.X - 290, Me.lbl_latest_alarms.Location.Y)
      Me.lbl_no_data.Location = New Point(Me.lbl_no_data.Location.X - 290, Me.lbl_no_data.Location.Y)
      Me.gb_severity.Location = New Point(Me.gb_severity.Location.X - 290, Me.gb_severity.Location.Y)
      Me.gb_user.Location = New Point(Me.gb_user.Location.X - 290, Me.gb_user.Location.Y)
      Me.gb_mode.Location = New Point(Me.gb_mode.Location.X - 290, Me.gb_mode.Location.Y)
      If GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        Me.gb_sources.Location = New Point(Me.gb_sources.Location.X - 545, Me.gb_sources.Location.Y)
      Else
        Me.gb_sources.Location = New Point(Me.gb_sources.Location.X - 290, Me.gb_sources.Location.Y)
      End If
      '  Me.ef_alarm_desc.Location = New Point(Me.ef_alarm_desc.Location.X - 290, Me.ef_alarm_desc.Location.Y)
      Me.uc_pr_list.Location = New Point(Me.uc_pr_list.Location.X - 290, Me.uc_pr_list.Location.Y)
      If m_is_center OrElse m_is_cashdeskdraw Then
        Me.uc_checked_list_alarms_catalog.Location = New Point(uc_checked_list_alarms_catalog.Location.X - 329, uc_checked_list_alarms_catalog.Location.Y)
        Me.uc_checked_list_alarms_catalog.Width = 400
      End If
      Me.uc_checked_list_alarms_catalog.Location = New Point(Me.uc_checked_list_alarms_catalog.Location.X - 290, Me.uc_checked_list_alarms_catalog.Location.Y)
    Else
      If WSI.Common.Misc.IsAGGEnabled() Then
        Me.uc_pr_list.Location = New Point(Me.uc_pr_list.Location.X - 275, Me.uc_pr_list.Location.Y)
      Else
        Me.uc_checked_list_alarms_catalog.Location = New Point(Me.uc_checked_list_alarms_catalog.Location.X - 328, Me.uc_checked_list_alarms_catalog.Location.Y)
        Me.gb_severity.Location = New Point(Me.gb_severity.Location.X, Me.gb_severity.Location.Y - 25)
      End If
      Me.gb_sources.Location = New Point(Me.gb_sources.Location.X - 255, Me.gb_sources.Location.Y)
      Me.ef_alarm_desc.Width = Me.uc_site_select.Width - 10
      Me.panel_filter.Height = Me.panel_filter.Height - 10
      Me.uc_checked_list_alarms_catalog.Width = uc_checked_list_alarms_catalog.Width + 38
    End If

    uc_checked_list_alarms_catalog.ColumnWidth(3, 7000)

    Call GUI_StyleSheetDeviceOperation()

    Call FillSourceFilterGrid()

    Call GUI_StyleSheet()

    ' Alarms catalog
    uc_checked_list_alarms_catalog.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
    uc_checked_list_alarms_catalog.SetLevels = 3
    uc_checked_list_alarms_catalog.btn_check_all.Width = 100
    uc_checked_list_alarms_catalog.btn_uncheck_all.Width = 115

    'Set filter Alarms info
    Call FillAlarmsCatalog()

    ' Set filter default values
    Call SetDefaultValues()

    'Set combo user
    '16-SEP-2015  
    If GLB_GuiMode = public_globals.ENUM_GUI.MULTISITE_GUI Then
      gb_user.Visible = False
      chk_user_filter.Visible = False
    Else

      'The id in anonim user is 0
      Me.cmb_user.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6520))
    End If

    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    If m_is_center Then
      ALARM_REFRESH_TIME = 30
    End If
    Me.tmr_monitor.Interval = GeneralParam.GetInt32("Monitor", "Refresh.Interval", ALARM_REFRESH_TIME) * 1000

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    gb_mode.Enabled = False

    m_counter_list(COUNTER_INFO) = 0
    m_counter_list(COUNTER_WARNING) = 0
    m_counter_list(COUNTER_ERROR) = 0
  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    ' gb_mode just can be enabled when ef_alarm_id doesn't have value
    gb_mode.Enabled = (ef_alarm_id.TextValue = "")

    If Grid.Counter(COUNTER_INFO).Value <> m_counter_list(COUNTER_INFO) Then
      Grid.Counter(COUNTER_INFO).Value = m_counter_list(COUNTER_INFO)
    End If
    If Grid.Counter(COUNTER_WARNING).Value <> m_counter_list(COUNTER_WARNING) Then
      Grid.Counter(COUNTER_WARNING).Value = m_counter_list(COUNTER_WARNING)
    End If
    If Grid.Counter(COUNTER_ERROR).Value <> m_counter_list(COUNTER_ERROR) Then
      Grid.Counter(COUNTER_ERROR).Value = m_counter_list(COUNTER_ERROR)
    End If
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' if there is an alarm Id defined to make the search, the other filters wil be  rejected
    If ef_alarm_id.Value = "" Then
      ' Date selection 
      If Me.dtp_from.Checked And Me.dtp_to.Checked Then
        If Me.dtp_from.Value > Me.dtp_to.Value Then
          Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call Me.dtp_to.Focus()

          Return False
        End If
      End If


      Dim _idx_row As Integer
      Dim _no_one As Boolean
      _no_one = True

      For _idx_row = 0 To Me.dg_sources.NumRows - 1

        If dg_sources.Cell(_idx_row, GRID_3_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _
             dg_sources.Cell(_idx_row, GRID_3_COLUMN_CODE).Value.Length > 0 Then
          _no_one = False
        End If
      Next

      If _no_one Then
        If Me.opt_history_mode.Checked Then
          Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(111), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Return False
        Else
          If lbl_no_data.Visible = False Then
            Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(111), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
          lbl_no_data.Visible = True
          Me.Grid.Clear()
          Return False
        End If
      End If
    End If

    If m_is_sites_alarms And Me.opt_history_mode.Checked Then
      If Not uc_site_select.FilterCheckSites() Then
        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: What to do when no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  '
  Protected Overrides Sub GUI_NoDataFound()
    If opt_history_mode.Checked Then
      Call MyBase.GUI_NoDataFound()
    End If
  End Sub ' GUI_NoDataFound

  ' PURPOSE: Indicates if to select the first row of the grid or not
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Dim _select_first As Boolean

    _select_first = opt_history_mode.Checked Or (opt_monitor_mode.Checked And m_first_time)
    m_first_time = False

    Return _select_first
  End Function ' GUI_SelectFirstRow

  ' PURPOSE: Overrides the clear grid routine
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Sub GUI_ClearGrid()
    If opt_history_mode.Checked Or (opt_monitor_mode.Checked And m_first_time) Then
      Call MyBase.GUI_ClearGrid()
    End If
  End Sub ' GUI_ClearGrid

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As String

    If opt_monitor_mode.Checked Then
      _str_sql = "SELECT   TOP " & MAX_ROWS_IN_MONITOR_MODE
    Else
      _str_sql = "SELECT   "
    End If
    If m_is_sites_alarms Then
      _str_sql = _str_sql & " XX_SITE_ID, "
    Else
      _str_sql = _str_sql & " 0 AS XX_SITE_ID, "
    End If

    _str_sql = _str_sql & "  XX_ALARM_ID " & _
                "          , XX_SOURCE_CODE " & _
                "          , XX_SOURCE_ID " & _
                "          , XX_SOURCE_NAME " & _
                "          , XX_ALARM_CODE " & _
                "          , ALCG_NAME" & _
                "          , XX_ALARM_NAME " & _
                "          , XX_ALARM_DESCRIPTION " & _
                "          , XX_SEVERITY " & _
                "          , XX_REPORTED " & _
                "          , XX_DATETIME " & _
                "          , XX_ACK_DATETIME " & _
                "          , XX_ACK_USER_ID " & _
                "          , XX_ACK_USER_NAME " & _
                "          , CAST (XX_TIMESTAMP AS BIGINT) AS XX_TIMESTAMP " & _
                "          , XX_TECHNICIAN_ID " & _
                "          , GU_USERNAME " & _
                "     FROM   TABLE_ALARM " & _
                "LEFT JOIN   ALARM_CATALOG ON ALCG_ALARM_CODE = XX_ALARM_CODE" & _
                "			 AND   ALCG_LANGUAGE_ID = " & Language.GetWigosGUILanguageId() & _
                "LEFT JOIN   GUI_USERS ON GU_USER_ID = XX_TECHNICIAN_ID "

    _str_sql = _str_sql & GetSqlWhere()

    _str_sql = _str_sql & " ORDER BY XX_DATETIME DESC" ' Order by XX_ALARM_ID Makes query to slow and blocks GUI.

    If m_is_sites_alarms Then
      _str_sql = Replace(_str_sql, "XX_", "SA_")
      _str_sql = Replace(_str_sql, "TABLE_ALARM", "SITE_ALARMS")
    Else
      _str_sql = Replace(_str_sql, "XX_", "AL_")
      _str_sql = Replace(_str_sql, "TABLE_ALARM", "ALARMS")
    End If

    Return _str_sql
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _str_sql As String
    Dim _idx_row As Integer
    Dim _idx_grid As Integer
    Dim _count As Integer
    Dim _db_row As CLASS_DB_ROW
    Dim _bln_redraw As Boolean
    Dim _function_name As String
    Dim _more_than_max As Boolean
    Dim _row_step As Integer
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _render_only_changes As Boolean
    Dim _has_changes As Boolean

    _table = Nothing
    m_table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"

    _max_rows = GUI_MaxRows()
    _render_only_changes = opt_monitor_mode.Checked

    lbl_no_data.Visible = False
    _has_changes = False

    Try
      SaveSelectedIds()

      Call tmr_monitor.Stop()

      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _table = GUI_GetTableUsingSQL(_str_sql, _max_rows)

      If _table Is Nothing Then
        lbl_no_data.Visible = True
        Me.Grid.Clear()

        Return
      End If

      If _table.Rows.Count = 0 Then
        lbl_no_data.Visible = True
        Me.Grid.Clear()

        Return
      End If

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_table.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      _bln_redraw = Me.Grid.Redraw
      Me.Grid.Redraw = False

      _more_than_max = False

      Call GUI_BeforeFirstRow()

      _row_step = _max_rows
      _row_step = _row_step / 20
      If _row_step < MAX_RECORDS_FIRST_LOAD Then
        _row_step = MAX_RECORDS_FIRST_LOAD
      End If

      _count = 0
      _idx_grid = 0
      _idx_row = 0

      While _idx_row < _table.Rows.Count

        Try

          ' JAB 12-JUN-2012: DoEvents run each second.
          If Not GUI_DoEvents(Me.Grid) Then
            Exit Sub
          End If

          _row = _table.Rows(_idx_row)
          _db_row = New CLASS_DB_ROW(_row)

          If _render_only_changes Then
            If DataGridMustChange(_idx_grid, _row) Then
              If Me.Grid.NumRows - 1 < _idx_grid Then
                Me.Grid.AddRow()
              End If
              If GUI_SetupRow(_idx_grid, _db_row) Then
                _has_changes = True
                _count = _count + 1
              End If
            End If
            _idx_grid = _idx_grid + 1
          Else
            ' Add the db row to the grid
            Me.Grid.AddRow()
            _idx_grid = Me.Grid.NumRows - 1

            If GUI_SetupRow(_idx_grid, _db_row) Then
              _count = _count + 1
            End If
          End If

          Call UpdateCounters(_db_row.Value(SQL_COLUMN_SEVERITY))

          _idx_row = _idx_row + 1

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "Alarms", _
                                _function_name, _
                                exception.Message)
          Exit While
        End Try

        ' RCI 22-DEC-2010: Need count > 0, otherwise it's always doing Redraw for 0 rows!!
        If _count > 0 And _count Mod _row_step = 0 Then
          Me.Grid.Redraw = True
          Call Application.DoEvents()
          Windows.Forms.Cursor.Current = Cursors.WaitCursor
          Me.Grid.Redraw = False
        End If

        If _count >= _max_rows Then
          _more_than_max = True

          Exit While
        End If
      End While

      If _render_only_changes Then
        While Me.Grid.NumRows > _table.Rows.Count
          Me.Grid.DeleteRowFast(Me.Grid.NumRows - 1)
          _has_changes = True
        End While
      End If

      Call GUI_AfterLastRow()

      Me.Grid.Redraw = True

      If _has_changes Then
        SelectLastSelectedRow()
      End If

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Alarms", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    Finally
      ' JAB 01-JUN-2012: Check if disposed.
      If Not Me.IsDisposed Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (Me.Grid.NumRows > 0 And Me.Permissions.Write)
      End If

      m_table = New DataTable()
      m_table = _table.Copy()

      If Not _render_only_changes Then
        If _table IsNot Nothing Then
          Call _table.Clear()
        End If
        _table = Nothing
      End If
      Call tmr_monitor.Start()
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal As String
    Dim _split() As String
    Dim _split2() As String
    Dim _pattern As String

    ' Timestamp
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TIMESTAMP).Value = DbRow.Value(SQL_COLUMN_TIMESTAMP)

    ' Site Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE_ID))

    ' Generated datetime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    ' Alarm Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_ID).Value = DbRow.Value(SQL_COLUMN_ALARM_ID)

    ' Severity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).Value = DecodeSeverity(DbRow.Value(SQL_COLUMN_SEVERITY))

    If m_alarms_dic.ContainsKey(DbRow.Value(SQL_COLUMN_ALARM_CODE)) Then
      ' Alarm Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_GROUP).Value = m_alarms_dic.Item(DbRow.Value(SQL_COLUMN_ALARM_CODE)).GroupName

      ' Alarm Category
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CATEGORY).Value = m_alarms_dic.Item(DbRow.Value(SQL_COLUMN_ALARM_CODE)).CategoryName
    Else
      ' Alarm Group
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_GROUP).Value = ""

      ' Alarm Category
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CATEGORY).Value = ""
    End If

    'Alarm Code Description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_CODE_DESCRIPTION).Value = Hex(DbRow.Value(SQL_COLUMN_ALARM_CODE)) + " - " + DbRow.Value(SQL_COLUMN_ALARM_CODE_DESCRIPTION)

    ' Source code
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOURCE_CODE).Value = DbRow.Value(SQL_COLUMN_SOURCE_CODE)

    ' Source name
    _terminal = DbRow.Value(SQL_COLUMN_SOURCE_NAME)
    _split = _terminal.Split("@")

    ' Check if it's a Cashier terminal (user@terminal)
    If (DbRow.Value(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.Cashier _
       Or DbRow.Value(SQL_COLUMN_SOURCE_CODE) = AlarmSourceCode.User) _
       AndAlso _split.Length = 2 Then

      'Check if it's like " - Autorizado por"
      _pattern = " - "
      _split2 = Regex.Split(_split(1), _pattern)

      If _split2.Length > 1 Then
        _terminal = _split(0) & "@" & Computer.Alias(_split2(0)) & _pattern
        For _idx As Integer = 1 To _split2.Length - 1
          _terminal &= _split2(_idx)
        Next
      Else
        _terminal = _split(0) & "@" & Computer.Alias(_split(1))
      End If
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SOURCE_NAME).Value = _terminal

    ' Alarm description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ALARM_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_ALARM_DESCRIPTION)

    ' Ack datetime
    If Not DbRow.IsNull(SQL_COLUMN_ACK_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ACK_DATETIME), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_DATETIME).Value = ""
    End If

    ' Ack user name
    If Not DbRow.IsNull(SQL_COLUMN_ACK_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_USER_NAME).Value = DbRow.Value(SQL_COLUMN_ACK_USER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACK_USER_NAME).Value = ""
    End If

    'technicia id
    If Not DbRow.IsNull(SQL_COLUMN_TECHNICIAN_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TECHNICIAN_ID).Value = DbRow.Value(SQL_COLUMN_TECHNICIAN_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TECHNICIAN_ID).Value = ""
    End If

    'technicia name
    If Not DbRow.IsNull(SQL_COLUMN_TECHNICIAN_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TECHNICIAN_NAME).Value = IIf(DbRow.Value(SQL_COLUMN_TECHNICIAN_ID) = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6520), DbRow.Value(SQL_COLUMN_TECHNICIAN_NAME))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TECHNICIAN_NAME).Value = ""
    End If

    Select Case DbRow.Value(SQL_COLUMN_SEVERITY)
      Case WSI.Common.AlarmSeverity.Info
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(COLOR_INFO_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).BackColor = GetColor(COLOR_INFO_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).ForeColor = GetColor(COLOR_INFO_FORE)

      Case WSI.Common.AlarmSeverity.Warning
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(COLOR_WARNING_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).BackColor = GetColor(COLOR_WARNING_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).ForeColor = GetColor(COLOR_WARNING_FORE)

      Case WSI.Common.AlarmSeverity.Error
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(COLOR_ERROR_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).BackColor = GetColor(COLOR_ERROR_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SEVERITY).ForeColor = GetColor(COLOR_ERROR_FORE)
    End Select

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        GUI_AckAlarms()
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        InquirySourcePatterns()
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Execute_Patterns()
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_first_time = False
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
        Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AAUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean
    Select Case AuditType
      Case AUDIT_FLAGS.SEARCH
        Return Me.m_monitor_enabled.Equals(False) Or (Me.m_monitor_enabled.Equals(True) And Me.m_num_of_refreshes < 1)
      Case Else
        Return MyBase.GUI_HasToBeAudited(AuditType)
    End Select
  End Function ' GUI_HasToBeAudited

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    Call RowSelect(SelectedRow)

  End Sub  '  GUI_RowSelectedEvent

  Private Sub RowSelect(ByVal SelectedRow As Integer)
    Dim _grid_value As Int32

    If opt_history_mode.Checked Then
      _grid_value = 0

      If IsNumeric(Me.Grid.Cell(SelectedRow, GRID_COLUMN_SOURCE_CODE).Value) Then
        _grid_value = CInt(Me.Grid.Cell(SelectedRow, GRID_COLUMN_SOURCE_CODE).Value)
      End If

      ' Habilitar botones con el permiso
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (_grid_value = WSI.Common.AlarmSourceCode.Pattern) And CurrentUser.Permissions(ENUM_FORM.FORM_ALARM_INQUIRY_SOURCE).Read
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = Me.Grid.NumRows > 0 And CurrentUser.Permissions(ENUM_FORM.FORM_ALARM_EXECUTE_PATTERNS).Execute
    End If

  End Sub   ' RowSelect

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(416), m_severity)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(202), m_code)
    If m_is_center OrElse m_is_cashdeskdraw OrElse m_is_sites_alarms Then
      PrintData.SetFilter("", "", True)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    End If
    If m_is_sites_alarms Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927), m_sites)
    End If

    If WSI.Common.Misc.IsAGGEnabled AndAlso m_is_sites_alarms Then
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    End If

    If opt_monitor_mode.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(204), m_monitoring)
    End If
    If uc_checked_list_alarms_catalog.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710), m_selected_alarms)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6522), m_users)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5711), m_alarm_desc)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5707), m_alarm_id)



    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _no_one As Boolean
    Dim _all As Boolean
    Dim _date_from As String = m_date_from
    Dim _date_to As String = m_date_to
    Dim _severity As String = m_severity
    Dim _code As String = m_code
    Dim _monitoring As String = m_monitoring
    Dim _terminals As String = m_terminals
    Dim _idx_rows As Integer
    Dim _all_checked As Boolean
    Dim _counter As Integer

    m_date_from = ""
    m_date_to = ""
    m_severity = ""
    m_terminals = ""
    m_alarm_types = ""
    m_monitoring = ""
    m_users = ""
    m_sites = ""
    m_alarm_desc = ""
    m_alarm_id = ""
    m_code = ""
    m_selected_alarms = ""

    If Me.ef_alarm_id.Value <> "" Then
      m_alarm_id = Me.ef_alarm_id.Value
    Else
      ' Date 
      If Me.gb_date.Enabled Then
        If Me.dtp_from.Checked Then
          m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                       ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                       ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If

        If Me.dtp_to.Checked Then
          m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                     ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
      End If

      ' Severity
      _no_one = True
      _all = True
      If chk_info.Checked Then
        m_severity = m_severity & Me.chk_info.Text & ", "
        _no_one = False
      Else
        _all = False
      End If

      If chk_warning.Checked Then
        m_severity = m_severity & Me.chk_warning.Text & ", "
        _no_one = False
      Else
        _all = False
      End If

      If chk_error.Checked Then
        m_severity = m_severity & Me.chk_error.Text & ", "
        _no_one = False
      Else
        _all = False
      End If

      If m_severity.Length <> 0 Then
        m_severity = Strings.Left(m_severity, Len(m_severity) - 2)
      End If

      If _no_one Or _all Then
        m_severity = GLB_NLS_GUI_ALARMS.GetString(277)
      End If

      ' Users
      If chk_user_filter.Checked Then
        If Me.opt_all_users.Checked Then
          m_users = Me.opt_all_users.Text
        ElseIf Me.opt_none.Checked Then
          m_users = Me.opt_none.Text
        Else
          m_users = Me.cmb_user.TextValue
        End If
      End If

      ' Providers - Terminals
      m_terminals = Me.uc_pr_list.GetTerminalReportText()

      ' 19-FEB-2014  JPJ    Defect WIG-485, WIG-518: Searches can't be done
      If m_terminals Is Nothing Then m_terminals = ""

      ' Source event
      m_alarm_types = Me.gb_sources.Text()

      _all_checked = True

      For _idx_rows = 0 To dg_sources.NumRows - 1
        If Not dg_sources.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then

          _all_checked = False
          Exit For

        End If
      Next

      If _all_checked = True Then
        m_code = GLB_NLS_GUI_AUDITOR.GetString(263)
      Else
        m_code = ""
        For _idx_rows = 0 To dg_sources.NumRows - 1
          If dg_sources.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
            m_code = m_code & dg_sources.Cell(_idx_rows, GRID_2_COLUMN_DESC).Value.Trim & "; "

            _counter = _counter + 1
            If _counter > MAX_FILTER_REPORT Then
              m_code = GLB_NLS_GUI_INVOICING.GetString(223)
              Exit For
            End If
          End If
        Next
      End If

      If opt_monitor_mode.Checked Then
        m_monitoring = GLB_NLS_GUI_CONTROLS.GetString(3)
      Else
        m_monitoring = GLB_NLS_GUI_CONTROLS.GetString(4)
      End If

      If Not _date_from.Equals(m_date_from) _
         Or Not _date_to.Equals(m_date_to) _
         Or Not _severity.Equals(m_severity) _
         Or Not _code.Equals(m_code) _
         Or Not _monitoring.Equals(m_monitoring) _
         Or Not _terminals.Equals(m_terminals) Then
        m_num_of_refreshes = 0
      Else
        m_num_of_refreshes = m_num_of_refreshes + 1
      End If

      If Me.ef_alarm_desc.Value <> "" Then
        m_alarm_desc = Me.ef_alarm_desc.Value
      End If

      ' Groups
      If uc_checked_list_alarms_catalog.SelectedIndexes.Length = uc_checked_list_alarms_catalog.Count() Then
        m_selected_alarms = GLB_NLS_GUI_AUDITOR.GetString(263) 'All
      Else
        m_selected_alarms = uc_checked_list_alarms_catalog.SelectedValuesList()
      End If
    End If

    If m_is_sites_alarms Then
      m_sites = uc_site_select.GetSitesIdListSelectedToPrint()
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_ALARMS

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub


  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_INFO).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      .Counter(COUNTER_ERROR).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_ERROR).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_ERROR).ForeColor = GetColor(COLOR_ERROR_FORE)

      .Counter(COUNTER_WARNING).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_WARNING).BackColor = GetColor(COLOR_WARNING_BACK)
      .Counter(COUNTER_WARNING).ForeColor = GetColor(COLOR_WARNING_FORE)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Source Code
      .Column(GRID_COLUMN_SOURCE_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_SOURCE_CODE).Header(1).Text = ""
      .Column(GRID_COLUMN_SOURCE_CODE).Width = 0

      ' Timestamp
      .Column(GRID_COLUMN_TIMESTAMP).Header(0).Text = ""
      .Column(GRID_COLUMN_TIMESTAMP).Header(1).Text = ""
      .Column(GRID_COLUMN_TIMESTAMP).Width = 0

      ' Alarm Id
      If m_is_sites_alarms Then
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
        .Column(GRID_COLUMN_SITE_ID).Width = 850
        .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Header(1).Text = ""
        .Column(GRID_COLUMN_SITE_ID).Width = 0
      End If

      ' Date Generated
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_DATETIME).Width = 2100
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Alarm Id
      .Column(GRID_COLUMN_ALARM_ID).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5707)
      .Column(GRID_COLUMN_ALARM_ID).Width = 1200

      ' Severity
      .Column(GRID_COLUMN_SEVERITY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_SEVERITY).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(416)
      .Column(GRID_COLUMN_SEVERITY).Width = 1100
      .Column(GRID_COLUMN_SEVERITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Alarm group
      .Column(GRID_COLUMN_ALARM_GROUP).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_GROUP).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(462)
      .Column(GRID_COLUMN_ALARM_GROUP).Width = 1500
      .Column(GRID_COLUMN_ALARM_GROUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Alarm category
      .Column(GRID_COLUMN_ALARM_CATEGORY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_CATEGORY).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(463)
      .Column(GRID_COLUMN_ALARM_CATEGORY).Width = 2000
      .Column(GRID_COLUMN_ALARM_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Alarm Code Description
      .Column(GRID_COLUMN_ALARM_CODE_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_CODE_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
      .Column(GRID_COLUMN_ALARM_CODE_DESCRIPTION).Width = 3100
      .Column(GRID_COLUMN_ALARM_CODE_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Source
      .Column(GRID_COLUMN_SOURCE_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_SOURCE_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(209)
      .Column(GRID_COLUMN_SOURCE_NAME).Width = 4000
      .Column(GRID_COLUMN_SOURCE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(207)
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(269)
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Width = 5200
      .Column(GRID_COLUMN_ALARM_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ack Datetime
      .Column(GRID_COLUMN_ACK_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
      .Column(GRID_COLUMN_ACK_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_ACK_DATETIME).Width = 2100
      .Column(GRID_COLUMN_ACK_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ack Username
      .Column(GRID_COLUMN_ACK_USER_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
      .Column(GRID_COLUMN_ACK_USER_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(267)
      .Column(GRID_COLUMN_ACK_USER_NAME).Width = 20000
      .Column(GRID_COLUMN_ACK_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Technician id
      .Column(GRID_COLUMN_TECHNICIAN_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6522)
      .Column(GRID_COLUMN_TECHNICIAN_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5707)
      .Column(GRID_COLUMN_TECHNICIAN_ID).Width = 2100

      ' Technician name
      .Column(GRID_COLUMN_TECHNICIAN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6522)
      .Column(GRID_COLUMN_TECHNICIAN_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(267)
      .Column(GRID_COLUMN_TECHNICIAN_NAME).Width = 2100
      .Column(GRID_COLUMN_TECHNICIAN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    Me.dtp_from.Checked = True

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    'Me.uc_pr_list.Width = 330
    Call Me.uc_pr_list.SetDefaultValues()

    ' Catalog Filter
    Call Me.uc_checked_list_alarms_catalog.CollapseAll()
    Call Me.uc_checked_list_alarms_catalog.SetDefaultValue(True)

    'User
    Me.chk_user_filter.Checked = False
    Me.opt_one_user.Enabled = False
    Me.opt_all_users.Enabled = False
    Me.opt_none.Enabled = False
    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    Me.chk_info.Checked = True
    Me.chk_warning.Checked = True
    Me.chk_error.Checked = True

    Me.opt_history_mode.Checked = True

    Me.uc_site_select.SetDefaultValues(False)

    Call btn_check_all_sources_Click()

    Me.ef_alarm_desc.Value = ""
    Me.ef_alarm_id.Value = ""


    If WSI.Common.Misc.IsAGGEnabled() AndAlso m_is_center Then
      'Set default value of uc_sites_sel here!
      Dim _dt_sites As DataTable = Me.uc_site_select.GetSitesDataTable()
      If Not _dt_sites Is Nothing AndAlso Not _dt_sites.Rows.Count = 0 Then
        Me.uc_site_select.SetSitesIdListSelected(_dt_sites.Rows(0).Item(SQL_COLUMN_SITE_ID))
      End If
    End If

    Call EnableControls(True)
  End Sub ' SetDefaultValues

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillAlarmsCatalog()

    m_alarms_dic = New Dictionary(Of Integer, ALARMS_CATALOG)
    Dim _alarm_id As Nullable(Of Int64) = Nothing

    If m_is_cashdeskdraw Then
      Return
    End If

    Try

      If Not GetAlarmsCatalog(True, _alarm_id, m_alarms_dic, Language.GetWigosGUILanguageId()) Then
        Debug.WriteLine(GLB_NLS_GUI_ALARMS.GetString(466))

        Return
      End If

      Call FillAlarmsGroupsControl(m_alarms_dic, uc_checked_list_alarms_catalog)

      Call uc_checked_list_alarms_catalog.ResizeGrid()
    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' FillAlarmsCatalog

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _source_code As String
    Dim _str_filter_site As String
    Dim _alarm_codes As String
    Dim _provider_list As String
    Dim _user_code As String
    Dim _severity As String

    m_source_list_no_terminal = String.Empty
    m_source_list_terminal = String.Empty

    _str_where = String.Empty
    _source_code = String.Empty
    _str_filter_site = String.Empty
    _alarm_codes = String.Empty
    _provider_list = String.Empty
    _user_code = String.Empty
    _severity = String.Empty

    ' Filter Dates
    If Me.gb_date.Enabled Then
      If Me.dtp_from.Checked = True Then
        _str_where = _str_where & " AND XX_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & " "
      End If

      If Me.dtp_to.Checked = True Then
        _str_where = _str_where & " AND XX_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & " "
      End If
    End If

    If gb_sources.Enabled Then

      Call GetSourceListSelected()

      _str_where = _str_where & " AND ( XX_SOURCE_CODE IN (" & m_source_list_no_terminal & ") "

      If Not m_is_center AndAlso Not m_is_cashdeskdraw OrElse
         (m_is_center AndAlso WSI.Common.Misc.IsAGGEnabled AndAlso m_is_sites_alarms) Then

        If m_enable_pr_list Then

          If Me.uc_pr_list.AllSelectedChecked Then
            _provider_list = ")  ) "
          Else
            _provider_list = ") AND XX_SOURCE_ID IN " & Me.uc_pr_list.GetProviderIdListSelected() & " ) "
          End If

          _str_where = _str_where & " OR (XX_SOURCE_CODE IN (" & m_source_list_terminal & _provider_list
        End If

      ElseIf m_is_sites_alarms Then

        _str_where = _str_where & " OR (XX_SOURCE_CODE IN (" & m_source_list_terminal & ")  ) "

      End If

      _str_where = _str_where & " ) "
    End If

    ' Severity
    If gb_severity.Enabled Then
      _severity = GetSeverityIdListSelected()
    End If

    'if all items are selected or all items unselected then XX_Severity is not needed in the Where Clause
    'otherwise include those selected
    If _severity.Length > 0 Then
      _str_where = _str_where & " AND XX_SEVERITY IN (" & _severity & ") "
    End If

    If opt_monitor_mode.Checked Then
      ' Last MONITOR_PERIOD days
      _str_where = _str_where & "  AND XX_DATETIME >= " & GUI_FormatDateDB(WSI.Common.Misc.TodayOpening().AddDays(-MONITOR_PERIOD_DAYS)) & " "
      If Not (WSI.Common.Misc.IsAGGEnabled() AndAlso m_is_sites_alarms) Then
        _str_where = _str_where & "  AND XX_ACK_DATETIME IS NULL "
      End If
    End If

    'Sites
    If m_is_sites_alarms Then
      _str_filter_site = uc_site_select.GetSitesIdListSelected()

      If (_str_filter_site <> String.Empty) Then
        _str_where = _str_where & " AND XX_SITE_ID IN (" & _str_filter_site & ")"

      End If
    End If

    If Me.ef_alarm_desc.Value <> "" Then
      _str_where = _str_where & " AND " & GUI_FilterField("XX_ALARM_DESCRIPTION", Me.ef_alarm_desc.Value, False, False, True)
    End If
    If Me.ef_alarm_id.Value <> "" Then
      _str_where = _str_where & " AND XX_ALARM_ID = " & Me.ef_alarm_id.Value
    End If

    'Alarm codes
    If Not Me.uc_checked_list_alarms_catalog.IsAllOrNoneSelected And Me.uc_checked_list_alarms_catalog.Enabled Then
      _alarm_codes = Me.uc_checked_list_alarms_catalog.SelectedIndexesList()
    End If

    If _alarm_codes.Length > 0 Then
      If _alarm_codes.Contains(START_RANGE_GAME_ALARMS) Then
        _str_where = _str_where & " AND ( XX_ALARM_CODE IN ( " & _alarm_codes & " ) " & _
                                  " OR ( XX_ALARM_CODE >= " & START_RANGE_GAME_ALARMS & " AND XX_ALARM_CODE <= " & END_RANGE_GAME_ALARMS & " ))"
      Else
        _str_where = _str_where & " AND XX_ALARM_CODE IN ( " & _alarm_codes & " ) "
      End If
    End If

    'User
    'one user
    If Me.opt_one_user.Checked And Me.chk_user_filter.Checked Then
      _str_where = _str_where & " AND (XX_TECHNICIAN_ID = " & Me.cmb_user.Value & ") "
    End If

    'all users
    If Me.opt_all_users.Checked And Me.chk_user_filter.Checked Then
      _str_where = _str_where & "AND (XX_TECHNICIAN_ID IS NOT NULL)"
    End If

    'none users
    If Me.opt_none.Checked And Me.chk_user_filter.Checked Then
      _str_where = _str_where & "AND (XX_TECHNICIAN_ID IS NULL) "
    End If

    ' Change first "AND" by "WHERE"
    If _str_where <> "" Then
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Get Severity Id list for selected severities
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSeverityIdListSelected() As String
    Dim list_type As String
    Dim no_one As Boolean

    no_one = True

    list_type = ""

    If chk_info.Checked Then
      If list_type.Length = 0 Then
        list_type = WSI.Common.AlarmSeverity.Info
        no_one = False
      Else
        list_type = list_type & ", " & WSI.Common.AlarmSeverity.Info
        no_one = False
      End If
    End If

    If chk_warning.Checked Then
      If list_type.Length = 0 Then
        list_type = WSI.Common.AlarmSeverity.Warning
        no_one = False
      Else
        list_type = list_type & ", " & WSI.Common.AlarmSeverity.Warning
        no_one = False
      End If
    End If

    If chk_error.Checked Then
      If list_type.Length = 0 Then
        list_type = WSI.Common.AlarmSeverity.Error
        no_one = False
      Else
        list_type = list_type & ", " & WSI.Common.AlarmSeverity.Error
        no_one = False
      End If
    End If

    'Bug 6650:Reporte de Alarmas - Filtro devuelve todos los registros y hace select innecesario
    'For SQL All Checked is the Same as None Checked, we don't need to include it in the SQL
    If (chk_info.Checked And chk_warning.Checked And chk_error.Checked) Or no_one Then
      list_type = ""
    End If

    'If no_one Then
    '  list_type = WSI.Common.AlarmSeverity.Info
    '  list_type = list_type & ", " & WSI.Common.AlarmSeverity.Warning
    '  list_type = list_type & ", " & WSI.Common.AlarmSeverity.Error
    'End If

    Return list_type
  End Function 'GetSeverityIdListSelected

  ' PURPOSE: Enable/Disable mode related controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableMonitorOrHistory()

    If opt_monitor_mode.Checked Then
      gb_date.Enabled = False
      m_monitor_enabled = True
      lbl_latest_alarms.Visible = True
      lbl_no_data.Visible = False
      Me.Grid.Sortable = False
      m_first_time = True
      Call tmr_monitor.Start()

      Me.Grid.Redraw = False

      With Me.Grid
        ' Description
        .Column(GRID_COLUMN_ALARM_DESCRIPTION).Width = 8400

        ' Ack Datetime
        .Column(GRID_COLUMN_ACK_DATETIME).Header(0).Text = ""
        .Column(GRID_COLUMN_ACK_DATETIME).Header(1).Text = ""
        .Column(GRID_COLUMN_ACK_DATETIME).Width = 1

        ' Ack Username
        .Column(GRID_COLUMN_ACK_USER_NAME).Header(0).Text = ""
        .Column(GRID_COLUMN_ACK_USER_NAME).Header(1).Text = ""
        .Column(GRID_COLUMN_ACK_USER_NAME).Width = 1
      End With

      tf_last_update.Visible = True

      Call tmr_monitor_Tick(Nothing, Nothing)

      Me.Grid.Redraw = True

    ElseIf opt_history_mode.Checked AndAlso gb_mode.Enabled Then
      gb_date.Enabled = True
      m_monitor_enabled = False
      lbl_latest_alarms.Visible = False
      lbl_no_data.Visible = False
      Me.Grid.Sortable = True
      m_first_time = False
      Call tmr_monitor.Stop()

      Me.Grid.Redraw = False

      With Me.Grid
        ' Description
        .Column(GRID_COLUMN_ALARM_DESCRIPTION).Width = 5200

        ' Ack Datetime
        .Column(GRID_COLUMN_ACK_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
        .Column(GRID_COLUMN_ACK_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
        .Column(GRID_COLUMN_ACK_DATETIME).Width = 2100

        ' Ack Username
        .Column(GRID_COLUMN_ACK_USER_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(208)
        .Column(GRID_COLUMN_ACK_USER_NAME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(267)
        .Column(GRID_COLUMN_ACK_USER_NAME).Width = 2000
      End With

      Me.Grid.Redraw = True

    End If

    m_init = False

  End Sub ' EnableMonitorOrHistory

  ' PURPOSE: Refresh the alarms grid information
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshGrid()
    m_first_time = False
    GUI_ExecuteQuery()
  End Sub ' RefreshGrid

  ' PURPOSE: Indicates if the indicated row of the Grid has changed and must be repainted.
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxGrid
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Private Function DataGridMustChange(ByVal IdxGrid As Integer, ByVal Row As DataRow) As Boolean

    If Me.Grid.NumRows - 1 < IdxGrid Then
      Return True
    End If

    ' Check timestamp field
    If Me.Grid.Cell(IdxGrid, GRID_COLUMN_TIMESTAMP).Value <> Row(SQL_COLUMN_TIMESTAMP) Then
      Return True
    End If

    Return False
  End Function ' DataGridMustChange

  ' PURPOSE: Return the String representation of an AlarmSeverity.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Id
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function DecodeSeverity(ByVal Id As WSI.Common.AlarmSeverity) As String

    Dim result As String

    result = ""
    Select Case Id
      Case WSI.Common.AlarmSeverity.Info
        result = GLB_NLS_GUI_ALARMS.GetString(206)
      Case WSI.Common.AlarmSeverity.Warning
        result = GLB_NLS_GUI_ALARMS.GetString(358)
      Case WSI.Common.AlarmSeverity.Error
        result = GLB_NLS_GUI_ALARMS.GetString(357)
    End Select

    Return result
  End Function ' DecodeSeverity

  ' PURPOSE: Acknowledge all the selected alarms from the Grid.
  '          It has in mind that we can be in monitor mode.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_AckAlarms()
    Dim tmr_started As Boolean

    ' Don't allow double-click.
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    tmr_started = True

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      If opt_monitor_mode.Checked Then
        Me.tmr_monitor.Stop()
        tmr_started = False
      End If

      AckAlarms()

      If opt_monitor_mode.Checked Then
        SleepWithDoEvents(1000)
        If Me.IsDisposed Then
          Return
        End If
        RefreshGrid()
        Me.tmr_monitor.Start()
        tmr_started = True
      End If

    Catch ex As Exception
      If Me.IsDisposed Then
        Return
      End If
      Debug.WriteLine(ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(102), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally
      If Not Me.IsDisposed Then
        If Not tmr_started Then
          Me.tmr_monitor.Start()
        End If

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (Me.Grid.NumRows > 0 And Me.Permissions.Write)
      End If
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try
  End Sub ' GUI_AckAlarms

  ' PURPOSE: Acknowledge all the selected alarms from the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AckAlarms()
    Dim idx_row As Integer
    Dim select_idx As Collection
    Dim has_errors As Boolean
    Dim now As Date

    ' If CurrentRowIndex = -1, there is no row selected.
    If Me.Grid.CurrentRow = -1 Then
      Return
    End If

    select_idx = New Collection()
    has_errors = False

    Try
      ' Get selected alarms
      For idx_row = 0 To Me.Grid.NumRows - 1
        If Me.Grid.IsSelected(idx_row) Then
          select_idx.Add(idx_row)
        End If
      Next

      ' No alarm selected
      If select_idx.Count = 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(103), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return
      End If

      now = WSI.Common.WGDB.Now

      For Each idx_row In select_idx
        ' If already ack'ed
        If Me.Grid.Cell(idx_row, GRID_COLUMN_ACK_DATETIME).Value <> "" Then
          Continue For
        End If

        If Not AckAlarm(Me.Grid.Cell(idx_row, GRID_COLUMN_ALARM_ID).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_ALARM_DESCRIPTION).Value, _
                        now, CurrentUser.Id, CurrentUser.Name) Then
          has_errors = True

          Exit For
        Else
          Me.Grid.Cell(idx_row, GRID_COLUMN_ACK_USER_NAME).Value = CurrentUser.Name
          Me.Grid.Cell(idx_row, GRID_COLUMN_ACK_DATETIME).Value = GUI_FormatDate(now, _
                                                                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
      Next

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      has_errors = True
      Return

    Finally
      If has_errors Then
        Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(104), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try
  End Sub ' AckAlarms

  ' PURPOSE: Acknowledge the indicated alarm and audit the operation with the user that has done it.
  '
  '  PARAMS:
  '     - INPUT:
  '           - AlarmId
  '           - AlarmDescription
  '           - DateNow
  '           - UserId
  '           - UserName
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if ack has been done correctly. False otherwise.
  '
  Private Function AckAlarm(ByVal AlarmId As Int64, _
                            ByVal AlarmDescription As String, _
                            ByVal DateNow As Date, _
                            ByVal UserId As Integer, _
                            ByVal UserName As String) As Boolean
    Dim sql_str As String
    Dim auditor_data As CLASS_AUDITOR_DATA

    Try
      Using db_trx As WSI.Common.DB_TRX = New WSI.Common.DB_TRX()
        sql_str = "UPDATE   ALARMS " & _
                  "   SET   AL_ACK_DATETIME  = @pDatetime " & _
                  "       , AL_ACK_USER_ID   = @pUserId " & _
                  "       , AL_ACK_USER_NAME = @pUserName" & _
                  " WHERE   AL_ALARM_ID      = @pAlarmId"

        Using sql_cmd As SqlCommand = New SqlCommand(sql_str)
          sql_cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = DateNow
          sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId
          sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = UserName
          sql_cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).Value = AlarmId

          If db_trx.ExecuteNonQuery(sql_cmd) <> 1 Then
            Return False
          End If
        End Using

        db_trx.Commit()
      End Using

      ' Audit
      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ALARMS)
      Call auditor_data.SetName(GLB_NLS_GUI_ALARMS.Id(212), GLB_NLS_GUI_ALARMS.GetString(213, AlarmDescription))
      Call auditor_data.SetField(GLB_NLS_GUI_ALARMS.Id(214), DateNow)
      Call auditor_data.SetField(GLB_NLS_GUI_ALARMS.Id(215), UserId.ToString() & "-" & UserName)
      Call auditor_data.Notify(CurrentUser.GuiId, _
                                 CurrentUser.Id, _
                                 CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                 0)

      Return True
    Catch ex As Exception
      Return False
    End Try
  End Function ' AckAlarm

  ' PURPOSE: Sleep the indicated Time doing DoEvents.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Time
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SleepWithDoEvents(ByVal Time As Integer)
    Dim tick0 As Integer

    tick0 = WSI.Common.Misc.GetTickCount()
    While WSI.Common.Misc.GetElapsedTicks(tick0) < Time
      System.Threading.Thread.Sleep(10)
      Application.DoEvents()
    End While
  End Sub ' SleepWithDoEvents

  ' PURPOSE: Save the selected Alarm Ids from the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SaveSelectedIds()
    Dim idx As Integer

    If m_selected_ids IsNot Nothing Then
      m_selected_ids.Clear()
      m_selected_ids = Nothing
    End If

    m_selected_ids = New List(Of Integer)
    idx = 0
    While idx < Me.Grid.NumRows
      If Me.Grid.Row(idx).IsSelected Then
        m_selected_ids.Add(Me.Grid.Cell(idx, GRID_COLUMN_ALARM_ID).Value)
      End If
      idx = idx + 1
    End While
  End Sub ' SaveSelectedIds

  ' PURPOSE: Select in the Grid the latest selected Alarm Ids that have been saved.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SelectLastSelectedRow()
    Dim idx As Integer
    Dim any_selected As Boolean

    If Me.Grid.NumRows = 0 Then
      Return
    End If
    If m_selected_ids Is Nothing Then
      Return
    End If
    If m_selected_ids.Count = 0 Then
      Return
    End If

    any_selected = False
    Me.Grid.Redraw = False

    Try
      idx = 0
      While idx < Me.Grid.NumRows
        Me.Grid.Row(idx).IsSelected = False
        Call Me.Grid.RepaintRow(idx)
        idx = idx + 1
      End While

      idx = 0
      While idx < Me.Grid.NumRows
        If m_selected_ids.Contains(Me.Grid.Cell(idx, GRID_COLUMN_ALARM_ID).Value) Then
          any_selected = True
          Me.Grid.Row(idx).IsSelected = True
          Call Me.Grid.RepaintRow(idx)
        End If
        idx = idx + 1
      End While

      If Not any_selected And Me.Grid.CurrentRow >= 0 Then
        Me.Grid.Row(Me.Grid.CurrentRow).IsSelected = True
        Call Me.Grid.RepaintRow(Me.Grid.CurrentRow)
      End If

    Finally
      Me.Grid.Redraw = True
    End Try
  End Sub ' SelectLastSelectedRow

  ' PURPOSE: Increment the indicated Severity counter.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Severity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub UpdateCounters(ByVal Severity As WSI.Common.AlarmSeverity)
    Select Case Severity
      Case WSI.Common.AlarmSeverity.Info
        m_counter_list(COUNTER_INFO) = m_counter_list(COUNTER_INFO) + 1

      Case WSI.Common.AlarmSeverity.Warning
        m_counter_list(COUNTER_WARNING) = m_counter_list(COUNTER_WARNING) + 1

      Case WSI.Common.AlarmSeverity.Error
        m_counter_list(COUNTER_ERROR) = m_counter_list(COUNTER_ERROR) + 1
    End Select
  End Sub ' UpdateCounters

  ' PURPOSE: Show new window with the information of the alarms that generated it.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Severity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub InquirySourcePatterns()
    Dim _idx_row As Short
    Dim _alarm_id As Integer
    Dim _site_id As Int64
    Dim _source_id As Int64
    Dim frm_alarm_inquiry_source As frm_alarm_inquiry_source

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    If Not (Me.Grid.Cell(_idx_row, GRID_COLUMN_SOURCE_CODE).Value = AlarmSourceCode.Pattern) Then
      Call NLS_MsgBox(GLB_NLS_GUI_ALARMS.Id(467), ENUM_MB_TYPE.MB_TYPE_INFO)

      Return
    End If

    If m_is_sites_alarms Then
      _site_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value
    End If
    _alarm_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ALARM_ID).Value
    _source_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SOURCE_CODE).Value

    frm_alarm_inquiry_source = New frm_alarm_inquiry_source
    Call frm_alarm_inquiry_source.ShowForEdit(Me.MdiParent, _alarm_id, m_is_sites_alarms, _site_id, _source_id)
    frm_alarm_inquiry_source = Nothing

    '    Call Me.Grid.Focus()

  End Sub ' InquirySourcePatterns

  ' PURPOSE: Increment the indicated Severity counter.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Severity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub Execute_Patterns()
    Dim frm_alarm_execute_patterns As frm_alarm_execute_patterns

    If Me.Grid.NumRows = 0 Then
      Return
    End If

    frm_alarm_execute_patterns = New frm_alarm_execute_patterns
    Call frm_alarm_execute_patterns.ShowForEdit(Me.MdiParent, m_is_sites_alarms, m_table, m_alarms_dic)
    frm_alarm_execute_patterns = Nothing

  End Sub ' Execute_Patterns

  Private Sub EnableControls(ByVal Value As Boolean)
    'uc_site_select.Enabled = Value
    gb_date.Enabled = Value
    gb_severity.Enabled = Value
    gb_mode.Enabled = Value
    gb_sources.Enabled = Value
    ef_alarm_desc.Enabled = Value
    uc_pr_list.Enabled = Value
    uc_checked_list_alarms_catalog.Enabled = Value
  End Sub

#Region " Grid Source Event "

  ' PURPOSE: Define all Grid Source Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetDeviceOperation()
    With Me.dg_sources
      Call .Init(GRID_3_COLUMNS, GRID_3_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_3_COLUMN_CODE).Header.Text = ""
      .Column(GRID_3_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_3_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_3_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_3_COLUMN_CHECKED).Fixed = True
      .Column(GRID_3_COLUMN_CHECKED).Editable = True
      .Column(GRID_3_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_3_COLUMN_DESC).Header.Text = ""
      .Column(GRID_3_COLUMN_DESC).WidthFixed = 2400
      .Column(GRID_3_COLUMN_DESC).Fixed = True
      .Column(GRID_3_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_3_COLUMN_TYPE).Header.Text = ""
      .Column(GRID_3_COLUMN_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetType

  ' PURPOSE: Get envents
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Events SOURCE_EVENT()
  Public Function GetSourceFilters() As SOURCE_EVENT()
    Dim _events() As SOURCE_EVENT
    Dim _idx As Integer = 0

    ReDim _events(System.Enum.GetValues(GetType(AlarmSourceCode)).Length)

    For Each item As Int32 In System.Enum.GetValues(GetType(AlarmSourceCode))

      If m_is_center And Not m_is_sites_alarms Then

        Select Case item
          Case AlarmSourceCode.User
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 218
            _idx = _idx + 1
          Case AlarmSourceCode.Service
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 224
            _idx = _idx + 1
          Case AlarmSourceCode.System
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 225
            _idx = _idx + 1
          Case AlarmSourceCode.MultiSite
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 227
            _idx = _idx + 1
          Case AlarmSourceCode.ELP01
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 228
            _idx = _idx + 1
        End Select

      ElseIf m_is_cashdeskdraw Then

        Select Case item

          Case AlarmSourceCode.User
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 218
            _idx = _idx + 1

          Case AlarmSourceCode.Service
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 224
            _idx = _idx + 1

          Case AlarmSourceCode.System
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 225
            _idx = _idx + 1

        End Select

      Else

        Select Case item
          Case AlarmSourceCode.Cashier
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 217
            _idx = _idx + 1
          Case AlarmSourceCode.User
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 218
            _idx = _idx + 1
          Case AlarmSourceCode.TerminalSASHost
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 219
            _idx = _idx + 1
          Case AlarmSourceCode.TerminalSASMachine
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 220
            _idx = _idx + 1
          Case AlarmSourceCode.TerminalSystem
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 221
            _idx = _idx + 1
          Case AlarmSourceCode.TerminalWCP
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 222
            _idx = _idx + 1
          Case AlarmSourceCode.Terminal3GS
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 223
            _idx = _idx + 1
          Case AlarmSourceCode.Service
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 224
            _idx = _idx + 1
          Case AlarmSourceCode.System
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 225
            _idx = _idx + 1
          Case AlarmSourceCode.PSASendReports
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 226
            _idx = _idx + 1
          Case AlarmSourceCode.Pattern
            _events(_idx).source_type = WCP_SOURCE_EVENT_LEVEL_1
            _events(_idx).source_code = item
            _events(_idx).source_nls_id = 478
            _idx = _idx + 1

        End Select

      End If

    Next

    ' ANG 04-DES-2012: Resize _event array to currently events to show
    _idx = Math.Max(0, _idx - 1)
    ReDim Preserve _events(_idx)

    Return _events

  End Function ' GetSourceFilters

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillSourceFilterGrid()
    Dim _events() As SOURCE_EVENT

    _events = GetSourceFilters()

    Me.dg_sources.Clear()
    Me.dg_sources.Redraw = False

    For Each ev As SOURCE_EVENT In _events
      Call AddOneFilterRowSource(ev.source_code, ev.source_type, GLB_NLS_GUI_ALARMS.GetString(ev.source_nls_id))
    Next

    If Me.dg_sources.NumRows > 0 Then
      Me.dg_sources.CurrentRow = 0
    End If

    Me.dg_sources.Redraw = True

  End Sub  ' FillSourceFilterGrid

  ' PURPOSE: Add a Row of a event to the Event grid according to 
  '          the given code
  '
  '  PARAMS:
  '     - INPUT:
  '           - Code 
  '   
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneFilterRowSource(ByVal EventCode As Integer, ByVal EventType As Integer, ByVal EventName As String)
    Dim _prev_redraw As Boolean

    _prev_redraw = Me.dg_sources.Redraw
    Me.dg_sources.Redraw = False

    dg_sources.AddRow()

    Me.dg_sources.Redraw = False

    dg_sources.Cell(dg_sources.NumRows - 1, GRID_3_COLUMN_TYPE).Value = EventType
    dg_sources.Cell(dg_sources.NumRows - 1, GRID_3_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_sources.Cell(dg_sources.NumRows - 1, GRID_3_COLUMN_CODE).Value = EventCode
    dg_sources.Cell(dg_sources.NumRows - 1, GRID_3_COLUMN_DESC).Value = GRID_2_TAB & EventName

    Me.dg_sources.Redraw = _prev_redraw
  End Sub ' AddOneFilterRowDevice

  ' PURPOSE: Change checked value to all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim _idx As Integer
    Dim _type_id_str As String

    For _idx = FirstRow + 1 To dg_sources.NumRows - 1
      _type_id_str = Me.dg_sources.Cell(_idx, GRID_3_COLUMN_CODE).Value
      If _type_id_str.Length = 0 Then

        Exit Sub
      End If

      Me.dg_sources.Cell(_idx, GRID_3_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Uncheck all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeUncheckofRows(ByVal FirstRow As Integer)
    Dim _idx As Integer

    For _idx = FirstRow - 1 To 0 Step -1

      If Me.dg_sources.Cell(_idx, GRID_3_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then

        Exit Sub
      End If
    Next
  End Sub ' ChangeUncheckofRows

  ' PURPOSE: Get Event Id list for selected operation
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSourceListSelected() As String
    Dim _idx_row As Integer
    Dim _list_type As String
    Dim _no_one As Boolean
    _no_one = True

    _list_type = ""
    m_enable_pr_list = False

    m_source_list_terminal = ""
    m_source_list_no_terminal = ""

    For _idx_row = 0 To Me.dg_sources.NumRows - 1

      If dg_sources.Cell(_idx_row, GRID_3_COLUMN_TYPE).Value = WCP_SOURCE_EVENT_LEVEL_1 Then
        If dg_sources.Cell(_idx_row, GRID_3_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _
           dg_sources.Cell(_idx_row, GRID_3_COLUMN_CODE).Value.Length > 0 Then
          _no_one = False
          Call EnabledTerminalList(dg_sources.Cell(_idx_row, GRID_3_COLUMN_CODE).Value)
          _list_type = _list_type & ", " & dg_sources.Cell(_idx_row, GRID_3_COLUMN_CODE).Value
        End If
      End If
    Next

    If _no_one Then
      For _idx_row = 0 To Me.dg_sources.NumRows - 1
        Call EnabledTerminalList(dg_sources.Cell(_idx_row, GRID_3_COLUMN_CODE).Value)
        _list_type = _list_type & ", " & dg_sources.Cell(_idx_row, GRID_3_COLUMN_CODE).Value
      Next
    End If

    _list_type = _list_type.Substring(2, _list_type.Length - 2)

    If m_source_list_terminal.Length > 2 Then
      m_source_list_terminal = m_source_list_terminal.Substring(2, m_source_list_terminal.Length - 2)
    Else
      m_source_list_terminal = " -9999 "
    End If
    If m_source_list_no_terminal.Length > 2 Then
      m_source_list_no_terminal = m_source_list_no_terminal.Substring(2, m_source_list_no_terminal.Length - 2)
    Else
      m_source_list_no_terminal = " -9999 "
    End If

    Return _list_type
  End Function 'GetEventIdListSelected

  ' PURPOSE: Enable/Disabled PR List
  '          
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnabledTerminalList(ByVal sourceChecked As String)
    Dim _source_checked As Int32

    _source_checked = sourceChecked

    Select Case _source_checked
      Case WSI.Common.AlarmSourceCode.GameTerminal
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case WSI.Common.AlarmSourceCode.Terminal3GS
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case WSI.Common.AlarmSourceCode.TerminalSASHost
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case WSI.Common.AlarmSourceCode.TerminalSASMachine
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case WSI.Common.AlarmSourceCode.TerminalSystem
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case WSI.Common.AlarmSourceCode.TerminalWCP
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case WSI.Common.AlarmSourceCode.Pattern
        m_enable_pr_list = True
        m_source_list_terminal = m_source_list_terminal & ", " & sourceChecked

      Case Else
        m_source_list_no_terminal = m_source_list_no_terminal & ", " & sourceChecked

    End Select

    uc_pr_list.Enabled = m_enable_pr_list AndAlso (ef_alarm_id.Value = "") ' if there is an alarm id defined , uc_pr_list can' t be enabled

  End Sub 'EnablePrList

#End Region ' Grid Source Event

#End Region  ' Private Functions

#Region " Events "

  Private Sub opt_monitor_mode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_monitor_mode.CheckedChanged
    If opt_monitor_mode.Checked Then
      Call EnableMonitorOrHistory()
    Else
      tf_last_update.Visible = False
      RowSelect(Me.Grid.CurrentRow)
    End If
    ef_alarm_id.Enabled = Not opt_monitor_mode.Checked
  End Sub ' opt_monitor_mode_CheckedChanged

  Private Sub opt_history_mode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_history_mode.CheckedChanged
    If opt_history_mode.Checked Then
      Call EnableMonitorOrHistory()
    End If
  End Sub ' opt_history_mode_CheckedChanged

  Private Sub tmr_monitor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_monitor.Tick
    If opt_monitor_mode.Checked Then
      Call GUI_ExecuteQueryCustom()
      Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
  End Sub ' tmr_monitor_Tick

  ' Source Event
  Private Sub dg_sources_DataSelectedEvent() Handles dg_sources.DataSelectedEvent

    Dim idx As Integer
    Dim last_row As Integer

    With Me.dg_sources

      If .CurrentCol <> GRID_3_COLUMN_CHECKED Then
        last_row = .CurrentRow
        For idx = .CurrentRow + 1 To .NumRows - 1
          last_row = idx
        Next

      End If
    End With
  End Sub ' dg_sources_DataSelectedEvent

  Private Sub dg_sources_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_sources.CellDataChangedEvent
    Dim _current_row As Integer

    If m_refreshing_grid = True Then

      Exit Sub
    End If

    With Me.dg_sources

      _current_row = .CurrentRow
      m_refreshing_grid = True

      If .Cell(.CurrentRow, GRID_3_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        Call ChangeUncheckofRows(.CurrentRow)
      End If

      m_refreshing_grid = False
      .CurrentRow = _current_row

      Call GetSourceListSelected()

    End With
  End Sub ' dg_sources_CellDataChangedEvent

  Private Sub btn_check_all_sources_Click() Handles btn_check_all_sources.ClickEvent
    Dim _idx_rows As Integer
    For _idx_rows = 0 To dg_sources.NumRows - 1
      dg_sources.Cell(_idx_rows, GRID_3_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      dg_sources_CellDataChangedEvent(_idx_rows, GRID_3_COLUMN_CHECKED)
    Next
  End Sub  ' btn_check_all_sources_Click

  Private Sub btn_uncheck_all_sources_Click() Handles btn_uncheck_all_sources.ClickEvent
    Dim _idx_rows As Integer
    For _idx_rows = 0 To dg_sources.NumRows - 1
      dg_sources.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      dg_sources_CellDataChangedEvent(_idx_rows, GRID_3_COLUMN_CHECKED)
    Next
  End Sub ' btn_uncheck_all_sources_Click

  Private Sub ef_alarm_id_EntryFieldValueChanged() Handles ef_alarm_id.EntryFieldValueChanged

    If ef_alarm_id.TextValue <> "" Then
      Call EnableControls(False)
    Else
      EnableControls(True)
    End If

  End Sub

  Private Sub chk_user_filter_CheckedChanged(sender As Object, e As EventArgs) Handles chk_user_filter.CheckedChanged
    If chk_user_filter.Checked Then
      Me.opt_one_user.Enabled = True
      Me.opt_all_users.Enabled = True
      Me.opt_none.Enabled = True

      If opt_one_user.Checked Then
        Me.cmb_user.Enabled = True
      Else
        Me.cmb_user.Enabled = False
      End If

    Else
      Me.opt_one_user.Enabled = False
      Me.opt_all_users.Enabled = False
      Me.opt_none.Enabled = False
      Me.cmb_user.Enabled = False
    End If
  End Sub 'chk_user_filter_CheckedChanged

  Private Sub opt_all_users_Click(sender As Object, e As EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub 'opt_all_users_Click

  Private Sub opt_one_user_Click(sender As Object, e As EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub 'opt_one_user_Click

  Private Sub opt_none_Click(sender As Object, e As EventArgs) Handles opt_none.Click
    Me.cmb_user.Enabled = False
  End Sub 'opt_none_Click

  Private Sub chk_show_all_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_all.CheckedChanged
    Call SetCombo(Me.cmb_user, "   SELECT   GU_USER_ID                          " & _
                               "          , GU_USERNAME                         " & _
                               "     FROM   GUI_USERS                           " & _
                               "LEFT JOIN   CARDS AS TECHNICIAN                 " & _
                               "       ON   TECHNICIAN.CA_LINKED_ID = GU_USER_ID" & _
                               "      AND   TECHNICIAN.CA_LINKED_TYPE = 10      " & _
                               "    WHERE   TECHNICIAN.CA_TRACKDATA IS NOT NULL " & _
                               IIf(chk_show_all.Checked, "", "      AND   GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE) & _
                               " ORDER BY   GU_USERNAME                         ")

    'The id in anonim user is 0
    Me.cmb_user.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6520))

  End Sub 'chk_show_all.
#End Region ' Events

End Class ' frm_alarms
