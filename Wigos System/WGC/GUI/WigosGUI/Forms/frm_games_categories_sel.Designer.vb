<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_games_categories_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_category_title = New GUI_Controls.uc_entry_field()
    Me.gb_category_status = New System.Windows.Forms.GroupBox()
    Me.chk_status_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_status_enabled = New System.Windows.Forms.CheckBox()
    Me.lbl_category_title = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_category_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_category_title)
    Me.panel_filter.Controls.Add(Me.gb_category_status)
    Me.panel_filter.Controls.Add(Me.ef_category_title)
    Me.panel_filter.Location = New System.Drawing.Point(5, 5)
    Me.panel_filter.Margin = New System.Windows.Forms.Padding(4)
    Me.panel_filter.Padding = New System.Windows.Forms.Padding(4, 4, 0, 0)
    Me.panel_filter.Size = New System.Drawing.Size(1107, 87)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_category_title, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_category_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_category_title, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 92)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(4)
    Me.panel_data.Padding = New System.Windows.Forms.Padding(4)
    Me.panel_data.Size = New System.Drawing.Size(1107, 343)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 4)
    Me.pn_separator_line.Margin = New System.Windows.Forms.Padding(4)
    Me.pn_separator_line.Padding = New System.Windows.Forms.Padding(0, 10, 0, 10)
    Me.pn_separator_line.Size = New System.Drawing.Size(1099, 30)
    '
    'pn_line
    '
    Me.pn_line.Location = New System.Drawing.Point(0, 10)
    Me.pn_line.Margin = New System.Windows.Forms.Padding(4)
    Me.pn_line.Size = New System.Drawing.Size(1099, 4)
    '
    'ef_category_title
    '
    Me.ef_category_title.DoubleValue = 0.0R
    Me.ef_category_title.IntegerValue = 0
    Me.ef_category_title.IsReadOnly = False
    Me.ef_category_title.Location = New System.Drawing.Point(187, 4)
    Me.ef_category_title.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_category_title.Name = "ef_category_title"
    Me.ef_category_title.PlaceHolder = Nothing
    Me.ef_category_title.Size = New System.Drawing.Size(251, 24)
    Me.ef_category_title.SufixText = "Sufix Text"
    Me.ef_category_title.SufixTextVisible = True
    Me.ef_category_title.TabIndex = 2
    Me.ef_category_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_title.TextValue = ""
    Me.ef_category_title.TextWidth = 0
    Me.ef_category_title.Value = ""
    Me.ef_category_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_category_status
    '
    Me.gb_category_status.Controls.Add(Me.chk_status_disabled)
    Me.gb_category_status.Controls.Add(Me.chk_status_enabled)
    Me.gb_category_status.Location = New System.Drawing.Point(4, 0)
    Me.gb_category_status.Margin = New System.Windows.Forms.Padding(4)
    Me.gb_category_status.Name = "gb_category_status"
    Me.gb_category_status.Padding = New System.Windows.Forms.Padding(4)
    Me.gb_category_status.Size = New System.Drawing.Size(113, 79)
    Me.gb_category_status.TabIndex = 12
    Me.gb_category_status.TabStop = False
    Me.gb_category_status.Text = "status"
    '
    'chk_status_disabled
    '
    Me.chk_status_disabled.AutoSize = True
    Me.chk_status_disabled.Location = New System.Drawing.Point(10, 49)
    Me.chk_status_disabled.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_status_disabled.Name = "chk_status_disabled"
    Me.chk_status_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_status_disabled.TabIndex = 2
    Me.chk_status_disabled.Text = "xDisabled"
    Me.chk_status_disabled.UseVisualStyleBackColor = True
    '
    'chk_status_enabled
    '
    Me.chk_status_enabled.AutoSize = True
    Me.chk_status_enabled.Location = New System.Drawing.Point(10, 23)
    Me.chk_status_enabled.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_status_enabled.Name = "chk_status_enabled"
    Me.chk_status_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_status_enabled.TabIndex = 1
    Me.chk_status_enabled.Text = "xEnabled"
    Me.chk_status_enabled.UseVisualStyleBackColor = True
    '
    'lbl_category_title
    '
    Me.lbl_category_title.AutoSize = True
    Me.lbl_category_title.Location = New System.Drawing.Point(127, 10)
    Me.lbl_category_title.Name = "lbl_category_title"
    Me.lbl_category_title.Size = New System.Drawing.Size(44, 13)
    Me.lbl_category_title.TabIndex = 13
    Me.lbl_category_title.Text = "Label1"
    '
    'frm_games_categories_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1117, 440)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Margin = New System.Windows.Forms.Padding(4)
    Me.Name = "frm_games_categories_sel"
    Me.Padding = New System.Windows.Forms.Padding(5)
    Me.Text = "frm_categories_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_category_status.ResumeLayout(False)
    Me.gb_category_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_category_title As GUI_Controls.uc_entry_field
  Friend WithEvents gb_category_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_status_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_category_title As System.Windows.Forms.Label
End Class
