<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_web_browser
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.wb_voucher = New System.Windows.Forms.WebBrowser
    Me.btn_close = New GUI_Controls.uc_button
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.SuspendLayout()
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.Location = New System.Drawing.Point(5, 4)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.wb_voucher)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.btn_close)
    Me.SplitContainer1.Size = New System.Drawing.Size(324, 604)
    Me.SplitContainer1.SplitterDistance = 546
    Me.SplitContainer1.TabIndex = 0
    '
    'wb_voucher
    '
    Me.wb_voucher.Dock = System.Windows.Forms.DockStyle.Fill
    Me.wb_voucher.Location = New System.Drawing.Point(0, 0)
    Me.wb_voucher.MinimumSize = New System.Drawing.Size(23, 20)
    Me.wb_voucher.Name = "wb_voucher"
    Me.wb_voucher.Size = New System.Drawing.Size(324, 546)
    Me.wb_voucher.TabIndex = 0
    '
    'btn_close
    '
    Me.btn_close.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_close.Location = New System.Drawing.Point(118, 12)
    Me.btn_close.Name = "btn_close"
    Me.btn_close.Size = New System.Drawing.Size(90, 30)
    Me.btn_close.TabIndex = 0
    Me.btn_close.ToolTipped = False
    Me.btn_close.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_web_browser
    '
    Me.AcceptButton = Me.btn_close
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_close
    Me.ClientSize = New System.Drawing.Size(334, 612)
    Me.Controls.Add(Me.SplitContainer1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_web_browser"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.Text = "frm_web_browser"
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents wb_voucher As System.Windows.Forms.WebBrowser
  Friend WithEvents btn_close As GUI_Controls.uc_button
End Class
