<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_multisite_configuration
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_multisite_configuration))
    Me.gb_multisite_params = New System.Windows.Forms.GroupBox()
    Me.lbl_multisite_status = New System.Windows.Forms.Label()
    Me.cmb_multisite_status = New GUI_Controls.uc_combo()
    Me.lbl_members_card_id = New System.Windows.Forms.Label()
    Me.lbl_center_address_2 = New System.Windows.Forms.Label()
    Me.lbl_center_address_1 = New System.Windows.Forms.Label()
    Me.ef_center_address_2 = New GUI_Controls.uc_entry_field()
    Me.ef_center_address_1 = New GUI_Controls.uc_entry_field()
    Me.ef_member_card_id = New GUI_Controls.uc_entry_field()
    Me.gb_head = New System.Windows.Forms.GroupBox()
    Me.lbl_site_id = New System.Windows.Forms.Label()
    Me.ef_site_id = New GUI_Controls.uc_entry_field()
    Me.lbl_site_name = New System.Windows.Forms.Label()
    Me.ef_site_name = New GUI_Controls.uc_entry_field()
    Me.gb_sas = New System.Windows.Forms.GroupBox()
    Me.uc_sas_flags = New GUI_Controls.uc_sas_flags()
    Me.panel_data.SuspendLayout()
    Me.gb_multisite_params.SuspendLayout()
    Me.gb_head.SuspendLayout()
    Me.gb_sas.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_sas)
    Me.panel_data.Controls.Add(Me.gb_head)
    Me.panel_data.Controls.Add(Me.gb_multisite_params)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(401, 718)
    '
    'gb_multisite_params
    '
    Me.gb_multisite_params.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_multisite_params.Controls.Add(Me.lbl_multisite_status)
    Me.gb_multisite_params.Controls.Add(Me.cmb_multisite_status)
    Me.gb_multisite_params.Controls.Add(Me.lbl_members_card_id)
    Me.gb_multisite_params.Controls.Add(Me.lbl_center_address_2)
    Me.gb_multisite_params.Controls.Add(Me.lbl_center_address_1)
    Me.gb_multisite_params.Controls.Add(Me.ef_center_address_2)
    Me.gb_multisite_params.Controls.Add(Me.ef_center_address_1)
    Me.gb_multisite_params.Controls.Add(Me.ef_member_card_id)
    Me.gb_multisite_params.Location = New System.Drawing.Point(3, 121)
    Me.gb_multisite_params.Name = "gb_multisite_params"
    Me.gb_multisite_params.Size = New System.Drawing.Size(391, 255)
    Me.gb_multisite_params.TabIndex = 4
    Me.gb_multisite_params.TabStop = False
    Me.gb_multisite_params.Text = "xMultisiteParameters"
    '
    'lbl_multisite_status
    '
    Me.lbl_multisite_status.AutoSize = True
    Me.lbl_multisite_status.Location = New System.Drawing.Point(14, 18)
    Me.lbl_multisite_status.Name = "lbl_multisite_status"
    Me.lbl_multisite_status.Size = New System.Drawing.Size(98, 13)
    Me.lbl_multisite_status.TabIndex = 0
    Me.lbl_multisite_status.Text = "xMultiSiteStatus"
    '
    'cmb_multisite_status
    '
    Me.cmb_multisite_status.AllowUnlistedValues = False
    Me.cmb_multisite_status.AutoCompleteMode = False
    Me.cmb_multisite_status.IsReadOnly = False
    Me.cmb_multisite_status.Location = New System.Drawing.Point(11, 34)
    Me.cmb_multisite_status.Name = "cmb_multisite_status"
    Me.cmb_multisite_status.SelectedIndex = -1
    Me.cmb_multisite_status.Size = New System.Drawing.Size(136, 24)
    Me.cmb_multisite_status.SufixText = "Sufix Text"
    Me.cmb_multisite_status.SufixTextVisible = True
    Me.cmb_multisite_status.TabIndex = 1
    Me.cmb_multisite_status.TextCombo = Nothing
    Me.cmb_multisite_status.TextWidth = 0
    '
    'lbl_members_card_id
    '
    Me.lbl_members_card_id.AutoSize = True
    Me.lbl_members_card_id.Location = New System.Drawing.Point(14, 61)
    Me.lbl_members_card_id.Name = "lbl_members_card_id"
    Me.lbl_members_card_id.Size = New System.Drawing.Size(108, 13)
    Me.lbl_members_card_id.TabIndex = 2
    Me.lbl_members_card_id.Text = "xMembersCardID"
    '
    'lbl_center_address_2
    '
    Me.lbl_center_address_2.AutoSize = True
    Me.lbl_center_address_2.Location = New System.Drawing.Point(14, 161)
    Me.lbl_center_address_2.Name = "lbl_center_address_2"
    Me.lbl_center_address_2.Size = New System.Drawing.Size(106, 13)
    Me.lbl_center_address_2.TabIndex = 6
    Me.lbl_center_address_2.Text = "xCenterAddress2"
    '
    'lbl_center_address_1
    '
    Me.lbl_center_address_1.AutoSize = True
    Me.lbl_center_address_1.Location = New System.Drawing.Point(14, 111)
    Me.lbl_center_address_1.Name = "lbl_center_address_1"
    Me.lbl_center_address_1.Size = New System.Drawing.Size(106, 13)
    Me.lbl_center_address_1.TabIndex = 4
    Me.lbl_center_address_1.Text = "xCenterAddress1"
    '
    'ef_center_address_2
    '
    Me.ef_center_address_2.DoubleValue = 0.0R
    Me.ef_center_address_2.IntegerValue = 0
    Me.ef_center_address_2.IsReadOnly = False
    Me.ef_center_address_2.Location = New System.Drawing.Point(11, 177)
    Me.ef_center_address_2.Name = "ef_center_address_2"
    Me.ef_center_address_2.PlaceHolder = Nothing
    Me.ef_center_address_2.Size = New System.Drawing.Size(368, 24)
    Me.ef_center_address_2.SufixText = "Sufix Text"
    Me.ef_center_address_2.SufixTextVisible = True
    Me.ef_center_address_2.TabIndex = 7
    Me.ef_center_address_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_center_address_2.TextValue = ""
    Me.ef_center_address_2.TextWidth = 0
    Me.ef_center_address_2.Value = ""
    Me.ef_center_address_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_center_address_1
    '
    Me.ef_center_address_1.DoubleValue = 0.0R
    Me.ef_center_address_1.IntegerValue = 0
    Me.ef_center_address_1.IsReadOnly = False
    Me.ef_center_address_1.Location = New System.Drawing.Point(11, 127)
    Me.ef_center_address_1.Name = "ef_center_address_1"
    Me.ef_center_address_1.PlaceHolder = Nothing
    Me.ef_center_address_1.Size = New System.Drawing.Size(368, 24)
    Me.ef_center_address_1.SufixText = "Sufix Text"
    Me.ef_center_address_1.SufixTextVisible = True
    Me.ef_center_address_1.TabIndex = 5
    Me.ef_center_address_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_center_address_1.TextValue = ""
    Me.ef_center_address_1.TextWidth = 0
    Me.ef_center_address_1.Value = ""
    Me.ef_center_address_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_member_card_id
    '
    Me.ef_member_card_id.DoubleValue = 0.0R
    Me.ef_member_card_id.IntegerValue = 0
    Me.ef_member_card_id.IsReadOnly = False
    Me.ef_member_card_id.Location = New System.Drawing.Point(11, 77)
    Me.ef_member_card_id.Name = "ef_member_card_id"
    Me.ef_member_card_id.PlaceHolder = Nothing
    Me.ef_member_card_id.Size = New System.Drawing.Size(368, 24)
    Me.ef_member_card_id.SufixText = "Sufix Text"
    Me.ef_member_card_id.SufixTextVisible = True
    Me.ef_member_card_id.TabIndex = 3
    Me.ef_member_card_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_member_card_id.TextValue = ""
    Me.ef_member_card_id.TextWidth = 0
    Me.ef_member_card_id.Value = ""
    Me.ef_member_card_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_head
    '
    Me.gb_head.Controls.Add(Me.lbl_site_id)
    Me.gb_head.Controls.Add(Me.ef_site_id)
    Me.gb_head.Controls.Add(Me.lbl_site_name)
    Me.gb_head.Controls.Add(Me.ef_site_name)
    Me.gb_head.Location = New System.Drawing.Point(3, 3)
    Me.gb_head.Name = "gb_head"
    Me.gb_head.Size = New System.Drawing.Size(391, 109)
    Me.gb_head.TabIndex = 5
    Me.gb_head.TabStop = False
    Me.gb_head.Text = "xSite"
    '
    'lbl_site_id
    '
    Me.lbl_site_id.AutoSize = True
    Me.lbl_site_id.Location = New System.Drawing.Point(14, 17)
    Me.lbl_site_id.Name = "lbl_site_id"
    Me.lbl_site_id.Size = New System.Drawing.Size(50, 13)
    Me.lbl_site_id.TabIndex = 4
    Me.lbl_site_id.Text = "xSiteID"
    '
    'ef_site_id
    '
    Me.ef_site_id.DoubleValue = 0.0R
    Me.ef_site_id.IntegerValue = 0
    Me.ef_site_id.IsReadOnly = False
    Me.ef_site_id.Location = New System.Drawing.Point(11, 33)
    Me.ef_site_id.Name = "ef_site_id"
    Me.ef_site_id.PlaceHolder = Nothing
    Me.ef_site_id.Size = New System.Drawing.Size(53, 24)
    Me.ef_site_id.SufixText = "Sufix Text"
    Me.ef_site_id.SufixTextVisible = True
    Me.ef_site_id.TabIndex = 5
    Me.ef_site_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_id.TextValue = ""
    Me.ef_site_id.TextWidth = 0
    Me.ef_site_id.Value = ""
    Me.ef_site_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_site_name
    '
    Me.lbl_site_name.AutoSize = True
    Me.lbl_site_name.Location = New System.Drawing.Point(14, 62)
    Me.lbl_site_name.Name = "lbl_site_name"
    Me.lbl_site_name.Size = New System.Drawing.Size(69, 13)
    Me.lbl_site_name.TabIndex = 6
    Me.lbl_site_name.Text = "xSiteName"
    '
    'ef_site_name
    '
    Me.ef_site_name.DoubleValue = 0.0R
    Me.ef_site_name.IntegerValue = 0
    Me.ef_site_name.IsReadOnly = False
    Me.ef_site_name.Location = New System.Drawing.Point(11, 78)
    Me.ef_site_name.Name = "ef_site_name"
    Me.ef_site_name.PlaceHolder = Nothing
    Me.ef_site_name.Size = New System.Drawing.Size(368, 24)
    Me.ef_site_name.SufixText = "Sufix Text"
    Me.ef_site_name.SufixTextVisible = True
    Me.ef_site_name.TabIndex = 7
    Me.ef_site_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_name.TextValue = ""
    Me.ef_site_name.TextWidth = 0
    Me.ef_site_name.Value = ""
    Me.ef_site_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_sas
    '
    Me.gb_sas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_sas.Controls.Add(Me.uc_sas_flags)
    Me.gb_sas.Location = New System.Drawing.Point(3, 340)
    Me.gb_sas.Name = "gb_sas"
    Me.gb_sas.Size = New System.Drawing.Size(391, 236)
    Me.gb_sas.TabIndex = 10
    Me.gb_sas.TabStop = False
    Me.gb_sas.Text = "xMultisiteParameters"
    '
    'uc_sas_flags
    '
    Me.uc_sas_flags.AFT_although_lock_0 = False
    Me.uc_sas_flags.BCD4 = 4
    Me.uc_sas_flags.BCD5 = 5
    Me.uc_sas_flags.Cmd1BHandPays = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.EnableDisableNoteAcceptor = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.ExtendedMeters = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.Ignore_no_bet_plays = False
    Me.uc_sas_flags.Location = New System.Drawing.Point(11, 20)
    Me.uc_sas_flags.Name = "uc_sas_flags"
    Me.uc_sas_flags.No_RTE = False
    Me.uc_sas_flags.ProgressiveMeters = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.PromotionalCredits = WSI.Common.ENUM_SAS_FLAGS.SITE_DEFINED
    Me.uc_sas_flags.Single_byte = False
    Me.uc_sas_flags.Size = New System.Drawing.Size(368, 213)
    Me.uc_sas_flags.TabIndex = 8
    '
    'frm_multisite_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(505, 587)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_multisite_configuration"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_multisite_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.gb_multisite_params.ResumeLayout(False)
    Me.gb_multisite_params.PerformLayout()
    Me.gb_head.ResumeLayout(False)
    Me.gb_head.PerformLayout()
    Me.gb_sas.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_multisite_params As System.Windows.Forms.GroupBox
  Friend WithEvents ef_center_address_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_center_address_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_member_card_id As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_center_address_2 As System.Windows.Forms.Label
  Friend WithEvents lbl_center_address_1 As System.Windows.Forms.Label
  Friend WithEvents lbl_members_card_id As System.Windows.Forms.Label
  Friend WithEvents cmb_multisite_status As GUI_Controls.uc_combo
  Friend WithEvents lbl_multisite_status As System.Windows.Forms.Label
  Friend WithEvents gb_head As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_site_id As System.Windows.Forms.Label
  Friend WithEvents ef_site_id As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_site_name As System.Windows.Forms.Label
  Friend WithEvents ef_site_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_sas As System.Windows.Forms.GroupBox
  Friend WithEvents uc_sas_flags As GUI_Controls.uc_sas_flags
End Class
