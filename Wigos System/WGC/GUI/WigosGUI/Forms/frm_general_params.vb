'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_general_params
' DESCRIPTION:   Edit general params
' AUTHOR:        Alberto Cuesta
' CREATION DATE: 04-OCT-2007
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 04-OCT-2007 ACC    Initial draft
' 15-OCT-2012 RRB    Now only SuperUser can access
' 12-DEC-2013 LJM  Fixed Bug #WIG-482 Search not audited.
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient

Imports WinSys.Wigos.Protocols.Business.Lite.Entities
Imports WinSys.Wigos.Protocols.Business.Lite.Service.Persistence
Imports WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Lite.Repositories

Public Class frm_genaral_params
	Inherits GUI_Controls.frm_base

#Region " Windows Form Designer generated code "

	Public Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Protected WithEvents panel_filter As System.Windows.Forms.Panel
	Friend WithEvents panel_filter_buttons As System.Windows.Forms.Panel
	Friend WithEvents btn_filter_reset As GUI_Controls.uc_button
	Friend WithEvents btn_filter_apply As GUI_Controls.uc_button
	Protected WithEvents pn_separator_line As System.Windows.Forms.Panel
	Protected WithEvents pn_line As System.Windows.Forms.Panel
	Friend WithEvents panel_buttons As System.Windows.Forms.Panel
	Friend WithEvents btn_custom_2 As GUI_Controls.uc_button
	Friend WithEvents btn_custom_1 As GUI_Controls.uc_button
	Friend WithEvents btn_custom_0 As GUI_Controls.uc_button
	Friend WithEvents panel_separator As System.Windows.Forms.Panel
	Friend WithEvents btn_info As GUI_Controls.uc_button
	Friend WithEvents btn_new As GUI_Controls.uc_button
	Friend WithEvents btn_print As GUI_Controls.uc_button
	Friend WithEvents btn_select As GUI_Controls.uc_button
	Friend WithEvents btn_cancel As GUI_Controls.uc_button
	Friend WithEvents Panel_grids As System.Windows.Forms.Panel
	Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
	Friend WithEvents gb_group_key As System.Windows.Forms.GroupBox
	Friend WithEvents opt_several_groups As System.Windows.Forms.RadioButton
	Friend WithEvents opt_all_groups As System.Windows.Forms.RadioButton
	Friend WithEvents cmb_group_key As GUI_Controls.uc_combo
	Friend WithEvents ef_text As GUI_Controls.uc_entry_field
	Friend WithEvents dg_general_params As System.Windows.Forms.DataGrid
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.panel_filter = New System.Windows.Forms.Panel
    Me.ef_text = New GUI_Controls.uc_entry_field
    Me.gb_group_key = New System.Windows.Forms.GroupBox
    Me.opt_several_groups = New System.Windows.Forms.RadioButton
    Me.opt_all_groups = New System.Windows.Forms.RadioButton
    Me.cmb_group_key = New GUI_Controls.uc_combo
    Me.panel_filter_buttons = New System.Windows.Forms.Panel
    Me.btn_filter_reset = New GUI_Controls.uc_button
    Me.btn_filter_apply = New GUI_Controls.uc_button
    Me.pn_separator_line = New System.Windows.Forms.Panel
    Me.pn_line = New System.Windows.Forms.Panel
    Me.panel_buttons = New System.Windows.Forms.Panel
    Me.btn_custom_2 = New GUI_Controls.uc_button
    Me.btn_custom_1 = New GUI_Controls.uc_button
    Me.btn_custom_0 = New GUI_Controls.uc_button
    Me.panel_separator = New System.Windows.Forms.Panel
    Me.btn_info = New GUI_Controls.uc_button
    Me.btn_new = New GUI_Controls.uc_button
    Me.btn_print = New GUI_Controls.uc_button
    Me.btn_select = New GUI_Controls.uc_button
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.Panel_grids = New System.Windows.Forms.Panel
    Me.dg_general_params = New System.Windows.Forms.DataGrid
    Me.Splitter1 = New System.Windows.Forms.Splitter
    Me.panel_filter.SuspendLayout()
		Me.gb_group_key.SuspendLayout()
		Me.panel_filter_buttons.SuspendLayout()
		Me.pn_separator_line.SuspendLayout()
		Me.panel_buttons.SuspendLayout()
		Me.Panel_grids.SuspendLayout()
		CType(Me.dg_general_params, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'panel_filter
		'
		Me.panel_filter.Controls.Add(Me.ef_text)
		Me.panel_filter.Controls.Add(Me.gb_group_key)
		Me.panel_filter.Controls.Add(Me.panel_filter_buttons)
		Me.panel_filter.Dock = System.Windows.Forms.DockStyle.Top
		Me.panel_filter.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.panel_filter.Location = New System.Drawing.Point(4, 4)
		Me.panel_filter.Name = "panel_filter"
		Me.panel_filter.Padding = New System.Windows.Forms.Padding(3)
		Me.panel_filter.Size = New System.Drawing.Size(902, 84)
		Me.panel_filter.TabIndex = 1
		'
		'ef_text
		'
    Me.ef_text.DoubleValue = 0
    Me.ef_text.IntegerValue = 0
		Me.ef_text.IsReadOnly = False
		Me.ef_text.Location = New System.Drawing.Point(284, 17)
		Me.ef_text.Name = "ef_text"
		Me.ef_text.Size = New System.Drawing.Size(297, 24)
		Me.ef_text.SufixText = "Sufix Text"
		Me.ef_text.SufixTextVisible = True
		Me.ef_text.TabIndex = 1
		Me.ef_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.ef_text.TextValue = ""
		Me.ef_text.TextWidth = 120
		Me.ef_text.Value = ""
		Me.ef_text.ValueForeColor = System.Drawing.Color.Blue
		Me.ef_text.Visible = False
		'
		'gb_group_key
		'
		Me.gb_group_key.Controls.Add(Me.opt_several_groups)
		Me.gb_group_key.Controls.Add(Me.opt_all_groups)
		Me.gb_group_key.Controls.Add(Me.cmb_group_key)
		Me.gb_group_key.Location = New System.Drawing.Point(6, 3)
		Me.gb_group_key.Name = "gb_group_key"
		Me.gb_group_key.Size = New System.Drawing.Size(272, 72)
		Me.gb_group_key.TabIndex = 0
		Me.gb_group_key.TabStop = False
		Me.gb_group_key.Text = "xGroup Key"
		'
		'opt_several_groups
		'
		Me.opt_several_groups.Location = New System.Drawing.Point(8, 14)
		Me.opt_several_groups.Name = "opt_several_groups"
		Me.opt_several_groups.Size = New System.Drawing.Size(72, 24)
		Me.opt_several_groups.TabIndex = 0
		Me.opt_several_groups.Text = "xOne"
		'
		'opt_all_groups
		'
		Me.opt_all_groups.Location = New System.Drawing.Point(8, 40)
		Me.opt_all_groups.Name = "opt_all_groups"
		Me.opt_all_groups.Size = New System.Drawing.Size(64, 24)
		Me.opt_all_groups.TabIndex = 2
		Me.opt_all_groups.Text = "xAll"
		'
		'cmb_group_key
		'
		Me.cmb_group_key.AllowUnlistedValues = False
		Me.cmb_group_key.AutoCompleteMode = True
		Me.cmb_group_key.IsReadOnly = False
		Me.cmb_group_key.Location = New System.Drawing.Point(80, 14)
		Me.cmb_group_key.Name = "cmb_group_key"
		Me.cmb_group_key.SelectedIndex = -1
		Me.cmb_group_key.Size = New System.Drawing.Size(184, 24)
		Me.cmb_group_key.SufixText = "Sufix Text"
		Me.cmb_group_key.SufixTextVisible = True
		Me.cmb_group_key.TabIndex = 1
    Me.cmb_group_key.TextVisible = False
		Me.cmb_group_key.TextWidth = 0
		'
		'panel_filter_buttons
		'
		Me.panel_filter_buttons.Controls.Add(Me.btn_filter_reset)
		Me.panel_filter_buttons.Controls.Add(Me.btn_filter_apply)
		Me.panel_filter_buttons.Dock = System.Windows.Forms.DockStyle.Right
		Me.panel_filter_buttons.Location = New System.Drawing.Point(811, 3)
		Me.panel_filter_buttons.Name = "panel_filter_buttons"
		Me.panel_filter_buttons.Size = New System.Drawing.Size(88, 78)
		Me.panel_filter_buttons.TabIndex = 2
		'
		'btn_filter_reset
		'
		Me.btn_filter_reset.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_filter_reset.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_filter_reset.Location = New System.Drawing.Point(0, 18)
		Me.btn_filter_reset.Name = "btn_filter_reset"
		Me.btn_filter_reset.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_filter_reset.Size = New System.Drawing.Size(90, 30)
		Me.btn_filter_reset.TabIndex = 1
		Me.btn_filter_reset.ToolTipped = False
		Me.btn_filter_reset.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_filter_apply
		'
		Me.btn_filter_apply.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_filter_apply.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_filter_apply.Location = New System.Drawing.Point(0, 48)
		Me.btn_filter_apply.Name = "btn_filter_apply"
		Me.btn_filter_apply.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_filter_apply.Size = New System.Drawing.Size(90, 30)
		Me.btn_filter_apply.TabIndex = 0
		Me.btn_filter_apply.ToolTipped = False
		Me.btn_filter_apply.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'pn_separator_line
		'
		Me.pn_separator_line.Controls.Add(Me.pn_line)
		Me.pn_separator_line.Dock = System.Windows.Forms.DockStyle.Top
		Me.pn_separator_line.Location = New System.Drawing.Point(4, 88)
		Me.pn_separator_line.Name = "pn_separator_line"
		Me.pn_separator_line.Padding = New System.Windows.Forms.Padding(0, 8, 0, 8)
		Me.pn_separator_line.Size = New System.Drawing.Size(902, 23)
		Me.pn_separator_line.TabIndex = 7
		'
		'pn_line
		'
		Me.pn_line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.pn_line.Dock = System.Windows.Forms.DockStyle.Top
		Me.pn_line.Location = New System.Drawing.Point(0, 8)
		Me.pn_line.Name = "pn_line"
		Me.pn_line.Size = New System.Drawing.Size(902, 4)
		Me.pn_line.TabIndex = 0
		'
		'panel_buttons
		'
		Me.panel_buttons.Controls.Add(Me.btn_custom_2)
		Me.panel_buttons.Controls.Add(Me.btn_custom_1)
		Me.panel_buttons.Controls.Add(Me.btn_custom_0)
		Me.panel_buttons.Controls.Add(Me.panel_separator)
		Me.panel_buttons.Controls.Add(Me.btn_info)
		Me.panel_buttons.Controls.Add(Me.btn_new)
		Me.panel_buttons.Controls.Add(Me.btn_print)
		Me.panel_buttons.Controls.Add(Me.btn_select)
		Me.panel_buttons.Controls.Add(Me.btn_cancel)
		Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
		Me.panel_buttons.Location = New System.Drawing.Point(818, 111)
		Me.panel_buttons.Name = "panel_buttons"
		Me.panel_buttons.Size = New System.Drawing.Size(88, 476)
		Me.panel_buttons.TabIndex = 8
		'
		'btn_custom_2
		'
		Me.btn_custom_2.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_custom_2.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_custom_2.Location = New System.Drawing.Point(0, 206)
		Me.btn_custom_2.Name = "btn_custom_2"
		Me.btn_custom_2.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_custom_2.Size = New System.Drawing.Size(90, 30)
		Me.btn_custom_2.TabIndex = 0
		Me.btn_custom_2.ToolTipped = False
		Me.btn_custom_2.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_custom_1
		'
		Me.btn_custom_1.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_custom_1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_custom_1.Location = New System.Drawing.Point(0, 236)
		Me.btn_custom_1.Name = "btn_custom_1"
		Me.btn_custom_1.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_custom_1.Size = New System.Drawing.Size(90, 30)
		Me.btn_custom_1.TabIndex = 1
		Me.btn_custom_1.ToolTipped = False
		Me.btn_custom_1.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_custom_0
		'
		Me.btn_custom_0.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_custom_0.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_custom_0.Location = New System.Drawing.Point(0, 266)
		Me.btn_custom_0.Name = "btn_custom_0"
		Me.btn_custom_0.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_custom_0.Size = New System.Drawing.Size(90, 30)
		Me.btn_custom_0.TabIndex = 2
		Me.btn_custom_0.ToolTipped = False
		Me.btn_custom_0.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'panel_separator
		'
		Me.panel_separator.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.panel_separator.Location = New System.Drawing.Point(0, 296)
		Me.panel_separator.Name = "panel_separator"
		Me.panel_separator.Size = New System.Drawing.Size(88, 30)
		Me.panel_separator.TabIndex = 3
		'
		'btn_info
		'
		Me.btn_info.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_info.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_info.Location = New System.Drawing.Point(0, 326)
		Me.btn_info.Name = "btn_info"
		Me.btn_info.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_info.Size = New System.Drawing.Size(90, 30)
		Me.btn_info.TabIndex = 4
		Me.btn_info.ToolTipped = False
		Me.btn_info.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_new
		'
		Me.btn_new.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_new.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_new.Location = New System.Drawing.Point(0, 356)
		Me.btn_new.Name = "btn_new"
		Me.btn_new.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_new.Size = New System.Drawing.Size(90, 30)
		Me.btn_new.TabIndex = 5
		Me.btn_new.ToolTipped = False
		Me.btn_new.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_print
		'
		Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_print.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_print.Location = New System.Drawing.Point(0, 386)
		Me.btn_print.Name = "btn_print"
		Me.btn_print.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_print.Size = New System.Drawing.Size(90, 30)
		Me.btn_print.TabIndex = 6
		Me.btn_print.ToolTipped = False
		Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_select
		'
		Me.btn_select.DialogResult = System.Windows.Forms.DialogResult.None
		Me.btn_select.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_select.Location = New System.Drawing.Point(0, 416)
		Me.btn_select.Name = "btn_select"
		Me.btn_select.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_select.Size = New System.Drawing.Size(90, 30)
		Me.btn_select.TabIndex = 7
		Me.btn_select.ToolTipped = False
		Me.btn_select.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'btn_cancel
		'
		Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.btn_cancel.Location = New System.Drawing.Point(0, 446)
		Me.btn_cancel.Name = "btn_cancel"
		Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
		Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
		Me.btn_cancel.TabIndex = 8
		Me.btn_cancel.ToolTipped = False
		Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
		'
		'Panel_grids
		'
		Me.Panel_grids.Controls.Add(Me.dg_general_params)
		Me.Panel_grids.Controls.Add(Me.Splitter1)
		Me.Panel_grids.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel_grids.Location = New System.Drawing.Point(4, 111)
		Me.Panel_grids.Name = "Panel_grids"
		Me.Panel_grids.Size = New System.Drawing.Size(814, 476)
		Me.Panel_grids.TabIndex = 9
		'
		'dg_general_params
		'
		Me.dg_general_params.AllowDrop = True
		Me.dg_general_params.AllowNavigation = False
		Me.dg_general_params.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.dg_general_params.CaptionVisible = False
		Me.dg_general_params.DataMember = ""
		Me.dg_general_params.Dock = System.Windows.Forms.DockStyle.Fill
		Me.dg_general_params.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.dg_general_params.HeaderForeColor = System.Drawing.SystemColors.ControlText
		Me.dg_general_params.Location = New System.Drawing.Point(0, 8)
		Me.dg_general_params.Name = "dg_general_params"
		Me.dg_general_params.Size = New System.Drawing.Size(814, 468)
		Me.dg_general_params.TabIndex = 8
		'
		'Splitter1
		'
		Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
		Me.Splitter1.Location = New System.Drawing.Point(0, 0)
		Me.Splitter1.Name = "Splitter1"
		Me.Splitter1.Size = New System.Drawing.Size(814, 8)
		Me.Splitter1.TabIndex = 7
		Me.Splitter1.TabStop = False
		'
		'frm_genaral_params
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
		Me.CancelButton = Me.btn_cancel
		Me.ClientSize = New System.Drawing.Size(910, 591)
		Me.Controls.Add(Me.Panel_grids)
		Me.Controls.Add(Me.panel_buttons)
		Me.Controls.Add(Me.pn_separator_line)
		Me.Controls.Add(Me.panel_filter)
    Me.Name = "frm_genaral_params"
		Me.Text = "General Params"
		Me.panel_filter.ResumeLayout(False)
		Me.gb_group_key.ResumeLayout(False)
		Me.panel_filter_buttons.ResumeLayout(False)
		Me.pn_separator_line.ResumeLayout(False)
		Me.panel_buttons.ResumeLayout(False)
		Me.Panel_grids.ResumeLayout(False)
		CType(Me.dg_general_params, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

#End Region

#Region " Members "

	Private m_sql_commandbuilder As SqlCommandBuilder
	Shared m_sql_conn As SqlConnection
	Private m_sql_adap As SqlDataAdapter
	Private m_sql_dataset As DataSet
	Private m_data_view_general_params As DataView
	Private federal_tax_on_won As Double
	Private state_tax_on_won As Double

	Private m_group As String
	Private m_text As String

#End Region

#Region " Button Events "

	Private Sub BaseButtonClick(ByVal ButtonId As ENUM_BUTTON)

		'XVV 16/04/2007
		'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
		Windows.Forms.Cursor.Current = Cursors.WaitCursor
		Call GUI_ButtonClick(ButtonId)

		'XVV 16/04/2007
		'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
		Windows.Forms.Cursor.Current = Cursors.Default
	End Sub

	Private Sub btn_filter_reset_ClickEvent() Handles btn_filter_reset.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_FILTER_RESET)
	End Sub

	Private Sub btn_filter_apply_ClickEvent() Handles btn_filter_apply.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
	End Sub

	Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_PRINT)
	End Sub

	Private Sub btn_info_ClickEvent() Handles btn_info.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_INFO)
	End Sub

	Private Sub btn_new_ClickEvent() Handles btn_new.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_NEW)
	End Sub

	Private Sub btn_select_ClickEvent() Handles btn_select.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_SELECT)
	End Sub

	Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_CANCEL)
	End Sub

	Private Sub btn_custom_2_ClickEvent() Handles btn_custom_2.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_2)
	End Sub
	Private Sub btn_custom_1_ClickEvent() Handles btn_custom_1.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_1)
	End Sub
	Private Sub btn_custom_0_ClickEvent() Handles btn_custom_0.ClickEvent
		Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_0)
	End Sub

#End Region

#Region " Overrides Functions "

	Public Overrides Sub GUI_SetFormId()
		Me.FormId = ENUM_FORM.FORM_GENERAL_PARAMS

		' TJG 01-FEB-2005
		' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
		' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

		'------------------------------------------------
		' XVV 13/04/2007
		' Translate tu BASE Form
		'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
		'------------------------------------------------

		'------------------------------------------------
		'XVV 13/04/2007
		'Call Base Form proc
		Call MyBase.GUI_SetFormId()
		'------------------------------------------------


	End Sub

	Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
		If DiscardChanges() Then
			CloseCanceled = False
		Else
			CloseCanceled = True
		End If
	End Sub

	Protected Overrides Sub GUI_InitControls()
		Dim idx As Integer
		Dim table As DataTable

		Call MyBase.GUI_InitControls()

		Me.Text = GLB_NLS_GUI_AUDITOR.GetString(460)

		Me.btn_print.Text = GLB_NLS_GUI_CONTROLS.GetString(5)
		Me.btn_new.Text = GLB_NLS_GUI_CONTROLS.GetString(6)
		Me.btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)
		Me.btn_select.Text = GLB_NLS_GUI_CONTROLS.GetString(9)
		Me.btn_filter_apply.Text = GLB_NLS_GUI_CONTROLS.GetString(8)
		Me.btn_filter_reset.Text = GLB_NLS_GUI_CONTROLS.GetString(7)
		Me.btn_custom_0.Text = GLB_NLS_GUI_CONTROLS.GetString(13)

		GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
		GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
		GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
		GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
		GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
		GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
		GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
		GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

		gb_group_key.Text = GLB_NLS_GUI_CONFIGURATION.GetString(61)
		Me.opt_several_groups.Text = GLB_NLS_GUI_ALARMS.GetString(276)
		Me.opt_all_groups.Text = GLB_NLS_GUI_ALARMS.GetString(277)

		' Fill ComboBox
		table = GUI_GetTableUsingSQL("SELECT DISTINCT GP_GROUP_KEY FROM GENERAL_PARAMS ORDER BY GP_GROUP_KEY", Integer.MaxValue)

		For idx = 0 To table.Rows.Count - 1
			cmb_group_key.Add(table.Rows.Item(idx).Item("GP_GROUP_KEY"))
		Next

		dg_general_params.Enabled = False

		ef_text.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 255)
		ef_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5754)
		ef_text.Visible = True

		Call SetDefaultValues()

		Me.KeyPreview = True
	End Sub

	Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

		If Not CurrentUser.IsSuperUser Then
			AndPerm.Delete = False
			AndPerm.Execute = False
			AndPerm.Read = True
			AndPerm.Write = False
		End If

	End Sub	'GUI_Permissions

	Protected Overrides Sub GUI_SetInitialFocus()

		Me.ef_text.Focus()

	End Sub	'GUI_SetInitialFocus

#End Region

#Region " Private Functions "

	Private Function GUI_Button(ByVal ButtonId As ENUM_BUTTON) As uc_button
		Select Case ButtonId
			Case ENUM_BUTTON.BUTTON_CANCEL
				Return Me.btn_cancel
			Case ENUM_BUTTON.BUTTON_FILTER_APPLY
				Return Me.btn_filter_apply
			Case ENUM_BUTTON.BUTTON_FILTER_RESET
				Return Me.btn_filter_reset
			Case ENUM_BUTTON.BUTTON_INFO
				Return Me.btn_info
			Case ENUM_BUTTON.BUTTON_NEW
				Return Me.btn_new
			Case ENUM_BUTTON.BUTTON_PRINT
				Return Me.btn_print
			Case ENUM_BUTTON.BUTTON_SELECT
				Return Me.btn_select
			Case ENUM_BUTTON.BUTTON_CUSTOM_0
				Return Me.btn_custom_0
			Case ENUM_BUTTON.BUTTON_CUSTOM_1
				Return Me.btn_custom_1
			Case ENUM_BUTTON.BUTTON_CUSTOM_2
				Return Me.btn_custom_2

			Case Else
				Return Nothing
		End Select
	End Function

	'  Protected Overridable Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
	Private Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
		Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

		Select Case ButtonId
			Case ENUM_BUTTON.BUTTON_CANCEL
				Me.Close()

			Case ENUM_BUTTON.BUTTON_FILTER_APPLY
				Call GUI_ExecuteQuery()

			Case ENUM_BUTTON.BUTTON_FILTER_RESET
				Call GUI_FilterReset()

			Case ENUM_BUTTON.BUTTON_INFO

			Case ENUM_BUTTON.BUTTON_NEW

			Case ENUM_BUTTON.BUTTON_PRINT

			Case ENUM_BUTTON.BUTTON_SELECT

			Case ENUM_BUTTON.BUTTON_CUSTOM_0
				' sle 09/11/09
				Call BeforeSaveChanges()

			Case ENUM_BUTTON.BUTTON_CUSTOM_1
			Case ENUM_BUTTON.BUTTON_CUSTOM_2
			Case ENUM_BUTTON.BUTTON_GRID_INFO

			Case Else
				'
		End Select

		If GUI_HasToBeAudited(ButtonId) Then
			_print_data = New GUI_Reports.CLASS_PRINT_DATA
			Call GUI_ReportFilter(_print_data)
			Call MyBase.AuditFormClick(ButtonId, _print_data)
		End If

	End Sub

	Private Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

		Call UpdateFilters()
		PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(61), m_group)
		PrintData.SetFilter(ef_text.Text, m_text)

	End Sub

	Private Sub UpdateFilters()
		If Me.opt_all_groups.Checked Then
			Me.m_group = GLB_NLS_GUI_ALARMS.GetString(277)
		Else
			Me.m_group = cmb_group_key.TextValue
		End If
		If Not String.IsNullOrEmpty(ef_text.TextValue) Then
			Me.m_text = ef_text.TextValue
		End If
	End Sub

	Private Function GUI_GetChanges(Optional ByVal Update As Boolean = False) As DataTable
		Dim dt_changes As New DataTable
		Dim idx As Integer
		Dim new_value As String
		Dim old_value As String
		Dim changed As Boolean

		changed = False

		Try
			If IsNothing(m_data_view_general_params) Then
				Return dt_changes
			End If

			For idx = 0 To m_data_view_general_params.Count - 1
				Select Case m_data_view_general_params.Table.Rows(idx).Item(1).ToString

					Case "FederalTaxOnWonPct" _
					   , "StateTaxOnWonPct" _
					   , "CardPrice" _
					   , "TaxOnWonPct" _
					   , "PrintNameArea_BottomX" _
					   , "PrintNameArea_BottomY" _
					   , "PrintNameArea_FontSize" _
					   , "PrintNameArea_TopX" _
					   , "PrintNameArea_TopY"

						new_value = m_data_view_general_params.Table.Rows(idx).Item(3)
						new_value = GUI_LocalNumberToDBNumber(new_value)

					Case Else

						If (m_data_view_general_params.Table.Rows(idx).Item(3) Is DBNull.Value) Then
							new_value = String.Empty
						Else
							new_value = m_data_view_general_params.Table.Rows(idx).Item(3)
						End If

				End Select

				If (m_data_view_general_params.Table.Rows(idx).Item(2) Is DBNull.Value) Then
					old_value = String.Empty
				Else
					old_value = m_data_view_general_params.Table.Rows(idx).Item(2)
				End If

				If new_value <> old_value Then
					If Update Then
						m_data_view_general_params.Table.Rows(idx).Item(2) = new_value
					End If
					changed = True
				End If
			Next

			If changed Then
				dt_changes = m_data_view_general_params.Table.GetChanges
			End If

		Catch ex As Exception
			Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

		Finally
		End Try

		If IsNothing(dt_changes) Then
			dt_changes = New DataTable
		End If

		Return dt_changes

	End Function


	Private Sub GUI_SaveChanges()
		Dim dt_changes As New DataTable

		Try
			dt_changes = GUI_GetChanges(True)
			If dt_changes.Rows.Count = 0 Then
				Exit Sub
			End If

			' Check and set correct ConfigurationId for S2S GSA Protocol
			Dim modifiedParams As New List(Of GeneralParamItem)
			For Each row As DataRow In dt_changes.Rows
				Dim newItem As GeneralParamItem = New GeneralParamItem()
				newItem.GroupKey = row.Item(0)
				newItem.SubjectKey = row.Item(1)
				newItem.Value = row.Item(2)
				modifiedParams.Add(newItem)
			Next
			Dim sequenceRepository As New SequenceRepository()
			Dim configurationService As New ConfigurationService(sequenceRepository)
			configurationService.CheckAndSetConfigurationIdIncrement(modifiedParams)

			'
			' UPDATE 
			'
			' Modified rows
			m_sql_adap.Update(m_sql_dataset.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent))

			' Auditory
			GUI_WriteAuditoryChanges(True, dt_changes)

			m_sql_dataset.AcceptChanges()

			Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

		Catch ex As Exception
			m_sql_dataset.AcceptChanges()
			Debug.WriteLine(ex.Message)
			GUI_ClearGrid()

			Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

		Finally
		End Try

	End Sub

	' PURPOSE: The sum of both taxes cannot exceed 100%.
	'         
	' PARAMS:
	'    - INPUT:
	'
	'    - OUTPUT:
	'
	'RETURNS:
	'
	Private Sub BeforeSaveChanges()
		Dim data_table_changes_general_params As DataTable
		Dim str As String
		Dim tax_on_won As Double
		Dim new_federal_tax_on_won As Double
		Dim new_state_tax_on_won As Double
		Dim num_changes As Integer
		Dim idx As Integer
		Dim _main_item_gp As String
		Dim _sp_profiles_layout_create As Boolean

		'Initialize variables
		_sp_profiles_layout_create = False
		_main_item_gp = String.Empty

		data_table_changes_general_params = GUI_GetChanges()
		num_changes = data_table_changes_general_params.Rows.Count

		' Previous Taxes
		new_federal_tax_on_won = federal_tax_on_won
		new_state_tax_on_won = state_tax_on_won

		' Loop looking for Taxes
		For idx = 0 To num_changes - 1

			'FJC 17-02-2015
			_main_item_gp = data_table_changes_general_params.Rows(idx).Item(0).ToString

			str = data_table_changes_general_params.Rows(idx).Item(1).ToString
			If data_table_changes_general_params.Rows(idx).Item(1).ToString = "FederalTaxOnWonPct" Then
				new_federal_tax_on_won = GUI_ParseNumber(data_table_changes_general_params.Rows(idx).Item(3))
			End If
			If data_table_changes_general_params.Rows(idx).Item(1).ToString = "StateTaxOnWonPct" Then
				new_state_tax_on_won = GUI_ParseNumber(data_table_changes_general_params.Rows(idx).Item(3))
			End If

			'FJC 17-02-2015
			'SmartFloor
			If Not _sp_profiles_layout_create AndAlso _
			   _main_item_gp = "Intellia" AndAlso _
			   str = "Enabled" AndAlso _
			   data_table_changes_general_params.Rows(idx).Item(3) = "1" Then

				' Create Default Profiles for Layout, only if GP has been activated.
				_sp_profiles_layout_create = True
				Call GUI_SQLExecuteNonQuery("EXEC CreateDefaultProfilesLayout")
			End If

		Next

		tax_on_won = new_federal_tax_on_won + new_state_tax_on_won
		If tax_on_won > 100 Then
			Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , "100%")

			Exit Sub
		End If

		Call GUI_SaveChanges()

	End Sub

	Private Sub GUI_FilterReset()

		Call SetDefaultValues()

	End Sub	'GUI_FilterReset

	Private Function GUI_FilterCheck() As Boolean

		Return True
	End Function 'GUI_FilterCheck

	Private Sub GUI_ClearGrid()

		dg_general_params.DataSource = Nothing

	End Sub	'GUI_ClearGrid

	Private Sub GUI_ExecuteQuery()
		Dim dt_changes As DataTable

		Try
			' Check the filter
			If GUI_FilterCheck() Then

				'XVV 16/04/2007
				'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
				Windows.Forms.Cursor.Current = Cursors.WaitCursor

				' If pending changes MsgBox
				If Not IsNothing(m_data_view_general_params) Then
					dt_changes = GUI_GetChanges()

					If Not IsNothing(dt_changes) Then
						If dt_changes.Rows.Count > 0 Then
							If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
								Return
							End If
						End If
					End If
				End If

				' Invalidate datagrid
				GUI_ClearGrid()

				' Execute the query        
				Call ExecuteQuery()

				GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (dg_general_params.VisibleRowCount > 0) _
															  And Me.Permissions.Write

			End If

		Catch exception As Exception
			Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
								  "TerminalsEdit", _
								  "FilterApply", _
								  "", _
								  "", _
								  "", _
								  exception.Message)

		Finally

			'XVV 16/04/2007
			'Change Me.Cursor.current to Windows.Forms.Cursor, compiler generate a warning
			Windows.Forms.Cursor.Current = Cursors.Default
		End Try

	End Sub	'GUI_ExecuteQuery

	Private Sub ExecuteQuery()
		Dim table_style As DataGridTableStyle
		Dim sql_query_general_params As String
		'XVV Variable not use Dim sql_command As SqlCommand

		' Initialize DataView for database
		Try
			m_sql_conn = Nothing
			m_sql_conn = NewSQLConnection()

			If Not (m_sql_conn Is Nothing) Then
				' Connected
				If m_sql_conn.State <> ConnectionState.Open Then
					' Connecting ..
					m_sql_conn.Open()
				End If

				If Not m_sql_conn.State = ConnectionState.Open Then
					Exit Try
				End If
			End If

			m_sql_dataset = New DataSet

			' Build Query -------------------------------------------------
			sql_query_general_params = "SELECT GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE FROM GENERAL_PARAMS"

			' Filter
			If opt_several_groups.Checked Then
				sql_query_general_params = sql_query_general_params & " WHERE GP_GROUP_KEY = '" & cmb_group_key.TextValue & "'"
			End If

			If Not String.IsNullOrEmpty(ef_text.TextValue) Then
				sql_query_general_params = sql_query_general_params & IIf(sql_query_general_params.Contains("WHERE"), " AND ", " WHERE ") & " GP_GROUP_KEY +'.'+ GP_SUBJECT_KEY LIKE '%" & ef_text.TextValue & "%'"
			End If

			' Order by
			sql_query_general_params = sql_query_general_params & " ORDER BY GP_GROUP_KEY, GP_SUBJECT_KEY"

			If Not IsNothing(m_sql_adap) Then
				m_sql_adap.Dispose()
				m_sql_adap = Nothing
			End If

			m_sql_adap = New SqlDataAdapter(sql_query_general_params, m_sql_conn)
			m_sql_adap.FillSchema(m_sql_dataset, SchemaType.Source)
			m_sql_adap.Fill(m_sql_dataset)

			' Default Values for Table Servers
			m_sql_dataset.Tables(0).Columns("GP_GROUP_KEY").ReadOnly = True
			m_sql_dataset.Tables(0).Columns("GP_SUBJECT_KEY").ReadOnly = True

			m_sql_dataset.Tables(0).Columns.Add("VISIBLE_KEY_VALUE", System.Type.GetType("System.String"))

			m_sql_commandbuilder = New SqlCommandBuilder(m_sql_adap)
			m_sql_adap.UpdateCommand = m_sql_commandbuilder.GetUpdateCommand
			m_sql_adap.DeleteCommand = m_sql_commandbuilder.GetDeleteCommand
			m_sql_adap.InsertCommand = m_sql_commandbuilder.GetInsertCommand

			m_data_view_general_params = New DataView(m_sql_dataset.Tables(0))
			m_data_view_general_params.AllowNew = False
			m_data_view_general_params.AllowDelete = False

			Call FillVisibleKeyValue()

			dg_general_params.Enabled = True
			dg_general_params.DataSource = m_data_view_general_params
			dg_general_params.AllowNavigation = False
			dg_general_params.CaptionVisible = False

			' Delete Permissions
			dg_general_params.AllowDrop = False

			' Columns Style
			If IsNothing(dg_general_params.TableStyles.Item(m_data_view_general_params.Table.TableName)) Then
				table_style = New DataGridTableStyle
				table_style.MappingName = m_data_view_general_params.Table.TableName
				dg_general_params.TableStyles.Add(table_style)
				dg_general_params.TableStyles(0).GridColumnStyles("GP_GROUP_KEY").HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(61)
				dg_general_params.TableStyles(0).GridColumnStyles("GP_SUBJECT_KEY").HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(62)
				dg_general_params.TableStyles(0).GridColumnStyles("GP_KEY_VALUE").HeaderText = ""
				dg_general_params.TableStyles(0).GridColumnStyles("GP_GROUP_KEY").Width = 140
				dg_general_params.TableStyles(0).GridColumnStyles("GP_SUBJECT_KEY").Width = 250
				dg_general_params.TableStyles(0).GridColumnStyles("GP_KEY_VALUE").Width = 0
				dg_general_params.TableStyles(0).GridColumnStyles("VISIBLE_KEY_VALUE").HeaderText = GLB_NLS_GUI_CONFIGURATION.GetString(63)
				dg_general_params.TableStyles(0).GridColumnStyles("VISIBLE_KEY_VALUE").Width = 368
			End If

		Catch ex As Exception
			' Do nothing
			Debug.WriteLine(ex.Message)

		Finally
		End Try

	End Sub

	' PURPOSE: Transform grid data in DB format to the Locale standard format
	'
	'  PARAMS:
	'     - INPUT:
	'     - OUTPUT:
	'
	' RETURNS:
	Private Sub FillVisibleKeyValue()

		Dim idx As Integer

		' Initial TaxOnWon
		For idx = 0 To m_data_view_general_params.Table.Rows.Count - 1

			Select Case m_data_view_general_params.Table.Rows(idx).Item(1).ToString

				Case "FederalTaxOnWonPct"
					federal_tax_on_won = GUI_ParseNumber(GUI_DBNumberToLocalNumber(m_data_view_general_params.Table.Rows(idx).Item(2)))
					m_data_view_general_params.Table.Rows(idx).Item(3) = GUI_FormatNumber(federal_tax_on_won, 2)

				Case "StateTaxOnWonPct"
					state_tax_on_won = GUI_ParseNumber(GUI_DBNumberToLocalNumber(m_data_view_general_params.Table.Rows(idx).Item(2)))
					m_data_view_general_params.Table.Rows(idx).Item(3) = GUI_FormatNumber(state_tax_on_won, 2)

				Case "CardPrice" _
				   , "TaxOnWonPct"
					m_data_view_general_params.Table.Rows(idx).Item(3) = GUI_FormatNumber(GUI_ParseNumber(GUI_DBNumberToLocalNumber(m_data_view_general_params.Table.Rows(idx).Item(2))), 2)

				Case "PrintNameArea_BottomX" _
				   , "PrintNameArea_BottomY" _
				   , "PrintNameArea_FontSize" _
				   , "PrintNameArea_TopX" _
				   , "PrintNameArea_TopY"
					m_data_view_general_params.Table.Rows(idx).Item(3) = GUI_FormatNumber(GUI_ParseNumber(GUI_DBNumberToLocalNumber(m_data_view_general_params.Table.Rows(idx).Item(2))), 0)

				Case Else
					m_data_view_general_params.Table.Rows(idx).Item(3) = m_data_view_general_params.Table.Rows(idx).Item(2)

			End Select

		Next

		m_data_view_general_params.Table.AcceptChanges()

	End Sub

	' PURPOSE: Set default values to filters
	'
	'  PARAMS:
	'     - INPUT:
	'           - None
	'     - OUTPUT:
	'           - None
	'
	' RETURNS:
	'     - None
	Private Sub SetDefaultValues()

		Me.opt_several_groups.Checked = False
		Me.opt_all_groups.Checked = True
		Me.cmb_group_key.Enabled = False
		Me.ef_text.TextValue = ""

	End Sub	'SetDefaultValues

	Private Function DiscardChanges() As Boolean
		Dim dt_changes As DataTable

		If Not IsNothing(m_data_view_general_params) Then
			dt_changes = GUI_GetChanges()

			If Not IsNothing(dt_changes) Then
				If dt_changes.Rows.Count > 0 Then
					If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
						Return False
					End If
				End If
			End If
		End If

		Return True
	End Function

	Private Sub GUI_WriteAuditoryChanges(ByVal Server As Boolean, ByVal ChangesDT As DataTable)
		Dim original_modified_dv As DataView
		Dim auditor_data As CLASS_AUDITOR_DATA
		Dim original_auditor_data As CLASS_AUDITOR_DATA
		Dim idx_change As Integer
		Dim idx_original As Integer
		Dim value_string As String

		If IsNothing(ChangesDT) Then
			Exit Sub
		End If

		original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

		'---------------------------------------------
		' Insert MODIFIED items
		'---------------------------------------------
		auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GENERAL_PARAMS)
		original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GENERAL_PARAMS)

		' Set Parameter
		Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(460), " ")
		Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(460), " ")

		For idx_change = 0 To ChangesDT.Rows.Count - 1

			'
			' Details for current rows
			'

			' Value
			value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("GP_KEY_VALUE")), "", ChangesDT.Rows(idx_change).Item("GP_KEY_VALUE"))
			Call auditor_data.SetField(0, IIf(value_string = "", AUDIT_NONE_STRING, value_string), ChangesDT.Rows(idx_change).Item("GP_GROUP_KEY") & "." & ChangesDT.Rows(idx_change).Item("GP_SUBJECT_KEY"))

			' Search original row
			For idx_original = 0 To original_modified_dv.Count - 1
				If original_modified_dv(idx_original).Item("GP_GROUP_KEY") = ChangesDT.Rows(idx_change).Item("GP_GROUP_KEY") _
				  And original_modified_dv(idx_original).Item("GP_SUBJECT_KEY") = ChangesDT.Rows(idx_change).Item("GP_SUBJECT_KEY") Then
					Exit For
				End If
			Next

			' Critical Error: exit
			If idx_original = original_modified_dv.Count Then
				' Logger message 

				Exit Sub
			End If

			'
			' Details for original rows
			'

			' Value
			value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("GP_KEY_VALUE")), "", original_modified_dv(idx_original).Item("GP_KEY_VALUE"))
			Call original_auditor_data.SetField(0, IIf(value_string = "", AUDIT_NONE_STRING, value_string), original_modified_dv(idx_original).Item("GP_GROUP_KEY") & "." & original_modified_dv(idx_original).Item("GP_SUBJECT_KEY"))

		Next

		If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
								   GLB_CurrentUser.Id, _
								   GLB_CurrentUser.Name, _
								   CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
								   0, _
								   original_auditor_data) Then
			' Logger message 
		End If

		auditor_data = Nothing
		original_auditor_data = Nothing

	End Sub

#End Region

#Region " Public functions "
	' PURPOSE: Opens dialog with default settings for edit mode
	'
	'  PARAMS:
	'     - INPUT:
	'           - none
	'
	'     - OUTPUT:
	'           - none
	'
	' RETURNS:
	'     - none
	Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

		Me.MdiParent = MdiParent
		Me.Display(False)

	End Sub	' ShowForEdit

#End Region

#Region " Options Events "

	Private Sub opt_several_groups_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_groups.Click
		Me.cmb_group_key.Enabled = True
	End Sub


	Private Sub opt_all_groups_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_groups.Click
		Me.cmb_group_key.Enabled = False
	End Sub

#End Region

#Region " DataGrid Events "

	Private Sub dg_general_params_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_general_params.Enter
		Me.CancelButton = Nothing
	End Sub

	Private Sub dg_general_params_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_general_params.Leave
		Me.CancelButton = btn_cancel
	End Sub

#End Region

	Private Sub frm_genaral_params_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		If e.KeyChar = Chr(Keys.Enter) Then
			Call BaseButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
		End If
	End Sub

End Class


