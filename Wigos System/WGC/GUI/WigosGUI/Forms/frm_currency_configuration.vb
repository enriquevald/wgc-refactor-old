'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_currency_configuration
' DESCRIPTION:   Currency configuration.
'
' AUTHOR:        Alberto Marcos
' CREATION DATE: 30-JUL-2013 
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 30-JUL-2013 AMF    Initial version
' 16-AUG-2013 DLL    Errors with examples and combos
' 20-AUG-2013 JBC    Fixed Bug WIG-135: allow the user save change rate with Execute permission and without Write permission
' 20-AUG-2013 JBC    New examples for Bank Card
' 27-AUG-2013 DRV    Errors with checkbox, combobox, saving data and permissions
' 05-SEP-2013 ICS    Fixed Bug WIG-189: Currencies: Configuration form allows to paste negative values
' 10-SEP-2013 ICS    Added suport for Check recharges
' 14-OCT-2013 ANG    Fix bug WIG-64
' 12-DEC-2013 FBA    Fixed bug WIG-481
' 19-FEB-2014 AMF    Fixed Bug WIG-647: Control minimum version
' 10-APR-2014 DLL    Added two columns for old and new Decimals
' 05-MAY-2016 ETP    PBI 12691: Added Multidivisa config.
' 14-MAY-2016 ETP    PBI 14340: Added uc_currency_selector to bank card & check config.
' 29-JUN-2016 ETP    Fixed bug 14938: Countr: Excepci�n no controlada al acceder a "operaciones con tarjeta/cheque/divisa"
' 30-JUN-2016 ESE    Fixed bug 13577: Disabled/enabled text box
' 20-SEP-2016 FOS    Fixed bug 17837: Delete 'X01' denomination
' 19-OCT-2016 RGR    Fixed bug 17108: The system allows the currency exchange without informing or zero value
' 28-OCT-2016 RGR    Fixed bug 19799: MALTA: exception not controllerd with credit card payments enabled
' 22-NOV-2016 JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
' 18-JUL-2017 ETP    WIGOS-3700 Cage - An error is registered when don't have foreign currencies.
'-------------------------------------------------------------------

#Region " Imports "

Option Strict Off
Option Explicit On

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text
Imports WSI.Common.PinPad

#End Region

Public Class frm_currency_configuration
  Inherits frm_base_edit


#Region " Constants "

  Private Const RECHARGE_VALUE_TO_CALCULATE = 100
  Private Const FORM_DB_MIN_VERSION As Short = 201
  Private Const UC_CURRENCY_NAME As String = "CURRENCY"
  Private Const UC_CURRENCY_POSITION_X As Integer = 15
  Private Const UC_CURRENCY_POSITION_Y As Integer = 20
  Private Const UC_CURRENCY_WIDTH As Integer = 500
  Private Const UC_CARD_NAME As String = "CARD"
  Private Const UC_CARD_POSITION_X As Integer = 15
  Private Const UC_CARD_POSITION_Y As Integer = 20
  Private Const TAB_CHECK_NAME As String = "TabCheck"
  Private Const TAB_BANK_CARD_NAME As String = "TabBankCard"

#End Region ' Constants

#Region " Members "

  Private m_national_ISO_code As String
  Private m_foreign_ISO_code As String
  Private m_chk_foreign_currecny_old As String
  Private m_foreign_decimal As Decimal
  Private m_currency_configuration As CLS_CURRENCY_CONFIGURATION
  Private m_national_datarow As DataRow()
  Private m_tab_order_changed As Boolean
  Private m_is_multi_currency As Boolean

#End Region ' Members

#Region " Overrides "

  'PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()
    m_is_multi_currency = WSI.Common.Misc.IsMultiCurrencyExchangeEnabled()


    Dim _currency_configuration As CLS_CURRENCY_CONFIGURATION
    _currency_configuration = Me.DbEditedObject

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2429)

    ' National Currency
    Me.ef_foreign_currency_voucher_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_bank_card_voucher_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_check_voucher_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_check_cash_advance_voucher_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.uc_bank_card_commission.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.uc_check_commission.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.uc_exchange_commission.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_bank_card_cash_advance_voucher_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_national_currency.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT)
    Me.ef_national_currency.IsReadOnly = True
    Me.ef_national_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2072)

    ' Credit Card
    Me.TabBankCard.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2558)
    Me.ef_bank_card_voucher_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)
    Me.chk_bank_card_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)
    Me.chk_credit_card.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2431)

    Me.chk_card_specification.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5464)
    Me.chk_bank_data_input.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5465)
    Me.TabRecharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
    Me.TabCashAdvance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)
    Me.TabCardComission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
    Me.chk_bank_card_cash_advance_hide_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)
    Me.ef_bank_card_cash_advance_voucher_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)
    Me.uc_bank_card_commission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)

    ' Check
    Me.TabCheck.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2594)
    Me.ef_check_voucher_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)
    Me.chk_check_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)
    Me.chk_check_cash_advance_hide_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)
    Me.chk_check.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2595)

    Me.chk_bank_check_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5465)
    Me.TabCheckRecharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
    Me.TabCheckCardAdvance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)
    Me.TabCheckCommission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
    Me.ef_check_cash_advance_voucher_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)
    Me.uc_check_commission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)

    ' Foreign Currency
    Me.gb_foreign_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2559)
    Me.ef_foreign_currency_voucher_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)
    Me.chk_foreign_currency_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2537)
    Me.TabExchangeRecharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
    Me.TabExchangeCommission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
    Me.uc_exchange_commission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2536)
    Me.lbl_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7305)

    If Permissions.Execute And Not Permissions.Write Then
      Me.chk_credit_card.Enabled = False

      Me.ef_bank_card_voucher_title.IsReadOnly = True
      Me.uc_bank_card_commission.IsReadOnly = True
      Me.chk_bank_card_voucher.Enabled = False
      Me.chk_foreign_currency_voucher.Enabled = False
      Me.ef_foreign_currency_voucher_title.Enabled = False

      Me.ef_check_voucher_title.IsReadOnly = True
      Me.chk_check_voucher.Enabled = False
      Me.chk_bank_card_cash_advance_hide_voucher.Enabled = False
      Me.ef_bank_card_cash_advance_voucher_title.IsReadOnly = True
      Me.chk_check_cash_advance_hide_voucher.Enabled = False
      Me.ef_check_cash_advance_voucher_title.IsReadOnly = True
      Me.uc_bank_card_commission.Enabled = False
      Me.uc_bank_card_commission.IsReadOnly = True
      Me.uc_check_commission.Enabled = False
      Me.uc_check_commission.IsReadOnly = True
      Me.uc_exchange_commission.Enabled = False
      Me.uc_exchange_commission.IsReadOnly = True
    End If

    ef_bank_card_cash_advance_voucher_title.TextVisible = True
    ef_bank_card_voucher_title.TextVisible = True

    ef_check_cash_advance_voucher_title.TextVisible = True

    ef_check_voucher_title.TextVisible = True
    ef_foreign_currency_voucher_title.TextVisible = True
    ef_national_currency.TextVisible = True
    Me.uc_bank_card_commission.TextVisible = True
    Me.uc_check_commission.TextVisible = True
    Me.uc_exchange_commission.TextVisible = True

    Me.uc_button_left.Text = "<--"
    Me.uc_button_right.Text = "-->"

    uc_button_left.Visible = m_is_multi_currency
    uc_button_right.Visible = m_is_multi_currency
    lbl_order.Visible = m_is_multi_currency
    Me.CurrencyTab.Visible = m_is_multi_currency

    m_tab_order_changed = False

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write Or Me.Permissions.Execute

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FirstActivation()

    Dim _sql_ctx As Integer

    m_currency_configuration = Me.DbReadObject

    If String.IsNullOrEmpty(m_currency_configuration.Currency_ISO_Code_GP) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3014), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Me.Dispose(True)
      Me.Close()
      Exit Sub
    End If

    m_national_ISO_code = m_currency_configuration.Currency_ISO_Code_GP

    m_national_datarow = m_currency_configuration.Currencies.Select("ISO_CODE = '" & m_currency_configuration.Currency_ISO_Code_GP & "'")

    If m_national_datarow.Length = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3015), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      'Me.Dispose(True)
      'Me.Close()
    End If

    GUI_SetScreenData(_sql_ctx)

  End Sub

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _selected_rows As DataRow()
    Dim _currency_configuration As CLS_CURRENCY_CONFIGURATION
    Dim _foreign_accepted As String()
    Dim _foreign_iso_code As String
    Dim _currency As CURRENCIES_ENABLED
    Dim _uc_check_selector As uc_currency_selector

    If Me.IsDisposed Then
      Return
    End If



    ' Check if there are available currencies
    If Not m_currency_configuration.Currencies.Rows.Count > 0 Then
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
      tab_national_currency.Enabled = False
      gb_foreign_currency.Enabled = False
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2480), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    ' Check if there are available foreign currencies
    If Not m_currency_configuration.Currencies.Rows.Count > 1 Then
      gb_foreign_currency.Enabled = False
    End If

    ' select national currency
    If Not m_national_datarow.Length = 0 Then
      ef_national_currency.Value = m_national_datarow(0)("DESCRIPTION")
    Else
      ef_national_currency.Value = m_currency_configuration.Currency_ISO_Code_GP
    End If

    SetNationalCurrencyData(m_currency_configuration)

    If Permissions.Write Then
      chk_bank_card_voucher.Enabled = chk_credit_card.Checked
      chk_check_voucher.Enabled = chk_check.Checked
    End If

    If chk_credit_card.Checked Then
      ef_bank_card_voucher_title.Enabled = Not chk_bank_card_voucher.Checked
      ef_bank_card_cash_advance_voucher_title.Enabled = Not chk_bank_card_cash_advance_hide_voucher.Checked
      Me.uc_bank_card_commission.Enabled = True

      chk_card_specification.Checked = m_currency_configuration.SpecificBankCardTypes
      chk_bank_data_input.Checked = m_currency_configuration.EditBankCardTransactionData
    Else
      ef_bank_card_voucher_title.Enabled = chk_credit_card.Checked
      ef_bank_card_cash_advance_voucher_title.Enabled = chk_credit_card.Checked
      chk_card_specification.Enabled = False
      chk_bank_data_input.Enabled = False
      chk_bank_card_cash_advance_hide_voucher.Enabled = False
      Me.uc_bank_card_commission.Enabled = False
    End If

    If chk_check.Checked Then
      ef_check_voucher_title.Enabled = Not chk_check_voucher.Checked
      chk_bank_check_data.Checked = m_currency_configuration.EditBankCheckTransactionData
      Me.uc_check_commission.Enabled = True

    Else
      ef_check_voucher_title.Enabled = chk_check.Checked
      chk_bank_check_data.Enabled = chk_check.Checked
      Me.uc_check_commission.Enabled = False

      chk_check_cash_advance_hide_voucher.Enabled = chk_check.Checked
      ef_check_cash_advance_voucher_title.Enabled = chk_check.Checked


      _uc_check_selector = GetCurrencyByTabPage(tab_national_currency, TAB_CHECK_NAME, GetControlNameByTabName(TAB_CHECK_NAME))
      _uc_check_selector.set_enabled(chk_check.Checked)
    End If

    ' Set data into Combo Foreign Currencies
    _selected_rows = m_currency_configuration.ForeignCurrencies.Select("ISO_CODE = '" & m_currency_configuration.Currency_ISO_Code_GP & "'")

    If Not _selected_rows.Length = 0 Then
      m_currency_configuration.ForeignCurrencies.Rows.Remove(_selected_rows(0))
    End If

    _currency_configuration = Me.DbEditedObject

    If (m_is_multi_currency) Then
      For Each Row As DataRow In m_currency_configuration.ForeignCurrencies.Rows
        _currency = New CURRENCIES_ENABLED()
        If Not (_currency_configuration.ForeignCurrencyExchangeList.TryGetValue(Row("ISO_CODE"), _currency)) Then
          Continue For
        End If
        AddCurrencyTab(CurrencyTab, Row("ISO_CODE"), Row("DESCRIPTION"), _currency_configuration.ForeignCurrencyExchangeList(Row("ISO_CODE")), _currency_configuration.ForeignCurrencies)
      Next
    Else
      If (_currency_configuration.ForeignCurrencyExchangeList.Count > 0) Then

        _foreign_iso_code = m_currency_configuration.Foreign_Currency_ISO_Code_GP
        If String.IsNullOrEmpty(_foreign_iso_code) Then
          _foreign_iso_code = m_currency_configuration.ForeignCurrencies.Rows(0)("ISO_CODE")
        End If

        _foreign_accepted = _foreign_iso_code.Split(",")
        _currency = New CURRENCIES_ENABLED()

        If Not (_currency_configuration.ForeignCurrencyExchangeList.TryGetValue(_foreign_accepted.GetValue(0), _currency)) Then

          For Each _keyvalue As KeyValuePair(Of String, CURRENCIES_ENABLED) In _currency_configuration.ForeignCurrencyExchangeList
            Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
            _currency = _cur
            Exit For
          Next
          Log.Warning("currencies_accepted is incorrect, load default currency")
        End If

        AddCurrencyControl(_currency, _currency_configuration.ForeignCurrencies)
      End If

    End If

    ef_foreign_currency_voucher_title.Value = m_currency_configuration.ForeignCurrency_Title
    chk_foreign_currency_voucher.Checked = m_currency_configuration.Hide_ForeignCurrency_Voucher


    Me.uc_exchange_commission.Value = m_currency_configuration.CurrencyVoucherCommission

    ef_foreign_currency_voucher_title.Enabled = Not chk_foreign_currency_voucher.Checked
    uc_exchange_commission.Enabled = Not chk_foreign_currency_voucher.Checked

    If Not GeneralParam.GetBoolean("Cashier", "Split.B.Enabled") OrElse Not WSI.Common.Misc.IsCommissionToCompanyBEnabled() Then
      Me.tab_bank_card_vouchers.TabPages.Remove(Me.TabCardComission)
      Me.tab_check_vouchers.TabPages.Remove(Me.TabCheckCommission)
      Me.tab_currency_exchange_vouchers.TabPages.Remove(Me.TabExchangeCommission)
    End If


    Call RefreshLabelTextsForeignCurrency()
    Call RefreshLabelTextsBankCard()
    Call RefreshLabelTextsCheck()

    AddHandler chk_credit_card.CheckedChanged, AddressOf chk_credit_card_CheckedChanged
    AddHandler chk_check.CheckedChanged, AddressOf chk_check_CheckedChanged
    AddHandler chk_foreign_currency_voucher.CheckedChanged, AddressOf chk_foreign_currency_voucher_tittle_CheckedChanged
    AddHandler chk_bank_card_voucher.CheckedChanged, AddressOf chk_bank_card_voucher_title_CheckedChanged
    AddHandler chk_bank_card_cash_advance_hide_voucher.CheckedChanged, AddressOf ch_cash_advance_hide_voucher_CheckedChanged
    AddHandler chk_check_voucher.CheckedChanged, AddressOf chk_check_voucher_title_CheckedChanged
    AddHandler chk_check_cash_advance_hide_voucher.CheckedChanged, AddressOf chk_check_cash_advance_hide_voucher_CheckedChange
    AddHandler Timer1.Tick, AddressOf Timer1_Tick

    For Each ctrl As TabPage In tab_national_currency.TabPages
      If ctrl.Name = TAB_CHECK_NAME Or ctrl.Name = TAB_CHECK_NAME Then
        Continue For
      End If
      AddHandler ctrl.Enter, AddressOf TabPageOnEnter
    Next


  End Sub 'GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _currency_configuration As CLS_CURRENCY_CONFIGURATION
    Dim _general_params As WSI.Common.GeneralParam.Dictionary
    Dim _isocodelist As List(Of String)
    Dim _isocode As String
    Dim _uc_currency_selector As uc_currency_selector
    Dim _uc_card_selector As uc_currency_selector
    Dim _uc_check_selector As uc_currency_selector
    Dim _tab_control As TabControl

    _general_params = WSI.Common.GeneralParam.Dictionary.Create()
    Try
      If Me.IsDisposed Then
        Return
      End If

      _currency_configuration = Me.DbEditedObject

      _currency_configuration.Currencies_Accepted_GP = _general_params.GetString("RegionalOptions", "CurrencyISOCode")

      _currency_configuration.ForeignCurrency_Title = Me.ef_foreign_currency_voucher_title.Value
      _currency_configuration.BankCard_Title = ef_bank_card_voucher_title.Value
      _currency_configuration.Check_Title = ef_check_voucher_title.Value
      _currency_configuration.Hide_BankCard_Voucher = Me.chk_bank_card_voucher.Checked
      _currency_configuration.Hide_Check_Voucher = Me.chk_check_voucher.Checked

      _currency_configuration.HideBankCardVoucherCashAdvance = Me.chk_bank_card_cash_advance_hide_voucher.Checked
      _currency_configuration.BankCardVoucherCashAdvanceTitle = Me.ef_bank_card_cash_advance_voucher_title.Value

      If GeneralParam.GetBoolean("Cashier", "Split.B.Enabled") And WSI.Common.Misc.IsCommissionToCompanyBEnabled() Then
        _currency_configuration.BankCardVoucherCommission = Me.uc_bank_card_commission.Value
        _currency_configuration.CheckVoucherCommission = Me.uc_check_commission.Value
        _currency_configuration.CurrencyVoucherCommission = Me.uc_exchange_commission.Value
      End If

      _currency_configuration.HideCheckVoucherCashAdvance = Me.chk_check_cash_advance_hide_voucher.Checked
      _currency_configuration.CheckVoucherCashAdvanceTitle = Me.ef_check_cash_advance_voucher_title.Value

      If Me.chk_check.Checked Then
        _currency_configuration.EditBankCheckTransactionData = Me.chk_bank_check_data.Checked
      Else
        _currency_configuration.EditBankCheckTransactionData = False
      End If

      _currency_configuration.Hide_ForeignCurrency_Voucher = Me.chk_foreign_currency_voucher.Checked


      'Credit Card Data

      _currency_configuration.BankCardEnabled = chk_credit_card.Checked

      If Me.chk_credit_card.Checked Then
        _currency_configuration.EditBankCardTransactionData = Me.chk_bank_data_input.Checked
        _currency_configuration.SpecificBankCardTypes = Me.chk_card_specification.Checked
      Else
        _currency_configuration.EditBankCardTransactionData = False
        _currency_configuration.SpecificBankCardTypes = False
      End If



      _tab_control = Tab_Credit_Card_Type
      If (Tab_Credit_Card_Type.TabPages.Count = 0) Then
        _tab_control = tab_national_currency
        Tab_Credit_Card_Type.Visible = False
      End If

      For Each _tab_page As TabPage In _tab_control.TabPages
        If _tab_page.Name = TAB_CHECK_NAME Then
          Continue For
        End If
        _uc_card_selector = CType(_tab_page.Controls(GetControlNameByTabName(_tab_page.Name)), uc_currency_selector)
        _uc_card_selector.GUI_GetScreenData()
        _currency_configuration.BankCardCurrencyExchangeList(_uc_card_selector.BankCard) = _uc_card_selector.ForeignCurrency
      Next


      'Check Data
      _currency_configuration.CheckCurrencyExchange.Status = chk_check.Checked



      _uc_check_selector = GetCurrencyByTabPage(tab_national_currency, TAB_CHECK_NAME, GetControlNameByTabName(TAB_CHECK_NAME))
      _uc_check_selector.GUI_GetScreenData()
      _currency_configuration.CheckCurrencyExchange = _uc_check_selector.ForeignCurrency.foreign_currency_exchange


      'Foreign Currency Data


      If (m_is_multi_currency) Then
        _isocodelist = New List(Of String)


        For Each _tab_page As TabPage In CurrencyTab.TabPages
          _uc_currency_selector = _tab_page.Controls(0)

          _uc_currency_selector.GUI_GetScreenData()
          _isocode = _uc_currency_selector.ForeignCurrency.foreign_currency_exchange.CurrencyCode

          _currency_configuration.ForeignCurrencyExchangeList.Item(_isocode) = _uc_currency_selector.ForeignCurrency


          If (_uc_currency_selector.ForeignCurrency.Enabled) Then
            _isocodelist.Add(_isocode)
            _currency_configuration.Currencies_Accepted_GP &= ";" & _isocode
          End If
        Next

        _currency_configuration.Foreign_Currency_ISO_Code_GP = String.Join(",", _isocodelist.ToArray())
      Else
        _uc_currency_selector = GetCurrencyByControl(Me.gb_foreign_currency, UC_CURRENCY_NAME)

        If Not (_uc_currency_selector Is Nothing) Then
          _uc_currency_selector.GUI_GetScreenData()
          _isocode = _uc_currency_selector.ForeignCurrency.foreign_currency_exchange.CurrencyCode

          _currency_configuration.ForeignCurrencyExchangeList.Item(_isocode) = _uc_currency_selector.ForeignCurrency

          If (_uc_currency_selector.ForeignCurrency.Enabled) Then
            _currency_configuration.Foreign_Currency_ISO_Code_GP = _isocode
            _currency_configuration.Currencies_Accepted_GP &= ";" & _isocode
          End If
        End If

      End If

    Catch ex As Exception
      ' Save error in log
      Log.Error("Currency configuration: Error when save configuration.")
      Log.Exception(ex)
    End Try

  End Sub 'GUI_GetScreenData




  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim currency_configuration As CLS_CURRENCY_CONFIGURATION

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '''Wrong DbVersion message already shown, do not show again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_CURRENCY_CONFIGURATION
        currency_configuration = DbEditedObject

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CURRENCY_CONFIGURATION

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Overrides the GUI_ButtonClick to allow the user save change rate
  '          when they have Execute permission but no Write permission.
  '
  '  PARAMS:
  '     - INPUT:
  '       - ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)



    If ButtonId = ENUM_BUTTON.BUTTON_OK AndAlso Permissions.Execute AndAlso Permissions.Write Then
      ApplyButton()
    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If

  End Sub ' GUI_ButtonClick
  ' PURPOSE:  Apply current configuration
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ApplyButton()

    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    If (m_tab_order_changed) Then
      SaveCurrencyOrder()
    End If

    If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
      If Me.CloseOnOkClick Then
        Call Me.Close()
      End If

      Exit Sub
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      If Me.CloseOnOkClick Then
        Call Me.Close()
      End If
    End If
  End Sub
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _control As uc_currency_selector
    Dim _tab_control As TabControl

    _tab_control = Tab_Credit_Card_Type
    If (Tab_Credit_Card_Type.TabPages.Count = 0) Then
      _tab_control = tab_national_currency
      Tab_Credit_Card_Type.Visible = False
    End If

    For Each _tab_page As TabPage In _tab_control.TabPages
      _control = CType(_tab_page.Controls(GetControlNameByTabName(_tab_page.Name)), uc_currency_selector)

      If Not (_control.IsControlDataOk()) Then
        _tab_control.SelectedTab = _tab_page
        Return False
      End If

    Next

    For Each _tab_page As TabPage In CurrencyTab.TabPages
      _control = _tab_page.Controls(0)

      If Not _control.IsControlDataOk() Then
        CurrencyTab.SelectedTab = _tab_page
        Return False

      End If

    Next

    'RGR 28-OCT-2016 
    If Not m_is_multi_currency Then

      If Me.gb_foreign_currency.Controls.ContainsKey(UC_CURRENCY_NAME) Then
        _control = Me.gb_foreign_currency.Controls(UC_CURRENCY_NAME)

        If Not _control.IsControlDataOk() Then
          Return False

        End If

      End If
    End If

    If (Not chk_bank_card_voucher.Checked) AndAlso ef_bank_card_voucher_title.Value = "" AndAlso (chk_credit_card.Checked) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_bank_card_voucher_title.Text, 0, 100)
      Call Me.ef_bank_card_voucher_title.Focus()

      Return False
    End If

    If (Not chk_bank_card_cash_advance_hide_voucher.Checked) AndAlso ef_bank_card_cash_advance_voucher_title.Value = "" AndAlso (chk_credit_card.Checked) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_bank_card_cash_advance_voucher_title.Text, 0, 100)
      Call Me.ef_bank_card_cash_advance_voucher_title.Focus()

      Return False
    End If

    If (Not chk_check_cash_advance_hide_voucher.Checked) AndAlso ef_check_cash_advance_voucher_title.Value = "" AndAlso (chk_check.Checked) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_check_cash_advance_voucher_title.Text, 0, 100)
      Call Me.ef_check_cash_advance_voucher_title.Focus()

      Return False
    End If

    If (Not chk_check_voucher.Checked) AndAlso ef_check_voucher_title.Value = "" AndAlso (chk_check.Checked) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_check_voucher_title.Text, 0, 100)
      Call Me.ef_check_voucher_title.Focus()

      Return False
    End If


    If WSI.Common.Misc.IsCommissionToCompanyBEnabled() Then
      If chk_credit_card.Checked AndAlso uc_bank_card_commission.Value = "" Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , uc_bank_card_commission.Text, 0, 100)
        Call Me.uc_bank_card_commission.Focus()

        Return False
      End If

      If chk_check.Checked AndAlso uc_check_commission.Value = "" Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , uc_check_commission.Text, 0, 100)
        Call Me.uc_check_commission.Focus()

        Return False
      End If


    End If


    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Check pending movements 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Bolean
  '
  ' RETURNS:
  '     - Boolean. True: there is movements. False: not movements.
  Private Function IsPendingMovements(ByVal ISOCode As String, Optional ByVal Denomination As String = "") As Boolean

    Dim currency_data As CLS_CURRENCY_CONFIGURATION
    Dim _num_CAA_denomi As Integer
    Dim _dt_restricted As DataTable

    Try

      currency_data = Me.DbReadObject

      Using _db_trx As New DB_TRX()

        _dt_restricted = currency_data.GetRestrictedAmountMovements(_db_trx)

        If Not String.IsNullOrEmpty(Denomination) And Denomination <> "0" Then
          Denomination = GUI_LocalNumberToDBNumber(Denomination) 'WSI.Common.Format.LocalNumberToDBNumber(Denomination)
          _num_CAA_denomi = _dt_restricted.Select(" CMD_ISO_CODE = '" & ISOCode & "' AND CMD_DENOMINATION = " & Denomination).Length
        Else
          _num_CAA_denomi = _dt_restricted.Select(" CMD_ISO_CODE = '" & ISOCode & "' ").Length

        End If

        If _num_CAA_denomi >= 1 Then
          Return True
        Else
          Return False
        End If

      End Using

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "IsPendingMovements", _
                            ex.Message)

      ' Exception error
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Function

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Call RefreshLabelTextsForeignCurrency()
    Call RefreshLabelTextsBankCard()
    Call RefreshLabelTextsCheck()
  End Sub

  Public Sub SetNationalCurrencyData(ByRef Currency_Configuration As CLS_CURRENCY_CONFIGURATION)

    Dim _uc_credit_card_selector As uc_currency_selector
    Dim _uc_check_selector As uc_currency_selector
    Dim _check_currency As CURRENCIES_ENABLED

    ' Credit Card
    ef_bank_card_voucher_title.Value = Currency_Configuration.BankCard_Title
    chk_bank_card_voucher.Checked = Currency_Configuration.Hide_BankCard_Voucher
    ef_bank_card_voucher_title.Enabled = Not chk_bank_card_voucher.Checked

    If GeneralParam.GetBoolean("Cashier", "Split.B.Enabled") And WSI.Common.Misc.IsCommissionToCompanyBEnabled() Then
      uc_bank_card_commission.Value = Currency_Configuration.BankCardVoucherCommission
      Me.uc_check_commission.Value = Currency_Configuration.CheckVoucherCommission
      Me.uc_exchange_commission.Value = Currency_Configuration.CurrencyVoucherCommission
    End If

    ef_bank_card_cash_advance_voucher_title.Value = Currency_Configuration.BankCardVoucherCashAdvanceTitle
    chk_bank_card_cash_advance_hide_voucher.Checked = Currency_Configuration.HideBankCardVoucherCashAdvance
    ef_bank_card_voucher_title.Enabled = Not chk_bank_card_cash_advance_hide_voucher.Checked

    uc_bank_card_commission.Value = Currency_Configuration.BankCardVoucherCommission

    chk_credit_card.Checked = Currency_Configuration.BankCardEnabled

    _uc_credit_card_selector = New uc_currency_selector(EntryMode.CardCheck, Permissions)


    Dim _tab_control As TabControl
    _tab_control = Tab_Credit_Card_Type
    If (Currency_Configuration.BankCardCurrencyExchangeList.Count = 1) Then
      _tab_control = tab_national_currency
      Tab_Credit_Card_Type.Visible = False
    End If

    For Each _keyvalue As KeyValuePair(Of bank_card, CURRENCIES_ENABLED) In Currency_Configuration.BankCardCurrencyExchangeList
      Dim _cur As CURRENCIES_ENABLED = _keyvalue.Value
      Dim _card_type As bank_card = _keyvalue.Key

      CreateCreditCardPage(_tab_control, _card_type, _cur)
    Next



    chk_credit_card_CheckedChanged(Nothing, Nothing)


    MoveBankCardControls(tab_national_currency.SelectedTab)

    ' Check


    chk_check.Checked = Currency_Configuration.CheckCurrencyExchange.Status

    _uc_check_selector = New uc_currency_selector(EntryMode.CardCheck, Permissions)

    _check_currency = New CURRENCIES_ENABLED()

    _check_currency.foreign_currency_exchange = Currency_Configuration.CheckCurrencyExchange.Copy()

    _uc_check_selector.InitializeControl(_check_currency, Nothing, "", m_national_ISO_code)

    _uc_check_selector.Name = GetControlNameByTabName(TAB_CHECK_NAME)
    _uc_check_selector.Width = UC_CURRENCY_WIDTH

    tab_national_currency.TabPages(TAB_CHECK_NAME).Controls.Add(_uc_check_selector)

    _uc_check_selector.set_enabled(_uc_check_selector.Enabled)
    _uc_check_selector.Location = New System.Drawing.Point(0, 35)


    ef_check_voucher_title.Value = Currency_Configuration.Check_Title
    chk_check_voucher.Checked = Currency_Configuration.Hide_Check_Voucher
    ef_check_voucher_title.Enabled = Not chk_check_voucher.Checked

    uc_check_commission.Value = Currency_Configuration.CheckVoucherCommission

    ef_check_cash_advance_voucher_title.Value = Currency_Configuration.CheckVoucherCashAdvanceTitle
    chk_check_cash_advance_hide_voucher.Checked = Currency_Configuration.HideCheckVoucherCashAdvance
    ef_check_cash_advance_voucher_title.Enabled = Not chk_check_cash_advance_hide_voucher.Checked

  End Sub ' SetNationalCurrencyData


  Private Sub CreateCreditCardPage(ByVal TabControl As TabControl, ByVal CardType As bank_card, ByVal Currency As CURRENCIES_ENABLED)
    Dim _card_currency As CURRENCIES_ENABLED
    Dim _uc_credit_card_selector As uc_currency_selector
    Dim _tap_page As TabPage
    Dim _tab_name As String
    Dim _control_name As String
    Dim _tab_text As String
    _tab_name = String.Empty
    _control_name = String.Empty
    _tab_text = String.Empty

    _card_currency = New CURRENCIES_ENABLED()
    _uc_credit_card_selector = New uc_currency_selector(EntryMode.CardCheck, Permissions)

    _card_currency.foreign_currency_exchange = Currency.foreign_currency_exchange.Copy()

    Get_Tab_Data(CardType, _tab_name, _control_name, _tab_text)
    _uc_credit_card_selector.InitializeControl(_card_currency, Nothing, _tab_text.ToLower(), m_national_ISO_code)



    _uc_credit_card_selector.Name = _control_name
    _uc_credit_card_selector.Width = UC_CURRENCY_WIDTH

    If Not (CardType.card_type = CARD_TYPE.NONE And CardType.card_scope = CARD_SCOPE.NONE) Then
      _tap_page = New TabPage(_tab_name)
      TabControl.TabPages.Add(_tap_page)

      _tap_page.Name = _tab_name
      _tap_page.Text = _tab_text
      _uc_credit_card_selector.Location = New System.Drawing.Point(0, -15)
    Else
      TabBankCard.Parent = TabControl
      _uc_credit_card_selector.Location = New System.Drawing.Point(0, 35)

    End If

    _tap_page = TabControl.TabPages(_tab_name)
    _tap_page.Controls.Add(_uc_credit_card_selector)


    _uc_credit_card_selector.set_enabled(_card_currency.Enabled)

  End Sub

  Public Sub Get_Tab_Data(ByVal Bank_Card_Type As bank_card, ByRef Tab_Name As String, ByRef Control_Name As String, ByRef Tab_Text As String)

    Tab_Name = TAB_BANK_CARD_NAME
    Tab_Text = String.Empty
    Select Case Bank_Card_Type.card_type
      Case CARD_TYPE.NONE
      Case CARD_TYPE.DEBIT
        Tab_Name += "D"
        Tab_Text = "D�bito"
      Case CARD_TYPE.CREDIT
        Tab_Name += TAB_BANK_CARD_NAME + "C"
        Tab_Text = "Cr�dito"
      Case Else
        Tab_Name = String.Empty
        Control_Name = String.Empty
        Log.Error("Bank card type not defined: " + Bank_Card_Type.card_type)
        Return
    End Select

    If Not (String.IsNullOrEmpty(Tab_Text)) Then
      Tab_Text += " "
    End If

    Select Case Bank_Card_Type.card_scope
      Case CARD_SCOPE.NONE
      Case CARD_SCOPE.NATIONAL
        Tab_Name += "N"
        Tab_Text += "Nacional"
      Case CARD_SCOPE.INTERNATIONAL
        Tab_Name += "I"
        Tab_Text += "Internacional"
      Case Else
        Tab_Name = String.Empty
        Control_Name = String.Empty
        Log.Error("Bank card scope not defined: " + Bank_Card_Type.card_scope)
        Return
    End Select


    Control_Name = GetControlNameByTabName(Tab_Name)

  End Sub

  ' PURPOSE:  Add a new tab for multiCurrencyExchange
  '  PARAMS:
  '     - INPUT:
  '           - IsoCode
  '           - Description
  '           - Currency (Current)
  '           - Currencies (all)
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddCurrencyTab(ByVal TabControl As TabControl, ByVal IsoCode As String, ByVal Description As String, ByVal Currency As CURRENCIES_ENABLED, ByVal Currencies As DataTable)

    Dim _new_tap_page As TabPage
    _new_tap_page = New TabPage(IsoCode)
    Dim _control As uc_currency_selector


    _control = New uc_currency_selector(EntryMode.Multicurrency, Permissions)
    _control.InitializeControl(Currency, Currencies, Description, m_national_ISO_code)



    _new_tap_page.Controls.Add(_control)

    TabControl.TabPages.Add(_new_tap_page)



  End Sub

  ' PURPOSE:  Add a new control for OneCurrencyExchange
  '  PARAMS:
  '     - INPUT:
  '           - Currency (Current)
  '           - Currencies (all)
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddCurrencyControl(ByVal Currency As CURRENCIES_ENABLED, ByVal Currencies As DataTable)

    Dim _control As uc_currency_selector


    _control = New uc_currency_selector(EntryMode.OneCurrency, Permissions)
    _control.InitializeControl(Currency, Currencies, "", m_national_ISO_code)


    _control.Name = UC_CURRENCY_NAME
    _control.Location = New System.Drawing.Point(UC_CURRENCY_POSITION_X, UC_CURRENCY_POSITION_Y)

    _control.Width = UC_CURRENCY_WIDTH
    gb_foreign_currency.Controls.Add(_control)



  End Sub 'AddCurrencyControl




  ' PURPOSE:  Update the values from labels
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshLabelTextsForeignCurrency()
    Dim _uc_currency_selector As uc_currency_selector


    If (m_is_multi_currency) Then
      If (CurrencyTab.TabCount > 0 AndAlso Not CurrencyTab.SelectedTab Is Nothing AndAlso CurrencyTab.SelectedTab.Controls.Count > 0) Then
        _uc_currency_selector = CurrencyTab.SelectedTab.Controls(0)
        _uc_currency_selector.RefreshLabelTextsForeignCurrency()

      End If

    Else
      _uc_currency_selector = GetCurrencyByControl(Me.gb_foreign_currency, UC_CURRENCY_NAME)
      If Not _uc_currency_selector Is Nothing Then
        _uc_currency_selector.RefreshLabelTextsForeignCurrency()
      End If
    End If

  End Sub ' RefreshLabelTextsForeignCurrency

  ' PURPOSE:  Update the values from labels of Bank Card payment
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshLabelTextsBankCard()
    Dim _uc_currency_selector As uc_currency_selector
    Dim _tab_page As TabPage
    Dim _tab_control As TabControl


    _tab_control = Tab_Credit_Card_Type
    If (Tab_Credit_Card_Type.TabPages.Count = 0) Then
      _tab_control = tab_national_currency
      Tab_Credit_Card_Type.Visible = False
    End If

    If (_tab_control.TabCount > 0 AndAlso Not _tab_control.SelectedTab Is Nothing AndAlso _tab_control.SelectedTab.Controls.Count > 0) Then

      Try
        _tab_page = _tab_control.SelectedTab

        _uc_currency_selector = _tab_control.SelectedTab.Controls(GetControlNameByTabName(_tab_page.Name))
        _uc_currency_selector.RefreshLabelTextsBankCard()
      Catch ex As Exception

      End Try



    End If

  End Sub ' RefreshLabelTextsBankCard

  ' PURPOSE:  Update the values from labels of Check payment
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshLabelTextsCheck()
    Dim _uc_currency_selector As uc_currency_selector

    _uc_currency_selector = GetCurrencyByTabPage(tab_national_currency, TAB_CHECK_NAME, GetControlNameByTabName(TAB_CHECK_NAME))

    If Not _uc_currency_selector Is Nothing Then
      _uc_currency_selector.RefreshLabelTextsCheck()
    End If

    'If (tab_national_currency.TabCount > 0 AndAlso Not tab_national_currency.SelectedTab Is Nothing AndAlso tab_national_currency.SelectedTab.Controls.Count > 0) Then
    '  _uc_currency_selector = tab_national_currency.SelectedTab.Controls(0)
    '  _uc_currency_selector.RefreshLabelTextsForeignCurrency()

    'End If

  End Sub ' RefreshLabelTextsBankCard

  Private Sub MoveTabTo(Left As Boolean)

    If (CurrencyTab.TabPages.Count = 0) Then
      Return
    End If

    Dim _selected_tab As TabPage
    _selected_tab = CurrencyTab.SelectedTab
    Dim _selected_index As Int32
    _selected_index = CurrencyTab.SelectedIndex

    If Left Then
      If (_selected_index <> 0) Then
        CurrencyTab.TabPages.Remove(_selected_tab)
        CurrencyTab.TabPages.Insert(_selected_index - 1, _selected_tab) 'add on specific position
        m_tab_order_changed = True
      End If
    Else
      If (_selected_index + 1 < CurrencyTab.TabPages.Count) Then
        CurrencyTab.TabPages.Remove(_selected_tab)
        CurrencyTab.TabPages.Insert(_selected_index + 1, _selected_tab) 'add on specific position
        m_tab_order_changed = True

      End If
    End If
    CurrencyTab.SelectedTab = _selected_tab
  End Sub

  Private Sub SaveCurrencyOrder()
    Dim _sb As StringBuilder

    Dim SqlTrans As SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      WSI.Common.Log.Error("SaveTabOrder: Could not connect to ddbb.")
      Return
    End If

    Try


      For Each _tab As TabPage In CurrencyTab.TabPages

        _sb = New StringBuilder()
        _sb.AppendLine(" UPDATE CURRENCY_EXCHANGE                           ")
        _sb.AppendLine("    SET CE_CURRENCY_ORDER = @pCurrencyOrder         ")
        _sb.AppendLine("  WHERE CE_CURRENCY_ISO_CODE = @pCurrencyIsoCode    ")
        _sb.AppendLine("    AND CE_TYPE = 0                                 ")

        Using _sql_cmd As New SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans)
          _sql_cmd.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar).Value = _tab.Text
          _sql_cmd.Parameters.Add("@pCurrencyOrder", SqlDbType.Int).Value = (CurrencyTab.TabPages.IndexOf(_tab) + 1)


          If 1 <> _sql_cmd.ExecuteNonQuery() Then
            Return
          End If
        End Using

      Next
      SqlTrans.Commit()
    Catch ex As Exception
      WSI.Common.Log.Exception(ex)
      WSI.Common.Log.Error("SaveTabOrder: Could not save currency order.")
      SqlTrans.Rollback()
    End Try

  End Sub


  Private Function GetCurrencyByTabPage(Control As TabControl, TabPageName As String, ControlName As String) As uc_currency_selector
    Dim _uc_currency_selector As uc_currency_selector

    Dim _tabpage As TabPage = Control.TabPages(TabPageName)
    _uc_currency_selector = CType(_tabpage.Controls(ControlName), uc_currency_selector)



    Return _uc_currency_selector
  End Function ' GetCurrencyByTabPage


  Private Function GetCurrencyByControl(Control As Control, ControlName As String) As uc_currency_selector
    Dim _uc_currency_selector As uc_currency_selector

    _uc_currency_selector = CType(Control.Controls(ControlName), uc_currency_selector)

    Return _uc_currency_selector
  End Function ' GetCurrencyByControl

  Dim _currency_configuration As CLS_CURRENCY_CONFIGURATION
  'Dim _selected_currency_value As String

#End Region ' Private Functions

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub ' New

#End Region ' Public Functions

#Region " Events "

  Private Sub chk_credit_card_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _uc_card_selector As uc_currency_selector
    Dim _tab_control As TabControl

    chk_card_specification.Enabled = chk_credit_card.Checked
    chk_bank_data_input.Enabled = chk_credit_card.Checked

    chk_card_specification.Enabled = chk_credit_card.Checked
    chk_bank_data_input.Enabled = chk_credit_card.Checked
    chk_bank_card_cash_advance_hide_voucher.Enabled = chk_credit_card.Checked

    ef_bank_card_voucher_title.Enabled = chk_credit_card.Checked
    ef_bank_card_cash_advance_voucher_title.Enabled = chk_credit_card.Checked
    chk_bank_card_voucher.Enabled = chk_credit_card.Checked
    uc_bank_card_commission.Enabled = chk_credit_card.Checked


    If Tab_Credit_Card_Type.TabPages.Count > 0 Then
      Tab_Credit_Card_Type.Enabled = chk_credit_card.Checked
    Else
      _tab_control = tab_national_currency
      For Each _tab_page As TabPage In _tab_control.TabPages
        If _tab_page.Name = TAB_CHECK_NAME Then
          Continue For
        End If
        _uc_card_selector = CType(_tab_page.Controls(GetControlNameByTabName(_tab_page.Name)), uc_currency_selector)
        _uc_card_selector.set_enabled(chk_credit_card.Checked)
      Next
    End If

  End Sub ' chk_credit_card_CheckedChanged

  Private Sub chk_check_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _uc_check_selector As uc_currency_selector

    chk_bank_check_data.Enabled = chk_check.Checked

    chk_check_voucher.Enabled = chk_check.Checked
    ef_check_voucher_title.Enabled = chk_check.Checked

    chk_check_cash_advance_hide_voucher.Enabled = chk_check.Checked
    ef_check_cash_advance_voucher_title.Enabled = chk_check.Checked

    uc_check_commission.Enabled = chk_check.Checked

    If chk_check.Checked Then
      ef_check_voucher_title.Enabled = Not chk_check_voucher.Checked
    Else
      ef_check_voucher_title.Enabled = chk_check.Checked
    End If

    chk_check_voucher.Enabled = chk_check.Checked

    _uc_check_selector = GetCurrencyByTabPage(tab_national_currency, TAB_CHECK_NAME, GetControlNameByTabName(TAB_CHECK_NAME))
    _uc_check_selector.set_enabled(chk_check.Checked)

  End Sub ' chk_check_CheckedChanged


  Private Sub chk_foreign_currency_voucher_tittle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    If Not Permissions.Write Then
      Return
    End If
    ef_foreign_currency_voucher_title.Enabled = Not chk_foreign_currency_voucher.Checked
  End Sub ' chk_foreign_currency_voucher_tittle_CheckedChanged

  Private Sub chk_bank_card_voucher_title_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    If Not Permissions.Write Then
      Return
    End If
    ef_bank_card_voucher_title.Enabled = Not chk_bank_card_voucher.Checked
  End Sub ' chk_bank_card_voucher_title_CheckedChanged

  Private Sub ch_cash_advance_hide_voucher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    If Not Permissions.Write Then
      Return
    End If
    ef_bank_card_cash_advance_voucher_title.Enabled = Not chk_bank_card_cash_advance_hide_voucher.Checked
  End Sub ' ch_cash_advance_hide_voucher_CheckedChanged

  Private Sub chk_check_cash_advance_hide_voucher_CheckedChange(ByVal sender As Object, ByVal e As System.EventArgs)
    If Not Permissions.Write Then
      Return
    End If
    ef_check_cash_advance_voucher_title.Enabled = Not chk_check_cash_advance_hide_voucher.Checked
  End Sub ' ch_cash_advance_hide_voucher_CheckedChanged

  Private Sub chk_check_voucher_title_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    If Not Permissions.Write Then
      Return
    End If
    ef_check_voucher_title.Enabled = Not chk_check_voucher.Checked
  End Sub ' chk_check_voucher_title_CheckedChanged


  Private Sub uc_button_left_Click(sender As Object, e As EventArgs) Handles uc_button_left.Click
    MoveTabTo(True)

  End Sub

  Private Sub uc_button_right_Click(sender As Object, e As EventArgs) Handles uc_button_right.Click
    MoveTabTo(False)
  End Sub

  Private Sub TabPageOnEnter(sender As Object, e As EventArgs)
    Dim _tab_page As TabPage = DirectCast(sender, TabPage)

    MoveBankCardControls(_tab_page)

  End Sub

  Private Sub MoveBankCardControls(Tab_Page As TabPage)
    'chk_credit_card.Parent = Tab_Page

    'chk_card_specification.Parent = Tab_Page
    'chk_card_specification.BringToFront()
    'chk_bank_data_input.Parent = Tab_Page
    'chk_bank_data_input.BringToFront()
    'tab_bank_card_vouchers.Parent = Tab_Page
    'tab_bank_card_vouchers.BringToFront()

  End Sub

  Private Function GetControlNameByTabName(ByVal TabName As String)
    Dim controlName As String = TabName.Substring(3)
    Return TabName.Substring(3)
  End Function

#End Region



End Class
