'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_games_admin_sel
' DESCRIPTION:   Games Admin
' AUTHOR:        Ariel Vasquez - Gustavo Al�
' CREATION DATE: 01-SEP-2016
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  --------   -----------------------------------------------
' 01-SEP-2016  AVZ GDA    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports System.Net
Imports System.Text
Imports System.IO
'Imports System.Net.Json
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class frm_games_admin_sel
  Inherits GUI_Controls.frm_base_sel


#Region "Members"
  Private m_games_title As String
  Private m_games_status As String
  Private m_games_category As String

#End Region

#Region " CONSTANT "


  Private Const GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 1


  Private Const GRID_WIDTH_DATE As Integer = 1500
  Private Const GRID_WIDTH_TITLE As Integer = 3700
  Private Const GRID_WIDTH_CODE As Integer = 2100
  Private Const GRID_WIDTH_VISIBLE As Integer = 1000
  Private Const GRID_WIDTH_CATEGORY As Integer = 3100


  Private Const GRID_COL_INDEX As Integer = 0
  Private Const GRID_COL_ID As Integer = 1
  Private Const GRID_COL_DATE As Integer = 2
  Private Const GRID_COL_TITLE As Integer = 3
  Private Const GRID_COL_CODE As Integer = 4
  Private Const GRID_COL_CATEGORY As Integer = 5
  Private Const GRID_COL_VISIBLE As Integer = 6


  'ge_game_external_id,ge_code,ge_title,ge_app_visible,ge_date_creation

  Private Const SQL_COL_ID As Integer = 0
  Private Const SQL_COL_CODE As Integer = 1
  Private Const SQL_COL_TITLE As Integer = 2
  Private Const SQL_COL_VISIBLE As Integer = 3
  Private Const SQL_COL_DATE As Integer = 4
  Private Const SQL_COL_CATEGORY As Integer = 5

#End Region

#Region " OVERRIDES "


  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COL_ID).Value = DbRow.Value(SQL_COL_ID).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_CODE).Value = DbRow.Value(SQL_COL_CODE).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_CATEGORY).Value = DbRow.Value(SQL_COL_CATEGORY).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_TITLE).Value = DbRow.Value(SQL_COL_TITLE).ToString()
    If IsDBNull(DbRow.Value(SQL_COL_DATE)) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DATE).Value = "-"
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    Me.Grid.Cell(RowIndex, GRID_COL_VISIBLE).Value = IIf(DbRow.Value(SQL_COL_VISIBLE), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))

    Return True

  End Function

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMES_ADMIN

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId


  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()


    ' Advertisement
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7532) ' Label form caption

    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.uc_checked_categories.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7987)
    Me.gb_category_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7549)
    Me.chk_status_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304)
    Me.chk_status_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305)
    Me.lbl_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7556)

    CategoriesComboFill()

    ' INITIALIZE LABEL CAPTIONS

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()


  End Sub ' GUI_InitControls



  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT " & _
              "          ge_game_external_id,ge_code,ge_title,ge_app_visible,ge_date_creation " & _
              "         ,mec.gec_title AS CATEGORY_TITLE " & _
              " FROM MAPP_GAMES_EXTERNAL " & _
              " LEFT JOIN mapp_games_external_categories mec on mapp_games_external.ge_game_external_category_id = mec.gec_game_external_categories_id "
    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY ge_game_external_id ASC"
    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_status As String = ""
    Dim str_type As String = ""
    Dim str_where_account As String = ""

    str_where = " WHERE 1=1 "

    ' Filter Category Title
    If Me.txt_name.Text <> "" Then
      str_where = str_where & " AND "
      str_where = str_where & GUI_FilterField("GE_CODE", txt_name.Text, False, False, True)
    End If

    If Me.dtp_from.Checked Then
      str_where = str_where & " AND ge_date_creation >= " & GUI_FormatDayDB(dtp_from.Value)
    End If

    If Me.dtp_to.Checked Then
      str_where = str_where & " AND ge_date_creation <= " & GUI_FormatDayDB(dtp_to.Value)
    End If

    ' STATUS Enabled
    If Me.chk_status_enabled.Checked Then
      str_where = str_where & " AND GE_APP_VISIBLE = 1 "
    End If

    ' STATUS Disabled
    If Me.chk_status_disabled.Checked Then
      str_where = str_where & " AND GE_APP_VISIBLE = 0 "
    End If

    Dim m_selected_categories As String

    If uc_checked_categories.SelectedIndexes.Length < uc_checked_categories.Count() Then
      m_selected_categories = uc_checked_categories.SelectedIndexesList()
      str_where += " AND ge_game_external_category_id IN (" + m_selected_categories + ")"
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Date selection 

    Return True
  End Function ' GUI_FilterCheck  


  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _ge_id As String
    Dim _frm_edit As frm_games_admin_edit 'GUI_Controls.frm_base_edit
    Dim _rows_to_move() As Integer

    _rows_to_move = Me.Grid.SelectedRows()

    ' ICS 18-DEC-2012: Check if there is a row selected
    If _rows_to_move Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _idx_row = _rows_to_move(0)
    _ge_id = Me.Grid.Cell(_idx_row, GRID_COL_ID).Value

    _frm_edit = New frm_games_admin_edit

    Call _frm_edit.ShowEditItem(_ge_id, "")


    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem


  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call ShowNewForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE



  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7523), m_games_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7553), m_games_title)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7555), m_games_category)


    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_games_title = ""
    m_games_status = ""
    m_games_category = ""


    ' Filter Game STATUS
    ' STATUS Enabled
    If Me.chk_status_enabled.Checked Then
      m_games_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7534)
    End If

    ' STATUS Disabled
    If Me.chk_status_disabled.Checked Then
      If m_games_status.Length > 0 Then
        m_games_status = m_games_status & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7521)
      Else
        m_games_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7521)
      End If
    End If

    'If cmb_category.SelectedIndex > 0 Then
    '  m_games_category = cmb_category.TextValue
    'End If

    'Game Title
    m_games_title = txt_name.Text

  End Sub 'GUI_ReportUpdateFilters

#End Region

#End Region

#Region " Private Functions "



  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False


      With .Column(GRID_COL_INDEX)
        .Header(0).Text = " "
        .Header(1).Text = " "
        .Width = 200
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' ID
      With .Column(GRID_COL_ID)
        .Width = 0
      End With

      .Column(GRID_COL_DATE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_COL_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COL_TITLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7546)
      .Column(GRID_COL_TITLE).Width = GRID_WIDTH_TITLE
      .Column(GRID_COL_TITLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COL_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7556)
      .Column(GRID_COL_CODE).Width = GRID_WIDTH_CODE
      .Column(GRID_COL_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COL_VISIBLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7549)
      .Column(GRID_COL_VISIBLE).Width = GRID_WIDTH_VISIBLE
      .Column(GRID_COL_VISIBLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COL_CATEGORY).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7548)
      .Column(GRID_COL_CATEGORY).Width = GRID_WIDTH_CATEGORY
      .Column(GRID_COL_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With
  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()
    txt_name.Text = ""
    chk_status_disabled.Checked = False
    chk_status_enabled.Checked = False
    dtp_from.Checked = False
    dtp_to.Checked = False
  End Sub


  ' PURPOSE: Get SQL WHERE 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Sub ShowNewForm()

    Dim frm_edit As frm_games_admin_edit

    frm_edit = New frm_games_admin_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewAdsForm

  Private Sub CategoriesComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT gec_game_external_categories_id, gec_title FROM mapp_games_external_categories WHERE(gec_active = 1)", Integer.MaxValue)

    'Me.cmb_category.Clear()
    'Me.cmb_category.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7903))
    'Me.cmb_category.Add(_table)

    Me.uc_checked_categories.Clear()
    Me.uc_checked_categories.Add(_table)

  End Sub ' CategoriesComboFill

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "


#End Region

  <Localizable>
  Private Sub pn_separator_line_Paint(sender As Object, e As PaintEventArgs) Handles pn_separator_line.Paint

  End Sub
  <AttributeUsage(AttributeTargets.All)> Public Class LocalizableAttribute
    Inherits System.Attribute
  End Class
End Class