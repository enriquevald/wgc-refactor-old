'-------------------------------------------------------------------
' Copyright © 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_player_tracking_income_report
' DESCRIPTION:   Player tracking income
' AUTHOR:        David Hernández
' CREATION DATE: 04-SEP-2014
'
' REVISION HISTORY:
'
' Date         Author   Description
' -----------  ------   -----------------------------------------------
' 04-SEP-2014  DHA      Initial version.
' 19-SEP-2014  DCS      Edit Procedure 
' 07-OCT-2014  DHA      Fixed Bug WIG-1424: set filters properly
' 08-OCT-2014  JML      Fixed Bug WIG-1439: PlayerTracking: Report columns with wrong format
' 12-FEB-2015  OPC      Fixed Bug WIG-1921: incorrect date format.
' 18-JUL-2016  RAB      Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
'----------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

#End Region


Public Class frm_player_tracking_income_report
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  ' Members for totals and subtotals
  Private m_type As String
  Private m_current_interval As String
  Private m_current_interval_aux As String
  Private m_hours_total As Decimal
  Private m_hours As Decimal

  Private m_buy_in As Decimal
  Private m_copy_dealer_validated_amount As Decimal
  Private m_chips_in As Decimal
  Private m_chips_out As Decimal
  Private m_played As Decimal
  Private m_average_bet As Decimal
  Private m_drop As Decimal
  Private m_hold As Decimal
  Private m_payout As Decimal
  Private m_netwin As Decimal

  Private m_buy_in_total As Decimal
  Private m_copy_dealer_validated_amount_total As Decimal
  Private m_chips_in_total As Decimal
  Private m_chips_out_total As Decimal
  Private m_played_total As Decimal
  Private m_average_bet_total As Decimal
  Private m_drop_total As Decimal
  Private m_hold_total As Decimal
  Private m_payout_total As Decimal
  Private m_netwin_total As Decimal

  ' Table type subtotals members
  Private m_buy_in_aux As Decimal
  Private m_copy_dealer_validated_amount_aux As Decimal
  Private m_chips_in_aux As Decimal
  Private m_chips_out_aux As Decimal
  Private m_played_aux As Decimal
  Private m_average_bet_aux As Decimal
  Private m_drop_aux As Decimal
  Private m_hold_aux As Decimal
  Private m_payout_aux As Decimal
  Private m_netwin_aux As Decimal
  Private m_hours_aux As Decimal

  ' Filter members
  Private m_date_from As String
  Private m_date_to As String
  Private m_report_base_type As GAMING_TABLES_REPORT_TYPE
  Private m_report_interval As GAMING_TABLES_REPORT_TIME_INTERVAL
  Private m_report_order As GAMING_TABLES_REPORT_ORDER
  Private m_report_only_activity As GAMING_TABLES_REPORT_ACTIVITY
  Private m_report_selected_types As String
  Private m_report_selected_types_names As String
  Private m_table_type_totals As Boolean

  ' Grid tooltip
  Private m_tool_tip As List(Of String)

  ' Selected currency
  Private m_selected_currency

#End Region

#Region " Constants "

  ' SQL Columns

  Private Const SQL_COLUMN_TABLE_ID As Integer = 0
  Private Const SQL_COLUMN_TABLE_NAME As Integer = 1
  Private Const SQL_COLUMN_TABLE_TYPE_ID As Integer = 2
  Private Const SQL_COLUMN_TABLE_TYPE_NAME As Integer = 3
  Private Const SQL_COLUMN_BUY_IN As Integer = 4
  Private Const SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_CHIPS_IN As Integer = 6
  Private Const SQL_COLUMN_CHIPS_OUT As Integer = 7
  Private Const SQL_COLUMN_TOTAL_PLAYED As Integer = 8
  Private Const SQL_COLUMN_AVERAGE_BET As Integer = 9
  Private Const SQL_COLUMN_DROP As Integer = 10
  Private Const SQL_COLUMN_HOLD As Integer = 11
  Private Const SQL_COLUMN_THEORIC_HOLD As Integer = 12
  Private Const SQL_COLUMN_PAYOUT As Integer = 13
  Private Const SQL_COLUMN_NETWIN As Integer = 14
  Private Const SQL_COLUMN_ISO_CODE As Integer = 15
  Private Const SQL_COLUMN_FIRST_OPEN_SESSION As Integer = 16
  Private Const SQL_COLUMN_LAST_CLOSE_SESSION As Integer = 17
  Private Const SQL_COLUMN_SESSION_IS_CLOSED As Integer = 18
  Private Const SQL_COLUMN_MINUTES_OPEN As Integer = 19
  Private Const SQL_COLUMN_INTERVAL_DATE As Integer = 20

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 19
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_TABLE_ID As Integer = 2
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 3
  Private Const GRID_COLUMN_ISO_CODE As Integer = 4
  Private Const GRID_COLUMN_BUY_IN As Integer = 5
  Private Const GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_CHIPS_IN As Integer = 7
  Private Const GRID_COLUMN_CHIPS_OUT As Integer = 8
  Private Const GRID_COLUMN_TOTAL_PLAYED As Integer = 9
  Private Const GRID_COLUMN_AVERAGE_BET As Integer = 10
  Private Const GRID_COLUMN_DROP As Integer = 11
  Private Const GRID_COLUMN_HOLD As Integer = 12
  Private Const GRID_COLUMN_THEORIC_HOLD As Integer = 13
  Private Const GRID_COLUMN_PAYOUT As Integer = 14
  Private Const GRID_COLUMN_NETWIN As Integer = 15
  Private Const GRID_COLUMN_SESSION_HOURS As Integer = 16
  Private Const GRID_COLUMN_SESSION_OPEN As Integer = 17
  Private Const GRID_COLUMN_SESSION_CLOSE As Integer = 18

  ' Excel
  Private Const EXCEL_COLUMN_DATE As Integer = 0

#End Region

#Region " Enums "

  ' Report Types
  Private Enum GAMING_TABLES_REPORT_TYPE
    GT_REPORT_TYPE_TOTAL = 0              ' By Table type
    GT_REPORT_TYPE_DETAIL = 1             ' By Table
  End Enum

  ' Report Time Interval
  Private Enum GAMING_TABLES_REPORT_TIME_INTERVAL
    GT_REPORT_TIME_INTERVAL_NONE = -1
    GT_REPORT_TIME_INTERVAL_BY_DAY = 0
    GT_REPORT_TIME_INTERVAL_BY_MONTH = 1
    GT_REPORT_TIME_INTERVAL_BY_YEAR = 2
  End Enum

  ' Report Order by
  Private Enum GAMING_TABLES_REPORT_ORDER
    GT_REPORT_ORDER_BY_DATE = 0
    GT_REPORT_ORDER_BY_TABLE = 1
  End Enum

  ' Report activity
  Private Enum GAMING_TABLES_REPORT_ACTIVITY
    GT_REPORT_ACTIVITY_ALL = 0
    GT_REPORT_ACTIVITY_ONLY = 1
  End Enum

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAYER_TRACKING_INCOME_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5493)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.uc_dsl.Init(GLB_NLS_GUI_ALARMS.Id(443), True)

    ' Meters
    Me.uc_checked_list_table_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409)

    ' Report Based On
    gb_report_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4417)
    rb_groupped_type_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4418)
    rb_grouped_type_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)

    ' Report Options
    gb_report_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421)
    chb_by_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4422)
    rb_interval_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4423)
    rb_interval_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4424)
    rb_interval_year.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4425)
    chb_order_by_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4426)
    chb_only_with_activity.Text = GLB_NLS_GUI_INVOICING.GetString(464)
    chb_table_type_totals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4578)

    ' Notes
    lbl_note1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4718)

    'Hold % =  (Chips Out / (Chips IN + Buy IN)) * 100

    lbl_hold.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3414) & " = (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288) & " / (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287) & " + " & WSI.Common.GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)) & ")) * 100"

    'Payout % = (Netwin / Total Jugado) * 100

    lbl_payout.Text = GLB_NLS_GUI_STATISTICS.GetString(303) & " = (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(539) & " / " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(537) & ") * 100"

    'Netwin = Chips IN + Buy IN – Chips OUT

    lbl_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539) & " = " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287) & " + " & WSI.Common.GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492)) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288)

    ' Fill operators
    Call Me.uc_checked_list_table_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)
    Call FillTableTypeFilterGrid()

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If m_report_interval <> GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE Then
      If m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE Then
        If m_current_interval <> "" AndAlso m_current_interval <> CType(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), Date).ToShortDateString() Then
          Call DrawRow(False)
          m_current_interval = CType(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), Date).ToShortDateString()
          Call ResetGlobal()
        Else
          m_current_interval = CType(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), Date).ToShortDateString()
        End If
      ElseIf m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE Then
        If m_current_interval <> "" AndAlso m_current_interval <> DbRow.Value(SQL_COLUMN_TABLE_NAME) Then
          Call DrawRow(False)
          m_current_interval = DbRow.Value(SQL_COLUMN_TABLE_NAME)

          Call ResetGlobal()
        Else
          m_current_interval = DbRow.Value(SQL_COLUMN_TABLE_NAME)

        End If
      End If
    End If

    ' Set table type subtotals
    If m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL AndAlso m_table_type_totals Then
      If m_current_interval_aux <> "" AndAlso m_current_interval_aux <> DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME) Then
        Call DrawRow(False, True)
        m_current_interval_aux = DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME)

        Call ResetGlobal(True)
      Else
        m_current_interval_aux = DbRow.Value(SQL_COLUMN_TABLE_TYPE_NAME)

      End If

      m_buy_in_aux += DbRow.Value(SQL_COLUMN_BUY_IN)
      m_copy_dealer_validated_amount_aux += DbRow.Value(SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT)
      m_chips_in_aux += DbRow.Value(SQL_COLUMN_CHIPS_IN)
      m_chips_out_aux += DbRow.Value(SQL_COLUMN_CHIPS_OUT)
      m_played_aux += DbRow.Value(SQL_COLUMN_TOTAL_PLAYED)
      m_average_bet_aux += DbRow.Value(SQL_COLUMN_AVERAGE_BET)
      m_drop_aux += DbRow.Value(SQL_COLUMN_DROP)
      m_netwin_aux += DbRow.Value(SQL_COLUMN_NETWIN)
      m_hours_aux += DbRow.Value(SQL_COLUMN_MINUTES_OPEN)

      m_hold_aux = 0
      m_payout_aux = 0

      If (m_chips_in_aux + m_buy_in_aux) <> 0 Then
        m_hold_aux = (m_chips_out_aux / (m_chips_in_aux + m_buy_in_aux)) * 100
      End If
      If (m_played_aux) <> 0 Then
        m_payout_aux = (m_netwin_aux / m_played_aux) * 100
      End If


    End If

    Call ProcessCounters(DbRow)

    Return True
  End Function

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - 
  Protected Overrides Sub GUI_AfterLastRow()

    If m_report_interval <> GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE AndAlso Me.Grid.NumRows > 0 Then

      Call DrawRow(False)

      If m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL AndAlso m_table_type_totals Then

        Call DrawRow(False, True)
      End If

    Else
      If m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL AndAlso m_table_type_totals Then

        Call DrawRow(False, True)
      End If

    End If

    ' Draw totals
    Call DrawRow(True)

  End Sub

  ' PURPOSE: Hide columns depending selected mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    ResetGlobal()
    ResetGlobal(True)

    m_buy_in_total = 0
    m_copy_dealer_validated_amount_total = 0
    m_chips_in_total = 0
    m_chips_out_total = 0
    m_played_total = 0
    m_average_bet_total = 0
    m_drop_total = 0
    m_hold_total = 0
    m_payout_total = 0
    m_hours_total = 0
    m_netwin_total = 0

    m_current_interval = ""
    m_current_interval_aux = ""
  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _time As TimeSpan
    Dim _current_hold As Decimal
    Dim _current_payout As Decimal

    'Calculate current hold and payout
    _current_hold = 0
    If Not (DbRow.Value(SQL_COLUMN_CHIPS_IN) + DbRow.Value(SQL_COLUMN_BUY_IN) = 0) Then
      _current_hold = ((DbRow.Value(SQL_COLUMN_CHIPS_OUT) / (DbRow.Value(SQL_COLUMN_CHIPS_IN) + DbRow.Value(SQL_COLUMN_BUY_IN))) * 100)
    End If

    _current_payout = 0
    If Not (DbRow.Value(SQL_COLUMN_TOTAL_PLAYED) = 0) Then
      _current_payout = ((DbRow.Value(SQL_COLUMN_NETWIN) / DbRow.Value(SQL_COLUMN_TOTAL_PLAYED)) * 100)
    End If

    With Me.Grid

      .Cell(RowIndex, GRID_COLUMN_INDEX).Value = ""

      ' Date
      Select Case m_report_interval
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
          .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
          .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
          .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INTERVAL_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_YEAR)

      End Select

      ' TableID 
      .Cell(RowIndex, GRID_COLUMN_TABLE_ID).Value = DbRow.Value(SQL_COLUMN_TABLE_ID)


      ' Table / Table type name
      .Cell(RowIndex, GRID_COLUMN_TABLE_NAME).Value = DbRow.Value(SQL_COLUMN_TABLE_NAME)


      .Cell(RowIndex, GRID_COLUMN_BUY_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BUY_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CHIPS_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CHIPS_OUT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_TOTAL_PLAYED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_PLAYED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AVERAGE_BET), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_DROP).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DROP), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(RowIndex, GRID_COLUMN_HOLD).Value = GUI_FormatNumber(_current_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
      .Cell(RowIndex, GRID_COLUMN_THEORIC_HOLD).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_THEORIC_HOLD), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
      .Cell(RowIndex, GRID_COLUMN_PAYOUT).Value = GUI_FormatNumber(_current_payout, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
      .Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      '
      If Not DbRow.Value(SQL_COLUMN_FIRST_OPEN_SESSION) Is DBNull.Value Then
        .Cell(RowIndex, GRID_COLUMN_SESSION_OPEN).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_FIRST_OPEN_SESSION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      '
      If Not DbRow.Value(SQL_COLUMN_LAST_CLOSE_SESSION) Is DBNull.Value Then
        .Cell(RowIndex, GRID_COLUMN_SESSION_CLOSE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_CLOSE_SESSION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Duration
      _time = TimeSpan.FromMinutes(DbRow.Value(SQL_COLUMN_MINUTES_OPEN))
      .Cell(RowIndex, GRID_COLUMN_SESSION_HOURS).Value = TimeSpanToString(_time) 'GUI_FormatNumber(TimeSpan.FromMinutes(DbRow.Value(SQL_COLUMN_MINUTES_OPEN)).TotalHours, 2)

      'Currency
      .Cell(RowIndex, GRID_COLUMN_ISO_CODE).Value = m_selected_currency
    End With

    Return True
  End Function

  ' PURPOSE : Set member values for filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty

    ' Dates filter checks
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Selected report type
    If rb_grouped_type_detail.Checked Then
      m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
    ElseIf rb_groupped_type_total.Checked Then
      m_report_base_type = GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
    End If

    ' Interval
    m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE
    If chb_by_date.Enabled AndAlso chb_by_date.Checked Then
      If rb_interval_day.Checked Then
        m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
      ElseIf rb_interval_month.Checked Then
        m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
      ElseIf rb_interval_year.Checked Then
        m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
      End If
    End If

    ' Order
    m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE
    If chb_order_by_name.Enabled AndAlso Not chb_order_by_name.Checked Then
      m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE
    End If

    ' Only Activity
    m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ALL
    If chb_only_with_activity.Checked Then
      m_report_only_activity = GAMING_TABLES_REPORT_ACTIVITY.GT_REPORT_ACTIVITY_ONLY
    End If

    ' Selected table types
    m_report_selected_types = Me.uc_checked_list_table_type.SelectedIndexesList()
    m_report_selected_types_names = Me.uc_checked_list_table_type.SelectedValuesList()

    m_table_type_totals = Me.chb_table_type_totals.Enabled AndAlso Me.chb_table_type_totals.Checked

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _report_type_value As String
    Dim _report_groupped_by As String

    _report_type_value = String.Empty
    _report_groupped_by = String.Empty

    Select Case m_report_base_type
      Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
        _report_type_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)
      Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
        _report_type_value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4418)
    End Select

    If m_report_interval <> GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE Then
      If m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE Then
        Select Case m_report_base_type
          Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
            _report_groupped_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4427)
          Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
            _report_groupped_by = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4426)
        End Select
      End If
      If _report_groupped_by <> String.Empty Then
        _report_groupped_by &= ", "
      End If
      Select Case m_report_interval
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
          _report_groupped_by &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(4423)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
          _report_groupped_by &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(4424)
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
          _report_groupped_by &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(4425)
      End Select
    End If

    ' Dates filter checks
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_date_to)

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Options
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4417), _report_type_value)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421), _report_groupped_by)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(464), IIf(m_report_only_activity, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4578), IIf(m_table_type_totals, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    ' Gaming Table Types
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409), m_report_selected_types_names)
    ' Note
    PrintData.SetFilter(String.Empty, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4718), False)
    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

    ' Currency type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_currency)
  End Sub

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow

    Call GUI_StyleSheet()

    m_selected_currency = uc_multicurrency.SelectedCurrency

    _sql_cmd = New SqlCommand("GT_Base_Report_Data_Player_Tracking")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pBaseType", SqlDbType.Int).Value = m_report_base_type
    _sql_cmd.Parameters.Add("@pTimeInterval", SqlDbType.Int).Value = m_report_interval
    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.FromDateSelected, System.DBNull.Value, m_date_from)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.ToDateSelected, System.DBNull.Value, m_date_to)
    _sql_cmd.Parameters.Add("@pOnlyActivity", SqlDbType.Int).Value = m_report_only_activity
    _sql_cmd.Parameters.Add("@pOrderBy", SqlDbType.Int).Value = m_report_order
    _sql_cmd.Parameters.Add("@pValidTypes", SqlDbType.VarChar).Value = m_report_selected_types
    _sql_cmd.Parameters.Add("@pSelectedCurrency", SqlDbType.NVarChar).Value = m_selected_currency

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    If _table.Rows.Count > 0 Then
      For Each _row In _table.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_gaming_tables_income_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If

  End Sub


  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

    If m_report_interval = GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH Then
      ExcelData.KeepDateFormat = True
    End If

  End Sub ' GUI_ReportParams

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTableTypeFilterGrid()
    Dim _data_table_tables_type As DataTable

    Call Me.uc_checked_list_table_type.Clear()
    Call Me.uc_checked_list_table_type.ReDraw(False)

    _data_table_tables_type = GamingTablesType_DBRead()
    Me.uc_checked_list_table_type.Add(_data_table_tables_type, "ID", "NAME")

    If Me.uc_checked_list_table_type.Count() > 0 Then
      Call Me.uc_checked_list_table_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_table_type.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Read the data from gaming tables types
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable: collected data
  Private Function GamingTablesType_DBRead() As DataTable
    Dim _str_sql As String
    Dim _data_table_tables_type As DataTable

    _str_sql = "SELECT   gtt_gaming_table_type_id AS ID" & _
              "        , GTT_NAME  AS NAME" & _
              "   FROM   GAMING_TABLES_TYPES " & _
              "  WHERE   GTT_ENABLED =  " & 1 & ""

    _data_table_tables_type = GUI_GetTableUsingSQL(_str_sql, 5000)

    Return _data_table_tables_type
  End Function

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    '' ToolTip for T-D%
    'If RowIndex = -3 And ColIndex = SQL_COLUMN_WIN_DROP Then
    '  ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3415)
    'End If

  End Sub ' GUI_SetToolTipText

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' TODO: DATE
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(297)
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      Select Case m_report_interval
        Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_NONE
          .Column(GRID_COLUMN_DATE).Width = 0
          .Column(GRID_COLUMN_DATE).IsColumnPrintable = False
        Case Else
          .Column(GRID_COLUMN_DATE).Width = 1250
          .Column(GRID_COLUMN_DATE).IsColumnPrintable = True
      End Select

      ' Table ID
      .Column(GRID_COLUMN_TABLE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TABLE_ID).Width = 0
      .Column(GRID_COLUMN_TABLE_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_TABLE_ID).IsColumnPrintable = False

      ' Table Name
      Select Case m_report_base_type
        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
          .Column(GRID_COLUMN_TABLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410)
        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
          .Column(GRID_COLUMN_TABLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4418)
      End Select

      .Column(GRID_COLUMN_TABLE_NAME).Width = 3000

      ' Currency iso code
      .Column(GRID_COLUMN_ISO_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5554)
      .Column(GRID_COLUMN_ISO_CODE).Width = 1000
      .Column(GRID_COLUMN_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' BUY_IN
      .Column(GRID_COLUMN_BUY_IN).Header(0).Text = ""
      .Column(GRID_COLUMN_BUY_IN).Width = 1
      .Column(GRID_COLUMN_BUY_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' COPY_DEALER_VALIDATED_AMOUNT
      .Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Width = 1
      .Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CHIPS_IN
      .Column(GRID_COLUMN_CHIPS_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287)
      .Column(GRID_COLUMN_CHIPS_IN).Width = 1700
      .Column(GRID_COLUMN_CHIPS_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' CHIPS_OUT
      .Column(GRID_COLUMN_CHIPS_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288)
      .Column(GRID_COLUMN_CHIPS_OUT).Width = 1700
      .Column(GRID_COLUMN_CHIPS_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TOTAL_PLAYED
      .Column(GRID_COLUMN_TOTAL_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_TOTAL_PLAYED).Width = 1700
      .Column(GRID_COLUMN_TOTAL_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' AVERAGE_BET
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = 1700
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DROP
      .Column(GRID_COLUMN_DROP).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3411)
      .Column(GRID_COLUMN_DROP).Width = 0
      .Column(GRID_COLUMN_DROP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' HOLD
      .Column(GRID_COLUMN_HOLD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3414)
      .Column(GRID_COLUMN_HOLD).Width = 1700
      .Column(GRID_COLUMN_HOLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' THEORIC HOLD
      .Column(GRID_COLUMN_THEORIC_HOLD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5629)
      '.Column(GRID_COLUMN_THEORIC_HOLD).Width = 1700
      .Column(GRID_COLUMN_THEORIC_HOLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' PAYOUT
      .Column(GRID_COLUMN_PAYOUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_PAYOUT).Width = 1700
      .Column(GRID_COLUMN_PAYOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NETWIN
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Width = 1700
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Session Open
      .Column(GRID_COLUMN_SESSION_OPEN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3462) ' "[Opened]"

      ' Session Close
      .Column(GRID_COLUMN_SESSION_CLOSE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3463) ' "[Closed]"

      ' Configure columns depend of report type
      Select Case m_report_base_type
        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_TOTAL
          .Column(GRID_COLUMN_THEORIC_HOLD).Width = 0
          .Column(GRID_COLUMN_THEORIC_HOLD).IsColumnPrintable = False

        Case GAMING_TABLES_REPORT_TYPE.GT_REPORT_TYPE_DETAIL
          .Column(GRID_COLUMN_THEORIC_HOLD).Width = 1700
          .Column(GRID_COLUMN_THEORIC_HOLD).IsColumnPrintable = True

      End Select

      ' Format of session open
      .Column(GRID_COLUMN_SESSION_OPEN).Width = 0
      .Column(GRID_COLUMN_SESSION_OPEN).IsColumnPrintable = False
      .Column(GRID_COLUMN_SESSION_CLOSE).Width = 0
      .Column(GRID_COLUMN_SESSION_CLOSE).IsColumnPrintable = False

      ' Session Hours
      .Column(GRID_COLUMN_SESSION_HOURS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(345) ' "[Duration]"
      .Column(GRID_COLUMN_SESSION_HOURS).Width = 1400

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _current_date As DateTime

    _current_date = WSI.Common.WGDB.Now

    Me.uc_dsl.ClosingTime = GetDefaultClosingTime()

    Me.uc_dsl.FromDate = New DateTime(_current_date.Year, _current_date.Month, _current_date.Day, 0, 0, 0)

    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddDays(1)

    Me.uc_dsl.ToDateSelected = False

    Me.rb_groupped_type_total.Checked = True

    Call Me.uc_checked_list_table_type.SetDefaultValue(True)

    chb_by_date.CheckState = CheckState.Unchecked
    rb_interval_month.Checked = True
    chb_only_with_activity.CheckState = CheckState.Unchecked
    chb_order_by_name.CheckState = CheckState.Unchecked
    chb_table_type_totals.Enabled = Me.rb_grouped_type_detail.Checked
    chb_table_type_totals.Checked = True

    Call GUI_ReportUpdateFilters()

    'MultiCurrency
    Me.uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.Init()

  End Sub ' SetDefaultValues

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub ProcessCounters(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    m_buy_in += DbRow.Value(SQL_COLUMN_BUY_IN)
    m_copy_dealer_validated_amount += DbRow.Value(SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT)
    m_chips_in += DbRow.Value(SQL_COLUMN_CHIPS_IN)
    m_chips_out += DbRow.Value(SQL_COLUMN_CHIPS_OUT)
    m_played += DbRow.Value(SQL_COLUMN_TOTAL_PLAYED)
    m_average_bet += DbRow.Value(SQL_COLUMN_AVERAGE_BET)
    m_drop += DbRow.Value(SQL_COLUMN_DROP)
    m_netwin += DbRow.Value(SQL_COLUMN_NETWIN)
    m_hours += DbRow.Value(SQL_COLUMN_MINUTES_OPEN)

    m_hold = 0
    m_payout = 0

    If (m_chips_in + m_buy_in) <> 0 Then
      m_hold = (m_chips_out / (m_chips_in + m_buy_in)) * 100
    End If
    If (m_played) <> 0 Then
      m_payout = (m_netwin / m_played) * 100
    End If

    m_buy_in_total += DbRow.Value(SQL_COLUMN_BUY_IN)
    m_copy_dealer_validated_amount_total += DbRow.Value(SQL_COLUMN_COPY_DEALER_VALIDATED_AMOUNT)
    m_chips_in_total += DbRow.Value(SQL_COLUMN_CHIPS_IN)
    m_chips_out_total += DbRow.Value(SQL_COLUMN_CHIPS_OUT)
    m_played_total += DbRow.Value(SQL_COLUMN_TOTAL_PLAYED)
    m_average_bet_total += DbRow.Value(SQL_COLUMN_AVERAGE_BET)
    m_drop_total += DbRow.Value(SQL_COLUMN_DROP)
    m_netwin_total += DbRow.Value(SQL_COLUMN_NETWIN)
    m_hours_total += DbRow.Value(SQL_COLUMN_MINUTES_OPEN)

    m_hold_total = 0
    m_payout_total = 0

    If (m_chips_in_total + m_buy_in_total) <> 0 Then
      m_hold_total = (m_chips_out_total / (m_chips_in_total + m_buy_in_total)) * 100
    End If
    If (m_played_total) <> 0 Then
      m_payout_total = (m_netwin_total / m_played_total) * 100
    End If


  End Sub

  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '          - LastTotal: Indicates the kind of total
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - True
  Private Function DrawRow(ByVal LastTotal As Boolean, Optional ByVal IsTableTypeSubTotal As Boolean = False) As Boolean
    Dim _idx_row As Integer
    Dim _row_back_color As ENUM_GUI_COLOR
    Dim _time As TimeSpan

    Dim _buy_in As Decimal
    Dim _copy_dealer_validated_amount As Decimal
    Dim _chips_in As Decimal
    Dim _chips_out As Decimal
    Dim _played As Decimal
    Dim _average_bet As Decimal
    Dim _drop As Decimal
    Dim _hold As Decimal
    Dim _payout As Decimal
    Dim _netwin As Decimal
    Dim _hours As Decimal

    ' Default row back color for subtotal 
    _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01

    If LastTotal Then
      m_type = GLB_NLS_GUI_INVOICING.GetString(205) ' TOTAL:

      _buy_in = m_buy_in_total
      _copy_dealer_validated_amount = m_copy_dealer_validated_amount_total
      _chips_in = m_chips_in_total
      _chips_out = m_chips_out_total
      _played = m_played_total
      _average_bet = m_average_bet_total
      _drop = m_drop_total
      _hold = m_hold_total
      _payout = m_payout_total
      _netwin = m_netwin_total
      _hours = m_hours_total

      ' Color for total
      _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
    Else

      _buy_in = m_buy_in
      _copy_dealer_validated_amount = m_copy_dealer_validated_amount
      _chips_in = m_chips_in
      _chips_out = m_chips_out
      _played = m_played
      _average_bet = m_average_bet
      _drop = m_drop
      _hold = m_hold
      _payout = m_payout
      _netwin = m_netwin
      _hours = m_hours

      Select Case m_report_order
        Case GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE
          Select Case m_report_interval
            Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_DAY
              m_type = GUI_FormatDate(Date.Parse(m_current_interval), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
            Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_MONTH
              m_type = GUI_FormatDate(Date.Parse(m_current_interval), ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR)
            Case GAMING_TABLES_REPORT_TIME_INTERVAL.GT_REPORT_TIME_INTERVAL_BY_YEAR
              m_type = GUI_FormatDate(Date.Parse(m_current_interval), ENUM_FORMAT_DATE.FORMAT_DATE_YEAR)
          End Select

        Case GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_TABLE
          m_type = m_current_interval
          _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
      End Select

      If IsTableTypeSubTotal Then
        m_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409) & Space(1) & m_current_interval_aux ' Table type: X
        _row_back_color = ENUM_GUI_COLOR.GUI_COLOR_BLUE_02

        _buy_in = m_buy_in_aux
        _copy_dealer_validated_amount = m_copy_dealer_validated_amount_aux
        _chips_in = m_chips_in_aux
        _chips_out = m_chips_out_aux
        _played = m_played_aux
        _average_bet = m_average_bet_aux
        _drop = m_drop_aux
        _hold = m_hold_aux
        _payout = m_payout_aux
        _hours = m_hours_aux
        _netwin = m_netwin_aux

      End If

    End If

    ' Add the totals row & data
    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty

      If m_report_order = GAMING_TABLES_REPORT_ORDER.GT_REPORT_ORDER_BY_DATE AndAlso Not LastTotal AndAlso Not IsTableTypeSubTotal Then
        .Cell(_idx_row, GRID_COLUMN_DATE).Value = m_type
      Else
        .Cell(_idx_row, GRID_COLUMN_TABLE_NAME).Value = m_type
      End If

      .Cell(_idx_row, GRID_COLUMN_BUY_IN).Value = GUI_FormatCurrency(_buy_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Value = GUI_FormatCurrency(_copy_dealer_validated_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_CHIPS_IN).Value = GUI_FormatCurrency(_chips_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_CHIPS_OUT).Value = GUI_FormatCurrency(_chips_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_TOTAL_PLAYED).Value = GUI_FormatCurrency(_played, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_DROP).Value = GUI_FormatCurrency(_drop, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)
      .Cell(_idx_row, GRID_COLUMN_HOLD).Value = GUI_FormatNumber(_hold, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
      .Cell(_idx_row, GRID_COLUMN_PAYOUT).Value = GUI_FormatNumber(_payout, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & Application.CurrentCulture.NumberFormat.PercentSymbol
      .Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      _time = TimeSpan.FromMinutes(_hours)
      .Cell(_idx_row, GRID_COLUMN_SESSION_HOURS).Value = TimeSpanToString(_time) ' GUI_FormatNumber(TimeSpan.FromMinutes(_hours).TotalHours, 2)

      .Row(_idx_row).BackColor = GetColor(_row_back_color)

      If m_buy_in_total <> 0 Then
        .Column(GRID_COLUMN_BUY_IN).Header(0).Text = WSI.Common.GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5492))
        .Column(GRID_COLUMN_BUY_IN).Width = 1700
      End If

      If m_copy_dealer_validated_amount_total <> 0 Then
        .Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8063)
        .Column(GRID_COLUMN_COPY_DEALER_VALIDATED_AMOUNT).Width = 2100
      End If

    End With

    Return True
  End Function ' DrawRow

  ' PURPOSE: Initialice the subtotal and total data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetGlobal(Optional ByVal IsTableTypeSubTotal As Boolean = False)

    If IsTableTypeSubTotal Then
      m_buy_in_aux = 0
      m_copy_dealer_validated_amount_aux = 0
      m_chips_in_aux = 0
      m_chips_out_aux = 0
      m_played_aux = 0
      m_average_bet_aux = 0
      m_drop_aux = 0
      m_hold_aux = 0
      m_payout_aux = 0
      m_netwin_aux = 0
      m_hours_aux = 0

    Else
      m_type = ""
      m_buy_in = 0
      m_copy_dealer_validated_amount = 0
      m_chips_in = 0
      m_chips_out = 0
      m_played = 0
      m_average_bet = 0
      m_drop = 0
      m_hold = 0
      m_payout = 0
      m_netwin = 0
      m_hours = 0

    End If

  End Sub

#End Region

#Region " Events "

  ' PURPOSE: Wen checked state changes on grouped by date interval
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender
  '           - e
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None
  Private Sub chb_by_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chb_by_date.CheckedChanged
    rb_interval_day.Enabled = chb_by_date.Checked
    rb_interval_month.Enabled = chb_by_date.Checked
    rb_interval_year.Enabled = chb_by_date.Checked
    chb_order_by_name.Enabled = chb_by_date.Checked
  End Sub

  Private Sub rb_groupped_type_total_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_groupped_type_total.CheckedChanged, rb_grouped_type_detail.CheckedChanged
    If rb_groupped_type_total.Checked Then
      chb_order_by_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4426)
    ElseIf rb_grouped_type_detail.Checked Then
      chb_order_by_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4427)
    End If
    chb_table_type_totals.Enabled = rb_grouped_type_detail.Checked
  End Sub

#End Region

End Class