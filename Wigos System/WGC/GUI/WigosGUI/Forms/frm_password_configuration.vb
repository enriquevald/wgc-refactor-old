'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_password_configuration
' DESCRIPTION:   Configure/Passwords Rules
' AUTHOR:        Humberto Braojos
' CREATION DATE: 02-Aug-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 02-AUG-2012 HBB    First Release
' 07-FEB-2013 JCM    Added Functionality Locked by inactivity
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On


Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users

Public Class frm_password_configuration
  Inherits frm_base_edit

  Private CHECK_PASSWORD_RULES_INITIAL_VALUE As Boolean


#Region " Overrides "

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:


  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    'setting text to the entry fields
    Me.ef_passw_number_character.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1167, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_PASSWORD_LENGHT_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST.ToString()))
    Me.ef_number_of_digits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1168, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_PASSWORD_DIGITS_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_PASSWORD_DIGITS_CONST.ToString()))
    Me.ef_number_of_lowercase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1169, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_PASSWORD_LOWERCASE_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_PASSWORD_LOWERCASE_CONST.ToString()))
    Me.ef_number_of_uppercase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1170, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_PASSWORD_UPPERCASE_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_PASSWORD_UPPERCASE_CONST.ToString()))
    Me.ef_number_special_character.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1171, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_PASSWORD_SPECIALCHARACTER_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_PASSWORD_SPECIALCHARACTER_CONST.ToString()))
    Me.ef_number_of_login_attempt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1172)
    Me.ef_days_without_login.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1173)
    Me.ef_number_of_password_history.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1174, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_PASSWORD_DIGITS_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_PASSWOD_HISTORY_CONST.ToString()))
    Me.chk_check_password_rules.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1183)
    Me.ef_login_number_character.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1167, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.PasswordPolicy.MIN_LOGIN_LENGTH_CONST.ToString(), WSI.Common.PasswordPolicy.MAX_LOGIN_LENGTH_CONST.ToString()))
    Me.gb_passwod_feaures.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1201)
    Me.gb_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202)
    Me.gb_block_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1207)
    Me.gb_session_timeout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1208)
    Me.ef_GUI_timeout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1209, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.Users.MIN_TIMEOUT_GUI.ToString(), WSI.Common.Users.MAX_TIMEOUT_GUI.ToString()))
    Me.ef_Cachier_timeout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1210, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1216, WSI.Common.Users.MIN_TIMEOUT_CAJERO.ToString(), WSI.Common.Users.MAX_TIMEOUT_CAJERO.ToString()))
    Me.ef_change_pass_in_x_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1211, "*")
    Me.tf_indicate_default_value.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1269, "*")
    Me.gb_password_rules.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1281)

    'setting the window name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1175)

    'setting properties to the entry fields: lenght
    Me.ef_passw_number_character.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_number_of_digits.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_number_of_lowercase.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_number_of_uppercase.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_number_special_character.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_number_of_login_attempt.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_days_without_login.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)  ' Allowed values 0-99
    Me.ef_number_of_password_history.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 1)
    Me.ef_login_number_character.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_GUI_timeout.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_Cachier_timeout.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_change_pass_in_x_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)


    'making entry fields editables
    Me.ef_passw_number_character.IsReadOnly = False
    Me.ef_number_of_digits.IsReadOnly = False
    Me.ef_number_of_lowercase.IsReadOnly = False
    Me.ef_number_of_uppercase.IsReadOnly = False
    Me.ef_number_special_character.IsReadOnly = False
    Me.ef_number_of_login_attempt.IsReadOnly = False
    Me.ef_days_without_login.IsReadOnly = False
    Me.ef_number_of_password_history.IsReadOnly = False
    Me.ef_login_number_character.IsReadOnly = False
    Me.ef_GUI_timeout.IsReadOnly = False
    Me.ef_Cachier_timeout.IsReadOnly = False
    Me.ef_change_pass_in_x_days.IsReadOnly = False

    Me.ef_number_of_login_attempt.Focus()
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
  End Sub

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_PASSWORD_CONFIGURATION
    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: It checks if the entry data are ok calling the function CheckEntryData
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _password_lenght As Int32
    Dim _login_length As Int32
    Dim _number_of_digits As Int32
    Dim _number_of_lowercase As Int32
    Dim _number_of_uppercase As Int32
    Dim _number_of_special_character As Int32
    Dim _days_to_change_pass As Int32
    Dim _timeout_gui As Int32
    Dim _timeout_cajero As Int32


    If chk_check_password_rules.Checked Then

      If Not CheckEntryData(ef_login_number_character.Value, WSI.Common.PasswordPolicy.MIN_LOGIN_LENGTH_CONST, WSI.Common.PasswordPolicy.MAX_LOGIN_LENGTH_CONST, _login_length) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1167), WSI.Common.PasswordPolicy.MIN_LOGIN_LENGTH_CONST, WSI.Common.PasswordPolicy.MAX_LOGIN_LENGTH_CONST)
        ef_login_number_character.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_number_of_password_history.Value, WSI.Common.PasswordPolicy.MIN_PASSWORD_HISTORY_CONST, WSI.Common.PasswordPolicy.MAX_PASSWOD_HISTORY_CONST) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1174), WSI.Common.PasswordPolicy.MIN_PASSWORD_HISTORY_CONST, WSI.Common.PasswordPolicy.MAX_PASSWOD_HISTORY_CONST)
        ef_number_of_password_history.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_change_pass_in_x_days.Value, WSI.Common.PasswordPolicy.MIN_CHANGE_PASS_N_DAYS, WSI.Common.PasswordPolicy.MAX_CHANGE_PASS_N_DAYS, _days_to_change_pass) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1211), WSI.Common.PasswordPolicy.MIN_CHANGE_PASS_N_DAYS)
        ef_change_pass_in_x_days.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_passw_number_character.Value, WSI.Common.PasswordPolicy.MIN_LOGIN_LENGTH_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST, _password_lenght) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1167), WSI.Common.PasswordPolicy.MIN_PASSWORD_LENGHT_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)
        ef_passw_number_character.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_number_of_digits.Value, WSI.Common.PasswordPolicy.MIN_PASSWORD_DIGITS_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_DIGITS_CONST, _number_of_digits) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1168), WSI.Common.PasswordPolicy.MIN_PASSWORD_DIGITS_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_DIGITS_CONST)
        ef_number_of_digits.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_number_of_lowercase.Value, WSI.Common.PasswordPolicy.MIN_PASSWORD_LOWERCASE_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_LOWERCASE_CONST, _number_of_lowercase) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1169), WSI.Common.PasswordPolicy.MIN_PASSWORD_LOWERCASE_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_LOWERCASE_CONST)
        ef_number_of_lowercase.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_number_of_uppercase.Value, WSI.Common.PasswordPolicy.MIN_PASSWORD_UPPERCASE_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_UPPERCASE_CONST, _number_of_uppercase) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1170), WSI.Common.PasswordPolicy.MIN_PASSWORD_UPPERCASE_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_UPPERCASE_CONST)
        ef_number_of_uppercase.Focus()

        Return False
      End If

      If Not CheckEntryData(ef_number_special_character.Value, WSI.Common.PasswordPolicy.MIN_PASSWORD_SPECIALCHARACTER_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_SPECIALCHARACTER_CONST, _number_of_special_character) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1171), WSI.Common.PasswordPolicy.MIN_PASSWORD_SPECIALCHARACTER_CONST, WSI.Common.PasswordPolicy.MAX_PASSWORD_SPECIALCHARACTER_CONST)
        ef_number_special_character.Focus()

        Return False
      End If

      If (_number_of_digits + _number_of_lowercase + _number_of_uppercase + _number_of_special_character) > _password_lenght Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1198), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
        ef_passw_number_character.Focus()

        Return False
      End If

    End If


    'The inactivity at GUI , Cashier and login attempt have to be checked anytime
    If Not CheckEntryData(ef_number_of_login_attempt.Value, WSI.Common.PasswordPolicy.MIN_LOGIN_ATTEMPTS_CONST, WSI.Common.PasswordPolicy.MAX_LOGIN_ATTEMPTS_CONST) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1172), WSI.Common.PasswordPolicy.MIN_LOGIN_ATTEMPTS_CONST)
      ef_number_of_login_attempt.Focus()

      Return False
    End If

    'HBB 31-AUG-2012 Check days without login
    If Not CheckEntryData(ef_days_without_login.Value, MIN_DAYS_WITHOUT_LOGIN_CONST, MAX_DAYS_WITHOUT_LOGIN_CONST) Then
      'If Math.Min(MAX_DAYS_WITHOUT_LOGIN_CONST, Math.Max(MIN_DAYS_WITHOUT_LOGIN_CONST, ef_days_without_login.Value)) <> ef_days_without_login.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1173), WSI.Common.Users.MIN_DAYS_WITHOUT_LOGIN_CONST)
      ef_days_without_login.Focus()

      Return False
    End If


    If Not CheckEntryData(ef_GUI_timeout.Value, WSI.Common.Users.MIN_TIMEOUT_GUI, WSI.Common.Users.MAX_TIMEOUT_GUI, _timeout_gui) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1209), WSI.Common.Users.MIN_TIMEOUT_GUI, WSI.Common.Users.MAX_TIMEOUT_GUI)
      ef_GUI_timeout.Focus()

      Return False
    End If

    If Not CheckEntryData(ef_Cachier_timeout.Value, WSI.Common.Users.MIN_TIMEOUT_CAJERO, WSI.Common.Users.MAX_TIMEOUT_CAJERO, _timeout_cajero) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1210), WSI.Common.Users.MIN_TIMEOUT_CAJERO, WSI.Common.Users.MAX_TIMEOUT_CAJERO)
      ef_Cachier_timeout.Focus()

      Return False
    End If

    Return True

  End Function

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _pass_config As PASSWORD_CONFIGURATION

    _pass_config = DbReadObject

    chk_check_password_rules.Checked = (_pass_config.CheckPasswordConfiguration = 1)
    If _pass_config.CheckPasswordConfiguration <> 1 Then
      _pass_config.CheckPasswordConfiguration = 0
    End If

    If _pass_config.PassNumberOfCharacter <> -1 Then
      ef_passw_number_character.TextValue = _pass_config.PassNumberOfCharacter
    Else
      ef_passw_number_character.TextValue = ""
      _pass_config.PassNumberOfCharacter = 0
    End If

    If _pass_config.LoginNumberOfCharacter <> -1 Then
      ef_login_number_character.TextValue = _pass_config.LoginNumberOfCharacter
    Else
      ef_login_number_character.TextValue = ""
      _pass_config.LoginNumberOfCharacter = 0
    End If

    If _pass_config.NumOfDigits <> -1 Then
      ef_number_of_digits.TextValue = _pass_config.NumOfDigits
    Else
      ef_number_of_digits.TextValue = ""
      _pass_config.NumOfDigits = 0
    End If

    If _pass_config.NumOfLowercase <> -1 Then
      ef_number_of_lowercase.TextValue = _pass_config.NumOfLowercase
    Else
      ef_number_of_lowercase.TextValue = ""
      _pass_config.NumOfLowercase = 0
    End If

    If _pass_config.NumOfUppercase <> -1 Then
      ef_number_of_uppercase.TextValue = _pass_config.NumOfUppercase
    Else
      ef_number_of_uppercase.TextValue = ""
      _pass_config.NumOfUppercase = 0
    End If

    If _pass_config.NumOfSpecialCharacter <> -1 Then
      ef_number_special_character.TextValue = _pass_config.NumOfSpecialCharacter
    Else
      ef_number_special_character.TextValue = ""
      _pass_config.NumOfSpecialCharacter = 0
    End If

    If _pass_config.NumOfLoginAttempt <> -1 Then
      ef_number_of_login_attempt.TextValue = _pass_config.NumOfLoginAttempt
    Else
      ef_number_of_login_attempt.TextValue = ""
      _pass_config.NumOfLoginAttempt = 0
    End If

    If _pass_config.NumOfPassHistory <> -1 Then
      ef_number_of_password_history.TextValue = _pass_config.NumOfPassHistory
    Else
      ef_number_of_password_history.TextValue = ""
      _pass_config.NumOfPassHistory = 0
    End If

    'max allowed days without login
    ef_days_without_login.TextValue = Math.Min(MAX_DAYS_WITHOUT_LOGIN_CONST, Math.Max(MIN_DAYS_WITHOUT_LOGIN_CONST, _pass_config.NumOfDaysWithoutLogin))

    If _pass_config.TimeOutCashier <> -1 Then
      ef_Cachier_timeout.TextValue = _pass_config.TimeOutCashier
    Else
      ef_Cachier_timeout.TextValue = ""
      _pass_config.TimeOutCashier = 0
    End If

    If _pass_config.TimeOutGUI <> -1 Then
      ef_GUI_timeout.TextValue = _pass_config.TimeOutGUI
    Else
      ef_GUI_timeout.TextValue = ""
      _pass_config.TimeOutGUI = 0
    End If

    If _pass_config.ChangePassInNDays <> -1 Then
      ef_change_pass_in_x_days.TextValue = _pass_config.ChangePassInNDays
    Else
      ef_change_pass_in_x_days.TextValue = ""
      _pass_config.ChangePassInNDays = 0
    End If

    CHECK_PASSWORD_RULES_INITIAL_VALUE = chk_check_password_rules.Checked

  End Sub

  ' PURPOSE: It gets the values from the entry field and set them to a PASSWORD_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _pass_config As PASSWORD_CONFIGURATION

    _pass_config = DbEditedObject
    Try
      _pass_config.CheckPasswordConfiguration = IIf(chk_check_password_rules.Checked, 1, 0)

      If Not Integer.TryParse(ef_passw_number_character.Value, _pass_config.PassNumberOfCharacter) Then
        _pass_config.PassNumberOfCharacter = 0
      End If

      If Not Integer.TryParse(ef_login_number_character.Value, _pass_config.LoginNumberOfCharacter) Then
        _pass_config.LoginNumberOfCharacter = 0
      End If

      If Not Integer.TryParse(ef_number_of_digits.TextValue, _pass_config.NumOfDigits) Then
        _pass_config.NumOfDigits = 0
      End If

      If Not Integer.TryParse(ef_number_of_lowercase.TextValue, _pass_config.NumOfLowercase) Then
        _pass_config.NumOfLowercase = 0
      End If

      If Not Integer.TryParse(ef_number_of_uppercase.TextValue, _pass_config.NumOfUppercase) Then
        _pass_config.NumOfUppercase = 0
      End If

      If Not Integer.TryParse(ef_number_special_character.TextValue, _pass_config.NumOfSpecialCharacter) Then
        _pass_config.NumOfSpecialCharacter = 0
      End If

      If Not Integer.TryParse(ef_number_of_login_attempt.TextValue, _pass_config.NumOfLoginAttempt) Then
        _pass_config.NumOfLoginAttempt = 0
      End If

      If Not Integer.TryParse(ef_number_of_password_history.TextValue, _pass_config.NumOfPassHistory) Then
        _pass_config.NumOfPassHistory = 0
      End If

      'Get de value from ef_days_without_login
      If Not Integer.TryParse(ef_days_without_login.TextValue, _pass_config.NumOfDaysWithoutLogin) Then
        _pass_config.NumOfDaysWithoutLogin = 0
      End If

      If Not Integer.TryParse(ef_Cachier_timeout.TextValue, _pass_config.TimeOutCashier) Then
        _pass_config.TimeOutCashier = 0
      End If

      If Not Integer.TryParse(ef_GUI_timeout.TextValue, _pass_config.TimeOutGUI) Then
        _pass_config.TimeOutGUI = 0
      End If

      If Not Integer.TryParse(ef_change_pass_in_x_days.TextValue, _pass_config.ChangePassInNDays) Then
        _pass_config.ChangePassInNDays = 0
      End If

    Catch
      Return
    End Try

  End Sub

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :


  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _pass_config As PASSWORD_CONFIGURATION

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New PASSWORD_CONFIGURATION()
        _pass_config = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Me.DbStatus = ENUM_STATUS.STATUS_OK

        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        If chk_check_password_rules.Checked <> CHECK_PASSWORD_RULES_INITIAL_VALUE Then
          If Not chk_check_password_rules.Checked Then
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1266), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

              DbStatus = ENUM_STATUS.STATUS_ERROR

              Return
            Else
              DbStatus = ENUM_STATUS.STATUS_OK

            End If
          Else
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1267), _
                                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                      ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

              DbStatus = ENUM_STATUS.STATUS_ERROR

              Return
            Else
              DbStatus = ENUM_STATUS.STATUS_OK

            End If
          End If
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If chk_check_password_rules.Checked = CHECK_PASSWORD_RULES_INITIAL_VALUE Or DbStatus = ENUM_STATUS.STATUS_OK Then 'UPDATE_REQUIRED
          Call MyBase.GUI_DB_Operation(DbOperation)

        End If


      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub

#End Region 'Overrides

#Region "Privates Methods"

  ' PURPOSE: This method checks if the Entry Value is ok
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Function CheckEntryData(ByVal EntryValue As String, ByVal MinPosibleValue As Int32, ByVal MaxPosibleValue As Int32, Optional ByRef EntryValueAfterParse As Int32 = 0) As Boolean

    If Integer.TryParse(EntryValue, EntryValueAfterParse) And EntryValueAfterParse >= MinPosibleValue And EntryValueAfterParse <= MaxPosibleValue Then
      Return True
    End If
    Return False
  End Function

#End Region

End Class