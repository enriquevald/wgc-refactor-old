﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_dealer_copy_configuration_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_dealer_copy = New System.Windows.Forms.GroupBox()
    Me.gb_dealer_copy_cashier = New System.Windows.Forms.GroupBox()
    Me.chk_dealer_copy_cashier_redeem_tickets = New System.Windows.Forms.CheckBox()
    Me.chk_dealer_copy_cashier_offline = New System.Windows.Forms.CheckBox()
    Me.chk_dealer_copy_cashier_validation = New System.Windows.Forms.CheckBox()
    Me.gb_dealer_copy_gui = New System.Windows.Forms.GroupBox()
    Me.chk_dealer_copy_gui_soft_count = New System.Windows.Forms.CheckBox()
    Me.chk_dealer_copy_gui_tickets_audit_report = New System.Windows.Forms.CheckBox()
    Me.chk_dealer_copy_gui_ticket_report = New System.Windows.Forms.CheckBox()
    Me.chk_dealer_copy_gui_cage_collection = New System.Windows.Forms.CheckBox()
    Me.gb_tickets_tito = New System.Windows.Forms.GroupBox()
    Me.gb_tickets_tito_gui = New System.Windows.Forms.GroupBox()
    Me.chk_tickets_tito_gui_reconcile = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_dealer_copy.SuspendLayout()
    Me.gb_dealer_copy_cashier.SuspendLayout()
    Me.gb_dealer_copy_gui.SuspendLayout()
    Me.gb_tickets_tito.SuspendLayout()
    Me.gb_tickets_tito_gui.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_tickets_tito)
    Me.panel_data.Controls.Add(Me.gb_dealer_copy)
    Me.panel_data.Size = New System.Drawing.Size(398, 272)
    '
    'gb_dealer_copy
    '
    Me.gb_dealer_copy.Controls.Add(Me.gb_dealer_copy_cashier)
    Me.gb_dealer_copy.Controls.Add(Me.gb_dealer_copy_gui)
    Me.gb_dealer_copy.Location = New System.Drawing.Point(10, 9)
    Me.gb_dealer_copy.Name = "gb_dealer_copy"
    Me.gb_dealer_copy.Size = New System.Drawing.Size(185, 257)
    Me.gb_dealer_copy.TabIndex = 0
    Me.gb_dealer_copy.TabStop = False
    Me.gb_dealer_copy.Text = "xDealer copy"
    '
    'gb_dealer_copy_cashier
    '
    Me.gb_dealer_copy_cashier.Controls.Add(Me.chk_dealer_copy_cashier_redeem_tickets)
    Me.gb_dealer_copy_cashier.Controls.Add(Me.chk_dealer_copy_cashier_offline)
    Me.gb_dealer_copy_cashier.Controls.Add(Me.chk_dealer_copy_cashier_validation)
    Me.gb_dealer_copy_cashier.Location = New System.Drawing.Point(7, 153)
    Me.gb_dealer_copy_cashier.Name = "gb_dealer_copy_cashier"
    Me.gb_dealer_copy_cashier.Size = New System.Drawing.Size(172, 97)
    Me.gb_dealer_copy_cashier.TabIndex = 2
    Me.gb_dealer_copy_cashier.TabStop = False
    Me.gb_dealer_copy_cashier.Text = "xCashier"
    '
    'chk_dealer_copy_cashier_redeem_tickets
    '
    Me.chk_dealer_copy_cashier_redeem_tickets.AutoSize = True
    Me.chk_dealer_copy_cashier_redeem_tickets.Location = New System.Drawing.Point(6, 22)
    Me.chk_dealer_copy_cashier_redeem_tickets.Name = "chk_dealer_copy_cashier_redeem_tickets"
    Me.chk_dealer_copy_cashier_redeem_tickets.Size = New System.Drawing.Size(121, 17)
    Me.chk_dealer_copy_cashier_redeem_tickets.TabIndex = 8
    Me.chk_dealer_copy_cashier_redeem_tickets.Text = "xRedeem tickets"
    Me.chk_dealer_copy_cashier_redeem_tickets.UseVisualStyleBackColor = True
    '
    'chk_dealer_copy_cashier_offline
    '
    Me.chk_dealer_copy_cashier_offline.AutoSize = True
    Me.chk_dealer_copy_cashier_offline.Location = New System.Drawing.Point(6, 66)
    Me.chk_dealer_copy_cashier_offline.Name = "chk_dealer_copy_cashier_offline"
    Me.chk_dealer_copy_cashier_offline.Size = New System.Drawing.Size(70, 17)
    Me.chk_dealer_copy_cashier_offline.TabIndex = 10
    Me.chk_dealer_copy_cashier_offline.Text = "xOffline"
    Me.chk_dealer_copy_cashier_offline.UseVisualStyleBackColor = True
    '
    'chk_dealer_copy_cashier_validation
    '
    Me.chk_dealer_copy_cashier_validation.AutoSize = True
    Me.chk_dealer_copy_cashier_validation.Location = New System.Drawing.Point(6, 44)
    Me.chk_dealer_copy_cashier_validation.Name = "chk_dealer_copy_cashier_validation"
    Me.chk_dealer_copy_cashier_validation.Size = New System.Drawing.Size(89, 17)
    Me.chk_dealer_copy_cashier_validation.TabIndex = 9
    Me.chk_dealer_copy_cashier_validation.Text = "xValidation"
    Me.chk_dealer_copy_cashier_validation.UseVisualStyleBackColor = True
    '
    'gb_dealer_copy_gui
    '
    Me.gb_dealer_copy_gui.Controls.Add(Me.chk_dealer_copy_gui_soft_count)
    Me.gb_dealer_copy_gui.Controls.Add(Me.chk_dealer_copy_gui_tickets_audit_report)
    Me.gb_dealer_copy_gui.Controls.Add(Me.chk_dealer_copy_gui_ticket_report)
    Me.gb_dealer_copy_gui.Controls.Add(Me.chk_dealer_copy_gui_cage_collection)
    Me.gb_dealer_copy_gui.Location = New System.Drawing.Point(7, 22)
    Me.gb_dealer_copy_gui.Name = "gb_dealer_copy_gui"
    Me.gb_dealer_copy_gui.Size = New System.Drawing.Size(172, 125)
    Me.gb_dealer_copy_gui.TabIndex = 1
    Me.gb_dealer_copy_gui.TabStop = False
    Me.gb_dealer_copy_gui.Text = "xWigosGUI"
    '
    'chk_dealer_copy_gui_soft_count
    '
    Me.chk_dealer_copy_gui_soft_count.AutoSize = True
    Me.chk_dealer_copy_gui_soft_count.Location = New System.Drawing.Point(6, 89)
    Me.chk_dealer_copy_gui_soft_count.Name = "chk_dealer_copy_gui_soft_count"
    Me.chk_dealer_copy_gui_soft_count.Size = New System.Drawing.Size(90, 17)
    Me.chk_dealer_copy_gui_soft_count.TabIndex = 7
    Me.chk_dealer_copy_gui_soft_count.Text = "xSoftCount"
    Me.chk_dealer_copy_gui_soft_count.UseVisualStyleBackColor = True
    '
    'chk_dealer_copy_gui_tickets_audit_report
    '
    Me.chk_dealer_copy_gui_tickets_audit_report.AutoSize = True
    Me.chk_dealer_copy_gui_tickets_audit_report.Location = New System.Drawing.Point(6, 67)
    Me.chk_dealer_copy_gui_tickets_audit_report.Name = "chk_dealer_copy_gui_tickets_audit_report"
    Me.chk_dealer_copy_gui_tickets_audit_report.Size = New System.Drawing.Size(140, 17)
    Me.chk_dealer_copy_gui_tickets_audit_report.TabIndex = 6
    Me.chk_dealer_copy_gui_tickets_audit_report.Text = "xTicketsAuditReport"
    Me.chk_dealer_copy_gui_tickets_audit_report.UseVisualStyleBackColor = True
    '
    'chk_dealer_copy_gui_ticket_report
    '
    Me.chk_dealer_copy_gui_ticket_report.AutoSize = True
    Me.chk_dealer_copy_gui_ticket_report.Location = New System.Drawing.Point(6, 45)
    Me.chk_dealer_copy_gui_ticket_report.Name = "chk_dealer_copy_gui_ticket_report"
    Me.chk_dealer_copy_gui_ticket_report.Size = New System.Drawing.Size(105, 17)
    Me.chk_dealer_copy_gui_ticket_report.TabIndex = 5
    Me.chk_dealer_copy_gui_ticket_report.Text = "xTicketReport"
    Me.chk_dealer_copy_gui_ticket_report.UseVisualStyleBackColor = True
    '
    'chk_dealer_copy_gui_cage_collection
    '
    Me.chk_dealer_copy_gui_cage_collection.AutoSize = True
    Me.chk_dealer_copy_gui_cage_collection.Location = New System.Drawing.Point(6, 23)
    Me.chk_dealer_copy_gui_cage_collection.Name = "chk_dealer_copy_gui_cage_collection"
    Me.chk_dealer_copy_gui_cage_collection.Size = New System.Drawing.Size(119, 17)
    Me.chk_dealer_copy_gui_cage_collection.TabIndex = 4
    Me.chk_dealer_copy_gui_cage_collection.Text = "xCageCollection"
    Me.chk_dealer_copy_gui_cage_collection.UseVisualStyleBackColor = True
    '
    'gb_tickets_tito
    '
    Me.gb_tickets_tito.Controls.Add(Me.gb_tickets_tito_gui)
    Me.gb_tickets_tito.Location = New System.Drawing.Point(205, 9)
    Me.gb_tickets_tito.Name = "gb_tickets_tito"
    Me.gb_tickets_tito.Size = New System.Drawing.Size(187, 76)
    Me.gb_tickets_tito.TabIndex = 3
    Me.gb_tickets_tito.TabStop = False
    Me.gb_tickets_tito.Text = "xTickets TITO"
    '
    'gb_tickets_tito_gui
    '
    Me.gb_tickets_tito_gui.Controls.Add(Me.chk_tickets_tito_gui_reconcile)
    Me.gb_tickets_tito_gui.Location = New System.Drawing.Point(8, 22)
    Me.gb_tickets_tito_gui.Name = "gb_tickets_tito_gui"
    Me.gb_tickets_tito_gui.Size = New System.Drawing.Size(172, 47)
    Me.gb_tickets_tito_gui.TabIndex = 1
    Me.gb_tickets_tito_gui.TabStop = False
    Me.gb_tickets_tito_gui.Text = "xWigosGUI"
    '
    'chk_tickets_tito_gui_reconcile
    '
    Me.chk_tickets_tito_gui_reconcile.AutoSize = True
    Me.chk_tickets_tito_gui_reconcile.Location = New System.Drawing.Point(6, 20)
    Me.chk_tickets_tito_gui_reconcile.Name = "chk_tickets_tito_gui_reconcile"
    Me.chk_tickets_tito_gui_reconcile.Size = New System.Drawing.Size(87, 17)
    Me.chk_tickets_tito_gui_reconcile.TabIndex = 11
    Me.chk_tickets_tito_gui_reconcile.Text = "xReconcile"
    Me.chk_tickets_tito_gui_reconcile.UseVisualStyleBackColor = True
    '
    'frm_dealer_copy_configuration_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(500, 281)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_dealer_copy_configuration_edit"
    Me.Text = "xDealer copy / tickets TITO configuration"
    Me.panel_data.ResumeLayout(False)
    Me.gb_dealer_copy.ResumeLayout(False)
    Me.gb_dealer_copy_cashier.ResumeLayout(False)
    Me.gb_dealer_copy_cashier.PerformLayout()
    Me.gb_dealer_copy_gui.ResumeLayout(False)
    Me.gb_dealer_copy_gui.PerformLayout()
    Me.gb_tickets_tito.ResumeLayout(False)
    Me.gb_tickets_tito_gui.ResumeLayout(False)
    Me.gb_tickets_tito_gui.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

  Friend WithEvents gb_dealer_copy As System.Windows.Forms.GroupBox
  Friend WithEvents gb_tickets_tito As System.Windows.Forms.GroupBox
  Friend WithEvents gb_tickets_tito_gui As System.Windows.Forms.GroupBox
  Friend WithEvents gb_dealer_copy_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents gb_dealer_copy_gui As System.Windows.Forms.GroupBox
  Friend WithEvents chk_tickets_tito_gui_reconcile As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_cashier_redeem_tickets As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_cashier_offline As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_cashier_validation As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_gui_soft_count As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_gui_tickets_audit_report As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_gui_ticket_report As System.Windows.Forms.CheckBox
  Friend WithEvents chk_dealer_copy_gui_cage_collection As System.Windows.Forms.CheckBox

End Class
