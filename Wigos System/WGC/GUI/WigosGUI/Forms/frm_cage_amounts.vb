'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_CAGE_amounts.vb
'
' DESCRIPTION : Cage Amounts.TEXT
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-NOV-2013  XMS    Initial version.   
' 29-NOV-2013  XMS    Fixed bug WIG-436: Some error in denomination form.
' 29-NOV-2013  XMS    Fixed bug WIG-450: there is some message when click Cancel without changes...
' 07-DEC-2013  CCG    Fixed Bug WIG-459: Denomination equal than 0 error. 
' 07-DEC-2013  JCA    Solved unusual behavior. 
' 30-JAN-2014  XMS    Solved unusual behavior.
' 18-FEB-2014  JAB    Don't show green cell border in not editable cells.
' 09-APR-2014  CCG    Reorder datagrid currency to show first the national currency.
' 06-JUN-2014  JAB    Added two decimal characters in denominations fields.
' 18-JUL-2014  JBC & JPJ Fixed Bug WIG-1098: When deleting a denomination. 
' 18-JUL-2014  JBC & JPJ Fixed Bug WIG-1099: When creating denominations. 
' 25-JUL-2014  DLL    Chips are moved to a separate table
' 02-SEP-2014  AMF    Fixed Bug WIG-1224: When deleting a denominations with decimals
' 03-SEP-2014  JBC    Fixed Bug WIG-1402: Error when you click a color button in a new chip
' 14-OCT-2014  SMN    Fixed Bug WIG-1489: When "AutodistributeMode" parameter is disabled , chips are disabled too.
' 22-OCT-2014  LRS    Added currency types, bill or coins.
' 13-NOV-2014  LRS    Make update funcionality
' 25-NOV-2014  LRS    Fixed Bug WIG-1742
' 29-OCT-2015  ECP    Backlog Item 4950 Tables: MultiDenominations - Chip Sets (Eliminate funcionality completely)
' 11-NOV-2015  DLL    Product Backlog Item 6379 - Error when create new denomination
' 22-MAR-2016  ETP    Fixed Bug 10212: Can't create new denominations if regional configuration is diferent from GUI.
' 08-APR-2016  RGR    Product Backlog Item 10936: Tables (Phase 1): Settings chips on different screens
' 12-AUG-2016  ETP    Fixed Bug 16719 Boveda denominations no showed.
' 19-SEP-2016  FAV    Fixed Bug 17808 Exception loading general param
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl
Imports System.Globalization

Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

#End Region           ' Imports

Public Class frm_cage_amounts
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CUR_CURRENCY_ALLOWED As Integer = 1
  Private Const GRID_COLUMN_CUR_CURRENCY_ISO_CODE As Integer = 2
  Private Const GRID_COLUMN_CUR_CURRENCY_NAME As Integer = 3

  Private Const GRID_COLUMNS_CUR As Integer = 4
  Private Const GRID_HEADER_ROWS_CUR As Integer = 1

  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_CUR_CURRENCY_ISO_CODE As Integer = 900
  Private Const GRID_WIDTH_CUR_CURRENCY_ALLOWED As Integer = 1100
  Private Const GRID_WIDTH_CUR_CURRENCY_NAME As Integer = 2625

  Private Const GRID_COLUMN_BILL_CURRENCY_ISO_CODE As Integer = 1
  Private Const GRID_COLUMN_BILL_CURRENCY_DENOMINATION As Integer = 2
  Private Const GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE As Integer = 3
  Private Const GRID_COLUMN_BILL_CURRENCY_TYPE_COIN As Integer = 4
  Private Const GRID_COLUMN_BILL_CURRENCY_TYPE_BILL As Integer = 5
  Private Const GRID_COLUMN_BILL_CURRENCY_REJECTED As Integer = 6
  Private Const GRID_COLUMN_BILL_CURRENCY_EDITABLE As Integer = 7       '0-No editable, 1-Editable, 8-Deleted
  Private Const GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE As Integer = 8


  Private Const GRID_COLUMNS_BILL As Integer = 9
  Private Const GRID_HEADER_ROWS_BILL As Integer = 1

  Private Const GRID_WIDTH_BILL_CURRENCY_ISO_CODE As Integer = 1000
  Private Const GRID_WIDTH_BILL_CURRENCY_TYPE As Integer = 900
  Private Const GRID_WIDTH_BILL_CURRENCY_DENOMINATION As Integer = 1880
  Private Const GRID_WIDTH_BILL_CURRENCY_ENABLED As Integer = 1800

  Private Const MIN_CURRENCIES_ALLOWED As Integer = 1
  Private Const MARK_DELETED As Integer = 8

  Private Const GRID_MAX_ROWS As Int16 = 30

  Private Const FORM_DB_MIN_VERSION As Short = 226 ' TODO: Real MIN_VERSION = 227  


#End Region       ' Constants

#Region "enum"

  Private Enum ROWACTION
    Editable = 2
    Added = 5
    Updated = 6
  End Enum
#End Region

#Region " Members "

  ' For return currencies selected 
  Private CurrenciesSelected() As Integer
  Private m_currency_selected As String
  Private m_currency_allowed As Boolean
  Private m_dt_restricted As DataTable
  Private m_dt_denominations As DataTable
  Private m_error_show As Boolean
  Private m_init_controls As Boolean
  Private m_selected_grid As Boolean
  Private m_selected_grid_bills As Boolean
  Private m_source_delete_getscreendata As Boolean

  Private m_current_row_index As Int32

#End Region         ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_AMOUNTS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub               ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3000)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)

    Me.gb_currencies.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
    Me.gb_bills.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3371)

    Me.dg_currencies.SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_bills.SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE

    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = Me.Permissions.Write

    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    Call InitMembers()

    Call GUI_StyleSheet()

  End Sub         ' GUI_InitControls

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL)
  End Sub      ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_GetScreenData()
    Dim _cage_amounts As cls_cage_amounts

    Try
      _cage_amounts = Me.DbEditedObject

      GetScreenCurrencies(_cage_amounts)

      GetScreenDenominations(_cage_amounts)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)

    End Try

  End Sub        ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _cage_amounts As cls_cage_amounts

    Try
      RemoveHandler dg_currencies.BeforeRowChanged, AddressOf dg_currencies_BeforeRowChanged
      _cage_amounts = Me.DbEditedObject

      SetScreenCurrencies(_cage_amounts)
      SetScreenDenominations(_cage_amounts)

      m_dt_denominations = _cage_amounts.DataCageAmountsDenomination

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

    AddHandler dg_bills.CellDataChangedEvent, AddressOf cellDataChange
    AddHandler dg_currencies.BeforeRowChanged, AddressOf dg_currencies_BeforeRowChanged
  End Sub        ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New cls_cage_amounts()

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub         ' GUI_DB_Operation

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _count As Int32
    Dim _currency_iso_code As String
    Dim _currency_iso_code_checked As Boolean

    _currency_iso_code_checked = False
    _count = 0

    Try

      _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "nothing")

      For _idx_row As Integer = 0 To dg_currencies.NumRows - 1
        If GridValueToBool(dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value) Then
          _count += 1
          If dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value = _currency_iso_code Then
            _currency_iso_code_checked = True
          End If
        End If
      Next

      If _count < MIN_CURRENCIES_ALLOWED Then
        Call dg_currencies.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2930), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If Not _currency_iso_code_checked Then
        Call dg_currencies.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3015), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      'We found that the value of the name is not 0 or ""
      For _idx_row As Int32 = 0 To dg_bills.NumRows - 1
        If String.IsNullOrEmpty(dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value.Trim) Then

          Call dg_bills.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1067), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return False
        Else

          If dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value <= 0 Then

            Call dg_bills.Focus()
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1067), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

            Return False
          End If
        End If
      Next

      'We check if we want to insert new records that already exist in row dg_bills.
      For _idx_row As Int32 = 0 To dg_bills.NumRows - 1
        If dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value.ToString = ROWACTION.Added Then

          For _idx_row2 As Int32 = 0 To dg_bills.NumRows - 1
            Dim a As Integer = GUI_LocalNumberToDBNumber(dg_bills.Cell(_idx_row2, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value.ToString)
            If GUI_LocalNumberToDBNumber(dg_bills.Cell(_idx_row2, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value.ToString) = GUI_LocalNumberToDBNumber(dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value) _
                And dg_bills.Cell(_idx_row2, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value = dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value _
                And _idx_row2 <> _idx_row Then
              'deleted this condition part because now all lines can be updated, And dg_bills.Cell(_idx_row2, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value.ToString = "0" _

              Call dg_bills.Focus()
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2943), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
              Return False
            End If

          Next
        End If
      Next

      'We found that the records you want to delete in the dg_bills are not moving dg_currencies DIVISAS
      For _idx_row As Int32 = 0 To dg_currencies.NumRows - 1

        If Not GridValueToBool(dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value) _
          AndAlso IsPendingMovements(dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value) Then

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2945), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return False
        End If

      Next


      ' We found that the records were marked for delete in BILLS' 
      ' They were not moving, when we Clicked Delete, 
      ' and now still no movement when you click OK
      For _idx_row As Integer = 0 To dg_bills.NumRows - 1

        If GridValueToBool(dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value.ToString = "1") _
        AndAlso Not m_error_show _
        AndAlso IsPendingMovements(dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value) Then

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2947), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
          m_error_show = True
          Return False

        End If

      Next

      If Not CheckTypeSelected() Then
        Return False
      End If

    Catch ex As Exception

    End Try

    Return True

  End Function  ' GUI_IsScreenDataOk

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If dg_currencies.ContainsFocus Then
          dg_currencies.KeyPressed(sender, e)
        End If
        If dg_bills.ContainsFocus Then
          dg_bills.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function        ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    MyBase.GUI_ButtonClick(ButtonId)

  End Sub          ' GUI_ButtonClick

#End Region         ' Overrides

#Region "Private Functions"

  Private Sub InitMembers()
    m_currency_selected = ""
    m_currency_allowed = False
    m_error_show = False
    m_init_controls = False
    m_selected_grid = False
    m_selected_grid_bills = False
    m_source_delete_getscreendata = False
    m_current_row_index = -1
  End Sub

  ' PURPOSE: Define all Grid Points Multiplier Per currency Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.dg_currencies
      Call .Init(GRID_COLUMNS_CUR, GRID_HEADER_ROWS_CUR, True)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Currency enable
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1058)
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Width = GRID_WIDTH_CUR_CURRENCY_ALLOWED
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Editable = False
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Currency Id
      .Column(GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
      .Column(GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Width = GRID_WIDTH_CUR_CURRENCY_ISO_CODE
      .Column(GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Description
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1069)
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Width = GRID_WIDTH_CUR_CURRENCY_NAME
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

    With Me.dg_bills
      Call .Init(GRID_COLUMNS_BILL, GRID_HEADER_ROWS_BILL, True)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Currency Id
      .Column(GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
      .Column(GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Width = GRID_WIDTH_BILL_CURRENCY_ISO_CODE
      .Column(GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Currency type coin
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5661) '"Moneda"
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Width = GRID_WIDTH_BILL_CURRENCY_TYPE
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).EditionControl.Type = CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Currency type bill
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5660) ' "Billete"
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Width = GRID_WIDTH_BILL_CURRENCY_TYPE
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).EditionControl.Type = CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Currency value
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Header.Text = ""
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Currency value
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1060)
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Width = GRID_WIDTH_BILL_CURRENCY_DENOMINATION
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).IsMerged = False
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 10, 2) ' (FORMAT_NUMBER, 5, 0)   

      ' Currency enable
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2347)
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).IsMerged = False
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Editable value row
      .Column(GRID_COLUMN_BILL_CURRENCY_EDITABLE).Header.Text = ""
      .Column(GRID_COLUMN_BILL_CURRENCY_EDITABLE).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_EDITABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Currency value Previous
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Header.Text = ""
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub           ' GUI_StyleSheet

  ' PURPOSE: Update the currencies DataTable with the allowed value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencies As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenCurrencies(ByVal CageAmounts As cls_cage_amounts)
    Dim _currencies() As DataRow

    If Not Me.dg_currencies Is Nothing AndAlso Not Me.dg_currencies.IsDisposed = True AndAlso Me.dg_currencies.NumRows > 0 Then
      For _idx_row As Integer = 0 To Me.dg_currencies.NumRows - 1
        _currencies = CageAmounts.DataCageAmounts.Select("CUR_ISO_CODE = '" & Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value & "' ")

        If _currencies.Length = 1 Then
          If _currencies(0)(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_ALLOWED) <> GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value.ToString()) Then
            _currencies(0)(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_ALLOWED) = GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value)
          End If
          If _currencies(0)(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_NAME) <> Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_NAME).Value Then
            _currencies(0)(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_NAME) = Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_NAME).Value
          End If
        End If

      Next
    End If

  End Sub      ' GetScreenCurrencies

  ' PURPOSE: Update the currencies denominations DataTable with the rejected value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencies denominations As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenDenominations(ByVal CageAmounts As cls_cage_amounts)
    Dim _cur_denomi() As DataRow
    Dim _prev_denomination_grid As Decimal
    Dim _denomination_grid As Decimal
    Dim _iso_code As String

    For _idx_row As Integer = 0 To Me.dg_bills.NumRows - 1
      '_cur_denomi = CurrDenomination.Select("    CAA_ISO_CODE = '" & Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value & "' " & _
      '                              "AND CAA_DENOMINATION = " & GUI_LocalNumberToDBNumber(CDec(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value)))
      _prev_denomination_grid = CDec(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Value)
      _denomination_grid = CDec(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value)

      _iso_code = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value

      _cur_denomi = CageAmounts.DataCageAmountsDenomination.Select("    ISO_CODE = '" & _iso_code & "' " & _
                                    "AND DENOMINATION = " & GUI_LocalNumberToDBNumber(_prev_denomination_grid.ToString())) ' & " AND CGC_CAGE_CURRENCY_TYPE=" & CheckRowCurrency(_idx_row))

      If _cur_denomi.Length = 0 Then
        _cur_denomi = CageAmounts.DataCageAmountsDenomination.Select("     ISO_CODE = '" & _iso_code & "' " & _
                              "AND DENOMINATION = 0")
      End If

      If _cur_denomi.Length >= 1 Then


        'REJECTED
        _cur_denomi(_cur_denomi.Length - 1)(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_REJECTED) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value)

        'DENOMINATION
        _cur_denomi(_cur_denomi.Length - 1)(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) = _denomination_grid
        Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Value = _denomination_grid

        'TYPE

        _cur_denomi(_cur_denomi.Length - 1)(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_TYPE_COIN) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Value)
        _cur_denomi(_cur_denomi.Length - 1)(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_TYPE_BILL) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Value)

        ' ACTION

        _cur_denomi(_cur_denomi.Length - 1)(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_EDITABLE) = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value

        _cur_denomi(_cur_denomi.Length - 1).AcceptChanges()
        ' Set row state for update or insert
        Select Case Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value
          Case ROWACTION.Added
            _cur_denomi(_cur_denomi.Length - 1).SetAdded()
          Case ROWACTION.Updated
            _cur_denomi(_cur_denomi.Length - 1).SetModified()
        End Select

        Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = _denomination_grid

      End If

    Next



  End Sub     ' GetScreenCurrDenomin

  ' PURPOSE: Set the uc_grid s from the screen data with the values from the currencys DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenCurrencies(ByVal CageAmounts As cls_cage_amounts)
    Dim _idx_row As Integer
    Dim _idx_row_selected As Integer = -1
    Dim _iso_code As String
    Dim _currency_row As DataRow()
    Dim _currency_iso_code As String
    Dim _general_params As GeneralParam.Dictionary


    RemoveHandler dg_bills.RowSelectedEvent, AddressOf uc_grid_table_points_RowSelectedEvent
    RemoveHandler dg_currencies.CellDataChangedEvent, AddressOf dg_currencies_CellDataChangedEvent
    RemoveHandler dg_currencies.RowSelectedEvent, AddressOf DgCurrenciesRowSelectedEvent

    Me.dg_currencies.Redraw = False
    Me.dg_currencies.Clear()
    Me.dg_currencies.Redraw = True
    Me.dg_currencies.Redraw = False

    _general_params = GeneralParam.Dictionary.Create()

    Try
      _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN")

      _currency_row = CageAmounts.DataCageAmounts.Select("CUR_ISO_CODE = '" & _currency_iso_code & "'")
      Me.dg_currencies.AddRow()

      Call Me.SetCurrencyRow(_currency_row(0), 0)

      For Each _currency As DataRow In CageAmounts.DataCageAmounts.Rows

        If Not _currency("CUR_ISO_CODE").Equals(_currency_iso_code) Then
          If Not (_currency("CUR_ISO_CODE").Equals(Cage.CHIPS_ISO_CODE) And _general_params.Item("GamingTables.Mode") = "0") Then ' 14-OCT-2014 SMN: Fixed Bug
            Me.dg_currencies.AddRow()
            _idx_row = dg_currencies.NumRows - 1

            Call Me.SetCurrencyRow(_currency, _idx_row)

            If _currency(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_ALLOWED) = uc_grid.GRID_CHK_CHECKED _
                And _idx_row_selected = -1 Then
              _idx_row_selected = _idx_row
            End If
          End If
        End If

      Next

    Finally

      If dg_currencies.NumRows > 0 Then

        _iso_code = WSI.Common.Misc.ReadGeneralParams("RegionalOptions", "CurrencyISOCode")

        If String.IsNullOrEmpty(_iso_code) Then
          _iso_code = WSI.Common.CurrencyExchange.GetNationalCurrency()
        End If

        For _idx_row = 0 To dg_currencies.NumRows - 1

          If dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value.Equals(_iso_code) Then

            dg_currencies.IsSelected(_idx_row) = True
            dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
            Call DgCurrenciesRowSelectedEvent(_idx_row)
            Exit For

          End If

        Next

      End If

      dg_currencies.Redraw = True


      AddHandler dg_bills.RowSelectedEvent, AddressOf uc_grid_table_points_RowSelectedEvent
      AddHandler dg_currencies.CellDataChangedEvent, AddressOf dg_currencies_CellDataChangedEvent
      AddHandler dg_currencies.RowSelectedEvent, AddressOf DgCurrenciesRowSelectedEvent

    End Try

  End Sub      ' SetScreenCurrencies

  ' PURPOSE: To fill a new row in the data grid currencies
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - IdxRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetCurrencyRow(ByVal Row As DataRow, ByVal IdxRow As Integer)

    dg_currencies.Cell(IdxRow, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value = Row(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_ISO_CODE)

    If Row(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_ALLOWED) Then
      dg_currencies.Cell(IdxRow, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    Else
      dg_currencies.Cell(IdxRow, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
    End If

    If IsDBNull(Row(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_NAME)) Then
      dg_currencies.Cell(IdxRow, GRID_COLUMN_CUR_CURRENCY_NAME).Value = ""
    Else
      dg_currencies.Cell(IdxRow, GRID_COLUMN_CUR_CURRENCY_NAME).Value = Row(cls_cage_amounts.SQL_COLUMN_CUR_CURRENCY_NAME)
    End If

  End Sub ' SetCurrencyRow


  ' PURPOSE: Set the uc_grid s from the screen data with the values from the currencys denomination DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys denomination As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetScreenDenominations(ByVal CageAmounts As cls_cage_amounts)
    Dim _idx_row As Integer
    Dim _currency_exchange As CurrencyExchangeProperties
    Dim _txt As String
    Dim _flag_color As Color
    Dim _iso_code As String


    RemoveHandler dg_bills.RowSelectedEvent, AddressOf uc_grid_table_points_RowSelectedEvent
    RemoveHandler dg_currencies.CellDataChangedEvent, AddressOf dg_currencies_CellDataChangedEvent
    RemoveHandler dg_currencies.RowSelectedEvent, AddressOf DgCurrenciesRowSelectedEvent

    dg_bills.Redraw = False
    dg_bills.Clear()
    dg_bills.Redraw = True
    dg_bills.Redraw = False

    Try
      For Each _currency As DataRow In CageAmounts.DataCageAmountsDenomination.Rows
        If _currency.RowState <> DataRowState.Deleted AndAlso _currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) < 0 Then
          Continue For
        End If
        '18-JUL-2014 JBC & JPJ WIGOS-1098
        If _currency.RowState <> DataRowState.Deleted Then
          _iso_code = _currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_ISO_CODE)

          If _iso_code = m_currency_selected Then

            dg_bills.AddRow()

            _idx_row = dg_bills.NumRows - 1

            _flag_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
            ProcessColor(_flag_color)

            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value = _iso_code
            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = _currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_DENOMINATION)
            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Value = _currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_DENOMINATION)
            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value = GUI_FormatNumber(_currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_DENOMINATION), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

            'LMRS 20/10/2014 SHOW COIN TYPE
            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Value = BoolToGridValue(_currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_TYPE_COIN))
            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Value = BoolToGridValue(_currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_TYPE_BILL))

            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value = BoolToGridValue(_currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_REJECTED))
            dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = ROWACTION.Editable '_currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_EDITABLE) '0

            _currency_exchange = CurrencyExchangeProperties.GetProperties(_iso_code)

            If Not _currency_exchange Is Nothing Then
              _txt = _currency_exchange.FormatCurrency(_currency(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_DENOMINATION))
              dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value = _txt
            End If
          End If
        End If
      Next

    Finally

      dg_bills.Redraw = True
      If Me.dg_bills.NumRows > 0 Then
        For x As Integer = 0 To dg_bills.NumRows - 1
          Me.dg_bills.IsSelected(x) = False
        Next x
        Me.dg_bills.IsSelected(0) = True
      End If


      AddHandler dg_bills.RowSelectedEvent, AddressOf uc_grid_table_points_RowSelectedEvent
      AddHandler dg_currencies.CellDataChangedEvent, AddressOf dg_currencies_CellDataChangedEvent
      AddHandler dg_currencies.RowSelectedEvent, AddressOf DgCurrenciesRowSelectedEvent

    End Try

  End Sub     ' SetScreenCurrDenomin

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String

    Return IIf(enabled, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)

  End Function     ' BoolToGridValue

  ' PURPOSE : Trick to set cell backround color to Black
  '
  '  PARAMS :
  '     - INPUT : Color
  '     - OUTPUT : Color
  '
  ' RETURNS : 
  '
  Private Sub ProcessColor(ByRef CageColor As Color)

    If (CageColor.A = Color.Black.A And _
        CageColor.R = Color.Black.R And _
        CageColor.G = Color.Black.G And _
        CageColor.B = Color.Black.B) Then

      CageColor = Color.FromArgb(CageColor.A, CageColor.R + 1, CageColor.G, CageColor.B)

    End If

  End Sub ' ProcessColor

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal Enabled As String) As Boolean

    Return (Enabled = uc_grid.GRID_CHK_CHECKED Or Enabled = uc_grid.GRID_CHK_CHECKED_DISABLED)

  End Function     ' GridValueToBool

  Private Sub InsertBillRow()
    Dim _row As Int32
    Dim _edit As cls_cage_amounts
    Dim _denomination_values(3) As String

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    If dg_bills.NumRows < GRID_MAX_ROWS Then

      _row = dg_bills.AddRow()

      dg_bills.ClearSelection()
      dg_bills.IsSelected(_row) = True
      dg_bills.Redraw = True
      dg_bills.TopRow = _row

      With dg_bills
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value = m_currency_selected
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = 0
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_PREVIOUS_VALUE).Value = 0
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value = GUI_FormatNumber(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Value = BoolToGridValue(False)
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Value = BoolToGridValue(True)
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value = dg_currencies.Cell(dg_currencies.SelectedRows(0), GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value 'BoolToGridValue(True)
        .Cell(_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = ROWACTION.Added

        .CurrentCol = GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE
        .CurrentRow = _row
        .Focus()
      End With

      _denomination_values(0) = m_currency_selected
      _denomination_values(1) = dg_bills.Cell(_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value
      _denomination_values(2) = GridValueToBool(dg_bills.Cell(_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value)
      _denomination_values(3) = ROWACTION.Added ' ADDED

      _edit = Me.DbEditedObject
      _edit.DataCageAmountsDenomination.Rows.Add(_denomination_values)

    End If

  End Sub            ' InsertBillRow

  Private Sub DeleteSelectedRows(ByVal Grid As GUI_Controls.uc_grid)

    Dim currency_config_edit As cls_cage_amounts
    Dim _CAA_denomi() As DataRow
    Dim _sql As String
    Dim _denomination As String


    Erase CurrenciesSelected
    CurrenciesSelected = Grid.SelectedRows

    If IsNothing(CurrenciesSelected) Then
      Exit Sub
    End If

    currency_config_edit = Me.DbEditedObject
    If Grid.Cell(CurrenciesSelected(0), GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value <> ROWACTION.Added Then
      GetScreenDenominations(currency_config_edit)
    End If

    For Each _selected_row As Integer In Grid.SelectedRows


      _denomination = Grid.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value

      _sql = "    ISO_CODE = '" & Grid.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value & "' " & _
                            "AND DENOMINATION = " & GUI_LocalNumberToDBNumber(_denomination)

      _CAA_denomi = currency_config_edit.DataCageAmountsDenomination.Select(_sql)

      If _CAA_denomi.Length > 0 Then
        _CAA_denomi(0).Delete()
      End If

      Grid.IsSelected(_selected_row) = False
      Grid.DeleteRowFast(_selected_row)

      m_source_delete_getscreendata = True
      Call GUI_GetScreenData()

      Exit For

    Next

  End Sub ' DeleteSelectedRows

  ' PURPOSE: Check if String has Currency values. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Value
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean. True: has Currency ok. False: otherwise.
  Private Function GetHasCurrency(ByVal Valor As String) As Boolean
    Dim _idx As Int16
    Dim _return As Boolean = False
    Dim _car As String

    For _idx = 0 To Valor.Length - 1
      _car = Valor.Substring(_idx, 1)
      If Not IsNumeric(_car) Then

        If _car <> "," And _car <> "." Then

          _return = True
          Exit For

        End If
      End If
    Next

    Return _return

  End Function

  Private Function CheckDeleteBillRow(ByVal ISO_CODE As String, ByVal Denomination As String, ByVal CurrencyType As CageCurrencyType, ByVal Cell_Status As String) As Boolean
    Dim _return As Boolean

    _return = False

    If Not String.IsNullOrEmpty(Cell_Status) Then ' now can be updated, this condition don't is needed AndAlso Cell_Status = ROWACTION.Update Then
      If Cell_Status = ROWACTION.Added Then
        _return = True
      End If
    End If

    If Not _return AndAlso Not IsPendingMovements(ISO_CODE, Denomination, CurrencyType) Then
      _return = True
    End If

    If Not _return AndAlso (Denomination = 0 And Cell_Status = 0) Then
      _return = True
    End If

    Return _return

  End Function            ' DeleteBillRow

  Private Sub ReadData()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

  End Sub                 ' ReadData


  ' PURPOSE: Check pending movements 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Bolean
  '
  ' RETURNS:
  '     - Boolean. True: there is movements. False: not movements.
  Private Function IsPendingMovements(ByVal ISOCode As String, Optional ByVal Denomination As String = "", Optional ByVal CurrencyType As CageCurrencyType = -1) As Boolean

    Dim currency_data As cls_cage_amounts
    Dim _num_CAA_denomi As Integer

    Try

      currency_data = Me.DbReadObject

      Using _db_trx As New DB_TRX()

        m_dt_restricted = currency_data.GetRestrictedAmountMovements(_db_trx)

        If Not String.IsNullOrEmpty(Denomination) Then
          Denomination = GUI_LocalNumberToDBNumber(Denomination) ' WSI.Common.Format.LocalNumberToDBNumber(
          _num_CAA_denomi = m_dt_restricted.Select(" CMD_ISO_CODE = '" & ISOCode & "' AND CMD_DENOMINATION = " & Denomination & " AND CMD_TYPE= " & CurrencyType).Length
        Else
          _num_CAA_denomi = m_dt_restricted.Select(" CMD_ISO_CODE = '" & ISOCode & "' ").Length

        End If

        If _num_CAA_denomi >= 1 Then
          Return True
        Else
          Return False
        End If

      End Using

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "IsPendingMovements", _
                            ex.Message)

      ' Exception error
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    End Try

  End Function

  ' PURPOSE: Check exist values "0" in denominations
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Bolean
  '
  ' RETURNS:
  '     - Boolean. True: there is values 0. False: not values 0.

  Private Function ExistNullValues() As Boolean
    Dim _currency_data As cls_cage_amounts
    Dim _currencies As DataTable
    Dim _da As DataRow()
    Dim _return As Boolean

    _return = False

    Try

      _currency_data = Me.DbEditedObject
      _currencies = _currency_data.DataCageAmountsDenomination
      _da = _currencies.Select(" DENOMINATION =  0 ")

      If _da.Length > 0 Then

        If m_source_delete_getscreendata Then
          m_source_delete_getscreendata = False
          For _idx As Integer = 0 To _da.Length() - 1
            _da(_idx).Delete()
          Next

          _return = False
        Else
          _return = True
        End If

      End If



    Catch ex As Exception

      ' 137 "Exception error has been found: \n%1"
    Finally

    End Try

    Return _return

  End Function      'ExistNullValues 

  ' PURPOSE: Check currency type in a row
  '
  '  PARAMS:
  '     - INPUT:
  '           - Seleted row
  '     - OUTPUT:
  '           - Bolean
  '
  ' RETURNS:
  '     - Boolean. True: there is values 0. False: not values 0.
  Private Function CheckRowCurrency(ByVal SelectedRow As Integer, ByVal Currency As CageCurrencyType) As Boolean

    Dim _col As Integer
    _col = GRID_COLUMN_BILL_CURRENCY_TYPE_BILL
    If Currency = CageCurrencyType.Coin Then
      _col = GRID_COLUMN_BILL_CURRENCY_TYPE_COIN
    End If

    If GridValueToBool(Me.dg_bills.Cell(SelectedRow, _col).Value) Then
      Return True
    Else
      Return False
    End If
  End Function


  Private Function EvaluateDelete(Optional ByVal Type As CageCurrencyType = -1) As Boolean
    Dim _val_edit As String
    Dim _row_to_delete As Boolean
    Dim _can_be_deleted As Boolean
    _can_be_deleted = True
    _row_to_delete = False

    Try
      If Not Permissions.Delete Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Return False
        Exit Function
      End If

      If Not Me.dg_bills.SelectedRows Is Nothing AndAlso Me.dg_bills.SelectedRows.Length > 0 Then

        For Each _selected_row As Integer In Me.dg_bills.SelectedRows

          _val_edit = Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value.ToString()
          If Type <> -1 Then
            _row_to_delete = CheckDeleteBillRow(Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value(), _
                                                Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value(), _
                                                Type, _
                                                _val_edit)
          Else
            _row_to_delete = CheckDeleteBillRow(Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value(), _
                                                Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value(), _
                                                CageCurrencyType.Bill, _
                                                _val_edit)
            If Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value() <> WSI.Common.Cage.CHIPS_ISO_CODE Then
              _row_to_delete = _row_to_delete And CheckDeleteBillRow(Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value(), _
                                                  Me.dg_bills.Cell(_selected_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value(), _
                                                  CageCurrencyType.Coin, _
                                                  _val_edit)
            End If
          End If
          If Not _row_to_delete Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2947), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
            _can_be_deleted = False
            Exit For

          End If

          Exit For
        Next

        If _row_to_delete And Type = -1 Then
          DeleteSelectedRows(Me.dg_bills)

          If Me.dg_bills.NumRows > 0 Then
            For x As Integer = 0 To dg_bills.NumRows - 1
              Me.dg_bills.IsSelected(x) = False
            Next x
            Me.dg_bills.IsSelected(0) = True
          End If
        End If

      End If

    Catch ex As Exception
    End Try
    Return _can_be_deleted
  End Function

#End Region ' Private Functions

#Region "Public Functions"

#End Region  ' Public Functions

#Region "Events"

  Private Sub dg_currencies_BeforeRowChanged(ByVal OriginRow As Integer, ByVal DestinyRow As Integer, ByRef CancelRowChange As Boolean) Handles dg_currencies.BeforeRowChanged
    If dg_currencies Is Nothing OrElse dg_currencies.NumRows <= 0 OrElse m_init_controls OrElse m_selected_grid Then
      Exit Sub
    End If
    CancelRowChange = Not GUI_IsScreenDataOk()
  End Sub



  Private Sub DgCurrenciesRowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_currencies.RowSelectedEvent
    Dim currency_data As cls_cage_amounts

    If dg_currencies Is Nothing OrElse dg_currencies.NumRows <= 0 OrElse m_init_controls OrElse m_selected_grid Then
      Exit Sub
    End If

    If SelectedRow >= 0 And dg_currencies.NumRows >= 0 Then
      m_selected_grid = True

      m_currency_selected = dg_currencies.Cell(SelectedRow, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value

      For x As Integer = 0 To dg_currencies.NumRows - 1
        If x <> SelectedRow Then
          Me.dg_currencies.IsSelected(x) = False
        Else
          Me.dg_currencies.IsSelected(x) = True
        End If
      Next x

      With dg_currencies

        .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Editable = False

        dg_bills.Column(GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Width = GRID_WIDTH_BILL_CURRENCY_TYPE
        dg_bills.Column(GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Width = GRID_WIDTH_BILL_CURRENCY_TYPE

      End With

      m_selected_grid = False
    End If

    Try

      If m_current_row_index <> SelectedRow Then
        Call GUI_GetScreenData() 'with this, save the new rows when change the row select in dg_currencies
        currency_data = Me.DbEditedObject
        Call SetScreenDenominations(currency_data)
      End If
      m_current_row_index = SelectedRow

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "dg_currencies_RowSelectedEvent", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub      ' dg_currencies_RowSelectedEvent

  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
    InsertBillRow()

  End Sub                  ' btn_add_ClickEvent

  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
    EvaluateDelete()
  End Sub

  ' PURPOSE: Handler to control BeforeStartEdition in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Column
  '           - EditionCanceled
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_bills_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_bills.BeforeStartEditionEvent
    Dim _val_edit As Int16

    EditionCanceled = False

    _val_edit = dg_bills.Cell(Row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value

    If _val_edit <> ROWACTION.Updated And _val_edit <> ROWACTION.Added And _val_edit <> ROWACTION.Editable Or (Column <> GRID_COLUMN_BILL_CURRENCY_TYPE_COIN And Column <> GRID_COLUMN_BILL_CURRENCY_TYPE_BILL And _val_edit <> ROWACTION.Added) Then
      EditionCanceled = True
    Else
      If (Column = GRID_COLUMN_BILL_CURRENCY_TYPE_COIN Or Column = GRID_COLUMN_BILL_CURRENCY_TYPE_BILL) And dg_bills.Cell(Row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = ROWACTION.Editable Then
        dg_bills.Cell(Row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = ROWACTION.Updated
      End If
    End If

  End Sub

  ' PURPOSE: Controls all denominations rows have a selected type
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   
  Private Function CheckTypeSelected() As Boolean
    ' lmrs 08/10/2014 if the two checkbox are unchecked, sel checked the check not clicked 
    Dim _result As Boolean
    Dim _cage_amounts As cls_cage_amounts
    Dim _act_iso_code As String

    _result = True
    _act_iso_code = ""
    ' check currency types selected for current currency grid 
    For _index As Integer = 0 To dg_bills.NumRows - 1
      _act_iso_code = dg_bills.Cell(_index, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value
      If dg_bills.Cell(_index, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value <> WSI.Common.Cage.CHIPS_ISO_CODE And dg_bills.Cell(_index, GRID_COLUMN_BILL_CURRENCY_TYPE_BILL).Value = uc_grid.GRID_CHK_UNCHECKED And dg_bills.Cell(_index, GRID_COLUMN_BILL_CURRENCY_TYPE_COIN).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _result = False
        Exit For
      End If
    Next

    ' check currency types selected in other currencies
    _cage_amounts = Me.DbEditedObject
    If _result Then
      For Each _data_row As DataRow In _cage_amounts.DataCageAmountsDenomination.Rows
        If _data_row(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_ISO_CODE) <> _act_iso_code Then
          If _data_row(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_ISO_CODE) <> WSI.Common.Cage.CHIPS_ISO_CODE And Not CBool(_data_row(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_TYPE_BILL)) And Not CBool(_data_row(cls_cage_amounts.SQL_COLUMN_BILL_CURRENCY_TYPE_COIN)) Then
            _result = False
            Exit For
          End If
        End If
      Next
    End If

    If Not _result Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5709), ENUM_MB_TYPE.MB_TYPE_INFO, , , )
    End If

    Return _result
  End Function    ' CheckTypeSelected

  Private Sub cellDataChange(ByVal Row As Integer, ByVal Column As Integer)
    Dim _currency_exchange As WSI.Common.CurrencyExchangeProperties
    Dim _denomination As Decimal
    Dim _is_number As Boolean

    RemoveHandler dg_bills.CellDataChangedEvent, AddressOf cellDataChange

    Try

      With dg_bills

        _currency_exchange = CurrencyExchangeProperties.GetProperties(.Cell(Row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value)
        _is_number = Decimal.TryParse(dg_bills.Cell(Row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value.ToString(), NumberStyles.Currency, Nothing, _denomination)

        If Not _currency_exchange Is Nothing And _is_number Then

          dg_bills.Cell(Row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = _denomination

          .Cell(Row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value = _currency_exchange.FormatCurrency(_denomination)
        End If

      End With

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

    AddHandler dg_bills.CellDataChangedEvent, AddressOf cellDataChange

  End Sub

  Private Sub uc_grid_table_points_RowSelectedEvent(ByVal SelectedRow As Integer) Handles dg_bills.RowSelectedEvent

    If m_init_controls OrElse dg_bills Is Nothing OrElse dg_bills.NumRows <= 0 OrElse m_selected_grid_bills Then
      Exit Sub
    End If

    Try

      If SelectedRow >= 0 And dg_bills.NumRows >= 0 Then
        m_selected_grid = True

        dg_bills.Redraw = False
        dg_bills.Redraw = True

        m_selected_grid = False

      End If
    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    End Try
  End Sub

  Private Sub dg_currencies_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_currencies.CellDataChangedEvent
    Dim _val_edit As String

    If Row < 0 Or Column < 0 Then
      Exit Sub
    End If

    With dg_currencies
      _val_edit = .Cell(Row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value.ToString

      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Editable = False
      .Cell(Row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED

    End With

    Me.dg_currencies.Row(Row).IsSelected = True

    Call DgCurrenciesRowSelectedEvent(Row)

  End Sub

  Private Sub dg_currencies_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_currencies.BeforeStartEditionEvent

    If Column <> GRID_COLUMN_CUR_CURRENCY_ALLOWED Then

      EditionCanceled = True
      If Row = 0 Then
        Call DgCurrenciesRowSelectedEvent(Row)
      End If
    End If

  End Sub

#End Region            ' Events





End Class   ' FORM_CURRENCIES
