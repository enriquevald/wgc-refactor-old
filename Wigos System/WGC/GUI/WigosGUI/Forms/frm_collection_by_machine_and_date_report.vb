'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_collection_by_machine_and_date_report.vb
' DESCRIPTION:   Collection by machine and date report
' AUTHOR:        Jos� Mart�nez L�pez
' CREATION DATE: 09-SEP-2014
'
' REVISION HISTORY: 
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-SEP-2014  JML    Initial version
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 20-MAY-2015  FAV    Two parameters added (@pShowMetters, @pOnlyGroupByTerminal) to 'PR_collection_by_machine_and_date'
'                     stored procedure for compatibility with frm_tito_win_control_report.
' 17-JUN-2015  FAV    WIG-2378: In the audit, one label is displayed as a screen filter
' 21-OCT-2015  RGR    4877:Codere Panam�: Report JCJ � Total Win change formula
' 30-NOV-2015  RGR    Bug 6255 improvement proposal - label Total Win
' 15-DIC-2016  ETP    Bug 21659 Totalized rows has incorrect Total Win formula.
' 25-AUG-2017  RGR    Bug 29456:WIGOS-3733 Cage - Minor errors in Income report by machine and date
' 19-SEP-2017  RGR    Bug 29555:WIGOS-3733 Cage - Minor errors in Income report by machine and date add filter '--' in denomination
' 28-SEP-2017  ETP    Fixed Bug 29953:[WIGOS-4910] Denominations filter values do not match with terminal denomination values 
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports System.Globalization

#End Region ' Imports

Public Class frm_collection_by_machine_and_date_report
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DB Columns
  Private Const SQL_COLUMN_TE_PROVIDER_ID As Integer = 0
  Private Const SQL_COLUMN_TE_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TE_NAME As Integer = 2
  Private Const SQL_COLUMN_DENOMINATION As Integer = 3
  Private Const SQL_COLUMN_ORDER_DATE As Integer = 4
  Private Const SQL_COLUMN_ORDER_DATE_END As Integer = 5
  Private Const SQL_COLUMN_COLLECTED_COIN_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_TICKET_IN_COUNT As Integer = 7
  Private Const SQL_COLUMN_TI_IN_AMOUNT_RE As Integer = 8
  Private Const SQL_COLUMN_TI_IN_AMOUNT_NO_RE As Integer = 9
  Private Const SQL_COLUMN_TI_IN_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_TICKET_OUT_COUNT As Integer = 11
  Private Const SQL_COLUMN_TI_OUT_AMOUNT_RE As Integer = 12
  Private Const SQL_COLUMN_TI_OUT_AMOUNT_NO_RE As Integer = 13
  Private Const SQL_COLUMN_TI_OUT_AMOUNT As Integer = 14
  Private Const SQL_COLUMN_NET_TITO As Integer = 15
  Private Const SQL_COLUMN_MANUAL As Integer = 16
  Private Const SQL_COLUMN_CREDIT_CANCEL As Integer = 17
  Private Const SQL_COLUMN_JACKPOT_DE_SALA As Integer = 18
  Private Const SQL_COLUMN_PROGRESIVES As Integer = 19
  Private Const SQL_COLUMN_NO_PROGRESIVES As Integer = 20
  Private Const SQL_COLUMN_HAND_PAYS_TOTAL As Integer = 21
  Private Const SQL_COLUMN_PROGRESIVE_PROVISIONS As Integer = 22
  Private Const SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS = 23
  Private Const SQL_COLUMN_GAP_BILL As Integer = 24 '0
  Private Const SQL_FIXED_COLUMNS As Integer = 31

  Private Const SQL_FIRST_COLUMN_VARIABLE As Integer = 31

  'Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_TE_NAME As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private TERMINAL_DATA_COLUMNS As Int32

  Private GRID_COLUMN_DENOMINATION As Integer
  Private GRID_COLUMN_ORDER_DATE As Integer
  Private GRID_COLUMN_ORDER_DATE_END As Integer
  'Private BILLS: VARIABLE COLUMNS
  Private GRID_INIT_BILLS_DATA As Integer
  Private BILLS_DATA_COLUMNS As Int32

  Private GRID_COLUMN_COLLECTED_COIN_AMOUNT As Integer
  Private GRID_COLUMN_TOTAL_COIN As Integer
  Private GRID_COLUMN_TICKET_IN_COUNT As Integer
  Private GRID_COLUMN_TI_IN_AMOUNT_RE As Integer
  Private GRID_COLUMN_TI_IN_AMOUNT_NO_RE As Integer
  Private GRID_COLUMN_TI_IN_AMOUNT As Integer
  Private GRID_COLUMN_TICKET_OUT_COUNT As Integer
  Private GRID_COLUMN_TI_OUT_AMOUNT_RE As Integer
  Private GRID_COLUMN_TI_OUT_AMOUNT_NO_RE As Integer
  Private GRID_COLUMN_TI_OUT_AMOUNT As Integer
  Private GRID_COLUMN_NET_TITO As Integer
  Private GRID_COLUMN_MANUAL As Integer
  Private GRID_COLUMN_CREDIT_CANCEL As Integer
  Private GRID_COLUMN_JACKPOT_DE_SALA As Integer
  Private GRID_COLUMN_PROGRESIVES As Integer
  Private GRID_COLUMN_PROGRESIVE_PROVISIONS As Integer
  Private GRID_COLUMN_HAND_PAYS_TOTAL As Integer
  Private GRID_COLUMN_NO_PROGRESIVES As Integer
  ' Private GRID_COLUMN_NETWIN_BILLS As Integer
  ' Private GRID_COLUMN_NETWIN_HAND_PAY As Integer
  Private GRID_COLUMN_NETWIN_TOTAL As Integer

  Private GRID_COLUMNS As Integer

  ' counters
  Private Const MAX_COUNTERS As Integer = 3
  Private Const COUNTER_DETAILS As Integer = 1
  Private Const COUNTER_SUBTOTALS As Integer = 2

#End Region

#Region " Member "

  'REPORT FILTER
  Dim m_date_from As String
  Dim m_date_to As String
  Dim m_is_check_dateTo As Boolean
  Dim m_terminal As String
  Dim m_denomination As String
  Dim m_list_type As String
  Dim m_include_jackpot As Boolean

  Dim m_subtotal_break As String

  Private m_query_as_datatable As DataTable

  Private m_nfi As System.Globalization.NumberFormatInfo

  Private m_counter_list(MAX_COUNTERS) As Integer

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False


#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COLLECTION_BY_MACHINE_AND_DATE

    'Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5412)

    ' - Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(5715), True, False)

    ' Show 
    Me.gb_show.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5413)
    Me.cb_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5414)
    Me.cb_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5415)
    Me.cb_bills_denominations.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5416)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5517)
    Me.chk_include_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4440, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5431))

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes())

    ' Denominations
    Me.uc_denominations.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5438)

    ' Remarks
    Me.lbl_remarks_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5686)
    Me.lbl_remarks_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5673)
    Me.lbl_remarks_3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6931)

    ' Culture
    m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat

    ' Style main Grid  (after DB read because it has dinamic columns)
    Call Me.GUI_StyleSheet()

    'Default values
    Call Me.SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call Me.SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function ' GUI_GetQueryType

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _sql_where As String
    Dim _row As DataRow
    Dim _final_time As DateTime

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    _final_time = New DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, Me.uc_dsl.ClosingTime, 0, 0)

    _sql_cmd = New SqlCommand("PR_collection_by_machine_and_date")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pFromDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.FromDateSelected, WGDB.Now(), Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime))
    _sql_cmd.Parameters.Add("@pToDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.ToDateSelected, _final_time.AddDays(1), Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime))
    _sql_cmd.Parameters.Add("@pClosingTime", SqlDbType.Int).Value = Me.uc_dsl.ClosingTime
    _sql_cmd.Parameters.Add("@pMaskStatus", SqlDbType.Int).Value = WSI.Common.HANDPAY_STATUS.MASK_STATUS
    _sql_cmd.Parameters.Add("@pStatusPaid", SqlDbType.Int).Value = WSI.Common.HANDPAY_STATUS.PAID
    'RGR 26-NOV-2015
    _sql_cmd.Parameters.Add("@pAll", SqlDbType.Int).Value = 0
    _sql_where = GetSqlWhere()
    _sql_cmd.Parameters.Add("@pTerminalWhere", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where), " ", _sql_where)
    _sql_cmd.Parameters.Add("@pBillDetail", SqlDbType.Bit).Value = Me.cb_bills_denominations.Checked

    'FAV 20-MAY-2015
    _sql_cmd.Parameters.Add("@pShowMetters", SqlDbType.Bit).Value = 0
    _sql_cmd.Parameters.Add("@pOnlyGroupByTerminal", SqlDbType.Bit).Value = 0

    'JML 18-FEB-2016 include JACKPOT room
    _sql_cmd.Parameters.Add("@pIncludeJackpotRoom", SqlDbType.Bit).Value = m_include_jackpot

    m_query_as_datatable = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    ' Style main Grid  (after DB read because it has dinamic columns)
    Call Me.GUI_StyleSheet()

    Call GUI_BeforeFirstRow()

    If m_query_as_datatable.Rows.Count > 0 Then
      For Each _row In m_query_as_datatable.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_collection_by_machine_and_date_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

    End If
    Call GUI_AfterLastRow()

  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_data As List(Of Object)
    Dim _temp_int As Int64
    Dim _temp_dec As Decimal
    Dim _temp_bills As Decimal

    'RGR 21-OCT-2015
    Dim _temp_net_tito As Decimal
    Dim _temp_hand_pays As Decimal

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' DENOMINATION
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = DbRow.Value(SQL_COLUMN_DENOMINATION)

    ' ORDER_DATE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORDER_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ORDER_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' ORDER_DATE_END
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORDER_DATE_END).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ORDER_DATE_END), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    'COLLECTED_COIN_AMOUNT
    If DbRow.IsNull(SQL_COLUMN_COLLECTED_COIN_AMOUNT) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_COLLECTED_COIN_AMOUNT)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_COIN).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TICKET_IN_COUNT
    If DbRow.IsNull(SQL_COLUMN_TICKET_IN_COUNT) Then
      _temp_int = 0
    Else
      _temp_int = DbRow.Value(SQL_COLUMN_TICKET_IN_COUNT)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKET_IN_COUNT).Value = GUI_FormatNumber(_temp_int, 0)

    'TI_IN_AMOUNT_RE
    If DbRow.IsNull(SQL_COLUMN_TI_IN_AMOUNT_RE) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_TI_IN_AMOUNT_RE)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_IN_AMOUNT_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_IN_AMOUNT_NO_RE
    If DbRow.IsNull(SQL_COLUMN_TI_IN_AMOUNT_NO_RE) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_TI_IN_AMOUNT_NO_RE)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_IN_AMOUNT
    If DbRow.IsNull(SQL_COLUMN_TI_IN_AMOUNT) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_TI_IN_AMOUNT)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_IN_AMOUNT).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TICKET_OUT_COUNT
    If DbRow.IsNull(SQL_COLUMN_TICKET_OUT_COUNT) Then
      _temp_int = 0
    Else
      _temp_int = DbRow.Value(SQL_COLUMN_TICKET_OUT_COUNT)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKET_OUT_COUNT).Value = GUI_FormatNumber(_temp_int, 0)

    'TI_OUT_AMOUNT_RE
    If DbRow.IsNull(SQL_COLUMN_TI_OUT_AMOUNT_RE) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_TI_OUT_AMOUNT_RE)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_OUT_AMOUNT_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_OUT_AMOUNT_NO_RE
    If DbRow.IsNull(SQL_COLUMN_TI_OUT_AMOUNT_NO_RE) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_TI_OUT_AMOUNT_NO_RE)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_OUT_AMOUNT
    If DbRow.IsNull(SQL_COLUMN_TI_OUT_AMOUNT) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_TI_OUT_AMOUNT)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_OUT_AMOUNT).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' MANUAL
    _temp_hand_pays = 0
    If DbRow.IsNull(SQL_COLUMN_MANUAL) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_MANUAL)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MANUAL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CREDIT_CANCEL
    If DbRow.IsNull(SQL_COLUMN_CREDIT_CANCEL) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_CREDIT_CANCEL)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' JACKPOT_DE_SALA
    If DbRow.IsNull(SQL_COLUMN_JACKPOT_DE_SALA) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_JACKPOT_DE_SALA)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT_DE_SALA).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVES
    If DbRow.IsNull(SQL_COLUMN_PROGRESIVES) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_PROGRESIVES)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESIVES).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NO_PROGRESIVES
    If DbRow.IsNull(SQL_COLUMN_NO_PROGRESIVES) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_NO_PROGRESIVES)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_PROGRESIVES).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' HAND_PAYS_TOTAL
    If DbRow.IsNull(SQL_COLUMN_HAND_PAYS_TOTAL) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_HAND_PAYS_TOTAL)
      _temp_hand_pays += _temp_dec
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HAND_PAYS_TOTAL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVE_PROVISIONS
    If DbRow.IsNull(SQL_COLUMN_PROGRESIVE_PROVISIONS) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_PROGRESIVE_PROVISIONS)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Bills
    _temp_bills = 0
    For _index As Integer = SQL_FIRST_COLUMN_VARIABLE To m_query_as_datatable.Columns.Count - 1
      If DbRow.IsNull(_index) Then
        _temp_dec = 0
      Else
        _temp_dec = DbRow.Value(_index)
        _temp_bills += DbRow.Value(_index)
      End If
      Me.Grid.Cell(RowIndex, GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Next

    Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTED_COIN_AMOUNT).Value = GUI_FormatCurrency(_temp_bills, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TITO
    _temp_net_tito = 0
    If DbRow.IsNull(SQL_COLUMN_NET_TITO) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_NET_TITO)
      _temp_net_tito = DbRow.Value(SQL_COLUMN_NET_TITO)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NET_TITO).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    '' NETWIN_BILLS
    'Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_BILLS).Value = GUI_FormatCurrency(_temp_win, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    '' NETWIN_HAND_PAY
    'Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_HAND_PAY).Value = GUI_FormatCurrency(_temp_hp, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TOTAL   SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS
    If DbRow.IsNull(SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS) Then
      _temp_dec = 0
    Else
      _temp_dec = DbRow.Value(SQL_COLUMN_NETWIN_TOTAL_WITHOUT_BILLS)
    End If
    'RGR 21-OCT-2015 - Change in formula: Total Win
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_bills - _temp_net_tito - _temp_hand_pays, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    m_counter_list(COUNTER_DETAILS) += 1

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If
    m_refresh_grid = False

    m_subtotal_break = ""
    m_counter_list(COUNTER_DETAILS) = 0
    m_counter_list(COUNTER_SUBTOTALS) = 0
  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _text_subtotal As String
    If Not Me.cb_total.Checked Then
      Return True
    End If

    Dim _provider_id As String
    _provider_id = TerminalReport.GetReportData(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID), ReportColumn.Provider)

    If m_subtotal_break <> "" And m_subtotal_break <> _provider_id Then
      _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)

      ProcesSubTotal(False, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal & " " & m_subtotal_break)
    End If

    If Not String.IsNullOrEmpty(_provider_id) Then
      m_subtotal_break = _provider_id
    End If

    If Not Me.cb_detail.Checked Then
      Return False
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _text_subtotal As String
    Dim _text_total As String

    _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)
    _text_total = GLB_NLS_GUI_INVOICING.GetString(205)

    If Me.cb_total.Checked Then
      ProcesSubTotal(False, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal & " " & m_subtotal_break)
    End If
    ProcesSubTotal(True, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, _text_total)

    If Grid.Counter(COUNTER_DETAILS).Value <> m_counter_list(COUNTER_DETAILS) Then
      Grid.Counter(COUNTER_DETAILS).Value = m_counter_list(COUNTER_DETAILS)
    End If
    If Grid.Counter(COUNTER_SUBTOTALS).Value <> m_counter_list(COUNTER_SUBTOTALS) Then
      Grid.Counter(COUNTER_SUBTOTALS).Value = m_counter_list(COUNTER_SUBTOTALS)
    End If

  End Sub ' GUI_AfterLastRow

#End Region  ' Overrides

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), IIf(m_is_check_dateTo, m_date_to, "---"))
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3477), m_denomination)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5361), m_list_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4440, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5431)), IIf(m_include_jackpot, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    PrintData.SetFilter("", Me.lbl_remarks_1.Text)
    PrintData.SetFilter("", Me.lbl_remarks_2.Text)
    PrintData.SetFilter("", Me.lbl_remarks_3.Text)

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    ' Date
    _date = uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _date = ToDate()
    m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    m_is_check_dateTo = uc_dsl.ToDateSelected

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()
    If m_terminal Is Nothing Then m_terminal = ""

    m_denomination = Me.uc_denominations.SelectedValuesList

    m_list_type = ""
    If Me.cb_detail.Checked Then
      m_list_type &= Me.cb_detail.Text & ", "
    End If

    If Me.cb_total.Checked Then
      m_list_type &= Me.cb_total.Text & ", "
    End If

    If Me.cb_bills_denominations.Checked Then
      m_list_type &= Me.cb_bills_denominations.Text & ", "
    End If

    If Me.chk_terminal_location.Checked Then
      m_list_type &= Me.chk_terminal_location.Text & ", "
    End If

    If m_list_type.Length > 2 Then
      m_list_type = m_list_type.Substring(0, m_list_type.Length - 2)
    End If

    m_include_jackpot = Me.chk_include_jackpot.Checked

  End Sub 'GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - MdiParent
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

#Region " Private Functions "

  ' PURPOSE: Put values to default
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.cb_detail.Checked = True
    Me.cb_total.Checked = False
    Me.cb_bills_denominations.Checked = True
    Me.chk_terminal_location.Checked = False
    Me.chk_include_jackpot.Checked = True

    ' Machines
    Call Me.uc_pr_list.SetDefaultValues()

    ' Denominations
    Call FillDenominationsFilterGrid()
    Call Me.uc_denominations.ExpandAll()
    Call Me.uc_denominations.SetDefaultValue(True)

  End Sub 'SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _index As Int32

    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .Sortable = True
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_DETAILS).Visible = True
      .Counter(COUNTER_DETAILS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_DETAILS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      .Counter(COUNTER_SUBTOTALS).Visible = True
      .Counter(COUNTER_SUBTOTALS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_SUBTOTALS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5417)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TE_NAME = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5417)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5421)
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DENOMINATION).Width = 1500

      ' ORDER_DATE
      .Column(GRID_COLUMN_ORDER_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5422)
      .Column(GRID_COLUMN_ORDER_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
      .Column(GRID_COLUMN_ORDER_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ORDER_DATE).Width = 1800

      ' ORDER_DATE_END
      .Column(GRID_COLUMN_ORDER_DATE_END).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5422)
      .Column(GRID_COLUMN_ORDER_DATE_END).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)
      .Column(GRID_COLUMN_ORDER_DATE_END).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ORDER_DATE_END).Width = 1800

      ' Bills totals
      .Column(GRID_COLUMN_COLLECTED_COIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5437)
      .Column(GRID_COLUMN_COLLECTED_COIN_AMOUNT).Header(1).Text = " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5426) & " " ' add space for separe titles
      .Column(GRID_COLUMN_COLLECTED_COIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_COLLECTED_COIN_AMOUNT).Width = 1800

      ' COLLECTED_COIN_AMOUNT
      .Column(GRID_COLUMN_TOTAL_COIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6734)
      .Column(GRID_COLUMN_TOTAL_COIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5426) ' no space for separe titles
      .Column(GRID_COLUMN_TOTAL_COIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TOTAL_COIN).Width = 1800

      ' TICKET_IN_COUNT
      .Column(GRID_COLUMN_TICKET_IN_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5423)
      .Column(GRID_COLUMN_TICKET_IN_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5425)
      .Column(GRID_COLUMN_TICKET_IN_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TICKET_IN_COUNT).Width = 1000

      ' TI_IN_AMOUNT_RE
      .Column(GRID_COLUMN_TI_IN_AMOUNT_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5423)
      .Column(GRID_COLUMN_TI_IN_AMOUNT_RE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5427)
      .Column(GRID_COLUMN_TI_IN_AMOUNT_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TI_IN_AMOUNT_RE).Width = 1500

      ' TI_IN_AMOUNT_NO_RE
      .Column(GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5423)
      .Column(GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5428)
      .Column(GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Width = 1500

      ' TI_IN_AMOUNT
      .Column(GRID_COLUMN_TI_IN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5423)
      .Column(GRID_COLUMN_TI_IN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5426)    ' view next title
      .Column(GRID_COLUMN_TI_IN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TI_IN_AMOUNT).Width = 1500

      ' TICKET_OUT_COUNT
      .Column(GRID_COLUMN_TICKET_OUT_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5424)
      .Column(GRID_COLUMN_TICKET_OUT_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5425)
      .Column(GRID_COLUMN_TICKET_OUT_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TICKET_OUT_COUNT).Width = 1000

      ' TI_OUT_AMOUNT_RE
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5424)
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_RE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5427)
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_RE).Width = 1500

      ' TI_OUT_AMOUNT_NO_RE
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5424)
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5428)
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Width = 1500

      ' TI_OUT_AMOUNT
      .Column(GRID_COLUMN_TI_OUT_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5424)
      .Column(GRID_COLUMN_TI_OUT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5426)
      .Column(GRID_COLUMN_TI_OUT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TI_OUT_AMOUNT).Width = 1500

      ' NET_TITO
      .Column(GRID_COLUMN_NET_TITO).Header(0).Text = " "
      '.Column(GRID_COLUMN_NET_TITO).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5436) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5427) & "*"
      .Column(GRID_COLUMN_NET_TITO).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5436) & "*"
      .Column(GRID_COLUMN_NET_TITO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NET_TITO).Width = 2200

      ' MANUAL
      .Column(GRID_COLUMN_MANUAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_MANUAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5429)
      .Column(GRID_COLUMN_MANUAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MANUAL).Width = 1800

      ' CREDIT_CANCEL
      .Column(GRID_COLUMN_CREDIT_CANCEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_CREDIT_CANCEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5430)
      .Column(GRID_COLUMN_CREDIT_CANCEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CREDIT_CANCEL).Width = 1800

      ' JACKPOT_DE_SALA
      If chk_include_jackpot.Checked Then
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5431)
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Width = 1800
      Else
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Header(1).Text = ""
        .Column(GRID_COLUMN_JACKPOT_DE_SALA).Width = 0
      End If

      ' PROGRESIVES
      .Column(GRID_COLUMN_PROGRESIVES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_PROGRESIVES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5432)
      .Column(GRID_COLUMN_PROGRESIVES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_PROGRESIVES).Width = 2200

      ' NO_PROGRESIVES
      .Column(GRID_COLUMN_NO_PROGRESIVES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_NO_PROGRESIVES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5433)
      .Column(GRID_COLUMN_NO_PROGRESIVES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NO_PROGRESIVES).Width = 2500

      ' HAND_PAYS_TOTAL
      .Column(GRID_COLUMN_HAND_PAYS_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_HAND_PAYS_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5372) & "**"
      .Column(GRID_COLUMN_HAND_PAYS_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_HAND_PAYS_TOTAL).Width = 1800

      ' PROGRESIVE_PROVISIONS
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5434)
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Width = 2500

      '' NETWIN_BILLS
      '.Column(GRID_COLUMN_NETWIN_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5435)
      '.Column(GRID_COLUMN_NETWIN_BILLS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5437)
      '.Column(GRID_COLUMN_NETWIN_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      '.Column(GRID_COLUMN_NETWIN_BILLS).Width = 1800

      '' NETWIN_HAND_PAY
      '.Column(GRID_COLUMN_NETWIN_HAND_PAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5435)
      '.Column(GRID_COLUMN_NETWIN_HAND_PAY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      '.Column(GRID_COLUMN_NETWIN_HAND_PAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      '.Column(GRID_COLUMN_NETWIN_HAND_PAY).Width = 1800

      ' NETWIN_TOTAL
      .Column(GRID_COLUMN_NETWIN_TOTAL).Header(0).Text = " "
      .Column(GRID_COLUMN_NETWIN_TOTAL).Header(1).Text = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5435), "***")
      .Column(GRID_COLUMN_NETWIN_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NETWIN_TOTAL).Width = 1800

      If Not m_query_as_datatable Is Nothing Then
        'Bills
        For _index = SQL_FIRST_COLUMN_VARIABLE To m_query_as_datatable.Columns.Count - 1
          If cb_bills_denominations.Checked Then
            .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5437)
            .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Header(1).Text = m_nfi.CurrencySymbol & m_query_as_datatable.Columns(_index).ColumnName
            .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Width = 1800
          Else
            .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Header(0).Text = ""
            .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Header(1).Text = ""
            .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Width = 0
          End If
          .Column(GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        Next
      End If

    End With

  End Sub ' GUI_StyleSheet

  Private Function ToDate() As Date

    If uc_dsl.ToDateSelected Then
      Return New Date(uc_dsl.ToDate.Year, uc_dsl.ToDate.Month, uc_dsl.ToDate.Day, uc_dsl.ClosingTime, 0, 0)
    Else
      Return New Date(Today.Year, Today.Month, Today.Day, uc_dsl.ClosingTime, 0, 0).AddDays(1)
    End If

  End Function  ' ToDate

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillDenominationsFilterGrid()
    Dim _denominations As String
    Dim _denominations_list As List(Of String)

    Dim _local_separator As String
    Dim _db_separator As String

    Call Me.uc_denominations.Clear()
    Call Me.uc_denominations.ReDraw(False)

    _denominations_list = New List(Of String)

    _local_separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
    _db_separator = CultureInfo.GetCultureInfo("en-US").NumberFormat.NumberDecimalSeparator

    _denominations = GeneralParam.GetString("EGM", "SingleDenominations").Replace(_db_separator, _local_separator)
    _denominations_list.AddRange(CLASS_TERMINAL_DENOMINATIONS.DenominationsToList(_denominations))

    _denominations = GeneralParam.GetString("EGM", "MultiDenominations").Replace(_db_separator, _local_separator)
    _denominations_list.AddRange(CLASS_TERMINAL_DENOMINATIONS.DenominationsToList(_denominations))

    _denominations_list.Add("--")

    Me.uc_denominations.Add(_denominations_list)

    Call Me.uc_denominations.CurrentRow(0)

    Call Me.uc_denominations.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As StringBuilder
    Dim _str_terminal_id As String
    Dim _selected_values As String()
    Dim _selected_values_string As String

    _str_where = New StringBuilder()
    _selected_values_string = ""

    ' Filter Providers - Terminals
    _str_terminal_id = " TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    _str_where.AppendLine(" AND " & _str_terminal_id)

    ' Filter Denominations
    If Me.uc_denominations.SelectedIndexes.Length <> Me.uc_denominations.Count() Then
      _selected_values = Me.uc_denominations.SelectedValuesArray
      For Each _item As String In _selected_values
        If _selected_values_string.Length > 0 Then
          _selected_values_string &= ","
        End If
        _selected_values_string &= "'" & _item & "'"
      Next
      _str_where.AppendLine(" AND ISNULL(TE_MULTI_DENOMINATION, '--') IN (" & _selected_values_string & ")  ")
    End If

    Return _str_where.ToString()

  End Function ' GetSqlWhere

  ' PURPOSE: Process subtotal value 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ProcesSubTotal(ByVal IsTotal As Boolean, ByVal IdxTotalColumn As Integer, ByVal ENUM_GUI As ControlColor.ENUM_GUI_COLOR, ByVal TextTotal As String)

    Dim _idx_row As Integer = Me.Grid.NumRows
    Dim _condition As String
    Dim _temp_obj As Object
    Dim _temp_dec As Decimal
    Dim _temp_net_tito As Decimal
    Dim _temp_hand_pays As Decimal
    Dim _temp_qty As Integer
    Dim _temp_bill As Decimal
    Dim _c_name As String

    If Not (Me.Grid.NumRows > 0 Or Me.cb_total.Checked) Then
      If IsTotal Then
        Call AddSubtotalWithoutValues(IdxTotalColumn)
      End If
      Return
    End If

    Me.Grid.AddRow()

    If IsTotal Then
      _condition = "1=1 "
    Else
      _condition = "TE_PROVIDER_ID = '" & m_subtotal_break & "'"
      m_counter_list(COUNTER_SUBTOTALS) += 1
    End If

    ' Label - TOTAL
    Me.Grid.Cell(_idx_row, IdxTotalColumn).Value = TextTotal

    'COLLECTED_COIN_AMOUNT
    _temp_obj = m_query_as_datatable.Compute("SUM(COLLECTED_COIN_AMOUNT)", _condition & " AND COLLECTED_COIN_AMOUNT IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(COLLECTED_COIN_AMOUNT)", _condition & " AND COLLECTED_COIN_AMOUNT IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_COIN).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TICKET_IN_COUNT
    _temp_obj = m_query_as_datatable.Compute("SUM(TICKET_IN_COUNT)", _condition & " AND TICKET_IN_COUNT IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_qty = 0
    Else
      _temp_qty = m_query_as_datatable.Compute("SUM(TICKET_IN_COUNT)", _condition & " AND TICKET_IN_COUNT IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKET_IN_COUNT).Value = GUI_FormatNumber(_temp_qty, 0)

    'TI_IN_AMOUNT_RE
    _temp_obj = m_query_as_datatable.Compute("SUM(TI_IN_AMOUNT_RE)", _condition & " AND TI_IN_AMOUNT_RE IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(TI_IN_AMOUNT_RE)", _condition & " AND TI_IN_AMOUNT_RE IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_IN_AMOUNT_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_IN_AMOUNT_NO_RE
    _temp_obj = m_query_as_datatable.Compute("SUM(TI_IN_AMOUNT_NO_RE)", _condition & " AND TI_IN_AMOUNT_NO_RE IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(TI_IN_AMOUNT_NO_RE)", _condition & " AND TI_IN_AMOUNT_NO_RE IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_IN_AMOUNT
    _temp_obj = m_query_as_datatable.Compute("SUM(TI_IN_AMOUNT)", _condition & " AND TI_IN_AMOUNT IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(TI_IN_AMOUNT)", _condition & " AND TI_IN_AMOUNT IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_IN_AMOUNT).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TICKET_OUT_COUNT
    _temp_obj = m_query_as_datatable.Compute("SUM(TICKET_OUT_COUNT)", _condition & " AND TICKET_OUT_COUNT IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_qty = 0
    Else
      _temp_qty = m_query_as_datatable.Compute("SUM(TICKET_OUT_COUNT)", _condition & " AND TICKET_OUT_COUNT IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKET_OUT_COUNT).Value = GUI_FormatNumber(_temp_qty, 0)

    'TI_OUT_AMOUNT_RE
    _temp_obj = m_query_as_datatable.Compute("SUM(TI_OUT_AMOUNT_RE)", _condition & " AND TI_OUT_AMOUNT_RE IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(TI_OUT_AMOUNT_RE)", _condition & " AND TI_OUT_AMOUNT_RE IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_OUT_AMOUNT_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_OUT_AMOUNT_NO_RE
    _temp_obj = m_query_as_datatable.Compute("SUM(TI_OUT_AMOUNT_NO_RE)", _condition & " AND TI_OUT_AMOUNT_NO_RE IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(TI_OUT_AMOUNT_NO_RE)", _condition & " AND TI_OUT_AMOUNT_NO_RE IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_OUT_AMOUNT
    _temp_obj = m_query_as_datatable.Compute("SUM(TI_OUT_AMOUNT)", _condition & " AND TI_OUT_AMOUNT IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(TI_OUT_AMOUNT)", _condition & " AND TI_OUT_AMOUNT IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_OUT_AMOUNT).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'NET_TITO 
    _temp_obj = m_query_as_datatable.Compute("SUM(NET_TITO)", _condition & " AND NET_TITO IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(NET_TITO)", _condition & " AND NET_TITO IS NOT NULL")
      _temp_net_tito = _temp_dec
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NET_TITO).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' MANUAL
    _temp_obj = m_query_as_datatable.Compute("SUM(MANUAL)", _condition & " AND MANUAL IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(MANUAL)", _condition & " AND MANUAL IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_MANUAL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CREDIT_CANCEL
    _temp_obj = m_query_as_datatable.Compute("SUM(CREDIT_CANCEL)", _condition & " AND CREDIT_CANCEL IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(CREDIT_CANCEL)", _condition & " AND CREDIT_CANCEL IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' JACKPOT_DE_SALA
    _temp_obj = m_query_as_datatable.Compute("SUM(JACKPOT_DE_SALA)", _condition & " AND JACKPOT_DE_SALA IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(JACKPOT_DE_SALA)", _condition & " AND JACKPOT_DE_SALA IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT_DE_SALA).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVES
    _temp_obj = m_query_as_datatable.Compute("SUM(PROGRESIVES)", _condition & " AND PROGRESIVES IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(PROGRESIVES)", _condition & " AND PROGRESIVES IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVES).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NO_PROGRESIVES
    _temp_obj = m_query_as_datatable.Compute("SUM(NO_PROGRESIVES)", _condition & " AND NO_PROGRESIVES IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(NO_PROGRESIVES)", _condition & " AND NO_PROGRESIVES IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NO_PROGRESIVES).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' HAND_PAYS_TOTAL
    _temp_obj = m_query_as_datatable.Compute("SUM(HAND_PAYS_TOTAL)", _condition & " AND HAND_PAYS_TOTAL IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = _temp_obj
      _temp_hand_pays += _temp_dec
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HAND_PAYS_TOTAL).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVE_PROVISIONS
    _temp_obj = m_query_as_datatable.Compute("SUM(PROGRESIVE_PROVISIONS)", _condition & " AND PROGRESIVE_PROVISIONS IS NOT NULL")
    If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
      _temp_dec = 0
    Else
      _temp_dec = m_query_as_datatable.Compute("SUM(PROGRESIVE_PROVISIONS)", _condition & " AND PROGRESIVE_PROVISIONS IS NOT NULL")
    End If
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Bills
    _temp_bill = 0
    For _index As Integer = SQL_FIRST_COLUMN_VARIABLE To m_query_as_datatable.Columns.Count - 1
      _c_name = "[" & m_query_as_datatable.Columns(_index).ColumnName & "]"
      _temp_obj = m_query_as_datatable.Compute("SUM(" & _c_name & ")", _condition & " AND " & _c_name & " IS NOT NULL")

      If _temp_obj Is Nothing OrElse IsDBNull(_temp_obj) Then
        _temp_dec = 0
      Else
        _temp_dec = m_query_as_datatable.Compute("SUM(" & _c_name & ")", _condition & " AND " & _c_name & " IS NOT NULL")
        _temp_bill += _temp_dec
      End If
      Me.Grid.Cell(_idx_row, GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Value = GUI_FormatCurrency(_temp_dec, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Next

    Me.Grid.Cell(_idx_row, GRID_COLUMN_COLLECTED_COIN_AMOUNT).Value = GUI_FormatCurrency(_temp_bill, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(_temp_bill - _temp_net_tito - _temp_hand_pays, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI)

  End Sub ' ProcesSubTotal

  Private Sub AddSubtotalWithoutValues(ByVal IdxTotalColumn As Integer)
    Dim _idx_row As Integer
    Dim _temp_bill As Integer

    _idx_row = Me.Grid.AddRow()

    ' Label - TOTAL
    Me.Grid.Cell(_idx_row, IdxTotalColumn).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    'COLLECTED_COIN_AMOUNT
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_COIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TICKET_IN_COUNT
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKET_IN_COUNT).Value = GUI_FormatNumber(0, 0)

    'TI_IN_AMOUNT_RE
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_IN_AMOUNT_RE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_IN_AMOUNT_NO_RE
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_IN_AMOUNT_NO_RE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_IN_AMOUNT
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_IN_AMOUNT).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TICKET_OUT_COUNT
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKET_OUT_COUNT).Value = GUI_FormatNumber(0, 0)

    'TI_OUT_AMOUNT_RE
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_OUT_AMOUNT_RE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_OUT_AMOUNT_NO_RE
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_OUT_AMOUNT_NO_RE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'TI_OUT_AMOUNT
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TI_OUT_AMOUNT).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'NET_TITO 
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NET_TITO).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' MANUAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_MANUAL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CREDIT_CANCEL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' JACKPOT_DE_SALA
    Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT_DE_SALA).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVES
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVES).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NO_PROGRESIVES
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NO_PROGRESIVES).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' HAND_PAYS_TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_HAND_PAYS_TOTAL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVE_PROVISIONS
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Bills
    _temp_bill = 0
    For _index As Integer = SQL_FIRST_COLUMN_VARIABLE To m_query_as_datatable.Columns.Count - 1
      Me.Grid.Cell(_idx_row, GRID_INIT_BILLS_DATA + _index - SQL_FIRST_COLUMN_VARIABLE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Next

    Me.Grid.Cell(_idx_row, GRID_COLUMN_COLLECTED_COIN_AMOUNT).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TOTAL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  End Sub

  ' PURPOSE: Set column order 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    If Not m_query_as_datatable Is Nothing Then
      BILLS_DATA_COLUMNS = m_query_as_datatable.Columns.Count - SQL_FIXED_COLUMNS
    Else
      BILLS_DATA_COLUMNS = 0
      m_refresh_grid = True
    End If

    GRID_COLUMN_INDEX = 0
    GRID_INIT_TERMINAL_DATA = 1

    GRID_COLUMN_DENOMINATION = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_ORDER_DATE = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_ORDER_DATE_END = TERMINAL_DATA_COLUMNS + 3
    'BILLS: VARIABLE(COLUMNS)
    GRID_INIT_BILLS_DATA = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_COLLECTED_COIN_AMOUNT = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 4

    GRID_COLUMN_TOTAL_COIN = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 5
    GRID_COLUMN_TICKET_IN_COUNT = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 6
    GRID_COLUMN_TI_IN_AMOUNT_RE = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 7
    GRID_COLUMN_TI_IN_AMOUNT_NO_RE = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 8
    GRID_COLUMN_TI_IN_AMOUNT = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 9
    GRID_COLUMN_TICKET_OUT_COUNT = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 10
    GRID_COLUMN_TI_OUT_AMOUNT_RE = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 11
    GRID_COLUMN_TI_OUT_AMOUNT_NO_RE = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 12
    GRID_COLUMN_TI_OUT_AMOUNT = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 13
    GRID_COLUMN_NET_TITO = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 14
    GRID_COLUMN_MANUAL = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 15
    GRID_COLUMN_CREDIT_CANCEL = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 16
    GRID_COLUMN_JACKPOT_DE_SALA = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 17
    GRID_COLUMN_PROGRESIVE_PROVISIONS = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 18
    GRID_COLUMN_NO_PROGRESIVES = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 19
    GRID_COLUMN_HAND_PAYS_TOTAL = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 20
    GRID_COLUMN_PROGRESIVES = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 21
    'GRID_COLUMN_NETWIN_BILLS = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 22
    'GRID_COLUMN_NETWIN_HAND_PAY = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 23
    GRID_COLUMN_NETWIN_TOTAL = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 22

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + BILLS_DATA_COLUMNS + 23

  End Sub

#End Region   ' Private Functions

#Region " Events"

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region 'Events

End Class