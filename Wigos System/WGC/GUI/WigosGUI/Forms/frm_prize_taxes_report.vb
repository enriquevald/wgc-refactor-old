'-------------------------------------------------------------------
' Copyright © 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
' 
' MODULE NAME:   frm_prize_taxes_report
' DESCRIPTION:   Information various from the movements of account and session playing 
' AUTHOR:        Javier Molina
' CREATION DATE: 23-JAN-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-JAN-2012  JMM    Initial version.
' 08-FEB-2012  JMM    Enabled evidence viewer.
' 24-AUG-2012  JAB    Control to show huge amounts of records & DoEvents run each second.
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 04-JUN-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 14-JUN-2013  DHA    Fixed Bug #848: Wrong date format on Date Ticket from Excel
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 02-OCT-2013  ANG    Add column 'PrizeInKind' in grid.
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 23-MAY-2014  HBB    Winnings taxes: Uncontrolled exception if it's set a wrong card number
' 05-AGO-2014  SGB    Fixed Bug #WIG-1155: columns RFC & CURP shown as account data parameters
' 10-MAR-2016  DDS    Fixed Bug 10564:Ref.16443: Added optional filter by accountId on SQL Query – Sala Jubile
' 15-MAR-2017  ATB    Third TAX - Report columns
' 16-MAR-2017  ATB    PBI 25737: Third TAX - Report columns
' 20-MAR-2017  ATB    PBI 25737: Third Tax - Report Columns
' 03-APR-2017  ATB    PBI 25737: Third TAX - Report columns
' 27-JUL-2017  MS     Bug: 29004: Winning taxes subtotal and total amounts do not match when "Premio en Especie (RE)" has been got
' 11-JAN-2018  JML    Fixed Bug 31238:WIGOS-7384 [Ticket #11446] Anulación de Venta Fichas,no se anula en reportes
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_prize_taxes_report
  Inherits frm_base_sel

#Region " Constants "

  Private Const GRID_COL_OPERATION_ID As Integer = 0
  Private GRID_COL_ACCOUNT_ID As Integer = 1
  Private GRID_COL_RFC As Integer = 2
  Private GRID_COL_CURP As Integer = 3
  Private GRID_COL_ACCOUNT_HOLDER_NAME As Integer = 4
  Private GRID_COL_DATE As Integer = 5
  Private Const GRID_COL_GROSS_PRIZE As Integer = 6
  Private Const GRID_COL_GROSS_PRIZE_IN_KIND As Integer = 7
  Private Const GRID_COL_GROSS_PRIZE_RE_IN_KIND As Integer = 8
  Private Const GRID_COL_TAX1 As Integer = 9
  Private Const GRID_COL_TAX2 As Integer = 10
  Private Const GRID_COL_TAX3 As Integer = 11
  Private Const GRID_COL_TOTAL_TAXES As Integer = 12
  Private Const GRID_COL_TOTAL_PAYMENT As Integer = 13
  Private Const GRID_COL_EVIDENCE As Integer = 14
  Private Const GRID_COL_DOCUMENT_ID As Integer = 15

  Private Const EXCEL_COL_GROSS_PRIZE_IN_KIND = 7
  Private Const GRID_COLUMNS As Integer = 16
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Counter
  Private Const COUNTER_DETAILS As Integer = 1
  Private Const COUNTER_ACCOUNT_TOTALS As Integer = 2

#End Region ' Constants

#Region " Structures "

  Private Class PRIZE_TAXES_REPORT_GRID_ROW
    Public m_oeperation_id As Object  'typed as object to allow null value on total row
    Public m_account_id As Object     'typed as object to allow null value on total row
    Public m_RFC As Object            'typed as object to allow null value on total row
    Public m_CURP As Object           'typed as object to allow null value on total row
    Public m_holder_name As Object    'typed as object to allow null value on total row
    Public m_prize_date As Object     'typed as object to allow null value on total row
    Public m_prize As Decimal
    Public m_prize_in_kind As Decimal
    Public m_prize_re_in_kind As Decimal
    Public m_tax1 As Decimal
    Public m_tax2 As Decimal
    Public m_tax3 As Decimal
    Public m_total_taxes As Decimal
    Public m_payment As Decimal
    Public m_evidence As Boolean
    Public m_document_id As Object

    Public Sub New(ByVal OperationID As Object, _
                   ByVal AccountID As Object, _
                   ByVal RFC As Object, _
                   ByVal CURP As Object, _
                   ByVal HolderName As Object, _
                   ByVal PrizeDate As Object, _
                   ByVal Prize As Decimal, _
                   ByVal PrizeInKind As Decimal, _
                   ByVal PrizeReInKind As Decimal, _
                   ByVal Tax1 As Decimal, _
                   ByVal Tax2 As Decimal, _
                   ByVal Tax3 As Decimal, _
                   ByVal Payment As Decimal, _
                   ByVal Evidence As Boolean, _
                   ByVal DocumentID As Object)
      m_oeperation_id = OperationID
      m_account_id = AccountID
      m_RFC = RFC
      m_CURP = CURP
      m_holder_name = HolderName
      m_prize_date = PrizeDate
      m_prize = Prize
      m_prize_in_kind = PrizeInKind
      m_prize_re_in_kind = PrizeReInKind
      m_tax1 = Tax1
      m_tax2 = Tax2
      m_tax3 = Tax3
      m_payment = Payment
      m_evidence = Evidence
      m_document_id = DocumentID
    End Sub
  End Class
#End Region ' Structures

#Region " Enums "

  Private Enum ENUM_REPORT_TYPE
    BY_PLAYER = 0
    CHRONOLOGICALLY = 1
  End Enum

  Private Enum ENUM_PRIZE_FILTER
    PRIZE_GREATER_THAN = 0
    ONLY_EVIDENCES = 1
    ONLY_EVIDENCES_GREATER_THAN = 2
  End Enum

#End Region ' Enums

#Region " Members "

  ' Report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_prize As String
  Private m_report_type As String
  Private m_evidence_flag_value As String
  Private m_prize_in_kind_has_values As Boolean
  Private m_prize_re_in_kind_has_values As Boolean
  Private m_hide_prize_in_kind_column As Boolean
  Private m_is_council_tax_active As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PRIZE_TAXES_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Prize Taxes
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(449)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(437)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Height += 10
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Prize filter...
    Me.gb_prize.Text = GLB_NLS_GUI_INVOICING.GetString(43)

    Me.cmb_prize_filter.Text = ""
    Me.cmb_prize_filter.Add(ENUM_PRIZE_FILTER.PRIZE_GREATER_THAN, GLB_NLS_GUI_INVOICING.GetString(429))
    Me.cmb_prize_filter.Add(ENUM_PRIZE_FILTER.ONLY_EVIDENCES, GLB_NLS_GUI_INVOICING.GetString(436))
    Me.cmb_prize_filter.Add(ENUM_PRIZE_FILTER.ONLY_EVIDENCES_GREATER_THAN, GLB_NLS_GUI_INVOICING.GetString(440))

    Me.ef_greater_than.Text = ""
    Me.ef_greater_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    'Report type combo box
    Me.cmb_report_type.Text = GLB_NLS_GUI_INVOICING.GetString(433)
    Me.cmb_report_type.Add(ENUM_REPORT_TYPE.BY_PLAYER, GLB_NLS_GUI_INVOICING.GetString(434))
    Me.cmb_report_type.Add(ENUM_REPORT_TYPE.CHRONOLOGICALLY, GLB_NLS_GUI_INVOICING.GetString(435))

    'Evicences explanantion label
    Me.lbl_info.Text = GLB_NLS_GUI_INVOICING.GetString(438)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_from

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' If is some greater than filter selected, there should be an value on greater than entry field
    If (Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.PRIZE_GREATER_THAN _
      Or Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES_GREATER_THAN) _
      And Me.ef_greater_than.Value = "" Then

      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(107), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.ef_greater_than.Focus()

      Return False

    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _ds As DataSet
    Dim _operation_id As Int64
    Dim _account_id As Int64
    Dim _show_totals As Boolean
    Dim _dr_current_account As DataRow
    Dim _prize_total As Decimal
    Dim _prize_in_kind_total As Decimal
    Dim _prize_re_in_kind_total As Decimal
    Dim _tax1_total As Decimal
    Dim _tax2_total As Decimal
    Dim _tax3_total As Decimal
    Dim _payment_total As Decimal
    Dim _prev_account_id As Int64
    Dim _dr_movs() As DataRow
    Dim _current_account_id As Int64
    Dim _payment As Decimal
    Dim _prize As Decimal
    Dim _prize_in_kind As Decimal
    Dim _prize_re_in_kind As Decimal
    Dim _tax1 As Decimal
    Dim _tax2 As Decimal
    Dim _tax3 As Decimal
    Dim _grid_row As PRIZE_TAXES_REPORT_GRID_ROW
    Dim _idx_row As Integer
    Dim _prize_higher_than_filter As Decimal
    Dim _sort As String
    Dim _from_date As Date
    Dim _to_date As Date
    Dim _account_filter As String
    Dim _acc_rows() As DataRow
    Dim _internal_card_id As String
    Dim _rc As Boolean
    Dim _card_type As Integer
    Dim _row_RFC As String
    Dim _row_CURP As String
    Dim _row_holder_name As String
    Dim _aux_holder_name As String
    Dim _aux_evidence_holder_name As String
    Dim _evidences_only As Boolean
    Dim _document_id As Object
    Dim _account_id_to_filter As Int64?

    _ds = Nothing
    _dr_current_account = Nothing
    _prize_total = 0
    _tax1_total = 0
    _tax2_total = 0
    _tax3_total = 0
    _payment_total = 0
    _prev_account_id = -1
    _sort = "NoCuenta, FechaHora" 'Default sort

    Try
      Call GUI_StyleSheet()

      ' If some greater-than filter is selected, we get the greater than entry field value
      If cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES_GREATER_THAN _
        Or cmb_prize_filter.Value = ENUM_PRIZE_FILTER.PRIZE_GREATER_THAN Then
        _prize_higher_than_filter = GUI_ParseNumber(Me.ef_greater_than.Value)
      Else
        _prize_higher_than_filter = 0
      End If

      'Get date filters
      If Me.dtp_from.Checked Then
        _from_date = Me.dtp_from.Value
      Else
        _from_date = New Date(2007, 1, 1) 'There wouldn't be any prize before 1-JAN-2007
      End If

      If Me.dtp_to.Checked Then
        _to_date = Me.dtp_to.Value
      Else
        _to_date = WSI.Common.Misc.TodayOpening().AddDays(1)
      End If

      _evidences_only = (Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES _
                          Or Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES_GREATER_THAN)

      'Get account id filter
      _account_id = GUI_ParseNumber(uc_account_sel.Account)
      If _account_id > 0 Then
        _account_id_to_filter = _account_id
      End If

      _ds = WSI.Common.XMLReport.AccountPrizes(_from_date, _
                                               _to_date, _
                                               _prize_higher_than_filter, _
                                               _evidences_only, _
                                               _account_id_to_filter)

      Me.Grid.Redraw = False

      If _ds.Tables.Count < 2 Then
        Throw New Exception("Minimum tables has not been retrieved. Tables.Count = " & _ds.Tables.Count)
      End If



      'Set the report order depending on the report type selected
      Select Case cmb_report_type.Value
        Case ENUM_REPORT_TYPE.BY_PLAYER
          _sort = "NoCuenta, FechaHora"
          _show_totals = True
        Case ENUM_REPORT_TYPE.CHRONOLOGICALLY
          _sort = "FechaHora, NoCuenta"
          _show_totals = False
      End Select

      _dr_movs = _ds.Tables("Movimiento").Select("", _sort)

      'Enable/disable totals counter depending on the report type selected
      Me.Grid.Counter(COUNTER_ACCOUNT_TOTALS).Visible = _show_totals

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_dr_movs.Length) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      Call GUI_BeforeFirstRow()

      For Each _row As DataRow In _dr_movs

        ' JAB 24-AUG-2012: DoEvents run each second.
        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _operation_id = _row.Item("IdOperacion")
        _current_account_id = _row.Item("NoCuenta")
        _document_id = _row.Item("DocumentID")

        'if get a different account from the previous...
        If _prev_account_id <> _current_account_id Then

          '...show previous account's totals if there is a previous one
          If _dr_current_account IsNot Nothing _
            And _show_totals Then
            Call ShowAccountTotals(_dr_current_account)
          End If

          'if the current account id is different than the account id set at the filters,
          '_dr_current_acount is set to Nothing to later discard this operation
          If _account_id <> 0 _
            And _account_id <> _current_account_id Then
            _dr_current_account = Nothing
          Else
            _account_filter = "NoCuenta = " & _current_account_id

            'trackdata filtering
            If uc_account_sel.TrackData <> "" Then
              If uc_account_sel.TrackData.Length = TRACK_DATA_LENGTH_WIN Then

                _internal_card_id = ""

                _rc = WSI.Common.CardNumber.TrackDataToInternal(uc_account_sel.TrackData, _
                                                                  _internal_card_id, _
                                                                  _card_type)

                If _rc Then
                  _account_filter += " AND TrackData = '" & _internal_card_id & "'"
                Else
                  _account_filter += " AND TrackData = '" & uc_account_sel.TrackData & "'"
                End If
              Else
                _account_filter += " AND TrackData = '" & uc_account_sel.TrackData & "'"
              End If
            End If

            'get the account data from the Cuenta dataTable
            _acc_rows = _ds.Tables("Cuenta").Select(_account_filter)

            If _acc_rows.Length = 1 Then
              _dr_current_account = _acc_rows(0)

              'Once get the account data, check if its name contains the name filter
              If uc_account_sel.Holder <> "" Then

                If Not IsDBNull(_dr_current_account.Item("Nombre")) Then
                  _aux_holder_name = _dr_current_account.Item("Nombre").ToString()
                Else
                  _aux_holder_name = ""
                End If

                If Not IsDBNull(_row.Item("Constancia.Nombre")) Then
                  _aux_evidence_holder_name = _row.Item("Constancia.Nombre").ToString()
                Else
                  _aux_evidence_holder_name = ""
                End If

                'if the account data doesn't contain the holder name input on filters by user,
                '_dr_current_acount is set to Nothing to later discard this operation
                If Not UCase(_aux_holder_name).ToString().Contains(UCase(uc_account_sel.Holder)) _
                  And Not UCase(_aux_evidence_holder_name).ToString().Contains(UCase(uc_account_sel.Holder)) Then
                  _dr_current_account = Nothing
                End If
              End If
            Else
              _dr_current_account = Nothing
            End If

          End If

          _prev_account_id = _current_account_id
        End If

        'process only if we get an account data
        If _dr_current_account IsNot Nothing Then

          If IsDBNull(_row.Item("Premio")) Then
            _prize = 0
          Else
            _prize = _row.Item("Premio")
          End If

          If IsDBNull(_row.Item("PremioEspecie")) Then
            _prize_in_kind = 0
          Else
            _prize_in_kind = _row.Item("PremioEspecie")
          End If

          If IsDBNull(_row.Item("PremioEspecieRE")) Then
            _prize_re_in_kind = 0
          Else
            _prize_re_in_kind = _row.Item("PremioEspecieRE")
          End If


          If IsDBNull(_row.Item("ISR1")) Then
            _tax1 = 0
          Else
            _tax1 = _row.Item("ISR1")
          End If

          If IsDBNull(_row.Item("ISR2")) Then
            _tax2 = 0
          Else
            _tax2 = _row.Item("ISR2")
          End If

          If IsDBNull(_row.Item("ISR3")) Then
            _tax3 = 0
          Else
            _tax3 = _row.Item("ISR3")
          End If

          If _prize <> 0 Then
            _payment = _prize - _tax1 - _tax2 - _tax3
          Else
            _payment = 0
          End If

          'if the operation has a related evidence, we get all the data from there...
          If _row.Item("Constancia") Then
            If Not IsDBNull(_row.Item("Constancia.RFC")) Then
              _row_RFC = _row.Item("Constancia.RFC")
            Else
              _row_RFC = ""
            End If

            If Not IsDBNull(_row.Item("Constancia.CURP")) Then
              _row_CURP = _row.Item("Constancia.CURP")
            Else
              _row_CURP = ""
            End If

            If Not IsDBNull(_row.Item("Constancia.Nombre")) Then
              _row_holder_name = _row.Item("Constancia.Nombre")
            Else
              _row_holder_name = ""
            End If
          Else
            '...if not, we get the data from the account table
            If Not IsDBNull(_dr_current_account.Item("RFC")) Then
              _row_RFC = _dr_current_account.Item("RFC")
            Else
              _row_RFC = ""
            End If

            If Not IsDBNull(_dr_current_account.Item("CURP")) Then
              _row_CURP = _dr_current_account.Item("CURP")
            Else
              _row_CURP = ""
            End If

            If Not IsDBNull(_dr_current_account.Item("Nombre")) Then
              _row_holder_name = _dr_current_account.Item("Nombre")
            Else
              _row_holder_name = ""
            End If
          End If

          _grid_row = New PRIZE_TAXES_REPORT_GRID_ROW(_operation_id, _
                                                      _current_account_id, _
                                                      _row_RFC, _
                                                      _row_CURP, _
                                                      _row_holder_name, _
                                                      _row.Item("FechaHora"), _
                                                      _prize, _
                                                      _prize_in_kind, _
                                                      _prize_re_in_kind, _
                                                      _tax1, _
                                                      _tax2, _
                                                      _tax3, _
                                                      _payment, _
                                                      _row.Item("Constancia"), _
                                                      _document_id)

          Me.Grid.AddRow()

          _idx_row = Me.Grid.NumRows - 1

          If Not SetupRow(_idx_row, _grid_row) Then
            ' The row can not be added
            Me.Grid.DeleteRowFast(_idx_row)
          Else
            Me.Grid.Counter(COUNTER_DETAILS).Value += 1
          End If

          _prize_total += _prize
          _prize_in_kind_total += _prize_in_kind
          _prize_re_in_kind_total += _prize_re_in_kind
          _tax1_total += _tax1
          _tax2_total += _tax2
          _tax3_total += _tax3
          ' Bug 29004: Recalculated on Total Call
          '_payment_total += _payment
        End If
      Next

      ' If the total is greater than 0 there are values
      If _prize_in_kind_total > 0.0 Then
        m_prize_in_kind_has_values = True
      End If

      If _prize_re_in_kind_total > 0.0 Then
        m_prize_re_in_kind_has_values = True
      End If

      Call GUI_AfterLastRow()

      'put on the grid the last account's totals 
      If _dr_current_account IsNot Nothing _
        And _show_totals Then
        Call ShowAccountTotals(_dr_current_account)
      End If

      ' Bug 29004: Winning taxes subtotal and total amounts do not match when "Premio en Especie (RE)" has been got
      _payment_total = _prize_total - (_tax1_total + _tax2_total + _tax3_total)
      'put on the grid the totals 
      Call ShowTotals(_prize_total, _prize_in_kind_total, _prize_re_in_kind_total, _tax1_total, _tax2_total, _tax3_total, _payment_total)

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Capacity", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      Call Application.DoEvents()

      If _ds IsNot Nothing Then
        Call _ds.Clear()
        Call _ds.Dispose()
        _ds = Nothing
      End If

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick


  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _idx_row As Short
    Dim _operation_id As Int64
    Dim _frm_evidenceEdit As frm_evidence_edit



    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Search the first row selected
      For _idx_row = 0 To Me.Grid.NumRows - 1
        If Me.Grid.Row(_idx_row).IsSelected Then
          Exit For
        End If
      Next

      If _idx_row < Me.Grid.NumRows Then

        If Me.Grid.Cell(_idx_row, GRID_COL_EVIDENCE).Value = m_evidence_flag_value Then

          ' Get the complete session ID and session date and launch select form 
          If Me.Grid.Cell(_idx_row, GRID_COL_OPERATION_ID).Value <> "" Then

            _operation_id = Me.Grid.Cell(_idx_row, GRID_COL_OPERATION_ID).Value

            _frm_evidenceEdit = New frm_evidence_edit
            Call _frm_evidenceEdit.ShowEditItem(_operation_id)


          End If
        Else

          Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(109), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK)

        End If
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Prize Taxes Report ", _
                            "GUI_ShowSelectedItem", _
                            ex.Message)

    Finally

      Windows.Forms.Cursor.Current = Cursors.Default

    End Try

  End Sub ' GUI_EditSelectedItem

#Region " GUI Reports "

  ' PURPOSE : Set Custom formats to excel
  '           Sets Prize in Kind as Currency
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  ' 
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    MyBase.GUI_ReportParams(ExcelData, FirstColIndex)

    ExcelData.SetColumnFormat(EXCEL_COL_GROSS_PRIZE_IN_KIND, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.CURRENCY)

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _info_caption As String
    Dim _info_value As String
    Dim _string_array() As String

    _info_caption = ""
    _info_value = ""

    'First col
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    'Second col
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    'Third col
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(43), m_prize)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(433), m_report_type)

    'Forth col

    _string_array = GLB_NLS_GUI_INVOICING.GetString(438).Split(":")

    If _string_array.Length = 2 Then
      _info_caption = _string_array(0).Trim()
      _info_value = _string_array(1).Trim()
    End If
    PrintData.SetFilter(_info_caption, _info_value)

    PrintData.FilterValueWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(3) = 3000
    PrintData.FilterHeaderWidth(4) = 300

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Card Account number
    m_account = Me.uc_account_sel.Account()
    m_track_data = Me.uc_account_sel.TrackData()
    m_holder = Me.uc_account_sel.Holder()
    m_holder_is_vip = Me.uc_account_sel.HolderIsVip()

    ' Date
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      m_date_from = ""
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      m_date_to = ""
    End If

    'Prize filter
    Select Case Me.cmb_prize_filter.Value
      Case ENUM_PRIZE_FILTER.PRIZE_GREATER_THAN
        m_prize = GLB_NLS_GUI_INVOICING.GetString(429) + " " + GUI_FormatCurrency(Me.ef_greater_than.Value)

      Case ENUM_PRIZE_FILTER.ONLY_EVIDENCES
        m_prize = GLB_NLS_GUI_INVOICING.GetString(436)

      Case ENUM_PRIZE_FILTER.ONLY_EVIDENCES_GREATER_THAN
        m_prize = GLB_NLS_GUI_INVOICING.GetString(440) + " " + GUI_FormatCurrency(Me.ef_greater_than.Value)

    End Select

    'Report type
    Select Case Me.cmb_report_type.Value
      Case ENUM_REPORT_TYPE.BY_PLAYER
        m_report_type = GLB_NLS_GUI_INVOICING.GetString(434)
      Case ENUM_REPORT_TYPE.CHRONOLOGICALLY
        m_report_type = GLB_NLS_GUI_INVOICING.GetString(435)
    End Select

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    Dim _merge_columns As Boolean
    m_hide_prize_in_kind_column = GeneralParam.GetBoolean("Cashier", "Promotion.HideUNR", True)
    _merge_columns = (cmb_report_type.Value = ENUM_REPORT_TYPE.BY_PLAYER)

    'Columns order is different for each report type
    Select Case cmb_report_type.Value

      Case ENUM_REPORT_TYPE.BY_PLAYER
        GRID_COL_ACCOUNT_ID = 1
        GRID_COL_RFC = 2
        GRID_COL_CURP = 3
        GRID_COL_ACCOUNT_HOLDER_NAME = 4
        GRID_COL_DATE = 5

      Case ENUM_REPORT_TYPE.CHRONOLOGICALLY
        GRID_COL_DATE = 1
        GRID_COL_ACCOUNT_ID = 2
        GRID_COL_RFC = 3
        GRID_COL_CURP = 4
        GRID_COL_ACCOUNT_HOLDER_NAME = 5

    End Select

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_DETAILS).Visible = True
      .Counter(COUNTER_DETAILS).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_DETAILS).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_ACCOUNT_TOTALS).Visible = True
      .Counter(COUNTER_ACCOUNT_TOTALS).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_ACCOUNT_TOTALS).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' OperationID
      .Column(GRID_COL_OPERATION_ID).Header(0).Text = ""
      .Column(GRID_COL_OPERATION_ID).Width = 0

      ' AccountID
      .Column(GRID_COL_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COL_ACCOUNT_ID).Width = 1150
      .Column(GRID_COL_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_ACCOUNT_ID).IsMerged = _merge_columns

      ' RFC
      .Column(GRID_COL_RFC).Header(0).Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
      .Column(GRID_COL_RFC).Width = IIf(IdentificationTypes.GetIdentificationTypes().ContainsKey(ACCOUNT_HOLDER_ID_TYPE.RFC), 1850, 0)
      .Column(GRID_COL_RFC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COL_RFC).IsMerged = _merge_columns

      ' CURP
      .Column(GRID_COL_CURP).Header(0).Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)
      .Column(GRID_COL_CURP).Width = IIf(IdentificationTypes.GetIdentificationTypes().ContainsKey(ACCOUNT_HOLDER_ID_TYPE.CURP), 1000, 0)
      .Column(GRID_COL_CURP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COL_CURP).IsMerged = _merge_columns

      ' Holder name
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(210)
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Width = 3280
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COL_ACCOUNT_HOLDER_NAME).IsMerged = _merge_columns

      ' Date
      .Column(GRID_COL_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COL_DATE).Width = 2000
      .Column(GRID_COL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Gross Prize
      .Column(GRID_COL_GROSS_PRIZE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(33)
      .Column(GRID_COL_GROSS_PRIZE).Width = 1500
      .Column(GRID_COL_GROSS_PRIZE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Gross Prize In Kind
      .Column(GRID_COL_GROSS_PRIZE_IN_KIND).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement") '' Prize In Kind
      .Column(GRID_COL_GROSS_PRIZE_IN_KIND).Width = IIf(m_hide_prize_in_kind_column, 0, 1800)
      .Column(GRID_COL_GROSS_PRIZE_IN_KIND).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GROSS_PRIZE_IN_KIND).Fixed = False

      ' Gross Prize In Kind Re
      .Column(GRID_COL_GROSS_PRIZE_RE_IN_KIND).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement") '' Prize In Kind RE
      .Column(GRID_COL_GROSS_PRIZE_RE_IN_KIND).Width = IIf(m_hide_prize_in_kind_column, 0, 1800)
      .Column(GRID_COL_GROSS_PRIZE_RE_IN_KIND).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GROSS_PRIZE_RE_IN_KIND).Fixed = False


      ' Tax1
      .Column(GRID_COL_TAX1).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")
      .Column(GRID_COL_TAX1).Width = 1750
      .Column(GRID_COL_TAX1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tax2
      .Column(GRID_COL_TAX2).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")
      .Column(GRID_COL_TAX2).Width = 1750
      .Column(GRID_COL_TAX2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Tax3
      .Column(GRID_COL_TAX3).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name")
      If m_is_council_tax_active Then
        .Column(GRID_COL_TAX3).Width = 1750
      Else
        .Column(GRID_COL_TAX3).Width = 0
      End If
      .Column(GRID_COL_TAX3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Taxes
      .Column(GRID_COL_TOTAL_TAXES).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(513)
      .Column(GRID_COL_TOTAL_TAXES).Width = 1500
      .Column(GRID_COL_TOTAL_TAXES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Total amount
      .Column(GRID_COL_TOTAL_PAYMENT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(432)
      .Column(GRID_COL_TOTAL_PAYMENT).Width = 1500
      .Column(GRID_COL_TOTAL_PAYMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Evidence
      .Column(GRID_COL_EVIDENCE).Header(0).Text = "*"
      .Column(GRID_COL_EVIDENCE).Width = 300
      .Column(GRID_COL_EVIDENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Docuement ID
      .Column(GRID_COL_DOCUMENT_ID).Header(0).Text = ""
      .Column(GRID_COL_DOCUMENT_ID).Width = 0
      .Column(GRID_COL_DOCUMENT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim initial_time As Date
    Dim _prize_greater_than As Double

    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Holder = ""

    initial_time = WSI.Common.Misc.TodayOpening()

    ' Set at current month's first day 
    Me.dtp_from.Value = initial_time.AddDays(-initial_time.Day + 1)
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    If Not Double.TryParse(WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "OnPrizeGreaterThan"), _prize_greater_than) Then
      _prize_greater_than = 10000
    End If

    Me.ef_greater_than.Value = _prize_greater_than

    Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.PRIZE_GREATER_THAN

    Me.cmb_report_type.Value = ENUM_REPORT_TYPE.BY_PLAYER

    Me.uc_account_sel.ShowVipClients = False

    Me.m_is_council_tax_active = WSI.Common.GeneralParam.GetDecimal("Cashier", "Promotions.RE.Tax.3.Pct") > 0

  End Sub ' SetDefaultValues  

  ' PURPOSE : Loads the totals into the Grid
  '
  '  PARAMS :
  '      - INPUT :
  '           - PrizeTotal : Decimal
  '           - Tax1Total : Decimal
  '           - Tax2Total : Decimal
  '           - Tax3Total : Decimal
  '           - PaymentTotal : Decimal
  '
  '      - OUTPUT :
  '
  Private Sub ShowTotals(ByVal PrizeTotal As Decimal, _
                         ByVal PrizeInKindTotal As Decimal, _
                         ByVal PrizeReInKindTotal As Decimal, _
                         ByVal Tax1Total As Decimal, _
                         ByVal Tax2Total As Decimal, _
                         ByVal Tax3Total As Decimal, _
                         ByVal PaymentTotal As Decimal)

    Dim _idx_row As Integer
    Dim _grid_row As PRIZE_TAXES_REPORT_GRID_ROW

    Me.Grid.AddRow()

    _grid_row = New PRIZE_TAXES_REPORT_GRID_ROW(DBNull.Value, _
                                                DBNull.Value, _
                                                DBNull.Value, _
                                                DBNull.Value, _
                                                GLB_NLS_GUI_INVOICING.GetString(205), _
                                                DBNull.Value, _
                                                PrizeTotal, _
                                                PrizeInKindTotal, _
                                                PrizeReInKindTotal, _
                                                Tax1Total, _
                                                Tax2Total,
                                                Tax3Total,
                                                PaymentTotal, _
                                                False, _
                                                DBNull.Value)

    _idx_row = Me.Grid.NumRows - 1

    SetupRow(_idx_row, _grid_row)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub
  ' PURPOSE : Loads the accounts totals into the Grid
  '
  '  PARAMS :
  '      - INPUT :
  '           - AccountDR : Account DataRow
  '
  '      - OUTPUT :
  '
  ' NOTES :
  '      - Columns names match the XML fields exported to the XML requiered by SAT
  Private Sub ShowAccountTotals(ByVal AccountDR As DataRow)

    Dim _idx_row As Integer
    Dim _account_id As Int64
    Dim _grid_row As PRIZE_TAXES_REPORT_GRID_ROW
    Dim _payment As Decimal
    Dim _prize As Decimal
    Dim _prize_in_kind As Decimal
    Dim _prize_re_in_kind As Decimal
    Dim _tax1 As Decimal
    Dim _tax2 As Decimal
    Dim _tax3 As Decimal
    Dim _evidences_only As Boolean

    _account_id = AccountDR("NoCuenta")

    If AccountDR IsNot Nothing Then

      If IsDBNull(AccountDR.Item("TotalPremio")) Then
        _prize = 0
      Else
        _prize = AccountDR.Item("TotalPremio")
      End If

      _evidences_only = (Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES _
                               Or Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES_GREATER_THAN)

      '' Only Not in Evidence mode..
      '' Prize In Kind isn't redeemable and not generates evidence.
      If _evidences_only Or IsDBNull(AccountDR.Item("TotalPremioEspecie")) Then
        _prize_in_kind = 0
      Else
        _prize_in_kind = AccountDR.Item("TotalPremioEspecie")
      End If

      If _evidences_only Or IsDBNull(AccountDR.Item("TotalPremioEspecieRe")) Then
        _prize_re_in_kind = 0
      Else
        _prize_re_in_kind = AccountDR.Item("TotalPremioEspecieRe")
      End If

      If IsDBNull(AccountDR.Item("TotalISR1")) Then
        _tax1 = 0
      Else
        _tax1 = AccountDR.Item("TotalISR1")
      End If

      If IsDBNull(AccountDR.Item("TotalISR2")) Then
        _tax2 = 0
      Else
        _tax2 = AccountDR.Item("TotalISR2")
      End If

      If IsDBNull(AccountDR.Item("TotalISR3")) Then
        _tax3 = 0
      Else
        _tax3 = AccountDR.Item("TotalISR3")
      End If

      If _prize > 0 Then
        _payment = _prize - _tax1 - _tax2 - _tax3
      Else
        _payment = 0
      End If

      _grid_row = New PRIZE_TAXES_REPORT_GRID_ROW(DBNull.Value, _
                                                  _account_id, _
                                                  AccountDR.Item("RFC"), _
                                                  AccountDR.Item("CURP"), _
                                                  AccountDR.Item("Nombre"), _
                                                  DBNull.Value, _
                                                  _prize, _
                                                  _prize_in_kind, _
                                                  _prize_re_in_kind, _
                                                  _tax1, _
                                                  _tax2, _
                                                  _tax3, _
                                                  _payment, _
                                                  False, _
                                                  DBNull.Value)

      Me.Grid.AddRow()

      _idx_row = Me.Grid.NumRows - 1

      SetupRow(_idx_row, _grid_row)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      Me.Grid.Counter(COUNTER_ACCOUNT_TOTALS).Value += 1
    End If
  End Sub

  ' PURPOSE : Called after fill grid.
  '           If no data will hide 'Prize in kind' Column.
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Call MyBase.GUI_AfterLastRow()

    If Not m_hide_prize_in_kind_column Or m_prize_in_kind_has_values Then
      Me.Grid.Column(GRID_COL_GROSS_PRIZE_IN_KIND).Width = 1800
    Else
      Me.Grid.Column(GRID_COL_GROSS_PRIZE_IN_KIND).Width = 0
    End If

    If Not m_hide_prize_in_kind_column Or m_prize_re_in_kind_has_values Then
      Me.Grid.Column(GRID_COL_GROSS_PRIZE_RE_IN_KIND).Width = 1900
    Else
      Me.Grid.Column(GRID_COL_GROSS_PRIZE_RE_IN_KIND).Width = 0
    End If

    m_prize_in_kind_has_values = False
    m_prize_re_in_kind_has_values = False

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Call before fill grid
  '           Reset prize in kind has value boolean.
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  ' 
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call MyBase.GUI_BeforeFirstRow()
    m_prize_in_kind_has_values = False
    m_prize_re_in_kind_has_values = False
  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  ' 
  ' NOTES :
  '     -  It differs from overridiable one on frm_base_sel

  Private Function SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As PRIZE_TAXES_REPORT_GRID_ROW) As Boolean

    If Not IsDBNull(DbRow.m_oeperation_id) Then
      Me.Grid.Cell(RowIndex, GRID_COL_OPERATION_ID).Value = DbRow.m_oeperation_id
    End If

    If Not IsDBNull(DbRow.m_account_id) Then
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.m_account_id
    End If

    If Not IsDBNull(DbRow.m_RFC) Then
      Me.Grid.Cell(RowIndex, GRID_COL_RFC).Value = DbRow.m_RFC
    End If

    If Not IsDBNull(DbRow.m_CURP) Then
      Me.Grid.Cell(RowIndex, GRID_COL_CURP).Value = DbRow.m_CURP
    End If

    If Not IsDBNull(DbRow.m_holder_name) Then
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_HOLDER_NAME).Value = DbRow.m_holder_name
    End If

    If Not IsDBNull(DbRow.m_prize_date) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DATE).Value = GUI_FormatDate(DbRow.m_prize_date, _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    Me.Grid.Cell(RowIndex, GRID_COL_GROSS_PRIZE).Value = GUI_FormatCurrency(DbRow.m_prize)
    Me.Grid.Cell(RowIndex, GRID_COL_TOTAL_TAXES).Value = GUI_FormatCurrency(DbRow.m_prize)

    If DbRow.m_prize_in_kind > 0 Then
      m_prize_in_kind_has_values = True
    End If

    Me.Grid.Cell(RowIndex, GRID_COL_GROSS_PRIZE_IN_KIND).Value = String.Format("{0} {1}" _
                                                                              , GUI_FormatNumber(DbRow.m_prize_in_kind, 2) _
                                                                              , GeneralParam.GetString("CashDesk.Draw", "Voucher.UNRLabel"))

    Me.Grid.Cell(RowIndex, GRID_COL_GROSS_PRIZE_RE_IN_KIND).Value = String.Format("{0} {1}" _
                                                                             , GUI_FormatNumber(DbRow.m_prize_re_in_kind, 2) _
                                                                             , GeneralParam.GetString("CashDesk.Draw", "Voucher.UNRLabel"))

    Me.Grid.Cell(RowIndex, GRID_COL_TAX1).Value = GUI_FormatCurrency(DbRow.m_tax1)
    Me.Grid.Cell(RowIndex, GRID_COL_TAX2).Value = GUI_FormatCurrency(DbRow.m_tax2)
    If DbRow.m_tax3 > 0 Then
      Me.Grid.Column(GRID_COL_TAX3).Fixed = False
      Me.Grid.Column(GRID_COL_TAX3).Width = 1750
    End If
    Me.Grid.Cell(RowIndex, GRID_COL_TAX3).Value = GUI_FormatCurrency(DbRow.m_tax3)

    Me.Grid.Cell(RowIndex, GRID_COL_TOTAL_TAXES).Value = GUI_FormatCurrency(DbRow.m_tax1 + DbRow.m_tax2 + DbRow.m_tax3)
    Me.Grid.Cell(RowIndex, GRID_COL_TOTAL_PAYMENT).Value = GUI_FormatCurrency(DbRow.m_payment)

    If DbRow.m_evidence Then
      Me.Grid.Cell(RowIndex, GRID_COL_EVIDENCE).Value = m_evidence_flag_value
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_EVIDENCE).Value = ""
    End If

    If Not IsDBNull(DbRow.m_document_id) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DOCUMENT_ID).Value = DbRow.m_document_id
    End If

    Return True

  End Function ' GUI_SetupRow

#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    m_evidence_flag_value = GLB_NLS_GUI_INVOICING.GetString(439)
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  Private Sub cmb_prize_filter_ValueChangedEvent() Handles cmb_prize_filter.ValueChangedEvent
    If Me.cmb_prize_filter.Value = ENUM_PRIZE_FILTER.ONLY_EVIDENCES Then
      Me.ef_greater_than.Enabled = False
    Else
      Me.ef_greater_than.Enabled = True
    End If
  End Sub

  Private Sub ef_greater_than_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_greater_than.Validated

    'if the grater than entry field is empty, let's set at 0
    If Me.ef_greater_than.Value = "" Then
      Me.ef_greater_than.Value = "0"
    End If

  End Sub

#End Region ' Events

End Class
