'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_meters
' DESCRIPTION:   
' AUTHOR:        Humberto Braojos 
' CREATION DATE: 18-SEP-2014
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 18-AUG-2014 HBB    Initial Version
' 22-AUG-2014 DRV    Added auditor, and changed the way that data is saved
' 01-OCT-2014 SMN    Show a message when data is updated
' 07-OCT-2014 DRV    Changed the way that counter modifications are saved
' 08-OCT-2014 JBC    New feature WIG-1423: Apply filter search for changes and ask for them
' 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
' 18-JUL-2017 MS     WIGOS-3737: Cash Cage meter is updating all data for the same concept at the same time
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Text
Imports WSI.Common.Misc
Imports WSI.Common

Public Class frm_cage_meters
  Inherits frm_base_print

#Region "Members"
  Dim m_meters_table As DataTable
  Dim m_current_meters_table() As DataRow
#End Region

#Region "Constants"
  Dim GRID_COLUMNS As Int16 = 6
  Dim GRID_HEADERS As Int16 = 1

  Const SQL_COLUMN_SOURCE_TARGET_NAME As Int16 = 0
  Const SQL_COLUMN_SOURCE_TARGET_ID As Int16 = 1
  Const SQL_COLUMN_CONCEPT_NAME As Int16 = 2
  Const SQL_COLUMN_CONCEPT_ID As Int16 = 3
  Const SQL_COLUMN_ISO_CODE As Int16 = 4
  Const SQL_COLUMN_CURRENCY_TYPE As Int16 = 5
  Const SQL_COLUMN_VALUE As Int16 = 6

  Const GRID_COLUMN_SOURCE_TARGET_ID As Int16 = 0
  Const GRID_COLUMN_CONCEPT_ID As Int16 = 1
  Const GRID_COLUMN_SOURCE_TARGET_NAME As Int16 = 2
  Const GRID_COLUMN_CONCEPT_NAME As Int16 = 3
  Const GRID_COLUMN_CURRENCY As Int16 = 4
  Const GRID_COLUMN_VALUE As Int16 = 5

  Const WIDTH_COLUMN_SOURCE_TARGET_ID As Int16 = 0
  Const WIDTH_COLUMN_CONCEPT_ID As Int16 = 0
  Const WIDTH_COLUMN_SOURCE_TARGET As Int16 = 3700
  Const WIDTH_COLUMN_CONCEPT As Int16 = 2700
  Const WIDTH_COLUMN_CURRENCY As Int16 = 3450
  Const WIDTH_COLUMN_VALUE As Int16 = 2000


  'origin/destination Grid Width
  Private Const GRID_SOURCE_WIDTH_COLUMN_DESCRIPTION As Int32 = 3300
  Private Const GRID_SOURCE_WIDTH_COLUMN_CHECKED As Int32 = 300

  Private Const GRID_SOURCE_HEADER_ROWS As Int32 = 0
  Private Const GRID_SOURCE_COLUMNS As Int32 = 3
  Private Const GRID_SOURCE_COLUMN_CHECKED As Int32 = 0
  Private Const GRID_SOURCE_COLUMN_ID As Int32 = 1
  Private Const GRID_SOURCE_COLUMN_DESCRIPTION As Int32 = 2

  Private Const SQL_SOURCE_COLUMN_ID As Int32 = 0
  Private Const SQL_SOURCE_DESCRIPTION As Int32 = 1

  'concepts Grid Width
  Private Const GRID_CONCEPTS_WIDTH_COLUMN_DESCRIPTION As Int32 = 3300
  Private Const GRID_CONCEPTS_WIDTH_COLUMN_CHECKED As Int32 = 300

  Private Const GRID_CONCEPTS_HEADER_ROWS As Int32 = 0
  Private Const GRID_CONCEPTS_COLUMNS As Int32 = 3
  Private Const GRID_CONCEPTS_COLUMN_CHECKED As Int32 = 0
  Private Const GRID_CONCEPTS_COLUMN_ID As Int32 = 1
  Private Const GRID_CONCEPTS_DESCRIPTION As Int32 = 2

  Private Const SQL_CONCEPTS_COLUMN_ID As Int32 = 0
  Private Const SQL_CONCEPTS_DESCRIPTION As Int32 = 1

#End Region

#Region "Overrides"
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_METERS
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '           - None
  '    - OUTPUT:
  '           - None
  ' RETURNS:
  '           - None
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5478)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_CONTROLS.GetString(1)      'OK
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13) 'Save
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    bt_check_all_concepts.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    bt_uncheck_all_concepts.Text = GLB_NLS_GUI_ALARMS.GetString(453)
    bt_check_all_sources.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    bt_uncheck_all_sources.Text = GLB_NLS_GUI_ALARMS.GetString(453)
    gb_origins_destinations.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5483)
    gb_concepts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5484)

    Call GUI_StyleSheetSourceGrid()
    Call GUI_StyleSheetConceptGrid()
    Call FillTargetGrid()
    Call FillConceptGrid()
    Call GUI_StyleSheet()
    Call GetMetersTable()
    m_current_meters_table = m_meters_table.Select() 'we will work allways with this array
    Call FillMetersGrid(m_meters_table)
  End Sub

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY

        If HasChanges() AndAlso NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, _
           ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Return
        Else
          m_current_meters_table = ApplyFilters()
          Call FillMetersGrid(m_current_meters_table)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call SaveChanges()
      Case ENUM_BUTTON.BUTTON_NEW
        Call SaveChanges()
        Call GUI_Exit()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call UnCheckAllTargets()
    Call UnCkeckAllConcepts()
  End Sub

  ' PURPOSE: Handle Form Key events
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: True / False 
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_cage_meters.ContainsFocus Then
      Return Me.dg_cage_meters.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If HasChanges() Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(156), _
                                ENUM_MB_TYPE.MB_TYPE_INFO, _
                                ENUM_MB_BTN.MB_BTN_YES_NO, _
                                ENUM_MB_DEF_BTN.MB_DEF_BTN_1) = ENUM_MB_RESULT.MB_RESULT_NO Then
        CloseCanceled = True
      End If
    End If
    MyBase.GUI_Closing(CloseCanceled)
  End Sub
#End Region

#Region "Public"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit
#End Region

#Region "Private"

  ' PURPOSE: Define all Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.dg_cage_meters

      .Clear()
      .Init(GRID_COLUMNS, GRID_HEADERS, True)
      .Sortable = False

      .Column(GRID_COLUMN_SOURCE_TARGET_ID).Width = WIDTH_COLUMN_SOURCE_TARGET_ID

      .Column(GRID_COLUMN_CONCEPT_ID).Width = WIDTH_COLUMN_CONCEPT_ID

      .Column(GRID_COLUMN_SOURCE_TARGET_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5479)
      .Column(GRID_COLUMN_SOURCE_TARGET_NAME).Width = WIDTH_COLUMN_SOURCE_TARGET
      .Column(GRID_COLUMN_SOURCE_TARGET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_CONCEPT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5480)
      .Column(GRID_COLUMN_CONCEPT_NAME).Width = WIDTH_COLUMN_CONCEPT
      .Column(GRID_COLUMN_CONCEPT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_CURRENCY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5481)
      .Column(GRID_COLUMN_CURRENCY).Width = WIDTH_COLUMN_CURRENCY
      .Column(GRID_COLUMN_CURRENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_VALUE).Editable = True
      .Column(GRID_COLUMN_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5482)
      .Column(GRID_COLUMN_VALUE).Width = WIDTH_COLUMN_VALUE
      .Column(GRID_COLUMN_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_VALUE).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_VALUE).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_VALUE).EditionControl.EntryField.SetFilter(FORMAT_NUMBER_ALSO_NEGATIVES, 20, 2)

    End With

  End Sub

  ' PURPOSE: Define all Source Grid Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetSourceGrid()

    dg_source_target.PanelRightVisible = False
    dg_source_target.Sortable = False
    dg_source_target.TopRow = -2

    With Me.dg_source_target
      Call .Init(GRID_SOURCE_COLUMNS, GRID_SOURCE_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' GRID_COL_CHECKBOX
      .Column(GRID_SOURCE_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_SOURCE_COLUMN_CHECKED).WidthFixed = GRID_SOURCE_WIDTH_COLUMN_CHECKED
      .Column(GRID_SOURCE_COLUMN_CHECKED).Fixed = True
      .Column(GRID_SOURCE_COLUMN_CHECKED).Editable = True
      .Column(GRID_SOURCE_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID COL ID
      .Column(GRID_SOURCE_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_DESCRIPTION
      .Column(GRID_SOURCE_COLUMN_DESCRIPTION).WidthFixed = GRID_SOURCE_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_SOURCE_COLUMN_DESCRIPTION).Fixed = True
      .Column(GRID_SOURCE_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Define all concept Grid Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetConceptGrid()

    dg_concepts.PanelRightVisible = False
    dg_concepts.Sortable = False
    dg_concepts.TopRow = -2

    With Me.dg_concepts
      Call .Init(GRID_CONCEPTS_COLUMNS, GRID_CONCEPTS_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' GRID_COL_CHECKBOX
      .Column(GRID_CONCEPTS_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_CONCEPTS_COLUMN_CHECKED).WidthFixed = GRID_CONCEPTS_WIDTH_COLUMN_CHECKED
      .Column(GRID_CONCEPTS_COLUMN_CHECKED).Fixed = True
      .Column(GRID_CONCEPTS_COLUMN_CHECKED).Editable = True
      .Column(GRID_CONCEPTS_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID COL ID
      .Column(GRID_CONCEPTS_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_DESCRIPTION
      .Column(GRID_CONCEPTS_DESCRIPTION).WidthFixed = GRID_CONCEPTS_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_CONCEPTS_DESCRIPTION).Fixed = True
      .Column(GRID_CONCEPTS_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Read Data base data and sets to Target grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTargetGrid()

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sb.AppendLine("  SELECT   CST_SOURCE_TARGET_ID   ")
        _sb.AppendLine("         , CST_SOURCE_TARGET_NAME ")
        _sb.AppendLine("    FROM   CAGE_SOURCE_TARGET     ")
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
          Call GUI_SetupRowTargetGrid(_table)
        End Using
      End Using
    Catch ex As Exception
    End Try

  End Sub

  ' PURPOSE: Fill the grid with the Target data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetupRowTargetGrid(ByVal Table As DataTable)

    Dim _actual_row As Int32

    _actual_row = 0

    For Each r As DataRow In Table.Rows
      dg_source_target.AddRow()
      Me.dg_source_target.Cell(_actual_row, GRID_SOURCE_COLUMN_ID).Value = r(SQL_SOURCE_COLUMN_ID)
      Me.dg_source_target.Cell(_actual_row, GRID_SOURCE_COLUMN_DESCRIPTION).Value = r(SQL_SOURCE_DESCRIPTION)
      _actual_row += 1
    Next

  End Sub

  ' PURPOSE: Read Data base data and sets to concept grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillConceptGrid()

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sb.AppendLine("  SELECT   CC_CONCEPT_ID   ")
        _sb.AppendLine("         , CC_NAME         ")
        _sb.AppendLine("    FROM   CAGE_CONCEPTS   ")
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
          Call GUI_SetupRowsConcepts(_table)
        End Using
      End Using
    Catch ex As Exception
    End Try

  End Sub

  ' PURPOSE: Fill the grid with the concepts
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetupRowsConcepts(ByVal Table As DataTable)

    Dim _actual_row As Int32

    _actual_row = 0

    For Each r As DataRow In Table.Rows
      Me.dg_concepts.AddRow()
      Me.dg_concepts.Cell(_actual_row, GRID_CONCEPTS_COLUMN_ID).Value = r(SQL_CONCEPTS_COLUMN_ID)
      Me.dg_concepts.Cell(_actual_row, GRID_CONCEPTS_DESCRIPTION).Value = r(SQL_CONCEPTS_DESCRIPTION)
      _actual_row += 1
    Next

  End Sub

  ' PURPOSE: Read DB editable data 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetMetersTable()

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter

    m_meters_table = New DataTable()
    _adapter = New SqlDataAdapter()
    _sb = New StringBuilder()

    _sb.AppendLine("  DECLARE @Currencies AS NVARCHAR(200)                                                                                              ")

    _sb.AppendLine("   SELECT @Currencies = GP_KEY_VALUE ")
    _sb.AppendLine("     FROM GENERAL_PARAMS                                                                                                            ")
    _sb.AppendLine("    WHERE GP_GROUP_KEY = 'RegionalOptions'                                                                                          ")
    _sb.AppendLine("      AND GP_SUBJECT_KEY = 'CurrenciesAccepted'                                                                                     ")

    _sb.AppendLine("   SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)           ")

    _sb.AppendLine("   SELECT CST_SOURCE_TARGET_NAME                                                                                                    ")
    _sb.AppendLine("        , CM_SOURCE_TARGET_ID                                                                                                       ")
    _sb.AppendLine("        , CC_NAME                                                                                                                   ")
    _sb.AppendLine("        , CM_CONCEPT_ID                                                                                                             ")
    _sb.AppendLine("        , CM_ISO_CODE                                                                                                               ")
    _sb.AppendLine("        , CM_CAGE_CURRENCY_TYPE                                                                                                     ")
    _sb.AppendLine("        , CM_VALUE                                                                                                                  ")
    _sb.AppendLine("        , CSTC_TYPE                                                                                                                 ")
    _sb.AppendLine("     FROM CAGE_SOURCE_TARGET                                                                                                        ")
    _sb.AppendLine("          INNER JOIN CAGE_METERS ON CST_SOURCE_TARGET_ID = CM_SOURCE_TARGET_ID                                                      ")
    _sb.AppendLine("          INNER JOIN CAGE_CONCEPTS ON CM_CONCEPT_ID = CC_CONCEPT_ID                                                                 ")
    _sb.AppendLine("          LEFT JOIN CAGE_SOURCE_TARGET_CONCEPTS ON CM_CONCEPT_ID = CSTC_CONCEPT_ID AND CM_SOURCE_TARGET_ID = CSTC_SOURCE_TARGET_ID  ")
    _sb.AppendLine("    WHERE CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)                                                                  ")
    _sb.AppendLine(" ORDER BY CM_SOURCE_TARGET_ID, CM_CONCEPT_ID, CM_ISO_CODE, CM_CAGE_CURRENCY_TYPE                                                    ")

    _sb.AppendLine(" DROP TABLE #TMP_CURRENCIES_ISO_CODES                                                                                               ")

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(m_meters_table)
        End Using
      End Using
    Catch ex As Exception
    End Try
  End Sub

  ' PURPOSE: Fill the grid with the meters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillMetersGrid(ByVal Meters As DataTable)

    Dim _actual_row As Int32

    _actual_row = 0
    Me.dg_cage_meters.Clear()

    For Each r As DataRow In Meters.Rows
      GUI_SetupRow(r, _actual_row)
      _actual_row += 1
    Next

  End Sub

  Private Sub FillMetersGrid(ByVal Meters As DataRow())

    Dim _actual_row As Int32

    _actual_row = 0
    Me.dg_cage_meters.Clear()

    For Each r As DataRow In Meters
      GUI_SetupRow(r, _actual_row)
      _actual_row += 1
    Next

  End Sub

  ' PURPOSE: Sets each row data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub GUI_SetupRow(ByVal Row As DataRow, ByVal RowNumber As Int32)

    Dim _currency_iso_type As CurrencyIsoType
    Dim _currency_to_show As String

    Me.dg_cage_meters.AddRow()
    Me.dg_cage_meters.Cell(RowNumber, GRID_COLUMN_SOURCE_TARGET_ID).Value = Row(SQL_COLUMN_SOURCE_TARGET_ID)
    Me.dg_cage_meters.Cell(RowNumber, GRID_COLUMN_CONCEPT_ID).Value = Row(SQL_COLUMN_CONCEPT_ID)
    Me.dg_cage_meters.Cell(RowNumber, GRID_COLUMN_SOURCE_TARGET_NAME).Value = Row(SQL_COLUMN_SOURCE_TARGET_NAME)
    Me.dg_cage_meters.Cell(RowNumber, GRID_COLUMN_CONCEPT_NAME).Value = Row(SQL_COLUMN_CONCEPT_NAME)

    _currency_iso_type = New CurrencyIsoType()
    _currency_iso_type.IsoCode = Row(SQL_COLUMN_ISO_CODE)
    _currency_iso_type.Type = Row(SQL_COLUMN_CURRENCY_TYPE)
    _currency_to_show = _currency_iso_type.IsoCode

    If FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type) Then
      _currency_to_show = FeatureChips.GetChipTypeDescription(_currency_iso_type)
    ElseIf _currency_iso_type.IsoCode = Cage.CHIPS_ISO_CODE Then
      _currency_to_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)
    Else
      Select Case _currency_iso_type.Type
        Case CageCurrencyType.Bill
          _currency_to_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5660) + "(" + _currency_to_show + ") "

        Case CageCurrencyType.Coin
          _currency_to_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5661) + "(" + _currency_to_show + ") "

        Case Else
          _currency_to_show = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6292) + "(" + _currency_to_show + ") "

      End Select
    End If

    Me.dg_cage_meters.Cell(RowNumber, GRID_COLUMN_CURRENCY).Value = _currency_to_show
    Me.dg_cage_meters.Cell(RowNumber, GRID_COLUMN_VALUE).Value = GUI_FormatCurrency(Row(SQL_COLUMN_VALUE), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False, Row(SQL_COLUMN_ISO_CODE))

  End Sub

  ' PURPOSE: Gets the selected targets IDs separated with ","
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSourceTargetSelected() As String

    Dim _idx_row As Integer
    Dim _all As Boolean
    Dim _at_least_one As Boolean
    Dim _target_list As String

    _target_list = ""
    _all = True
    _at_least_one = False

    For _idx_row = 0 To Me.dg_source_target.NumRows - 1
      If dg_source_target.Cell(_idx_row, GRID_SOURCE_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _target_list.Length = 0 Then
          _target_list = dg_source_target.Cell(_idx_row, GRID_SOURCE_COLUMN_ID).Value
        Else
          _target_list = _target_list & ", " & dg_source_target.Cell(_idx_row, GRID_SOURCE_COLUMN_ID).Value
        End If
        _at_least_one = True
      Else
        _all = False
      End If

    Next

    Return _target_list

  End Function ' 

  ' PURPOSE: Gets the selected concept IDs separated with ","
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetConceptsSelected() As String

    Dim _idx_row As Integer
    Dim _all As Boolean
    Dim _at_least_one As Boolean
    Dim _concepts_list As String

    _concepts_list = ""
    _all = True
    _at_least_one = False

    For _idx_row = 0 To Me.dg_concepts.NumRows - 1
      If dg_concepts.Cell(_idx_row, GRID_SOURCE_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _concepts_list.Length = 0 Then
          _concepts_list = dg_concepts.Cell(_idx_row, GRID_SOURCE_COLUMN_ID).Value
        Else
          _concepts_list = _concepts_list & ", " & dg_concepts.Cell(_idx_row, GRID_SOURCE_COLUMN_ID).Value
        End If
        _at_least_one = True
      Else
        _all = False
      End If

    Next

    Return _concepts_list

  End Function ' 

  ' PURPOSE: Gets the filtered roews
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function ApplyFilters() As DataRow()
    Dim _concepts As String
    Dim _targets As String
    Dim _where As String

    _concepts = ""
    _targets = ""
    _where = ""

    _concepts = GetConceptsSelected()
    _targets = GetSourceTargetSelected()

    'reset the changes since it the datatable was loaded or AcceptChangeS()
    m_meters_table.RejectChanges()
    Call GetMetersTable()

    If _concepts.Length > 0 Then
      _where = "CM_CONCEPT_ID IN (" & _concepts & ") "
      If _targets.Length > 0 Then
        _where = _where & "AND CM_SOURCE_TARGET_ID IN (" & _targets & ")"
      End If
    Else
      If _targets.Length > 0 Then
        _where = "CM_SOURCE_TARGET_ID IN (" & _targets & ")"
      End If
    End If

    Return m_meters_table.Select(_where)
  End Function

  ' PURPOSE: Update changes
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - true/false
  Private Function UpdateMeters() As Boolean

    Dim _sb As StringBuilder
    Dim _affected_rows As Int32

    _sb = New StringBuilder()

    _sb.AppendLine("     UPDATE   CAGE_METERS                                                      ")
    _sb.AppendLine("        SET   CM_VALUE            = @pvalue                                    ")
    _sb.AppendLine("            , CM_VALUE_IN = CASE WHEN @pvalue = 0                              ")
    _sb.AppendLine("									             THEN 0                                          ")
    _sb.AppendLine("		           							   ELSE                                            ")
    _sb.AppendLine("						           			     CASE WHEN @pType = 0                          ")
    _sb.AppendLine("  	  				           			      THEN @pvalue                               ")
    _sb.AppendLine("						           			     WHEN @pType = 1                               ")
    _sb.AppendLine("  	  				           			      THEN CM_VALUE_IN                           ")
    _sb.AppendLine("  	  					           		   ELSE                                          ")
    _sb.AppendLine("  	  				           			     CASE WHEN @pvalue > @pOldValue              ")
    _sb.AppendLine("  	  							                 THEN CM_VALUE_IN + @pvalue - @pOldValue   ")
    _sb.AppendLine("  	  							                 ELSE CM_VALUE_IN                          ")
    _sb.AppendLine("  	  							               END                                         ")
    _sb.AppendLine("  	  							             END                                           ")
    _sb.AppendLine("  	  							        END                                                ")
    _sb.AppendLine("            , CM_VALUE_OUT = CASE WHEN @pvalue = 0                             ")
    _sb.AppendLine("									              THEN 0                                         ")
    _sb.AppendLine("									              ELSE                                           ")
    _sb.AppendLine("										              CASE WHEN @pType = 0                         ")
    _sb.AppendLine("  	  								              THEN CM_VALUE_OUT                          ")
    _sb.AppendLine("										              WHEN @pType = 1                              ")
    _sb.AppendLine("  	  								              THEN (@pvalue * -1)                        ")
    _sb.AppendLine("  	  								            ELSE                                         ")
    _sb.AppendLine("  	  								  	          CASE WHEN @pvalue < @pOldValue             ")
    _sb.AppendLine("  	  								  	      	    THEN CM_VALUE_OUT + @pOldValue - @pvalue ")
    _sb.AppendLine("  	  								  	      	    ELSE CM_VALUE_OUT                        ")
    _sb.AppendLine("  	  							                END                                        ")
    _sb.AppendLine("  	  							              END                                          ")
    _sb.AppendLine("  	  							         END                                               ")
    _sb.AppendLine("      WHERE   CM_SOURCE_TARGET_ID   = @pSourceTargetId                         ")
    _sb.AppendLine("        AND   CM_CONCEPT_ID         = @pConceptId                              ")
    _sb.AppendLine("        AND   CM_ISO_CODE           = @pISOCode                                ")
    _sb.AppendLine("        AND   CM_CAGE_CURRENCY_TYPE = @pCageCurrencyType                       ")

    Using _db_trx As DB_TRX = New DB_TRX()
      Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pvalue", SqlDbType.Decimal).SourceColumn = "CM_VALUE"
        _cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).SourceColumn = "CM_SOURCE_TARGET_ID"
        _cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).SourceColumn = "CM_CONCEPT_ID"
        _cmd.Parameters.Add("@pISOCode", SqlDbType.VarChar).SourceColumn = "CM_ISO_CODE"
        _cmd.Parameters.Add("@pOldValue", SqlDbType.Decimal).SourceColumn = "CM_VALUE"
        _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "CSTC_TYPE"
        _cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).SourceColumn = "CM_CAGE_CURRENCY_TYPE"
        _cmd.Parameters("@pOldValue").SourceVersion = DataRowVersion.Original
        _cmd.UpdatedRowSource = UpdateRowSource.None

        Using _sql_da As SqlDataAdapter = New SqlDataAdapter()
          _sql_da.UpdateCommand = _cmd
          _sql_da.DeleteCommand = Nothing
          _sql_da.SelectCommand = Nothing
          _sql_da.InsertCommand = Nothing
          _sql_da.UpdateBatchSize = 500

          Try
            _affected_rows = _sql_da.Update(m_meters_table)
            _db_trx.Commit()
            m_meters_table.AcceptChanges()

          Catch _ex As Exception
            Log.Exception(_ex)

            Return False
          End Try

        End Using
      End Using
    End Using

    Return True

  End Function

  ' PURPOSE: Audits changes
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function AuditChanges() As Boolean
    Dim _old_value As CLASS_AUDITOR_DATA
    Dim _new_value As CLASS_AUDITOR_DATA
    Dim _sb As StringBuilder

    _old_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
    _new_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_CAGE)
    _sb = New StringBuilder()

    For Each row As DataRow In m_current_meters_table
      CreateAudit(row, DataRowVersion.Original, _old_value)
      CreateAudit(row, DataRowVersion.Current, _new_value)
    Next


    'making the text of audit
    _sb.Append(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5478))

    _old_value.SetName(0, "", _sb.ToString())
    _new_value.SetName(0, "", _sb.ToString())

    If Not _new_value.Notify(GLB_CurrentUser.GuiId, _
                                 GLB_CurrentUser.Id, _
                                 GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                 0, _
                                 _old_value) Then


      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Sets auditor value
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CreateAudit(ByVal Row As DataRow, ByVal RowVersion As DataRowVersion, ByVal AuditorData As CLASS_AUDITOR_DATA)

    Dim _exchange As CurrencyExchangeProperties
    _exchange = CurrencyExchangeProperties.GetProperties(Row(SQL_COLUMN_ISO_CODE))

    AuditorData.SetField(0, _exchange.FormatCurrency(Row.Item(SQL_COLUMN_VALUE, RowVersion)), _
                         Row.Item(SQL_COLUMN_SOURCE_TARGET_NAME, RowVersion) & "-" & Row.Item(SQL_COLUMN_CONCEPT_NAME, RowVersion) & "-" & Row.Item(SQL_COLUMN_ISO_CODE, RowVersion))

  End Sub


  ' PURPOSE: Audit changes and update db meters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SaveChanges()
    Dim _rows_affected() As DataRow

    _rows_affected = m_meters_table.Select("", "", DataViewRowState.ModifiedCurrent)

    If _rows_affected.Length = 0 Then

      Return
    End If

    Call AuditChanges()
    If Not UpdateMeters() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853), _
                      ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

      Return
    End If

    Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), _
                    ENUM_MB_TYPE.MB_TYPE_INFO, _
                    ENUM_MB_BTN.MB_BTN_OK, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

  End Sub

  ' PURPOSE: Checks if there are changes
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function HasChanges() As Boolean
    Dim _modified_rows() As DataRow

    _modified_rows = m_meters_table.Select("", "", DataViewRowState.ModifiedCurrent)

    If _modified_rows.Length > 0 Then
      Return True
    Else
      Return False
    End If

  End Function
#End Region

#Region "Events"

  Private Sub bt_check_all_origins_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_check_all_sources.Click
    Dim idx_rows As Integer

    For idx_rows = 0 To dg_source_target.NumRows - 1
      dg_source_target.Cell(idx_rows, GRID_SOURCE_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next

  End Sub

  Private Sub bt_uncheck_all_origins_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_uncheck_all_sources.Click
    Call UnCheckAllTargets()
  End Sub

  Private Sub bt_check_all_concepts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_check_all_concepts.Click
    Dim idx_rows As Integer

    For idx_rows = 0 To dg_concepts.NumRows - 1
      dg_concepts.Cell(idx_rows, GRID_CONCEPTS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next

  End Sub

  Private Sub bt_uncheck_all_concepts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_uncheck_all_concepts.Click
    Call UnCkeckAllConcepts()
  End Sub

  Private Sub UnCkeckAllConcepts()

    Dim idx_rows As Integer

    For idx_rows = 0 To dg_concepts.NumRows - 1
      dg_concepts.Cell(idx_rows, GRID_CONCEPTS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub

  Private Sub UnCheckAllTargets()

    Dim idx_rows As Integer

    For idx_rows = 0 To dg_source_target.NumRows - 1
      dg_source_target.Cell(idx_rows, GRID_SOURCE_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub

  Private Sub uc_grid_games_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_cage_meters.CellDataChangedEvent

    Dim _new_value As Decimal
    If Not String.IsNullOrEmpty(dg_cage_meters.Cell(Row, Column).Value) Then
      _new_value = CDec(dg_cage_meters.Cell(Row, Column).Value)
    Else
      _new_value = 0.0
    End If

    If _new_value <> m_current_meters_table(Row).Item(SQL_COLUMN_VALUE) Then
    m_current_meters_table(Row).Item(SQL_COLUMN_VALUE) = _new_value
    End If

    dg_cage_meters.Cell(Row, Column).Value = _new_value

  End Sub

#End Region


End Class