'------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'------------------------------------------------------------------------
'
' MODULE NAME:   frm_cashdesk_draws_configuration_sel_edit
' DESCRIPTION:   Cash Desk Draws configuration parameters edition
' AUTHOR:        Marcos Mohedano
' CREATION DATE: 30-JUL-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------------
' 30-JUL-2013 MMG    Initial version
'-------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.IO
Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common
'Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
'Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_cashdesk_draws_configuration_sel_edit
  Inherits GUI_Controls.frm_base_sel_edit

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_CDC_GROUP_KEY As Integer = 0
  Private Const SQL_COLUMN_CDC_SUBJECT_KEY As Integer = 1
  Private Const SQL_COLUMN_CDC_KEY_VALUE As Integer = 2
  'Private Const SQL_COLUMN_CDC_TIMESTAMP As Integer = 3

  ' Grid Columns
  Private Const GRID_COLUMN_CDC_GROUP_KEY As Integer = 0
  Private Const GRID_COLUMN_CDC_SUBJECT_KEY As Integer = 1
  Private Const GRID_COLUMN_CDC_KEY_VALUE As Integer = 2
  'Private Const GRID_COLUMN_CDC_TIMESTAMP As Integer = 3

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Columns Width
  Private Const GRID_WIDTH_CDC_GROUP_KEY As Integer = 1800
  Private Const GRID_WIDTH_CDC_SUBJECT_KEY As Integer = 2800
  Private Const GRID_WIDTH_CDC_KEY_VALUE As Integer = 1800
  'Private Const GRID_WIDTH_CDC_TIMESTAMP As Integer = 1100

#End Region ' Constants

#Region " Members "


#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASHDESK_DRAWS_CONFIGURATION

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    'Set form name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2283)

    ' Buttons
    Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = False
    Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False

    ' Grid
    Call GUI_StyleSheet()
    Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine("SELECT ")
    _str_sql.AppendLine("   CDC_GROUP_KEY")
    _str_sql.AppendLine("   , CDC_SUBJECT_KEY")
    _str_sql.AppendLine("   , CDC_KEY_VALUE")
    _str_sql.AppendLine("FROM")
    _str_sql.AppendLine("   CASHDESK_DRAWS_CONFIGURATION")

    _str_sql.AppendLine("ORDER BY CDC_GROUP_KEY, CDC_SUBJECT_KEY")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                             ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'Group
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CDC_GROUP_KEY).Value = DbRow.Value(SQL_COLUMN_CDC_GROUP_KEY)

    'Subject
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CDC_SUBJECT_KEY).Value = DbRow.Value(SQL_COLUMN_CDC_SUBJECT_KEY)

    'Value
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CDC_KEY_VALUE).Value = DbRow.Value(SQL_COLUMN_CDC_KEY_VALUE)

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Dim _selected_row As DataRow

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT

        '_selected_row =
        Return

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid

      'Grid creation
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      'Group  
      With .Column(GRID_COLUMN_CDC_GROUP_KEY)
        .Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(61)
        .Width = GRID_WIDTH_CDC_GROUP_KEY
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT        
      End With

      'Subject 
      With .Column(GRID_COLUMN_CDC_SUBJECT_KEY)
        .Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(62)
        .Width = GRID_WIDTH_CDC_SUBJECT_KEY
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'Value
      With .Column(GRID_COLUMN_CDC_KEY_VALUE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1405)
        .Width = GRID_WIDTH_CDC_KEY_VALUE
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Editable = True
        .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      End With

    End With

    Me.Grid.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth

  End Sub ' GUI_StyleSheet

#End Region ' Private Functions

End Class