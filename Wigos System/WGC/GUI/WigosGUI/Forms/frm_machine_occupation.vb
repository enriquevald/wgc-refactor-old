'-------------------------------------------------------------------
' Copyright © 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_base
' DESCRIPTION:   Information various from the movements of account and session playing 
' AUTHOR:        Marcos Piedra
' CREATION DATE: 15-DEC-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-DEC-2011  MPO    Initial version.
' 04-JAN-2012  JMM    Interface improvement
' 01-FEB-2012  RCI & MPO    Routine TimeSpanToString() is now in module mdl_misc.vb.
' 01-FEB-2012  RCI & AJQ    Calculte ToDate correctly: using TodayOpening.
' 01-FEB-2012  MPO    Column OCCUPIED, align center
' 04-APR-2012  JCM    NLS String "Varios" changed to NLS String "Multijuego"
' 02-MAY-2012  JAB & RCI    Fixed bug #287: Check divide by zero condition.
' 03-MAY-2012  JAB    Fixed bug #290: Check the date filter
' 08-JUN-2012  JAB    DoEvents run each second.
' 12-JUN-2012  JAB    Check if disposed.
' 24-AUG-2012  JAB    Control to show huge amounts of records.
' 27-AUG-2012  MPO    Check if the terminal type is NULL. 
' 17-MAY-2013  DHA    Fixed Bug #786: Wrong formatting "WHERE" filter, due to apostrophe
' 27-MAY-2013  RBG    Fixed Bug #801: The bug #786 is not completely resolved
' 17-JUN-2013  NMR    Fixed Bug #860: Control the correct number of rows to show/draw
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 29-JAN-2015  ANM    Don't show terminal name nor id floor if terminals isn't checked (but providers is)
' 23-JUN-2015  DHA    Fixed Bug #WIG-2454: Statistics reports: Error displaying cloned terminals
' 18-SEP-2015  FAV    Fixed Bug 4528: Occupation Machine: Error in query with sql 2005 database.
' 06-NOV-2015  MPO    Bug 6268: Modified date type of the query
' 22-APR-2016  FAV    Bug 10568: Added index to improve the response time
' 19-SEP-2016  FAV    Bug 17757: Timeout exception in report
' 09-FEB-2017  AMF    Bug 23488:Ocupación de maquinas no exporta correctamente a Excel
' 10-FEB-2017  JML    Bug 24283:Reportes sin informar correctamente la denominación contable
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_machine_occupation
  Inherits frm_base_sel

#Region " Member"

  Private m_date_from As String
  Private m_date_to As String
  Private m_show_providers_str As String
  Private m_show_terminals_str As String

  Private m_show_providers As Boolean
  Private m_show_terminals As Boolean
  Private m_grid_sorted As Boolean

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

  Private m_excel_column_occupation As Integer
  Private m_excel_column_ratio As Integer

#End Region

#Region " Constants "

  'TERMINAL_ID, PROVIDER_ID, TERMINAL_NAME, GAME, OCCUPIED, CONNECTED 

  ' Index Columns of SQL
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_PROVIDER As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 3
  Private Const SQL_COLUMN_GAME_NAME As Integer = 4
  Private Const SQL_COLUMN_OCCUPIED As Integer = 5
  Private Const SQL_COLUMN_CONNECTED As Integer = 6
  Private Const SQL_COLUMN_PLAYED As Integer = 7
  Private Const SQL_COLUMN_NETWIN As Integer = 8
  Private Const SQL_COLUMN_MASTER_ID_CONNECTED As Integer = 9

  Private Const SQL_COLUMNS As Integer = 10

  Private TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

  ' Index Columns Grids
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_INIT_TERMINAL_DATA As Integer = 1
  Private GRID_COLUMN_NUM_TERMINAL As Integer = TERMINAL_DATA_COLUMNS + 1
  Private GRID_COLUMN_GAME_NAME As Integer = TERMINAL_DATA_COLUMNS + 2
  Private GRID_COLUMN_CONNECTED_DAYS As Integer = TERMINAL_DATA_COLUMNS + 3
  Private GRID_COLUMN_OCCUPIED As Integer = TERMINAL_DATA_COLUMNS + 4
  Private GRID_COLUMN_OCCUPATION As Integer = TERMINAL_DATA_COLUMNS + 5
  Private GRID_COLUMN_RATIO_PROVIDER As Integer = TERMINAL_DATA_COLUMNS + 6
  Private GRID_COLUMN_RATIO_SITE As Integer = TERMINAL_DATA_COLUMNS + 7
  Private GRID_COLUMN_PLAYED As Integer = TERMINAL_DATA_COLUMNS + 8
  Private GRID_COLUMN_NETWIN As Integer = TERMINAL_DATA_COLUMNS + 9

  ' Num columns
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private GRID_COLUMNS As Integer = TERMINAL_DATA_COLUMNS + 10

  ' Width columns
  Private Const GRID_WIDTH_PROVIDER As Integer = 2100
  Private Const GRID_WIDTH_TERMINAL_NAME As Integer = 2100
  Private Const GRID_WIDTH_GAME As Integer = 2200
  Private Const GRID_WIDTH_NUMBER As Integer = 1200
  Private Const GRID_WIDTH_TIME As Integer = 1200
  Private Const GRID_WIDTH_PERCENTAGE As Integer = 1100
  Private Const GRID_WIDTH_RATIO As Integer = 1000
  Private Const GRID_WIDTH_AMOUNT As Integer = 1250

  'Counter
  Private Const COUNTER_PROVIDER As Integer = 1

#End Region

#Region " OVERRIDES "

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck


  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MACHINE_OCCUPATION

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Machine occupation
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(444)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Me.chk_providers.Text = GLB_NLS_GUI_STATISTICS.GetString(443)
    Me.chk_terminales.Text = GLB_NLS_GUI_STATISTICS.GetString(472)
    Me.gpr_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)

    ' Caption
    Me.gb_caption.Text = GLB_NLS_GUI_STATISTICS.GetString(449)
    Me.lbl_color_total_terminals.BackColor() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    Me.lbl_color_total_providers.BackColor() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    Me.lbl_caption_total_terminals.Text = GLB_NLS_GUI_STATISTICS.GetString(300)
    Me.lbl_caption_total_providers.Text = GLB_NLS_GUI_STATISTICS.GetString(315)

    Me.gb_ratio.Text = GLB_NLS_GUI_STATISTICS.GetString(462)
    Me.lbl_025.Text = GUI_FormatNumber(0.25)
    Me.lbl_05.Text = GUI_FormatNumber(0.5)
    Me.lbl_1.Text = GUI_FormatNumber(1)
    Me.lbl_25.Text = GUI_FormatNumber(2.5)
    Me.lbl_4.Text = GUI_FormatNumber(4)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sql As New System.Text.StringBuilder
    Dim _str_several As String

    'TERMINAL_ID, PROVIDER_ID, TERMINAL_NAME, GAME, OCCUPIED, CONNECTED 

    _str_several = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634) 'Multijuego

    _sql.AppendLine(" DECLARE @_ini_date AS DATETIME ")
    _sql.AppendLine(" DECLARE @_end_date AS DATETIME ")
    _sql.AppendLine(" DECLARE @_ini_date_zero AS DATETIME ")
    _sql.AppendLine(" DECLARE @_end_date_zero AS DATETIME ")
    _sql.AppendLine(" DECLARE @_multi_game AS VARCHAR(10) ")

    _sql.AppendLine(" set @_ini_date = " & GUI_FormatDateDB(GetFromDate()))
    _sql.AppendLine(" set @_end_date = " & GUI_FormatDateDB(GetToDate()))
    _sql.AppendLine(" set @_ini_date_zero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@_ini_date),0)) ")
    _sql.AppendLine(" set @_end_date_zero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@_end_date),0)) ")
    _sql.AppendLine(" set @_multi_game = '" & _str_several & "'")


    _sql.AppendLine("SELECT * FROM                                                                                                              ")
    _sql.AppendLine("     (                                                                                                                     ")
    _sql.AppendLine("   SELECT   TE_TERMINAL_ID   AS TE_TERMINAL_ID                                                                             ")
    _sql.AppendLine("          , TE_PROVIDER_ID   AS PROVIDER_ID                                                                                ")
    _sql.AppendLine("          , TE_NAME          AS TERMINAL_NAME                                                                              ")
    _sql.AppendLine("          , TE_TERMINAL_TYPE AS TERMINAL_TYPE                                                                              ")
    _sql.AppendLine("          , ISNULL((SELECT CASE COUNT(PG_GAME_NAME) WHEN 1 THEN MAX(PG_GAME_NAME) ELSE @_multi_game END                    ")
    _sql.AppendLine("                     FROM  TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES                                                      ")
    _sql.AppendLine("                     WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID                                                             ")
    _sql.AppendLine("                     AND TGT_TERMINAL_ID = TE_TERMINAL_ID GROUP BY TGT_TERMINAL_ID),'UNKNOWN') AS GAME                     ")
    _sql.AppendLine("          , ISNULL(SUM(DATEDIFF(SECOND, PS_STARTED, PS_FINISHED)),0)            AS OCCUPIED                                ")
    _sql.AppendLine("          , isnull(DATE_COUNTS.CONNECTED,0)                                     AS TERMINALS_CONNECTED                     ")
    _sql.AppendLine("          , ISNULL(SUM(PS_TOTAL_PLAYED),0)                                      AS PLAYED                                  ")
    _sql.AppendLine("          , ISNULL(SUM(PS_TOTAL_CASH_IN),0) - ISNULL(SUM(PS_TOTAL_CASH_OUT),0)  AS NETWIN                                  ")
    _sql.AppendLine("          , ISNULL(DATE_COUNTS.CONNECTED,0)                                     AS TERMINAL_COUNT_MASTER                   ")
    _sql.AppendLine("   FROM TERMINALS                                                                                                          ")
    _sql.AppendLine("   LEFT JOIN (SELECT TC_TERMINAL_ID, COUNT(1) AS CONNECTED                                                                 ")
    _sql.AppendLine("              FROM TERMINALS_CONNECTED                                                                                     ")
    _sql.AppendLine("              WHERE TC_DATE >= @_ini_date_zero AND  TC_DATE < @_end_date_zero GROUP BY TC_TERMINAL_ID)  AS DATE_COUNTS     ")
    _sql.AppendLine("   ON DATE_COUNTS.TC_TERMINAL_ID = TE_TERMINAL_ID                                                                          ")
    _sql.AppendLine("   LEFT JOIN   PLAY_SESSIONS WITH(INDEX(IX_ps_finished)) ON TE_TERMINAL_ID = PS_TERMINAL_ID AND PS_FINISHED >= @_ini_date AND PS_FINISHED <  @_end_date   ")
    _sql.AppendLine("   WHERE   TE_TERMINAL_TYPE IN (1,3,5,7, 106)                                                                              ")
    _sql.AppendLine("   GROUP BY   TE_TERMINAL_ID                                                                                               ")
    _sql.AppendLine("            , TE_PROVIDER_ID                                                                                               ")
    _sql.AppendLine("            , TE_NAME                                                                                                      ")
    _sql.AppendLine("            , TE_TERMINAL_TYPE                                                                                             ")
    _sql.AppendLine("            , TE_MASTER_ID                                                                                                 ")
    _sql.AppendLine("            , DATE_COUNTS.CONNECTED                                                                                        ")
    _sql.AppendLine("     ) AS Y                                                                                                                ")
    _sql.AppendLine("WHERE Y.TERMINALS_CONNECTED > 0 Or (Y.OCCUPIED + Y.PLAYED + ABS(Y.NETWIN)) > 0                                             ")
    _sql.AppendLine("ORDER BY   Y.PROVIDER_ID, Y.TERMINAL_NAME                                                                                  ")


    Return _sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _ds As DataSet
    Dim _dt As DataTable
    Dim _num_row As Integer
    Dim _provider As String
    Dim _column_provider As String
    Dim _column_occupied As String
    Dim _column_connected As String
    Dim _column_master_id_connected As String
    Dim _column_played As String
    Dim _column_netwin As String

    Dim _count_terminal As Integer
    Dim _time As TimeSpan
    Dim _timespan_duration As TimeSpan
    Dim _time_provider As Int64
    Dim _days As Int64
    Dim _occupation As Decimal
    Dim _prov_occupation As Decimal
    Dim _occupation_site As Decimal
    Dim _played_provider As Decimal
    Dim _netwin_provider As Decimal
    Dim _played_site As Decimal
    Dim _netwin_site As Decimal

    Dim _ratio_site As Decimal
    Dim _ratio_provider As Decimal
    Dim _ratio_played As Decimal
    Dim _ratio_netwin As Decimal

    Dim _color_cell As Color

    Dim _total_days As Int64
    Dim _total_time_provider As Int64
    Dim _total_terminals As Int64
    Dim _total_ocuppied As Int64
    Dim _total_connected As Int64
    Dim _total_played As Decimal
    Dim _total_netwin As Decimal
    Dim _seconds_connected As Int64

    Dim _terminal_types() As WSI.Common.TerminalTypes
    Dim _terminal_type As WSI.Common.TerminalTypes
    Dim _col As Integer
    Dim _n As Integer
    Dim _selected_rows As Integer
    Dim _first_time As Boolean

    Dim _terminal_data_ok As Boolean

    _db_trx = Nothing
    _sql_da = Nothing
    _ds = New DataSet()
    _provider = ""

    _count_terminal = 0
    _time_provider = 0
    _days = 0
    _played_provider = 0
    _netwin_provider = 0

    _total_terminals = 0
    _total_time_provider = 0
    _total_days = 0
    _total_played = 0
    _total_netwin = 0

    _timespan_duration = New TimeSpan()

    Call GUI_StyleSheet()

    Try

      Call Me.Grid.Clear()
      Me.Grid.Redraw = False

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand())
      _sql_da.SelectCommand.CommandTimeout = 60
      _sql_da.SelectCommand.CommandText = GUI_FilterGetSqlQuery()
      _db_trx.Fill(_sql_da, _ds)

      _terminal_data_ok = WSI.Common.TerminalReport.GetReportDataTable(_ds.Tables(0), m_terminal_report_type)

      If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
        _dt = _ds.Tables(0)

        ' Exclude rows without activity
        _selected_rows = _dt.Select("TERMINAL_TYPE IS NOT NULL ").Length

        ' JAB 24-AUG-2012: Control to show huge amounts of records.
        If Not ShowHugeNumberOfRows(_selected_rows) Then
          MyBase.m_user_canceled_data_shows = True
          Exit Sub
        End If

        _column_provider = _dt.Columns(SQL_COLUMN_PROVIDER).ColumnName
        _column_occupied = _dt.Columns(SQL_COLUMN_OCCUPIED).ColumnName
        _column_connected = _dt.Columns(SQL_COLUMN_CONNECTED).ColumnName
        _column_played = _dt.Columns(SQL_COLUMN_PLAYED).ColumnName
        _column_netwin = _dt.Columns(SQL_COLUMN_NETWIN).ColumnName
        _column_master_id_connected = _dt.Columns(SQL_COLUMN_MASTER_ID_CONNECTED).ColumnName

        _total_ocuppied = _dt.Compute("SUM(" & _column_occupied & ")", "")
        _total_connected = _dt.Compute("SUM(" & _column_connected & ")", "")
        If _total_connected > 0 Then
          _occupation_site = ((New TimeSpan(_total_ocuppied * 10000000)).TotalSeconds * 100) / (_total_connected * 24 * 60 * 60)  ' IN SECONDS !!!
        Else
          _occupation_site = 0
        End If

        _played_site = _dt.Compute("SUM(" & _column_played & ")", "")
        _netwin_site = _dt.Compute("SUM(" & _column_netwin & ")", "")

        If _total_connected > 0 Then
          _played_site = _played_site / _total_connected
          _netwin_site = _netwin_site / _total_connected
        Else
          _played_site = 0
          _netwin_site = 0
        End If

        _provider = ""
        _first_time = True

        _terminal_types = WSI.Common.Misc.GamingTerminalTypeList()

        For _n = 0 To _dt.Rows.Count - 1

          ' JAB 12-JUN-2012: DoEvents run each second.
          If Not GUI_DoEvents(Me.Grid) Then
            Exit Sub
          End If

          If _dt.Rows(_n).IsNull(SQL_COLUMN_TERMINAL_TYPE) Then
            Continue For
          End If

          _terminal_type = _dt.Rows(_n).Item(SQL_COLUMN_TERMINAL_TYPE)
          If Array.IndexOf(_terminal_types, _terminal_type) < 0 Then
            Continue For
          End If

          With Me.Grid

            'Add the provider

            '
            ' TOTAL PROVIDER
            '
            If _first_time Or _provider <> _dt.Rows(_n).Item(SQL_COLUMN_PROVIDER) Then

              If Not _first_time And m_show_providers Then
                .AddRow()
                _num_row = Me.Grid.NumRows - 1

                .Cell(_num_row, GRID_INIT_TERMINAL_DATA).Value = _provider
                .Cell(_num_row, GRID_COLUMN_NUM_TERMINAL).Value = GUI_FormatNumber(_count_terminal, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
                .Cell(_num_row, GRID_COLUMN_CONNECTED_DAYS).Value = GUI_FormatNumber(_days, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

                _seconds_connected = _time_provider
                If _days > 0 Then
                  _seconds_connected = _seconds_connected / _days
                Else
                  _seconds_connected = 0
                End If
                _time = New TimeSpan(_seconds_connected * 10000000)
                .Cell(_num_row, GRID_COLUMN_OCCUPIED).Value = TimeSpanToString(_time)

                If _days > 0 Then
                  _occupation = (_time_provider * 100) / (_days * 24 * 60 * 60)  ' IN SECONDS !!!
                Else
                  _occupation = 0
                End If
                .Cell(_num_row, GRID_COLUMN_OCCUPATION).Value = GUI_FormatNumber(_occupation, 3) & "%"

                For _col = 0 To Me.Grid.NumColumns - 1
                  .Cell(_num_row, _col).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
                Next

                If _occupation_site > 0 Then
                  _ratio_site = _occupation / _occupation_site
                Else
                  _ratio_site = 0
                End If
                .Cell(_num_row, GRID_COLUMN_RATIO_SITE).Value = GUI_FormatNumber(_ratio_site, 2)

                _color_cell = GetColorCell(_ratio_site)
                If _color_cell <> Color.White Then
                  .Cell(_num_row, GRID_COLUMN_RATIO_SITE).BackColor = _color_cell
                End If

                ' JAB & RCI 02-MAY-2012: Check divide by zero condition.
                If _days <> 0 Then
                  .Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(_played_provider / _days, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
                  .Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_netwin_provider / _days, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
                Else
                  .Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
                  .Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
                End If
                If _days <> 0 And _played_site <> 0 Then
                  _ratio_played = (_played_provider / _days) / _played_site
                Else
                  _ratio_played = 0
                End If
                If _days <> 0 And _netwin_site <> 0 Then
                  _ratio_netwin = (_netwin_provider / _days) / _netwin_site
                Else
                  _ratio_netwin = 0
                End If


                '_color_cell = GetColorCell(_ratio_played)
                'If _color_cell <> Color.White Then
                '  .Cell(_num_row, GRID_COLUMN_PLAYED).BackColor = _color_cell
                'End If

                '_color_cell = GetColorCell(_ratio_netwin)
                'If _color_cell <> Color.White Then
                '  .Cell(_num_row, GRID_COLUMN_NETWIN).BackColor = _color_cell
                'End If

                .Counter(COUNTER_PROVIDER).Value += 1
              End If

              _total_days += _days
              _total_time_provider += _time_provider
              _total_terminals += _count_terminal
              _total_played += _played_provider
              _total_netwin += _netwin_provider

              _days = 0
              _time_provider = 0
              _count_terminal = 0
              _played_provider = 0
              _netwin_provider = 0

              _provider = _dt.Rows(_n).Item(SQL_COLUMN_PROVIDER)
              _first_time = False

              _total_ocuppied = _dt.Compute("SUM(" & _column_occupied & ")", _column_provider & "='" & _provider.Replace("'", "''") & "'")
              _total_connected = _dt.Compute("SUM(" & _column_master_id_connected & ")", _column_provider & "='" & _provider.Replace("'", "''") & "'")

              If _total_connected > 0 Then
                _prov_occupation = ((New TimeSpan(_total_ocuppied * 10000000)).TotalSeconds * 100) / (_total_connected * 24 * 60 * 60)  ' IN SECONDS !!!
              Else
                _prov_occupation = 0
              End If

            End If

            If m_show_terminals Then

              'Add the terminal

              _num_row = .AddRow()

              If _terminal_data_ok Then
                For _idx As Int32 = 0 To TERMINAL_DATA_COLUMNS - 1
                  If Not _dt.Rows(_n).IsNull(SQL_COLUMNS + _idx) Then
                    .Cell(_num_row, GRID_INIT_TERMINAL_DATA + _idx).Value = _dt.Rows(_n).Item(SQL_COLUMNS + _idx)
                  End If
                Next
              End If

              .Cell(_num_row, GRID_COLUMN_NUM_TERMINAL).Value = ""
              .Cell(_num_row, GRID_COLUMN_GAME_NAME).Value = IIf(_dt.Rows(_n).IsNull(SQL_COLUMN_GAME_NAME), "", _dt.Rows(_n).Item(SQL_COLUMN_GAME_NAME))
              .Cell(_num_row, GRID_COLUMN_CONNECTED_DAYS).Value = GUI_FormatNumber(_dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

              _seconds_connected = _dt.Rows(_n).Item(SQL_COLUMN_OCCUPIED)
              If _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED) > 0 Then
                _seconds_connected = _seconds_connected / _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED)
              Else
                _seconds_connected = 0
              End If
              _time = New TimeSpan(_seconds_connected * 10000000)
              .Cell(_num_row, GRID_COLUMN_OCCUPIED).Value = TimeSpanToString(_time)

              If _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED) > 0 Then
                _occupation = (_dt.Rows(_n).Item(SQL_COLUMN_OCCUPIED) * 100) / (_dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED) * 24 * 60 * 60)  ' IN SECONDS !!!
              Else
                _occupation = 0
              End If

              .Cell(_num_row, GRID_COLUMN_OCCUPATION).Value = GUI_FormatNumber(_occupation, 3) & "%"

              If _prov_occupation > 0 Then
                _ratio_provider = _occupation / _prov_occupation
              Else
                _ratio_provider = 0
              End If
              .Cell(_num_row, GRID_COLUMN_RATIO_PROVIDER).Value = GUI_FormatNumber(_ratio_provider, 2)

              _color_cell = GetColorCell(_ratio_provider)
              If _color_cell <> Color.White Then
                .Cell(_num_row, GRID_COLUMN_RATIO_PROVIDER).BackColor = _color_cell
              End If

              If _occupation_site > 0 Then
                _ratio_site = _occupation / _occupation_site
              Else
                _ratio_site = 0
              End If
              .Cell(_num_row, GRID_COLUMN_RATIO_SITE).Value = GUI_FormatNumber(_ratio_site, 2)

              _color_cell = GetColorCell(_ratio_site)
              If _color_cell <> Color.White Then
                .Cell(_num_row, GRID_COLUMN_RATIO_SITE).BackColor = _color_cell
              End If

              ' JAB & RCI 02-MAY-2012: Check divide by zero condition.
              If _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED) <> 0 Then
                .Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(_dt.Rows(_n).Item(SQL_COLUMN_PLAYED) / _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED), 2)
                .Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_dt.Rows(_n).Item(SQL_COLUMN_NETWIN) / _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED), 2)
              Else
                .Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(0, 2)
                .Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, 2)
              End If
              If _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED) <> 0 And _played_site <> 0 Then
                _ratio_played = (_dt.Rows(_n).Item(SQL_COLUMN_PLAYED) / _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED)) / _played_site
              Else
                _ratio_played = 0
              End If
              If _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED) <> 0 And _netwin_site <> 0 Then
                _ratio_netwin = (_dt.Rows(_n).Item(SQL_COLUMN_NETWIN) / _dt.Rows(_n).Item(SQL_COLUMN_MASTER_ID_CONNECTED)) / _netwin_site
              Else
                _ratio_netwin = 0
              End If

              '_color_cell = GetColorCell(_ratio_played)
              'If _color_cell <> Color.White Then
              '  .Cell(_num_row, GRID_COLUMN_PLAYED).BackColor = _color_cell
              'End If

              '_color_cell = GetColorCell(_ratio_netwin)
              'If _color_cell <> Color.White Then
              '  .Cell(_num_row, GRID_COLUMN_NETWIN).BackColor = _color_cell
              'End If

            End If

            _days += _dt.Rows(_n).Item(SQL_COLUMN_CONNECTED)
            _time_provider += _dt.Rows(_n).Item(SQL_COLUMN_OCCUPIED)
            _count_terminal += 1
            _played_provider += _dt.Rows(_n).Item(SQL_COLUMN_PLAYED)
            _netwin_provider += _dt.Rows(_n).Item(SQL_COLUMN_NETWIN)

          End With

        Next

        '
        ' TOTAL PROVIDER
        '
        If m_show_providers Then
          Me.Grid.AddRow()
          _num_row = Me.Grid.NumRows - 1
          Me.Grid.Cell(_num_row, GRID_INIT_TERMINAL_DATA).Value = _provider
          Me.Grid.Cell(_num_row, GRID_COLUMN_NUM_TERMINAL).Value = GUI_FormatNumber(_count_terminal, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
          Me.Grid.Cell(_num_row, GRID_COLUMN_CONNECTED_DAYS).Value = GUI_FormatNumber(_days, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

          _seconds_connected = _time_provider
          If _days > 0 Then
            _seconds_connected = _seconds_connected / _days
          Else
            _seconds_connected = 0
          End If
          _time = New TimeSpan(_seconds_connected * 10000000)
          Me.Grid.Cell(_num_row, GRID_COLUMN_OCCUPIED).Value = TimeSpanToString(_time)

          If _days > 0 Then
            _occupation = (_time_provider * 100) / (_days * 24 * 60 * 60)  ' IN SECONDS !!!
          Else
            _occupation = 0
          End If
          Me.Grid.Cell(_num_row, GRID_COLUMN_OCCUPATION).Value = GUI_FormatNumber(_occupation, 3) & "%"

          For _col = 0 To Me.Grid.NumColumns - 1
            Me.Grid.Cell(_num_row, _col).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          Next

          If _occupation_site > 0 Then
            _ratio_site = _occupation / _occupation_site
          Else
            _ratio_site = 0
          End If
          Me.Grid.Cell(_num_row, GRID_COLUMN_RATIO_SITE).Value = GUI_FormatNumber(_ratio_site, 2)

          _color_cell = GetColorCell(_ratio_site)
          If _color_cell <> Color.White Then
            Me.Grid.Cell(_num_row, GRID_COLUMN_RATIO_SITE).BackColor = _color_cell
          End If

          ' JAB & RCI 02-MAY-2012: Check divide by zero condition.
          If _days <> 0 Then
            Me.Grid.Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(_played_provider / _days, 2)
            Me.Grid.Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_netwin_provider / _days, 2)
          Else
            Me.Grid.Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(0, 2)
            Me.Grid.Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, 2)
          End If
          If _days <> 0 And _played_site <> 0 Then
            _ratio_played = (_played_provider / _days) / _played_site
          Else
            _ratio_played = 0
          End If
          If _days <> 0 And _netwin_site <> 0 Then
            _ratio_netwin = (_netwin_provider / _days) / _netwin_site
          Else
            _ratio_netwin = 0
          End If

          '_color_cell = GetColorCell(_ratio_played)
          'If _color_cell <> Color.White Then
          '  Me.Grid.Cell(_num_row, GRID_COLUMN_PLAYED).BackColor = _color_cell
          'End If

          '_color_cell = GetColorCell(_ratio_netwin)
          'If _color_cell <> Color.White Then
          '  Me.Grid.Cell(_num_row, GRID_COLUMN_NETWIN).BackColor = _color_cell
          'End If

          Me.Grid.Counter(COUNTER_PROVIDER).Value += 1
        End If

        _total_days += _days
        _total_time_provider += _time_provider
        _total_terminals += _count_terminal
        _total_played += _played_provider
        _total_netwin += _netwin_provider
      End If

      '
      ' TOTAL
      '
      Me.Grid.AddRow()
      _num_row = Me.Grid.NumRows - 1

      With Me.Grid

        .Cell(_num_row, GRID_INIT_TERMINAL_DATA).Value = GLB_NLS_GUI_STATISTICS.GetString(203)
        .Cell(_num_row, GRID_COLUMN_NUM_TERMINAL).Value = GUI_FormatNumber(_total_terminals, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
        .Cell(_num_row, GRID_COLUMN_CONNECTED_DAYS).Value = GUI_FormatNumber(_total_days, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)

        _seconds_connected = _total_time_provider
        If _total_days > 0 Then
          _seconds_connected = _seconds_connected / _total_days
        Else
          _seconds_connected = 0
        End If
        _time = New TimeSpan(_seconds_connected * 10000000)
        .Cell(_num_row, GRID_COLUMN_OCCUPIED).Value = TimeSpanToString(_time)

        If _total_days > 0 Then
          _occupation = (_total_time_provider * 100) / (_total_days * 24 * 60 * 60)  ' IN SECONDS !!!
        Else
          _occupation = 0
        End If
        .Cell(_num_row, GRID_COLUMN_OCCUPATION).Value = GUI_FormatNumber(_occupation, 3) & "%"

        If _total_days > 0 Then
          Me.Grid.Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(_total_played / _total_days, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Me.Grid.Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_total_netwin / _total_days, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else
          Me.Grid.Cell(_num_row, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          Me.Grid.Cell(_num_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If

        .Row(_num_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      End With

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Customer base", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)
    Finally

      ' JAB 08-JUN-2012: Check if disposed.
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      If Not IsNothing(_db_trx) Then
        _db_trx.Dispose()
      End If
      If Not IsNothing(_sql_da) Then
        _sql_da.Dispose()
      End If

      If _ds IsNot Nothing Then
        Call _ds.Dispose()
        _ds = Nothing
      End If

    End Try

  End Sub

  ' PURPOSE: Overridable routine to initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    If RowIndex >= 0 Or ColIndex <> GRID_COLUMN_CONNECTED_DAYS Then
      Return
    End If

    ToolTipTxt = GLB_NLS_GUI_STATISTICS.GetString(448)

  End Sub ' GUI_SetToolTipText

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(m_excel_column_occupation, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.PERCENTAGE, 3)
    ExcelData.SetColumnFormat(m_excel_column_ratio, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.NUMERIC)

  End Sub

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(443), m_show_providers_str)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(472), m_show_terminals_str)
    PrintData.SetFilter("", "", True)

    PrintData.FilterValueWidth(1) = 5000


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_show_providers_str = ""
    m_show_terminals_str = ""

    ' Date
    _date = GetFromDate()
    m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _date = GetToDate()
    m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    m_show_providers = False
    m_show_terminals = False
    m_grid_sorted = False

    If Me.chk_providers.Checked Or (Not Me.chk_providers.Checked And Not Me.chk_terminales.Checked) Then
      m_show_providers = True
    End If

    If Me.chk_terminales.Checked Or (Not Me.chk_providers.Checked And Not Me.chk_terminales.Checked) Then
      m_show_terminals = True
    End If

    If (Me.chk_providers.Checked And Not Me.chk_terminales.Checked) Or (Not Me.chk_providers.Checked And Me.chk_terminales.Checked) Then
      m_grid_sorted = True
    End If

    If m_show_providers Then
      m_show_providers_str = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_providers_str = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If m_show_terminals Then
      m_show_terminals_str = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_terminals_str = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Calculate excel column
    If chk_providers.Checked And Not chk_terminales.Checked Then
      m_excel_column_occupation = 5
      m_excel_column_ratio = 6
    ElseIf chk_terminal_location.Checked Then
      m_excel_column_occupation = 11
      m_excel_column_ratio = 12
    Else
      m_excel_column_occupation = 8
      m_excel_column_ratio = 9
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    With Me.Grid

      .Redraw = False
      .Sortable = m_grid_sorted

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_PROVIDER).Visible = True
      .Counter(COUNTER_PROVIDER).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_PROVIDER).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Index
      'GRID_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).WidthFixed = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = " "
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column <> ReportColumn.Provider Then
          'Don't show terminal name nor id floor if terminals check button isn't checked (but providers check button is)
          If chk_providers.Checked And chk_terminales.Checked = False Then
            .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = 0
          End If
        End If
      Next

      'GRID_COLUMN_NUM_TERMINAL
      .Column(GRID_COLUMN_NUM_TERMINAL).Header(0).Text = " "
      .Column(GRID_COLUMN_NUM_TERMINAL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(212)
      .Column(GRID_COLUMN_NUM_TERMINAL).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_NUM_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'GRID_COLUMN_GAME_NAME 
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = " "
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COLUMN_GAME_NAME).Width = GRID_WIDTH_GAME + 500 ' GRID_WIDTH_GAME 
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_CONNECTED_DAYS 
      .Column(GRID_COLUMN_CONNECTED_DAYS).Header(0).Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(223)
      .Column(GRID_COLUMN_CONNECTED_DAYS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(461)
      .Column(GRID_COLUMN_CONNECTED_DAYS).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_CONNECTED_DAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'GRID_COLUMN_OCCUPIED 
      .Column(GRID_COLUMN_OCCUPIED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(441)
      .Column(GRID_COLUMN_OCCUPIED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(450)
      .Column(GRID_COLUMN_OCCUPIED).Width = GRID_WIDTH_TIME
      .Column(GRID_COLUMN_OCCUPIED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'GRID_COLUMN_OCCUPATION 
      .Column(GRID_COLUMN_OCCUPATION).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(441)
      .Column(GRID_COLUMN_OCCUPATION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(460)
      .Column(GRID_COLUMN_OCCUPATION).Width = GRID_WIDTH_PERCENTAGE
      .Column(GRID_COLUMN_OCCUPATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'GRID_COLUMN_RATIO_PROVIDER
      .Column(GRID_COLUMN_RATIO_PROVIDER).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(445)
      .Column(GRID_COLUMN_RATIO_PROVIDER).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(446)
      .Column(GRID_COLUMN_RATIO_PROVIDER).Width = GRID_WIDTH_RATIO
      .Column(GRID_COLUMN_RATIO_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_RATIO_PROVIDER).HighLightWhenSelected = False

      'GRID_COLUMN_RATIO_SITE
      .Column(GRID_COLUMN_RATIO_SITE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(445)
      .Column(GRID_COLUMN_RATIO_SITE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(447)
      .Column(GRID_COLUMN_RATIO_SITE).Width = GRID_WIDTH_RATIO
      .Column(GRID_COLUMN_RATIO_SITE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_RATIO_SITE).HighLightWhenSelected = False

      'GRID_COLUMN_PLAYED
      .Column(GRID_COLUMN_PLAYED).Header(0).Text = " " ' GLB_NLS_GUI_STATISTICS.GetString(463)
      .Column(GRID_COLUMN_PLAYED).Header(1).Text = " " ' GLB_NLS_GUI_STATISTICS.GetString(301)
      .Column(GRID_COLUMN_PLAYED).Width = 0 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'GRID_COLUMN_NETWIN
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(311) ' GLB_NLS_GUI_STATISTICS.GetString(463)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(463) ' GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NETWIN).Width = 2000 ' GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Redraw = True

    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _date As DateTime
    Dim _closing_time As Integer
    Dim _initial_time As Date

    _initial_time = WSI.Common.Misc.TodayOpening()
    _closing_time = GetDefaultClosingTime()

    _date = New DateTime(Now.Year, Now.Month, 1)
    Me.uc_dsl.FromDate = _date.AddMonths(-1)
    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddMonths(1)
    Me.uc_dsl.ToDateSelected = True

    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_providers.Checked = True
    Me.chk_terminales.Checked = True

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Return TO date of filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Date

  Private Function GetToDate() As Date

    Dim _to_time As Date = New Date(Me.uc_dsl.ToDate.Year, Me.uc_dsl.ToDate.Month, Me.uc_dsl.ToDate.Day, Me.uc_dsl.ClosingTime, 0, 0)
    Dim _now As Date = WSI.Common.Misc.TodayOpening().AddDays(1)

    If Me.uc_dsl.ToDateSelected Then
      If _to_time > _now Then
        _to_time = _now
      End If
    Else
      _to_time = _now
    End If

    Return _to_time

  End Function

  ' PURPOSE: Return from date of filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Date

  Private Function GetFromDate() As Date

    Return New Date(Me.uc_dsl.FromDate.Year, Me.uc_dsl.FromDate.Month, Me.uc_dsl.FromDate.Day, Me.uc_dsl.ClosingTime, 0, 0)

  End Function

  ' PURPOSE: Return a color based on the value
  '
  '  PARAMS:
  '     - INPUT:
  '           - Duration, DaysOfActivity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Color

  Private Function GetColorCell(ByVal RatioValue As Decimal) As Color

    Dim _r As Integer
    Dim _g As Integer
    Dim _b As Integer
    Dim _v As Decimal

    RatioValue = Math.Max(0.25, RatioValue)
    RatioValue = Math.Min(4, RatioValue)

    _v = Math.Log(RatioValue) / Math.Log(2.0)

    If _v = 0 Then
      Return Color.White
    ElseIf _v > 0 Then
      _v = (_v * _v) / 2.0
      _g = 255
      _r = 255 - (255.0 * _v / 2.0)
      _b = 255 - (255.0 * _v / 2.0)
    Else
      _v = -_v
      _r = 255
      _g = 255 - (255.0 * _v / 2.0)
      _b = 255 - (255.0 * _v / 2.0)
    End If

    Return Color.FromArgb(_r, _g, _b)

  End Function

#End Region

#Region " Public "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  Private Sub lbl_gradient_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles lbl_gradient.Paint

    Dim _b As Brush
    Dim _p As Pen
    Dim _i As Integer
    Dim _r As Rectangle
    Dim _rat As Decimal

    For _i = 0 To 150
      _r = New Rectangle(Math.Min(_i, lbl_gradient.Width - 1), 0, 1, lbl_gradient.Height)
      _rat = (_i / 2 + 25.0) / 100.0
      _p = New Pen(GetColorCell(_rat))
      _b = _p.Brush
      e.Graphics.FillRectangle(_b, _r)
    Next

    For _i = 0 To 150
      _r = New Rectangle(Math.Min(_i + 150, lbl_gradient.Width - 1), 0, 1, lbl_gradient.Height)
      _rat = ((_i / 2 / 25) + 1.0)
      _p = New Pen(GetColorCell(_rat))
      _b = _p.Brush
      e.Graphics.FillRectangle(_b, _r)
    Next

  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region

End Class