'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_accounting_taxes
' DESCRIPTION:   This screen allows to view the taxes:
'                           - between two dates 
'
' AUTHOR:        Agust� Poch
' CREATION DATE: 29-AUG-2007
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     -----------------------------------------------
' 29-AUG-2007  APB        Initial version
' 18-JAN-2013  QMP        Tax columns names are now read from GeneralParams
' 25-NOV-2013  RMS & QMP  Rewritten SQL Query
' 15-MAR-2017  ATB        Third TAX - Report columns
' 16-MAR-2017  ATB        PBI 25737: Third TAX - Report columns
' 03-APR-2017  ATB        PBI 25737: Third TAX - Report columns
' 10-AUG-2017  DPC        WIGOS-3910: Third Tax column hidden and then displayed with 0 value in Winnings taxes daily summary when there is no values applied
' 21-FEB-2018  EOR        Bug 31638: WIGOS-3678 Title is cut when exporting data in Winning taxes daily summary screen (in Spanish)
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text

Public Class frm_acct_taxes
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Size = New System.Drawing.Size(1113, 127)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 131)
    Me.panel_data.Size = New System.Drawing.Size(1113, 512)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1107, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1107, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.FromDateText = "#5339"
    Me.uc_dsl.Location = New System.Drawing.Point(10, 2)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDateText = "#5340"
    '
    'frm_acct_taxes
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1121, 647)
    Me.Name = "frm_acct_taxes"
    Me.Text = "frm_acct_taxes"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_DATETIME As Integer = 0
  Private Const SQL_COLUMN_GROSS_AMOUNT As Integer = 1
  Private Const SQL_COLUMN_FEDERAL_TAXES_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_STATE_TAXES_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_COUNCIL_TAXES_AMOUNT As Integer = 4

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE_FROM As Integer = 1
  Private Const GRID_COLUMN_DATE_TO As Integer = 2
  Private Const GRID_COLUMN_GROSS_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_FEDERAL_TAXES_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_STATE_TAXES_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_COUNCIL_TAXES_AMOUNT As Integer = 6

  Private Const GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 1750
  Private Const GRID_WIDTH_AMOUNT As Integer = 2500
  Private Const GRID_WIDTH_COUNT As Integer = 1100

  ' Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

  ' For amount grid
  Dim m_total_gross_amount As Double
  Dim m_total_federal_taxes_amount As Double
  Dim m_total_state_taxes_amount As Double
  Dim m_total_council_taxes_amount As Double

  Dim m_is_council_tax_active As Boolean

#End Region ' Members

#Region " OVERRIDES "

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function 'GUI_MaxRows

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCT_TAXES

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(204)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    If m_is_council_tax_active Then
      Me.Grid.Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Fixed = False
      Me.Grid.Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Width = GRID_WIDTH_AMOUNT
    End If

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    m_total_gross_amount = 0
    m_total_federal_taxes_amount = 0
    m_total_state_taxes_amount = 0
    m_total_council_taxes_amount = 0

  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.AddRow()

    Dim idx_row As Integer = Me.Grid.NumRows - 1

    ' Date
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_FROM).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Gross amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_GROSS_AMOUNT).Value = GUI_FormatCurrency(m_total_gross_amount, _
                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Federal Taxes amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_FEDERAL_TAXES_AMOUNT).Value = GUI_FormatCurrency(m_total_federal_taxes_amount, _
                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' State Taxes amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_STATE_TAXES_AMOUNT).Value = GUI_FormatCurrency(m_total_state_taxes_amount, _
                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Council Taxes amount
    Me.Grid.Cell(idx_row, GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Value = GUI_FormatCurrency(m_total_council_taxes_amount, _
                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If (m_is_council_tax_active OrElse m_total_council_taxes_amount > 0) Then
      Me.Grid.Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Fixed = False
      Me.Grid.Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Width = GRID_WIDTH_AMOUNT
    Else
      Me.Grid.Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Width = 0
    End If

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    Dim _str_where As String
    Dim _base_date As String

    _base_date = "01/01/2007 " & Me.uc_dsl.ClosingTime.ToString("00") & ":00:00"
    _str_where = GetSqlWhere()

    _str_sql = New StringBuilder()
    _str_sql.AppendLine(" SELECT   DATEADD(DAY, DATEDIFF(HOUR, '" & _base_date & "', AM_DATETIME)/24, '" & _base_date & "') TAX_DATE ")
    _str_sql.AppendLine("        , SUM (CASE WHEN AM_TYPE =  2 THEN AM_SUB_AMOUNT ELSE 0 END) AMOUNT ")
    _str_sql.AppendLine("        , SUM (CASE WHEN (AM_TYPE =  4  OR AM_TYPE =  1600) THEN AM_SUB_AMOUNT ELSE 0 END) FEDERAL ")
    _str_sql.AppendLine("        , SUM (CASE WHEN (AM_TYPE = 13  OR AM_TYPE =  1601) THEN AM_SUB_AMOUNT ELSE 0 END) STATE ")
    _str_sql.AppendLine("        , SUM (CASE WHEN (AM_TYPE = 250)  THEN AM_SUB_AMOUNT ELSE 0 END) COUNCIL ")

    _str_sql.AppendLine("   FROM   ACCOUNT_MOVEMENTS WITH( INDEX(IX_type_date_account)) ")
    _str_sql.AppendLine("  WHERE   AM_TYPE IN (2, 4, 13,1600,1601, 250) ")
    _str_sql.AppendLine(_str_where)
    _str_sql.AppendLine("  GROUP   BY DATEDIFF(HOUR, '" & _base_date & "', AM_DATETIME)/24 ")
    _str_sql.AppendLine("  ORDER   BY TAX_DATE ASC ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim gross_amount As Double
    Dim federal_taxes_amount As Double
    Dim state_taxes_amount As Double
    Dim council_taxes_amount As Double
    Dim to_date As Date

    gross_amount = 0
    federal_taxes_amount = 0
    state_taxes_amount = 0

    ' Date From
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Date To
    to_date = DbRow.Value(SQL_COLUMN_DATETIME)
    to_date = to_date.AddDays(1)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(to_date, _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Gross Amount
    gross_amount = DbRow.Value(SQL_COLUMN_GROSS_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GROSS_AMOUNT).Value = GUI_FormatCurrency(gross_amount, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_total_gross_amount += gross_amount

    ' Federal Taxes Amount
    federal_taxes_amount = DbRow.Value(SQL_COLUMN_FEDERAL_TAXES_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FEDERAL_TAXES_AMOUNT).Value = GUI_FormatCurrency(federal_taxes_amount, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_total_federal_taxes_amount += federal_taxes_amount

    ' State Taxes Amount
    state_taxes_amount = DbRow.Value(SQL_COLUMN_STATE_TAXES_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATE_TAXES_AMOUNT).Value = GUI_FormatCurrency(state_taxes_amount, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_total_state_taxes_amount += state_taxes_amount

    ' Council Taxes Amount
    council_taxes_amount = DbRow.Value(SQL_COLUMN_COUNCIL_TAXES_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Value = GUI_FormatCurrency(council_taxes_amount, _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_total_council_taxes_amount += council_taxes_amount
    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(204)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""

    'Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date from
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(202)
      .Column(GRID_COLUMN_DATE_FROM).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DATE_FROM).ExcelWidth = 20

      ' Date TO
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(203)
      .Column(GRID_COLUMN_DATE_TO).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DATE_TO).ExcelWidth = 20

      ' Gross Amount
      .Column(GRID_COLUMN_GROSS_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_GROSS_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(206)
      .Column(GRID_COLUMN_GROSS_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_GROSS_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_GROSS_AMOUNT).ExcelWidth = 20

      ' Federal Taxes Amount
      .Column(GRID_COLUMN_FEDERAL_TAXES_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(207)
      .Column(GRID_COLUMN_FEDERAL_TAXES_AMOUNT).Header(1).Text = GetCashierData("Tax.OnPrize.1.Name")
      .Column(GRID_COLUMN_FEDERAL_TAXES_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_FEDERAL_TAXES_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_FEDERAL_TAXES_AMOUNT).ExcelWidth = 20

      ' State Taxes Amount
      .Column(GRID_COLUMN_STATE_TAXES_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(207)
      .Column(GRID_COLUMN_STATE_TAXES_AMOUNT).Header(1).Text = GetCashierData("Tax.OnPrize.2.Name")
      .Column(GRID_COLUMN_STATE_TAXES_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_STATE_TAXES_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_STATE_TAXES_AMOUNT).ExcelWidth = 20

      ' Council Taxes Amount
      .Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(207)
      .Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Header(1).Text = GetCashierData("Tax.OnPrize.3.Name")
      .Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Fixed = False
      .Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Width = 0

      .Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Fixed = True

      .Column(GRID_COLUMN_COUNCIL_TAXES_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Sortable = False

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim from_date As DateTime
    Dim closing_time As Integer

    closing_time = GetDefaultClosingTime()

    from_date = New DateTime(Now.Year, Now.Month, 1)
    Me.uc_dsl.FromDate = from_date.AddMonths(-1)
    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddMonths(1)
    Me.uc_dsl.ToDateSelected = True

    Me.uc_dsl.ClosingTime = closing_time

    m_is_council_tax_active = WSI.Common.GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.3.Pct") > 0

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim date_from As Date
    Dim date_to As Date

    date_from = Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    date_to = Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime)

    ' Filter Dates
    If Me.uc_dsl.FromDateSelected = True Then
      str_where = str_where & " AND (AM_DATETIME >= " & GUI_FormatDateDB(date_from) & ") "
    End If

    If Me.uc_dsl.ToDateSelected = True Then
      str_where = str_where & " AND (AM_DATETIME < " & GUI_FormatDateDB(date_to) & ") "
    End If

    Return str_where
  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Events"

#End Region ' Events

End Class
