<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mailing_programming_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.ef_prog_name = New GUI_Controls.uc_entry_field
    Me.gb_scheduling = New System.Windows.Forms.GroupBox
    Me.cmb_time_step = New GUI_Controls.uc_combo
    Me.dtp_time_from = New GUI_Controls.uc_date_picker
    Me.dtp_time_to = New GUI_Controls.uc_date_picker
    Me.chk_sunday = New System.Windows.Forms.CheckBox
    Me.chk_saturday = New System.Windows.Forms.CheckBox
    Me.chk_friday = New System.Windows.Forms.CheckBox
    Me.chk_thursday = New System.Windows.Forms.CheckBox
    Me.chk_wednesday = New System.Windows.Forms.CheckBox
    Me.chk_tuesday = New System.Windows.Forms.CheckBox
    Me.chk_monday = New System.Windows.Forms.CheckBox
    Me.cmb_prog_type = New GUI_Controls.uc_combo
    Me.gb_address_list = New System.Windows.Forms.GroupBox
    Me.txt_address_list = New System.Windows.Forms.TextBox
    Me.ef_subject = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_scheduling.SuspendLayout()
    Me.gb_address_list.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.ef_subject)
    Me.panel_data.Controls.Add(Me.gb_address_list)
    Me.panel_data.Controls.Add(Me.cmb_prog_type)
    Me.panel_data.Controls.Add(Me.gb_scheduling)
    Me.panel_data.Controls.Add(Me.ef_prog_name)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(545, 428)
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(444, 14)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(94, 68)
    Me.gb_enabled.TabIndex = 1
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(13, 40)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(13, 18)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'ef_prog_name
    '
    Me.ef_prog_name.DoubleValue = 0
    Me.ef_prog_name.IntegerValue = 0
    Me.ef_prog_name.IsReadOnly = False
    Me.ef_prog_name.Location = New System.Drawing.Point(14, 20)
    Me.ef_prog_name.Name = "ef_prog_name"
    Me.ef_prog_name.Size = New System.Drawing.Size(380, 24)
    Me.ef_prog_name.SufixText = "Sufix Text"
    Me.ef_prog_name.SufixTextVisible = True
    Me.ef_prog_name.TabIndex = 0
    Me.ef_prog_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_prog_name.TextValue = ""
    Me.ef_prog_name.Value = ""
    Me.ef_prog_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_scheduling
    '
    Me.gb_scheduling.Controls.Add(Me.cmb_time_step)
    Me.gb_scheduling.Controls.Add(Me.dtp_time_from)
    Me.gb_scheduling.Controls.Add(Me.dtp_time_to)
    Me.gb_scheduling.Controls.Add(Me.chk_sunday)
    Me.gb_scheduling.Controls.Add(Me.chk_saturday)
    Me.gb_scheduling.Controls.Add(Me.chk_friday)
    Me.gb_scheduling.Controls.Add(Me.chk_thursday)
    Me.gb_scheduling.Controls.Add(Me.chk_wednesday)
    Me.gb_scheduling.Controls.Add(Me.chk_tuesday)
    Me.gb_scheduling.Controls.Add(Me.chk_monday)
    Me.gb_scheduling.Location = New System.Drawing.Point(14, 237)
    Me.gb_scheduling.Name = "gb_scheduling"
    Me.gb_scheduling.Size = New System.Drawing.Size(524, 185)
    Me.gb_scheduling.TabIndex = 5
    Me.gb_scheduling.TabStop = False
    Me.gb_scheduling.Text = "xScheduling"
    '
    'cmb_time_step
    '
    Me.cmb_time_step.AllowUnlistedValues = False
    Me.cmb_time_step.AutoCompleteMode = False
    Me.cmb_time_step.IsReadOnly = False
    Me.cmb_time_step.Location = New System.Drawing.Point(136, 20)
    Me.cmb_time_step.Name = "cmb_time_step"
    Me.cmb_time_step.SelectedIndex = -1
    Me.cmb_time_step.Size = New System.Drawing.Size(244, 24)
    Me.cmb_time_step.SufixText = "Sufix Text"
    Me.cmb_time_step.SufixTextVisible = True
    Me.cmb_time_step.TabIndex = 7
    Me.cmb_time_step.TextWidth = 100
    '
    'dtp_time_from
    '
    Me.dtp_time_from.Checked = True
    Me.dtp_time_from.IsReadOnly = False
    Me.dtp_time_from.Location = New System.Drawing.Point(186, 51)
    Me.dtp_time_from.Name = "dtp_time_from"
    Me.dtp_time_from.ShowCheckBox = False
    Me.dtp_time_from.ShowUpDown = False
    Me.dtp_time_from.Size = New System.Drawing.Size(112, 24)
    Me.dtp_time_from.SufixText = "Sufix Text"
    Me.dtp_time_from.SufixTextVisible = True
    Me.dtp_time_from.TabIndex = 8
    Me.dtp_time_from.TextWidth = 50
    Me.dtp_time_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_time_to
    '
    Me.dtp_time_to.Checked = True
    Me.dtp_time_to.IsReadOnly = False
    Me.dtp_time_to.Location = New System.Drawing.Point(186, 81)
    Me.dtp_time_to.Name = "dtp_time_to"
    Me.dtp_time_to.ShowCheckBox = False
    Me.dtp_time_to.ShowUpDown = False
    Me.dtp_time_to.Size = New System.Drawing.Size(112, 24)
    Me.dtp_time_to.SufixText = "Sufix Text"
    Me.dtp_time_to.SufixTextVisible = True
    Me.dtp_time_to.TabIndex = 9
    Me.dtp_time_to.TextWidth = 50
    Me.dtp_time_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(20, 158)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_sunday.TabIndex = 6
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(20, 135)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_saturday.TabIndex = 5
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(20, 112)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_friday.TabIndex = 4
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(20, 89)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_thursday.TabIndex = 3
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(20, 66)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(99, 17)
    Me.chk_wednesday.TabIndex = 2
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(20, 43)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(81, 17)
    Me.chk_tuesday.TabIndex = 1
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(20, 20)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_monday.TabIndex = 0
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'cmb_prog_type
    '
    Me.cmb_prog_type.AllowUnlistedValues = False
    Me.cmb_prog_type.AutoCompleteMode = False
    Me.cmb_prog_type.IsReadOnly = False
    Me.cmb_prog_type.Location = New System.Drawing.Point(14, 51)
    Me.cmb_prog_type.Name = "cmb_prog_type"
    Me.cmb_prog_type.SelectedIndex = -1
    Me.cmb_prog_type.Size = New System.Drawing.Size(380, 24)
    Me.cmb_prog_type.SufixText = "Sufix Text"
    Me.cmb_prog_type.SufixTextVisible = True
    Me.cmb_prog_type.TabIndex = 2
    '
    'gb_address_list
    '
    Me.gb_address_list.Controls.Add(Me.txt_address_list)
    Me.gb_address_list.Location = New System.Drawing.Point(14, 111)
    Me.gb_address_list.Name = "gb_address_list"
    Me.gb_address_list.Size = New System.Drawing.Size(524, 117)
    Me.gb_address_list.TabIndex = 4
    Me.gb_address_list.TabStop = False
    Me.gb_address_list.Text = "xAddress List"
    '
    'txt_address_list
    '
    Me.txt_address_list.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_address_list.Location = New System.Drawing.Point(6, 20)
    Me.txt_address_list.MaxLength = 500
    Me.txt_address_list.Multiline = True
    Me.txt_address_list.Name = "txt_address_list"
    Me.txt_address_list.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_address_list.Size = New System.Drawing.Size(512, 91)
    Me.txt_address_list.TabIndex = 0
    '
    'ef_subject
    '
    Me.ef_subject.DoubleValue = 0
    Me.ef_subject.IntegerValue = 0
    Me.ef_subject.IsReadOnly = False
    Me.ef_subject.Location = New System.Drawing.Point(14, 81)
    Me.ef_subject.Name = "ef_subject"
    Me.ef_subject.Size = New System.Drawing.Size(380, 24)
    Me.ef_subject.SufixText = "Sufix Text"
    Me.ef_subject.SufixTextVisible = True
    Me.ef_subject.TabIndex = 3
    Me.ef_subject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_subject.TextValue = ""
    Me.ef_subject.Value = ""
    Me.ef_subject.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_mailing_programming_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(648, 438)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_mailing_programming_edit"
    Me.Text = "frm_mailing_programming_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_scheduling.ResumeLayout(False)
    Me.gb_scheduling.PerformLayout()
    Me.gb_address_list.ResumeLayout(False)
    Me.gb_address_list.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents ef_prog_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_scheduling As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_monday As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_prog_type As GUI_Controls.uc_combo
  Friend WithEvents gb_address_list As System.Windows.Forms.GroupBox
  Friend WithEvents txt_address_list As System.Windows.Forms.TextBox
  Friend WithEvents cmb_time_step As GUI_Controls.uc_combo
  Friend WithEvents ef_subject As GUI_Controls.uc_entry_field
End Class
