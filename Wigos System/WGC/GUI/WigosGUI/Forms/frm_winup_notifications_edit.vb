﻿
'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_winup_notifications_edit
' DESCRIPTION:   WinUp notifications edit
' AUTHOR:        Pablo Molina
' CREATION DATE: 19-ENE-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-ENE-2017  PDM    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports WSI.Common
Imports GUI_Reports
Imports System.IO

Public Class frm_winup_notifications_edit
  Inherits frm_base_edit

#Region "Members"

  Private m_class_base As CLASS_WINUP_NOTIFICATIONS

  Private m_id As Integer
  Private m_title As String
  Private m_message As String
  Private m_target As String
  Private m_type_target As Integer
  Private m_isurl As Boolean
  Private m_segmentation As Int32
  Private m_schedule As Int32
  Private m_status As Boolean

  Private m_account_id_selected As Integer

  Private m_promotion_copied_id As Long
  Private m_promotion_copy_operation As Boolean
  Private m_current_directory As String
  Private m_dt_acc As DataTable
  Private m_dt_error As DataTable
  ' Progress bar
  Private m_progress As frm_excel_progress = Nothing

  Private m_screen_data_ok As Boolean

#End Region

#Region " Enums "

  Private Enum GRID_DATA_STATUS
    OK = 1
    VALUE_NOT_VALID = 2
    NO_ONE_CHECKED = 3
    MAX_FLAGS_EXCEEDED = 4
  End Enum

#End Region ' Enums

#Region "Structures"

#End Region  ' Structures

#Region "Constants"

  'GRID ACCOUNTS
  Private Const GRID_AC_ID As Integer = 0
  Private Const GRID_AC_NAME As Integer = 1
  Private Const GRID_AC_EMAIL As Integer = 2
  Private Const GRID_AC_DATE_BIRTH As Integer = 3

#End Region

#Region "Constructor"

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    m_class_base = New CLASS_WINUP_NOTIFICATIONS()

  End Sub

#End Region

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_WINUP_NOTIFICATION_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7866)

    ef_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
    chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)

    btn_sent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7893)

    lbl_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
    lbl_type_section.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7868)
    ef_url.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7872)
    lbl_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428)
    gb_scheduler.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7869)
    gb_segmentation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7874)
    btn_find_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7880)

    opt_section.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7867)
    opt_url.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7872)
    optInformative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7916)

    gb_goto.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7873)
    btn_import.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1238)

    Me.ef_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)
    Me.ef_url.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 1000)

    opt_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7876)
    opt_customer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7877)
    opt_customer_group.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7878)
    opt_not_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7879)
    opt_all_devices.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7887)

    opt_inmediate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7870)
    opt_schedule.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7871)
    ' Filters
    tab_filters.TabPages(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(213)
    tab_filters.TabPages(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(216)
    ' AVZ 01-OCT-2015: Tab Control initialization
    tab_filters.TabPages(2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6720)
    tab_filters.TabPages(3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(404)
    tab_filters.TabPages(4).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(408)
    chk_by_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)
    opt_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)
    chk_by_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_birth_date_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(217)
    opt_birth_month_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(218)
    'FBA 08-OCT-2013 new age range filter
    chk_by_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2730)
    opt_birth_include_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702)
    opt_birth_exclude_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703)
    ef_age_to1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2789)
    ef_age_to1.TextVisible = True
    ef_age_to2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2790)
    ef_age_to2.TextVisible = True
    ef_age_from1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_age_from2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_age_to1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_age_to2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' AVZ 01-OCT-2015: Controls initialization
    chk_by_created_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_created_account_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6721)
    opt_created_account_working_day_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6722)
    opt_created_account_working_day_plus.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6723)
    ef_created_account_working_day_plus.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6724)
    ef_created_account_working_day_plus.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    ef_created_account_working_day_plus.TextVisible = True

    opt_created_account_anniversary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6725)
    opt_created_account_anniversary_whole_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6726)
    opt_created_account_anniversary_day_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6727)
    chk_by_anniversary_range_of_years.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6728)
    ef_created_account_anniversary_year_from.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6729)
    ef_created_account_anniversary_year_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    ef_created_account_anniversary_year_from.TextVisible = True
    ef_created_account_anniversary_year_to.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6730)
    ef_created_account_anniversary_year_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    ef_created_account_anniversary_year_to.TextVisible = True

    'By Level
    chk_by_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(407)
    chk_level_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If
    chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802)

    chk_by_freq.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    ef_freq_last_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
    ef_freq_last_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_freq_min_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
    ef_freq_min_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_freq_min_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(411)
    ef_freq_min_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)

    lbl_freq_filter_01.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(412, ef_freq_last_days.Value, ef_freq_min_days.Value)
    lbl_freq_filter_02.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(413, ef_freq_min_cash_in.Value)
    ' RCI 16-NOV-2010: BUG Text Visible
    ef_freq_last_days.TextVisible = True
    ef_freq_min_days.TextVisible = True
    ef_freq_min_cash_in.TextVisible = True

    chk_by_gender.Checked = False
    chk_by_gender_CheckedChanged(Nothing, Nothing)

    chk_by_birth_date.Checked = False
    chk_by_birth_date_CheckedChanged(Nothing, Nothing)

    chk_by_age.Checked = False
    chk_by_age_CheckedChanged(Nothing, Nothing)

    chk_by_created_account.Checked = False
    chk_by_created_account_CheckedChanged(Nothing, Nothing)

    chk_by_level.Checked = False
    chk_by_level_CheckedChanged(Nothing, Nothing)

    chk_by_freq.Checked = False
    chk_by_freq_CheckedChanged(Nothing, Nothing)

    '
    m_dt_acc = Nothing
    m_dt_error = Nothing
    '

    Me.uc_schedule.OnlyTimeToEnable = True

    Call Me.Grid_Accounts_StyleSheet()

    Call Me.LoadSections()
    Call MyBase.GUI_InitControls()

  End Sub

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)


    Me.ef_title.TextValue = m_class_base.Title
    Me.txt_message.Text = m_class_base.Message
    Me.chk_enabled.Checked = m_class_base.Status

    If m_class_base.IsUrl Then
      opt_url.Checked = True
      ef_url.TextValue = m_class_base.Target
    Else
      If m_class_base.Target = String.Empty Then
        optInformative.Checked = True
        cmb_section.TextValue = String.Empty
      Else
        opt_section.Checked = True
        cmb_section.TextValue = m_class_base.Target

        If m_class_base.Target_Type > 0 Then
          cmb_section_type.Value = m_class_base.Target_Type
        End If
      End If
    End If

    Me.uc_image.Image = m_class_base.Image

    Select Case m_class_base.Schedule

      Case 0
        opt_inmediate.Checked = True
        opt_schedule.Checked = False

        Me.uc_schedule.Weekday = 0
        Me.uc_schedule.DateFrom = DateTime.MinValue
        Me.uc_schedule.DateTo = DateTime.MinValue
        Me.uc_schedule.TimeFrom = 0
        Me.uc_schedule.TimeTo = 0

      Case 1

        opt_inmediate.Checked = False
        opt_schedule.Checked = True

        Me.uc_schedule.Weekday = m_class_base.Weekdays
        Me.uc_schedule.DateFrom = m_class_base.DateFrom
        Me.uc_schedule.DateTo = m_class_base.DateTo
        Me.uc_schedule.TimeFrom = m_class_base.TimeFrom
        Me.uc_schedule.TimeTo = m_class_base.TimeTo

    End Select

    Select Case m_class_base.Segmentation

      Case 0

        opt_filter.Checked = True

        ' Gender Filter
        Select Case m_class_base.GenderFilter
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.NOT_SET
            chk_by_gender.Checked = False
            opt_gender_male.Enabled = False
            opt_gender_male.Checked = False
            opt_gender_female.Enabled = False
            opt_gender_female.Checked = False

          Case CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.MEN_ONLY
            chk_by_gender.Checked = True
            opt_gender_male.Checked = True
            opt_gender_female.Checked = False

          Case CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.WOMEN_ONLY
            chk_by_gender.Checked = True
            opt_gender_male.Checked = False
            opt_gender_female.Checked = True

        End Select

        ' Birthday Filter
        Select Case m_class_base.BirthdayFilter
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.NOT_SET
            chk_by_birth_date.Checked = False
            opt_birth_date_only.Checked = False
            opt_birth_date_only.Enabled = False
            opt_birth_month_only.Checked = False
            opt_birth_month_only.Enabled = False

          Case CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.DAY_ONLY
            chk_by_birth_date.Checked = True
            opt_birth_date_only.Checked = True
            opt_birth_month_only.Checked = False

          Case CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
            chk_by_birth_date.Checked = True
            opt_birth_date_only.Checked = False
            opt_birth_month_only.Checked = True

        End Select

        Select Case m_class_base.AgeFilter
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.NOT_SET
            opt_birth_include_age.Checked = False
            opt_birth_include_age.Enabled = False
            opt_birth_exclude_age.Checked = False
            opt_birth_exclude_age.Enabled = False
            ef_age_from2.Value = ""
            ef_age_from2.Enabled = False
            ef_age_from1.Value = ""
            ef_age_from1.Enabled = False
            ef_age_to2.Value = ""
            ef_age_to2.Enabled = False
            ef_age_to1.Value = ""
            ef_age_to1.Enabled = False

          Case CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.INCLUDE_AGE
            chk_by_age.Checked = True
            opt_birth_include_age.Checked = True
            opt_birth_exclude_age.Checked = False
            ef_age_from1.Enabled = True
            ef_age_from1.Value = m_class_base.BirthdayAgeFrom
            ef_age_to1.Enabled = True
            ef_age_to1.Value = m_class_base.BirthdayAgeTo

          Case CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE
            chk_by_age.Checked = True
            opt_birth_include_age.Checked = False
            opt_birth_exclude_age.Checked = True
            ef_age_from2.Enabled = True
            ef_age_from2.Value = m_class_base.BirthdayAgeFrom
            ef_age_to2.Enabled = True
            ef_age_to2.Value = m_class_base.BirthdayAgeTo

        End Select

        ' AVZ 01-OCT-2015
        Select Case m_class_base.CreatedAccountFilter
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
            chk_by_created_account.Checked = False
            opt_created_account_date.Checked = False
            opt_created_account_working_day_only.Checked = False
            opt_created_account_working_day_plus.Checked = False
            opt_created_account_anniversary.Checked = False
            opt_created_account_anniversary_whole_month.Checked = False
            opt_created_account_anniversary_day_month.Checked = False

            opt_created_account_date.Enabled = False
            opt_created_account_working_day_only.Enabled = False
            opt_created_account_working_day_plus.Enabled = False
            opt_created_account_anniversary.Enabled = False
            opt_created_account_anniversary_whole_month.Enabled = False
            opt_created_account_anniversary_day_month.Enabled = False
            chk_by_anniversary_range_of_years.Enabled = False
            ef_created_account_working_day_plus.Enabled = False
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAY_ONLY
            chk_by_created_account.Checked = True
            opt_created_account_date.Checked = True
            opt_created_account_working_day_only.Checked = True
            opt_created_account_working_day_plus.Checked = False
            opt_created_account_anniversary.Checked = False
            opt_created_account_anniversary_whole_month.Checked = False
            opt_created_account_anniversary_day_month.Checked = False

            opt_created_account_anniversary_whole_month.Enabled = False
            opt_created_account_anniversary_day_month.Enabled = False
            chk_by_anniversary_range_of_years.Enabled = False
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAYS_INCLUDED
            chk_by_created_account.Checked = True
            opt_created_account_date.Checked = True
            opt_created_account_working_day_only.Checked = False
            opt_created_account_working_day_plus.Checked = True
            opt_created_account_anniversary.Checked = False
            opt_created_account_anniversary_whole_month.Checked = False
            opt_created_account_anniversary_day_month.Checked = False

            opt_created_account_anniversary_whole_month.Enabled = False
            opt_created_account_anniversary_day_month.Enabled = False
            chk_by_anniversary_range_of_years.Enabled = False
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WHOLE_MONTH
            chk_by_created_account.Checked = True
            opt_created_account_date.Checked = False
            opt_created_account_working_day_only.Checked = False
            opt_created_account_working_day_plus.Checked = False
            opt_created_account_anniversary.Checked = True
            opt_created_account_anniversary_whole_month.Checked = True
            opt_created_account_anniversary_day_month.Checked = False

            opt_created_account_working_day_only.Enabled = False
            opt_created_account_working_day_plus.Enabled = False
            ef_created_account_working_day_plus.Enabled = False
          Case CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WORKING_DAY_ONLY
            chk_by_created_account.Checked = True
            opt_created_account_date.Checked = False
            opt_created_account_working_day_only.Checked = False
            opt_created_account_working_day_plus.Checked = False
            opt_created_account_anniversary.Checked = True
            opt_created_account_anniversary_whole_month.Checked = False
            opt_created_account_anniversary_day_month.Checked = True

            opt_created_account_working_day_only.Enabled = False
            opt_created_account_working_day_plus.Enabled = False
            ef_created_account_working_day_plus.Enabled = False
        End Select

        ef_created_account_working_day_plus.Value = m_class_base.CreatedAccountWorkingDaysIncluded
        ef_created_account_anniversary_year_from.Value = m_class_base.CreatedAccountAnniversaryYearFrom
        ef_created_account_anniversary_year_to.Value = m_class_base.CreatedAccountAnniversaryYearTo
        chk_by_anniversary_range_of_years.Checked = m_class_base.CreatedAccountAnniversaryYearFrom <> 0

        If Not chk_by_anniversary_range_of_years.Checked Then
          ef_created_account_anniversary_year_from.Enabled = False
          ef_created_account_anniversary_year_to.Enabled = False
        End If

        ' Level Filter
        SetLevelFilter(m_class_base.LevelFilter)

        ' Frequency Filter
        If m_class_base.FreqFilterLastDays = 0 Then
          chk_by_freq.Checked = False
          ef_freq_last_days.Enabled = False
          ef_freq_min_days.Enabled = False
          ef_freq_min_cash_in.Enabled = False
        Else
          chk_by_freq.Checked = True
          ef_freq_last_days.Enabled = True
          ef_freq_min_days.Enabled = True
          ef_freq_min_cash_in.Enabled = True

          ef_freq_last_days.Value = m_class_base.FreqFilterLastDays
          ef_freq_min_days.Value = m_class_base.FreqFilterMinDays
          ef_freq_min_cash_in.Value = m_class_base.FreqFilterMinCashIn
        End If
        lbl_freq_filter_01.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(412, ef_freq_last_days.Value, ef_freq_min_days.Value)
        lbl_freq_filter_02.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(413, ef_freq_min_cash_in.Value)
        ' Select first activated tab, in order.
        ' AVZ 01-OCT-2015: Tab "Filter By created account date" added
        tab_filters.SelectedIndex = 0
        If chk_by_gender.Checked = False Then
          If chk_by_birth_date.Checked = False And chk_by_age.Checked = False Then
            If chk_by_created_account.Checked = False Then
              If chk_by_level.Checked = False Then
                If chk_by_freq.Checked = True Then
                  tab_filters.SelectedIndex = 4
                End If
              Else
                tab_filters.SelectedIndex = 3
              End If
            Else
              tab_filters.SelectedIndex = 2
            End If
          Else
            tab_filters.SelectedIndex = 1
          End If
        End If

        chk_only_vip.Checked = m_class_base.IsVip

      Case 1
        opt_customer.Checked = True
        LoadGridAccounts(" ac_account_id=" + m_class_base.AccountId.ToString())
        Me.uc_account_sel.Account = m_class_base.AccountId
      Case 2
        opt_customer_group.Checked = True

        m_dt_acc = m_class_base.GetAccountsByIds(m_class_base.AccountsIds)

        ShowAccounts(m_dt_acc, Nothing)

      Case 3
        opt_not_account.Checked = True
      Case 4
        opt_all_devices.Checked = True
    End Select

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      opt_section.Checked = True
      opt_schedule.Checked = True
      If cmb_section.Count > 0 Then
        cmb_section.SelectedIndex = 0
        cmb_section.Focus()
      End If
    End If

    ef_title.Focus()

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    m_class_base.Title = Me.ef_title.TextValue
    m_class_base.Message = Me.txt_message.Text
    m_class_base.Status = Me.chk_enabled.Checked

    If opt_section.Checked Then
      m_class_base.IsUrl = False
      m_class_base.Target = cmb_section.TextValue

      If cmb_section_type.Value > -1 Then
        m_class_base.Target_Type = cmb_section_type.Value
      Else
        m_class_base.Target_Type = 0
      End If
    End If

    If opt_url.Checked Then
      m_class_base.IsUrl = True
      m_class_base.Target = ef_url.TextValue
      m_class_base.Target_Type = 0
    End If

    If optInformative.Checked Then
      m_class_base.IsUrl = False
      m_class_base.Target = String.Empty
      m_class_base.Target_Type = 0
    End If

    If opt_inmediate.Checked Then
      m_class_base.Schedule = 0
      m_class_base.Weekdays = 0
      m_class_base.DateFrom = m_class_base.DefaultDate
      m_class_base.DateTo = m_class_base.DefaultDate
      m_class_base.TimeFrom = 0
      m_class_base.TimeTo = 0
    End If

    If opt_schedule.Checked Then
      m_class_base.Schedule = 1
      m_class_base.Weekdays = Me.uc_schedule.Weekday
      m_class_base.DateFrom = Me.uc_schedule.DateFrom
      m_class_base.DateTo = Me.uc_schedule.DateTo
      m_class_base.TimeFrom = Me.uc_schedule.TimeFrom
      m_class_base.TimeTo = Me.uc_schedule.TimeTo
    End If


    If Not Me.uc_image.Image Is Nothing Then
      m_class_base.Image = Me.uc_image.Image
    Else
      m_class_base.Image = Nothing
    End If

    'DEFAULTS VALUES==============================================
    m_class_base.GenderFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.NOT_SET
    m_class_base.BirthdayFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.NOT_SET
    m_class_base.AgeFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.NOT_SET
    m_class_base.BirthdayAgeFrom = 0
    m_class_base.BirthdayAgeTo = 0
    m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
    m_class_base.FreqFilterLastDays = 0
    m_class_base.FreqFilterMinDays = 0
    m_class_base.FreqFilterMinCashIn = 0
    m_class_base.LevelFilter = 0
    m_class_base.AccountId = 0
    m_class_base.IsVip = False
    m_class_base.AccountsIds = ""
    m_account_id_selected = 0
    '==============================================================

    If opt_filter.Checked Then

      m_class_base.Segmentation = 0

      'GENDER =======================================================================
      If chk_by_gender.Checked Then
        If opt_gender_male.Checked Then
          m_class_base.GenderFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.MEN_ONLY
        ElseIf opt_gender_female.Checked Then
          m_class_base.GenderFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.WOMEN_ONLY
        End If
      End If

      'BIRTHDAY ======================================================================
      If chk_by_birth_date.Checked Then
        If opt_birth_date_only.Checked Then
          m_class_base.BirthdayFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.DAY_ONLY
        ElseIf opt_birth_month_only.Checked Then
          m_class_base.BirthdayFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
        End If
      End If


      If chk_by_age.Checked Then
        If Me.ef_age_from1.Value = "" Then ef_age_from1.Value = 0
        If Me.ef_age_from2.Value = "" Then ef_age_from2.Value = 0
        If Me.ef_age_to1.Value = "" Then ef_age_to1.Value = 0
        If Me.ef_age_to2.Value = "" Then ef_age_to2.Value = 0
        If opt_birth_include_age.Checked Then
          m_class_base.BirthdayFilter = WSI.Common.Promotion.EncodeBirthdayFilter(m_class_base.BirthdayFilter, CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.INCLUDE_AGE, ef_age_from1.Value, ef_age_to1.Value)
          m_class_base.AgeFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.INCLUDE_AGE
        ElseIf opt_birth_exclude_age.Checked Then
          m_class_base.BirthdayFilter = WSI.Common.Promotion.EncodeBirthdayFilter(m_class_base.BirthdayFilter, CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE, ef_age_from2.Value, ef_age_to2.Value)
          m_class_base.AgeFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE
        End If
      End If

      m_class_base.BirthdayAgeFrom = IIf(ef_age_from2.Value = "", 0, ef_age_from2.Value)
      m_class_base.BirthdayAgeTo = IIf(ef_age_to2.Value = "", 0, ef_age_to2.Value)

      'ACCOUNT ========================================================================
      m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
      Dim working_day_plus As Integer = 0
      Dim anniversary_year_from As Integer = 0
      Dim anniversary_year_to As Integer = 0

      If chk_by_created_account.Checked Then

        If opt_created_account_date.Checked Then
          If opt_created_account_working_day_only.Checked Then
            m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAY_ONLY
          ElseIf opt_created_account_working_day_plus.Checked Then
            m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAYS_INCLUDED

            If (ef_created_account_working_day_plus.TextValue <> String.Empty) Then
              working_day_plus = ef_created_account_working_day_plus.Value
            End If
          End If
        ElseIf opt_created_account_anniversary.Checked Then
          If opt_created_account_anniversary_whole_month.Checked Then
            m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WHOLE_MONTH
          ElseIf opt_created_account_anniversary_day_month.Checked Then
            m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WORKING_DAY_ONLY
          End If

          If chk_by_anniversary_range_of_years.Checked Then
            If (ef_created_account_anniversary_year_from.TextValue <> String.Empty) Then
              anniversary_year_from = ef_created_account_anniversary_year_from.Value
            End If
            If (ef_created_account_anniversary_year_to.TextValue <> String.Empty) Then
              anniversary_year_to = ef_created_account_anniversary_year_to.Value
            End If
          End If
        End If
      End If

      m_class_base.CreatedAccountFilter = WSI.Common.Promotion.EncodeCreatedAccountFilter(m_class_base.CreatedAccountFilter, working_day_plus, anniversary_year_from, anniversary_year_to)
      m_class_base.CreatedAccountWorkingDaysIncluded = working_day_plus
      m_class_base.CreatedAccountAnniversaryYearFrom = anniversary_year_from
      m_class_base.CreatedAccountAnniversaryYearTo = anniversary_year_to

      'LEVEL ============================================
      m_class_base.LevelFilter = GetLevelFilter()

      'FREQUENCY=========================================
      If chk_by_freq.Checked Then
        m_class_base.FreqFilterLastDays = GUI_ParseNumber(ef_freq_last_days.Value)
        m_class_base.FreqFilterMinDays = GUI_ParseNumber(ef_freq_min_days.Value)
        m_class_base.FreqFilterMinCashIn = GUI_ParseCurrency(ef_freq_min_cash_in.Value)
      Else
        m_class_base.FreqFilterLastDays = 0
        m_class_base.FreqFilterMinDays = 0
        m_class_base.FreqFilterMinCashIn = 0
      End If

      lbl_freq_filter_01.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(412, ef_freq_last_days.Value, ef_freq_min_days.Value)
      lbl_freq_filter_02.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(413, ef_freq_min_cash_in.Value)
      m_class_base.IsVip = chk_only_vip.Checked

    End If

    If opt_customer.Checked Then
      m_class_base.Segmentation = 1
      m_class_base.AccountId = GetAccountIdSelected()
    End If

    If opt_customer_group.Checked Then
      m_class_base.Segmentation = 2
      m_class_base.AccountsIds = GetAccountIds()
    End If

    If opt_not_account.Checked Then
      m_class_base.Segmentation = 3
    End If

    If opt_all_devices.Checked Then
      m_class_base.Segmentation = 4
    End If

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean


    Dim _nls_param1 As String
    Dim _nls_param2 As String
    Dim _freq_last_days As Integer
    Dim _freq_min_days As Integer
    Dim _control_limit As UserControl
    Dim _selected_index As Integer
    Dim _legal_age As Integer
    Dim _date_from As Integer
    Dim _date_to As Integer

    _control_limit = Nothing
    _selected_index = 0

    If ef_title.TextValue = String.Empty Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7882), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call ef_title.Focus()
      Return False
    End If

    If txt_message.Text.Trim() = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7883), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call txt_message.Focus()
      Return False
    End If

    If opt_section.Checked AndAlso cmb_section.Value < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7875), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call cmb_section.Focus()
      Return False
    End If

    If opt_url.Checked AndAlso ef_url.TextValue = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7884), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call ef_url.Focus()
      Return False
    End If

    If opt_url.Checked AndAlso Not ef_url.TextValue = String.Empty Then
      If Not Uri.IsWellFormedUriString(ef_url.TextValue, UriKind.Absolute) Then
        ' the url is valid
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8478), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call ef_url.Focus()
        Return False
      End If
    End If

    


    If opt_schedule.Checked Then

      If uc_schedule.DateFrom > uc_schedule.DateTo Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call uc_schedule.SetFocus(1)

        Return False
      End If

      ' No Week days selected
      If uc_schedule.Weekday = uc_schedule.NoWeekDaysSelected() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call uc_schedule.SetFocus(4)

        Return False
      End If
    End If


    If opt_customer_group.Checked Then
      If GetAccountIds() = String.Empty Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1368), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      End If
    End If

    If opt_customer.Checked AndAlso GetAccountIdSelected() = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1368), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False
    End If

    ' Gender filter
    If chk_by_gender.Checked Then
      If opt_gender_male.Checked = False And opt_gender_female.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 0
        Call opt_gender_male.Focus()

        Return False
      End If
    End If

    ' Birth Date filter
    If chk_by_birth_date.Checked Then
      If opt_birth_date_only.Checked = False And opt_birth_month_only.Checked = False And opt_birth_include_age.Checked = False And opt_birth_exclude_age.Checked = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 1
        Call opt_birth_date_only.Focus()

        Return False
      End If

    End If

    If chk_by_age.Checked Then
      If opt_birth_exclude_age.Checked Then
        Integer.TryParse(ef_age_from2.Value, _date_from)
        Integer.TryParse(ef_age_to2.Value, _date_to)
        If ef_age_from2.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        ElseIf Not ef_age_from2.ValidateFormat Or ef_age_from2.Value > 255 Or ef_age_from2.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        End If
        If ef_age_to2.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to2.Focus()
          Return False
        ElseIf Not ef_age_to2.ValidateFormat Or ef_age_to2.Value > 255 Or ef_age_to2.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to2.Focus()
          Return False
        End If
        If _date_from > _date_to Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        End If
        If _date_from < _legal_age Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), _legal_age)
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        End If
      ElseIf opt_birth_include_age.Checked Then
        Integer.TryParse(ef_age_from1.Value, _date_from)
        Integer.TryParse(ef_age_to1.Value, _date_to)
        If ef_age_from1.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        ElseIf Not ef_age_from1.ValidateFormat Or ef_age_from1.Value > 255 Or ef_age_from1.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        End If
        If ef_age_to1.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to1.Focus()
          Return False
        ElseIf Not ef_age_to1.ValidateFormat Or ef_age_to1.Value > 255 Or ef_age_to1.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to1.Focus()
          Return False
        End If
        If _date_from > _date_to Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        End If
        If _date_from < _legal_age Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), _legal_age)
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        End If
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1321), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703))
        Return False
      End If

    End If

    ' AVZ 01-OCT-2015: Validation
    If chk_by_created_account.Checked Then

      If Not opt_created_account_date.Checked And Not opt_created_account_anniversary.Checked Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call opt_created_account_date.Focus()

        Return False
      End If

      If opt_created_account_date.Checked And Not opt_created_account_working_day_only.Checked And Not opt_created_account_working_day_plus.Checked Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call opt_created_account_working_day_only.Focus()

        Return False
      End If

      If opt_created_account_anniversary.Checked And Not opt_created_account_anniversary_whole_month.Checked And Not opt_created_account_anniversary_day_month.Checked Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call opt_created_account_anniversary_whole_month.Focus()

        Return False
      End If

      If opt_created_account_date.Checked And opt_created_account_working_day_plus.Checked Then
        If ef_created_account_working_day_plus.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6731))
          tab_filters.SelectedIndex = 2
          Call ef_created_account_working_day_plus.Focus()
          Return False
        ElseIf ef_created_account_working_day_plus.Value < 0 Or ef_created_account_working_day_plus.Value > 99 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6731))
          tab_filters.SelectedIndex = 2
          ef_created_account_working_day_plus.Focus()
          Return False
        End If
      End If

      If opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked Then
        If ef_created_account_anniversary_year_from.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732))
          tab_filters.SelectedIndex = 2
          Call ef_created_account_anniversary_year_from.Focus()
          Return False
        ElseIf ef_created_account_anniversary_year_from.Value = 0 Or ef_created_account_anniversary_year_from.Value > 99 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732))
          tab_filters.SelectedIndex = 2
          ef_created_account_anniversary_year_from.Focus()
          Return False
        End If

        If ef_created_account_anniversary_year_to.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          tab_filters.SelectedIndex = 2
          Call ef_created_account_anniversary_year_to.Focus()
          Return False
        ElseIf ef_created_account_anniversary_year_to.Value = 0 Or ef_created_account_anniversary_year_to.Value > 99 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          tab_filters.SelectedIndex = 2
          ef_created_account_anniversary_year_to.Focus()
          Return False
        End If

        If Integer.Parse(ef_created_account_anniversary_year_from.Value) > Integer.Parse(ef_created_account_anniversary_year_to.Value) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6782), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          tab_filters.SelectedIndex = 2
          ef_created_account_anniversary_year_to.Focus()
          Return False
        End If
      End If

    End If

    ' Level filter
    If chk_by_level.Checked Then
      If chk_level_anonymous.Checked = False _
         And chk_level_01.Checked = False _
         And chk_level_02.Checked = False _
         And chk_level_03.Checked = False _
         And chk_level_04.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 3
        Call chk_level_anonymous.Focus()

        Return False
      End If
    End If

    ' Frequency filter
    If chk_by_freq.Checked Then
      If ef_freq_last_days.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tab_filters.SelectedIndex = 4
        Call ef_freq_last_days.Focus()

        Return False
      End If
      If ef_freq_min_days.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tab_filters.SelectedIndex = 4
        Call ef_freq_min_days.Focus()

        Return False
      End If
      If ef_freq_min_cash_in.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(411)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tab_filters.SelectedIndex = 4
        Call ef_freq_min_cash_in.Focus()

        Return False
      End If

      _freq_last_days = GUI_ParseNumber(ef_freq_last_days.Value)
      _freq_min_days = GUI_ParseNumber(ef_freq_min_days.Value)
      If _freq_last_days < _freq_min_days Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
        _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
        tab_filters.SelectedIndex = 4
        Call ef_freq_last_days.Focus()

        Return False
      End If
    End If

    m_screen_data_ok = True

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal AdsId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbEditedObject = m_class_base
    Me.DbObjectId = AdsId


    ''Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = m_class_base

        opt_section.Checked = True
        opt_schedule.Checked = True
        uc_schedule.DateFrom = DateTime.Now
        uc_schedule.DateTo = DateTime.Now
        m_class_base.DateFrom = DateTime.Now
        m_class_base.DateTo = DateTime.Now
        m_class_base.Target = String.Empty
        m_class_base.Target_Type = 0
        m_class_base.Schedule = 1
        m_class_base.Segmentation = 0
        m_class_base.GenderFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_GENDER_FILTER.NOT_SET
        m_class_base.BirthdayFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.NOT_SET
        m_class_base.AgeFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_BIRTHDAY_FILTER.NOT_SET
        m_class_base.BirthdayAgeFrom = 0
        m_class_base.BirthdayAgeTo = 0
        m_class_base.CreatedAccountFilter = CLASS_WINUP_NOTIFICATIONS.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
        m_class_base.FreqFilterLastDays = 0
        m_class_base.FreqFilterMinDays = 0
        m_class_base.FreqFilterMinCashIn = 0
        m_class_base.LevelFilter = 0
        m_class_base.AccountId = 0
        m_class_base.IsVip = False
        m_class_base.AccountsIds = ""

        ef_title.Focus()

        DbStatus = ENUM_STATUS.STATUS_OK
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  ' PURPOSE : Override default behavior when user press key
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

      Case Chr(Keys.Escape)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)
    End Select
    '
  End Function


#End Region

#Region "Private Methods"

  ' PURPOSE: Enable/Disable the buttons for edit promotion
  '
  '  PARAMS:
  '     - INPUT:
  '           - Enable:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EnableButtons(ByVal Enable As Boolean)

    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Enable
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Enabled = Enable

  End Sub

  Private Sub RechargeFreqLabels()
    lbl_freq_filter_01.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(412, ef_freq_last_days.Value, ef_freq_min_days.Value)
    lbl_freq_filter_02.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(413, ef_freq_min_cash_in.Value)
  End Sub

  Private Sub SetLevelFilter(ByVal LevelFilter As Integer)

    Dim str_binary As String
    Dim bit_array As Char()

    chk_by_level.Checked = LevelFilter <> 0
    chk_by_level_CheckedChanged(Nothing, Nothing)

    str_binary = Convert.ToString(LevelFilter, 2)
    str_binary = New String("0", 5 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(4) = "0") Then chk_level_anonymous.Checked = False Else chk_level_anonymous.Checked = True ' Anonymous Player
    If (bit_array(3) = "0") Then chk_level_01.Checked = False Else chk_level_01.Checked = True ' Level 01
    If (bit_array(2) = "0") Then chk_level_02.Checked = False Else chk_level_02.Checked = True ' Level 02
    If (bit_array(1) = "0") Then chk_level_03.Checked = False Else chk_level_03.Checked = True ' Level 03
    If (bit_array(0) = "0") Then chk_level_04.Checked = False Else chk_level_04.Checked = True ' Level 04

  End Sub ' SetLevelFilter

  Private Function GetLevelFilter() As Integer
    Dim bit_array As Char()

    If Not chk_by_level.Checked Then
      Return 0
    End If

    bit_array = New String("00000").ToCharArray()

    bit_array(4) = Convert.ToInt32(chk_level_anonymous.Checked).ToString()  ' Anonymous Player

    bit_array(3) = Convert.ToInt32(chk_level_01.Checked).ToString()  ' Level 01
    bit_array(2) = Convert.ToInt32(chk_level_02.Checked).ToString()  ' Level 02
    bit_array(1) = Convert.ToInt32(chk_level_03.Checked).ToString()  ' Level 03
    bit_array(0) = Convert.ToInt32(chk_level_04.Checked).ToString()  ' Level 04

    Return Convert.ToInt32(bit_array, 2)
  End Function ' GetLevelFilter

  Private Sub LoadSections()

    cmb_section.Clear()

    cmb_section.Add(m_class_base.GetSections())

    If cmb_section.Count > 0 Then
      cmb_section.SelectedIndex = 0
    End If

  End Sub

  Private Sub LoadTypeSections(ByVal IdSection As Integer)

    cmb_section_type.Clear()

    cmb_section_type.Add(m_class_base.GetTypeSections(IdSection))

    If cmb_section_type.Count > 0 Then
      cmb_section_type.SelectedIndex = 0
    End If

  End Sub


  Private Sub Grid_Accounts_StyleSheet()

    With Me.uc_grid_accounts
      Call .Init(2)

      '' ID
      .Column(GRID_AC_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1910)
      .Column(GRID_AC_ID).Width = 0
      .Column(GRID_AC_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '' NAME
      .Column(GRID_AC_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6266)
      .Column(GRID_AC_NAME).Width = 6300
      .Column(GRID_AC_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' '' NAME
      '.Column(GRID_AC_EMAIL).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2385)
      '.Column(GRID_AC_EMAIL).Width = 2300
      '.Column(GRID_AC_EMAIL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' '' NAME
      '.Column(GRID_AC_DATE_BIRTH).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(746)
      '.Column(GRID_AC_DATE_BIRTH).Width = 1300
      '.Column(GRID_AC_DATE_BIRTH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub


  Private Function GetAccountIdSelected() As Integer

    Dim _idx_row As Integer
    m_account_id_selected = 0

    For _idx_row = 0 To Me.uc_grid_accounts.NumRows - 1
      If Me.uc_grid_accounts.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row >= 0 Then
      m_account_id_selected = Me.uc_grid_accounts.Cell(_idx_row, 0).Value
    End If

    Return m_account_id_selected

  End Function


  Private Sub LoadGridAccounts(ByVal SqlWhere As String)

    Dim _table As DataTable
    Dim _index As Integer

    _index = 0

    _table = m_class_base.GetAccountsByFilter(SqlWhere)

    Me.uc_grid_accounts.Clear()

    If _table.Rows.Count > 0 Then

      For Each _dr As DataRow In _table.Rows

        Me.uc_grid_accounts.AddRow()
        Me.uc_grid_accounts.Cell(_index, GRID_AC_ID).Value = _dr.Item(GRID_AC_ID)
        Me.uc_grid_accounts.Cell(_index, GRID_AC_NAME).Value = IIf(IsDBNull(_dr.Item(GRID_AC_NAME)), "", _dr.Item(GRID_AC_NAME))
        'Me.uc_grid_accounts.Cell(_index, GRID_AC_EMAIL).Value = IIf(IsDBNull(_dr.Item(GRID_AC_EMAIL)), "", _dr.Item(GRID_AC_EMAIL))
        'Me.uc_grid_accounts.Cell(_index, GRID_AC_DATE_BIRTH).Value = IIf(IsDBNull(_dr.Item(GRID_AC_DATE_BIRTH)), "", _dr.Item(GRID_AC_DATE_BIRTH))

        _index = _index + 1

      Next

      If Me.uc_grid_accounts.NumRows > 0 Then
        Me.uc_grid_accounts.SelectFirstRow(True)
        '' Call Me.GetAccountIdSelected()
      End If

    End If

  End Sub

  ' PURPOSE: Show report in richtextbox
  '
  '  PARAMS:
  '     - INPUT:
  '           - DtAccount:
  '           - DtErrors:
  '           - ReportMode:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub ShowAccounts(ByVal DtAccount As DataTable, ByVal DtErrors As DataTable)

    Dim _report As System.Text.StringBuilder
    _report = New System.Text.StringBuilder()

    '' _report.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4978) + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979))

    For Each _dr As DataRow In DtAccount.Rows
      _report.AppendLine(_dr.Item(0).ToString() + " - " + _dr.Item(1).ToString())
    Next

    rtb_report.Text = _report.ToString()

  End Sub

  Private Function GetAccountIds() As String

    Dim _ids As String
    _ids = String.Empty

    If m_dt_acc Is Nothing Then
      Return _ids
    End If

    For Each _dr As DataRow In m_dt_acc.Rows
      If _ids = String.Empty Then
        _ids = _dr.Item(0).ToString()
      Else
        _ids = _ids & "," & _dr.Item(0).ToString()
      End If
    Next

    Return _ids

  End Function

#End Region

#Region "Public Methods"

  ' PURPOSE: Update the progress bar.
  '
  '    - INPUT:
  '         - Count As Int32
  '         - Max As Int32
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub UpdateProgressBar(ByVal Count As Int32, ByVal Max As Int32)
    If m_progress IsNot Nothing Then
      m_progress.SetValues(Count, Max)
    End If
  End Sub ' UpdateProgressBar


#End Region

#Region "Events"

  Private Sub opt_section_CheckedChanged(sender As Object, e As EventArgs) Handles opt_section.CheckedChanged
    If opt_section.Checked Then
      cmb_section.Enabled = True
      cmb_section_type.Enabled = True
      ef_url.Enabled = False
    End If
  End Sub

  Private Sub opt_url_CheckedChanged(sender As Object, e As EventArgs) Handles opt_url.CheckedChanged
    If opt_url.Checked Then
      cmb_section.Enabled = False
      cmb_section_type.Enabled = False
      ef_url.Enabled = True
    End If
  End Sub

  Private Sub cmb_section_ValueChangedEvent() Handles cmb_section.ValueChangedEvent
    LoadTypeSections(cmb_section.Value)
  End Sub

  Private Sub opt_schedule_CheckedChanged(sender As Object, e As EventArgs) Handles opt_schedule.CheckedChanged
    uc_schedule.Visible = opt_schedule.Checked
    chk_enabled.Visible = opt_schedule.Checked
  End Sub

  Private Sub opt_inmediate_CheckedChanged(sender As Object, e As EventArgs) Handles opt_inmediate.CheckedChanged
    uc_schedule.Visible = Not opt_inmediate.Checked
    chk_enabled.Visible = Not opt_inmediate.Checked
  End Sub

  Private Sub opt_filter_CheckedChanged(sender As Object, e As EventArgs) Handles opt_filter.CheckedChanged
    If opt_filter.Checked Then
      pnl_filter.BringToFront()
    End If
  End Sub

  Private Sub opt_customer_CheckedChanged(sender As Object, e As EventArgs) Handles opt_customer.CheckedChanged
    If opt_customer.Checked Then
      pnl_customer.BringToFront()
    End If
  End Sub

  Private Sub opt_customer_group_CheckedChanged(sender As Object, e As EventArgs) Handles opt_customer_group.CheckedChanged
    If opt_customer_group.Checked Then
      pnl_customer_group.BringToFront()
    End If
  End Sub

  Private Sub opt_not_account_CheckedChanged(sender As Object, e As EventArgs) Handles opt_not_account.CheckedChanged
    If opt_not_account.Checked Then
      pnl_not_account.BringToFront()
    End If
  End Sub

  Private Sub opt_all_devices_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_devices.CheckedChanged
    If opt_all_devices.Checked Then
      pnl_not_account.BringToFront()
    End If
  End Sub


  Private Sub chk_by_gender_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_gender.CheckedChanged
    opt_gender_male.Enabled = chk_by_gender.Checked
    opt_gender_female.Enabled = chk_by_gender.Checked
  End Sub

  Private Sub chk_by_birth_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_birth_date.CheckedChanged
    opt_birth_date_only.Enabled = chk_by_birth_date.Checked
    opt_birth_month_only.Enabled = chk_by_birth_date.Checked
  End Sub

  Private Sub chk_by_age_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_age.CheckedChanged
    opt_birth_include_age.Enabled = chk_by_age.Checked
    opt_birth_exclude_age.Enabled = chk_by_age.Checked
    If Not chk_by_age.Checked Then
      ef_age_from2.Enabled = False
      ef_age_from1.Enabled = False
      ef_age_to2.Enabled = False
      ef_age_to1.Enabled = False
      ef_age_from2.Value = ""
      ef_age_from1.Value = ""
      ef_age_to2.Value = ""
      ef_age_to1.Value = ""
    ElseIf opt_birth_include_age.Checked Then
      ef_age_from1.Enabled = True
      ef_age_to1.Enabled = True
    ElseIf opt_birth_exclude_age.Checked Then
      ef_age_from2.Enabled = True
      ef_age_to2.Enabled = True
    End If
  End Sub

  'FBA 08-OCT-2013 new age range filter
  Private Sub opt_include_exclude_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_birth_include_age.CheckedChanged, opt_birth_exclude_age.CheckedChanged
    ef_age_from2.Enabled = opt_birth_exclude_age.Checked
    ef_age_from1.Enabled = opt_birth_include_age.Checked
    ef_age_to2.Enabled = opt_birth_exclude_age.Checked
    ef_age_to1.Enabled = opt_birth_include_age.Checked
    If Not ef_age_from2.Enabled And Not ef_age_to2.Enabled And Not ef_age_from1.Enabled And Not ef_age_to1.Enabled Then
      ef_age_from1.Value = ""
      ef_age_to1.Value = ""
      ef_age_from2.Value = ""
      ef_age_to2.Value = ""
    ElseIf Not ef_age_from2.Enabled And Not ef_age_to2.Enabled Then
      ef_age_from2.Value = ""
      ef_age_to2.Value = ""
    ElseIf Not ef_age_from1.Enabled And Not ef_age_to1.Enabled Then
      ef_age_from1.Value = ""
      ef_age_to1.Value = ""
    End If
  End Sub

  ' AVZ 01-OCT-2015: new filter by created account date
  Private Sub chk_by_created_account_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_created_account.CheckedChanged
    opt_created_account_date.Enabled = chk_by_created_account.Checked
    opt_created_account_anniversary.Enabled = chk_by_created_account.Checked
    If chk_by_created_account.Checked Then
      EnableCreatedAccountDateOptions()
    Else
      opt_created_account_working_day_only.Enabled = False
      opt_created_account_working_day_plus.Enabled = False
      ef_created_account_working_day_plus.Enabled = False
      opt_created_account_anniversary_whole_month.Enabled = False
      opt_created_account_anniversary_day_month.Enabled = False
      chk_by_anniversary_range_of_years.Enabled = False
      ef_created_account_anniversary_year_from.Enabled = False
      ef_created_account_anniversary_year_to.Enabled = False
    End If
  End Sub

  Private Sub opt_created_account_date_CheckedChanged(sender As Object, e As EventArgs) Handles opt_created_account_date.CheckedChanged
    EnableCreatedAccountDateOptions()
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub opt_created_account_anniversary_CheckedChanged(sender As Object, e As EventArgs) Handles opt_created_account_anniversary.CheckedChanged
    EnableCreatedAccountDateOptions()
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub chk_by_anniversary_range_of_years_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_anniversary_range_of_years.CheckedChanged
    ef_created_account_anniversary_year_from.Enabled = chk_by_anniversary_range_of_years.Checked
    ef_created_account_anniversary_year_to.Enabled = chk_by_anniversary_range_of_years.Checked
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub EnableCreatedAccountDateOptions()
    opt_created_account_working_day_only.Enabled = opt_created_account_date.Checked
    opt_created_account_working_day_plus.Enabled = opt_created_account_date.Checked
    ef_created_account_working_day_plus.Enabled = opt_created_account_date.Checked
    opt_created_account_anniversary_whole_month.Enabled = opt_created_account_anniversary.Checked
    opt_created_account_anniversary_day_month.Enabled = opt_created_account_anniversary.Checked
    chk_by_anniversary_range_of_years.Enabled = opt_created_account_anniversary.Checked
    ef_created_account_anniversary_year_from.Enabled = opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked
    ef_created_account_anniversary_year_to.Enabled = opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked
  End Sub

  Private Sub chk_by_level_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_level.CheckedChanged
    chk_level_anonymous.Enabled = chk_by_level.Checked
    chk_level_01.Enabled = chk_by_level.Checked
    chk_level_02.Enabled = chk_by_level.Checked
    chk_level_03.Enabled = chk_by_level.Checked
    chk_level_04.Enabled = chk_by_level.Checked
  End Sub

  Private Sub chk_by_freq_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_freq.CheckedChanged
    ef_freq_last_days.Enabled = chk_by_freq.Checked
    ef_freq_min_days.Enabled = chk_by_freq.Checked
    ef_freq_min_cash_in.Enabled = chk_by_freq.Checked
  End Sub


  Private Sub btn_find_accounts_Click(sender As Object, e As EventArgs) Handles btn_find_accounts.Click
    If Me.uc_account_sel.Validate Then
      LoadGridAccounts(Me.uc_account_sel.GetFilterSQL)
    End If

  End Sub

  Private Sub uc_grid_accounts_RowSelectedEvent(SelectedRow As Integer) Handles uc_grid_accounts.RowSelectedEvent
    ''GetAccountIdSelected()
  End Sub


  Private Sub btn_import_ClickEvent() Handles btn_import.ClickEvent

    Dim _filename As String
    Dim _open_file As System.Windows.Forms.OpenFileDialog
    Dim _report_mode As WSI.Common.AccountsImport.REPORT_MODE

    _open_file = New System.Windows.Forms.OpenFileDialog

    If Not IsNothing(m_dt_acc) Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1245), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

        Return
      End If
    End If

    If Not String.IsNullOrEmpty(m_current_directory) Then
      _open_file.InitialDirectory = m_current_directory
    End If

    _open_file.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1241)
    _open_file.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1242)
    If _open_file.ShowDialog() <> Windows.Forms.DialogResult.OK Then

      Return
    End If

    _filename = _open_file.FileName
    m_current_directory = Path.GetDirectoryName(_filename)

    m_progress = New frm_excel_progress()
    m_progress.Show()

    Cursor = Cursors.WaitCursor
    Call EnableButtons(False)

    m_dt_acc = Nothing
    m_dt_error = Nothing
    If Not WSI.Common.AccountsImport.ImportForAccountSummary(_filename, m_dt_acc, m_dt_error, AddressOf UpdateProgressBar) Then
      _report_mode = AccountsImport.REPORT_MODE.ONLY_ERRORS
    End If

    Call EnableButtons(True)
    Cursor = Cursors.Default

    m_progress.Close()
    m_progress.Dispose()
    m_progress = Nothing

    If m_dt_acc Is Nothing Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1268), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
    ElseIf m_dt_acc.Rows.Count = 0 Then
      m_dt_acc = Nothing
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1268), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Call ShowAccounts(m_dt_acc, m_dt_error)
    End If
  End Sub

  Private Sub btn_sent_Click(sender As Object, e As EventArgs) Handles btn_sent.Click

    m_screen_data_ok = False
    Me.chk_enabled.Checked = True
    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7894), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then

      MyBase.CloseOnOkClick = False
      Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
      MyBase.CloseOnOkClick = True

      If Me.DbStatus = ENUM_STATUS.STATUS_OK AndAlso m_class_base.Id > 0 And m_screen_data_ok Then

        If Not m_class_base.CheckNotificationToSent(m_class_base.Id) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7896), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7895), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
        End If

        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
          Me.Close()
        End If

      End If

    End If

  End Sub

  Private Sub uc_image_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles uc_image.ImageSelected
    Me.m_class_base.ImageFileName = String.Empty
  End Sub

  Private Sub optInformative_CheckedChanged(sender As Object, e As EventArgs) Handles optInformative.CheckedChanged
    If optInformative.Checked Then
      cmb_section.Enabled = False
      cmb_section_type.Enabled = False
      ef_url.Enabled = False
    End If
  End Sub

  Private Sub ef_freq_min_days_Leave(sender As Object, e As EventArgs) Handles ef_freq_min_days.Leave
    RechargeFreqLabels()
  End Sub
  Private Sub ef_freq_last_days_Leave(sender As Object, e As EventArgs) Handles ef_freq_last_days.Leave
    RechargeFreqLabels()
  End Sub
  Private Sub ef_freq_min_cash_in_Leave(sender As Object, e As EventArgs) Handles ef_freq_min_cash_in.Leave
    RechargeFreqLabels()
  End Sub
#End Region


End Class