<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bank_transaction_data_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_bank_transaction_data_sel))
    Me.gb_movement_type = New System.Windows.Forms.GroupBox()
    Me.chk_card_replacement = New System.Windows.Forms.CheckBox()
    Me.chk_card_payment = New System.Windows.Forms.CheckBox()
    Me.chk_cash_advance = New System.Windows.Forms.CheckBox()
    Me.chk_recharge = New System.Windows.Forms.CheckBox()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.opt_type_check = New System.Windows.Forms.CheckBox()
    Me.opt_type_card = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_movement_type.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_movement_type)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1164, 177)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_movement_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 181)
    Me.panel_data.Size = New System.Drawing.Size(1164, 437)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1158, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1158, 4)
    '
    'gb_movement_type
    '
    Me.gb_movement_type.Controls.Add(Me.chk_card_replacement)
    Me.gb_movement_type.Controls.Add(Me.chk_card_payment)
    Me.gb_movement_type.Controls.Add(Me.chk_cash_advance)
    Me.gb_movement_type.Controls.Add(Me.chk_recharge)
    Me.gb_movement_type.Location = New System.Drawing.Point(548, 71)
    Me.gb_movement_type.Name = "gb_movement_type"
    Me.gb_movement_type.Size = New System.Drawing.Size(186, 101)
    Me.gb_movement_type.TabIndex = 3
    Me.gb_movement_type.TabStop = False
    Me.gb_movement_type.Text = "xMovementType"
    '
    'chk_card_replacement
    '
    Me.chk_card_replacement.Location = New System.Drawing.Point(6, 79)
    Me.chk_card_replacement.Name = "chk_card_replacement"
    Me.chk_card_replacement.Size = New System.Drawing.Size(150, 16)
    Me.chk_card_replacement.TabIndex = 3
    Me.chk_card_replacement.Text = "xCardReplacement"
    '
    'chk_card_payment
    '
    Me.chk_card_payment.Location = New System.Drawing.Point(6, 59)
    Me.chk_card_payment.Name = "chk_card_payment"
    Me.chk_card_payment.Size = New System.Drawing.Size(150, 16)
    Me.chk_card_payment.TabIndex = 2
    Me.chk_card_payment.Text = "xCardPayment"
    '
    'chk_cash_advance
    '
    Me.chk_cash_advance.Location = New System.Drawing.Point(6, 39)
    Me.chk_cash_advance.Name = "chk_cash_advance"
    Me.chk_cash_advance.Size = New System.Drawing.Size(150, 16)
    Me.chk_cash_advance.TabIndex = 1
    Me.chk_cash_advance.Text = "xCashAdvance"
    '
    'chk_recharge
    '
    Me.chk_recharge.Location = New System.Drawing.Point(6, 19)
    Me.chk_recharge.Name = "chk_recharge"
    Me.chk_recharge.Size = New System.Drawing.Size(150, 16)
    Me.chk_recharge.TabIndex = 0
    Me.chk_recharge.Text = "xRecharge"
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(267, 3)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(272, 80)
    Me.gb_user.TabIndex = 1
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(81, 49)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 18)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 44)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 18)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 3)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(255, 80)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 17)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(740, 0)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = False
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 108)
    Me.uc_account_sel1.TabIndex = 4
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.opt_type_check)
    Me.gb_type.Controls.Add(Me.opt_type_card)
    Me.gb_type.Location = New System.Drawing.Point(548, 3)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(186, 68)
    Me.gb_type.TabIndex = 2
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xType"
    '
    'opt_type_check
    '
    Me.opt_type_check.Location = New System.Drawing.Point(6, 43)
    Me.opt_type_check.Name = "opt_type_check"
    Me.opt_type_check.Size = New System.Drawing.Size(150, 16)
    Me.opt_type_check.TabIndex = 1
    Me.opt_type_check.Text = "xCashAdvance"
    '
    'opt_type_card
    '
    Me.opt_type_card.Location = New System.Drawing.Point(6, 23)
    Me.opt_type_card.Name = "opt_type_card"
    Me.opt_type_card.Size = New System.Drawing.Size(150, 16)
    Me.opt_type_card.TabIndex = 0
    Me.opt_type_card.Text = "xRecharge"
    '
    'frm_bank_transaction_data_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1174, 622)
    Me.Name = "frm_bank_transaction_data_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_bank_transaction_data_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_movement_type.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_movement_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_cash_advance As System.Windows.Forms.CheckBox
  Friend WithEvents chk_recharge As System.Windows.Forms.CheckBox
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_type_check As System.Windows.Forms.CheckBox
  Friend WithEvents opt_type_card As System.Windows.Forms.CheckBox
  Friend WithEvents chk_card_replacement As System.Windows.Forms.CheckBox
  Friend WithEvents chk_card_payment As System.Windows.Forms.CheckBox
End Class
