'--------------------------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_area_bank
'
'   DESCRIPTION :  Area & Bank/Island
'
'        AUTHOR :  Javier G�mez Rubio
'
' CREATION DATE :  17-FEB-2012
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 17-FEB-2012  JGR    Initial version
' 23-FEB-2012  JGR    Add New Columns: Number Terminals (by Area - Bank/Island) and Banks
' 20-SEP-2012  JAB    Fixed bug, when island�s name has a "'" fails.
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 05-JUN-2013  DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
' 21-FEB-2014  AMF    Fixed Bug WIG-161: Alignment correct in column name island
' 28-OCT-2016  OVS    Extends lenght of ef_bank_filter, from 3 to 15 and accepts alphanumeric
' 08-NOV-2017  XGJ    Wigos-4096 MobiBank: GUI management - Runners and zones/banks (Add new runner)
' 28-NOV-2017  FGB    WIGOS-3390: MobiBank: GUI management - Runners and zones/banks (Report)
' 20-MAR-2018  RGR    Bug 31999:WIGOS-8763 "�reas e islas" -> Button "Nueva �rea" is always disabled 
'---------------------------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class frm_area_bank_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call
  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_TYPE As Integer = 0
  Private Const SQL_COLUMN_AREA_ID As Integer = 1
  Private Const SQL_COLUMN_AREA_NAME As Integer = 2
  Private Const SQL_COLUMN_AREA_SMOKING As Integer = 3
  Private Const SQL_COLUMN_BANK_ID As Integer = 4
  Private Const SQL_COLUMN_BANK_NAME As Integer = 5
  Private Const SQL_COLUMN_NUM_BANKS As Integer = 6
  Private Const SQL_COLUMN_NUM_TERMS As Integer = 7
  Private Const SQL_COLUMN_NUM_RUNNERS As Integer = 8

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_TYPE As Integer = 1
  Private Const GRID_COLUMN_AREA_ID As Integer = 2
  Private Const GRID_COLUMN_AREA_NAME As Integer = 3
  Private Const GRID_COLUMN_AREA_SMOKING As Integer = 4
  Private Const GRID_COLUMN_AREA_NUM_BANKS As Integer = 5
  Private Const GRID_COLUMN_AREA_NUM_TERMS As Integer = 6
  Private Const GRID_COLUMN_BANK_ID As Integer = 7
  Private Const GRID_COLUMN_BANK_NAME As Integer = 8
  Private Const GRID_COLUMN_BANK_NUM_TERMS As Integer = 9
  Private Const GRID_COLUMN_BANK_NUM_RUNNERS As Integer = 10

  Private Const GRID_COLUMNS As Integer = 11
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_TYPE As Integer = 1200
  Private Const GRID_WIDTH_AREA_ID As Integer = 1000
  Private Const GRID_WIDTH_AREA_NAME As Integer = 2600
  Private Const GRID_WIDTH_AREA_SMOKING As Integer = 1500
  Private Const GRID_WIDTH_AREA_NUM_BANKS As Integer = 800
  Private Const GRID_WIDTH_AREA_NUM_TERMS As Integer = 1300
  Private Const GRID_WIDTH_BANK_ID As Integer = 1000
  Private Const GRID_WIDTH_BANK_NAME As Integer = 1600
  Private Const GRID_WIDTH_BANK_NUM_TERMS As Integer = 1300
  Private Const GRID_WIDTH_BANK_NUM_RUNNERS As Integer = 1300

  ' Area Smoking (TRUE/FALSE)
  Private Const AREA_SMOKING As Boolean = True
  Private Const AREA_NO_SMOKING As Boolean = False

  ' Type (Area - Bank/Island)
  Private Const TYPE_AREA As String = "AREA"
  Private Const TYPE_BANK As String = "BANK"

  ' Default row (Area - Bank/Island)
  Private Const DEFAULT_ROW As Integer = 0

#End Region ' Constants

#Region " Member"

  ' Report filters
  Private m_area As String
  Private m_bank As String
  Private m_zone As String
  Private m_bank_runner As Integer

  ' Mobibank in user mode?
  Private m_is_mobibank_enabled As Boolean

  ' Totals
  Private m_total_terms As Int32
  Private m_total_banks As Int32
#End Region ' Member

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_AREA_BANK

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_is_mobibank_enabled = WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice

    ' Area & Bank/Island
    Me.Text = GLB_NLS_GUI_CONTROLS.GetString(425)

    ' Buttons
    ' New Bank/Island
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(30)
    ' New Area
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(29)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True

    ' XGJ 08-NOV-2017 MobiBank: GUI management - Runners and zones/banks (Add new runner)
    ' Runners button
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8767)

    ' Select / New / Cancel
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Area Name filter
    Me.ef_area_name.Text = GLB_NLS_GUI_CONTROLS.GetString(426)
    Me.ef_area_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Bank Name filter
    Me.ef_bank_name.Text = GLB_NLS_GUI_CONTROLS.GetString(427)
    Me.ef_bank_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 12)

    ' Zone filter (Smoking / No Smoking)
    Me.gb_zone.Text = GLB_NLS_GUI_CONTROLS.GetString(439)
    Me.chk_smoking.Text = GLB_NLS_GUI_CONTROLS.GetString(428)
    Me.chk_noSmoking.Text = GLB_NLS_GUI_CONTROLS.GetString(440)

    ' Resize if mobibank
    If m_is_mobibank_enabled Then
      Me.Size = New Size(Me.Size.Width + ConvertTwipsToPixels(GRID_WIDTH_BANK_NUM_RUNNERS),
                         Me.Size.Height)
    End If


    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = m_is_mobibank_enabled

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Convert twips to pixels (more or less exact)
  '''   uc_grid columns width are in twips
  ''' </summary>
  ''' <param name="WidthInTiwps"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConvertTwipsToPixels(ByVal WidthInTiwps As Integer) As Integer
    Return (WidthInTiwps * (96 / 1440))
  End Function

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _id_area_bank As Integer
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    ' If no row is selected
    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Area - Bank/Island ID, and launch the editor
    Select Case Me.Grid.Cell(_idx_row, GRID_COLUMN_TYPE).Value
      Case TYPE_AREA
        _frm_edit = New frm_area_edit
        _id_area_bank = Me.Grid.Cell(_idx_row, GRID_COLUMN_AREA_ID).Value

      Case TYPE_BANK
        _frm_edit = New frm_island_edit
        _id_area_bank = Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_ID).Value

      Case Else
        Return
    End Select

    Call _frm_edit.ShowEditItem(_id_area_bank)

    'Call RefreshGrid()

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem


  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database
  '
  '   NOTES :
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder

    ' Get Select and from
    _sb.AppendLine(" SELECT   *                                                     ")
    _sb.AppendLine("   FROM                                                         ")
    _sb.AppendLine(" (                                                              ")
    _sb.AppendLine("       SELECT   'BANK'  AS  TYPE                                ")
    _sb.AppendLine("              , AR_AREA_ID                                      ")
    _sb.AppendLine("              , AR_NAME                                         ")
    _sb.AppendLine("              , AR_SMOKING                                      ")
    _sb.AppendLine("              , BK_BANK_ID                                      ")
    _sb.AppendLine("              , BK_NAME                                         ")
    _sb.AppendLine("              , 0  AS  NUM_BANKS                                ")
    _sb.AppendLine("              , ( SELECT   COUNT(*)                             ")
    _sb.AppendLine("                    FROM   TERMINALS                            ")
    _sb.AppendLine("                   WHERE   TE_BANK_ID = BK_BANK_ID              ")
    _sb.AppendLine("                )  AS  NUM_TERMS                                ")
    _sb.AppendLine("              , ( SELECT   COUNT(*)                             ")
    _sb.AppendLine("                    FROM   MOBIBANK_AREAS                       ")
    _sb.AppendLine("                   WHERE   MBA_BANK_ID = BK_BANK_ID            ")
    _sb.AppendLine("                )  AS  NUM_RUNNERS                              ")
    _sb.AppendLine("         FROM   BANKS                                           ")
    _sb.AppendLine("   INNER JOIN   AREAS  ON  BK_AREA_ID = AR_AREA_ID              ")
    _sb.AppendLine("        UNION                                                   ")
    _sb.AppendLine("       SELECT   'AREA'  AS  TYPE                                ")
    _sb.AppendLine("              , AR_AREA_ID                                      ")
    _sb.AppendLine("              , AR_NAME                                         ")
    _sb.AppendLine("              , AR_SMOKING                                      ")
    _sb.AppendLine("              , NULL  AS  BK_BANK_ID                            ")
    _sb.AppendLine("              , NULL  AS  BK_NAME                               ")
    _sb.AppendLine("              , ( SELECT   COUNT(*)                             ")
    _sb.AppendLine("                    FROM   BANKS                                ")
    _sb.AppendLine("                   WHERE   BK_AREA_ID = AR_AREA_ID              ")
    _sb.AppendLine("                )  AS  NUM_BANKS                                ")
    _sb.AppendLine("              , ( SELECT   COUNT(*)                             ")
    _sb.AppendLine("                    FROM   TERMINALS                            ")
    _sb.AppendLine("              INNER JOIN   BANKS  ON  TE_BANK_ID = BK_BANK_ID   ")
    _sb.AppendLine("                   WHERE   BK_AREA_ID = AR_AREA_ID              ")
    _sb.AppendLine("                )  AS  NUM_TERMS                                ")
    _sb.AppendLine("              , ( SELECT   COUNT(*)                             ")
    _sb.AppendLine("                    FROM   MOBIBANK_AREAS                       ")
    _sb.AppendLine("              INNER JOIN   BANKS  ON  MBA_BANK_ID = BK_BANK_ID ")
    _sb.AppendLine("                   WHERE   BK_AREA_ID = AR_AREA_ID              ")
    _sb.AppendLine("                )  AS  NUM_RUNNERS                              ")
    _sb.AppendLine("         FROM   AREAS                                           ")
    _sb.AppendLine(" ) AI                                                           ")

    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine(" ORDER BY AR_NAME, BK_NAME                                      ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _area_smoking As String

    ' Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = DbRow.Value(SQL_COLUMN_TYPE)

    If DbRow.Value(SQL_COLUMN_AREA_ID) = DEFAULT_ROW Then
      ' Area default
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_02)
    Else
      ' Color row (Type: Area - Bank/Island)
      Select Case Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value
        Case TYPE_AREA
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

        Case TYPE_BANK
          Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      End Select
    End If

    ' Area ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA_ID).Value = DbRow.Value(SQL_COLUMN_AREA_ID)

    ' Area Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA_NAME).Value = DbRow.Value(SQL_COLUMN_AREA_NAME)

    ' Area Smoking (Zone)
    _area_smoking = ""
    Select Case DbRow.Value(SQL_COLUMN_AREA_SMOKING)
      Case AREA_SMOKING
        _area_smoking = GLB_NLS_GUI_CONTROLS.GetString(428)

      Case AREA_NO_SMOKING
        _area_smoking = GLB_NLS_GUI_CONTROLS.GetString(440)
    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA_SMOKING).Value = _area_smoking

    ' Num Banks (by Area)
    If Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = TYPE_AREA Then
      m_total_banks += Math.Truncate(DbRow.Value(SQL_COLUMN_NUM_BANKS))

      Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA_NUM_BANKS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_BANKS), 0)
    End If

    ' Bank ID
    If Not DbRow.IsNull(SQL_COLUMN_BANK_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_ID).Value = DbRow.Value(SQL_COLUMN_BANK_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_ID).Value = ""
    End If

    ' Bank Name
    If Not DbRow.IsNull(SQL_COLUMN_BANK_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NAME).Value = DbRow.Value(SQL_COLUMN_BANK_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NAME).Value = ""
    End If

    ' Num Terminals
    Select Case Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value
      Case TYPE_AREA
        'Num Terminals (by Area)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AREA_NUM_TERMS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_TERMS), 0)

      Case TYPE_BANK
        'Num Terminals (by Bank/Island)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NUM_TERMS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_TERMS), 0)
        m_total_terms += Math.Truncate(DbRow.Value(SQL_COLUMN_NUM_TERMS))
    End Select

    ' Num Runners
    Select Case Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value
      Case TYPE_AREA
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NUM_RUNNERS).Value = ""

      Case TYPE_BANK
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NUM_RUNNERS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_RUNNERS), 0)
    End Select

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '         - None
  '
  ' RETURNS:
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    ' Options :
    '   BUTTON_CUSTOM_0:  New Bank/Island
    '   BUTTON_CUSTOM_1:  New Area
    '   BUTTON_CUSTOM_2:  New Runner
    '   BUTTON_SELECT:    Edit Area - Bank/Island
    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call EditNewBank()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call EditNewArea()

        ' XGJ 08-NOV-2017
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call AddRunner()

      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    Dim _grid_value As Int32

    _grid_value = 0

    If Not m_is_mobibank_enabled Then
      Exit Sub
    End If

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    If IsNumeric(Me.Grid.Cell(SelectedRow, GRID_COLUMN_BANK_ID).Value) Then
      If (CurrentUser.Permissions(ENUM_FORM.FORM_MOBIBANK_USER_ASSIGNATION).Read) Then
        m_bank_runner = CInt(Me.Grid.Cell(SelectedRow, GRID_COLUMN_BANK_ID).Value)
        Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True
      End If
    End If


  End Sub  ' GUI_RowSelectedEvent

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - MdiParent
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ''' <summary>
  ''' Begore first row: Initialize totals
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call InitializeTotals()
  End Sub ' GUI_BeforeFirsRow

  ''' <summary>
  ''' Initialize totals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeTotals()
    m_total_banks = 0
    m_total_terms = 0
  End Sub

  ''' <summary>
  ''' After last row: Show totals
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer
    _idx_row = Me.Grid.AddRow()

    ' Color yellow
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_AREA_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Num banks
    Me.Grid.Cell(_idx_row, GRID_COLUMN_AREA_NUM_BANKS).Value = GUI_FormatNumber(m_total_banks, 0)

    ' Num terminals
    Me.Grid.Cell(_idx_row, GRID_COLUMN_AREA_NUM_TERMS).Value = GUI_FormatNumber(m_total_terms, 0)
  End Sub ' GUI_AfterLastRow

#End Region ' Public Functions

#Region " GUI Reports "

  ' PURPOSE : Set proper values for form filters being sent to the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - PrintData
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Area - Bank/Island filter
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(426), m_area)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(427), m_bank)

    ' Zone filter (Smoking / No Smoking)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(439), m_zone)

    ' With
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(2) = 1800
    PrintData.FilterHeaderWidth(3) = 1800
    PrintData.FilterValueWidth(3) = 3200

  End Sub ' GUI_ReportFilter

  ' PURPOSE : Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Area filter
    m_area = Me.ef_area_name.Value

    ' Bank/Island filter
    m_bank = Me.ef_bank_name.Value

    ' Zone filter (Smoking / No Smoking)
    m_zone = GetZone(False)

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Private Functions "

  ' PURPOSE: Open area edit form to create New Area
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  '   NOTES :
  '
  Private Sub EditNewArea()

    Dim _idx_row As Int32
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    _frm_edit = New frm_area_edit
    Call _frm_edit.ShowNewItem()

    'Call RefreshGrid()

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' EditNewArea

  Private Sub AddRunner()
    Dim _frm_edit As Object

    'Dim _idx_row As Int32
    '' Search the first row selected
    'For _idx_row = 0 To Me.Grid.NumRows - 1
    '  If Me.Grid.Row(_idx_row).IsSelected Then
    '    _bank = Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_ID).Value
    '    Exit For
    '  End If
    'Next

    _frm_edit = New frm_users_assignation
    'Call _frm_edit.ShowForEdit()
    Call _frm_edit.Show(m_bank_runner)

    'Call RefreshGrid()

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' AddRunner

  ' PURPOSE : Open bank/island edit form to create New Bank/Island
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub EditNewBank()
    Dim _idx_row As Int32
    Dim _area_id As Integer

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      ' If no row is selected create a new area without bank preselected
      _area_id = DEFAULT_ROW
    Else
      ' If a row is selected create a new area with the same bank preselected
      If Me.Grid.Cell(_idx_row, GRID_COLUMN_AREA_ID).Value <> "" Then
        _area_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_AREA_ID).Value
      Else
        _area_id = DEFAULT_ROW
      End If
    End If

    Using _frm_edit As New frm_island_edit
      Call _frm_edit.ShowNewItem(_area_id)
    End Using

    'Call RefreshGrid()

    Call Me.Grid.Focus()

  End Sub ' EditNewBank

  ' PURPOSE: Refresh grid information
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  '   NOTES :
  '
  Private Sub RefreshGrid()

    GUI_ExecuteQuery()

  End Sub ' RefreshGrid

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Disable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Type
      .Column(GRID_COLUMN_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_TYPE).Width = 0

      ' Area ID
      .Column(GRID_COLUMN_AREA_ID).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(426)
      .Column(GRID_COLUMN_AREA_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_AREA_ID).Width = 0

      ' Area Name
      .Column(GRID_COLUMN_AREA_NAME).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(426)
      .Column(GRID_COLUMN_AREA_NAME).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(251)
      .Column(GRID_COLUMN_AREA_NAME).Width = GRID_WIDTH_AREA_NAME
      .Column(GRID_COLUMN_AREA_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Area Smoking (Zone)
      .Column(GRID_COLUMN_AREA_SMOKING).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(426)
      .Column(GRID_COLUMN_AREA_SMOKING).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(439)
      .Column(GRID_COLUMN_AREA_SMOKING).Width = GRID_WIDTH_AREA_SMOKING
      .Column(GRID_COLUMN_AREA_SMOKING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Num Banks/Islands (by Area)
      .Column(GRID_COLUMN_AREA_NUM_BANKS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(426)
      .Column(GRID_COLUMN_AREA_NUM_BANKS).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(430)
      .Column(GRID_COLUMN_AREA_NUM_BANKS).Width = GRID_WIDTH_AREA_NUM_BANKS
      .Column(GRID_COLUMN_AREA_NUM_BANKS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Num Terminals (by Area)
      .Column(GRID_COLUMN_AREA_NUM_TERMS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(426)
      .Column(GRID_COLUMN_AREA_NUM_TERMS).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(435)
      .Column(GRID_COLUMN_AREA_NUM_TERMS).Width = GRID_WIDTH_AREA_NUM_TERMS
      .Column(GRID_COLUMN_AREA_NUM_TERMS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Bank/Island ID
      .Column(GRID_COLUMN_BANK_ID).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(427)
      .Column(GRID_COLUMN_BANK_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_BANK_ID).Width = 0

      ' Bank/Island Name
      .Column(GRID_COLUMN_BANK_NAME).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(427)
      .Column(GRID_COLUMN_BANK_NAME).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(251)
      .Column(GRID_COLUMN_BANK_NAME).Width = GRID_WIDTH_BANK_NAME
      .Column(GRID_COLUMN_BANK_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Num Terminals (by Island)
      .Column(GRID_COLUMN_BANK_NUM_TERMS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(427)
      .Column(GRID_COLUMN_BANK_NUM_TERMS).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(435)
      .Column(GRID_COLUMN_BANK_NUM_TERMS).Width = GRID_WIDTH_BANK_NUM_TERMS
      .Column(GRID_COLUMN_BANK_NUM_TERMS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Num Runners (only for MobiBank)
      .Column(GRID_COLUMN_BANK_NUM_RUNNERS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8767) 'Runners
      .Column(GRID_COLUMN_BANK_NUM_RUNNERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8782) 'Quantity
      .Column(GRID_COLUMN_BANK_NUM_RUNNERS).Width = IIf(m_is_mobibank_enabled, GRID_WIDTH_BANK_NUM_RUNNERS, 0)
      .Column(GRID_COLUMN_BANK_NUM_RUNNERS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub SetDefaultValues()

    ' Area Name filter
    Me.ef_area_name.Value = ""

    ' Bank/Islad Name filter
    Me.ef_bank_name.Value = ""

    ' Zone filter (Smoking / No Smoking)
    Me.chk_smoking.Checked = False
    Me.chk_noSmoking.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE : Get Zone list for selected smoking or no
  '           Format list to build query: X, X
  '           X is the Id if GetIds = True
  '           otherwise, X is the Name of the control
  '  PARAMS :
  '     - INPUT :
  '           - GetIds As Boolean
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - String
  '
  '   NOTES :
  '
  Private Function GetZone(Optional ByVal GetIds As Boolean = True) As String

    Dim _list_type As String

    _list_type = ""
    If Me.chk_smoking.Checked Then
      _list_type = IIf(GetIds, "1", Me.chk_smoking.Text)
    End If

    If Me.chk_noSmoking.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If

      _list_type = _list_type & IIf(GetIds, "0", Me.chk_noSmoking.Text)
    End If

    Return _list_type

  End Function ' GetZone

  ' PURPOSE : Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '           main SQL Query
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - String
  '
  '   NOTES :
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    ' Area filter
    If Me.ef_area_name.Value <> "" Then
      _str_where &= " " & GUI_FilterField("AR_NAME", Me.ef_area_name.Value, False, False, True)
    End If

    ' Bank/Island filter
    If Me.ef_bank_name.Value <> "" Then
      If Not String.IsNullOrEmpty(_str_where) Then _str_where = _str_where & " AND "
      _str_where &= " " & GUI_FilterField("BK_NAME", Me.ef_bank_name.Value, True, False, True)
    End If

    ' Zone filter (Smoking / No Smoking)
    If Me.chk_smoking.Checked Or _
       Me.chk_noSmoking.Checked Then
      If Not String.IsNullOrEmpty(_str_where) Then _str_where = _str_where & " AND "
      _str_where = _str_where & " AR_SMOKING IN (" & GetZone() & ") "
    End If

    If Not String.IsNullOrEmpty(_str_where) Then _str_where = " WHERE " & _str_where

    Return _str_where

  End Function ' GetSqlWhere

#End Region ' Private Functions

End Class ' frm_area_bank_sel
