<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cash_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.ef_total_no_redeemable = New GUI_Controls.uc_entry_field()
    Me.ef_total_redeemable = New GUI_Controls.uc_entry_field()
    Me.gb_redeemable = New System.Windows.Forms.GroupBox()
    Me.lbl_captions_a = New System.Windows.Forms.Label()
    Me.lbl_captions_g = New System.Windows.Forms.Label()
    Me.lbl_captions_h = New System.Windows.Forms.Label()
    Me.lbl_captions_t = New System.Windows.Forms.Label()
    Me.lbl_captions_i = New System.Windows.Forms.Label()
    Me.lbl_captions_f = New System.Windows.Forms.Label()
    Me.lbl_captions_e = New System.Windows.Forms.Label()
    Me.lbl_captions_d = New System.Windows.Forms.Label()
    Me.lbl_captions_c = New System.Windows.Forms.Label()
    Me.lbl_captions_b = New System.Windows.Forms.Label()
    Me.lbl_captions_j = New System.Windows.Forms.Label()
    Me.lbl_captions_k = New System.Windows.Forms.Label()
    Me.lbl_captions_l = New System.Windows.Forms.Label()
    Me.lbl_note = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_redeemable.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_note)
    Me.panel_filter.Controls.Add(Me.lbl_captions_l)
    Me.panel_filter.Controls.Add(Me.lbl_captions_k)
    Me.panel_filter.Controls.Add(Me.lbl_captions_j)
    Me.panel_filter.Controls.Add(Me.lbl_captions_b)
    Me.panel_filter.Controls.Add(Me.lbl_captions_c)
    Me.panel_filter.Controls.Add(Me.lbl_captions_d)
    Me.panel_filter.Controls.Add(Me.lbl_captions_e)
    Me.panel_filter.Controls.Add(Me.lbl_captions_f)
    Me.panel_filter.Controls.Add(Me.lbl_captions_i)
    Me.panel_filter.Controls.Add(Me.lbl_captions_t)
    Me.panel_filter.Controls.Add(Me.lbl_captions_h)
    Me.panel_filter.Controls.Add(Me.lbl_captions_g)
    Me.panel_filter.Controls.Add(Me.lbl_captions_a)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_redeemable)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 154)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_redeemable, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_a, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_g, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_h, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_t, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_i, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_f, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_e, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_d, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_c, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_b, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_j, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_k, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_captions_l, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_note, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 158)
    Me.panel_data.Size = New System.Drawing.Size(1226, 542)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Margin = New System.Windows.Forms.Padding(0)
    Me.pn_separator_line.Padding = New System.Windows.Forms.Padding(0)
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 10)
    '
    'pn_line
    '
    Me.pn_line.Location = New System.Drawing.Point(0, 0)
    Me.pn_line.Size = New System.Drawing.Size(1220, 0)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, -3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(260, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'ef_total_no_redeemable
    '
    Me.ef_total_no_redeemable.DoubleValue = 0.0R
    Me.ef_total_no_redeemable.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_total_no_redeemable.IntegerValue = 0
    Me.ef_total_no_redeemable.IsReadOnly = False
    Me.ef_total_no_redeemable.Location = New System.Drawing.Point(6, 45)
    Me.ef_total_no_redeemable.Name = "ef_total_no_redeemable"
    Me.ef_total_no_redeemable.PlaceHolder = Nothing
    Me.ef_total_no_redeemable.Size = New System.Drawing.Size(483, 25)
    Me.ef_total_no_redeemable.SufixText = "Sufix Text"
    Me.ef_total_no_redeemable.SufixTextVisible = True
    Me.ef_total_no_redeemable.TabIndex = 1
    Me.ef_total_no_redeemable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_total_no_redeemable.TextValue = ""
    Me.ef_total_no_redeemable.TextWidth = 350
    Me.ef_total_no_redeemable.Value = ""
    Me.ef_total_no_redeemable.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_total_redeemable
    '
    Me.ef_total_redeemable.DoubleValue = 0.0R
    Me.ef_total_redeemable.IntegerValue = 0
    Me.ef_total_redeemable.IsReadOnly = False
    Me.ef_total_redeemable.Location = New System.Drawing.Point(6, 14)
    Me.ef_total_redeemable.Name = "ef_total_redeemable"
    Me.ef_total_redeemable.PlaceHolder = Nothing
    Me.ef_total_redeemable.Size = New System.Drawing.Size(483, 24)
    Me.ef_total_redeemable.SufixText = "Sufix Text"
    Me.ef_total_redeemable.SufixTextVisible = True
    Me.ef_total_redeemable.TabIndex = 0
    Me.ef_total_redeemable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_total_redeemable.TextValue = ""
    Me.ef_total_redeemable.TextWidth = 350
    Me.ef_total_redeemable.Value = ""
    Me.ef_total_redeemable.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_redeemable
    '
    Me.gb_redeemable.Controls.Add(Me.ef_total_redeemable)
    Me.gb_redeemable.Controls.Add(Me.ef_total_no_redeemable)
    Me.gb_redeemable.Location = New System.Drawing.Point(272, -3)
    Me.gb_redeemable.Name = "gb_redeemable"
    Me.gb_redeemable.Size = New System.Drawing.Size(495, 80)
    Me.gb_redeemable.TabIndex = 2
    Me.gb_redeemable.TabStop = False
    '
    'lbl_captions_a
    '
    Me.lbl_captions_a.AutoSize = True
    Me.lbl_captions_a.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_a.Location = New System.Drawing.Point(5, 83)
    Me.lbl_captions_a.Name = "lbl_captions_a"
    Me.lbl_captions_a.Size = New System.Drawing.Size(72, 13)
    Me.lbl_captions_a.TabIndex = 3
    Me.lbl_captions_a.Text = "xCaptionsA"
    '
    'lbl_captions_g
    '
    Me.lbl_captions_g.AutoSize = True
    Me.lbl_captions_g.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_g.Location = New System.Drawing.Point(501, 83)
    Me.lbl_captions_g.Name = "lbl_captions_g"
    Me.lbl_captions_g.Size = New System.Drawing.Size(73, 13)
    Me.lbl_captions_g.TabIndex = 9
    Me.lbl_captions_g.Text = "xCaptionsG"
    '
    'lbl_captions_h
    '
    Me.lbl_captions_h.AutoSize = True
    Me.lbl_captions_h.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_h.Location = New System.Drawing.Point(501, 99)
    Me.lbl_captions_h.Name = "lbl_captions_h"
    Me.lbl_captions_h.Size = New System.Drawing.Size(72, 13)
    Me.lbl_captions_h.TabIndex = 10
    Me.lbl_captions_h.Text = "xCaptionsH"
    '
    'lbl_captions_t
    '
    Me.lbl_captions_t.AutoSize = True
    Me.lbl_captions_t.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_t.Location = New System.Drawing.Point(765, 131)
    Me.lbl_captions_t.Name = "lbl_captions_t"
    Me.lbl_captions_t.Size = New System.Drawing.Size(71, 13)
    Me.lbl_captions_t.TabIndex = 12
    Me.lbl_captions_t.Text = "xCaptionsT"
    '
    'lbl_captions_i
    '
    Me.lbl_captions_i.AutoSize = True
    Me.lbl_captions_i.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_i.Location = New System.Drawing.Point(501, 115)
    Me.lbl_captions_i.Name = "lbl_captions_i"
    Me.lbl_captions_i.Size = New System.Drawing.Size(69, 13)
    Me.lbl_captions_i.TabIndex = 11
    Me.lbl_captions_i.Text = "xCaptionsI"
    '
    'lbl_captions_f
    '
    Me.lbl_captions_f.AutoSize = True
    Me.lbl_captions_f.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_f.Location = New System.Drawing.Point(279, 115)
    Me.lbl_captions_f.Name = "lbl_captions_f"
    Me.lbl_captions_f.Size = New System.Drawing.Size(70, 13)
    Me.lbl_captions_f.TabIndex = 8
    Me.lbl_captions_f.Text = "xCaptionsF"
    '
    'lbl_captions_e
    '
    Me.lbl_captions_e.AutoSize = True
    Me.lbl_captions_e.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_e.Location = New System.Drawing.Point(279, 99)
    Me.lbl_captions_e.Name = "lbl_captions_e"
    Me.lbl_captions_e.Size = New System.Drawing.Size(71, 13)
    Me.lbl_captions_e.TabIndex = 7
    Me.lbl_captions_e.Text = "xCaptionsE"
    '
    'lbl_captions_d
    '
    Me.lbl_captions_d.AutoSize = True
    Me.lbl_captions_d.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_d.Location = New System.Drawing.Point(279, 83)
    Me.lbl_captions_d.Name = "lbl_captions_d"
    Me.lbl_captions_d.Size = New System.Drawing.Size(73, 13)
    Me.lbl_captions_d.TabIndex = 6
    Me.lbl_captions_d.Text = "xCaptionsD"
    '
    'lbl_captions_c
    '
    Me.lbl_captions_c.AutoSize = True
    Me.lbl_captions_c.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_c.Location = New System.Drawing.Point(5, 115)
    Me.lbl_captions_c.Name = "lbl_captions_c"
    Me.lbl_captions_c.Size = New System.Drawing.Size(73, 13)
    Me.lbl_captions_c.TabIndex = 5
    Me.lbl_captions_c.Text = "xCaptionsC"
    '
    'lbl_captions_b
    '
    Me.lbl_captions_b.AutoSize = True
    Me.lbl_captions_b.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_b.Location = New System.Drawing.Point(5, 99)
    Me.lbl_captions_b.Name = "lbl_captions_b"
    Me.lbl_captions_b.Size = New System.Drawing.Size(72, 13)
    Me.lbl_captions_b.TabIndex = 4
    Me.lbl_captions_b.Text = "xCaptionsB"
    '
    'lbl_captions_j
    '
    Me.lbl_captions_j.AutoSize = True
    Me.lbl_captions_j.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_j.Location = New System.Drawing.Point(764, 83)
    Me.lbl_captions_j.Name = "lbl_captions_j"
    Me.lbl_captions_j.Size = New System.Drawing.Size(69, 13)
    Me.lbl_captions_j.TabIndex = 13
    Me.lbl_captions_j.Text = "xCaptionsJ"
    '
    'lbl_captions_k
    '
    Me.lbl_captions_k.AutoSize = True
    Me.lbl_captions_k.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_k.Location = New System.Drawing.Point(764, 99)
    Me.lbl_captions_k.Name = "lbl_captions_k"
    Me.lbl_captions_k.Size = New System.Drawing.Size(72, 13)
    Me.lbl_captions_k.TabIndex = 14
    Me.lbl_captions_k.Text = "xCaptionsK"
    '
    'lbl_captions_l
    '
    Me.lbl_captions_l.AutoSize = True
    Me.lbl_captions_l.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_captions_l.Location = New System.Drawing.Point(765, 115)
    Me.lbl_captions_l.Name = "lbl_captions_l"
    Me.lbl_captions_l.Size = New System.Drawing.Size(70, 13)
    Me.lbl_captions_l.TabIndex = 15
    Me.lbl_captions_l.Text = "xCaptionsL"
    '
    'lbl_note
    '
    Me.lbl_note.AutoSize = True
    Me.lbl_note.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_note.Location = New System.Drawing.Point(5, 131)
    Me.lbl_note.Name = "lbl_note"
    Me.lbl_note.Size = New System.Drawing.Size(105, 13)
    Me.lbl_note.TabIndex = 16
    Me.lbl_note.Text = "x*: Remember..."
    '
    'frm_cash_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1234, 704)
    Me.Name = "frm_cash_report"
    Me.Text = "frm_cash_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_redeemable.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents ef_total_no_redeemable As GUI_Controls.uc_entry_field
  Friend WithEvents ef_total_redeemable As GUI_Controls.uc_entry_field
  Friend WithEvents gb_redeemable As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_captions_a As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_b As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_c As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_d As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_e As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_f As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_i As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_t As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_h As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_g As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_j As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_k As System.Windows.Forms.Label
  Friend WithEvents lbl_captions_l As System.Windows.Forms.Label
  Friend WithEvents lbl_note As System.Windows.Forms.Label
End Class
