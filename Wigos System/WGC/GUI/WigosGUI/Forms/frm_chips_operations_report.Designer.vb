<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_chips_operations_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_chips_operations_report))
    Me.uc_checked_list_table_type = New GUI_Controls.uc_checked_list()
    Me.gb_movement_type = New System.Windows.Forms.GroupBox()
    Me.rb_movements_purchases = New System.Windows.Forms.RadioButton()
    Me.rb_movements_sales = New System.Windows.Forms.RadioButton()
    Me.rb_movements_all = New System.Windows.Forms.RadioButton()
    Me.gb_tables_status = New System.Windows.Forms.GroupBox()
    Me.rb_status_disabled = New System.Windows.Forms.RadioButton()
    Me.rb_status_enabled = New System.Windows.Forms.RadioButton()
    Me.rb_status_all = New System.Windows.Forms.RadioButton()
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.chb_show_tables_only = New System.Windows.Forms.CheckBox()
    Me.chk_grouped_by_type = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_movement_type.SuspendLayout()
    Me.gb_tables_status.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.chk_grouped_by_type)
    Me.panel_filter.Controls.Add(Me.chb_show_tables_only)
    Me.panel_filter.Controls.Add(Me.gb_area_island)
    Me.panel_filter.Controls.Add(Me.gb_tables_status)
    Me.panel_filter.Controls.Add(Me.gb_movement_type)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_table_type)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1206, 217)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_table_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_movement_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_tables_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_area_island, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chb_show_tables_only, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_grouped_by_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 221)
    Me.panel_data.Size = New System.Drawing.Size(1206, 417)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1200, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1200, 4)
    '
    'uc_checked_list_table_type
    '
    Me.uc_checked_list_table_type.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_table_type.Location = New System.Drawing.Point(314, 6)
    Me.uc_checked_list_table_type.m_resize_width = 353
    Me.uc_checked_list_table_type.multiChoice = True
    Me.uc_checked_list_table_type.Name = "uc_checked_list_table_type"
    Me.uc_checked_list_table_type.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_table_type.SelectedIndexesList = ""
    Me.uc_checked_list_table_type.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_table_type.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_table_type.SelectedValuesList = ""
    Me.uc_checked_list_table_type.SetLevels = 2
    Me.uc_checked_list_table_type.Size = New System.Drawing.Size(353, 155)
    Me.uc_checked_list_table_type.TabIndex = 3
    Me.uc_checked_list_table_type.ValuesArray = New String(-1) {}
    '
    'gb_movement_type
    '
    Me.gb_movement_type.Controls.Add(Me.rb_movements_purchases)
    Me.gb_movement_type.Controls.Add(Me.rb_movements_sales)
    Me.gb_movement_type.Controls.Add(Me.rb_movements_all)
    Me.gb_movement_type.Location = New System.Drawing.Point(161, 97)
    Me.gb_movement_type.Name = "gb_movement_type"
    Me.gb_movement_type.Size = New System.Drawing.Size(149, 100)
    Me.gb_movement_type.TabIndex = 2
    Me.gb_movement_type.TabStop = False
    Me.gb_movement_type.Text = "xMovementType"
    '
    'rb_movements_purchases
    '
    Me.rb_movements_purchases.AutoSize = True
    Me.rb_movements_purchases.Location = New System.Drawing.Point(15, 68)
    Me.rb_movements_purchases.Name = "rb_movements_purchases"
    Me.rb_movements_purchases.Size = New System.Drawing.Size(90, 17)
    Me.rb_movements_purchases.TabIndex = 2
    Me.rb_movements_purchases.TabStop = True
    Me.rb_movements_purchases.Text = "xPurchases"
    Me.rb_movements_purchases.UseVisualStyleBackColor = True
    '
    'rb_movements_sales
    '
    Me.rb_movements_sales.AutoSize = True
    Me.rb_movements_sales.Location = New System.Drawing.Point(15, 45)
    Me.rb_movements_sales.Name = "rb_movements_sales"
    Me.rb_movements_sales.Size = New System.Drawing.Size(63, 17)
    Me.rb_movements_sales.TabIndex = 1
    Me.rb_movements_sales.TabStop = True
    Me.rb_movements_sales.Text = "xSales"
    Me.rb_movements_sales.UseVisualStyleBackColor = True
    '
    'rb_movements_all
    '
    Me.rb_movements_all.AutoSize = True
    Me.rb_movements_all.Location = New System.Drawing.Point(15, 22)
    Me.rb_movements_all.Name = "rb_movements_all"
    Me.rb_movements_all.Size = New System.Drawing.Size(46, 17)
    Me.rb_movements_all.TabIndex = 0
    Me.rb_movements_all.TabStop = True
    Me.rb_movements_all.Text = "xAll"
    Me.rb_movements_all.UseVisualStyleBackColor = True
    '
    'gb_tables_status
    '
    Me.gb_tables_status.Controls.Add(Me.rb_status_disabled)
    Me.gb_tables_status.Controls.Add(Me.rb_status_enabled)
    Me.gb_tables_status.Controls.Add(Me.rb_status_all)
    Me.gb_tables_status.Location = New System.Drawing.Point(6, 97)
    Me.gb_tables_status.Name = "gb_tables_status"
    Me.gb_tables_status.Size = New System.Drawing.Size(149, 100)
    Me.gb_tables_status.TabIndex = 1
    Me.gb_tables_status.TabStop = False
    Me.gb_tables_status.Text = "xTableStatus"
    '
    'rb_status_disabled
    '
    Me.rb_status_disabled.AutoSize = True
    Me.rb_status_disabled.Location = New System.Drawing.Point(16, 68)
    Me.rb_status_disabled.Name = "rb_status_disabled"
    Me.rb_status_disabled.Size = New System.Drawing.Size(81, 17)
    Me.rb_status_disabled.TabIndex = 3
    Me.rb_status_disabled.TabStop = True
    Me.rb_status_disabled.Text = "xDisabled"
    Me.rb_status_disabled.UseVisualStyleBackColor = True
    '
    'rb_status_enabled
    '
    Me.rb_status_enabled.AutoSize = True
    Me.rb_status_enabled.Location = New System.Drawing.Point(16, 45)
    Me.rb_status_enabled.Name = "rb_status_enabled"
    Me.rb_status_enabled.Size = New System.Drawing.Size(77, 17)
    Me.rb_status_enabled.TabIndex = 2
    Me.rb_status_enabled.TabStop = True
    Me.rb_status_enabled.Text = "xEnabled"
    Me.rb_status_enabled.UseVisualStyleBackColor = True
    '
    'rb_status_all
    '
    Me.rb_status_all.AutoSize = True
    Me.rb_status_all.Location = New System.Drawing.Point(16, 22)
    Me.rb_status_all.Name = "rb_status_all"
    Me.rb_status_all.Size = New System.Drawing.Size(46, 17)
    Me.rb_status_all.TabIndex = 1
    Me.rb_status_all.TabStop = True
    Me.rb_status_all.Text = "xAll"
    Me.rb_status_all.UseVisualStyleBackColor = True
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(674, 6)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(311, 100)
    Me.gb_area_island.TabIndex = 6
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xLocation"
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(291, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 60
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(291, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 60
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.Size = New System.Drawing.Size(291, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 60
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'chb_show_tables_only
    '
    Me.chb_show_tables_only.AutoSize = True
    Me.chb_show_tables_only.Location = New System.Drawing.Point(316, 165)
    Me.chb_show_tables_only.Name = "chb_show_tables_only"
    Me.chb_show_tables_only.Size = New System.Drawing.Size(126, 17)
    Me.chb_show_tables_only.TabIndex = 4
    Me.chb_show_tables_only.Text = "xShowTablesOnly"
    Me.chb_show_tables_only.UseVisualStyleBackColor = True
    '
    'chk_grouped_by_type
    '
    Me.chk_grouped_by_type.AutoSize = True
    Me.chk_grouped_by_type.Location = New System.Drawing.Point(316, 188)
    Me.chk_grouped_by_type.Name = "chk_grouped_by_type"
    Me.chk_grouped_by_type.Size = New System.Drawing.Size(124, 17)
    Me.chk_grouped_by_type.TabIndex = 5
    Me.chk_grouped_by_type.Text = "xGroupedByType"
    Me.chk_grouped_by_type.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(304, 82)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 44)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.HeightControl = 0
    Me.uc_multicurrency.Location = New System.Drawing.Point(675, 108)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.OpenModeControl = GUI_Controls.uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.SelectedCurrency = ""
    Me.uc_multicurrency.SelectedOption = GUI_Controls.uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode
    Me.uc_multicurrency.Size = New System.Drawing.Size(185, 78)
    Me.uc_multicurrency.TabIndex = 7
    Me.uc_multicurrency.WidthControl = 0
    '
    'frm_chips_operations_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1216, 642)
    Me.Name = "frm_chips_operations_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_chips_operations_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_movement_type.ResumeLayout(False)
    Me.gb_movement_type.PerformLayout()
    Me.gb_tables_status.ResumeLayout(False)
    Me.gb_tables_status.PerformLayout()
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents uc_checked_list_table_type As GUI_Controls.uc_checked_list
  Friend WithEvents gb_movement_type As System.Windows.Forms.GroupBox
  Friend WithEvents rb_movements_all As System.Windows.Forms.RadioButton
  Friend WithEvents rb_movements_purchases As System.Windows.Forms.RadioButton
  Friend WithEvents rb_movements_sales As System.Windows.Forms.RadioButton
  Friend WithEvents gb_tables_status As System.Windows.Forms.GroupBox
  Friend WithEvents rb_status_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents rb_status_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents rb_status_all As System.Windows.Forms.RadioButton
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents chb_show_tables_only As System.Windows.Forms.CheckBox
  Friend WithEvents chk_grouped_by_type As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
End Class
