'-------------------------------------------------------------------
' Copyright � 2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_capacity_details.vb
' DESCRIPTION : Show full details for a cashier session 
'
' REVISION HISTORY :
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 19-JAN-2012 TJG    Capacity details (hour by hour) of the Provider/Site-Day.
' 20-MAR-2012 JAB    Add print and excel buttons.
' 22-MAR-2012 RCI    When Print/Excel operations, set the PrintDateTime when FILTER_APPLY button is clicked.
' 30-AUG-2012 RCI    Added explicit use of index IX_ps_started.
' 03-JUN-2013 RBG    Fixed Bug #786: Error when selecting a provider with character: '.
' 04-DEC-2013 LJM    Changes to use the standard procedures for Button click events.
' 29-MAY-2014 JAB    Added permissions: To buttons "Excel" and "Print".
' 28-JAN-2015 DCS    In TITO mode anonymous accounts don't displayed
' 26-JUL-2017 ETP    Bug 28988: [WIGOS-3800] Statistics - Capacity by provider is not showing all columns.
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Data.SqlClient

Public Class frm_capacity_details
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COL_DAY As Integer = 0
  Private Const GRID_COL_HOUR As Integer = 1
  Private Const GRID_COL_MEN As Integer = 2
  Private Const GRID_COL_WOMEN As Integer = 3
  Private Const GRID_COL_ANONYMOUS As Integer = 4
  Private Const GRID_COL_ALL As Integer = 5

#End Region ' Constants

#Region " Enums"
#End Region ' Enums

#Region " Types "
#End Region ' Types

#Region " Members "

  Private m_is_provider As Boolean
  Private m_provider_id As String
  Private m_terminal_types As String
  Private m_date_from As Date
  Private m_date_to As Date

  Private m_cashier_session_id As Long

  Private m_print_datetime As Date

  Private m_only_preview As Boolean

  Private m_is_tito_mode As Boolean

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Overload function to set a AUDIT_FLAGS for buttons that don't have any.
  '         
  ' PARAMS:
  '    - INPUT: ButtonId, ENUM_BUTTON that may get audited
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '    - The AUDIT_FLAGS asociated to that button.
  Protected Overrides Function GUI_GetAuditFormType(ByVal ButtonId As ENUM_BUTTON) As AUDIT_FLAGS
    Dim _AUDIT_FLAGS As AUDIT_FLAGS

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        _AUDIT_FLAGS = AUDIT_FLAGS.PRINT

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        _AUDIT_FLAGS = AUDIT_FLAGS.EXCEL

      Case Else
        _AUDIT_FLAGS = MyBase.GUI_GetAuditFormType(ButtonId)
    End Select

    Return _AUDIT_FLAGS
  End Function

  ' PURPOSE: Overload function to use the buttons of type "custom" assigning them functions
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0 'Imprimir
        ButtonId = ENUM_BUTTON.BUTTON_PRINT

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 'Excel
        ButtonId = ENUM_BUTTON.BUTTON_EXCEL

      Case Else
        ' Set button pressed as Cancel - Even when 'Enter' key is pressed.
        ' (Base edit assumes when key Enter is pressed 
        ButtonId = ENUM_BUTTON.BUTTON_CANCEL
    End Select

    MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_PrintResultList()
    m_only_preview = True
    Try
      Call GUI_ExcelResultList()
    Finally
      m_only_preview = False
    End Try
  End Sub

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAPACITY

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_InitControls()

    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

    ' Initialize Form Controls
    '   - Form
    '   - Proveedor
    '   - Grid

    '   - Form
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(422)                ' 422 "Aforo Detallado por Hora"

    '   - Proveedor
    Me.tf_provider.Text = GLB_NLS_GUI_AUDITOR.GetString(404)      ' 404 "Proveedor"

    '   - Grid
    With Me.dg_capacity_hours

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False
      .PanelRightVisible = False

      ' Day
      .Column(GRID_COL_DAY).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(415)         ' 415 "D�a"
      .Column(GRID_COL_DAY).Width = 1340
      .Column(GRID_COL_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Hour
      .Column(GRID_COL_HOUR).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(421)        ' 421 "Hora"
      .Column(GRID_COL_HOUR).Width = 800
      .Column(GRID_COL_HOUR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Men
      .Column(GRID_COL_MEN).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(401)         ' 401 "Hombres"
      .Column(GRID_COL_MEN).Width = 1000
      .Column(GRID_COL_MEN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Women
      .Column(GRID_COL_WOMEN).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(402)       ' 402 "Mujeres"
      .Column(GRID_COL_WOMEN).Width = 1000
      .Column(GRID_COL_WOMEN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Anonymous / Uncarded
      .Column(GRID_COL_ANONYMOUS).Header(0).Text = IIf(m_is_tito_mode, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8554), GLB_NLS_GUI_AUDITOR.GetString(403)) '
      .Column(GRID_COL_ANONYMOUS).Width = 1200
      .Column(GRID_COL_ANONYMOUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' All
      .Column(GRID_COL_ALL).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(263)
      .Column(GRID_COL_ALL).Width = 1000
      .Column(GRID_COL_ALL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

    ' Buttons
    ' Disable unused buttons: only the Cancel button is needed
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Enable custom buttons to use "print" and "excel" options.
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(5)   ' Imprimir
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(27)  ' Excel
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

    ' Enable / Disable controls


  End Sub 'GUI_InitControls

  ' PURPOSE: Check for screen data.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    ' Nothing to check
    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : 
  '         
  ' PARAMS :
  '    - INPUT :
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _sql_trans As SqlTransaction = Nothing

    If m_is_provider Then
      tf_provider.Value = m_provider_id
    Else
      tf_provider.Value = GLB_NLS_GUI_AUDITOR.GetString(406)   ' 406 "Toda la sala"
    End If

    Call Insert24h()

    _sql_trans = WSI.Common.WGDB.Connection().BeginTransaction()

    ReadFromDB()

    If Not _sql_trans Is Nothing Then
      _sql_trans.Dispose()
    End If

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Sets database permissions.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub  'GUI_Permissions

  ' PURPOSE: Sets the initial focus
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Focus()
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
  End Sub 'GUI_DB_Operation

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE : Insert empty rows to grid
  '
  '  PARAMS :
  '      - INPUT :
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  Private Sub Insert24h()

    Dim _idx As Integer
    Dim _current_time As DateTime

    ' Don't care about the sort order because rows are sorted when inserted in the grid (AddCapacityToGrid)   

    _current_time = m_date_from

    _idx = 0
    While _current_time < m_date_to

      dg_capacity_hours.AddRow()

      dg_capacity_hours.Cell(_idx, GRID_COL_DAY).Value = GUI_FormatDate(_current_time, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      dg_capacity_hours.Cell(_idx, GRID_COL_HOUR).Value = GUI_FormatTime(_current_time, ENUM_FORMAT_TIME.FORMAT_HOURS)

      dg_capacity_hours.Cell(_idx, GRID_COL_MEN).Value = GUI_FormatNumber(0, 0)
      dg_capacity_hours.Cell(_idx, GRID_COL_WOMEN).Value = GUI_FormatNumber(0, 0)
      dg_capacity_hours.Cell(_idx, GRID_COL_ANONYMOUS).Value = GUI_FormatNumber(0, 0)
      dg_capacity_hours.Cell(_idx, GRID_COL_ALL).Value = GUI_FormatNumber(0, 0)

      ' Step to next day
      _current_time = _current_time.AddHours(1)
      _idx += 1

    End While

  End Sub ' Insert24h

  ' PURPOSE : Return the SQL Query to obtain the provider capacity per hour.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String
  Private Function SqlQueryProvider() As String

    Dim _str_sql As String = ""

    _str_sql += "SELECT MinTimeOrder" _
                   + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 1 THEN 1 ELSE 0 END), 0)              VisitsMen" _
                   + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 2 THEN 1 ELSE 0 END), 0)              VisitsWomen" _
                   + ", ISNULL(SUM(CASE ISNULL (AC_HOLDER_GENDER, 0) WHEN 0 THEN 1 ELSE 0 END), 0)  VisitsAnonym" _
                  + ", COUNT (*)															                                      Visits" _
               + " FROM (" _
                      + " SELECT PS_ACCOUNT_ID" _
                            + ", AC_HOLDER_GENDER" _
                            + ", MIN (DATEDIFF(hour, @from_date, PS_STARTED) % 24) as               MinTimeOrder" _
                        + " FROM PLAY_SESSIONS with(index(IX_ps_started))" _
                            + ", TERMINALS" _
                            + ", ACCOUNTS" _
                       + " WHERE PS_STARTED >= @from_date" _
                         + " AND PS_STARTED <  @to_date" _
                         + " AND PS_TERMINAL_ID = TE_TERMINAL_ID" _
                         + " AND TE_TERMINAL_TYPE IN (" + m_terminal_types + ")" _
                         + " AND AC_ACCOUNT_ID  = PS_ACCOUNT_ID" _
                         + " AND TE_PROVIDER_ID = @provider_id" _
                    + " GROUP BY PS_ACCOUNT_ID" _
                            + ", AC_HOLDER_GENDER" _
                     + ") VisitsPerHour" _
           + " GROUP BY MinTimeOrder" _
           + " ORDER BY MinTimeOrder"

    Return _str_sql

  End Function ' SqlQueryProvider

  ' PURPOSE : Return the SQL Query to obtain the site capacity per hour.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String
  Private Function SqlQuerySite() As String

    Dim _str_sql As String = ""

    _str_sql += "SELECT MinTimeOrder" _
                    + ", COUNT (*)															                                      Visits" _
                    + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 1 THEN 1 ELSE 0 END), 0)              VisitsMen" _
                    + ", ISNULL(SUM(CASE AC_HOLDER_GENDER WHEN 2 THEN 1 ELSE 0 END), 0)              VisitsWomen" _
                    + ", ISNULL(SUM(CASE ISNULL (AC_HOLDER_GENDER, 0) WHEN 0 THEN 1 ELSE 0 END), 0)  VisitsAnonym" _
                    + " FROM (" _
                       + " SELECT PS_ACCOUNT_ID" _
                             + ", AC_HOLDER_GENDER" _
                             + ", MIN (DATEDIFF(hour, @from_date, PS_STARTED) % 24) as               MinTimeOrder" _
                         + " FROM PLAY_SESSIONS with(index(IX_ps_started))" _
                             + ", TERMINALS" _
                             + ", ACCOUNTS" _
                        + " WHERE PS_STARTED >= @from_date" _
                          + " AND PS_STARTED <  @to_date" _
                          + " AND PS_TERMINAL_ID = TE_TERMINAL_ID" _
                          + " AND TE_TERMINAL_TYPE IN (" + m_terminal_types + ")" _
                          + " AND AC_ACCOUNT_ID  = PS_ACCOUNT_ID" _
                     + " GROUP BY PS_ACCOUNT_ID" _
                          + ", AC_HOLDER_GENDER" _
                     + ") VisitsPerHour" _
              + " GROUP BY MinTimeOrder" _
              + " ORDER BY MinTimeOrder"

    Return _str_sql

  End Function ' SqlQuerySite

  Private Function ReadFromDB() As Integer

    Dim _str_sql As String
    Dim _dt As DataTable
    Dim _dr As DataRow
    Dim _visits_by_hour As DataRow()
    Dim _idx As Integer
    Dim _total(0 To 3) As Integer

    Try

      _str_sql = " DECLARE @from_date AS DATETIME" _
               + " DECLARE @to_date AS DATETIME" _
               + " DECLARE @provider_id varchar(100)" _
               + " SET @from_date = " + GUI_FormatDateDB(m_date_from) _
               + " SET @to_date = " + GUI_FormatDateDB(m_date_to) _
               + " SET @provider_id = '" + m_provider_id.Replace("'", "''") + "'"

      If m_is_provider Then
        _str_sql += SqlQueryProvider()
      Else
        _str_sql += SqlQuerySite()
      End If

      _dt = GUI_GetTableUsingSQL(_str_sql, 100)

      If IsNothing(_dt) Then
        Return ENUM_COMMON_RC.COMMON_ERROR_DB
      End If

      For _idx = 0 To 3
        _total(_idx) = 0
      Next

      If _dt.Rows.Count() > 0 Then

        For _idx = 0 To 23
          _visits_by_hour = _dt.Select("MinTimeOrder = " + _idx.ToString())

          If _visits_by_hour.Length > 0 Then
            For Each _dr In _visits_by_hour
              dg_capacity_hours.Cell(_idx, GRID_COL_MEN).Value = GUI_FormatNumber(_dr("VisitsMen"), 0)
              dg_capacity_hours.Cell(_idx, GRID_COL_WOMEN).Value = GUI_FormatNumber(_dr("VisitsWomen"), 0)
              dg_capacity_hours.Cell(_idx, GRID_COL_ANONYMOUS).Value = GUI_FormatNumber(_dr("VisitsAnonym"), 0)
              dg_capacity_hours.Cell(_idx, GRID_COL_ALL).Value = GUI_FormatNumber(_dr("Visits"), 0)

              _total(0) += _dr("VisitsMen")
              _total(1) += _dr("VisitsWomen")
              _total(2) += _dr("VisitsAnonym")
              _total(3) += _dr("Visits")
            Next
          Else
            dg_capacity_hours.Cell(_idx, GRID_COL_MEN).Value = GUI_FormatNumber(0, 0)
            dg_capacity_hours.Cell(_idx, GRID_COL_WOMEN).Value = GUI_FormatNumber(0, 0)
            dg_capacity_hours.Cell(_idx, GRID_COL_ANONYMOUS).Value = GUI_FormatNumber(0, 0)
            dg_capacity_hours.Cell(_idx, GRID_COL_ALL).Value = GUI_FormatNumber(0, 0)
          End If
        Next

      End If

      _idx = 24

      ' Add total   
      dg_capacity_hours.AddRow()
      dg_capacity_hours.Row(_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      dg_capacity_hours.Cell(_idx, GRID_COL_DAY).Value = ""
      dg_capacity_hours.Cell(_idx, GRID_COL_HOUR).Value = GLB_NLS_GUI_AUDITOR.GetString(423)   ' 423 "Total"
      dg_capacity_hours.Cell(_idx, GRID_COL_HOUR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      dg_capacity_hours.Cell(_idx, GRID_COL_MEN).Value = GUI_FormatNumber(_total(0), 0)
      dg_capacity_hours.Cell(_idx, GRID_COL_WOMEN).Value = GUI_FormatNumber(_total(1), 0)
      dg_capacity_hours.Cell(_idx, GRID_COL_ANONYMOUS).Value = GUI_FormatNumber(_total(2), 0)
      dg_capacity_hours.Cell(_idx, GRID_COL_ALL).Value = GUI_FormatNumber(_total(3), 0)

      Return ENUM_COMMON_RC.COMMON_OK

    Catch _ex As Exception

    End Try

  End Function ' ReadFromDB

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor

    _prev_cursor = Me.Cursor
    Me.Cursor = Cursors.WaitCursor

    Try
      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      frm_base_sel.GUI_GenerateExcel(dg_capacity_hours, GLB_NLS_GUI_AUDITOR.GetString(422), m_print_datetime, _print_data, m_only_preview)

    Finally
      Me.Cursor = _prev_cursor
    End Try

  End Sub ' GUI_ExcelResultList

#End Region ' Private Functions

#Region "Public Functions"

  ' PURPOSE : Functions to load the form
  '
  '  PARAMS :
  '     - INPUT :
  '           - ProviderId
  '           - DateFrom
  '           - TerminalTypes
  '
  '     - OUTPUT :
  '
  ' RETURNS:

  Public Overloads Sub ShowEditItem(ByVal ProviderId As String, _
                                    ByVal DateFrom As Date, _
                                    ByVal TerminalTypes As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    m_is_provider = (ProviderId <> "")
    m_provider_id = ProviderId
    m_terminal_types = TerminalTypes
    m_date_from = DateFrom
    m_date_to = DateFrom.AddDays(1)

    m_cashier_session_id = 0

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

#End Region ' Public Functions

#Region "Events"

#End Region ' Events

#Region "GUI Reports"

  ' PURPOSE: Filters and sets its value to display in the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Sub GUI_ReportFilter(ByRef PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(404), tf_provider.Value)
    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

#End Region


End Class