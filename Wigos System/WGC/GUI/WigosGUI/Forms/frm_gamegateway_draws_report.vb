﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_report_bonoplay_sel
' DESCRIPTION:   This screen allows to view the games statistics between:
'                           - for partners 
'                           - for games
' AUTHOR:        Rubén Darío Soria
' CREATION DATE: 19-NOV-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-NOV-2015  RDS    Initial Version
' 07-DIC-2015  FJC    Fixed BUG: 7364 (Error when we try search)
' 22-FEB-2016  AMF    PBI 9434: Award prizes
' 09-JUN-2016  JCA    Bug 14150:PimPamGo: en "reporte de sorteos" no salen las anulaciones
' 04-OCT-2016  JBP    PBI 18464:PariPlay - Ajustar pantallas de BonoPlay
'--------------------------------------------------------------------
Option Explicit On
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Public Class frm_gamegateway_draws_report
  Inherits GUI_Controls.frm_base_sel


#Region " Constants "

  ' DB Columns
  Private Const SQL_COL_PARTNER As Integer = 0
  Private Const SQL_COL_GG_PARTNER_ID As Integer = 1
  Private Const SQL_COL_GGI_GAME_ID As Integer = 2
  Private Const SQL_COL_GG_NAME As Integer = 3
  Private Const SQL_COL_GG_GAME_INSTANCE_ID As Integer = 4
  Private Const SQL_COL_GG_NAME_TRANSLATED As Integer = 5
  Private Const SQL_COL_GGI_STARTS As Integer = 6
  Private Const SQL_COL_GGI_PLAYED As Integer = 7
  Private Const SQL_COL_GGI_WON As Integer = 8
  Private Const SQL_COL_GGI_WON_JACKPOT As Integer = 9
  Private Const SQL_COL_GGI_NUM_BETS As Integer = 10
  Private Const SQL_COL_GGI_NUM_PRIZES As Integer = 11
  Private Const SQL_COL_PAYOUT As Integer = 12
  Private Const SQL_COL_GGI_CREATED As Integer = 13
  Private Const SQL_COL_GGI_FINISHED As Integer = 14
  Private Const SQL_COL_GGI_WON_TOTAL As Integer = 15
  Private Const SQL_COL_GGI_PENDING As Integer = 16
  Private Const SQL_COL_GGI_NUM_PENDING As Integer = 17
  Private Const SQL_COL_GGI_NUM_BETS_ROLLBACK As Integer = 18


  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COL_PARTNER As Integer = 1
  Private Const GRID_COL_GG_NAME As Integer = 2
  Private Const GRID_COL_GGI_GAME_INSTANCE_ID As Integer = 3
  Private Const GRID_COL_GG_NAME_TRANSLATED As Integer = 4
  Private Const GRID_COL_GGI_STARTS As Integer = 5
  Private Const GRID_COL_GGI_CREATED As Integer = 6
  Private Const GRID_COL_GGI_FINISHED As Integer = 7
  Private Const GRID_COL_GGI_NUM_BETS As Integer = 8
  Private Const GRID_COL_GGI_NUM_BETS_ROLLBACK As Integer = 9
  Private Const GRID_COL_GGI_PLAYED As Integer = 10
  Private Const GRID_COL_GGI_NUM_PRIZES As Integer = 11
  Private Const GRID_COL_GGI_WON As Integer = 12
  Private Const GRID_COL_GGI_WON_JACKPOT As Integer = 13
  Private Const GRID_COL_GGI_PENDING As Integer = 14
  Private Const GRID_COL_GGI_WON_TOTAL As Integer = 15
  Private Const GRID_COL_PAYOUT As Integer = 16


  ' Width Columns
  Private Const GRID_WIDTH_PARTNER As Integer = 1500
  Private Const GRID_WIDTH_GG_NAME As Integer = 3190
  Private Const GRID_WIDTH_GGI_GAME_INSTANCE_ID As Integer = 1700
  Private Const GRID_WIDTH_GG_NAME_TRANSLATED As Integer = 3170
  Private Const GRID_WIDTH_GGI_STARTS As Integer = 1900
  Private Const GRID_WIDTH_GGI_PLAYED As Integer = 1900
  Private Const GRID_WIDTH_GGI_WON As Integer = 1900
  Private Const GRID_WIDTH_GGI_WON_JACKPOT As Integer = 1900
  Private Const GRID_WIDTH_GGI_NUM_BETS As Integer = 1900
  Private Const GRID_WIDTH_GGI_NUM_PRIZES As Integer = 1900
  Private Const GRID_WIDTH_PAYOUT As Integer = 1900
  Private Const GRID_WIDTH_GGI_CREATED As Integer = 0
  Private Const GRID_WIDTH_GGI_FINISHED As Integer = 0
  Private Const GRID_WIDTH_GGI_PENDING As Integer = 1900
  Private Const GRID_WIDTH_GGI_NUM_BETS_ROLLBACK As Integer = 1900

  Private Const GRID_COLUMNS_COUNT As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const FORM_DB_MIN_VERSION As Short = 149
#End Region ' Constants

#Region " Structures"

  ' For subtotal amount grid
  Private Structure TYPE_TOTAL_SUBTOTAL_GAMES
    Dim m_subtotal_game_id As Integer
    Dim m_subtotal_name As String
    Dim m_subtotal_name_translated As String
    Dim m_subtotal_played_amount As Decimal
    Dim m_subtotal_won_amount_total As Decimal
    Dim m_subtotal_won_amount As Decimal
    Dim m_subtotal_wonjackpot_amount As Decimal
    Dim m_subtotal_num_bets_count As Integer
    Dim m_subtotal_num_prizes_count As Integer
    Dim m_subtotal_payout As Decimal
    Dim m_subtotal_pending_amount As Decimal
    Dim m_subtotal_num_bets_rollback_count As Integer
  End Structure

#End Region ' Structures

#Region " Members "

  ' For report filters 
  Private m_provider_name As String
  Private m_game_name As String
  Private m_dtp_from_ini As String
  Private m_dtp_to_ini As String
  Private m_dtp_from_end As String
  Private m_dtp_to_end As String
  Private m_chk_hide_games_without_played As String

  ' For total amount grid
  Dim m_total_played_amount As Decimal
  Dim m_total_won_amount_total As Decimal
  Dim m_total_won_amount As Decimal
  Dim m_total_wonjackpot_amount As Decimal
  Dim m_total_num_bets_count As Integer
  Dim m_total_num_prizes_count As Integer
  Dim m_total_payout As Decimal
  Dim m_total_pending_amount As Decimal
  Dim m_total_num_bets_rollback_count As Integer

  Dim m_type_subtotal_game As TYPE_TOTAL_SUBTOTAL_GAMES
  Dim m_dic_subtotal_game As Dictionary(Of Integer, TYPE_TOTAL_SUBTOTAL_GAMES)

#End Region ' Members

#Region " Overrides "
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_REPORT_BONOPLAY_SEL

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset



  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call ResetTotalCounters()
    Call ResetSubTotalCounters()

  End Sub

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Dim _num_rows As Long

    _num_rows = Me.Grid.NumRows

    If _num_rows > 0 Then
      Me.Grid.AddRow()

      'InsertSubtotal(Me.Grid.NumRows - 1)

      Call AddGameSubTotal()

      InsertTotalByGame(Me.Grid.NumRows - 1)

      InsertTotal(Me.Grid.NumRows - 1)

    End If
    ResetTotalCounters()

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder
    'sb.AppendLine("    SELECT   RIGHT('000'+CAST(GP_GG.PART_ID AS VARCHAR(3)),3) AS PROVIDER                                     ")
    _sb.AppendLine("    DECLARE @pPartnerName AS VARCHAR (255)                                                                    ")
    _sb.AppendLine("    SET @pPartnerName = ( SELECT  GP_KEY_VALUE FROM  GENERAL_PARAMS                                           ")
    _sb.AppendLine("                           WHERE  GP_GROUP_KEY	= 'PariPlay'                                                  ")
    _sb.AppendLine("                             AND  GP_SUBJECT_KEY	= 'Name'	 )                                                ")

    _sb.AppendLine("    SELECT   GP_GG.PARTNER_NAME                                        AS PROVIDER                            ")
    _sb.AppendLine("           , GG.GG_PARTNER_ID                                                                                 ")
    _sb.AppendLine("           , GGI.GGI_GAME_ID                                                                                  ")
    _sb.AppendLine("           , GG.GG_NAME                                                                                       ")
    _sb.AppendLine("           , GGI.GGI_GAME_INSTANCE_ID                                                                         ")
    _sb.AppendLine("           , GG.GG_NAME_TRANSLATED                                                                            ")
    _sb.AppendLine("           , GGI.GGI_STARTS                                                                                   ")
    _sb.AppendLine("           , GGI.GGI_PLAYED                                                                                   ")
    _sb.AppendLine("           , (GGI.GGI_WON - GGI.GGI_WON_JACKPOT) AS GGI_WON                                                   ")
    _sb.AppendLine("           , GGI.GGI_WON_JACKPOT                                                                              ")
    _sb.AppendLine("           , GGI.GGI_NUM_BETS + ISNULL(GGI.GGI_NUM_BETS_ROLLBACK,0)   AS  GGI_NUM_BETS                        ")
    _sb.AppendLine("           , GGI.GGI_NUM_PRIZES + GGI.GGI_NUM_PENDING AS GGI_NUM_WON                                          ")
    _sb.AppendLine("           , CASE WHEN GGI.GGI_PLAYED > 0 THEN (((GGI.GGI_WON + GGI.GGI_PENDING) * 100) / GGI.GGI_PLAYED) ELSE 100 END AS PAYOUT ")
    _sb.AppendLine("           , GGI.GGI_CREATED                                                                                  ")
    _sb.AppendLine("           , GGI.GGI_FINISHED                                                                                 ")
    _sb.AppendLine("           , GGI.GGI_WON + GGI.GGI_PENDING AS GGI_WON_TOTAL                                                   ")
    _sb.AppendLine("           , GGI.GGI_PENDING                                                                                  ")
    _sb.AppendLine("           , GGI.GGI_NUM_PENDING                                                                              ")
    _sb.AppendLine("           , ISNULL(GGI.GGI_NUM_BETS_ROLLBACK,0)                                                              ")
    _sb.AppendLine("      FROM                                                                                                    ")
    _sb.AppendLine("          (                                                                                                 ")

    ' Dynamic Providers GameGateway
    If GeneralParam.GetBoolean("GameGateway", "Enabled", False) Then
      _sb.AppendLine("           -- Dynamic Providers GameGateway                                                                 ")
      _sb.AppendLine("           SELECT   CAST(PIDS.PART_ID AS INT)                          AS PARTNER_ID                        ")
      _sb.AppendLine("                  , PNAS.PNAME + ' (' + PIDS.PART_ID + ')'             AS PARTNER_NAME                      ")
      _sb.AppendLine("             FROM                                                                                           ")
      _sb.AppendLine("                 (SELECT   GP_KEY_VALUE AS PART_ID                                                          ")
      _sb.AppendLine("                    FROM   GENERAL_PARAMS                                                                   ")
      _sb.AppendLine("                   WHERE   GP_GROUP_KEY  = 'GameGateway'                                                    ")
      _sb.AppendLine("                     AND   GP_SUBJECT_KEY LIKE 'Provider.%.PartnerId') PIDS                                 ")
      _sb.AppendLine("              INNER JOIN                                                                                    ")
      _sb.AppendLine("                 (SELECT   GP_KEY_VALUE AS PNAME                                                            ")
      _sb.AppendLine("                         , SUBSTRING(GP_SUBJECT_KEY,10,3)              AS PNAMEKEY                          ")
      _sb.AppendLine("                    FROM   GENERAL_PARAMS                                                                   ")
      _sb.AppendLine("                   WHERE   GP_GROUP_KEY  = 'GameGateway'                                                    ")
      _sb.AppendLine("                     AND   GP_SUBJECT_KEY LIKE 'Provider.%.Name') PNAS                                      ")
      _sb.AppendLine("                      ON   CAST(PIDS.PART_ID AS INT) = CAST(PNAMEKEY AS INT)                                ")
    End If

    ' Provider: ParyPlay.
    If GeneralParam.GetBoolean("PariPlay", "Enabled", False) Then

      ' Add union y GameGateway is enabled.
      If GeneralParam.GetBoolean("GameGateway", "Enabled", False) Then
        _sb.AppendLine("      UNION                                                                                               ")
      End If

      _sb.AppendLine("           -- PariPlay                                                                                      ")
      _sb.AppendLine("           SELECT  2	AS PARTNER_ID, @pPartnerName   AS PARTNER_NAME                                        ")
    End If

    _sb.AppendLine("          )  AS GP_GG                                                                                         ")
    _sb.AppendLine("INNER JOIN   GAMEGATEWAY_GAMES GG                                                                             ")
    _sb.AppendLine("        ON   GG.GG_PARTNER_ID = GP_GG.PARTNER_ID                                                              ")
    _sb.AppendLine("INNER JOIN   GAMEGATEWAY_GAME_INSTANCES GGI                                                                   ")
    _sb.AppendLine("        ON   GGI.GGI_GAME_ID = GG.GG_GAME_ID                                                                  ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("  ORDER BY   GGI_STARTS DESC, GGI.GGI_GAME_ID                                                                 ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery



  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    m_dic_subtotal_game = New Dictionary(Of Integer, TYPE_TOTAL_SUBTOTAL_GAMES)

    ' Form name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6938)

    ' Style datagrid
    Call GUI_StyleSheet()

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Text = GLB_NLS_GUI_CONTROLS.GetString(5)

    'Session filter
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4976)           ' Date - Started

    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    'Hide Games without played
    Me.chk_hide_games_without_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6989)

    'Provider / Game filter
    Me.uc_combo_game_gateway.Init()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Provider 
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5418), m_provider_name)

    ' Games
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008), m_game_name)

    ' Filter Dates
    If m_dtp_from_ini <> String.Empty Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4976) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_dtp_from_ini)
    End If

    If m_dtp_to_ini <> String.Empty Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4976) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_dtp_to_ini)
    End If

    If m_dtp_from_end <> String.Empty Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4332) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_dtp_from_end)
    End If

    If m_dtp_to_end <> String.Empty Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4332) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_dtp_to_end)
    End If

    ' Hide games without played
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6989), m_chk_hide_games_without_played)

  End Sub ' GUI_ReportFilter


  Protected Overrides Sub GUI_ReportUpdateFilters()

    'Inititalize
    m_provider_name = String.Empty
    m_game_name = String.Empty
    m_dtp_from_ini = String.Empty
    m_dtp_to_ini = String.Empty
    m_dtp_from_end = String.Empty
    m_dtp_to_end = String.Empty
    m_chk_hide_games_without_played = String.Empty

    ' Provider 
    'm_provider_name = Me.cmb_partner.TextValue
    m_provider_name = Me.uc_combo_game_gateway.GetSelectedProviderValue

    ' Games
    'If Me.uc_combo_games.IsOneChecked Then
    ' m_game_name = Me.uc_combo_games.Combo.TextValue
    'Else
    'm_game_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    'End If
    m_game_name = Me.uc_combo_game_gateway.GetSelectedGameValue

    ' Filter Dates
    If Me.dtp_from.Checked Then
      m_dtp_from_ini = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.dtp_to.Checked Then
      m_dtp_to_ini = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.chk_hide_games_without_played.Checked Then
      m_chk_hide_games_without_played = GLB_NLS_GUI_INVOICING.GetString(479) ' Yes
    Else
      m_chk_hide_games_without_played = GLB_NLS_GUI_INVOICING.GetString(480) ' No
    End If
  End Sub
  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'Subtotal change
    If m_type_subtotal_game.m_subtotal_game_id > 0 And m_type_subtotal_game.m_subtotal_game_id <> DbRow.Value(SQL_COL_GGI_GAME_ID) Then
      'Call InsertSubtotal(RowIndex)


      Call AddGameSubTotal()

      'RowIndex = RowIndex + 1

      'Reset subtotals
      ResetSubTotalCounters()
    End If

    m_type_subtotal_game.m_subtotal_game_id = DbRow.Value(SQL_COL_GGI_GAME_ID)


    ' PARTNER
    If Not DbRow.IsNull(SQL_COL_PARTNER) Then
      Me.Grid.Cell(RowIndex, GRID_COL_PARTNER).Value = DbRow.Value(SQL_COL_PARTNER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_PARTNER).Value = ""
    End If

    ' GG_NAME
    If Not DbRow.IsNull(SQL_COL_GG_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GG_NAME).Value = DbRow.Value(SQL_COL_GG_NAME)
      m_type_subtotal_game.m_subtotal_name = DbRow.Value(SQL_COL_GG_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COL_GG_NAME).Value = ""
      m_type_subtotal_game.m_subtotal_name = ""
    End If

    ' GGI_GAME_INSTANCE_ID
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_GAME_INSTANCE_ID).Value = DbRow.Value(SQL_COL_GG_GAME_INSTANCE_ID)

    ' GG_NAME_TRANSLATED
    If Not DbRow.IsNull(SQL_COL_GG_NAME_TRANSLATED) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GG_NAME_TRANSLATED).Value = DbRow.Value(SQL_COL_GG_NAME_TRANSLATED)
      m_type_subtotal_game.m_subtotal_name_translated = DbRow.Value(SQL_COL_GG_NAME_TRANSLATED)
    End If

    ' GGI_STARTS
    If Not DbRow.IsNull(SQL_COL_GGI_STARTS) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_STARTS).Value = GUI_FormatDate(DbRow.Value(SQL_COL_GGI_STARTS), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM)

    End If

    ' GGI_CREATED
    If Not DbRow.IsNull(SQL_COL_GGI_CREATED) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COL_GGI_CREATED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' GGI_FINISHED
    If Not DbRow.IsNull(SQL_COL_GGI_FINISHED) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_FINISHED).Value = GUI_FormatDate(DbRow.Value(SQL_COL_GGI_FINISHED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' GGI_NUM_BETS
    If Not DbRow.IsNull(SQL_COL_GGI_NUM_BETS) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_BETS).Value = DbRow.Value(SQL_COL_GGI_NUM_BETS)
      m_type_subtotal_game.m_subtotal_num_bets_count += DbRow.Value(SQL_COL_GGI_NUM_BETS)
      m_total_num_bets_count += DbRow.Value(SQL_COL_GGI_NUM_BETS)
    End If

    ' GGI_PLAYED
    If Not DbRow.IsNull(SQL_COL_GGI_PLAYED) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_PLAYED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_GGI_PLAYED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_type_subtotal_game.m_subtotal_played_amount += DbRow.Value(SQL_COL_GGI_PLAYED)
      m_total_played_amount += DbRow.Value(SQL_COL_GGI_PLAYED)
    End If

    ' GGI_NUM_PRIZES
    If Not DbRow.IsNull(SQL_COL_GGI_NUM_PRIZES) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_PRIZES).Value = DbRow.Value(SQL_COL_GGI_NUM_PRIZES)
      m_type_subtotal_game.m_subtotal_num_prizes_count += DbRow.Value(SQL_COL_GGI_NUM_PRIZES)
      m_total_num_prizes_count += DbRow.Value(SQL_COL_GGI_NUM_PRIZES)
    End If

    ' GGI_WON
    If Not DbRow.IsNull(SQL_COL_GGI_WON) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_GGI_WON), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_type_subtotal_game.m_subtotal_won_amount += DbRow.Value(SQL_COL_GGI_WON)
      m_total_won_amount += DbRow.Value(SQL_COL_GGI_WON)
    End If

    ' GGI_WON_JACKPOT
    If Not DbRow.IsNull(SQL_COL_GGI_WON_JACKPOT) AndAlso DbRow.Value(SQL_COL_GGI_WON_JACKPOT) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON_JACKPOT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_GGI_WON_JACKPOT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_type_subtotal_game.m_subtotal_wonjackpot_amount += DbRow.Value(SQL_COL_GGI_WON_JACKPOT)
      m_total_wonjackpot_amount += DbRow.Value(SQL_COL_GGI_WON_JACKPOT)
    End If

    ' GGI_PENDING
    If Not DbRow.IsNull(SQL_COL_GGI_PENDING) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_PENDING).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_GGI_PENDING), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_type_subtotal_game.m_subtotal_pending_amount += DbRow.Value(SQL_COL_GGI_PENDING)
      m_total_pending_amount += DbRow.Value(SQL_COL_GGI_PENDING)
    End If

    ' GGI_WON_TOTAL
    If Not DbRow.IsNull(SQL_COL_GGI_WON_TOTAL) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON_TOTAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_GGI_WON_TOTAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_type_subtotal_game.m_subtotal_won_amount_total += DbRow.Value(SQL_COL_GGI_WON_TOTAL)
      m_total_won_amount_total += DbRow.Value(SQL_COL_GGI_WON_TOTAL)
    End If

    ' PAYOUT
    If Not DbRow.IsNull(SQL_COL_PAYOUT) Then
      If Not DbRow.IsNull(SQL_COL_GGI_WON) AndAlso Not DbRow.IsNull(SQL_COL_GGI_WON_JACKPOT) AndAlso Not DbRow.IsNull(SQL_COL_GGI_PENDING) AndAlso _
            (DbRow.Value(SQL_COL_GGI_WON) > 0 Or DbRow.Value(SQL_COL_GGI_WON_JACKPOT) > 0 Or DbRow.Value(SQL_COL_GGI_PENDING) > 0) Then
        Me.Grid.Cell(RowIndex, GRID_COL_PAYOUT).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_PAYOUT), 2) & "%"
        m_type_subtotal_game.m_subtotal_payout += DbRow.Value(SQL_COL_PAYOUT)
        m_total_payout += DbRow.Value(SQL_COL_PAYOUT)
      End If
    End If

    'NUM_BETS_ROLLBACK
    If Not DbRow.IsNull(SQL_COL_GGI_NUM_BETS_ROLLBACK) Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_BETS_ROLLBACK).Value = DbRow.Value(SQL_COL_GGI_NUM_BETS_ROLLBACK)
      m_type_subtotal_game.m_subtotal_num_bets_rollback_count += DbRow.Value(SQL_COL_GGI_NUM_BETS_ROLLBACK)
      m_total_num_bets_rollback_count += DbRow.Value(SQL_COL_GGI_NUM_BETS_ROLLBACK)
    End If

    Return True

  End Function ' GUI_SetupRow

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public functions

#Region " Private Functions "
  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters()
    m_type_subtotal_game.m_subtotal_game_id = 0
    m_total_played_amount = 0
    m_total_won_amount = 0
    m_total_won_amount_total = 0
    m_total_wonjackpot_amount = 0
    m_total_num_bets_count = 0
    m_total_num_prizes_count = 0
    m_total_payout = 0
    m_total_pending_amount = 0
    m_total_num_bets_rollback_count = 0

    m_dic_subtotal_game.Clear()

  End Sub ' ResetTotalCounters
  ' PURPOSE : Reset SubTotal Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetSubTotalCounters()

    m_type_subtotal_game.m_subtotal_played_amount = 0
    m_type_subtotal_game.m_subtotal_won_amount = 0
    m_type_subtotal_game.m_subtotal_wonjackpot_amount = 0
    m_type_subtotal_game.m_subtotal_num_bets_count = 0
    m_type_subtotal_game.m_subtotal_num_prizes_count = 0
    m_type_subtotal_game.m_subtotal_payout = 0
    m_type_subtotal_game.m_subtotal_won_amount_total = 0
    m_type_subtotal_game.m_subtotal_pending_amount = 0
    m_type_subtotal_game.m_subtotal_num_bets_rollback_count = 0

  End Sub ' ResetSubTotalCounters

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)


      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' PROVIDER
      .Column(GRID_COL_PARTNER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_PARTNER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5418)
      .Column(GRID_COL_PARTNER).Width = GRID_WIDTH_PARTNER
      .Column(GRID_COL_PARTNER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_PARTNER).Mapping = SQL_COL_PARTNER
      .Column(GRID_COL_PARTNER).IsMerged = False

      ' GG_NAME
      .Column(GRID_COL_GG_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_GG_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6940)
      .Column(GRID_COL_GG_NAME).Width = GRID_WIDTH_GG_NAME
      .Column(GRID_COL_GG_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_GG_NAME).Mapping = SQL_COL_GG_NAME
      .Column(GRID_COL_GG_NAME).IsMerged = False

      ' GGI_GAME_INSTANCE_ID
      .Column(GRID_COL_GGI_GAME_INSTANCE_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_GGI_GAME_INSTANCE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1579)
      .Column(GRID_COL_GGI_GAME_INSTANCE_ID).Width = GRID_WIDTH_GGI_GAME_INSTANCE_ID
      .Column(GRID_COL_GGI_GAME_INSTANCE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_GAME_INSTANCE_ID).Mapping = SQL_COL_GG_GAME_INSTANCE_ID
      .Column(GRID_COL_GGI_GAME_INSTANCE_ID).IsMerged = False

      ' GG_NAME_TRANSLATED (Hidden)
      .Column(GRID_COL_GG_NAME_TRANSLATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_GG_NAME_TRANSLATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6941)
      .Column(GRID_COL_GG_NAME_TRANSLATED).Width = 0
      .Column(GRID_COL_GG_NAME_TRANSLATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COL_GG_NAME_TRANSLATED).Mapping = SQL_COL_GG_NAME_TRANSLATED
      .Column(GRID_COL_GG_NAME_TRANSLATED).IsMerged = False

      ' GGI_STARTS
      .Column(GRID_COL_GGI_STARTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_GGI_STARTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_COL_GGI_STARTS).Width = GRID_WIDTH_GGI_STARTS
      .Column(GRID_COL_GGI_STARTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_GGI_STARTS).Mapping = SQL_COL_GGI_STARTS
      .Column(GRID_COL_GGI_STARTS).IsMerged = False

      ' GGI_CREATED
      .Column(GRID_COL_GGI_CREATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_GGI_CREATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2286)
      .Column(GRID_COL_GGI_CREATED).Width = GRID_WIDTH_GGI_CREATED
      .Column(GRID_COL_GGI_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_GGI_CREATED).Mapping = SQL_COL_GGI_CREATED
      .Column(GRID_COL_GGI_CREATED).IsMerged = False

      ' GGI_FINISHED
      .Column(GRID_COL_GGI_FINISHED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
      .Column(GRID_COL_GGI_FINISHED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356)
      .Column(GRID_COL_GGI_FINISHED).Width = GRID_WIDTH_GGI_FINISHED
      .Column(GRID_COL_GGI_FINISHED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_GGI_FINISHED).Mapping = SQL_COL_GGI_FINISHED
      .Column(GRID_COL_GGI_FINISHED).IsMerged = False

      ' GGI_NUM_BETS
      .Column(GRID_COL_GGI_NUM_BETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6943)
      .Column(GRID_COL_GGI_NUM_BETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6946)
      .Column(GRID_COL_GGI_NUM_BETS).Width = GRID_WIDTH_GGI_NUM_BETS
      .Column(GRID_COL_GGI_NUM_BETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_NUM_BETS).Mapping = SQL_COL_GGI_NUM_BETS
      .Column(GRID_COL_GGI_NUM_BETS).IsMerged = False

      ' NUM_BETS_ROLLBACK
      .Column(GRID_COL_GGI_NUM_BETS_ROLLBACK).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6943)
      .Column(GRID_COL_GGI_NUM_BETS_ROLLBACK).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7354)
      .Column(GRID_COL_GGI_NUM_BETS_ROLLBACK).Width = GRID_WIDTH_GGI_NUM_BETS_ROLLBACK
      .Column(GRID_COL_GGI_NUM_BETS_ROLLBACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_NUM_BETS_ROLLBACK).Mapping = SQL_COL_GGI_NUM_BETS_ROLLBACK
      .Column(GRID_COL_GGI_NUM_BETS_ROLLBACK).IsMerged = False

      ' GGI_PLAYED
      .Column(GRID_COL_GGI_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6943)
      .Column(GRID_COL_GGI_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COL_GGI_PLAYED).Width = GRID_WIDTH_GGI_PLAYED
      .Column(GRID_COL_GGI_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_PLAYED).Mapping = SQL_COL_GGI_PLAYED
      .Column(GRID_COL_GGI_PLAYED).IsMerged = False

      ' GGI_NUM_PRIZES
      .Column(GRID_COL_GGI_NUM_PRIZES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6944)
      .Column(GRID_COL_GGI_NUM_PRIZES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6947)
      .Column(GRID_COL_GGI_NUM_PRIZES).Width = GRID_WIDTH_GGI_NUM_PRIZES
      .Column(GRID_COL_GGI_NUM_PRIZES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_NUM_PRIZES).Mapping = SQL_COL_GGI_NUM_PRIZES
      .Column(GRID_COL_GGI_NUM_PRIZES).IsMerged = False

      ' GGI_WON
      .Column(GRID_COL_GGI_WON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6944)
      .Column(GRID_COL_GGI_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6944)
      .Column(GRID_COL_GGI_WON).Width = GRID_WIDTH_GGI_WON
      .Column(GRID_COL_GGI_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_WON).Mapping = SQL_COL_GGI_WON
      .Column(GRID_COL_GGI_WON).IsMerged = False

      ' GGI_WON_JACKPOT
      .Column(GRID_COL_GGI_WON_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6944)
      .Column(GRID_COL_GGI_WON_JACKPOT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6945)
      .Column(GRID_COL_GGI_WON_JACKPOT).Width = GRID_WIDTH_GGI_WON_JACKPOT
      .Column(GRID_COL_GGI_WON_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_WON_JACKPOT).Mapping = SQL_COL_GGI_WON_JACKPOT
      .Column(GRID_COL_GGI_WON_JACKPOT).IsMerged = False

      ' GGI_PENDING
      .Column(GRID_COL_GGI_PENDING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6944)
      .Column(GRID_COL_GGI_PENDING).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1973)
      .Column(GRID_COL_GGI_PENDING).Width = IIf(GeneralParam.GetBoolean("GameGateway", "AwardPrizes", True), 0, GRID_WIDTH_GGI_PENDING)
      .Column(GRID_COL_GGI_PENDING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_PENDING).Mapping = SQL_COL_GGI_PENDING
      .Column(GRID_COL_GGI_PENDING).IsMerged = False

      ' GGI_WON_TOTAL
      .Column(GRID_COL_GGI_WON_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6944)
      .Column(GRID_COL_GGI_WON_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7049)
      .Column(GRID_COL_GGI_WON_TOTAL).Width = GRID_WIDTH_GGI_WON
      .Column(GRID_COL_GGI_WON_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_GGI_WON_TOTAL).Mapping = SQL_COL_GGI_WON
      .Column(GRID_COL_GGI_WON_TOTAL).IsMerged = False

      ' PAYOUT
      .Column(GRID_COL_PAYOUT).Header(0).Text = String.Empty
      .Column(GRID_COL_PAYOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6948)
      .Column(GRID_COL_PAYOUT).Width = GRID_WIDTH_PAYOUT
      .Column(GRID_COL_PAYOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COL_PAYOUT).Mapping = SQL_COL_PAYOUT
      .Column(GRID_COL_PAYOUT).IsMerged = False

    End With

  End Sub ' GUI_StyleSheet



  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    'Provider / Game filter
    Me.uc_combo_game_gateway.SetDefaultValues()

    ' Dates
    _today_opening = WSI.Common.Misc.TodayOpening()
    Me.dtp_to.Value = _today_opening.AddDays(1)
    Me.dtp_to.Checked = True

    Me.dtp_from.Value = _today_opening
    Me.dtp_from.Checked = True

    Me.dtp_from.Enabled = True
    Me.dtp_to.Enabled = True

    Me.chk_hide_games_without_played.Checked = False

  End Sub ' SetDefaultValues


  ' PURPOSE: Insert Subtotal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertSubtotal(ByVal RowIndex As Integer)

    ' GG_NAME
    Me.Grid.Cell(RowIndex, GRID_COL_PARTNER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
    Me.Grid.Cell(RowIndex, GRID_COL_GG_NAME).Value = m_type_subtotal_game.m_subtotal_name

    ' GG_NAME_TRANSLATED
    Me.Grid.Cell(RowIndex, GRID_COL_GG_NAME_TRANSLATED).Value = m_type_subtotal_game.m_subtotal_name 'subtotal_name_translated

    ' GGI_NUM_BETS
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_BETS).Value = m_type_subtotal_game.m_subtotal_num_bets_count

    ' GGI_PLAYED
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_PLAYED).Value = GUI_FormatCurrency(m_type_subtotal_game.m_subtotal_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'GGI_NUM_PRIZES
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_PRIZES).Value = m_type_subtotal_game.m_subtotal_num_prizes_count

    ' GGI_WON
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON).Value = GUI_FormatCurrency(m_type_subtotal_game.m_subtotal_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' GGI_WON_JACKPOT
    If m_type_subtotal_game.m_subtotal_wonjackpot_amount > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON_JACKPOT).Value = GUI_FormatCurrency(m_type_subtotal_game.m_subtotal_wonjackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' GGI_PENDING
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_PENDING).Value = GUI_FormatCurrency(m_type_subtotal_game.m_subtotal_pending_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' GGI_WON_TOTAL
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON_TOTAL).Value = GUI_FormatCurrency(m_type_subtotal_game.m_subtotal_won_amount_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PAYOUT
    If (m_type_subtotal_game.m_subtotal_won_amount_total > 0 And m_type_subtotal_game.m_subtotal_played_amount > 0) Then
      Me.Grid.Cell(RowIndex, GRID_COL_PAYOUT).Value = GUI_FormatNumber(m_type_subtotal_game.m_subtotal_won_amount_total / IIf(m_type_subtotal_game.m_subtotal_played_amount = 0, 1, m_type_subtotal_game.m_subtotal_played_amount) * 100, 2) & "%"
    End If

    ' GGI_NUM_BETS
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_BETS_ROLLBACK).Value = m_type_subtotal_game.m_subtotal_num_bets_rollback_count

    ' Color Row
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Me.Grid.AddRow()

  End Sub ' InsertSubtotal

  ' PURPOSE: Insert Total by Game
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertTotalByGame(ByVal RowIndex As Integer)

    For Each kvp As KeyValuePair(Of Integer, TYPE_TOTAL_SUBTOTAL_GAMES) In m_dic_subtotal_game

      '  Dim v1 As Integer = kvp.Key
      Dim _subtotal As TYPE_TOTAL_SUBTOTAL_GAMES = kvp.Value

      ' GG_NAME_TRANSLATED
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_PARTNER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GG_NAME).Value = _subtotal.m_subtotal_name

      ' GGI_NUM_BETS
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_NUM_BETS).Value = _subtotal.m_subtotal_num_bets_count

      ' GGI_PLAYED
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_PLAYED).Value = GUI_FormatCurrency(_subtotal.m_subtotal_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      'GGI_NUM_PRIZES
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_NUM_PRIZES).Value = _subtotal.m_subtotal_num_prizes_count

      ' GGI_WON
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_WON).Value = GUI_FormatCurrency(_subtotal.m_subtotal_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' GGI_WON_JACKPOT
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_WON_JACKPOT).Value = GUI_FormatCurrency(_subtotal.m_subtotal_wonjackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' GGI_PENDING
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_PENDING).Value = GUI_FormatCurrency(_subtotal.m_subtotal_pending_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' GGI_WON_TOTAL
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_WON_TOTAL).Value = GUI_FormatCurrency(_subtotal.m_subtotal_won_amount_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' PAYOUT
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_PAYOUT).Value = GUI_FormatNumber(_subtotal.m_subtotal_won_amount_total / IIf(_subtotal.m_subtotal_played_amount = 0, 1, _subtotal.m_subtotal_played_amount) * 100, 2) & "%"

      ' GGI_NUM_BETS_ROLLBACK
      Me.Grid.Cell(Me.Grid.NumRows - 1, GRID_COL_GGI_NUM_BETS_ROLLBACK).Value = _subtotal.m_subtotal_num_bets_rollback_count


      ' Color Row
      Me.Grid.Row(Me.Grid.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      Me.Grid.AddRow()
    Next

  End Sub ' InsertTotalByGame

  ' PURPOSE: Insert Total
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertTotal(ByVal RowIndex As Integer)

    ' GG_NAME_TRANSLATED
    Me.Grid.Cell(RowIndex, GRID_COL_PARTNER).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    ' GGI_NUM_BETS
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_BETS).Value = m_total_num_bets_count

    ' GGI_PLAYED
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_PLAYED).Value = GUI_FormatCurrency(m_total_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'GGI_NUM_PRIZES
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_PRIZES).Value = m_total_num_prizes_count

    ' GGI_WON
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON).Value = GUI_FormatCurrency(m_total_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' GGI_WON_JACKPOT
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON_JACKPOT).Value = GUI_FormatCurrency(m_total_wonjackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' GGI_PENDING
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_PENDING).Value = GUI_FormatCurrency(m_total_pending_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' GGI_WON_TOTAL
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_WON_TOTAL).Value = GUI_FormatCurrency(m_total_won_amount_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PAYOUT
    Me.Grid.Cell(RowIndex, GRID_COL_PAYOUT).Value = GUI_FormatNumber((m_total_won_amount_total / IIf(m_total_played_amount = 0, 1, m_total_played_amount)) * 100, 2) & "%"

    'GRID_COL_GGI_NUM_BETS_ROLLBACK
    Me.Grid.Cell(RowIndex, GRID_COL_GGI_NUM_BETS_ROLLBACK).Value = m_total_num_bets_rollback_count


    ' Color Row
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01)

  End Sub ' InsertTotal


  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    ' Filter Game
    If Me.uc_combo_game_gateway.GetSelectedGameId <> String.Empty Then
      _str_where = " AND GGI.GGI_GAME_ID = " & Me.uc_combo_game_gateway.GetSelectedGameId
    End If

    '  Filter Partner
    If Me.uc_combo_game_gateway.GetSelectedProviderId <> String.Empty Then
      _str_where = _str_where & " AND CAST(GG.GG_PARTNER_ID AS INT) = " & Me.uc_combo_game_gateway.GetSelectedProviderId
    End If

    ' Filter Dates
    If Me.dtp_from.Checked Then
      _str_where = _str_where & " AND GGI.GGI_STARTS >= " & GUI_FormatDateDB(dtp_from.Value)
    End If

    If Me.dtp_to.Checked Then
      _str_where = _str_where & " AND GGI.GGI_STARTS < " & GUI_FormatDateDB(dtp_to.Value)
    End If

    ' Filter Hide Games Without Played
    If Me.chk_hide_games_without_played.Checked Then
      _str_where = _str_where & " AND (GGI.GGI_PLAYED > 0 OR ISNULL(GGI.GGI_NUM_BETS_ROLLBACK,0) > 0)"
    End If

    ' Final processing
    If Len(_str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If


    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Add in a list Total for each Game
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddGameSubTotal()

    Dim _subtotal_game As TYPE_TOTAL_SUBTOTAL_GAMES
    _subtotal_game = New TYPE_TOTAL_SUBTOTAL_GAMES

    _subtotal_game.m_subtotal_game_id = m_type_subtotal_game.m_subtotal_game_id

    If (Not m_dic_subtotal_game.ContainsKey(_subtotal_game.m_subtotal_game_id)) Then
      m_dic_subtotal_game.Add(_subtotal_game.m_subtotal_game_id, _subtotal_game)
    End If

    _subtotal_game = m_dic_subtotal_game(_subtotal_game.m_subtotal_game_id)


    _subtotal_game.m_subtotal_name = m_type_subtotal_game.m_subtotal_name
    _subtotal_game.m_subtotal_name_translated = m_type_subtotal_game.m_subtotal_name_translated

    _subtotal_game.m_subtotal_num_bets_count = _subtotal_game.m_subtotal_num_bets_count + m_type_subtotal_game.m_subtotal_num_bets_count
    _subtotal_game.m_subtotal_num_prizes_count = _subtotal_game.m_subtotal_num_prizes_count + m_type_subtotal_game.m_subtotal_num_prizes_count
    _subtotal_game.m_subtotal_played_amount = _subtotal_game.m_subtotal_played_amount + m_type_subtotal_game.m_subtotal_played_amount
    _subtotal_game.m_subtotal_won_amount = _subtotal_game.m_subtotal_won_amount + m_type_subtotal_game.m_subtotal_won_amount
    _subtotal_game.m_subtotal_wonjackpot_amount = _subtotal_game.m_subtotal_wonjackpot_amount + m_type_subtotal_game.m_subtotal_wonjackpot_amount
    _subtotal_game.m_subtotal_won_amount_total = _subtotal_game.m_subtotal_won_amount_total + m_type_subtotal_game.m_subtotal_won_amount_total
    _subtotal_game.m_subtotal_pending_amount = _subtotal_game.m_subtotal_pending_amount + m_type_subtotal_game.m_subtotal_pending_amount
    _subtotal_game.m_subtotal_num_bets_rollback_count = _subtotal_game.m_subtotal_num_bets_rollback_count + m_type_subtotal_game.m_subtotal_num_bets_rollback_count

    If (m_type_subtotal_game.m_subtotal_played_amount <> 0 And m_type_subtotal_game.m_subtotal_won_amount_total <> 0) Then
      _subtotal_game.m_subtotal_payout = m_type_subtotal_game.m_subtotal_won_amount_total / IIf(m_type_subtotal_game.m_subtotal_played_amount = 0, 1, m_type_subtotal_game.m_subtotal_played_amount) * 100
    End If


    m_dic_subtotal_game(_subtotal_game.m_subtotal_game_id) = _subtotal_game

  End Sub

#End Region ' Private Functions

End Class