'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_events_history
' DESCRIPTION:   This screen allows to view the history of all events
' AUTHOR:        Gabi Zutel
' CREATION DATE: 04-MAY-2004
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-MAY-2004  GZ     Initial version
' 04-APR-2007  MTM    WIGOS changed to read wigos event    
' 25-OCT-2011  JCA    Addade new Operation (COMMON_OPERATION_CODE_EVENT and SAS Events)
' 24-ABR-2012  SSC    Added new Operation (COMMON_OPERATION_CODE_CHANGE_STACKER)
' 03-MAY-2012  JAB    Fixed bug #290: Check the date filter
' 01-JUN-2012  RCI    Changed CHANGE_STACKER operation message
' 19-APR-2013  AMF    dg_devices order by description
' 30-APR-2013  HBB    Fixed Bug #747: The undefined SAS exceptions 110/111/112/113/114/115/116/121/122/123/124/126 are generated
' 05-MAR-2014  JCOR   Added PENDING_TRANSFER.
' 13-MAR-2014  ICS    Fixed bug #729: Missing descriptions for some event codes
' 18-MAR-2014  JCOR   Added CARD_ABANDONED for TITO.
' 03-JUL-2014  JMM    Added Technician card events
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_events_history
  Inherits frm_base_sel

  Public Structure TYPE_EVENT
    Dim event_type As Integer
    Dim event_code As Integer
    Dim event_nls_id As Integer

    Public Sub TYPE_EVENT(ByVal T As Integer, ByVal ID As Integer)

    End Sub
  End Structure

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_operations As System.Windows.Forms.GroupBox
  Friend WithEvents dg_devices As GUI_Controls.uc_grid
  Friend WithEvents gb_severity As System.Windows.Forms.GroupBox
  Friend WithEvents cb_error As System.Windows.Forms.CheckBox
  Friend WithEvents cb_warning As System.Windows.Forms.CheckBox
  Friend WithEvents cb_ok As System.Windows.Forms.CheckBox
  Friend WithEvents gb_event_type As System.Windows.Forms.GroupBox
  Friend WithEvents rb_all As System.Windows.Forms.RadioButton
  Friend WithEvents rb_devices As System.Windows.Forms.RadioButton
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents btn_check_all As GUI_Controls.uc_button
  Friend WithEvents rb_operations As System.Windows.Forms.RadioButton
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.gb_operations = New System.Windows.Forms.GroupBox
    Me.btn_uncheck_all = New GUI_Controls.uc_button
    Me.btn_check_all = New GUI_Controls.uc_button
    Me.dg_devices = New GUI_Controls.uc_grid
    Me.gb_severity = New System.Windows.Forms.GroupBox
    Me.cb_error = New System.Windows.Forms.CheckBox
    Me.cb_warning = New System.Windows.Forms.CheckBox
    Me.cb_ok = New System.Windows.Forms.CheckBox
    Me.gb_event_type = New System.Windows.Forms.GroupBox
    Me.rb_all = New System.Windows.Forms.RadioButton
    Me.rb_devices = New System.Windows.Forms.RadioButton
    Me.rb_operations = New System.Windows.Forms.RadioButton
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_operations.SuspendLayout()
    Me.gb_severity.SuspendLayout()
    Me.gb_event_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_event_type)
    Me.panel_filter.Controls.Add(Me.gb_severity)
    Me.panel_filter.Controls.Add(Me.gb_operations)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1099, 200)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_operations, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_severity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_event_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 204)
    Me.panel_data.Size = New System.Drawing.Size(1099, 514)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1093, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1093, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(272, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_operations
    '
    Me.gb_operations.Controls.Add(Me.btn_uncheck_all)
    Me.gb_operations.Controls.Add(Me.btn_check_all)
    Me.gb_operations.Controls.Add(Me.dg_devices)
    Me.gb_operations.Location = New System.Drawing.Point(623, 6)
    Me.gb_operations.Name = "gb_operations"
    Me.gb_operations.Size = New System.Drawing.Size(323, 181)
    Me.gb_operations.TabIndex = 5
    Me.gb_operations.TabStop = False
    Me.gb_operations.Text = "xOperations"
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Location = New System.Drawing.Point(101, 148)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 1
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Location = New System.Drawing.Point(6, 148)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 0
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'dg_devices
    '
    Me.dg_devices.CurrentCol = -1
    Me.dg_devices.CurrentRow = -1
    Me.dg_devices.Location = New System.Drawing.Point(6, 14)
    Me.dg_devices.Name = "dg_devices"
    Me.dg_devices.PanelRightVisible = False
    Me.dg_devices.Redraw = True
    Me.dg_devices.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_devices.Size = New System.Drawing.Size(311, 132)
    Me.dg_devices.Sortable = False
    Me.dg_devices.SortAscending = True
    Me.dg_devices.SortByCol = 0
    Me.dg_devices.TabIndex = 2
    Me.dg_devices.ToolTipped = True
    Me.dg_devices.TopRow = -2
    '
    'gb_severity
    '
    Me.gb_severity.Controls.Add(Me.cb_error)
    Me.gb_severity.Controls.Add(Me.cb_warning)
    Me.gb_severity.Controls.Add(Me.cb_ok)
    Me.gb_severity.Location = New System.Drawing.Point(128, 84)
    Me.gb_severity.Name = "gb_severity"
    Me.gb_severity.Size = New System.Drawing.Size(152, 89)
    Me.gb_severity.TabIndex = 3
    Me.gb_severity.TabStop = False
    Me.gb_severity.Text = "xSeverity"
    '
    'cb_error
    '
    Me.cb_error.AutoSize = True
    Me.cb_error.Location = New System.Drawing.Point(6, 65)
    Me.cb_error.Name = "cb_error"
    Me.cb_error.Size = New System.Drawing.Size(75, 17)
    Me.cb_error.TabIndex = 2
    Me.cb_error.Text = "cb_error"
    Me.cb_error.UseVisualStyleBackColor = True
    '
    'cb_warning
    '
    Me.cb_warning.AutoSize = True
    Me.cb_warning.Location = New System.Drawing.Point(6, 42)
    Me.cb_warning.Name = "cb_warning"
    Me.cb_warning.Size = New System.Drawing.Size(91, 17)
    Me.cb_warning.TabIndex = 1
    Me.cb_warning.Text = "cb_warning"
    Me.cb_warning.UseVisualStyleBackColor = True
    '
    'cb_ok
    '
    Me.cb_ok.AutoSize = True
    Me.cb_ok.Location = New System.Drawing.Point(6, 20)
    Me.cb_ok.Name = "cb_ok"
    Me.cb_ok.Size = New System.Drawing.Size(60, 17)
    Me.cb_ok.TabIndex = 0
    Me.cb_ok.Text = "cb_ok"
    Me.cb_ok.UseVisualStyleBackColor = True
    '
    'gb_event_type
    '
    Me.gb_event_type.Controls.Add(Me.rb_all)
    Me.gb_event_type.Controls.Add(Me.rb_devices)
    Me.gb_event_type.Controls.Add(Me.rb_operations)
    Me.gb_event_type.Location = New System.Drawing.Point(8, 84)
    Me.gb_event_type.Name = "gb_event_type"
    Me.gb_event_type.Size = New System.Drawing.Size(114, 89)
    Me.gb_event_type.TabIndex = 2
    Me.gb_event_type.TabStop = False
    Me.gb_event_type.Text = "xEvent Type"
    '
    'rb_all
    '
    Me.rb_all.AutoSize = True
    Me.rb_all.Checked = True
    Me.rb_all.Location = New System.Drawing.Point(6, 65)
    Me.rb_all.Name = "rb_all"
    Me.rb_all.Size = New System.Drawing.Size(57, 17)
    Me.rb_all.TabIndex = 2
    Me.rb_all.TabStop = True
    Me.rb_all.Text = "rb_all"
    Me.rb_all.UseVisualStyleBackColor = True
    '
    'rb_devices
    '
    Me.rb_devices.AutoSize = True
    Me.rb_devices.Location = New System.Drawing.Point(6, 42)
    Me.rb_devices.Name = "rb_devices"
    Me.rb_devices.Size = New System.Drawing.Size(87, 17)
    Me.rb_devices.TabIndex = 1
    Me.rb_devices.Text = "rb_devices"
    Me.rb_devices.UseVisualStyleBackColor = True
    '
    'rb_operations
    '
    Me.rb_operations.AutoSize = True
    Me.rb_operations.Location = New System.Drawing.Point(6, 19)
    Me.rb_operations.Name = "rb_operations"
    Me.rb_operations.Size = New System.Drawing.Size(104, 17)
    Me.rb_operations.TabIndex = 0
    Me.rb_operations.Text = "rb_operations"
    Me.rb_operations.UseVisualStyleBackColor = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(286, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 4
    '
    'frm_events_history
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1107, 722)
    Me.Name = "frm_events_history"
    Me.Text = "frm_events_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_operations.ResumeLayout(False)
    Me.gb_severity.ResumeLayout(False)
    Me.gb_severity.PerformLayout()
    Me.gb_event_type.ResumeLayout(False)
    Me.gb_event_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' event type
  Const WCP_EVENT_TYPE_DEVICE_STATUS As Integer = 1
  Const WCP_EVENT_TYPE_OPERATION As Integer = 2

  ' priority
  Const COMMON_PRIORITY_OK = 0
  Const COMMON_PRIORITY_WARNING = 1
  Const COMMON_PRIORITY_ERROR = 2

  ' Device Codes 
  Const COMMON_DEVICE_CODE_NO_DEVICE = 0
  Const COMMON_DEVICE_CODE_PRINTER = 1
  Const COMMON_DEVICE_CODE_NOTE_ACCEPTOR = 2
  Const COMMON_DEVICE_CODE_SMARTCARD_READER = 3
  Const COMMON_DEVICE_CODE_CUSTOMER_DISPLAY = 4
  Const COMMON_DEVICE_CODE_COIN_ACCEPTOR = 5
  Const COMMON_DEVICE_CODE_UPPER_DISPLAY = 6
  Const COMMON_DEVICE_CODE_BAR_CODE_READER = 7
  Const COMMON_DEVICE_CODE_INSTRUSION = 8
  Const COMMON_DEVICE_CODE_UPS = 9
  Const COMMON_DEVICE_CODE_MODULE_IO = 15
  ' SLE: Devices Codes Added or Removed 18/11/09
  'Const COMMON_DEVICE_CODE_COMMUNICATIONS = 8
  'Const COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION = 10
  'Const COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION = 11
  'Const COMMON_DEVICE_CODE_FLASH_1 = 12
  'Const COMMON_DEVICE_CODE_FLASH_2 = 13
  'Const COMMON_DEVICE_CODE_KIOSK = 14
  ' Const COMMON_DEVICE_CODE_AGENCY_CONNECTION = 16

  Const COMMON_DEVICE_CODE_DISPLAY_MAIN = 17
  Const COMMON_DEVICE_CODE_DISPLAY_TOP = 18
  Const COMMON_DEVICE_CODE_TOUCH_PAD = 19
  Const COMMON_DEVICE_CODE_KEYBOARD = 20

  Const COMMON_DEVICE_CODE_UNICASH_KIT_LCD = 21
  Const COMMON_DEVICE_CODE_UNICASH_KIT_KEYPAD = 22
  Const COMMON_DEVICE_CODE_UNICASH_KIT_CARD_READER = 23
  Const COMMON_DEVICE_CODE_UNICASH_KIT_BOARD = 24

  Const COMMON_DEVICE_STATUS_UNKNOWN = 0
  Const COMMON_DEVICE_STATUS_OK = 1
  Const COMMON_DEVICE_STATUS_ERROR = 2
  Const COMMON_DEVICE_STATUS_NOT_INSTALLED = 100

  ' Play status
  Const COMMON_PLAY_STATUS_OK = 0
  Const COMMON_PLAY_STATUS_ERROR_GET_RNG = 1

  ' Printer status
  Const COMMON_DEVICE_STATUS_PRINTER_OFF = 3
  Const COMMON_DEVICE_STATUS_PRINTER_READY = 4
  Const COMMON_DEVICE_STATUS_PRINTER_NOT_READY = 5
  Const COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT = 6
  Const COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW = 7
  Const COMMON_DEVICE_STATUS_PRINTER_OFFLINE = 8

  ' Note acceptor status
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM = 3
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR = 4
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL = 5
  Const COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN = 6

  ' Ups status
  Const COMMON_DEVICE_STATUS_UPS_NO_AC = 3
  Const COMMON_DEVICE_STATUS_UPS_BATTERY_LOW = 4
  Const COMMON_DEVICE_STATUS_UPS_OVERLOAD = 5
  Const COMMON_DEVICE_STATUS_UPS_OFF = 6
  Const COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL = 7

  ' Smartcard status
  Const COMMON_DEVICE_STATUS_SMARTCARD_REMOVED = 3
  Const COMMON_DEVICE_STATUS_SMARTCARD_INSERTED = 4

  '' SLE: Devices Status Removed 18/11/09
  '' software status
  'Const COMMON_DEVICE_STATUS_VERSION_ERROR = 3
  'Const COMMON_DEVICE_STATUS_DL_ERROR = 4

  '' Kiosk status
  'Const COMMON_DEVICE_STATUS_KIOSK_ACTIVE = 3
  'Const COMMON_DEVICE_STATUS_KIOSK_NOT_ACTIVE = 4

  '' Agency connection
  'Const COMMON_DEVICE_STATUS_CONNECTED = 3
  'Const COMMON_DEVICE_STATUS_NOT_CONNECTED = 4

  ' Coin acceptor status
  Const COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM = 3
  Const COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR = 4
  Const COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL = 5

  Const COMMON_KIOSK_DOOR_1 = 1
  Const COMMON_KIOSK_DOOR_2 = 2
  Const COMMON_KIOSK_DOOR_3 = 3
  Const COMMON_KIOSK_DOOR_4 = 4
  Const COMMON_KIOSK_DOOR_5 = 5

  ' Operations
  Const COMMON_OPERATION_CODE_NO_OPERATION = 0
  Const COMMON_OPERATION_CODE_START = 1
  Const COMMON_OPERATION_CODE_SHUTDOWN = 2
  Const COMMON_OPERATION_CODE_RESTART = 3
  Const COMMON_OPERATION_CODE_TUNE_SCREEN = 4
  Const COMMON_OPERATION_CODE_LAUNCH_EXPLORER = 5
  Const COMMON_OPERATION_CODE_ASSIGN_KIOSK = 6
  Const COMMON_OPERATION_CODE_DEASSIGN_KIOSK = 7
  Const COMMON_OPERATION_CODE_UNLOCK_DOOR = 8
  Const COMMON_OPERATION_CODE_LOGIN = 9
  Const COMMON_OPERATION_CODE_LOGOUT = 10
  Const COMMON_OPERATION_CODE_DISPLAY_SETTINGS = 11
  Const COMMON_OPERATION_CODE_DOOR_OPENED = 12
  Const COMMON_OPERATION_CODE_DOOR_CLOSED = 13
  Const COMMON_OPERATION_CODE_BLOCK_KIOSK = 14
  Const COMMON_OPERATION_CODE_UNBLOCK_KIOSK = 15
  Const COMMON_OPERATION_CODE_JACKPOT_WON = 16
  Const COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE = 17
  Const COMMON_OPERATION_CODE_CALL_ATTENDANT = 18
  Const COMMON_OPERATION_CODE_TEST_MODE_ENTER = 19
  Const COMMON_OPERATION_CODE_TEST_MODE_LEAVE = 20
  Const COMMON_OPERATION_CODE_LOGIN_ERROR = 21
  Const COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER = 22
  Const COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT = 23
  Const COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER = 24
  Const COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT = 25
  Const COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED = 26
  Const COMMON_OPERATION_CODE_HANDPAY_REQUESTED = 27
  Const COMMON_OPERATION_CODE_HANDPAY_RESET = 28
  Const COMMON_OPERATION_CODE_CARD_ABANDONED = 29
  Const COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS = 30
	Const COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE = 31
	' JCA 25-OCT-2011
  Const COMMON_OPERATION_CODE_EVENT = 32
  ' SSC 19-ABR-2012
  Const COMMON_OPERATION_CODE_CHANGE_STACKER = 33
  Const COMMON_OPERATION_CODE_SECURITY_CASHOUT_UNITS = 34
  Const COMMON_OPERATION_CODE_GAME_FATAL_ERROR = 35
  Const COMMON_OPERATION_CODE_PENDING_TRANSFER_IN = 36
  Const COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT = 37
  Const COMMON_OPERATION_CODE_TECH_OFFLINE_INSERTED = 38
  Const COMMON_OPERATION_CODE_TECH_OFFLINE_REMOVED = 39

  Private Const SQL_COLUMN_EVENT_ID As Integer = 0
  Private Const SQL_COLUMN_REPORTED As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 2
  Private Const SQL_COLUMN_DATETIME As Integer = 3
  Private Const SQL_COLUMN_EVENT_TYPE As Integer = 4
  Private Const SQL_COLUMN_EVENT_DATA As Integer = 5
  Private Const SQL_COLUMN_EVENT_DEVICE_CODE As Integer = 6
  Private Const SQL_COLUMN_EVENT_STATUS As Integer = 7
  Private Const SQL_COLUMN_EVENT_PRIORITY As Integer = 8
  Private Const SQL_COLUMN_OPERATION_CODE As Integer = 9
  Private Const SQL_COLUMN_OPERATION_DATA As Integer = 10

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATETIME As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 2
  Private Const GRID_COLUMN_PRIORITY As Integer = 3
  Private Const GRID_COLUMN_DEVICE As Integer = 4
  Private Const GRID_COLUMN_STATUS As Integer = 5

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_TYPE As Integer = 3
  Private Const GRID_2_COLUMNS As Integer = 4
  Private Const GRID_2_HEADER_ROWS As Integer = 0
  Private Const GRID_2_TAB As String = "    "

  Private Const TYPE_OPERATION As Integer = 1
  Private Const TYPE_DEVICE As Integer = 2

  Private Const DEVICE_AGENCY As Integer = 1
  Private Const DEVICE_KIOSK As Integer = 2
  Private Const DEVICE_BOTH As Integer = DEVICE_AGENCY + DEVICE_KIOSK

  Private Const MAX_KIOSK_LEN As Integer = 3

  'Device Status/priority
  Private Const DEVICE_PRIORITY_OK As Integer = 0
  Private Const DEVICE_PRIORITY_WARNING As Integer = 1
  Private Const DEVICE_PRIORITY_ERROR As Integer = 2

  'Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

  ' JCA 25-OCT-2011
  ' ICS 13-MAR-2014: new event codes
	' SAS Events
	Private Const EVENT_SAS_SLOT_DOOR_OPENED = 17
	Private Const EVENT_SAS_SLOT_DOOR_CLOSED = 18
	Private Const EVENT_SAS_DROP_DOOR_OPENED = 19
	Private Const EVENT_SAS_DROP_DOOR_CLOSED = 20
	Private Const EVENT_SAS_CARD_CAGE_OPENED = 21
	Private Const EVENT_SAS_CARD_CAGE_CLOSED = 22
	Private Const EVENT_SAS_AC_POWER_APPLIED_GAMING_MACHINE = 23
	Private Const EVENT_SAS_AC_POWER_LOST_GAMING_MACHINE = 24
	Private Const EVENT_SAS_CASHBOX_DOOR_OPENED = 25
	Private Const EVENT_SAS_CASHBOX_DOOR_CLOSED = 26
	Private Const EVENT_SAS_CASHBOX_REMOVED = 27
	Private Const EVENT_SAS_CASHBOX_INSTALLED = 28
	Private Const EVENT_SAS_BELLY_DOOR_OPENED = 29
	Private Const EVENT_SAS_BELLY_DOOR_CLOSED = 30
  Private Const EVENT_SAS_GENERAL_LITL = 32
  Private Const EVENT_SAS_CASHBOX_FULL = 39 ' 0x0027
  Private Const EVENT_SAS_BILL_JAM = 40 ' 0x0028
  Private Const EVENT_SAS_BILL_VAL_HARDWARE_FAILURE = 41 ' 0x0029
  Private Const EVENT_SAS_REVERSE_BILL = 42 ' 0x002A
  Private Const EVENT_SAS_BILL_REJECTED = 43 ' 0x002B
  Private Const EVENT_SAS_BILL_COUNTERFEIT = 44 ' 0x002C
  Private Const EVENT_SAS_CASHBOX_NEAR_FULL = 46 ' 0x002E
	Private Const EVENT_SAS_CMOS_RAM_ERROR_DATA_RECOVERED_FROM_EEPROM = 49
	Private Const EVENT_SAS_CMOS_RAM_ERROR_NO_DATA_RECOVERED_FROM_EEPROM = 50
	Private Const EVENT_SAS_CMOS_RAM_ERROR_BAD_DEVICE = 51
	Private Const EVENT_SAS_EEPROM_ERROR_DATA_ERROR = 52
	Private Const EVENT_SAS_EEPROM_ERROR_BAD_DEVICE = 53
	Private Const EVENT_SAS_EPROM_ERROR_DIFFERENT_CHECKSUM_VERSION_CHANGED = 54
	Private Const EVENT_SAS_EPROM_ERROR_BAD_CHECKSUM_COMPARE = 55
	Private Const EVENT_SAS_PARTITIONED_EPROM_ERROR_CHECKSUM_VERSION_CHANGED = 56
	Private Const EVENT_SAS_PARTITIONED_EPROM_ERROR_AD_CHECKSUM_COMPARE = 57
	Private Const EVENT_SAS_MEMORY_ERROR_RESET = 58
	Private Const EVENT_SAS_LOW_BACKUP_BATTERY_DETECTED = 59
	Private Const EVENT_SAS_OPERATOR_CHANGED_OPTIONS = 60
	Private Const EVENT_SAS_HANDPAY_PENDING = 81
	Private Const EVENT_SAS_HANDPAY_RESET = 82
	Private Const EVENT_SAS_PROGRESSIVE_WIN = 84
	Private Const EVENT_SAS_PALYER_CANCELLED_HANDPAY_REQUEST = 85
  Private Const EVENT_SAS_PROGRESSIVE_LEVEL_HIT = 86
  Private Const EVENT_SAS_PRINTER_COMM_ERROR = 96 ' 0x0060
  Private Const EVENT_SAS_PRINTER_PAPER_OUT = 97 ' 0x0061  
	Private Const EVENT_SAS_EXCEPTION_BUFFER_OVERFLOW = 112
	Private Const EVENT_SAS_GAME_CHANGE_LAMP_ON = 113
  Private Const EVENT_SAS_GAME_CHANGE_LAMP_OFF = 114
  Private Const EVENT_SAS_PRINTER_PAPER_LOW = 116 ' 0x0074
  Private Const EVENT_SAS_PRINTER_POWER_OFF = 117 ' 0x0075
  Private Const EVENT_SAS_PRINTER_POWER_ON = 118 ' 0x0076
  Private Const EVENT_SAS_PRINTER_REPLACE_RIBBON = 119 ' 0x0077
  Private Const EVENT_SAS_PRINTER_CARRIAGE_JAMMED = 120 ' 0x0078
	Private Const EVENT_SAS_GAMING_MACHINE_SOFT_METERS_RESET_TO_ZERO = 122
	Private Const EVENT_SAS_DISPLAY_METERS_OR_ATTENDANT_MENU_ENTERED = 130
	Private Const EVENT_SAS_DISPLAY_METERS_OR_ATTENDANT_MENU_EXITED = 131
	Private Const EVENT_SAS_SELF_TEST_OR_OPERATOR_MENU_ENTERED = 132
	Private Const EVENT_SAS_SELF_TEST_OR_OPERATOR_MENU_EXITED = 133
	Private Const EVENT_SAS_GAMING_MACHINE_OUT_OF_SERVICE = 134
	Private Const EVENT_SAS_GAME_RECALL_ENTRY_DISPLAYED = 138
	Private Const EVENT_SAS_POWER_OFF_CARD_CAGE_ACCESS = 152
	Private Const EVENT_SAS_POWER_OFF_SLOT_DOOR_ACCESS = 153
	Private Const EVENT_SAS_POWER_OFF_CASHBOX_DOOR_ACCESS = 154
	Private Const EVENT_SAS_POWER_OFF_DROP_DOOR_ACCESS = 155
	Private Const EVENT_SAS_DISCONNECTED = 257
  Private Const EVENT_SAS_CONNECTED = 258
  Private Const EVENT_SAS_COULD_NOT_GET_ANY_COUNTER = &H110
  Private Const EVENT_SAS_ERROR_GETTING_COUNTERS = &H111
  Private Const EVENT_SAS_ERROR_GETTING_HAND_PAYS_COUNTER = &H112
  Private Const EVENT_SAS_ERROR_GETING_COUNTERS_GAME = &H113
  Private Const EVENT_SAS_ERROR_GETTING_COUNTERS_EXTENDED = &H114
  Private Const EVENT_SAS_ERROR_GETTING_EXTENDED_COUNTER_OF_MANUAL_PAYMENTS = &H115
  Private Const EVENT_SAS_ERROR_GETTING_COUNTERS_EXTENDED_PLAY = &H116
  Private Const EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS = &H121
  Private Const EVENT_SAS_RECURRENT_ERROR_GETTING_HAND_PAYS_COUNTER = &H122
  Private Const EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS_GAME = &H123
  Private Const EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS_EXTENDED = &H124
  Private Const EVENT_SAS_RECURRENT_ERROR_GETTING_EXTENDED_COUNTER_OF_MANUAL_PAYMENTS = &H125
  Private Const EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS_EXTENDED_PLAY = &H126

  Private Const MAX_FILTER_REPORT = 4

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_events As String
  Private m_terminals As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_severity As String
  Private m_code As String

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_EVENTS_HISTORY

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(267)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_ALARMS.GetString(443)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Operation
    Me.gb_operations.Text = GLB_NLS_GUI_ALARMS.GetString(445)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes())

    'Severity
    Me.gb_severity.Text = GLB_NLS_GUI_ALARMS.GetString(448)
    Me.cb_ok.Text = GLB_NLS_GUI_ALARMS.GetString(356)
    Me.cb_warning.Text = GLB_NLS_GUI_ALARMS.GetString(358)
    Me.cb_error.Text = GLB_NLS_GUI_ALARMS.GetString(357)

    Me.gb_event_type.Text = GLB_NLS_GUI_ALARMS.GetString(451)
    Me.rb_devices.Text = GLB_NLS_GUI_ALARMS.GetString(446)
    Me.rb_operations.Text = GLB_NLS_GUI_ALARMS.GetString(445)
    Me.rb_all.Text = GLB_NLS_GUI_ALARMS.GetString(277)


    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    Call GUI_StyleSheetDeviceOperation()

    Call GUI_StyleSheet()

    FillFilterGrid()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' JAB 04-MAY-2012: Check the date filter
    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Events  selection
    If (GetEventIdListSelected() = "" And GetSeverityIdListSelected() = "") Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(104), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Me.dg_devices.CurrentRow = 0

      'XVV 16/04/2007
      'Change Me.XXX for frm_events_history, compiler generate a warning
      Me.dg_devices.CurrentCol = GRID_2_COLUMN_CHECKED
      Me.dg_devices.Focus()
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = "SELECT  EH_EVENT_ID " & _
                   " , EH_REPORTED " & _
                   " , TE_NAME " & _
                   " , EH_DATETIME " & _
                   " , EH_EVENT_TYPE " & _
                   " , EH_EVENT_DATA " & _
                   " , EH_DEVICE_CODE " & _
                   " , EH_DEVICE_STATUS " & _
                   " , EH_DEVICE_PRIORITY " & _
                   " , EH_OPERATION_CODE " & _
                   " , EH_OPERATION_DATA " & _
                   " FROM EVENT_HISTORY " & _
                      "INNER JOIN TERMINALS " & _
                          "ON EH_TERMINAL_ID=TE_TERMINAL_ID"

    str_sql = str_sql & GetSqlWhere()

    str_sql = str_sql & " ORDER BY EH_DATETIME DESC, EH_EVENT_ID DESC"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Reported
    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    ' Terminal
    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Event type
    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning

    Select Case DbRow.Value(SQL_COLUMN_EVENT_TYPE)

      Case WCP_EVENT_TYPE_DEVICE_STATUS
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DEVICE).Value = DecodeDevice(DbRow.Value(SQL_COLUMN_EVENT_DEVICE_CODE), DbRow.Value(SQL_COLUMN_EVENT_STATUS), 1)
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_EVENT_PRIORITY)) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PRIORITY).Value = DecodePriority(DbRow.Value(SQL_COLUMN_EVENT_PRIORITY))
        End If
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = DecodeDevice(DbRow.Value(SQL_COLUMN_EVENT_DEVICE_CODE), DbRow.Value(SQL_COLUMN_EVENT_STATUS), 2)

      Case WCP_EVENT_TYPE_OPERATION
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DEVICE).Value = DecodeOperationCode(DbRow.Value(SQL_COLUMN_OPERATION_CODE), DbRow.Value(SQL_COLUMN_OPERATION_DATA))
    End Select

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(451), m_events)
    If rb_devices.Checked Or rb_all.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(416), m_severity)
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(445), m_code)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 1000
    PrintData.FilterValueWidth(3) = 5000
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    PrintData.Settings.Columns(0).Width = 2000
    PrintData.Settings.Columns(0).IsWidthFixed = True
    PrintData.Settings.Columns(1).Width = 2300
    PrintData.Settings.Columns(1).IsWidthFixed = True
    PrintData.Settings.Columns(2).Width = 2000
    PrintData.Settings.Columns(2).IsWidthFixed = True
    PrintData.Settings.Columns(3).Width = 5000

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim no_one As Boolean
    Dim all As Boolean
    Dim counter As Integer

    m_events = ""
    m_terminals = ""
    m_date_from = ""
    m_date_to = ""
    m_severity = ""

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    '' Devices / Operations
    If rb_all.Checked Then
      m_events = Me.rb_all.Text
    End If

    If rb_devices.Checked Then
      m_events = Me.rb_devices.Text
    End If

    If rb_operations.Checked Then
      m_events = Me.rb_operations.Text
    End If

    no_one = True
    all = True
    If cb_ok.Checked Then
      m_severity = m_severity & Me.cb_ok.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If cb_warning.Checked Then
      m_severity = m_severity & Me.cb_warning.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If cb_error.Checked Then
      m_severity = m_severity & Me.cb_error.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If m_severity.Length <> 0 Then
      m_severity = Strings.Left(m_severity, Len(m_severity) - 2)
    End If

    If no_one Or all Then
      m_severity = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    Dim idx_rows As Integer
    Dim all_checked As Boolean

    all_checked = True

    For idx_rows = 0 To dg_devices.NumRows - 1
      If Not dg_devices.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then

        all_checked = False
        Exit For

      End If
    Next

    If all_checked = True Then
      m_code = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_code = ""
      For idx_rows = 0 To dg_devices.NumRows - 1
        If dg_devices.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          m_code = m_code & dg_devices.Cell(idx_rows, GRID_2_COLUMN_DESC).Value & "; "

          counter = counter + 1
          If counter > MAX_FILTER_REPORT Then
            m_code = GLB_NLS_GUI_INVOICING.GetString(223)
            Exit For
          End If
        End If
      Next

    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Date Report
      'XVV 16/04/2007
      'Change Me.XXX for frm_events_history, compiler generate a warning
      '------------------------------------------------------------------------------------------------
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_DATETIME).Width = 2100
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(442)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 2500
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Severity
      .Column(GRID_COLUMN_PRIORITY).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(416)
      .Column(GRID_COLUMN_PRIORITY).Width = 1000
      .Column(GRID_COLUMN_PRIORITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Device
      .Column(GRID_COLUMN_DEVICE).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(444)
      .Column(GRID_COLUMN_DEVICE).Width = 5700
      .Column(GRID_COLUMN_DEVICE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(447)
      .Column(GRID_COLUMN_STATUS).Width = 2000
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      '----------------------------------------------------------------------------------------------------

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    Me.dtp_from.Checked = True

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

    rb_operations.Checked = True
    rb_devices.Checked = True
    rb_all.Checked = True

    cb_ok.Checked = True
    cb_warning.Checked = True
    cb_error.Checked = True

    Call btn_check_all_Click()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (EH_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (EH_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    ' Filter Providers - Terminals
    str_where = str_where & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    If rb_all.Checked Then
      str_where = str_where & "  AND (EH_OPERATION_CODE IN (" & GetEventIdListSelected() & ") "
      str_where = str_where & " OR EH_DEVICE_PRIORITY IN (" & GetSeverityIdListSelected() & ") )"
    Else
      If rb_devices.Checked Then
        str_where = str_where & "  AND (EH_DEVICE_PRIORITY IN (" & GetSeverityIdListSelected() & ") )"
      Else
        str_where = str_where & "  AND (EH_OPERATION_CODE IN (" & GetEventIdListSelected() & ") ) "
      End If
    End If

    If str_where <> "" Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where

  End Function ' GetSqlWhere

  Private Function DecodeDevice(ByVal DeviceNameId As Integer, ByVal DeviceStatusId As Integer, ByVal ReturnedValue As Integer) As String

    Dim DeviceName As String
    Dim DeviceStatus As String
    Dim Result As String

    DeviceName = ""
    DeviceStatus = ""

    Select Case DeviceNameId

      Case COMMON_DEVICE_CODE_PRINTER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(295)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_PRINTER_OFF
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_PRINTER_OFFLINE
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(306)
          Case COMMON_DEVICE_STATUS_PRINTER_NOT_READY
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(307)
          Case COMMON_DEVICE_STATUS_PRINTER_READY
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(321)
          Case COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(322)
          Case COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(323)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_NOTE_ACCEPTOR
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(296)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(308)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(324)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(325)
          Case COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_OPEN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(387)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_SMARTCARD_READER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(297)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_CUSTOMER_DISPLAY
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(300)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UPPER_DISPLAY
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(302)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_BAR_CODE_READER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(298)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_INSTRUSION
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(374) ' Intrusion
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320) ' OK
          Case Else
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(375) ' Detected
        End Select


      Case COMMON_DEVICE_CODE_UPS
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(304)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UPS_NO_AC
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(309)
          Case COMMON_DEVICE_STATUS_UPS_BATTERY_LOW
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(328)
          Case COMMON_DEVICE_STATUS_UPS_OVERLOAD
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(327)
          Case COMMON_DEVICE_STATUS_UPS_OFF
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(329)
          Case COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(330)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

        'Case COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(312)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_NOT_INSTALLED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(311)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_NOT_INSTALLED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_FLASH_1
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(315)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_FLASH_2
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(316)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_ERROR
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        '    Case COMMON_DEVICE_STATUS_OK
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        '  End Select

        'Case COMMON_DEVICE_CODE_KIOSK
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(341)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_UNKNOWN
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
        'Case COMMON_DEVICE_STATUS_KIOSK_ACTIVE
        '  DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(333)
        'Case COMMON_DEVICE_STATUS_KIOSK_NOT_ACTIVE
        '  DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(334)
        'End Select

        'Case COMMON_DEVICE_CODE_AGENCY_CONNECTION
        '  DeviceName = GLB_NLS_GUI_ALARMS.GetString(343)
        '  Select Case DeviceStatusId
        '    Case COMMON_DEVICE_STATUS_CONNECTED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(344)
        '    Case COMMON_DEVICE_STATUS_NOT_CONNECTED
        '      DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(345)
        '  End Select

      Case COMMON_DEVICE_CODE_MODULE_IO
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(348)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_COIN_ACCEPTOR
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(350)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(308)
          Case COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(324)
          Case COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(325)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_LCD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(370)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_KEYPAD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(371)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_CARD_READER
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(372)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_UNICASH_KIT_BOARD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(373)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_DISPLAY_MAIN
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(379)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_DISPLAY_TOP
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(380)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_TOUCH_PAD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(381)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

      Case COMMON_DEVICE_CODE_KEYBOARD
        DeviceName = GLB_NLS_GUI_ALARMS.GetString(382)
        Select Case DeviceStatusId
          Case COMMON_DEVICE_STATUS_ERROR
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(305)
          Case COMMON_DEVICE_STATUS_UNKNOWN
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(326)
          Case COMMON_DEVICE_STATUS_NOT_INSTALLED
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(351)
          Case COMMON_DEVICE_STATUS_OK
            DeviceStatus = GLB_NLS_GUI_ALARMS.GetString(320)
        End Select

    End Select
    Result = ""
    If ReturnedValue = 1 Then
      Result = DeviceName
    End If
    If ReturnedValue = 2 Then
      Result = DeviceStatus
    End If

    Return Result
  End Function ' DecodeDevice


  Private Function DecodePriority(ByVal Id As Integer) As String

    Dim result As String

    result = ""
    Select Case Id
      Case COMMON_PRIORITY_OK
        result = GLB_NLS_GUI_ALARMS.GetString(356)
      Case COMMON_PRIORITY_ERROR
        result = GLB_NLS_GUI_ALARMS.GetString(357)
      Case COMMON_PRIORITY_WARNING
        result = GLB_NLS_GUI_ALARMS.GetString(358)
    End Select

    Return result
  End Function ' DecodePriority

  Private Function DecodeOperationCode(ByVal Id As Integer, ByVal Data As Decimal) As String

    Dim result As String
    Dim aux As String
    Dim _int_value As Integer

    result = ""
    Select Case Id
      Case COMMON_OPERATION_CODE_START
        result = GLB_NLS_GUI_ALARMS.GetString(331)
      Case COMMON_OPERATION_CODE_SHUTDOWN
        result = GLB_NLS_GUI_ALARMS.GetString(332)
      Case COMMON_OPERATION_CODE_RESTART
        result = GLB_NLS_GUI_ALARMS.GetString(336)
      Case COMMON_OPERATION_CODE_LAUNCH_EXPLORER
        result = GLB_NLS_GUI_ALARMS.GetString(337)
      Case COMMON_OPERATION_CODE_TUNE_SCREEN
        result = GLB_NLS_GUI_ALARMS.GetString(338)
      Case COMMON_OPERATION_CODE_ASSIGN_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(339, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_DEASSIGN_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(340, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_UNLOCK_DOOR
        result = GLB_NLS_GUI_ALARMS.GetString(335)
      Case COMMON_OPERATION_CODE_LOGIN
        result = GLB_NLS_GUI_ALARMS.GetString(346, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_LOGOUT
        result = GLB_NLS_GUI_ALARMS.GetString(347, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_DISPLAY_SETTINGS
        result = GLB_NLS_GUI_ALARMS.GetString(349)
      Case COMMON_OPERATION_CODE_DOOR_OPENED
        result = GLB_NLS_GUI_ALARMS.GetString(352, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_DOOR_CLOSED
        result = GLB_NLS_GUI_ALARMS.GetString(353, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_BLOCK_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(354)
      Case COMMON_OPERATION_CODE_UNBLOCK_KIOSK
        result = GLB_NLS_GUI_ALARMS.GetString(355)
      Case COMMON_OPERATION_CODE_JACKPOT_WON
        result = GLB_NLS_GUI_ALARMS.GetString(359, GUI_FormatCurrency(Data))
      Case COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE
        result = GLB_NLS_GUI_ALARMS.GetString(360, GUI_FormatCurrency(Data))
      Case COMMON_OPERATION_CODE_CALL_ATTENDANT
        result = GLB_NLS_GUI_ALARMS.GetString(361)
      Case COMMON_OPERATION_CODE_TEST_MODE_ENTER
        result = GLB_NLS_GUI_ALARMS.GetString(362)
      Case COMMON_OPERATION_CODE_TEST_MODE_LEAVE
        result = GLB_NLS_GUI_ALARMS.GetString(363)
      Case COMMON_OPERATION_CODE_LOGIN_ERROR
        result = GLB_NLS_GUI_ALARMS.GetString(364)
      Case COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER
        result = GLB_NLS_GUI_ALARMS.GetString(365)
      Case COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT
        result = GLB_NLS_GUI_ALARMS.GetString(366)
      Case COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER
        result = GLB_NLS_GUI_ALARMS.GetString(367)
      Case COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT
        result = GLB_NLS_GUI_ALARMS.GetString(368)
      Case COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED
        result = GLB_NLS_GUI_ALARMS.GetString(369)
      Case COMMON_OPERATION_CODE_HANDPAY_REQUESTED
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(376, aux)
      Case COMMON_OPERATION_CODE_HANDPAY_RESET
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(377, aux)
      Case COMMON_OPERATION_CODE_CARD_ABANDONED
        If TITO.Utils.IsTitoMode() Then
          result = GLB_NLS_GUI_ALARMS.GetString(390)
        Else
          aux = GLB_NLS_GUI_ALARMS.GetString(385, GUI_FormatNumber(Data, 0))
          result = GLB_NLS_GUI_ALARMS.GetString(383, aux)
        End If        
      Case COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(384, aux)
      Case COMMON_OPERATION_CODE_SECURITY_CASHOUT_UNITS
        aux = GLB_NLS_GUI_ALARMS.GetString(378, GUI_FormatCurrency(Data))
        result = GLB_NLS_GUI_ALARMS.GetString(384, aux)
      Case COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE
        aux = GetTerminalStatusString(((GUI_FormatNumber(Data, 0) / 100) - 1) Mod 3) + " -> " + GetTerminalStatusString((GUI_FormatNumber(Data, 0) Mod 100) Mod 3)
        result = GLB_NLS_GUI_ALARMS.GetString(386, aux)
      Case COMMON_OPERATION_CODE_EVENT
        'JCA 25-OCT-2011
        result = DecodeEvent(Data)
      Case COMMON_OPERATION_CODE_CHANGE_STACKER
        'SSC 19-04-2012
        result = GLB_NLS_GUI_ALARMS.GetString(417)
      Case COMMON_OPERATION_CODE_GAME_FATAL_ERROR
        result = GLB_NLS_GUI_ALARMS.GetString(418, GUI_FormatNumber(Data, 0))
      Case COMMON_OPERATION_CODE_PENDING_TRANSFER_IN
        aux = GLB_NLS_GUI_ALARMS.GetString(389, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(388, aux)
      Case COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT
        aux = GLB_NLS_GUI_ALARMS.GetString(389, GUI_FormatCurrency(Data / 100))
        result = GLB_NLS_GUI_ALARMS.GetString(388, aux)
      Case COMMON_OPERATION_CODE_TECH_OFFLINE_INSERTED
        If (Data > 0) Then
          _int_value = Data
          result = GLB_NLS_GUI_ALARMS.GetString(422, _int_value.ToString())
        Else
          result = GLB_NLS_GUI_ALARMS.GetString(419)
        End If
      Case COMMON_OPERATION_CODE_TECH_OFFLINE_REMOVED
        result = GLB_NLS_GUI_ALARMS.GetString(420)
    End Select

    Return result
  End Function ' DecodeOperationCode

  Private Function DecodeEvent(ByVal Data As Decimal) As String
    Dim _str_message As String = ""
    Dim _event As UShort

    If (CType(Data, UInteger) And &HFFFF0000UI) = &H10000UI Then
      _event = CType(Data, UInteger) And &HFFFFUI

      Select Case _event
        Case EVENT_SAS_SLOT_DOOR_OPENED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(147))

        Case EVENT_SAS_SLOT_DOOR_CLOSED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(148))

        Case EVENT_SAS_DROP_DOOR_OPENED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(149))

        Case EVENT_SAS_DROP_DOOR_CLOSED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(150))

        Case EVENT_SAS_CARD_CAGE_OPENED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(151))

        Case EVENT_SAS_CARD_CAGE_CLOSED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(152))

        Case EVENT_SAS_AC_POWER_APPLIED_GAMING_MACHINE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(153))

        Case EVENT_SAS_AC_POWER_LOST_GAMING_MACHINE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(154))

        Case EVENT_SAS_CASHBOX_DOOR_OPENED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(155))

        Case EVENT_SAS_CASHBOX_DOOR_CLOSED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(156))

        Case EVENT_SAS_CASHBOX_REMOVED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(157))

        Case EVENT_SAS_CASHBOX_INSTALLED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(158))

        Case EVENT_SAS_BELLY_DOOR_OPENED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(159))

        Case EVENT_SAS_BELLY_DOOR_CLOSED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(160))

        Case EVENT_SAS_GENERAL_LITL
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(161))

        Case EVENT_SAS_CMOS_RAM_ERROR_DATA_RECOVERED_FROM_EEPROM
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(162))

        Case EVENT_SAS_CMOS_RAM_ERROR_NO_DATA_RECOVERED_FROM_EEPROM
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(163))

        Case EVENT_SAS_CMOS_RAM_ERROR_BAD_DEVICE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(164))

        Case EVENT_SAS_EEPROM_ERROR_DATA_ERROR
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(165))

        Case EVENT_SAS_EEPROM_ERROR_BAD_DEVICE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(166))

        Case EVENT_SAS_EPROM_ERROR_DIFFERENT_CHECKSUM_VERSION_CHANGED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(167))

        Case EVENT_SAS_EPROM_ERROR_BAD_CHECKSUM_COMPARE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(168))

        Case EVENT_SAS_PARTITIONED_EPROM_ERROR_CHECKSUM_VERSION_CHANGED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(169))

        Case EVENT_SAS_PARTITIONED_EPROM_ERROR_AD_CHECKSUM_COMPARE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(170))

        Case EVENT_SAS_MEMORY_ERROR_RESET
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(171))

        Case EVENT_SAS_LOW_BACKUP_BATTERY_DETECTED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(172))

        Case EVENT_SAS_OPERATOR_CHANGED_OPTIONS
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(173))

        Case EVENT_SAS_HANDPAY_PENDING
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(174))

        Case EVENT_SAS_HANDPAY_RESET
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(175))

        Case EVENT_SAS_PROGRESSIVE_WIN
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(176))

        Case EVENT_SAS_PALYER_CANCELLED_HANDPAY_REQUEST
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(177))

        Case EVENT_SAS_PROGRESSIVE_LEVEL_HIT
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(178))

        Case EVENT_SAS_EXCEPTION_BUFFER_OVERFLOW
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(179))

        Case EVENT_SAS_GAME_CHANGE_LAMP_ON
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(120))

        Case EVENT_SAS_GAME_CHANGE_LAMP_OFF
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(122))

        Case EVENT_SAS_GAMING_MACHINE_SOFT_METERS_RESET_TO_ZERO
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(180))

        Case EVENT_SAS_DISPLAY_METERS_OR_ATTENDANT_MENU_ENTERED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(181))

        Case EVENT_SAS_DISPLAY_METERS_OR_ATTENDANT_MENU_EXITED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(182))

        Case EVENT_SAS_SELF_TEST_OR_OPERATOR_MENU_ENTERED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(183))

        Case EVENT_SAS_SELF_TEST_OR_OPERATOR_MENU_EXITED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(184))

        Case EVENT_SAS_GAMING_MACHINE_OUT_OF_SERVICE
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(185))

        Case EVENT_SAS_GAME_RECALL_ENTRY_DISPLAYED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(186))

        Case EVENT_SAS_POWER_OFF_CARD_CAGE_ACCESS
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(187))

        Case EVENT_SAS_POWER_OFF_SLOT_DOOR_ACCESS
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(188))

        Case EVENT_SAS_POWER_OFF_CASHBOX_DOOR_ACCESS
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(189))

        Case EVENT_SAS_POWER_OFF_DROP_DOOR_ACCESS
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(190))

        Case EVENT_SAS_DISCONNECTED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(191))

        Case EVENT_SAS_CONNECTED
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(192))

        Case EVENT_SAS_COULD_NOT_GET_ANY_COUNTER
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(112))

        Case EVENT_SAS_ERROR_GETTING_COUNTERS
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(113))

        Case EVENT_SAS_ERROR_GETTING_HAND_PAYS_COUNTER
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(114))

        Case EVENT_SAS_ERROR_GETING_COUNTERS_GAME
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(115))

        Case EVENT_SAS_ERROR_GETTING_COUNTERS_EXTENDED
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(116))

        Case EVENT_SAS_ERROR_GETTING_EXTENDED_COUNTER_OF_MANUAL_PAYMENTS
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(117))

        Case EVENT_SAS_ERROR_GETTING_COUNTERS_EXTENDED_PLAY
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(118))

        Case EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(119))

        Case EVENT_SAS_RECURRENT_ERROR_GETTING_HAND_PAYS_COUNTER
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(120))

        Case EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS_GAME
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(121))

        Case EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS_EXTENDED
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(122))

        Case EVENT_SAS_RECURRENT_ERROR_GETTING_EXTENDED_COUNTER_OF_MANUAL_PAYMENTS
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(123))

        Case EVENT_SAS_RECURRENT_ERROR_GETTING_COUNTERS_EXTENDED_PLAY
          _str_message = NLS_GetString(GLB_NLS_GUI_ALARMS.Id(124))

          ' ICS 13-MAR-2014: New SAS event codes
        Case EVENT_SAS_CASHBOX_FULL
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4028))

        Case EVENT_SAS_BILL_JAM
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4040))

        Case EVENT_SAS_BILL_VAL_HARDWARE_FAILURE
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4041))

        Case EVENT_SAS_REVERSE_BILL
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4042))

        Case EVENT_SAS_BILL_REJECTED
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4043))

        Case EVENT_SAS_BILL_COUNTERFEIT
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4044))

        Case EVENT_SAS_CASHBOX_NEAR_FULL
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4027))

        Case EVENT_SAS_PRINTER_COMM_ERROR
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4096))

        Case EVENT_SAS_PRINTER_PAPER_OUT
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4097))

        Case EVENT_SAS_PRINTER_PAPER_LOW
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4116))

        Case EVENT_SAS_PRINTER_POWER_OFF
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4117))

        Case EVENT_SAS_PRINTER_POWER_ON
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4118))

        Case EVENT_SAS_PRINTER_REPLACE_RIBBON
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4119))

        Case EVENT_SAS_PRINTER_CARRIAGE_JAMMED
          _str_message = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(4120))

        Case Else
          _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(85), _event.ToString("X"))

      End Select

    Else
      _str_message = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(87), GUI_FormatNumber(Data, 0))

    End If

    Return _str_message

  End Function ' DecodeEvent

  Private Function GetTerminalStatusString(ByVal Code As Integer) As String
    Select Case Code
      Case WSI.Common.TerminalStatus.ACTIVE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)

      Case WSI.Common.TerminalStatus.OUT_OF_SERVICE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)

      Case WSI.Common.TerminalStatus.RETIRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(399)

      Case Else
        Return "Unknown"

    End Select
  End Function

#Region " Grid Event "

  ' PURPOSE: Define all Grid Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetDeviceOperation()
    With Me.dg_devices
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 3900
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetType

  ' PURPOSE: Get envents
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Events TYPE_EVENT()
  Public Function GetEventsFilters() As TYPE_EVENT()
    Dim events(36) As TYPE_EVENT
    Dim ev As TYPE_EVENT

    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_START
    ev.event_nls_id = 331
    events(0) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_SHUTDOWN
    ev.event_nls_id = 332
    events(1) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_RESTART
    ev.event_nls_id = 336
    events(2) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_LAUNCH_EXPLORER
    ev.event_nls_id = 337
    events(3) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_TUNE_SCREEN
    ev.event_nls_id = 338
    events(4) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_ASSIGN_KIOSK
    ev.event_nls_id = 339
    events(5) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_DEASSIGN_KIOSK
    ev.event_nls_id = 340
    events(6) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_UNLOCK_DOOR
    ev.event_nls_id = 335
    events(7) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_LOGIN
    ev.event_nls_id = 346
    events(8) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_LOGOUT
    ev.event_nls_id = 347
    events(9) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_DISPLAY_SETTINGS
    ev.event_nls_id = 349
    events(10) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_DOOR_OPENED
    ev.event_nls_id = 352
    events(11) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_DOOR_CLOSED
    ev.event_nls_id = 353
    events(12) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_BLOCK_KIOSK
    ev.event_nls_id = 354
    events(13) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_UNBLOCK_KIOSK
    ev.event_nls_id = 355
    events(14) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_JACKPOT_WON
    ev.event_nls_id = 359
    events(15) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE
    ev.event_nls_id = 360
    events(16) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_CALL_ATTENDANT
    ev.event_nls_id = 361
    events(17) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_TEST_MODE_ENTER
    ev.event_nls_id = 362
    events(18) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_TEST_MODE_LEAVE
    ev.event_nls_id = 363
    events(19) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_LOGIN_ERROR
    ev.event_nls_id = 364
    events(20) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_ATTENDANT_MENU_ENTER
    ev.event_nls_id = 365
    events(21) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_ATTENDANT_MENU_EXIT
    ev.event_nls_id = 366
    events(22) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_OPERATOR_MENU_ENTER
    ev.event_nls_id = 367
    events(23) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_OPERATOR_MENU_EXIT
    ev.event_nls_id = 368
    events(24) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED
    ev.event_nls_id = 369
    events(25) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_HANDPAY_REQUESTED
    ev.event_nls_id = 376
    events(26) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_HANDPAY_RESET
    ev.event_nls_id = 377
    events(27) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_CARD_ABANDONED
    ev.event_nls_id = 383
    events(28) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS
    ev.event_nls_id = 384
    events(29) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_TERMINAL_STATUS_CHANGE
    ev.event_nls_id = 386
    events(30) = ev
    ' JCA 25-OCT-2011
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_EVENT
    ev.event_nls_id = 440
    events(31) = ev
    ' SSC 19-ABR-2012
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_CHANGE_STACKER
    ev.event_nls_id = 417
    events(32) = ev
    ' ACC 08-AUG-2013
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_GAME_FATAL_ERROR
    ev.event_nls_id = 418
    events(33) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_PENDING_TRANSFER_IN 'It's valid for COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT
    ev.event_nls_id = 388
    events(34) = ev
    ' JMM 02-JUL-2014
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_TECH_OFFLINE_INSERTED
    ev.event_nls_id = 423
    events(35) = ev
    ev.event_type = WCP_EVENT_TYPE_OPERATION
    ev.event_code = COMMON_OPERATION_CODE_TECH_OFFLINE_REMOVED
    ev.event_nls_id = 424
    events(36) = ev

    Return events

  End Function


  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillFilterGrid()
    Dim events() As TYPE_EVENT
    Dim idx_event As Integer

    events = GetEventsFilters()

    Me.dg_devices.Clear()
    Me.dg_devices.Redraw = False

    idx_event = 0
    For Each ev As TYPE_EVENT In events
      Call AddOneFilterRowDevice(ev.event_code, ev.event_type, GLB_NLS_GUI_ALARMS.GetString(ev.event_nls_id))
      idx_event += 1
    Next

    If Me.dg_devices.NumRows > 0 Then
      Me.dg_devices.CurrentRow = 0
    End If

    Me.dg_devices.IsSortable = True
    Me.dg_devices.SortGrid(GRID_2_COLUMN_DESC)

    Me.dg_devices.Redraw = True

  End Sub

  Private Function GetTypeName(ByVal EventType As Integer) As String
    Dim type_name As String
    type_name = ""
    Select Case EventType
      Case WCP_EVENT_TYPE_DEVICE_STATUS
        type_name = GLB_NLS_GUI_ALARMS.GetString(416)
      Case WCP_EVENT_TYPE_OPERATION
        type_name = GLB_NLS_GUI_ALARMS.GetString(445)
    End Select
    Return type_name
  End Function

  ' PURPOSE: Add a Row of a event to the Event grid according to 
  '          the given code
  '
  '  PARAMS:
  '     - INPUT:
  '           - Code 
  '   
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneFilterRowDevice(ByVal EventCode As Integer, ByVal EventType As Integer, ByVal EventName As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_devices.Redraw
    Me.dg_devices.Redraw = False

    dg_devices.AddRow()

    Me.dg_devices.Redraw = False

    'XVV 16/04/2007
    'Change Me.XXX for frm_events_history, compiler generate a warning
    '-----------------------------------------------------------------------------------------------------
    dg_devices.Cell(dg_devices.NumRows - 1, GRID_2_COLUMN_TYPE).Value = EventType
    dg_devices.Cell(dg_devices.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_devices.Cell(dg_devices.NumRows - 1, GRID_2_COLUMN_CODE).Value = EventCode
    dg_devices.Cell(dg_devices.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & EventName
    '-----------------------------------------------------------------------------------------------------

    Me.dg_devices.Redraw = prev_redraw
  End Sub


  ' PURPOSE: Change checked value to all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer
    Dim type_id_str As String

    For idx = FirstRow + 1 To dg_devices.NumRows - 1
      type_id_str = Me.dg_devices.Cell(idx, GRID_2_COLUMN_CODE).Value
      If type_id_str.Length = 0 Then

        Exit Sub
      End If

      Me.dg_devices.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub

  ' PURPOSE: Uncheck all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim idx As Integer

    For idx = FirstRow - 1 To 0 Step -1

      If Me.dg_devices.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then

        Exit Sub
      End If
    Next
  End Sub


  ' PURPOSE: Get Event Id list for selected operation
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetEventIdListSelected() As String
    Dim idx_row As Integer
    Dim list_type As String
    Dim no_one As Boolean
    no_one = True

    list_type = ""


    For idx_row = 0 To Me.dg_devices.NumRows - 1

      'XVV 16/04/2007
      'Change Me.XXX for frm_events_history, compiler generate a warning
      '------------------------------------------------------------------------------------------------
      If dg_devices.Cell(idx_row, GRID_2_COLUMN_TYPE).Value = WCP_EVENT_TYPE_OPERATION Then
        If dg_devices.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _
           dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value.Length > 0 Then
          If list_type.Length = 0 Then
            list_type = dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value
            no_one = False
          Else
            list_type = list_type & ", " & dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value
            no_one = False
          End If

          ' Same NLS 2 codes
          If dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value = COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS Then
            list_type = list_type & ", " & COMMON_OPERATION_CODE_SECURITY_CASHOUT_UNITS
          End If

          ' Same NLS 2 codes
          If dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value = COMMON_OPERATION_CODE_PENDING_TRANSFER_IN Then
            list_type = list_type & ", " & COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT
          End If

        End If
      End If
      '------------------------------------------------------------------------------------------------
    Next

    If no_one Then
      For idx_row = 0 To Me.dg_devices.NumRows - 1
        If list_type.Length = 0 Then
          list_type = dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value
        Else
          list_type = list_type & ", " & dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value
        End If

        ' Same NLS 2 codes
        If dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value = COMMON_OPERATION_CODE_SECURITY_CASHOUT_CENTS Then
          list_type = list_type & ", " & COMMON_OPERATION_CODE_SECURITY_CASHOUT_UNITS
        End If

        ' Same NLS 2 codes
        If dg_devices.Cell(idx_row, GRID_2_COLUMN_CODE).Value = COMMON_OPERATION_CODE_PENDING_TRANSFER_IN Then
          list_type = list_type & ", " & COMMON_OPERATION_CODE_PENDING_TRANSFER_OUT
        End If

      Next
    End If

    Return list_type
  End Function 'GetEventIdListSelected



  ' PURPOSE: Get Event Id list for selected severity
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSeverityIdListSelected() As String
    Dim list_type As String
    Dim no_one As Boolean
    no_one = True

    list_type = ""

    If cb_ok.Checked Then
      If list_type.Length = 0 Then
        list_type = COMMON_PRIORITY_OK
        no_one = False
      Else
        list_type = list_type & ", " & COMMON_PRIORITY_OK
        no_one = False
      End If
    End If

    If cb_warning.Checked Then
      If list_type.Length = 0 Then
        list_type = COMMON_PRIORITY_WARNING
        no_one = False
      Else
        list_type = list_type & ", " & COMMON_PRIORITY_WARNING
        no_one = False
      End If
    End If

    If cb_error.Checked Then
      If list_type.Length = 0 Then
        list_type = COMMON_PRIORITY_ERROR
        no_one = False
      Else
        list_type = list_type & ", " & COMMON_PRIORITY_ERROR
        no_one = False
      End If
    End If

    If no_one Then
      list_type = COMMON_PRIORITY_OK
      list_type = list_type & ", " & COMMON_PRIORITY_WARNING
      list_type = list_type & ", " & COMMON_PRIORITY_ERROR
    End If

    Return list_type
  End Function 'GetEventIdListSelected


#End Region ' Grid Event

#End Region  ' Private Functions

#Region " Events "

  Private Sub dg_devices_DataSelectedEvent() Handles dg_devices.DataSelectedEvent

    Dim idx As Integer
    Dim last_row As Integer

    'XVV 16/04/2007
    'Unnused variable, disabled declaration because compiler generate a warning
    'Dim row_is_header As Boolean

    With Me.dg_devices

      'row_is_header = RowIsHeader(.CurrentRow)

      'If row_is_header And .CurrentCol <> GRID_2_COLUMN_CHECKED Then
      If .CurrentCol <> GRID_2_COLUMN_CHECKED Then
        last_row = .CurrentRow
        For idx = .CurrentRow + 1 To .NumRows - 1
          'If RowIsHeader(idx) Then

          '  Exit For
          'End If
          last_row = idx
        Next

        'Me.dg_devices.CollapseExpand(.CurrentRow, last_row, GRID_2_COLUMN_DESC)

      End If
    End With
  End Sub ' dg_devices_DataSelectedEvent

  Private Sub dg_devices_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_devices.CellDataChangedEvent
    Dim current_row As Integer

    If m_refreshing_grid = True Then

      Exit Sub
    End If

    With Me.dg_devices

      current_row = .CurrentRow
      m_refreshing_grid = True


      'If RowIsHeader(.CurrentRow) Then
      '  Call ChangeCheckofRows(.CurrentRow, _
      '                           .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
      'Else
      If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        Call TryToUncheckHeader(.CurrentRow)
      End If
      'End If

      m_refreshing_grid = False
      .CurrentRow = current_row

    End With
  End Sub ' dg_devices_CellDataChangedEvent

  Private Sub btn_check_all_Click() Handles btn_check_all.ClickEvent
    Dim idx_rows As Integer
    For idx_rows = 0 To dg_devices.NumRows - 1
      dg_devices.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next
  End Sub

  Private Sub btn_uncheck_all_Click() Handles btn_uncheck_all.ClickEvent
    Dim idx_rows As Integer
    For idx_rows = 0 To dg_devices.NumRows - 1
      dg_devices.Cell(idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub

#End Region ' Events

  Private Sub rb_operations_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_operations.CheckedChanged
    gb_operations.Enabled = rb_operations.Checked
  End Sub

  Private Sub rb_devices_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_devices.CheckedChanged
    gb_severity.Enabled = rb_devices.Checked
  End Sub

  Private Sub rb_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_all.CheckedChanged
    gb_severity.Enabled = rb_all.Checked
    gb_operations.Enabled = rb_all.Checked
  End Sub
End Class
