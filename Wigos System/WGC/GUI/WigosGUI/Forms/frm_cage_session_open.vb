'-------------------------------------------------------------------
' Copyright � 2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cage_session_open
' DESCRIPTION:   New cage session
' AUTHOR:        Alberto Marcos
' CREATION DATE: 17-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-NOV-2014  AMF    Initial version.
' 07-SEP-2015  ETP    BUG 3824 Fixed Avoid double sessions if the dialog is closed.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_cage_session_open
  Inherits frm_base_edit

#Region " Properties "

  Private m_session_name As String
  Private m_working_day As String
  Private m_canceled As Boolean
  Private m_trx As DB_TRX
  Private m_name_init_value As String = String.Empty

  Public Property SessionName() As String
    Get
      SessionName = m_session_name
    End Get

    Set(ByVal value As String)
      m_session_name = value
    End Set
  End Property

  Public Property WorkingDay() As String
    Get
      WorkingDay = m_working_day
    End Get

    Set(ByVal value As String)
      m_working_day = value
    End Set
  End Property

  Public Property Canceled() As Boolean
    Get
      Canceled = m_canceled
    End Get

    Set(ByVal value As Boolean)
      m_session_name = value
    End Set
  End Property

#End Region ' Constants

#Region " Constants "

  Private Const LEN_SESSION_NAME As Integer = 50

#End Region ' Constants

#Region " Members "

  Private m_input_session_name As String

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SESSIONS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()
    Dim _now As Date

    _now = WSI.Common.Misc.TodayOpening()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5726)

    ' Delete button is only available whe editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ef_session_name.Text = GLB_NLS_GUI_CONTROLS.GetString(323) ' Name
    ef_session_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_SESSION_NAME)
    ef_session_name.Value = GLB_NLS_GUI_INVOICING.GetString(401) & " " & Trim(GUI_FormatDate(Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))
    m_name_init_value = ef_session_name.Value.ToUpper()

    dtp_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
    dtp_working_day.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_working_day.Value = New DateTime(_now.Year, _now.Month, _now.Day)

    m_canceled = True

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _gaming_day As DateTime
    Dim _opening As DateTime = WSI.Common.Misc.TodayOpening()

    If ef_session_name.Value.Length = 0 Then
      ' 123 "Debe introducir un valor."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_session_name.Text)
      Call ef_session_name.Focus()
      m_canceled = True
      Return False
    End If

    _gaming_day = New DateTime(dtp_working_day.Value.Year, dtp_working_day.Value.Month, dtp_working_day.Value.Day, _opening.Hour, _opening.Minute, _opening.Second)

    If CheckIfAlreadyOpenedCageSession(_gaming_day) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6345), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , CurrentUser.Name, GUI_FormatDate(_gaming_day, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
      Call dtp_working_day.Focus()
      m_canceled = True
      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    m_session_name = ef_session_name.Value
    m_working_day = dtp_working_day.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        m_canceled = True
        Me.Close()
      Case ENUM_BUTTON.BUTTON_OK
        m_canceled = False
        MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        '
    End Select

  End Sub ' GUI_ButtonClick
  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    DbStatus = ENUM_STATUS.STATUS_OK

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_session_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: checks if there is already an opened cage session for this user
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Function CheckIfAlreadyOpenedCageSession(ByVal GamingDay As DateTime) As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder
    _sb.AppendLine(" SELECT   TOP 1 *                                ")
    _sb.AppendLine("   FROM   CAGE_SESSIONS                          ")
    _sb.AppendLine("  WHERE   CGS_OPEN_USER_ID  = @pUserID           ")
    _sb.AppendLine("    AND   CGS_CLOSE_DATETIME IS NULL             ")
    _sb.AppendLine("    AND   CGS_WORKING_DAY = @pGamingDay        ")

    Using _cmd As New SqlCommand(_sb.ToString())
      _cmd.Parameters.Add("@pUserID", SqlDbType.Int).Value = CurrentUser.Id
      _cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = GamingDay

      Using _reader As SqlDataReader = m_trx.ExecuteReader(_cmd)
        If _reader.Read() Then
          Return True
        End If
      End Using
    End Using

    Return False

  End Function


  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal AreaId As Integer, ByVal Trx As DB_TRX)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    m_trx = Trx

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

#End Region ' Overrides

  Private Sub dtp_working_day_DatePickerValueChanged() Handles dtp_working_day.DatePickerValueChanged
    If m_name_init_value.Equals(Me.ef_session_name.Value.ToUpper) Then
      Me.ef_session_name.Value = GLB_NLS_GUI_INVOICING.GetString(401) & " " & Trim(GUI_FormatDate(dtp_working_day.Value.Date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))
      m_name_init_value = ef_session_name.Value.ToUpper()
    End If
  End Sub
End Class
