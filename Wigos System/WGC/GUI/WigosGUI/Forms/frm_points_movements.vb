'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_points_movements
'
' DESCRIPTION:   This screen allows to view account movements:
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
'                           - for one or all cashier sessions
' AUTHOR:        Santi Lopez
' CREATION DATE: 22-DIC-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-DIC-2009  SLE    Initial version
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 17-APR-2013  DMR    Changing cashier name for its alias
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_points_movements
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_terminal As GUI_Controls.uc_combo
  Friend WithEvents opt_one_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_terminals As System.Windows.Forms.RadioButton
  Friend WithEvents fra_account As System.Windows.Forms.GroupBox
  Friend WithEvents ef_account_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_track_data As GUI_Controls.uc_entry_field
  Friend WithEvents ef_target_account As GUI_Controls.uc_entry_field
  Friend WithEvents fra_account_2 As System.Windows.Forms.GroupBox
  Friend WithEvents ef_track_data_2 As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_points_movements))
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.gb_terminal = New System.Windows.Forms.GroupBox
    Me.opt_one_terminal = New System.Windows.Forms.RadioButton
    Me.cmb_terminal = New GUI_Controls.uc_combo
    Me.opt_one_cashier = New System.Windows.Forms.RadioButton
    Me.opt_all_terminals = New System.Windows.Forms.RadioButton
    Me.cmb_cashier = New GUI_Controls.uc_combo
    Me.fra_account = New System.Windows.Forms.GroupBox
    Me.ef_target_account = New GUI_Controls.uc_entry_field
    Me.ef_account_number = New GUI_Controls.uc_entry_field
    Me.ef_track_data = New GUI_Controls.uc_entry_field
    Me.fra_account_2 = New System.Windows.Forms.GroupBox
    Me.ef_track_data_2 = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_terminal.SuspendLayout()
    Me.fra_account.SuspendLayout()
    Me.fra_account_2.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.fra_account_2)
    Me.panel_filter.Controls.Add(Me.fra_account)
    Me.panel_filter.Controls.Add(Me.gb_terminal)
    Me.panel_filter.Controls.Add(Me.gb_date)
    resources.ApplyResources(Me.panel_filter, "panel_filter")
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_account, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_account_2, 0)
    '
    'panel_data
    '
    resources.ApplyResources(Me.panel_data, "panel_data")
    '
    'pn_separator_line
    '
    resources.ApplyResources(Me.pn_separator_line, "pn_separator_line")
    '
    'pn_line
    '
    resources.ApplyResources(Me.pn_line, "pn_line")
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    resources.ApplyResources(Me.gb_date, "gb_date")
    Me.gb_date.Name = "gb_date"
    Me.gb_date.TabStop = False
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    resources.ApplyResources(Me.dtp_to, "dtp_to")
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    resources.ApplyResources(Me.dtp_from, "dtp_from")
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.opt_one_terminal)
    Me.gb_terminal.Controls.Add(Me.cmb_terminal)
    Me.gb_terminal.Controls.Add(Me.opt_one_cashier)
    Me.gb_terminal.Controls.Add(Me.opt_all_terminals)
    Me.gb_terminal.Controls.Add(Me.cmb_cashier)
    resources.ApplyResources(Me.gb_terminal, "gb_terminal")
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.TabStop = False
    '
    'opt_one_terminal
    '
    resources.ApplyResources(Me.opt_one_terminal, "opt_one_terminal")
    Me.opt_one_terminal.Name = "opt_one_terminal"
    '
    'cmb_terminal
    '
    Me.cmb_terminal.IsReadOnly = False
    resources.ApplyResources(Me.cmb_terminal, "cmb_terminal")
    Me.cmb_terminal.Name = "cmb_terminal"
    Me.cmb_terminal.SelectedIndex = -1
    Me.cmb_terminal.SufixText = "Sufix Text"
    Me.cmb_terminal.SufixTextVisible = True
    Me.cmb_terminal.TextVisible = False
    Me.cmb_terminal.TextWidth = 0
    '
    'opt_one_cashier
    '
    resources.ApplyResources(Me.opt_one_cashier, "opt_one_cashier")
    Me.opt_one_cashier.Name = "opt_one_cashier"
    '
    'opt_all_terminals
    '
    resources.ApplyResources(Me.opt_all_terminals, "opt_all_terminals")
    Me.opt_all_terminals.Name = "opt_all_terminals"
    '
    'cmb_cashier
    '
    Me.cmb_cashier.IsReadOnly = False
    resources.ApplyResources(Me.cmb_cashier, "cmb_cashier")
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'fra_account
    '
    Me.fra_account.Controls.Add(Me.ef_account_number)
    Me.fra_account.Controls.Add(Me.ef_track_data)
    resources.ApplyResources(Me.fra_account, "fra_account")
    Me.fra_account.Name = "fra_account"
    Me.fra_account.TabStop = False
    '
    'ef_target_account
    '
    Me.ef_target_account.DoubleValue = 0
    Me.ef_target_account.IntegerValue = 0
    Me.ef_target_account.IsReadOnly = False
    resources.ApplyResources(Me.ef_target_account, "ef_target_account")
    Me.ef_target_account.Name = "ef_target_account"
    Me.ef_target_account.SufixText = "Sufix Text"
    Me.ef_target_account.SufixTextVisible = True
    Me.ef_target_account.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_target_account.TextValue = ""
    Me.ef_target_account.TextWidth = 58
    Me.ef_target_account.Value = ""
    '
    'ef_account_number
    '
    Me.ef_account_number.DoubleValue = 0
    Me.ef_account_number.IntegerValue = 0
    Me.ef_account_number.IsReadOnly = False
    resources.ApplyResources(Me.ef_account_number, "ef_account_number")
    Me.ef_account_number.Name = "ef_account_number"
    Me.ef_account_number.SufixText = "Sufix Text"
    Me.ef_account_number.SufixTextVisible = True
    Me.ef_account_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_number.TextValue = ""
    Me.ef_account_number.TextWidth = 58
    Me.ef_account_number.Value = ""
    '
    'ef_track_data
    '
    Me.ef_track_data.DoubleValue = 0
    Me.ef_track_data.IntegerValue = 0
    Me.ef_track_data.IsReadOnly = False
    resources.ApplyResources(Me.ef_track_data, "ef_track_data")
    Me.ef_track_data.Name = "ef_track_data"
    Me.ef_track_data.SufixText = "Sufix Text"
    Me.ef_track_data.SufixTextVisible = True
    Me.ef_track_data.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_track_data.TextValue = ""
    Me.ef_track_data.TextWidth = 58
    Me.ef_track_data.Value = ""
    '
    'fra_account_2
    '
    Me.fra_account_2.Controls.Add(Me.ef_target_account)
    Me.fra_account_2.Controls.Add(Me.ef_track_data_2)
    resources.ApplyResources(Me.fra_account_2, "fra_account_2")
    Me.fra_account_2.Name = "fra_account_2"
    Me.fra_account_2.TabStop = False
    '
    'ef_track_data_2
    '
    Me.ef_track_data_2.DoubleValue = 0
    Me.ef_track_data_2.IntegerValue = 0
    Me.ef_track_data_2.IsReadOnly = False
    resources.ApplyResources(Me.ef_track_data_2, "ef_track_data_2")
    Me.ef_track_data_2.Name = "ef_track_data_2"
    Me.ef_track_data_2.SufixText = "Sufix Text"
    Me.ef_track_data_2.SufixTextVisible = True
    Me.ef_track_data_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_track_data_2.TextValue = ""
    Me.ef_track_data_2.TextWidth = 58
    Me.ef_track_data_2.Value = ""
    '
    'frm_points_movements
    '
    resources.ApplyResources(Me, "$this")
    Me.Name = "frm_points_movements"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_terminal.ResumeLayout(False)
    Me.fra_account.ResumeLayout(False)
    Me.fra_account_2.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_ACCT_NUMBER As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 1
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 2
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 3
  Private Const SQL_COLUMN_DATE As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 5
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 6
  Private Const SQL_COLUMN_TYPE_ID As Integer = 7
  Private Const SQL_COLUMN_INIT_BALANCE As Integer = 8
  Private Const SQL_COLUMN_SUB_POINTS As Integer = 9
  Private Const SQL_COLUMN_ADD_POINTS As Integer = 10
  Private Const SQL_COLUMN_FINAL_BALANCE As Integer = 11
  Private Const SQL_COLUMN_SERVICE_ACC_ID As Integer = 12

  ' Grid Columns
  Private Const GRID_COLUMN_ACCT_NUMBER As Integer = 0
  Private Const GRID_COLUMN_ACCOUNT_TYPE As Integer = 1
  Private Const GRID_COLUMN_CARD_TRACK As Integer = 2
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 3
  Private Const GRID_COLUMN_DATE As Integer = 4
  Private Const GRID_COLUMN_TYPE_ID As Integer = 5
  Private Const GRID_COLUMN_TYPE_NAME As Integer = 6
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 7
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 8
  Private Const GRID_COLUMN_SERVICE_ACC_ID As Integer = 9
  Private Const GRID_COLUMN_INIT_BALANCE As Integer = 10
  Private Const GRID_COLUMN_SUB_POINTS As Integer = 11
  Private Const GRID_COLUMN_ADD_POINTS As Integer = 12
  Private Const GRID_COLUMN_FINAL_BALANCE As Integer = 13
  'Private Const GRID_COLUMN_INIT_BALANCE As Integer = 9
  'Private Const GRID_COLUMN_SUB_POINTS As Integer = 10
  'Private Const GRID_COLUMN_ADD_POINTS As Integer = 11
  'Private Const GRID_COLUMN_FINAL_BALANCE As Integer = 12
  'Private Const GRID_COLUMN_SERVICE_ACC_ID As Integer = 13

  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TIME As Integer = 1300
  Private Const GRID_WIDTH_TERMINAL As Integer = 1200
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1400
  Private Const GRID_WIDTH_MOVEMENT_TYPE_NAME As Integer = 2100
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 1450
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300

  ' Database codes
  Private Const MOV_TYPE_DB_PLAY As Integer = 0
  'Private Const MOV_TYPE_DB_CASH_IN As Integer = 1
  'Private Const MOV_TYPE_DB_CASH_OUT As Integer = 2
  'Private Const MOV_TYPE_DB_DEVOLUTION As Integer = 3
  'Private Const MOV_TYPE_DB_FEDERAL_TAXES As Integer = 4
  'Private Const MOV_TYPE_DB_START_SESSION As Integer = 5
  'Private Const MOV_TYPE_DB_END_SESSION As Integer = 6
  'Private Const MOV_TYPE_DB_ACTIVATE As Integer = 7
  'Private Const MOV_TYPE_DB_DEACTIVATE As Integer = 8
  'Private Const MOV_TYPE_DB_NO_REDEEMABLE As Integer = 9
  'Private Const MOV_TYPE_DB_CARD_DEPOSIT_IN As Integer = 10
  'Private Const MOV_TYPE_DB_CARD_DEPOSIT_OUT As Integer = 11
  'Private Const MOV_TYPE_DB_CARD_NO_REDEEMABLE_CANCEL As Integer = 12
  'Private Const MOV_TYPE_DB_STATE_TAXES As Integer = 13
  Private Const MOV_TYPE_DB_PROMO_PTS_REDEEM_NO_REDEEMABLE As Integer = 14
  Private Const MOV_TYPE_DB_PROMO_PTS_REDEEM_COUPON As Integer = 15
  Private Const MOV_TYPE_DB_PROMO_PTS_TRANSFER_RESTAURANT As Integer = 16
  Private Const MOV_TYPE_DB_PROMO_PTS_TRANSFER_PLAYABLE As Integer = 17
  Private Const MOV_TYPE_DB_PROMO_EXPIRED_CREDITS As Integer = 18
  Private Const MOV_TYPE_DB_EXPIRED_POINTS As Integer = 19

#End Region ' Constants

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_account As String
  Private m_target_account As String
  Private m_track_data As String
  Private m_track_data_2 As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String

  ' For calls from other screens
  Private m_initial_account As Int64
#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_POINTS_MOVEMENTS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(306)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Account
    Me.fra_account.Text = GLB_NLS_GUI_INVOICING.GetString(230)
    Me.ef_account_number.Text = GLB_NLS_GUI_INVOICING.GetString(231)
    Call Me.ef_account_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6)
    Me.ef_track_data.Text = GLB_NLS_GUI_INVOICING.GetString(212)
    Call Me.ef_track_data.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)
    'ef_target_account
    Me.fra_account_2.Text = GLB_NLS_GUI_INVOICING.GetString(307)
    Me.ef_track_data_2.Text = GLB_NLS_GUI_INVOICING.GetString(212)
    Call Me.ef_track_data_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)
    Me.ef_target_account.Text = GLB_NLS_GUI_INVOICING.GetString(231) '308
    Call Me.ef_target_account.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6)

    ' Terminals
    Me.gb_terminal.Text = GLB_NLS_GUI_INVOICING.GetString(232)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(234)
    Me.opt_one_terminal.Text = GLB_NLS_GUI_INVOICING.GetString(233)
    Me.opt_all_terminals.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Cashier Terminals
    Call SetComboCashierAlias(Me.cmb_cashier)
    Me.cmb_cashier.Enabled = False

    ' Set combo with Gaming Terminals
    Call SetCombo(Me.cmb_terminal, m_select_terminals)
    Me.cmb_terminal.Enabled = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String = ""

    ' Get Select and from
    str_sql = "SELECT " & _
                  " AM_ACCOUNT_ID, " & _
                  " AC_TYPE, " & _
                  " AC_TRACK_DATA, " & _
                  " AC_HOLDER_NAME, " & _
                  " AM_DATETIME, " & _
                  " CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_ID " & _
                       " WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_ID " & _
                       " ELSE NULL " & _
                  " END, " & _
                  " CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_NAME " & _
                       "WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_NAME " & _
                       "ELSE NULL " & _
                  " END, " & _
                  " AM_TYPE, " & _
                  " AM_INITIAL_POINTS_BALANCE, " & _
                  " AM_SUB_POINTS, " & _
                  " AM_ADD_POINTS, " & _
                  " AM_FINAL_POINTS_BALANCE, " & _
                  " AM_SECOND_ACCOUNT_ID " & _
             " FROM ACCOUNTS, " & _
                  " ACCOUNT_MOVEMENTS " & _
            " WHERE (AC_ACCOUNT_ID = AM_ACCOUNT_ID) " & _
                  " AND AM_TYPE IN (0,14,15,16,17,19)"
    'MOV_TYPE_DB_CASH_OUT ???

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _mov_type As Integer
    Dim _str_type As String = ""
    Dim _track_data As String
    Dim _account_type As AccountType

    ' Account Type
    _account_type = CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = _account_type

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _account_type)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = _track_data
    End If

    ' Card Account Number
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCT_NUMBER).Value = DbRow.Value(SQL_COLUMN_ACCT_NUMBER)

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = ""
    End If

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), _
                                                                                ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Movement type
    _mov_type = DbRow.Value(SQL_COLUMN_TYPE_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = DbRow.Value(SQL_COLUMN_TYPE_ID)

    ' Movement type names
    Select Case _mov_type
      Case MOV_TYPE_DB_PLAY
        _str_type = GLB_NLS_GUI_INVOICING.GetString(41)

        'Case MOV_TYPE_DB_CASH_IN
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(42)

        'Case MOV_TYPE_DB_CASH_OUT
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(43)

        'Case MOV_TYPE_DB_DEVOLUTION
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(44)

        'Case MOV_TYPE_DB_FEDERAL_TAXES
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(56)

        'Case MOV_TYPE_DB_STATE_TAXES
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(57)

        'Case MOV_TYPE_DB_START_SESSION
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(46)

        'Case MOV_TYPE_DB_END_SESSION
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(47)

        'Case MOV_TYPE_DB_ACTIVATE
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(48)

        'Case MOV_TYPE_DB_DEACTIVATE
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(49)

        'Case MOV_TYPE_DB_NO_REDEEMABLE
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(50)

        'Case MOV_TYPE_DB_CARD_NO_REDEEMABLE_CANCEL
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(53)

        'Case MOV_TYPE_DB_CARD_DEPOSIT_IN
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(51)

        'Case MOV_TYPE_DB_CARD_DEPOSIT_OUT
        '  str_type = GLB_NLS_GUI_INVOICING.GetString(52)

      Case MOV_TYPE_DB_PROMO_PTS_REDEEM_NO_REDEEMABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(58)

      Case MOV_TYPE_DB_PROMO_PTS_REDEEM_COUPON
        _str_type = GLB_NLS_GUI_INVOICING.GetString(59)

      Case MOV_TYPE_DB_PROMO_PTS_TRANSFER_RESTAURANT
        _str_type = GLB_NLS_GUI_INVOICING.GetString(60)

      Case MOV_TYPE_DB_PROMO_PTS_TRANSFER_PLAYABLE
        _str_type = GLB_NLS_GUI_INVOICING.GetString(61)

      Case MOV_TYPE_DB_PROMO_EXPIRED_CREDITS
        _str_type = GLB_NLS_GUI_INVOICING.GetString(62)

      Case MOV_TYPE_DB_EXPIRED_POINTS
        _str_type = GLB_NLS_GUI_INVOICING.GetString(63)

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = _str_type

    ' Terminal Id
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = ""
    End If

    ' Terminal Name
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) And DbRow.Value(0) = ENUM_GUI.CASHIER Then
      Dim _term_name() As String = "sdfhlkfdh".ToString().Split("@")
      Dim _terminal_name() As String = DbRow.Value(SQL_COLUMN_TERMINAL_NAME).ToString().Split("@")
      If _terminal_name.Length > 1 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = _terminal_name(0) & "@" & Computer.Alias(_terminal_name(1))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = _terminal_name(0)
      End If

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = ""
    End If

    ' Initial balance
    If Not DbRow.IsNull(SQL_COLUMN_INIT_BALANCE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_BALANCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_BALANCE).Value = "0"
    End If

    ' Subtract amount
    If Not DbRow.IsNull(SQL_COLUMN_SUB_POINTS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_POINTS), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_POINTS).Value = "0"
    End If

    ' Add amount
    If Not DbRow.IsNull(SQL_COLUMN_ADD_POINTS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_POINTS), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_POINTS).Value = "0"
    End If

    ' Final balance
    If Not DbRow.IsNull(SQL_COLUMN_FINAL_BALANCE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_FINAL_BALANCE), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = "0"
    End If

    ' Target Account 
    ' (if mov_type <> MOV_TYPE_DB_PROMO_PTS_ADDED_PLAYABLE or MOV_TYPE_DB_PROMO_EXPIRED_CREDITS: SQL_COLUMN_SERVICE_ACC_ID should be null)
    If _mov_type <> MOV_TYPE_DB_PROMO_PTS_TRANSFER_PLAYABLE And _mov_type <> MOV_TYPE_DB_PROMO_PTS_TRANSFER_RESTAURANT Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_ACC_ID).Value = ""
    Else
      If Not DbRow.IsNull(SQL_COLUMN_SERVICE_ACC_ID) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_ACC_ID).Value = DbRow.Value(SQL_COLUMN_SERVICE_ACC_ID)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVICE_ACC_ID).Value = ""
      End If
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_account_number
  End Sub ' GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(231), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(231), m_target_account) '308
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data_2)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    If Me.opt_one_terminal.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(233), m_terminal)
    End If
    If Me.opt_one_cashier.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(234), m_terminal)
    End If
    If Me.opt_all_terminals.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(232), m_terminal)
    End If

    ' Allow enough space for terminal type labels in the report
    PrintData.FilterHeaderWidth(3) = 2000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line
    PrintData.Settings.Columns(GRID_COLUMN_DATE).IsWidthFixed = True
    PrintData.Settings.Columns(GRID_COLUMN_CARD_TRACK).IsWidthFixed = True

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(306)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_account = ""
    m_target_account = ""
    m_track_data = ""
    m_track_data_2 = ""
    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    ' Card Account number
    If Me.ef_account_number.TextValue <> "" Then
      m_account = Me.ef_account_number.Value
    End If

    'ef_target_account
    If Me.ef_target_account.TextValue <> "" Then
      m_target_account = Me.ef_target_account.Value
    End If

    ' Card Track data
    If Me.ef_track_data.TextValue <> "" Then
      m_track_data = Me.ef_track_data.TextValue
    End If

    ' Card Track data 2
    If Me.ef_track_data_2.TextValue <> "" Then
      m_track_data_2 = Me.ef_track_data_2.TextValue
    End If

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Terminals
    If Me.opt_all_terminals.Checked Then
      m_terminal = Me.opt_all_terminals.Text
    End If
    If Me.opt_one_cashier.Checked Then
      m_terminal = Me.cmb_cashier.TextValue
    End If
    If Me.opt_one_terminal.Checked Then
      m_terminal = Me.cmb_terminal.TextValue
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    m_initial_account = 0
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal account_id As Int64)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_initial_account = account_id
    ef_account_number.Value = account_id

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Account number
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCT_NUMBER).Width = 900
      .Column(GRID_COLUMN_CARD_TRACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account type
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Width = 0

      ' Card Track
      .Column(GRID_COLUMN_CARD_TRACK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CARD_TRACK).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_CARD_TRACK).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_CARD_TRACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Type Id
      .Column(GRID_COLUMN_TYPE_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TYPE_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TYPE_ID).Width = 0
      .Column(GRID_COLUMN_TYPE_ID).IsColumnPrintable = False

      ' Operation Type Name
      .Column(GRID_COLUMN_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(211)
      .Column(GRID_COLUMN_TYPE_NAME).Width = GRID_WIDTH_MOVEMENT_TYPE_NAME
      .Column(GRID_COLUMN_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      ' Terminal Name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Initial balance
      .Column(GRID_COLUMN_INIT_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_INIT_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(213)
      .Column(GRID_COLUMN_INIT_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_INIT_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Sub amount
      .Column(GRID_COLUMN_SUB_POINTS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_SUB_POINTS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(214)
      .Column(GRID_COLUMN_SUB_POINTS).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_SUB_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Add amount
      .Column(GRID_COLUMN_ADD_POINTS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_ADD_POINTS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(215)
      .Column(GRID_COLUMN_ADD_POINTS).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_ADD_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Final balance
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(216)
      .Column(GRID_COLUMN_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Target Account
      .Column(GRID_COLUMN_SERVICE_ACC_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_SERVICE_ACC_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(307)
      .Column(GRID_COLUMN_SERVICE_ACC_ID).Width = 900
      .Column(GRID_COLUMN_SERVICE_ACC_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    If m_initial_account = 0 Then
      Me.ef_account_number.TextValue = ""
    Else
      Me.ef_account_number.TextValue = m_initial_account
    End If

    Me.ef_target_account.TextValue = ""
    Me.ef_track_data.TextValue = ""
    Me.ef_track_data_2.TextValue = ""

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Me.opt_all_terminals.Checked = True
    Me.cmb_cashier.Enabled = False
    Me.cmb_terminal.Enabled = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim internal_card_id As String
    Dim card_type As Integer
    Dim rc As Boolean

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (AM_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (AM_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    '' Filter Track Data sle: pendiente revision
    'If Me.ef_track_data.TextValue <> "" Then
    '  ' First convert external (encrypted) card number into the internal id used in the database
    '  internal_card_id = ""
    '  If Me.ef_track_data.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
    '    rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_track_data.TextValue, internal_card_id, card_type)
    '    If rc = True Then
    '      str_where = str_where & " AND AC1.AC_TRACK_DATA = '" & internal_card_id & "'"
    '    Else
    '      str_where = str_where & " AND AC1.AC_TRACK_DATA = '" & Me.ef_track_data.TextValue & "'"
    '    End If
    '  Else
    '    str_where = str_where & " AND AC1.AC_TRACK_DATA = '" & Me.ef_track_data.TextValue & "'"
    '  End If
    'End If

    ' Filter Track Data 2
    'If Me.ef_track_data_2.TextValue <> "" Then
    '  ' First convert external (encrypted) card number into the internal id used in the database
    '  str_where = str_where & " AND (AC2.AC_ACCOUNT_ID = AM_SECOND_ACCOUNT_ID)"
    '  internal_card_id = ""
    '  If Me.ef_track_data_2.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
    '    rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_track_data_2.TextValue, internal_card_id, card_type)
    '    If rc = True Then
    '      str_where = str_where & " AND AC2.AC_TRACK_DATA = '" & internal_card_id & "'"
    '    Else
    '      str_where = str_where & " AND AC2.AC_TRACK_DATA = '" & Me.ef_track_data_2.TextValue & "'"
    '    End If
    '  Else
    '    str_where = str_where & " AND AC2.AC_TRACK_DATA = '" & Me.ef_track_data_2.TextValue & "'"
    '  End If
    'End If

    ' Filter Track Data sle: pendiente revision
    If Me.ef_track_data.TextValue <> "" Then
      ' First convert external (encrypted) card number into the internal id used in the database
      internal_card_id = ""
      If Me.ef_track_data.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
        rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_track_data.TextValue, internal_card_id, card_type)
        If rc = True Then
          str_where = str_where & " AND AM_ACCOUNT_ID = '" & GetSqlWhereAccount(internal_card_id) & "'"
        Else
          str_where = str_where & " AND AM_ACCOUNT_ID = '" & GetSqlWhereAccount(Me.ef_track_data.TextValue) & "'"
        End If
      Else
        str_where = str_where & " AND AM_ACCOUNT_ID = '" & GetSqlWhereAccount(Me.ef_track_data.TextValue) & "'"
      End If
    End If

    ' Filter Track Data 2
    If Me.ef_track_data_2.TextValue <> "" Then
      ' First convert external (encrypted) card number into the internal id used in the database
      internal_card_id = ""
      If Me.ef_track_data_2.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
        rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_track_data_2.TextValue, internal_card_id, card_type)
        If rc = True Then
          str_where = str_where & " AND AM_SECOND_ACCOUNT_ID = '" & GetSqlWhereAccount(internal_card_id) & "'"
        Else
          str_where = str_where & " AND AM_SECOND_ACCOUNT_ID = '" & GetSqlWhereAccount(Me.ef_track_data_2.TextValue) & "'"
        End If
      Else
        str_where = str_where & " AND AM_SECOND_ACCOUNT_ID = '" & GetSqlWhereAccount(Me.ef_track_data_2.TextValue) & "'"
      End If
    End If

    ' Filter Account Number
    If Me.ef_account_number.TextValue <> "" Then
      str_where = str_where & " AND (AM_ACCOUNT_ID = " & Me.ef_account_number.Value & ") "
    End If

    'ef_target_account
    If Me.ef_target_account.TextValue <> "" Then
      str_where = str_where & " AND (AM_SECOND_ACCOUNT_ID = " & Me.ef_target_account.Value & ") "
    End If

    ' Filter Cashier
    If Me.opt_one_cashier.Checked = True Then
      str_where = str_where & " AND (AM_CASHIER_ID = " & Me.cmb_cashier.Value & ") "
    End If

    ' Filter Terminal
    If Me.opt_one_terminal.Checked = True Then
      str_where = str_where & " AND (AM_TERMINAL_ID = " & Me.cmb_terminal.Value & ") "
    End If

    Return str_where
  End Function ' GetSqlWhere

  Private Function GetSqlWhereAccount(ByVal str_track_data As String) As Long
    Dim acc_id As Long = 0
    Dim sql_cmd As System.Data.SqlClient.SqlCommand
    Dim m_sql_conn As System.Data.SqlClient.SqlConnection

    Try
      m_sql_conn = Nothing
      m_sql_conn = NewSQLConnection()

      If Not (m_sql_conn Is Nothing) Then
        ' Connected
        If m_sql_conn.State <> ConnectionState.Open Then
          ' Connecting ..
          m_sql_conn.Open()
        End If

        If Not m_sql_conn.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      sql_cmd = New System.Data.SqlClient.SqlCommand("SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TYPE IN (2,3,4) AND AC_TRACK_DATA = @p1 ", m_sql_conn)
      sql_cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = str_track_data
      acc_id = CLng(sql_cmd.ExecuteScalar())
      m_sql_conn.Close()

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    Finally
    End Try

    Return acc_id
  End Function

#End Region ' Private Functions

#Region " Events "

  Private Sub opt_all_cashiers_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Me.cmb_cashier.Enabled = False
  End Sub ' opt_all_cashiers_Click

  Private Sub opt_one_cashier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
    Me.cmb_terminal.Enabled = False
  End Sub ' opt_one_cashier_Click

  Private Sub opt_one_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_terminal.Click
    Me.cmb_cashier.Enabled = False
    Me.cmb_terminal.Enabled = True
  End Sub ' opt_one_terminal_Click

  Private Sub opt_all_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_terminals.Click
    Me.cmb_terminal.Enabled = False
    Me.cmb_cashier.Enabled = False
  End Sub ' opt_all_terminals_Click

#End Region ' Events

End Class
