'------------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'------------------------------------------------------------------------
'
' MODULE NAME:   frm_cashdesk_draws_configuration_edit
' DESCRIPTION:   Cash Desk Draws configuration parameters edition
' AUTHOR:        Marcos Mohedano
' CREATION DATE: 30-JUL-2013
'
' REVISION HISTORY: 
'
' Date        Author Description
' ----------  ------ -----------------------------------------------------
' 30-JUL-2013 MMG    Initial version
' 13-AUG-2013 DMR    Modified GUI_SetScreenData and GUI_InitControls functions
' 14-AUG-2013 DMR    Modified GUI_GetScreenData and GUI_IsScreenDataOk functions
' 19-AUG-2013 MMG    Modified GUI_GetScreenData and GUI_IsScreenDataOk functions
'                    Modified form controls TabIndex order
' 26-AUG-2013 MMG    Modified GUI_InitControls and GUI_SetScreenData functions
' 28-AUG-2013 DHA    Added general param: Non Redeemable cash desk draw prize'
' 14-OCT-2013 ANG    Fix bug WIG-64
' 30-OCT-2013 QMP    Fixed bug #WIG-345: Validation error when UNR credits are disabled
' 14-OCT-2015 SDS    Add New Promotion "In Kind (RE)"
' 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
' 22-FEB-2016 JML    Bug 9788:Sorteo de caja: Error en la configuración Sorteo
' 12-SEP-2016 RGR    Product Backlog Item 17445:Televisa - Cashier draw (Cash prize) - Configuration
' 14-SEP-2016 ESE    PBI 17115: Draws gamings tables
' 14-SEP-2016 RGR    Product Backlog Item 17445:Televisa - Cashier draw (Cash prize) - Configuration Change Enum
' 22-SEP-2016 RGR    Product Backlog Item 17964: Televisa - Draw Cash (Cash prize) - Configuration
' 22-SEP-2016 LTC    Product Backlog Item 17960:Tables 46 - Cashier draw for tables : Configuration Screen
' 26-SEP-2016 RGR    PBI 17965: Televisa - Draw Cash (Cash prize) - Probability of prizes
' 06-OCT-2016 RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
' 10-OCT-2016 RGR    Bug 18729: WIGOS GUI: In the draw cash game tables and columns and the Probability Example Recharge $ 100 overlap and can not be displayed or configured correctly
' 11-OCT-2016 RGR    Bug 18968: Draw box with cash prize (Televisa) - Probability: the value to deactivate and reactivate the line is lost
' 17-OCT-2016 JBC    Bug 19220: WigosGUI: Example Recharge $ 100 always show 0.
' 17-OCT-2017 AMF    Bug 29147:Wigos-4186: Terminal Draw: need to enlarge price participation
' 17-OCT-2017 AMF    PBI 30297:[WIGOS-33] Cash draw with a configurable bet amount
' 09-NOV-2017 MS     PBI 30550:[WIGOS-5561] Codere EGM Draw
' 21-NOV-2017 DPC    WIGOS 6747: unhandled exception error when saving data in Gambling Table Draw configuration screen
' 05-FEB-2018 AGS    Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
' 12-FEB-2018 AGS    Bug 31490:WIGOS-8171 Terminal draw configuration screen has a field enabled when the screen is disabled
' 14-FEB-2018 AGS    Fixed Bug 31411:WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
' 04-JUL-2018 DLM    Bug 33475: WIGOS-13382 Losing changes warning message appear without any change in the screen
'-------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports System.Text
Imports VSFlex8
Imports WSI.Common
Imports WigosGUI.CLASS_CASHDESK_DRAWS_CONFIGURATION


Public Class frm_cashdesk_draws_configuration_edit
  Inherits frm_base_edit

#Region "Constants"
  Private Const VOUCHER_NAMES_MAXLENGHT = 37
  Private Const MAX_PERCENTAGE = 100
  Private Const MIN_PERCENTAGE = -100
  Private Const RECHARGE_VALUE = "100"

  Private Const PRICE_POSITION_RIGHT = 0
  Private Const PRICE_MAX_POSITION_LEFT = 1
  Private Const PRICE_POSITION_LEFT = 2
  Private Const PRICE_MAX_POSITION_RIGHT = 3


#End Region ' Constants

#Region "Members "
  Private m_draw_configuration As ENUM_DRAW_CONFIGURATION
  Dim _general_params As WSI.Common.GeneralParam.Dictionary = WSI.Common.GeneralParam.Dictionary.Create()
  Dim m_max_price_position_isset As Boolean
  Dim m_price_position_isset As Boolean
  Dim m_participation_positions(4) As Point
#End Region ' Members

#Region "Constructors"

  Public Sub New()
    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_00
    Me.FormId = ENUM_FORM.FORM_CASHDESK_DRAWS_CONFIGURATION
  End Sub

  Public Sub New(ByVal DrawConfiguration As ENUM_DRAW_CONFIGURATION)
    m_draw_configuration = DrawConfiguration

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_00 Then
      Me.FormId = ENUM_FORM.FORM_CASHDESK_DRAWS_CONFIGURATION
    ElseIf m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_DRAWS_CONFIGURATION
    ElseIf m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 Then
      Me.FormId = ENUM_FORM.FORM_TERMINAL_DRAWS_CONFIGURATION
    End If
    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

#End Region ' Constructors

#Region "Overrides"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()


    Call GUI_SetMinDbVersion(149)

    'Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Function IsCashDeskDrawEnabled() As Boolean
    Return _general_params.GetBoolean("CashDesk.Draw", "Enabled", False)
  End Function

  Function IsTerminalDrawEnabled() As Boolean
    Return _general_params.GetBoolean("CashDesk.Draw.02", "Enabled", False)
  End Function

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _currency_symbol As String
    Dim _str_split_as_won As String

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_00 Then
      Me.FormId = ENUM_FORM.FORM_CASHDESK_DRAWS_CONFIGURATION
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2283)
      Me.chk_enable_draws.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2435)
      Me.chk_enable_draws.Enabled = Not IsTerminalDrawEnabled()
      gb_terminal.Enabled = False
      gb_terminal.Visible = False
    ElseIf m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_DRAWS_CONFIGURATION
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7588)
      Me.chk_enable_draws.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7589)
      gb_terminal.Enabled = False
      gb_terminal.Visible = False
    ElseIf m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 Then
      Me.FormId = ENUM_FORM.FORM_TERMINAL_DRAWS_CONFIGURATION
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7823)
      Me.chk_enable_draws.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7825)
      gb_cashier.Enabled = False
      gb_cashier.Visible = False
      gb_terminal.Enabled = True
      gb_terminal.Visible = True
      gb_ball_config.Enabled = True
      Me.chk_enable_draws.Enabled = Not IsCashDeskDrawEnabled()
      Me.chk_show_buy_dialog.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8464)
      Me.chk_player_can_cancel_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8465)
      Me.chk_allow_cancel_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8762)
      Me.chk_allow_cancel_recharge.Enabled = Me.chk_enable_draws.Checked
      Me.chk_allow_withdraw_pending.Enabled = Me.chk_enable_draws.Checked
      Me.chk_allow_withdraw_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8763)
    End If

    MyBase.GUI_InitControls()

    Me.InitializePrizeControlsPositions()

    'Set form name

    'Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    'Set currency symbol according to the current culture
    _currency_symbol = Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol

    'Controls texts and formats
    Me.ef_voucher_name_winner.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2550)
    Me.ef_voucher_name_winner.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, VOUCHER_NAMES_MAXLENGHT)
    Me.ef_voucher_name_loser.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2550)
    Me.ef_voucher_name_loser.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, VOUCHER_NAMES_MAXLENGHT)

    If WSI.Common.Misc.IsVoucherModeTV Then 'Solo para Televisa
      Me.gb_ball_config.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7684)
    Else
      Me.gb_ball_config.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2445)
    End If

    Me.ef_extracted_balls.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2446)
    Me.ef_extracted_balls.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_balls_participant.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2447)
    Me.ef_balls_participant.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_total_balls.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2448)
    Me.ef_total_balls.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)

    Me.gb_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    Me.gb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7823)
    Me.chk_show_draws_cd.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2438)
    Me.chk_print_voucher_winner.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2439)
    Me.chk_print_voucher_loser.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2529)
    Me.chk_ask_participation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2440)
    Me.lbl_not_connected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2441)
    Me.panel_not_connected.Enabled = Not Me.rb_opt_Local_DB.Checked
    Me.rb_dont_recharge.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2442)

    _str_split_as_won = GetPromoName(Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN)
    Me.rb_recharge_red.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2443) & " (" & _str_split_as_won & ")"

    Me.rb_recharge_no_red.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2444)

    Me.rb_recharge_no_red.Visible = False

    Me.gb_prizes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2455)

    Me.lbl_prizes_category.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2524)
    Me.lbl_prizes_enable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2525)
    Me.lbl_prizes_fixed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2450)
    Me.lbl_prizes_percentage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2451)

    Me.lbl_prizes_access_perc.Text = GetCashierData("Split.B.Name")

    Me.lbl_prizes_example.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2527, _currency_symbol + RECHARGE_VALUE)

    Me.lbl_loser.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2449)
    Me.lbl_winner_prize1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2456)
    Me.lbl_winner_prize2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2457)
    Me.lbl_winner_prize3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2458)
    Me.lbl_winner_prize4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2459)

    Me.lbl_probability.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7626)

    'Me.chk_enable_loser.Checked = True
    'Me.chk_enable_loser.Visible = False

    Me.ef_cprize_fixed.Enabled = Me.chk_enable_loser.Checked
    Me.ef_cprize_fixed.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES, 8, 2)
    Me.ef_cprize_percentage.Enabled = Me.chk_enable_loser.Checked
    Me.ef_cprize_percentage.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)
    Me.ef_cprize_percentage.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)

    Me.gb_prize_probability.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2452)
    Me.ef_participants.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2453)
    Me.ef_participants.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6)
    Me.ef_winners.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2454)
    Me.ef_winners.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6)

    Me.ef_fixed_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_fixed_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES, 8, 2)

    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_1.Enabled = False
    Else
      Me.ef_perc_1.Enabled = Me.chk_enable_winner_1.Checked
    End If

    Me.ef_perc_1.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_perc_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_fixed_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.ef_fixed_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES, 8, 2)

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_2.Enabled = False
    Else
      Me.ef_perc_2.Enabled = Me.chk_enable_winner_2.Checked
    End If

    Me.ef_perc_2.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_perc_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_fixed_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.ef_fixed_3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES, 8, 2)

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_3.Enabled = False
    Else
      Me.ef_perc_3.Enabled = Me.chk_enable_winner_3.Checked
    End If

    Me.ef_perc_3.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_perc_3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_fixed_4.Enabled = Me.chk_enable_winner_4.Checked
    Me.ef_fixed_4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES, 8, 2)

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_4.Enabled = False
    Else
      Me.ef_perc_4.Enabled = Me.chk_enable_winner_4.Checked
    End If

    Me.ef_perc_4.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_perc_4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.gb_draw_connection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2469)

    Me.rb_opt_Local_DB.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2470)
    Me.rb_opt_webservice.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2471)

    Me.rb_opt_ext_db.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2472)

    Me.ef_center_address_1.Enabled = Me.rb_opt_webservice.Checked
    Me.ef_center_address_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)
    Me.lbl_center_address_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2436)

    Me.ef_center_address_2.Enabled = Me.rb_opt_webservice.Checked
    Me.ef_center_address_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)
    Me.lbl_center_address_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2437)

    'Tax provisions
    Me.lbl_tax_provisions.Text = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7151))
    Me.ef_tax_provisions_1.IsReadOnly = True
    Me.ef_tax_provisions_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_tax_provisions_1.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_tax_provisions_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_tax_provisions_2.IsReadOnly = True
    Me.ef_tax_provisions_2.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_tax_provisions_2.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_tax_provisions_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_tax_provisions_3.IsReadOnly = True
    Me.ef_tax_provisions_3.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_tax_provisions_3.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_tax_provisions_3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_tax_provisions_4.IsReadOnly = True
    Me.ef_tax_provisions_4.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_tax_provisions_4.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_tax_provisions_4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_tax_provisions_loser.IsReadOnly = True
    Me.ef_tax_provisions_loser.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_tax_provisions_loser.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_tax_provisions_loser.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.lbl_tax_provisions_remarks.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7659)

    InitExternalDbEntryFields()

    Me.ef_access_perc_loser.IsReadOnly = True
    Me.ef_access_perc_loser.Enabled = Me.chk_enable_loser.Checked
    Me.ef_access_perc_loser.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_access_perc_loser.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_access_perc_winner_1.IsReadOnly = True
    Me.ef_access_perc_winner_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_access_perc_winner_1.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_access_perc_winner_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_access_perc_winner_2.IsReadOnly = True
    Me.ef_access_perc_winner_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.ef_access_perc_winner_2.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_access_perc_winner_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_access_perc_winner_3.IsReadOnly = True
    Me.ef_access_perc_winner_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.ef_access_perc_winner_3.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_access_perc_winner_3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.ef_access_perc_winner_4.IsReadOnly = True
    Me.ef_access_perc_winner_4.Enabled = Me.chk_enable_winner_4.Checked
    Me.ef_access_perc_winner_4.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_access_perc_winner_4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 6, 2)

    Me.lbl_example_loser.Enabled = Me.chk_enable_loser.Checked
    Me.lbl_example_winner_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.lbl_example_winner_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.lbl_example_winner_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.lbl_example_winner_4.Enabled = Me.chk_enable_winner_4.Checked

    Me.cmb_loser_credit_type.Enabled = Me.chk_enable_loser.Checked
    Me.cmb_prize1_credit_type.Enabled = Me.chk_enable_winner_1.Checked
    Me.cmb_prize2_credit_type.Enabled = Me.chk_enable_winner_2.Checked
    Me.cmb_prize3_credit_type.Enabled = Me.chk_enable_winner_3.Checked
    Me.cmb_prize4_credit_type.Enabled = Me.chk_enable_winner_4.Checked

    Me.lbl_prizes_nonredemable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2598) 'Type

    Me.lbl_prize_explanation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2567, GetCashierData("Split.A.CashInPct"))
    Me.lbl_cpromo_explanation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2568, GetCashierData("Split.A.CashInPct"))

    Me.cmb_prize_mode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2588)

    Call InitializePriceLocations()

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 Then
      Me.ef_participation_price.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7587)
      Me.ef_participation_price.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
      Me.ef_participation_price.SufixTextVisible = True
      Me.ef_participation_price.SufixTextWidth = 14
      Me.ef_participation_price.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
      ef_participation_max_price.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7888)
      ef_participation_max_price.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
      ef_participation_max_price.Visible = True
      ef_terminal_game_url.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7889)
      ef_terminal_game_url.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 240)
      ef_terminal_game_url.Visible = True ' TODO: Visible = True
      ef_terminal_game_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      ef_terminal_game_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 240)
      ef_terminal_game_timeout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7892)
      ef_terminal_game_timeout.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
      ef_terminal_game_timeout.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7891)
      ef_terminal_game_timeout.SufixTextWidth = 80 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(7889)
      ef_terminal_game_timeout.SufixTextVisible = True
      ef_terminal_game_timeout.Visible = True ' TODO: Visible = True
      ef_terminal_game_draws.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8764)
      ef_terminal_game_draws.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
      ef_terminal_game_draws.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8765)
      ef_terminal_game_draws.SufixTextWidth = 80 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(7889)
      ef_terminal_game_draws.SufixTextVisible = True
      ef_terminal_game_draws.Visible = True ' TODO: Visible = True
      ef_terminal_game_draws.TextWidth = 150
      ef_terminal_game_name.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_timeout.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_draws.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_url.Enabled = Me.chk_enable_draws.Checked


      Call SetMaxPriceLocation()

    Else

      Me.ef_participation_price.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7587)
      Me.ef_participation_price.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
      ef_participation_max_price.Visible = False
      ef_terminal_game_url.Visible = False
      ef_terminal_game_timeout.Visible = False
      ef_terminal_game_name.Visible = False
      Me.chk_show_buy_dialog.Visible = False
      Me.chk_player_can_cancel_draw.Visible = False
      Me.chk_allow_cancel_recharge.Visible = False
      Me.chk_allow_withdraw_pending.Visible = False
    End If

    Me.ef_probability_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_probability_1.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_probability_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    Me.ef_probability_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.ef_probability_2.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_probability_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    Me.ef_probability_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.ef_probability_3.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_probability_3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    Me.ef_probability_4.Enabled = Me.chk_enable_winner_4.Checked
    Me.ef_probability_4.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_probability_4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    Me.ef_probability_5.Enabled = Me.chk_enable_loser.Checked
    Me.ef_probability_5.SufixText = GLB_NLS_GUI_INVOICING.GetString(160)
    Me.ef_probability_5.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)

    Me.lbl_sum.Text = String.Empty

    CmbPrizeLoad()

    CmbPrizesCreditTypesLoad()

    CmbLoserCreditTypesLoad()

    Me.panel_webservice.Enabled = False
    Me.panel_webservice.Visible = False



  End Sub

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _cdc_params As CLASS_CASHDESK_DRAWS_CONFIGURATION
    Dim _csb As SqlConnectionStringBuilder
    Dim _point As Point
    Dim _size As Size
    _cdc_params = DbReadObject

    Me.chk_enable_draws.Checked = _cdc_params.Enabled AndAlso Me.chk_enable_draws.Enabled
    Me.pnl_enable_draws.Enabled = Me.chk_enable_draws.Checked
    Me.chk_show_draws_cd.Checked = _cdc_params.ShowCashdeskDraws
    Me.chk_print_voucher_winner.Checked = _cdc_params.VoucherOnCashdeskDrawsWinner
    Me.ef_voucher_name_winner.Enabled = Me.chk_print_voucher_winner.Checked
    Me.ef_voucher_name_winner.Value = _cdc_params.WinnerVoucherTitle
    Me.chk_print_voucher_loser.Checked = _cdc_params.VoucherOnCashdeskDrawsLoser
    Me.ef_voucher_name_loser.Enabled = Me.chk_print_voucher_loser.Checked
    Me.ef_voucher_name_loser.Value = _cdc_params.LoserVoucherTitle
    Me.chk_ask_participation.Checked = _cdc_params.AskForParticipation

    Select Case _cdc_params.WaitNotConnected
      Case CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.DONT_RECHARGE
        Me.rb_dont_recharge.Checked = True

      Case CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.RECHARGE_REDIMIBLE
        Me.rb_recharge_red.Checked = True

      Case CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.RECHARGE_NONREDIMIBLE
        Me.rb_recharge_no_red.Checked = True

      Case Else
        Me.rb_dont_recharge.Checked = True

    End Select

    Select Case _cdc_params.DrawLocation
      Case CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_DRAW_LOCATION.LOCAL_DB
        Me.rb_opt_Local_DB.Checked = True

      Case CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_DRAW_LOCATION.WEBSERVICE_OR_EXTERNALDB
        Me.rb_opt_webservice.Checked = True

      Case Else
        Me.rb_opt_Local_DB.Checked = True

    End Select

    Me.rb_opt_ext_db.Checked = Not String.IsNullOrEmpty(_cdc_params.ServerDbConnectionString)

    Me.ef_center_address_1.Value = _cdc_params.ServerAddress1
    Me.ef_center_address_2.Value = _cdc_params.ServerAddress2

    If Me.rb_opt_ext_db.Checked Then
      _csb = New SqlConnectionStringBuilder(_cdc_params.ServerDbConnectionString)

      Me.ef_extdb_server_1.Value = IIf(Not String.IsNullOrEmpty(_csb.DataSource), _csb.DataSource, "")
      Me.ef_extdb_server_2.Value = IIf(Not String.IsNullOrEmpty(_csb.FailoverPartner), _csb.FailoverPartner, "")
      Me.ef_extdb_database.Value = IIf(Not String.IsNullOrEmpty(_csb.InitialCatalog), _csb.InitialCatalog, "")
      Me.ef_extdb_user.Value = IIf(Not String.IsNullOrEmpty(_csb.UserID), _csb.UserID, "")
      Me.ef_extdb_pwd.Value = IIf(Not String.IsNullOrEmpty(_csb.Password), _csb.Password, "")
    Else
      ClearExternalDbEntryFields()
    End If

    Me.ef_extracted_balls.Value = _cdc_params.BallsExtracted.ToString()
    Me.ef_balls_participant.Value = _cdc_params.BallsOfParticipant.ToString()
    Me.ef_total_balls.Value = _cdc_params.TotalBallsNumber.ToString()
    Me.ef_participants.Value = _cdc_params.NumberOfParticipants.ToString()
    Me.ef_winners.Value = _cdc_params.NumberOfWinners.ToString()

    Me.ef_cprize_fixed.Value = _cdc_params.LoserPrizeFixed.ToString()
    Me.ef_cprize_percentage.Value = _cdc_params.LoserPrizePercentage.ToString()
    Me.chk_enable_loser.Checked = _cdc_params.LoserPrizeEnabled
    Me.ef_probability_5.Value = _cdc_params.LoserPrizeProbability.ToString()

    Me.ef_fixed_1.Value = _cdc_params.WinnerPrize1Fixed.ToString()
    Me.ef_perc_1.Value = _cdc_params.WinnerPrize1Percentage.ToString()
    Me.chk_enable_winner_1.Checked = _cdc_params.WinnerPrize1Enabled
    Me.ef_probability_1.Value = _cdc_params.WinnerPrize1Probability.ToString()

    Me.ef_fixed_2.Value = _cdc_params.WinnerPrize2Fixed.ToString()
    Me.ef_perc_2.Value = _cdc_params.WinnerPrize2Percentage.ToString()
    Me.chk_enable_winner_2.Checked = _cdc_params.WinnerPrize2Enabled
    Me.ef_probability_2.Value = _cdc_params.WinnerPrize2Probability.ToString()

    Me.ef_fixed_3.Value = _cdc_params.WinnerPrize3Fixed.ToString()
    Me.ef_perc_3.Value = _cdc_params.WinnerPrize3Percentage.ToString()
    Me.chk_enable_winner_3.Checked = _cdc_params.WinnerPrize3Enabled
    Me.ef_probability_3.Value = _cdc_params.WinnerPrize3Probability.ToString()

    Me.ef_fixed_4.Value = _cdc_params.WinnerPrize4Fixed.ToString()
    Me.ef_perc_4.Value = _cdc_params.WinnerPrize4Percentage.ToString()
    Me.chk_enable_winner_4.Checked = _cdc_params.WinnerPrize4Enabled
    Me.ef_probability_4.Value = _cdc_params.WinnerPrize4Probability.ToString()

    Me.ef_access_perc_loser.Value = _cdc_params.LoserPrizeAccessPercentage
    Me.ef_access_perc_winner_1.Value = _cdc_params.WinnerPrize1AccessPercentage
    Me.ef_access_perc_winner_2.Value = _cdc_params.WinnerPrize2AccessPercentage
    Me.ef_access_perc_winner_3.Value = _cdc_params.WinnerPrize3AccessPercentage
    Me.ef_access_perc_winner_4.Value = _cdc_params.WinnerPrize4AccessPercentage

    Me.ef_tax_provisions_loser.Value = _cdc_params.LoserTaxProvisionsPercentage
    Me.ef_tax_provisions_1.Value = _cdc_params.WinnerTaxProvisions1Percentage
    Me.ef_tax_provisions_2.Value = _cdc_params.WinnerTaxProvisions2Percentage
    Me.ef_tax_provisions_3.Value = _cdc_params.WinnerTaxProvisions3Percentage
    Me.ef_tax_provisions_4.Value = _cdc_params.WinnerTaxProvisions4Percentage

    Me.lbl_tax_provisions.Visible = _cdc_params.TaxProvisionsEnable
    Me.ef_tax_provisions_loser.Visible = _cdc_params.TaxProvisionsEnable
    Me.ef_tax_provisions_1.Visible = _cdc_params.TaxProvisionsEnable
    Me.ef_tax_provisions_2.Visible = _cdc_params.TaxProvisionsEnable
    Me.ef_tax_provisions_3.Visible = _cdc_params.TaxProvisionsEnable
    Me.ef_tax_provisions_4.Visible = _cdc_params.TaxProvisionsEnable
    Me.lbl_tax_provisions_remarks.Visible = _cdc_params.TaxProvisionsEnable

    Call InitializePriceLocations()

    If Not _cdc_params.TaxProvisionsEnable Then
      _point = Me.lbl_prizes_example.Location
      _point.X -= 100
      Me.lbl_prizes_example.Location = _point

      _point = Me.lbl_example_winner_1.Location
      _point.X -= 100
      Me.lbl_example_winner_1.Location = _point

      _point = Me.lbl_example_winner_2.Location
      _point.X -= 100
      Me.lbl_example_winner_2.Location = _point

      _point = Me.lbl_example_winner_3.Location
      _point.X -= 100
      Me.lbl_example_winner_3.Location = _point

      _point = Me.lbl_example_winner_4.Location
      _point.X -= 100
      Me.lbl_example_winner_4.Location = _point

      _point = Me.lbl_example_loser.Location
      _point.X -= 100
      Me.lbl_example_loser.Location = _point

      _point = Me.lbl_probability.Location
      _point.X -= 100
      Me.lbl_probability.Location = _point

      _point = Me.ef_probability_1.Location
      _point.X -= 100
      Me.ef_probability_1.Location = _point

      _point = Me.ef_probability_2.Location
      _point.X -= 100
      Me.ef_probability_2.Location = _point

      _point = Me.ef_probability_3.Location
      _point.X -= 100
      Me.ef_probability_3.Location = _point

      _point = Me.ef_probability_4.Location
      _point.X -= 100
      Me.ef_probability_4.Location = _point

      _point = Me.ef_probability_5.Location
      _point.X -= 100
      Me.ef_probability_5.Location = _point

      _point = Me.lbl_sum.Location
      _point.X -= 100
      Me.lbl_sum.Location = _point

      _point = Me.cmb_prize_mode.Location
      _point.X -= 100
      Me.cmb_prize_mode.Location = _point

      _point = Me.ef_total_balls.Location
      _point.X -= 100
      Me.ef_total_balls.Location = _point

      _point = Me.ef_extracted_balls.Location
      _point.X -= 100
      Me.ef_extracted_balls.Location = _point

      _point = Me.ef_balls_participant.Location
      _point.X -= 100
      Me.ef_balls_participant.Location = _point

      _size = Me.gb_ball_config.Size
      _size.Width -= 100
      Me.gb_ball_config.Size = _size

      _size = Me.gb_prizes.Size
      _size.Width -= 100
      Me.gb_prizes.Size = _size

      _size = Me.pnl_enable_draws.Size
      _size.Width -= 100
      Me.pnl_enable_draws.Size = _size

      _size = Me.Size
      _size.Width -= 100
      Me.Size = _size

    End If

    Call UpdateWinners()

    Me.cmb_prize_mode.Value = _cdc_params.PrizeMode

    If (_cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.CashPrize Or
        _cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.Terminal Or
        _cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.PlayerSession) Then
      Me.ef_participation_price.Value = _cdc_params.ParticipationPrice
      Me.ef_participation_price.Visible = True
      If (_cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.Terminal) Then
        ' 04-JUL-2018  DLM    Bug : WIGOS-13382 Losing changes warning message appear without any change in the screen
        'ef_participation_max_price.Value = _cdc_params.ParticipationMaxPrice
        'ef_terminal_game_timeout.Value = IIf(_cdc_params.TerminalGameTimeout = 0, 30, _cdc_params.TerminalGameTimeout)
        'ef_terminal_game_draws.Value = _cdc_params.GroupDraws        
        'ef_terminal_game_name.Value = _cdc_params.TerminalGameName
        ' 04-JUL-2018  DLM    Bug : WIGOS-13382 Losing changes warning message appear without any change in the screen
        ef_participation_max_price.Visible = True
        ef_terminal_game_url.Visible = True ' TODO Value = Visible
        ef_terminal_game_url.Value = _cdc_params.TerminalGameUrl
        ef_terminal_game_timeout.Visible = True ' TODO Value = Visible
        ef_terminal_game_name.Visible = True

        Call SetMaxPriceLocation()
      Else
      End If
    Else
      Me.ef_participation_price.Value = 0
    End If

    ' 04-JUL-2018  DLM    Bug : WIGOS-13382 Losing changes warning message appear without any change in the screen
    ef_terminal_game_draws.Value = _cdc_params.GroupDraws
    ef_participation_max_price.Value = _cdc_params.ParticipationMaxPrice
    ef_terminal_game_name.Value = _cdc_params.TerminalGameName
    ef_terminal_game_timeout.Value = IIf(_cdc_params.TerminalGameTimeout = 0, 30, _cdc_params.TerminalGameTimeout)
    ' 04-JUL-2018  DLM    Bug : WIGOS-13382 Losing changes warning message appear without any change in the screen

    CmbPrizesCreditTypesLoad()

    Me.lbl_example_loser.Text = GetExampleValue(RECHARGE_VALUE, Me.ef_cprize_fixed.Value, Me.ef_cprize_percentage.Value, Me.ef_access_perc_loser.Value, Me.ef_tax_provisions_loser.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_1.Text = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_1.Value, Me.ef_perc_1.Value, Me.ef_access_perc_winner_1.Value, Me.ef_tax_provisions_1.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_2.Text = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_2.Value, Me.ef_perc_2.Value, Me.ef_access_perc_winner_2.Value, Me.ef_tax_provisions_2.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_3.Text = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_3.Value, Me.ef_perc_3.Value, Me.ef_access_perc_winner_3.Value, Me.ef_tax_provisions_3.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_4.Text = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_4.Value, Me.ef_perc_4.Value, Me.ef_access_perc_winner_4.Value, Me.ef_tax_provisions_4.Value, Me.ef_participation_price.Value)


    'Update the values and selected items on all the prizes credit types, depending of the previous DB values
    CmbPrizeCreditTypeUpdate(Me.cmb_prize1_credit_type, _cdc_params.WinnerPrize1CreditType)
    CmbPrizeCreditTypeUpdate(Me.cmb_prize2_credit_type, _cdc_params.WinnerPrize2CreditType)
    CmbPrizeCreditTypeUpdate(Me.cmb_prize3_credit_type, _cdc_params.WinnerPrize3CreditType)
    CmbPrizeCreditTypeUpdate(Me.cmb_prize4_credit_type, _cdc_params.WinnerPrize4CreditType)
    CmbPrizeCreditTypeUpdate(Me.cmb_loser_credit_type, _cdc_params.LoserCreditType)

    Call Me.UpdateSum()
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 Then
      ef_terminal_game_name.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_timeout.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_draws.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_url.Enabled = Me.chk_enable_draws.Checked
      chk_show_buy_dialog.Enabled = Me.chk_enable_draws.Checked
      chk_player_can_cancel_draw.Enabled = Me.chk_enable_draws.Checked
      Me.chk_allow_withdraw_pending.Checked = _cdc_params.WithdrawalWithPending
      Me.chk_allow_cancel_recharge.Checked = _cdc_params.CancelWithPending

      Me.chk_player_can_cancel_draw.Checked = _cdc_params.PlayerCanCancelDraw
      Me.chk_show_buy_dialog.Checked = _cdc_params.ShowBuyDialog

      If (Not Me.chk_show_buy_dialog.Checked) Then
        Me.chk_player_can_cancel_draw.Enabled = False
      End If

    End If

  End Sub ' GUI_SetScreenData

  ' PURPOSE: It gets the values from the entry field and set them to a CLASS_CASHDESK_DRAWS_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _cdc_params As CLASS_CASHDESK_DRAWS_CONFIGURATION
    Dim _str_external_db_connection_string As String

    _cdc_params = DbEditedObject

    _cdc_params.Enabled = Me.chk_enable_draws.Checked
    _cdc_params.ShowCashdeskDraws = Me.chk_show_draws_cd.Checked
    _cdc_params.VoucherOnCashdeskDrawsWinner = Me.chk_print_voucher_winner.Checked
    _cdc_params.WinnerVoucherTitle = Me.ef_voucher_name_winner.Value
    _cdc_params.VoucherOnCashdeskDrawsLoser = Me.chk_print_voucher_loser.Checked
    _cdc_params.LoserVoucherTitle = Me.ef_voucher_name_loser.Value
    _cdc_params.AskForParticipation = Me.chk_ask_participation.Checked

    If Me.rb_dont_recharge.Checked Or Me.rb_opt_Local_DB.Checked Then
      _cdc_params.WaitNotConnected = CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.DONT_RECHARGE

    ElseIf Me.rb_recharge_red.Checked Then
      _cdc_params.WaitNotConnected = CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.RECHARGE_REDIMIBLE

    ElseIf Me.rb_recharge_no_red.Checked Then
      _cdc_params.WaitNotConnected = CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.RECHARGE_NONREDIMIBLE
    Else
      _cdc_params.WaitNotConnected = CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_WAIT_NOT_CONECTED.DONT_RECHARGE

    End If

    If Me.rb_opt_Local_DB.Checked Then
      _cdc_params.DrawLocation = CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_DRAW_LOCATION.LOCAL_DB

    Else
      _cdc_params.DrawLocation = CLASS_CASHDESK_DRAWS_CONFIGURATION.ENUM_DRAW_LOCATION.WEBSERVICE_OR_EXTERNALDB

    End If

    _cdc_params.ServerAddress1 = Me.ef_center_address_1.Value
    _cdc_params.ServerAddress2 = Me.ef_center_address_2.Value

    'When creating the external DB Connection String, if one of the fields 'Main Server' or 'Secondary Server' is empty, set the empty field with the same value as the other one
    _str_external_db_connection_string = WGDB.CreateConnectionString(IIf(String.IsNullOrEmpty(Me.ef_extdb_server_1.Value), Me.ef_extdb_server_2.Value, Me.ef_extdb_server_1.Value), _
                                                                     IIf(String.IsNullOrEmpty(Me.ef_extdb_server_2.Value), Me.ef_extdb_server_1.Value, Me.ef_extdb_server_2.Value), _
                                                                     Me.ef_extdb_database.Value, _
                                                                     Me.ef_extdb_user.Value, _
                                                                     Me.ef_extdb_pwd.Value, _
                                                                     1)

    _cdc_params.ServerDbConnectionString = IIf(Not String.IsNullOrEmpty(_str_external_db_connection_string), _str_external_db_connection_string, "")
    _cdc_params.BallsExtracted = IIf(String.IsNullOrEmpty(Me.ef_extracted_balls.Value), 0, Me.ef_extracted_balls.Value)
    _cdc_params.BallsOfParticipant = IIf(String.IsNullOrEmpty(Me.ef_balls_participant.Value), 0, Me.ef_balls_participant.Value)
    _cdc_params.TotalBallsNumber = IIf(String.IsNullOrEmpty(Me.ef_total_balls.Value), 0, Me.ef_total_balls.Value)
    _cdc_params.NumberOfParticipants = IIf(String.IsNullOrEmpty(Me.ef_participants.Value), 0, Me.ef_participants.Value)
    _cdc_params.NumberOfWinners = IIf(String.IsNullOrEmpty(Me.ef_winners.Value), 0, Me.ef_winners.Value)

    _cdc_params.LoserPrizeFixed = IIf(String.IsNullOrEmpty(Me.ef_cprize_fixed.Value), 0, Me.ef_cprize_fixed.Value)
    _cdc_params.LoserPrizePercentage = IIf(String.IsNullOrEmpty(Me.ef_cprize_percentage.Value), 0, Me.ef_cprize_percentage.Value)
    _cdc_params.LoserPrizeEnabled = Me.chk_enable_loser.Checked
    _cdc_params.LoserPrizeProbability = IIf(String.IsNullOrEmpty(Me.ef_probability_5.Value), 0, Me.ef_probability_5.Value)

    _cdc_params.WinnerPrize1Fixed = IIf(String.IsNullOrEmpty(Me.ef_fixed_1.Value), 0, Me.ef_fixed_1.Value)
    _cdc_params.WinnerPrize1Percentage = IIf(String.IsNullOrEmpty(Me.ef_perc_1.Value), 0, Me.ef_perc_1.Value)
    _cdc_params.WinnerPrize1Enabled = Me.chk_enable_winner_1.Checked
    _cdc_params.WinnerPrize1Probability = IIf(String.IsNullOrEmpty(Me.ef_probability_1.Value), 0, Me.ef_probability_1.Value)

    _cdc_params.WinnerPrize2Fixed = IIf(String.IsNullOrEmpty(Me.ef_fixed_2.Value), 0, Me.ef_fixed_2.Value)
    _cdc_params.WinnerPrize2Percentage = IIf(String.IsNullOrEmpty(Me.ef_perc_2.Value), 0, Me.ef_perc_2.Value)
    _cdc_params.WinnerPrize2Enabled = Me.chk_enable_winner_2.Checked
    _cdc_params.WinnerPrize2Probability = IIf(String.IsNullOrEmpty(Me.ef_probability_2.Value), 0, Me.ef_probability_2.Value)

    _cdc_params.WinnerPrize3Fixed = IIf(String.IsNullOrEmpty(Me.ef_fixed_3.Value), 0, Me.ef_fixed_3.Value)
    _cdc_params.WinnerPrize3Percentage = IIf(String.IsNullOrEmpty(Me.ef_perc_3.Value), 0, Me.ef_perc_3.Value)
    _cdc_params.WinnerPrize3Enabled = Me.chk_enable_winner_3.Checked
    _cdc_params.WinnerPrize3Probability = IIf(String.IsNullOrEmpty(Me.ef_probability_3.Value), 0, Me.ef_probability_3.Value)

    _cdc_params.WinnerPrize4Fixed = IIf(String.IsNullOrEmpty(Me.ef_fixed_4.Value), 0, Me.ef_fixed_4.Value)
    _cdc_params.WinnerPrize4Percentage = IIf(String.IsNullOrEmpty(Me.ef_perc_4.Value), 0, Me.ef_perc_4.Value)
    _cdc_params.WinnerPrize4Enabled = Me.chk_enable_winner_4.Checked
    _cdc_params.WinnerPrize4Probability = IIf(String.IsNullOrEmpty(Me.ef_probability_4.Value), 0, Me.ef_probability_4.Value)

    _cdc_params.WinnerPrize1CreditType = Me.cmb_prize1_credit_type.Value
    _cdc_params.WinnerPrize2CreditType = Me.cmb_prize2_credit_type.Value
    _cdc_params.WinnerPrize3CreditType = Me.cmb_prize3_credit_type.Value
    _cdc_params.WinnerPrize4CreditType = Me.cmb_prize4_credit_type.Value
    _cdc_params.LoserCreditType = Me.cmb_loser_credit_type.Value

    _cdc_params.PrizeMode = Me.cmb_prize_mode.Value
    _cdc_params.ParticipationMaxPrice = IIf(String.IsNullOrEmpty(Me.ef_participation_max_price.Value), 0, Me.ef_participation_max_price.Value)
    _cdc_params.TerminalGameTimeout = IIf(String.IsNullOrEmpty(Me.ef_terminal_game_timeout.Value), 0, Me.ef_terminal_game_timeout.Value)
    _cdc_params.GroupDraws = IIf(String.IsNullOrEmpty(Me.ef_terminal_game_draws.Value), 0, Me.ef_terminal_game_draws.Value)
    _cdc_params.TerminalGameUrl = Me.ef_terminal_game_url.TextValue
    _cdc_params.TerminalGameName = Me._ef_terminal_game_name.TextValue
    If (_cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.CashPrize Or
        _cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.Terminal Or
        _cdc_params.PrizeMode = CashDeskDrawBusinessLogic.PrizeMode.PlayerSession) Then
      _cdc_params.ParticipationPrice = IIf(String.IsNullOrEmpty(Me.ef_participation_price.Value), 0, Me.ef_participation_price.Value)
    Else
      _cdc_params.ParticipationPrice = 0
    End If

    _cdc_params.CancelWithPending = Me.chk_allow_cancel_recharge.Checked
    _cdc_params.WithdrawalWithPending = Me.chk_allow_withdraw_pending.Checked


    _cdc_params.ShowBuyDialog = Me.chk_show_buy_dialog.Checked
    _cdc_params.PlayerCanCancelDraw = Me.chk_player_can_cancel_draw.Checked

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _cdc_params As CLASS_CASHDESK_DRAWS_CONFIGURATION

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message already shown, do not show again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_CASHDESK_DRAWS_CONFIGURATION(m_draw_configuration)
        _cdc_params = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If DbStatus <> ENUM_STATUS.STATUS_OK Then
          DbStatus = ENUM_STATUS.STATUS_OK
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Checks if the entry data are ok 
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  ' 
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    'Only make validations if Cashdesk Draws are enabled
    If Not Me.chk_enable_draws.Checked Then
      Return True

    End If

    If ef_terminal_game_draws.Visible AndAlso (String.IsNullOrEmpty(ef_terminal_game_draws.Value) OrElse ef_terminal_game_draws.Value < 1 OrElse ef_terminal_game_draws.Value > 20) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8773), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , "1", "20")
      Me.ef_terminal_game_draws.Focus()

      Return False
    End If

    If Not Me.ef_extracted_balls.ValidateFormat() Or _
       String.IsNullOrEmpty(Me.ef_extracted_balls.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_extracted_balls.Text)
      Me.ef_extracted_balls.Focus()

      Return False

    End If

    If Not Me.ef_balls_participant.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_balls_participant.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_balls_participant.Text)
      Me.ef_balls_participant.Focus()

      Return False

    End If

    'Balls per paticipant must be greater than 0
    If Not CType(Me.ef_balls_participant.Value, Integer) > 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_balls_participant.Text)
      Me.ef_balls_participant.Focus()

      Return False

    End If

    If Not Me.ef_total_balls.ValidateFormat() Or _
      String.IsNullOrEmpty(Me.ef_total_balls.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_total_balls.Text)
      Me.ef_total_balls.Focus()

      Return False

    End If

    'If the "webservice" option is checked, at least one of the addresses must be writed
    If Me.panel_webservice.Visible Then
      If Me.rb_opt_webservice.Checked Then
        If String.IsNullOrEmpty(Me.ef_center_address_1.Value) AndAlso _
        String.IsNullOrEmpty(Me.ef_center_address_2.Value) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , Me.lbl_center_address_1.Text)
          Call Me.ef_center_address_1.Focus()

          Return False
        End If

        If Me.ef_center_address_1.Value = "" Then
          '1786 "Field %1 is empty.  Continue anyway?"
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1786), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , Me.lbl_center_address_1.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Call Me.ef_center_address_1.Focus()

            Return False
          End If
        End If

        If Me.ef_center_address_2.Value = "" Then
          '1786 "Field %1 is empty.  Continue anyway?"
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1786), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , Me.lbl_center_address_2.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Call Me.ef_center_address_2.Focus()

            Return False
          End If
        End If

      End If

    End If


    'If the "save on external database" option is checked, the Connection String related fields can't be empty    
    If Me.rb_opt_ext_db.Checked Then

      'Server 1 and Server 2 can't be empty at the same time
      If String.IsNullOrEmpty(Me.ef_extdb_server_1.Value) AndAlso String.IsNullOrEmpty(Me.ef_extdb_server_2.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , Me.ef_extdb_server_1.Text)
        Me.ef_extdb_server_1.Focus()

        Return False

      End If

      'Database can't be empty
      If String.IsNullOrEmpty(Me.ef_extdb_database.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_extdb_database.Text)
        Me.ef_extdb_database.Focus()

        Return False

      End If

      'User can't be empty
      If String.IsNullOrEmpty(Me.ef_extdb_user.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_extdb_user.Text)
        Me.ef_extdb_user.Focus()

        Return False

      End If

      'Password can't be empty
      If String.IsNullOrEmpty(Me.ef_extdb_pwd.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_extdb_pwd.Text)
        Me.ef_extdb_pwd.Focus()

        Return False

      End If

    End If

    'Extracted balls must be lower than total balls
    If Not (CType(Me.ef_extracted_balls.Value, Long) < CType(Me.ef_total_balls.Value, Long)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2316), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_extracted_balls.Text, Me.ef_total_balls.Text)
      Me.ef_extracted_balls.Focus()

      Return False

    End If

    'Balls per participant must be lower than extracted balls
    If Not (CType(Me.ef_balls_participant.Value, Long) < CType(Me.ef_extracted_balls.Value, Long)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2316), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_balls_participant.Text, Me.ef_extracted_balls.Text)
      Me.ef_balls_participant.Focus()

      Return False

    End If

    'Number of winners must be lower or equal than number of participants
    If (CType(Me.ef_winners.Value, Integer) > CType(Me.ef_participants.Value, Integer)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(338), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_winners.Text, Me.ef_participants.Text)
      Me.ef_winners.Focus()

      Return False

    End If

    'At least one of the prizes must be enabled
    If Not Me.chk_enable_winner_1.Checked AndAlso _
       Not Me.chk_enable_winner_2.Checked AndAlso _
       Not Me.chk_enable_winner_3.Checked AndAlso _
       Not Me.chk_enable_winner_4.Checked Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2551), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , )
      Me.chk_enable_winner_1.Focus()

      Return False

    End If

    If Not Me.ef_participants.ValidateFormat() Or _
       String.IsNullOrEmpty(Me.ef_participants.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_participants.Text)
      Me.ef_participants.Focus()

      Return False

    End If

    If Not Me.ef_winners.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_winners.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_winners.Text)
      Me.ef_winners.Focus()

      Return False

    End If

    If Not Me.ef_fixed_1.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_fixed_1.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_1.Focus()

      Return False

    End If

    If Not Me.ef_perc_1.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_perc_1.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text)
      Me.ef_perc_1.Focus()

      Return False

    End If

    If Not Me.ef_fixed_2.ValidateFormat() Or _
           String.IsNullOrEmpty(Me.ef_fixed_2.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_2.Focus()

      Return False

    End If

    If Not Me.ef_perc_2.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_perc_2.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text)
      Me.ef_perc_2.Focus()

      Return False

    End If

    If Not Me.ef_fixed_3.ValidateFormat() Or _
           String.IsNullOrEmpty(Me.ef_fixed_3.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_3.Focus()

      Return False

    End If

    If Not Me.ef_perc_3.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_perc_3.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text)
      Me.ef_perc_3.Focus()

      Return False

    End If

    If Not Me.ef_fixed_4.ValidateFormat() Or _
       String.IsNullOrEmpty(Me.ef_fixed_4.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_4.Focus()

      Return False

    End If

    If Not Me.ef_perc_4.ValidateFormat() Or _
          String.IsNullOrEmpty(Me.ef_perc_4.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text)
      Me.ef_perc_4.Focus()

      Return False

    End If

    If Not Me.ef_cprize_fixed.ValidateFormat() Or _
      String.IsNullOrEmpty(Me.ef_cprize_fixed.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_cprize_fixed.Focus()

      Return False

    End If

    If Not Me.ef_cprize_percentage.ValidateFormat() Or _
       String.IsNullOrEmpty(Me.ef_cprize_percentage.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text)
      Me.ef_cprize_percentage.Focus()

      Return False

    End If

    'The percentages must be <=100 and >=-100

    If (Not String.IsNullOrEmpty(Me.ef_cprize_percentage.Value) AndAlso (Me.ef_cprize_percentage.Value > MAX_PERCENTAGE Or Me.ef_cprize_percentage.Value < MIN_PERCENTAGE)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text, MIN_PERCENTAGE, MAX_PERCENTAGE)
      Me.ef_cprize_percentage.Focus()

      Return False

    End If

    If (Not String.IsNullOrEmpty(Me.ef_perc_1.Value) AndAlso (Me.ef_perc_1.Value > MAX_PERCENTAGE Or Me.ef_perc_1.Value < MIN_PERCENTAGE)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text, MIN_PERCENTAGE, MAX_PERCENTAGE)
      Me.ef_perc_1.Focus()

      Return False

    End If

    If (Not String.IsNullOrEmpty(Me.ef_perc_2.Value) AndAlso (Me.ef_perc_2.Value > MAX_PERCENTAGE Or Me.ef_perc_2.Value < MIN_PERCENTAGE)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text, MIN_PERCENTAGE, MAX_PERCENTAGE)
      Me.ef_perc_2.Focus()

      Return False

    End If

    If (Not String.IsNullOrEmpty(Me.ef_perc_3.Value) AndAlso (Me.ef_perc_3.Value > MAX_PERCENTAGE Or Me.ef_perc_3.Value < MIN_PERCENTAGE)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text, MIN_PERCENTAGE, MAX_PERCENTAGE)
      Me.ef_perc_3.Focus()

      Return False

    End If

    If (Not String.IsNullOrEmpty(Me.ef_perc_4.Value) AndAlso (Me.ef_perc_4.Value > MAX_PERCENTAGE Or Me.ef_perc_4.Value < MIN_PERCENTAGE)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_percentage.Text, MIN_PERCENTAGE, MAX_PERCENTAGE)
      Me.ef_perc_4.Focus()

      Return False

    End If

    'The sum of percentage and acces percentage can't be a negative number
    If (Me.chk_enable_winner_1.Checked And ((CType(Me.ef_perc_1.Value, Double) + CType(Me.ef_access_perc_winner_1.Value, Double)) < 0)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2528), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_access_perc.Text, Me.lbl_prizes_percentage.Text)
      Me.ef_perc_1.Focus()

      Return False

    End If

    If (Me.chk_enable_winner_2.Checked And ((CType(Me.ef_perc_2.Value, Double) + CType(Me.ef_access_perc_winner_2.Value, Double)) < 0)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2528), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_access_perc.Text, Me.lbl_prizes_percentage.Text)
      Me.ef_perc_2.Focus()

      Return False

    End If

    If (Me.chk_enable_winner_3.Checked And ((CType(Me.ef_perc_3.Value, Double) + CType(Me.ef_access_perc_winner_3.Value, Double)) < 0)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2528), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_access_perc.Text, Me.lbl_prizes_percentage.Text)
      Me.ef_perc_3.Focus()

      Return False

    End If

    If (Me.chk_enable_winner_4.Checked And ((CType(Me.ef_perc_4.Value, Double) + CType(Me.ef_access_perc_winner_4.Value, Double)) < 0)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2528), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_access_perc.Text, Me.lbl_prizes_percentage.Text)
      Me.ef_perc_4.Focus()

      Return False

    End If

    If (Me.chk_enable_loser.Checked And ((CType(Me.ef_cprize_percentage.Value, Double) + CType(Me.ef_access_perc_loser.Value, Double)) < 0)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2528), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_access_perc.Text, Me.lbl_prizes_percentage.Text)
      Me.ef_cprize_percentage.Focus()

      Return False

    End If

    'When the sum of percentage and acces percentage is 0, the fixed must be greater than 0
    If (Me.chk_enable_winner_1.Checked And _
       ((CType(Me.ef_perc_1.Value, Double) + CType(Me.ef_access_perc_winner_1.Value, Double)) = 0) And _
        Me.ef_fixed_1.Value <= 0) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_1.Focus()

      Return False

    End If

    If (Me.chk_enable_winner_2.Checked And _
      ((CType(Me.ef_perc_2.Value, Double) + CType(Me.ef_access_perc_winner_2.Value, Double)) = 0) And _
       Me.ef_fixed_2.Value <= 0) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_2.Focus()

      Return False

    End If

    If (Me.chk_enable_winner_3.Checked And _
      ((CType(Me.ef_perc_3.Value, Double) + CType(Me.ef_access_perc_winner_3.Value, Double)) = 0) And _
       Me.ef_fixed_3.Value <= 0) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_3.Focus()

      Return False

    End If

    If (Me.chk_enable_winner_4.Checked And _
      ((CType(Me.ef_perc_4.Value, Double) + CType(Me.ef_access_perc_winner_4.Value, Double)) = 0) And _
       Me.ef_fixed_4.Value <= 0) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_fixed_4.Focus()

      Return False

    End If

    If (Me.chk_enable_loser.Checked And _
      ((CType(Me.ef_cprize_percentage.Value, Double) + CType(Me.ef_access_perc_loser.Value, Double)) = 0) And _
       Me.ef_cprize_fixed.Value < 0) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_fixed.Text)
      Me.ef_cprize_fixed.Focus()

      Return False

    End If

    If Me.ef_participation_price.Visible Then
      If Not Me.ef_participation_price.ValidateFormat() OrElse String.IsNullOrEmpty(Me.ef_participation_price.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_participation_price.Text)
        Call Me.ef_participation_price.Focus()
        Return False

      End If

    End If

    If Me.ef_probability_1.Enabled Then
      If Not Me.ef_probability_1.ValidateFormat() OrElse String.IsNullOrEmpty(Me.ef_probability_1.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_probability.Text)
        Call Me.ef_probability_1.Focus()
        Return False

      End If

    End If

    If Me.ef_probability_2.Enabled Then
      If Not Me.ef_probability_2.ValidateFormat() OrElse String.IsNullOrEmpty(Me.ef_probability_2.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_probability.Text)
        Call Me.ef_probability_2.Focus()
        Return False

      End If

    End If

    If Me.ef_probability_3.Enabled Then
      If Not Me.ef_probability_3.ValidateFormat() OrElse String.IsNullOrEmpty(Me.ef_probability_3.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_probability.Text)
        Call Me.ef_probability_3.Focus()
        Return False

      End If

    End If

    If Me.ef_probability_4.Enabled Then
      If Not Me.ef_probability_4.ValidateFormat() OrElse String.IsNullOrEmpty(Me.ef_probability_4.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_probability.Text)
        Call Me.ef_probability_4.Focus()
        Return False

      End If

    End If

    If Me.ef_probability_5.Enabled Then
      If Not Me.ef_probability_5.ValidateFormat() OrElse String.IsNullOrEmpty(Me.ef_probability_5.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_probability.Text)
        Call Me.ef_probability_5.Focus()
        Return False

      End If

    End If

    If Not Me.ValidateSumProbabilitys() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7624), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return False

    End If

    'Validate prizes credit types
    If Not ValidateCreditTypes() Then
      Return False

    End If

    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then

      If (Me.ef_fixed_1.ValidateFormat() Or _
          Not String.IsNullOrEmpty(Me.ef_fixed_1.Value)) And Me.ef_fixed_1.Enabled = True Then

        If (Convert.ToDecimal(Me.ef_fixed_1.Value) - Math.Round(Convert.ToDecimal(Me.ef_fixed_1.Value))) <> 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7625), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.lbl_prizes_fixed.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Me.ef_fixed_1.Focus()
            Return False
          End If
        End If

      End If

      If (Me.ef_fixed_2.ValidateFormat() Or _
            Not String.IsNullOrEmpty(Me.ef_fixed_2.Value)) And Me.ef_fixed_2.Enabled = True Then

        If (Convert.ToDecimal(Me.ef_fixed_2.Value) - Math.Round(Convert.ToDecimal(Me.ef_fixed_2.Value))) <> 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7625), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.lbl_prizes_fixed.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Me.ef_fixed_2.Focus()
            Return False
          End If
        End If

      End If

      If (Me.ef_fixed_3.ValidateFormat() Or _
            Not String.IsNullOrEmpty(Me.ef_fixed_3.Value)) And Me.ef_fixed_3.Enabled = True Then

        If (Convert.ToDecimal(Me.ef_fixed_3.Value) - Math.Round(Convert.ToDecimal(Me.ef_fixed_3.Value))) <> 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7625), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.lbl_prizes_fixed.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Me.ef_fixed_3.Focus()
            Return False
          End If
        End If

      End If

      If (Me.ef_fixed_4.ValidateFormat() Or _
            Not String.IsNullOrEmpty(Me.ef_fixed_4.Value)) And Me.ef_fixed_4.Enabled = True Then

        If (Convert.ToDecimal(Me.ef_fixed_4.Value) - Math.Round(Convert.ToDecimal(Me.ef_fixed_4.Value))) <> 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7625), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.lbl_prizes_fixed.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Me.ef_fixed_4.Focus()
            Return False
          End If
        End If

      End If

      If (Me.ef_cprize_fixed.ValidateFormat() Or _
            Not String.IsNullOrEmpty(Me.ef_cprize_fixed.Value)) And Me.ef_cprize_fixed.Enabled = True Then

        If (Convert.ToDecimal(Me.ef_cprize_fixed.Value) - Math.Round(Convert.ToDecimal(Me.ef_cprize_fixed.Value))) <> 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7625), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.lbl_prizes_fixed.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Me.ef_cprize_fixed.Focus()
            Return False
          End If
        End If

      End If

    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region "Public Functions"

#End Region ' Public Functions

#Region "Private Functions"

  ' PURPOSE: Calculates the value of the recharge example based on the imput parameters
  '
  '  PARAMS:
  '     - INPUT:
  '           - RechargeValue
  '           - PriceFixed 
  '           - PricePercentage
  '           - AccessPercentageValue
  '     - OUTPUT:
  '           - Result of the recharge example value
  '
  ' RETURNS:
  '     - none
  Private Function GetExampleValue(ByVal RechargeValue As String, ByVal PriceFixed As String, ByVal PricePercentage As String, ByVal AccessPercentageValue As String, ByVal TaxProvisionsPercentage As String, ByVal DrawPrice As String) As String
    Dim _recharge As Double
    Dim _recharge_A As Double
    Dim _prize_fixed As Double
    Dim _prize_perc As Double
    Dim _access_perc As Double
    Dim _tax_provisions_perc As Double
    Dim _draw_prize As Double
    Dim _max_prize As Double

    If Not Double.TryParse(RechargeValue, _recharge) Or _
       Not Double.TryParse(PriceFixed, _prize_fixed) Or _
       Not Double.TryParse(PricePercentage, _prize_perc) Or _
       Not Double.TryParse(AccessPercentageValue, _access_perc) Or _
       Not Double.TryParse(TaxProvisionsPercentage, _tax_provisions_perc) Or _
       Not Double.TryParse(DrawPrice, _draw_prize) Then

      Return ""
    End If

    ' Promotion dont have draw prize
    If (Me.cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.Promotion) Then
      _draw_prize = 0
    End If

    _recharge_A = _recharge - _access_perc

    If cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.Terminal Then
      Double.TryParse(ef_participation_max_price.Value, _max_prize)
      _draw_prize = Math.Min((RECHARGE_VALUE / 100) * _draw_prize, _max_prize)
    End If
    Return GUI_FormatCurrency((_recharge * (_access_perc + _prize_perc) / 100) + _prize_fixed + _draw_prize + (_recharge_A * _tax_provisions_perc / 100), 2)


  End Function

  ''' <summary>
  ''' Update all winner examples
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub UpdateWinners()

    Call UpdateWinnerExample1()
    Call UpdateWinnerExample2()
    Call UpdateWinnerExample3()
    Call UpdateWinnerExample4()
    Call UpdateWinnerExampleLoser()

  End Sub ' UpdateWinners

  ' PURPOSE: Updates the value of the recharge example for the lbl_example_winner_1
  '
  '  PARAMS:
  '     - INPUT:
  '           - RechargeValue
  '           - PriceFixed 
  '           - PricePercentage
  '           - AccessPercentageValue
  '     - OUTPUT:
  '           - Result of the recharge example value
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateWinnerExample1()
    Dim _string_value As String

    _string_value = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_1.Value, Me.ef_perc_1.Value, Me.ef_access_perc_winner_1.Value, Me.ef_tax_provisions_1.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_1.Text = GUI_FormatCurrency(GUI_ParseCurrency(_string_value), 2)
  End Sub

  ' PURPOSE: Updates the value of the recharge example for the lbl_example_winner_2
  '
  '  PARAMS:
  '     - INPUT:
  '           - RechargeValue
  '           - PriceFixed 
  '           - PricePercentage
  '           - AccessPercentageValue
  '     - OUTPUT:
  '           - Result of the recharge example value
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateWinnerExample2()
    Dim _string_value As String

    _string_value = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_2.Value, Me.ef_perc_2.Value, Me.ef_access_perc_winner_2.Value, Me.ef_tax_provisions_2.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_2.Text = GUI_FormatCurrency(GUI_ParseCurrency(_string_value), 2)
  End Sub

  ' PURPOSE: Updates the value of the recharge example for the lbl_example_winner_3
  '
  '  PARAMS:
  '     - INPUT:
  '           - RechargeValue
  '           - PriceFixed 
  '           - PricePercentage
  '           - AccessPercentageValue
  '     - OUTPUT:
  '           - Result of the recharge example value
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateWinnerExample3()
    Dim _string_value As String

    _string_value = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_3.Value, Me.ef_perc_3.Value, Me.ef_access_perc_winner_3.Value, Me.ef_tax_provisions_3.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_3.Text = GUI_FormatCurrency(GUI_ParseCurrency(_string_value), 2)
  End Sub

  ' PURPOSE: Updates the value of the recharge example for the lbl_example_winner_4
  '
  '  PARAMS:
  '     - INPUT:
  '           - RechargeValue
  '           - PriceFixed 
  '           - PricePercentage
  '           - AccessPercentageValue
  '     - OUTPUT:
  '           - Result of the recharge example value
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateWinnerExample4()
    Dim _string_value As String

    _string_value = GetExampleValue(RECHARGE_VALUE, Me.ef_fixed_4.Value, Me.ef_perc_4.Value, Me.ef_access_perc_winner_4.Value, Me.ef_tax_provisions_4.Value, Me.ef_participation_price.Value)
    Me.lbl_example_winner_4.Text = GUI_FormatCurrency(GUI_ParseCurrency(_string_value), 2)

  End Sub

  ' PURPOSE: Updates the value of the recharge example for the lbl_example_loser
  '
  '  PARAMS:
  '     - INPUT:
  '           - RechargeValue
  '           - PriceFixed 
  '           - PricePercentage
  '           - AccessPercentageValue
  '     - OUTPUT:
  '           - Result of the recharge example value
  '
  ' RETURNS:
  '     - none
  Private Sub UpdateWinnerExampleLoser()
    Dim _string_value As String

    _string_value = GetExampleValue(RECHARGE_VALUE, Me.ef_cprize_fixed.Value, Me.ef_cprize_percentage.Value, Me.ef_access_perc_loser.Value, Me.ef_tax_provisions_loser.Value, Me.ef_participation_price.Value)
    Me.lbl_example_loser.Text = GUI_FormatCurrency(GUI_ParseCurrency(_string_value), 2)

  End Sub

  ' PURPOSE: Loads the cmb_prize_mode
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:None
  Private Sub CmbPrizeLoad()

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then ' Sorteo en Mesas
      Me.cmb_prize_mode.Add(CashDeskDrawBusinessLogic.PrizeMode.PlayerSession, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2590)) 'Player session
    ElseIf m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_00 Then
      Me.cmb_prize_mode.Add(CashDeskDrawBusinessLogic.PrizeMode.Promotion, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2589)) 'Promotion
      Me.cmb_prize_mode.Add(CashDeskDrawBusinessLogic.PrizeMode.PlayerSession, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2590)) 'Player session

      If (Me.m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_00) Then
        Me.cmb_prize_mode.Add(CashDeskDrawBusinessLogic.PrizeMode.CashPrize, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7586)) 'Participation price)
      End If

    ElseIf m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 Then
      Me.cmb_prize_mode.Add(CashDeskDrawBusinessLogic.PrizeMode.Terminal, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7823)) 'Machine
    End If



  End Sub 'CmbPrizeLoad 

  ' PURPOSE: Loads the combo boxes of the prizes
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:None
  Private Sub CmbPrizesCreditTypesLoad()

    'LTC 22-SEP-2016
    Dim _types_dictionary As Dictionary(Of Integer, String)
    _types_dictionary = New Dictionary(Of Integer, String)

    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      _types_dictionary.Add(PrizeType.Redeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)) 'Redeemable

    Else
      _types_dictionary.Add(PrizeType.Redeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)) 'Redeemable

      ' RGR 21-SEP-2016
      If Me.cmb_prize_mode.Value <> CashDeskDrawBusinessLogic.PrizeMode.CashPrize Then
        _types_dictionary.Add(PrizeType.NoRedeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)) 'Non-Redeemable
      End If

      If Me.cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.Promotion And _
         Not IsHideUNR() Then
        _types_dictionary.Add(PrizeType.UNR, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2599)) 'In kind (UNR)       
      End If

      ' SDS 14-10-2015
      If Me.cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.Promotion Then
        _types_dictionary.Add(PrizeType.ReInKind, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6797)) 'In kind (RE)
      End If
    End If

    Me.cmb_prize1_credit_type.Clear()
    Me.cmb_prize2_credit_type.Clear()
    Me.cmb_prize3_credit_type.Clear()
    Me.cmb_prize4_credit_type.Clear()
    Me.cmb_prize1_credit_type.Add(_types_dictionary)
    Me.cmb_prize2_credit_type.Add(_types_dictionary)
    Me.cmb_prize3_credit_type.Add(_types_dictionary)
    Me.cmb_prize4_credit_type.Add(_types_dictionary)

  End Sub ' CmbPrizesCreditTypesLoad

  ' PURPOSE: Updates the content of a combo box depending of the value of the prize credit type
  '
  '  PARAMS:
  '     - INPUT: 
  '             -ComboBox: The combo box to update.
  '             -PrizeCreditTypeParam: The prize credit type.
  '     - OUTPUT: None
  '
  ' RETURNS:None
  Private Sub CmbPrizeCreditTypeUpdate(ByRef MyComboBox As GUI_Controls.uc_combo, ByVal PrizeCreditTypeParam As PrizeType)

    Select Case PrizeCreditTypeParam
      Case PrizeType.UNR
        If IsHideUNR() And _
           MyComboBox.Name <> Me.cmb_loser_credit_type.Name Then 'Loser prize never has 'In kind (UNR)' credit type

          'If general param 'Cashier-->Promotion.HideUNR' is "1" but the previous DB value is 'In kind(UNR)', must keep the old value
          MyComboBox.Add(PrizeCreditTypeParam, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2599)) 'In kind

        End If

        MyComboBox.Value = PrizeCreditTypeParam

      Case PrizeType.Redeemable
        MyComboBox.Value = PrizeCreditTypeParam

      Case PrizeType.NoRedeemable
        MyComboBox.Value = PrizeCreditTypeParam

        ' SDS 14-10-2015
      Case PrizeType.ReInKind
        MyComboBox.Value = PrizeCreditTypeParam

      Case Else
        MyComboBox.Value = 0

    End Select

    If MyComboBox.Value <> PrizeCreditTypeParam Then
      MyComboBox.Value = PrizeType.Redeemable

    End If

  End Sub 'CmbPrizeCreditTypeUpdate

  ' PURPOSE: Loads the cmb_loser
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:None
  Private Sub CmbLoserCreditTypesLoad()

    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.cmb_loser_credit_type.Add(PrizeType.Redeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)) 'Redeemable
    Else
      Me.cmb_loser_credit_type.Add(PrizeType.Redeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)) 'Redeemable
      Me.cmb_loser_credit_type.Add(PrizeType.NoRedeemable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)) 'Non-Redeemable
    End If


  End Sub ' CmbPrizesCreditTypesLoad

  ' PURPOSE: Gets the name for a type of promotion
  '
  '  PARAMS:
  '     - INPUT: The promotion type
  '     - OUTPUT: The promotion name
  '
  ' RETURNS:None
  Private Function GetPromoName(ByVal PromoType As WSI.Common.Promotion.PROMOTION_TYPE)

    Dim _str_sql As StringBuilder
    Dim _data_table_promotions As DataTable
    Dim _value As String
    _value = ""

    ' Build query
    _str_sql = New System.Text.StringBuilder()
    _str_sql.AppendLine("SELECT PM_NAME")
    _str_sql.AppendLine("FROM PROMOTIONS")
    _str_sql.AppendLine("WHERE PM_TYPE=" & PromoType)

    ' Check query results
    _data_table_promotions = GUI_GetTableUsingSQL(_str_sql.ToString, 1)

    If IsNothing(_data_table_promotions) Then
      Return _value
    End If

    ' Check for rows received.
    If _data_table_promotions.Rows.Count() < 1 Then
      Return _value
    End If

    _value = _data_table_promotions.Rows(0).Item("PM_NAME")

    Return _value

  End Function 'GetPromoName

  ' PURPOSE: Initializes external data base related entry fields
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:None
  Private Sub InitExternalDbEntryFields()

    'Container group box
    Me.gb_ext_db_connection_string.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2473)
    Me.gb_ext_db_connection_string.Enabled = Me.rb_opt_ext_db.Checked

    'Server 1
    Me.ef_extdb_server_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2693)
    Me.ef_extdb_server_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    'Server 2
    Me.ef_extdb_server_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2694)
    Me.ef_extdb_server_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    'Data Base
    Me.ef_extdb_database.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2695)
    Me.ef_extdb_database.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    'User
    Me.ef_extdb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.ef_extdb_user.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    'Password
    Me.ef_extdb_pwd.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2697)
    Me.ef_extdb_pwd.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_extdb_pwd.Password = True

  End Sub 'InitDbEntryFields

  ' PURPOSE: Clear external data base related entry fields
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS:None
  Private Sub ClearExternalDbEntryFields()
    'Server 1
    Me.ef_extdb_server_1.Value = ""

    'Server 2
    Me.ef_extdb_server_2.Value = ""

    'Data Base
    Me.ef_extdb_database.Value = ""

    'User
    Me.ef_extdb_user.Value = ""

    'Password
    Me.ef_extdb_pwd.Value = ""

  End Sub 'ClearDbEntryFields

  ' PURPOSE: Gets the state of the general param 'Cashier--> Promotion.HideUNR'
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS: boolean indicating the state of the general param (0-->False, 1-->True)
  Private Function IsHideUNR() As Boolean
    Dim _general_params As GeneralParam.Dictionary

    _general_params = GeneralParam.Dictionary.Create()

    Return _general_params.GetBoolean("Cashier", "Promotion.HideUNR", True)

  End Function 'IsHideUNR

  ' PURPOSE: Validates the values of the prizes credit types combos
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS: Boolean indicating if all the combos passed (true) or not (false) the validation
  Private Function ValidateCreditTypes() As Boolean
    If IsHideUNR() Then
      If Me.chk_enable_winner_1.Checked And Me.cmb_prize1_credit_type.Value = PrizeType.UNR Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2526), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_nonredemable.Text, Me.cmb_prize1_credit_type.TextValue)
        Me.cmb_prize1_credit_type.Select()
        Me.cmb_prize1_credit_type.Focus()

        Return False

      End If

      If Me.chk_enable_winner_2.Checked And Me.cmb_prize2_credit_type.Value = PrizeType.UNR Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2526), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_nonredemable.Text, Me.cmb_prize2_credit_type.TextValue)
        Me.cmb_prize2_credit_type.Select()
        Me.cmb_prize2_credit_type.Focus()

        Return False

      End If

      If Me.chk_enable_winner_3.Checked And Me.cmb_prize3_credit_type.Value = PrizeType.UNR Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2526), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_nonredemable.Text, Me.cmb_prize3_credit_type.TextValue)
        Me.cmb_prize3_credit_type.Select()
        Me.cmb_prize3_credit_type.Focus()

        Return False

      End If

      If Me.chk_enable_winner_4.Checked And Me.cmb_prize4_credit_type.Value = PrizeType.UNR Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2526), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_nonredemable.Text, Me.cmb_prize4_credit_type.TextValue)
        Me.cmb_prize4_credit_type.Select()
        Me.cmb_prize4_credit_type.Focus()

        Return False

      End If

      If Me.chk_enable_loser.Checked And Me.cmb_loser_credit_type.Value = PrizeType.UNR Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2526), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.lbl_prizes_nonredemable.Text, Me.cmb_loser_credit_type.TextValue)
        Me.cmb_loser_credit_type.Select()
        Me.cmb_loser_credit_type.Focus()

        Return False

      End If

    End If

    Return True

  End Function 'ValidateCreditTypes


  ' PURPOSE: Validates the values of the active propabilitys
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS: Boolean indicating if all propabilitys sum 100% passed (true) or not (false) the validation

  ' PURPOSE: Validates the values of the active propabilitys
  '
  '  PARAMS:
  '     - INPUT: None
  '     - OUTPUT: None
  '
  ' RETURNS: Boolean indicating if all propabilitys sum 100% passed (true) or not (false) the validation
  Private Function ValidateSumProbabilitys() As Boolean
    Dim _sum As Decimal

    _sum = GetValidValue(ef_probability_1) +
      GetValidValue(ef_probability_2) +
      GetValidValue(ef_probability_3) +
      GetValidValue(ef_probability_4) +
      GetValidValue(ef_probability_5)

    Return _sum = 100
  End Function


#End Region ' Private Functions

#Region "Events"

  Private Sub chk_enable_draws_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_draws.CheckedChanged
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 And Not Me.chk_enable_draws.Checked Then

      ' Add warning a draw is pending
      '
      If DrawPending() Then
        RemoveHandler chk_enable_draws.CheckedChanged, AddressOf chk_enable_draws_CheckedChanged
        If Not NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7848), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
          Me.chk_enable_draws.Checked = True
        End If
        AddHandler chk_enable_draws.CheckedChanged, AddressOf chk_enable_draws_CheckedChanged
      End If
    End If
    Me.pnl_enable_draws.Enabled = Me.chk_enable_draws.Checked
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_02 Then
      ef_terminal_game_name.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_timeout.Enabled = Me.chk_enable_draws.Checked
      ef_terminal_game_draws.Enabled = Me.chk_enable_draws.Checked
      _ef_terminal_game_url.Enabled = Me.chk_enable_draws.Checked
      Me.chk_player_can_cancel_draw.Enabled = Me.chk_enable_draws.Checked
      Me.chk_show_buy_dialog.Enabled = Me.chk_enable_draws.Checked
      Me.chk_allow_withdraw_pending.Enabled = Me.chk_enable_draws.Checked
      Me.chk_allow_cancel_recharge.Enabled = Me.chk_enable_draws.Checked
    End If
    If _cmb_prize_mode.SelectedIndex < 0 Then
      _cmb_prize_mode.SelectedIndex = 0
    End If
    Me.UpdateSum()
  End Sub
  Private Function DrawPending() As Boolean
    Dim sb As StringBuilder = New StringBuilder()
    sb.AppendLine("select count(tdr_status) ")
    sb.AppendLine("   from terminal_draws_recharges ")
    sb.AppendLine(" WHERE  tdr_status=0")
    Dim table As DataTable
    Using _db_trx As New DB_TRX()
      table = GUI_GetTableUsingSQL(sb.ToString(), 5000, "terminal_draws_recharges", _db_trx.SqlTransaction)
    End Using
    If (table.Rows.Count = 1 And table.Rows(0).Item(0) > 0) Then
      Return True
    End If
    Return False
  End Function
  Private Sub rb_opt_Local_DB_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Me.panel_not_connected.Enabled = Not Me.rb_opt_Local_DB.Checked
    'If Not Me.rb_opt_Local_DB.Checked Then
    '  Me.rb_opt_ext_db.Checked = False

    'End If

    'Me.rb_opt_ext_db.Enabled = Not Me.rb_opt_Local_DB.Checked

    If Me.rb_opt_Local_DB.Checked Then
      Me.ef_center_address_1.Value = ""
      Me.ef_center_address_2.Value = ""

    End If

  End Sub

  Private Sub rb_opt_webservice_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Me.ef_center_address_1.Enabled = Me.rb_opt_webservice.Checked
    Me.ef_center_address_2.Enabled = Me.rb_opt_webservice.Checked
    Me.gb_ext_db_connection_string.Enabled = Me.rb_opt_ext_db.Checked

  End Sub

  Private Sub chk_enable_winner_1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_winner_1.CheckedChanged
    Me.ef_fixed_1.Enabled = Me.chk_enable_winner_1.Checked
    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_1.Enabled = False
    Else
      Me.ef_perc_1.Enabled = Me.chk_enable_winner_1.Checked
    End If

    Me.ef_access_perc_winner_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_tax_provisions_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.lbl_example_winner_1.Enabled = Me.chk_enable_winner_1.Checked
    Me.cmb_prize1_credit_type.Enabled = Me.chk_enable_winner_1.Checked
    Me.ef_probability_1.Enabled = Me.chk_enable_winner_1.Checked


    Call Me.UpdateSum()

  End Sub

  Private Sub chk_enable_winner_2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_winner_2.CheckedChanged
    Me.ef_fixed_2.Enabled = Me.chk_enable_winner_2.Checked
    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_2.Enabled = False
    Else
      Me.ef_perc_2.Enabled = Me.chk_enable_winner_2.Checked
    End If

    Me.ef_access_perc_winner_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.ef_tax_provisions_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.lbl_example_winner_2.Enabled = Me.chk_enable_winner_2.Checked
    Me.cmb_prize2_credit_type.Enabled = Me.chk_enable_winner_2.Checked
    Me.ef_probability_2.Enabled = Me.chk_enable_winner_2.Checked


    Call Me.UpdateSum()

  End Sub

  Private Sub chk_enable_winner_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_winner_3.CheckedChanged
    Me.ef_fixed_3.Enabled = Me.chk_enable_winner_3.Checked
    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_3.Enabled = False
    Else
      Me.ef_perc_3.Enabled = Me.chk_enable_winner_3.Checked
    End If

    Me.ef_access_perc_winner_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.ef_tax_provisions_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.lbl_example_winner_3.Enabled = Me.chk_enable_winner_3.Checked
    Me.cmb_prize3_credit_type.Enabled = Me.chk_enable_winner_3.Checked
    Me.ef_probability_3.Enabled = Me.chk_enable_winner_3.Checked

    Call Me.UpdateSum()

  End Sub

  Private Sub chk_enable_winner_4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_winner_4.CheckedChanged
    Me.ef_fixed_4.Enabled = Me.chk_enable_winner_4.Checked
    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_perc_4.Enabled = False
    Else
      Me.ef_perc_4.Enabled = Me.chk_enable_winner_4.Checked
    End If

    Me.ef_access_perc_winner_4.Enabled = Me.chk_enable_winner_4.Checked
    Me.ef_tax_provisions_4.Enabled = Me.chk_enable_winner_4.Checked
    Me.lbl_example_winner_4.Enabled = Me.chk_enable_winner_4.Checked
    Me.cmb_prize4_credit_type.Enabled = Me.chk_enable_winner_4.Checked
    Me.ef_probability_4.Enabled = Me.chk_enable_winner_4.Checked

    Call Me.UpdateSum()

  End Sub

  Private Sub chk_enable_loser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_loser.CheckedChanged
    Me.ef_cprize_fixed.Enabled = Me.chk_enable_loser.Checked
    'LTC 22-SEP-2016
    If m_draw_configuration = ENUM_DRAW_CONFIGURATION.DRAW_01 Then
      Me.ef_cprize_percentage.Enabled = False
    Else
      Me.ef_cprize_percentage.Enabled = Me.chk_enable_loser.Checked
    End If

    Me.ef_access_perc_loser.Enabled = Me.chk_enable_loser.Checked
    Me.ef_tax_provisions_loser.Enabled = Me.chk_enable_loser.Checked
    Me.lbl_example_loser.Enabled = Me.chk_enable_loser.Checked
    Me.cmb_loser_credit_type.Enabled = Me.chk_enable_loser.Checked
    Me.ef_probability_5.Enabled = Me.chk_enable_loser.Checked

    Call Me.UpdateSum()

  End Sub

  Private Sub ef_fixed_1_EntryFieldValueChanged() Handles ef_fixed_1.EntryFieldValueChanged, ef_perc_1.EntryFieldValueChanged

    Call UpdateWinnerExample1()

  End Sub


  Private Sub ef_fixed_2_EntryFieldValueChanged() Handles ef_fixed_2.EntryFieldValueChanged, ef_perc_2.EntryFieldValueChanged
    Call UpdateWinnerExample2()

  End Sub

  Private Sub ef_fixed_3_EntryFieldValueChanged() Handles ef_fixed_3.EntryFieldValueChanged, ef_perc_3.EntryFieldValueChanged
    Call UpdateWinnerExample3()

  End Sub

  Private Sub ef_fixed_4_EntryFieldValueChanged() Handles ef_fixed_4.EntryFieldValueChanged, ef_perc_4.EntryFieldValueChanged
    Call UpdateWinnerExample4()

  End Sub


  Private Sub ef_cprize_fixed_EntryFieldValueChanged() Handles ef_cprize_fixed.EntryFieldValueChanged, ef_cprize_percentage.EntryFieldValueChanged
    Call UpdateWinnerExampleLoser()

  End Sub

  Private Sub chk_print_voucher_winner_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_print_voucher_winner.CheckedChanged
    Me.ef_voucher_name_winner.Enabled = Me.chk_print_voucher_winner.Checked

  End Sub

  Private Sub chk_print_voucher_loser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_print_voucher_loser.CheckedChanged
    Me.ef_voucher_name_loser.Enabled = Me.chk_print_voucher_loser.Checked

  End Sub
  Private Sub ef_participation_price_EntryFieldValueChanged() Handles ef_participation_price.EntryFieldValueChanged, ef_participation_max_price.EntryFieldValueChanged

    Call UpdateWinners()

  End Sub

  Private Sub cmb_prize_mode_ValueChangedEvent() Handles cmb_prize_mode.ValueChangedEvent
    Dim _prize1_credit_type_old As Integer
    Dim _prize2_credit_type_old As Integer
    Dim _prize3_credit_type_old As Integer
    Dim _prize4_credit_type_old As Integer
    Dim _loser_credit_type_old As Integer

    'Save old values of the combos
    _prize1_credit_type_old = Me.cmb_prize1_credit_type.Value
    _prize2_credit_type_old = Me.cmb_prize2_credit_type.Value
    _prize3_credit_type_old = Me.cmb_prize3_credit_type.Value
    _prize4_credit_type_old = Me.cmb_prize4_credit_type.Value
    _loser_credit_type_old = Me.cmb_loser_credit_type.Value

    'Reload the combos
    CmbPrizesCreditTypesLoad()

    'Select the appropiate values of the combos depending of the old values: 
    ' -If the old value was 'In Kind(UNR) and now this vaue doesn't exists in the combo, select 'Redeemable' by default.
    ' -In other case, preserve the old value. 
    CmbPrizeCreditTypeUpdate(Me.cmb_prize1_credit_type, _prize1_credit_type_old)
    CmbPrizeCreditTypeUpdate(Me.cmb_prize2_credit_type, _prize2_credit_type_old)
    CmbPrizeCreditTypeUpdate(Me.cmb_prize3_credit_type, _prize3_credit_type_old)
    CmbPrizeCreditTypeUpdate(Me.cmb_prize4_credit_type, _prize4_credit_type_old)

    ef_participation_price.Visible = False
    ef_participation_price.Enabled = False
    ef_participation_max_price.Visible = False
    ef_participation_max_price.Enabled = False
    ef_terminal_game_timeout.Visible = False
    ef_terminal_game_timeout.Enabled = False
    ef_terminal_game_draws.Enabled = False
    ef_terminal_game_draws.Visible = False
    ef_terminal_game_url.Visible = False
    ef_terminal_game_url.Enabled = False
    ef_terminal_game_name.Visible = False
    ef_terminal_game_name.Enabled = False

    Call InitializePriceLocations()

    If (Me.cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.CashPrize Or Me.cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.PlayerSession) Then
      ef_participation_price.Visible = True
      ef_participation_price.Enabled = True

    ElseIf Me.cmb_prize_mode.Value = CashDeskDrawBusinessLogic.PrizeMode.Terminal Then
      ef_participation_price.Visible = True
      ef_participation_price.Enabled = True
      ef_participation_max_price.Visible = True
      ef_participation_max_price.Enabled = True
      ef_terminal_game_timeout.Visible = True ' TODO Visible = True
      ef_terminal_game_timeout.Enabled = True
      ef_terminal_game_draws.Enabled = True
      ef_terminal_game_draws.Visible = True
      ef_terminal_game_url.Visible = True ' TODO Visible = True
      ef_terminal_game_url.Enabled = True
      ef_terminal_game_name.Visible = True ' TODO Visible = True
      ef_terminal_game_name.Enabled = True
    End If

    Call UpdateWinners()

  End Sub

  Private Sub rb_opt_ext_db_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Me.gb_ext_db_connection_string.Enabled = Me.rb_opt_ext_db.Checked

    If (Not Me.gb_ext_db_connection_string.Enabled) Then
      ClearExternalDbEntryFields()

    End If
  End Sub

  Private Sub ef_probability_x_EntryFieldValueChanged() Handles ef_probability_1.EntryFieldValueChanged,
                                                                ef_probability_2.EntryFieldValueChanged,
                                                                ef_probability_3.EntryFieldValueChanged,
                                                                ef_probability_4.EntryFieldValueChanged,
                                                                ef_probability_5.EntryFieldValueChanged

    Call UpdateSum()
  End Sub

  Private Sub UpdateSum()
    Me.lbl_sum.Text = String.Format("{0} {1}", GUI_FormatNumber(UpdateSumCalc, 2), GLB_NLS_GUI_INVOICING.GetString(160))
  End Sub

  Private Function UpdateSumCalc() As Decimal
    Return GetValidValue(ef_probability_1) +
      GetValidValue(ef_probability_2) +
      GetValidValue(ef_probability_3) +
      GetValidValue(ef_probability_4) +
      GetValidValue(ef_probability_5)
  End Function


  Private Function GetValidValue(Control As uc_entry_field) As Decimal
    Return IIf(Control.Enabled AndAlso Not String.IsNullOrEmpty(Control.Value) AndAlso Control.ValidateFormat(), Control.Value, 0)
  End Function

  ''' <summary>
  ''' Control checkbox funcionality
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_show_buy_dialog_Click(sender As Object, e As EventArgs) Handles chk_show_buy_dialog.Click

    If (Not Me.chk_show_buy_dialog.Checked) Then

      Me.chk_player_can_cancel_draw.Enabled = False
      Me.chk_player_can_cancel_draw.Checked = False

    Else

      Me.chk_player_can_cancel_draw.Enabled = True

    End If

  End Sub


#End Region ' Events


  Private Sub SetMaxPriceLocation()

    Dim _point As Point
    Dim _cdc_params As CLASS_CASHDESK_DRAWS_CONFIGURATION
    _cdc_params = DbReadObject

    If m_max_price_position_isset Then
      Return
    End If

    m_max_price_position_isset = True

    ef_participation_price.Location = m_participation_positions(PRICE_POSITION_LEFT)
    ef_participation_max_price.Location = m_participation_positions(PRICE_MAX_POSITION_RIGHT)

    If Not _cdc_params.TaxProvisionsEnable Then

      _point = Me.ef_participation_max_price.Location
      _point.X -= 100
      Me.ef_participation_max_price.Location = _point
    End If

  End Sub

  Private Sub InitializePrizeControlsPositions()

    m_participation_positions(PRICE_POSITION_RIGHT) = New Point(742, 152)
    m_participation_positions(PRICE_MAX_POSITION_LEFT) = New Point(423, 152)
    m_participation_positions(PRICE_POSITION_LEFT) = New Point(423, 152)
    m_participation_positions(PRICE_MAX_POSITION_RIGHT) = New Point(742, 152)
  End Sub

  Private Sub InitializePriceLocations()

    Dim _point As Point
    Dim _cdc_params As CLASS_CASHDESK_DRAWS_CONFIGURATION
    _cdc_params = DbReadObject

    If m_price_position_isset Then
      Return
    End If

    m_price_position_isset = True

    ef_participation_price.Location = m_participation_positions(PRICE_POSITION_RIGHT)
    ef_participation_max_price.Location = m_participation_positions(PRICE_MAX_POSITION_LEFT)

    If Not _cdc_params.TaxProvisionsEnable Then
      _point = Me.ef_participation_price.Location
      _point.X -= 110
      Me.ef_participation_price.Location = _point

    End If
  End Sub

  Private Sub ef_terminal_game_url_Load(sender As Object, e As EventArgs) Handles ef_terminal_game_url.Load

  End Sub


End Class