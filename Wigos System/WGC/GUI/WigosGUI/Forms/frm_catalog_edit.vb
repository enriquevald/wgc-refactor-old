﻿'-------------------------------------------------------------------
' Copyright © 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_catalog_edit.vb
'
' DESCRIPTION : Window for edit catalogs and items of catalog
'               In this window can add and edit items to the selected catalog,
'               edit name, description and enabled or disabled the item or catalog.
'
' AUTHOR:        Sergio Daniel Soria
'
' CREATION DATE: 14-SEP-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-SEP-2015  SDS    Initial version   
' 04-NOV-2015  DLL    Bug 6174: Incorrect NLS
' 11-DEC-2015  GMV    Product Backlog Item 6857:Review Iteración 15: Catalogos
' 25-JAN-2016  RAB    Bug 7819:Errores varios en la pantalla de Edición de catálogos
' 09-FEB-2016  RAB    Bug 9088:Catálogos: Es posible añadir elementos a catálogos de sistema
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl

#End Region  ' Imports

Public Class frm_catalog_edit
  Inherits frm_base_edit
#Region " Constants "

  Private Const LEN_CATALOG_NAME As Integer = 50
  Private Const LEN_CATALOG_DESCRIPTION As Integer = 250


  ' Database codes
  Private Const CATALOG_STATUS_ENABLED As Integer = 0
  Private Const CATALOG_STATUS_DISABLED As Integer = 1

  Private Const CATALOG_TYPE_USER As Integer = 0
  Private Const CATALOG_TYPE_SYSTEM As Integer = 1
  Private Const CATALOG_TYPE_SYSTEM_SEALED As Integer = 2
  Private Const CATALOG_TYPE_SYSTEM_READONLY As Integer = 4

  ' Grid columns
  Private Const GRID_COLUMNS_ITEM As Integer = 5
  Private Const GRID_HEADER_ROWS_ITEM As Integer = 1

  Private Const GRID_COLUMN_ID = 0
  Private Const GRID_COLUMN_INDEX = 1
  Private Const GRID_COLUMN_VISIBLE = 2
  Private Const GRID_COLUMN_NAME = 3
  Private Const GRID_COLUMN_DESCRIPTION = 4

  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_INDEX As Integer = 0
  Private Const GRID_WIDTH_VISIBLE As Integer = 900
  Private Const GRID_WIDTH_NAME As Integer = 2000
  Private Const GRID_WIDTH_DESCRIPTION As Integer = 3200

  'RAB 25-JAN-2016
  Private Const ROW_HEIGHT_CATALOG_ITEMS As Integer = 240

#End Region

#Region " Members "

  Private m_catalog As CLS_CATALOG
  Private m_changed_values As Boolean
  Private m_new_count_id As Integer
  Private m_new_items As Stack



#End Region ' Members

#Region " Overrides "

  Public Overloads Sub ShowEditItem(Optional ByVal OperationId As Int64 = 0)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    If OperationId = 0 Then
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    End If

    Me.DbObjectId = OperationId


    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CATALOG_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button 
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6696)
      m_changed_values = False
      rb_catalog_enabled.Checked = True
    Else
      ef_catalog_name.TextVisible = True
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6695)
      m_changed_values = True
    End If

    'Catalog Name
    Me.ef_catalog_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    Me.ef_catalog_name.TextVisible = True
    Me.ef_catalog_name.Value = ""
    Me.ef_catalog_name.Focus()
    Me.ef_catalog_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATALOG_NAME)

    'Catalog Filter
    Me.btn_catalog_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6664)
    Me.ef_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)
    Me.ef_filter.TextVisible = True
    Me.ef_filter.Value = ""
    Me.ef_filter.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATALOG_NAME)


    'Catalog Description
    lbl_catalog_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)
    tb_description.MaxLength = LEN_CATALOG_DESCRIPTION

    'Catalog Status
    gb_catalog_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008)
    rb_catalog_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692)
    rb_catalog_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693)

    'Catalog Items
    gb_catalog_items.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6925)

    'Catalog item grid buttons
    ' Button Add / Delete rows to grid
    Me.btn_catalog_add.Enabled = Me.Permissions.Write
    Me.btn_catalog_del.Enabled = Me.Permissions.Write
    btn_catalog_add.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    btn_catalog_del.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1683)

    AddHandler dg_catalog_items.RowSelectedEvent, AddressOf dg_catalog_items_RowSelect
    AddHandler dg_catalog_items.CellDataChangedEvent, AddressOf dg_catalog_items_CellDataChange

    m_catalog.ReadCatalogItems()
    m_new_count_id = 0
    m_new_items = New Stack

    If (m_catalog.Type And CATALOG_TYPE_SYSTEM_SEALED) = CATALOG_TYPE_SYSTEM_SEALED Then
        Me.btn_catalog_add.Enabled = False
        Me.btn_catalog_del.Enabled = False
    End If

    If (m_catalog.Type And CATALOG_TYPE_SYSTEM_READONLY) = CATALOG_TYPE_SYSTEM_READONLY Then
        Me.btn_catalog_add.Enabled = False
        Me.btn_catalog_del.Enabled = False
        Me.gb_catalog_state.Enabled = False
        Me.tb_description.Enabled = False
        Me.ef_catalog_name.Enabled = False
    End If

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    m_catalog = Me.DbReadObject

    ' Name
    Me.ef_catalog_name.Value = m_catalog.Name

    ' Description
    Me.tb_description.Text = m_catalog.Description

    ' Enabled
    Me.rb_catalog_enabled.Checked = m_catalog.Enabled
    Me.rb_catalog_disabled.Checked = Not m_catalog.Enabled


    Call SetCatalogItems()

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _all_data_ok As Boolean
    Dim _num_catalog_checked As Integer
    m_catalog = DbEditedObject
    _all_data_ok = True
    _num_catalog_checked = 0

    ' Check title Catalog
    If Not Me.ef_catalog_name.Value.Length > 0 Then
      _all_data_ok = False
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6697), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)) ' "Field '%1' must not be left blank." param %1-> "Name"
    Else
      If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
        If m_catalog.ExistsCatalogName(Me.ef_catalog_name.Value) <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6698), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.ef_catalog_name.Value) ' "A catalog named %1 already exists."
          _all_data_ok = False
        End If
      End If
    End If



    ' Check at least one row in grid
    If _all_data_ok And Not Me.dg_catalog_items.NumRows > 0 Then
      _all_data_ok = False
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6699), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR) ' "You must add at least one element to the list."
    Else
      ' Check all data GRID
      For _idx_row As Integer = 0 To Me.dg_catalog_items.NumRows - 1

        ' Check Not Null rows
        If _all_data_ok And IsNullRow(_idx_row) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6700), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6702)) ' "You must enter a value for %1." Param $1-> "Value"
          _all_data_ok = False
          Exit For
        End If

        ' Check Not duplicate Names
        If _all_data_ok And Exist_Name(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value, _idx_row) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6701), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , )
          _all_data_ok = False
          Exit For
        End If
      Next
    End If

    Return _all_data_ok

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _catalog() As DataRow
    Try
      m_catalog = Me.DbEditedObject

      ' Name
      m_catalog.Name = Me.ef_catalog_name.Value

      ' Enabled
      m_catalog.Enabled = Me.rb_catalog_enabled.Checked

      ' Description
      m_catalog.Description = Me.tb_description.Text

      ' Grid Info
      If Not Me.dg_catalog_items Is Nothing AndAlso Me.dg_catalog_items.NumRows > 0 Then
        For _idx_row As Integer = 0 To Me.dg_catalog_items.NumRows - 1
          _catalog = m_catalog.CatalogItems.Select("CAI_ID = " & Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_ID).Value)

          If Not String.IsNullOrEmpty(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value) AndAlso _
             _catalog(0)(CLS_CATALOG.SQL_COLUMN_CAI_NAME) <> Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value Then
            _catalog(0)(CLS_CATALOG.SQL_COLUMN_CAI_NAME) = Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value
          End If

          '     If _catalog(0)(CLS_CATALOG.SQL_COLUMN_CAI_ENABLED) <> GridValueToBool(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value.ToString()) Then
          _catalog(0)(CLS_CATALOG.SQL_COLUMN_CAI_ENABLED) = GridValueToBool(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value)
          '      End If

          If Not String.IsNullOrEmpty(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value) AndAlso _
                          _catalog(0)(CLS_CATALOG.SQL_COLUMN_CAI_DESCRIPTION) <> Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value Then
            _catalog(0)(CLS_CATALOG.SQL_COLUMN_CAI_DESCRIPTION) = Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value
          End If

        Next

      End If
    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLS_CATALOG()
        m_catalog = DbEditedObject
        m_catalog.CatalogId = 0
        DbStatus = ENUM_STATUS.STATUS_OK
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Button overridable operations. 
  '          Define actions for each button are pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId


      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT
        If GUI_IsScreenDataOk() Then
          'Accept changes
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub  ' GUI_ButtonClick

#End Region '  Overrides 

#Region " Private Functions "

  ' PURPOSE: Define style for catalog grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub GUI_StyleSheet()

    With Me.dg_catalog_items
      Call .Init(GRID_COLUMNS_ITEM, GRID_HEADER_ROWS_ITEM, True)

      ' ID Empty
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Visible
      .Column(GRID_COLUMN_VISIBLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6694)
      .Column(GRID_COLUMN_VISIBLE).Width = GRID_WIDTH_VISIBLE
      .Column(GRID_COLUMN_VISIBLE).EditionControl.Type = CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_VISIBLE).Editable = (m_catalog.Type <> CATALOG_TYPE_SYSTEM_READONLY)
      .Column(GRID_COLUMN_VISIBLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Editable = (m_catalog.Type <> CATALOG_TYPE_SYSTEM_READONLY)
      .Column(GRID_COLUMN_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_NAME).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATALOG_NAME, 0)

      ' Description
      .Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_DESCRIPTION).Editable = (m_catalog.Type <> CATALOG_TYPE_SYSTEM_READONLY)
      .Column(GRID_COLUMN_DESCRIPTION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DESCRIPTION).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATALOG_DESCRIPTION, 0)



    End With
  End Sub ' GUI_StyleSheet


  ' PURPOSE: Set Catalog items with information in items datatable
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub SetCatalogItems()
    Dim _idx_row As Integer

    _idx_row = 0

    For Each _currency As DataRow In Me.m_catalog.CatalogItems.Rows
      dg_catalog_items.AddRow()

      _idx_row = dg_catalog_items.NumRows - 1

      dg_catalog_items.Cell(_idx_row, GRID_COLUMN_ID).Value = _currency(CLS_CATALOG.SQL_COLUMN_CAI_ID)
      dg_catalog_items.Cell(_idx_row, GRID_COLUMN_INDEX).Value = _currency(CLS_CATALOG.SQL_COLUMN_CAI_CAT_ID)

      If _currency(CLS_CATALOG.SQL_COLUMN_ENABLED) Then
        dg_catalog_items.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_CHECKED
      Else
        dg_catalog_items.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value = uc_grid.GRID_CHK_UNCHECKED
      End If

      dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value = _currency(CLS_CATALOG.SQL_COLUMN_CAI_NAME)
      dg_catalog_items.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value = _currency(CLS_CATALOG.SQL_COLUMN_CAI_DESCRIPTION)


    Next

    dg_catalog_items.Redraw = True
    If dg_catalog_items.NumRows > 0 Then
      dg_catalog_items.IsSelected(0) = True
    End If

  End Sub ' SetCatalogItems

  ' PURPOSE: Insert a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function InsertRow() As Integer

    Dim _idx_row As Integer
    Dim _str_new_catalog(4) As String

    _idx_row = dg_catalog_items.AddRow()
    _idx_row = dg_catalog_items.NumRows - 1
    m_new_count_id = m_new_count_id - 1

    dg_catalog_items.Cell(_idx_row, GRID_COLUMN_ID).Value = m_new_count_id
    dg_catalog_items.Cell(_idx_row, GRID_COLUMN_INDEX).Value = m_catalog.CatalogId
    dg_catalog_items.Cell(_idx_row, GRID_COLUMN_VISIBLE).Value = BoolToGridValue(False)
    dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value = String.Empty
    dg_catalog_items.Cell(_idx_row, GRID_COLUMN_DESCRIPTION).Value = String.Empty

    _str_new_catalog(CLS_CATALOG.SQL_COLUMN_CAI_ID) = m_new_count_id
    _str_new_catalog(CLS_CATALOG.SQL_COLUMN_CAI_CAT_ID) = m_catalog.CatalogId
    _str_new_catalog(CLS_CATALOG.SQL_COLUMN_CAI_ENABLED) = False
    _str_new_catalog(CLS_CATALOG.SQL_COLUMN_CAI_NAME) = String.Empty
    _str_new_catalog(CLS_CATALOG.SQL_COLUMN_CAI_DESCRIPTION) = String.Empty

    m_catalog = Me.DbEditedObject
    m_catalog.CatalogItems.Rows.Add(_str_new_catalog)

    Return _idx_row

  End Function ' InsertRow

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String

    Return IIf(enabled, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)

  End Function     ' BoolToGridValue

  ' PURPOSE: Delete a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row As Integer 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DeleteRow(ByVal _idx_row As Integer)

    Dim _dr_delete_catalog() As DataRow

    m_catalog = Me.DbEditedObject
    _dr_delete_catalog = m_catalog.CatalogItems.Select("CAI_ID = " & Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_ID).Value)

    _dr_delete_catalog(0).Delete()


    Me.dg_catalog_items.IsSelected(_idx_row) = False
    Me.dg_catalog_items.DeleteRowFast(_idx_row)


  End Sub ' DeleteRow

  ' PURPOSE:  Check row is null
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row AS Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Private Function IsNullRow(ByVal _idx_row As Integer) As Boolean

    If String.IsNullOrEmpty(dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value) Or dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value = "" Then
      Return True
    End If

  End Function ' IsNullRow

  ' PURPOSE: Verify that the name not exist.
  '
  '  PARAMS:
  '     - INPUT:
  '           - name select
  '     - OUTPUT:
  '           - true or false. if the name exist in grid.
  '
  ' RETURNS:
  '     - None
  Private Function Exist_Name(ByVal name As String, ByVal row As Integer) As Boolean
    Dim _return As Boolean

    _return = False

    For _idx_row As Integer = 0 To Me.dg_catalog_items.NumRows - 1

      If Trim(name) = Trim(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value) Then

        If row <> _idx_row Then

          _return = True
          Exit For

        End If

      End If

    Next

    '  End If

    Return _return

  End Function ' Exist_Name

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function ' GridValueToBool

#End Region ' private Functions

#Region "Events"

  Private Sub btn_catalog_add_ClickEvent() Handles btn_catalog_add.ClickEvent
    Dim _idx_row As Integer

    ' Check Permissions
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Exit Sub
    End If

    _idx_row = InsertRow()

    ' Focus on Row added
    dg_catalog_items.ClearSelection()
    dg_catalog_items.IsSelected(_idx_row) = True
    dg_catalog_items.Redraw = True
    dg_catalog_items.TopRow = _idx_row
    m_new_items.Push(_idx_row)

    Call GUI_GetScreenData()
  End Sub

  Private Sub btn_catalog_del_ClickEvent() Handles btn_catalog_del.ClickEvent
    Dim _idx_row As Integer

    ' Check Permissions
    If Not Permissions.Delete Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Exit Sub
    End If

    ' Check are selected a row
    If Not Me.dg_catalog_items.SelectedRows Is Nothing AndAlso Me.dg_catalog_items.SelectedRows.Length > 0 Then
      _idx_row = Me.dg_catalog_items.SelectedRows.GetValue(0)

      'Check are new item for delete
      If m_new_items.Contains(_idx_row) Then
        DeleteRow(_idx_row)
      End If

    End If

    ' Select previous Row
    If dg_catalog_items.NumRows > 0 Then
      If _idx_row > dg_catalog_items.NumRows - 1 Then
        _idx_row = dg_catalog_items.NumRows - 1
      End If

      dg_catalog_items.ClearSelection()
      dg_catalog_items.IsSelected(_idx_row) = True
      dg_catalog_items.Redraw = True
    End If

    Call GUI_GetScreenData()
  End Sub

  ' PURPOSE: Handler to control cells changes  in grid CatalogItems
  '           - Save Denomination value in hide column and apply format in visible denomination column  
  '
  '  PARAMS:
  '     - INPUT:
  '           - Index Row
  '           - Index Col
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_catalog_items_CellDataChange(ByVal Row As Integer, ByVal Column As Integer) Handles dg_catalog_items.CellDataChangedEvent

    RemoveHandler dg_catalog_items.CellDataChangedEvent, AddressOf dg_catalog_items_CellDataChange

    If dg_catalog_items Is Nothing OrElse dg_catalog_items.NumRows <= 0 Then
      Exit Sub
    End If

    dg_catalog_items.Cell(Row, GRID_COLUMN_NAME).Value = IIf(String.IsNullOrEmpty(dg_catalog_items.Cell(Row, GRID_COLUMN_NAME).Value), "", dg_catalog_items.Cell(Row, GRID_COLUMN_NAME).Value)
    dg_catalog_items.Cell(Row, GRID_COLUMN_DESCRIPTION).Value = IIf(String.IsNullOrEmpty(dg_catalog_items.Cell(Row, GRID_COLUMN_DESCRIPTION).Value), "", dg_catalog_items.Cell(Row, GRID_COLUMN_DESCRIPTION).Value)
    'dg_catalog_items.Cell(Row, GRID_COLUMN_VISIBLE).Value = GridValueToBool(dg_catalog_items.Cell(Row, GRID_COLUMN_VISIBLE).Value)


    Call GUI_GetScreenData()



    AddHandler dg_catalog_items.CellDataChangedEvent, AddressOf dg_catalog_items_CellDataChange
  End Sub ' dg_catalog_items_CellDataChange

  ' PURPOSE: Handler to control selected row in the grid
  '          - Unpaint column color to view the real color
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_catalog_items_RowSelect(ByVal SelectedRow As Integer) Handles dg_catalog_items.RowSelectedEvent

    If dg_catalog_items Is Nothing OrElse dg_catalog_items.NumRows <= 0 Then
      Exit Sub
    End If

    Try

      If SelectedRow >= 0 And dg_catalog_items.NumRows >= 0 Then

        dg_catalog_items.Redraw = False
        dg_catalog_items.Redraw = True

      End If
    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    End Try
  End Sub ' dg_catalog_items_RowSelect

  ' PURPOSE: Handler to control key pressed intro in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If dg_catalog_items.ContainsFocus Then
          dg_catalog_items.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function ' GUI_KeyPress


  Private Sub btn_catalog_filter_ClickEvent() Handles btn_catalog_filter.ClickEvent

    If Not String.IsNullOrEmpty(ef_filter.TextValue) Then

      If dg_catalog_items Is Nothing OrElse dg_catalog_items.NumRows <= 0 Then
        Exit Sub
      End If

      'RAB 25-JAN-2016
      If (Not EnabledAllCatalogItemsRows(ROW_HEIGHT_CATALOG_ITEMS)) Then
        Exit Sub
      End If

      If Not Me.dg_catalog_items Is Nothing AndAlso Me.dg_catalog_items.NumRows > 0 Then
        For _idx_row As Integer = 0 To Me.dg_catalog_items.NumRows - 1
          If Not String.IsNullOrEmpty(Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value) AndAlso _
              Not Me.dg_catalog_items.Cell(_idx_row, GRID_COLUMN_NAME).Value.ToUpper().Contains(ef_filter.TextValue.ToUpper()) Then

            Me.dg_catalog_items.Row(_idx_row).Height = 0

          End If
        Next

      End If

    Else
      If (Not EnabledAllCatalogItemsRows(ROW_HEIGHT_CATALOG_ITEMS)) Then
        Exit Sub
      End If
    End If
  End Sub

  ' PURPOSE: Enabled visibility catalog items rows
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowHeight: Height size of row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Succesfull enabled rows
  '     - False: Not succesfull enabled rows
  Private Function EnabledAllCatalogItemsRows(ByVal RowHeight As String) As Boolean
    Try
      For _idx_row As Integer = 0 To Me.dg_catalog_items.NumRows - 1
        Me.dg_catalog_items.Row(_idx_row).Height = RowHeight
      Next

      Return True
    Catch ex As Exception
      Return False
    End Try
  End Function

#End Region 'Events

End Class