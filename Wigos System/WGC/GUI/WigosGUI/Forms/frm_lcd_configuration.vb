'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_lcd_configuration.vb
' DESCRIPTION:   LCD Display configuration
' AUTHOR:        Jes�s �ngel Blanco
' CREATION DATE: 22-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author      Description
' -----------  ---------   ------------------------------------------
' 22-JUL-2014  JAB         Initial version.
' 30-JUL-2014  JAB         Add new tab to configure the initial video.
' 29-SEP-2014  OPC         Fixed Bug #1292: Change NLS, format bytes text.
' 15-OCT-2014  JAB         Fixed Bug #1513: Errors in the LCD Display configuration screen.
' 15-OCT-2014  JAB         Fixed Bug #1292: Inconsistencies in the LCD Display configuration screen.
' 17-OCT-2014  JAB         Fixed Bug #1522: Error when video is downloaded.
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.IO


Public Class frm_lcd_configuration
  Inherits GUI_Controls.frm_base_edit

#Region " Consts "

  Private Const FORM_DB_MIN_VERSION As Short = 224

  ' GRID FUNCTIONALITITES DEFINITIONS
  Private Const GRID_NUM_COL_FUNC As Integer = 3 ' NUMER OF COLUMNS
  Private Const GRID_COLUMN_FUNC_ID As Integer = 0
  Private Const GRID_COLUMN_FUNC_NAME As Integer = 1
  Private Const GRID_COLUMN_FUNC_ENABLED As Integer = 2

  Private Const GRID_FUNC_NAME_WIDTH As Integer = 3115
  Private Const GRID_CHECKED_COLUMN_WIDTH As Integer = 1040

  Private Const VIDEO_MAX_SIZE_ALLOWED As Int32 = 4718592 ' Size allowed: 4,5 MB

#End Region

#Region " Members "

  Private m_first_time As Boolean
  Private m_current_directory As String
  Private m_lcd_intro_video As IO.MemoryStream

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_LCD_CONFIGURATION
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5156)                       ' LCD Display - Configuraci�n

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.m_first_time = True
    Call img_preview.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)

    Me.gb_functionalities.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5158)      ' Funcionalidades / functionalities
    Me.gb_lcd_logo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5159)             ' Logo mostrado / shown logo
    Me.lb_img_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5160)      ' Tama�o recomendado 148x114 / Recommended size 148x114

    Me.gb_lcd_video.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5185)            ' Video de introducci�n
    Me.btn_change_intro_video.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5187)  ' LDC Video

    Me.lnk_video_preview.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(486)        ' Vista previa

    Call Me.GUI_StyleFunctionalities()

    ' CONFIGURE SAVE BUTTON
    With Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0)
      .Visible = True
      .Enabled = Permissions.Write
      .Text = GLB_NLS_GUI_CONTROLS.GetString(13) ' Label button save
    End With

  End Sub ' GUI_InitControls

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        Me.DbEditedObject = New CLS_LCD_CONFIGURATION
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        ' Mensaje si se da un error al actualizar
        'If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
        '  Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        'End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        ' Mensaje si se da un error al actualizar
        'If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
        '  Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        'End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Set data to screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _idx_row As Integer
    Dim _extension As String
    Dim _lcd_config_read As CLS_LCD_CONFIGURATION
    Dim _lcd_config_edited As CLS_LCD_CONFIGURATION
    Dim _image_resource_id As Int64
    Dim _video_resource_id As Int64
    Dim _resource_name As String
    Dim _video_size As String
    Dim _video_name As String
    Dim _ms As MemoryStream
    Dim _image_length As Int64
    Dim _image_cached As System.Drawing.Image
    Dim _video_cached As System.IO.MemoryStream
    Dim _video_length As Int64

    _lcd_config_read = DbReadObject
    _lcd_config_edited = DbEditedObject
    _extension = ""
    _resource_name = ""
    _image_resource_id = 0
    _video_resource_id = 0

    Try

      ' Fill functionalaties datagrid
      For Each _functionality As DataRow In _lcd_config_read.Functionalities.Rows
        _idx_row = Me.dg_functionalities.AddRow()
        With Me.dg_functionalities
          .Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value = _functionality(GRID_COLUMN_FUNC_ID)
          '.Cell(_idx_row, GRID_COLUMN_FUNC_NAME).Value = _functionality(GRID_COLUMN_FUNC_NAME)
          .Cell(_idx_row, GRID_COLUMN_FUNC_NAME).Value = _lcd_config_read.GetFunctionalityName(_functionality(GRID_COLUMN_FUNC_ID))
          .Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value = BoolToGridValue(_functionality(GRID_COLUMN_FUNC_ENABLED))

          If _functionality(GRID_COLUMN_FUNC_ID) = TYPE_LCD_FUNCTIONALITIES.ACCOUNT_INFO Then
            .Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          End If
        End With
      Next

      ' Load logo image
      _ms = New MemoryStream
      _image_cached = GetImage(_image_resource_id, _resource_name)
      If Not _image_cached Is Nothing Then
        _image_cached.Save(_ms, _image_cached.RawFormat)
        _image_length = Convert.ToInt32(_ms.Position)
        _lcd_config_read.Image.image_cached = _image_cached.Clone()
        _lcd_config_edited.Image.image_cached = _image_cached.Clone()
      Else
        _image_length = 0
        _lcd_config_read.Image.image_cached = _image_cached
        _lcd_config_edited.Image.image_cached = _image_cached
      End If

      _lcd_config_read.Image.image_length = _image_length
      _lcd_config_read.Image.image_name = _resource_name
      _lcd_config_read.Image.image_has_changes = False
      _lcd_config_read.Image.image_deleted = False
      _lcd_config_read.Image.resource_id = _image_resource_id
      _lcd_config_read.Image.resource_type_id = CLS_LCD_CONFIGURATION.RESOURCE_TYPE.IMAGE_LOGO

      _lcd_config_read.Image.image_length = _image_length
      _lcd_config_edited.Image.image_name = _resource_name
      _lcd_config_edited.Image.image_has_changes = False
      _lcd_config_edited.Image.image_deleted = False
      _lcd_config_edited.Image.resource_id = _image_resource_id
      _lcd_config_edited.Image.resource_type_id = CLS_LCD_CONFIGURATION.RESOURCE_TYPE.IMAGE_LOGO

      Me.img_preview.Image = _lcd_config_read.Image.image_cached
      Me.img_preview.ImageName = _resource_name

      ' Load intro video 
      _video_cached = New System.IO.MemoryStream()
      _video_cached = GetVideo(_video_resource_id, _extension, _resource_name)
      If Not _video_cached Is Nothing Then
        _video_length = _video_cached.Length
      Else
        _video_length = 0
      End If

      _lcd_config_read.Video.video_cached = _video_cached
      _lcd_config_read.Video.video_length = _video_length
      _lcd_config_read.Video.video_has_changes = False
      _lcd_config_read.Video.video_deleted = False
      _lcd_config_read.Video.resource_id = _video_resource_id
      _lcd_config_read.Video.video_name = _resource_name
      _lcd_config_read.Video.video_extension = _extension
      _lcd_config_read.Video.resource_type_id = CLS_LCD_CONFIGURATION.RESOURCE_TYPE.VIDEO_INTRO

      _lcd_config_edited.Video.video_cached = _video_cached
      _lcd_config_edited.Video.video_length = _video_length
      _lcd_config_edited.Video.video_has_changes = False
      _lcd_config_edited.Video.video_deleted = False
      _lcd_config_edited.Video.resource_id = _video_resource_id
      _lcd_config_edited.Video.video_name = _resource_name
      _lcd_config_edited.Video.video_extension = _extension
      _lcd_config_edited.Video.resource_type_id = CLS_LCD_CONFIGURATION.RESOURCE_TYPE.VIDEO_INTRO

      ' Insert row to disable video
      _idx_row = Me.dg_functionalities.AddRow()
      With Me.dg_functionalities
        .Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value = -1
        .Cell(_idx_row, GRID_COLUMN_FUNC_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5186)       ' Disable video
        .Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value = BoolToGridValue(_video_resource_id <> 0)
      End With

      If _lcd_config_read.Video.video_cached Is Nothing Then
        '_video_size = "(0 bytes)"
        _video_size = String.Empty
        lnk_video_preview.Enabled = False
      Else
        _video_size = "(" & GUI_FormatNumber(_lcd_config_read.Video.video_cached.Length, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE) & " bytes)"
        lnk_video_preview.Enabled = True
      End If

      _video_name = IIf(String.IsNullOrEmpty(_lcd_config_read.Video.video_name), AUDIT_NONE_STRING, _lcd_config_read.Video.video_name)

      lnk_video_preview.Text = _video_name & " " & _video_size
      btn_change_intro_video.Enabled = _video_resource_id <> 0

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      "Exception", _
                      "", _
                      "", _
                      "", _
                      _ex.Message)

    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim _lcd_config As CLS_LCD_CONFIGURATION
    Dim _idx_row As Integer
    Dim _ms As MemoryStream
    Dim _img_len As Int64
    Dim _image_cached As System.Drawing.Image

    _lcd_config = Me.DbEditedObject

    Try

      For _idx_row = 0 To Me.dg_functionalities.NumRows - 1
        With Me.dg_functionalities
          If .Cell(_idx_row, GRID_COLUMN_FUNC_ID).Value <> -1 Then
            If .Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value <> BoolToGridValue(_lcd_config.Functionalities.Rows(_idx_row).Item(GRID_COLUMN_FUNC_ENABLED)) Then
              _lcd_config.Functionalities.Rows(_idx_row).Item(GRID_COLUMN_FUNC_ENABLED) = GridValueToBool(.Cell(_idx_row, GRID_COLUMN_FUNC_ENABLED).Value)
            End If
          End If
        End With
      Next

      _image_cached = img_preview.Image
      If Not _image_cached Is Nothing Then
        _ms = New MemoryStream
        _image_cached.Save(_ms, _image_cached.RawFormat)
        _img_len = Convert.ToInt32(_ms.Position)
      Else
        _img_len = 0
      End If

      _lcd_config.Image.image_cached = _image_cached
      _lcd_config.Image.image_length = _img_len
      _lcd_config.Image.image_name = img_preview.ImageName
      _lcd_config.Image.resource_type_id = CLS_LCD_CONFIGURATION.RESOURCE_TYPE.IMAGE_LOGO

      If Not m_lcd_intro_video Is Nothing Then
        _lcd_config.Video.video_cached = m_lcd_intro_video
        _lcd_config.Video.video_length = m_lcd_intro_video.Length
      End If

      _lcd_config.Video.resource_type_id = CLS_LCD_CONFIGURATION.RESOURCE_TYPE.VIDEO_INTRO

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      "Exception", _
                      "", _
                      "", _
                      "", _
                      _ex.Message)

    End Try

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Handle Form buttons cliks
  '
  '  PARAMS:
  '     - INPUT: Clicked button object
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0

        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

        ' Trick!
        ' Restart images status
        ' Necessary to detect new changes in images and to audit image changes  
        ''For Each _db_obj As CLS_ADS_CONFIGURATION In New List(Of CLS_ADS_CONFIGURATION)(New CLS_ADS_CONFIGURATION() {DbReadObject, DbEditedObject})
        ''  For Each _image As CLS_ADS_CONFIGURATION.WKT_Image In _db_obj.Images
        ''    _image.image_cached = Nothing
        ''    _image.image_deleted = False
        ''    _image.image_has_changes = False
        ''  Next
        ''Next

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function 'GUI_IsScreenDataOk

#End Region

#Region " Events "

  ' PURPOSE : Change the intro video.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub btn_change_intro_video_ClickEvent() Handles btn_change_intro_video.ClickEvent
    Dim _filename As String
    Dim _selected_file As System.Windows.Forms.OpenFileDialog
    Dim _current_config As CLS_LCD_CONFIGURATION
    Dim _current_video As CLS_LCD_CONFIGURATION.LCD_Video
    Dim _video_size As String
    Dim _video_name As String

    _selected_file = New System.Windows.Forms.OpenFileDialog
    _current_config = Me.DbEditedObject

    If _current_config Is Nothing Then
      Exit Sub
    End If

    _current_video = _current_config.Video

    If Not String.IsNullOrEmpty(m_current_directory) Then
      _selected_file.InitialDirectory = m_current_directory
    End If

    _selected_file.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5181)      ' Seleccionar video
    _selected_file.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5182)     ' Video|*.avi;*.mpg;*.wmv;*,flv
    If _selected_file.ShowDialog() <> Windows.Forms.DialogResult.OK Then

      Return
    End If

    _filename = _selected_file.FileName
    m_current_directory = Path.GetDirectoryName(_filename)
    If Not _selected_file.Filter.ToUpper.Contains(Path.GetExtension(_filename).ToUpper) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5183), ENUM_MB_TYPE.MB_TYPE_INFO) ' Debe de seleccionar un archivo de video v�lido

      Call btn_change_intro_video_ClickEvent()
      Return
    End If

    Using _fileStream As FileStream = File.OpenRead(_filename)
      m_lcd_intro_video = New MemoryStream()
      m_lcd_intro_video.SetLength(_fileStream.Length)
      _fileStream.Read(m_lcd_intro_video.GetBuffer(), 0, CInt(Fix(_fileStream.Length)))

      If m_lcd_intro_video.Length > VIDEO_MAX_SIZE_ALLOWED Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5637), ENUM_MB_TYPE.MB_TYPE_ERROR) ' El tama�o del fichero supera el permitido
        Call btn_change_intro_video_ClickEvent()

        Return
      End If

      If m_lcd_intro_video.Length > 0 Then
        _current_video.video_name = _selected_file.SafeFileName
        _current_video.video_cached = m_lcd_intro_video
        _current_video.video_length = m_lcd_intro_video.Length
        _current_video.video_has_changes = True
        _current_video.video_deleted = False
        _current_video.video_extension = Path.GetExtension(_filename)
      End If

    End Using

    If _current_video.video_cached Is Nothing Then
      '_video_size = "(0 bytes)"
      _video_size = String.Empty
      lnk_video_preview.Enabled = False
    Else
      _video_size = "(" & GUI_FormatNumber(_current_video.video_cached.Length, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE) & " bytes)"
      lnk_video_preview.Enabled = True
    End If
    _video_name = IIf(String.IsNullOrEmpty(_current_video.video_name), AUDIT_NONE_STRING, _current_video.video_name)

    lnk_video_preview.Text = _video_name & _video_size

    Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5184), ENUM_MB_TYPE.MB_TYPE_INFO) ' El video se ha cargado correctamente

  End Sub ' btn_change_intro_video_ClickEvent

  ' PURPOSE : Disable the intro video.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub dg_functionalities_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_functionalities.CellDataChangedEvent
    Dim _disable_video As Boolean
    Dim _video_size As String
    Dim _video_name As String

    With dg_functionalities
      If .Cell(Row, GRID_COLUMN_FUNC_ID).Value = -1 Then
        _disable_video = Not GridValueToBool(.Cell(Row, GRID_COLUMN_FUNC_ENABLED).Value)
        btn_change_intro_video.Enabled = Not _disable_video

        If _disable_video Then
          Dim _current_config As CLS_LCD_CONFIGURATION
          Dim _current_video As CLS_LCD_CONFIGURATION.LCD_Video

          _current_config = Me.DbEditedObject
          If _current_config Is Nothing Then
            Exit Sub
          End If

          _current_video = _current_config.Video

          _current_video.video_cached = Nothing
          _current_video.video_length = 0
          _current_video.video_has_changes = True
          _current_video.video_deleted = True
          _current_video.video_extension = ""
          _current_video.resource_id = 0
          _current_video.video_name = ""

          '_video_size = "(0 bytes)"
          _video_size = String.Empty
          _video_name = AUDIT_NONE_STRING
          lnk_video_preview.Text = _video_name & _video_size
          lnk_video_preview.Enabled = False

        End If

      End If
    End With

  End Sub ' dg_functionalities_CellDataChangedEvent

  ' PURPOSE : Open video.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Sub lnk_video_preview_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnk_video_preview.LinkClicked
    Dim _path_file As String

    Try
      _path_file = GetPathFile()

      If Not String.IsNullOrEmpty(_path_file) Then
        Process.Start(_path_file)
      End If

    Catch _ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      "Exception", _
                      "", _
                      "", _
                      "", _
                      _ex.Message)

    End Try

  End Sub ' lnk_video_preview_LinkClicked

  ' PURPOSE : Save video file and preview.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Function GetPathFile() As String
    Dim _file_path As String
    Dim _lcd_config_edited As CLS_LCD_CONFIGURATION
    Dim _video_cached As System.IO.MemoryStream
    Dim _video_file As System.IO.FileStream

    _lcd_config_edited = DbEditedObject
    _video_cached = New System.IO.MemoryStream()
    _file_path = System.Environment.CurrentDirectory & Path.DirectorySeparatorChar & _lcd_config_edited.Video.video_name
    _video_file = New System.IO.FileStream(_file_path, FileMode.OpenOrCreate)

    _video_cached = _lcd_config_edited.Video.video_cached
    _video_cached.WriteTo(_video_file)

    _video_file.Close()

    Return _file_path
  End Function ' GetPathFile

#End Region

#Region " Private "

  ' PURPOSE:  Set the columns from Functionalities Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleFunctionalities()

    With Me.dg_functionalities
      Call .Init(GRID_NUM_COL_FUNC, 1, True)

      .Column(GRID_COLUMN_FUNC_ID).Width = 0

      ' Functionalities
      .Column(GRID_COLUMN_FUNC_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(756) ' Functionality
      .Column(GRID_COLUMN_FUNC_NAME).Width = GRID_FUNC_NAME_WIDTH

      ' Enabled
      .Column(GRID_COLUMN_FUNC_ENABLED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(757) ' Enabled
      .Column(GRID_COLUMN_FUNC_ENABLED).Width = GRID_CHECKED_COLUMN_WIDTH
      .Column(GRID_COLUMN_FUNC_ENABLED).Editable = True
      .Column(GRID_COLUMN_FUNC_ENABLED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

    End With

  End Sub ' GUI_StyleFunctionalities()

  ' PURPOSE: img_preview_ImageChanged
  '                  
  '  PARAMS:         
  '     - INPUT:     
  '           - 
  '     - OUTPUT:  
  '           -  OldImage As System.Drawing.Image
  '           -  NewImage As System.Drawing.Image 
  '                  
  ' RETURNS:         
  '     -None
  '     
  Private Sub img_preview_ImageChanged(ByVal OldImage As System.Drawing.Image, ByVal NewImage As System.Drawing.Image) Handles img_preview.ImageChanged
    Dim _current_config As CLS_LCD_CONFIGURATION
    Dim _current_image As CLS_LCD_CONFIGURATION.LCD_Image
    Dim _ms As MemoryStream
    Dim _img_len As Int64

    _current_config = Me.DbEditedObject

    If _current_config Is Nothing Then
      Exit Sub
    End If

    _current_image = _current_config.Image

    If m_first_time Then
      m_first_time = False

      Exit Sub
    End If

    If NewImage Is Nothing Then
      '
      ' Image Deleted
      '
      _current_image.image_cached = Nothing
      _current_image.image_length = 0
      _current_image.image_has_changes = True
      _current_image.image_deleted = True
      _current_image.resource_id = 0

    Else
      '
      ' Image Load or Updated
      '
      _ms = New MemoryStream
      NewImage.Save(_ms, NewImage.RawFormat)
      _img_len = Convert.ToInt32(_ms.Position)

      _current_image.image_cached = NewImage
      _current_image.image_length = _img_len
      _current_image.image_has_changes = True
      _current_image.image_deleted = False

    End If
  End Sub ' img_preview_ImageChanged

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal Enabled As Boolean) As String
    If (Enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function ' BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function GridValueToBool(ByVal Enabled As String) As Boolean

    Select Case (Enabled)
      Case uc_grid.GRID_CHK_CHECKED
        Return True

      Case uc_grid.GRID_CHK_CHECKED_DISABLED
        Return True

      Case uc_grid.GRID_CHK_UNCHECKED
        Return False

      Case Else
        Return False

    End Select

  End Function ' GridValueToBool

  ' PURPOSE:  Get image instance from Resource ID
  '
  '  PARAMS:
  '     - INPUT:
  '           - Integer : ResourceId 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Image object
  '
  Private Function GetImage(ByRef ResourceId As Int64, ByRef ResourceName As String) As Image

    Dim _sql_transaction As SqlClient.SqlTransaction
    Dim _image As Image
    _sql_transaction = Nothing
    _image = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _image = Nothing
      End If

      If Not WSI.Common.LCDResources.Load(_image, ResourceId, ResourceName, _sql_transaction) Then
        _image = Nothing
      End If

    Catch _ex As Exception
      _image = Nothing

      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      "Exception", _
                      "", _
                      "", _
                      "", _
                      _ex.Message)

    End Try

    Return _image

  End Function ' GetImage

  ' PURPOSE:  Get video instance from Resource ID
  '
  '  PARAMS:
  '     - INPUT:
  '           - Integer : ResourceId 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - IO.FileStream object
  '
  Private Function GetVideo(ByRef ResourceId As Int64, ByRef Extension As String, ByRef ResourceName As String) As IO.MemoryStream

    Dim _sql_transaction As SqlClient.SqlTransaction
    Dim _video As IO.MemoryStream

    _sql_transaction = Nothing
    _video = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        _video = Nothing
      End If

      If Not WSI.Common.LCDResources.Load(ResourceId, _video, Extension, ResourceName, _sql_transaction) Then
        _video = Nothing
      End If

    Catch _ex As Exception
      _video = Nothing

      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      "Exception", _
                      "", _
                      "", _
                      "", _
                      _ex.Message)

    End Try

    Return _video

  End Function ' GetVideo


  ' PURPOSE : Before start edition event
  '
  '  PARAMS:
  '     - INPUT:
  '         - Row
  '         - Column
  '     - OUTPUT:
  '         - EditionCanceled
  '
  ' RETURNS :
  '
  Private Sub uc_grid_collections_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_functionalities.BeforeStartEditionEvent

    If Column = GRID_COLUMN_FUNC_ENABLED Then
      If dg_functionalities.Cell(Row, GRID_COLUMN_FUNC_ENABLED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Then
        EditionCanceled = True
      End If
    End If

  End Sub ' uc_grid_collections_BeforeStartEditionEvent

#End Region

End Class