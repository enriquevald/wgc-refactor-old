'-------------------------------------------------------------------
' Copyright © 2015 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_with_stacker.vb
'
' DESCRIPTION:   Allows to change the stacker of the terminals
'
' AUTHOR:        David Rigal
'
' CREATION DATE: 09-JAN-2015
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ---------  ---------------------------------------------------
' 09-JAN-2015  DRV        Initial Version.
' 26-MAR-2015  DRV        TFS ITEM 676: Several Changes
' 27-APR-2015  DRV        WIG-2254: Allows to enter a stacker id when UseStackerId general param was disabled
' 12-MAY-2015  YNM        Fixed Bug WIG-2282: With GP Terminal.UseStackerID = 1, It was allowing to change stacker id = 0. 
' 18-MAY-2015  YNM        Fixed Bug WIG-2337: Incorrect change stacker with retired terminals.
' 18-MAY-2015  YNM        Fixed Bug WIG-2362: Change stacker in cashless takes into account GP 'Terminal.UseStackerId'
' 28-MAY-2015  YNM        Fixed Bug WIG-2385: Change stacker does not show the ID Stacker 
' 07-SEP-2015  MPO        TFS ITEM 2194: SAS16: Estadísticas multi-denominación: WCP
' 09-MAR-2016  FAV        Fixed Bug 9552: Validate that the money collection is not open when the Terminal is going to be to retired
' 24-FEB-2017  ETP        Fixed Bug 25102: Change stacker column is hide when opening the window.
' 10-AUG-2017  AMF        Bug 29307:[WIGOS-3729] Missing selection criteria for exporting data in Stacker Change screen
' ---------------------------------------------------------------------------

Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.TITO

Public Class frm_terminal_with_stacker
  Inherits frm_base_sel_edit

#Region " Structs "

  Private Structure TITO_FILTERS
    Dim StackerID As Integer
    Dim LastHours As Integer
    Dim Terminals As String
  End Structure

  Private Structure CASHLESS_FILTERS
    Dim WithActivity As Boolean
    Dim LastHours As Integer
    Dim Terminals As String
  End Structure

  Private Structure TERMINAL_STACKER
    Dim TerminalID As Int32
    Dim StackerID As Int64
    Dim Provider As String
    Dim Name As String
  End Structure

#End Region

#Region " Constants "

  Private TERMINAL_DATA_COLUMNS As Integer = 2
  Private GRID_NUM_COLUMNS As Integer = 5
  Private GRID_NUM_HEADERS As Integer = 2

  Private GRID_COLUMN_SELECT As Integer = 0
  Private GRID_COLUMN_TERMINAL_ID As Integer = 1
  Private GRID_COLUMN_INIT_TERMINAL_DATA As Integer = 2
  Private GRID_COLUMN_STACKER_ID As Integer = 2
  Private GRID_COLUMN_DATETIME As Integer = 3
  Private GRID_COLUMN_CHANGE_STACKER As Integer = 4
  Private GRID_COLUMN_CHANGE_STACKER_PRINTABLE As Integer = 5
  Private GRID_COLUMN_NEW_STACKER_ID As Integer = 6

  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 100
  Private Const GRID_COLUMN_WIDTH_TERMINAL_ID As Integer = 1
  Private Const GRID_COLUMN_WIDTH_STACKER_ID As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_CHANGE_STACKER As Integer = 1800
  Private Const GRID_COLUMN_WIDTH_NEW_STACKER_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_WITH_ACCEPTOR As Integer = 2300
  Private Const GRID_COLUMN_WIDTH_DATETIME As Integer = 2200

  Private Const SQL_TERMINAL_ID As Integer = 0
  Private Const SQL_TITO_STACKER_ID As Integer = 1
  Private Const SQL_DATETIME As Integer = 2

#End Region

#Region " Members "

  Private m_is_tito_mode As Boolean
  Private m_terminal_columns As List(Of ColumnSettings)
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_tito_filters As TITO_FILTERS
  Private m_cashless_filters As CASHLESS_FILTERS
  Private m_checked_terminals As Int32
  Private m_use_stacker_id As Boolean
  Private m_show_terminal_location As String

#End Region

#Region " Overrides "

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_WITH_STACKER

    MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Initialize all form controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Protected Overrides Sub GUI_InitControls()


    Call MyBase.GUI_InitControls()

    m_is_tito_mode = TITO.Utils.IsTitoMode()
    m_use_stacker_id = WSI.Common.Misc.UseStackerId()
    m_checked_terminals = 0

    ' Form Title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5827)

    ' Form Size
    If m_is_tito_mode Then
      If Not m_use_stacker_id Then
        Me.Size = New Size(990, 620)
      End If
    Else
      Me.Size = New Size(1004, 620)
    End If

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION


    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6083)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Size = New Size(90, 45)

    uc_provider_filter.Init(WSI.Common.Misc.AcceptorTerminalTypeList())

    opt_without_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5841)
    opt_with_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5840)
    ef_stacker_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5845)
    chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5842)
    ef_last_hours.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3, 0)
    ef_last_hours.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6081)
    ef_last_hours.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6080)
    ef_last_hours.TextValue = 8

    If m_is_tito_mode Then
      'Dates
      opt_with_activity.Visible = False
      opt_without_activity.Visible = False

      gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5843)
      gb_date.Size = New Size(214, 50)
      ef_last_hours.Location = New Point(7, 20)

      'Stacker ID
      If m_use_stacker_id Then
        ef_stacker_id.Location = New Point(14, 64)
        ef_stacker_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
      Else
        ef_stacker_id.Visible = False
      End If
    Else
      'Dates
      gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5844)

      'Stacker ID
      ef_stacker_id.Visible = False
    End If
    ' Dates
    '    dp_from.t()

    Call GUI_StyleSheet()

  End Sub

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_NUM_COLUMNS, GRID_NUM_HEADERS, True)

      ' Sentinel
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = 200
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_COLUMN_WIDTH_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      '  Terminal Report
      For _idx As Int32 = 0 To m_terminal_columns.Count - 1
        .Column(GRID_COLUMN_INIT_TERMINAL_DATA + _idx).Width = m_terminal_columns(_idx).Width
        .Column(GRID_COLUMN_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)
        .Column(GRID_COLUMN_INIT_TERMINAL_DATA + _idx).Header(1).Text = m_terminal_columns(_idx).Header1
        .Column(GRID_COLUMN_INIT_TERMINAL_DATA + _idx).Alignment = m_terminal_columns(_idx).Alignment
        .Column(GRID_COLUMN_INIT_TERMINAL_DATA + _idx).IsColumnSortable = True
      Next

      .Column(GRID_COLUMN_DATETIME).Header(0).Text = ""
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6082)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_COLUMN_WIDTH_DATETIME
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DATETIME).IsColumnPrintable = True

      .Column(GRID_COLUMN_CHANGE_STACKER).Width = GRID_COLUMN_WIDTH_CHANGE_STACKER
      .Column(GRID_COLUMN_CHANGE_STACKER).Header(0).Text = ""
      .Column(GRID_COLUMN_CHANGE_STACKER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5834)
      .Column(GRID_COLUMN_CHANGE_STACKER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CHANGE_STACKER).Editable = True
      .Column(GRID_COLUMN_CHANGE_STACKER).IsColumnPrintable = False
      .Column(GRID_COLUMN_CHANGE_STACKER).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      .Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Width = 1
      .Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Header(0).Text = ""
      .Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5834)
      .Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).IsColumnPrintable = True

      If m_is_tito_mode Then
        .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5843)
        .Column(GRID_COLUMN_DATETIME).Width = GRID_COLUMN_WIDTH_DATETIME - 200

        .Column(GRID_COLUMN_STACKER_ID).Width = GRID_COLUMN_WIDTH_STACKER_ID
        .Column(GRID_COLUMN_STACKER_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_STACKER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5833)
        .Column(GRID_COLUMN_STACKER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_STACKER_ID).Width = 1
        .Column(GRID_COLUMN_STACKER_ID).IsColumnPrintable = False

        If m_use_stacker_id Then
          .Column(GRID_COLUMN_STACKER_ID).Width = GRID_COLUMN_WIDTH_STACKER_ID
          .Column(GRID_COLUMN_STACKER_ID).IsColumnPrintable = True

          .Column(GRID_COLUMN_NEW_STACKER_ID).Width = GRID_COLUMN_WIDTH_NEW_STACKER_ID
          .Column(GRID_COLUMN_NEW_STACKER_ID).Header(0).Text = ""
          .Column(GRID_COLUMN_NEW_STACKER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5835)
          .Column(GRID_COLUMN_NEW_STACKER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
          .Column(GRID_COLUMN_NEW_STACKER_ID).Editable = True
          .Column(GRID_COLUMN_NEW_STACKER_ID).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
          .Column(GRID_COLUMN_NEW_STACKER_ID).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7)
        End If
      End If



    End With

  End Sub

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    If m_is_tito_mode Then

      _sb.AppendLine("    SELECT   TE_TERMINAL_ID ")
      _sb.AppendLine("           , ST_STACKER_ID  ")
      _sb.AppendLine("           , MC_DATETIME    ")
      _sb.AppendLine("      FROM   TERMINALS      ")
      _sb.AppendLine(" LEFT JOIN   STACKERS       ")
      _sb.AppendLine("        ON   TE_TERMINAL_ID = ST_INSERTED ")
      _sb.AppendLine(" LEFT JOIN   MONEY_COLLECTIONS  ")
      _sb.AppendLine("        ON   MC_TERMINAL_ID = TE_TERMINAL_ID ")
      _sb.AppendLine("       AND   MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.OPEN).ToString() & " ")

      'If uc_provider_filter.opt_one_terminal.Checked Then
      '  _sb.AppendLine(" WHERE TE_TERMINAL_ID = " & Me.uc_provider_filter.GetProviderIdListSelected())
      'Else
      _sb.AppendLine(" WHERE TE_TERMINAL_ID IN " & Me.uc_provider_filter.GetProviderIdListSelected())
      'End If

      If m_tito_filters.StackerID <> -1 Then
        If m_tito_filters.StackerID <> 0 Then
          _sb.AppendLine(" AND   ST_STACKER_ID = " & m_tito_filters.StackerID & " ")
        Else
          _sb.AppendLine(" AND   ST_STACKER_ID IS NULL ")
        End If
      End If

      If m_tito_filters.LastHours > 0 Then
        _sb.AppendLine(" AND   MC_DATETIME < DATEADD(HOUR, -" & m_tito_filters.LastHours.ToString() & ", GETDATE()) ")
      End If
      _sb.AppendLine(" AND (TE_STATUS = " & TerminalStatus.ACTIVE & "  OR (TE_STATUS IN (" & TerminalStatus.OUT_OF_SERVICE & "," & TerminalStatus.RETIRED & ") AND TE_TERMINAL_ID = ST_INSERTED))")
    Else
      _sb.AppendLine("    SELECT   TE_TERMINAL_ID                                        ")
      _sb.AppendLine("           , 0                                                     ")
      _sb.AppendLine("           , CS_OPENING_DATE                                       ")
      _sb.AppendLine("      FROM   TERMINALS                                             ")
      _sb.AppendLine(" LEFT JOIN   CASHIER_TERMINALS                                     ")
      _sb.AppendLine("        ON   CT_TERMINAL_ID = TE_TERMINAL_ID                       ")
      _sb.AppendLine(" LEFT JOIN   CASHIER_SESSIONS                                      ")
      _sb.AppendLine("        ON   CT_CASHIER_ID = CS_CASHIER_ID                         ")

      If Not m_cashless_filters.WithActivity Then
        _sb.AppendLine(" AND   CS_STATUS = " & CShort(CASHIER_SESSION_STATUS.OPEN) & " ")
      End If

      'If uc_provider_filter.opt_one_terminal.Checked Then
      '  _sb.AppendLine(" WHERE TE_TERMINAL_ID = " & Me.uc_provider_filter.GetProviderIdListSelected())
      'Else
      _sb.AppendLine(" WHERE TE_TERMINAL_ID IN " & Me.uc_provider_filter.GetProviderIdListSelected())
      'End If

      _sb.AppendLine(" AND CT_CASHIER_ID IS NOT NULL ")

      If m_cashless_filters.WithActivity Then

        _sb.AppendLine(" AND   CS_STATUS = " & CShort(CASHIER_SESSION_STATUS.OPEN) & " ")

        If m_cashless_filters.LastHours Then
          _sb.AppendLine(" AND   CS_OPENING_DATE < DATEADD(HOUR, -" & m_cashless_filters.LastHours.ToString() & ", GETDATE()) ")
        End If
      Else
        _sb.AppendLine(" AND ( CS_STATUS <> " & CShort(CASHIER_SESSION_STATUS.OPEN) & " OR CS_STATUS IS NULL ) ")
      End If


    End If

    Return _sb.ToString()
  End Function

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        m_checked_terminals = 0
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        If ChangeSelectedTerminalsStacker() Then
          Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
        End If
      Case ENUM_BUTTON.BUTTON_EXCEL _
         , ENUM_BUTTON.BUTTON_PRINT
        Me.Grid.Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Width = 10
      Case Else
    End Select

    Call MyBase.GUI_ButtonClick(ButtonId)

    Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Gui_ScreenDataChanged() And Me.Permissions.Write

    ' Button excel and print must hide printable column after export
    If ButtonId = ENUM_BUTTON.BUTTON_EXCEL Or ButtonId = ENUM_BUTTON.BUTTON_PRINT Then
      Me.Grid.Column(GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Width = 1
    End If
  End Sub

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - TRUE: filter values are accepted
  '   - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_data As List(Of Object)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_TERMINAL_ID)

    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_TERMINAL_ID), m_terminal_report_type)

    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next
    If Not DbRow.IsNull(SQL_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_DATETIME), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If m_is_tito_mode Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_ID).Value = IIf(DbRow.Value(SQL_TITO_STACKER_ID) Is DBNull.Value, "", DbRow.Value(SQL_TITO_STACKER_ID))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_UNCHECKED
    End If

    Call SetChangeStackerPrintableColumnText(RowIndex)

    Return True
  End Function

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_show_terminal_location = String.Empty

    If m_is_tito_mode Then

      m_tito_filters.LastHours = IIf(String.IsNullOrEmpty(ef_last_hours.TextValue), 0, Me.ef_last_hours.TextValue)
      m_tito_filters.StackerID = IIf(String.IsNullOrEmpty(Me.ef_stacker_id.TextValue), -1, Me.ef_stacker_id.TextValue)

      m_tito_filters.Terminals = Me.uc_provider_filter.GetTerminalReportText()

    Else

      m_cashless_filters.WithActivity = opt_with_activity.Checked
      m_cashless_filters.LastHours = IIf(String.IsNullOrEmpty(ef_last_hours.TextValue), 0, Me.ef_last_hours.TextValue)

      m_cashless_filters.Terminals = Me.uc_provider_filter.GetTerminalReportText()

    End If

    ' Show Terminal Location
    If Me.chk_terminal_location.Checked Then
      m_show_terminal_location = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_terminal_location = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter
    If m_is_tito_mode Then
      PrintData.SetFilter(gb_date.Text & " " & ef_last_hours.Text.ToLower(), IIf(m_tito_filters.LastHours > 0, m_tito_filters.LastHours, "---") & " " & ef_last_hours.SufixText)
      PrintData.SetFilter(ef_stacker_id.Text, IIf(m_tito_filters.StackerID <> -1, m_tito_filters.StackerID, "---"))
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), m_tito_filters.Terminals)
    Else
      PrintData.SetFilter(gb_date.Text, IIf(m_cashless_filters.WithActivity, opt_with_activity.Text, opt_without_activity.Text))
      If m_cashless_filters.WithActivity Then
        PrintData.SetFilter(gb_date.Text & " " & ef_last_hours.Text.ToLower(), IIf(m_cashless_filters.LastHours > 0, m_cashless_filters.LastHours, "---") & " " & ef_last_hours.SufixText)
      End If
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), m_cashless_filters.Terminals)
    End If

    PrintData.SetFilter(chk_terminal_location.Text, m_show_terminal_location)
  End Sub

  ' PURPOSE: Sets filter default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    opt_with_activity.Checked = True

    Me.ef_last_hours.TextValue = 8

    uc_provider_filter.SetDefaultValues()

    ef_stacker_id.TextValue = ""

    chk_terminal_location.Checked = False

    MyBase.GUI_FilterReset()
  End Sub

  ' PURPOSE: Check if data has been modified
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - TRUE: data has changed
  '   - FALSE: no changes
  Protected Overrides Function Gui_ScreenDataChanged() As Boolean
    Return (m_checked_terminals > 0)
  End Function

#End Region

#Region " Public Methods "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Private Methods "
  ' PURPOSE: Recaluclates Column Indexes for each mode (Cashless/TITO)
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)
    m_terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)

    If m_is_tito_mode Then
      GRID_COLUMN_DATETIME = GRID_COLUMN_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS
      GRID_COLUMN_STACKER_ID = GRID_COLUMN_DATETIME + 1
      GRID_COLUMN_CHANGE_STACKER = GRID_COLUMN_STACKER_ID + 1
      GRID_COLUMN_CHANGE_STACKER_PRINTABLE = GRID_COLUMN_CHANGE_STACKER + 1
      GRID_COLUMN_NEW_STACKER_ID = GRID_COLUMN_CHANGE_STACKER_PRINTABLE + 1
      If m_use_stacker_id Then
        GRID_NUM_COLUMNS = GRID_COLUMN_NEW_STACKER_ID + 1
      Else
        GRID_NUM_COLUMNS = GRID_COLUMN_CHANGE_STACKER_PRINTABLE + 1
      End If
    Else
      GRID_COLUMN_DATETIME = GRID_COLUMN_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS
      GRID_COLUMN_CHANGE_STACKER = GRID_COLUMN_DATETIME + 1
      GRID_COLUMN_CHANGE_STACKER_PRINTABLE = GRID_COLUMN_CHANGE_STACKER + 1
      GRID_NUM_COLUMNS = GRID_COLUMN_CHANGE_STACKER_PRINTABLE + 1
    End If

  End Sub

  ' PURPOSE: Change the stacker of each selected terminal and register it in auditor data
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Function ChangeSelectedTerminalsStacker() As Boolean
    Dim _stacker_id As Int32
    Dim _terminal_id As Int32
    Dim _errors As Int32
    Dim _error_description As String
    Dim _terminals As List(Of TERMINAL_STACKER)
    Dim _aux As TERMINAL_STACKER
    Dim _terminal As CLASS_TERMINAL
    Dim _is_zero_stacker_Id As Boolean


    _errors = 0
    _error_description = String.Empty
    _stacker_id = 0
    _terminals = New List(Of TERMINAL_STACKER)
    _terminal = New CLASS_TERMINAL()
    _is_zero_stacker_Id = False

    Try

      For _idx As Int32 = 0 To Me.Grid.NumRows - 1

        If Me.Grid.Cell(_idx, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_CHECKED Then
          _terminal_id = Me.Grid.Cell(_idx, GRID_COLUMN_TERMINAL_ID).Value
          If Not (_terminal.DB_Read(_terminal_id, 0) = CLASS_BASE.ENUM_STATUS.STATUS_OK) Then

          End If
          If Not m_use_stacker_id Or _terminal.Status = TerminalStatus.RETIRED Then
            _stacker_id = -1
          ElseIf Not String.IsNullOrEmpty(Me.Grid.Cell(_idx, GRID_COLUMN_NEW_STACKER_ID).Value) Then
            _stacker_id = Me.Grid.Cell(_idx, GRID_COLUMN_NEW_STACKER_ID).Value
          End If

          If m_use_stacker_id And _stacker_id > 0 And IsStackerAssignedToRetiredTerminal(_stacker_id) Then
            Log.Error(String.Format("The Stacker [{0}] is assigned to a retired terminal", _stacker_id))
          End If

          If _stacker_id <> 0 Then
            Using _db_trx As New DB_TRX()
              If Not ChangeStacker(_terminal_id, _stacker_id, Me.Grid.Cell(_idx, GRID_COLUMN_INIT_TERMINAL_DATA + 1).Value, _error_description, _db_trx) Then
                _errors += 1
              Else
                _aux.TerminalID = _terminal_id
                _aux.StackerID = _stacker_id
                _aux.Name = Me.Grid.Cell(_idx, GRID_COLUMN_INIT_TERMINAL_DATA).Value
                _aux.Provider = Me.Grid.Cell(_idx, GRID_COLUMN_INIT_TERMINAL_DATA + 1).Value
                _terminals.Add(_aux)

                _db_trx.Commit()
              End If
            End Using
          Else
            _is_zero_stacker_Id = True
          End If
        End If
      Next

      If _errors > 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5876), _
                   ENUM_MB_TYPE.MB_TYPE_ERROR, _
                   ENUM_MB_BTN.MB_BTN_OK, , _
                   _errors.ToString())
      End If
      If (_is_zero_stacker_Id) Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6341), _
                   ENUM_MB_TYPE.MB_TYPE_ERROR, _
                   ENUM_MB_BTN.MB_BTN_OK, , )
      End If

      If _terminals.Count > 0 Then
        AuditStackerChange(_terminals)
      End If

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try
  End Function

  ' PURPOSE: Change the stacker of the specified terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - Terminal: Terminal Id
  '           - StackerID: New stacker Id
  '           - TerminalName
  '           - _db_trx
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Function ChangeStacker(ByVal Terminal As Int32, ByVal StackerID As Int32, ByVal TerminalName As String, ByRef _error_description As String, ByRef _db_trx As DB_TRX) As Boolean

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    If m_is_tito_mode Then
      If Not ChangeStackerTITOMode(Terminal, StackerID, TerminalName, _error_description, _db_trx) Then

        Return False
      End If
    Else
      If Not ChangeStackerCashlesMode(Terminal, TerminalName, _db_trx) Then

        Return False
      End If
    End If
    Windows.Forms.Cursor.Current = Cursors.Default

    Return True
  End Function

  ' PURPOSE: Change the stacker of the specified terminal (for Cahsless mode)
  '
  '  PARAMS:
  '     - INPUT:
  '           - Terminal: Terminal Id
  '           - TerminalName
  '           - _db_trx
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Function ChangeStackerCashlesMode(ByVal Terminal As Int32, ByVal TerminalName As String, ByRef _db_trx As DB_TRX) As Boolean
    Dim _money_collection_id As Int64
    Dim _money_collection As MoneyCollection

    Try
      If Not MoneyCollection.DB_GetMoneyCollection(Terminal, 0, _money_collection_id, _db_trx.SqlTransaction) Then
        Log.Error("Error getting Money Collection.")
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5875), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , TerminalName)
        Return False
      End If
      _money_collection = MoneyCollection.DB_Read(_money_collection_id, _db_trx.SqlTransaction)
      If _money_collection Is Nothing OrElse Not _money_collection.DB_ChangeStatusToPendingClosing(_db_trx.SqlTransaction) Then
        Log.Error("Error changing Money Collection status.")
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5875), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , TerminalName)
        Return False
      End If

      If Not Cashier.WCPCashierSessionPendingClosing(Terminal, _db_trx.SqlTransaction) = MB_CASHIER_SESSION_CLOSE_STATUS.OK Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5876), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , TerminalName)
        Return False
      End If

      Return True

    Catch ex As Exception

      Return False
    End Try
  End Function

  ' PURPOSE: Change the stacker of the specified terminal (for TITO mode)
  '
  '  PARAMS:
  '     - INPUT:
  '           - Terminal: Terminal Id
  '           - StackerId: New stacker ID
  '           - TerminalName
  '           - _db_trx
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Function ChangeStackerTITOMode(ByVal Terminal As Int32, ByVal StackerID As Int32, ByVal TerminalName As String, ByRef _error_description As String, ByRef _db_trx As DB_TRX) As Boolean
    Dim _pending_meters As DataTable
    Dim _stacker As Stacker

    _pending_meters = New DataTable()

    Try
      If Not SAS_Meter.GetSasMeters(SAS_Meter.ENUM_METERS_TYPE.BILLS_AND_TICKETS, Terminal, False, _pending_meters, _db_trx.SqlTransaction) OrElse _pending_meters.Rows.Count > 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5877), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , TerminalName)

        Return False
      End If

      _stacker = Nothing
      If StackerID <> -1 Then
        _stacker = Stacker.DB_Read(StackerID, _db_trx.SqlTransaction)
        If _stacker Is Nothing Then

          Return False
        End If
      End If

      If Not WSI.Common.TITO.TITO_ChangeStacker.OfflineChangeStackerInTitoMode(Terminal, _
                                                                               _stacker, _
                                                                            _db_trx.SqlTransaction, _
                                                                               GLB_CurrentUser.Id, _
                                                                               GLB_CurrentUser.Name, _
                                                                               True, _
                                                                               False, _
                                                                            _error_description) Then
        Return False
      End If

      Return True

    Catch ex As Exception
      Log.Exception(ex)

      Return False
    End Try
  End Function


  ' PURPOSE: Audit the stacker change for each terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub AuditStackerChange(ByVal Terminals As List(Of TERMINAL_STACKER))
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim _stacker As String
    Dim _audit_description As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    'auditor_data.IsAuditable = False
    auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5834), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5834))
    If m_is_tito_mode Then
      For Each _terminalID As TERMINAL_STACKER In Terminals
        _stacker = IIf(_terminalID.StackerID <= 0, "---", _terminalID.StackerID)
        _audit_description = WSI.Common.Misc.ConcatProviderAndTerminal(_terminalID.Provider, _terminalID.Name) & IIf(m_use_stacker_id, " = " & _stacker, "")

        Call auditor_data.SetField(0 _
                                  , Nothing _
                                  , _audit_description _
                                  , CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)
      Next
    Else
      _stacker = ""
      For _row_idx As Int32 = 0 To Grid.NumRows - 1
        If Me.Grid.Cell(_row_idx, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_CHECKED Then
          _audit_description = WSI.Common.Misc.ConcatProviderAndTerminal(Me.Grid.Cell(_row_idx, GRID_COLUMN_INIT_TERMINAL_DATA).Value, Me.Grid.Cell(_row_idx, GRID_COLUMN_INIT_TERMINAL_DATA + 1).Value)

          Call auditor_data.SetField(0 _
                                    , Nothing _
                                    , _audit_description _
                                    , CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)
        End If
      Next
    End If
    auditor_data.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                         0)

  End Sub

  ' PURPOSE: Sets the value of the printable cell (1 - YES, 2 - NO)
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row: Index of the modified row
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub SetChangeStackerPrintableColumnText(ByVal Row As Int32)
    If Me.Grid.Cell(Row, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_CHECKED Then
      Me.Grid.Cell(Row, GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5872)
    Else
      Me.Grid.Cell(Row, GRID_COLUMN_CHANGE_STACKER_PRINTABLE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5873)
    End If
  End Sub

  Private Function IsStackerAvailable(ByVal StackerId As Int64) As Boolean
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    _sb.AppendLine("SELECT   COUNT(1)                    ")
    _sb.AppendLine("  FROM   STACKERS                    ")
    _sb.AppendLine(" WHERE   ST_STACKER_ID = @pStackerID ")
    _sb.AppendLine("   AND   ST_INSERTED IS NULL         ")
    _sb.AppendLine("   AND   ST_STATUS = @pStatusActive  ")

    Try
      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString())
          _sql_cmd.Parameters.Add("@pStackerID", SqlDbType.BigInt).Value = StackerId
          _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = TITO_STACKER_STATUS.ACTIVE

          If _db_trx.ExecuteScalar(_sql_cmd) > 0 Then
            Return True
          End If
        End Using
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function

  Private Function IsStackerAssignedToRetiredTerminal(ByVal StackerId As Int64) As Boolean
    Dim _sb As StringBuilder
    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   T.TE_TERMINAL_ID                   ")
    _sb.AppendLine("      FROM   STACKERS S                         ")
    _sb.AppendLine("INNER JOIN   TERMINALS T                        ")
    _sb.AppendLine("        ON   S.ST_INSERTED = T.TE_TERMINAL_ID   ")
    _sb.AppendLine("     WHERE   ST_STACKER_ID = @pStackerID        ")
    _sb.AppendLine("       AND   T.TE_STATUS = @pStatus             ")

    Try
      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString())
          _sql_cmd.Parameters.Add("@pStackerID", SqlDbType.BigInt).Value = StackerId
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TerminalStatus.RETIRED

          If _db_trx.ExecuteScalar(_sql_cmd) > 0 Then
            Return True
          End If
        End Using
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function


#End Region

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Full
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

  Protected Overrides Sub GUI_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean)
    Select Case Column
      Case GRID_COLUMN_CHANGE_STACKER
        EditionCanceled = Me.Grid.Cell(Row, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED

      Case GRID_COLUMN_NEW_STACKER_ID
        If Me.Grid.Cell(Row, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_CHECKED Then
          EditionCanceled = False
        Else
          EditionCanceled = True
        End If

      Case Else
        EditionCanceled = True
    End Select
  End Sub

  Protected Overrides Sub GUI_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

    Select Case Column
      Case GRID_COLUMN_CHANGE_STACKER
        If Me.Grid.Cell(Row, GRID_COLUMN_CHANGE_STACKER).Value = uc_grid.GRID_CHK_UNCHECKED Then
          If m_is_tito_mode And m_use_stacker_id Then
            Me.Grid.Cell(Row, GRID_COLUMN_NEW_STACKER_ID).Value = ""
          End If
          m_checked_terminals -= 1
        Else
          m_checked_terminals += 1
        End If

        Call SetChangeStackerPrintableColumnText(Row)

        Me.GUI_Button(frm_base_sel_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Gui_ScreenDataChanged() And Me.Permissions.Write 'Disable "Save" button
      Case GRID_COLUMN_NEW_STACKER_ID
        If Not String.IsNullOrEmpty(Me.Grid.Cell(Row, GRID_COLUMN_NEW_STACKER_ID).Value) Then
          If IsStackerAvailable(Me.Grid.Cell(Row, GRID_COLUMN_NEW_STACKER_ID).Value) Then
            Me.Grid.Cell(Row, GRID_COLUMN_NEW_STACKER_ID).ForeColor = Color.Black
          Else
            Me.Grid.Cell(Row, GRID_COLUMN_NEW_STACKER_ID).ForeColor = Color.Red
          End If
        Else
          Me.Grid.Cell(Row, GRID_COLUMN_NEW_STACKER_ID).ForeColor = Color.Black
        End If
    End Select
  End Sub

  Private Sub rb_without_activity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_without_activity.CheckedChanged
    ef_last_hours.Enabled = opt_with_activity.Checked
    ef_last_hours.TextValue = 8
  End Sub

#End Region

End Class
