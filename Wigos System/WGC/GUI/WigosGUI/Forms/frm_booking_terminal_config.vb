'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_booking_terminal_config.vb
'
' DESCRIPTION : Cashier Configuration form
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-AUG-2016  ESE    Product Backlog Item 16212:JOP09: Booking terminal
' 22-FEB-2017  ATB    Fixed some misfunctionality with booking terminal config
' 24-JUL-2017  ATB    PBI 28710: EGM Reserve � Settings
' 08-AUG-2017  ATB    PBI 28710: EGM Reserve � Settings
' 14-AUG-2017  FGB    Bug 29350: Booking terminal - Showing error message when a level checkbox is not selected
' 24-AUG-2017  ATB    Bug 29350: Booking terminal - Showing error message when a level checkbox is not selected (WIGOS-4673)
' 28-SEP-2017  XGJ    Bug Wigos-5430: EGM Reserve: wrong time unit in Configuration screen
' 03-APR-2018  JGC    Bug 32138:[WIGOS-9605]: Terminal Reservation Configuration - Changes to Interval field doesn't been saved
' 06-APR-2018  RLO    WIGOS-9605 Terminal Reservation Configuration - Changes to Interval field doesn't been saved
' 06-APR-2018  FOS    WIGOS-9781 EGM Reservation configuration allows negative number for Coin in
' 11-APR-2018  AGS    Bug 32293:WIGOS-10021 The new value is saved when clicking on Cancel button in Terminal Reservation Configuration screen.
' 19-APR-2018  EOR    Bug 32394: WIGOS-10118 A warning message is not appear indicating that the changes will be lost 
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Public Class frm_booking_terminal_config
  Inherits frm_base_edit

#Region " Constants"
  Private Const _EXPANDED_HEIGHT As Integer = 200
  Private Const _MAXIMUM_DIGITS_COIN_IN As Integer = 9
  Private Const _LEVEL_NUMBER As Integer = 3
  Private Const _CHECKBOX_LEVEL_ENABLED_PREFFIX As String = "chk_enabled_level_"
#End Region

#Region " Members "
  Private m_reserve_terminal As New CLASS_BOOKING_TERMINAL_CONFIG
#End Region

#Region " Overrides "

  ''' <summary>
  ''' Sets the proper form identifier
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BOOKING_TERMINALS_CONFIGURATION

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ''' <summary>
  ''' Initializes form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    Me.Text = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7510))
    Dim m_hours As Array

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    SetFormLabels()
    SetTerminalsCombo()

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    ' Minutes combos
    m_hours = {10, 15, 20, 30, 45, 60, 90, 120}
    Me.cmb_minutes_level_01.ValueMember = "Time"
    Me.cmb_minutes_level_01.DataSource = New ArrayList(m_hours)
    Me.cmb_minutes_level_02.ValueMember = "Time"
    Me.cmb_minutes_level_02.DataSource = New ArrayList(m_hours)
    Me.cmb_minutes_level_03.ValueMember = "Time"
    Me.cmb_minutes_level_03.DataSource = New ArrayList(m_hours)
    Me.cmb_minutes_level_04.ValueMember = "Time"
    Me.cmb_minutes_level_04.DataSource = New ArrayList(m_hours)

    ' Text fields for coin-in
    Me.txt_coin_level_01.IsReadOnly = False
    Me.txt_coin_level_01.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, _MAXIMUM_DIGITS_COIN_IN)
    Me.txt_coin_level_02.IsReadOnly = False
    Me.txt_coin_level_02.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, _MAXIMUM_DIGITS_COIN_IN)
    Me.txt_coin_level_03.IsReadOnly = False
    Me.txt_coin_level_03.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, _MAXIMUM_DIGITS_COIN_IN)
    Me.txt_coin_level_04.IsReadOnly = False
    Me.txt_coin_level_04.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, _MAXIMUM_DIGITS_COIN_IN)

    ' Forces the form to read the checks to enable/disable elements
    Me.CheckedChange(Me.chk_enabled_level_01, cmb_minutes_level_01, txt_coin_level_01, uc_terminals_group_level_01)
    Me.CheckedChange(Me.chk_enabled_level_02, cmb_minutes_level_02, txt_coin_level_02, uc_terminals_group_level_02)
    Me.CheckedChange(Me.chk_enabled_level_03, cmb_minutes_level_03, txt_coin_level_03, uc_terminals_group_level_03)
    Me.CheckedChange(Me.chk_enabled_level_04, cmb_minutes_level_04, txt_coin_level_04, uc_terminals_group_level_04)
    Me.EnableDisableAll()

    'btn_custom_0 is used as the 'Apply/Save' button
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    ' WIGOS-5274 add 2 gp configuration
    ' XGJ 28-SEP-2017
    Me.label_interval.Text = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(8693)) + " (" + NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(442)) + ") "
    Me.cb_another.Text = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(8694))

  End Sub 'GUI_InitControls

  ''' <summary>
  ''' Auditory method to set every state
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _cls_terminal_config As CLASS_BOOKING_TERMINAL_CONFIG

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_BOOKING_TERMINAL_CONFIG
        _cls_terminal_config = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Me.DbStatus = ENUM_STATUS.STATUS_OK
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' Fill the form fields with the terminals reservation config
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    ' It reads from the loaded information, within Me.DbReadObject that is filled in the class's Duplicate method

    Try
      m_reserve_terminal = Me.DbReadObject


      With m_reserve_terminal

        Me.chk_reservation.Checked = .ReservationEnabled
        Me.cb_another.Checked = .AllowCard
        Me.TextBox_interval.Text = .TerminalInterval.ToString()
        Me.chk_enabled_level_01.Checked = .ReservationLevel01 And .ReservationEnabled
        Me.chk_enabled_level_02.Checked = .ReservationLevel02 And .ReservationEnabled
        Me.chk_enabled_level_03.Checked = .ReservationLevel03 And .ReservationEnabled
        Me.chk_enabled_level_04.Checked = .ReservationLevel04 And .ReservationEnabled
        Me.cmb_minutes_level_01.SelectedIndex = Me.cmb_minutes_level_01.FindStringExact(.TimeLevel01)
        Me.cmb_minutes_level_02.SelectedIndex = Me.cmb_minutes_level_02.FindStringExact(.TimeLevel02)
        Me.cmb_minutes_level_03.SelectedIndex = Me.cmb_minutes_level_03.FindStringExact(.TimeLevel03)
        Me.cmb_minutes_level_04.SelectedIndex = Me.cmb_minutes_level_04.FindStringExact(.TimeLevel04)


        Me.txt_coin_level_01.Value = IIf(Convert.ToDecimal(.CoinInLevel01) < 0, 0, Convert.ToDecimal(.CoinInLevel01))
        Me.txt_coin_level_02.Value = IIf(Convert.ToDecimal(.CoinInLevel02) < 0, 0, Convert.ToDecimal(.CoinInLevel02))
        Me.txt_coin_level_03.Value = IIf(Convert.ToDecimal(.CoinInLevel03) < 0, 0, Convert.ToDecimal(.CoinInLevel03))
        Me.txt_coin_level_04.Value = IIf(Convert.ToDecimal(.CoinInLevel04) < 0, 0, Convert.ToDecimal(.CoinInLevel04))

        Me.uc_terminals_group_level_01.SetTerminalList(GetTerminalList(.AccountTimeLevel01))
        Me.uc_terminals_group_level_02.SetTerminalList(GetTerminalList(.AccountTimeLevel02))
        Me.uc_terminals_group_level_03.SetTerminalList(GetTerminalList(.AccountTimeLevel03))
        Me.uc_terminals_group_level_04.SetTerminalList(GetTerminalList(.AccountTimeLevel04))

      End With
    Catch ex As Exception
    End Try
  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Get the form fields to save changes in terminals reservation config
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()
    Dim _aux_dataTable As DataTable
    Dim _checked_terminal As TerminalList

    Try
      ' Setting the data for the auditory
      m_reserve_terminal = Me.DbEditedObject

      With m_reserve_terminal
        .ReservationEnabled = Me.chk_reservation.Checked
        .AllowCard = Me.cb_another.Checked
        .TerminalInterval = CInt(TextBox_interval.Text)

        .ReservationLevel01 = Me.chk_enabled_level_01.Checked
        .ReservationLevel02 = Me.chk_enabled_level_02.Checked
        .ReservationLevel03 = Me.chk_enabled_level_03.Checked
        .ReservationLevel04 = Me.chk_enabled_level_04.Checked

        .TimeLevel01 = Me.cmb_minutes_level_01.SelectedValue
        .TimeLevel02 = Me.cmb_minutes_level_02.SelectedValue
        .TimeLevel03 = Me.cmb_minutes_level_03.SelectedValue
        .TimeLevel04 = Me.cmb_minutes_level_04.SelectedValue

        If Not (String.IsNullOrEmpty(Me.txt_coin_level_01.Value)) Then
          .CoinInLevel01 = IIf(Convert.ToDecimal(Me.txt_coin_level_01.Value) < 0, 0, Convert.ToDecimal(Me.txt_coin_level_01.Value))
        Else
          .CoinInLevel01 = 0
        End If

        If Not (String.IsNullOrEmpty(Me.txt_coin_level_02.Value)) Then
          .CoinInLevel02 = IIf(Convert.ToDecimal(Me.txt_coin_level_02.Value) < 0, 0, Convert.ToDecimal(Me.txt_coin_level_02.Value))
        Else
          .CoinInLevel02 = 0
        End If

        If Not (String.IsNullOrEmpty(Me.txt_coin_level_03.Value)) Then
          .CoinInLevel03 = IIf(Convert.ToDecimal(Me.txt_coin_level_03.Value) < 0, 0, Convert.ToDecimal(Me.txt_coin_level_03.Value))
        Else
          .CoinInLevel03 = 0
        End If

        If Not (String.IsNullOrEmpty(Me.txt_coin_level_04.Value)) Then
          .CoinInLevel04 = IIf(Convert.ToDecimal(Me.txt_coin_level_04.Value) < 0, 0, Convert.ToDecimal(Me.txt_coin_level_04.Value))
        Else
          .CoinInLevel04 = 0
        End If

        .TypeLevel01 = Me.uc_terminals_group_level_01.SelectedIndex
        .TypeLevel02 = Me.uc_terminals_group_level_02.SelectedIndex
        .TypeLevel03 = Me.uc_terminals_group_level_03.SelectedIndex
        .TypeLevel04 = Me.uc_terminals_group_level_04.SelectedIndex

        .Terminals01 = New List(Of Integer)()
        .Terminals02 = New List(Of Integer)()
        .Terminals03 = New List(Of Integer)()
        .Terminals04 = New List(Of Integer)()

        .TerminalsLevel01.Clear()
        .TerminalsLevel02.Clear()
        .TerminalsLevel03.Clear()
        .TerminalsLevel04.Clear()

        _aux_dataTable = New DataTable()
        _checked_terminal = New TerminalList()
        uc_terminals_group_level_01.GetTerminalsFromControl(_checked_terminal)
        .TerminalList01 = _checked_terminal.ToXml()
        _checked_terminal = New TerminalList()
        _aux_dataTable = uc_terminals_group_level_01.GetTerminalIdSelected(_checked_terminal)
        For Each _row As DataRow In _aux_dataTable.Rows
          .Terminals01.Add(CType(_row("TE_TERMINAL_ID"), Integer))
          .TerminalsLevel01.Add(_row.Item("TE_NAME").ToString())
        Next

        _aux_dataTable = New DataTable()
        _checked_terminal = New TerminalList()
        uc_terminals_group_level_02.GetTerminalsFromControl(_checked_terminal)
        .TerminalList02 = _checked_terminal.ToXml()
        _checked_terminal = New TerminalList()
        _aux_dataTable = uc_terminals_group_level_02.GetTerminalIdSelected(_checked_terminal)
        For Each _row As DataRow In _aux_dataTable.Rows
          .Terminals02.Add(CType(_row("TE_TERMINAL_ID"), Integer))
          .TerminalsLevel02.Add(_row.Item("TE_NAME").ToString())
        Next

        _aux_dataTable = New DataTable()
        _checked_terminal = New TerminalList()
        uc_terminals_group_level_03.GetTerminalsFromControl(_checked_terminal)
        .TerminalList03 = _checked_terminal.ToXml()
        _checked_terminal = New TerminalList()
        _aux_dataTable = uc_terminals_group_level_03.GetTerminalIdSelected(_checked_terminal)
        For Each _row As DataRow In _aux_dataTable.Rows
          .Terminals03.Add(CType(_row("TE_TERMINAL_ID"), Integer))
          .TerminalsLevel03.Add(_row.Item("TE_NAME").ToString())
        Next

        _aux_dataTable = New DataTable()
        _checked_terminal = New TerminalList()
        uc_terminals_group_level_04.GetTerminalsFromControl(_checked_terminal)
        .TerminalList04 = _checked_terminal.ToXml()
        _checked_terminal = New TerminalList()
        _aux_dataTable = uc_terminals_group_level_04.GetTerminalIdSelected(_checked_terminal)
        For Each _row As DataRow In _aux_dataTable.Rows
          .Terminals04.Add(CType(_row("TE_TERMINAL_ID"), Integer))
          .TerminalsLevel04.Add(_row.Item("TE_NAME").ToString())
        Next

      End With
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Checks if the form fields are filled ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    ' At least one option have to be selected
    If Me.chk_reservation.Checked Then
      'No level selected
      If (Not IsThereALevelSelected()) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8529), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call uc_terminals_group_filter.Focus()

        Return False
      End If

      ' If a level is enabled, must select a time
      ' All the combos must have a selected value from the list
      If ((chk_enabled_level_01.Checked AndAlso ((cmb_minutes_level_01.SelectedItem = Nothing) OrElse (cmb_minutes_level_01.SelectedValue = -1))) _
        OrElse (chk_enabled_level_02.Checked AndAlso ((cmb_minutes_level_02.SelectedItem = Nothing) OrElse (cmb_minutes_level_02.SelectedValue = -1))) _
        OrElse (chk_enabled_level_03.Checked AndAlso ((cmb_minutes_level_03.SelectedItem = Nothing) OrElse (cmb_minutes_level_03.SelectedValue = -1))) _
        OrElse (chk_enabled_level_04.Checked AndAlso ((cmb_minutes_level_04.SelectedItem = Nothing) OrElse (cmb_minutes_level_04.SelectedValue = -1)))) _
      Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7509), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call uc_terminals_group_filter.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Private Methods "

  ''' <summary>
  ''' Is there a level selected?
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsThereALevelSelected() As Boolean
    'Check all the level enabled checkboxes and find if there is at least one selected 
    For Each ctrl As Control In Me.grp_terminal_minutes.Controls
      If ctrl.Name.Contains(_CHECKBOX_LEVEL_ENABLED_PREFFIX) AndAlso CType(ctrl, CheckBox).Checked Then
        Return True
      End If
    Next

    Return False
  End Function

  ''' <summary>
  ''' Set the terminals's data into the object
  ''' </summary>
  Private Function SetItemTerminal(Level As HolderLevelType, MaxMinutes As Integer, XML_Terminals As String, List_Terminals As List(Of Integer), Active As Boolean, CoinIn As Decimal) As ReservedTerminal.AccountTimeLevel
    Dim _aux_item As ReservedTerminal.AccountTimeLevel
    _aux_item = New ReservedTerminal.AccountTimeLevel

    'Set all the properties in an item
    _aux_item.LevelType = Level
    _aux_item.Minutes = MaxMinutes
    _aux_item.TerminalList = XML_Terminals
    _aux_item.Terminals = List_Terminals
    _aux_item.Enabled = Active
    _aux_item.CoinIn = CoinIn

    Return _aux_item
  End Function

  ''' <summary>
  ''' Set the labels
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetFormLabels()

    Dim _aux_lbl_minutes As String
    _aux_lbl_minutes = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1351)

    Me.lbl01_minutes.Text = _aux_lbl_minutes
    Me.lbl02_minutes.Text = _aux_lbl_minutes
    Me.lbl03_minutes.Text = _aux_lbl_minutes
    Me.lbl04_minutes.Text = _aux_lbl_minutes

    Me.chk_reservation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8527)
    Me.grp_terminal_minutes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8525)
    Me.lbl_levels.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)
    Me.lbl_time.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8528)
    Me.lbl_terminals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8526)
    Me.lbl_coinin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8531)
    Me.lbl_reservation_time_legend.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8532)
    Me.lbl_coin_in_legend.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8533)

    Me.chk_enabled_level_01.Text = GeneralParam.GetString("PlayerTracking", "Level01.Name")
    If Me.chk_enabled_level_01.Text <> String.Empty Then
      Me.chk_enabled_level_01.Visible = True
      Me.cmb_minutes_level_01.Visible = True
      Me.txt_coin_level_01.Visible = True
    End If

    Me.chk_enabled_level_02.Text = GeneralParam.GetString("PlayerTracking", "Level02.Name")
    If Me.chk_enabled_level_02.Text <> String.Empty Then
      Me.chk_enabled_level_02.Visible = True
      Me.cmb_minutes_level_02.Visible = True
      Me.txt_coin_level_02.Visible = True
    End If

    Me.chk_enabled_level_03.Text = GeneralParam.GetString("PlayerTracking", "Level03.Name")
    If Me.chk_enabled_level_03.Text <> String.Empty Then
      Me.chk_enabled_level_03.Visible = True
      Me.cmb_minutes_level_03.Visible = True
      Me.txt_coin_level_03.Visible = True
    End If

    Me.chk_enabled_level_04.Text = GeneralParam.GetString("PlayerTracking", "Level04.Name")
    If Me.chk_enabled_level_04.Text <> String.Empty Then
      Me.chk_enabled_level_04.Visible = True
      Me.cmb_minutes_level_04.Visible = True
      Me.txt_coin_level_04.Visible = True
    End If

  End Sub

  ''' <summary>
  ''' Set the combo settings
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetTerminalsCombo()

    Me.uc_terminals_group_level_01.SetGroupBoxText = Resource.String("STR_TITLE_GROUPBOX_BOOKING_TERMINAL")
    Me.uc_terminals_group_level_01.ShowGroups = New List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
    Me.uc_terminals_group_level_01.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.PROVIDER)
    Me.uc_terminals_group_level_01.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.ZONE)
    Me.uc_terminals_group_level_01.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.GROUP)
    Me.uc_terminals_group_level_01.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.TERMINALS)
    Me.uc_terminals_group_level_01.SetTerminalList(New TerminalList())
    Me.uc_terminals_group_level_01.SetDisplayMode = uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_01.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_01.Top = chk_enabled_level_01.Top + grp_terminal_minutes.Top
    Me.uc_terminals_group_level_01.HeightOnExpanded = _EXPANDED_HEIGHT

    Me.uc_terminals_group_level_02.SetGroupBoxText = Resource.String("STR_TITLE_GROUPBOX_BOOKING_TERMINAL")
    Me.uc_terminals_group_level_02.ShowGroups = New List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
    Me.uc_terminals_group_level_02.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.PROVIDER)
    Me.uc_terminals_group_level_02.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.ZONE)
    Me.uc_terminals_group_level_02.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.GROUP)
    Me.uc_terminals_group_level_02.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.TERMINALS)
    Me.uc_terminals_group_level_02.SetTerminalList(New TerminalList())
    Me.uc_terminals_group_level_02.SetDisplayMode = uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_02.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_02.Top = chk_enabled_level_02.Top + grp_terminal_minutes.Top
    Me.uc_terminals_group_level_02.HeightOnExpanded = _EXPANDED_HEIGHT

    Me.uc_terminals_group_level_03.SetGroupBoxText = Resource.String("STR_TITLE_GROUPBOX_BOOKING_TERMINAL")
    Me.uc_terminals_group_level_03.ShowGroups = New List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
    Me.uc_terminals_group_level_03.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.PROVIDER)
    Me.uc_terminals_group_level_03.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.ZONE)
    Me.uc_terminals_group_level_03.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.GROUP)
    Me.uc_terminals_group_level_03.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.TERMINALS)
    Me.uc_terminals_group_level_03.SetTerminalList(New TerminalList())
    Me.uc_terminals_group_level_03.SetDisplayMode = uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_03.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_03.Top = chk_enabled_level_03.Top + grp_terminal_minutes.Top
    Me.uc_terminals_group_level_03.HeightOnExpanded = _EXPANDED_HEIGHT

    Me.uc_terminals_group_level_04.SetGroupBoxText = Resource.String("STR_TITLE_GROUPBOX_BOOKING_TERMINAL")
    Me.uc_terminals_group_level_04.ShowGroups = New List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
    Me.uc_terminals_group_level_04.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.PROVIDER)
    Me.uc_terminals_group_level_04.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.ZONE)
    Me.uc_terminals_group_level_04.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.GROUP)
    Me.uc_terminals_group_level_04.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.TERMINALS)
    Me.uc_terminals_group_level_04.SetTerminalList(New TerminalList())
    Me.uc_terminals_group_level_04.SetDisplayMode = uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_level_04.ShowGroupBoxLine = False
    Me.uc_terminals_group_level_04.Top = chk_enabled_level_04.Top + grp_terminal_minutes.Top
    Me.uc_terminals_group_level_04.HeightOnExpanded = _EXPANDED_HEIGHT
  End Sub

  ''' <summary>
  ''' Enables or disables the checked related elements
  ''' </summary>
  ''' <param name="checkbox"></param>
  ''' <param name="combo"></param>
  ''' <param name="txt"></param>
  ''' <param name="uc"></param>
  ''' <remarks></remarks>
  Private Sub CheckedChange(ByVal checkbox As CheckBox, ByVal combo As ComboBox, ByVal txt As uc_entry_field, ByVal uc As uc_terminals_group_filter)
    combo.Enabled = checkbox.Checked
    txt.Enabled = checkbox.Checked
    uc.Enabled = checkbox.Checked
  End Sub

  ''' <summary>
  ''' Enables or disables all the form data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnableDisableAll()
    If (Me.chk_reservation.Checked) Then
      Me.grp_terminal_minutes.Enabled = True
      Me.TextBox_interval.Enabled = True
      Me.cb_another.Enabled = True
      If (Me.chk_enabled_level_01.Checked) Then
        uc_terminals_group_level_01.Enabled = True
      End If
      If (Me.chk_enabled_level_02.Checked) Then
        uc_terminals_group_level_02.Enabled = True
      End If
      If (Me.chk_enabled_level_03.Checked) Then
        uc_terminals_group_level_03.Enabled = True
      End If
      If (Me.chk_enabled_level_04.Checked) Then
        uc_terminals_group_level_04.Enabled = True
      End If
    Else
      Me.grp_terminal_minutes.Enabled = False
      uc_terminals_group_level_01.Enabled = False
      uc_terminals_group_level_02.Enabled = False
      uc_terminals_group_level_03.Enabled = False
      uc_terminals_group_level_04.Enabled = False
      Me.TextBox_interval.Enabled = False
      Me.cb_another.Enabled = False
    End If

  End Sub

  ''' <summary>
  ''' Gets the Coin-in value in a valid way
  ''' </summary>
  ''' <param name="textbox"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetCoinIn(ByVal textbox As uc_entry_field) As Decimal
    If (textbox.Text <> "" And Not String.IsNullOrEmpty(textbox.Value)) Then
      Return Convert.ToDecimal(textbox.Value)
    Else
      Return CDec(0)
    End If
  End Function

  ''' <summary>
  ''' Gets the Minutes value in a valid way
  ''' </summary>
  ''' <param name="combo"></param>
  ''' <returns></returns>
  Private Function GetMaxMinutes(ByVal combo As ComboBox) As Integer
    If ((Not combo.SelectedItem = Nothing) AndAlso (combo.SelectedValue <> -1)) Then
      Return Integer.Parse(combo.SelectedItem)
    Else
      Return CInt(0)
    End If
  End Function

  ''' <summary>
  ''' Loads the terminals list from the object
  ''' </summary>
  ''' <param name="terminals_to_book"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetTerminalList(ByVal terminals_to_book As ReservedTerminal.AccountTimeLevel) As TerminalList
    Dim _terminal_list As TerminalList
    Dim _xml_terminals As System.Xml.XmlDocument

    _terminal_list = New TerminalList()
    _xml_terminals = New System.Xml.XmlDocument()
    _xml_terminals.LoadXml(terminals_to_book.TerminalList)
    _terminal_list.FromXml(terminals_to_book.TerminalList)

    If _xml_terminals.SelectSingleNode("ProviderList").SelectSingleNode("ListType").SelectSingleNode("Type").InnerText = 0 Then
      _terminal_list.ListType = TerminalList.TerminalListType.All
    End If

    Return _terminal_list
  End Function
#End Region

#Region " Events "

  Private Sub txt_level_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txt_coin_level_01.KeyPress, txt_coin_level_02.KeyPress, txt_coin_level_04.KeyPress, txt_coin_level_03.KeyPress
    'Only accept numeric char
    If Not IsNumeric(e.KeyChar) AndAlso e.KeyChar <> ControlChars.Back AndAlso e.KeyChar <> "." Then
      e.KeyChar = String.Empty
      Return
    End If
  End Sub

  Private Sub chk_level_01_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled_level_01.CheckedChanged
    Me.CheckedChange(Me.chk_enabled_level_01, cmb_minutes_level_01, txt_coin_level_01, uc_terminals_group_level_01)
  End Sub

  Private Sub chk_level_02_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled_level_02.CheckedChanged
    Me.CheckedChange(Me.chk_enabled_level_02, cmb_minutes_level_02, txt_coin_level_02, uc_terminals_group_level_02)
  End Sub

  Private Sub chk_level_03_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled_level_03.CheckedChanged
    Me.CheckedChange(Me.chk_enabled_level_03, cmb_minutes_level_03, txt_coin_level_03, uc_terminals_group_level_03)
  End Sub

  Private Sub chk_level_04_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled_level_04.CheckedChanged
    Me.CheckedChange(Me.chk_enabled_level_04, cmb_minutes_level_04, txt_coin_level_04, uc_terminals_group_level_04)
  End Sub

  Private Sub chk_reservation_CheckedChanged(sender As Object, e As EventArgs) Handles chk_reservation.CheckedChanged
    Me.EnableDisableAll()
  End Sub

#End Region

End Class ' frm_booking_terminal_config