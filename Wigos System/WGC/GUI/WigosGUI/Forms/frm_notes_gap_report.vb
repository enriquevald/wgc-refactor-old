'-------------------------------------------------------------------
' Copyright � 2007-2013 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_notes_gap_report
' DESCRIPTION:   Square of notes delivered.
' AUTHOR:        Ignasi Carre�o
' CREATION DATE: 08-MAY-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-MAY-2013  ICS    Initial version.
'
' -----------  ------ -----------------------------------------------

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc

Public Class frm_notes_gap_report
  Inherits frm_base_sel

#Region " Constants "
  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_TERMINAL As Integer = 1
  Private Const GRID_COLUMN_FACE_VALUE As Integer = 2
  Private Const GRID_COLUMN_NUM_COLLECTED As Integer = 3
  Private Const GRID_COLUMN_SUM_COLLECTED As Integer = 4
  Private Const GRID_COLUMN_NUM_EXPECTED As Integer = 5
  Private Const GRID_COLUMN_SUM_EXPECTED As Integer = 6
  Private Const GRID_COLUMN_NUM_MISMATCH As Integer = 7
  Private Const GRID_COLUMN_SUM_MISMATCH As Integer = 8

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 9

  ' Sql Columns
  Private Const SQL_COLUMN_FACE_VALUE As Integer = 0
  Private Const SQL_COLUMN_NUM_COLLECTED As Integer = 1
  Private Const SQL_COLUMN_NUM_EXPECTED As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3

  Private Const COLOR_GAP_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_GAP_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_PENDING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_PENDING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_COMPLETED_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_COMPLETED_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

  ' counters
  Private Const COUNTER_COMPLETED As Integer = 1
  Private Const COUNTER_PENDING As Integer = 2
  Private Const COUNTER_GAP As Integer = 3
  Private Const MAX_COUNTERS As Integer = 3


#End Region ' Constants

#Region " Members "

  Private m_cashier_session_id As Long
  Private m_cashier_session_name As String
  Private m_counter_list(MAX_COUNTERS) As Integer
  Private m_collection_by_terminal As Boolean
  Private m_status As String
  Private m_first_time As Boolean = True

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_NOTES_GAP_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Notes gap report
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1966)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.chk_completed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.chk_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1973)
    Me.chk_gap.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)

    Me.lbl_color_completed.BackColor = GetColor(COLOR_COMPLETED_BACK)
    Me.lbl_color_pending.BackColor = GetColor(COLOR_PENDING_BACK)
    Me.lbl_color_gap.BackColor = GetColor(COLOR_GAP_BACK)

    ' Set cashier session name
    Me.lbl_cs_name.Value = m_cashier_session_name

    ' Define grid columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Performs a first search
    Call GUI_ExecuteQuery()

    m_first_time = False

  End Sub ' GUI_InitControl

  ' PURPOSE: What to do when no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_NoDataFound()

    If Not m_first_time Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End If
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

  End Sub ' GUI_NoDataFound

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), Me.m_cashier_session_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971), m_status)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _all As Boolean
    Dim _no_one As Boolean

    m_status = ""
    _all = True
    _no_one = True

    If chk_completed.Checked Then
      m_status = m_status & chk_completed.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_pending.Checked Then
      m_status = m_status & chk_pending.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_gap.Checked Then
      m_status = m_status & chk_gap.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_status.Length <> 0 Then
      m_status = Strings.Left(m_status, Len(m_status) - 2)
    End If

    If _no_one Or _all Then
      m_status = GLB_NLS_GUI_ALARMS.GetString(277)
    End If


  End Sub ' GUI_ReportUpdateFilters()

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _face_value As Decimal
    Dim _num_collected As Integer
    Dim _sum_collected As Decimal
    Dim _num_expected As Integer
    Dim _sum_expected As Decimal
    Dim _num_mismatch As Integer
    Dim _sum_mismatch As Decimal
    Dim _terminal As String

    _terminal = ""
    _face_value = DbRow.Value(SQL_COLUMN_FACE_VALUE)
    _num_collected = DbRow.Value(SQL_COLUMN_NUM_COLLECTED)
    _num_expected = DbRow.Value(SQL_COLUMN_NUM_EXPECTED)
    _num_mismatch = _num_collected - _num_expected
    _sum_collected = _face_value * _num_collected
    _sum_expected = _face_value * _num_expected
    _sum_mismatch = _sum_collected - _sum_expected

    ' Terminal name
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_NAME)) Then
      _terminal = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = _terminal

    ' Check if there is a collection by terminal
    If Not m_collection_by_terminal AndAlso _
       Not String.IsNullOrEmpty(_terminal) Then
      m_collection_by_terminal = True
    End If

    ' Note face value
    If _face_value = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FACE_VALUE).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FACE_VALUE).Value = GUI_FormatCurrency(_face_value, 2)
    End If

    ' Num collected notes
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = GUI_FormatNumber(_num_collected, 0)

    ' Sum collected notes
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(_sum_collected, 2)

    If _num_mismatch = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_MISMATCH).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = GUI_FormatNumber(_num_expected, 0)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(_sum_expected, 2)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_MISMATCH).Value = GUI_FormatNumber(_num_mismatch, 0)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).Value = GUI_FormatCurrency(_sum_mismatch, 2)
    End If

    ' Set row color and update counters
    Select Case _num_mismatch
      Case Is = 0
        Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_COMPLETED_BACK)
        Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_COMPLETED_FORE)
        m_counter_list(COUNTER_COMPLETED) = m_counter_list(COUNTER_COMPLETED) + 1
      Case Is < 0
        Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_PENDING_BACK)
        Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_PENDING_FORE)
        m_counter_list(COUNTER_PENDING) = m_counter_list(COUNTER_PENDING) + 1
      Case Is > 0
        Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_GAP_BACK)
        Me.Grid.Row(RowIndex).ForeColor = GetColor(COLOR_GAP_FORE)
        m_counter_list(COUNTER_GAP) = m_counter_list(COUNTER_GAP) + 1
    End Select

    Return True

  End Function ' GUI_Setup_Row

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call GUI_StyleSheet()

    ' Reset counters
    m_counter_list(COUNTER_COMPLETED) = 0
    m_counter_list(COUNTER_PENDING) = 0
    m_counter_list(COUNTER_GAP) = 0

    ' Reset collection by terminal flag
    m_collection_by_terminal = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As System.Text.StringBuilder
    Dim _str_where As String

    _str_sql = New System.Text.StringBuilder()
    _str_where = GetSqlWhere()

    _str_sql.AppendLine(" DECLARE @collect_by_terminal as int ")
    _str_sql.AppendLine(" DECLARE @collected_table as TABLE( AMOUNT money, COLLECTED int, EXPECTED int, TERMINAL int ) ")
    _str_sql.AppendLine("  ")
    _str_sql.AppendLine(" SET @collect_by_terminal = (SELECT   COUNT(*) ")
    _str_sql.AppendLine("                               FROM   MONEY_COLLECTIONS ")
    _str_sql.AppendLine("                              WHERE   MC_TERMINAL_ID IS NOT NULL ")
    _str_sql.AppendLine("                                AND   MC_CASHIER_SESSION_ID = " & m_cashier_session_id & ")")
    _str_sql.AppendLine("  ")
    _str_sql.AppendLine(" IF @collect_by_terminal > 0  ")
    _str_sql.AppendLine(" BEGIN ")
    _str_sql.AppendLine(" INSERT INTO   @collected_table (AMOUNT, COLLECTED, EXPECTED, TERMINAL ) ")
    _str_sql.AppendLine("      SELECT   MCD_FACE_VALUE AS AMOUNT ")
    _str_sql.AppendLine("             , SUM( MCD_NUM_COLLECTED ) AS COLLECTED ")
    _str_sql.AppendLine("             , ( SELECT   COUNT(*) ")
    _str_sql.AppendLine("                   FROM   TERMINAL_MONEY WITH(INDEX(IX_tm_cashier_session_amount_terminal)) ")
    _str_sql.AppendLine("                  WHERE   TM_AMOUNT = MCD_FACE_VALUE ")
    _str_sql.AppendLine("                    AND   TM_CASHIER_SESSION_ID = " & m_cashier_session_id)
    _str_sql.AppendLine("                    AND   TM_INTO_ACCEPTOR = 0 ")
    _str_sql.AppendLine("                    AND   TM_TERMINAL_ID = MC_TERMINAL_ID ")
    _str_sql.AppendLine("               ) AS EXPECTED ")
    _str_sql.AppendLine("             , MC_TERMINAL_ID ")
    _str_sql.AppendLine("        FROM   MONEY_COLLECTION_DETAILS ")
    _str_sql.AppendLine("  INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID ")
    _str_sql.AppendLine("   LEFT JOIN   TERMINALS ON MC_TERMINAL_ID = TE_TERMINAL_ID ")
    _str_sql.AppendLine("       WHERE   MC_CASHIER_SESSION_ID = " & m_cashier_session_id)
    _str_sql.AppendLine("    GROUP BY   MC_TERMINAL_ID, MCD_FACE_VALUE ")
    _str_sql.AppendLine("  ")
    _str_sql.AppendLine("   SELECT   AMOUNT, COLLECTED, EXPECTED, TE_NAME AS TERMINAL ")
    _str_sql.AppendLine("     FROM   @collected_table ")
    _str_sql.AppendLine("LEFT JOIN   TERMINALS ON TERMINAL = TE_TERMINAL_ID ")
    _str_sql.AppendLine("        " & _str_where)
    _str_sql.AppendLine("    UNION ")
    _str_sql.AppendLine("   SELECT   AMOUNT, COLLECTED, EXPECTED, TERMINAL ")
    _str_sql.AppendLine("     FROM  ( ")
    _str_sql.AppendLine("            SELECT   TM_AMOUNT AS AMOUNT ")
    _str_sql.AppendLine("                   , 0 AS COLLECTED ")
    _str_sql.AppendLine("                   , COUNT( TM_AMOUNT ) AS EXPECTED ")
    _str_sql.AppendLine("                   , TE_NAME AS TERMINAL ")
    _str_sql.AppendLine("              FROM   TERMINAL_MONEY ")
    _str_sql.AppendLine("         LEFT JOIN   TERMINALS ON TM_TERMINAL_ID = TE_TERMINAL_ID ")
    _str_sql.AppendLine("             WHERE   TM_CASHIER_SESSION_ID = " & m_cashier_session_id)
    _str_sql.AppendLine("               AND   TM_INTO_ACCEPTOR = 0 ")
    _str_sql.AppendLine("               AND   NOT EXISTS ( ")
    _str_sql.AppendLine("                                 SELECT  1 ")
    _str_sql.AppendLine("                                   FROM  @collected_table ")
    _str_sql.AppendLine("                                  WHERE  AMOUNT = TM_AMOUNT ")
    _str_sql.AppendLine("                                    AND  TERMINAL = TM_TERMINAL_ID ")
    _str_sql.AppendLine("                                 ) ")
    _str_sql.AppendLine("          GROUP BY  TE_NAME, TM_AMOUNT ")
    _str_sql.AppendLine("           ) NOT_COLLECTED ")
    _str_sql.AppendLine("       " & _str_where)
    _str_sql.AppendLine(" END ")
    _str_sql.AppendLine(" ELSE ")
    _str_sql.AppendLine(" BEGIN ")
    _str_sql.AppendLine("   INSERT INTO   @collected_table ( AMOUNT, COLLECTED, EXPECTED ) ")
    _str_sql.AppendLine("        SELECT   MCD_FACE_VALUE AS AMOUNT ")
    _str_sql.AppendLine("               , SUM( MCD_NUM_COLLECTED ) AS COLLECTED")
    _str_sql.AppendLine("               , ( SELECT   COUNT(*) ")
    _str_sql.AppendLine("                     FROM   TERMINAL_MONEY WITH(INDEX(IX_tm_cashier_session_amount_terminal)) ")
    _str_sql.AppendLine("                    WHERE   TM_CASHIER_SESSION_ID = " & m_cashier_session_id)
    _str_sql.AppendLine("                      AND   TM_AMOUNT = MCD_FACE_VALUE ")
    _str_sql.AppendLine("                      AND   TM_INTO_ACCEPTOR = 0 ")
    _str_sql.AppendLine("                  ) AS EXPECTED ")
    _str_sql.AppendLine("          FROM   MONEY_COLLECTION_DETAILS ")
    _str_sql.AppendLine("    INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID ")
    _str_sql.AppendLine("         WHERE   MC_CASHIER_SESSION_ID = " & m_cashier_session_id)
    _str_sql.AppendLine("      GROUP BY   MCD_FACE_VALUE ")
    _str_sql.AppendLine("    ")
    _str_sql.AppendLine("   SELECT   AMOUNT, COLLECTED, EXPECTED, '' FROM @collected_table " & _str_where)
    _str_sql.AppendLine("    UNION ")
    _str_sql.AppendLine("   SELECT   AMOUNT, COLLECTED, EXPECTED, '' ")
    _str_sql.AppendLine("     FROM   ( ")
    _str_sql.AppendLine("             SELECT   TM_AMOUNT AS AMOUNT ")
    _str_sql.AppendLine("                    , 0 AS COLLECTED ")
    _str_sql.AppendLine("                    , COUNT( TM_AMOUNT ) AS EXPECTED ")
    _str_sql.AppendLine("               FROM   TERMINAL_MONEY ")
    _str_sql.AppendLine("              WHERE   TM_CASHIER_SESSION_ID = " & m_cashier_session_id)
    _str_sql.AppendLine("                AND   NOT EXISTS ( ")
    _str_sql.AppendLine("                                  SELECT   1 ")
    _str_sql.AppendLine("                                    FROM   @collected_table ")
    _str_sql.AppendLine("                                   WHERE   AMOUNT = TM_AMOUNT ")
    _str_sql.AppendLine("                                 ) ")
    _str_sql.AppendLine("                AND   TM_INTO_ACCEPTOR = 0 ")
    _str_sql.AppendLine("           GROUP BY   TM_AMOUNT ")
    _str_sql.AppendLine("           ) NOT_COLLECTED ")
    _str_sql.AppendLine("        " & _str_where)
    _str_sql.AppendLine(" END ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform final processing for the grid data
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    ' Update counters
    Grid.Counter(COUNTER_COMPLETED).Value = m_counter_list(COUNTER_COMPLETED)
    Grid.Counter(COUNTER_PENDING).Value = m_counter_list(COUNTER_PENDING)
    Grid.Counter(COUNTER_GAP).Value = m_counter_list(COUNTER_GAP)

    ' Show terminal column if there is a collection by terminal
    With Me.Grid
      If (m_collection_by_terminal) Then
        .Column(GRID_COLUMN_TERMINAL).Width = 2000
      Else
        .Column(GRID_COLUMN_TERMINAL).Width = 0
      End If
    End With

  End Sub ' GUI_AfterLastRow


#End Region ' Overrides

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' Completed Counter
      .Counter(COUNTER_COMPLETED).Visible = True
      .Counter(COUNTER_COMPLETED).BackColor = GetColor(COLOR_COMPLETED_BACK)
      .Counter(COUNTER_COMPLETED).ForeColor = GetColor(COLOR_COMPLETED_FORE)

      ' Pending Counter
      .Counter(COUNTER_PENDING).Visible = True
      .Counter(COUNTER_PENDING).BackColor = GetColor(COLOR_PENDING_BACK)
      .Counter(COUNTER_PENDING).ForeColor = GetColor(COLOR_PENDING_FORE)

      ' Gap Counter
      .Counter(COUNTER_GAP).Visible = True
      .Counter(COUNTER_GAP).BackColor = GetColor(COLOR_GAP_BACK)
      .Counter(COUNTER_GAP).ForeColor = GetColor(COLOR_GAP_FORE)

      .Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' TERMINAL
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576)
      .Column(GRID_COLUMN_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)
      .Column(GRID_COLUMN_TERMINAL).Width = 2000
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' NOTE FACE VALUE 
      .Column(GRID_COLUMN_FACE_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576)
      .Column(GRID_COLUMN_FACE_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1967)
      .Column(GRID_COLUMN_FACE_VALUE).Width = 1200
      .Column(GRID_COLUMN_FACE_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' NUM NOTES COLLECTED
      .Column(GRID_COLUMN_NUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(GRID_COLUMN_NUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(GRID_COLUMN_NUM_COLLECTED).Width = 1200
      .Column(GRID_COLUMN_NUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES COLLECTED
      .Column(GRID_COLUMN_SUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(GRID_COLUMN_SUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(GRID_COLUMN_SUM_COLLECTED).Width = 1700
      .Column(GRID_COLUMN_SUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES EXPECTED
      .Column(GRID_COLUMN_NUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_NUM_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(GRID_COLUMN_NUM_EXPECTED).Width = 1200
      .Column(GRID_COLUMN_NUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES EXPECTED
      .Column(GRID_COLUMN_SUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_SUM_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(GRID_COLUMN_SUM_EXPECTED).Width = 1700
      .Column(GRID_COLUMN_SUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES MISMATCH
      .Column(GRID_COLUMN_NUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(499)
      .Column(GRID_COLUMN_NUM_MISMATCH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(GRID_COLUMN_NUM_MISMATCH).Width = 1200
      .Column(GRID_COLUMN_NUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES MISMATCH
      .Column(GRID_COLUMN_SUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(499)
      .Column(GRID_COLUMN_SUM_MISMATCH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(GRID_COLUMN_SUM_MISMATCH).Width = 1700
      .Column(GRID_COLUMN_SUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_completed.Checked = True
    Me.chk_pending.Checked = True
    Me.chk_gap.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    ' Filter status
    If Me.chk_completed.Checked Then
      _str_where = " collected = expected OR "
    End If

    If Me.chk_pending.Checked Then
      _str_where &= " collected < expected OR "
    End If

    If Me.chk_gap.Checked Then
      _str_where &= " collected > expected OR "
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & Strings.Left(_str_where, Len(_str_where) - 3)
    End If

    Return _str_where

  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowModalForEdit(ByVal session_id As Integer, ByVal session_name As String)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_cashier_session_id = session_id
    Me.m_cashier_session_name = session_name
    Me.m_collection_by_terminal = False

    Me.Display(True)

  End Sub ' ShowForEdit

#End Region ' Public Functions

End Class