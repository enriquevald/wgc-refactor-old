'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_screen_selector
'
' DESCRIPTION:   Screen selection form
' AUTHOR:        Luis Mesa
' CREATION DATE: 25-NOV-2014
'
' REVISION HISTORY:
'
' Date          Author   Description
' -----------   ------   -----------------------------------------
' 25-NOV-2014   LEM      First Release.
'------------------------------------------------------------------- 

Option Strict Off
Option Explicit On

Imports System
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_screen_selector
  Inherits frm_base_edit

#Region "Members"

  Public Const HotKeyCode As Keys = Keys.F12

  Private m_screens() As Form
  Private m_default_back_color As Color
  Private m_selected_index As Int32

#End Region

#Region "Publics"

  Public Sub New()

    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    m_screens = Nothing
    m_default_back_color = SystemColors.Info

    m_selected_index = 0

  End Sub

  Public Sub Open(ByVal MainForm As Form, ByVal Forward As Boolean)

    Me.m_screens = MainForm.MdiChildren

    If m_screens Is Nothing OrElse m_screens.Length = 0 Then

      Return
    End If

    Me.m_selected_index = 0
    For _idx As Int32 = 0 To m_screens.Length - 1
      If m_screens(_idx).Name = MainForm.ActiveMdiChild.Name Then
        Me.m_selected_index = _idx
      End If
    Next

    Call LoadItems()
    Call MatchSizeItems()

    Call SelectNextItem(Forward)

    Me.ShowDialog()

  End Sub

#End Region

#Region "Overrides"

  Protected Overrides Sub GUI_InitControls()

    Me.HideButtons()

    Me.flp_list.AutoSize = True
    Me.lbl_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5761)

  End Sub

  Protected Overrides Sub GUI_FirstActivation()
    ' It's necessary to ignore read permission checking
    ' Do nothing
  End Sub

  Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
    Get
      Dim cp As CreateParams = MyBase.CreateParams
      cp.ClassStyle = cp.ClassStyle Or &H20000
      Return cp
    End Get
  End Property

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    Call OnSelectScreen(m_selected_index)
  End Sub

#End Region

#Region "Privates"

  Private Sub LoadItems()
    Dim _view As Control
    Dim _id As Int32

    flp_list.AutoSize = True
    flp_list.Controls.Clear()

    _id = 1
    For Each _item As frm_base In m_screens
      _view = CreateView(_id, _item.FormTitle)
      _id += 1

      AddHandler _view.Click, AddressOf ItemClickEvent

      flp_list.Controls.Add(_view)
    Next
  End Sub

  Private Sub MatchSizeItems()
    Dim _width As Int32

    _width = flp_list.Width
    For Each _item As Control In flp_list.Controls
      _item.AutoSize = False
      _item.Width = _width
    Next

    pnl_line.Width = _width
  End Sub

  Private Sub SelectNextItem(ByVal Forward As Boolean)

    If m_selected_index >= 0 AndAlso m_selected_index < flp_list.Controls.Count Then
      Call UnFocusItem(m_selected_index)
    End If

    m_selected_index = (m_selected_index + IIf(Forward, 1, -1)) Mod flp_list.Controls.Count
    If m_selected_index < 0 Then
      m_selected_index = flp_list.Controls.Count - 1
    End If

    Call FocusItem(m_selected_index)

  End Sub

  Private Sub FocusItem(ByVal Index As Int32)
    Dim _item As Control

    _item = flp_list.Controls(Index)
    _item.BackColor = SystemColors.ControlDark

    _item.Focus()
  End Sub

  Private Sub UnFocusItem(ByVal Index As Int32)
    Dim _item As Control

    _item = flp_list.Controls(Index)
    _item.BackColor = m_default_back_color
  End Sub

  Private Sub OnSelectScreen(ByVal Index As Int32)
    If m_screens IsNot Nothing Then
      If m_screens(Index).WindowState = FormWindowState.Minimized Then
        m_screens(Index).WindowState = FormWindowState.Normal
      End If

      m_screens(Index).BringToFront()
    End If
  End Sub

  Private Function CreateView(ByVal Id As Int32, ByVal Title As String) As Control
    Dim _label As Label

    _label = New Label()
    _label.FlatStyle = FlatStyle.Popup
    _label.Text = Id & " " & Title
    _label.TextAlign = ContentAlignment.MiddleLeft
    _label.AutoSize = True
    _label.BackColor = Color.Transparent
    _label.BorderStyle = BorderStyle.None

    Return _label
  End Function

#End Region

#Region "Events"

  Private Sub ItemClickEvent(ByVal sender As Object, ByVal e As EventArgs)
    m_selected_index = flp_list.Controls.GetChildIndex(CType(sender, Control))

    Me.Close()
  End Sub

  Private Sub KeyDownEvent(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

    If e.Control Then

      Select Case e.KeyCode
        Case Keys.F12
          Call SelectNextItem(Not e.Shift)

        Case Keys.Up
          Call SelectNextItem(False)

        Case Keys.Down
          Call SelectNextItem(True)

        Case Else
          Return

      End Select

    Else
      Me.Close()

    End If

  End Sub

  Private Sub KeyUpEvent(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp

    If Not e.Control Then
      Me.Close()
    End If

  End Sub

  Private Sub MouseWheelEvent(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MyBase.MouseWheel
    Call SelectNextItem(e.Delta < 0)
  End Sub

#End Region

End Class