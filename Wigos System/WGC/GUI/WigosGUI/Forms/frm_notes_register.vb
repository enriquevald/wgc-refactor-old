'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_notes_register
' DESCRIPTION:   Collect money of terminals
' AUTHOR:        Susana Sams�
' CREATION DATE: 25-ABR-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-ABR-2012 SSC    Initial version
' 18-MAY-2012 RCI    Redesign of functionalities
' 18-MAY-2012 DDM    Changed Log.Exception, Log.Warning and Log.Error by Common_LoggerMsg
' 22-MAY-2012 ACC    Add code and functions for Acceptors funcionality. Change Auditor code.
' 13-FEB-2013 HBB & MPO    Added functionality of one cashier session for each PromoBOX.
' 28-FEB-2013 LEM    Removed voucher for NACardCashDeposit
' 26-APR-2013 HBB & ICS    Added functionality of one cashier session for each SYS-Acceptor.
' 03-MAY-2013 ICS    Fixed collection by terminal and all has been adapted for working with one mobile bank per terminal. 
' 18-MAR-2014 ICS    Added functionality to update money_collection with expected and collected bills amounts
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_notes_register
  Inherits frm_base_edit

#Region "Constants"

  Private Const RELATION_COLLECTION_TERMINALMONEY As String = "Collection_TerminalMoney"
  Private Const RELATION_COLLECTION_DETAILS As String = "Collection_Details"

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 2
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_AMOUNT As Integer = 0
  Private Const GRID_COLUMN_NUM_COLLECTED As Integer = 1

#End Region

#Region " Enums "

  Private Enum ENUM_REPORT_TYPE
    BY_TERMINAL = 0
    BY_FACE_AND_TERMINAL = 1
  End Enum

  Private Enum ENUM_TABLES
    COLLECTION = 0
    COLLECTION_DETAILS = 1
    TERMINAL_MONEY = 2
  End Enum

#End Region

#Region " Members "

  Private m_data_table_grid_values As DataTable
  Private m_ds_money_collections As DataSet = Nothing
  Private m_collection_by_terminal As Boolean
  Private m_str_terminal_id As String
  Private m_terminal_name As String
  Private m_cashier_session_id As Long
  Private m_gu_user_type As GU_USER_TYPE
  Private m_cashier_terminal_id As Int32
  Private m_dt_terminal_totals As DataTable

#End Region

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_NOTES_REGISTER
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If
  End Sub 'GUI_Closing

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Money Collections
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(589)

    Me.gb_num_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(667)
    Me.gb_terminal_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(668)

    Me.gb_num_notes.Enabled = True
    Me.gb_terminal_options.Enabled = True

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    ' Configure Data Grid
    Call GUI_StyleView()

    ' Providers - Terminals
    ' Read CollectionByTerminal from GeneralParams
    Me.m_collection_by_terminal = WSI.Common.Misc.ReadGeneralParams("NoteAcceptor", "CollectionByTerminal")

    Call Me.uc_pr_list.Init(WSI.Common.Misc.AcceptorTerminalTypeList, uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL)

    If Not m_collection_by_terminal Then
      Me.uc_pr_list.Visible = False

      Me.gb_terminal_options.Visible = False
      Me.Width = Me.Width - 350

      Me.gb_num_notes.Height = Me.gb_num_notes.Height - 45
      Me.Height = Me.Height - 45

      Me.CenterToScreen()
    End If

    Call SetDefaultValues()

  End Sub 'GUI_InitControls

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = dg_collect_list

  End Sub 'GUI_SetInitialFocus

  Private Sub GUI_StyleView()
    With Me.dg_collect_list

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      ' Grid Columns
      'AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_AMOUNT).Header.Text = GLB_NLS_GUI_INVOICING.GetString(443)
      .Column(GRID_COLUMN_AMOUNT).Width = 2500
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_AMOUNT).Editable = False

      'NUM COLLECTED
      .Column(GRID_COLUMN_NUM_COLLECTED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NUM_COLLECTED).Header.Text = GLB_NLS_GUI_INVOICING.GetString(317)
      .Column(GRID_COLUMN_NUM_COLLECTED).Width = 2500
      .Column(GRID_COLUMN_NUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NUM_COLLECTED).Editable = True
      .Column(GRID_COLUMN_NUM_COLLECTED).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_NUM_COLLECTED).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_NUM_COLLECTED).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 5, 0)

    End With

  End Sub ' GUI_StyleView

  ' PURPOSE: Create DataGridViews with SQL queries, and show info in screen.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _str_bank_notes As String
    Dim _str_bank_notes_array() As String
    Dim _row_note As DataRow
    Dim _idx As Integer

    Try
      'Get the amounts list of general params
      _str_bank_notes = WSI.Common.Misc.ReadGeneralParams("NoteAcceptor", "BankNotes").Trim()
      _str_bank_notes_array = _str_bank_notes.Split(";")

      m_data_table_grid_values = New DataTable("COLLECTED_NOTES")
      m_data_table_grid_values.Columns.Add("AMOUNT", Type.GetType("System.String")).Unique = True
      m_data_table_grid_values.Columns.Add("NUM_COLLECTED", Type.GetType("System.Int32"))

      For Each _note As String In _str_bank_notes_array
        _row_note = m_data_table_grid_values.NewRow()
        _row_note.Item("AMOUNT") = GUI_FormatCurrency(_note, 2)
        m_data_table_grid_values.Rows.Add(_row_note)
      Next

      'Fill datagrid
      dg_collect_list.Enabled = True
      _idx = 0
      For Each _row As DataRow In m_data_table_grid_values.Rows
        dg_collect_list.AddRow()
        Me.dg_collect_list.Cell(_idx, 0).Value = _row("AMOUNT")
        _idx += 1
      Next

    Catch _ex As Exception
      ' Do nothing
      Debug.WriteLine(_ex.Message)

    Finally
    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        Call GUI_SaveChanges()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#End Region

#Region " Private Functions "

  ' PURPOSE: Save DataGridView data to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Private Sub GUI_SaveChanges()

    Dim _idx As Integer
    Dim _row_note As DataRow
    Dim _amount_to_deposit As Currency
    Dim _total_amount As Currency
    Dim _save_changes As Boolean
    Dim _terminal_id As Integer

    Try
      m_data_table_grid_values = New DataTable("COLLECTED_NOTES")
      m_data_table_grid_values.Columns.Add("AMOUNT", Type.GetType("System.String")).Unique = True
      m_data_table_grid_values.Columns.Add("NUM_COLLECTED", Type.GetType("System.Int32"))

      ' Get collected amount for each note face
      For _idx = 0 To Me.dg_collect_list.NumRows - 1
        _row_note = m_data_table_grid_values.NewRow()
        _row_note("AMOUNT") = dg_collect_list.Cell(_idx, GRID_COLUMN_AMOUNT).Value

        If dg_collect_list.Cell(_idx, GRID_COLUMN_NUM_COLLECTED).Value <> "" Then
          _row_note("NUM_COLLECTED") = GUI_ParseNumber(dg_collect_list.Cell(_idx, GRID_COLUMN_NUM_COLLECTED).Value)
        Else
          _row_note("NUM_COLLECTED") = 0
        End If

        m_data_table_grid_values.Rows.Add(_row_note)
      Next

      If IsNothing(m_data_table_grid_values) Then
        Exit Sub
      End If

      ' Check some cell has a value
      If m_data_table_grid_values.Select("NUM_COLLECTED <= 0").Length = dg_collect_list.NumRows Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(640), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
        Exit Sub
      End If

      'If collection type by terminal, one terminal must be selected
      If m_collection_by_terminal Then
        If Me.uc_pr_list.GetTerminalIdListSelected() Is Nothing Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1411), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
          Exit Sub
        End If
      End If

      Using _db_trx As DB_TRX = New DB_TRX()

        m_str_terminal_id = ""
        m_terminal_name = ""
        If m_collection_by_terminal Then
          _terminal_id = Me.uc_pr_list.GetTerminalIdListSelected()(0)
          m_str_terminal_id = _terminal_id.ToString()
          If Not WSI.Common.Misc.GetTerminalName(_terminal_id, m_terminal_name, _db_trx.SqlTransaction) Then
            Throw New Exception("Error. GetTerminalName. Id: " + _terminal_id + ".")
          End If
        End If

        ' Collect Money into DataSet
        If Not CollectMoney(_db_trx.SqlTransaction) Then
          Throw New Exception("Error. CollectMoney")
        End If

        _save_changes = ConfirmChanges()

        If Not _save_changes Then
          Return
        End If

        ' Insert into MONEY_COLLECTIONS
        If Not InsertMoneyCollection(_db_trx.SqlTransaction) Then
          Throw New Exception("Error. InsertMoneyCollection")
        End If

        ' Update TERMINAL_MONEY
        If Not UpdateTerminalMoney(_db_trx.SqlTransaction) Then
          Throw New Exception("Error. UpdateTerminalMoney")
        End If

        ' Insert into MONEY_COLLECTION_DETAILS
        If Not InsertMoneyCollectionDetails(_db_trx.SqlTransaction) Then
          Throw New Exception("Error. InsertMoneyCollectionDetails")
        End If

        _total_amount = 0

        'Deposit Mobile Bank
        For Each _row As DataRow In m_dt_terminal_totals.Rows
          _amount_to_deposit = CDec(_row("TOTAL_AMOUNT"))

          ' Only for terminals that have collected notes
          If CDec(_amount_to_deposit) > 0 Then
            _terminal_id = CInt(_row("TERMINAL_ID"))

            If Not NACardCashDeposit(_amount_to_deposit, _terminal_id, _db_trx.SqlTransaction) Then
              Throw New Exception("Error. NACardCashDeposit")
            End If
            _total_amount = CDec(_total_amount) + CDec(_amount_to_deposit)
          End If
        Next

        _db_trx.Commit()

      End Using ' Using _db_trx

      Call GUI_WriteAuditoryChanges(_total_amount)

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1675), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _ex.Message)

    Finally
      If _save_changes Then
        For _idx = 0 To Me.dg_collect_list.NumRows - 1
          Me.dg_collect_list.Cell(_idx, GRID_COLUMN_NUM_COLLECTED).Value = ""
        Next
        If m_collection_by_terminal Then
          Call SetDefaultValues()
        Else
          Call Me.Close()
        End If
      End If
    End Try

  End Sub 'GUI_SaveChanges

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_collect_list.ContainsFocus Then
      Return Me.dg_collect_list.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Call Me.uc_pr_list.SetDefaultValues()

  End Sub 'SetDefaultValues

  ' PURPOSE: Ask user to discard changes or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Discard changes. False: Otherwise
  '
  Private Function DiscardChanges() As Boolean

    For _idx As Int32 = 0 To Me.dg_collect_list.NumRows - 1
      If dg_collect_list.Cell(_idx, GRID_COLUMN_NUM_COLLECTED).Value <> "" Then
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return False
        Else
          Return True
        End If
      End If
    Next

    Return True
  End Function 'DiscardChanges

  ' PURPOSE: Write auditory changes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - AmountToDeposit
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_WriteAuditoryChanges(ByVal AmountToDeposit As Currency)
    Dim _rows_grid As DataRow()
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _name As String
    Dim _value As String
    Dim _total_by_face As Decimal
    Dim _amount As Decimal
    Dim _num_collected As Int32

    _rows_grid = m_data_table_grid_values.Select("NUM_COLLECTED > 0")

    If _rows_grid.Length = 0 Then
      Return
    End If

    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_ACCEPTORS)

    ' Set Parameter
    If m_collection_by_terminal Then
      Call _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(725), GLB_NLS_GUI_PLAYER_TRACKING.GetString(725) & " " & m_terminal_name)
    Else
      Call _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(589), GLB_NLS_GUI_PLAYER_TRACKING.GetString(589))
    End If

    Call _auditor.SetField(0, GUI_FormatCurrency(AmountToDeposit), GLB_NLS_GUI_PLAYER_TRACKING.GetString(685))

    For Each _row As DataRow In _rows_grid

      _amount = _row("AMOUNT")
      _num_collected = _row("NUM_COLLECTED")
      _total_by_face = _amount * _num_collected

      _name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(679) + GUI_FormatCurrency(_amount, 2)
      _value = GUI_FormatNumber(_num_collected, 0) & ", Total = " & GUI_FormatCurrency(_total_by_face, 2)

      Call _auditor.SetField(0, _value, _name)
    Next

    _auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)

  End Sub 'GUI_WriteAuditoryChanges

  ' PURPOSE: Get the query string for Terminal_money select.
  '         
  ' PARAMS:
  '    - INPUT: 
  '           - ReportType (by face, by terminal or both of them)
  '           - TerminalId: If report type is by face and terminal, select one terminal
  '
  '    - OUTPUT:
  '           - None
  '
  'RETURNS:
  '       - String (query)
  '
  Private Function GetStringSQL(ByVal ReportType As ENUM_REPORT_TYPE, ByVal TerminalId As Int64) As String

    Dim _str_sql As System.Text.StringBuilder
    Dim _str_where_account As String = ""

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" SELECT	 ")

    If ReportType = ENUM_REPORT_TYPE.BY_FACE_AND_TERMINAL Then
      _str_sql.AppendLine("        X.TM_AMOUNT AS AMOUNT, ")
    End If

    _str_sql.AppendLine(" 	       X.TM_TERMINAL_ID AS TERMINAL_ID ")
    _str_sql.AppendLine(" 	     , SUM(CASE WHEN X.TM_REPORTED IS NULL THEN 0 ELSE 1 END) AS NUM_EXPECTED ")
    _str_sql.AppendLine(" 	     , 0 AS MC_COLLECTION_ID ")
    _str_sql.AppendLine("   FROM   TERMINAL_MONEY X WITH(INDEX(IX_tm_cashier_session_terminal)) ")
    _str_sql.AppendLine("  INNER   JOIN TERMINALS T ON X.TM_TERMINAL_ID = T.TE_TERMINAL_ID ")
    _str_sql.AppendLine("  WHERE   T.TE_TERMINAL_TYPE IN (" & Me.uc_pr_list.GetTerminalTypes() & " ) ")

    If ReportType = ENUM_REPORT_TYPE.BY_FACE_AND_TERMINAL Then
      _str_sql.AppendLine("       AND X.TM_TERMINAL_ID =  " & TerminalId)

    Else
      If m_collection_by_terminal Then
        _str_sql.AppendLine("     AND X.TM_TERMINAL_ID = " & m_str_terminal_id)
      End If
    End If

    'Pending to collect
    _str_sql.AppendLine("         AND X.TM_COLLECTION_ID IS NULL ")

    'Notes out of the Terminal Stacker
    _str_sql.AppendLine("         AND X.TM_INTO_ACCEPTOR = 0 ")

    'Notes belong to this Cashier Session
    _str_sql.AppendLine("         AND X.TM_CASHIER_SESSION_ID = " & m_cashier_session_id)

    _str_sql.AppendLine(" GROUP BY  ")

    If ReportType = ENUM_REPORT_TYPE.BY_FACE_AND_TERMINAL Then
      _str_sql.AppendLine("          X.TM_AMOUNT, ")
    End If

    _str_sql.AppendLine("            X.TM_TERMINAL_ID ")

    _str_sql.AppendLine("   HAVING   SUM(CASE WHEN X.TM_REPORTED IS NULL THEN 0 ELSE 1 END) <>   ")
    _str_sql.AppendLine("            SUM(CASE WHEN X.TM_COLLECTION_ID IS NULL THEN 0 ELSE 1 END) ")

    _str_sql.AppendLine(" ORDER BY  ")


    If ReportType = ENUM_REPORT_TYPE.BY_TERMINAL Then
      _str_sql.AppendLine("          MAX(X.TM_REPORTED) ")
    Else
      _str_sql.AppendLine("          X.TM_AMOUNT ")
    End If

    Return _str_sql.ToString()

  End Function 'GetStringSQL

  ' PURPOSE: Deposit cash from a mobile bank into the cashier.
  ' 
  '  PARAMS:
  '      - INPUT:
  '          - AmountToDeposit
  '          - CashierTerminalId
  '          - CashierTerminalName
  '          - Trx
  '
  '      - OUTPUT:
  '
  ' RETURNS:
  '      - true: operation completed successfully
  '      - false: operation failed
  ' 
  '   NOTES:
  '      - Steps:
  '           1. Read Mobile Bank (system type) information
  '           2. Get Cashier info
  '           3. MB Card Cash Deposit.
  '
  Public Function NACardCashDeposit(ByVal AmountToDeposit As Currency, _
                                    ByVal TerminalId As Long, _
                                    ByVal Trx As SqlTransaction) As Boolean

    Dim _error As Boolean
    Dim _movement_id As Long

    Dim _cashier_user_id As Int32
    Dim _cashier_user_name As String
    Dim _cashier_terminal_id As Int32
    Dim _cashier_terminal_name As String
    Dim _cashier_balance As Currency

    Dim _session_info As CashierSessionInfo
    Dim _cashier_movements As CashierMovementsTable

    Dim _terminal_name As String
    Dim _mb_user_type As MB_USER_TYPE
    Dim _mb_account_id As Int64
    Dim _mb_track_data As String
    Dim _dummy_mb_cashier_session_id As Int64

    ' Initialize variables.

    _cashier_user_id = 0
    _cashier_user_name = ""
    _cashier_terminal_id = 0
    _cashier_terminal_name = ""
    _cashier_balance = 0
    _terminal_name = ""
    _mb_track_data = ""

    ' Initialize Db variables.
    _error = True

    Try

      If TerminalId > 0 Then
        If Not WSI.Common.Misc.GetTerminalName(TerminalId, _terminal_name, Trx) Then
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "notes_register", "NACardCashDeposit", "NACardCashDeposit Error. GetTerminalName")

          Return False
        End If
      End If

      '1. Read Mobile Bank (system type) information
      _mb_user_type = IIf(m_gu_user_type = GU_USER_TYPE.SYS_ACCEPTOR, MB_USER_TYPE.SYS_ACCEPTOR, MB_USER_TYPE.SYS_PROMOBOX)

      If Not WSI.Common.Cashier.ReadMobileBankNoteUser(Trx, _mb_user_type, _mb_account_id, _mb_track_data, _dummy_mb_cashier_session_id, _terminal_name) Then
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "notes_register", "NACardCashDeposit", "NACardCashDeposit Error. ReadMobileBankNoteUser")

        Return False
      End If

      ' _dummy_mb_cashier_session_id is not used. The MOBILE_BANK register can be used for another cashier_session.
      ' Deliveries are made by cashier_session, but we only need the ID and TRACK_DATA from the MOBILE_BANK.

      '2. Get Cashier info
      GetCashierInfo(m_cashier_session_id, Trx, _cashier_user_id, _cashier_user_name, _cashier_terminal_id, _cashier_terminal_name, _cashier_balance)

      '3. MB Card Cash Deposit.
      _movement_id = 0
      MobileBank.DB_InsertMBCardMovement(_cashier_terminal_id, _mb_account_id, MBMovementType.DepositCash, m_cashier_session_id, 0, 0, AmountToDeposit, AmountToDeposit, _movement_id, Trx, _terminal_name)

      ' RCI & ICS 13-MAY-2013: SetCashierInformation is only needed when generating vouchers (now, no voucher is generated).
      ' CommonCashierInformation.SetCashierInformation(m_cashier_session_id, _cashier_user_id, _cashier_user_name, _cashier_id, _cashier_name)

      ' Set session information
      _session_info = New CashierSessionInfo()
      _session_info.CashierSessionId = m_cashier_session_id
      _session_info.TerminalId = _cashier_terminal_id
      _session_info.UserId = _cashier_user_id
      _session_info.TerminalName = _cashier_terminal_name
      _session_info.UserName = _cashier_user_name
      _session_info.AuthorizedByUserId = _session_info.UserId
      _session_info.AuthorizedByUserName = _session_info.UserName

      ' Insert MB_CASH_IN cashier movement
      _cashier_movements = New CashierMovementsTable(_session_info)
      _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_CASH_IN, AmountToDeposit, _mb_account_id, _mb_track_data)

      If Not _cashier_movements.Save(Trx) Then
        Return False
      End If

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "notes_register", "NACardCashDeposit", "Exception throw: Error in Mobile Bank card deposit operation. Account type: System.")
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "NACardCashDeposit", _ex.Message)

      Return False
    End Try

    Return True
  End Function  ' NACardCashDeposit

  ' PURPOSE: Collect money --> INSERT into MONEY_COLLECTIONS and MONEY_COLLECTION_DETAILS and UPDATE TERMINAL_MONEY
  '
  '  PARAMS:
  '     - INPUT:
  '           - Trx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if changes saved
  '
  ' NOTES :
  '  Steps:
  '        1. Create DataSet MoneyCollections with new tables: "MONEY_COLLECTIONS", "MONEY_COLLECTIONS_DETAILS" and "TERMINAL_MONEY"
  '        2. Get all terminals with not collected amounts (applying the terminal's filter , so if one terminal's option is checked there will be only one terminal)
  '        3. Get pending to collect amounts from Terminal Money with the same TerminalId of current Terminal. For each register do next steps
  '        4. Get collected number from grid values
  '        5. Get num expected of current amount
  '        6. Get older registers in the "terminal_money" table and fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables)
  '        7. Get num_collected > 0 from grid which amount doesn't exist in expected
  '
  Private Function CollectMoney(ByVal Trx As SqlTransaction) As Boolean

    Dim _sql_str As String

    Dim _terminal_id As Int64
    Dim _amount As Int32
    Dim _collection_id As Long

    'Tables
    Dim _dt_terminals As DataTable
    Dim _dt_face_values As DataTable

    'NumExpected and NumCollected of each amount
    Dim _num_collected_grid As Integer
    Dim _num_expected As Integer
    Dim _real_num_collected As Integer
    Dim _money_from_grid As New Dictionary(Of Decimal, Int32)

    ' Total amount colected for each terminal
    Dim _total_amount As Integer
    Dim _row_details As DataRow

    Try
      ' Create terminal totals datatable
      Me.m_dt_terminal_totals = New DataTable("TERMINAL_TOTALS")
      m_dt_terminal_totals.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"))
      m_dt_terminal_totals.Columns.Add("TOTAL_AMOUNT", Type.GetType("System.Int32"))

      For Each _row As DataRow In m_data_table_grid_values.Rows
        _money_from_grid.Add(GUI_ParseCurrency(_row("AMOUNT")), _row("NUM_COLLECTED"))
      Next

      '1. Create DataSet MoneyCollections with new tables: "MONEY_COLLECTIONS", "MONEY_COLLECTIONS_DETAILS" and "TERMINAL_MONEY"
      CreateDataSetMoneyCollections()

      '2. Get all terminals with not collected amounts (applying the terminal's filter , so if one terminal's option is checked there will be only one terminal)
      _sql_str = GetStringSQL(ENUM_REPORT_TYPE.BY_TERMINAL, 0)
      _dt_terminals = GUI_GetTableUsingSQL(_sql_str, 5000, , Trx)

      ' Initialize variables
      _collection_id = -1
      _terminal_id = 0

      ' For Each Terminal
      For Each _row_terminal As DataRow In _dt_terminals.Rows
        _terminal_id = _row_terminal("TERMINAL_ID")
        _total_amount = 0

        '3. Get pending to collect amounts from Terminal Money with the same TerminalId of current Terminal.
        '   For each register do next steps
        _sql_str = GetStringSQL(ENUM_REPORT_TYPE.BY_FACE_AND_TERMINAL, _terminal_id)
        _dt_face_values = GUI_GetTableUsingSQL(_sql_str, 5000, , Trx)

        ' For Each amount
        For Each _row_face_value As DataRow In _dt_face_values.Rows

          '4. Get collected number from grid values for current amount
          _amount = _row_face_value("AMOUNT")
          If _money_from_grid.ContainsKey(_amount) Then
            _num_collected_grid = _money_from_grid(_amount)
          Else
            _num_collected_grid = 0
          End If

          '5. Get num expected of current amount
          _num_expected = _row_face_value("NUM_EXPECTED")

          '6. Get older registers in the "terminal_money" table and fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables)
          If Not CollectMoneyByFaceTerminal(_terminal_id, _amount, _collection_id, _num_expected, _num_collected_grid, _real_num_collected, Trx) Then
            Throw New Exception("Error. CollectMoneyByFaceTerminal")
          End If

          ' Set remaining notes
          If _money_from_grid.ContainsKey(_amount) Then
            _money_from_grid(_amount) = Math.Max(0, _money_from_grid(_amount) - _real_num_collected)
          End If

          ' Calculate total amount for current terminal
          _total_amount += _amount * _real_num_collected

        Next ' For Each _row_face_value

        ' Save total amount for current terminal
        _row_details = m_dt_terminal_totals.NewRow()
        _row_details("TERMINAL_ID") = _terminal_id
        _row_details("TOTAL_AMOUNT") = _total_amount
        m_dt_terminal_totals.Rows.Add(_row_details)

      Next ' For Each _row_terminal

      ' 7. Get num_collected > 0 from grid which amount doesn't exist in expected
      CollectNotExpectedNotes(_collection_id, _money_from_grid, _total_amount, Trx)

      ' Save total amount for unexpected notes
      If _total_amount > 0 Then
        _row_details = m_dt_terminal_totals.NewRow()
        _row_details("TERMINAL_ID") = 0
        _row_details("TOTAL_AMOUNT") = _total_amount
        m_dt_terminal_totals.Rows.Add(_row_details)
      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(109), _
                             GLB_NLS_GUI_AUDITOR.GetString(109) & _ex.Message, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End Try

    Return True

  End Function ' CollectMoney

  ' PURPOSE: Create a Dataset with tables MONEY_COLLECTION, MONEY_COLLECTION_DETAILS, TERMINAL_MONEY
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  '
  ' NOTES :
  '
  Private Sub CreateDataSetMoneyCollections()

    Dim _table As DataTable
    Dim _column_parent As DataColumn
    Dim _column_child As DataColumn
    Dim _data_relation As DataRelation

    m_ds_money_collections = New DataSet()

    ' Create Collection table
    _table = New DataTable("MONEY_COLLECTION")
    _table.Columns.Add("MC_COLLECTION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("MC_TERMINAL_ID", Type.GetType("System.Int32"))
    _table.Columns.Add("MC_CASHIER_SESSION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("MC_EXPECTED_BILL_AMOUNT", Type.GetType("System.Decimal"))
    _table.Columns.Add("MC_EXPECTED_BILL_COUNT", Type.GetType("System.Int32"))
    _table.Columns.Add("MC_COLLECTED_BILL_AMOUNT", Type.GetType("System.Decimal"))
    _table.Columns.Add("MC_COLLECTED_BILL_COUNT", Type.GetType("System.Int32"))
    m_ds_money_collections.Tables.Add(_table)

    ' Create Collection Details table
    _table = New DataTable("MONEY_COLLECTION_DETAILS")
    _table.Columns.Add("MCD_COLLECTION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("MCD_FACE_VALUE", Type.GetType("System.Int32"))
    _table.Columns.Add("MCD_NUM_EXPECTED", Type.GetType("System.Int32"))
    _table.Columns.Add("MCD_NUM_COLLECTED", Type.GetType("System.Int32"))
    m_ds_money_collections.Tables.Add(_table)

    ' Create Terminal Money table
    _table = New DataTable("TERMINAL_MONEY")
    _table.Columns.Add("TM_TERMINAL_ID", Type.GetType("System.Int32"))
    _table.Columns.Add("TM_TRANSACTION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("TM_STACKER_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("TM_AMOUNT", Type.GetType("System.Int32"))
    _table.Columns.Add("TM_COLLECTION_ID", Type.GetType("System.Int64"))
    m_ds_money_collections.Tables.Add(_table)

    ' Relations
    _column_parent = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Columns("MC_COLLECTION_ID")

    _column_child = m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).Columns("TM_COLLECTION_ID")
    _data_relation = New DataRelation(RELATION_COLLECTION_TERMINALMONEY, _column_parent, _column_child, True)
    m_ds_money_collections.Relations.Add(_data_relation)

    _column_child = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Columns("MCD_COLLECTION_ID")
    _data_relation = New DataRelation(RELATION_COLLECTION_DETAILS, _column_parent, _column_child, True)
    m_ds_money_collections.Relations.Add(_data_relation)

  End Sub ' CreateDataSetMoneyCollections

  ' PURPOSE: Get NumCollected from Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Amount
  '           - DataTableChangesNum
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer: _num_collected_grid
  '
  ' NOTES :
  '
  Private Function GetNumCollectedFromGrid(ByVal Amount As Decimal) As Integer

    Dim _num_collected_grid As Integer
    Dim _face_grid_value As Decimal
    Dim _idx_change As Integer

    _num_collected_grid = 0

    Try

      For _idx_change = 0 To m_data_table_grid_values.Rows.Count - 1
        _face_grid_value = GUI_ParseCurrency(m_data_table_grid_values.Rows(_idx_change).Item("AMOUNT"))
        If _face_grid_value = Amount Then
          _num_collected_grid = m_data_table_grid_values.Rows(_idx_change).Item("NUM_COLLECTED")

          Exit For
        End If
      Next

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "GetNumCollectedFromGrid", _ex.Message)

    End Try

    Return _num_collected_grid

  End Function ' GetNumCollectedFromGrid

  ' PURPOSE: Fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables) with not expected notes
  '
  '  PARAMS:
  '     - INPUT:
  '           - CollectionId
  '           - MoneyFromGrid
  '           - SqlTrx
  '     - OUTPUT:
  '           - MoneyFromGrid
  '           - TotalUnexpectedAmount
  '
  ' RETURNS:
  '     - Boolean
  '
  ' NOTES :
  '
  Private Function CollectNotExpectedNotes(ByVal CollectionId As Int64, _
                                           ByRef MoneyFromGrid As Dictionary(Of Decimal, Int32), _
                                           ByRef TotalUnexpectedAmount As Int32, _
                                           ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _rows_grid As DataRow()
    Dim _data_row As DataRow
    Dim _num_collected_grid As Int32
    Dim _dummy_real_num_collected As Int32
    Dim _amount As Int32
    Dim _filter As String
    Dim _collection_details_rows As DataRow()
    Dim _row_details As DataRow
    Dim _collection_rows As DataRow()
    Dim _row_collection As DataRow
    Dim _terminal_id = 0
    Dim _total_unexpected_count As Int32 = 0

    Try
      _rows_grid = m_data_table_grid_values.Select("NUM_COLLECTED > 0")
      TotalUnexpectedAmount = 0

      If m_collection_by_terminal Then
        _terminal_id = GUI_ParseNumber(m_str_terminal_id)
      End If

      ' First ForEach is used to insert unexpected banknotes received.
      For Each _data_row In _rows_grid

        _amount = GUI_ParseCurrency(_data_row(0))
        _filter = "MCD_COLLECTION_ID = " & CollectionId & " AND MCD_FACE_VALUE = " & _amount

        _collection_details_rows = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Select(_filter)
        If _collection_details_rows.Length = 0 Then
          ' Unexpected note received. Insert into CollectionDetails with expected = 0 and collected = 0.

          If Not CollectMoneyByFaceTerminal(_terminal_id, _amount, CollectionId, 0, 0, _dummy_real_num_collected, SqlTrx) Then
            Throw New Exception("Error. CollectMoneyByFaceTerminal")
          End If

          ' Calculate total not expected amount
          TotalUnexpectedAmount += _amount * _dummy_real_num_collected
        End If
      Next

      ' Second ForEach is used to take in account the excess banknotes received.
      ' Must be done after finish First ForEach. Can't be done together.
      For Each _data_row In _rows_grid

        _amount = GUI_ParseCurrency(_data_row(0))
        _filter = "MCD_COLLECTION_ID = " & CollectionId & " AND MCD_FACE_VALUE = " & _amount

        _collection_details_rows = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Select(_filter)
        ' Must be always > 0.
        If _collection_details_rows.Length > 0 Then

          If MoneyFromGrid.ContainsKey(_amount) Then
            _num_collected_grid = MoneyFromGrid(_amount)
            MoneyFromGrid(_amount) = 0
          Else
            _num_collected_grid = 0
          End If

          _row_details = _collection_details_rows(0)
          ' Received more notes from a face value.
          _row_details("MCD_NUM_COLLECTED") += _num_collected_grid

          ' Calculate total not expected amount
          TotalUnexpectedAmount += _amount * _num_collected_grid
          _total_unexpected_count += _num_collected_grid
        End If
      Next

      _filter = "MC_COLLECTION_ID = " & CollectionId
      _collection_rows = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Select(_filter)

      ' Must be always > 0.
      If _collection_rows.Length > 0 Then
        _row_collection = _collection_rows(0)
        _row_collection("MC_COLLECTED_BILL_AMOUNT") += TotalUnexpectedAmount
        _row_collection("MC_COLLECTED_BILL_COUNT") += _total_unexpected_count
      End If

      Return True

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "CollectNotExpectedNotes", _ex.Message)

      Return False
    End Try

  End Function ' CollectNotExpectedNotes

  ' PURPOSE: Fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables)
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalId
  '           - Amount
  '           - CollectionId
  '           - RowTerminal
  '           - NumExpected
  '           - NumCollectedGrid
  '           - SqlTrx
  '     - OUTPUT:
  '           - RealNumCollected
  '
  ' RETURNS:
  '     - Boolean: _min_open_stacker_found
  '
  ' NOTES :
  '     - Steps:
  '            1. Add to Collection table
  '            2. Add to Collection Detail table
  '            3. Select Terminal Money registers pending to update
  '            4. Add to Terminal Money table
  ' 
  Private Function CollectMoneyByFaceTerminal(ByVal TerminalId As Int64, _
                                              ByVal Amount As Decimal, _
                                              ByVal CollectionId As Int64, _
                                              ByVal NumExpected As Int32, _
                                              ByVal NumCollectedGrid As Int32, _
                                              ByRef RealNumCollected As Int32, _
                                              ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _transaction_id As Int64
    Dim _stacker_id As Int64
    Dim _row As DataRow
    Dim _row_collection As DataRow
    Dim _row_details As DataRow
    Dim _read_data As Boolean
    Dim _filter As String
    Dim _collection_rows As DataRow()
    Dim _collection_details_rows As DataRow()

    RealNumCollected = 0
    _transaction_id = 0

    Try

      '1. Add to Collection table
      _filter = "MC_COLLECTION_ID = " & CollectionId
      _collection_rows = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Select(_filter)
      If _collection_rows.Length = 0 Then

        _row_collection = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).NewRow()
        _row_collection("MC_COLLECTION_ID") = CollectionId
        _row_collection("MC_CASHIER_SESSION_ID") = m_cashier_session_id
        _row_collection("MC_EXPECTED_BILL_AMOUNT") = Amount * NumExpected
        _row_collection("MC_EXPECTED_BILL_COUNT") = NumExpected
        _row_collection("MC_COLLECTED_BILL_AMOUNT") = 0
        _row_collection("MC_COLLECTED_BILL_COUNT") = 0

        If m_collection_by_terminal Then
          If TerminalId = 0 Then
            _row_collection("MC_TERMINAL_ID") = DBNull.Value
          Else
            _row_collection("MC_TERMINAL_ID") = TerminalId
          End If
        Else
          _row_collection("MC_TERMINAL_ID") = DBNull.Value
        End If

        m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Rows.Add(_row_collection)
      Else
        _row_collection = _collection_rows(0)
        _row_collection("MC_EXPECTED_BILL_AMOUNT") += Amount * NumExpected
        _row_collection("MC_EXPECTED_BILL_COUNT") += NumExpected
      End If

      '2. Add to Collection Detail table
      _filter = "MCD_COLLECTION_ID = " & CollectionId & " AND MCD_FACE_VALUE = " & CInt(Amount)
      _collection_details_rows = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Select(_filter)
      If _collection_details_rows.Length = 0 Then
        _row_details = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).NewRow()
        _row_details("MCD_COLLECTION_ID") = CollectionId
        _row_details("MCD_FACE_VALUE") = CInt(Amount)
        _row_details("MCD_NUM_COLLECTED") = 0 ' It's updated later, when the real collected are obtained. User can lie!!!
        _row_details("MCD_NUM_EXPECTED") = NumExpected
        m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Rows.Add(_row_details)
      Else
        _row_details = _collection_details_rows(0)
        _row_details("MCD_NUM_EXPECTED") += NumExpected
      End If

      '3. Select Terminal Money registers to update
      If NumExpected = 0 Then
        Return True
      End If

      If RealNumCollected < NumCollectedGrid Then

        _sql_str = "SELECT   TM_TRANSACTION_ID " & _
                   "       , TM_STACKER_ID " & _
                   "  FROM   TERMINAL_MONEY " & _
                   " WHERE   TM_TERMINAL_ID = @pTerminalId " & _
                   "   AND   TM_TRANSACTION_ID = " & _
                   "         ( " & _
                   "           SELECT   MIN(TM_TRANSACTION_ID) " & _
                   "             FROM   TERMINAL_MONEY WITH(INDEX(IX_tm_cashier_session_amount_terminal)) " & _
                   "            WHERE   TM_TERMINAL_ID    = @pTerminalId " & _
                   "              AND   TM_AMOUNT         = @pAmount " & _
                   "              AND   TM_COLLECTION_ID IS NULL " & _
                   "              AND   TM_INTO_ACCEPTOR  = 0 " & _
                   "              AND   TM_TRANSACTION_ID > @pTransactionId " & _
                   "              AND   TM_CASHIER_SESSION_ID = @pCashierSessionId " & _
                   "         ) "

        Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount
          _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt)
          _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = m_cashier_session_id

          While RealNumCollected < NumCollectedGrid
            _sql_cmd.Parameters("@pTransactionId").Value = _transaction_id

            _read_data = False

            Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()
              If _sql_reader.Read() Then
                ' StackerId can be NULL. In this case, don't add it.
                If Not _sql_reader.IsDBNull(1) Then

                  _transaction_id = _sql_reader.GetInt64(0)
                  _stacker_id = _sql_reader.GetInt64(1)

                  _read_data = True

                  '4. Add to Terminal Money table
                  _row = m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).NewRow()
                  _row("TM_TERMINAL_ID") = TerminalId
                  _row("TM_TRANSACTION_ID") = _transaction_id
                  _row("TM_STACKER_ID") = _stacker_id
                  _row("TM_AMOUNT") = Amount
                  _row("TM_COLLECTION_ID") = CollectionId
                  m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).Rows.Add(_row)

                  RealNumCollected += 1
                End If
              End If
            End Using 'using _sql_reader

            If Not _read_data Then
              Exit While
            End If

          End While
        End Using 'using _cql_cmd 

      End If ' If RealNumCollected < NumCollectedGrid

      If RealNumCollected > 0 Then
        _row_details("MCD_NUM_COLLECTED") += RealNumCollected

        ' Update money collection with total real collected
        _filter = "MC_COLLECTION_ID = " & CollectionId
        _collection_rows = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Select(_filter)
        If _collection_rows.Length > 0 Then
          _row_collection = _collection_rows(0)
          _row_collection("MC_COLLECTED_BILL_AMOUNT") += Amount * RealNumCollected
          _row_collection("MC_COLLECTED_BILL_COUNT") += RealNumCollected
        End If
      End If

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "CollectMoneyByFaceTerminal", _ex.Message)

      Return False
    End Try

    Return True
  End Function 'CollectMoneyByFaceTerminal

  ' PURPOSE: Insert values into MONEY_COLLECTIONS table
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if Collection inserted, false if not.
  '
  ' NOTES :
  '  
  Private Function InsertMoneyCollection(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _parameter As SqlParameter
    Dim _num_rows_inserted As Integer
    Dim _dt_collection As DataTable

    Try

      _dt_collection = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION)

      ' MONEY_COLLECTIONS
      _sql_str = " INSERT INTO   MONEY_COLLECTIONS                  " & _
                 "             ( MC_DATETIME                        " & _
                 "             , MC_TERMINAL_ID                     " & _
                 "             , MC_USER_ID                         " & _
                 "             , MC_CASHIER_SESSION_ID              " & _
                 "             , MC_EXPECTED_BILL_AMOUNT            " & _
                 "             , MC_EXPECTED_BILL_COUNT             " & _
                 "             , MC_COLLECTED_BILL_AMOUNT           " & _
                 "             , MC_COLLECTED_BILL_COUNT            " & _
                 "             )                                    " & _
                 "      VALUES ( @pDatetime                         " & _
                 "             , @pTerminalId                       " & _
                 "             , @pUserId                           " & _
                 "             , @pCashierSessionId                 " & _
                 "             , @pExpectedBillAmount               " & _
                 "             , @pExpectedBillCount                " & _
                 "             , @pCollectedBillAmount              " & _
                 "             , @pCollectedBillCount               " & _
                 "             )                                    " & _
                 "         SET   @pCollectionId = SCOPE_IDENTITY()  "

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
        With _sql_adap
          .SelectCommand = Nothing
          .DeleteCommand = Nothing
          .UpdateCommand = Nothing

          Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MC_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = GLB_CurrentUser.Id
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).SourceColumn = "MC_CASHIER_SESSION_ID"
            _sql_cmd.Parameters.Add("@pExpectedBillAmount", SqlDbType.Money).SourceColumn = "MC_EXPECTED_BILL_AMOUNT"
            _sql_cmd.Parameters.Add("@pExpectedBillCount", SqlDbType.Int).SourceColumn = "MC_EXPECTED_BILL_COUNT"
            _sql_cmd.Parameters.Add("@pCollectedBillAmount", SqlDbType.Money).SourceColumn = "MC_COLLECTED_BILL_AMOUNT"
            _sql_cmd.Parameters.Add("@pCollectedBillCount", SqlDbType.Int).SourceColumn = "MC_COLLECTED_BILL_COUNT"

            _parameter = _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt)
            _parameter.SourceColumn = "MC_COLLECTION_ID"
            _parameter.Direction = ParameterDirection.Output

            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .InsertCommand = _sql_cmd

            .UpdateBatchSize = 500
            _num_rows_inserted = .Update(_dt_collection)

            If _num_rows_inserted <> _dt_collection.Rows.Count Then
              Return False
            End If

          End Using

        End With
      End Using ' _sql_adap for TerminalMoney

      Return True

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "notes_register", "InsertMoneyCollection", _ex.Message)

      Return False
    End Try

  End Function 'InsertMoneyCollection

  ' PURPOSE: Update IdCollection value in TERMINAL_MONEY table
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: true if rows updated
  '
  ' NOTES :
  '  
  Private Function UpdateTerminalMoney(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _num_rows_updated As Integer
    Dim _dt_terminal_money As DataTable

    Try

      _dt_terminal_money = m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY)

      'TERMINAL_MONEY
      _sql_str = "UPDATE   TERMINAL_MONEY                       " & _
                 "   SET   TM_COLLECTION_ID  = @pCollectionId   " & _
                 " WHERE   TM_TERMINAL_ID    = @pTerminalId     " & _
                 "   AND   TM_TRANSACTION_ID = @pTransactionId  " & _
                 "   AND   TM_STACKER_ID     = @pStackerId      " & _
                 "   AND   TM_AMOUNT         = @pAmount         "

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()

        With _sql_adap
          .SelectCommand = Nothing
          .UpdateCommand = Nothing
          .DeleteCommand = Nothing

          Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TM_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).SourceColumn = "TM_TRANSACTION_ID"
            _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).SourceColumn = "TM_STACKER_ID"
            _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).SourceColumn = "TM_AMOUNT"
            _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "TM_COLLECTION_ID"

            _sql_cmd.UpdatedRowSource = UpdateRowSource.None
            ' We use the InsertCommand because the RowState of the DataTable is Added.
            .InsertCommand = _sql_cmd

            .UpdateBatchSize = 500

            _num_rows_updated = .Update(_dt_terminal_money)

            If _num_rows_updated <> _dt_terminal_money.Rows.Count Then
              Return False
            End If

          End Using

        End With
      End Using  ' _sql_adap for TerminalMoney

      Return True

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "notes_register", "UpdateTerminalMoney", _ex.Message)

      Return False
    End Try

  End Function 'UpdateTerminalMoney

  ' PURPOSE: Insert values into MONEY_COLLECTION_DETAILS table
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if Collection inserted, false if not.
  '
  ' NOTES :
  '  
  Private Function InsertMoneyCollectionDetails(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _num_rows_inserted_details As Integer
    Dim _dt_collection_details As DataTable

    Try

      _dt_collection_details = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS)

      ' MONEY_COLLECTIONS
      _sql_str = "     INSERT INTO   MONEY_COLLECTION_DETAILS         " & _
                 "                 ( MCD_COLLECTION_ID                " & _
                 "                 , MCD_FACE_VALUE                   " & _
                 "                 , MCD_NUM_EXPECTED                 " & _
                 "                 , MCD_NUM_COLLECTED                " & _
                 "                 )                                  " & _
                 "          VALUES ( @pCollectionId                   " & _
                 "                 , @pFaceValue                      " & _
                 "                 , @pNumExpected                    " & _
                 "                 , @pNumCollected                   " & _
                 "                 )                                  "

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
        With _sql_adap
          .SelectCommand = Nothing
          .UpdateCommand = Nothing
          .DeleteCommand = Nothing

          Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

            _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MCD_COLLECTION_ID"
            _sql_cmd.Parameters.Add("@pFaceValue", SqlDbType.Money).SourceColumn = "MCD_FACE_VALUE"
            _sql_cmd.Parameters.Add("@pNumExpected", SqlDbType.Int).SourceColumn = "MCD_NUM_EXPECTED"
            _sql_cmd.Parameters.Add("@pNumCollected", SqlDbType.Int).SourceColumn = "MCD_NUM_COLLECTED"

            _sql_cmd.UpdatedRowSource = UpdateRowSource.None
            .InsertCommand = _sql_cmd

            .UpdateBatchSize = 500

            _num_rows_inserted_details = .Update(_dt_collection_details)

            If _num_rows_inserted_details <> _dt_collection_details.Rows.Count Then
              Return False
            End If

          End Using

        End With
      End Using ' _sql_adap for TerminalMoney

      Return True

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_GENERIC_ERROR, "notes_register", "InsertMoneyCollectionDetails", _ex.Message)

      Return False
    End Try

  End Function 'InsertMoneyCollectionDetails

  ' PURPOSE: Show confirm message.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Continue. False: Exit.
  '  
  Private Function ConfirmChanges() As Boolean
    Dim _message As String
    Dim _amount As Int32
    Dim _money_from_grid As New Dictionary(Of Decimal, Int32)
    Dim _total_bills As Integer
    Dim _total_amount As Integer

    _total_bills = 0
    _total_amount = 0

    For Each _value As DataRow In m_data_table_grid_values.Rows
      _money_from_grid.Add(GUI_ParseCurrency(_value("AMOUNT")), 0)
    Next

    _message = ""

    ' Sum all expected and collected for more than one terminal collection
    For Each _collection_detail As DataRow In m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Rows
      _amount = _collection_detail("MCD_FACE_VALUE")

      If _money_from_grid.ContainsKey(_amount) Then
        _money_from_grid(_amount) += _collection_detail("MCD_NUM_COLLECTED")
      End If

    Next

    For Each _amount In _money_from_grid.Keys
      If _money_from_grid(_amount) > 0 Then

        _total_bills = _total_bills + _money_from_grid(_amount)
        _total_amount = _total_amount + (_money_from_grid(_amount) * _amount)

        _message &= vbTab & GUI_FormatCurrency(_amount) & " x " & GUI_FormatNumber(_money_from_grid(_amount), 0) & " = "
        _message &= GUI_FormatCurrency(_money_from_grid(_amount) * _amount) + Environment.NewLine
      End If

    Next

    _message &= Environment.NewLine & GLB_NLS_GUI_INVOICING.GetString(488) & ": "
    _message &= Environment.NewLine & vbTab & GUI_FormatNumber(_total_bills, 0) & " " & GLB_NLS_GUI_INVOICING.GetString(37)
    _message &= Environment.NewLine & vbTab & GUI_FormatCurrency(_total_amount)
    _message &= Environment.NewLine

    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(671), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_YES_NO, , _message) = ENUM_MB_RESULT.MB_RESULT_YES Then
      Return True
    Else
      Return False
    End If

  End Function 'ConfirmChanges

#End Region

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowModalForEdit(ByVal CashierSessionId As Long, ByVal UserType As GU_USER_TYPE, ByVal CashierTerminalId As Int32)

    m_cashier_session_id = CashierSessionId
    m_gu_user_type = UserType
    m_cashier_terminal_id = CashierTerminalId

    Me.Display(True)

  End Sub ' ShowForEdit

#End Region

#Region " Options Events "


#End Region

End Class ' frm_notes_register

Public Class MBCardData

  Public CardId As Int64
  Public HolderName As String
  Public Blocked As Boolean
  Public TrackData As String
  Public CurrentBalance As Currency
  Public InitialLimit As Currency
  Public DisposedCash As Currency
  Public DepositCash As Currency
  Public LimitExtensions As Currency
  Public PendingCash As Currency
  Public Pin As String
  Public Timestamp As Int64
  Public SiteId As Int32
  Public LastTerminalId As Int32
  Public LastTerminalName As String
  Public LastActivity As DateTime
  Public CurrentCashierSessionId As Int64

End Class ' MBCardData