'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_chips_sets.vb
'
' DESCRIPTION:   This screen allows to view the types of sets that may have the chips.
'
' AUTHOR:        Didac Campanals
'
' CREATION DATE: 01-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2014  DCS    Initial version
' 09-FEB-2016  RGR    Task 5778: Group chips: Design changes in Group chips
' 16-FEB-2016  LTC    Task 9340: Group chips: Design changes in Group chips
' 22-FEB-2016  RGR    Product Backlog Item 9339:Sprint Review 18 Winions - Mex
' 26-FEB-2016  LTC    Product Backlog Item 9339:Sprint Review 18 Winions - Mex
' 11-APR-2016  EOR    Product Backlog Item 10936:Mesas (Fase 1): Ajustes en diferentes pantallas de fichas
' 13-SEP-2016  FOS    Fixed Bug 17179: MESAS - GUI: Teniendo los grupos de fichas deshabilitados, nos permite a�adir fichas y nos muestra un pop-up que no es correcto.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Collections.Generic
Imports WSI.Common
Imports System.Data.SqlClient

#End Region ' Imports

Public Class frm_chips_sets_sel
  Inherits frm_base_sel

  ' RGR 29-OCT-2015
#Region " Structures"
  Private Structure ITEM_FILTER
    Dim code As Integer
    Dim name As String
  End Structure
#End Region

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_CHIP_SET_ID As Integer = 0
  Private Const SQL_COLUMN_CHIP_SET_CHIP_TYPE As Integer = 1
  Private Const SQL_COLUMN_CHIP_SET_ISO_CODE As Integer = 2
  Private Const SQL_COLUMN_CHIP_SET_NAME As Integer = 3
  Private Const SQL_COLUMN_CHIP_SET_ALLOWED As Integer = 4

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_CHIP_SET_ID As Integer = 1
  Private Const GRID_COLUMN_CHIP_SET_NAME As Integer = 2
  Private Const GRID_COLUMN_CHIP_SET_ISO_CODE As Integer = 3
  Private Const GRID_COLUMN_CHIP_SET_CHIP_TYPE As Integer = 4
  Private Const GRID_COLUMN_CHIP_SET_ALLOWED As Integer = 5

  ' Width Columns
  Private Const GRID_WIDTH_ID As Integer = 200
  Private Const GRID_WIDTH_CHIP_SET_ID As Integer = 0
  Private Const GRID_WIDTH_CHIP_SET_NAME As Integer = 4550
  Private Const GRID_WIDTH_CHIP_SET_CHIP_TYPE As Integer = 1580
  Private Const GRID_WIDTH_CHIP_SET_ISO_CODE As Integer = 800
  Private Const GRID_WIDTH_CHIP_SET_ALLOWED As Integer = 1000

  ' Other Constants
  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const NULL_TEXT As String = ""

#End Region

#Region " Members "

  ' RGR 29-OCT-2015
  Private m_name As String
  Private m_alloweds As List(Of ITEM_FILTER)
  Private m_chips_types As List(Of ITEM_FILTER)
  Private m_chips_set_names As IDictionary(Of FeatureChips.ChipType, String)
  Private m_multiple_currency As Boolean

#End Region

#Region "Constructor"

  Sub New()

    InitializeComponent()
    m_chips_set_names = CLS_CHIPS_SET.GetNamesChipType()

    'EOR 11-APR-2016 Is Multiple Currency
    m_multiple_currency = GeneralParam.GetBoolean("GamingTables", "DualCurrency.Enabled", False)
  End Sub

#End Region

#Region " Overrides"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CHIPS_SETS_SEL
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    'Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(20) ' "Select"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(6) ' "New"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' LTC 16-FEB-2016
    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6577) '  "Chips"

    ' Set control names
    ' LTC 16-FEB-2016
    Me.ef_chips_sets_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) '  "Group"
    Me.gb_group_status.Text = GLB_NLS_GUI_CONTROLS.GetString(281) ' "Allowed"
    Me.chk_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264) ' SI
    Me.chk_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265) ' NO
    Me.gb_chips_types.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5062) '  "Types of chips"
    Me.chk_chip_type_RE.Text = m_chips_set_names.Item(FeatureChips.ChipType.RE)  ' "Value chips"
    Me.chk_chip_type_NR.Text = m_chips_set_names.Item(FeatureChips.ChipType.NR)
    Me.chk_chip_type_Color.Text = m_chips_set_names.Item(FeatureChips.ChipType.COLOR) ' "Color chips"

    If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.RE) Then
      Me.chk_chip_type_RE.Visible = True
    Else
      Me.chk_chip_type_RE.Visible = False
      TableLayoutPanel1.RowStyles(0).Height = 0
    End If
    If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.NR) Then
      Me.chk_chip_type_NR.Visible = True
    Else
      Me.chk_chip_type_NR.Visible = False
      TableLayoutPanel1.RowStyles(1).Height = 0
    End If
    If FeatureChips.IsChipsTypeEnabledGP(FeatureChips.ChipType.COLOR) Then
      Me.chk_chip_type_Color.Visible = True
    Else
      Me.chk_chip_type_Color.Visible = False
      TableLayoutPanel1.RowStyles(2).Height = 0
    End If

    ' Initialize controls
    Me.ef_chips_sets_name.Focus()
    Me.ef_chips_sets_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)

    ' Initialize Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    'If not exist any chips' group, disable add new groups' button
    If IsDisabledAllChipsGroup() Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      gb_chips_types.Visible = False
    End If

  End Sub 'GUI_InitControls

  ' PURPOSE: Call function to reset Filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'GUI_FilterReset

  ' RGR 29-OCT-2015
  ' PURPOSE: Get query type
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_DATABASE
  End Function 'GUI_GetQueryType

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' RGR 29-OCT-2015
    Me.m_alloweds = New List(Of ITEM_FILTER)
    Me.m_chips_types = New List(Of ITEM_FILTER)

    Me.m_name = Me.ef_chips_sets_name.Value.Trim()

    If IsEnabledAllChipsGroup() Then

      If Me.chk_chip_type_RE.Checked Then
        Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.RE, m_chips_set_names.Item(FeatureChips.ChipType.RE)))
      End If

      If Me.chk_chip_type_NR.Checked Then
        Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.NR, m_chips_set_names.Item(FeatureChips.ChipType.NR)))
      End If

      If Me.chk_chip_type_Color.Checked Then
        Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.COLOR, m_chips_set_names.Item(FeatureChips.ChipType.COLOR)))
      End If

    Else

      If IsNotCheckedAnyChipsGroup() AndAlso (Me.chk_chip_type_RE.Checked Or Me.chk_chip_type_RE.Visible) Then
        Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.RE, m_chips_set_names.Item(FeatureChips.ChipType.RE)))
      Else
        If Me.chk_chip_type_RE.Checked Then
          Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.RE, m_chips_set_names.Item(FeatureChips.ChipType.RE)))
        End If
      End If

      If IsNotCheckedAnyChipsGroup() AndAlso (Me.chk_chip_type_NR.Checked Or Me.chk_chip_type_NR.Visible) Then
        Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.NR, m_chips_set_names.Item(FeatureChips.ChipType.NR)))
      Else
        If Me.chk_chip_type_NR.Checked Then
          Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.NR, m_chips_set_names.Item(FeatureChips.ChipType.NR)))
        End If
      End If

      If IsNotCheckedAnyChipsGroup() AndAlso (Me.chk_chip_type_Color.Checked Or Me.chk_chip_type_Color.Visible) Then
        Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.COLOR, m_chips_set_names.Item(FeatureChips.ChipType.COLOR)))
      Else
        If Me.chk_chip_type_Color.Checked Then
          Me.m_chips_types.Add(Me.CreateItemFilter(FeatureChips.ChipType.COLOR, m_chips_set_names.Item(FeatureChips.ChipType.COLOR)))
        End If
      End If


    End If

    If Me.chk_enabled.Checked Then
      m_alloweds.Add(Me.CreateItemFilter(1, GLB_NLS_GUI_CONFIGURATION.GetString(264)))
    End If

    If Me.chk_disabled.Checked Then
      m_alloweds.Add(Me.CreateItemFilter(0, GLB_NLS_GUI_CONFIGURATION.GetString(265)))
    End If

  End Sub 'GUI_ReportUpdateFilters

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    ' RGR 29-OCT-2015
    Dim _sql_sb As StringBuilder

    _sql_sb = New StringBuilder()

    _sql_sb.Append("   SELECT CHS_CHIP_SET_ID, CHS_CHIP_TYPE, CHS_ISO_CODE, CHS_NAME, CHS_ALLOWED")
    _sql_sb.Append("   FROM CHIPS_SETS")
    _sql_sb.Append("   WHERE " + Me.GetSqlWhere())
    _sql_sb.Append("   ORDER BY CHS_NAME, CHS_ALLOWED")

    Return _sql_sb.ToString()
  End Function 'GUI_FilterGetSqlQuery

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        'SEARCH
        Call GUI_StyleSheet()
        If Not IsDisabledAllChipsGroup() Then
          Call MyBase.GUI_ButtonClick(ButtonId)
          If Me.Grid.NumRows > 0 Then
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
          Else
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
          End If

          If (Me.chk_chip_type_RE.Visible = False AndAlso Me.chk_chip_type_NR.Visible = False AndAlso Me.chk_chip_type_Color.Visible = False) Then
            Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
          End If
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        'EDIT
        GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        'NEW
        GUI_ShowEditChipSetsForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        'Double click in grid
        GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Sets the values of a row after show in datagrid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _yes_text As String
    Dim _no_text As String


    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    'Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    If Not DbRow.IsNull(SQL_COLUMN_CHIP_SET_CHIP_TYPE) Then

      If DbRow.Value(SQL_COLUMN_CHIP_SET_CHIP_TYPE) = FeatureChips.ChipType.COLOR Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_NAME).Value = DbRow.Value(SQL_COLUMN_CHIP_SET_NAME)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_ISO_CODE).Value = "---"
      Else
        'EOR 11-APR-2016 
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_NAME).Value = DbRow.Value(SQL_COLUMN_CHIP_SET_NAME)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_ISO_CODE).Value = DbRow.Value(SQL_COLUMN_CHIP_SET_ISO_CODE)
      End If
    End If


    'Change allowed to yes/no
    If Not DbRow.IsNull(SQL_COLUMN_CHIP_SET_ALLOWED) Then
      If DbRow.Value(SQL_COLUMN_CHIP_SET_ALLOWED) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_ALLOWED).Value = _yes_text
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_ALLOWED).Value = _no_text
      End If
    End If

    ' Change ISO CODE to NLS, if iso_code = Currency as CHIP_VALUE
    If Not DbRow.IsNull(SQL_COLUMN_CHIP_SET_ISO_CODE) Then
      ' RGR 29-OCT-2015
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_CHIP_TYPE).Value = Me.GetChipDescription(Me.Grid.Cell(RowIndex, GRID_COLUMN_CHIP_SET_CHIP_TYPE).Value)
    End If

    Return True
  End Function 'GUI_SetupRow

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS:
  '     - none       
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_chips_sets_name

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Activated when a row of the grid is double clicked, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _chip_sets_id As Integer

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    'Get the Chip ID and call function for edit
    _chip_sets_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CHIP_SET_ID).Value
    GUI_ShowEditChipSetsForm(_chip_sets_id)

  End Sub 'GUI_EditSelectedItem

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    ' RGR 29-OCT-2015
    Dim _descriptions As List(Of String)

    _descriptions = New List(Of String)()

    ' LTC  16-FEB-2016
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420), Me.m_name)

    ' LTC 16-FEB-2016
    ' LTC 26-FEB-2016
    If (Not Me.m_chips_types Is Nothing) Then
      For Each _item As ITEM_FILTER In Me.m_chips_types
        _descriptions.Add(_item.name)
      Next
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5062), String.Join(", ", _descriptions.ToArray()))
    End If

    _descriptions.Clear()
    If (Not Me.m_alloweds Is Nothing) Then
      For Each _item As ITEM_FILTER In Me.m_alloweds
        _descriptions.Add(_item.name)
      Next
      PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(281), String.Join(", ", _descriptions.ToArray()))
    End If

    PrintData.SetFilter(NULL_TEXT, NULL_TEXT, True)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5059)

  End Sub ' GUI_ReportParams

#End Region

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - MdiParent
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid

      ' Initialize Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' GRID INDEX
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ID).IsColumnPrintable = False

      ' CHIP type chips
      .Column(GRID_COLUMN_CHIP_SET_CHIP_TYPE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5062) '  "Types of chips"
      .Column(GRID_COLUMN_CHIP_SET_CHIP_TYPE).Width = GRID_WIDTH_CHIP_SET_CHIP_TYPE
      .Column(GRID_COLUMN_CHIP_SET_CHIP_TYPE).Mapping = SQL_COLUMN_CHIP_SET_CHIP_TYPE

      ' CHIP INDEX
      .Column(GRID_COLUMN_CHIP_SET_ID).Header.Text = "ID"
      .Column(GRID_COLUMN_CHIP_SET_ID).Width = GRID_WIDTH_CHIP_SET_ID
      .Column(GRID_COLUMN_CHIP_SET_ID).Mapping = SQL_COLUMN_CHIP_SET_ID

      ' NAME
      ' LTC 16-FEB-2016
      .Column(GRID_COLUMN_CHIP_SET_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) '  "Group"
      .Column(GRID_COLUMN_CHIP_SET_NAME).Width = GRID_WIDTH_CHIP_SET_NAME
      .Column(GRID_COLUMN_CHIP_SET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ISO CODE
      .Column(GRID_COLUMN_CHIP_SET_ISO_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430) ' "Divisa"
      .Column(GRID_COLUMN_CHIP_SET_ISO_CODE).Width = GRID_WIDTH_CHIP_SET_ISO_CODE
      .Column(GRID_COLUMN_CHIP_SET_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ALLOWED
      .Column(GRID_COLUMN_CHIP_SET_ALLOWED).Header.Text = GLB_NLS_GUI_CONTROLS.GetString(281) '   "Allowed"
      .Column(GRID_COLUMN_CHIP_SET_ALLOWED).Width = GRID_WIDTH_CHIP_SET_ALLOWED
      .Column(GRID_COLUMN_CHIP_SET_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CHIP_SET_ALLOWED).Mapping = SQL_COLUMN_CHIP_SET_ALLOWED

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_enabled.Checked = True
    Me.chk_disabled.Checked = False
    Me.ef_chips_sets_name.Value = NULL_TEXT
    'Me.chk_chip_type_1001.Checked = True
    'Me.chk_chip_type_1003.Checked = True
    'Me.chk_chip_type_1002.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Get SQL WHERE 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  ' RGR 29-OCT-2015
  Private Function GetSqlWhere() As String
    Dim _sb As StringBuilder
    Dim _codes As List(Of String)

    _sb = New StringBuilder()
    _codes = New List(Of String)()

    _sb.Append(String.Format("CHS_NAME LIKE '%{0}%'", Me.m_name.Replace("'", "''")))

    If Me.m_alloweds.Count > 0 Then
      _sb.Append(" AND CHS_ALLOWED ")
      For Each item As ITEM_FILTER In Me.m_alloweds
        _codes.Add(item.code)
      Next
      _sb.Append(String.Format("IN ({0})", String.Join(",", _codes.ToArray())))
    End If
    _codes.Clear()
    For Each item As ITEM_FILTER In Me.m_chips_types
      _codes.Add(item.code)
    Next

    If (_codes.Count <> 0) Then
      _sb.Append(" AND CHS_CHIP_TYPE ")
      _sb.Append(String.Format("IN ({0})", String.Join(",", _codes.ToArray())))
    End If

    'EOR 11-APR-2016 
    If Not WSI.Common.Misc.IsMultiCurrencyExchangeEnabled() Then
      _sb.Append(String.Format(" AND CHS_ISO_CODE IN('{0}','{1}')", WSI.Common.CurrencyExchange.GetNationalCurrency(), Cage.CHIPS_COLOR))
    End If

    Return _sb.ToString()
  End Function ' GetSqlWhere

  ' PURPOSE : Launch form for edit / add a chip set, with id as parameter
  '
  '  PARAMS :
  '     - INPUT :
  '         
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_ShowEditChipSetsForm(Optional ByVal m_id As String = NULL_TEXT, _
                                       Optional ByVal m_name As String = NULL_TEXT, _
                                       Optional ByVal m_allowed As String = NULL_TEXT)
    Dim frm_edit As frm_chips_set_edit
    frm_edit = New frm_chips_set_edit()

    ' We select parameter or not
    If m_id <> NULL_TEXT Then
      frm_edit.ShowEditItem(m_id)
    Else
      frm_edit.ShowEditItem(0)
    End If

    frm_edit = Nothing
    Call Me.Grid.Focus()
  End Sub ' GUI_ShowEditChipSetsForm

  ' PURPOSE : Create object Item_Filter
  '
  '  PARAMS :
  '     - INPUT :
  '           - m_id (Optional)
  '           - m_name (Optional)
  '           - m_allowed (Optional)
  '     - OUTPUT :
  '
  ' RETURNS :Item_Filter
  Private Function CreateItemFilter(ByVal Code As Integer, ByVal Name As String) As ITEM_FILTER
    Dim _item As ITEM_FILTER

    _item.code = Code
    _item.name = Name

    Return _item
  End Function 'CreateItemFilter

  ' RGR 29-OCT-2015
  ' PURPOSE : Get chip description
  '
  '  PARAMS :
  '     - INPUT :
  '         
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetChipDescription(ByVal chipType As FeatureChips.ChipType) As String
    Dim _desc As String

    _desc = m_chips_set_names.Item(chipType)

    Return _desc
  End Function 'GetChipDescription

  Private Function IsDisabledAllChipsGroup() As Boolean

    Return (Me.chk_chip_type_RE.Visible = False AndAlso Me.chk_chip_type_NR.Visible = False AndAlso Me.chk_chip_type_Color.Visible = False)
  End Function

  Private Function IsEnabledAllChipsGroup() As Boolean

    Return (Me.chk_chip_type_RE.Visible = True AndAlso Me.chk_chip_type_NR.Visible = True AndAlso Me.chk_chip_type_Color.Visible = True)
  End Function

  Private Function IsNotCheckedAnyChipsGroup() As Boolean

    Return (Me.chk_chip_type_RE.Checked = False AndAlso Me.chk_chip_type_NR.Checked = False AndAlso Me.chk_chip_type_Color.Checked = False)
  End Function

#End Region

End Class