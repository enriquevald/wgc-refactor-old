'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_status
' DESCRIPTION:   Terminal Status 
' AUTHOR:        Ignasi Carre�o
' CREATION DATE: 17-JAN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-JAN-2014  ICS    Initial version.
' 31-JAN-2014  JBC    Hide column when payout flag is activated.
' 06-FEB-2014  AMF    Fixed bug WIGOSTITO-1045: Error on filter.
' 12-FEB-2014  AMF    Fixed bug WIGOSTITO-1062: Error on exit form.
' 19-MAR-2014  DLL    Fixed bug WIGOSTITO-1155: Update filters only when search.
' 08-APR-2014  AMF    Added validation software.
' 22-APR-2014  JBC    New flags played, won, jackpot, canceled credits...
' 06-MAY-2014  JBC    Fixed bug WIG-869: Added new terminal types in the query.
' 29-MAY-2014  JAB    Added permissions: To buttons "Excel" and "Print".
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 06-NOV-2014  LEM    Fixed Bug WIG-1619: Wrong columns shown when "Show terminal location" is activated.
' 18-NOV-2014  SGB    Fixed Bug WIG-1663: Not show alerts that do not apply the filter.
' 27-NOV-2014  DCS    Fixed Bug WIG-1774: Column names not shown when any data in grid
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 18-JUN-2015  FOS    Fixed Bug WIG-2468: Excel filter information incomplete
' 30-JUN-2015  DCS    Fixed Bug WIG-2468: Reorder filters, when there are a lot of alarms selected show "several" and add filter show only selected alarms
' 20-OCT-2015  GMV    TFS 5277 : HighRollers
' 11-NOV-2015  GMV    TFS 5275 : HighRollers Extended definition
' 21-NOV-2017  JML    Product Backlog Item 29621:AGG Data transfer [WIGOS-4458]
' 23-NOV-2017  FGB    WIGOS-6809: Some selection criteria are not taken into account in Terminal Status screen
' 08-FEB-2018  DPC    Bug 31440:[WIGOS-8104]: AGG - MULTISITE: "Details" terminal status windows form is showed in Multisite lottery
' 25-APR-2018  AGS    Bug 32442:WIGOS-10419 [13956] Terminal status screen does not show details
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class frm_terminal_status
  Inherits frm_base_sel

#Region " Structures "

  Public Structure TYPE_dg_alerts_ELEMENT
    Dim parent_code As Integer
    Dim code As Integer
    Dim description As String
    Dim elem_type As Integer

  End Structure

#End Region ' Structures

#Region " Constants "
  Private Const FORM_DATABASE_MIN_VERISON As Short = 203

  Private m_print_data As GUI_Reports.CLASS_PRINT_DATA
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA
  Private m_print_datetime As Date

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private GRID_COLUMN_SENTINEL As Int32
  Private GRID_COLUMN_TERMINAL_ID As Int32
  Private GRID_INIT_TERMINAL_DATA As Int32
  Private GRID_COLUMN_STATUS_SESSION As Int32
  Private GRID_COLUMN_STATUS_SAS_HOST As Int32
  Private GRID_COLUMN_STATUS_WCP_WC2 As Int32
  Private GRID_COLUMN_WS_SERVER_NAME As Int32
  Private GRID_COLUMN_W2S_SERVER_NAME As Int32

  Private GRID_COLUMNS As Integer = 7

  Private Const GRID_COLUMN_FLAG_WIDTH = 2500

  Private Const SQL_COLUMN_TERMINAL_ID = 0
  Private Const SQL_COLUMN_TERMINAL = 1
  Private Const SQL_COLUMN_PROVIDER = 2
  Private Const SQL_COLUMN_IN_USE = 3
  Private Const SQL_COLUMN_WCP_STATUS = 4
  Private Const SQL_COLUMN_WC2_STATUS = 5
  Private Const SQL_COLUMN_SAS_HOST_ERROR = 6
  Private Const SQL_COLUMN_DOOR_BITMASK = 7
  Private Const SQL_COLUMN_BILL_BITMASK = 8
  Private Const SQL_COLUMN_COIN_BITMASK = 9
  Private Const SQL_COLUMN_PRINTER_BITMASK = 10
  Private Const SQL_COLUMN_EGM_BITMASK = 11
  Private Const SQL_COLUMN_PLAYED_WON_BITMASK = 12
  Private Const SQL_COLUMN_JACKPOT_BITMASK = 13
  Private Const SQL_COLUMN_STACKER_STATUS = 14
  Private Const SQL_COLUMN_CALL_ATTENDANT_FLAG = 15
  Private Const SQL_COLUMN_MACHINE_BITMASK = 16
  Private Const SQL_COLUMN_WS_SERVER_NAME = 17
  Private Const SQL_COLUMN_W2S_SERVER_NAME = 18

  ' Grid counters
  Private Const COUNTER_TERMINAL_OK As Integer = 1
  Private Const COUNTER_TERMINAL_WARNING As Integer = 2
  Private Const COUNTER_TERMINAL_ERROR As Integer = 3

  ' Session Status
  Private Const TERM_STATUS_FREE = 1
  Private Const TERM_STATUS_BUSY = 2

  ' WCP & WC2 status
  Private Const TERM_STATUS_DISCONNECTED = 0
  Private Const TERM_STATUS_CONNECTED = 1

  ' Comuniction alert code
  Private Const ALERT_COMM_WCP_WC2 = 0
  Private Const ALERT_COMM_SAS = 1

  Private Const PLAY_SESSION_STATUS_OPEN = 0

  ' Time since last message to consider a terminal as stopped
  Private Const ACTIVITY_TIMEOUT = 180

  ' Timer interval to refresh services status (in milliseconds)
  Private Const REFRESH_TIMEOUT = 5000

  ' ALERTS FILTER GRID
  Private Const GRID_2_COLUMN_PARENT_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CODE As Integer = 1
  Private Const GRID_2_COLUMN_CHECKED As Integer = 2
  Private Const GRID_2_COLUMN_DESC As Integer = 3
  Private Const GRID_2_COLUMN_ROW_TYPE As Integer = 4
  Private Const GRID_2_COLUMNS As Integer = 5
  Private Const GRID_2_HEADER_ROWS As Integer = 0
  Private Const GRID_2_TAB As String = "    "
  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1

  Private Const GRID_MAX_COLUMNS As Integer = 40

  Private Const ALERT_GROUPS_COUNT As Integer = 11

#End Region ' Constants

#Region " Enums "

  Private Enum ENUM_TERMINAL_STATUS
    STATUS_OK = 0
    STATUS_WARNING = 1
    STATUS_ERROR = 2
  End Enum

#End Region ' Enums

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  Private m_filter_term_id_list As String
  Private m_filter_term_selected_one As Boolean
  Private m_filter_term_selected_several As Boolean
  Private m_filter_term_location As String

  Private m_filter_session_free As Boolean
  Private m_filter_session_oc As Boolean

  Private m_filter_status_ok As String
  Private m_filter_status_warning As String
  Private m_filter_status_error As String


  Private m_selected_ids As List(Of Integer) = Nothing
  Private m_first_time As Boolean = True
  Private m_grid_last_rows As Int32 = 0

  Private m_grid_columns As Integer
  Private m_show_stacker_counter As Boolean = False
  Private m_show_won_amount_excedeed As Boolean = False
  Private m_show_plays_second As Boolean = False
  Private m_show_cents_second As Boolean = False
  Private m_show_current_payout As Boolean = False

  Private m_session As String
  Private m_terminal As String
  Private m_warning_filter As String
  Private m_only_alarms_selected As String

  Private m_timer_search As Boolean = False
  Private m_num_of_refreshes As Integer

  Private m_system_mode As SYSTEM_MODE

  Private m_NLS_events_value As Dictionary(Of String, String)

  Private m_refresh_grid As Boolean = False

  Private m_current_site As String = ""
  Private m_terminal_types() As Integer = Nothing

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINALS_STATUS

    Call GUI_SetMinDbVersion(FORM_DATABASE_MIN_VERISON)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _fore_color As Color
    Dim _back_color As Color
    Dim _location As Size

    Call MyBase.GUI_InitControls()

    ' Read System mode
    m_system_mode = WSI.Common.Misc.SystemMode()

    ' Set screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    ' Init grid columns
    m_grid_columns = GRID_COLUMNS

    ' Form Title
    Me.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(213)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(5)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = True

    Me.gb_status.Text = GLB_NLS_GUI_ALARMS.GetString(416)

    Me.cb_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(302)
    GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_OK, _
                          _fore_color, _
                          _back_color)
    lbl_ok.BackColor = _back_color
    lbl_ok.ForeColor = _fore_color

    Me.cb_error.Text = GLB_NLS_GUI_CONTROLS.GetString(303)
    GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_ERROR, _
                          _fore_color, _
                          _back_color)
    lbl_error.BackColor = _back_color
    lbl_error.ForeColor = _fore_color

    Me.cb_warning.Text = GLB_NLS_GUI_CONTROLS.GetString(356)
    GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_WARNING, _
                          _fore_color, _
                          _back_color)
    lbl_warning.BackColor = _back_color
    lbl_warning.ForeColor = _fore_color

    Me.gb_session.Text = GLB_NLS_GUI_CONTROLS.GetString(354)
    Me.cb_session_free.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(218)
    Me.cb_session_oc.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)

    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)
    Me.chk_show_all_alarms.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5722)

    Call GUI_StyleSheet()

    ' Warnings grid
    Call GUI_StyleSheetAlerts()

    gb_alerts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4370)

    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)    ' 452 "Marcar todos"
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)  ' 453 "Desmarcar todos"
    btn_check_all.Width = 100
    btn_uncheck_all.Width = 115

    m_num_of_refreshes = 0

    'JML  20-NOV-2017
    ' Sites
    If WSI.Common.Misc.IsMultisiteCenter() Then
      uc_site_select.Visible = True
      uc_site_select.ShowMultisiteRow = False
      uc_site_select.MultiSelect = False
      uc_site_select.Init()
      uc_site_select.SetDefaultValues(False)
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    Else
      uc_site_select.Visible = False

      _location = uc_pr_list.Location
      _location.Width -= 288
      uc_pr_list.Location = _location

      _location = gb_alerts.Location
      _location.Width -= 288
      gb_alerts.Location = _location

      _location = chk_show_all_alarms.Location
      _location.Width -= 288
      chk_show_all_alarms.Location = _location

      _location = chk_terminal_location.Location
      _location.Width -= 200
      chk_terminal_location.Location = _location
    End If

    ' Set filter default values
    Call SetDefaultValues()

    ' Set Timer
    Me.tmr_monitor.Interval = REFRESH_TIMEOUT
    Me.tmr_monitor.Enabled = True

    If Not WSI.Common.Misc.IsMultisiteCenter() Then
      MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

    ' Refresh grid for first time
    Call TimerRefreshGrid()

  End Sub ' GUI_InitControls

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Me.tmr_monitor.Stop()

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_num_of_refreshes = 0
        MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

    If ButtonId <> ENUM_BUTTON.BUTTON_CANCEL Then
      Me.tmr_monitor.Start()
    End If
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AAUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean
    Select Case AuditType
      Case AUDIT_FLAGS.SEARCH
        Return (Me.m_num_of_refreshes < 1)

      Case Else
        Return MyBase.GUI_HasToBeAudited(AuditType)
    End Select
  End Function ' GUI_HasToBeAudited

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Int32
    Dim _bool_warning As Boolean

    _idx_row = 0
    _bool_warning = False

    Grid.Counter(COUNTER_TERMINAL_OK).Value = 0
    Grid.Counter(COUNTER_TERMINAL_WARNING).Value = 0
    Grid.Counter(COUNTER_TERMINAL_ERROR).Value = 0

    While _idx_row < Me.Grid.NumRows
      If (Me.Grid.Cell(_idx_row, GRID_COLUMN_STATUS_SAS_HOST).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)) Then
        Grid.Counter(COUNTER_TERMINAL_ERROR).Value += 1
      ElseIf (Me.Grid.Cell(_idx_row, GRID_COLUMN_STATUS_WCP_WC2).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)) Then
        Grid.Counter(COUNTER_TERMINAL_ERROR).Value += 1
      Else
        For _idx_column As Int32 = GRID_COLUMNS To Me.Grid.NumColumns - 1
          If Me.Grid.Cell(_idx_row, _idx_column).Value <> String.Empty Then
            Grid.Counter(COUNTER_TERMINAL_WARNING).Value += 1
            _bool_warning = True

            Exit For
          End If
        Next

        If Not _bool_warning Then
          Grid.Counter(COUNTER_TERMINAL_OK).Value += 1
        End If
      End If

      _idx_row += 1
    End While

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    'JML  20-NOV-2017
    ' Site Selected
    If WSI.Common.Misc.IsMultisiteCenter() Then
      ' Debe seleccionar un site."
      If uc_site_select.GetSiteIdSelected() = "" Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4921), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_site_select.Focus()

        Return False
      End If
    End If

    Call GetFiltersValues()

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Get values of filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetFiltersValues()
    m_filter_term_selected_one = Me.uc_pr_list.OneTerminalChecked
    m_filter_term_selected_several = Me.uc_pr_list.SeveralTerminalsChecked
    m_filter_term_id_list = Me.uc_pr_list.GetProviderIdListSelected()

    m_filter_session_free = Me.cb_session_free.Checked()
    m_filter_session_oc = Me.cb_session_oc.Checked()

    m_filter_status_ok = Me.cb_ok.Checked
    m_filter_status_warning = Me.cb_warning.Checked
    m_filter_status_error = Me.cb_error.Checked
  End Sub

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Indicates if to select the first row of the grid or not
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Dim _select_first As Boolean

    _select_first = m_first_time
    m_first_time = False

    Return _select_first
  End Function ' GUI_SelectFirstRow

  ''' <summary>
  ''' Get selected alerts
  ''' </summary>
  ''' <param name="AlarmsSelected"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSelectedAlerts(ByRef AlarmsSelected() As Int32) As Int32
    Dim _alerts_list As List(Of TYPE_dg_alerts_ELEMENT)

    _alerts_list = GetAlertsListSelected()

    'SGB 18-NOV-2014: Not show alerts that do not apply the filter
    For Each _element As TYPE_dg_alerts_ELEMENT In _alerts_list
      Select Case _element.parent_code
        Case TerminalStatusFlags.BITMASK_TYPE.Door
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Door) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Bill
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Bill) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Coin
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Coin) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Printer
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Printer) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.EGM
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.EGM) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Plays
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Plays) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Jackpot
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Jackpot) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Stacker_status
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Stacker_status) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Call_attendant
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Call_attendant) += _element.code

        Case TerminalStatusFlags.BITMASK_TYPE.Machine_status
          AlarmsSelected(TerminalStatusFlags.BITMASK_TYPE.Machine_status) += _element.code
      End Select
    Next

    Return _alerts_list.Count
  End Function

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder
    Dim _str_where As String
    Dim _alarm_selected_code(ALERT_GROUPS_COUNT) As Int32
    Dim _num_selected_alarms As Int32

    'Get values of screen filters
    Call GetFiltersValues()

    'Get selected alarms
    _num_selected_alarms = GetSelectedAlerts(_alarm_selected_code)

    'Get where
    _str_where = GetSqlWhere()

    _str_sql = New StringBuilder()
    _str_sql.AppendLine(" SELECT  TE_TERMINAL_ID ")
    _str_sql.AppendLine("       , TE_NAME ")
    _str_sql.AppendLine("       , TE_PROVIDER_ID ")

    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("       , SESSION_STATE = (CASE ")
      _str_sql.AppendLine("           WHEN (ISNULL(TS_PS_BUSY, 0) = 1) THEN " & TERM_STATUS_BUSY & " ")
      _str_sql.AppendLine("           ELSE " & TERM_STATUS_FREE & " ")
      _str_sql.AppendLine("         END) ")
    Else
      _str_sql.AppendLine("       , SESSION_STATE = (CASE ")
      _str_sql.AppendLine("           WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN " & TERM_STATUS_BUSY & " ")
      _str_sql.AppendLine("           WHEN (PS_PLAY_SESSION_ID IS NULL) THEN " & TERM_STATUS_FREE & " ")
      _str_sql.AppendLine("           ELSE 0 ")
      _str_sql.AppendLine("         END) ")
    End If

    _str_sql.AppendLine("       , WCP_STATE = (CASE ")
    _str_sql.AppendLine("           WHEN (DATEDIFF(SECOND, WS_LAST_RCVD_MSG, GETDATE()) <= " & ACTIVITY_TIMEOUT & ") THEN " & TERM_STATUS_CONNECTED & " ")
    _str_sql.AppendLine("           ELSE " & TERM_STATUS_DISCONNECTED & " ")
    _str_sql.AppendLine("         END) ")
    _str_sql.AppendLine("       , WC2_STATE = (CASE ")
    _str_sql.AppendLine("           WHEN (DATEDIFF(SECOND, W2S_LAST_RCVD_MSG, GETDATE()) <= " & ACTIVITY_TIMEOUT & ") THEN " & TERM_STATUS_CONNECTED & " ")
    _str_sql.AppendLine("           ELSE " & TERM_STATUS_DISCONNECTED & " ")
    _str_sql.AppendLine("         END) ")
    _str_sql.AppendLine("       , ISNULL(TS_SAS_HOST_ERROR, 0) ")

    If Not chk_show_all_alarms.Checked Or (_num_selected_alarms = 0) Then
      _str_sql.AppendLine("     , ISNULL(ts_door_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_bill_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_coin_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_printer_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_EGM_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_played_won_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_jackpot_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_stacker_status, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_call_attendant_flags, 0) ")
      _str_sql.AppendLine("     , ISNULL(ts_machine_flags, 0) ")
    Else
      _str_sql.AppendLine("     , CASE WHEN (TS_DOOR_FLAGS is NULL)            THEN 0 ELSE TS_DOOR_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Door) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_BILL_FLAGS is NULL)            THEN 0 ELSE TS_BILL_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Bill) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_COIN_FLAGS is NULL)            THEN 0 ELSE TS_COIN_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Coin) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_PRINTER_FLAGS is NULL)         THEN 0 ELSE TS_PRINTER_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Printer) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_EGM_FLAGS is NULL)             THEN 0 ELSE TS_EGM_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.EGM) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_PLAYED_WON_FLAGS is NULL)      THEN 0 ELSE TS_PLAYED_WON_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Plays) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_JACKPOT_FLAGS is NULL)         THEN 0 ELSE TS_JACKPOT_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Jackpot) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_STACKER_STATUS is NULL)        THEN 0 ELSE TS_STACKER_STATUS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Stacker_status) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_CALL_ATTENDANT_FLAGS is NULL)  THEN 0 ELSE TS_CALL_ATTENDANT_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Call_attendant) & " END ")
      _str_sql.AppendLine("     , CASE WHEN (TS_MACHINE_FLAGS is NULL)         THEN 0 ELSE TS_MACHINE_FLAGS & " & _alarm_selected_code(TerminalStatusFlags.BITMASK_TYPE.Machine_status) & " END ")
    End If

    _str_sql.AppendLine("       , WS_SERVER_NAME ")
    _str_sql.AppendLine("       , W2S_SERVER_NAME ")
    _str_sql.AppendLine("  FROM   TERMINALS  ")

    If Not WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("  LEFT   OUTER JOIN (SELECT ")
      _str_sql.AppendLine("                              PS_TERMINAL_ID, MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID ")
      _str_sql.AppendLine("                       FROM   PLAY_SESSIONS ")
      _str_sql.AppendLine("                      WHERE   PS_STATUS = " & PLAY_SESSION_STATUS_OPEN & "  ")
      _str_sql.AppendLine("                   GROUP BY   PS_TERMINAL_ID ")
      _str_sql.AppendLine("                    ) LAST_PLAY_SESSIONS ON TE_TERMINAL_ID = PS_TERMINAL_ID ")
    End If

    _str_sql.AppendLine("  LEFT   OUTER JOIN (SELECT ")
    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                            WS_SITE_ID, ")
    End If
    _str_sql.AppendLine("                              WS_TERMINAL_ID, MAX(WS_LAST_RCVD_MSG) AS WS_LAST_RCVD_MSG ")
    _str_sql.AppendLine("                            , MAX(CASE WHEN WS_STATUS = 0 THEN ISNULL(WS_SERVER_NAME, '') ELSE '' END) AS WS_SERVER_NAME ")
    _str_sql.AppendLine("                       FROM   WCP_SESSIONS WITH(INDEX(IX_wcp_status)) ")
    _str_sql.AppendLine("                      WHERE   WS_STATUS = " & PLAY_SESSION_STATUS_OPEN & "  ")
    _str_sql.AppendLine("                   GROUP BY   WS_TERMINAL_ID ")
    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                          , WS_SITE_ID ")
    End If
    _str_sql.AppendLine("                    ) LAST_WCP_SESSIONS ON TE_TERMINAL_ID = WS_TERMINAL_ID ")
    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                                     AND WS_SITE_ID = TE_SITE_ID ")
    End If

    _str_sql.AppendLine("  LEFT   OUTER JOIN (SELECT ")
    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                            W2S_SITE_ID, ")
    End If
    _str_sql.AppendLine("                              W2S_TERMINAL_ID, MAX(W2S_LAST_RCVD_MSG) AS W2S_LAST_RCVD_MSG ")
    _str_sql.AppendLine("                            , MAX(CASE WHEN W2S_STATUS = 0 THEN ISNULL(W2S_SERVER_NAME, '') ELSE '' END) AS W2S_SERVER_NAME ")
    _str_sql.AppendLine("                       FROM   WC2_SESSIONS WITH(INDEX(IX_wc2_status)) ")

    _str_sql.AppendLine("                      WHERE   W2S_STATUS = " & PLAY_SESSION_STATUS_OPEN & "  ")
    _str_sql.AppendLine("                   GROUP BY   W2S_TERMINAL_ID ")

    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                          , W2S_SITE_ID ")
    End If

    _str_sql.AppendLine("                   ) LAST_WC2_SESSIONS ON TE_TERMINAL_ID = W2S_TERMINAL_ID ")

    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                                    AND W2S_SITE_ID = TE_SITE_ID ")
    End If

    _str_sql.AppendLine("       LEFT OUTER JOIN TERMINAL_STATUS ON TE_TERMINAL_ID = TS_TERMINAL_ID ")
    If WSI.Common.Misc.IsMultisiteCenter() Then
      _str_sql.AppendLine("                                    AND TS_SITE_ID = TE_SITE_ID ")
    End If

    _str_sql.AppendLine(_str_where)
    _str_sql.AppendLine("       ORDER BY TE_PROVIDER_ID, TE_NAME, WCP_STATE, WC2_STATE, SESSION_STATE ")

    Return _str_sql.ToString

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _str_sql As String
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_columns As Integer
    Dim _grid_rows As Integer
    Dim _has_changed As Boolean

    Dim _terminal_id As Integer

    _table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"
    _has_changed = False
    _max_rows = GUI_MaxRows()

    Try
      If WSI.Common.Misc.IsMultisiteCenter() And String.IsNullOrEmpty(m_current_site) Then
        Return
      End If

      Me.tmr_monitor.Stop()

      SaveSelectedIds()

      'LTC  29-OCT-2017
      ' Sites
      If (WSI.Common.Misc.IsMultisiteCenter()) Then
        m_current_site = uc_site_select.GetSiteIdSelected()
      End If

      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then

        Return
      End If

      _table = GUI_GetTableUsingSQL(_str_sql, _max_rows)

      If _table Is Nothing Then

        Return
      End If

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      Call GridColumnReIndex()

      _grid_columns = CalculateColumns(_table)

      If _grid_columns <> m_grid_columns Then
        m_grid_columns = _grid_columns
        m_refresh_grid = True
      End If

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      'First, look for terminals and update data if exists in datagrid.
      While _idx_row < Me.Grid.NumRows

        _terminal_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value

        Dim _found_rows() As Data.DataRow

        ' Search for terminal
        _found_rows = _table.Select("te_terminal_id = " & _terminal_id & "")

        If _found_rows.Length > 0 Then

          _db_row = New CLASS_DB_ROW(_found_rows(0))
          Me.Grid.Redraw = False

          If GUI_SetupRow(_idx_row, _db_row) Then
            Me.Grid.Redraw = True
          End If

          ' mark row from datatable to be deleted
          _found_rows(0).Delete()

          _idx_row += 1
        Else
          ' Terminal not found, delete grid row!
          Me.Grid.Redraw = False
          Me.Grid.DeleteRow(_idx_row)
          Me.Grid.Redraw = True
        End If
      End While

      _table.AcceptChanges()

      'Insert new rows of new terminals
      For Each _dr As Data.DataRow In _table.Rows

        Me.Grid.Redraw = False
        Me.Grid.AddRow()

        _db_row = New CLASS_DB_ROW(_dr)

        If GUI_SetupRow(_idx_row, _db_row) Then
          _count += 1
          _idx_row += 1
        End If
        Me.Grid.Redraw = True
      Next

      SelectLastSelectedRow()

      Call GUI_AfterLastRow()

      m_grid_last_rows = _grid_rows

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If
      Me.tmr_monitor.Start()

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Terminal Status", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _fore_color As Color
    Dim _back_color As Color
    Dim _field_status As ENUM_TERMINAL_STATUS
    Dim _bitmask As Int32
    Dim _wcp_wc2_status As Int32
    Dim _current_column As Int32
    Dim _description As String
    Dim _has_changes As Boolean
    Dim _str_terminal_status As String
    Dim _terminal_data As List(Of Object)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Terminal Report
    If (WSI.Common.Misc.IsMultisiteCenter()) Then
      _terminal_data = TerminalReport.GetReportDataList(m_current_site, DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    Else
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    End If

    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value _
         AndAlso Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value <> _terminal_data(_idx) Then

        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        _has_changes = True

      End If
    Next

    If DbRow.Value(SQL_COLUMN_WCP_STATUS) = TERM_STATUS_CONNECTED AndAlso _
       DbRow.Value(SQL_COLUMN_WC2_STATUS) = TERM_STATUS_CONNECTED Then

      If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).Value <> GLB_NLS_GUI_SW_DOWNLOAD.GetString(325) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
        _has_changes = True
      End If

      _field_status = ENUM_TERMINAL_STATUS.STATUS_OK
      Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).BackColor = _back_color

      Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).BackColor = _back_color

      Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).BackColor = _back_color

      _wcp_wc2_status = TERM_STATUS_CONNECTED

    ElseIf DbRow.Value(SQL_COLUMN_WCP_STATUS) = TERM_STATUS_CONNECTED Then

      If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).Value <> GLB_NLS_GUI_SW_DOWNLOAD.GetString(325) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
        _has_changes = True
      End If

      _field_status = ENUM_TERMINAL_STATUS.STATUS_WARNING
      Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).BackColor = _back_color

      _field_status = ENUM_TERMINAL_STATUS.STATUS_OK
      Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).BackColor = _back_color

      _field_status = ENUM_TERMINAL_STATUS.STATUS_WARNING
      Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).BackColor = _back_color

      _wcp_wc2_status = TERM_STATUS_DISCONNECTED

    Else
      _wcp_wc2_status = TERM_STATUS_DISCONNECTED

      If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).Value <> GLB_NLS_GUI_SW_DOWNLOAD.GetString(326) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)
        _has_changes = True

        _field_status = ENUM_TERMINAL_STATUS.STATUS_ERROR

        Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).ForeColor = _fore_color
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_WC2).BackColor = _back_color

        Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).ForeColor = _fore_color
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).BackColor = _back_color

        _field_status = ENUM_TERMINAL_STATUS.STATUS_WARNING
        Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).ForeColor = _fore_color
        Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).BackColor = _back_color

      End If
    End If

    'Private GRID_COLUMN_WS_SERVER_NAME As Int32
    If Not DbRow.Value(SQL_COLUMN_WS_SERVER_NAME) Is DBNull.Value AndAlso _
       DbRow.Value(SQL_COLUMN_WCP_STATUS) = TERM_STATUS_CONNECTED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).Value = DbRow.Value(SQL_COLUMN_WS_SERVER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WS_SERVER_NAME).Value = "---"
    End If

    'Private GRID_COLUMN_W2S_SERVER_NAME As Int32
    If Not DbRow.Value(SQL_COLUMN_W2S_SERVER_NAME) Is DBNull.Value AndAlso _
       DbRow.Value(SQL_COLUMN_WC2_STATUS) = TERM_STATUS_CONNECTED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).Value = DbRow.Value(SQL_COLUMN_W2S_SERVER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_W2S_SERVER_NAME).Value = "---"
    End If

    ' Session status
    Select Case DbRow.Value(SQL_COLUMN_IN_USE)
      Case TERM_STATUS_FREE
        _str_terminal_status = GLB_NLS_GUI_SW_DOWNLOAD.GetString(218)

      Case TERM_STATUS_BUSY
        _str_terminal_status = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)

      Case Else
        _str_terminal_status = String.Empty
    End Select

    If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SESSION).Value <> _str_terminal_status Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SESSION).Value = _str_terminal_status
      _has_changes = True
    End If

    ' SAS_HOST_ERROR
    If _wcp_wc2_status = TERM_STATUS_DISCONNECTED Then

      If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).Value <> GLB_NLS_GUI_CONTROLS.GetString(408) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).Value = GLB_NLS_GUI_CONTROLS.GetString(408)
        _has_changes = True

        ' Unknown
        _field_status = ENUM_TERMINAL_STATUS.STATUS_WARNING

        Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).ForeColor = _fore_color
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).BackColor = _back_color
      End If

    Else
      If DbRow.Value(SQL_COLUMN_SAS_HOST_ERROR) Then

        If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).Value <> GLB_NLS_GUI_SW_DOWNLOAD.GetString(326) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)
          _has_changes = True

          _field_status = ENUM_TERMINAL_STATUS.STATUS_ERROR

          Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).ForeColor = _fore_color
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).BackColor = _back_color

        End If

      Else
        If Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).Value <> GLB_NLS_GUI_SW_DOWNLOAD.GetString(325) Then

          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
          _has_changes = True
          _field_status = ENUM_TERMINAL_STATUS.STATUS_OK

          Call GetUpdateStatusColor(_field_status, _fore_color, _back_color)

          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).ForeColor = _fore_color
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_SAS_HOST).BackColor = _back_color
        End If
      End If
    End If

    _current_column = GRID_COLUMNS - 1

    ' EGM STATUS
    _bitmask = DbRow.Value(SQL_COLUMN_EGM_BITMASK)

    ' DATA RECOVERED FROM CMOS
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.CMOS_RAM_DATA_RECOVERED) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65585")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4049), m_NLS_events_value("65585"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' NO DATA RECOVERED FROM CMOS
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.CMOS_RAM_NO_DATA_RECOVERED) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65586")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4050), m_NLS_events_value("65586"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' CMOS BAD DEVICE
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.CMOS_RAM_BAD_DEVICE) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65587")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4051), m_NLS_events_value("65587"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' EEPROM DATA ERROR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.EPROM_DATA_ERROR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65588")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4052), m_NLS_events_value("65588"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' EEPROM BAD DEVICE
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.EPROM_BAD_DEVICE) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65589")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4053), m_NLS_events_value("65589"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' EEPROM DIFF CHECKSUM
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.EPROM_DIFF_CHECKSUM) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65590")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4054), m_NLS_events_value("65590"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' EEPROM BAD CHECKSUM
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.EPROM_BAD_CHECKSUM) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65591")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4055), m_NLS_events_value("65591"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' PARTITIONED EEPROM DIFF CHECKSUM
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.PART_EPROM_DIFF_CHECKSUM) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65592")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4056), m_NLS_events_value("65592"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' PARTITIONED EEPROM BAD CHECKSUM
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.PART_EPROM_BAD_CHECKSUM) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65593")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4057), m_NLS_events_value("65593"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End If

    ' LOW BACKUP BATTERY DETECTED
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.LOW_BACKUP_BATTERY) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65595")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4059), m_NLS_events_value("65595"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' TECHNICIAN MENU ACTIVATED
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.EGM_FLAG.WITH_TECHNICIAN) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6538)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' DOORS STATUS
    _bitmask = DbRow.Value(SQL_COLUMN_DOOR_BITMASK)

    ' BELLY_DOOR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.DOOR_FLAG.BELLY_DOOR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4491)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' SLOT_DOOR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.DOOR_FLAG.SLOT_DOOR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4492)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' DROP_DOOR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.DOOR_FLAG.DROP_DOOR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4493)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' CASHBOX_DOOR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.DOOR_FLAG.CASHBOX_DOOR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4494)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' CARD_CAGE
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.DOOR_FLAG.CARD_CAGE) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4495)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' BILLS STATUS    
    _bitmask = DbRow.Value(SQL_COLUMN_BILL_BITMASK)

    ' BILL_JAM
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.BILL_FLAG.BILL_JAM) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4514) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4515)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' BILL_ACCEPTOR_HARD_ERROR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.BILL_FLAG.HARD_FAILURE) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4514) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4516)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' COUNTERFEIT_EXCEEDED
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4514) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4517)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' COINS STATUS    
    _bitmask = DbRow.Value(SQL_COLUMN_COIN_BITMASK)

    ' COIN_IN_TILT
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.COIN_FLAG.COIN_IN_TILT) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6712) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6713)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' REVERSE_COIN
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.COIN_FLAG.REVERSE_COIN) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6712) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6714)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' COIN_IN_LOCKOUT
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.COIN_FLAG.COIN_IN_LOCKOUT) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6712) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6715)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PRINTER STATUS
    _bitmask = DbRow.Value(SQL_COLUMN_PRINTER_BITMASK)

    ' PRINTER_COMM_ERROR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PRINTER_FLAG.COMM_ERROR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65632")), _
                                                                              GLB_NLS_GUI_PLAYER_TRACKING.GetString(4519), m_NLS_events_value("65632"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PRINTER_PAPER_LOW
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PRINTER_FLAG.PAPER_LOW) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65652")), _
                                                                        GLB_NLS_GUI_PLAYER_TRACKING.GetString(4520), m_NLS_events_value("65652"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PRINTER_CARRIAGE_JAMMED
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PRINTER_FLAG.CARRIAGE_JAM) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65656")), _
                                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(4521), m_NLS_events_value("65656"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PRINTER_PAPER_OUT_ERROR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PRINTER_FLAG.PAPER_OUT_ERROR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65633")), _
                                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(4522), m_NLS_events_value("65633"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PRINTER_POWER_ERROR
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PRINTER_FLAG.POWER_ERROR) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65653")), _
                                                                  GLB_NLS_GUI_PLAYER_TRACKING.GetString(4523), m_NLS_events_value("65653"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PRINTER REPLACE RIBBON
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PRINTER_FLAG.REPLACE_RIBBON) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": " & IIf(String.IsNullOrEmpty(m_NLS_events_value("65655")), _
                                                            GLB_NLS_GUI_PLAYER_TRACKING.GetString(4524), m_NLS_events_value("65655"))
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PLAYED WON STATUS
    _bitmask = DbRow.Value(SQL_COLUMN_PLAYED_WON_BITMASK)

    ' PLAYED
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PLAYS_FLAG.PLAYED) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' WON
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PLAYS_FLAG.WON) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' PAYOUT
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4498)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' WITHOUT PLAYS
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PLAYS_FLAG.WITHOUT_PLAYS) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4853)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' HIGHROLLER
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' JACKPOT
    _bitmask = DbRow.Value(SQL_COLUMN_JACKPOT_BITMASK)

    ' JACKPOTS
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS) Then
      _current_column += 1
      _description = Resource.String("STR_UC_BANK_HANDPAYS") & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4525)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' CANCELED CREDITS
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT) Then
      _current_column += 1
      _description = Resource.String("STR_UC_BANK_HANDPAYS") & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' STACKER STATUS
    Select Case DbRow.Value(SQL_COLUMN_STACKER_STATUS)
      Case TerminalStatusFlags.STACKER_STATUS.ALMOST_FULL
        _current_column += 1
        _has_changes = SetupFlagRow(RowIndex, _current_column, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4367), ENUM_TERMINAL_STATUS.STATUS_WARNING)
      Case TerminalStatusFlags.STACKER_STATUS.FULL
        _current_column += 1
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4378)
        _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
    End Select

    ' CALL ATTENDANT
    If DbRow.Value(SQL_COLUMN_CALL_ATTENDANT_FLAG) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4541) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4372)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    ' MACHINE STATUS
    _bitmask = DbRow.Value(SQL_COLUMN_MACHINE_BITMASK)

    'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
    ' BLOCKED_INTRUSED
    If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_INTRUSION) Then
      _current_column += 1
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4912) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7115)
      _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
    End If

    If m_system_mode = SYSTEM_MODE.WASS Then
      ' BY SIGNATURE
      If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE) Then
        _current_column += 1
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4912) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4813)
        _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_ERROR)
      End If

      ' MANUALLY
      If TerminalStatusFlags.IsFlagActived(_bitmask, TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY) Then
        _current_column += 1
        _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4912) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4913)
        _has_changes = SetupFlagRow(RowIndex, _current_column, _description, ENUM_TERMINAL_STATUS.STATUS_WARNING)
      End If
    End If


    ' Reset remaining columns
    GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_OK, _fore_color, _back_color)

    While (_current_column < m_grid_columns - 1)
      _current_column += 1
      If Me.Grid.Cell(RowIndex, _current_column).Value <> String.Empty Then
        Me.Grid.Cell(RowIndex, _current_column).Value = String.Empty
        _has_changes = True
        Me.Grid.Cell(RowIndex, _current_column).ForeColor = _fore_color
        Me.Grid.Cell(RowIndex, _current_column).BackColor = _back_color
      End If
    End While

    Return _has_changes
  End Function ' GUI_SetupRow

  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim _idx_row As Int32
    Dim _terminal_id As Integer
    Dim _frm_terminal_status As frm_terminal_status_detail
    Dim _selected_found As Boolean

    Me.tmr_monitor.Stop()
    _terminal_id = 0
    _frm_terminal_status = New frm_terminal_status_detail()
    _selected_found = False

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        _selected_found = True
        Exit For
      End If
    Next

    If Not _selected_found Then
      Return
    End If

    If Not Integer.TryParse(Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value, _terminal_id) Then
      Return
    End If

    _frm_terminal_status.ShowItem(_terminal_id)

    Me.tmr_monitor.Start()

    Call TimerRefreshGrid()

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_EditSelectedItem()
    Call GUI_ShowSelectedItem()
  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Overrides the clear grid routine
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Sub GUI_ClearGrid()
    If m_first_time Then
      Call MyBase.GUI_ClearGrid()
    End If
  End Sub ' GUI_ClearGrid

  ' PURPOSE: What to do when no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  '
  Protected Overrides Sub GUI_NoDataFound()
    If Not m_timer_search Then
      Call MyBase.GUI_NoDataFound()
    End If
  End Sub ' GUI_NoDataFound

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_ALARMS.GetString(416), m_filter_status_ok & ", " & m_filter_status_warning & ", " & m_filter_status_error)

    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(354), m_session)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4371), m_warning_filter)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), m_filter_term_location)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5722), m_only_alarms_selected)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _idx_rows As Int32
    Dim _str_parent As String

    m_session = ""
    m_terminal = ""
    m_warning_filter = ""
    _str_parent = ""
    m_filter_term_location = ""
    m_only_alarms_selected = ""

    m_session = Me.cb_session_oc.Text & ": "
    If Me.cb_session_oc.Checked Then
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_session = m_session & ", " & Me.cb_session_free.Text & ": "

    If Me.cb_session_free.Checked Then
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_filter_status_ok = Me.cb_ok.Text & ": "
    If Me.cb_ok.Checked Then
      m_filter_status_ok = m_filter_status_ok & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_status_ok = m_filter_status_ok & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_filter_status_warning = Me.cb_warning.Text & ": "
    If Me.cb_warning.Checked Then
      m_filter_status_warning = m_filter_status_warning & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_status_warning = m_filter_status_warning & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_filter_status_error = Me.cb_error.Text & ": "
    If Me.cb_error.Checked Then
      m_filter_status_error = m_filter_status_error & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_status_error = m_filter_status_error & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    For _idx_rows = 0 To dg_alerts.NumRows - 1
      If dg_alerts.Cell(_idx_rows, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_DATA _
        AndAlso dg_alerts.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        Select Case CType(dg_alerts.Cell(_idx_rows, GRID_2_COLUMN_PARENT_CODE).Value, TerminalStatusFlags.BITMASK_TYPE)
          Case TerminalStatusFlags.BITMASK_TYPE.Bill
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4514) & ": "
          Case TerminalStatusFlags.BITMASK_TYPE.Door
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490) & ": "
          Case TerminalStatusFlags.BITMASK_TYPE.EGM
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501) & ": "
          Case TerminalStatusFlags.BITMASK_TYPE.Jackpot
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4525) & ": "
          Case TerminalStatusFlags.BITMASK_TYPE.Plays
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496) & ": "
          Case TerminalStatusFlags.BITMASK_TYPE.Printer
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518) & ": "
          Case TerminalStatusFlags.BITMASK_TYPE.Call_attendant
            _str_parent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4541) & ": "
          Case Else
            _str_parent = ""
        End Select

        m_warning_filter &= _str_parent & dg_alerts.Cell(_idx_rows, GRID_2_COLUMN_DESC).Value.Remove(0, GRID_2_TAB.Length()) & ", "
        If m_warning_filter.Length > 100 Then
          m_warning_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2203)
          Exit For
        End If
      End If
    Next

    If String.IsNullOrEmpty(m_warning_filter) Then
      m_warning_filter = GLB_NLS_GUI_INVOICING.GetString(272)
    End If

    If chk_show_all_alarms.Checked Then
      m_only_alarms_selected = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_only_alarms_selected = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If chk_terminal_location.Checked Then
      m_filter_term_location = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_filter_term_location = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Load NLS from meters catalog table
  '
  '  PARAMS:
  '
  ' RETURNS:
  '
  Public Function LoadMetersByGroup() As Dictionary(Of String, String)
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _meter_nls As Dictionary(Of String, String)

    _meter_nls = New Dictionary(Of String, String)
    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _group_id = 0

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("  SELECT   SMC_METER_CODE                                              ")
        _str_sql.AppendLine("         , ISNULL(SMC_NAME, '') AS CATALOG_NAME                        ")
        _str_sql.AppendLine("    FROM   SAS_METERS_CATALOG_PER_GROUP                                ")
        _str_sql.AppendLine("   INNER   JOIN SAS_METERS_CATALOG ON SMC_METER_CODE = SMCG_METER_CODE ")
        _str_sql.AppendLine("   WHERE   SMCG_GROUP_ID =10003 ")
        _str_sql.AppendLine("ORDER BY   SMCG_GROUP_ID ")

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)

            If _sql_adapter.Fill(_dt_counters) > 0 Then
              For Each _row As DataRow In _dt_counters.Rows
                _meter_nls.Add(_row(0), _row(1))
              Next
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _meter_nls
  End Function

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _fore_color As Color
    Dim _back_color As Color
    Dim _idx_grid As Int32
    Dim _terminal_columns As List(Of ColumnSettings)

    _idx_grid = 0

    With Me.Grid
      Call .Init(GRID_MAX_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_OK, _fore_color, _back_color)
      .Counter(COUNTER_TERMINAL_OK).Visible = True
      .Counter(COUNTER_TERMINAL_OK).BackColor = _back_color
      .Counter(COUNTER_TERMINAL_OK).ForeColor = _fore_color

      GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_WARNING, _fore_color, _back_color)
      .Counter(COUNTER_TERMINAL_WARNING).Visible = True
      .Counter(COUNTER_TERMINAL_WARNING).BackColor = _back_color
      .Counter(COUNTER_TERMINAL_WARNING).ForeColor = _fore_color

      GetUpdateStatusColor(ENUM_TERMINAL_STATUS.STATUS_ERROR, _fore_color, _back_color)
      .Counter(COUNTER_TERMINAL_ERROR).Visible = True
      .Counter(COUNTER_TERMINAL_ERROR).BackColor = _back_color
      .Counter(COUNTER_TERMINAL_ERROR).ForeColor = _fore_color

      GridColumnReIndex()

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' Terminal ID
      .Column(GRID_COLUMN_TERMINAL_ID).Header.Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5417)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      ' Status Session
      .Column(GRID_COLUMN_STATUS_SESSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5417)
      .Column(GRID_COLUMN_STATUS_SESSION).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS_SESSION).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)
      .Column(GRID_COLUMN_STATUS_SESSION).Width = 1200
      .Column(GRID_COLUMN_STATUS_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status SAS_HOST_ERROR
      .Column(GRID_COLUMN_STATUS_SAS_HOST).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4487)
      .Column(GRID_COLUMN_STATUS_SAS_HOST).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS_SAS_HOST).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4489)
      .Column(GRID_COLUMN_STATUS_SAS_HOST).Width = 1800
      .Column(GRID_COLUMN_STATUS_SAS_HOST).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_STATUS_SAS_HOST).HighLightWhenSelected = False

      ' Status WCP + WC2
      .Column(GRID_COLUMN_STATUS_WCP_WC2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4488)
      .Column(GRID_COLUMN_STATUS_WCP_WC2).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS_WCP_WC2).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5890)
      .Column(GRID_COLUMN_STATUS_WCP_WC2).Width = 1800
      .Column(GRID_COLUMN_STATUS_WCP_WC2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_STATUS_WCP_WC2).HighLightWhenSelected = False

      ' GRID_COLUMN_WS_SERVER_NAME As Int32
      .Column(GRID_COLUMN_WS_SERVER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4488)
      .Column(GRID_COLUMN_WS_SERVER_NAME).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_WS_SERVER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5891)
      .Column(GRID_COLUMN_WS_SERVER_NAME).Width = 1800
      .Column(GRID_COLUMN_WS_SERVER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_WS_SERVER_NAME).HighLightWhenSelected = False

      ' GRID_COLUMN_W2S_SERVER_NAME As Int32
      .Column(GRID_COLUMN_W2S_SERVER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4488)
      .Column(GRID_COLUMN_W2S_SERVER_NAME).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_W2S_SERVER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5892)
      .Column(GRID_COLUMN_W2S_SERVER_NAME).Width = 1800
      .Column(GRID_COLUMN_W2S_SERVER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_W2S_SERVER_NAME).HighLightWhenSelected = False

      ' FLAGS COLUMNS
      For _idx As Integer = GRID_COLUMNS To m_grid_columns
        _idx_grid += 1
        .Column(_idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5893)
        .Column(_idx).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(_idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4370) & " " & (_idx_grid)
        .Column(_idx).Width = GRID_COLUMN_FLAG_WIDTH
        .Column(_idx).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(_idx).HighLightWhenSelected = False
      Next

      ' HIDING FLAGS COLUMNS
      For _idx As Integer = (m_grid_columns) To GRID_MAX_COLUMNS - 1
        .Column(_idx).Width = 0
      Next

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ReDim Preserve m_terminal_types(0 To 0)
    m_terminal_types(0) = WSI.Common.TerminalTypes.SAS_HOST

    ' Sites
    'JML  20-NOV-2017
    If (WSI.Common.Misc.IsMultisiteCenter()) Then
      Call uc_site_select.Init()
      uc_site_select.SetDefaultValues(False)
      m_current_site = uc_site_select.GetFirstSiteId()
      uc_site_select.SetSitesIdListSelected(m_current_site)
    End If

    'JML 20-NOV-2017
    If (WSI.Common.Misc.IsMultisiteCenter()) Then
      Call Me.uc_pr_list.Init(m_terminal_types, GUI_Controls.uc_provider.UC_FILTER_TYPE.ALL_ACTIVE, IIf(WSI.Common.Misc.IsMultisiteCenter(), m_current_site, Nothing), True)
    Else
      Call Me.uc_pr_list.Init(m_terminal_types)
    End If
    ' Set checkboxs status
    cb_session_free.Checked = True
    cb_session_oc.Checked = True
    cb_ok.Checked = False
    cb_warning.Checked = True
    cb_error.Checked = True

    chk_terminal_location.Checked = False

    Call InitAlertsFilter()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As String
    Dim _sql_filter_type As String
    Dim _sql_session_filter As String
    Dim _sql_alerts_filter As String
    Dim _sql_status_filter As String
    Dim _sql_site_filter As String

    _sql_site_filter = String.Empty

    _sql_filter_type = " AND TE_STATUS = " & WSI.Common.TerminalStatus.ACTIVE
    If Not String.IsNullOrEmpty(m_filter_term_id_list) Then
      _sql_filter_type &= " AND TE_TERMINAL_ID IN " & m_filter_term_id_list
    End If

    'Session
    _sql_session_filter = GetSqlWhereSession()
    If Not String.IsNullOrEmpty(_sql_session_filter) Then
      _sql_filter_type = _sql_filter_type & " AND (" + _sql_session_filter + ") "
    End If

    'Alerts
    _sql_alerts_filter = GetSqlWhereAlerts()
    If Not String.IsNullOrEmpty(_sql_alerts_filter) Then
      _sql_filter_type = _sql_filter_type & " AND (" + _sql_alerts_filter + ") "
    End If

    'Status
    _sql_status_filter = GetSqlWhereStatus()
    If Not String.IsNullOrEmpty(_sql_status_filter) Then
      _sql_filter_type = _sql_filter_type & " AND (" + _sql_status_filter + ") "
    End If

    If WSI.Common.Misc.IsMultisiteCenter() And Not String.IsNullOrEmpty(m_current_site) Then
      _sql_site_filter = "AND TE_SITE_ID = " + m_current_site + " "
    End If

    _str_where = " WHERE te_client_id IS NOT NULL " & _
                  " AND te_build_id IS NOT NULL " & _
                  _sql_site_filter & _
    _sql_filter_type

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Calculate columns to show
  '
  '  PARAMS:
  '     - INPUT:
  '           - Datatable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Number of columns to show
  '
  Private Function CalculateColumns(ByVal Table As DataTable) As Int32
    Dim _flag_columns As Int32 = 0
    Dim _row_flags As Int32

    For Each _row As DataRow In Table.Rows
      _row_flags = TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_DOOR_BITMASK))
      _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_BILL_BITMASK))
      _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_COIN_BITMASK))
      _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_EGM_BITMASK))
      _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_PRINTER_BITMASK))

      'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
      'If m_system_mode = SYSTEM_MODE.WASS Then
      _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_MACHINE_BITMASK))
      'End If

      If TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_PLAYED_WON_BITMASK)) <> 0 Then
        _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_PLAYED_WON_BITMASK))
      End If

      _row_flags += TerminalStatusFlags.CountSetBits(_row.Item(SQL_COLUMN_JACKPOT_BITMASK))

      If _row.Item(SQL_COLUMN_CALL_ATTENDANT_FLAG) Then
        _row_flags += 1
      End If

      If _row.Item(SQL_COLUMN_STACKER_STATUS) <> TerminalStatusFlags.STACKER_STATUS.OK Then
        _row_flags += 1
      End If

      If _row_flags > _flag_columns Then
        _flag_columns = _row_flags
      End If
    Next

    Return _flag_columns + GRID_COLUMNS
  End Function ' CalculateColumns

  ' PURPOSE: Get color by status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_TERMINAL_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '
  Private Sub GetUpdateStatusColor(ByVal Status As ENUM_TERMINAL_STATUS, _
                                    ByRef ForeColor As Color, _
                                    ByRef BackColor As Color)
    Select Case Status

      Case ENUM_TERMINAL_STATUS.STATUS_OK
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      Case ENUM_TERMINAL_STATUS.STATUS_WARNING
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      Case ENUM_TERMINAL_STATUS.STATUS_ERROR
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_00)

    End Select
  End Sub 'GetUpdateStatusColor

  ' PURPOSE: Save the selected Terminal Ids from the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SaveSelectedIds()
    Dim idx As Integer
    Dim _term_id As Integer

    If m_selected_ids IsNot Nothing Then
      m_selected_ids.Clear()
      m_selected_ids = Nothing
    End If

    m_selected_ids = New List(Of Integer)
    idx = 0
    While idx < Me.Grid.NumRows
      If Me.Grid.Row(idx).IsSelected Then
        If Integer.TryParse(Me.Grid.Cell(idx, GRID_COLUMN_TERMINAL_ID).Value, _term_id) Then
          m_selected_ids.Add(_term_id)
        End If
      End If
      idx = idx + 1
    End While
  End Sub ' SaveSelectedIds

  ' PURPOSE: Select in the Grid the latest selected Alarm Ids that have been saved.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SelectLastSelectedRow()
    Dim idx As Integer
    Dim any_selected As Boolean
    Dim _term_id As Integer

    If Me.Grid.NumRows = 0 Then
      Return
    End If
    If m_selected_ids Is Nothing Then
      Return
    End If
    If m_selected_ids.Count = 0 Then
      Return
    End If

    any_selected = False
    Me.Grid.Redraw = False

    Try
      idx = 0
      While idx < Me.Grid.NumRows
        Me.Grid.Row(idx).IsSelected = False
        Call Me.Grid.RepaintRow(idx)
        idx = idx + 1
      End While

      idx = 0
      While idx < Me.Grid.NumRows
        If Not Integer.TryParse(Me.Grid.Cell(idx, GRID_COLUMN_TERMINAL_ID).Value, _term_id) Then
          idx = idx + 1
          Continue While
        End If
        If m_selected_ids.Contains(_term_id) Then
          any_selected = True
          Me.Grid.Row(idx).IsSelected = True
          Call Me.Grid.RepaintRow(idx)
        End If
        idx = idx + 1
      End While

      If Not any_selected And Me.Grid.CurrentRow >= 0 Then
        Me.Grid.Row(Me.Grid.CurrentRow).IsSelected = True
        Call Me.Grid.RepaintRow(Me.Grid.CurrentRow)
      End If

    Finally
      Me.Grid.Redraw = True
    End Try
  End Sub ' SelectLastSelectedRow

  ' PURPOSE: Update grid with especific flag value
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex
  '           - ColIndex
  '           - Description
  '           - FlagStatus
  '     - OUTPUT:
  '           - TerminalStatus
  '
  ' RETURNS:
  '     - None
  '
  Private Function SetupFlagRow(ByVal RowIndex As Int32, ByVal ColIndex As Int32, ByVal Description As String, _
                           ByVal FlagStatus As ENUM_TERMINAL_STATUS) As Boolean
    Dim _fore_color As Color
    Dim _back_color As Color
    Dim _result As Boolean

    If Me.Grid.Cell(RowIndex, ColIndex).Value <> Description Then
      Me.Grid.Cell(RowIndex, ColIndex).Value = Description

      Call GetUpdateStatusColor(FlagStatus, _fore_color, _back_color)

      Me.Grid.Cell(RowIndex, ColIndex).ForeColor = _fore_color
      Me.Grid.Cell(RowIndex, ColIndex).BackColor = _back_color

      _result = True
    Else
      _result = False
    End If

    Return _result
  End Function 'SetupFlagRow

  ' PURPOSE: Execute proper actions when programmed timer elapses
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub TimerRefreshGrid()
    m_timer_search = True
    m_num_of_refreshes += 1

    Call GUI_ExecuteQueryCustom()
    Call GUI_AfterLastRow()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_PRINT_ENABLED).Read
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = (Me.Grid.NumRows > 0)
    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    m_timer_search = False

    Me.Grid.Redraw = True
  End Sub ' TimerRefreshGrid

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in status filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add
  Private Function GetSqlWhereStatus() As String
    Dim _str_where As String

    _str_where = ""

    ' Check if all them are checked or all are not checked
    If (cb_ok.Checked AndAlso cb_warning.Checked AndAlso cb_error.Checked) Or _
        (Not cb_ok.Checked AndAlso Not cb_warning.Checked AndAlso Not cb_error.Checked) Then
      Return String.Empty
    End If

    If cb_ok.Checked Then
      _str_where = "( TS_SAS_HOST_ERROR = 0 " & _
                   " AND (datediff(second,ws_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") AND (datediff(second,w2s_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") " & _
                   " AND TS_BILL_FLAGS = 0 " & _
                   " AND TS_PLAYED_WON_FLAGS = 0 " & _
                   " AND TS_CALL_ATTENDANT_FLAGS = 0 " & _
                   " AND TS_DOOR_FLAGS = 0 " & _
                   " AND TS_EGM_FLAGS = 0 " & _
                   " AND TS_JACKPOT_FLAGS = 0 " & _
                   " AND TS_PRINTER_FLAGS = 0 " & _
                   " AND TS_STACKER_STATUS = 0 " & _
                   " AND TS_MACHINE_FLAGS = 0  " & _
                   " ) "
    End If

    If cb_warning.Checked Then
      If Not String.IsNullOrEmpty(_str_where) Then
        _str_where &= " OR "
      End If

      _str_where &= "TS_BILL_FLAGS <> 0 " & _
                    "OR TS_PLAYED_WON_FLAGS <> 0 " & _
                    "OR TS_CALL_ATTENDANT_FLAGS <> 0 " & _
                    "OR TS_DOOR_FLAGS <> 0 " & _
                    "OR TS_JACKPOT_FLAGS <> 0 " & _
                    "OR TS_PRINTER_FLAGS <> 0 " & _
                    "OR TS_STACKER_STATUS <> 0 " & _
                    "OR TS_MACHINE_FLAGS <> 0 " & _
                    "OR TS_EGM_FLAGS = " & TerminalStatusFlags.EGM_FLAG.LOW_BACKUP_BATTERY & _
                    "OR TS_EGM_FLAGS = " & TerminalStatusFlags.EGM_FLAG.WITH_TECHNICIAN
    End If

    If cb_error.Checked Then
      If Not String.IsNullOrEmpty(_str_where) Then
        _str_where &= " OR "
      End If

      _str_where &= "TS_SAS_HOST_ERROR = 1 " & _
                    "OR ( ws_last_rcvd_msg IS NULL OR (datediff(second,ws_last_rcvd_msg,getdate()) > " & ACTIVITY_TIMEOUT & ")) OR ( w2s_last_rcvd_msg IS NULL OR (datediff(second,w2s_last_rcvd_msg,getdate()) > " & ACTIVITY_TIMEOUT & "))" & _
                    "OR ( TS_EGM_FLAGS <> " & TerminalStatusFlags.EGM_FLAG.LOW_BACKUP_BATTERY & " AND TS_EGM_FLAGS <> 0 )"

    End If

    Return _str_where
  End Function ' GetSqlWhereAlerts

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SENTINEL = 0
    GRID_COLUMN_TERMINAL_ID = 1
    GRID_INIT_TERMINAL_DATA = 2
    GRID_COLUMN_STATUS_SESSION = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_STATUS_SAS_HOST = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_STATUS_WCP_WC2 = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_WS_SERVER_NAME = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_W2S_SERVER_NAME = TERMINAL_DATA_COLUMNS + 6

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 7

  End Sub


#Region " Alerts Filter "

  ' PURPOSE: Initialize the warnings filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitAlertsFilter()
    ' Reset dg alerts
    Call FillAlertsFilterGrid(GetAlertsFilterGridData())

  End Sub ' InitMovementTypesFilter

  ' PURPOSE: Obtain the list of elements that will populate the 
  '         alerts filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  Private Function GetAlertsFilterGridData() As List(Of TYPE_dg_alerts_ELEMENT)

    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _dg_alerts_rows_list As List(Of TYPE_dg_alerts_ELEMENT)
    Dim _row As TYPE_dg_alerts_ELEMENT


    m_NLS_events_value = LoadMetersByGroup()

    _dg_alerts_rows_list = New List(Of TYPE_dg_alerts_ELEMENT)

    ' Header row - Comunications
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Sas_host_error
    _row.code = 0
    _row.description = GLB_NLS_GUI_ALARMS.GetString(303)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' TERMINAL - COLECTOR DISCONNECTED
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Sas_host_error
    _row.code = ALERT_COMM_SAS
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4369)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' COLECTOR - SITE DISCONNECTED
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Sas_host_error
    _row.code = ALERT_COMM_WCP_WC2
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4368)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - PLAYS
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' PLAYED
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = TerminalStatusFlags.PLAYS_FLAG.PLAYED
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' WON
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = TerminalStatusFlags.PLAYS_FLAG.WON
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CURRENT PAYOUT
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4498)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' WITHOUT PLAYS
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = TerminalStatusFlags.PLAYS_FLAG.WITHOUT_PLAYS
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4853)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' HIGHROLLER
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Plays
    _row.code = TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - HANDPAYS
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Jackpot
    _row.code = 0
    _row.description = Resource.String("STR_UC_BANK_HANDPAYS")
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' JACKPOT
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Jackpot
    _row.code = TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4525)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CANCELED CREDITS
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Jackpot
    _row.code = TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - BILLS
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Bill
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4514)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' BILL JAM 65576
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Bill
    _row.code = TerminalStatusFlags.BILL_FLAG.BILL_JAM
    If Not String.IsNullOrEmpty(m_NLS_events_value("65576")) Then
      _row.description = m_NLS_events_value("65576")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4515)
    End If

    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' HARDWARE FAILURE 65577
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Bill
    _row.code = TerminalStatusFlags.BILL_FLAG.HARD_FAILURE
    If Not String.IsNullOrEmpty(m_NLS_events_value("65577")) Then
      _row.description = m_NLS_events_value("65577")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4516)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' COUNTERFEIT_EXCEEDED
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Bill
    _row.code = TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - COINS
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Coin
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6712)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' BILL JAM 65576
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Coin
    _row.code = TerminalStatusFlags.COIN_FLAG.COIN_IN_TILT
    If Not String.IsNullOrEmpty(m_NLS_events_value("65576")) Then
      _row.description = m_NLS_events_value("65576")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6713)
    End If

    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' HARDWARE FAILURE 65577
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Coin
    _row.code = TerminalStatusFlags.COIN_FLAG.REVERSE_COIN
    If Not String.IsNullOrEmpty(m_NLS_events_value("65577")) Then
      _row.description = m_NLS_events_value("65577")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6714)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' COUNTERFEIT_EXCEEDED
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Coin
    _row.code = TerminalStatusFlags.COIN_FLAG.COIN_IN_LOCKOUT
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6715)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - Satcker Status
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Stacker_status
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2987)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' NEAR FULL
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Stacker_status
    _row.code = TerminalStatusFlags.STACKER_STATUS.ALMOST_FULL
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4367)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' FULL
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Stacker_status
    _row.code = TerminalStatusFlags.STACKER_STATUS.FULL
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4378)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - Call Attendant
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Call_attendant
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4541)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' CALL_ATTENDANT
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Call_attendant
    _row.code = 1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4372)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - PRINTER
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' COMM_ERROR 65632
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = TerminalStatusFlags.PRINTER_FLAG.COMM_ERROR
    If Not String.IsNullOrEmpty(m_NLS_events_value("65632")) Then
      _row.description = m_NLS_events_value("65632")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4519)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' PAPER_OUT_ERROR 65633
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = TerminalStatusFlags.PRINTER_FLAG.PAPER_OUT_ERROR
    If Not String.IsNullOrEmpty(m_NLS_events_value("65633")) Then
      _row.description = m_NLS_events_value("65633")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4522)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' REPLACE_RIBBON 65655
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = TerminalStatusFlags.PRINTER_FLAG.REPLACE_RIBBON
    If Not String.IsNullOrEmpty(m_NLS_events_value("65655")) Then
      _row.description = m_NLS_events_value("65655")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4524)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' PAPER_LOW 65652
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = TerminalStatusFlags.PRINTER_FLAG.PAPER_LOW
    If Not String.IsNullOrEmpty(m_NLS_events_value("65652")) Then
      _row.description = m_NLS_events_value("65652")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4520)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CARRIAGE_JAM 65656
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = TerminalStatusFlags.PRINTER_FLAG.CARRIAGE_JAM
    If Not String.IsNullOrEmpty(m_NLS_events_value("65656")) Then
      _row.description = m_NLS_events_value("65656")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4521)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' POWER_ERROR 65653
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Printer
    _row.code = TerminalStatusFlags.PRINTER_FLAG.POWER_ERROR
    If Not String.IsNullOrEmpty(m_NLS_events_value("65653")) Then
      _row.description = m_NLS_events_value("65653")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4523)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - Doors
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Door
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' SLOT_DOOR
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Door
    _row.code = TerminalStatusFlags.DOOR_FLAG.SLOT_DOOR
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4492)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' DROOP DOOR
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Door
    _row.code = TerminalStatusFlags.DOOR_FLAG.DROP_DOOR
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4493)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CARD_CAGE
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Door
    _row.code = TerminalStatusFlags.DOOR_FLAG.CARD_CAGE
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4495)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' BELLY DOOR
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Door
    _row.code = TerminalStatusFlags.DOOR_FLAG.BELLY_DOOR
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4491)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CASHBOX_DOOR
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Door
    _row.code = TerminalStatusFlags.DOOR_FLAG.CASHBOX_DOOR
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4494)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' Header row - EGM
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)

    ' CMOS_RAM_DATA_RECOVERED 65585
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.CMOS_RAM_DATA_RECOVERED
    If Not String.IsNullOrEmpty(m_NLS_events_value("65585")) Then
      _row.description = m_NLS_events_value("65585")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4049)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CMOS_RAM_NO_DATA_RECOVERED 65586
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.CMOS_RAM_NO_DATA_RECOVERED
    If Not String.IsNullOrEmpty(m_NLS_events_value("65586")) Then
      _row.description = m_NLS_events_value("65586")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4050)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' CMOS_RAM_BAD_DEVICE 65587
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.CMOS_RAM_BAD_DEVICE
    If Not String.IsNullOrEmpty(m_NLS_events_value("65587")) Then
      _row.description = m_NLS_events_value("65587")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4051)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' EPROM_DATA_ERROR 65588
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.EPROM_DATA_ERROR
    If Not String.IsNullOrEmpty(m_NLS_events_value("65588")) Then
      _row.description = m_NLS_events_value("65588")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4052)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' EPROM_BAD_DEVICE 65589
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.EPROM_BAD_DEVICE
    If Not String.IsNullOrEmpty(m_NLS_events_value("65589")) Then
      _row.description = m_NLS_events_value("65589")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4053)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' EPROM_DIFF_CHECKSUM 65590
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.EPROM_DIFF_CHECKSUM
    If Not String.IsNullOrEmpty(m_NLS_events_value("65590")) Then
      _row.description = m_NLS_events_value("65590")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4054)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' EPROM_BAD_CHECKSUM 65591
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.EPROM_BAD_CHECKSUM
    If Not String.IsNullOrEmpty(m_NLS_events_value("65591")) Then
      _row.description = m_NLS_events_value("65591")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4055)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' PART_EPROM_DIFF_CHECKSUM 65592
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.PART_EPROM_DIFF_CHECKSUM
    If Not String.IsNullOrEmpty(m_NLS_events_value("65592")) Then
      _row.description = m_NLS_events_value("65592")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4056)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' PART_EPROM_BAD_CHECKSUM 65593
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.PART_EPROM_BAD_CHECKSUM
    If Not String.IsNullOrEmpty(m_NLS_events_value("65593")) Then
      _row.description = m_NLS_events_value("65593")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4057)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' LOW_BACKUP_BATTERY 65595
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.LOW_BACKUP_BATTERY
    If Not String.IsNullOrEmpty(m_NLS_events_value("65595")) Then
      _row.description = m_NLS_events_value("65595")
    Else
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4059)
    End If
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    ' LOW_BACKUP_BATTERY 65595
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.EGM
    _row.code = TerminalStatusFlags.EGM_FLAG.WITH_TECHNICIAN
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6538)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
    ' Header row - Machine status
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Machine_status
    _row.code = 0
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4912)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    _dg_alerts_rows_list.Add(_row)
    ' Intrusion
    _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Machine_status
    _row.code = TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_INTRUSION
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7115)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _dg_alerts_rows_list.Add(_row)

    If m_system_mode = SYSTEM_MODE.WASS Then
      ' Locked by signature
      _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Machine_status
      _row.code = TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4813)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_alerts_rows_list.Add(_row)

      ' Locked manually
      _row.parent_code = TerminalStatusFlags.BITMASK_TYPE.Machine_status
      _row.code = TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4913)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      _dg_alerts_rows_list.Add(_row)
    End If

    Return _dg_alerts_rows_list

  End Function ' GetWarningsFilterGridData

  ' PURPOSE: Define the layout of the alerts grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetAlerts()
    With Me.dg_alerts
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_PARENT_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_PARENT_CODE).WidthFixed = 0
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 3400
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_ROW_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_ROW_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetWarnings

  ' PURPOSE: Add a new data row the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - WarningCode: numeric identifier
  '           - WarningDesc: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowData(ByVal WarningCode As Integer, ByVal WarningParentCode As Integer, ByVal WarningDesc As String)
    Dim _prev_redraw As Boolean

    _prev_redraw = Me.dg_alerts.Redraw
    Me.dg_alerts.Redraw = False

    Me.dg_alerts.AddRow()

    Me.dg_alerts.Redraw = False

    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_PARENT_CODE).Value = WarningParentCode
    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_CODE).Value = WarningCode
    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & WarningDesc
    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_DATA

    Me.dg_alerts.Row(dg_alerts.NumRows - 1).Height = 0

    Me.dg_alerts.Redraw = _prev_redraw
  End Sub ' AddOneRowData

  ' PURPOSE: Add a new header row to the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - HeaderMsg: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowHeader(ByVal HeaderMsg As String)

    dg_alerts.AddRow()

    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_DESC).Value = HeaderMsg
    dg_alerts.Cell(dg_alerts.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER
    Call dg_alerts.Row(dg_alerts.NumRows - 1).SetSignalAllowExpand(GRID_2_COLUMN_DESC)
  End Sub ' AddOneRowHeader

  ' PURPOSE: Populate the alerts data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillAlertsFilterGrid(ByVal DgList As List(Of TYPE_dg_alerts_ELEMENT))

    Me.dg_alerts.Clear()
    Me.dg_alerts.Redraw = False

    ' Get the filters array and insert it into the grid.

    For Each _element As TYPE_dg_alerts_ELEMENT In DgList
      If (_element.elem_type = GRID_2_ROW_TYPE_DATA) Then
        Call AddOneRowData(_element.code, _element.parent_code, _element.description)
      Else
        If (_element.elem_type = GRID_2_ROW_TYPE_HEADER) Then
          Call AddOneRowHeader(_element.description)
        End If
      End If
    Next

    Me.dg_alerts.Redraw = True

  End Sub ' FillWarningsFilterGrid

  ' PURPOSE: Indicate if a given row in the filter data grid is a header or a data row
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIdx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: specified row is a header row
  '     - False: specified row is a data row
  Private Function RowIsHeader(ByVal RowIdx As Integer) As Boolean

    Return (Me.dg_alerts.Cell(RowIdx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER)

  End Function ' RowIsHeader

  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To dg_alerts.NumRows - 1
      If Me.dg_alerts.Cell(idx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

        Exit Sub
      End If

      Me.dg_alerts.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been unchecked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _checked As Boolean

    _idx_bottom_header = -1
    _idx_header = -1
    ' Go look for the header from current row upwards
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If

      If Me.dg_alerts.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _idx_header = -1 Then
        ' One or more upper rows are already unchecked
        _checked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_alerts.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_alerts.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _checked = True
      End If
    Next

    'update de check status of the header row when some or all the dependant rows has been checked
    If Not _checked Then
      Me.dg_alerts.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Else
      Me.dg_alerts.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If

  End Sub ' TryToUncheckHeader

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToCheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _is_unchecked As Boolean

    _is_unchecked = False
    _idx_header = -1
    _idx_bottom_header = -1
    ' Go look for the header from current row upwards usually controls if any item is unchecked
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If
      If _idx_header = -1 And Me.dg_alerts.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If

    Next
    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_alerts.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_alerts.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Updates the header check status if all rows are checked
    If _is_unchecked = False Then
      Me.dg_alerts.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.dg_alerts.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If
  End Sub ' TryToCheckHeader

  ' PURPOSE: Get the list of the items selected in the filter data grid
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String contining id's of the selected rows
  Private Function GetAlertsListSelected() As List(Of TYPE_dg_alerts_ELEMENT)
    Dim _idx_row As Integer
    Dim _list_warnings As List(Of TYPE_dg_alerts_ELEMENT)
    Dim _element As TYPE_dg_alerts_ELEMENT

    _list_warnings = New List(Of TYPE_dg_alerts_ELEMENT)

    For _idx_row = 0 To Me.dg_alerts.NumRows - 1
      If dg_alerts.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _
         dg_alerts.Cell(_idx_row, GRID_2_COLUMN_ROW_TYPE).Value = CStr(GRID_2_ROW_TYPE_DATA) Then
        _element = New TYPE_dg_alerts_ELEMENT()
        _element.parent_code = dg_alerts.Cell(_idx_row, GRID_2_COLUMN_PARENT_CODE).Value
        _element.code = dg_alerts.Cell(_idx_row, GRID_2_COLUMN_CODE).Value
        _element.description = dg_alerts.Cell(_idx_row, GRID_2_COLUMN_DESC).Value
        _element.elem_type = GRID_2_ROW_TYPE_DATA
        _list_warnings.Add(_element)
      End If
    Next

    Return _list_warnings
  End Function 'GetWarningsListSelected

  ' PURPOSE: Check consistency of the data grid filter values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: filter values are consistent
  '     - False: filter values are not consistent
  Private Function CheckAlertsFilter() As Boolean

    If Not cb_ok.Checked AndAlso (cb_warning.Checked OrElse cb_error.Checked) Then
      If GetAlertsListSelected().Count = 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(102), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dg_alerts.CurrentRow = 0
        Me.dg_alerts.CurrentCol = GRID_2_COLUMN_CHECKED
        Me.dg_alerts.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' CheckWarningsFilter

  ''' <summary>
  ''' Get sql filter for session
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlWhereSession() As String
    Dim _str_where As String

    _str_where = String.Empty

    If m_filter_session_free And m_filter_session_oc Then
      _str_where = String.Empty
    ElseIf m_filter_session_free Then
      If WSI.Common.Misc.IsMultisiteCenter Then
        _str_where = "(ISNULL(TS_PS_BUSY, 0) = 0)"
      Else
        _str_where = "(PS_PLAY_SESSION_ID IS NULL)"
      End If
    ElseIf m_filter_session_oc Then
      If WSI.Common.Misc.IsMultisiteCenter Then
        _str_where = "(ISNULL(TS_PS_BUSY, 0) = 1)"
      Else
        _str_where = "(PS_PLAY_SESSION_ID IS NOT NULL)"
      End If
    End If

    Return _str_where
  End Function

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add
  Private Function GetSqlWhereAlerts() As String
    Dim _str_where As String
    Dim _alerts_list As List(Of TYPE_dg_alerts_ELEMENT)
    Dim _sql_part_where As String

    _str_where = ""
    _alerts_list = GetAlertsListSelected()

    ' Check that ok button is checked and there is at least on alert filter actived
    If cb_ok.Checked And _alerts_list.Count > 0 Then
      _str_where &= "( TS_SAS_HOST_ERROR = 0 " & _
                    " AND (datediff(second,ws_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") AND (datediff(second,w2s_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") " & _
                    " AND TS_BILL_FLAGS = 0 " & _
                    " AND TS_COIN_FLAGS = 0 " & _
                    " AND TS_CALL_ATTENDANT_FLAGS = 0 " & _
                    " AND TS_DOOR_FLAGS = 0 " & _
                    " AND TS_EGM_FLAGS = 0 " & _
                    " AND TS_JACKPOT_FLAGS = 0 " & _
                    " AND TS_PRINTER_FLAGS = 0 " & _
                    " AND TS_STACKER_STATUS = 0 " & _
                    " AND TS_MACHINE_FLAGS = 0 " & _
                    " ) "
    End If

    ' Operation Code Type Filter
    If (Not cb_warning.Checked AndAlso Not cb_error.Checked) Then
      Return _str_where
    End If

    _alerts_list = GetAlertsListSelected()
    For Each _element As TYPE_dg_alerts_ELEMENT In _alerts_list
      _sql_part_where = String.Empty

      Select Case _element.parent_code
        Case TerminalStatusFlags.BITMASK_TYPE.Bill
          _sql_part_where = "TS_BILL_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Coin
          _sql_part_where = "TS_COIN_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Call_attendant
          _sql_part_where = "TS_CALL_ATTENDANT_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Door
          _sql_part_where = "TS_DOOR_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.EGM
          _sql_part_where = "TS_EGM_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Jackpot
          _sql_part_where = "TS_JACKPOT_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Plays
          _sql_part_where = "TS_PLAYED_WON_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Printer
          _sql_part_where = "TS_PRINTER_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Stacker_status
          _sql_part_where = "TS_STACKER_STATUS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Machine_status
          _sql_part_where = "TS_MACHINE_FLAGS & " & _element.code & " = " & _element.code
        Case TerminalStatusFlags.BITMASK_TYPE.Sas_host_error
          If _element.code = ALERT_COMM_WCP_WC2 Then
            _sql_part_where = "( ws_last_rcvd_msg IS NULL OR (datediff(second,ws_last_rcvd_msg,getdate()) > " & ACTIVITY_TIMEOUT & ")) OR ( w2s_last_rcvd_msg IS NULL OR (datediff(second,w2s_last_rcvd_msg,getdate()) > " & ACTIVITY_TIMEOUT & "))"
          End If
          If _element.code = ALERT_COMM_SAS Then
            _sql_part_where = "TS_SAS_HOST_ERROR = 1"
          End If
      End Select

      If _str_where = String.Empty Then
        _str_where = _sql_part_where
      Else
        _str_where &= " OR " & _sql_part_where
      End If
    Next

    Return _str_where
  End Function ' GetSqlWhereAlerts

#End Region ' Alerts Filter

#End Region  ' Private Functions

#Region " Events "

  Private Sub tmr_monitor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_monitor.Tick
    Call TimerRefreshGrid()
  End Sub ' tmr_monitor_Tick

  Private Sub btn_check_all_ClickEvent() Handles btn_check_all.ClickEvent
    Dim _idx_rows As Integer

    For _idx_rows = 0 To dg_alerts.NumRows - 1
      dg_alerts.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next
  End Sub ' btn_check_all_ClickEvent

  Private Sub btn_uncheck_all_ClickEvent() Handles btn_uncheck_all.ClickEvent
    Dim _idx_rows As Integer

    For _idx_rows = 0 To dg_alerts.NumRows - 1
      dg_alerts.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub ' btn_uncheck_all_ClickEvent

  Private Sub dg_alerts_DataSelectedEvent() Handles dg_alerts.DataSelectedEvent
    Dim _idx As Integer
    Dim _last_row As Integer
    Dim _row_is_header As Boolean

    With Me.dg_alerts
      _row_is_header = RowIsHeader(.CurrentRow)

      If _row_is_header And .CurrentCol <> GRID_2_COLUMN_CHECKED Then
        _last_row = .CurrentRow
        For _idx = .CurrentRow + 1 To .NumRows - 1
          If RowIsHeader(_idx) Then

            Exit For
          End If
          _last_row = _idx
        Next

        Me.dg_alerts.CollapseExpand(.CurrentRow, _last_row, GRID_2_COLUMN_DESC)

      End If
    End With
  End Sub ' dg_alerts_DataSelectedEvent

  Private Sub dg_alerts_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_alerts.CellDataChangedEvent
    Dim _current_row As Integer

    ' Prevent recursive calls
    If m_refreshing_grid = True Then

      Exit Sub
    End If

    With Me.dg_alerts

      _current_row = .CurrentRow
      m_refreshing_grid = True

      If RowIsHeader(.CurrentRow) Then
        Call ChangeCheckofRows(.CurrentRow, .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
      Else
        If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          Call TryToUncheckHeader(.CurrentRow)
        Else
          Call TryToCheckHeader(.CurrentRow)
        End If
      End If

      m_refreshing_grid = False
      .CurrentRow = _current_row

    End With
  End Sub ' dg_alerts_CellDataChangedEvent

  ' PURPOSE: Helps to refresh grid when the focus is out
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           - None
  '
  Private Sub frm_terminal_status_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
    Me.Grid.Redraw = True
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

  'JML  20-NOV-2017
  ' PURPOSE: Update terminals grid when the siteis changed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           - None
  '
  Private Sub CellDataChangedEvent() Handles uc_site_select.CellDataChangedEvent

    m_current_site = uc_site_select.GetSiteIdSelected()
    If m_current_site = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4921), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.uc_site_select.Focus()
      Return
    End If

    'Clean Terminals
    Call Me.uc_pr_list.Init(m_terminal_types, GUI_Controls.uc_provider.UC_FILTER_TYPE.ALL_ACTIVE, "", True)

    'Populate Terminals
    Call Me.uc_pr_list.Init(m_terminal_types, GUI_Controls.uc_provider.UC_FILTER_TYPE.ALL_ACTIVE, m_current_site, True)

  End Sub

#End Region ' Events

End Class ' frm_terminal_status
