﻿'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cash_out_report
' DESCRIPTION:   This screen allows to view the cash out receipts:
'                           
'
' AUTHOR:        Jesús García
' CREATION DATE: 11-JUN-2018
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-JUN-2018  JGC    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Public Class frm_cash_out_report

  Inherits frm_base_sel

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_PROV_ID As Integer = 0
  Private Const SQL_COLUMN_DATE_TIME As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 2
  Private Const SQL_COLUMN_VOUCHER_ID As Integer = 3
  Private Const SQL_COLUMN_PLAY_SESSION_ID As Integer = 4
  Private Const SQL_COLUMN_STATUS As Integer = 5
  Private Const SQL_COLUMN_PROVIDER As Integer = 6
  Private Const SQL_COLUMN_EGM As Integer = 7
  Private Const SQL_COLUMN_TRANSACTION_NO As Integer = 8
  Private Const SQL_COLUMN_RECEIPT_NO As Integer = 9
  Private Const SQL_COLUMN_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 11
  Private Const SQL_COLUMN_ACCOUNT_HOLDER As Integer = 12

  ' Grid Columns
  Private Const GRID_COLUMN_DATE_TIME As Integer = 7
  Private Const GRID_COLUMN_PROVIDER As Integer = 0
  Private Const GRID_COLUMN_EGM As Integer = 1
  Private Const GRID_COLUMN_TRANSACTION_NO As Integer = 4
  Private Const GRID_COLUMN_RECEIPT_NO As Integer = 3
  Private Const GRID_COLUMN_AMOUNT As Integer = 2
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 6
  Private Const GRID_COLUMN_ACCOUNT_HOLDER As Integer = 5

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' DATAGRID Width
  Private Const GRID_WIDTH_DATE_TIME As Integer = 2000
  Private Const GRID_WIDTH_PROVIDER As Integer = 2000
  Private Const GRID_WIDTH_EGM As Integer = 2200
  Private Const GRID_WIDTH_TRANSACTION_NO As Integer = 1800
  Private Const GRID_WIDTH_RECEIPT_NO As Integer = 1800
  Private Const GRID_WIDTH_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1150
  Private Const GRID_WIDTH_ACCOUNT_HOLDER As Integer = 2500

  ' Grid counters
  Private Const COUNTER_TOTAL As Integer = 0
  Private Const COUNTER_INFO As Integer = 1
  Private Const COUNTER_TOTAL_VISIBLE As Boolean = False
  Private Const COUNTER_INFO_VISIBLE As Boolean = True
  Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

  ' Anonymous
  Private Const ANONYMOUS_ID = -1

#End Region ' Constants

#Region " Members "

  ' Filters
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_account As String
  Private m_card As String
  Private m_holder As String
  Private m_providers As String
  Private m_anonymous As String

  Private m_totals_data_table As DataTable
  Private m_list_different_partial_terminal As List(Of String)
  Private m_list_different_provider As List(Of String)
  Private m_list_different_holder As List(Of String)
  Private m_list_different_terminal As List(Of String)
  Private m_partial_totals_data_table As DataTable
  Private m_current_provider As Integer
  Private m_current_provider_name As String

  Private m_first_load As Boolean = False
  Private m_grid_counter As Integer
#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASH_OUT_REPORT
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9104)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date time filter
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Only anonymous
    chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)

    Call GUI_StyleSheet()

    Call InitTotalsDataTable()

    ' Set filter default values
    Call SetDefaultValues()

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Providers only
    Call Me.uc_pr_only_list.Init(WSI.Common.Misc.GamingTerminalTypeList(), uc_provider.UC_FILTER_TYPE.ONLY_PROVIDERS)

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    'Call Grid.Clear()
    SetDefaultValues()
    ResetPartialTotalsDataTable()
    ResetTotalsDataTable()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Return cls_cash_out_report.GetDataReport(dtp_from, dtp_to, uc_pr_only_list, uc_pr_list, uc_account_sel1, chk_only_anonymous.Checked)
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_account_sel1
  End Sub ' GUI_SetInitialFocus

  ''' <summary>
  ''' Set the total row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    ' Update grid counter
    Me.Grid.Counter(COUNTER_INFO).Value = m_grid_counter

    AddPartialTotalRow()
    ResetPartialTotalsDataTable()

    Dim _idx_row As Integer
    Dim _row_totals As DataRow

    If Me.Grid.NumRows > 0 Then
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      _row_totals = m_totals_data_table.Rows(0)

      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = GLB_NLS_GUI_INVOICING.GetString(205) + _row_totals("PROV_TOTAL")
      Me.Grid.Cell(_idx_row, GRID_COLUMN_EGM).Value = _row_totals("EGM_TOTAL")
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_row_totals("AMOUNT_TOTAL"))
      Me.Grid.Cell(_idx_row, GRID_COLUMN_RECEIPT_NO).Value = _row_totals("RECEIPT_TOTAL")
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_HOLDER).Value = _row_totals("HOLDER_TOTAL")

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    End If

    ResetTotalsDataTable()

  End Sub ' GUI_AfterLastRow

  ''' <summary>
  ''' GUI_CheckOutRowBeforeAdd
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal Row As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If m_current_provider = 0 Then
      m_first_load = True
      m_current_provider = Row.Value(SQL_COLUMN_PROV_ID)
      m_current_provider_name = Row.Value(SQL_COLUMN_PROVIDER).ToString
    End If

    If m_current_provider <> Row.Value(SQL_COLUMN_PROV_ID) Then
      ' Add partial total and reset counters
      AddPartialTotalRow()
      ResetPartialTotalsDataTable()

      ' Save new provider
      m_current_provider = Row.Value(SQL_COLUMN_PROV_ID)
      m_current_provider_name = Row.Value(SQL_COLUMN_PROVIDER).ToString
    End If

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If Not DbRow.IsNull(SQL_COLUMN_DATE_TIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TIME).Value = DbRow.Value(SQL_COLUMN_DATE_TIME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PROVIDER) Then
      If m_current_provider_name <> DbRow.Value(SQL_COLUMN_PROVIDER) Or m_first_load Then

        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)

        ' Only on first load put value by default
        If m_first_load Then
          m_first_load = False
        End If
      End If
    End If

    If Not DbRow.IsNull(SQL_COLUMN_EGM) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EGM).Value = DbRow.Value(SQL_COLUMN_EGM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TRANSACTION_NO) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSACTION_NO).Value = DbRow.Value(SQL_COLUMN_TRANSACTION_NO)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_RECEIPT_NO) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_RECEIPT_NO).Value = DbRow.Value(SQL_COLUMN_RECEIPT_NO)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_HOLDER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_HOLDER).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_HOLDER)
    End If

    Call SetTotalsDataTable(DbRow)

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Report filters.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = String.Empty
    m_date_to = String.Empty
    m_providers = String.Empty
    m_terminals = String.Empty
    m_account = String.Empty
    m_card = String.Empty
    m_holder = String.Empty
    m_anonymous = String.Empty

    If Not String.IsNullOrEmpty(uc_account_sel1.Account) Then
      m_account = uc_account_sel1.Account
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.TrackData) Then
      m_card = uc_account_sel1.TrackData
    End If

    If Not String.IsNullOrEmpty(uc_account_sel1.Holder) Then
      m_holder = uc_account_sel1.Holder
    End If

    ' Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText

    ' Providers
    m_providers = Me.uc_pr_only_list.GetTerminalReportText

    If Not String.IsNullOrEmpty(dtp_from.Value) Then
      m_date_from = dtp_from.Value
    End If

    If Not String.IsNullOrEmpty(dtp_to.Value) Then
      m_date_to = dtp_to.Value
    End If

    m_anonymous = Me.chk_only_anonymous.Checked

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(115), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_card)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(String.Format("{0} {1}", GLB_NLS_GUI_INVOICING.GetString(201), GLB_NLS_GUI_INVOICING.GetString(202)), m_date_from)
    PrintData.SetFilter(String.Format("{0} {1}", GLB_NLS_GUI_INVOICING.GetString(201), GLB_NLS_GUI_INVOICING.GetString(203)), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(232), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(423), m_providers)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), Me.chk_only_anonymous.Checked)

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 3000
    PrintData.FilterHeaderWidth(3) = 800
    PrintData.FilterValueWidth(3) = 3300

  End Sub ' GUI_ReportFilter

  ' ''' <summary>
  ' ''' Validates the Filter selection where needed
  ' ''' </summary>
  ' ''' <returns></returns>
  ' ''' <remarks></remarks>
  'Protected Overrides Function GUI_FilterCheck() As Boolean

  '  ' Account selection
  '  If Not Me.uc_account_sel1.ValidateFormat() Then
  '    Return False
  '  End If

  '  Return True
  'End Function ' GUI_FilterCheck

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.uc_pr_list.SetGroupBoxTitle(GLB_NLS_GUI_INVOICING.GetString(232))

    Me.uc_account_sel1.Clear()

    Me.chk_only_anonymous.Checked = False

    Me.Grid.Counter(COUNTER_INFO).Value = 0

  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Style Sheet for the look of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_TOTAL).Visible = COUNTER_TOTAL_VISIBLE

      .Counter(COUNTER_INFO).Visible = COUNTER_INFO_VISIBLE
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      ' DATE TIME
      .Column(GRID_COLUMN_DATE_TIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9093)
      .Column(GRID_COLUMN_DATE_TIME).Width = GRID_WIDTH_DATE_TIME
      .Column(GRID_COLUMN_DATE_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9097)
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' EGM
      .Column(GRID_COLUMN_EGM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9098)
      .Column(GRID_COLUMN_EGM).Width = GRID_WIDTH_EGM
      .Column(GRID_COLUMN_EGM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TRANSACTION NO 
      .Column(GRID_COLUMN_TRANSACTION_NO).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9099)
      .Column(GRID_COLUMN_TRANSACTION_NO).Width = GRID_WIDTH_TRANSACTION_NO
      .Column(GRID_COLUMN_TRANSACTION_NO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' RECEIPT NO
      .Column(GRID_COLUMN_RECEIPT_NO).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9100)
      .Column(GRID_COLUMN_RECEIPT_NO).Width = GRID_WIDTH_RECEIPT_NO
      .Column(GRID_COLUMN_RECEIPT_NO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9101)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ACCOUNT ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9102)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT_ID
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ACCOUNT HOLDER
      .Column(GRID_COLUMN_ACCOUNT_HOLDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9103)
      .Column(GRID_COLUMN_ACCOUNT_HOLDER).Width = GRID_WIDTH_ACCOUNT_HOLDER
      .Column(GRID_COLUMN_ACCOUNT_HOLDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With
  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Fill total datatable
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <remarks></remarks>
  Private Sub SetTotalsDataTable(ByRef Row As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _row_totals As DataRow
    Dim _new_provider As Boolean

    _new_provider = False

    ' Full total data table
    If m_totals_data_table.Rows.Count = 0 Then
      _row_totals = m_totals_data_table.NewRow()
      _row_totals.Item("PROV_TOTAL") = 0
      _row_totals.Item("EGM_TOTAL") = 0
      _row_totals.Item("AMOUNT_TOTAL") = 0
      _row_totals.Item("RECEIPT_TOTAL") = 0
      _row_totals.Item("HOLDER_TOTAL") = 0
      m_totals_data_table.Rows.Add(_row_totals)
    End If

    _row_totals = m_totals_data_table.Rows(0)

    If Not Row.IsNull(SQL_COLUMN_ACCOUNT_HOLDER) Then
      If Not (m_list_different_holder.Contains(Row.Value(SQL_COLUMN_ACCOUNT_HOLDER))) Then
        m_list_different_holder.Add(Row.Value(SQL_COLUMN_ACCOUNT_HOLDER))
      End If
    Else
      ' If anonymous we have to add one time to count total anonymous
      If Not (m_list_different_holder.Contains(ANONYMOUS_ID)) Then
        m_list_different_holder.Add(ANONYMOUS_ID)
      End If
    End If

    If Not Row.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      ' Add to terminal list
      If Not (m_list_different_terminal.Contains(Row.Value(SQL_COLUMN_TERMINAL_ID))) Then
        m_list_different_terminal.Add(Row.Value(SQL_COLUMN_TERMINAL_ID))
      End If

      ' Add to partial terminal list
      If Not (m_list_different_partial_terminal.Contains(Row.Value(SQL_COLUMN_TERMINAL_ID))) Then
        m_list_different_partial_terminal.Add(Row.Value(SQL_COLUMN_TERMINAL_ID))
      End If
    End If

    If Not Row.IsNull(SQL_COLUMN_PROV_ID) Then
      If Not (m_list_different_provider.Contains(Row.Value(SQL_COLUMN_PROV_ID))) Then
        m_list_different_provider.Add(Row.Value(SQL_COLUMN_PROV_ID))
      End If
    End If

    _row_totals.Item("PROV_TOTAL") = m_list_different_provider.Count
    _row_totals.Item("EGM_TOTAL") = m_list_different_terminal.Count
    _row_totals.Item("AMOUNT_TOTAL") = _row_totals.Item("AMOUNT_TOTAL") + Row.Value(SQL_COLUMN_AMOUNT)
    _row_totals.Item("RECEIPT_TOTAL") = _row_totals.Item("RECEIPT_TOTAL") + 1
    _row_totals.Item("HOLDER_TOTAL") = m_list_different_holder.Count

    ' Partial total data table
    If m_partial_totals_data_table.Rows.Count = 0 Then
      _row_totals = m_partial_totals_data_table.NewRow()
      _row_totals.Item("EGM_PARTIAL") = 0
      _row_totals.Item("AMOUNT_PARTIAL") = 0
      m_partial_totals_data_table.Rows.Add(_row_totals)
    End If

    _row_totals = m_partial_totals_data_table.Rows(0)

    _row_totals.Item("EGM_PARTIAL") = m_list_different_partial_terminal.Count
    _row_totals.Item("AMOUNT_PARTIAL") = _row_totals.Item("AMOUNT_PARTIAL") + Row.Value(SQL_COLUMN_AMOUNT)

    'grid counter
    m_grid_counter = m_grid_counter + 1

  End Sub ' SetTotalsDataTable

  ''' <summary>
  ''' Create total datatable 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitTotalsDataTable()

    Dim _column As DataColumn

    If m_totals_data_table IsNot Nothing Then
      m_totals_data_table.Dispose()
      m_totals_data_table = Nothing
      m_partial_totals_data_table.Dispose()
      m_partial_totals_data_table = Nothing
    End If

    m_totals_data_table = New DataTable("TOTAL")
    m_partial_totals_data_table = New DataTable("PARTIAL")

    ' Full total
    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.String")
    _column.ColumnName = "PROV_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ColumnName = "EGM_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)


    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ColumnName = "AMOUNT_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)


    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ColumnName = "RECEIPT_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ColumnName = "HOLDER_TOTAL"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    ' Partial total
    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.String")
    _column.ColumnName = "PROV_PARTIAL"
    _column.ReadOnly = False
    m_partial_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ColumnName = "EGM_PARTIAL"
    _column.ReadOnly = False
    m_partial_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ColumnName = "AMOUNT_PARTIAL"
    _column.ReadOnly = False
    m_partial_totals_data_table.Columns.Add(_column)

    m_list_different_provider = New List(Of String)
    m_list_different_holder = New List(Of String)
    m_list_different_terminal = New List(Of String)
    m_list_different_partial_terminal = New List(Of String)

    m_current_provider = 0

  End Sub ' InitTotalsDataTable

  ''' <summary>
  ''' Reet partial total datatable 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetTotalsDataTable()

    Dim _row_totals As DataRow

    If m_totals_data_table.Rows.Count > 0 Then

      _row_totals = m_totals_data_table.Rows(0)
      _row_totals.Item("PROV_TOTAL") = 0
      _row_totals.Item("EGM_TOTAL") = 0
      _row_totals.Item("AMOUNT_TOTAL") = 0
      _row_totals.Item("RECEIPT_TOTAL") = 0
      _row_totals.Item("HOLDER_TOTAL") = 0

      m_list_different_provider.Clear()
      m_list_different_holder.Clear()
      m_list_different_terminal.Clear()
      m_list_different_partial_terminal.Clear()

    End If

    m_grid_counter = 0
    m_first_load = True

  End Sub ' ResetPartialTotalsDataTable

  ''' <summary>
  ''' Reet partial total datatable 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetPartialTotalsDataTable()

    Dim _row_totals As DataRow

    If m_partial_totals_data_table.Rows.Count > 0 Then
      _row_totals = m_partial_totals_data_table.Rows(0)
      _row_totals.Item("EGM_PARTIAL") = 0
      _row_totals.Item("AMOUNT_PARTIAL") = 0
    End If

    m_first_load = True
    m_list_different_partial_terminal.Clear()

  End Sub ' ResetTotalsDataTable

  ''' <summary>
  ''' Set the total row
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddPartialTotalRow()

    Dim _idx_row As Integer
    Dim _row_totals As DataRow

    If Me.Grid.NumRows > 0 Then
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      _row_totals = m_partial_totals_data_table.Rows(0)

      'Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = m_current_provider_name
      Me.Grid.Cell(_idx_row, GRID_COLUMN_EGM).Value = _row_totals("EGM_PARTIAL")
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(_row_totals("AMOUNT_PARTIAL"))

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    End If
  End Sub ' AddPartialTotalRow

#End Region ' Private Functions

End Class ' frm_cash_out_report