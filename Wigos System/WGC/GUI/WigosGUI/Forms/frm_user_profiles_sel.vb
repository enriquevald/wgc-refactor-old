'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_user_profiles_sel.vb
' DESCRIPTION:   User Profiles selection form
' AUTHOR:        Ignasi Ripoll
' CREATION DATE: 18-OCT-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-OCT-2002  IRP    Initial version.
' 16-AUG-2012  XCD    Added filter show state and column user 
'                     fullname on grid
' 07-FEB-2013  JCM    Search locked filter: Locked by inactivity + Locked login attemps
' 24-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 22-MAY-2013  ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters
' 13-FEB-2014  LJM    Added Employee code to user
' 19-FEB-2014  AMF    Fixed Bug WIG-647: Control minimum version
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 19-MAY-2015  JML    Multisite-Multicurrency, User sales limit disabled
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On 

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

#End Region

Public Class frm_user_profiles_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_results As System.Windows.Forms.GroupBox
  Friend WithEvents chk_user As System.Windows.Forms.CheckBox
  Friend WithEvents chk_profile As System.Windows.Forms.CheckBox
  Friend WithEvents ef_profile As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_color_user As System.Windows.Forms.Label
  Friend WithEvents lbl_color_profile As System.Windows.Forms.Label
  Friend WithEvents chk_show_unsubscribed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_not_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_master_profile As System.Windows.Forms.Label
  Friend WithEvents chk_master_profile As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_corporate_user As System.Windows.Forms.Label
  Friend WithEvents chk_corporate_user As System.Windows.Forms.CheckBox
  Friend WithEvents ef_user As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_results = New System.Windows.Forms.GroupBox
    Me.lbl_color_corporate_user = New System.Windows.Forms.Label
    Me.chk_corporate_user = New System.Windows.Forms.CheckBox
    Me.lbl_color_master_profile = New System.Windows.Forms.Label
    Me.chk_master_profile = New System.Windows.Forms.CheckBox
    Me.lbl_color_user = New System.Windows.Forms.Label
    Me.lbl_color_profile = New System.Windows.Forms.Label
    Me.chk_user = New System.Windows.Forms.CheckBox
    Me.chk_profile = New System.Windows.Forms.CheckBox
    Me.chk_show_disabled = New System.Windows.Forms.CheckBox
    Me.chk_show_not_blocked = New System.Windows.Forms.CheckBox
    Me.chk_show_unsubscribed = New System.Windows.Forms.CheckBox
    Me.ef_profile = New GUI_Controls.uc_entry_field
    Me.ef_user = New GUI_Controls.uc_entry_field
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.chk_show_blocked = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_results.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.ef_user)
    Me.panel_filter.Controls.Add(Me.ef_profile)
    Me.panel_filter.Controls.Add(Me.gb_results)
    Me.panel_filter.Size = New System.Drawing.Size(1150, 120)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_results, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_profile, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 124)
    Me.panel_data.Size = New System.Drawing.Size(1150, 482)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1144, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1144, 4)
    '
    'gb_results
    '
    Me.gb_results.Controls.Add(Me.lbl_color_corporate_user)
    Me.gb_results.Controls.Add(Me.chk_corporate_user)
    Me.gb_results.Controls.Add(Me.lbl_color_master_profile)
    Me.gb_results.Controls.Add(Me.chk_master_profile)
    Me.gb_results.Controls.Add(Me.lbl_color_user)
    Me.gb_results.Controls.Add(Me.lbl_color_profile)
    Me.gb_results.Controls.Add(Me.chk_user)
    Me.gb_results.Controls.Add(Me.chk_profile)
    Me.gb_results.Location = New System.Drawing.Point(454, 10)
    Me.gb_results.Name = "gb_results"
    Me.gb_results.Size = New System.Drawing.Size(208, 63)
    Me.gb_results.TabIndex = 3
    Me.gb_results.TabStop = False
    Me.gb_results.Text = "xResultados"
    '
    'lbl_color_corporate_user
    '
    Me.lbl_color_corporate_user.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_color_corporate_user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_corporate_user.Location = New System.Drawing.Point(181, 85)
    Me.lbl_color_corporate_user.Name = "lbl_color_corporate_user"
    Me.lbl_color_corporate_user.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_corporate_user.TabIndex = 110
    '
    'chk_corporate_user
    '
    Me.chk_corporate_user.Enabled = False
    Me.chk_corporate_user.Location = New System.Drawing.Point(10, 86)
    Me.chk_corporate_user.Name = "chk_corporate_user"
    Me.chk_corporate_user.Size = New System.Drawing.Size(150, 16)
    Me.chk_corporate_user.TabIndex = 3
    Me.chk_corporate_user.Text = "xUsuarioCorporativo"
    '
    'lbl_color_master_profile
    '
    Me.lbl_color_master_profile.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.lbl_color_master_profile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_master_profile.Location = New System.Drawing.Point(181, 63)
    Me.lbl_color_master_profile.Name = "lbl_color_master_profile"
    Me.lbl_color_master_profile.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_master_profile.TabIndex = 108
    '
    'chk_master_profile
    '
    Me.chk_master_profile.Enabled = False
    Me.chk_master_profile.Location = New System.Drawing.Point(10, 64)
    Me.chk_master_profile.Name = "chk_master_profile"
    Me.chk_master_profile.Size = New System.Drawing.Size(150, 16)
    Me.chk_master_profile.TabIndex = 2
    Me.chk_master_profile.Text = "xPerfilMaestro"
    '
    'lbl_color_user
    '
    Me.lbl_color_user.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_color_user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_user.Location = New System.Drawing.Point(181, 41)
    Me.lbl_color_user.Name = "lbl_color_user"
    Me.lbl_color_user.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_user.TabIndex = 106
    '
    'lbl_color_profile
    '
    Me.lbl_color_profile.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.lbl_color_profile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_profile.Location = New System.Drawing.Point(181, 19)
    Me.lbl_color_profile.Name = "lbl_color_profile"
    Me.lbl_color_profile.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_profile.TabIndex = 105
    '
    'chk_user
    '
    Me.chk_user.Location = New System.Drawing.Point(10, 42)
    Me.chk_user.Name = "chk_user"
    Me.chk_user.Size = New System.Drawing.Size(150, 16)
    Me.chk_user.TabIndex = 1
    Me.chk_user.Text = "xUsuario"
    '
    'chk_profile
    '
    Me.chk_profile.Location = New System.Drawing.Point(10, 20)
    Me.chk_profile.Name = "chk_profile"
    Me.chk_profile.Size = New System.Drawing.Size(150, 16)
    Me.chk_profile.TabIndex = 0
    Me.chk_profile.Text = "xPerfil"
    '
    'chk_show_disabled
    '
    Me.chk_show_disabled.Location = New System.Drawing.Point(13, 64)
    Me.chk_show_disabled.Name = "chk_show_disabled"
    Me.chk_show_disabled.Size = New System.Drawing.Size(154, 16)
    Me.chk_show_disabled.TabIndex = 2
    Me.chk_show_disabled.Text = "chk show disabled"
    Me.chk_show_disabled.UseVisualStyleBackColor = True
    '
    'chk_show_not_blocked
    '
    Me.chk_show_not_blocked.Location = New System.Drawing.Point(13, 20)
    Me.chk_show_not_blocked.Name = "chk_show_not_blocked"
    Me.chk_show_not_blocked.Size = New System.Drawing.Size(154, 16)
    Me.chk_show_not_blocked.TabIndex = 0
    Me.chk_show_not_blocked.Text = "chk show not blocked"
    Me.chk_show_not_blocked.UseVisualStyleBackColor = True
    '
    'chk_show_unsubscribed
    '
    Me.chk_show_unsubscribed.Location = New System.Drawing.Point(13, 86)
    Me.chk_show_unsubscribed.Name = "chk_show_unsubscribed"
    Me.chk_show_unsubscribed.Size = New System.Drawing.Size(154, 16)
    Me.chk_show_unsubscribed.TabIndex = 3
    Me.chk_show_unsubscribed.Text = "chk_show_unsubscribed"
    Me.chk_show_unsubscribed.UseVisualStyleBackColor = True
    '
    'ef_profile
    '
    Me.ef_profile.DoubleValue = 0
    Me.ef_profile.IntegerValue = 0
    Me.ef_profile.IsReadOnly = False
    Me.ef_profile.Location = New System.Drawing.Point(8, 15)
    Me.ef_profile.Name = "ef_profile"
    Me.ef_profile.OnlyUpperCase = True
    Me.ef_profile.Size = New System.Drawing.Size(428, 25)
    Me.ef_profile.SufixText = "Sufix Text"
    Me.ef_profile.SufixTextVisible = True
    Me.ef_profile.TabIndex = 1
    Me.ef_profile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_profile.TextValue = ""
    Me.ef_profile.TextWidth = 60
    Me.ef_profile.Value = ""
    Me.ef_profile.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_user
    '
    Me.ef_user.DoubleValue = 0
    Me.ef_user.IntegerValue = 0
    Me.ef_user.IsReadOnly = False
    Me.ef_user.Location = New System.Drawing.Point(8, 47)
    Me.ef_user.Name = "ef_user"
    Me.ef_user.OnlyUpperCase = True
    Me.ef_user.Size = New System.Drawing.Size(428, 25)
    Me.ef_user.SufixText = "Sufix Text"
    Me.ef_user.SufixTextVisible = True
    Me.ef_user.TabIndex = 2
    Me.ef_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_user.TextValue = ""
    Me.ef_user.TextWidth = 60
    Me.ef_user.Value = ""
    Me.ef_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_show_blocked)
    Me.gb_status.Controls.Add(Me.chk_show_not_blocked)
    Me.gb_status.Controls.Add(Me.chk_show_unsubscribed)
    Me.gb_status.Controls.Add(Me.chk_show_disabled)
    Me.gb_status.Location = New System.Drawing.Point(679, 10)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(228, 106)
    Me.gb_status.TabIndex = 4
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xEstado"
    '
    'chk_show_blocked
    '
    Me.chk_show_blocked.Location = New System.Drawing.Point(13, 42)
    Me.chk_show_blocked.Name = "chk_show_blocked"
    Me.chk_show_blocked.Size = New System.Drawing.Size(154, 16)
    Me.chk_show_blocked.TabIndex = 1
    Me.chk_show_blocked.Text = "chk_show_blocked"
    Me.chk_show_blocked.UseVisualStyleBackColor = True
    '
    'frm_user_profiles_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1158, 610)
    Me.Name = "frm_user_profiles_sel"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_results.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const LEN_PROFILE_NAME As Integer = 40
  Private Const LEN_USER_NAME As Integer = 15

  ' State
  Private Const USER_STATE_ENABLED As Integer = 1
  Private Const USER_STATE_DISABLED As Integer = 0

  ' Row type
  Private Const ROW_TYPE_PROFILE As Integer = 0
  Private Const ROW_TYPE_USER As Integer = 1

  Private Const SQL_COLUMN_ROW_TYPE As Integer = 0
  Private Const SQL_COLUMN_PROFILE_ID As Integer = 1
  Private Const SQL_COLUMN_PROFILE_NAME As Integer = 2
  Private Const SQL_COLUMN_USER_ID As Integer = 3
  Private Const SQL_COLUMN_USER_NAME As Integer = 4
  Private Const SQL_COLUMN_USER_FULL_NAME As Integer = 5
  Private Const SQL_COLUMN_USER_STATE As Integer = 6
  Private Const SQL_COLUMN_USER_LIMIT_SALES As Integer = 7
  Private Const SQL_COLUMN_USER_LIMIT_SALES_MB As Integer = 8
  Private Const SQL_COLUMN_PROFILE_MASTER As Integer = 9
  Private Const SQL_COLUMN_EMPLOYEE_CODE As Integer = 10

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ROW_TYPE As Integer = 1
  Private Const GRID_COLUMN_PROFILE_ID As Integer = 2
  Private Const GRID_COLUMN_PROFILE_NAME As Integer = 3
  Private Const GRID_COLUMN_PROFILE_MASTER As Integer = 4
  Private Const GRID_COLUMN_USER_ID As Integer = 5
  Private Const GRID_COLUMN_USER_NAME As Integer = 6
  Private Const GRID_COLUMN_USER_FULL_NAME As Integer = 7
  Private Const GRID_COLUMN_EMPLOYEE_CODE As Integer = 8
  Private Const GRID_COLUMN_USER_STATE As Integer = 9
  Private Const GRID_COLUMN_USER_LIMIT_SALES As Integer = 10
  Private Const GRID_COLUMN_USER_LIMIT_SALES_MB As Integer = 11


  Private Const GRID_COLUMNS As Integer = 12
  Private Const GRID_HEADER_ROWS As Integer = 1


  'IRP
  'Public Const INDEX_FORM_PROFILE_EDIT As Integer = 0
  'Public Const INDEX_FORM_USER_EDIT As Integer = 1

  ' Result type
  Public Enum ENUM_RESULT_TYPE
    RT_PROFILE = 0
    RT_USER = 1
  End Enum

  Private Const INVALID_PROFILE_ID As Integer = -1

  Private Const FORM_DB_MIN_VERSION As Short = 183
#End Region

#Region " Members "

  ' Input parameters
  Private ResultType As ENUM_RESULT_TYPE

  ' For return items selected in selection mode
  Private UsersSelected() As Integer

  ' For report filters 
  Private m_profile_name As String
  Private m_user_name As String
  Private m_results As String
  Private m_state As String
  Private m_master As Boolean

#End Region

#Region " Properties "

  Public Property SelOnlyMaster() As Boolean
    Get
      Return m_master
    End Get
    Set(ByVal Value As Boolean)
      m_master = Value
    End Set
  End Property

#End Region

#Region " Overridable GUI function "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_USER_PROFILE_SEL

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Configuration.GUI_Configuration.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------


  End Sub 'GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Buttons control. Replace New button by 2 custom buttons
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION Then
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
      If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
        GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
        GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Size = New System.Drawing.Size(90, 45)
      End If
    End If

    'Results
    If GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Or WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      gb_results.Height = 106
      chk_master_profile.Enabled = True
      chk_corporate_user.Enabled = True
      Me.Width = 1241

      'Call set form features resolutions when size's form exceeds desktop area
      Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)
    End If

    ' Fix and disable result type when in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      If ResultType = ENUM_RESULT_TYPE.RT_PROFILE Then
        If SelOnlyMaster Then
          chk_profile.Checked = False
          chk_profile.Enabled = False
        Else
          chk_profile.Checked = True
        End If
        chk_user.Enabled = False
        chk_corporate_user.Enabled = False
        chk_master_profile.Checked = True
      End If
      If ResultType = ENUM_RESULT_TYPE.RT_USER Then
        chk_user.Checked = True
        chk_corporate_user.Checked = True
        chk_profile.Enabled = False
        chk_master_profile.Enabled = False
      End If
    Else
      chk_user.Checked = True
    End If

    ' Assign NLS id's
    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(383)
    gb_results.Text = GLB_NLS_GUI_CONFIGURATION.GetString(379)
    chk_user.Text = GLB_NLS_GUI_CONFIGURATION.GetString(326)
    chk_corporate_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2359)
    chk_profile.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)
    chk_master_profile.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2087)
    gb_status.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259)
    chk_show_unsubscribed.Text = GLB_NLS_GUI_CONFIGURATION.GetString(88)
    chk_show_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(91)
    chk_show_blocked.Text = GLB_NLS_GUI_CONFIGURATION.GetString(92)
    chk_show_not_blocked.Text = GLB_NLS_GUI_CONFIGURATION.GetString(93)
    ef_profile.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)
    ef_user.Text = GLB_NLS_GUI_CONFIGURATION.GetString(326)

    chk_show_not_blocked.Checked = True
    chk_show_blocked.Checked = True
    chk_show_disabled.Checked = False
    chk_show_unsubscribed.Checked = False

    ' New User
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(3)
    ' New Profile
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(2)
    ' New Master User
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2366)

    ' Filters
    Call ef_profile.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROFILE_NAME)
    Call ef_user.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, LEN_USER_NAME)

    ' Colors
    Me.lbl_color_user.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    Me.lbl_color_corporate_user.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.lbl_color_profile.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)
    Me.lbl_color_master_profile.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)

    Call GUI_FilterReset()

    '  ef_user.Focus()

    Call GUI_StyleView()

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_GetSelectedItems()

    'XVV Variable not use im idx_row As Short
    Dim result_column As Integer
    Dim user_id As Integer

    Erase UsersSelected

    UsersSelected = Me.Grid.SelectedRows
    If IsNothing(UsersSelected) Then
      Exit Sub
    End If

    ' When in selection mode, only one type of result is selected
    ' We must get the ID of these type of data looking for the correct column.
    If ResultType = ENUM_RESULT_TYPE.RT_PROFILE Then
      result_column = GRID_COLUMN_PROFILE_ID
    Else
      result_column = GRID_COLUMN_USER_ID
    End If

    user_id = Me.Grid.Cell(UsersSelected(0), result_column).Value
    UsersSelected(0) = user_id

  End Sub 'GUI_GetSelectedItems

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Short
    Dim row_type As Integer
    Dim item_id As Integer
    Dim item_name As String
    Dim frm_user_edit As frm_user_edit
    Dim frm_profile_edit As frm_profile_edit

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the selected row's type
    row_type = Me.Grid.Cell(idx_row, GRID_COLUMN_ROW_TYPE).Value

    ' Get the user or profile ID and launch de adequated editor depending on row type
    If row_type = ROW_TYPE_PROFILE Then
      item_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_ID).Value
      item_name = Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_NAME).Value
      If Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_MASTER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2095) And _
         WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        frm_profile_edit = New frm_profile_edit(ENUM_FORM.FORM_MASTER_PROFILE_EDIT)
      Else
        frm_profile_edit = New frm_profile_edit
      End If
      Call frm_profile_edit.ShowEditItem(item_id, item_name)
      frm_profile_edit = Nothing
    Else
      item_id = Me.Grid.Cell(idx_row, GRID_COLUMN_USER_ID).Value
      item_name = Me.Grid.Cell(idx_row, GRID_COLUMN_USER_NAME).Value
      If Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_MASTER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2095) And _
         WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        frm_user_edit = New frm_user_edit(ENUM_FORM.FORM_MASTER_USER_EDIT)
      Else
        frm_user_edit = New frm_user_edit
      End If
      Call frm_user_edit.ShowEditItem(item_id, item_name)
      frm_user_edit = Nothing
    End If

    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Any data is ok in filter fields
    Return True
  End Function 'GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim user_filter_profiles As String
    Dim user_filter_users As String
    Dim user_query_profiles As String
    Dim user_query_users As String
    Dim user_query As String

    user_query = ""

    ' Query for profiles when profile is checked
    ' and when none is checked (If none is checked it acts like both was checked)
    If chk_profile.Checked Or chk_master_profile.Checked Or (Not chk_user.Checked And Not chk_corporate_user.Checked) Then
      user_query_profiles = GetUserSqlProfiles()
      user_filter_profiles = GetUserFilterProfiles()
      If InStr(user_query_profiles, "WHERE") = 0 Then
        user_query = user_query_profiles & " WHERE " & user_filter_profiles
      Else
        user_query = user_query_profiles & " AND " & user_filter_profiles
      End If

    End If

    ' If both are checked or if both are unchecked (If only one is checked, no union is required)
    If (Not chk_profile.Checked And Not chk_user.Checked And Not chk_corporate_user.Checked And Not chk_master_profile.Checked) Or _
       ((chk_user.Checked Or chk_corporate_user.Checked) And (chk_master_profile.Checked Or chk_profile.Checked)) Then
      user_query = user_query & " UNION "
    End If

    ' Query for users when user is checked
    ' and when none is checked (If none is checked it acts like both was checked)
    If (chk_user.Checked Or chk_corporate_user.Checked) Or (Not chk_profile.Checked And Not chk_master_profile.Checked) Then
      user_query_users = GetUserSqlUsers()
      user_filter_users = GetUserFilterUsers()
      user_query = user_query & user_query_users & " AND " & user_filter_users
    End If

    user_query = user_query & " ORDER BY GUP_PROFILE_ID " & _
                                       " , GU_USER_ID "

    Return user_query

  End Function 'GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_FilterReset()

    ef_profile.Value = ""
    ef_user.Value = ""

    ' Do not reset result type in edition Mode because it is looked
    If Not ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_profile.Checked = False
      chk_master_profile.Checked = False
      chk_user.Checked = True
      chk_corporate_user.Checked = chk_corporate_user.Enabled
      chk_show_unsubscribed.Checked = False
      chk_show_unsubscribed.Enabled = True
      chk_show_disabled.Checked = False
      chk_show_disabled.Enabled = True
      chk_show_not_blocked.Checked = True
      chk_show_not_blocked.Enabled = True
      chk_show_blocked.Checked = True
      chk_show_blocked.Enabled = True
    End If

  End Sub 'GUI_FilterReset

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim row_color As System.Drawing.Color
    Dim block_reason As Integer
    Dim string_block_reason As String

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' If row is a profile-type row, set row color for profile row type
    If DbRow.Value(SQL_COLUMN_ROW_TYPE) = ROW_TYPE_PROFILE Then
      If DbRow.IsNull(SQL_COLUMN_PROFILE_MASTER) Then
        row_color = lbl_color_profile.BackColor
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROFILE_MASTER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2094)
      Else
        row_color = lbl_color_master_profile.BackColor
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROFILE_MASTER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2095)
      End If
    End If

    ' If row is a user-type row
    If DbRow.Value(SQL_COLUMN_ROW_TYPE) = ROW_TYPE_USER Then

      ' State 
      ' XCD 16-Aug-2012 Blocked/Unblocked
      block_reason = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_USER_STATE))
      string_block_reason = AUDIT_NONE_STRING

      If (block_reason And GUI_USER_BLOCK_REASON.WRONG_PASSWORD).Equals(GUI_USER_BLOCK_REASON.WRONG_PASSWORD) Or _
         (block_reason And GUI_USER_BLOCK_REASON.INACTIVITY).Equals(GUI_USER_BLOCK_REASON.INACTIVITY) Then

        string_block_reason = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558)
      End If

      If (block_reason And GUI_USER_BLOCK_REASON.DISABLED).Equals(GUI_USER_BLOCK_REASON.DISABLED) Then
        string_block_reason = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1220)
      End If

      If (block_reason And GUI_USER_BLOCK_REASON.UNSUBSCRIBED).Equals(GUI_USER_BLOCK_REASON.UNSUBSCRIBED) Then
        string_block_reason = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1223)
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_STATE).Value = string_block_reason

      If DbRow.IsNull(SQL_COLUMN_PROFILE_MASTER) Then
        row_color = lbl_color_user.BackColor
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROFILE_MASTER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2094)
      Else
        row_color = lbl_color_corporate_user.BackColor
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROFILE_MASTER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2095)
      End If

    End If

    ' Set row color
    Me.Grid.Row(RowIndex).BackColor = row_color

    If (DbRow.IsNull(SQL_COLUMN_PROFILE_MASTER) Or Not WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      If Not (DbRow.IsNull(SQL_COLUMN_USER_LIMIT_SALES)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_LIMIT_SALES).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_USER_LIMIT_SALES), 2)
      End If
      If Not (DbRow.IsNull(SQL_COLUMN_USER_LIMIT_SALES_MB)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_LIMIT_SALES_MB).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_USER_LIMIT_SALES_MB), 2)
      End If
    End If


    If Not DbRow.IsNull(SQL_COLUMN_EMPLOYEE_CODE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EMPLOYEE_CODE).Value = DbRow.Value(SQL_COLUMN_EMPLOYEE_CODE)
    End If

    Return True

  End Function      ' GUI_SetupRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call EditNewUser()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call EditNewProfile()

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call EditNewMasterProfile()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_profile
  End Sub 'GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(319), m_profile_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(326), m_user_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(379), m_results)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(259), m_state)

  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_CONFIGURATION.GetString(467)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_profile_name = ""
    m_user_name = ""
    m_results = ""
    m_state = ""


    ' Profile name
    If Me.ef_profile.Value.Length > 0 Then
      m_profile_name = Me.ef_profile.Value
    End If

    ' User name
    If Me.ef_user.Value.Length > 0 Then
      m_user_name = Me.ef_user.Value
    End If

    ' Results
    If Me.chk_profile.Checked Then
      m_results = Me.chk_profile.Text
    End If

    If Me.chk_master_profile.Checked Then
      If m_results.Length > 0 Then
        m_results = m_results & ", " & Me.chk_master_profile.Text
      Else
        m_results = Me.chk_master_profile.Text
      End If
    End If

    If Me.chk_user.Checked Then
      If m_results.Length > 0 Then
        m_results = m_results & ", " & Me.chk_user.Text
      Else
        m_results = Me.chk_user.Text
      End If
    End If

    If Me.chk_corporate_user.Checked Then
      If m_results.Length > 0 Then
        m_results = m_results & ", " & Me.chk_corporate_user.Text
      Else
        m_results = Me.chk_corporate_user.Text
      End If
    End If

    ' State
    If Me.chk_show_not_blocked.Checked Then
      m_state = Me.chk_show_not_blocked.Text
    End If

    If Me.chk_show_unsubscribed.Checked And Me.chk_show_unsubscribed.Enabled Then
      If m_state.Length > 0 Then
        m_state = m_state & ", " & Me.chk_show_unsubscribed.Text
      Else
        m_state = Me.chk_show_unsubscribed.Text
      End If
    End If

    If Me.chk_show_disabled.Checked And Me.chk_show_disabled.Enabled Then
      If m_state.Length > 0 Then
        m_state = m_state & ", " & Me.chk_show_disabled.Text
      Else
        m_state = Me.chk_show_disabled.Text
      End If
    End If

    If Me.chk_show_blocked.Checked And Me.chk_show_blocked.Enabled Then
      If m_state.Length > 0 Then
        m_state = m_state & ", " & Me.chk_show_blocked.Text
      Else
        m_state = Me.chk_show_blocked.Text
      End If
    End If


  End Sub

#End Region ' GUI Reports

#End Region

#Region " Private Functions "

  ' PURPOSE: Open user edit form to create New User
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewUser()

    Dim idx_row As Short
    'XVV Variable not use Dim row_type As Integer
    Dim profile_id As Integer
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next
    'IRP
    'frm_edit = EditFormArray(INDEX_FORM_USER_EDIT)
    frm_edit = New frm_user_edit

    If idx_row = Me.Grid.NumRows Then
      ' If no row is selected create a new user without profile preselected
      Call frm_edit.ShowNewItem()
    Else
      ' If a row is selected create a new user with the same profile preselected
      profile_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_ID).Value
      Call frm_edit.ShowNewItem(profile_id)
    End If
    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'EditNewUser

  ' PURPOSE: Open profile edit form to create New Profile
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewProfile()

    Dim frm_edit As Object
    'IRP
    'frm_edit = EditFormArray(INDEX_FORM_PROFILE_EDIT)
    frm_edit = New frm_profile_edit

    Call frm_edit.ShowNewItem()
    frm_edit = Nothing
    Call Grid.Focus()

  End Sub 'EditNewProfile

  Private Sub EditNewMasterProfile()

    Dim frm_edit As frm_profile_edit

    frm_edit = New frm_profile_edit(ENUM_FORM.FORM_MASTER_PROFILE_EDIT)

    Call frm_edit.ShowNewMaster()
    frm_edit = Nothing
    Call Grid.Focus()

  End Sub 'EditNewMasterProfile

  ' PURPOSE: Create the filter to query for profiles
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with conditions based on dialog items state
  '
  Private Function GetUserFilterProfiles() As String

    Dim filter_string As String

    filter_string = ""
    filter_string = filter_string & "GUP_PROFILE_ID > 0"

    ' Restrict to profiles with names that match the pattern
    If ef_profile.Value <> "" Then
      filter_string = filter_string & " AND" & GUI_FilterField("GUP_NAME", ef_profile.Value, False, False, True)
    End If

    ' Restrict to profiles that have users with names matching the pattern 
    If ef_user.Value <> "" Then
      filter_string = filter_string & " AND GUP_PROFILE_ID IN ( SELECT GU_PROFILE_ID " & _
                                                               "FROM GUI_USERS " & _
                                                               "WHERE " & GUI_FilterField("GU_USERNAME", ef_user.Value, False, False, True) & " ) "
    End If

    'If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
    ' filter_string = filter_string & " AND GUP_MASTER_ID IS NULL "
    'Else
    If chk_master_profile.Checked And Not chk_profile.Checked Then
      filter_string = filter_string & " AND GUP_MASTER_ID IS NOT NULL "
    ElseIf chk_profile.Checked And Not chk_master_profile.Checked Then
      filter_string = filter_string & " AND GUP_MASTER_ID IS NULL "
    End If
    'End If

    Return filter_string

  End Function 'GetUserFilterProfiles

  ' PURPOSE: Create the filter to query for users
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with conditions based on dialog items state
  '
  Private Function GetUserFilterUsers() As String

    Dim filter_string As String

    filter_string = ""

    ' Restruct to users with id and profileId greater than zero
    filter_string = filter_string & " GU_PROFILE_ID > 0 " & _
                                    " AND GU_USER_ID > 0 "

    ' Restrict to users with names that match the pattern
    If ef_user.Value <> "" Then
      filter_string = filter_string & " AND" & GUI_FilterField("GU_USERNAME", ef_user.Value, False, False, True)
    End If

    ' Restrict to users with profiles that its name match the pattern
    If ef_profile.Value <> "" Then
      filter_string = filter_string & " AND" & GUI_FilterField("GUP_NAME", ef_profile.Value, False, False, True)
    End If

    ' XCD 29-AUG-2012 Restrict users by filter state
    If Not (chk_show_not_blocked.Checked = False And _
            chk_show_blocked.Checked = False And _
            chk_show_unsubscribed.Checked = False And _
            chk_show_disabled.Checked = False) Then

      filter_string = filter_string & GetUserFilterState()

    End If

    'If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
    'filter_string = filter_string & " AND GU_MASTER_ID IS NULL "
    'Else
    If chk_corporate_user.Checked And Not chk_user.Checked Then
      filter_string = filter_string & " AND GU_MASTER_ID IS NOT NULL "
    ElseIf chk_user.Checked And Not chk_corporate_user.Checked Then
      filter_string = filter_string & " AND GU_MASTER_ID IS NULL "
    End If
    'End If

    Return filter_string

  End Function 'GetUserFilterUsers

  ' PURPOSE: Create query statment without filter to get profiles
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with query statment
  '
  ' NOTES:
  '     - The number of fiels and its type must be equal
  '       than these on the the query for users because 
  '       we want make a UNION betwen these two queries
  '
  Private Function GetUserSqlProfiles() As String

    Dim str_sql As String
    'XVV Variable not use Dim profiles_allowed As String

    str_sql = "SELECT   0 as TIPO " & _
                    " , GUP_PROFILE_ID " & _
                    " , GUP_NAME " & _
                    " , -1 as GU_USER_ID " & _
                    " , NULL as GU_USERNAME " & _
                    " , NULL as GU_FULL_NAME " & _
                    " , NULL as GU_BLOCK_REASON " & _
                    " , NULL as GU_SALES_LIMIT " & _
                    " , NULL as GU_MB_SALES_LIMIT " & _
                    " , GUP_MASTER_ID " & _
                    " , NULL as GU_EMPLOYEE_CODE " & _
                "FROM   GUI_USER_PROFILES "

    Return str_sql

  End Function 'GetUserSqlProfiles

  ' PURPOSE: Create query statment without filter to get users
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with query statment
  '
  ' NOTES:
  '     - The number of fiels and its type must be equal
  '       than these on the the query for profiles because 
  '       we want make a UNION betwen these two queries
  '
  Private Function GetUserSqlUsers() As String

    Dim str_sql As String

    str_sql = "SELECT   1 as TIPO " & _
                    " , GUP_PROFILE_ID " & _
                    " , GUP_NAME " & _
                    " , GU_USER_ID " & _
                    " , GU_USERNAME " & _
                    " , GU_FULL_NAME " & _
                    " , GU_BLOCK_REASON " & _
                    " , GU_SALES_LIMIT " & _
                    " , GU_MB_SALES_LIMIT " & _
                    " , GU_MASTER_ID " & _
                    " , GU_EMPLOYEE_CODE " & _
                "FROM   GUI_USER_PROFILES " & _
                    " , GUI_USERS " & _
               "WHERE   GUP_PROFILE_ID = GU_PROFILE_ID "

    ' Super User can view all users, exept himself
    str_sql = str_sql & " AND GU_USER_ID <> " & CurrentUser.Id & " "

    Return str_sql

  End Function 'GetUserSqlUsers

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)
    Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    ' ROW_TYPE
    Me.Grid.Column(GRID_COLUMN_ROW_TYPE).Width = 0
    Me.Grid.Column(GRID_COLUMN_ROW_TYPE).Mapping = SQL_COLUMN_ROW_TYPE

    ' GUP_INDEX
    Me.Grid.Column(GRID_COLUMN_INDEX).Header.Text = " "
    Me.Grid.Column(GRID_COLUMN_INDEX).Width = 200
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    ' GUP_PROFILE_ID
    Me.Grid.Column(GRID_COLUMN_PROFILE_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_PROFILE_ID).Mapping = SQL_COLUMN_PROFILE_ID

    ' GUP_NAME
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Width = 2650
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Mapping = SQL_COLUMN_PROFILE_NAME
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' GU_MASTER
    Me.Grid.Column(GRID_COLUMN_PROFILE_MASTER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2093)
    If GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Or WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      Me.Grid.Column(GRID_COLUMN_PROFILE_MASTER).Width = 1000
    Else
      Me.Grid.Column(GRID_COLUMN_PROFILE_MASTER).Width = 0
    End If
    Me.Grid.Column(GRID_COLUMN_PROFILE_MASTER).Mapping = SQL_COLUMN_PROFILE_MASTER
    Me.Grid.Column(GRID_COLUMN_PROFILE_MASTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    Me.Grid.Column(GRID_COLUMN_PROFILE_MASTER).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' GU_USER_ID
    Me.Grid.Column(GRID_COLUMN_USER_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_USER_ID).Mapping = SQL_COLUMN_USER_ID

    ' GU_USERNAME
    Me.Grid.Column(GRID_COLUMN_USER_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(326)
    Me.Grid.Column(GRID_COLUMN_USER_NAME).Width = 1700
    Me.Grid.Column(GRID_COLUMN_USER_NAME).Mapping = SQL_COLUMN_USER_NAME
    Me.Grid.Column(GRID_COLUMN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_USER_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' GU_FULL_NAME
    Me.Grid.Column(GRID_COLUMN_USER_FULL_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(327)
    Me.Grid.Column(GRID_COLUMN_USER_FULL_NAME).Width = 2650
    Me.Grid.Column(GRID_COLUMN_USER_FULL_NAME).Mapping = SQL_COLUMN_USER_FULL_NAME
    Me.Grid.Column(GRID_COLUMN_USER_FULL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_USER_FULL_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    If (GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      Me.Grid.Column(GRID_COLUMN_USER_FULL_NAME).Width = 2650 + 1400 + 1600
    End If


    'GUP_EMPLOYEE_CODE
    Me.Grid.Column(GRID_COLUMN_EMPLOYEE_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4704)
    Me.Grid.Column(GRID_COLUMN_EMPLOYEE_CODE).Width = 2650
    Me.Grid.Column(GRID_COLUMN_EMPLOYEE_CODE).Mapping = SQL_COLUMN_EMPLOYEE_CODE
    Me.Grid.Column(GRID_COLUMN_EMPLOYEE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    ' GU_BLOCK_REASON
    'Me.Grid.Column(GRID_COLUMN_USER_STATE).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(311)
    Me.Grid.Column(GRID_COLUMN_USER_STATE).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(259)
    Me.Grid.Column(GRID_COLUMN_USER_STATE).Width = 1700
    Me.Grid.Column(GRID_COLUMN_USER_STATE).Mapping = SQL_COLUMN_USER_STATE
    Me.Grid.Column(GRID_COLUMN_USER_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    ' GU_SALES_LIMIT
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1176)
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES).Width = 1400
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    If (GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES).Header.Text = String.Empty
      Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES).Width = 0
    End If

    ' GU_SALES_LIMIT_MB
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES_MB).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1178)
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES_MB).Width = 1600
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES_MB).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES_MB).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    If (GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency) Then
      Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES_MB).Header.Text = String.Empty
      Me.Grid.Column(GRID_COLUMN_USER_LIMIT_SALES_MB).Width = 0
    End If

  End Sub 'GUI_StyleView

  Private Function GetUserFilterState()
    Dim str_sql As String
    Dim block_reasons As Integer

    block_reasons = WSI.Common.GUI_USER_BLOCK_REASON.NONE
    str_sql = ""

    ' Blocked
    If chk_show_blocked.Checked Then
      block_reasons += WSI.Common.GUI_USER_BLOCK_REASON.WRONG_PASSWORD + WSI.Common.GUI_USER_BLOCK_REASON.INACTIVITY
    End If

    ' Unsubscribed
    If chk_show_unsubscribed.Checked Then
      block_reasons += WSI.Common.GUI_USER_BLOCK_REASON.UNSUBSCRIBED
    End If

    ' Disabled
    If chk_show_disabled.Checked Then
      block_reasons += WSI.Common.GUI_USER_BLOCK_REASON.DISABLED
    End If

    If block_reasons = WSI.Common.GUI_USER_BLOCK_REASON.NONE Then
      ' Only not blocked users
      If chk_show_not_blocked.Checked Then
        str_sql = " AND GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE
      End If
    Else
      If chk_show_not_blocked.Checked Then
        ' Not blocked users and others
        str_sql = " AND (( GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ) OR "
      Else
        ' Users with block reason <> NONE
        str_sql = " AND ("
      End If

      '
      ' BLOCK STATE PRIORITY
      '
      ' First block state discard the others:
      '   1 - UNSUBSCRIBED
      '   2 - DISABLED
      '   3 - BLOCKED ( WRONG_PASSWORD OR INACTIVITY )
      '   4 - NOT BLOCKED
      '
      str_sql = str_sql & _
      " GU_BLOCK_REASON & (" & _
                          " SELECT " & _
                          "   CASE WHEN GU_BLOCK_REASON & " & GUI_USER_BLOCK_REASON.UNSUBSCRIBED & " = " & GUI_USER_BLOCK_REASON.UNSUBSCRIBED & " THEN " & GUI_USER_BLOCK_REASON.UNSUBSCRIBED & _
                          "        WHEN GU_BLOCK_REASON & " & GUI_USER_BLOCK_REASON.DISABLED & " = " & GUI_USER_BLOCK_REASON.DISABLED & " THEN " & GUI_USER_BLOCK_REASON.DISABLED & _
                          "        WHEN GU_BLOCK_REASON <> " & GUI_USER_BLOCK_REASON.NONE & " THEN " & GUI_USER_BLOCK_REASON.WRONG_PASSWORD + GUI_USER_BLOCK_REASON.INACTIVITY & _
                          "        ELSE " & GUI_USER_BLOCK_REASON.NONE & _
                          "   END " & _
                        " ) & " & block_reasons & " > " & GUI_USER_BLOCK_REASON.NONE & " ) "


    End If


    Return str_sql

  End Function

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for select mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelResType: Fix filter to this type of results
  '           - SelOnlyMaster: Fix filter to only master
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - List of ProfileId or UserId selected
  Public Function ShowForSelect(ByVal SelResType As ENUM_RESULT_TYPE, ByVal OnlyMaster As Boolean) As Integer()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    ResultType = SelResType
    Me.SelOnlyMaster = OnlyMaster

    Me.Display(True)

    Return UsersSelected

  End Function

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub

#End Region

#Region " Events "

  Private Sub chk_profile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_profile.Click

    ' User can't change this check in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_profile.Checked = Not chk_profile.Checked
    End If

  End Sub

  Private Sub chk_master_profile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_master_profile.Click

    ' User can't change this check in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_master_profile.Checked = Not chk_master_profile.Checked
    End If

  End Sub

  Private Sub chk_user_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_user.Click

    ' User can't change this check in selection mode
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION Then
      chk_user.Checked = Not chk_user.Checked
      chk_show_unsubscribed.Enabled = chk_user.Checked
      chk_show_not_blocked.Enabled = chk_user.Checked
      chk_show_disabled.Enabled = chk_user.Checked
    End If

  End Sub

  Private Sub chk_user_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_user.CheckedChanged

    If chk_user.Checked Or chk_corporate_user.Checked Then
      chk_show_unsubscribed.Enabled = True
      chk_show_not_blocked.Enabled = True
      chk_show_disabled.Enabled = True
      chk_show_blocked.Enabled = True
    ElseIf Not chk_profile.Checked Then
      chk_show_unsubscribed.Enabled = True
      chk_show_not_blocked.Enabled = True
      chk_show_disabled.Enabled = True
      chk_show_blocked.Enabled = True
    Else
      chk_show_unsubscribed.Enabled = False
      chk_show_not_blocked.Enabled = False
      chk_show_disabled.Enabled = False
      chk_show_blocked.Enabled = False
    End If

  End Sub

  Private Sub chk_corporate_user_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_corporate_user.CheckedChanged

    If chk_user.Checked Or chk_corporate_user.Checked Then
      chk_show_unsubscribed.Enabled = True
      chk_show_not_blocked.Enabled = True
      chk_show_disabled.Enabled = True
      chk_show_blocked.Enabled = True
    ElseIf Not chk_profile.Checked Then
      chk_show_unsubscribed.Enabled = True
      chk_show_not_blocked.Enabled = True
      chk_show_disabled.Enabled = True
      chk_show_blocked.Enabled = True
    Else
      chk_show_unsubscribed.Enabled = False
      chk_show_not_blocked.Enabled = False
      chk_show_disabled.Enabled = False
      chk_show_blocked.Enabled = False
    End If

  End Sub

  Private Sub chk_profile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_profile.CheckedChanged

    If (chk_master_profile.Checked Or chk_profile.Checked) And Not (chk_user.Checked Or chk_corporate_user.Checked) Then
      chk_show_unsubscribed.Enabled = False
      chk_show_not_blocked.Enabled = False
      chk_show_disabled.Enabled = False
      chk_show_blocked.Enabled = False
    Else
      chk_show_unsubscribed.Enabled = True
      chk_show_not_blocked.Enabled = True
      chk_show_disabled.Enabled = True
      chk_show_blocked.Enabled = True
    End If

  End Sub

  Private Sub chk_master_profile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_master_profile.CheckedChanged

    If (chk_master_profile.Checked Or chk_profile.Checked) And Not (chk_user.Checked Or chk_corporate_user.Checked) Then
      chk_show_unsubscribed.Enabled = False
      chk_show_not_blocked.Enabled = False
      chk_show_disabled.Enabled = False
      chk_show_blocked.Enabled = False
    Else
      chk_show_unsubscribed.Enabled = True
      chk_show_not_blocked.Enabled = True
      chk_show_disabled.Enabled = True
      chk_show_blocked.Enabled = True
    End If

  End Sub

#End Region


End Class
