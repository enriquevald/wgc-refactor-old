'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_automatic_promotions_reports
' DESCRIPTION:   Automatic promotions reports 
' AUTHOR:        Daniel Dom�nguez
' CREATION DATE: 24-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-MAY-2012  DDM    Initial version
' 08-JUN-2012  JCM    Modified filter for 'promotion date' instead of 'promo award'
' 03-JUL-2012  MPO    Force to use index in clause FROM
' 03-JUL-2012  JAB    Fixed error to access dictionary elements.
' 12-JUL-2012  DDM    Added filter 'only not played'
' 27-JUL-2012  JCM    Added suport to credit type 'points' from automatic promotions
' 01-AUG-2012  DDM    Fixed Bug #326: Was not necessary to check the checkbox 
'                     "holder data" in GUI_ReportParams.
' 07-AUG-2012  RCI    Fixed Bug #329: Played and won in promotions is not shown.
' 14-AUG-2012  DDM    Adapted report for preassigned promotions.
'                     Modified Permissions.     
' 17-AUG-2012  DDM    Modified the filter from name of the promotion.
' 21-AUG-2012  DDM    Added the filter preassigned in promotion type and counters subtotals.
'                     Modified  audit, now show more detail
' 22-AUG-2012  DDM    Deleted filter: 'preassigned status'                     
' 24-AUG-2012  MPO    The order of agrupation is ACP_PROMO_NAME, ACP_PROMO_ID, previously ACP_PROMO_NAME
' 19-SEP-2012  DDM    Added the cost of promotion
' 21-SEP-2012  XIT    Added Categories
' 25-SEP-2012  DDM    Added Filter: Only promotions with cost
' 07-NOV-2012  DDM    Modified Query:  No included the Cover cupon and Prize Cupon in the filter from 'all promotions'.
' 04-FEB-2013  ICS    Fixed Bug #561: Exception in GetDataTableFromSelectedRows when there is no item selected.
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 11-JUN-2013  RBG    Fixed Bug #834: Force format in excel columns
' 28-AUG-2013  DHA    Modified automatic promotions filter for complementary cash desk draw
' 05-SEP-2013  CCG    Added account massive search
' 12-SEP-2013  DHA    Added Cash Desk Draw promotions
' 26-SEP-2013  CCG    Added Multitype massive search
' 16-DEC-2013  RMS    In TITO Mode virtual account id should be hidden
' 13-FEB-2014  JBC    Fixed bug WIGOSTITO-1064: Hidden buttons on tito mode and changed query.
' 24-FEB-2014  JFC    Fixed bug WIG-162: Assingned personalized excel width to "points" and "cash-in" columns.
' 27-MAR-2014  LEM    Added m_only_user_accounts to indicate if virtual accounts are hidden    
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 27-NOV-2014  DRV    Fixed Bug WIG-1771: label 'TOTAL' in the wrong place
' 19-FEB-2015  YNM    Fixed Bug WIG-2085: Add Exausted and Redeemed as options on the StatusPromotion Filter
' 21-AGO-2015  FJC    Product BackLog Item: 3702
' 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
' 24-FEB-2016  RAB    Product Backlog Item 8257:Multiple buckets: Canje de buckets NR
'-------------------------------------------------------------------
Imports GUI_Controls
Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports WSI.Common
Imports GUI_CommonMisc

Public Class frm_automatic_promotions_reports
  Inherits GUI_Controls.frm_base_sel

#Region " Enums "
  Private Enum ENUM_PROMO_STATUS_ACTION
    TO_APPLY = 0
    TO_CANCEL = 1
  End Enum
#End Region ' " Enums "

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_LEVEL As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 1
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 2
  Private Const SQL_COLUMN_PROMOTION_DATE As Integer = 3
  Private Const SQL_COLUMN_PROMOTION_NAME As Integer = 4
  Private Const SQL_COLUMN_INIT_BALANCE As Integer = 5
  Private Const SQL_COLUMN_PLAYED As Integer = 6
  Private Const SQL_COLUMN_BALANCE As Integer = 7
  Private Const SQL_COLUMN_STATUS As Integer = 8
  Private Const SQL_COLUMN_CREDIT_TYPE As Integer = 9
  Private Const SQL_COLUMN_CASH_IN As Integer = 10
  Private Const SQL_COLUMN_PROMO_ID As Integer = 11
  Private Const SQL_COLUMN_ACP_POINT As Integer = 12
  Private Const SQL_COLUMN_WON As Integer = 13
  Private Const SQL_COLUMN_DETAILS As Integer = 14
  Private Const SQL_COLUMN_PROMOTION_CREATED As Integer = 15
  Private Const SQL_COLUMN_UNIQUE_ID As Integer = 16
  Private Const SQL_COLUMN_COST As Integer = 17
  Private Const SQL_COLUMN_PROMO_CATEGORY As Integer = 18

  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 19

  Private Const SQL_INIT_COLUMNS_HOLDER_DATA As Integer = 20

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COLUMN_LEVEL As Integer = 1
  Private GRID_COLUMN_ACCOUNT_ID As Integer = 2
  Private GRID_COLUMN_ACCOUNT_NAME As Integer = 3
  Private GRID_COLUMN_PROMOTION_NAME As Integer = 4
  Private GRID_COLUMN_PROMO_CATEGORY As Integer = 5
  Private Const GRID_COLUMN_PROMOTION_CREATED As Integer = 6
  Private Const GRID_COLUMN_STATUS_NAME As Integer = 7
  Private Const GRID_COLUMN_PROMO_POINTS As Integer = 8
  Private Const GRID_COLUMN_PROMO_REDIM As Integer = 9
  Private Const GRID_COLUMN_PROMO_NON_REDIM As Integer = 10
  Private Const GRID_COLUMN_PLAYED As Integer = 11
  Private Const GRID_COLUMN_WON As Integer = 12
  Private Const GRID_COLUMN_BALANCE As Integer = 13
  Private Const GRID_COLUMN_COST As Integer = 14
  Private Const GRID_COLUMN_DETAILS As Integer = 15
  Private Const GRID_COLUMN_POINTS As Integer = 16
  Private Const GRID_COLUMN_CASH_IN As Integer = 17
  Private Const GRID_COLUMN_UNIQUE As Integer = 18
  Private Const GRID_COLUMN_STATUS_CODE As Integer = 19
  Private Const GRID_COLUMN_CREDIT_TYPE As Integer = 20


  Private Const GRID_COLUMNS As Integer = 21
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const LINE_TYPE_SUBTOTAL_OR_TOTAL As Integer = 0

  ' Text format columns
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_1 As Integer = 22
  Private Const EXCEL_COLUMN_TELEPHONE_NUMBER_2 As Integer = 23
  Private Const EXCEL_COLUMN_POSTAL_CODE As Integer = 28

  Private Const FORM_DB_MIN_VERSION As Short = 160

  'Width
  Private Const GRID_WIDTH_EXCEL_POINTS As Decimal = 9
  Private Const GRID_WIDTH_EXCEL_CASH_IN As Decimal = 9

#End Region ' Constants

#Region " MEMBERS "

  Private m_level_names As Dictionary(Of Integer, String)
  Private m_account_promo_status As Dictionary(Of Integer, String)
  ' filter
  Private m_init_date_from As String
  Private m_init_date_to As String
  Private m_order As String
  Private m_promotion_type As String
  Private m_promotion_status As String
  Private m_promo_name As String
  Private m_promo_type_credit As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_category As String
  Private m_only_user_accounts As Boolean

  ' Subtotal 1
  Private m_subtotal1_cash_in As Decimal
  Private m_subtotal1_promo_points As Decimal
  Private m_subtotal1_redim As Decimal
  Private m_subtotal1_noredim As Decimal
  Private m_subtotal1_played As Decimal
  Private m_subtotal1_balance As Decimal
  Private m_subtotal1_won As Decimal
  Private m_subtotal1_points As Decimal
  Private m_subtotal1_cost As Decimal

  ' Subtotal 2
  Private m_subtotal2_cash_in As Decimal
  Private m_subtotal2_promo_points As Decimal
  Private m_subtotal2_redim As Decimal
  Private m_subtotal2_noredim As Decimal
  Private m_subtotal2_played As Decimal
  Private m_subtotal2_balance As Decimal
  Private m_subtotal2_won As Decimal
  Private m_subtotal2_points As Decimal
  Private m_subtotal2_cost As Decimal

  ' Total 
  Private m_total_cash_in As Decimal
  Private m_total_promo_points As Decimal
  Private m_total_redim As Decimal
  Private m_total_noredim As Decimal
  Private m_total_played As Decimal
  Private m_total_balance As Decimal
  Private m_total_won As Decimal
  Private m_total_points As Decimal
  Private m_total_cost As Decimal

  ' save last value the subtotal.(Level id or Promotion id)
  Private m_last_subtotal1 As Integer
  ' save last value the subtotal.(Account id or Level)
  Private m_last_subtotal2 As Integer

  Private m_sql_column_subtotal2 As Integer
  Private m_sql_column_subtotal1 As Integer

  Private m_num_rows_detail As Integer
  Private m_num_rows_to_apply As Integer
  Private m_num_rows_to_cancel As Integer

  Private m_num_rows_subtotal1 As Integer
  Private m_num_rows_subtotal2 As Integer

  Private m_holder_data As Boolean

  Private m_is_tito_mode As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTION_REPORTS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode()

    If m_is_tito_mode Then
      uc_account_sel.InitExtendedQuery(True)
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(808)

    ' Grid
    'Me.Grid.PanelRightVisible = False

    Me.m_holder_data = True

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    If Not m_is_tito_mode Then
      ' Buttons custom
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1094) ' Apply promotion
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Size = New System.Drawing.Size(90, 45)

      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1104) ' Cancel promotion
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Size = New System.Drawing.Size(90, 45)
    Else
      ' Buttons custom
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    End If

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Promo type
    Me.gb_promo_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(779)
    Me.opt_promo_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287) & "*"
    Me.opt_promo_manual.Text = GLB_NLS_GUI_INVOICING.GetString(330)
    Me.opt_promo_system_period.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(780)
    Me.opt_promo_system_auto.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(809)
    Me.opt_promo_per_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(810)
    Me.opt_promo_preassigned.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1231)
    Me.lbl_msg_no_includes_auto.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1550)

    Call GUI_StyleSheet()

    ' Order by
    Me.gb_order.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(781)
    Me.opt_order_level_account_prom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(782)
    Me.opt_order_prom_level_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(783)

    ' Status
    Me.gb_status_promo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(811)
    Me.opt_promo_status_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(788)
    Me.opt_promo_status_awarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(812)
    Me.opt_promo_status_not_awarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(813)
    Me.opt_promo_status_canceled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1076)
    Me.opt_promo_status_expired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(790)
    Me.opt_promo_status_exausted.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(789)
    Me.opt_promo_status_redeemed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(791)
    Me.chk_promo_status_not_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(784)

    ' Cost of promotion
    Me.chk_promo_with_cost.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1357)

    ' View data to the account
    Me.chk_view_data_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(552)

    ' Promo filter
    Me.ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(256)
    Me.ef_promo_name.TextVisible = True
    Me.ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Categories filter
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)  ' Category
    Me.cmb_categories.Add(-1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337))
    Me.cmb_categories.Add(PromotionCategories.Categories())

    ' Set filter default values
    Call SetDefaultValues()

    ' Get Levels name for the Dictionary
    m_level_names = mdl_account_for_report.GetLevelNames()

    m_account_promo_status = New Dictionary(Of Integer, String)
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.UNKNOWN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(786))     ' UNKNOWN
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.AWARDED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(787))     ' AWARDED
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.ACTIVE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(788))      ' ACTIVE 
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.EXHAUSTED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(789))   ' EXHAUSTED 
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.EXPIRED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(790))     ' EXPIRED
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.REDEEMED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(791))    ' REDEEMED
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.NOT_AWARDED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(813)) ' NOT_AWARDED
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.CANCELLED_NOT_ADDED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1075)) ' CANCELLED_NOT_ADDED
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.CANCELLED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1076))           ' CANCELLED
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1077)) ' CANCELLED_BY_PLAYER
    m_account_promo_status.Add(ACCOUNT_PROMO_STATUS.PREASSIGNED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(813)) ' PREASSIGNED ' shows the status as NOT_AWARDED!

    Me.gb_promo_credit_type.Text = GLB_NLS_GUI_INVOICING.GetString(336)
    Me.chk_credit_type_points.Text = GLB_NLS_GUI_INVOICING.GetString(343)
    Me.chk_credit_type_redeem.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    Me.chk_credit_type_non_redeem.Text = GLB_NLS_GUI_INVOICING.GetString(338)

    ' 05-SEP-2013 CCG: Activate account massive search
    Me.uc_account_sel.ShowMassiveSearch = True

  End Sub ' GUI_InitControls

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AndPerm TYPE_PERMISSIONS
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    ' GroupBox Permissions
    Me.chk_view_data_account.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_SHOW_HOLDER_DATA).Read


  End Sub ' GUI_Permissions

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder


    _str_sql = New System.Text.StringBuilder()

    If Not String.IsNullOrEmpty(Me.uc_account_sel.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel.CreateAndInsertAccountData())
    End If

    _str_sql.AppendLine(" SELECT 	 ACP_ACCOUNT_LEVEL ") '0
    _str_sql.AppendLine("        , AC_HOLDER_NAME    ") '1
    _str_sql.AppendLine("        , ACP_ACCOUNT_ID    ") '2
    _str_sql.AppendLine("        , ACP_PROMO_DATE    ") '3   
    _str_sql.AppendLine("        , ACP_PROMO_NAME    ") '4
    _str_sql.AppendLine("        , ACP_INI_BALANCE   ") '5
    _str_sql.AppendLine("        , ACP_PLAYED        ") '6
    _str_sql.AppendLine("        , ACP_BALANCE       ") '7
    _str_sql.AppendLine("        , ACP_STATUS        ") '8
    _str_sql.AppendLine("        , ACP_CREDIT_TYPE   ") '9
    _str_sql.AppendLine("        , ACP_CASH_IN       ") '10
    _str_sql.AppendLine("        , ACP_PROMO_ID      ") '11
    _str_sql.AppendLine("        , ACP_POINTS        ") '12
    _str_sql.AppendLine("        , ACP_WON           ") '13
    _str_sql.AppendLine("        , ACP_DETAILS       ") '14
    _str_sql.AppendLine("        , ACP_CREATED       ") '15
    _str_sql.AppendLine("        , ACP_UNIQUE_ID     ") '16
    _str_sql.AppendLine("        , ACP_REDEEMABLE_COST ") '17
    _str_sql.AppendLine("        , ACP_PROMO_CATEGORY_ID ") '18
    _str_sql.AppendLine("        , AC_TYPE ") ' 19

    'Data to the account
    _str_sql.AppendLine(mdl_account_for_report.AccountFieldsSql())


    Dim _str_sql_with As String

    _str_sql_with = ""

    ' Filter dates
    _str_sql_with = Me.uc_dsl.GetSqlFilterCondition("ACP_CREATED")
    If _str_sql_with.Length > 0 Then
      _str_sql.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_created_status))")
    Else
      _str_sql.AppendLine("  FROM   ACCOUNT_PROMOTIONS ")
    End If

    _str_sql.AppendLine(" INNER   JOIN ACCOUNTS ON ACP_ACCOUNT_ID = AC_ACCOUNT_ID ")

    If Not String.IsNullOrEmpty(Me.uc_account_sel.MassiveSearchNumbers) Then
      If Me.uc_account_sel.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_sql.AppendLine(Me.uc_account_sel.GetInnerJoinAccountMassiveSearch("ACP_ACCOUNT_ID"))
      Else
        _str_sql.AppendLine(Me.uc_account_sel.GetInnerJoinAccountMassiveSearch("AC_TRACK_DATA"))
      End If
    End If

    _str_sql.AppendLine(GetSqlWhere())

    If opt_order_level_account_prom.Checked Then
      _str_sql.AppendLine(" ORDER   BY  ACP_ACCOUNT_LEVEL, ACP_ACCOUNT_ID, ACP_PROMO_NAME, ACP_PROMO_ID, ACP_CREATED DESC ")
    ElseIf opt_order_prom_level_account.Checked Then
      _str_sql.AppendLine(" ORDER   BY  ACP_PROMO_NAME, ACP_PROMO_ID, ACP_ACCOUNT_LEVEL, ACP_ACCOUNT_ID, ACP_CREATED DESC ")
    End If

    If Not String.IsNullOrEmpty(Me.uc_account_sel.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel.DropTableAccountMassiveSearch())
    End If

    Return _str_sql.ToString()
  End Function ' GUI_FilterGetSqlQuery  

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call GUI_StyleSheet()

    Call InitSubtotalValues(2)

    m_total_cash_in = 0
    m_total_promo_points = 0
    m_total_redim = 0
    m_total_noredim = 0
    m_total_played = 0
    m_total_balance = 0
    m_total_won = 0
    m_total_points = 0
    m_total_cost = 0

    m_num_rows_to_apply = 0
    m_num_rows_to_cancel = 0

    m_last_subtotal2 = -1
    m_last_subtotal1 = -1

    m_num_rows_subtotal1 = 0
    m_num_rows_subtotal2 = 0
    m_num_rows_detail = 0

    If Me.opt_order_level_account_prom.Checked Then
      m_sql_column_subtotal2 = SQL_COLUMN_ACCOUNT_ID
      m_sql_column_subtotal1 = SQL_COLUMN_LEVEL
    ElseIf Me.opt_order_prom_level_account.Checked Then
      m_sql_column_subtotal2 = SQL_COLUMN_LEVEL
      m_sql_column_subtotal1 = SQL_COLUMN_PROMO_ID
    End If

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    _row_index = Me.Grid.NumRows

    If _row_index > 0 Then
      Me.Grid.AddRow()
      ' Subtotal 1
      AddSubtotalRow(_row_index, 1)
      Me.Grid.AddRow()
      _row_index = _row_index + 1
      ' Subtotal 2
      AddSubtotalRow(_row_index, 2)
    End If

    ' Total
    _row_index = Me.Grid.AddRow()

    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMOTION_CREATED).Value = GLB_NLS_GUI_STATISTICS.GetString(203)
    Me.Grid.Cell(_row_index, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(m_total_cash_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(m_total_promo_points, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(m_total_redim, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(m_total_noredim, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(m_total_played, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(m_total_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_WON).Value = GUI_FormatCurrency(m_total_won, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(m_total_points, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_COST).Value = GUI_FormatCurrency(m_total_cost, 2)
    Me.Grid.Cell(_row_index, GRID_COLUMN_UNIQUE).Value = LINE_TYPE_SUBTOTAL_OR_TOTAL

    ' Style
    Me.Grid.Row(_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    'BUTTON_CUSTOM_0 - Apply
    'BUTTON_CUSTOM_1 - Canceled
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = m_num_rows_to_apply > 0 AndAlso Me.Permissions.Write
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_num_rows_to_cancel > 0 AndAlso Me.Permissions.Delete

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _promo_action As ENUM_PROMO_STATUS_ACTION

    ' count rows the detail
    m_num_rows_detail = m_num_rows_detail + 1

    If m_last_subtotal2 <> DbRow.Value(m_sql_column_subtotal2) AndAlso m_last_subtotal2 <> -1 Or _
        m_last_subtotal1 <> DbRow.Value(m_sql_column_subtotal1) AndAlso m_last_subtotal1 <> -1 Then
      AddSubtotalRow(RowIndex, 1)
      Me.Grid.AddRow()
      RowIndex = RowIndex + 1
    End If

    If m_last_subtotal1 <> DbRow.Value(m_sql_column_subtotal1) AndAlso m_last_subtotal1 <> -1 Then
      AddSubtotalRow(RowIndex, 2)
      Me.Grid.AddRow()
      RowIndex = RowIndex + 1
    End If

    m_num_rows_subtotal1 = m_num_rows_subtotal1 + 1
    m_num_rows_subtotal2 = m_num_rows_subtotal2 + 1

    m_last_subtotal2 = DbRow.Value(m_sql_column_subtotal2)
    m_last_subtotal1 = DbRow.Value(m_sql_column_subtotal1)

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' JAB 03-JUL-2012: Fixed error to access dictionary elements.
    ' Level
    If m_level_names.ContainsKey(DbRow.Value(SQL_COLUMN_LEVEL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = m_level_names(DbRow.Value(SQL_COLUMN_LEVEL))
    End If

    ' Account name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME), "", DbRow.Value(SQL_COLUMN_ACCOUNT_NAME))

    ' Accound id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CREATED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_PROMOTION_CREATED), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMM)
    ' Promotion name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = DbRow.Value(SQL_COLUMN_PROMOTION_NAME)
    If Me.opt_promo_system_period.Checked Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = DbRow.Value(SQL_COLUMN_PROMOTION_NAME) & " " & _
                                                                 Strings.Format(DbRow.Value(SQL_COLUMN_PROMOTION_DATE), "dd/MMM")
    End If

    ' Cash in
    If Not DbRow.IsNull(SQL_COLUMN_CASH_IN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_IN), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_subtotal1_cash_in = m_subtotal1_cash_in + DbRow.Value(SQL_COLUMN_CASH_IN)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = ""
    End If

    ' Init. Balance. is Redimible or non redimible.
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = ""

    ' Credit type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_TYPE).Value = DbRow.Value(SQL_COLUMN_CREDIT_TYPE)

    Select Case DbRow.Value(SQL_COLUMN_CREDIT_TYPE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.NR1, ACCOUNT_PROMO_CREDIT_TYPE.NR2
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        m_subtotal1_noredim = m_subtotal1_noredim + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        m_subtotal1_redim = m_subtotal1_redim + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.POINT
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
        m_subtotal1_promo_points = m_subtotal1_promo_points + DbRow.Value(SQL_COLUMN_INIT_BALANCE)
    End Select

    ' Played
    If DbRow.Value(SQL_COLUMN_PLAYED) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED), 2)
      m_subtotal1_played = m_subtotal1_played + DbRow.Value(SQL_COLUMN_PLAYED)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = ""
    End If

    ' Balance.
    If Me.opt_promo_status_not_awarded.Checked Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = ""
    Else
      If DbRow.Value(SQL_COLUMN_CREDIT_TYPE) <> ACCOUNT_PROMO_CREDIT_TYPE.POINT Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BALANCE), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        m_subtotal1_balance = m_subtotal1_balance + DbRow.Value(SQL_COLUMN_BALANCE)
      End If
    End If

    ' Cost promotion
    If Not DbRow.IsNull(SQL_COLUMN_COST) AndAlso DbRow.Value(SQL_COLUMN_COST) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COST).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COST), 2)
      m_subtotal1_cost = m_subtotal1_cost + DbRow.Value(SQL_COLUMN_COST)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COST).Value = ""
    End If

    ' Status
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = m_account_promo_status(DbRow.Value(SQL_COLUMN_STATUS))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_CODE).Value = DbRow.Value(SQL_COLUMN_STATUS)

    SetStatusCounters(DbRow.Value(SQL_COLUMN_STATUS), _promo_action)

    ' Points
    If Not DbRow.IsNull(SQL_COLUMN_ACP_POINT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ACP_POINT), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      m_subtotal1_points = m_subtotal1_points + DbRow.Value(SQL_COLUMN_ACP_POINT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = ""
    End If

    ' Details
    If Not DbRow.IsNull(SQL_COLUMN_DETAILS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = DbRow.Value(SQL_COLUMN_DETAILS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = ""
    End If

    ' Won
    If DbRow.Value(SQL_COLUMN_WON) > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_subtotal1_won = m_subtotal1_won + DbRow.Value(SQL_COLUMN_WON)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = ""
    End If

    ' Unique.
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNIQUE).Value = DbRow.Value(SQL_COLUMN_UNIQUE_ID)

    ' Holder data colums
    If Me.chk_view_data_account.Checked Then
      mdl_account_for_report.SetupRowHolderData(Me.Grid, DbRow, RowIndex, GRID_COLUMNS, SQL_INIT_COLUMNS_HOLDER_DATA)
    End If

    ' Promotion category
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_CATEGORY).Value = PromotionCategories.Value(DbRow.Value(SQL_COLUMN_PROMO_CATEGORY))

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(781), m_order)
    PrintData.SetFilter(gb_promo_type.Text, m_promotion_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(811), m_promotion_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(256), m_promo_name)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(336), m_promo_type_credit)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336), m_category)

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_init_date_from = ""
    m_init_date_to = ""
    m_promo_type_credit = ""

    ' Init Date From
    If Me.uc_dsl.FromDateSelected Then
      m_init_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime), _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Init Date To
    If Me.uc_dsl.ToDateSelected Then
      m_init_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime), _
                                      ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Promotion type
    m_promotion_type = ""

    If Me.opt_promo_all.Checked Then
      m_promotion_type = Me.opt_promo_all.Text
    ElseIf Me.opt_promo_manual.Checked Then
      m_promotion_type = Me.opt_promo_manual.Text
    ElseIf Me.opt_promo_system_period.Checked Then
      m_promotion_type = Me.opt_promo_system_period.Text
    ElseIf Me.opt_promo_system_auto.Checked Then
      m_promotion_type = Me.opt_promo_system_auto.Text
    ElseIf Me.opt_promo_per_points.Checked Then
      m_promotion_type = Me.opt_promo_per_points.Text
    ElseIf Me.opt_promo_preassigned.Checked Then
      m_promotion_type = Me.opt_promo_preassigned.Text
    End If

    ' Order
    If opt_order_level_account_prom.Checked Then
      m_order = opt_order_level_account_prom.Text
    ElseIf opt_order_prom_level_account.Checked Then
      m_order = opt_order_prom_level_account.Text
    End If

    ' Status promotion 
    m_promotion_status = ""
    If Me.opt_promo_status_active.Checked Then
      Me.m_promotion_status = opt_promo_status_active.Text
    ElseIf Me.opt_promo_status_expired.Checked Then
      Me.m_promotion_status = opt_promo_status_expired.Text
    ElseIf Me.opt_promo_status_awarded.Checked Then
      Me.m_promotion_status = opt_promo_status_awarded.Text
    ElseIf Me.opt_promo_status_not_awarded.Checked Then
      Me.m_promotion_status = opt_promo_status_not_awarded.Text
    ElseIf Me.opt_promo_status_canceled.Checked Then
      Me.m_promotion_status = opt_promo_status_canceled.Text
    ElseIf Me.opt_promo_status_exausted.Checked Then
      Me.m_promotion_status = opt_promo_status_exausted.Text
    ElseIf Me.opt_promo_status_redeemed.Checked Then
      Me.m_promotion_status = opt_promo_status_redeemed.Text
    End If

    ' Substatus Not played promotion
    If Me.chk_promo_status_not_played.Checked Then
      Me.m_promotion_status &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(784)
    ElseIf Me.chk_promo_with_cost.Checked Then
      Me.m_promotion_status &= " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1357)
    End If

    ' Account
    m_account = Me.uc_account_sel.Account()
    m_track_data = Me.uc_account_sel.TrackData()
    m_holder = Me.uc_account_sel.Holder()
    m_holder_is_vip = Me.uc_account_sel.HolderIsVip()

    ' Promo
    m_promo_name = Me.ef_promo_name.Value

    ' Promotion Credit Type
    If Me.chk_credit_type_points.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(343)
    End If
    If Me.chk_credit_type_redeem.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(337)
    End If
    If Me.chk_credit_type_non_redeem.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(338)
    End If

    ' Promotion Category
    m_category = Me.cmb_categories.TextValue

    m_only_user_accounts = False

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    If m_holder_data Then

      ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_TELEPHONE_NUMBER_2, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_POSTAL_CODE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

    End If

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    mdl_account_for_report.HolderColumnsPrintable(Me.Grid, GRID_COLUMNS, False)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call AwardPromotions()
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call CancelPromotions()
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_holder_data = chk_view_data_account.Checked
        Call MyBase.GUI_ButtonClick(ButtonId)
        Me.Grid.Counter(0).Value = m_num_rows_detail

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

#End Region ' GUI Reports

#Region " Public Functions  "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Call Me.Display(False)

  End Sub ' ShowForEdit 

#End Region ' Public Functions 

#Region " Private Functions "

  ' PURPOSE: Set counters by status
  '
  '  PARAMS:
  '     - INPUT:
  '           - status
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Private Sub SetStatusCounters(ByVal Status As ACCOUNT_PROMO_STATUS, ByRef PromoAction As ENUM_PROMO_STATUS_ACTION)


    Select Case Status
      Case ACCOUNT_PROMO_STATUS.PREASSIGNED
        ' DDM 14-08-2012: Preassigned can be to apply or to cancel 
        m_num_rows_to_apply += 1
        m_num_rows_to_cancel += 1

      Case ACCOUNT_PROMO_STATUS.NOT_AWARDED
        m_num_rows_to_apply += 1
        PromoAction = ENUM_PROMO_STATUS_ACTION.TO_APPLY

      Case ACCOUNT_PROMO_STATUS.ACTIVE, _
           ACCOUNT_PROMO_STATUS.AWARDED
        m_num_rows_to_cancel += 1
        PromoAction = ENUM_PROMO_STATUS_ACTION.TO_CANCEL

    End Select

  End Sub ' SetStatusCounters

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex: Index of grid
  '           - TypeSubtotal: The type of subtotal, we says that subtotal is, according with order.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddSubtotalRow(ByVal RowIndex As Integer, ByVal TypeSubtotal As Integer)

    'Case the selected order "level, account and promotion"  TypeSubtotal = 1 is about account and  TypeSubtotal = 2 is about level
    '                        "promotion, level, account"  TypeSubtotal = 1 is about level and  TypeSubtotal = 2 is about promotion

    If TypeSubtotal = 1 Then
      ' Subtotal 1
      ' add data in reference to subtotal
      If Me.opt_order_level_account_prom.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_LEVEL).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_ACCOUNT_ID).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NAME).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_ACCOUNT_NAME).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_CREATED).Value = GLB_NLS_GUI_INVOICING.GetString(375)
      ElseIf Me.opt_order_prom_level_account.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_LEVEL).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_PROMOTION_NAME).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_CATEGORY).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_PROMO_CATEGORY).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = GLB_NLS_GUI_INVOICING.GetString(375)
      End If
      ' calculated fields
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(m_subtotal1_cash_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(m_subtotal1_promo_points, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(m_subtotal1_redim, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(m_subtotal1_noredim, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(m_subtotal1_played, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(m_subtotal1_points, 0)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = GUI_FormatCurrency(m_subtotal1_won, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(m_subtotal1_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COST).Value = GUI_FormatCurrency(m_subtotal1_cost, 2)
      ' number rows subtotal
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal1, 0))
      m_num_rows_subtotal1 = 0
      ' style
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

      Call CalculateSubtotals2()

    ElseIf TypeSubtotal = 2 Then
      ' Subtotal 2
      ' add data in reference to subtotal
      If Me.opt_order_level_account_prom.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_LEVEL).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = GLB_NLS_GUI_INVOICING.GetString(375)
      ElseIf Me.opt_order_prom_level_account.Checked Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION_NAME).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_PROMOTION_NAME).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_CATEGORY).Value = Me.Grid.Cell(RowIndex - 1, GRID_COLUMN_PROMO_CATEGORY).Value
        Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = GLB_NLS_GUI_INVOICING.GetString(375)
      End If

      Call CalculateTotals()

      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(m_subtotal2_cash_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_POINTS).Value = GUI_FormatNumber(m_subtotal2_promo_points, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_REDIM).Value = GUI_FormatCurrency(m_subtotal2_redim, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NON_REDIM).Value = GUI_FormatCurrency(m_subtotal2_noredim, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED).Value = GUI_FormatCurrency(m_subtotal2_played, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(m_subtotal2_points, 0)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = GUI_FormatCurrency(m_subtotal2_won, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(m_subtotal2_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COST).Value = GUI_FormatCurrency(m_subtotal2_cost, 2)
      ' number rows subtotal
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1256, GUI_FormatNumber(m_num_rows_subtotal2, 0))
      m_num_rows_subtotal2 = 0
      ' style
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    End If
    ' 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_UNIQUE).Value = LINE_TYPE_SUBTOTAL_OR_TOTAL
    'init values
    Call InitSubtotalValues(TypeSubtotal)

  End Sub ' AddSubtotaRow

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _grid_num_columns As Integer

    ' Order columns
    If opt_order_level_account_prom.Checked Then
      GRID_COLUMN_LEVEL = 1
      GRID_COLUMN_ACCOUNT_ID = 2
      GRID_COLUMN_ACCOUNT_NAME = 3
      GRID_COLUMN_PROMOTION_NAME = 4
      GRID_COLUMN_PROMO_CATEGORY = 5
    ElseIf opt_order_prom_level_account.Checked Then
      GRID_COLUMN_PROMOTION_NAME = 1
      GRID_COLUMN_PROMO_CATEGORY = 2
      GRID_COLUMN_LEVEL = 3
      GRID_COLUMN_ACCOUNT_ID = 4
      GRID_COLUMN_ACCOUNT_NAME = 5
    End If

    ' view account columns
    If Me.chk_view_data_account.Checked Then
      _grid_num_columns = GRID_COLUMNS + mdl_account_for_report.GRID_NUM_COLUMNS_HOLDER_DATA
    Else
      _grid_num_columns = GRID_COLUMNS
    End If

    With Me.Grid

      Call .Init(_grid_num_columns, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Level
      .Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
      .Column(GRID_COLUMN_LEVEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443)
      .Column(GRID_COLUMN_LEVEL).Width = 1200
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account name
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(794)
      .Column(GRID_COLUMN_ACCOUNT_NAME).Width = 2500
      .Column(GRID_COLUMN_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1150
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Created
      .Column(GRID_COLUMN_PROMOTION_CREATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMOTION_CREATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(604)
      .Column(GRID_COLUMN_PROMOTION_CREATED).Width = 2000
      .Column(GRID_COLUMN_PROMOTION_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Promotion name
      .Column(GRID_COLUMN_PROMOTION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMOTION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693)
      .Column(GRID_COLUMN_PROMOTION_NAME).Width = 2500
      .Column(GRID_COLUMN_PROMOTION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Promotion Category
      .Column(GRID_COLUMN_PROMO_CATEGORY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_CATEGORY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
      .Column(GRID_COLUMN_PROMO_CATEGORY).Width = 2100
      .Column(GRID_COLUMN_PROMO_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Points Credit
      .Column(GRID_COLUMN_PROMO_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_POINTS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(343)
      .Column(GRID_COLUMN_PROMO_POINTS).Width = 1600
      .Column(GRID_COLUMN_PROMO_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' No redimible Credit
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Width = 1600
      .Column(GRID_COLUMN_PROMO_NON_REDIM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redimible Credit 
      .Column(GRID_COLUMN_PROMO_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PROMO_REDIM).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_PROMO_REDIM).Width = 1600
      .Column(GRID_COLUMN_PROMO_REDIM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played
      .Column(GRID_COLUMN_PLAYED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_PLAYED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
      .Column(GRID_COLUMN_PLAYED).Width = 1600
      .Column(GRID_COLUMN_PLAYED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balance 
      .Column(GRID_COLUMN_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(541)
      .Column(GRID_COLUMN_BALANCE).Width = 1600
      .Column(GRID_COLUMN_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cost Promotion
      .Column(GRID_COLUMN_COST).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_COST).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1319)
      .Column(GRID_COLUMN_COST).Width = 1600
      .Column(GRID_COLUMN_COST).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Status Name
      .Column(GRID_COLUMN_STATUS_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_STATUS_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS_NAME).Width = 1600
      .Column(GRID_COLUMN_STATUS_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Won 
      .Column(GRID_COLUMN_WON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
      .Column(GRID_COLUMN_WON).Width = 1600
      .Column(GRID_COLUMN_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Details
      .Column(GRID_COLUMN_DETAILS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(609)
      .Column(GRID_COLUMN_DETAILS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)
      .Column(GRID_COLUMN_DETAILS).Width = 2000
      .Column(GRID_COLUMN_DETAILS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Point
      .Column(GRID_COLUMN_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(818)
      .Column(GRID_COLUMN_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_POINTS).Width = 1600
      .Column(GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Column are fixed in a specific width when printed
      .Column(GRID_COLUMN_POINTS).ExcelWidth = GRID_WIDTH_EXCEL_POINTS

      ' Cash-in
      .Column(GRID_COLUMN_CASH_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(818)
      .Column(GRID_COLUMN_CASH_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
      .Column(GRID_COLUMN_CASH_IN).Width = 1600
      .Column(GRID_COLUMN_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Column are fixed in a specific width when printed
      .Column(GRID_COLUMN_CASH_IN).ExcelWidth = GRID_WIDTH_EXCEL_CASH_IN

      ' Line type.
      .Column(GRID_COLUMN_UNIQUE).Header(0).Text = " "
      .Column(GRID_COLUMN_UNIQUE).Header(1).Text = " "
      .Column(GRID_COLUMN_UNIQUE).Width = 0

      ' Status code
      .Column(GRID_COLUMN_STATUS_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_STATUS_CODE).Header(1).Text = ""
      .Column(GRID_COLUMN_STATUS_CODE).Width = 0

      ' Credit Type
      .Column(GRID_COLUMN_CREDIT_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_CREDIT_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_CREDIT_TYPE).Width = 0
    End With

    ' Add account columns
    If Me.chk_view_data_account.Checked Then
      mdl_account_for_report.StyleSheetHolderData(Grid, GRID_COLUMNS, , chk_view_data_account.Checked)
    End If

  End Sub ' GUI_StyleSheet


  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date

    ' uc date control
    _final_time = WSI.Common.Misc.TodayOpening()
    _closing_time = _final_time.Hour

    Me.uc_dsl.FromDate = _final_time.AddDays(1 - _final_time.Day)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _final_time.AddDays(1)
    Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.ClosingTime = _closing_time

    ' Order
    Me.opt_order_prom_level_account.Checked = True

    ' Promotion type.
    Me.opt_promo_all.Checked = True

    ' Promotion status
    Me.opt_promo_status_active.Checked = True

    ' Show account data 
    Me.chk_view_data_account.Checked = False

    ' Promotion filter 
    Me.ef_promo_name.Value = ""
    Me.chk_promo_status_not_played.Checked = False
    Me.chk_promo_with_cost.Checked = False

    ' Account filter
    Me.uc_account_sel.Clear()

    ' Promotion Credit Type
    Me.chk_credit_type_points.Checked = False
    Me.chk_credit_type_redeem.Checked = False
    Me.chk_credit_type_non_redeem.Checked = False

    ' Promotion Categories
    Me.cmb_categories.Value = -1

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_sql_where As String
    Dim _str_where_account As String
    Dim _str_where_credit_type As String
    Dim _filter_condition As String

    _str_sql_where = ""
    _str_where_credit_type = ""
    _filter_condition = ""

    'Only User Accounts 
    If m_only_user_accounts Then
      _str_sql_where &= " AND AC_TYPE NOT IN (" & AccountType.ACCOUNT_VIRTUAL_CASHIER & ", " & AccountType.ACCOUNT_VIRTUAL_TERMINAL & ")"
    End If

    ' Filter dates
    _filter_condition = Me.uc_dsl.GetSqlFilterCondition("ACP_CREATED")

    If _filter_condition.Length > 0 Then
      _str_sql_where = _str_sql_where & " AND " & _filter_condition
    End If

    ' Filter promotion status
    '  Promotion active
    If Me.opt_promo_status_active.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.ACTIVE
    End If
    '  Promotion expired
    If Me.opt_promo_status_expired.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.EXPIRED
    End If
    '  Promotion awarded
    If Me.opt_promo_status_awarded.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS NOT IN (" & ACCOUNT_PROMO_STATUS.NOT_AWARDED & _
                                                              ", " & ACCOUNT_PROMO_STATUS.PREASSIGNED & ")"
    End If
    '  Promotion not awarded
    If Me.opt_promo_status_not_awarded.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS IN (" & ACCOUNT_PROMO_STATUS.NOT_AWARDED & _
                                                          ", " & ACCOUNT_PROMO_STATUS.PREASSIGNED & ")"
    End If
    '  Promotion Cancel (Cancelled not added, Cancelled or Cancelled by player)
    If Me.opt_promo_status_canceled.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS IN ( " & ACCOUNT_PROMO_STATUS.CANCELLED_NOT_ADDED & ", " & _
                                                                  ACCOUNT_PROMO_STATUS.CANCELLED & " ," & _
                                                                  ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER & " )"
    End If
    '  Promotion exausted
    If Me.opt_promo_status_exausted.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.EXHAUSTED
    End If
    '  Promotion redeemed
    If Me.opt_promo_status_redeemed.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_STATUS = " & ACCOUNT_PROMO_STATUS.REDEEMED
    End If
    '  Substatus, Promotion not played
    If Me.chk_promo_status_not_played.Checked Then
      _str_sql_where = _str_sql_where & " AND ACP_PLAYED = 0"
    End If

    ' Promotions with cost
    If Me.chk_promo_with_cost.Checked Then
      _str_sql_where = _str_sql_where & " AND ISNULL(ACP_REDEEMABLE_COST,0) > 0"
    End If

    ' Filter promotion type
    If Not Me.opt_promo_all.Checked Then
      '  Promo manual
      If Me.opt_promo_manual.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE = " & Promotion.PROMOTION_TYPE.MANUAL
      End If
      '  Promo system periodic
      If Me.opt_promo_system_period.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE IN (" & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL & _
                                              " , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED & _
                                              " , " & Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY & " )"
      End If
      '  Promo system automatic
      If Me.opt_promo_system_auto.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE IN (" & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_PRIZE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT & _
                                                      " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT & " )"

      End If
      '  Promo per points
      If Me.opt_promo_per_points.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE IN ( " & Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE & _
                                          " , " & Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE & " ) "
      End If

      ' Promo preassigned
      If Me.opt_promo_preassigned.Checked Then
        _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE = " & Promotion.PROMOTION_TYPE.PREASSIGNED
      End If

    Else
      _str_sql_where = _str_sql_where & " AND ACP_PROMO_TYPE NOT IN (" & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT & _
                                              " , " & Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT & " )"

    End If ' end if promo_all

    ' Filter Account
    _str_where_account = Me.uc_account_sel.GetFilterSQL()
    If _str_where_account <> "" Then
      _str_sql_where = _str_sql_where & " AND " & _
                  " ACP_ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE " & _str_where_account & ") "
    End If

    ' Filter Promo Name
    If Me.ef_promo_name.Value <> "" Then
      _str_sql_where = _str_sql_where & " AND (" & GUI_FilterField("ACP_PROMO_NAME", Me.ef_promo_name.Value, False, False, True) & ") "
    End If

    ' Filter Credit Type
    If Me.chk_credit_type_points.Checked Then
      _str_where_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.POINT
    End If
    If Me.chk_credit_type_redeem.Checked Then
      If Not String.IsNullOrEmpty(_str_where_credit_type) Then
        _str_where_credit_type = _str_where_credit_type & ","
      End If
      _str_where_credit_type = _str_where_credit_type & " " & ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    End If
    If Me.chk_credit_type_non_redeem.Checked Then
      If Not String.IsNullOrEmpty(_str_where_credit_type) Then
        _str_where_credit_type = _str_where_credit_type & ","
      End If
      _str_where_credit_type = _str_where_credit_type & " " & ACCOUNT_PROMO_CREDIT_TYPE.NR1 & ", " & ACCOUNT_PROMO_CREDIT_TYPE.NR2
    End If
    If Not String.IsNullOrEmpty(_str_where_credit_type) Then
      _str_where_credit_type = " ACP_CREDIT_TYPE IN ( " & _str_where_credit_type & ")"
      If Not String.IsNullOrEmpty(_str_sql_where) Then
        _str_sql_where = _str_sql_where & " AND "
      End If
      _str_sql_where = _str_sql_where & _str_where_credit_type
    End If

    ' Filter Promo Category
    If Not cmb_categories.Value = -1 Then
      _str_sql_where = _str_sql_where & " AND ACP_PROMO_CATEGORY_ID = " & cmb_categories.Value
    End If


    If _str_sql_where <> "" Then
      _str_sql_where = Strings.Right(_str_sql_where, Len(_str_sql_where) - 5)
      _str_sql_where = " WHERE " & _str_sql_where
    End If

    Return _str_sql_where

  End Function ' GetSqlWhere

  ' PURPOSE: Initialization the subtotals.
  '
  '  PARAMS:
  '     - INPUT :
  '               - TypeSubtotal 
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub InitSubtotalValues(ByVal TypeSubtotal As Integer)

    'always initialize subtotal1
    m_subtotal1_cash_in = 0
    m_subtotal1_promo_points = 0
    m_subtotal1_redim = 0
    m_subtotal1_noredim = 0
    m_subtotal1_played = 0
    m_subtotal1_balance = 0
    m_subtotal1_points = 0
    m_subtotal1_won = 0
    m_subtotal1_cost = 0

    If TypeSubtotal = 2 Then
      m_subtotal2_cash_in = 0
      m_subtotal2_promo_points = 0
      m_subtotal2_redim = 0
      m_subtotal2_noredim = 0
      m_subtotal2_played = 0
      m_subtotal2_balance = 0
      m_subtotal2_points = 0
      m_subtotal2_won = 0
      m_subtotal2_cost = 0
    End If

  End Sub ' InitSubtotalValues

  ' PURPOSE: Activate the selected promotions
  '
  '  PARAMS:
  '     - INPUT :
  '
  '
  '     - OUTPUT :
  '
  Private Sub AwardPromotions()
    Dim _selected_promos As DataTable
    Dim _num_changed As Int32
    Dim _accounts_promotions_updated

    _selected_promos = New DataTable()
    If Not GetDataTableFromSelectedRows(ENUM_PROMO_STATUS_ACTION.TO_APPLY, _selected_promos) Then
      Return
    End If
    If _selected_promos.Rows.Count > 1 Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1080), _
                          ENUM_MB_TYPE.MB_TYPE_INFO, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, _
                          , _selected_promos.Rows.Count) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If
    Else
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1102), _
                      ENUM_MB_TYPE.MB_TYPE_INFO, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, , ) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If
    End If
    _accounts_promotions_updated = AccountPromotion.CreateAccountPromotionTable()
    _num_changed = AccountPromotion.ChangeStatusPromotions(GLB_NLS_GUI_PLAYER_TRACKING.GetString(160, GLB_CurrentUser.Name), _
                                                           _selected_promos, ACCOUNT_PROMO_STATUS.AWARDED, _accounts_promotions_updated)

    If _num_changed > 0 Then
      InsertActionsInAuditor(ENUM_PROMO_STATUS_ACTION.TO_APPLY, _num_changed, _accounts_promotions_updated)
    End If

    If _num_changed = _selected_promos.Rows.Count Then
      ' Pop-up informing that system will activate to awarded promotions..
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1074), ENUM_MB_TYPE.MB_TYPE_INFO, , , _num_changed)
    Else
      ' Any promotion has not been activated
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1082), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _num_changed, _selected_promos.Rows.Count)
    End If

  End Sub ' ActivePromotions

  ' PURPOSE: Cancel the selected promotions
  '
  '  PARAMS:
  '     - INPUT :
  '
  '
  '     - OUTPUT :
  '
  Private Sub CancelPromotions()
    Dim _selected_promos As DataTable
    Dim _num_cancelled As Int32
    Dim _accounts_promotions_updated As DataTable

    _selected_promos = New DataTable()

    If Not GetDataTableFromSelectedRows(ENUM_PROMO_STATUS_ACTION.TO_CANCEL, _selected_promos) Then
      Return
    End If
    If _selected_promos.Rows.Count > 1 Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1081), _
                      ENUM_MB_TYPE.MB_TYPE_INFO, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      , _selected_promos.Rows.Count) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If
    Else
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1103), _
                      ENUM_MB_TYPE.MB_TYPE_INFO, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, , ) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If
    End If
    _accounts_promotions_updated = AccountPromotion.CreateAccountPromotionTable()
    _num_cancelled = AccountPromotion.AccountsPromotionCancelation(GLB_NLS_GUI_PLAYER_TRACKING.GetString(161, GLB_CurrentUser.Name), _
                                                                   _selected_promos, _accounts_promotions_updated)

    If _num_cancelled > 0 Then
      InsertActionsInAuditor(ENUM_PROMO_STATUS_ACTION.TO_CANCEL, _num_cancelled, _accounts_promotions_updated)
    End If

    If _num_cancelled = _selected_promos.Rows.Count Then
      ' Cancelled all promotions
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1078), ENUM_MB_TYPE.MB_TYPE_INFO, , , _num_cancelled)
    Else
      ' Any promotion has not been canceled
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1079), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _num_cancelled, _selected_promos.Rows.Count)
    End If

  End Sub ' CancelPromotions


  ' PURPOSE:  Generate the audit to according with action ( Cancel or Apply)
  '
  '  PARAMS:
  '     - INPUT :
  '           - Action
  '           - NumActions
  '           -  AccountPromotions
  '
  '     - OUTPUT :
  '
  Private Sub InsertActionsInAuditor(ByVal Action As ENUM_PROMO_STATUS_ACTION, ByVal NumActions As Integer, ByVal AccountPromotions As DataTable)

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _value As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAYER_PROMOTIONS)

    Select Case Action
      Case ENUM_PROMO_STATUS_ACTION.TO_APPLY
        If NumActions > 1 Then
          Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(256), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1161, NumActions.ToString()))
        Else
          Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(256), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1162, NumActions.ToString()))
        End If
      Case ENUM_PROMO_STATUS_ACTION.TO_CANCEL
        If NumActions > 1 Then
          Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(256), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1159, NumActions.ToString()))
        Else
          Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(256), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1160, NumActions.ToString()))
        End If
    End Select

    For Each _dr_account_promo As DataRow In AccountPromotions.Rows

      'Cuenta = xxxx, Balance = 10.000 NR, Promo. = Nivel B
      _value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & " = " & _dr_account_promo(AccountPromotion.SQL_COLUMN_ACP_ACCOUNT_ID) & ", "
      _value &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(541) & " = " & AccountPromotion.GetFormatBalance(_dr_account_promo(AccountPromotion.SQL_COLUMN_ACP_BALANCE), _
                                                                                                                _dr_account_promo(AccountPromotion.SQL_COLUMN_ACP_CREDIT_TYPE)) & ", "
      _value &= GLB_NLS_GUI_PLAYER_TRACKING.GetString(1255) & " = " & _dr_account_promo(AccountPromotion.SQL_COLUMN_ACP_PROMO_NAME)

      Call _auditor_data.SetField(0, Strings.Left(_value, 150), "", CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)

    Next

    Call _auditor_data.Notify(CurrentUser.GuiId, _
    CurrentUser.Id, _
    CurrentUser.Name, _
    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
    0, Nothing)

  End Sub

  ' PURPOSE: Function that obtain the rows selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - SelectedPromos Datatable with the rows selected. 
  '
  ' RETURNS:
  '     - Return True: if promotions selected. False: if not promotions selected (Subtotal or Total)
  Private Function GetDataTableFromSelectedRows(ByVal PromoAction As ENUM_PROMO_STATUS_ACTION, _
                                                ByRef SelectedPromos As DataTable) As Boolean
    Dim _promo As DataRow
    Dim _promo_action As ENUM_PROMO_STATUS_ACTION

    'if not selected rows
    If Me.Grid.SelectedRows() Is Nothing OrElse Me.Grid.SelectedRows().Length = 0 Then
      Return False
    End If

    SelectedPromos = AccountPromotion.CreateAccountPromotionTable()

    For Each _selected_row As Integer In Me.Grid.SelectedRows()
      If Me.Grid.Cell(_selected_row, GRID_COLUMN_UNIQUE).Value = LINE_TYPE_SUBTOTAL_OR_TOTAL Then
        Continue For
      End If

      _promo_action = PromoAction
      SetStatusCounters(Me.Grid.Cell(_selected_row, GRID_COLUMN_STATUS_CODE).Value, _promo_action)

      If PromoAction <> _promo_action Then
        Continue For
      End If

      ' Detail line
      _promo = SelectedPromos.NewRow()
      _promo(AccountPromotion.SQL_COLUMN_ACP_UNIQUE_ID) = Me.Grid.Cell(_selected_row, GRID_COLUMN_UNIQUE).Value
      _promo(AccountPromotion.SQL_COLUMN_ACP_ACCOUNT_ID) = Me.Grid.Cell(_selected_row, GRID_COLUMN_ACCOUNT_ID).Value
      _promo(AccountPromotion.SQL_COLUMN_ACP_STATUS) = Me.Grid.Cell(_selected_row, GRID_COLUMN_STATUS_CODE).Value
      _promo(AccountPromotion.SQL_COLUMN_ACP_CREDIT_TYPE) = Me.Grid.Cell(_selected_row, GRID_COLUMN_CREDIT_TYPE).Value
      _promo(AccountPromotion.SQL_COLUMN_ACP_PROMO_NAME) = Me.Grid.Cell(_selected_row, GRID_COLUMN_PROMOTION_NAME).Value

      SelectedPromos.Rows.Add(_promo)
    Next

    If SelectedPromos.Rows.Count = 0 Then
      ' You must select promotions valid for this action.
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1073), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True
  End Function ' GetDataTableFromSelectedRows

  Private Sub EnableDisableStatusOptions()
    Me.opt_promo_status_not_awarded.Enabled = Me.opt_promo_system_period.Checked Or Me.opt_promo_all.Checked Or Me.opt_promo_preassigned.Checked

    If Not Me.opt_promo_system_period.Checked And _
     Not Me.opt_promo_all.Checked And _
     Not Me.opt_promo_preassigned.Checked And _
     Me.opt_promo_status_not_awarded.Checked Then
      Me.opt_promo_status_active.Checked = True
    End If
  End Sub ' EnableDisableStatusOptions

  ' PURPOSE: Calculate Totals of grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CalculateTotals()
    m_total_cash_in = m_total_cash_in + m_subtotal2_cash_in
    m_total_promo_points = m_total_promo_points + m_subtotal2_promo_points
    m_total_redim = m_total_redim + m_subtotal2_redim
    m_total_noredim = m_total_noredim + m_subtotal2_noredim
    m_total_played = m_total_played + m_subtotal2_played
    m_total_balance = m_total_balance + m_subtotal2_balance
    m_total_points = m_total_points + m_subtotal2_points
    m_total_won = m_total_won + m_subtotal2_won
    m_total_cost = m_total_cost + m_subtotal2_cost
  End Sub

  ' PURPOSE: Calculate Subtotals 2 of grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CalculateSubtotals2()
    m_subtotal2_cash_in = m_subtotal2_cash_in + m_subtotal1_cash_in
    m_subtotal2_promo_points = m_subtotal2_promo_points + m_subtotal1_promo_points
    m_subtotal2_redim = m_subtotal2_redim + m_subtotal1_redim
    m_subtotal2_noredim = m_subtotal2_noredim + m_subtotal1_noredim
    m_subtotal2_played = m_subtotal2_played + m_subtotal1_played
    m_subtotal2_balance = m_subtotal2_balance + m_subtotal1_balance
    m_subtotal2_won = m_subtotal2_won + m_subtotal1_won
    m_subtotal2_points = m_subtotal2_points + m_subtotal1_points
    m_subtotal2_cost = m_subtotal2_cost + m_subtotal1_cost
  End Sub

#End Region ' Private functions

#Region " EVENTS "

  ' PURPOSE: Functions to be performed when the "Checked" the object "opt_promo_system_period" changes (Handler events)
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Sender
  '           - e
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub opt_promo_type_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_promo_system_period.CheckedChanged, opt_promo_preassigned.CheckedChanged, opt_promo_all.CheckedChanged
    EnableDisableStatusOptions()
  End Sub ' opt_promo_type_CheckedChanged

  ' PURPOSE: Functions to be performed when the "Checked" the object "chk_promo_with_cost" changes (Handler events)
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Sender
  '           - e
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub chk_promo_with_cost_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_promo_with_cost.CheckedChanged
    If Me.chk_promo_with_cost.Checked Then
      Me.chk_credit_type_non_redeem.Checked = True
      Me.chk_credit_type_redeem.Checked = False
      Me.chk_credit_type_points.Checked = False
      Me.chk_promo_status_not_played.Checked = False
      Me.opt_promo_status_awarded.Checked = True
      Me.gb_status_promo.Enabled = False
      Me.chk_promo_status_not_played.Enabled = False
      Me.gb_promo_credit_type.Enabled = False
    Else
      Me.chk_credit_type_non_redeem.Checked = False
      Me.chk_credit_type_redeem.Checked = False
      Me.chk_credit_type_points.Checked = False
      Me.chk_promo_status_not_played.Checked = False
      Me.opt_promo_status_active.Checked = True
      Me.gb_status_promo.Enabled = True
      Me.chk_promo_status_not_played.Enabled = True
      Me.gb_promo_credit_type.Enabled = True
    End If
  End Sub

#End Region 'Events

End Class
