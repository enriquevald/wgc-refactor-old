'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_pcd_configuration_sel.vb
' DESCRIPTION:   Flags selection form
' AUTHOR:        Xavier Cots
' CREATION DATE: 16-AUG-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-AUG-2012  XCD    Initial version
' 06-MAY-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_pcd_configuration_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_name)
    Me.panel_filter.Size = New System.Drawing.Size(760, 105)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 109)
    Me.panel_data.Size = New System.Drawing.Size(760, 346)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(754, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(754, 4)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(6, 75)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(271, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 2
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_pcd_configuration_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(768, 459)
    Me.Name = "frm_pcd_configuration_sel"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' Grid Rows
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CONFIGURATION_ID As Integer = 1
  Private Const GRID_COLUMN_CONFIGURATION_NAME As Integer = 2
  Private Const GRID_COLUMN_CONFIGURATION_DESCRIPTION As Integer = 3

  Private Const SQL_COLUMN_CONFIGURATION_ID As Integer = 0
  Private Const SQL_COLUMN_CONFIGURATION_NAME As Integer = 1
  Private Const SQL_COLUMN_CONFIGURATION_DESCRIPTION As Integer = 2

  Private Const GRID_COLUMNS As Integer = 4
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region

#Region " Members "
  ' For return items selected in selection mode
  Private ConfigurationSelected() As Integer

  ' For report
  Private m_configuration_name As String

#End Region

#Region " Properties "

#End Region

#Region " Overridable GUI function "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PCD_TERMINALS_CONFIGURATION_SEL
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)

    ' GUI Buttons - Visible
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    ' GUI Buttons - Enabled
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    ' Text
    ' Cancel button text =>  "Exit"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    ' New button text =>  "Nueva"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1)

    ' Flag name entry field
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6018) ' Bandera

    ' Filters set format
    Call ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_PCD_CONFIGURATION.MAX_NAME_LEN)

    ' Default values
    SetDefaultValues()

    ' Grid style View
    Call GUI_StyleView()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_GetSelectedItems()

    Dim _idx As Integer

    ' Init
    Erase ConfigurationSelected
    ConfigurationSelected = Me.Grid.SelectedRows()

    If IsNothing(ConfigurationSelected) Then
      Exit Sub
    Else
      _idx = 0
      For Each _selected_row As Integer In Me.Grid.SelectedRows
        ConfigurationSelected(_idx) = Me.Grid.Cell(_selected_row, GRID_COLUMN_CONFIGURATION_ID).Value
        _idx += 1
      Next
    End If

  End Sub 'GUI_GetSelectedItems

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Short
    Dim _item_id As Integer
    Dim _item_name As String
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the flag ID and launch editor
    _item_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_CONFIGURATION_ID).Value
    _item_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_CONFIGURATION_NAME).Value
    _frm_edit = New frm_pcd_configuration_edit

    Call _frm_edit.ShowEditItem(_item_id, _item_name)
    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Return True
  End Function 'GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" SELECT   SC_CONFIGURATION_ID ")
    _str_sql.AppendLine("        , SC_NAME ")
    _str_sql.AppendLine("        , SC_DESCRIPTION ")
    _str_sql.AppendLine("   FROM   SMIB_CONFIGURATION ")
    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine(" ORDER BY SC_NAME ASC, SC_CONFIGURATION_ID ASC ")

    Return _str_sql.ToString()

  End Function 'GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub 'GUI_FilterReset

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CONFIGURATION_NAME).Value = DbRow.Value(SQL_COLUMN_CONFIGURATION_NAME).ToString


    Return True
  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_NEW
        Call EditNewPCDConfiguration()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  'PURPOSE: Define the ExecuteQuery customized

  ' PARAMS:
  '    - INPUT:

  '    - OUTPUT:

  'RETURNS:
  '    -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _str_sql As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _dataset As DataSet = Nothing
    Dim _datatable As DataTable
    Dim _datarow As System.Data.DataRow
    Dim _n As Int32

    Try

      ' Obtain the SqlQuery
      _str_sql = GUI_FilterGetSqlQuery()

      If String.IsNullOrEmpty(_str_sql) Then
        Return
      End If

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand(_str_sql))
      _sql_da.SelectCommand.CommandTimeout = 100
      _dataset = New DataSet()

      _db_trx.Fill(_sql_da, _dataset)

      _datatable = _dataset.Tables(0)

      Me.Grid.Redraw = False

      If Not ShowHugeNumberOfRows(_datatable.Rows.Count) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      For _n = 0 To _datatable.Rows.Count - 1

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

        _datarow = _datatable.Rows(_n Mod _datatable.Rows.Count)

        Me.Grid.AddRow()
        Call GUI_SetupRow(Me.Grid.NumRows - 1, New CLASS_DB_ROW(_datarow))

      Next _n

      _sql_da.Dispose()
      _db_trx.Dispose()

    Catch ex As OutOfMemoryException
      ' Specific error: Out of memory catch the exception.
      Me.Grid.Redraw = True
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(150), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "PCD configuration selection", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)
    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "PCD configuration selection", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      If _dataset IsNot Nothing Then
        Call _dataset.Clear()
        Call _dataset.Dispose()
        _dataset = Nothing
      End If
    End Try

  End Sub

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6018), m_configuration_name)
  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6017)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()


    m_configuration_name = ""
    
    ' Flag name
    If Not String.IsNullOrEmpty(Me.ef_name.Value()) Then
      m_configuration_name = Me.ef_name.Value()
    End If
  End Sub

#End Region ' GUI Reports

#End Region

#Region " Private Functions "

  ' PURPOSE: Open user edit form to create New User
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewPCDConfiguration()

    Dim idx_row As Short
    ''XVV Variable not use Dim row_type As Integer
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    frm_edit = New frm_pcd_configuration_edit

    Call frm_edit.ShowNewItem()
    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'EditNewUser

  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    If Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION Then
      Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    End If

    ' COLUMN_INDEX
    Me.Grid.Column(GRID_COLUMN_INDEX).Header.Text = " "
    Me.Grid.Column(GRID_COLUMN_INDEX).Width = 200
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    ' CONFIGURATION_ID
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_ID).Mapping = SQL_COLUMN_CONFIGURATION_ID

    ' CONFIGURATION_NAME
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6018) ' Name
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_NAME).Width = 2500
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_NAME).Mapping = SQL_COLUMN_CONFIGURATION_NAME
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6026) ' Description
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_DESCRIPTION).Width = 6000
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_DESCRIPTION).Mapping = SQL_COLUMN_CONFIGURATION_DESCRIPTION
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_CONFIGURATION_DESCRIPTION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

  End Sub 'GUI_StyleView

  Private Sub SetDefaultValues()
    ef_name.Value = ""

  End Sub ' SetDefaultValues

  Private Function GetSqlWhere() As String

    Dim _str_where As System.Text.StringBuilder
    Dim _str_expiration As System.Text.StringBuilder

    _str_where = New System.Text.StringBuilder()
    _str_expiration = New System.Text.StringBuilder()

    '_str_where.AppendLine(" WHERE SC_COMM_TYPE == " + WSI.Common.SmibCommunicationType.PULSES)

    ' Name filter
    If Not String.IsNullOrEmpty(ef_name.Value()) Then
      _str_where.AppendLine(" AND " & GUI_FilterField("SC_NAME", ef_name.Value(), False, False, True))
    End If

    Return _str_where.ToString()

  End Function ' GetSqlWhere

#End Region

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE : Opens dialog with default settings for select mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Function ShowForSelect()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.Display(True)
    Return ConfigurationSelected

  End Function ' ShowForSelect

#End Region

#Region " Events "

#End Region

End Class
