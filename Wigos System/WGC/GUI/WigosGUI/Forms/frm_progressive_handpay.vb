'-------------------------------------------------------------------
' Copyright � 2007-2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_progressive_handpay
' DESCRIPTION:   Progressive edition.
' AUTHOR:        Jes�s �ngel Blanco
' CREATION DATE: 03-SEP-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-SEP-2014  JAB    Initial version.
' 09-SEP-2014  FJC    Control: If there's no terminals: POPUP and EXIT FORM
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text

Public Class frm_progressive_handpay
  Inherits frm_base_edit

#Region " Const "

  Private Const FORM_DB_MIN_VERSION As Short = 224

  Private Const SQL_PROGRESSIVE_LEVEL_ID As Integer = 0
  Private Const SQL_PROGRESSIVE_LEVEL_NAME As Integer = 1

  Private Const SQL_PROGRESSIVE_TERMINAL_ID As Integer = 0
  Private Const SQL_PROGRESSIVE_TERMINAL_NAME As Integer = 1

#End Region

#Region " Members "

  Public m_current_progressive As CLASS_PROGRESSIVE.TYPE_PROGRESSIVE
  Private m_terminals_data As DataTable
#End Region

#Region " Overrides "

  ' PURPOSE: Sets form Id 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROGRESSIVE_HANDPAY
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal ProgressiveId As Integer)
    ' Sets the screen mode
    DbObjectId = ProgressiveId
    DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

    Me.m_current_progressive = New CLASS_PROGRESSIVE.TYPE_PROGRESSIVE
    Me.m_current_progressive.progressive_id = ProgressiveId
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.m_current_progressive.progressive_screen_mode = Me.ScreenMode

    Me.MdiParent = MdiParent

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Return
    End If

    ' Check it there are terminals
    Me.m_terminals_data = Me.GetProgressiveTerminals()

    ' If there's no terminals: POPUP and EXIT
    If Me.m_terminals_data Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1411), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowEditItem

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Award Progressive
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5216)                               ' Otorgar progresivo

    ' Bottoms
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)       ' Aceptar

    ' Controls
    'Progressive name
    Me.ef_progrssive_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217)            ' Progresivo
    Me.ef_progrssive_name.IsReadOnly = True
    Me.ef_progrssive_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 25)

    ' Progressive amount
    Me.ef_progressive_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214)         ' Amount
    Me.ef_progressive_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

    ' Progressive levels
    Me.uc_progressive_levels.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281)         ' Nivel
    Me.uc_progressive_levels.IsReadOnly = False

    ' Progressive terminals
    Me.uc_progressive_terminals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254)      ' Terminal
    Me.uc_progressive_terminals.IsReadOnly = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Fill Combos
    Call FillComboProgressiveLevels()
    If Not Me.m_terminals_data Is Nothing Then
      Me.FillComboProgressiveTerminals(Me.m_terminals_data)
    End If
    ''Call FillComboProgressiveTerminals()

  End Sub ' GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _progressive_item As CLASS_PROGRESSIVE

    _progressive_item = DbReadObject

    Me.ef_progrssive_name.TextValue = _progressive_item.ProgressiveName
    Me.uc_progressive_levels.SelectedIndex = 0
    Me.uc_progressive_terminals.SelectedIndex = 0
    Me.ef_progressive_amount.TextValue = GUI_FormatCurrency(_progressive_item.ProgressiveHandpayAmount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Get data from screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim _progressive_item_edited As CLASS_PROGRESSIVE

    _progressive_item_edited = DbEditedObject

    _progressive_item_edited.ProgressiveHandpayAmount = Me.ef_progressive_amount.TextValue
    _progressive_item_edited.ProgressiveCurrentLevelId = Me.uc_progressive_levels.Value
    _progressive_item_edited.ProgressiveCurrentLevelName = Me.uc_progressive_levels.TextValue
    _progressive_item_edited.ProgressiveCurrentTerminalId = Me.uc_progressive_terminals.Value
    _progressive_item_edited.ProgressiveCurrentTerminalName = Me.uc_progressive_terminals.TextValue

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _progressive_class As CLASS_PROGRESSIVE
    Dim _nls_param1 As String
    Dim _nls_param2 As String
    Dim _nls_param3 As String
    Dim _nls_param4 As String

    If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROGRESSIVE
        _progressive_class = DbEditedObject

        _progressive_class.ProgressiveScreenMode = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_SCREEN_MODE.MODE_EDIT
        _progressive_class.ProgressiveIsHandPay = True

        DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then

          _nls_param1 = ""
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5256), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        _progressive_class = Me.DbEditedObject
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) & ": " & _progressive_class.ProgressiveName
        _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & ": " & _progressive_class.ProgressiveCurrentLevelName
        _nls_param3 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) & ": " & _progressive_class.ProgressiveHandpayAmount
        _nls_param4 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254) & ": " & _progressive_class.ProgressiveCurrentTerminalName

        Me.DbStatus = IIf(NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5395), _
                          ENUM_MB_TYPE.MB_TYPE_INFO, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2, _
                          _nls_param3, _
                          _nls_param4) = ENUM_MB_RESULT.MB_RESULT_YES, _
                          CLASS_BASE.ENUM_STATUS.STATUS_OK, _
                          CLASS_BASE.ENUM_STATUS.STATUS_ERROR)

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          _progressive_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5217) & ": " & _progressive_class.ProgressiveName
          _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5281) & ": " & _progressive_class.ProgressiveCurrentLevelName
          _nls_param3 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214) & ": " & _progressive_class.ProgressiveHandpayAmount
          _nls_param4 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254) & ": " & _progressive_class.ProgressiveCurrentTerminalName

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5396), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2, _
                          _nls_param3, _
                          _nls_param4)
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5398), _
                                    ENUM_MB_TYPE.MB_TYPE_INFO, _
                                    ENUM_MB_BTN.MB_BTN_OK, _
                                    ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub ' GUI_DB_Operation

  ' PURPOSE: To determine if screen data are ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    ' Chack amount
    If Format.ParseCurrency(ef_progressive_amount.TextValue) = Format.ParseCurrency("0") Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5394), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    Return True
  End Function ' GUI_IsScreenDataOk


#End Region

#Region " Privates "

  ' PURPOSE: Fill data to Levels combo.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillComboProgressiveLevels()
    Dim _progressive_item As CLASS_PROGRESSIVE

    _progressive_item = DbReadObject
    For Each _row As DataRow In _progressive_item.ProgressiveLevelsList.Rows
      Me.uc_progressive_levels.Add(_row(SQL_PROGRESSIVE_LEVEL_ID), _row(SQL_PROGRESSIVE_LEVEL_NAME))
    Next

    Me.uc_progressive_levels.SelectedIndex = -1

  End Sub ' FillComboProgressiveLevels

  ' PURPOSE: Get data to Terminals combo.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: Datatable with terminals
  '
  Private Function GetProgressiveTerminals() As DataTable
    Dim _progressive_item As CLASS_PROGRESSIVE
    Dim _terminals_db_read As DataTable
    Dim _terminals_list As DataTable
    Dim _terminal_id_list As StringBuilder
    Dim _first_time As Boolean
    Dim _tmp As String

    Try

      _progressive_item = DbReadObject
      _terminals_db_read = New DataTable
      _terminal_id_list = New StringBuilder
      _first_time = True
      _tmp = ""

      Call WSI.Common.GroupsService.GetTerminalListFromXml(_progressive_item.ProgressiveTerminalList.ToXml, _terminals_db_read, EXPLOIT_ELEMENT_TYPE.PROGRESSIVE)

      If _terminals_db_read.Rows.Count > 0 Then
        _terminal_id_list.AppendLine("          SELECT   te_terminal_id           ")
        _terminal_id_list.AppendLine("                 , te_name                  ")
        _terminal_id_list.AppendLine("            FROM   terminals                ")
        _terminal_id_list.AppendLine("           WHERE   te_terminal_id IN(       ")


        For Each _row As DataRow In _terminals_db_read.Rows
          If _first_time Then
            _first_time = False
          Else
            _tmp += ", "
          End If
          _tmp += _row(SQL_PROGRESSIVE_TERMINAL_ID).ToString()
        Next
        _terminal_id_list.AppendLine(_tmp)
        _terminal_id_list.AppendLine("    )   ")

        _terminals_list = GUI_GetTableUsingSQL(_terminal_id_list.ToString(), 5000)
        If _terminals_list.Rows.Count > 0 Then

          Return _terminals_list
        Else

          Return Nothing
        End If
      Else

        Return Nothing
      End If

    Catch _ex As Exception
      Log.Exception(_ex)

      Return Nothing
    End Try

  End Function

  ' PURPOSE: Fill data to Terminals combo.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillComboProgressiveTerminals(ByVal terminals_db As DataTable)
    For Each _row As DataRow In terminals_db.Rows
      Me.uc_progressive_terminals.Add(_row(SQL_PROGRESSIVE_TERMINAL_ID), _row(SQL_PROGRESSIVE_TERMINAL_NAME))
    Next
    Me.uc_progressive_levels.SelectedIndex = -1

  End Sub

  ''' PURPOSE: Fill data to Terminals combo.
  '''
  '''  PARAMS:
  '''     - INPUT:
  '''
  '''     - OUTPUT:
  '''
  ''' RETURNS:
  '''
  ''''Private Sub FillComboProgressiveTerminals()
  ''''  Dim _progressive_item As CLASS_PROGRESSIVE
  ''''  Dim _terminals_db_read As DataTable
  ''''  Dim _terminals_list As DataTable
  ''''  Dim _terminal_id_list As StringBuilder
  ''''  Dim _first_time As Boolean
  ''''  Dim _tmp As String

  ''''  Try

  ''''    _progressive_item = DbReadObject
  ''''    _terminals_db_read = New DataTable
  ''''    _terminal_id_list = New StringBuilder
  ''''    _first_time = True
  ''''    _tmp = ""

  ''''    Call WSI.Common.GroupsService.GetTerminalListFromXml(_progressive_item.ProgressiveTerminalList.ToXml, _terminals_db_read, EXPLOIT_ELEMENT_TYPE.PROGRESSIVE)

  ''''    _terminal_id_list.AppendLine("          SELECT   te_terminal_id           ")
  ''''    _terminal_id_list.AppendLine("                 , te_name                  ")
  ''''    _terminal_id_list.AppendLine("            FROM   terminals                ")
  ''''    _terminal_id_list.AppendLine("           WHERE   te_terminal_id IN(       ")

  ''''    For Each _row As DataRow In _terminals_db_read.Rows
  ''''      If _first_time Then
  ''''        _first_time = False
  ''''      Else
  ''''        _tmp += ", "
  ''''      End If
  ''''      _tmp += _row(SQL_PROGRESSIVE_TERMINAL_ID).ToString()
  ''''    Next
  ''''    _terminal_id_list.AppendLine(_tmp)
  ''''    _terminal_id_list.AppendLine("    )   ")

  ''''    _terminals_list = GUI_GetTableUsingSQL(_terminal_id_list.ToString(), 5000)

  ''''    For Each _row As DataRow In _terminals_list.Rows
  ''''      Me.uc_progressive_terminals.Add(_row(SQL_PROGRESSIVE_TERMINAL_ID), _row(SQL_PROGRESSIVE_TERMINAL_NAME))
  ''''    Next

  ''''  Catch _ex As Exception

  ''''    Log.Exception(_ex)
  ''''  End Try

  ''''  Me.uc_progressive_levels.SelectedIndex = -1

  ''''End Sub ' FillComboProgressiveTerminals

#End Region

End Class