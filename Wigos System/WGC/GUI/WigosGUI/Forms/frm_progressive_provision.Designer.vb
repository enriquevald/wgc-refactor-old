<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_progressive_provision
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_progressive_provision))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_levels = New System.Windows.Forms.GroupBox()
    Me.btn_incr_2_prov = New System.Windows.Forms.Button()
    Me.btn_theo_2_prov = New System.Windows.Forms.Button()
    Me.dg_levels = New GUI_Controls.uc_grid()
    Me.gb_machine_provisions = New System.Windows.Forms.GroupBox()
    Me.btn_equally = New System.Windows.Forms.Button()
    Me.btn_as_played = New System.Windows.Forms.Button()
    Me.dg_terminals = New GUI_Controls.uc_grid()
    Me.ef_cage_provision_amount = New GUI_Controls.uc_entry_field()
    Me.cmb_progressive = New GUI_Controls.uc_combo()
    Me.lbl_canceled = New System.Windows.Forms.Label()
    Me.lbl_progressive_info_010 = New System.Windows.Forms.Label()
    Me.lbl_progressive_info_020 = New System.Windows.Forms.Label()
    Me.lbl_progressive_info_011 = New System.Windows.Forms.Label()
    Me.lbl_progressive_info_021 = New System.Windows.Forms.Label()
    Me.ef_user_name = New GUI_Controls.uc_entry_field()
    Me.lbl_progressive_info_022 = New System.Windows.Forms.Label()
    Me.lbl_progressive_info_012 = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_levels.SuspendLayout()
    Me.gb_machine_provisions.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_progressive_info_012)
    Me.panel_data.Controls.Add(Me.lbl_progressive_info_022)
    Me.panel_data.Controls.Add(Me.lbl_progressive_info_021)
    Me.panel_data.Controls.Add(Me.lbl_progressive_info_011)
    Me.panel_data.Controls.Add(Me.lbl_progressive_info_020)
    Me.panel_data.Controls.Add(Me.lbl_progressive_info_010)
    Me.panel_data.Controls.Add(Me.lbl_canceled)
    Me.panel_data.Controls.Add(Me.cmb_progressive)
    Me.panel_data.Controls.Add(Me.ef_cage_provision_amount)
    Me.panel_data.Controls.Add(Me.gb_machine_provisions)
    Me.panel_data.Controls.Add(Me.gb_levels)
    Me.panel_data.Controls.Add(Me.gb_date)
    Me.panel_data.Controls.Add(Me.ef_user_name)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(877, 741)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(329, 3)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(263, 103)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(16, 59)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(236, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 60
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = True
    Me.dtp_from.Location = New System.Drawing.Point(16, 23)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(236, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 60
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_levels
    '
    Me.gb_levels.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_levels.Controls.Add(Me.btn_incr_2_prov)
    Me.gb_levels.Controls.Add(Me.btn_theo_2_prov)
    Me.gb_levels.Controls.Add(Me.dg_levels)
    Me.gb_levels.Location = New System.Drawing.Point(3, 111)
    Me.gb_levels.Name = "gb_levels"
    Me.gb_levels.Size = New System.Drawing.Size(869, 179)
    Me.gb_levels.TabIndex = 3
    Me.gb_levels.TabStop = False
    Me.gb_levels.Text = "xProvision"
    '
    'btn_incr_2_prov
    '
    Me.btn_incr_2_prov.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_incr_2_prov.Location = New System.Drawing.Point(11, 143)
    Me.btn_incr_2_prov.Name = "btn_incr_2_prov"
    Me.btn_incr_2_prov.Size = New System.Drawing.Size(110, 30)
    Me.btn_incr_2_prov.TabIndex = 9
    Me.btn_incr_2_prov.Text = "xIncr2Prov"
    Me.btn_incr_2_prov.UseVisualStyleBackColor = True
    '
    'btn_theo_2_prov
    '
    Me.btn_theo_2_prov.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_theo_2_prov.Location = New System.Drawing.Point(139, 143)
    Me.btn_theo_2_prov.Name = "btn_theo_2_prov"
    Me.btn_theo_2_prov.Size = New System.Drawing.Size(110, 30)
    Me.btn_theo_2_prov.TabIndex = 8
    Me.btn_theo_2_prov.Text = "xTheo2Prov"
    Me.btn_theo_2_prov.UseVisualStyleBackColor = True
    '
    'dg_levels
    '
    Me.dg_levels.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_levels.CurrentCol = -1
    Me.dg_levels.CurrentRow = -1
    Me.dg_levels.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_levels.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_levels.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_levels.Location = New System.Drawing.Point(11, 20)
    Me.dg_levels.Name = "dg_levels"
    Me.dg_levels.PanelRightVisible = False
    Me.dg_levels.Redraw = True
    Me.dg_levels.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_levels.Size = New System.Drawing.Size(847, 117)
    Me.dg_levels.Sortable = False
    Me.dg_levels.SortAscending = True
    Me.dg_levels.SortByCol = 0
    Me.dg_levels.TabIndex = 0
    Me.dg_levels.ToolTipped = True
    Me.dg_levels.TopRow = -2
    Me.dg_levels.WordWrap = False
    '
    'gb_machine_provisions
    '
    Me.gb_machine_provisions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_machine_provisions.Controls.Add(Me.btn_equally)
    Me.gb_machine_provisions.Controls.Add(Me.btn_as_played)
    Me.gb_machine_provisions.Controls.Add(Me.dg_terminals)
    Me.gb_machine_provisions.Location = New System.Drawing.Point(3, 298)
    Me.gb_machine_provisions.Name = "gb_machine_provisions"
    Me.gb_machine_provisions.Size = New System.Drawing.Size(869, 410)
    Me.gb_machine_provisions.TabIndex = 4
    Me.gb_machine_provisions.TabStop = False
    Me.gb_machine_provisions.Text = "xProvisionToTerminals"
    '
    'btn_equally
    '
    Me.btn_equally.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_equally.Location = New System.Drawing.Point(139, 369)
    Me.btn_equally.Name = "btn_equally"
    Me.btn_equally.Size = New System.Drawing.Size(110, 30)
    Me.btn_equally.TabIndex = 6
    Me.btn_equally.Text = "xEqually"
    Me.btn_equally.UseVisualStyleBackColor = True
    '
    'btn_as_played
    '
    Me.btn_as_played.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_as_played.Location = New System.Drawing.Point(11, 369)
    Me.btn_as_played.Name = "btn_as_played"
    Me.btn_as_played.Size = New System.Drawing.Size(110, 30)
    Me.btn_as_played.TabIndex = 5
    Me.btn_as_played.Text = "xAsPlayed"
    Me.btn_as_played.UseVisualStyleBackColor = True
    '
    'dg_terminals
    '
    Me.dg_terminals.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_terminals.CurrentCol = -1
    Me.dg_terminals.CurrentRow = -1
    Me.dg_terminals.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_terminals.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_terminals.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_terminals.Location = New System.Drawing.Point(11, 20)
    Me.dg_terminals.Name = "dg_terminals"
    Me.dg_terminals.PanelRightVisible = False
    Me.dg_terminals.Redraw = True
    Me.dg_terminals.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_terminals.Size = New System.Drawing.Size(847, 330)
    Me.dg_terminals.Sortable = False
    Me.dg_terminals.SortAscending = True
    Me.dg_terminals.SortByCol = 0
    Me.dg_terminals.TabIndex = 0
    Me.dg_terminals.ToolTipped = True
    Me.dg_terminals.TopRow = -2
    Me.dg_terminals.WordWrap = False
    '
    'ef_cage_provision_amount
    '
    Me.ef_cage_provision_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ef_cage_provision_amount.DoubleValue = 0.0R
    Me.ef_cage_provision_amount.IntegerValue = 0
    Me.ef_cage_provision_amount.IsReadOnly = False
    Me.ef_cage_provision_amount.Location = New System.Drawing.Point(601, 712)
    Me.ef_cage_provision_amount.Name = "ef_cage_provision_amount"
    Me.ef_cage_provision_amount.PlaceHolder = Nothing
    Me.ef_cage_provision_amount.Size = New System.Drawing.Size(244, 24)
    Me.ef_cage_provision_amount.SufixText = "Sufix Text"
    Me.ef_cage_provision_amount.SufixTextVisible = True
    Me.ef_cage_provision_amount.TabIndex = 5
    Me.ef_cage_provision_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cage_provision_amount.TextValue = ""
    Me.ef_cage_provision_amount.TextWidth = 150
    Me.ef_cage_provision_amount.Value = ""
    Me.ef_cage_provision_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_progressive
    '
    Me.cmb_progressive.AllowUnlistedValues = False
    Me.cmb_progressive.AutoCompleteMode = False
    Me.cmb_progressive.IsReadOnly = False
    Me.cmb_progressive.Location = New System.Drawing.Point(3, 17)
    Me.cmb_progressive.Name = "cmb_progressive"
    Me.cmb_progressive.SelectedIndex = -1
    Me.cmb_progressive.Size = New System.Drawing.Size(312, 24)
    Me.cmb_progressive.SufixText = "Sufix Text"
    Me.cmb_progressive.SufixTextVisible = True
    Me.cmb_progressive.TabIndex = 1
    Me.cmb_progressive.TextCombo = Nothing
    Me.cmb_progressive.TextWidth = 120
    '
    'lbl_canceled
    '
    Me.lbl_canceled.BackColor = System.Drawing.Color.Red
    Me.lbl_canceled.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_canceled.ForeColor = System.Drawing.Color.White
    Me.lbl_canceled.Location = New System.Drawing.Point(9, 74)
    Me.lbl_canceled.Name = "lbl_canceled"
    Me.lbl_canceled.Size = New System.Drawing.Size(306, 23)
    Me.lbl_canceled.TabIndex = 10
    Me.lbl_canceled.Text = "xCanceledProvision"
    Me.lbl_canceled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_progressive_info_010
    '
    Me.lbl_progressive_info_010.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_progressive_info_010.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progressive_info_010.Location = New System.Drawing.Point(9, 53)
    Me.lbl_progressive_info_010.Name = "lbl_progressive_info_010"
    Me.lbl_progressive_info_010.Size = New System.Drawing.Size(129, 15)
    Me.lbl_progressive_info_010.TabIndex = 11
    Me.lbl_progressive_info_010.Text = "xProgressiveInfo010"
    '
    'lbl_progressive_info_020
    '
    Me.lbl_progressive_info_020.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_progressive_info_020.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progressive_info_020.Location = New System.Drawing.Point(9, 69)
    Me.lbl_progressive_info_020.Name = "lbl_progressive_info_020"
    Me.lbl_progressive_info_020.Size = New System.Drawing.Size(129, 15)
    Me.lbl_progressive_info_020.TabIndex = 12
    Me.lbl_progressive_info_020.Text = "xProgressiveInfo02"
    '
    'lbl_progressive_info_011
    '
    Me.lbl_progressive_info_011.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_progressive_info_011.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progressive_info_011.Location = New System.Drawing.Point(133, 53)
    Me.lbl_progressive_info_011.Name = "lbl_progressive_info_011"
    Me.lbl_progressive_info_011.Size = New System.Drawing.Size(100, 15)
    Me.lbl_progressive_info_011.TabIndex = 15
    Me.lbl_progressive_info_011.Text = "xProgressiveInfo011"
    '
    'lbl_progressive_info_021
    '
    Me.lbl_progressive_info_021.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_progressive_info_021.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progressive_info_021.Location = New System.Drawing.Point(133, 69)
    Me.lbl_progressive_info_021.Name = "lbl_progressive_info_021"
    Me.lbl_progressive_info_021.Size = New System.Drawing.Size(100, 15)
    Me.lbl_progressive_info_021.TabIndex = 16
    Me.lbl_progressive_info_021.Text = "xProgressiveInfo021"
    '
    'ef_user_name
    '
    Me.ef_user_name.DoubleValue = 0.0R
    Me.ef_user_name.IntegerValue = 0
    Me.ef_user_name.IsReadOnly = False
    Me.ef_user_name.Location = New System.Drawing.Point(43, 44)
    Me.ef_user_name.Name = "ef_user_name"
    Me.ef_user_name.PlaceHolder = Nothing
    Me.ef_user_name.Size = New System.Drawing.Size(216, 24)
    Me.ef_user_name.SufixText = "Sufix Text"
    Me.ef_user_name.SufixTextVisible = True
    Me.ef_user_name.TabIndex = 21
    Me.ef_user_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_user_name.TextValue = ""
    Me.ef_user_name.Value = ""
    Me.ef_user_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_progressive_info_022
    '
    Me.lbl_progressive_info_022.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_progressive_info_022.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progressive_info_022.Location = New System.Drawing.Point(230, 68)
    Me.lbl_progressive_info_022.Name = "lbl_progressive_info_022"
    Me.lbl_progressive_info_022.Size = New System.Drawing.Size(94, 15)
    Me.lbl_progressive_info_022.TabIndex = 22
    Me.lbl_progressive_info_022.Text = "xProgressiveInfo042"
    '
    'lbl_progressive_info_012
    '
    Me.lbl_progressive_info_012.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_progressive_info_012.ForeColor = System.Drawing.Color.Navy
    Me.lbl_progressive_info_012.Location = New System.Drawing.Point(230, 53)
    Me.lbl_progressive_info_012.Name = "lbl_progressive_info_012"
    Me.lbl_progressive_info_012.Size = New System.Drawing.Size(94, 16)
    Me.lbl_progressive_info_012.TabIndex = 23
    Me.lbl_progressive_info_012.Text = "xProgressiveInfo042"
    '
    'frm_progressive_provision
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(981, 749)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_progressive_provision"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_progressive_provision"
    Me.panel_data.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_levels.ResumeLayout(False)
    Me.gb_machine_provisions.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_levels As System.Windows.Forms.GroupBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents dg_levels As GUI_Controls.uc_grid
  Friend WithEvents gb_machine_provisions As System.Windows.Forms.GroupBox
  Friend WithEvents dg_terminals As GUI_Controls.uc_grid
  Friend WithEvents ef_cage_provision_amount As GUI_Controls.uc_entry_field
  Friend WithEvents btn_as_played As System.Windows.Forms.Button
  Friend WithEvents btn_equally As System.Windows.Forms.Button
  Friend WithEvents cmb_progressive As GUI_Controls.uc_combo
  Friend WithEvents btn_theo_2_prov As System.Windows.Forms.Button
  Friend WithEvents btn_incr_2_prov As System.Windows.Forms.Button
  Friend WithEvents lbl_canceled As System.Windows.Forms.Label
  Friend WithEvents lbl_progressive_info_020 As System.Windows.Forms.Label
  Friend WithEvents lbl_progressive_info_010 As System.Windows.Forms.Label
  Friend WithEvents lbl_progressive_info_011 As System.Windows.Forms.Label
  Friend WithEvents lbl_progressive_info_021 As System.Windows.Forms.Label
  Friend WithEvents ef_user_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_progressive_info_012 As System.Windows.Forms.Label
  Friend WithEvents lbl_progressive_info_022 As System.Windows.Forms.Label
End Class
