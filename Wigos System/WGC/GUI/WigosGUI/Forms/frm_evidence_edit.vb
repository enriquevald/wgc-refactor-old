'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_evidence_edit
' DESCRIPTION:   Edit Evidence
' AUTHOR:        Joaquim Cid
' CREATION DATE: 20-MAR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-MAR-2012  JCM    Initial version
' 02-APR-2012  JCM    Control column length NAME, ID1, ID2 from fields business/ representative/ holder
' 04-APR-2012  JCM    Print result ok or failed alert
' 16-APR-2012  JAB    Add functionality to choice print (move "GetPrinterName" & "SelectPrinterDlg" functions to WSI.Common.Printer.cs).
' 02-MAY-2012  MPO    The button cancel/apply in the same functionality. Added new control for indetity data (RFC/CURP/Name).
' 10-AUG-2012  DDM    Added fields Name, last name 1 and last name 2. Deleted full name.
' 23-AUG-2012  MPO    Fixed bug: GetScreenData SetScreenData the controls that retrieve data it's wrong.
' 30-JAN-2013  LEM    Fixed bug #556: Button Edit/Cancel appear disabled with write permissions
' 14-FEB-2013  JCM    Fixed bug: Verify changes although user no enter Edit Mode on Form
' 14-FEB-2013  JCM    Moved DeleTempFiles to Misc, and Fixed Bug: infinite loop when app from third lock files
' 18-FEB-2013  JCM    Added function DiscardChanges used on Cancel and Exit
' 04-APR-2013  DMR    Fixed Bug #318: Don't allow evidence edit if disabled
' 11-JUN-2013  NMR    Preventing the focus losing when showing details
' 04-DEC-2013  LJM    Changes to use the standard behaviour on ButtonClick events
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 30-SEP-2015  FOS    Show/hide retention documents is these are generated
' 05-APR-2016  FOS    Fixed Bug 10142:It doesn't print witholding documents from principal tab.
' 20-JUN-2016  LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
' 29-JUN-2016  LTC    Bug 14941: Witholding Evidence - Witholding viewer hide tag
' 14-JUL-2016  LTC    Bug 15605: Witholding - Unhandled exception while printing a witholding codument
' 31-OCT-2016  FGB    Bug 19793: Gui -> Witholding: error opening the report on prizes' retention
' 01-MAR-2017  FGB    Bug 24126: Withholding generation: The Excel data is incorrect
' 16-MAR-2017  ATB    PBI 25737:Third TAX - Report columns
' 29-MAY-2017  LTC    Bug 27737: Witholding document can't open - Error "terminales conectados"
' 21-JUN-2017  LTC    Bug 28314:WIGOS-2992 Printing Witholding - Must print just selected document
' 01-AGU-2017  DHA    Bug 29072:WIGOS-3983 [Ticket #7379] Avoid to print 2 Witholding Document
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.IO
Imports System.Drawing.Printing
Imports WSI.Common
Imports WSI.Common.Utilities.Extensions
Imports System.Collections.Generic

Public Class frm_evidence_edit
  Inherits frm_base_edit

#Region "Constants"

#End Region

#Region "Members"
  Private m_evidence As CLASS_EVIDENCE
  Private m_screen_data_ok As Boolean
  Private m_old_temp_folders() As String
  Private m_edition_mode As Boolean
#End Region

#Region "Properties"

#End Region

#Region "Overrides"

  Public Sub New()
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
  End Sub

  ' PURPOSE : GUI_CLOSING, BEFORE CLOSE, IF USER NOT CANCEL THEN RELEASE TEMP MEMORY USED BY PDF AND DELETE TEMP FILES
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    If m_edition_mode Then
      If DiscardChanges(ENUM_BUTTON.BUTTON_CANCEL) Then
        CloseCanceled = False
      Else
        CloseCanceled = True
      End If
    End If

    If Not CloseCanceled Then
      While tab_evidence_viewer.TabCount > 1
        Me.tab_evidence_viewer.TabPages.RemoveAt(0)
      End While

      Call Me.wb_evidence.Dispose()
      Call DeleteTempFiles(m_evidence.Temp_Folder)
    End If

  End Sub 'GUI_Closing

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal OperationId As Int64)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = OperationId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0  'PRINT
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)

      Case ENUM_BUTTON.BUTTON_OK        'APPLY
        m_screen_data_ok = False
        If CheckAllowChanges() Then
          Call MyBase.GUI_ButtonClick(ButtonId)
          If m_screen_data_ok Then
            Me.m_evidence.DB_UpdateDocument()
            Set_Controls_Edit(False)
            Me.RefreshTabsEvidence(True) ' LTC 20-JUN-2016 
          End If
        End If

      Case ENUM_BUTTON.BUTTON_DELETE    'EDIT OR CANCEL
        ' CANCEL
        If m_edition_mode Then
          If DiscardChanges(ENUM_BUTTON.BUTTON_DELETE) Then
            Call Set_Controls_Edit(False)
            Call Me.RefreshTabsEvidence(True) ' LTC 20-JUN-2016 
          End If

        Else
          ' EDIT 
          If CheckAllowChanges() Then
            While tab_evidence_viewer.TabCount > 1
              Me.tab_evidence_viewer.TabPages.RemoveAt(1)
            End While

            Set_Controls_Edit(True)
            Update_Business_or_Representative()
          End If
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  'PURPOSE: Executed when User Click Cancel or Exit
  '         Disacrd Changes If User agree undo Changes
  '
  ' PARAMS:
  '    - INPUT :  Button Id wich call the function, usefull for NLS
  '
  '    - OUTPUT :
  '       TRUE:   User  agreee  discard changes  or  No Changes
  '      FALSE:   User disagree discard changes
  ' RETURNS:
  '
  Protected Function DiscardChanges(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON) As Boolean
    Dim _nls_id As Integer

    ' Update EditedObject with Screen Data
    Call GUI_GetScreenData()

    'If no changes, exit
    If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

      Return True
    End If

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE    'EDIT OR CANCEL
        _nls_id = GLB_NLS_GUI_CONTROLS.Id(156)
      Case ENUM_BUTTON.BUTTON_CANCEL
        ' Exit 
        _nls_id = GLB_NLS_GUI_CONTROLS.Id(101)
      Case Else
        _nls_id = GLB_NLS_GUI_CONTROLS.Id(101)
    End Select

    ' Request User Agree Discard Changes
    If NLS_MsgBox(_nls_id, _
                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  ENUM_MB_BTN.MB_BTN_YES_NO, _
                  ENUM_MB_DEF_BTN.MB_DEF_BTN_2) _
                  = ENUM_MB_RESULT.MB_RESULT_YES Then
      ' Undo Changes
      DbEditedObject = DbReadObject.Duplicate()
      m_evidence = DbEditedObject

      ' Useful when exit if Authorization Changed in GP 
      '  and necessary when Cancel
      Call GUI_SetScreenData(0)

      Return True
    End If

    Return False
  End Function ' DiscardChanges


  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_EVIDENCE_VIEWER
    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()
    Dim _size As Size

    'Grow window only by height, always with fixed width
    _size.Width = Me.Size.Width
    _size.Height = 2500
    Me.MaximumSize = _size
    _size.Height = Me.Size.Height
    Me.MinimumSize = _size

    'Disable Close Windows Ok_Click_Event, OK functionally is replaced by Apply
    CloseOnOkClick = False

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(444)    'Evidence Edit Viewer

    'Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)  'EXIT

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(31)      'APPLY

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(5) 'PRINT

    If Me.m_evidence.IsGeneratedDocument Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CONTROLS.GetString(25)  'EDIT

    Me.tab_evidence_viewer.TabPages.Item(0).Text = GLB_NLS_GUI_INVOICING.GetString(437)

    Set_Controls_Edit(False)   'Set Property Controls Before Edit Key Pressed

    'Operation data fields
    Me.gb_operation.Text = GLB_NLS_GUI_INVOICING.GetString(350)
    'date
    Me.dt_operation_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dt_operation_date.IsReadOnly = True
    Me.dt_operation_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    'Gross prize
    Me.ef_prize_amount.Text = GLB_NLS_GUI_INVOICING.GetString(33)
    Me.ef_prize_amount.IsReadOnly = True
    Me.ef_prize_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    'Tax1
    Me.ef_tax1_amount.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")
    Me.ef_tax1_amount.IsReadOnly = True
    Me.ef_tax1_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    'Tax2
    Me.ef_tax2_amount.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")
    Me.ef_tax2_amount.IsReadOnly = True
    Me.ef_tax2_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    ' Total payment
    Me.ef_total_payment.Text = GLB_NLS_GUI_INVOICING.GetString(432)
    Me.ef_total_payment.IsReadOnly = True
    Me.ef_total_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    Me.id_company.Init()
    Me.id_representative.Init()

    'LTC 29-JUN-2016
    Me.lnkEvidenceFile.Visible = False

    m_edition_mode = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    m_screen_data_ok = False

    '   - Holder Name
    If Me.id_holder_data.IdentityName.Trim() = "" Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186))
      Call Me.id_holder_data.SetFocusIdentityName()

      Return False

    End If

    '   - Holder Last name 1
    If Me.id_holder_data.IdentityLastName1.Trim() = "" Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1188))
      Call Me.id_holder_data.SetFocusIdentityLastName1()

      Return False

    End If

    '   - Holder  RFC
    If Me.id_holder_data.IdentityID1.Trim() = "" Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC))
      Call Me.id_holder_data.SetFocusIdentityID1()

      Return False

    End If

    If ValidateFormat.WrongRFC(Me.id_holder_data.IdentityID1.Trim()) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5050), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC))
      Call Me.id_holder_data.SetFocusIdentityID1()

      Return False

    End If

    m_screen_data_ok = Me.id_holder_data.ValidateFormat()

    Return m_screen_data_ok

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    m_evidence = DbEditedObject

    m_evidence.Player_Id1 = Me.id_holder_data.IdentityID1
    m_evidence.Player_Id2 = Me.id_holder_data.IdentityID2
    m_evidence.Player_Name3 = Me.id_holder_data.IdentityName
    m_evidence.Player_Name1 = Me.id_holder_data.IdentityLastName1
    m_evidence.Player_Name2 = Me.id_holder_data.IdentityLastName2

    m_evidence.Business_Name = Me.id_company.CompanyName
    m_evidence.Business_Id1 = Me.id_company.CompanyID1
    m_evidence.Business_Id2 = Me.id_company.CompanyID2

    m_evidence.Representative_Name = Me.id_representative.RepresentativeName
    m_evidence.Representative_Id1 = Me.id_representative.RepresentativeID1
    m_evidence.Representative_Id2 = Me.id_representative.RepresentativeID2

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Me.id_holder_data.IdentityName = m_evidence.Player_Name3
    Me.id_holder_data.IdentityID1 = m_evidence.Player_Id1
    Me.id_holder_data.IdentityID2 = m_evidence.Player_Id2
    Me.id_holder_data.IdentityLastName1 = m_evidence.Player_Name1
    Me.id_holder_data.IdentityLastName2 = m_evidence.Player_Name2

    Me.id_company.CompanyName = m_evidence.Business_Name
    Me.id_company.CompanyID1 = m_evidence.Business_Id1
    Me.id_company.CompanyID2 = m_evidence.Business_Id2

    Me.id_representative.RepresentativeName = m_evidence.Representative_Name
    Me.id_representative.RepresentativeID1 = m_evidence.Representative_Id1
    Me.id_representative.RepresentativeID2 = m_evidence.Representative_Id2

    dt_operation_date.Value = m_evidence.Date_Time
    ef_prize_amount.Value = m_evidence.Prize
    ef_tax1_amount.Value = m_evidence.Witholding_tax1
    ef_tax2_amount.Value = m_evidence.Witholding_tax2
    ef_tax3_amount.Value = m_evidence.Witholding_tax3
    ef_total_payment.Value = m_evidence.Total_Payment

    If m_evidence.Doc_List Is Nothing Then
      Me.wb_evidence.Visible = False
    Else
      Me.wb_evidence.Visible = True
    End If

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_EVIDENCE
        m_evidence = DbEditedObject
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          m_evidence = Me.DbEditedObject
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(585), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        Else
          'Generates Docs
          RefreshTabsEvidence(False) ' LTC 20-JUN-2016 
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          m_evidence = Me.DbEditedObject
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(586), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.id_holder_data.SetFocusIdentityName()
  End Sub 'GUI_SetInitialFocus

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub
#End Region

#Region "Private Functions"
  ' PURPOSE: Sets the controls when user are editing or are reading the form.
  '         
  ' PARAMS:
  '    - INPUT:   enable = True    -> Set User Controls When Users Pressed Edit  Button
  '               enable = False   -> Set User Controls When Users Pressed Apply Button
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub Set_Controls_Edit(ByVal ShowEnable As Boolean)
    Me.tab_evidence_viewer.SelectTab(0)

    If ShowEnable Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CONTROLS.GetString(2)   'CANCEL
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CONTROLS.GetString(25)  'EDIT
    End If

    m_edition_mode = ShowEnable

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = Me.Permissions.Write

    Me.id_holder_data.ReadOnlyIdentity = Not ShowEnable

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = ShowEnable
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Not ShowEnable   'Print
  End Sub 'Controls_Edit

  ' PURPOSE: Check the (Operation_date + GLB_MAX_DAYS_EDIT_EVIDENCE) < Current_Date. If the operation date don't exceed from allow time to change
  '          Check also user_permissions
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Function CheckAllowChanges() As Boolean
    Dim _allow_edit As Boolean
    Dim _allow_time As Integer
    Dim _op_date As Date

    _allow_edit = GeneralParam.GetBoolean("Witholding.Document", "Enabled")

    If Not _allow_edit Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1881), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    _allow_time = GeneralParam.GetInt32("Witholding.Document", "EditableMinutes")
    _op_date = m_evidence.Date_Time.AddMinutes(_allow_time)

    If _op_date > WSI.Common.WGDB.Now Then
      Return Me.Permissions.Write 'The modifications is allowed, but depends of Permissions.Write.
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(588), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

  End Function ' CheckAllowChanges

  ' PURPOSE: 'Check If in General Params the Business and Representative has been changed, then ask if we want to update this evidences params with udpated
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :

  Private Sub Update_Business_or_Representative()

    Me.id_company.LoadCompanyData()
    Me.id_representative.LoadRepresentativeData()

  End Sub 'Update_Business_or_Representative

#Region " Generate Docs "

  ''' <summary>
  ''' Load documents from DB
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function LoadDocuments() As Boolean
    Dim _doc_status As CLASS_EVIDENCE.EVIDENCE_DOCUMENT_STATUS
    _doc_status = m_evidence.LoadDocuments()

    Select Case _doc_status
      Case CLASS_EVIDENCE.EVIDENCE_DOCUMENT_STATUS.EDS_ERROR
        '131 "Se produjo un error al generar el fichero."
        NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id((131)), _
                   ENUM_MB_TYPE.MB_TYPE_INFO, _
                   ENUM_MB_BTN.MB_BTN_OK)

      Case CLASS_EVIDENCE.EVIDENCE_DOCUMENT_STATUS.EDS_NOT_CREATED_YET
        '7705 "A�n no se ha generado el documento de constancia de retenci�n, espere por favor..."
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id((7705)), _
                   ENUM_MB_TYPE.MB_TYPE_INFO, _
                   ENUM_MB_BTN.MB_BTN_OK)
    End Select

    Return (_doc_status = CLASS_EVIDENCE.EVIDENCE_DOCUMENT_STATUS.EDT_OK)

  End Function


  ' PURPOSE: Refresh & Regenerates de taps and pdfs
  '         
  ' PARAMS:
  '    - INPUT: ShowPagePDF = True -> PDF control is created
  '             ShowPagePDF = False -> PDF control is not created; used only when windows is showed at first time
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub RefreshTabsEvidence(ByVal ShowPagePDF As Boolean)

    If Me.m_evidence.IsGeneratedDocument Then
      Dim _old_temp_folder As String

      _old_temp_folder = ""
      If Not m_evidence.Temp_Folder Is Nothing Then
        _old_temp_folder = m_evidence.Temp_Folder
      End If

      If Not LoadDocuments() Then
        'If something goes wrong loading documents from DB, the form should be closed
        DbStatus = ENUM_STATUS.STATUS_ERROR   'If the rows are void or null 
        Call Me.Close()

        Exit Sub
      End If

      'Go on if document loaded OK
      Call InitTabControl()
      Call Me.wb_evidence.Dispose()
      Me.wb_evidence = New WebBrowser()

      If ShowPagePDF Then
        ' LTC 20-JUN-2016 
        Call ShowTabPageEvidence(0)
        Call ShowTabPageEvidence(1)
      End If

      'Del If previous docs generated
      If Not _old_temp_folder = "" Then
        DeleteTempFiles(_old_temp_folder)
      End If
    End If
  End Sub

  ''' <summary>
  ''' Create a tab for each document of the withholding
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CreateDocumentTabs()

    Dim _idx As Integer
    Dim _generated_excel_document As Boolean

    If m_evidence.IsGeneratedDocument Then

      m_evidence.Temp_Folder = String.Empty
      m_evidence.Temp_Files = New String(m_evidence.Doc_List.Count - 1) {}

      _generated_excel_document = Witholding.GenerateWitholdingDocumentExcel()

      For _idx = 0 To m_evidence.Doc_List.Count - 1

        GenerateTabForDocument(_idx, _generated_excel_document)
      Next
    End If
  End Sub

  ' PURPOSE: Initilizes the tab control where the documents will be shown and shows the first one
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub InitTabControl()

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      While tab_evidence_viewer.TabCount > 1
        Me.tab_evidence_viewer.TabPages.RemoveAt(1)
      End While

      If m_evidence.Doc_List.Count < 1 Then

        '108 "No documents related to the evidence found."
        NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id((108)), _
                 ENUM_MB_TYPE.MB_TYPE_INFO, _
                 ENUM_MB_BTN.MB_BTN_OK)

        DbStatus = ENUM_STATUS.STATUS_ERROR   'If the rows are void or null 
        Call Me.Close()
        Exit Sub

      End If

      CreateDocumentTabs()

      'when all documents tabs loaded, let's show the first one
      'Call ShowTabPagePDF(0)

    Catch ex As Exception

      NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id((123)), _
                 ENUM_MB_TYPE.MB_TYPE_ERROR, _
                 ENUM_MB_BTN.MB_BTN_OK)

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Evidence Viewer ", _
                            "InitTabControl", _
                            ex.Message)

      'If something goes wrong loading evidences from DB, the form should be closed
      Call Me.Close()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try

  End Sub 'InitTabControl

  'Does generate excel for codere
  Private Function DoesGenerateExcelDocument() As Boolean
    Dim _generate_document As String
    Dim _generated_excel_document As Integer

    _generate_document = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "GenerateDocument")
    _generated_excel_document = (Convert.ToInt32(IIf(_generate_document <> String.Empty _
                                                     , _generate_document _
                                                     , CashierDocs.EVIDENCE_TYPE.ESTATAL)))

    Return (_generated_excel_document = CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE)
  End Function

  Private Function GetNamePrize1() As String

    Return WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")

  End Function

  Private Function GetNamePrize2() As String

    Return WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")

  End Function

  Private Function GetNameOtherPrize() As String

    Return "Other"

  End Function

  Private Function GetTabCaptionForDocument(ByVal DocumentIndex As Integer, ByVal GenerateTabForDocument As Boolean) As String

    If m_evidence.Doc_List(DocumentIndex).Name.Contains(CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE.ToString()) Then
      If m_evidence.Doc_List(DocumentIndex).Name.Contains("xlsx") Then
        Return GetNamePrize1() + " (xlsx)"  'Federal
      End If
      Return GetNamePrize1()  'Federal
    ElseIf m_evidence.Doc_List(DocumentIndex).Name.Contains(CashierDocs.EVIDENCE_TYPE.FEDERAL.ToString()) Then
      Return GetNamePrize1()  'Federal
    ElseIf m_evidence.Doc_List(DocumentIndex).Name.Contains(CashierDocs.EVIDENCE_TYPE.ESTATAL.ToString()) Then
      Return GetNamePrize2()  'Statal
    End If

    'if not GenerateTabForDocument the system generates the documents in the following order:
    '  0 - Federal
    '  1 - Statal

    'if GenerateTabForDocument the system generates the documents in the following order:
    '  0 - Federal
    '  1 - Federal Coder (Excel)
    '  2 - Statal

    Select Case DocumentIndex
      Case 0
        Return GetNamePrize1()    'Federal

      Case 1
        If GenerateTabForDocument Then
          Return GetNamePrize1()  'Federal Codere
        Else
          Return GetNamePrize2()  'Statal
        End If

      Case 2
        If GenerateTabForDocument Then
          Return GetNamePrize2()  'Statal
        End If
    End Select

    Return GetNameOtherPrize()    'Other

  End Function

  Private Sub GenerateTabForDocument(ByVal DocumentIndex As Integer, ByVal GenerateTabForDocument As Boolean)
    Dim _temp_file As String
    Dim _tab_caption As String

    _tab_caption = GetTabCaptionForDocument(DocumentIndex, GenerateTabForDocument)

    Me.tab_evidence_viewer.TabPages.Add(_tab_caption)

    '    Me.tab_evidence_viewer.TabPages.Add(WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name"))  'EStatal

    _temp_file = GetTempFile(DocumentIndex)

    If Not _temp_file Is Nothing Then
      If File.Exists(_temp_file) Then
        File.Delete(_temp_file)
      End If

      Call WriteStreamToTempFolder(_temp_file, m_evidence.Doc_List(DocumentIndex).Content)
    End If

    m_evidence.Temp_Files(DocumentIndex) = _temp_file
  End Sub

  ''' <summary>
  ''' Generates temp document to show on tab
  ''' </summary>
  ''' <param name="DocumentIndex"></param>
  ''' <remarks></remarks>
  Private Sub GenerateTempDocument(ByVal DocumentIndex As Integer)
    Dim _temp_file As String

    _temp_file = GetTempFile(DocumentIndex)

    If Not _temp_file Is Nothing Then
      If File.Exists(_temp_file) Then
        File.Delete(_temp_file)
      End If

      Call WriteStreamToTempFolder(_temp_file, m_evidence.Doc_List(DocumentIndex).Content)
    End If

    m_evidence.Temp_Files(DocumentIndex) = _temp_file
  End Sub

  ' PURPOSE: Gets the tab page activated for the user and shows its corresponding PDF on the web browser
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub ShowTabPageEvidence(ByVal TabIndex As Integer)

    Dim _document_path As String

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

      'set the webbrowser to a blank page
      Call wb_evidence.Navigate("about:blank")

      If TabIndex < Me.tab_evidence_viewer.TabPages.Count - 1 Then

        If TabIndex < m_evidence.Doc_List.Count Then

          'If there is a envidence related to the current tab...
          If Not m_evidence.Doc_List(TabIndex) Is Nothing Then
            '...we look for its temp PDF file...
            _document_path = GetTempFile(TabIndex)

            If _document_path IsNot Nothing Then
              '...and the tell the webbrowser to show it
              ' LTC 20-JUN-2016 
              If (IsAnExcelFile(_document_path)) Then

                Me.lnkEvidenceFile.Text = _document_path.Replace(m_evidence.Temp_Folder, "").ToString().Replace("\", "").Replace(".xlsx", "").Replace(".xls", "") ' LTC 14-JUL-2016
                Me.lnkEvidenceFile.Parent = Me.tab_evidence_viewer.TabPages(TabIndex + 1)
                Me.lnkEvidenceFile.Dock = DockStyle.Bottom

                If (OfficeInfo.IsInstalled(OfficeInfo.MSOfficeApplications.Excel)) Then
                  Me.lnkEvidenceFile.Tag = _document_path.Replace(m_evidence.Temp_Folder, "") ' LTC 14-JUL-2016
                Else
                  Me.lnkEvidenceFile.Tag = "_path"
                End If

                'LTC 29-JUN-2016
                Me.lnkEvidenceFile.Visible = True
              Else
                Call Me.wb_evidence.Navigate(_document_path & "#toolbar=0&statusbar=0&navpanes=0&view=FitH")
                Me.lnkEvidenceFile.Tag = ""

                'LTC 29-JUN-2016
                Me.lnkEvidenceFile.Visible = False
              End If

              Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
            End If
          End If
        End If

        'Finally, let's place the webbrowser control on the tab selected by user
        Me.wb_evidence.Parent = Me.tab_evidence_viewer.TabPages(TabIndex + 1)
        Me.wb_evidence.Dock = DockStyle.Fill
      End If

    Catch ex As Exception

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Evidence Viewer ", _
                            "ShowTabPagePDF", _
                            ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try
  End Sub 'ShowTabPagePDF

  ''' <summary>
  ''' Is an Excel file
  ''' </summary>
  ''' <param name="DocumentFileName"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsAnExcelFile(DocumentFileName As String) As Boolean
    Dim _extension As String

    _extension = Path.GetExtension(DocumentFileName)

    Return (_extension.Equals(".xlsx") OrElse (_extension.Equals(".xls")))
  End Function

  ' PURPOSE: Saves a stream to a temp file to load it into the webbrowser control
  '         
  ' PARAMS:
  '    - INPUT:
  '       StreamPDF
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Function GetTempFile(ByVal IdxPDF As Integer) As String
    Dim _temp_file As String

    Try

      'An empty file name means that this evidence hasn't been saved to a temp file previously
      If m_evidence.Temp_Folder = "" Then

        'let's make a random and expected "unique" filename to save the evidence to a temp file
        m_evidence.Temp_Folder = Path.GetRandomFileName()
        m_evidence.Temp_Folder = m_evidence.Temp_Folder.Replace(".", "")
        m_evidence.Temp_Folder = Path.GetTempPath & m_evidence.Temp_Folder

        'ensure that the file doesn't exist
        While Directory.Exists(m_evidence.Temp_Folder)

          'if exists, let's make another random and expected "unique" filename
          m_evidence.Temp_Folder = Path.GetRandomFileName()
          m_evidence.Temp_Folder = m_evidence.Temp_Folder.Replace(".", "")
          m_evidence.Temp_Folder = Path.GetTempPath & m_evidence.Temp_Folder

        End While

        Directory.CreateDirectory(m_evidence.Temp_Folder)
      End If

      'loop up for a previously saved PDF
      _temp_file = m_evidence.Temp_Files(IdxPDF)

      If _temp_file Is Nothing _
        Or Not File.Exists(_temp_file) Then

        'if there is not a previously saved PDF, let's generate it
        _temp_file = m_evidence.Temp_Folder & Path.DirectorySeparatorChar & m_evidence.Doc_List(IdxPDF).Name
        Call WriteStreamToTempFolder(_temp_file, m_evidence.Doc_List(IdxPDF).Content)
        m_evidence.Temp_Files(IdxPDF) = _temp_file

      End If

      Return _temp_file

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Evidence Viewer ", _
                            "GetTempFile", _
                            ex.Message)

      Return Nothing

    End Try

  End Function 'GetTempFile

  ' PURPOSE: Save the file stored previously on the DB to a temp file
  '         
  ' PARAMS:
  '    - INPUT:
  '       FilePath
  '        Buffer
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub WriteStreamToTempFolder(ByVal FilePath As String, ByVal Buffer() As Byte)
    Dim _file_stream As FileStream

    _file_stream = New FileStream(FilePath, FileMode.CreateNew, FileAccess.Write, FileShare.Read)

    _file_stream.Write(Buffer, 0, Buffer.Length)

    _file_stream.Close()

  End Sub 'WriteStreamToTempFolder

#End Region 'Generate Docs

#Region " Print Docs "
  ' PURPOSE: Prints the evidence
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Protected Overrides Sub GUI_PrintResultList()
    Dim _print_correct As Boolean
    Dim _printer_name As String
    Dim _print_params As WSI.Common.GhostScriptParameter
    Dim _index_to_print As Integer
    Dim _witholding_document_num_copies As Integer

    ' LTC 14-JUL-2016
    _printer_name = ""
    _print_correct = True
    _index_to_print = 0

    If m_evidence.Temp_Files Is Nothing Then
      '632 There was an error sending the documents to the print queue.
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(632), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _witholding_document_num_copies = GeneralParam.GetInt32("Witholding.Document", "NumCopies", 2)

    For Each _file As String In m_evidence.Temp_Files

      ' LTC 2017-JUN-21
      _index_to_print += 1
      If Not tab_evidence_viewer.SelectedIndex = _index_to_print And tab_evidence_viewer.SelectedIndex <> 0 Then
        Continue For
      End If

      ' LTC 14-JUL-2016
      If _file.ToUpper().Contains(".PDF") Then

        ' 2 is the default number of copies
        If _printer_name = "" Then
          _printer_name = Printer.GetPrinterName(_witholding_document_num_copies)
        End If

        If String.IsNullOrEmpty(_printer_name) Then
          Continue For
        End If

        _print_params = New WSI.Common.GhostScriptParameter(_file, _witholding_document_num_copies, _printer_name)
        _print_correct = WSI.Common.GSPrint.Print(_print_params)

      ElseIf _file.ToUpper().Contains(".XLS") Then

        ' LTC 20-JUN-2016 
        If (OfficeInfo.IsInstalled(OfficeInfo.MSOfficeApplications.Excel)) Then
          Dim fi As System.IO.FileInfo
          fi = New FileInfo(_file)

          If (fi.Exists) Then
            System.Diagnostics.Process.Start(fi.FullName)
          End If

        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7372), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)  ' LTC 2017-MAY-29
        End If

      End If
    Next

    If Not _print_correct Then
      '632 There was an error sending the documents to the print queue.
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(632), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    '633 The documents have been sent successfully to the print queue.
    NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(633), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
  End Sub 'GUI_PrintResultList

#End Region    'Print Documents

#End Region

#Region "Events"
  Private Sub tab_evidence_viewer_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tab_evidence_viewer.Selected

    If e.TabPageIndex = 0 Then                  'Only show Web Browser (pdf viewer) when the selected tab is not the 0 (data entry)
      Me.wb_evidence.Visible = False
    Else
      Me.wb_evidence.Dispose()                  'If document tab change, we must release the previous buffer memory, ifnot will have errors deleting temp files
      Me.wb_evidence = New WebBrowser()
      Call ShowTabPageEvidence(e.TabPageIndex - 1) 'LTC 20-JUN-2016 
      Me.wb_evidence.Visible = True
    End If

  End Sub

  ' LTC 20-JUN-2016 
  ' PURPOSE: Open linked excel file
  '         
  ' PARAMS:
  '    - INPUT: LinkLabel Object, Args
  '
  '    - OUTPUT: 
  '
  ' RETURNS :
  Private Sub lnkEvidenceFile_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkEvidenceFile.LinkClicked
    If (OfficeInfo.IsInstalled(OfficeInfo.MSOfficeApplications.Excel)) Then
      ' LTC 14-JUL-2016
      Call Me.wb_evidence.Navigate(m_evidence.Temp_Folder & Me.lnkEvidenceFile.Tag & "#toolbar=0&statusbar=0&navpanes=0&view=FitH")
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7372), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
    End If
  End Sub

#End Region

End Class