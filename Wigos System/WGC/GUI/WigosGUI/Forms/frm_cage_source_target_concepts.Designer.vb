<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_source_target_concepts
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_group = New System.Windows.Forms.GroupBox
    Me.chk_disabled = New System.Windows.Forms.CheckBox
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.chk_system = New System.Windows.Forms.CheckBox
    Me.ef_Name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_group)
    Me.panel_filter.Controls.Add(Me.chk_system)
    Me.panel_filter.Controls.Add(Me.ef_Name)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(989, 75)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_Name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_system, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 79)
    Me.panel_data.Size = New System.Drawing.Size(989, 376)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(983, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(983, 4)
    '
    'gb_group
    '
    Me.gb_group.Controls.Add(Me.chk_disabled)
    Me.gb_group.Controls.Add(Me.chk_enabled)
    Me.gb_group.Location = New System.Drawing.Point(6, 6)
    Me.gb_group.Name = "gb_group"
    Me.gb_group.Size = New System.Drawing.Size(140, 66)
    Me.gb_group.TabIndex = 0
    Me.gb_group.TabStop = False
    Me.gb_group.Text = "xLeyenda"
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(14, 42)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(100, 17)
    Me.chk_disabled.TabIndex = 1
    Me.chk_disabled.Text = "xInhabilitado"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(14, 19)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(89, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xHabilitado"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_system
    '
    Me.chk_system.AutoSize = True
    Me.chk_system.Location = New System.Drawing.Point(239, 48)
    Me.chk_system.Name = "chk_system"
    Me.chk_system.Size = New System.Drawing.Size(126, 17)
    Me.chk_system.TabIndex = 2
    Me.chk_system.Text = "xMostrar Sistema"
    Me.chk_system.UseVisualStyleBackColor = True
    '
    'ef_Name
    '
    Me.ef_Name.DoubleValue = 0
    Me.ef_Name.IntegerValue = 0
    Me.ef_Name.IsReadOnly = False
    Me.ef_Name.Location = New System.Drawing.Point(157, 18)
    Me.ef_Name.Name = "ef_Name"
    Me.ef_Name.Size = New System.Drawing.Size(295, 24)
    Me.ef_Name.SufixText = "Sufix Text"
    Me.ef_Name.SufixTextVisible = True
    Me.ef_Name.TabIndex = 1
    Me.ef_Name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_Name.TextValue = ""
    Me.ef_Name.Value = ""
    Me.ef_Name.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_cage_source_target_concepts
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(999, 459)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_source_target_concepts"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_source_target_concepts"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group.ResumeLayout(False)
    Me.gb_group.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_group As System.Windows.Forms.GroupBox
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_system As System.Windows.Forms.CheckBox
  Friend WithEvents ef_Name As GUI_Controls.uc_entry_field
End Class
