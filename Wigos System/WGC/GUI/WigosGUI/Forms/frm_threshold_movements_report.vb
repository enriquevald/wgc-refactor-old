﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_threshold_movements_report.vb
' DESCRIPTION:   Displays list of threshold movements
' AUTHOR:        Alberto Marcos
' CREATION DATE: 15-NOV-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 15-NOV-2017  AMF     Initial version
' 27-NOV-2017  MS     [WIGOS-6840] Document type data instead of "player identification number" data in "Threshold movements report" results grid
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonMisc
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Currency
Imports System.Text

Public Class frm_threshold_movements_report
  Inherits frm_base_sel

#Region " Members "
  Private m_init_date_from As Date
  Private m_threshold_movements As CLASS_THRESHOLD_MOVEMENTS

  Private m_filter_from As String
  Private m_filter_to As String
  Private m_filter_type As String
  Private m_filter_client As String
  Private m_filter_user As String

#End Region ' Members

#Region " Constants "

  ' SQL Columns

  Private Const SQL_COLUMN_THRESHOLD_ID As Integer = 0
  Private Const SQL_COLUMN_DATE As Integer = 1
  Private Const SQL_COLUMN_TRANSACTION_TYPE As Integer = 2
  Private Const SQL_COLUMN_TRANSACTION_SUBTYPE As Integer = 3
  Private Const SQL_COLUMN_CASH_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_TOTAL_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_USER_ID As Integer = 6
  Private Const SQL_COLUMN_USER_NAME As Integer = 7
  Private Const SQL_COLUMN_CLIENT_NAME As Integer = 8
  Private Const SQL_COLUMN_HOLDER_ADDRESS_01 As Integer = 9
  Private Const SQL_COLUMN_HOLDER_ADDRESS_02 As Integer = 10
  Private Const SQL_COLUMN_HOLDER_ADDRESS_03 As Integer = 11
  Private Const SQL_COLUMN_HOLDER_EXT_NUM As Integer = 12
  Private Const SQL_COLUMN_HOLDER_CITY As Integer = 13
  Private Const SQL_COLUMN_HOLDER_FED_ENTITY As Integer = 14
  Private Const SQL_COLUMN_HOLDER_ZIP As Integer = 15
  Private Const SQL_COLUMN_HOLDER_ADDRESS_COUNTRY As Integer = 16
  Private Const SQL_COLUMN_HOLDER_NATIONALITY As Integer = 17
  Private Const SQL_COLUMN_HOLDER_BIRTH_DATE As Integer = 18
  Private Const SQL_COLUMN_HOLDER_OCCUPATION_ID As Integer = 19
  Private Const SQL_COLUMN_HOLDER_TYPE As Integer = 20
  Private Const SQL_COLUMN_HOLDER_ID As Integer = 21
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 22
  Private Const SQL_COLUMN_SITE_ID As Integer = 23
  Private Const SQL_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 24

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_THRESHOLD_ID As Integer = 1
  Private Const GRID_COLUMN_SITE_ID As Integer = 2
  Private Const GRID_COLUMN_DATE As Integer = 3
  Private Const GRID_COLUMN_TRANSACTION_TYPE As Integer = 4
  Private Const GRID_COLUMN_TRANSACTION_SUBTYPE As Integer = 5
  Private Const GRID_COLUMN_CASH_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_TOTAL_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_USER_ID As Integer = 8
  Private Const GRID_COLUMN_USER_NAME As Integer = 9
  Private Const GRID_COLUMN_CLIENT_NAME As Integer = 10
  Private Const GRID_COLUMN_HOLDER As Integer = 11
  Private Const GRID_COLUMN_HOLDER_ADDRESS As Integer = 12
  Private Const GRID_COLUMN_HOLDER_EXT_NUM As Integer = 13
  Private Const GRID_COLUMN_HOLDER_CITY As Integer = 14
  Private Const GRID_COLUMN_HOLDER_FED_ENTITY As Integer = 15
  Private Const GRID_COLUMN_HOLDER_ZIP As Integer = 16
  Private Const GRID_COLUMN_HOLDER_ADDRESS_COUNTRY As Integer = 17
  Private Const GRID_COLUMN_HOLDER_NATIONALITY As Integer = 18
  Private Const GRID_COLUMN_HOLDER_BIRTH_DATE As Integer = 19
  Private Const GRID_COLUMN_HOLDER_OCCUPATION As Integer = 20
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 21
  Private Const GRID_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 22

  Private Const GRID_COLUMNS As Integer = 23
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_TAB As String = "     "

  ' DATAGRID Width
  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_ID As Integer = 1000
  Private Const GRID_WIDTH_VALUE As Integer = 2740
  Private Const GRID_WIDTH_TYPE As Integer = 1500
  Private Const GRID_WIDTH_DATES As Integer = 2000
  Private Const GRID_WIDTH_DATE_SHORT As Integer = 1100
  Private Const GRID_WIDTH_HOLDER_ZIP As Integer = 900
  Private Const GRID_WIDTH_HOLDER_DOC As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_CITY As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_COUNTRY As Integer = 2000
  Private Const GRID_WIDTH_HOLDER_NATIONALITY As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT As Integer = 1150
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_HOLDER_FED_ENTITY As Integer = 2500
  Private Const GRID_WIDTH_HOLDER_EXT_NUM As Integer = 1650
  Private Const GRID_WIDTH_HOLDER_OCCUPATION_DESCRIPTION As Integer = 3000

  ' Text format columns
  Private Const EXCEL_COLUMN_ZIP As Integer = 14

#End Region ' Constants

#Region " Constructor "

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

  End Sub ' New

#End Region 'Constructor

#Region " Public Methods "

  ''' <summary>
  ''' Sets the Form Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_THRESHOLD_MOVEMENTS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Show the Form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    m_init_date_from = Nothing

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Methods

#Region " Overrides "

  ''' <summary>
  ''' Initialise all the form controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Customer base
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8776)

    ' Date Range
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)

    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.gb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
    Me.opt_one_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(286)
    Me.opt_all_users.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287)
    Me.chk_show_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7038)

    Me.gp_client.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
    Call Me.ef_holder_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 50)

    Me.uc_transaction_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)

    Me.m_threshold_movements = New CLASS_THRESHOLD_MOVEMENTS

    Call SetDefaultValues()

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set te Form Filters to their Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  ''' <summary>
  ''' Calls the threshold class to return the SQL Query
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String
    Dim _user As String

    str_sql = ""
    _user = String.Empty

    If Not opt_all_users.Checked Then
      _user = cmb_user.Value
    End If


    str_sql = m_threshold_movements.getThresholdMovements(dtp_from, dtp_to, uc_transaction_type, _user, ef_holder_name.Value) ' TODO HULK

    Return str_sql

  End Function 'GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Validates the Filter selection where needed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    'Date selecteciton
    If Me.dtp_from.Checked AndAlso Me.dtp_to.Checked Then
      If dtp_from.Value > dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1688), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Writes the Grid Row reformating where required and adding calculated cell values
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As CLASS_DB_ROW) As Boolean

    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)
    End If

    'Movements
    If Not DbRow.IsNull(SQL_COLUMN_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TRANSACTION_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSACTION_TYPE).Value = CurrencyExchange.GetDescriptionType(DbRow.Value(SQL_COLUMN_TRANSACTION_TYPE))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TRANSACTION_SUBTYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSACTION_SUBTYPE).Value = CurrencyExchange.GetDescriptionSubType(DbRow.Value(SQL_COLUMN_TRANSACTION_SUBTYPE))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_CASH_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_AMOUNT))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TOTAL_AMOUNT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_USER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_ID).Value = DbRow.Value(SQL_COLUMN_USER_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)
    End If

    'Client

    If Not DbRow.IsNull(SQL_COLUMN_CLIENT_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CLIENT_NAME).Value = DbRow.Value(SQL_COLUMN_CLIENT_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ADDRESS_01) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ADDRESS).Value = DbRow.Value(SQL_COLUMN_HOLDER_ADDRESS_01)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ADDRESS_02) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ADDRESS).Value = String.Format("{0} {1}", Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ADDRESS).Value, DbRow.Value(SQL_COLUMN_HOLDER_ADDRESS_02))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ADDRESS_03) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ADDRESS).Value = String.Format("{0} {1}", Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ADDRESS).Value, DbRow.Value(SQL_COLUMN_HOLDER_ADDRESS_03))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_EXT_NUM) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_EXT_NUM).Value = DbRow.Value(SQL_COLUMN_HOLDER_EXT_NUM)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_CITY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_CITY).Value = DbRow.Value(SQL_COLUMN_HOLDER_CITY)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_FED_ENTITY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_FED_ENTITY).Value = CardData.FedEntitiesString(DbRow.Value(SQL_COLUMN_HOLDER_FED_ENTITY))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ZIP) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ZIP).Value = DbRow.Value(SQL_COLUMN_HOLDER_ZIP)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ADDRESS_COUNTRY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_ADDRESS_COUNTRY).Value = CardData.CountriesString(DbRow.Value(SQL_COLUMN_HOLDER_ADDRESS_COUNTRY))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NATIONALITY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NATIONALITY).Value = CardData.NationalitiesString(DbRow.Value(SQL_COLUMN_HOLDER_NATIONALITY))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_BIRTH_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_BIRTH_DATE).Value = DbRow.Value(SQL_COLUMN_HOLDER_BIRTH_DATE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_OCCUPATION_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_OCCUPATION).Value = CardData.OccupationsDescriptionString(DbRow.Value(SQL_COLUMN_HOLDER_OCCUPATION_ID))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER).Value = IdentificationTypes.DocIdTypeStringList(DbRow.Value(SQL_COLUMN_HOLDER_TYPE), False)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_ID) Then
      If Not DbRow.IsNull(SQL_COLUMN_HOLDER_TYPE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER).Value = String.Format("{0}-{1}", Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER).Value, DbRow.Value(SQL_COLUMN_HOLDER_ID))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER).Value = DbRow.Value(SQL_COLUMN_HOLDER_ID)
      End If

    End If

    'Account

    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    End If

    'Actual Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_HOLDER_NAME)
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Update filters for report.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_from = String.Empty
    m_filter_to = String.Empty
    m_filter_type = String.Empty
    m_filter_client = String.Empty
    m_filter_user = String.Empty

    If dtp_from.Checked Then
      m_filter_from = dtp_from.Value
    End If

    If dtp_to.Checked Then
      m_filter_to = dtp_to.Value
    End If

    If Not String.IsNullOrEmpty(ef_holder_name.Value) Then
      m_filter_client = ef_holder_name.Value
    End If

    m_filter_type = uc_transaction_type.SelectedValuesList()

    If opt_all_users.Checked Then
      m_filter_user = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807) 'All
    Else
      m_filter_user = cmb_user.TextValue
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), m_filter_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), m_filter_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(254), m_filter_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862), m_filter_client)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(603), m_filter_user)
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_ZIP, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

#End Region ' Overrides

#Region " Private Methods "

  ''' <summary>
  ''' Sets Default Values for the Filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = GUI_GetDate().AddDays(-1).AddHours(GetDefaultClosingTime())
    Me.dtp_to.Value = GUI_GetDate().AddHours(GetDefaultClosingTime())

    Me.dtp_from.Checked = True
    Me.dtp_to.Checked = False

    Me.opt_all_users.Checked = True

    Me.chk_show_all.Checked = False

    Me.ef_holder_name.Value = String.Empty

    Call FillComboUsers(False)

    Call FillMovementTypeGrid()

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Populate the Combo Grid for Statuses
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FillMovementTypeGrid()

    uc_transaction_type.Clear()
    uc_transaction_type.Add(m_threshold_movements.getListCurrencies())
    uc_transaction_type.SetDefaultValue(False)

  End Sub ' FillStatusGrid

  ''' <summary>
  ''' Fill combo users
  ''' </summary>
  ''' <param name="All"></param>
  ''' <remarks></remarks>
  Private Sub FillComboUsers(ByVal All As Boolean)

    Dim _sql As StringBuilder

    _sql = New StringBuilder()

    _sql.AppendLine("   SELECT   GU_USER_ID            ")
    _sql.AppendLine("          , GU_USERNAME           ")
    _sql.AppendLine("     FROM   GUI_USERS             ")
    _sql.AppendLine(String.Format("    WHERE   GU_USER_TYPE    = {0} ", Int32.Parse(WSI.Common.GU_USER_TYPE.USER)))

    If Not All Then
      _sql.AppendLine(String.Format("      AND   GU_BLOCK_REASON = {0} ", Int32.Parse(WSI.Common.GUI_USER_BLOCK_REASON.NONE)))
    End If

    _sql.AppendLine(" ORDER BY   GU_USERNAME           ")

    Call SetCombo(Me.cmb_user, _sql.ToString())

  End Sub ' FillComboUsers

  ''' <summary>
  ''' Style Sheet for the look of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    With Me.Grid

      ' Initialize the index of unfixed columns and return the number of columns
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Enable sortable in Main Grid Columns
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'Movements
      ' Threshold id
      .Column(GRID_COLUMN_THRESHOLD_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_THRESHOLD_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_THRESHOLD_ID).Width = 0
      .Column(GRID_COLUMN_THRESHOLD_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_THRESHOLD_ID).IsColumnPrintable = False

      ' Site id
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SITE_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927)
      .Column(GRID_COLUMN_SITE_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_SITE_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Transaction date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2287)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATES
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Transaction type
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6601)
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TRANSACTION_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Transaction subtype
      .Column(GRID_COLUMN_TRANSACTION_SUBTYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_TRANSACTION_SUBTYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8777)
      .Column(GRID_COLUMN_TRANSACTION_SUBTYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TRANSACTION_SUBTYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cash amount
      .Column(GRID_COLUMN_CASH_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_CASH_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6576)
      .Column(GRID_COLUMN_CASH_AMOUNT).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_CASH_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total amount
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6629)
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' User id
      .Column(GRID_COLUMN_USER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_USER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6676)
      .Column(GRID_COLUMN_USER_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_USER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' User name
      .Column(GRID_COLUMN_USER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786)
      .Column(GRID_COLUMN_USER_NAME).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Client
      ' Full name
      .Column(GRID_COLUMN_CLIENT_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_CLIENT_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(758)
      .Column(GRID_COLUMN_CLIENT_NAME).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_CLIENT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Holder
      .Column(GRID_COLUMN_HOLDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1566)
      .Column(GRID_COLUMN_HOLDER).Width = GRID_WIDTH_HOLDER_DOC
      .Column(GRID_COLUMN_HOLDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Address
      .Column(GRID_COLUMN_HOLDER_ADDRESS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_ADDRESS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7629)
      .Column(GRID_COLUMN_HOLDER_ADDRESS).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_HOLDER_ADDRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ext num
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2091)
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Width = GRID_WIDTH_HOLDER_EXT_NUM
      .Column(GRID_COLUMN_HOLDER_EXT_NUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' City
      .Column(GRID_COLUMN_HOLDER_CITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_CITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(762)
      .Column(GRID_COLUMN_HOLDER_CITY).Width = GRID_WIDTH_HOLDER_CITY
      .Column(GRID_COLUMN_HOLDER_CITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Fed idenity
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Header(1).Text = AccountFields.GetStateName()
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Width = GRID_WIDTH_HOLDER_FED_ENTITY
      .Column(GRID_COLUMN_HOLDER_FED_ENTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ZIP
      .Column(GRID_COLUMN_HOLDER_ZIP).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_ZIP).Header(1).Text = AccountFields.GetZipName()
      .Column(GRID_COLUMN_HOLDER_ZIP).Width = GRID_WIDTH_HOLDER_ZIP
      .Column(GRID_COLUMN_HOLDER_ZIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' country
      .Column(GRID_COLUMN_HOLDER_ADDRESS_COUNTRY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_ADDRESS_COUNTRY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5090)
      .Column(GRID_COLUMN_HOLDER_ADDRESS_COUNTRY).Width = GRID_WIDTH_HOLDER_COUNTRY
      .Column(GRID_COLUMN_HOLDER_ADDRESS_COUNTRY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Nationality
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2089)
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Width = GRID_WIDTH_HOLDER_NATIONALITY
      .Column(GRID_COLUMN_HOLDER_NATIONALITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Birthday
      .Column(GRID_COLUMN_HOLDER_BIRTH_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_BIRTH_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651)
      .Column(GRID_COLUMN_HOLDER_BIRTH_DATE).Width = GRID_WIDTH_DATE_SHORT
      .Column(GRID_COLUMN_HOLDER_BIRTH_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Occupation
      .Column(GRID_COLUMN_HOLDER_OCCUPATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6862)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2058)
      .Column(GRID_COLUMN_HOLDER_OCCUPATION).Width = GRID_WIDTH_HOLDER_OCCUPATION_DESCRIPTION
      .Column(GRID_COLUMN_HOLDER_OCCUPATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2107)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account Holder Name
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1567)
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

#End Region ' Private Methods

#Region " Events "

  ''' <summary>
  ''' Event changed check show all
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_show_all_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_all.CheckedChanged
    Call FillComboUsers(chk_show_all.Checked)
  End Sub ' chk_show_all_CheckedChanged

#End Region ' Events

End Class
