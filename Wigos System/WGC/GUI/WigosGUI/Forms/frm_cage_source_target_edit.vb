'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_CAGE_source_target.vb
'
' DESCRIPTION : Cage Amounts.TEXT
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-DEC-2013  JCA    Initial version   
' 11-DEC-2013  XMS    Initial Source Code.
' 13-JAN-2014  CCG    Fixed Bug WIG-520
' 24-JAN-2014  AMF    Fixed Bug WIGOSTITO-988
' 16-SEP-2014  OPC    Not allowed to edit/delete an Id less than 1000 (System).
' 02-OCT-2014  LRS    Show msgbox when don't have row selected
' 14-APR-2015  YNM    Fixed Bug TFS-1103: Creation of origins and destinations was limited to 30 entries.
' 21-MAR-2016  ETP    Fixed Bug 1134: Priority changed when validating data.
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl

Imports GUI_Reports.PrintDataset
Imports GUI_Controls.frm_base_sel

Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text

#End Region           ' Imports

Public Class frm_cage_source_target_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CST_NAME As Integer = 1
  Private Const GRID_COLUMN_CST_SOURCE As Integer = 2
  Private Const GRID_COLUMN_CST_TARGET As Integer = 3
  Private Const GRID_COLUMN_CST_EDITABLE As Integer = 4
  Private Const GRID_COLUMN_CST_TYPE As Integer = 5
  Private Const GRID_COLUMN_CST_ID As Integer = 6

  Private Const GRID_COLUMNS_CST As Integer = 7
  Private Const GRID_HEADER_ROWS_CST As Integer = 1

  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_CST_NAME As Integer = 4200
  Private Const GRID_WIDTH_CST_SOURCE As Integer = 1100
  Private Const GRID_WIDTH_CST_TARGET As Integer = 1100

  Private Const MIN_CURRENCIES_ALLOWED As Integer = 1
  Private Const MARK_DELETED As Integer = 8

  Private Const MIN_SYSTEM_ID As Integer = 1000

#End Region       ' Constants

#Region " Members "

  ' For return currencies selected 
  Private CurrenciesSelected() As Integer
  Private m_currency_selected As String = ""
  Private m_currency_allowed As Boolean = False
  Private m_dt_restricted As DataTable
  Private m_error_show As Boolean = False
  Private m_init_controls As Boolean = False
  Private m_selected_grid As Boolean = False
  Private m_selected_grid_bills As Boolean = False

  ' OPC
  Private m_close_form As Boolean = True

#End Region         ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CAGE_SOURCE_TARGET

    Call MyBase.GUI_SetFormId()

  End Sub               ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3376)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)

    ' Save Button
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    ' Excel button
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False 'True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = Me.Permissions.Write

    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)
    Me.btn_concepts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5472)
    Me.btn_relation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5505)

    If Not Cage.IsCageEnabled Then
      Me.btn_concepts.Visible = False
      Me.btn_relation.Visible = False
    End If

    m_init_controls = True
    ''' Player Tracking
    Call GUI_StyleSheet()
    m_init_controls = False

  End Sub         ' GUI_InitControls

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL)
  End Sub      ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_GetScreenData()

    Dim CAGE_source_target_config_edit As cls_cage_source_target
    Dim CAGE_source_target_config_read As cls_cage_source_target

    Try

      CAGE_source_target_config_edit = Me.DbEditedObject
      CAGE_source_target_config_read = Me.DbReadObject

      GetScreenSourceTarget(CAGE_source_target_config_edit.DataCageSourceTarget)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    Finally

    End Try

  End Sub        ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim source_sourcetarget As cls_cage_source_target

    Try
      source_sourcetarget = Me.DbReadObject

      SetScreenSourceTarget(source_sourcetarget.DataCageSourceTarget)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub        ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim source_target_data As cls_cage_source_target
    ' add here new currency class objects, once per control

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New cls_cage_source_target
        source_target_data = DbEditedObject
        ' add here new currency data, once per control

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub         ' GUI_DB_Operation

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _count As Integer = 0
    Dim _cst_source_checked As Boolean
    Dim _cst_target_checked As Boolean
    Dim _dg_cell As String

    _cst_source_checked = False
    _cst_target_checked = False

    Try

      'Fixed bug 1134 priority changed.
      For _idx_row As Integer = 0 To dg_list.NumRows - 1

        If String.IsNullOrEmpty(dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value.ToString) Then

          Call dg_list.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4341), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , dg_list.Column(GRID_COLUMN_CST_NAME).Header.Text)

          Return False
        End If

      Next

      'Check if the new rows have the column name empty or equal then other existing row.
      For _idx_row As Integer = 0 To dg_list.NumRows - 1

        If dg_list.Cell(_idx_row, GRID_COLUMN_CST_EDITABLE).Value.ToString = "2" Then
          _dg_cell = dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value

          For _idx_row2 As Integer = 0 To dg_list.NumRows - 1

            If dg_list.Cell(_idx_row2, GRID_COLUMN_CST_NAME).Value = _dg_cell AndAlso _idx_row2 <> _idx_row Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4459), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _dg_cell)
              Call dg_list.Focus()

              Return False
            End If

          Next
        End If
      Next


    Catch ex As Exception

    End Try

    Return True

  End Function  ' GUI_IsScreenDataOk

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If dg_list.ContainsFocus Then
          dg_list.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function        ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    'If ButtonId = ENUM_BUTTON.BUTTON_CUSTOM_0 Then

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        m_close_form = True
        ApplyButton()

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        'Excel button
        Call ExcelPrintDetails(False, False)
        Call MyBase.AuditFormClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        ' Save Button
        m_close_form = False
        ApplyButton()
        'call mybase.GUI_SetScreenData(

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub          ' GUI_ButtonClick

#End Region         ' Overrides

#Region "Private Functions"

  ' PURPOSE: Control the editables elements of dg_list 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row: number of the selected row
  '           - Column: number of the selected column
  '           - EditionCanceled: boolean to cancel the edition
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_list_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_list.BeforeStartEditionEvent

    If Not String.IsNullOrEmpty(dg_list.Cell(Row, GRID_COLUMN_CST_ID).Value) Then
      Dim _int_id As Integer = GUI_ParseNumber(dg_list.Cell(Row, GRID_COLUMN_CST_ID).Value)

      If _int_id < MIN_SYSTEM_ID AndAlso Column <> GRID_COLUMN_CST_NAME Then
        EditionCanceled = True
      End If

    End If

  End Sub

  Private Sub ApplyButton()

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to change the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    If Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

      ' Save data when some option has been changed, added or deleted in the form
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      End If
      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
      End If

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      End If

    End If

    ' LJM 02-DEC-2013 Necesary to set this property to false, or when closing the form
    ' an exception is raised on DiscardChanges()
    CheckChanges = False

    If m_close_form Then
      Call GUI_Exit()
    Else
      Call RefreshGrid()
    End If

  End Sub              ' ApplyButton

  ' PURPOSE: Refresh the grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshGrid()
    Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    Call GUI_SetScreenData(0)
  End Sub

  ' PURPOSE: Define all Grid Points Multiplier Per currency Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.dg_list
      Call .Init(GRID_COLUMNS_CST, GRID_HEADER_ROWS_CST, True)

      Me.dg_list.SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Name
      .Column(GRID_COLUMN_CST_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)
      .Column(GRID_COLUMN_CST_NAME).Width = GRID_WIDTH_CST_NAME
      .Column(GRID_COLUMN_CST_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CST_NAME).Editable = True
      .Column(GRID_COLUMN_CST_NAME).IsMerged = False
      .Column(GRID_COLUMN_CST_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_CST_NAME).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_PROVIDERS_NAME, 50)

      ' Source
      .Column(GRID_COLUMN_CST_SOURCE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427)
      .Column(GRID_COLUMN_CST_SOURCE).Width = GRID_WIDTH_CST_SOURCE
      .Column(GRID_COLUMN_CST_SOURCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CST_SOURCE).Editable = True
      .Column(GRID_COLUMN_CST_SOURCE).IsMerged = False
      .Column(GRID_COLUMN_CST_SOURCE).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Target
      .Column(GRID_COLUMN_CST_TARGET).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3396)
      .Column(GRID_COLUMN_CST_TARGET).Width = GRID_WIDTH_CST_SOURCE
      .Column(GRID_COLUMN_CST_TARGET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CST_TARGET).Editable = True
      .Column(GRID_COLUMN_CST_TARGET).IsMerged = False
      .Column(GRID_COLUMN_CST_TARGET).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Editable
      .Column(GRID_COLUMN_CST_EDITABLE).Width = 0

      ' Type
      .Column(GRID_COLUMN_CST_TYPE).Width = 0

      ' Id
      .Column(GRID_COLUMN_CST_ID).Width = 0

    End With

  End Sub           ' GUI_StyleSheet

  ' PURPOSE: Update the currencies DataTable with the allowed value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencies As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenSourceTarget(ByVal Currencies As DataTable)

    Dim _source_target() As DataRow
    Dim _source_target_aux() As DataRow
    Dim _name As String
    Dim _idx As String

    If Not Me.dg_list Is Nothing AndAlso Not Me.dg_list.IsDisposed = True AndAlso Me.dg_list.NumRows > 0 Then
      For _idx_row As Integer = 0 To Me.dg_list.NumRows - 1

        _name = Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value
        _idx = Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value

        If String.IsNullOrEmpty(_idx) Then
          _source_target = Currencies.Select("CST_SOURCE_TARGET_NAME = '' ")
        Else
          _source_target = Currencies.Select("CST_SOURCE_TARGET_ID = '" & _idx & "' ")
        End If

        If _source_target.Length = 0 Then
          Continue For
        End If

        If _source_target(0)(cls_cage_source_target.SQL_COLUMN_CST_NAME) <> Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value Then
          _source_target_aux = Currencies.Select("CST_SOURCE_TARGET_NAME = '" & _name & "' ")
          If _source_target_aux.Length = 1 Then
            Continue For
          End If
          _source_target(0)(cls_cage_source_target.SQL_COLUMN_CST_NAME) = Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value
        End If

        If _source_target(0)(cls_cage_source_target.SQL_COLUMN_CST_SOURCE) _
           <> GridValueToBool(Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value.ToString()) Then

          _source_target(0)(cls_cage_source_target.SQL_COLUMN_CST_SOURCE) = GridValueToBool(Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value)
        End If

        If _source_target(0)(cls_cage_source_target.SQL_COLUMN_CST_TARGET) _
           <> GridValueToBool(Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value.ToString()) Then

          _source_target(0)(cls_cage_source_target.SQL_COLUMN_CST_TARGET) = GridValueToBool(Me.dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value)
        End If

      Next
    End If

  End Sub      ' GetScreenSourceTarget

  ' PURPOSE: Set the uc_grid s from the screen data with the values from the currencys DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenSourceTarget(ByVal source_target As DataTable)
    Dim _idx_row As Integer
    Dim _idx_row_selected As Integer = -1
    Dim _currencies_accepted_GP As String
    Dim _general_params As WSI.Common.GeneralParam.Dictionary


    dg_list.Redraw = False
    dg_list.Clear()
    dg_list.Redraw = True
    dg_list.Redraw = False

    _general_params = WSI.Common.GeneralParam.Dictionary.Create()

    _currencies_accepted_GP = _general_params.GetString("RegionalOptions", "CurrenciesAccepted")

    Try

      For Each _source_target As DataRow In source_target.Rows

        dg_list.AddRow()
        _idx_row = dg_list.NumRows - 1

        dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value = _source_target(cls_cage_source_target.SQL_COLUMN_CST_NAME)

        'Id
        If IsDBNull(_source_target(cls_cage_source_target.SQL_COLUMN_CST_ID)) Then
          dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value = ""
        Else
          dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value = _source_target(cls_cage_source_target.SQL_COLUMN_CST_ID)
        End If

        'Name
        If IsDBNull(_source_target(cls_cage_source_target.SQL_COLUMN_CST_NAME)) Then
          dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value = ""
        Else
          dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value = _source_target(cls_cage_source_target.SQL_COLUMN_CST_NAME)
        End If

        'Source
        If _source_target(cls_cage_source_target.SQL_COLUMN_CST_SOURCE) Then
          If (GUI_ParseNumber(dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value) < MIN_SYSTEM_ID) Then          ' SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          Else                                                                                        ' NOT SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value = uc_grid.GRID_CHK_CHECKED
          End If
        Else
          If (GUI_ParseNumber(dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value) < MIN_SYSTEM_ID) Then          ' SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          Else                                                                                        ' NOT SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value = uc_grid.GRID_CHK_UNCHECKED
          End If
        End If

        'Target
        If _source_target(cls_cage_source_target.SQL_COLUMN_CST_TARGET) Then
          If (GUI_ParseNumber(dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value) < MIN_SYSTEM_ID) Then          ' SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          Else                                                                                        ' NOT SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value = uc_grid.GRID_CHK_CHECKED
          End If
        Else
          If (GUI_ParseNumber(dg_list.Cell(_idx_row, GRID_COLUMN_CST_ID).Value) < MIN_SYSTEM_ID) Then          ' SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          Else                                                                                        ' NOT SYSTEM
            dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value = uc_grid.GRID_CHK_UNCHECKED
          End If
        End If

      Next

    Finally

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (Me.dg_list.NumRows > 0)
      dg_list.Redraw = True

    End Try

  End Sub      ' SetScreenSourceTarget

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function     ' BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function     ' GridValueToBool

  ' PURPOSE:  get the true or false
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetValueToBool(ByVal cell As uc_grid.CLASS_CELL_DATA) As Boolean
    Dim _return As Boolean

    Select Case cell.Value

      Case uc_grid.GRID_CHK_CHECKED
        _return = True

      Case uc_grid.GRID_CHK_UNCHECKED
        _return = False

    End Select

    Return _return

  End Function     ' GridValueToBool

  Private Sub InsertSourceTargetRow()
    Dim _idx_row As Integer
    Dim CAGE_source_target_config_edit As cls_cage_source_target
    Dim _array_value(3) As String

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'Deselected Rows
    For _idx_row = 0 To dg_list.NumRows - 1
      dg_list.IsSelected(_idx_row) = False
    Next

    dg_list.AddRow()
    _idx_row = dg_list.NumRows - 1

    dg_list.IsSelected(_idx_row) = True
    dg_list.Redraw = True
    dg_list.TopRow = _idx_row

    dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value = ""
    dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_list.Cell(_idx_row, GRID_COLUMN_CST_EDITABLE).Value = 2
    dg_list.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    _array_value(1) = dg_list.Cell(_idx_row, GRID_COLUMN_CST_NAME).Value
    _array_value(2) = GetValueToBool(dg_list.Cell(_idx_row, GRID_COLUMN_CST_SOURCE))
    _array_value(3) = GetValueToBool(dg_list.Cell(_idx_row, GRID_COLUMN_CST_TARGET))

    CAGE_source_target_config_edit = Me.DbEditedObject
    CAGE_source_target_config_edit.DataCageSourceTarget.Rows.Add(_array_value)

    Call GUI_GetScreenData()

  End Sub            ' InsertSourceTargetRow

  Private Sub DeleteSelectedRows(ByVal Grid As GUI_Controls.uc_grid)

    Dim _source_target_config_edit As cls_cage_source_target
    Dim _CST_cage() As DataRow
    Dim _selected_row As Integer
    Dim _hasMove As Boolean

    Erase CurrenciesSelected
    CurrenciesSelected = Grid.SelectedRows

    If IsNothing(CurrenciesSelected) Then
      Exit Sub
    End If

    If Grid.Cell(CurrenciesSelected(0), GRID_COLUMN_CST_ID).Value < MIN_SYSTEM_ID Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5466), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                      , _
                      Grid.Cell(CurrenciesSelected(0), GRID_COLUMN_CST_NAME).Value)
      Exit Sub
    End If

    _source_target_config_edit = Me.DbEditedObject

    If Grid.Cell(CurrenciesSelected(0), GRID_COLUMN_CST_EDITABLE).Value <> "2" Then
      GetScreenSourceTarget(_source_target_config_edit.DataCageSourceTarget)
    End If

    _selected_row = Grid.SelectedRows(0)

    If Not String.IsNullOrEmpty(Grid.Cell(_selected_row, GRID_COLUMN_CST_ID).Value) Then
      _CST_cage = _source_target_config_edit.DataCageSourceTarget.Select("    CST_SOURCE_TARGET_ID = '" & Grid.Cell(_selected_row, GRID_COLUMN_CST_ID).Value & "' ")
      If _CST_cage.Length >= 1 Then
        If _source_target_config_edit.HasMovements(_CST_cage(0)(0)) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4455), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          _hasMove = True
        Else
          _hasMove = False
        End If
      End If
    Else
      _CST_cage = _source_target_config_edit.DataCageSourceTarget.Select(" CST_SOURCE_TARGET_NAME = '" & Grid.Cell(_selected_row, GRID_COLUMN_CST_NAME).Value & _
                                                                         " ' AND CST_SOURCE_TARGET_ID IS NULL")
      If _CST_cage.Length = 0 Then
        _CST_cage = _source_target_config_edit.DataCageSourceTarget.Select(" CST_SOURCE_TARGET_NAME = ''")
      End If
      _hasMove = False
    End If

    If Not _hasMove Then

      Call _CST_cage(0).Delete()
      Grid.IsSelected(_selected_row) = False
      Grid.DeleteRowFast(_selected_row)

    End If

  End Sub

  Private Function CheckDeleteRow(ByVal Cell_Status As String) As Boolean
    Dim _return As Boolean

    _return = False

    If Not String.IsNullOrEmpty(Cell_Status) AndAlso Cell_Status = "2" Then
      _return = True
    End If

    Return _return

  End Function            ' CheckDeleteRow

  Private Sub ReadData()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

  End Sub                 ' ReadData

  ' PURPOSE: Check exist values "0" in denominations
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Bolean
  '
  ' RETURNS:
  '     - Boolean. True: there is values 0. False: not values 0.

  Private Function ExistNullValues() As Boolean
    Dim _currency_data As cls_cage_amounts
    Dim _currencies As DataTable
    Dim _da As DataRow()
    Dim _return As Boolean

    _return = False

    Try

      _currency_data = Me.DbEditedObject
      _currencies = _currency_data.DataCageAmountsDenomination
      _da = _currencies.Select(" DENOMINATION =  0 ")

      If _da.Length > 0 Then
        _return = True
      End If

    Catch ex As Exception

      ' 137 "Exception error has been found: \n%1"
    Finally

    End Try

    Return _return

  End Function      'ExistNullValues 

  ' PURPOSE: Print report with Table Details
  '
  '  PARAMS:
  '     - INPUT:
  '       - Title: report tittle
  '       - Grid: source grid
  '
  '     - OUTPUT:
  '
  Private Sub ExcelPrintDetails(Optional ByVal ToPrint As Boolean = False, Optional ByVal WithPreview As Boolean = False)
    Dim _print_excel_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid As ExcelReportLayout.SheetLayout.UcGridDefinition
    Dim _cage_source_control As cls_cage_source_target
    Dim _addYesOrNot As Boolean = True

    '_new_collection = DbReadObject
    _cage_source_control = Me.DbEditedObject

    'GUI_FormatDate(PrintDateTime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _print_excel_data = New GUI_Reports.CLASS_PRINT_DATA()
    _report_layout = New ExcelReportLayout()

    Try
      _sheet_layout = New ExcelReportLayout.SheetLayout()
      _sheet_layout.Title = Me.FormTitle
      _sheet_layout.Name = Me.Text

      With _report_layout
        .PrintData = _print_excel_data
        .PrintDateTime = frm_base_sel.GUI_GetPrintDateTime()

        If dg_list.NumRows > 0 Then
          _uc_grid = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid.UcGrid = Me.dg_list

          _sheet_layout.ListOfUcGrids.Add(_uc_grid)
        End If

        Call AddYesorNot(_sheet_layout.ListOfUcGrids(0))

        _addYesOrNot = True

        .ListOfSheets.Add(_sheet_layout)
      End With

      Call _report_layout.GUI_GenerateExcel()
      Call RemoveYesorNot(_sheet_layout.ListOfUcGrids(0))
    Catch

    End Try





  End Sub ' PrintDetails

  ' PURPOSE: changes 0 or 1 for "Yes" or "Not"
  '
  '  PARAMS:
  '     - INPUT:
  '       - Collection: uc_grid
  '
  '     - OUTPUT:
  '
  Private Sub AddYesorNot(ByVal uc_grid_cst As ExcelReportLayout.SheetLayout.UcGridDefinition)

    For _idx As Integer = 0 To dg_list.NumRows - 1
      uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_SOURCE).Value = IIf(uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_SOURCE).Value = 1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(279), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
      uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_TARGET).Value = IIf(uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_TARGET).Value = 1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(279), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    Next

  End Sub 'AddTotalizator

  ' PURPOSE: changes "Yes" or "Not" for 0 or 1
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub RemoveYesorNot(ByVal uc_grid_cst As ExcelReportLayout.SheetLayout.UcGridDefinition)

    For _idx As Integer = 0 To dg_list.NumRows - 1
      uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_SOURCE).Value = IIf(uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_SOURCE).Value.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), uc_grid.GRID_CHK_UNCHECKED, uc_grid.GRID_CHK_CHECKED)
      uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_TARGET).Value = IIf(uc_grid_cst.UcGrid.Cell(_idx, GRID_COLUMN_CST_TARGET).Value.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)), uc_grid.GRID_CHK_UNCHECKED, uc_grid.GRID_CHK_CHECKED)
    Next

  End Sub 'RemoveTotalizator

#End Region ' Private Functions

#Region "Events"

  Private Sub btn_relation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_relation.Click

    If Not Me.dg_list.SelectedRows Is Nothing AndAlso Me.dg_list.SelectedRows.Length > 0 Then

      If Not String.IsNullOrEmpty(Me.dg_list.Cell(Me.dg_list.SelectedRows(0), GRID_COLUMN_CST_ID).Value) Then

        If Me.dg_list.Cell(Me.dg_list.SelectedRows(0), GRID_COLUMN_CST_ID).Value <> 0 Then

          Windows.Forms.Cursor.Current = Cursors.WaitCursor

          Dim _frm As frm_cage_concepts_relation

          _frm = New frm_cage_concepts_relation
          _frm.ShowEditItem(Me.dg_list.Cell(Me.dg_list.SelectedRows(0), GRID_COLUMN_CST_ID).Value, 0)

          Windows.Forms.Cursor.Current = Cursors.Default

        Else

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5519), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

        End If

      Else

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5575), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      End If
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5607), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
    End If


  End Sub ' btn_relation_Click

  Private Sub btn_concepts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_concepts.Click
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Dim _frm As frm_cage_source_target_concepts

    _frm = New frm_cage_source_target_concepts()
    _frm.ShowNew()

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub ' btn_concepts_ClickEvent

  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent

    InsertSourceTargetRow()

  End Sub ' btn_add_ClickEvent

  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
    Dim _row_with_movements As Boolean
    Dim _row_selected As Integer
    Dim _valor As Integer
    Dim _cage_source_control As cls_cage_source_target
    Dim _dataTable_cage_movements As DataTable

    _cage_source_control = Me.DbEditedObject
    _row_with_movements = False

    Try
      If Not Permissions.Delete Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Exit Sub
      End If

      If Not Me.dg_list.SelectedRows Is Nothing AndAlso Me.dg_list.SelectedRows.Length > 0 Then

        _row_selected = dg_list.SelectedRows(0)

        If Not String.IsNullOrEmpty(dg_list.Cell(_row_selected, GRID_COLUMN_CST_ID).Value) Then
          _valor = dg_list.Cell(_row_selected, GRID_COLUMN_CST_ID).Value
          _dataTable_cage_movements = _cage_source_control.IsMovementsSourceTarget(_valor)

          If _dataTable_cage_movements.Select().Length >= 1 Then _row_with_movements = True
        Else
          dg_list.DeleteRow(_row_selected)
          _cage_source_control.DataCageSourceTarget.Rows.RemoveAt(_row_selected)
          Exit Sub
        End If

        If Not _row_with_movements Then

          If cls_cage_source_target.InUse(_valor) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5508), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
            Exit Sub
          End If

          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5504), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        , _
                        dg_list.Cell(_row_selected, GRID_COLUMN_CST_NAME).Value) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Exit Sub
          End If

          DeleteSelectedRows(Me.dg_list)

          If Me.dg_list.NumRows > 0 Then
            For x As Integer = 0 To dg_list.NumRows - 1
              Me.dg_list.IsSelected(x) = False
            Next x
            Me.dg_list.IsSelected(0) = True
          End If
        End If
      Else
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5607), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' 07-OCT-2014 SMN
      End If

    Catch ex As Exception
    End Try

  End Sub ' btn_del_ClickEvent

#End Region            ' Events

End Class