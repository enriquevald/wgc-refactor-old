'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_accounts_import
'
' DESCRIPTION : Allows to record magnetic cards.
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-JUL-2012  RXM    Initial version
' 02-MAY-2013  RBG    Change Request #756: Register import on audit 
' 02-MAY-2013  RBG    Fixed Bug #761: Exception on stop import process
' 19-FEB-2016  FGB    Changed all AC_POINTS (table Accounts) references to CBU_VALUE (table CUSTOMER_BUCKET)
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices
Imports WSI.Common

#End Region

Public Class frm_accounts_import
  Inherits frm_base_edit
#Region " Constants "

  Private Const MODULE_NAME As String = "frm_accounts_import"
  Private Const BATCH_UPATE_SIZE As Int32 = 300
#End Region

#Region " Structures "

#End Region

#Region " Members "
  Private m_accounts_import As AccountsImport
  Private m_thread_import As Thread
  Private models_combo_filled As Boolean
  Private m_log_file As String
  Private m_permission_client_data As CLASS_GUI_USER.TYPE_PERMISSIONS
  Private m_no_timeout_timer As System.Timers.Timer
  Private m_state As WSI.Common.AccountsImport.IMPORT_STATES


#End Region

#Region " Delegates "
  Private Delegate Sub SetTextCallback(ByVal e As WSI.Common.AccountImportEventArgs)
  Private Delegate Function ShowErrorLog(ByVal ErrorsDataTable As DataTable, ByVal Message As String, ByVal ButtonOKText As String, ByVal ButtonCancelText As String) As Boolean
  Private Delegate Sub SetStopButtonDelegate(ByVal Enabled As Boolean)



#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNTS_IMPORT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _permission_balance As CLASS_GUI_USER.TYPE_PERMISSIONS
    Dim _permission_level As CLASS_GUI_USER.TYPE_PERMISSIONS
    Dim _permission_points As CLASS_GUI_USER.TYPE_PERMISSIONS

    Call MyBase.GUI_InitControls()

    ' Form title
    Text = GLB_NLS_GUI_CLASS_II.GetString(410)

    ' - Buttons

    '   - Stop
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CLASS_II.GetString(427) ' stop
    '   - Start
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CLASS_II.GetString(426) ' start
    '   - Exit
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CLASS_II.GetString(3) ' start
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False


    ' Site input data
    ef_file_path.Text = GLB_NLS_GUI_CLASS_II.GetString(428)
    ef_file_path.Value = ""

    gb_new_accounts.Text = GLB_NLS_GUI_CLASS_II.GetString(413)
    gb_balacence.Text = GLB_NLS_GUI_CLASS_II.GetString(412)
    gb_reference.Text = GLB_NLS_GUI_CLASS_II.GetString(411)
    gb_level.Text = GLB_NLS_GUI_CLASS_II.GetString(414)
    gb_points.Text = GLB_NLS_GUI_CLASS_II.GetString(415)
    '  gb_client_data.Text = GLB_NLS_GUI_CLASS_II.GetString(416)
    gb_output.Text = GLB_NLS_GUI_CLASS_II.GetString(425)

    rb_external_reference.Text = GLB_NLS_GUI_CLASS_II.GetString(417)
    rb_account_id.Text = GLB_NLS_GUI_CLASS_II.GetString(418)

    chk_add_accounts.Text = GLB_NLS_GUI_CLASS_II.GetString(419)
    chk_update_clients.Text = GLB_NLS_GUI_CLASS_II.GetString(420)
    chk_update_level.Text = GLB_NLS_GUI_CLASS_II.GetString(421)
    chk_update_points.Text = GLB_NLS_GUI_CLASS_II.GetString(422)
    chk_reset_points.Text = GLB_NLS_GUI_CLASS_II.GetString(423)
    chk_reset_balances.Text = GLB_NLS_GUI_CLASS_II.GetString(423)
    chk_update_balance.Text = GLB_NLS_GUI_CLASS_II.GetString(424)

    _permission_balance = CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNTS_IMPORT_BALANCES)
    _permission_level = CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNTS_IMPORT_LEVEL)
    _permission_points = CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNTS_IMPORT_POINTS)
    m_permission_client_data = CurrentUser.Permissions(ENUM_FORM.FORM_ACCOUNTS_IMPORT_CLIENT_DATA)

    chk_update_balance.Enabled = _permission_balance.Write
    chk_reset_balances.Enabled = _permission_balance.Delete
    chk_add_accounts.Enabled = m_permission_client_data.Write
    chk_update_clients.Enabled = m_permission_client_data.Delete
    chk_update_level.Enabled = _permission_level.Delete
    chk_update_points.Enabled = _permission_points.Write
    chk_reset_points.Enabled = _permission_points.Delete


    tb_output.Enabled = True
    tb_output.ReadOnly = True

    rb_external_reference.Checked = True

    If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      chk_reset_balances.Checked = False
      chk_update_balance.Checked = False

      chk_reset_balances.Enabled = False
      chk_update_balance.Enabled = False
    End If

  End Sub 'GUI_InitControls

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT :
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        Call StartAccountsImport()

      Case ENUM_BUTTON.BUTTON_CANCEL
        If m_thread_import Is Nothing Then
          Me.Close()
        ElseIf (m_thread_import.ThreadState = ThreadState.Stopped) Then
          Me.Close()
        End If

      Case ENUM_BUTTON.BUTTON_DELETE
        Call StopAccountsImport()

        'Case ENUM_BUTTON.BUTTON_CUSTOM_0
        'Call PauseAccountsImport()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ

      Case ENUM_DB_OPERATION.DB_OPERATION_READ
        ' Read Operations:

        ' Initialize variables


      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

#End Region

#Region " Public Functions "

  ' PURPOSE: Constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    models_combo_filled = False

  End Sub

  ' PURPOSE: Form entry point from menu selection.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem

#End Region

#Region " Private Functions "

  ' PURPOSE: Audit import action
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RegisterAudit()
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _file_name As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207), GLB_NLS_GUI_CLASS_II.GetString(410))

    _file_name = System.IO.Path.GetFileName(ef_file_path.Value)
    _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(550), _file_name)

    If rb_external_reference.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(411), GLB_NLS_GUI_CLASS_II.GetString(417))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(411), GLB_NLS_GUI_CLASS_II.GetString(418))
    End If

    If chk_add_accounts.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(419), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(419), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    If chk_update_clients.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(420), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(420), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    If chk_update_level.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(421), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(421), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    If chk_reset_balances.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(551), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(551), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    If chk_update_balance.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(552), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(552), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    If chk_reset_points.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(553), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(553), GLB_NLS_GUI_INVOICING.GetString(480))
    End If

    If chk_update_points.Checked Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(554), GLB_NLS_GUI_INVOICING.GetString(479))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(554), GLB_NLS_GUI_INVOICING.GetString(480))
    End If


    _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                 GLB_CurrentUser.Id, _
                 GLB_CurrentUser.Name, _
                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                 0)

  End Sub ' RegisterAudit

  ' PURPOSE: Calls threadstart for AccountsImportWork
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub StartAccountsImport()

    If (String.IsNullOrEmpty(ef_file_path.Value) Or Not File.Exists(ef_file_path.Value)) Then

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(160), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return

    End If

    If m_thread_import Is Nothing Then
      m_thread_import = New Thread(AddressOf AccountsImportThread)
      m_no_timeout_timer = New System.Timers.Timer(New TimeSpan(0, 1, 0).TotalMilliseconds)
      AddHandler m_no_timeout_timer.Elapsed, AddressOf m_no_timeout_timer_tick
      Me.tb_output.Text = ""
      m_no_timeout_timer.Start()
      m_thread_import.Start()
      GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True

    ElseIf (m_thread_import.ThreadState = ThreadState.Stopped) Then
      m_thread_import = New Thread(AddressOf AccountsImportThread)
      m_no_timeout_timer = New System.Timers.Timer(New TimeSpan(0, 1, 0).TotalMilliseconds)
      AddHandler m_no_timeout_timer.Elapsed, AddressOf m_no_timeout_timer_tick
      Me.tb_output.Text = ""
      m_no_timeout_timer.Start()
      m_thread_import.Start()
      GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True

    Else
    End If

  End Sub


  ' PURPOSE: avoid automatic session close 
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Shared Sub m_no_timeout_timer_tick(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs)
    WSI.Common.Users.SetLastAction("InProgress", Nothing)

  End Sub


  ' PURPOSE: calls stop on m_thread_import (AccountsImportWork)
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub StopAccountsImport()
    If Not m_thread_import Is Nothing Then
      If (m_thread_import.ThreadState <> ThreadState.Stopped) Then
        m_thread_import.Abort()
        pgb_import_progress.Value = 0
        tb_output.Text += vbCrLf + GLB_NLS_GUI_CLASS_II.GetString(431)
        Call m_accounts_import.WriteLog(vbCrLf + GLB_NLS_GUI_CLASS_II.GetString(431))
        tb_output.SelectionStart = tb_output.TextLength
        tb_output.ScrollToCaret()
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        m_no_timeout_timer.Stop()
      End If
    End If
  End Sub


  ' PURPOSE: use wsi.commond.accountsimport functions to import accounts
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AccountsImportThread()
    Dim _ds_accounts As DataSet
    Dim _stop_button_callback As New SetStopButtonDelegate(AddressOf SetStopButton)

    Try

      m_log_file = ef_file_path.Value + WGDB.Now.ToString("yyyy.MM.dd") + ".txt"

      m_accounts_import = New AccountsImport(m_log_file)

      AddHandler m_accounts_import.OnRowUpdated, AddressOf UpdateProcess

      _ds_accounts = New DataSet()

      If (Not m_accounts_import.ExcelGetAccounts(ef_file_path.Value, _ds_accounts)) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1586), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If


      If (_ds_accounts.Tables("duplicate").Rows.Count = 0) Then

        Dim _ask_for_continue_callback As New ShowErrorLog(AddressOf ShowBrowserMessage)
        Dim _correct_accounts_count As Integer
        Dim _message As String
        Dim _txt_ok As String
        Dim _txt_cancel As String
        Dim _there_are_errors As Boolean
        Dim _continue_with_errors As Boolean

        _txt_ok = GLB_NLS_GUI_CONTROLS.GetString(3)
        _txt_cancel = GLB_NLS_GUI_CONTROLS.GetString(4)
        _message = ""
        _correct_accounts_count = 0

        For Each _account As DataRow In _ds_accounts.Tables("accounts").Rows
          If _account.RowState = DataRowState.Added Then
            _correct_accounts_count += 1
            Exit For
          End If
        Next

        If (_correct_accounts_count = 0) Then
          _message = GLB_NLS_GUI_CLASS_II.GetString(433)
          _txt_ok = ""
          _txt_cancel = GLB_NLS_GUI_CONTROLS.GetString(1)
        End If

        _there_are_errors = _ds_accounts.Tables("errors").Rows.Count > 0

        If (_there_are_errors) Then
          _continue_with_errors = Me.Invoke(_ask_for_continue_callback, _ds_accounts.Tables("errors"), _message, _txt_ok, _txt_cancel)
        End If

        If (Not _there_are_errors Or _continue_with_errors) Then

          If (Not m_accounts_import.AddAccounts(_ds_accounts.Tables("accounts"), _
                                                BATCH_UPATE_SIZE, rb_external_reference.Checked, _
                                                chk_add_accounts.Checked, chk_update_clients.Checked, _
                                                chk_update_level.Checked, _
                                                chk_reset_balances.Checked, chk_update_balance.Checked, _
                                                chk_reset_points.Checked, chk_update_points.Checked, _
                                                CurrentUser.Name + "@" + Environment.MachineName)) Then

            ' error en Add_Accounts
          Else
            Call RegisterAudit()

          End If

        Else
          'don't continue
        End If
      Else
        Dim _callback As New ShowErrorLog(AddressOf ShowBrowserMessage)

        Me.Invoke(_callback, _ds_accounts.Tables("duplicate"), "", "", "")
        ' duplicated reference on document don't continue
      End If


      Me.Invoke(_stop_button_callback, False)

    Catch ex As Exception

      Me.Invoke(_stop_button_callback, False)

    End Try

  End Sub

  ' PURPOSE: handle m_accounts_import.OnRowUpdated event
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub UpdateProcess(ByVal Sender As Object, ByVal UpdateEventArgs As WSI.Common.AccountImportEventArgs)
    AccountsImportStep(UpdateEventArgs)
  End Sub

  ' PURPOSE: called from m_thread_import thow a delegate, asks for continue and show errors
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function ShowBrowserMessage(ByVal ErrorsDataTable As DataTable, ByVal Message As String, ByVal ButtonOKText As String, ByVal ButtonCancelText As String) As Boolean
    Dim _message_text As String
    Dim _frm_dialog As frm_YesNo_web_browser
    Dim _str_errors As String
    _frm_dialog = New frm_YesNo_web_browser()
    _str_errors = ""

    If Message.Trim() = "" Then
      _message_text = GLB_NLS_GUI_CLASS_II.GetString(430)
    Else
      _message_text = Message
    End If


    ' show 200 errors
    If (ErrorsDataTable.Rows.Count > 200) Then
      For _shownerrors As Integer = 0 To 200
        _str_errors += GLB_NLS_GUI_CLASS_II.GetString(432) + ErrorsDataTable.Rows(_shownerrors)(0).ToString() + ": " + ErrorsDataTable.Rows(_shownerrors)(1).ToString().Replace("<", """").Replace(">", """") + "</br>"
      Next
      _str_errors += "" + "+ " + (ErrorsDataTable.Rows.Count - 200).ToString
    Else
      For Each _row As DataRow In ErrorsDataTable.Rows
        _str_errors += GLB_NLS_GUI_CLASS_II.GetString(432) + _row(0).ToString() + ": " + _row(1).ToString().Replace("<", """").Replace(">", """") + "</br>"
      Next
    End If

    If (_str_errors.Trim = "") Then
      _str_errors = GLB_NLS_GUI_CLASS_II.GetString(416)
      _frm_dialog.Init(_message_text, ButtonOKText, ButtonCancelText)
    Else
      _frm_dialog.Init(GLB_NLS_GUI_CLASS_II.GetString(429) + vbCrLf + m_log_file + vbCrLf + vbCrLf + _message_text, ButtonOKText, ButtonCancelText)
    End If
    Call m_accounts_import.WriteLog(_str_errors.Replace("</br>", vbCrLf))
    _frm_dialog.wb_browser.DocumentText = _str_errors

    '' dialog show
    If (chk_add_accounts.Checked Or chk_reset_balances.Checked Or chk_reset_points.Checked _
         Or chk_update_balance.Checked Or chk_update_clients.Checked Or chk_update_level.Checked _
         Or chk_update_points.Checked) Then

      If (_frm_dialog.ShowDialog() = Windows.Forms.DialogResult.Yes) Then
        Call m_accounts_import.WriteLog(_message_text + ButtonOKText)
        Return True
      Else '' show warning -- no action selected
        Call m_accounts_import.WriteLog(_message_text + ButtonCancelText)
        Return False
      End If

    Else

      '  _frm_dialog.Init( + vbCrLf + m_log_file + vbCrLf + vbCrLf + _message_text, ButtonOKText, ButtonCancelText)
      _frm_dialog.Init(GLB_NLS_GUI_CLASS_II.GetString(429) + vbCrLf + m_log_file + vbCrLf + vbCrLf + GLB_NLS_GUI_CONTROLS.GetString(161), "", "Aceptar")
      Call _frm_dialog.ShowDialog()
      'Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(161), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK) 'proceso de validacion finalizado
      Return True
    End If

  End Function

  ' PURPOSE: called from m_thread_import throw a delegate, set BUTTON_DELETE enabled/disabled
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetStopButton(ByVal Enabled As Boolean)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = Enabled
    If Not Enabled Then
      m_no_timeout_timer.Stop()
    End If

  End Sub

  ' PURPOSE: called from m_thread_import throw a delegate, set output text and progressbar progress
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AccountsImportStep(ByVal AccountImportEventArgs As WSI.Common.AccountImportEventArgs)

    Dim _callback As SetTextCallback
    Dim _progress As Double

    If (Me.tb_output.InvokeRequired) Then
      _callback = New SetTextCallback(AddressOf AccountsImportStep)
      Me.Invoke(_callback, New Object() {AccountImportEventArgs})
    Else

      _progress = 0
      tb_output.Text += AccountImportEventArgs.Message
      tb_output.SelectionStart = tb_output.TextLength
      tb_output.ScrollToCaret()



      Try
        _progress = ((CDbl(AccountImportEventArgs.Row_count) - CDbl(AccountImportEventArgs.Pending_rows)) / CDbl(AccountImportEventArgs.Row_count) * 100)
        Me.pgb_import_progress.Value = CInt(_progress)
      Catch ex As Exception

      End Try

      Me.m_state = AccountImportEventArgs.State

      If (m_state = AccountsImport.IMPORT_STATES.DocumentReaded) Then
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True


      End If
    End If

  End Sub

#End Region

  ' PURPOSE: opens a file dialog to chose the document to import
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_open_file_dialog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_open_file_dialog.Click
    Dim _file_dialog As OpenFileDialog
    _file_dialog = New OpenFileDialog()
    _file_dialog.Multiselect = False
    _file_dialog.Filter = "Excel Documents(*.XLS;*.XLSX)|*.XLS;*.XLSX|All files (*.*)|*.*"
    _file_dialog.ShowDialog()
    ef_file_path.Value = _file_dialog.FileName.ToString()

  End Sub

  Private Sub rb_external_reference_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_external_reference.CheckedChanged
    If rb_external_reference.Checked Then
      Me.chk_add_accounts.Enabled = m_permission_client_data.Write
    Else
      Me.chk_add_accounts.Enabled = False
      Me.chk_add_accounts.Checked = False
    End If

  End Sub

End Class
