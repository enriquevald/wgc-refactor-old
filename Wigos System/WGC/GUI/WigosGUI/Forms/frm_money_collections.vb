'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_money_collections
' DESCRIPTION:   List Money collected.
' AUTHOR:        Susana Sams�
' CREATION DATE: 27-MAR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-MAR-2012  SSC    Initial version.
' 16-MAY-2012  DDM    Modifications: - The Terminal column, Machine Filter and report type are not visible.
'                                    - Change the control: uc_dtp by uc_daily_session_selector.
'                                    - Change size of the form. 
'                                    - Literals of filter for excel.
' 17-MAY-2012  DDM    Modifications: - The Lost Column, undo button and filter of notes collected 
'                                      are not visible.
'                                    - Changed Log.Exception by Common_LoggerMsg
' 04-APR-2013  DMR    Fixed Bug #316: Changed to correct date format on functions CollectionSubtotalRow and GUI_SetupRow
' 02-APR-2014  ICS    Fixed Bug WIGOSTITO-1185: Bill collection control, displays information from other payment types
' 07-MAY-2014  DRV    Fixed Bug WIG-901: changed the row order
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports WSI.Common
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_money_collections
  Inherits frm_base_sel


#Region " Constants "
  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private GRID_COLUMN_COLLECTION_ID As Integer = 1
  Private GRID_COLUMN_DATETIME As Integer = 2
  Private GRID_COLUMN_USER_NAME As Integer = 3
  Private GRID_COLUMN_TERMINAL As Integer = 4
  Private GRID_COLUMN_FACE_VALUE As Integer = 5
  Private GRID_COLUMN_NUM_EXPECTED As Integer = 6
  Private GRID_COLUMN_SUM_EXPECTED As Integer = 7
  Private GRID_COLUMN_NUM_COLLECTED As Integer = 8
  Private GRID_COLUMN_SUM_COLLECTED As Integer = 9
  Private GRID_COLUMN_NUM_LOST As Integer = 10
  Private GRID_COLUMN_SUM_LOST As Integer = 11

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 12

  ' Sql Columns
  Private Const SQL_COLUMN_COLLECTION_ID As Integer = 0
  Private Const SQL_COLUMN_DATETIME As Integer = 1
  Private Const SQL_COLUMN_USER_NAME As Integer = 2
  Private Const SQL_COLUMN_TERMINAL As Integer = 3
  Private Const SQL_COLUMN_MONEY_LOST As Integer = 4
  Private Const SQL_COLUMN_FACE_VALUE As Integer = 5
  Private Const SQL_COLUMN_NUM_EXPECTED As Integer = 6
  Private Const SQL_COLUMN_SUM_EXPECTED As Integer = 7
  Private Const SQL_COLUMN_NUM_COLLECTED As Integer = 8
  Private Const SQL_COLUMN_SUM_COLLECTED As Integer = 9
  Private Const SQL_COLUMN_NUM_LOST As Integer = 10
  Private Const SQL_COLUMN_SUM_LOST As Integer = 11

#End Region ' Constants

#Region "Enums"
  Private Enum ENUM_REPORT_TYPE
    BY_FACE = 0
    BY_COLLECTION = 1
  End Enum

  Private Enum ENUM_UNDO_COLLECTED
    NOT_ASSIGNED = -1
    COLLECTED = 0
    UNDO_COLLECTED = 1
  End Enum
#End Region ' Enums

#Region " Members "

  'REPORT FILTER
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_type_collection As String
  Private m_report_type As String
  Private m_change_prov As Boolean

  'TOTAL
  Dim m_num_expected As Decimal
  Dim m_sum_expected As Decimal
  Dim m_num_collected As Decimal
  Dim m_sum_collected As Decimal
  Dim m_num_lost As Decimal
  Dim m_sum_lost As Decimal

  'SUBTOTAL
  Dim m_ant_collection As String
  Dim m_num_expected_subtotal As Decimal
  Dim m_sum_expected_subtotal As Decimal
  Dim m_num_collected_subtotal As Decimal
  Dim m_sum_collected_subtotal As Decimal
  Dim m_num_lost_subtotal As Decimal
  Dim m_sum_lost_subtotal As Decimal
  Dim m_date_subtotal As Date

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MONEY_COLLECTIONS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Money Collections
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(641)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(673)
    'DDM 17-MAY-2012: The button is not visible
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    ' Date
    'DDM 16-MAY-2012: changed the controls: uc_dtp by uc_daily_session_selector   
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Notes collection
    Me.gb_Notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(589)
    Me.opt_all_notes.Text = GLB_NLS_GUI_INVOICING.GetString(254)
    Me.opt_no_collected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(681)
    Me.opt_money_lost.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(675)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AcceptorTerminalTypeList())

    'Report type combo box
    Me.cmb_report_type.Text = GLB_NLS_GUI_INVOICING.GetString(433)
    Me.cmb_report_type.Add(ENUM_REPORT_TYPE.BY_COLLECTION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(642))
    Me.cmb_report_type.Add(ENUM_REPORT_TYPE.BY_FACE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(643))

    ' Events
    AddHandler opt_all_notes.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler opt_no_collected.CheckedChanged, AddressOf opt_CheckedChanged
    AddHandler opt_money_lost.CheckedChanged, AddressOf opt_CheckedChanged

    Me.m_change_prov = False

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    'DDM 16-MAY-2012: change the controls: uc_dtp by uc_daily_session_selector
    Me.ActiveControl = Me.uc_dsl

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As System.Text.StringBuilder
    Dim _string_sql = ""
    Dim _str_where_account As String = ""

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" DECLARE @STARTED AS DATETIME ")
    _str_sql.AppendLine(" DECLARE @FINISHED AS DATETIME ")
    _str_sql.AppendLine(" SET @STARTED = " & GUI_FormatDateDB(uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime)))
    _str_sql.AppendLine(" SET @FINISHED = " & GUI_FormatDateDB(uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime)))
    _str_sql.AppendLine(" SELECT   MC_COLLECTION_ID	  ")
    _str_sql.AppendLine("        , MC_DATETIME        ")
    _str_sql.AppendLine("        , GU_USERNAME        ")
    _str_sql.AppendLine("        , ' '  AS TE_NAME    ")
    _str_sql.AppendLine("        , MC_MONEY_LOST      ")

    If cmb_report_type.Value = ENUM_REPORT_TYPE.BY_FACE Then
      _str_sql.AppendLine("      , MCD_FACE_VALUE    	                                      ")
      _str_sql.AppendLine("      , MCD_NUM_EXPECTED AS NUM_NOTES_EXPECTED                   ")
      _str_sql.AppendLine("      ,(MCD_FACE_VALUE * MCD_NUM_EXPECTED) AS SUM_EXPECTED       ")
      _str_sql.AppendLine("      , MCD_NUM_COLLECTED AS NUM_NOTES_COLLECTED                 ")
      _str_sql.AppendLine("      ,(MCD_FACE_VALUE * MCD_NUM_COLLECTED) AS SUM_COLLECTED     ")
    Else
      _str_sql.AppendLine("      , 0                                                        ")
      _str_sql.AppendLine("      , SUM(MCD_NUM_EXPECTED) AS NUM_NOTES_EXPECTED              ")
      _str_sql.AppendLine("      , SUM(MCD_FACE_VALUE * MCD_NUM_EXPECTED) AS SUM_EXPECTED   ")
      _str_sql.AppendLine("      , SUM(MCD_NUM_COLLECTED) AS NUM_NOTES_COLLECTED            ")
      _str_sql.AppendLine("      , SUM(MCD_FACE_VALUE * MCD_NUM_COLLECTED) AS SUM_COLLECTED ")
    End If

    _str_sql.AppendLine("   FROM   MONEY_COLLECTION_DETAILS                             ")
    _str_sql.AppendLine("          INNER JOIN  MONEY_COLLECTIONS                        ")
    _str_sql.AppendLine("          ON  MC_COLLECTION_ID = MCD_COLLECTION_ID             ")
    _str_sql.AppendLine("          LEFT OUTER JOIN GUI_USERS ON MC_USER_ID = GU_USER_ID ")
    _str_sql.AppendLine("  WHERE   MC_UNDO_COLLECT = 0 ")
    _str_sql.AppendLine("    AND   MCD_FACE_VALUE > 0 ")

    'DDM 16-MAY-2012: change controls: uc_dtp by uc_daily_session_selector
    If Me.uc_dsl.ToDateSelected Then
      _str_sql.AppendLine("  AND   MC_DATETIME < @FINISHED ")
    End If

    If Me.uc_dsl.FromDateSelected Then
      _str_sql.AppendLine("  AND   MC_DATETIME >= @STARTED ")
    End If

    If opt_money_lost.Checked Then
      _str_sql.AppendLine("  AND   MC_MONEY_LOST = 1 ")
    End If

    _str_sql.AppendLine(" GROUP BY  MC_COLLECTION_ID   ")
    _str_sql.AppendLine("         , MC_DATETIME        ")
    _str_sql.AppendLine("         , GU_USERNAME        ")
    _str_sql.AppendLine("         , MC_MONEY_LOST ")


    If cmb_report_type.Value = ENUM_REPORT_TYPE.BY_FACE Then
      _str_sql.AppendLine("       , MCD_FACE_VALUE   	      ")
      _str_sql.AppendLine("       , MCD_NUM_EXPECTED        ")
      _str_sql.AppendLine("       , MCD_NUM_COLLECTED   	  ")
    End If

    If Me.opt_no_collected.Checked Then
      _str_sql.AppendLine("HAVING ")
      If cmb_report_type.Value = ENUM_REPORT_TYPE.BY_FACE Then
        _str_sql.AppendLine("       MCD_NUM_EXPECTED <> MCD_NUM_COLLECTED           ")
      Else
        _str_sql.AppendLine("       SUM(MCD_NUM_EXPECTED) <> SUM(MCD_NUM_COLLECTED) ")
      End If
    End If

    _str_sql.AppendLine("ORDER BY   MC_DATETIME")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery 

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _face_value As Int32

    ' DDM 21-MAY-2012: If is one register from expected, not be inserted
    If DbRow.Value(SQL_COLUMN_NUM_COLLECTED) = 0 Then

      Return False
    End If

    If (cmb_report_type.Value = ENUM_REPORT_TYPE.BY_FACE) And (RowIndex <> 0) And (m_ant_collection <> DbRow.Value(SQL_COLUMN_COLLECTION_ID)) Then
      Call CollectionSubtotalRow(RowIndex)
      Me.Grid.AddRow()
      RowIndex += 1
    End If

    m_ant_collection = DbRow.Value(SQL_COLUMN_COLLECTION_ID)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTION_ID).Value = DbRow.Value(SQL_COLUMN_COLLECTION_ID)
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL)
    End If

    m_date_subtotal = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                         ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(m_date_subtotal, _
                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USER_NAME)

    _face_value = DbRow.Value(SQL_COLUMN_FACE_VALUE)

    If _face_value = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FACE_VALUE).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FACE_VALUE).Value = GUI_FormatCurrency(_face_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_EXPECTED), 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SUM_EXPECTED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Money Lost
    If DbRow.Value(SQL_COLUMN_MONEY_LOST) = 1 Then
      Me.Grid.Row(RowIndex).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_LOST).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_COLLECTED), 0)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_LOST).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SUM_COLLECTED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = ""
    Else
      'Collected
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_COLLECTED), 0)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SUM_COLLECTED), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_LOST).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_LOST).Value = ""
    End If

    'Expected
    m_num_expected += DbRow.Value(SQL_COLUMN_NUM_EXPECTED)
    m_sum_expected += DbRow.Value(SQL_COLUMN_SUM_EXPECTED)
    ' Subtotal
    m_num_expected_subtotal += DbRow.Value(SQL_COLUMN_NUM_EXPECTED)
    m_sum_expected_subtotal += DbRow.Value(SQL_COLUMN_SUM_EXPECTED)

    If DbRow.Value(SQL_COLUMN_MONEY_LOST) = 1 Then
      'Lost
      m_num_lost += DbRow.Value(SQL_COLUMN_NUM_COLLECTED)
      m_sum_lost += DbRow.Value(SQL_COLUMN_SUM_COLLECTED)
      ' Subtotal
      m_num_lost_subtotal += DbRow.Value(SQL_COLUMN_NUM_COLLECTED)
      m_sum_lost_subtotal += DbRow.Value(SQL_COLUMN_SUM_COLLECTED)
    Else
      'Collected
      m_num_collected += DbRow.Value(SQL_COLUMN_NUM_COLLECTED)
      m_sum_collected += DbRow.Value(SQL_COLUMN_SUM_COLLECTED)
      ' Subtotal
      m_num_collected_subtotal += DbRow.Value(SQL_COLUMN_NUM_COLLECTED)
      m_sum_collected_subtotal += DbRow.Value(SQL_COLUMN_SUM_COLLECTED)
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    'If m_change_prov Then
    '  m_change_prov = False
    '  Call GUI_StyleSheet()
    'End If

    m_num_expected = 0
    m_sum_expected = 0
    m_num_collected = 0
    m_sum_collected = 0
    m_num_lost = 0
    m_sum_lost = 0

    m_num_expected_subtotal = 0
    m_sum_expected_subtotal = 0
    m_num_collected_subtotal = 0
    m_sum_collected_subtotal = 0
    m_num_lost_subtotal = 0
    m_sum_lost_subtotal = 0
    m_date_subtotal = New Date()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    'Subtotal
    If cmb_report_type.Value = ENUM_REPORT_TYPE.BY_FACE Then
      If Grid.NumRows > 0 Then
        Me.Grid.AddRow()
        _row_index = Me.Grid.NumRows - 1
        Call CollectionSubtotalRow(_row_index)
      End If
    End If

    Me.Grid.AddRow()
    _row_index = Me.Grid.NumRows - 1

    ' Total
    'DDM 18-MAY-2012: Changed total to datetime column
    Me.Grid.Cell(_row_index, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_STATISTICS.GetString(203)
    Me.Grid.Cell(_row_index, GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_EXPECTED).Value = "" ' GUI_FormatNumber(m_num_expected, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_EXPECTED).Value = "" 'GUI_FormatCurrency(m_sum_expected, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_COLLECTED).Value = GUI_FormatNumber(m_num_collected, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(m_sum_collected, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_NUM_LOST).Value = GUI_FormatNumber(m_num_lost, 0)
    Me.Grid.Cell(_row_index, GRID_COLUMN_SUM_LOST).Value = GUI_FormatCurrency(m_sum_lost, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Row(_row_index).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub 'GUI_AfterLastRow

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        UndoCollection()
        Return

      Case ENUM_BUTTON.BUTTON_SELECT
        Return

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'First column
    'DDM 17-MAY-2012 m_type_collection is not visible.
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    'Second column
    'DDM 16-MAY-2012 m_terminal and m_report_type are NOT visible.
    'PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    'PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(433), m_report_type)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""
    m_type_collection = ""

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' All notes, or pending collected
    ' DDM  16-MAY-2012: The m_type_collection variable should be equal to selected option 
    If opt_all_notes.Checked = True Then
      m_type_collection = opt_all_notes.Text
    ElseIf opt_no_collected.Checked = True Then
      m_type_collection = opt_no_collected.Text
    ElseIf opt_money_lost.Checked = True Then
      m_type_collection = opt_money_lost.Text
    End If

    ' Report type
    Select Case Me.cmb_report_type.Value
      Case ENUM_REPORT_TYPE.BY_COLLECTION
        m_report_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(642)
      Case ENUM_REPORT_TYPE.BY_FACE
        m_report_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(643)
    End Select

    ' Date 
    'DDM 16-MAY-2012: change controls: uc_dtp by uc_daily_session_selector
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      ' COLLECTION ID
      .Column(GRID_COLUMN_COLLECTION_ID).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(647)
      .Column(GRID_COLUMN_COLLECTION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_COLLECTION_ID).Width = 0 '1400
      .Column(GRID_COLUMN_COLLECTION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DATETIME
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(647)
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(604) '""
      .Column(GRID_COLUMN_DATETIME).Width = 2000
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TERMINAL
      'DDM 16-MAY-2012: The Terminal column is not visible
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(647)
      .Column(GRID_COLUMN_TERMINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL).Width = 0  '2300
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' USER 
      .Column(GRID_COLUMN_USER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(647)
      .Column(GRID_COLUMN_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603) '""
      .Column(GRID_COLUMN_USER_NAME).Width = 2000
      .Column(GRID_COLUMN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' FACE VALUE
      .Column(GRID_COLUMN_FACE_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(647)
      .Column(GRID_COLUMN_FACE_VALUE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(443) '" "
      .Column(GRID_COLUMN_FACE_VALUE).Width = 1150
      .Column(GRID_COLUMN_FACE_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DDM 16-MAY-2012: Columns: NUM_EXPECTED, SUM_EXPECTED are NOT visible
      ' NUM EXPECTED
      .Column(GRID_COLUMN_NUM_EXPECTED).Header(0).Text = " " ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_NUM_EXPECTED).Header(1).Text = " " 'GLB_NLS_GUI_INVOICING.GetString(317)
      .Column(GRID_COLUMN_NUM_EXPECTED).Width = 0 '  1200
      .Column(GRID_COLUMN_NUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM EXPECTED
      .Column(GRID_COLUMN_SUM_EXPECTED).Header(0).Text = " " ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_SUM_EXPECTED).Header(1).Text = " " ' GLB_NLS_GUI_INVOICING.GetString(488)
      .Column(GRID_COLUMN_SUM_EXPECTED).Width = 0 '2000
      .Column(GRID_COLUMN_SUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'DDM 16-MAY-2012: Columns: NUM_LOST, SUM_LOST are NOT visible
      ' NUM COLLECTED
      .Column(GRID_COLUMN_NUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(GRID_COLUMN_NUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(317)
      .Column(GRID_COLUMN_NUM_COLLECTED).Width = 1200
      .Column(GRID_COLUMN_NUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM COLLECTED
      .Column(GRID_COLUMN_SUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(GRID_COLUMN_SUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(488)
      .Column(GRID_COLUMN_SUM_COLLECTED).Width = 2000
      .Column(GRID_COLUMN_SUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM LOST
      .Column(GRID_COLUMN_NUM_LOST).Header(0).Text = " " ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(672)
      .Column(GRID_COLUMN_NUM_LOST).Header(1).Text = " " ' GLB_NLS_GUI_INVOICING.GetString(317)
      .Column(GRID_COLUMN_NUM_LOST).Width = 0 ' 1200
      .Column(GRID_COLUMN_NUM_LOST).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM LOST
      .Column(GRID_COLUMN_SUM_LOST).Header(0).Text = " " ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(672)
      .Column(GRID_COLUMN_SUM_LOST).Header(1).Text = " " ' GLB_NLS_GUI_INVOICING.GetString(349)
      .Column(GRID_COLUMN_SUM_LOST).Width = 0 '2000
      .Column(GRID_COLUMN_SUM_LOST).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date

    ' Terminals
    Call Me.uc_pr_list.SetDefaultValues()

    ' Dates
    'DDM 16-MAY-2012: change the controls: uc_dtp by uc_daily_session_selector
    _final_time = WSI.Common.Misc.TodayOpening()
    _closing_time = _final_time.Hour
    _final_time = _final_time.Date

    Me.uc_dsl.FromDate = _final_time
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _final_time.AddDays(1)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    ' DDM 16-MAY-2012:Option by defect : BY_FACE 'entrega y billete.
    Me.cmb_report_type.Value = ENUM_REPORT_TYPE.BY_FACE
    Me.opt_all_notes.Checked = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

  End Sub ' SetDefaultValues

  ' PURPOSE: match values 
  '
  '  PARAMS:
  '     - INPUT:
  '           - DbRow
  '     - OUTPUT:
  '           - True: value accepted <> value transferred
  '           - False: value accepted = value transferred
  '
  ' RETURNS:
  '     - None
  '
  Private Function IsMisMatch(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If (GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_EXPECTED), 0) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_COLLECTED), 0)) <> 0 Then
      Return True
    End If

    Return False

  End Function 'IsMisMatch

  ' PURPOSE: Results for each Collection in a SubtotalRow
  '
  '  PARAMS:
  '     - INPUT:
  '           - DbRow
  '     - OUTPUT:
  '           - True: value accepted <> value transferred
  '           - False: value accepted = value transferred
  '
  ' RETURNS:
  '     - None
  '
  Private Sub CollectionSubtotalRow(ByVal RowIndex)

    ' DDM 17-MAY-2012: Changed literal "subtotal" by "date"
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(m_date_subtotal, _
                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    ' Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = GUI_FormatNumber(m_num_expected_subtotal, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(m_sum_expected_subtotal, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' DDM 18-MAY-2012: If subtotal is 0 also assigned in cell
    'If m_num_collected_subtotal <> 0 Then
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = GUI_FormatNumber(m_num_collected_subtotal, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(m_sum_collected_subtotal, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'End If

    'If m_num_lost_subtotal <> 0 Then
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_LOST).Value = GUI_FormatNumber(m_num_lost_subtotal, 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_LOST).Value = GUI_FormatCurrency(m_sum_lost_subtotal, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    'End If

    'Color
    Me.Grid.Row(RowIndex).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    'Subtotal by terminal
    m_num_expected_subtotal = 0
    m_sum_expected_subtotal = 0
    m_num_collected_subtotal = 0
    m_sum_collected_subtotal = 0
    m_num_lost_subtotal = 0
    m_sum_lost_subtotal = 0

  End Sub 'CollectionSubtotalRow

  ' PURPOSE: Update selected collection/s as undo collected. All notes of collection will be pending to collect
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub UndoCollection()

    Dim _collection_id As Int64
    Dim _collections_list As List(Of Int64)
    Dim _collection_already_undone As Boolean

    _collection_already_undone = False

    ' If CurrentRow = -1, there is no row selected.
    If Me.Grid.CurrentRow = -1 Then
      Return
    End If

    ' No collection selected
    If Me.Grid.SelectedRows().Length = 0 Then
      ' 674 "Debe seleccionar al menos una entrega."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(674), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return
    End If

    Try

      ' Get selected collections
      _collections_list = New List(Of Int64)

      Using _db_trx As DB_TRX = New DB_TRX()

        For Each _selected_row As Integer In Me.Grid.SelectedRows()
          ' If it's a subtotal row
          If IsDBNull(Me.Grid.Cell(_selected_row, GRID_COLUMN_TERMINAL).Value) Or Me.Grid.Cell(_selected_row, GRID_COLUMN_TERMINAL).Value = "" Then
            Continue For
          End If

          _collection_id = GUI_ParseCurrency(Me.Grid.Cell(_selected_row, GRID_COLUMN_COLLECTION_ID).Value)

          'Check if collection has been undone
          If Not CheckCollectionUndo(_collection_id, _db_trx.SqlTransaction, _collection_already_undone) Then
            Continue For
          End If

          If Not _collection_already_undone And Not _collections_list.Contains(_collection_id) Then
            _collections_list.Add(_collection_id)
          End If
        Next

        If _collections_list.Count = 0 Then
          If _collection_already_undone Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(682), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
            Return
          End If

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(674), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
          Return
        End If

        ' 678 "Todos los billetes de la entrega seleccionada pasar�n a estar pendientes de entregar."
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(678), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return
        End If

        ' Update MoneyCollection undo
        For Each _collection_id In _collections_list
          If Not UpdateMoneyCollectionUndo(_collection_id, _db_trx.SqlTransaction) Then
            Throw New Exception("Error. UndoCollection")
          End If
        Next

        _db_trx.Commit()
      End Using

      ' Auditory
      GUI_WriteAuditoryChanges(True, _collections_list)

      '108 "Los cambios se han guardado correctamente."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(109), _
                             GLB_NLS_GUI_AUDITOR.GetString(109) & _ex.Message, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
    End Try

  End Sub ' UndoCollection

  ' PURPOSE: Check if collection has been undone
  '
  '  PARAMS:
  '     - INPUT:
  '           - CollectionId
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  '
  Private Function CheckCollectionUndo(ByVal CollectionId As Int64, ByVal SqlTrx As SqlTransaction, ByRef CollectionUndone As Boolean) As Boolean

    Dim _sb As StringBuilder

    CollectionUndone = False

    _sb = New StringBuilder()

    Try
      _sb.AppendLine(" SELECT   MC_UNDO_COLLECT                    ")
      _sb.AppendLine("   FROM   MONEY_COLLECTIONS                  ")
      _sb.AppendLine("  WHERE   MC_COLLECTION_ID  = @pCollectionId ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId

        Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()
          If Not _sql_reader.Read() Then
            Throw New Exception("Error. CollectionAlreadyUndo")
          End If
          If Not _sql_reader.IsDBNull(0) Then
            CollectionUndone = _sql_reader.GetSqlBoolean(0)
          End If
        End Using
      End Using

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "money_collections", "CheckCollectionUndo", _ex.Message)

      Return False
    End Try

    Return True
  End Function ' CollectionAlreadyUndo

  ' PURPOSE: Update selected collection/s as undo collected. All notes of collection will be pending to collect
  '
  '  PARAMS:
  '     - INPUT:
  '           - CollectionId
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  '
  Private Function UpdateMoneyCollectionUndo(ByVal CollectionId As Int64, ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    Try

      _sb.AppendLine(" UPDATE   MONEY_COLLECTIONS                  ")
      _sb.AppendLine("    SET   MC_UNDO_COLLECT   = @pUndoCollect  ")
      _sb.AppendLine("  WHERE   MC_COLLECTION_ID  = @pCollectionId ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _sql_cmd.Parameters.Add("@pUndoCollect", SqlDbType.Bit).Value = ENUM_UNDO_COLLECTED.UNDO_COLLECTED
        _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId

        If _sql_cmd.ExecuteNonQuery() <> 1 Then
          Return False
        End If

      End Using

      'Update TERMINAL_MONEY registers of the collection modified
      _sb = New StringBuilder()
      _sb.AppendLine(" UPDATE   TERMINAL_MONEY                        ")
      _sb.AppendLine("    SET   TM_COLLECTION_ID  = @pCollectionId    ")
      _sb.AppendLine("  WHERE   TM_COLLECTION_ID  = @pAntCollectionId ")

      Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx)
        _sql_cmd.Parameters.Add("@pAntCollectionId", SqlDbType.BigInt).Value = CollectionId
        _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = DBNull.Value

        _sql_cmd.ExecuteNonQuery()

      End Using

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "money_collections", "UpdateMoneyCollectionUndo", _ex.Message)

      Return False
    End Try

    Return True
  End Function ' UpdateCollectionsUndo

  ' PURPOSE: Write auditory changes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Server As Boolean
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_WriteAuditoryChanges(ByVal Server As Boolean, ByVal ChangesList As List(Of Int64))

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _original_auditor_data As CLASS_AUDITOR_DATA
    Dim _value_string As String

    If IsNothing(ChangesList) Or ChangesList.Count = 0 Then
      Exit Sub
    End If

    ' Insert MODIFIED items
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    ' Set Parameter
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(684), " ")
    Call _original_auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(684), " ")

    For Each _collection As Int64 In ChangesList
      _value_string = IIf(IsDBNull(_collection), "", _collection)

      Call _auditor_data.SetField(0, IIf(_value_string = "", AUDIT_NONE_STRING, _value_string), GLB_NLS_GUI_PLAYER_TRACKING.GetString(647))
      _value_string = 0

      Call _original_auditor_data.SetField(0, IIf(_value_string = "", AUDIT_NONE_STRING, _value_string), "")

    Next

    If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                GLB_CurrentUser.Id, _
                                GLB_CurrentUser.Name, _
                                CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.DELETE, _
                                0, _
                                _original_auditor_data) Then
      ' Logger message 
    End If

    _auditor_data = Nothing
    _original_auditor_data = Nothing

  End Sub 'GUI_WriteAuditoryChanges

#End Region ' Private 

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

  ' PURPOSE: All notes, Lost money or Only gap options are checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '          - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub opt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Me.m_change_prov = True

  End Sub 'opt_CheckedChanged

#End Region ' Events

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

End Class 'frm_money_collections