'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_lcd_message_edit
' DESCRIPTION:   New or Edit LCD message
' AUTHOR:        David Hernández Arias
' CREATION DATE: 01-DEC-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-DEC-2014  DHA    Initial version.
' 17-DEC-2014  DHA    Fixed Bug #WIG-1850: validate at least one site is selected
' 22-DEC-2014  DHA    Fixed Bug #WIG-1886: changed NLS on MultiSite for "Participating providers"
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_lcd_message_edit
  Inherits frm_base_edit

#Region "Members"
  Private m_lcd_message As CLASS_LCD_MESSAGE
  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)
  Private m_elp_mode As Integer = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode")
  Private len_message_per_line As Integer
  Private len_message_max_lines As Integer
#End Region

#Region "Constants"
  Private Const MSG_MAX_LINE_LONG_LCD_1 As Integer = 20
  Private Const MSG_MAX_LINE_NUM_LCD_1 As Integer = 2
#End Region

#Region "Enums"

#End Region

#Region "Structures"

#End Region

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_LCD_MESSAGE_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' TODO: For adapt messages to another lcd
    ' Update MEMBERS:
    ' len_message_per_line = Max length per line in the lcd (carry retunrs are not taken into account )
    ' Add constant MSG_MAX_LINE_LONG_LCD_2 with new length and select right lcd constant

    len_message_per_line = MSG_MAX_LINE_LONG_LCD_1

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5771)

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Message Line 1
    Me.ef_message_line_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5703, "1")
    Me.ef_message_line_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MSG_MAX_LINE_LONG_LCD_1)

    ' Message Line 2
    Me.ef_message_line_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5703, "2")
    Me.ef_message_line_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MSG_MAX_LINE_LONG_LCD_1)

    Me.gb_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5772)

    ' Types
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5775) ' Tipo
    Me.cmb_categories.Add(CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE.WITHOUT_CARD, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5773))
    Me.cmb_categories.Add(CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE.ANONYMOUS_ACCOUNT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5774))

    ' Only for ELP mode
    If m_elp_mode > 0 Then
      Me.cmb_categories.Add(CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE.CANCELED_ACCOUNT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5788))
    End If

    ' Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)

    ' Providers
    If m_is_center Then
      Me.uc_terminals_group_filter.SetGroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5819)
      Me.uc_terminals_group_filter.ShowGroups = New List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
      Me.uc_terminals_group_filter.ShowGroups.Add(WSI.Common.Groups.GROUP_NODE_ELEMENT.PROVIDER)
    Else
      Me.uc_terminals_group_filter.SetGroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(477)
    End If
    Me.uc_terminals_group_filter.SetTerminalList(New TerminalList())

    ' uc_Schedule control
    uc_schedule.SetDefaults()
    uc_schedule.ShowCheckBoxDateTo = True
    uc_schedule.LabelDateFrom = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5797)

    ' Sites
    If m_is_center Then
      Me.uc_site_select.ShowMultisiteRow = False
      Me.uc_site_select.Init()
      Me.uc_site_select.SetDefaultValues(False)
    Else
      Me.uc_site_select.Visible = False
    End If

    If Not m_is_center And m_lcd_message.MasterSequenceId <> 0 And DbObjectId <> Nothing Then
      EnableControls(False)
    Else
      EnableControls(True)
    End If

    If Not m_is_center Then
      Me.Size = New Size(Me.Width, Me.Height - 150)
    End If

  End Sub

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    m_lcd_message = DbReadObject

    ' Message line 1
    Me.ef_message_line_1.TextValue = m_lcd_message.MessageLine1

    ' Message line 2
    Me.ef_message_line_2.TextValue = m_lcd_message.MessageLine2

    ' Type
    cmb_categories.SelectedIndex = m_lcd_message.Type

    chk_enabled.Checked = m_lcd_message.Enabled

    ' Terminals
    uc_terminals_group_filter.SetTerminalList(m_lcd_message.TerminalList)

    uc_schedule.DateFrom = m_lcd_message.ScheduleStart

    If m_lcd_message.ScheduleEnd = Nothing Then
      uc_schedule.CheckedDateTo = False
    Else
      uc_schedule.DateTo = m_lcd_message.ScheduleEnd
    End If

    If m_lcd_message.ScheduleWeekday = Nothing Then
      Me.uc_schedule.Weekday = Me.uc_schedule.AllWeekDaysSelected
    Else
      uc_schedule.Weekday = m_lcd_message.ScheduleWeekday
    End If
    uc_schedule.TimeFrom = m_lcd_message.Schedule1TimeFrom
    uc_schedule.TimeTo = m_lcd_message.Schedule1TimeTo
    uc_schedule.SecondTime = m_lcd_message.Schedule2Enabled
    uc_schedule.SecondTimeFrom = m_lcd_message.Schedule2TimeFrom
    uc_schedule.SecondTimeTo = m_lcd_message.Schedule2TimeTo

    ' Sites
    If m_is_center Then
      uc_site_select.SetSitesIdListSelected(m_lcd_message.SiteList)
    End If


  End Sub ' GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String

    ' Empty Message
    If (String.IsNullOrEmpty(Me.ef_message_line_1.TextValue) And String.IsNullOrEmpty(Me.ef_message_line_2.TextValue)) Then
      ' 3441 "El campo 'Mensaje' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , Me.gb_message.Text)
      Call Me.ef_message_line_1.Focus()
      Return False
    End If

    If Not m_is_center Then
      ' Providers (if listed or not listed must have at least one selected)
      If Not Me.uc_terminals_group_filter.CheckAtLeastOneSelected() Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(172)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call uc_terminals_group_filter.Focus()

        Return False
      End If
    End If

    If m_is_center Then
      ' You must select at least one option from: %1
      If String.IsNullOrEmpty(Me.uc_site_select.GetSitesIdListSelectedAll()) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1292), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803))
        Call uc_site_select.Focus()

        Return False
      End If
    End If

    ' Expiration dates range
    If uc_schedule.DateFrom > uc_schedule.DateTo Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_schedule.SetFocus(1)

      Return False
    End If

    ' No Week days selected
    If uc_schedule.Weekday = uc_schedule.NoWeekDaysSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_schedule.SetFocus(4)

      Return False
    End If

    Return True

  End Function

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    m_lcd_message = Me.DbEditedObject

    ' Type
    m_lcd_message.Type = cmb_categories.SelectedIndex

    ' Enabled
    m_lcd_message.Enabled = chk_enabled.Checked

    ' Message line 1
    m_lcd_message.MessageLine1 = Me.ef_message_line_1.TextValue

    ' Message line 2
    m_lcd_message.MessageLine2 = Me.ef_message_line_2.TextValue

    ' ScheduleStart
    m_lcd_message.ScheduleStart = uc_schedule.DateFrom

    ' ScheduleEnd
    If uc_schedule.CheckedDateTo Then
      m_lcd_message.ScheduleEnd = uc_schedule.DateTo
    Else
      m_lcd_message.ScheduleEnd = Nothing
    End If

    ' ScheduleWeekday
    m_lcd_message.ScheduleWeekday = uc_schedule.Weekday

    ' Schedule1TimeFrom
    m_lcd_message.Schedule1TimeFrom = uc_schedule.TimeFrom

    ' Schedule1TimeTo
    m_lcd_message.Schedule1TimeTo = uc_schedule.TimeTo

    ' Schedule2Enabled
    m_lcd_message.Schedule2Enabled = uc_schedule.SecondTime

    ' Schedule2TimeFrom
    m_lcd_message.Schedule2TimeFrom = uc_schedule.SecondTimeFrom

    ' Schedule2TimeTo
    m_lcd_message.Schedule2TimeTo = uc_schedule.SecondTimeTo

    ' Terminals
    uc_terminals_group_filter.GetTerminalsFromControl(m_lcd_message.TerminalList)

    ' Sites
    If m_is_center Then
      m_lcd_message.SiteList = uc_site_select.GetSitesIdListSelectedAll()
    End If


  End Sub

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowEditItem(ByVal MessageId As Int64)
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = MessageId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub

  ' PURPOSE : Override form permissions to disallow writting in MultiSite Member
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    'Dim _lcd_message As CLASS_LCD_MESSAGE

    '_lcd_message = Me.DbReadObject

    If Not m_is_center And m_lcd_message.MasterSequenceId <> 0 Then
      AndPerm.Write = False
    End If

  End Sub

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_message_line_1

  End Sub ' GUI_SetInitialFocus

#End Region

#Region "Private"
  ' PURPOSE: Enable/disable controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub EnableControls(ByVal Enable As Boolean)

    Me.ef_message_line_1.Enabled = Enable
    Me.ef_message_line_2.Enabled = Enable
    cmb_categories.Enabled = Enable
    chk_enabled.Enabled = Enable
    If Enable Then
      uc_terminals_group_filter.Enabled = True
    Else
      uc_terminals_group_filter.Enabled = True
      Me.uc_terminals_group_filter.ThreeStateCheckMode = False
    End If
    uc_schedule.Enabled = Enable
    uc_site_select.Enabled = Enable

  End Sub

#End Region

#Region "Public"

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal LCDMessageCopiedId As Long = 0)
    Dim _lcd_message As CLASS_LCD_MESSAGE

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    If LCDMessageCopiedId > 0 Then
      _lcd_message = DbEditedObject
      DbObjectId = LCDMessageCopiedId
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      DbObjectId = Nothing

      Call _lcd_message.ResetIdValues()
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _aux_date As DateTime

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_LCD_MESSAGE
        m_lcd_message = DbEditedObject
        _aux_date = Now.AddMonths(1)
        m_lcd_message.Enabled = True
        m_lcd_message.ScheduleStart = New DateTime(_aux_date.Year, _aux_date.Month, 1, GetDefaultClosingTime(), 0, 0)
        m_lcd_message.ScheduleEnd = m_lcd_message.ScheduleStart.AddMonths(1)
        m_lcd_message.Schedule1TimeFrom = 0     ' 00:00:00
        m_lcd_message.Schedule1TimeTo = 0       ' 00:00:00
        m_lcd_message.Schedule2Enabled = False
        m_lcd_message.Schedule2TimeFrom = 0     ' 00:00:00
        m_lcd_message.Schedule2TimeTo = 0       ' 00:00:00
        m_lcd_message.MessageLine1 = ""
        m_lcd_message.MessageLine2 = ""
        m_lcd_message.ScheduleWeekday = uc_schedule.AllWeekDaysSelected() ' all days

        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5779), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5780), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5781), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5779), ENUM_MB_TYPE.MB_TYPE_ERROR)
          Else
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5782), ENUM_MB_TYPE.MB_TYPE_ERROR)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        ' Not used
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4305), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation


#End Region

#Region "Events"


#End Region


End Class