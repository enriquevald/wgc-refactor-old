﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_smartfloor_report_task
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_date_creation = New System.Windows.Forms.RadioButton()
    Me.opt_date_resolution = New System.Windows.Forms.RadioButton()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.gb_severity = New System.Windows.Forms.GroupBox()
    Me.chk_severity_high = New System.Windows.Forms.CheckBox()
    Me.chk_severity_medium = New System.Windows.Forms.CheckBox()
    Me.chk_severity_low = New System.Windows.Forms.CheckBox()
    Me.gb_state = New System.Windows.Forms.GroupBox()
    Me.chk_state_scaled = New System.Windows.Forms.CheckBox()
    Me.chk_state_pending_all = New System.Windows.Forms.CheckBox()
    Me.chk_state_validated = New System.Windows.Forms.CheckBox()
    Me.chk_state_solved = New System.Windows.Forms.CheckBox()
    Me.chk_state_progress = New System.Windows.Forms.CheckBox()
    Me.chk_state_pending = New System.Windows.Forms.CheckBox()
    Me.chk_state_not_pending = New System.Windows.Forms.CheckBox()
    Me.uc_checked_list_alarms_catalog = New GUI_Controls.uc_checked_list()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_severity.SuspendLayout()
    Me.gb_state.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_show_all)
    Me.panel_filter.Controls.Add(Me.gb_severity)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_state)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_alarms_catalog)
    Me.panel_filter.Size = New System.Drawing.Size(1212, 168)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_alarms_catalog, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_severity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_all, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 172)
    Me.panel_data.Size = New System.Drawing.Size(1212, 402)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1206, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1206, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_date_creation)
    Me.gb_date.Controls.Add(Me.opt_date_resolution)
    Me.gb_date.Controls.Add(Me.uc_dsl)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(265, 131)
    Me.gb_date.TabIndex = 11
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_date_creation
    '
    Me.opt_date_creation.AutoSize = True
    Me.opt_date_creation.Checked = True
    Me.opt_date_creation.Location = New System.Drawing.Point(6, 20)
    Me.opt_date_creation.Name = "opt_date_creation"
    Me.opt_date_creation.Size = New System.Drawing.Size(121, 17)
    Me.opt_date_creation.TabIndex = 1
    Me.opt_date_creation.TabStop = True
    Me.opt_date_creation.Text = "xSearchCreation"
    Me.opt_date_creation.UseVisualStyleBackColor = True
    '
    'opt_date_resolution
    '
    Me.opt_date_resolution.AutoSize = True
    Me.opt_date_resolution.Location = New System.Drawing.Point(6, 43)
    Me.opt_date_resolution.Name = "opt_date_resolution"
    Me.opt_date_resolution.Size = New System.Drawing.Size(131, 17)
    Me.opt_date_resolution.TabIndex = 0
    Me.opt_date_resolution.Text = "xSearchResolution"
    Me.opt_date_resolution.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(3, 47)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = False
    Me.uc_dsl.Size = New System.Drawing.Size(260, 79)
    Me.uc_dsl.TabIndex = 126
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_severity
    '
    Me.gb_severity.Controls.Add(Me.chk_severity_high)
    Me.gb_severity.Controls.Add(Me.chk_severity_medium)
    Me.gb_severity.Controls.Add(Me.chk_severity_low)
    Me.gb_severity.Location = New System.Drawing.Point(728, 6)
    Me.gb_severity.Name = "gb_severity"
    Me.gb_severity.Size = New System.Drawing.Size(95, 81)
    Me.gb_severity.TabIndex = 12
    Me.gb_severity.TabStop = False
    Me.gb_severity.Text = "xSeverity"
    '
    'chk_severity_high
    '
    Me.chk_severity_high.Location = New System.Drawing.Point(6, 19)
    Me.chk_severity_high.Name = "chk_severity_high"
    Me.chk_severity_high.Size = New System.Drawing.Size(75, 16)
    Me.chk_severity_high.TabIndex = 2
    Me.chk_severity_high.Text = "xHigh"
    '
    'chk_severity_medium
    '
    Me.chk_severity_medium.Location = New System.Drawing.Point(6, 39)
    Me.chk_severity_medium.Name = "chk_severity_medium"
    Me.chk_severity_medium.Size = New System.Drawing.Size(75, 16)
    Me.chk_severity_medium.TabIndex = 1
    Me.chk_severity_medium.Text = "xMedium"
    '
    'chk_severity_low
    '
    Me.chk_severity_low.Location = New System.Drawing.Point(6, 59)
    Me.chk_severity_low.Name = "chk_severity_low"
    Me.chk_severity_low.Size = New System.Drawing.Size(75, 16)
    Me.chk_severity_low.TabIndex = 0
    Me.chk_severity_low.Text = "xLow"
    '
    'gb_state
    '
    Me.gb_state.Controls.Add(Me.chk_state_scaled)
    Me.gb_state.Controls.Add(Me.chk_state_pending_all)
    Me.gb_state.Controls.Add(Me.chk_state_validated)
    Me.gb_state.Controls.Add(Me.chk_state_solved)
    Me.gb_state.Controls.Add(Me.chk_state_progress)
    Me.gb_state.Controls.Add(Me.chk_state_pending)
    Me.gb_state.Controls.Add(Me.chk_state_not_pending)
    Me.gb_state.Location = New System.Drawing.Point(583, 6)
    Me.gb_state.Name = "gb_state"
    Me.gb_state.Size = New System.Drawing.Size(140, 159)
    Me.gb_state.TabIndex = 123
    Me.gb_state.TabStop = False
    Me.gb_state.Text = "xState"
    '
    'chk_state_scaled
    '
    Me.chk_state_scaled.Location = New System.Drawing.Point(6, 116)
    Me.chk_state_scaled.Name = "chk_state_scaled"
    Me.chk_state_scaled.Size = New System.Drawing.Size(110, 16)
    Me.chk_state_scaled.TabIndex = 5
    Me.chk_state_scaled.Text = "xScaled"
    '
    'chk_state_pending_all
    '
    Me.chk_state_pending_all.Location = New System.Drawing.Point(6, 18)
    Me.chk_state_pending_all.Name = "chk_state_pending_all"
    Me.chk_state_pending_all.Size = New System.Drawing.Size(110, 16)
    Me.chk_state_pending_all.TabIndex = 4
    Me.chk_state_pending_all.Text = "xPendingAll"
    '
    'chk_state_validated
    '
    Me.chk_state_validated.Location = New System.Drawing.Point(6, 136)
    Me.chk_state_validated.Name = "chk_state_validated"
    Me.chk_state_validated.Size = New System.Drawing.Size(110, 16)
    Me.chk_state_validated.TabIndex = 4
    Me.chk_state_validated.Text = "xValidated"
    '
    'chk_state_solved
    '
    Me.chk_state_solved.Location = New System.Drawing.Point(6, 96)
    Me.chk_state_solved.Name = "chk_state_solved"
    Me.chk_state_solved.Size = New System.Drawing.Size(110, 16)
    Me.chk_state_solved.TabIndex = 3
    Me.chk_state_solved.Text = "xSolved"
    '
    'chk_state_progress
    '
    Me.chk_state_progress.Location = New System.Drawing.Point(26, 76)
    Me.chk_state_progress.Name = "chk_state_progress"
    Me.chk_state_progress.Size = New System.Drawing.Size(105, 16)
    Me.chk_state_progress.TabIndex = 2
    Me.chk_state_progress.Text = "xProgress"
    '
    'chk_state_pending
    '
    Me.chk_state_pending.Location = New System.Drawing.Point(26, 56)
    Me.chk_state_pending.Name = "chk_state_pending"
    Me.chk_state_pending.Size = New System.Drawing.Size(105, 16)
    Me.chk_state_pending.TabIndex = 1
    Me.chk_state_pending.Text = "xPending"
    '
    'chk_state_not_pending
    '
    Me.chk_state_not_pending.Location = New System.Drawing.Point(26, 36)
    Me.chk_state_not_pending.Name = "chk_state_not_pending"
    Me.chk_state_not_pending.Size = New System.Drawing.Size(105, 16)
    Me.chk_state_not_pending.TabIndex = 0
    Me.chk_state_not_pending.Text = "xNotPending"
    '
    'uc_checked_list_alarms_catalog
    '
    Me.uc_checked_list_alarms_catalog.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_alarms_catalog.Location = New System.Drawing.Point(276, 6)
    Me.uc_checked_list_alarms_catalog.m_resize_width = 301
    Me.uc_checked_list_alarms_catalog.multiChoice = True
    Me.uc_checked_list_alarms_catalog.Name = "uc_checked_list_alarms_catalog"
    Me.uc_checked_list_alarms_catalog.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_alarms_catalog.SelectedIndexesList = ""
    Me.uc_checked_list_alarms_catalog.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_alarms_catalog.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_alarms_catalog.SelectedValuesList = ""
    Me.uc_checked_list_alarms_catalog.SetLevels = 2
    Me.uc_checked_list_alarms_catalog.Size = New System.Drawing.Size(301, 159)
    Me.uc_checked_list_alarms_catalog.TabIndex = 125
    Me.uc_checked_list_alarms_catalog.ValuesArray = New String(-1) {}
    '
    'chk_show_all
    '
    Me.chk_show_all.Location = New System.Drawing.Point(11, 143)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(250, 16)
    Me.chk_show_all.TabIndex = 126
    Me.chk_show_all.Text = "xShowInformation"
    '
    'frm_smartfloor_report_task
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1220, 578)
    Me.Name = "frm_smartfloor_report_task"
    Me.Text = "frm_smartfloor_report_task"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.gb_severity.ResumeLayout(False)
    Me.gb_state.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_date_creation As System.Windows.Forms.RadioButton
  Friend WithEvents opt_date_resolution As System.Windows.Forms.RadioButton
  Friend WithEvents gb_severity As System.Windows.Forms.GroupBox
  Friend WithEvents chk_severity_high As System.Windows.Forms.CheckBox
  Friend WithEvents chk_severity_medium As System.Windows.Forms.CheckBox
  Friend WithEvents chk_severity_low As System.Windows.Forms.CheckBox
  Friend WithEvents gb_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_state_validated As System.Windows.Forms.CheckBox
  Friend WithEvents chk_state_solved As System.Windows.Forms.CheckBox
  Friend WithEvents chk_state_progress As System.Windows.Forms.CheckBox
  Friend WithEvents chk_state_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_state_not_pending As System.Windows.Forms.CheckBox
  Friend WithEvents uc_checked_list_alarms_catalog As GUI_Controls.uc_checked_list
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents chk_state_scaled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_state_pending_all As System.Windows.Forms.CheckBox
End Class
