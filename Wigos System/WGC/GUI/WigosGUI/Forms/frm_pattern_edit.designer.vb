<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_pattern_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_pattern_edit))
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.gb_alarm = New System.Windows.Forms.GroupBox
    Me.chk_mailing = New System.Windows.Forms.CheckBox
    Me.gb_mailing = New System.Windows.Forms.GroupBox
    Me.txt_alarm_mailing_address_list = New System.Windows.Forms.TextBox
    Me.lbl_subject = New System.Windows.Forms.Label
    Me.lbl_address_list = New System.Windows.Forms.Label
    Me.txt_alarm_mailing_subject = New System.Windows.Forms.TextBox
    Me.ef_alarm_name = New GUI_Controls.uc_entry_field
    Me.lbl_alarm_description = New System.Windows.Forms.Label
    Me.txt_alarm_description = New System.Windows.Forms.TextBox
    Me.ef_alarm_code = New GUI_Controls.uc_entry_field
    Me.gb_severity = New System.Windows.Forms.GroupBox
    Me.lbl_color_error = New System.Windows.Forms.Label
    Me.lbl_color_warning = New System.Windows.Forms.Label
    Me.opt_error = New System.Windows.Forms.RadioButton
    Me.lbl_color_info = New System.Windows.Forms.Label
    Me.opt_warning = New System.Windows.Forms.RadioButton
    Me.opt_info = New System.Windows.Forms.RadioButton
    Me.dg_providers = New GUI_Controls.uc_terminals_group_filter
    Me.txt_description = New System.Windows.Forms.TextBox
    Me.lbl_description = New System.Windows.Forms.Label
    Me.gb_type = New System.Windows.Forms.GroupBox
    Me.opt_with_no_order_with_repetitions = New System.Windows.Forms.RadioButton
    Me.opt_with_no_order = New System.Windows.Forms.RadioButton
    Me.opt_no_consecutive_with_order = New System.Windows.Forms.RadioButton
    Me.opt_consecutive = New System.Windows.Forms.RadioButton
    Me.checked_list_alarms = New GUI_Controls.uc_checked_list
    Me.gb_schedule = New System.Windows.Forms.GroupBox
    Me.dtp_time_from = New GUI_Controls.uc_date_picker
    Me.dtp_time_to = New GUI_Controls.uc_date_picker
    Me.chk_schedule = New System.Windows.Forms.CheckBox
    Me.dg_elements = New GUI_Controls.uc_grid
    Me.btn_add_element = New System.Windows.Forms.Button
    Me.btn_clear = New System.Windows.Forms.Button
    Me.btn_remove_element = New System.Windows.Forms.Button
    Me.btn_move_down = New System.Windows.Forms.Button
    Me.btn_move_up = New System.Windows.Forms.Button
    Me.gb_elements = New System.Windows.Forms.GroupBox
    Me.ef_last_find = New GUI_Controls.uc_entry_field
    Me.ef_num_occurrencies = New GUI_Controls.uc_entry_field
    Me.ef_life_time = New GUI_Controls.uc_entry_field
    Me.gb_misc = New System.Windows.Forms.GroupBox
    Me.lbl_min = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_alarm.SuspendLayout()
    Me.gb_mailing.SuspendLayout()
    Me.gb_severity.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.gb_schedule.SuspendLayout()
    Me.gb_elements.SuspendLayout()
    Me.gb_misc.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.dg_providers)
    Me.panel_data.Controls.Add(Me.gb_alarm)
    Me.panel_data.Controls.Add(Me.gb_misc)
    Me.panel_data.Controls.Add(Me.chk_schedule)
    Me.panel_data.Controls.Add(Me.gb_schedule)
    Me.panel_data.Controls.Add(Me.gb_type)
    Me.panel_data.Controls.Add(Me.lbl_description)
    Me.panel_data.Controls.Add(Me.txt_description)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Controls.Add(Me.gb_elements)
    Me.panel_data.Size = New System.Drawing.Size(912, 690)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(14, 14)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.Size = New System.Drawing.Size(368, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(7, 48)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(152, 95)
    Me.gb_enabled.TabIndex = 1
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(48, 58)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(82, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(48, 31)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(82, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'gb_alarm
    '
    Me.gb_alarm.Controls.Add(Me.chk_mailing)
    Me.gb_alarm.Controls.Add(Me.gb_mailing)
    Me.gb_alarm.Controls.Add(Me.ef_alarm_name)
    Me.gb_alarm.Controls.Add(Me.lbl_alarm_description)
    Me.gb_alarm.Controls.Add(Me.txt_alarm_description)
    Me.gb_alarm.Controls.Add(Me.ef_alarm_code)
    Me.gb_alarm.Controls.Add(Me.gb_severity)
    Me.gb_alarm.Location = New System.Drawing.Point(490, 3)
    Me.gb_alarm.Name = "gb_alarm"
    Me.gb_alarm.Size = New System.Drawing.Size(417, 391)
    Me.gb_alarm.TabIndex = 8
    Me.gb_alarm.TabStop = False
    Me.gb_alarm.Text = "xAlarm"
    '
    'chk_mailing
    '
    Me.chk_mailing.AutoSize = True
    Me.chk_mailing.Location = New System.Drawing.Point(20, 229)
    Me.chk_mailing.Name = "chk_mailing"
    Me.chk_mailing.Size = New System.Drawing.Size(72, 17)
    Me.chk_mailing.TabIndex = 6
    Me.chk_mailing.Text = "xMailing"
    Me.chk_mailing.UseVisualStyleBackColor = True
    '
    'gb_mailing
    '
    Me.gb_mailing.Controls.Add(Me.txt_alarm_mailing_address_list)
    Me.gb_mailing.Controls.Add(Me.lbl_subject)
    Me.gb_mailing.Controls.Add(Me.lbl_address_list)
    Me.gb_mailing.Controls.Add(Me.txt_alarm_mailing_subject)
    Me.gb_mailing.Location = New System.Drawing.Point(10, 230)
    Me.gb_mailing.Name = "gb_mailing"
    Me.gb_mailing.Size = New System.Drawing.Size(401, 154)
    Me.gb_mailing.TabIndex = 14
    Me.gb_mailing.TabStop = False
    '
    'txt_alarm_mailing_address_list
    '
    Me.txt_alarm_mailing_address_list.Location = New System.Drawing.Point(9, 99)
    Me.txt_alarm_mailing_address_list.MaxLength = 500
    Me.txt_alarm_mailing_address_list.Multiline = True
    Me.txt_alarm_mailing_address_list.Name = "txt_alarm_mailing_address_list"
    Me.txt_alarm_mailing_address_list.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_alarm_mailing_address_list.Size = New System.Drawing.Size(386, 49)
    Me.txt_alarm_mailing_address_list.TabIndex = 3
    '
    'lbl_subject
    '
    Me.lbl_subject.AutoSize = True
    Me.lbl_subject.Location = New System.Drawing.Point(13, 21)
    Me.lbl_subject.Name = "lbl_subject"
    Me.lbl_subject.Size = New System.Drawing.Size(57, 13)
    Me.lbl_subject.TabIndex = 0
    Me.lbl_subject.Text = "xSubject"
    '
    'lbl_address_list
    '
    Me.lbl_address_list.AutoSize = True
    Me.lbl_address_list.Location = New System.Drawing.Point(12, 83)
    Me.lbl_address_list.Name = "lbl_address_list"
    Me.lbl_address_list.Size = New System.Drawing.Size(80, 13)
    Me.lbl_address_list.TabIndex = 2
    Me.lbl_address_list.Text = "xAddress list"
    '
    'txt_alarm_mailing_subject
    '
    Me.txt_alarm_mailing_subject.Location = New System.Drawing.Point(10, 40)
    Me.txt_alarm_mailing_subject.MaxLength = 200
    Me.txt_alarm_mailing_subject.Name = "txt_alarm_mailing_subject"
    Me.txt_alarm_mailing_subject.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_alarm_mailing_subject.Size = New System.Drawing.Size(385, 21)
    Me.txt_alarm_mailing_subject.TabIndex = 1
    '
    'ef_alarm_name
    '
    Me.ef_alarm_name.DoubleValue = 0
    Me.ef_alarm_name.IntegerValue = 0
    Me.ef_alarm_name.IsReadOnly = False
    Me.ef_alarm_name.Location = New System.Drawing.Point(10, 51)
    Me.ef_alarm_name.Name = "ef_alarm_name"
    Me.ef_alarm_name.Size = New System.Drawing.Size(226, 24)
    Me.ef_alarm_name.SufixText = "Sufix Text"
    Me.ef_alarm_name.SufixTextVisible = True
    Me.ef_alarm_name.TabIndex = 1
    Me.ef_alarm_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_alarm_name.TextValue = ""
    Me.ef_alarm_name.TextWidth = 60
    Me.ef_alarm_name.Value = ""
    Me.ef_alarm_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_alarm_description
    '
    Me.lbl_alarm_description.AutoSize = True
    Me.lbl_alarm_description.Location = New System.Drawing.Point(12, 83)
    Me.lbl_alarm_description.Name = "lbl_alarm_description"
    Me.lbl_alarm_description.Size = New System.Drawing.Size(78, 13)
    Me.lbl_alarm_description.TabIndex = 3
    Me.lbl_alarm_description.Text = "xDescription"
    '
    'txt_alarm_description
    '
    Me.txt_alarm_description.Location = New System.Drawing.Point(9, 102)
    Me.txt_alarm_description.MaxLength = 200
    Me.txt_alarm_description.Multiline = True
    Me.txt_alarm_description.Name = "txt_alarm_description"
    Me.txt_alarm_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_alarm_description.Size = New System.Drawing.Size(402, 63)
    Me.txt_alarm_description.TabIndex = 4
    '
    'ef_alarm_code
    '
    Me.ef_alarm_code.DoubleValue = 0
    Me.ef_alarm_code.IntegerValue = 0
    Me.ef_alarm_code.IsReadOnly = False
    Me.ef_alarm_code.Location = New System.Drawing.Point(10, 24)
    Me.ef_alarm_code.Name = "ef_alarm_code"
    Me.ef_alarm_code.Size = New System.Drawing.Size(138, 24)
    Me.ef_alarm_code.SufixText = "Sufix Text"
    Me.ef_alarm_code.SufixTextVisible = True
    Me.ef_alarm_code.TabIndex = 0
    Me.ef_alarm_code.TabStop = False
    Me.ef_alarm_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_alarm_code.TextValue = ""
    Me.ef_alarm_code.TextWidth = 60
    Me.ef_alarm_code.Value = ""
    Me.ef_alarm_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_severity
    '
    Me.gb_severity.Controls.Add(Me.lbl_color_error)
    Me.gb_severity.Controls.Add(Me.lbl_color_warning)
    Me.gb_severity.Controls.Add(Me.opt_error)
    Me.gb_severity.Controls.Add(Me.lbl_color_info)
    Me.gb_severity.Controls.Add(Me.opt_warning)
    Me.gb_severity.Controls.Add(Me.opt_info)
    Me.gb_severity.Location = New System.Drawing.Point(242, 11)
    Me.gb_severity.Name = "gb_severity"
    Me.gb_severity.Size = New System.Drawing.Size(169, 88)
    Me.gb_severity.TabIndex = 2
    Me.gb_severity.TabStop = False
    Me.gb_severity.Text = "xSeverity"
    '
    'lbl_color_error
    '
    Me.lbl_color_error.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_error.Location = New System.Drawing.Point(6, 18)
    Me.lbl_color_error.Name = "lbl_color_error"
    Me.lbl_color_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_error.TabIndex = 0
    '
    'lbl_color_warning
    '
    Me.lbl_color_warning.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_warning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_warning.Location = New System.Drawing.Point(6, 40)
    Me.lbl_color_warning.Name = "lbl_color_warning"
    Me.lbl_color_warning.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_warning.TabIndex = 2
    '
    'opt_error
    '
    Me.opt_error.AutoSize = True
    Me.opt_error.Location = New System.Drawing.Point(27, 18)
    Me.opt_error.Name = "opt_error"
    Me.opt_error.Size = New System.Drawing.Size(61, 17)
    Me.opt_error.TabIndex = 1
    Me.opt_error.Text = "xError"
    Me.opt_error.UseVisualStyleBackColor = True
    '
    'lbl_color_info
    '
    Me.lbl_color_info.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_info.Location = New System.Drawing.Point(6, 63)
    Me.lbl_color_info.Name = "lbl_color_info"
    Me.lbl_color_info.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_info.TabIndex = 4
    '
    'opt_warning
    '
    Me.opt_warning.AutoSize = True
    Me.opt_warning.Location = New System.Drawing.Point(27, 40)
    Me.opt_warning.Name = "opt_warning"
    Me.opt_warning.Size = New System.Drawing.Size(79, 17)
    Me.opt_warning.TabIndex = 3
    Me.opt_warning.Text = "xWarning"
    Me.opt_warning.UseVisualStyleBackColor = True
    '
    'opt_info
    '
    Me.opt_info.AutoSize = True
    Me.opt_info.Location = New System.Drawing.Point(27, 63)
    Me.opt_info.Name = "opt_info"
    Me.opt_info.Size = New System.Drawing.Size(99, 17)
    Me.opt_info.TabIndex = 5
    Me.opt_info.Text = "xInformative"
    Me.opt_info.UseVisualStyleBackColor = True
    '
    'dg_providers
    '
    Me.dg_providers.ForbiddenGroups = CType(resources.GetObject("dg_providers.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.dg_providers.HeightOnExpanded = 300
    Me.dg_providers.Location = New System.Drawing.Point(500, 172)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.SelectedIndex = 0
    Me.dg_providers.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.dg_providers.SetGroupBoxText = ""
    Me.dg_providers.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.dg_providers.ShowGroupBoxLine = True
    Me.dg_providers.ShowGroups = Nothing
    Me.dg_providers.Size = New System.Drawing.Size(401, 54)
    Me.dg_providers.TabIndex = 5
    Me.dg_providers.ThreeStateCheckMode = True
    '
    'txt_description
    '
    Me.txt_description.Location = New System.Drawing.Point(7, 293)
    Me.txt_description.MaxLength = 200
    Me.txt_description.Multiline = True
    Me.txt_description.Name = "txt_description"
    Me.txt_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_description.Size = New System.Drawing.Size(477, 101)
    Me.txt_description.TabIndex = 7
    '
    'lbl_description
    '
    Me.lbl_description.AutoSize = True
    Me.lbl_description.Location = New System.Drawing.Point(11, 275)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(78, 13)
    Me.lbl_description.TabIndex = 6
    Me.lbl_description.Text = "xDescription"
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.opt_with_no_order_with_repetitions)
    Me.gb_type.Controls.Add(Me.opt_with_no_order)
    Me.gb_type.Controls.Add(Me.opt_no_consecutive_with_order)
    Me.gb_type.Controls.Add(Me.opt_consecutive)
    Me.gb_type.Location = New System.Drawing.Point(165, 145)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(319, 117)
    Me.gb_type.TabIndex = 5
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xType"
    '
    'opt_with_no_order_with_repetitions
    '
    Me.opt_with_no_order_with_repetitions.Location = New System.Drawing.Point(67, 88)
    Me.opt_with_no_order_with_repetitions.Name = "opt_with_no_order_with_repetitions"
    Me.opt_with_no_order_with_repetitions.Size = New System.Drawing.Size(224, 16)
    Me.opt_with_no_order_with_repetitions.TabIndex = 3
    Me.opt_with_no_order_with_repetitions.Text = "xWithNoOrderWithRepetitions"
    '
    'opt_with_no_order
    '
    Me.opt_with_no_order.Location = New System.Drawing.Point(67, 66)
    Me.opt_with_no_order.Name = "opt_with_no_order"
    Me.opt_with_no_order.Size = New System.Drawing.Size(224, 16)
    Me.opt_with_no_order.TabIndex = 2
    Me.opt_with_no_order.Text = "xWithNoOrder"
    '
    'opt_no_consecutive_with_order
    '
    Me.opt_no_consecutive_with_order.Location = New System.Drawing.Point(67, 44)
    Me.opt_no_consecutive_with_order.Name = "opt_no_consecutive_with_order"
    Me.opt_no_consecutive_with_order.Size = New System.Drawing.Size(224, 16)
    Me.opt_no_consecutive_with_order.TabIndex = 1
    Me.opt_no_consecutive_with_order.Text = "xNoConsecutiveWithOrder"
    '
    'opt_consecutive
    '
    Me.opt_consecutive.Location = New System.Drawing.Point(67, 22)
    Me.opt_consecutive.Name = "opt_consecutive"
    Me.opt_consecutive.Size = New System.Drawing.Size(224, 16)
    Me.opt_consecutive.TabIndex = 0
    Me.opt_consecutive.Text = "xConsecutive"
    '
    'checked_list_alarms
    '
    Me.checked_list_alarms.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.checked_list_alarms.GroupBoxText = "xCheckedList"
    Me.checked_list_alarms.Location = New System.Drawing.Point(7, 23)
    Me.checked_list_alarms.m_resize_width = 395
    Me.checked_list_alarms.multiChoice = True
    Me.checked_list_alarms.Name = "checked_list_alarms"
    Me.checked_list_alarms.SelectedIndexes = New Integer(-1) {}
    Me.checked_list_alarms.SelectedIndexesList = ""
    Me.checked_list_alarms.SelectedIndexesListLevel2 = ""
    Me.checked_list_alarms.SelectedValuesArray = New String(-1) {}
    Me.checked_list_alarms.SelectedValuesList = ""
    Me.checked_list_alarms.SetLevels = 2
    Me.checked_list_alarms.Size = New System.Drawing.Size(395, 251)
    Me.checked_list_alarms.TabIndex = 0
    Me.checked_list_alarms.ValuesArray = New String(-1) {}
    '
    'gb_schedule
    '
    Me.gb_schedule.Controls.Add(Me.dtp_time_from)
    Me.gb_schedule.Controls.Add(Me.dtp_time_to)
    Me.gb_schedule.Location = New System.Drawing.Point(7, 145)
    Me.gb_schedule.Name = "gb_schedule"
    Me.gb_schedule.Size = New System.Drawing.Size(152, 117)
    Me.gb_schedule.TabIndex = 4
    Me.gb_schedule.TabStop = False
    '
    'dtp_time_from
    '
    Me.dtp_time_from.Checked = True
    Me.dtp_time_from.IsReadOnly = False
    Me.dtp_time_from.Location = New System.Drawing.Point(8, 33)
    Me.dtp_time_from.Name = "dtp_time_from"
    Me.dtp_time_from.ShowCheckBox = False
    Me.dtp_time_from.ShowUpDown = False
    Me.dtp_time_from.Size = New System.Drawing.Size(130, 24)
    Me.dtp_time_from.SufixText = "Sufix Text"
    Me.dtp_time_from.SufixTextVisible = True
    Me.dtp_time_from.TabIndex = 0
    Me.dtp_time_from.TextWidth = 60
    Me.dtp_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_to
    '
    Me.dtp_time_to.Checked = True
    Me.dtp_time_to.IsReadOnly = False
    Me.dtp_time_to.Location = New System.Drawing.Point(8, 66)
    Me.dtp_time_to.Name = "dtp_time_to"
    Me.dtp_time_to.ShowCheckBox = False
    Me.dtp_time_to.ShowUpDown = False
    Me.dtp_time_to.Size = New System.Drawing.Size(130, 24)
    Me.dtp_time_to.SufixText = "Sufix Text"
    Me.dtp_time_to.SufixTextVisible = True
    Me.dtp_time_to.TabIndex = 1
    Me.dtp_time_to.TextWidth = 60
    Me.dtp_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'chk_schedule
    '
    Me.chk_schedule.AutoSize = True
    Me.chk_schedule.Location = New System.Drawing.Point(15, 143)
    Me.chk_schedule.Name = "chk_schedule"
    Me.chk_schedule.Size = New System.Drawing.Size(85, 17)
    Me.chk_schedule.TabIndex = 3
    Me.chk_schedule.Text = "xSchedule"
    Me.chk_schedule.UseVisualStyleBackColor = True
    '
    'dg_elements
    '
    Me.dg_elements.CurrentCol = -1
    Me.dg_elements.CurrentRow = -1
    Me.dg_elements.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_elements.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_elements.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_elements.Location = New System.Drawing.Point(454, 29)
    Me.dg_elements.Name = "dg_elements"
    Me.dg_elements.PanelRightVisible = False
    Me.dg_elements.Redraw = True
    Me.dg_elements.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_elements.Size = New System.Drawing.Size(393, 207)
    Me.dg_elements.Sortable = False
    Me.dg_elements.SortAscending = True
    Me.dg_elements.SortByCol = 0
    Me.dg_elements.TabIndex = 0
    Me.dg_elements.ToolTipped = True
    Me.dg_elements.TopRow = -2
    '
    'btn_add_element
    '
    Me.btn_add_element.Location = New System.Drawing.Point(411, 35)
    Me.btn_add_element.Name = "btn_add_element"
    Me.btn_add_element.Size = New System.Drawing.Size(33, 23)
    Me.btn_add_element.TabIndex = 1
    Me.btn_add_element.Text = ">"
    Me.btn_add_element.UseVisualStyleBackColor = True
    '
    'btn_clear
    '
    Me.btn_clear.Location = New System.Drawing.Point(454, 243)
    Me.btn_clear.Name = "btn_clear"
    Me.btn_clear.Size = New System.Drawing.Size(110, 28)
    Me.btn_clear.TabIndex = 6
    Me.btn_clear.Text = "xClear"
    Me.btn_clear.UseVisualStyleBackColor = True
    '
    'btn_remove_element
    '
    Me.btn_remove_element.Location = New System.Drawing.Point(411, 65)
    Me.btn_remove_element.Name = "btn_remove_element"
    Me.btn_remove_element.Size = New System.Drawing.Size(33, 23)
    Me.btn_remove_element.TabIndex = 2
    Me.btn_remove_element.Text = "<"
    Me.btn_remove_element.UseVisualStyleBackColor = True
    '
    'btn_move_down
    '
    Me.btn_move_down.Location = New System.Drawing.Point(853, 65)
    Me.btn_move_down.Name = "btn_move_down"
    Me.btn_move_down.Size = New System.Drawing.Size(35, 23)
    Me.btn_move_down.TabIndex = 5
    Me.btn_move_down.Text = "\/"
    Me.btn_move_down.UseVisualStyleBackColor = True
    '
    'btn_move_up
    '
    Me.btn_move_up.Location = New System.Drawing.Point(853, 35)
    Me.btn_move_up.Name = "btn_move_up"
    Me.btn_move_up.Size = New System.Drawing.Size(35, 23)
    Me.btn_move_up.TabIndex = 4
    Me.btn_move_up.Text = "/\"
    Me.btn_move_up.UseVisualStyleBackColor = True
    '
    'gb_elements
    '
    Me.gb_elements.Controls.Add(Me.checked_list_alarms)
    Me.gb_elements.Controls.Add(Me.btn_move_down)
    Me.gb_elements.Controls.Add(Me.btn_add_element)
    Me.gb_elements.Controls.Add(Me.btn_move_up)
    Me.gb_elements.Controls.Add(Me.btn_clear)
    Me.gb_elements.Controls.Add(Me.btn_remove_element)
    Me.gb_elements.Controls.Add(Me.dg_elements)
    Me.gb_elements.Location = New System.Drawing.Point(7, 400)
    Me.gb_elements.Name = "gb_elements"
    Me.gb_elements.Size = New System.Drawing.Size(900, 287)
    Me.gb_elements.TabIndex = 10
    Me.gb_elements.TabStop = False
    Me.gb_elements.Text = "xElements"
    '
    'ef_last_find
    '
    Me.ef_last_find.DoubleValue = 0
    Me.ef_last_find.IntegerValue = 0
    Me.ef_last_find.IsReadOnly = False
    Me.ef_last_find.Location = New System.Drawing.Point(27, 14)
    Me.ef_last_find.Name = "ef_last_find"
    Me.ef_last_find.Size = New System.Drawing.Size(285, 24)
    Me.ef_last_find.SufixText = "Sufix Text"
    Me.ef_last_find.SufixTextVisible = True
    Me.ef_last_find.TabIndex = 0
    Me.ef_last_find.TabStop = False
    Me.ef_last_find.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_last_find.TextValue = ""
    Me.ef_last_find.TextWidth = 100
    Me.ef_last_find.Value = ""
    Me.ef_last_find.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_num_occurrencies
    '
    Me.ef_num_occurrencies.DoubleValue = 0
    Me.ef_num_occurrencies.IntegerValue = 0
    Me.ef_num_occurrencies.IsReadOnly = False
    Me.ef_num_occurrencies.Location = New System.Drawing.Point(27, 40)
    Me.ef_num_occurrencies.Name = "ef_num_occurrencies"
    Me.ef_num_occurrencies.Size = New System.Drawing.Size(180, 24)
    Me.ef_num_occurrencies.SufixText = "Sufix Text"
    Me.ef_num_occurrencies.SufixTextVisible = True
    Me.ef_num_occurrencies.TabIndex = 1
    Me.ef_num_occurrencies.TabStop = False
    Me.ef_num_occurrencies.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_num_occurrencies.TextValue = ""
    Me.ef_num_occurrencies.TextWidth = 100
    Me.ef_num_occurrencies.Value = ""
    Me.ef_num_occurrencies.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_life_time
    '
    Me.ef_life_time.DoubleValue = 0
    Me.ef_life_time.IntegerValue = 0
    Me.ef_life_time.IsReadOnly = False
    Me.ef_life_time.Location = New System.Drawing.Point(27, 66)
    Me.ef_life_time.Name = "ef_life_time"
    Me.ef_life_time.Size = New System.Drawing.Size(180, 24)
    Me.ef_life_time.SufixText = "Sufix Text"
    Me.ef_life_time.SufixTextVisible = True
    Me.ef_life_time.TabIndex = 2
    Me.ef_life_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_life_time.TextValue = ""
    Me.ef_life_time.TextWidth = 100
    Me.ef_life_time.Value = ""
    Me.ef_life_time.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_misc
    '
    Me.gb_misc.Controls.Add(Me.lbl_min)
    Me.gb_misc.Controls.Add(Me.ef_last_find)
    Me.gb_misc.Controls.Add(Me.ef_life_time)
    Me.gb_misc.Controls.Add(Me.ef_num_occurrencies)
    Me.gb_misc.Location = New System.Drawing.Point(165, 48)
    Me.gb_misc.Name = "gb_misc"
    Me.gb_misc.Size = New System.Drawing.Size(319, 95)
    Me.gb_misc.TabIndex = 2
    Me.gb_misc.TabStop = False
    '
    'lbl_min
    '
    Me.lbl_min.AutoSize = True
    Me.lbl_min.Location = New System.Drawing.Point(213, 72)
    Me.lbl_min.Name = "lbl_min"
    Me.lbl_min.Size = New System.Drawing.Size(33, 13)
    Me.lbl_min.TabIndex = 11
    Me.lbl_min.Text = "xMin"
    '
    'frm_pattern_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1008, 701)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_pattern_edit"
    Me.Text = "xPatternEdit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_alarm.ResumeLayout(False)
    Me.gb_alarm.PerformLayout()
    Me.gb_mailing.ResumeLayout(False)
    Me.gb_mailing.PerformLayout()
    Me.gb_severity.ResumeLayout(False)
    Me.gb_severity.PerformLayout()
    Me.gb_type.ResumeLayout(False)
    Me.gb_schedule.ResumeLayout(False)
    Me.gb_elements.ResumeLayout(False)
    Me.gb_misc.ResumeLayout(False)
    Me.gb_misc.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents gb_alarm As System.Windows.Forms.GroupBox
  Friend WithEvents ef_alarm_code As GUI_Controls.uc_entry_field
  Friend WithEvents gb_severity As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_color_error As System.Windows.Forms.Label
  Friend WithEvents lbl_color_warning As System.Windows.Forms.Label
  Friend WithEvents opt_error As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_color_info As System.Windows.Forms.Label
  Friend WithEvents opt_warning As System.Windows.Forms.RadioButton
  Friend WithEvents opt_info As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_description As System.Windows.Forms.TextBox
  Friend WithEvents lbl_alarm_description As System.Windows.Forms.Label
  Friend WithEvents txt_alarm_description As System.Windows.Forms.TextBox
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_no_consecutive_with_order As System.Windows.Forms.RadioButton
  Friend WithEvents opt_consecutive As System.Windows.Forms.RadioButton
  Friend WithEvents dg_providers As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents checked_list_alarms As GUI_Controls.uc_checked_list
  Friend WithEvents ef_alarm_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_schedule As System.Windows.Forms.GroupBox
  Friend WithEvents opt_with_no_order As System.Windows.Forms.RadioButton
  Friend WithEvents opt_with_no_order_with_repetitions As System.Windows.Forms.RadioButton
  Friend WithEvents dg_elements As GUI_Controls.uc_grid
  Friend WithEvents btn_add_element As System.Windows.Forms.Button
  Friend WithEvents btn_clear As System.Windows.Forms.Button
  Friend WithEvents btn_remove_element As System.Windows.Forms.Button
  Friend WithEvents btn_move_down As System.Windows.Forms.Button
  Friend WithEvents btn_move_up As System.Windows.Forms.Button
  Friend WithEvents gb_elements As System.Windows.Forms.GroupBox
  Friend WithEvents ef_num_occurrencies As GUI_Controls.uc_entry_field
  Friend WithEvents ef_last_find As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents chk_schedule As System.Windows.Forms.CheckBox
  Friend WithEvents ef_life_time As GUI_Controls.uc_entry_field
  Friend WithEvents gb_misc As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_min As System.Windows.Forms.Label
  Friend WithEvents lbl_address_list As System.Windows.Forms.Label
  Friend WithEvents txt_alarm_mailing_address_list As System.Windows.Forms.TextBox
  Friend WithEvents lbl_subject As System.Windows.Forms.Label
  Friend WithEvents txt_alarm_mailing_subject As System.Windows.Forms.TextBox
  Friend WithEvents gb_mailing As System.Windows.Forms.GroupBox
  Friend WithEvents chk_mailing As System.Windows.Forms.CheckBox
End Class
