'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_class_II_draw_audit
' DESCRIPTION : This screen allows to browse the draw audit data
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-JUL-2008  TJG    Initial version
' 15-DIC-2008  SLE    Modified query to avoid timeouts 
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 22-MAY-2013  JCA    Add new View GAMES_V2
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_class_II_draw_audit
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_draw_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents lbl_include As System.Windows.Forms.Label
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents opt_several_terminals As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_terminal As GUI_Controls.uc_combo
  Friend WithEvents gb_draw As System.Windows.Forms.GroupBox
  Friend WithEvents ef_draw_id As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_combo_games As GUI_Controls.uc_combo_games
  Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
  Friend WithEvents opt_all_terminals As System.Windows.Forms.RadioButton
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_draw_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.lbl_include = New System.Windows.Forms.Label
    Me.gb_terminal = New System.Windows.Forms.GroupBox
    Me.opt_several_terminals = New System.Windows.Forms.RadioButton
    Me.cmb_terminal = New GUI_Controls.uc_combo
    Me.opt_all_terminals = New System.Windows.Forms.RadioButton
    Me.gb_draw = New System.Windows.Forms.GroupBox
    Me.ef_draw_id = New GUI_Controls.uc_entry_field
    Me.Uc_combo_games = New GUI_Controls.uc_combo_games
    Me.ComboBox1 = New System.Windows.Forms.ComboBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_draw_date.SuspendLayout()
    Me.gb_terminal.SuspendLayout()
    Me.gb_draw.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ComboBox1)
    Me.panel_filter.Controls.Add(Me.Uc_combo_games)
    Me.panel_filter.Controls.Add(Me.gb_draw)
    Me.panel_filter.Controls.Add(Me.gb_terminal)
    Me.panel_filter.Controls.Add(Me.lbl_include)
    Me.panel_filter.Controls.Add(Me.gb_draw_date)
    Me.panel_filter.Size = New System.Drawing.Size(1012, 143)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_draw_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_include, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_draw, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.Uc_combo_games, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ComboBox1, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 147)
    Me.panel_data.Size = New System.Drawing.Size(1012, 496)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1006, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1006, 4)
    '
    'gb_draw_date
    '
    Me.gb_draw_date.Controls.Add(Me.dtp_to)
    Me.gb_draw_date.Controls.Add(Me.dtp_from)
    Me.gb_draw_date.Location = New System.Drawing.Point(6, 59)
    Me.gb_draw_date.Name = "gb_draw_date"
    Me.gb_draw_date.Size = New System.Drawing.Size(243, 72)
    Me.gb_draw_date.TabIndex = 1
    Me.gb_draw_date.TabStop = False
    Me.gb_draw_date.Text = "xDraw Date"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(227, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(227, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'lbl_include
    '
    Me.lbl_include.AutoSize = True
    Me.lbl_include.Location = New System.Drawing.Point(11, 159)
    Me.lbl_include.Name = "lbl_include"
    Me.lbl_include.Size = New System.Drawing.Size(39, 13)
    Me.lbl_include.TabIndex = 10
    Me.lbl_include.Text = "xText"
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.opt_several_terminals)
    Me.gb_terminal.Controls.Add(Me.cmb_terminal)
    Me.gb_terminal.Controls.Add(Me.opt_all_terminals)
    Me.gb_terminal.Location = New System.Drawing.Point(264, 59)
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.Size = New System.Drawing.Size(282, 72)
    Me.gb_terminal.TabIndex = 4
    Me.gb_terminal.TabStop = False
    Me.gb_terminal.Text = "xTerminal"
    '
    'opt_several_terminals
    '
    Me.opt_several_terminals.Location = New System.Drawing.Point(8, 14)
    Me.opt_several_terminals.Name = "opt_several_terminals"
    Me.opt_several_terminals.Size = New System.Drawing.Size(86, 24)
    Me.opt_several_terminals.TabIndex = 1
    Me.opt_several_terminals.TabStop = True
    Me.opt_several_terminals.Text = "xTerminal"
    '
    'cmb_terminal
    '
    Me.cmb_terminal.AllowUnlistedValues = False
    Me.cmb_terminal.AutoCompleteMode = False
    Me.cmb_terminal.IsReadOnly = False
    Me.cmb_terminal.Location = New System.Drawing.Point(92, 14)
    Me.cmb_terminal.Name = "cmb_terminal"
    Me.cmb_terminal.SelectedIndex = -1
    Me.cmb_terminal.Size = New System.Drawing.Size(184, 24)
    Me.cmb_terminal.SufixText = "Sufix Text"
    Me.cmb_terminal.SufixTextVisible = True
    Me.cmb_terminal.TabIndex = 4
    Me.cmb_terminal.TextVisible = False
    Me.cmb_terminal.TextWidth = 0
    '
    'opt_all_terminals
    '
    Me.opt_all_terminals.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_terminals.Name = "opt_all_terminals"
    Me.opt_all_terminals.Size = New System.Drawing.Size(174, 24)
    Me.opt_all_terminals.TabIndex = 2
    Me.opt_all_terminals.Text = "xAll"
    '
    'gb_draw
    '
    Me.gb_draw.Controls.Add(Me.ef_draw_id)
    Me.gb_draw.Location = New System.Drawing.Point(6, 6)
    Me.gb_draw.Name = "gb_draw"
    Me.gb_draw.Size = New System.Drawing.Size(184, 47)
    Me.gb_draw.TabIndex = 12
    Me.gb_draw.TabStop = False
    Me.gb_draw.Text = "xDraw"
    '
    'ef_draw_id
    '
    Me.ef_draw_id.DoubleValue = 0
    Me.ef_draw_id.IntegerValue = 0
    Me.ef_draw_id.IsReadOnly = False
    Me.ef_draw_id.Location = New System.Drawing.Point(16, 14)
    Me.ef_draw_id.Name = "ef_draw_id"
    Me.ef_draw_id.PlaceHolder = Nothing
    Me.ef_draw_id.Size = New System.Drawing.Size(160, 24)
    Me.ef_draw_id.SufixText = "Sufix Text"
    Me.ef_draw_id.SufixTextVisible = True
    Me.ef_draw_id.TabIndex = 0
    Me.ef_draw_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_id.TextValue = ""
    Me.ef_draw_id.TextWidth = 58
    Me.ef_draw_id.Value = ""
    Me.ef_draw_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_combo_games
    '
    Me.Uc_combo_games.Location = New System.Drawing.Point(552, 59)
    Me.Uc_combo_games.Name = "Uc_combo_games"
    Me.Uc_combo_games.Size = New System.Drawing.Size(272, 72)
    Me.Uc_combo_games.TabIndex = 13
    '
    'ComboBox1
    '
    Me.ComboBox1.FormattingEnabled = True
    Me.ComboBox1.Location = New System.Drawing.Point(272, 20)
    Me.ComboBox1.Name = "ComboBox1"
    Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
    Me.ComboBox1.TabIndex = 14
    '
    'frm_class_II_draw_audit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1020, 647)
    Me.Name = "frm_class_II_draw_audit"
    Me.Text = "frm_class_II_draw_audit"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_draw_date.ResumeLayout(False)
    Me.gb_terminal.ResumeLayout(False)
    Me.gb_draw.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_DRAW_ID As Integer = 0
  'Private Const SQL_COLUMN_DRAW_DATETIME As Integer = 1
  Private Const SQL_COLUMN_PLAY_ID As Integer = 1
  Private Const SQL_COLUMN_PLAY_DATETIME As Integer = 2
  Private Const SQL_COLUMN_GAME_ID As Integer = 3
  Private Const SQL_COLUMN_GAME_NAME As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 5
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 6
  Private Const SQL_COLUMN_DENOMINATION As Integer = 7
  Private Const SQL_COLUMN_PLAYED_CREDITS As Integer = 8
  Private Const SQL_COLUMN_WON_CREDITS As Integer = 9
  Private Const SQL_COLUMN_JACKPOT_BOUND_CREDITS As Integer = 10
  Private Const SQL_COLUMN_PRIZE_LIST As Integer = 11
  Private Const SQL_COLUMN_CARDS As Integer = 12

  Private Const GRID_COLUMN_DRAW_ID As Integer = 0
  Private Const GRID_COLUMN_PLAY_DATETIME As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const GRID_COLUMN_GAME_NAME As Integer = 3
  Private Const GRID_COLUMN_DENOMINATION As Integer = 4
  Private Const GRID_COLUMN_PLAYED_CREDITS As Integer = 5
  Private Const GRID_COLUMN_WON_CREDITS As Integer = 6
  Private Const GRID_COLUMN_JACKPOT_BOUND_CREDITS As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_draw_id As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_game As String

#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CLASS_II_DRAW_AUDIT

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_CLASS_II.GetString(249)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CLASS_II.GetString(4)

    ' Draw Id
    Me.gb_draw.Text = GLB_NLS_GUI_CLASS_II.GetString(200)
    Me.ef_draw_id.Text = GLB_NLS_GUI_CLASS_II.GetString(204)
    Call Me.ef_draw_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 16)

    ' Draw Date
    Me.gb_draw_date.Text = GLB_NLS_GUI_CLASS_II.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_CLASS_II.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_CLASS_II.GetString(203)

    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminals 
    Me.gb_terminal.Text = GLB_NLS_GUI_CLASS_II.GetString(208)
    Me.opt_several_terminals.Text = GLB_NLS_GUI_CLASS_II.GetString(209)
    Me.opt_all_terminals.Text = GLB_NLS_GUI_CLASS_II.GetString(210)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Terminals
    Call SetCombo(Me.cmb_terminal, m_select_terminals)
    Me.cmb_terminal.Enabled = False

    ' Set combo with Games
    'Call SetCombo(Me.cmb_game, SELECT_GAMES)
    'Call SetCombo(Me.cmb_game, SELECT_GAMES_V2)
    Call Me.Uc_combo_games.Init()

    ' lbl_included
    Me.lbl_include.Text = ""

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT
        Call ShowAuditDetails()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Sub GUI_AfterLastRow()

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function  ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String
    Dim table_id As DataTable = Nothing

    str_sql = "select " _
                 & " DAP_DRAW_ID" _
                 & ", DAP_PLAY_ID" _
                 & ", DAP_PLAY_DATETIME" _
                 & ", DAP_GAME_ID" _
                 & ", DAP_GAME_NAME" _
                 & ", DAP_TERMINAL_ID" _
                 & ", DAP_TERMINAL_NAME" _
                 & ", DAP_DENOMINATION" _
                 & ", DAP_PLAYED_CREDITS" _
                 & ", DAP_WON_CREDITS" _
                 & ", DAP_JACKPOT_BOUND_CREDITS" _
                 & " from C2_DRAW_AUDIT_PLAYS_V inner join C2_DRAW_AUDIT on dap_draw_id = da_draw_id"

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY DAP_DRAW_ID DESC"

    Return str_sql

  End Function  ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Draw Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_ID).Value = DbRow.Value(SQL_COLUMN_DRAW_ID)

    ' Draw DateTime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAY_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_PLAY_DATETIME), , ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Play
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_GAME_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DENOMINATION))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_CREDITS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_CREDITS), 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_CREDITS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WON_CREDITS), 0)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT_BOUND_CREDITS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_JACKPOT_BOUND_CREDITS), 0)

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_draw_id

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(232), m_draw_id)
    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(230), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(231), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(208), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(211), m_game)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_CLASS_II.GetString(249)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_draw_id = ""
    m_date_from = ""
    m_date_to = ""
    m_terminals = ""
    m_game = ""

    ' Draw Id
    m_draw_id = Me.ef_draw_id.TextValue.Trim

    ' Draw Date
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Terminals
    If Me.opt_all_terminals.Checked Then
      m_terminals = Me.opt_all_terminals.Text
    Else
      m_terminals = Me.cmb_terminal.TextValue
    End If

    ' Game
    If Me.Uc_combo_games.GetSelectedId() = String.Empty Then
      m_game = Me.Uc_combo_games.opt_all_game.Text
    Else
      m_game = Me.Uc_combo_games.GetSelectedValue
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Draw Id
      .Column(GRID_COLUMN_DRAW_ID).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(400)
      .Column(GRID_COLUMN_DRAW_ID).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(401)
      .Column(GRID_COLUMN_DRAW_ID).Width = 1750
      .Column(GRID_COLUMN_DRAW_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DRAW_ID).IsMerged = True

      ' Draw Date
      .Column(GRID_COLUMN_PLAY_DATETIME).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(400)
      .Column(GRID_COLUMN_PLAY_DATETIME).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(402)
      .Column(GRID_COLUMN_PLAY_DATETIME).Width = 2050
      .Column(GRID_COLUMN_PLAY_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Game Name
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(220)
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(221)
      .Column(GRID_COLUMN_GAME_NAME).Width = 2600
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Name 
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(220)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(222)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = 1880
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(220)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(223)
      .Column(GRID_COLUMN_DENOMINATION).Width = 1450
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Credits
      .Column(GRID_COLUMN_PLAYED_CREDITS).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(224)
      .Column(GRID_COLUMN_PLAYED_CREDITS).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(225)
      .Column(GRID_COLUMN_PLAYED_CREDITS).Width = 1500
      .Column(GRID_COLUMN_PLAYED_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Credits
      .Column(GRID_COLUMN_WON_CREDITS).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(224)
      .Column(GRID_COLUMN_WON_CREDITS).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(226)
      .Column(GRID_COLUMN_WON_CREDITS).Width = 1500
      .Column(GRID_COLUMN_WON_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot Bound Credits
      .Column(GRID_COLUMN_JACKPOT_BOUND_CREDITS).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(224)
      .Column(GRID_COLUMN_JACKPOT_BOUND_CREDITS).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(227)
      .Column(GRID_COLUMN_JACKPOT_BOUND_CREDITS).Width = 1450
      .Column(GRID_COLUMN_JACKPOT_BOUND_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub SetDefaultValues()

    Me.ef_draw_id.TextValue = ""

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.opt_all_terminals.Checked = True
    Me.cmb_terminal.Enabled = False

    Call Me.Uc_combo_games.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim aux_text As String

    ' Draw Id 
    aux_text = Me.ef_draw_id.TextValue.Trim
    If aux_text <> "" Then
      ' Take Value instead of TextValue to remove the EF formatting
      aux_text = Me.ef_draw_id.Value.ToString
      str_where = str_where & " AND (DAP_DRAW_ID = " & aux_text & ")"
    End If

    ' Date From
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (DA_DRAW_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ")"
    End If

    ' Date To
    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (DA_DRAW_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ")"
    End If

    ' Terminal
    If Me.opt_several_terminals.Checked = True Then
      str_where = str_where & " AND (DAP_TERMINAL_ID = " & Me.cmb_terminal.Value & ")"
    End If

    ' Game
    'If Me.opt_several_game.Checked = True Then
    '  'str_where = str_where & " AND (DAP_GAME_ID = " & Me.cmb_game.Value & ")"
    '  If (Me.cmb_game.TextValue = "UNKNOWN") Then
    '    str_where = str_where & " AND (DAP_GAME_ID IS NULL"
    '    If Me.cmb_game.Value <> -1 Then
    '      str_where = str_where & " OR DAP_GAME_ID = " & Me.cmb_game.Value
    '    End If
    '    str_where = str_where & " )"
    '  Else
    '    str_where = str_where & " AND (DAP_GAME_ID = " & Me.cmb_game.Value & ")"
    '  End If

    'End If

    ' Filter Game
    If Me.Uc_combo_games.GetSelectedId <> String.Empty Then
      str_where = str_where & " AND DAP_GAME_ID = " & Me.Uc_combo_games.GetSelectedId
    End If

    ' Adapt Where clause
    If str_where.Length > 0 Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where

    End If

    Return str_where

  End Function ' GetSqlWhere


  ' PURPOSE : Open Draw Details form to display audit details
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowAuditDetails()

    Dim idx_row As Int32
    Dim row_selected As Boolean
    Dim draw_id As Int64
    Dim frm_edit As Object

    ' Get the selected row
    row_selected = False
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        row_selected = True

        Exit For
      End If
    Next

    If Not row_selected Then
      ' No selected row 
      Call Me.Grid.Focus()

      Exit Sub
    End If

    frm_edit = New frm_class_II_draw_audit_plays

    ' Show the selected draw
    draw_id = Me.Grid.Cell(idx_row, GRID_COLUMN_DRAW_ID).Value
    Call frm_edit.ShowEditItem(draw_id)

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowAuditDetails

#End Region  ' Private Functions

#Region "Events"

  Private Sub opt_several_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_terminals.Click
    Me.cmb_terminal.Enabled = True
  End Sub

  Private Sub opt_all_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_terminals.Click
    Me.cmb_terminal.Enabled = False
  End Sub

#End Region ' Events

End Class
