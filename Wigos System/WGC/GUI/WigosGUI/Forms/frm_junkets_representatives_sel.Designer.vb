﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_representatives_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.chkStatusEnabled = New System.Windows.Forms.CheckBox()
    Me.chkInternalYes = New System.Windows.Forms.CheckBox()
    Me.ef_code = New GUI_Controls.uc_entry_field()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.chkInternalNo = New System.Windows.Forms.CheckBox()
    Me.chkStatusDisabled = New System.Windows.Forms.CheckBox()
    Me.gb_internal_employee = New System.Windows.Forms.GroupBox()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_internal_employee.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_internal_employee)
    Me.panel_filter.Controls.Add(Me.ef_name)
    Me.panel_filter.Controls.Add(Me.ef_code)
    Me.panel_filter.Size = New System.Drawing.Size(964, 99)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_code, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_internal_employee, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 103)
    Me.panel_data.Size = New System.Drawing.Size(964, 483)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(958, 30)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(958, 4)
    '
    'chkStatusEnabled
    '
    Me.chkStatusEnabled.AutoSize = True
    Me.chkStatusEnabled.Location = New System.Drawing.Point(17, 20)
    Me.chkStatusEnabled.Name = "chkStatusEnabled"
    Me.chkStatusEnabled.Size = New System.Drawing.Size(114, 17)
    Me.chkStatusEnabled.TabIndex = 0
    Me.chkStatusEnabled.Text = "xStatusEnabled"
    Me.chkStatusEnabled.UseVisualStyleBackColor = True
    '
    'chkInternalYes
    '
    Me.chkInternalYes.AutoSize = True
    Me.chkInternalYes.Location = New System.Drawing.Point(17, 20)
    Me.chkInternalYes.Name = "chkInternalYes"
    Me.chkInternalYes.Size = New System.Drawing.Size(98, 17)
    Me.chkInternalYes.TabIndex = 0
    Me.chkInternalYes.Text = "xInternalYes"
    Me.chkInternalYes.UseVisualStyleBackColor = True
    '
    'ef_code
    '
    Me.ef_code.DoubleValue = 0.0R
    Me.ef_code.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_code.IntegerValue = 0
    Me.ef_code.IsReadOnly = False
    Me.ef_code.Location = New System.Drawing.Point(16, 21)
    Me.ef_code.Name = "ef_code"
    Me.ef_code.PlaceHolder = Nothing
    Me.ef_code.Size = New System.Drawing.Size(281, 25)
    Me.ef_code.SufixText = "Sufix Text"
    Me.ef_code.SufixTextVisible = True
    Me.ef_code.TabIndex = 0
    Me.ef_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_code.TextValue = ""
    Me.ef_code.TextWidth = 60
    Me.ef_code.Value = ""
    Me.ef_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(16, 52)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(281, 25)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 60
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chkInternalNo
    '
    Me.chkInternalNo.AutoSize = True
    Me.chkInternalNo.Location = New System.Drawing.Point(17, 48)
    Me.chkInternalNo.Name = "chkInternalNo"
    Me.chkInternalNo.Size = New System.Drawing.Size(93, 17)
    Me.chkInternalNo.TabIndex = 1
    Me.chkInternalNo.Text = "xInternalNo"
    Me.chkInternalNo.UseVisualStyleBackColor = True
    '
    'chkStatusDisabled
    '
    Me.chkStatusDisabled.AutoSize = True
    Me.chkStatusDisabled.Location = New System.Drawing.Point(17, 48)
    Me.chkStatusDisabled.Name = "chkStatusDisabled"
    Me.chkStatusDisabled.Size = New System.Drawing.Size(118, 17)
    Me.chkStatusDisabled.TabIndex = 1
    Me.chkStatusDisabled.Text = "xStatusDisabled"
    Me.chkStatusDisabled.UseVisualStyleBackColor = True
    '
    'gb_internal_employee
    '
    Me.gb_internal_employee.Controls.Add(Me.chkInternalYes)
    Me.gb_internal_employee.Controls.Add(Me.chkInternalNo)
    Me.gb_internal_employee.Location = New System.Drawing.Point(318, 6)
    Me.gb_internal_employee.Name = "gb_internal_employee"
    Me.gb_internal_employee.Size = New System.Drawing.Size(134, 73)
    Me.gb_internal_employee.TabIndex = 2
    Me.gb_internal_employee.TabStop = False
    Me.gb_internal_employee.Text = "xInternalEmployee"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chkStatusEnabled)
    Me.gb_status.Controls.Add(Me.chkStatusDisabled)
    Me.gb_status.Location = New System.Drawing.Point(473, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(134, 73)
    Me.gb_status.TabIndex = 3
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'frm_junkets_representatives_sel
    '
    Me.ClientSize = New System.Drawing.Size(972, 590)
    Me.Name = "frm_junkets_representatives_sel"
    Me.Text = "frm_junkets_representatives_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_internal_employee.ResumeLayout(False)
    Me.gb_internal_employee.PerformLayout()
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chkStatusEnabled As System.Windows.Forms.CheckBox
  Friend WithEvents chkInternalYes As System.Windows.Forms.CheckBox
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_code As GUI_Controls.uc_entry_field
  Friend WithEvents chkStatusDisabled As System.Windows.Forms.CheckBox
  Friend WithEvents chkInternalNo As System.Windows.Forms.CheckBox
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents gb_internal_employee As System.Windows.Forms.GroupBox

End Class
