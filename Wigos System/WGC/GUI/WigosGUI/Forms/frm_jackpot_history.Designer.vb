<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpot_history
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_jackpot_type = New System.Windows.Forms.GroupBox
    Me.chk_jackpot3 = New System.Windows.Forms.CheckBox
    Me.chk_jackpot2 = New System.Windows.Forms.CheckBox
    Me.chk_jackpot1 = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_jackpot_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_jackpot_type)
    Me.panel_filter.Size = New System.Drawing.Size(1007, 131)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_jackpot_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 135)
    Me.panel_data.Size = New System.Drawing.Size(1007, 338)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1001, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1001, 4)
    '
    'gb_jackpot_type
    '
    Me.gb_jackpot_type.Controls.Add(Me.chk_jackpot3)
    Me.gb_jackpot_type.Controls.Add(Me.chk_jackpot2)
    Me.gb_jackpot_type.Controls.Add(Me.chk_jackpot1)
    Me.gb_jackpot_type.Location = New System.Drawing.Point(279, 8)
    Me.gb_jackpot_type.Name = "gb_jackpot_type"
    Me.gb_jackpot_type.Size = New System.Drawing.Size(124, 90)
    Me.gb_jackpot_type.TabIndex = 1
    Me.gb_jackpot_type.TabStop = False
    Me.gb_jackpot_type.Text = "xJackpot Type"
    '
    'chk_jackpot3
    '
    Me.chk_jackpot3.AutoSize = True
    Me.chk_jackpot3.Location = New System.Drawing.Point(18, 65)
    Me.chk_jackpot3.Name = "chk_jackpot3"
    Me.chk_jackpot3.Size = New System.Drawing.Size(83, 17)
    Me.chk_jackpot3.TabIndex = 2
    Me.chk_jackpot3.Text = "xJackpot3"
    Me.chk_jackpot3.UseVisualStyleBackColor = True
    '
    'chk_jackpot2
    '
    Me.chk_jackpot2.AutoSize = True
    Me.chk_jackpot2.Location = New System.Drawing.Point(18, 42)
    Me.chk_jackpot2.Name = "chk_jackpot2"
    Me.chk_jackpot2.Size = New System.Drawing.Size(83, 17)
    Me.chk_jackpot2.TabIndex = 1
    Me.chk_jackpot2.Text = "xJackpot2"
    Me.chk_jackpot2.UseVisualStyleBackColor = True
    '
    'chk_jackpot1
    '
    Me.chk_jackpot1.AutoSize = True
    Me.chk_jackpot1.Location = New System.Drawing.Point(18, 19)
    Me.chk_jackpot1.Name = "chk_jackpot1"
    Me.chk_jackpot1.Size = New System.Drawing.Size(83, 17)
    Me.chk_jackpot1.TabIndex = 0
    Me.chk_jackpot1.Text = "xJackpot1"
    Me.chk_jackpot1.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chk_terminal_location.Location = New System.Drawing.Point(279, 104)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(250, 19)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_jackpot_history
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1015, 477)
    Me.Name = "frm_jackpot_history"
    Me.Text = "frm_jackpot_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_jackpot_type.ResumeLayout(False)
    Me.gb_jackpot_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_jackpot_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_jackpot3 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot1 As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
