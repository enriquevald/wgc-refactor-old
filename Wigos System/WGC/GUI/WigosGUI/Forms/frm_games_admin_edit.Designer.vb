<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_games_admin_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_games_admin_edit))
    Me.lbl_name = New System.Windows.Forms.Label()
    Me.lbl_category = New System.Windows.Forms.Label()
    Me.cmb_category = New GUI_Controls.uc_combo()
    Me.txt_name = New System.Windows.Forms.TextBox()
    Me.Uc_imageList = New GUI_Controls.uc_image()
    Me.chk_app = New System.Windows.Forms.CheckBox()
    Me.lblVisible = New System.Windows.Forms.Label()
    Me.lbl_code = New System.Windows.Forms.Label()
    Me.txt_code = New System.Windows.Forms.TextBox()
    Me.lbl_url = New System.Windows.Forms.Label()
    Me.txt_url = New System.Windows.Forms.TextBox()
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_url)
    Me.panel_data.Controls.Add(Me.txt_url)
    Me.panel_data.Controls.Add(Me.lbl_code)
    Me.panel_data.Controls.Add(Me.txt_code)
    Me.panel_data.Controls.Add(Me.lblVisible)
    Me.panel_data.Controls.Add(Me.chk_app)
    Me.panel_data.Controls.Add(Me.Uc_imageList)
    Me.panel_data.Controls.Add(Me.lbl_name)
    Me.panel_data.Controls.Add(Me.lbl_category)
    Me.panel_data.Controls.Add(Me.cmb_category)
    Me.panel_data.Controls.Add(Me.txt_name)
    Me.panel_data.Location = New System.Drawing.Point(3, 3)
    Me.panel_data.Size = New System.Drawing.Size(602, 292)
    '
    'lbl_name
    '
    Me.lbl_name.AutoSize = True
    Me.lbl_name.Location = New System.Drawing.Point(3, 37)
    Me.lbl_name.Name = "lbl_name"
    Me.lbl_name.Size = New System.Drawing.Size(47, 13)
    Me.lbl_name.TabIndex = 22
    Me.lbl_name.Text = "xName"
    '
    'lbl_category
    '
    Me.lbl_category.AutoSize = True
    Me.lbl_category.Location = New System.Drawing.Point(3, 9)
    Me.lbl_category.Name = "lbl_category"
    Me.lbl_category.Size = New System.Drawing.Size(67, 13)
    Me.lbl_category.TabIndex = 21
    Me.lbl_category.Text = "xCategory"
    '
    'cmb_category
    '
    Me.cmb_category.AllowUnlistedValues = False
    Me.cmb_category.AutoCompleteMode = False
    Me.cmb_category.IsReadOnly = False
    Me.cmb_category.Location = New System.Drawing.Point(74, 4)
    Me.cmb_category.Name = "cmb_category"
    Me.cmb_category.SelectedIndex = -1
    Me.cmb_category.Size = New System.Drawing.Size(189, 24)
    Me.cmb_category.SufixText = "Sufix Text"
    Me.cmb_category.SufixTextVisible = True
    Me.cmb_category.TabIndex = 0
    Me.cmb_category.TextCombo = Nothing
    Me.cmb_category.TextVisible = False
    Me.cmb_category.TextWidth = 0
    '
    'txt_name
    '
    Me.txt_name.Location = New System.Drawing.Point(76, 34)
    Me.txt_name.Multiline = True
    Me.txt_name.Name = "txt_name"
    Me.txt_name.Size = New System.Drawing.Size(185, 24)
    Me.txt_name.TabIndex = 1
    '
    'Uc_imageList
    '
    Me.Uc_imageList.AutoSize = True
    Me.Uc_imageList.ButtonDeleteEnabled = True
    Me.Uc_imageList.FreeResize = False
    Me.Uc_imageList.Image = Nothing
    Me.Uc_imageList.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList.ImageName = Nothing
    Me.Uc_imageList.Location = New System.Drawing.Point(276, 5)
    Me.Uc_imageList.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList.Name = "Uc_imageList"
    Me.Uc_imageList.Size = New System.Drawing.Size(303, 284)
    Me.Uc_imageList.TabIndex = 23
    '
    'chk_app
    '
    Me.chk_app.AutoSize = True
    Me.chk_app.Location = New System.Drawing.Point(76, 214)
    Me.chk_app.Name = "chk_app"
    Me.chk_app.Size = New System.Drawing.Size(15, 14)
    Me.chk_app.TabIndex = 4
    Me.chk_app.UseVisualStyleBackColor = True
    '
    'lblVisible
    '
    Me.lblVisible.AutoSize = True
    Me.lblVisible.Location = New System.Drawing.Point(3, 213)
    Me.lblVisible.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
    Me.lblVisible.Name = "lblVisible"
    Me.lblVisible.Size = New System.Drawing.Size(51, 13)
    Me.lblVisible.TabIndex = 24
    Me.lblVisible.Text = "xVisible"
    '
    'lbl_code
    '
    Me.lbl_code.AutoSize = True
    Me.lbl_code.Location = New System.Drawing.Point(3, 183)
    Me.lbl_code.Name = "lbl_code"
    Me.lbl_code.Size = New System.Drawing.Size(47, 13)
    Me.lbl_code.TabIndex = 26
    Me.lbl_code.Text = "xName"
    '
    'txt_code
    '
    Me.txt_code.Location = New System.Drawing.Point(76, 180)
    Me.txt_code.Multiline = True
    Me.txt_code.Name = "txt_code"
    Me.txt_code.Size = New System.Drawing.Size(185, 24)
    Me.txt_code.TabIndex = 3
    '
    'lbl_url
    '
    Me.lbl_url.AutoSize = True
    Me.lbl_url.Location = New System.Drawing.Point(3, 67)
    Me.lbl_url.Name = "lbl_url"
    Me.lbl_url.Size = New System.Drawing.Size(47, 13)
    Me.lbl_url.TabIndex = 28
    Me.lbl_url.Text = "xName"
    '
    'txt_url
    '
    Me.txt_url.Location = New System.Drawing.Point(76, 64)
    Me.txt_url.Multiline = True
    Me.txt_url.Name = "txt_url"
    Me.txt_url.Size = New System.Drawing.Size(185, 108)
    Me.txt_url.TabIndex = 2
    '
    'frm_games_admin_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(702, 298)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_games_admin_edit"
    Me.Padding = New System.Windows.Forms.Padding(3)
    Me.Text = "frm_games_admin_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Uc_imageList As GUI_Controls.uc_image
  Friend WithEvents lbl_name As System.Windows.Forms.Label
  Friend WithEvents lbl_category As System.Windows.Forms.Label
  Friend WithEvents cmb_category As GUI_Controls.uc_combo
  Friend WithEvents txt_name As System.Windows.Forms.TextBox
  Friend WithEvents chk_app As System.Windows.Forms.CheckBox
  Friend WithEvents lblVisible As System.Windows.Forms.Label
  Friend WithEvents lbl_url As System.Windows.Forms.Label
  Friend WithEvents txt_url As System.Windows.Forms.TextBox
  Friend WithEvents lbl_code As System.Windows.Forms.Label
  Friend WithEvents txt_code As System.Windows.Forms.TextBox
End Class
