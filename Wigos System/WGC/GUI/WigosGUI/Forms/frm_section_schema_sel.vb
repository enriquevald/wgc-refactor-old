'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_section_schema_sel
' DESCRIPTION:   Sections schema for app mobile
' AUTHOR:        Sergio Soria
' CREATION DATE: 16-SEPT-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-SEPT-2016  SDS    Initial version
' 19-DIC-2016   DMT    Product Backlog Item 21626:Correcciones en el GUI
' 23-DIC-2017   DMT    Product Backlog Item 22394:Secciones - Listado - Ordenar los items padres e hijos por n�mero de orden
' 24-DIC-2017   DMT    Product Backlog Item 22395:Secciones - Listado - Pintar las secciones padres en el listado
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports System.Net
Imports System.Text
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class frm_section_schema_sel
  Inherits GUI_Controls.frm_base_sel

#Region "Members"

  Private m_campaign_custom_filter_controls As New List(Of Control)
  Private m_filt_campaign As String
  Private m_filt_campaign_date As String
  Private m_filt_campaign_search_filter As String
  Private m_filt_date_from As String
  Private m_filt_date_to As String
  Private m_filt_enabled As Boolean
  Private m_filt_disabled As Boolean
  Private m_filt_name As String
  Private m_filt_section As String
  Private m_filt_status As String
  Private m_dataTableSections As DataTable

#End Region

  Public Class SectionSchema
    Public Property SectionSchemaId() As Long
      Get
        Return m_SectionSchemaId
      End Get
      Set(value As Long)
        m_SectionSchemaId = value
      End Set
    End Property
    Private m_SectionSchemaId As Long
    Public Property ParentId() As System.Nullable(Of Long)
      Get
        Return m_ParentId
      End Get
      Set(value As System.Nullable(Of Long))
        m_ParentId = value
      End Set
    End Property
    Private m_ParentId As System.Nullable(Of Long)
    Public Property Name() As [String]
      Get
        Return m_Name
      End Get
      Set(value As [String])
        m_Name = value
      End Set
    End Property
    Private m_Name As [String]
    Public Property MainTitle() As [String]
      Get
        Return m_MainTitle
      End Get
      Set(value As [String])
        m_MainTitle = value
      End Set
    End Property
    Private m_MainTitle As [String]
    Public Property MainSubTitle() As [String]
      Get
        Return m_MainSubTitle
      End Get
      Set(value As [String])
        m_MainSubTitle = value
      End Set
    End Property
    Private m_MainSubTitle As [String]
    Public Property Title() As [String]
      Get
        Return m_Title
      End Get
      Set(value As [String])
        m_Title = value
      End Set
    End Property
    Private m_Title As [String]
    Public Property SubTitle() As [String]
      Get
        Return m_SubTitle
      End Get
      Set(value As [String])
        m_SubTitle = value
      End Set
    End Property
    Private m_SubTitle As [String]
    Public Property Abstract() As [String]
      Get
        Return m_Abstract
      End Get
      Set(value As [String])
        m_Abstract = value
      End Set
    End Property
    Private m_Abstract As [String]
    Public Property Description() As [String]
      Get
        Return m_Description
      End Get
      Set(value As [String])
        m_Description = value
      End Set
    End Property
    Private m_Description As [String]
    Public Property HeadlineText() As [String]
      Get
        Return m_HeadlineText
      End Get
      Set(value As [String])
        m_HeadlineText = value
      End Set
    End Property
    Private m_HeadlineText As [String]
    Public Property ButtonText() As [String]
      Get
        Return m_ButtonText
      End Get
      Set(value As [String])
        m_ButtonText = value
      End Set
    End Property
    Private m_ButtonText As [String]
    Public Property Active() As Boolean
      Get
        Return m_Active
      End Get
      Set(value As Boolean)
        m_Active = value
      End Set
    End Property
    Private m_Active As Boolean
    Public Property Deleted() As Boolean
      Get
        Return m_Deleted
      End Get
      Set(value As Boolean)
        m_Deleted = value
      End Set
    End Property
    Private m_Deleted As Boolean
    Public Property BackgroundImageId() As System.Nullable(Of Long)
      Get
        Return m_BackgroundImageId
      End Get
      Set(value As System.Nullable(Of Long))
        m_BackgroundImageId = value
      End Set
    End Property
    Private m_BackgroundImageId As System.Nullable(Of Long)

    Public Property IconImageId() As System.Nullable(Of Long)
      Get
        Return m_IconImageId
      End Get
      Set(value As System.Nullable(Of Long))
        m_IconImageId = value
      End Set
    End Property
    Private m_IconImageId As System.Nullable(Of Long)

    Public Property LanguageId() As Nullable(Of Long)
      Get
        Return m_LanguageId
      End Get
      Set(value As Nullable(Of Long))
        m_LanguageId = value
      End Set
    End Property
    Private m_LanguageId As Nullable(Of Long)
  End Class

#Region " CONSTANT "

  Private Const GRID_COLUMNS As Integer = 9
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_WIDTH_PARENT As Integer = 1800
  Private Const GRID_WIDTH_NAME As Integer = 1800
  Private Const GRID_WIDTH_TITLE As Integer = 5500
  Private Const GRID_WIDTH_ACTIVE As Integer = 800
  Private Const GRID_WIDTH_ORDER As Integer = 800

  Private Const GRID_COL_INDEX As Integer = 0
  Private Const GRID_COL_ID As Integer = 1
  Private Const GRID_COL_PARENT_NAME As Integer = 2
  Private Const GRID_COL_NAME As Integer = 3
  Private Const GRID_COL_MAIN_TITLE As Integer = 4
  Private Const GRID_COL_SUBTITLE As Integer = 5
  Private Const GRID_COL_ACTIVE As Integer = 6
  Private Const GRID_COL_DELETED As Integer = 7
  Private Const GRID_COL_ORDER As Integer = 8

  Private Const SQL_COL_ID As Integer = 0
  Private Const SQL_COL_PARENT As Integer = 1
  Private Const SQL_COL_NAME As Integer = 2
  Private Const SQL_COL_MAIN_TITLE As Integer = 3
  Private Const SQL_COL_TITLE As Integer = 5
  Private Const SQL_COL_ACTIVE As Integer = 11
  Private Const SQL_COL_PARENT_NAME As Integer = 23

  Private Const SQL_COL_ORDER As Integer = 16

  Private Const LEN_NAME As Integer = 50

#End Region

#Region " OVERRIDES "

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Dim _active As String

    Me.Grid.Cell(RowIndex, GRID_COL_ID).Value = DbRow.Value(SQL_COL_ID).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_PARENT_NAME).Value = DbRow.Value(SQL_COL_PARENT_NAME).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_NAME).Value = DbRow.Value(SQL_COL_NAME).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_MAIN_TITLE).Value = DbRow.Value(SQL_COL_MAIN_TITLE).ToString
    Me.Grid.Cell(RowIndex, GRID_COL_ORDER).Value = DbRow.Value(SQL_COL_ORDER).ToString

    If CType(DbRow.Value(SQL_COL_ACTIVE), Boolean) Then
      _active = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7901) ' Active
    Else
      _active = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7902) ' Inactive
    End If

    Me.Grid.Cell(RowIndex, GRID_COL_ACTIVE).Value = _active
    Return True


  End Function

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SECTION_SCHEMA_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Section Schema
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7596)   ' Label form caption

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    'Enabled
    gb_sections.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5149)
    gb_group_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7598)
    chk_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7901)
    chk_deactive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7902)

    Me.lblParent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7817)
    Me.lblTitle.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7602)

    'Filters
    Me.txt_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_NAME)
    Me.txt_title.Enabled = True

    ' INITIALIZE LABEL CAPTIONS

    'Title
    Me.txt_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7602)

    'Section Schema
    Me.uc_section_parent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7817)

    SeccionSchemaComboFill()

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls


  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetCustomDataTable() As DataTable
    m_dataTableSections = New DataTable
    LoadSection("Home")
    m_dataTableSections = ApplyFilter(m_dataTableSections)

    Return m_dataTableSections
  End Function 'GUI_GetCustomDataTable

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer
    Dim _has_changed As Boolean

    _table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"
    _has_changed = False
    _max_rows = GUI_MaxRows()

    Try

      'SaveSelectedGridIds()

      _table = GUI_GetCustomDataTable()

      Call GUI_ReportUpdateFilters()

      If _table Is Nothing Then

        Return
      End If

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      For _idx_row = 0 To _table.Rows.Count - 1

        If _idx_row >= Me.Grid.NumRows Then
          Me.Grid.AddRow()
        End If

        _db_row = New CLASS_DB_ROW(_table.Rows(_idx_row))
        Me.Grid.Redraw = False

        If GUI_SetupRow(_idx_row, _db_row) Then
          Me.Grid.Redraw = True
        End If

        PaintColorFather(_idx_row, _db_row)

      Next

      While _idx_row < Me.Grid.NumRows
        Me.Grid.DeleteRow(_idx_row)
      End While

      Call GUI_AfterLastRow()

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cash Monitor", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True
    Finally

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Date selection 

    Return True
  End Function ' GUI_FilterCheck  


  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _sch_id As Integer
    'Dim _sch_name As String
    Dim _frm_edit As frm_new_section_schema_edit  'GUI_Controls.frm_base_edit
    Dim _rows_to_move() As Integer

    _rows_to_move = Me.Grid.SelectedRows()

    ' ICS 18-DEC-2012: Check if there is a row selected
    If _rows_to_move Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    _idx_row = _rows_to_move(0)
    _sch_id = Me.Grid.Cell(_idx_row, GRID_COL_ID).Value
    '_adv_name = Me.Grid.Cell(_idx_row, GRID_COL_NAME).Value

    _frm_edit = New frm_new_section_schema_edit

    Call _frm_edit.ShowEditItem(_sch_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem


  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call ShowNewSchemaForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE



  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(253), m_filt_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7612), m_filt_section)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(280), m_filt_status)

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_filt_name = ""
    m_filt_section = ""
    m_filt_status = ""

    'Section
    If uc_section_parent.SelectedIndex > 0 Then
      m_filt_section = uc_section_parent.TextValue
    End If

    'Name
    m_filt_name = txt_title.Value

    ' Enabled
    If (chk_active.Checked And Not chk_deactive.Checked) Then
      m_filt_status = GLB_NLS_GUI_CONTROLS.GetString(281)
    ElseIf (Not chk_active.Checked And chk_deactive.Checked) Then
      m_filt_status = GLB_NLS_GUI_CONTROLS.GetString(282)
    Else
      m_filt_status = GLB_NLS_GUI_CONTROLS.GetString(474)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region

#End Region

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False


      With .Column(GRID_COL_INDEX)
        .Header(0).Text = " "
        .Header(1).Text = " "
        .Width = 200
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' ID
      With .Column(GRID_COL_ID)
        .Width = 0
      End With
      'PARENT NAME
      With .Column(GRID_COL_PARENT_NAME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7817) ' Label Parent Name
        .Width = GRID_WIDTH_PARENT
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With
      'NAME
      With .Column(GRID_COL_NAME)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7601) ' Label Name
        .Width = GRID_WIDTH_NAME
        .Width = 0
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With
      'PARENT
      With .Column(GRID_COL_DELETED)
        .Width = 0
      End With

      'SUBTITLE
      With .Column(GRID_COL_SUBTITLE)
        .Width = 0
      End With

      'MAIN TITLE
      With .Column(GRID_COL_MAIN_TITLE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7602) ' Header Main Title
        .Width = GRID_WIDTH_TITLE
      End With

      ' ACTIVE
      With .Column(GRID_COL_ACTIVE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7598) ' Estado
        .Width = GRID_WIDTH_ACTIVE
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      With .Column(GRID_COL_ORDER)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) ' Orden
        .Width = GRID_WIDTH_ORDER
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With
    End With
  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()
    txt_title.Value = ""
    uc_section_parent.SelectedIndex = 0
    Me.chk_active.Checked = True
    Me.chk_deactive.Checked = False
  End Sub


  ' PURPOSE: Get SQL WHERE 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Sub ShowNewSchemaForm()

    Dim frm_edit As frm_new_section_schema_edit

    frm_edit = New frm_new_section_schema_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewSchemaForm

  Private Sub SeccionSchemaComboFill()

    Me.uc_section_parent.Clear()
    Me.uc_section_parent.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7903))

    Dim _url As String
    Dim _response, _responseSanitized As String
    Dim _data As DataTable
    Dim _jarray As JArray
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/getParentsByLanguage/1"

    Dim _api As New CLS_API(_url)

    _response = _api.getData()
    _responseSanitized = _api.sanitizeData(_response)
    _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
    _data = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())
    uc_section_parent.Add(_data, 0, 3)

  End Sub ' SeccionSchemaComboFill

  Private Sub PaintColorFather(ByVal RowIndex As Integer, _
                               ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Try
      If CType(DbRow.Value(SQL_COL_PARENT_NAME), String) = "" Then
        Me.Grid.Row([RowIndex]).BackColor = Color.Yellow
      End If
    Catch ex As Exception

    End Try

  End Sub ' PaintColorFather

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

  Public Function GetUrlForApi(ByVal metodo As String)

    Dim _url As String

    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/" + metodo + "/1"
    ''_url = "/sectionsschema/" + metodo + "/1"
    Return _url

  End Function ' GetUrlForApi

  Public Function FindChilds(ByVal dtParents As DataTable) As DataTable

    Dim _url As String
    Dim _api As CLS_API
    Dim dtChilds As DataTable
    Dim _response, _responseSanitized As String
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    Dim _jarray As JArray
    Dim dataTableRelationship As DataTable
    dataTableRelationship = New DataTable()
    Try

      _url = GetUrlForApi("getSchemaOrganizedChilds")

      _api = New CLS_API(_url)
      _response = _api.getData()

      _responseSanitized = _api.sanitizeData(_response)
      _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
      dtChilds = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())

      ''aca lo que hago es relacionar el parentId con el sectionschema de los padres
      dataTableRelationship = dtParents.Clone()
      For Each rowParent As DataRow In dtParents.Rows
        dataTableRelationship.ImportRow(rowParent)
        For Each rowChilds As DataRow In dtChilds.Rows
          If rowParent("sectionSchemaId") = rowChilds("parentId") Then
            dataTableRelationship.ImportRow(rowChilds)
            ''busco los hijos de los hijos
            For Each rowChildsChilds As DataRow In dtChilds.Rows
              If rowChilds("sectionSchemaId") = rowChildsChilds("parentId") Then
                dataTableRelationship.ImportRow(rowChildsChilds)

                ''Busco 3 niveles
                For Each rowChildsChildsChilds As DataRow In dtChilds.Rows
                  If rowChildsChilds("sectionSchemaId") = rowChildsChildsChilds("parentId") Then
                    dataTableRelationship.ImportRow(rowChildsChildsChilds)
                  End If
                Next

              End If
            Next
            ''**************
          End If
        Next
      Next

      ''*****************************************************

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

    dataTableRelationship = ApplyFilter(dataTableRelationship)

    Return dataTableRelationship

  End Function ' FindChilds



  Private Sub LoadSection(ByVal parentName As String)

    Dim _url As String
    Dim _api As CLS_API
    Dim dtSections As DataTable
    Dim _response, _responseSanitized As String
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    Dim _jarray As JArray

    Try

      _url = GetUrlForApi("getSchemaOrganizedChilds/" & parentName)

      _api = New CLS_API(_url)
      _response = _api.getData()

      _responseSanitized = _api.sanitizeData(_response)
      _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
      dtSections = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())
      If m_dataTableSections.Rows.Count = 0 Then
        m_dataTableSections = dtSections.Clone
        m_dataTableSections.Rows.Clear()
      End If
      If (Not dtSections Is Nothing) AndAlso dtSections.Rows.Count > 0 Then
        For Each rowParent As DataRow In dtSections.Rows
          m_dataTableSections.ImportRow(rowParent)

          LoadSection(rowParent("name"))
        Next
      End If
    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))
    End Try

  End Sub



  Public Function ApplyFilter(ByVal dataTableRelationship As DataTable) As DataTable
    Dim _active
    Dim dataTableFilter As DataTable = New DataTable()
    dataTableFilter = dataTableRelationship.Clone()

    If (chk_active.Checked And Not chk_deactive.Checked) Then
      _active = "True"
    ElseIf (Not chk_active.Checked And chk_deactive.Checked) Then
      _active = "False"
    Else
      _active = ""
    End If

    'Aplicar filtro de busqueda por Title And Section
    If txt_title.Value.Length > 0 And uc_section_parent.Value > 0 Then 'si filtro por los 2
      For Each row As DataRow In dataTableRelationship.Rows
        If Not row("parentId") Is DBNull.Value Then
          ' aplico el filtro
          If Not _active = "" Then
            If row("active").ToString() = _active And row("parentId") = uc_section_parent.Value.ToString() And row("mainTitle").ToString().ToUpper().Contains(txt_title.Value.ToString().ToUpper()) Then
              dataTableFilter.ImportRow(row)
              'busco los hijos de los hijos para la busqueda
              For Each rowX As DataRow In dataTableRelationship.Rows
                If Not rowX("parentId") Is DBNull.Value Then
                  If row("sectionSchemaId") = rowX("parentId") Then
                    dataTableFilter.ImportRow(rowX)
                  End If
                End If
              Next
            End If
          Else
            If row("parentId") = uc_section_parent.Value.ToString() And row("mainTitle").ToString().ToUpper().Contains(txt_title.Value.ToString().ToUpper()) Then
              dataTableFilter.ImportRow(row)
              'busco los hijos de los hijos para la busqueda
              For Each dtRel As DataRow In dataTableRelationship.Rows
                If Not dtRel("parentId") Is DBNull.Value Then
                  If row("sectionSchemaId") = dtRel("parentId") Then
                    dataTableFilter.ImportRow(dtRel)
                  End If
                End If
              Next
            End If
          End If
        End If
      Next ''******************************************************
    ElseIf (txt_title.Value.Length = 0 And uc_section_parent.Value > 0) Then 'si filtro por el combo
      For Each row As DataRow In dataTableRelationship.Rows
        ' aplico el filtro
        If Not _active = "" Then
          If Not row("parentId") Is DBNull.Value Then
            If row("active").ToString() = _active And row("parentId") = uc_section_parent.Value.ToString() Then
              ''busco los hijos de los hijos para la busqueda
              dataTableFilter.ImportRow(row)
              For Each dtRel As DataRow In dataTableRelationship.Rows
                If Not dtRel("parentId") Is DBNull.Value Then
                  If row("sectionSchemaId") = dtRel("parentId") Then
                    dataTableFilter.ImportRow(dtRel)
                  End If
                End If
              Next
            End If
          ElseIf row("active").ToString() = _active And row("sectionSchemaId") = uc_section_parent.Value.ToString() Then
            dataTableFilter.ImportRow(row)
          End If
        ElseIf Not row("parentId") Is DBNull.Value Then
          If row("parentId") = uc_section_parent.Value.ToString() Then
            dataTableFilter.ImportRow(row)
            'busco los hijos de los hijos para la busqueda
            For Each dtRel As DataRow In dataTableRelationship.Rows
              If Not dtRel("parentId") Is DBNull.Value Then
                If row("sectionSchemaId") = dtRel("parentId") Then
                  dataTableFilter.ImportRow(dtRel)
                  'busco los hijos de los hijos para la busqueda
                  For Each rowy As DataRow In dataTableRelationship.Rows
                    If Not rowy("parentId") Is DBNull.Value Then
                      If dtRel("sectionSchemaId") = rowy("parentId") Then
                        dataTableFilter.ImportRow(rowy)
                      End If
                    End If
                  Next
                End If
              End If
            Next
          End If
        ElseIf row("sectionSchemaId") = uc_section_parent.Value.ToString() Then
          dataTableFilter.ImportRow(row)
        End If
      Next ''******************************************************

    ElseIf (txt_title.Value.Length > 0 And uc_section_parent.Value = 0) Then 'si filtro por el textbox

      If Not _active = "" Then
        For Each row As DataRow In dataTableRelationship.Rows
          If row("active").ToString() = _active And row("mainTitle").ToString().ToUpper().Contains(txt_title.Value.ToString().ToUpper()) Then
            dataTableFilter.ImportRow(row)
          ElseIf row("active").ToString() = _active And row("parentName").ToString().ToUpper().Contains(txt_title.Value.ToString().ToUpper()) Then
            dataTableFilter.ImportRow(row)
          End If
        Next
      Else
        For Each row As DataRow In dataTableRelationship.Rows
          If row("mainTitle").ToString().ToUpper().Contains(txt_title.Value.ToString().ToUpper()) Then
            dataTableFilter.ImportRow(row)
          ElseIf row("parentName").ToString().ToUpper().Contains(txt_title.Value.ToString().ToUpper()) Then
            dataTableFilter.ImportRow(row)
          End If
        Next
      End If

    Else 'sino si solamente filtra por los checkbox y ambos estan o no seleccionados
      If Not _active = "" Then
        For Each row As DataRow In dataTableRelationship.Rows
          If row("active").ToString() = _active Then
            dataTableFilter.ImportRow(row)
          End If
        Next
      Else
        dataTableFilter = dataTableRelationship
      End If
    End If
    Return dataTableFilter
  End Function ' ApplyFilter

#End Region

#Region " Events "

  Protected Function ReadSectionSchema() As DataTable
    Dim _url As String
    Dim _api As CLS_API
    Dim _tbl As DataTable
    Dim _response, _responseSanitized As String
    Dim _items() As String = {"backgroundImage", "iconImage", "parent", "childs"}
    Dim _jarray As JArray
    Dim dataTableRelationship As DataTable
    dataTableRelationship = New DataTable()

    Try

      _url = GetUrlForApi("getSchemaOrganizedParent")

      _api = New CLS_API(_url)
      _response = _api.getData()

      _responseSanitized = _api.sanitizeData(_response)
      _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
      _tbl = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())
      dataTableRelationship = ApplyFilter(_tbl) ' FindChilds(_tbl)

    Catch
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7662) + " " + GLB_NLS_GUI_PLAYER_TRACKING.Id(7661), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7662), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7661))

    End Try

    Return dataTableRelationship

  End Function 'ReadSectionSchema

#End Region

End Class