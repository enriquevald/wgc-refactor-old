'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_terminals_pending
'
' DESCRIPTION : Machine Management (aka Provisioning) of pending terminals
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 16-AUG-2010 MBF    Initial version
' 19-MAR-2013 LEM    Fixed Bug #652: Database error if terminal name include character ('). 
' 15-APR-2013 DMR    Fixed Bug #714: Terminals Pending and 3GS: Error date format
' 22-MAY-2013 ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters
' 05-JUL-2013 JCOR   Call to frm_terminals_sel changed to frm_terminals.
' 17-SEP-2013 ANG    Fixed Bug #WIG-208
' 10-OCT-2013 ACM    Fixed Bug WIG-277 Check for non printable characters in terminals name
' 19-NOV-2013 LJM    Changes for auditing forms
' 13-MAR-2014 AMF    Fixed Bug WIG-716: duplicate external id
' 12-APR-2014 DHA    Added 'Insert' on Terminal_status for WASS Mode
' 13-OCT-2014 LEM    Fixed Bug WIG-1482: Don't show created terminal name correctly.
' 14-JAN-2015 SGB    Set related type and related id of terminal in auditor.
' 20-JAN-2015 FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 27-JAN-2016 ETP    Fixed Bug 8711 Dual Currency: Need column TE_ISO_CODE.
' 05-JAN-2017 CCG    Bug 21898:GUI: Terminales pendientes - mensaje de error incorrecto
' 09-MAY-2017 ATB    PBI 27207: Terminals report - Date of Replacement / PBI 27206:Terminals report - Date of Creation
' 10-MAY-2017 ATB    PBI 27207: Terminals report - Date of Replacement / PBI 27206:Terminals report - Date of Creation
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_terminals_pending
  Inherits frm_base_print

#Region " Constants "

  'Terminal Type
  Private Const TERMINAL_TYPE_TERMINAL As Integer = 1

  Private Const TYPE_PENDING As Integer = 1
  Private Const TYPE_TERMINALS As Integer = 2
  Private Const TYPE_GLOBAL As Integer = 3

  ' DataGrid PENDING Columns
  Private Const GRID_COLUMN_PENDING_VENDOR_ID As Integer = 0
  Private Const GRID_COLUMN_PENDING_SERIAL_NUMBER As Integer = 1
  Private Const GRID_COLUMN_PENDING_TYPE As Integer = 2
  Private Const GRID_COLUMN_PENDING_REPORTED As Integer = 3
  Private Const GRID_COLUMN_PENDING_IGNORE As Integer = 4
  Private Const GRID_PENDING_COLUMNS As Integer = 5

  Private Const GRID_COLUMN_TERMINAL_PROVIDER_ID As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_SERIAL_NUMBER As Integer = 2
  Private Const GRID_TERMINAL_COLUMNS As Integer = 3

  ' Width
  Private Const GRID_WIDTH_VENDOR_ID As Integer = 120
  Private Const GRID_WIDTH_SERIAL_NUMBER As Integer = 180
  Private Const GRID_WIDTH_TP_REPORTED As Integer = 180
  Private Const GRID_WIDTH_TP_IGNORE As Integer = 60

#End Region ' Constants

#Region " Members "

  Shared m_sql_conn As SqlConnection

  Private m_sql_adap_pending As SqlDataAdapter
  Private m_sql_dataset_pending As DataSet
  Private m_data_view_pending As DataView
  Private m_sql_commandbuilder_pending As SqlCommandBuilder

  Private m_sql_adap_terminals As SqlDataAdapter
  Private m_sql_dataset_terminals As DataSet
  Private m_data_view_terminals As DataView
  Private m_sql_commandbuilder_terminals As SqlCommandBuilder

  Private m_terms_added As Boolean

  ' Original DataGrid.RowHeaderWidth for 3gs.
  ' After RowError, RowHeaderWidth is increased. Use this value to restore it.
  Public m_original_row_header_width As Integer

  'filters
  Private m_report_vendor As String
  Private m_report_serial As String
  Private m_report_ignored As String

  Private m_system_mode As SYSTEM_MODE

  Private m_replaced_pending_terminals As List(Of Int32)

#End Region ' Members

#Region " Public functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Undelete a previous delete pending terminal row.
  '          Must be Public because Class MyDataGrid calls it.
  '
  '  PARAMS:
  '     - INPUT:
  '           - dataRowView As DataRowView
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Sub UndeleteAssignedRow(ByVal dataRowView As DataRowView)
    Dim rows_deleted() As DataRow
    Dim row As DataRow

    rows_deleted = m_sql_dataset_pending.Tables(0).Select("", "", DataViewRowState.Deleted)

    For Each row In rows_deleted
      If Not dataRowView.Item("TP_ID") Is DBNull.Value Then
        If dataRowView.Item("TP_ID") = row.Item("TP_ID", DataRowVersion.Original) Then
          row.RejectChanges()

          Exit For
        End If
      End If
    Next

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write
  End Sub ' UndeleteAssignedRow

#End Region  ' Public functions

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_PENDING
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  'PURPOSE: Executed just before closing form.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    '' Force to grid to lost focus
    '' to allow to detect changes on uncommited cells
    Me.ActiveControl = Nothing

    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If

  End Sub 'GUI_Closing

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ExecuteQuery()
    Dim user_msg As Boolean
    Dim data_table_changes_terminals_pending As New DataTable
    Dim data_table_changes_terminals As New DataTable

    Try
      ' Check the filter
      If GUI_FilterCheck() Then

        Windows.Forms.Cursor.Current = Cursors.WaitCursor

        ' If pending changes MsgBox
        user_msg = False
        If Not IsNothing(m_data_view_pending) Then
          data_table_changes_terminals_pending = m_data_view_pending.Table.GetChanges

          If Not IsNothing(data_table_changes_terminals_pending) Then
            If data_table_changes_terminals_pending.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
              user_msg = True
            End If
          End If
        End If

        If Not IsNothing(m_data_view_terminals) And Not user_msg Then
          data_table_changes_terminals = m_data_view_terminals.Table.GetChanges

          If Not IsNothing(data_table_changes_terminals) Then
            If data_table_changes_terminals.Rows.Count > 0 Then
              If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
                Return
              End If
            End If
          End If
        End If

        ' Invalidate datagrid
        GUI_ClearGrid()

        ' Execute the query        
        Call ExecuteQuery()

        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write

        'GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_3gs.Count > 0 _
        '                                              And Me.Permissions.Write

        'GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = m_data_view_3gs.Count > 0

      End If

    Catch exception As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "TerminalsPending", _
                            "GUI_ExecuteQuery", _
                            "", _
                            "", _
                            "", _
                            exception.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub 'GUI_ExecuteQuery

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Read System mode
    m_system_mode = WSI.Common.Misc.SystemMode()

    ' - Form title
    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(463)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_AUDITOR.GetString(19)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_AUDITOR.GetString(18)

    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    gb_terminal_pending.Text = GLB_NLS_GUI_AUDITOR.GetString(345)
    ef_terminal_pending_vendor.Text = GLB_NLS_GUI_AUDITOR.GetString(341)
    ef_terminal_pending_serial_number.Text = GLB_NLS_GUI_AUDITOR.GetString(331)
    chk_hide_ignored.Text = GLB_NLS_GUI_AUDITOR.GetString(353)
    cmb_source.Text = GLB_NLS_GUI_AUDITOR.GetString(349)

    ' Possible source for pending terminals.
    cmb_source.Add(0, GLB_NLS_GUI_AUDITOR.GetString(355))
    cmb_source.Add(1, GLB_NLS_GUI_AUDITOR.GetString(356))
    cmb_source.Add(2, GLB_NLS_GUI_AUDITOR.GetString(357))
    cmb_source.Add(3, GLB_NLS_GUI_AUDITOR.GetString(358))
    cmb_source.Add(4, GLB_NLS_GUI_AUDITOR.GetString(359))
    cmb_source.Add(5, GLB_NLS_GUI_AUDITOR.GetString(360))

    Call ef_terminal_pending_vendor.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call ef_terminal_pending_serial_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)

    dg_terminals_pending.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(347)
    dg_terminals_added.CaptionText = GLB_NLS_GUI_AUDITOR.GetString(465)

    dg_terminals_pending.Enabled = False

    m_original_row_header_width = dg_terminals_added.RowHeaderWidth

    m_replaced_pending_terminals = New List(Of Int32)

  End Sub ' GUI_InitControls

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_SaveChanges()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call GUI_ReplacePendingTerminal()

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call GUI_AssignPendingTerminal()

      Case Else
        Call GUI_ReportUpdateFilters(New System.Windows.Forms.DataGrid)
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()

    ef_terminal_pending_vendor.Value = ""
    ef_terminal_pending_serial_number.Value = ""
    chk_hide_ignored.Checked = True
    cmb_source.Value = 0

  End Sub 'GUI_FilterReset

  ' PURPOSE: Initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(345) & " " & GLB_NLS_GUI_CONFIGURATION.GetString(205), m_report_vendor)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(345) & " " & GLB_NLS_GUI_CONFIGURATION.GetString(214), m_report_serial)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(353), m_report_ignored)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(1) = 1500
  End Sub

  ' PURPOSE: Update report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - MyDataGrid As DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters(ByVal MyDataGrid As DataGrid)

    Call MyBase.GUI_ReportUpdateFilters(MyDataGrid)

    m_report_vendor = Me.ef_terminal_pending_vendor.Value
    m_report_serial = Me.ef_terminal_pending_vendor.Value

    If Me.chk_hide_ignored.Checked Then
      m_report_ignored = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_report_ignored = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region  ' Overrides

#Region " Private Functions "

  ' PURPOSE: Execute function Update for the proper SqlAdapter (using Type parameter) and according to RowState.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Type As Integer
  '           - RowState As DataViewRowState
  '     - OUTPUT:
  '           - MsgError As String
  '
  ' RETURNS:
  '     - True:  If inserts are ok
  '     - False: If inserts are NOT ok

  Private Function ExecuteAdapterUpdate(ByVal Type As Integer, ByVal RowState As DataViewRowState, ByRef MsgError As String) As Boolean
    Dim rows_update As DataRow()
    Dim dataset As DataSet
    Dim sqladap As SqlDataAdapter
    Dim dataview As DataView
    Dim nr As Integer

    Select Case Type
      Case TYPE_PENDING
        dataset = m_sql_dataset_pending
        sqladap = m_sql_adap_pending
        dataview = m_data_view_pending
      Case TYPE_TERMINALS
        dataset = m_sql_dataset_terminals
        sqladap = m_sql_adap_terminals
        dataview = m_data_view_terminals
      Case Else
        MsgError = "Internal Error!"
        Return False
    End Select

    rows_update = dataset.Tables(0).Select(dataview.RowFilter, "", RowState)

    If rows_update.Length > 0 Then

      nr = sqladap.Update(rows_update)

      If rows_update.Length <> nr Then
        MsgError = GetMsgErrorFromRow(rows_update)
        Return False
      End If

      If Type = TYPE_TERMINALS And RowState = DataViewRowState.Added Then
        m_terms_added = True
      End If
    End If

    Return True
  End Function ' ExecuteAdapterUpdate

  ' PURPOSE: Update terminal status for new terminals as bloqued by signature.
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowState As DataViewRowState
  '           - Trx As SqlTransaction
  '     - OUTPUT:
  '           - MsgError As String
  '
  ' RETURNS:
  '     - True:  If inserts are ok
  '     - False: If inserts are NOT ok

  Private Function ExecuteUpdateTerminalStatusWASS(ByVal TerminalsInserted As DataRow(), ByVal Trx As SqlTransaction) As Boolean
    If TerminalsInserted.Length > 0 Then
      For Each _row As DataRow In TerminalsInserted
        If Not WSI.Common.TerminalStatusFlags.DB_SetValue(WSI.Common.TerminalStatusFlags.BITMASK_TYPE.Machine_status, _row("TE_TERMINAL_ID"), WSI.Common.TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE, Trx) Then
          Return False
        End If
      Next
    End If

    Return True
  End Function ' ExecuteAdapterUpdate

  ' PURPOSE: Delete GAME_METERS from substitute terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx As SqlTransaction
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub DeleteSubstitutesGameMeters(ByVal SqlTrx As SqlTransaction)
    Dim rows_orig As DataRow()
    Dim game_meters_to_delete As DataTable
    Dim row As DataRow
    Dim sqladap As SqlDataAdapter
    Dim sql_txt As String
    Dim nr As Integer

    rows_orig = m_sql_dataset_terminals.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent)

    If rows_orig.Length > 0 Then

      game_meters_to_delete = m_sql_dataset_terminals.Tables(0).Clone()
      For Each row In rows_orig
        game_meters_to_delete.ImportRow(row)
      Next
      game_meters_to_delete.AcceptChanges()

      For Each row In game_meters_to_delete.Rows
        row.Delete()
      Next

      sql_txt = "DELETE   GAME_METERS                   " & _
                " WHERE   GM_TERMINAL_ID = @pTerminalId "

      sqladap = New SqlDataAdapter()
      sqladap.ContinueUpdateOnError = True
      sqladap.DeleteCommand = New SqlCommand(sql_txt, SqlTrx.Connection, SqlTrx)
      sqladap.DeleteCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TE_TERMINAL_ID"

      nr = sqladap.Update(game_meters_to_delete)

      sqladap.Dispose()
      game_meters_to_delete.Dispose()
    End If

  End Sub ' ExecuteAdapterUpdate

  ' PURPOSE: Check if the name of the terminals to add already exists.
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  If any terminal name already exists
  '     - False: else

  Private Function CheckTerminalsExists(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim added_terminals() As DataRow
    Dim row As DataRow
    Dim term_names As String
    Dim term_name As String
    Dim term_idx As Integer
    Dim sql_txt As String
    Dim sql_cmd As SqlCommand
    Dim value As Object
    Dim duplicated_term As String

    added_terminals = m_sql_dataset_terminals.Tables(0).Select(m_data_view_terminals.RowFilter, "", DataViewRowState.Added)
    If added_terminals.Length <= 0 Then
      Return False
    End If

    term_names = ""
    sql_cmd = New SqlCommand()

    sql_txt = " SELECT   TOP 1 ISNULL (TE_BASE_NAME, '') " & _
              "   FROM   TERMINALS                       " & _
              "  WHERE   TE_NAME IN ( "

    term_idx = 1
    For Each row In added_terminals
      term_name = "@pTerm" & term_idx.ToString("000")
      sql_cmd.Parameters.Add(term_name, SqlDbType.NVarChar).Value = row.Item("TE_BASE_NAME")

      term_names = term_names & ", " & term_name

      term_idx = term_idx + 1
    Next

    ' term_names is always > 0, not necessary to protect Substring(2)
    term_names = term_names.Substring(2)
    sql_txt = sql_txt & term_names & " )"

    sql_cmd.CommandText = sql_txt
    sql_cmd.Connection = SqlTrx.Connection
    sql_cmd.Transaction = SqlTrx

    duplicated_term = ""
    value = sql_cmd.ExecuteScalar()
    If value IsNot Nothing And value IsNot DBNull.Value Then
      duplicated_term = value
    End If

    If duplicated_term <> "" Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(126), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , duplicated_term)

      Return True
    End If

    Return False
  End Function ' CheckTerminalsExists

  ' PURPOSE: Check if the external of the terminals to add already exists.
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  If any external already exists
  '     - False: else
  Private Function CheckExternalsExists(ByVal SqlTrx As SqlTransaction) As Boolean
    Dim _added_terminals() As DataRow
    Dim _row As DataRow
    Dim _external_term_names As String
    Dim _external_term_name As String
    Dim _term_idx As Integer
    Dim _sql_txt As String
    Dim _sql_cmd As SqlCommand
    Dim _value As Object
    Dim _duplicated_external As String

    _added_terminals = m_sql_dataset_terminals.Tables(0).Select(m_data_view_terminals.RowFilter, "", DataViewRowState.Added)
    If _added_terminals.Length <= 0 Then

      Return False
    End If

    _external_term_names = ""
    _sql_cmd = New SqlCommand()

    _sql_txt = " SELECT   TOP 1 ISNULL (TE_EXTERNAL_ID, '') " & _
               "   FROM   TERMINALS " & _
               "  WHERE   TE_EXTERNAL_ID IN ( "

    _term_idx = 1
    For Each _row In _added_terminals
      _external_term_name = "@pTerm" & _term_idx.ToString("000")
      _sql_cmd.Parameters.Add(_external_term_name, SqlDbType.NVarChar).Value = _row.Item("TE_EXTERNAL_ID")

      _external_term_names = _external_term_names & ", " & _external_term_name

      _term_idx = _term_idx + 1
    Next

    ' term_names is always > 0, not necessary to protect Substring(2)
    _external_term_names = _external_term_names.Substring(2)
    _sql_txt = _sql_txt & _external_term_names & " )"

    _sql_cmd.CommandText = _sql_txt
    _sql_cmd.Connection = SqlTrx.Connection
    _sql_cmd.Transaction = SqlTrx

    _duplicated_external = ""
    _value = _sql_cmd.ExecuteScalar()
    If _value IsNot Nothing And _value IsNot DBNull.Value Then
      _duplicated_external = _value
    End If

    If _duplicated_external <> "" Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4736), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _duplicated_external)

      Return True
    End If

    Return False
  End Function ' CheckExternalsExists

  ' PURPOSE: Find the first message error in a array of rows.
  '
  '  PARAMS:
  '     - INPUT: DataRows() Array of rows to search for a message error
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - First msg_error found
  '
  Private Function GetMsgErrorFromRow(ByVal DataRows() As DataRow) As String
    Dim row As DataRow
    Dim msg_error As String = ""

    For Each row In DataRows
      If row.HasErrors Then
        msg_error = row.RowError

        Exit For
      End If
    Next

    Return msg_error

  End Function ' GetMsgErrorFromRow

  ' PURPOSE: Save the changes made in the DataViews (pending and 3gs terminals dataviews).
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Function GUI_SaveChanges() As Boolean

    Dim data_table_changes_terminals_pending As New DataTable
    Dim data_table_changes_terminals As New DataTable
    Dim dt_pending_orig As DataTable = Nothing
    Dim dt_3gs_orig As DataTable = Nothing
    Dim trx As SqlTransaction = Nothing
    Dim trx_close As Boolean
    Dim msg_error As String
    Dim rows_3gs_orig() As DataRow = Nothing
    Dim terminals_inserted As DataRow()

    msg_error = ""
    trx_close = False
    m_terms_added = False
    terminals_inserted = Nothing

    Try
      If Not IsNothing(m_data_view_pending) Then
        data_table_changes_terminals_pending = m_data_view_pending.Table.GetChanges
      End If

      If Not IsNothing(m_data_view_terminals) Then
        data_table_changes_terminals = m_data_view_terminals.Table.GetChanges
      End If

      ' Nothing has changed... exit!
      If IsNothing(data_table_changes_terminals_pending) _
         And IsNothing(data_table_changes_terminals) Then
        Return False
      End If

      ' RCI 08-APR-2011: Trim spaces from String values.
      Call TrimDataValues(m_sql_dataset_terminals.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added))

      If Not GUI_IsDataChangedOk() Then
        Return False
      End If

      '
      ' UPDATE 
      '

      ' Use only one transaction, because the SqlConnection is the same and it doesn't support parallel transactions.
      trx = m_sql_adap_pending.SelectCommand.Connection.BeginTransaction

      If CheckTerminalsExists(trx) Then
        Return False
      End If

      If CheckExternalsExists(trx) Then

        Return False
      End If

      For Each _terminal_id As Int32 In m_replaced_pending_terminals
        If Not ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_MACHINE_NUID_CHANGE, _terminal_id, trx) Then
          Return False
        End If
      Next

      m_sql_adap_pending.DeleteCommand.Transaction = trx
      m_sql_adap_pending.UpdateCommand.Transaction = trx
      m_sql_adap_pending.ContinueUpdateOnError = True

      m_sql_adap_terminals.UpdateCommand.Transaction = trx
      m_sql_adap_terminals.InsertCommand.Transaction = trx

      m_sql_adap_terminals.ContinueUpdateOnError = True

      ' TERMINALS_PENDING

      ' Copy DataTable with originals values
      dt_pending_orig = m_sql_dataset_pending.Tables(0).Copy()

      ' Copy DataTable with originals values
      dt_3gs_orig = m_sql_dataset_terminals.Tables(0).Copy()
      rows_3gs_orig = dt_3gs_orig.Select(m_data_view_terminals.RowFilter, "", _
                                         DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)

      ' TERMINALS

      ' RCI 08-APR-2011: Delete GAME_METERS from terminal if it is a substitute
      DeleteSubstitutesGameMeters(trx)

      'DHA 14-APR-2014: Added to update/insert terminal status on WASS Mode
      If m_system_mode = SYSTEM_MODE.WASS Then
        terminals_inserted = m_sql_dataset_terminals.Tables(0).Select(m_data_view_terminals.RowFilter, "", DataRowState.Added)
      End If

      ' Added rows
      If Not ExecuteAdapterUpdate(TYPE_TERMINALS, DataViewRowState.Added, msg_error) Then
        Return False
      End If

      If m_system_mode = SYSTEM_MODE.WASS Then
        If Not ExecuteUpdateTerminalStatusWASS(terminals_inserted, trx) Then
          Return False
        End If
      End If

      If Not ExecuteAdapterUpdate(TYPE_TERMINALS, DataViewRowState.ModifiedCurrent, msg_error) Then
        Return False
      End If

      ' TERMINALS_PENDING
      ' Deleted rows
      If Not ExecuteAdapterUpdate(TYPE_PENDING, DataViewRowState.Deleted, msg_error) Then
        Return False
      End If

      If Not ExecuteAdapterUpdate(TYPE_PENDING, DataViewRowState.ModifiedCurrent, msg_error) Then
        Return False
      End If

      trx.Commit()
      trx_close = True

      GUI_WriteAuditoryChangesPending(data_table_changes_terminals_pending)
      GUI_WriteAuditoryChangesTerminals(data_table_changes_terminals)

      m_sql_dataset_pending.AcceptChanges()
      m_sql_dataset_terminals.AcceptChanges()

      ' LEM & RCI 2014-10-13: Refresh data for TerminalReport module on new terminal creation
      Call TerminalReport.ForceRefresh()

      ' 108 "Los cambios se han guardado correctamente."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

      GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

      Return True

    Catch ex As Exception
      If Not trx Is Nothing Then
        trx.Rollback()
      End If

      trx_close = True

      'TODO: Do nothing with m_sql_datasets...
      'm_sql_dataset_pending.RejectChanges()
      'm_sql_dataset_3gs.RejectChanges()
      Debug.WriteLine(ex.Message)

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Return False

    Finally

      If Not trx Is Nothing Then
        If Not trx_close Then
          trx.Rollback()
        End If
      End If

      If msg_error <> "" Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , msg_error)
      End If

    End Try

  End Function 'GUI_SaveChanges

  ' PURPOSE: Check if data in the dataviews is ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok.
  '     - False: Data is NOT ok.
  '
  Private Function GUI_IsDataChangedOk() As Boolean
    Dim total_rows() As DataRow
    Dim row As DataRow
    Dim nls_string As String
    Dim _regex As RegularExpressions.Regex

    total_rows = m_sql_dataset_terminals.Tables(0).Select("", "", _
                                                    DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)

    For Each row In total_rows
      row.ClearErrors()
    Next

    _regex = New RegularExpressions.Regex("[\x00-\x1f]") 'Non printable characters

    For Each row In total_rows

      ' Null values are not allowed
      If row.Item("TE_PROVIDER_ID") Is DBNull.Value _
         Or row.Item("TE_PROVIDER_ID") = "" Then

        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(469))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If
      If row.Item("TE_BASE_NAME") Is DBNull.Value _
         Or row.Item("TE_BASE_NAME") = "" Then

        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONFIGURATION.GetString(210))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      'Non printable characters
      If _regex.IsMatch(row.Item("TE_BASE_NAME")) Then
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(131), GLB_NLS_GUI_CONFIGURATION.GetString(210))
        AssignRowError(row, nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(131), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If Not CheckDataRow(row, False) Then
        Return False
      End If

    Next

    Return True
  End Function ' GUI_IsDataChangedOk

  ' PURPOSE: Set RowError for the Row. Also restore the RowHeaderWidth for the 3gs DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow
  '           - MsgError As String
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Private Sub AssignRowError(ByVal Row As DataRow, ByVal MsgError As String)
    Row.RowError = MsgError
    ' Restore original RowHeaderWidth.
    If dg_terminals_added.TableStyles.Count > 0 Then
      dg_terminals_added.TableStyles(0).RowHeaderWidth = m_original_row_header_width
    End If
  End Sub ' AssignRowError

  ' PURPOSE: Write auditory changes made in data table, that is related with global and terminals table.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_WriteAuditoryChangesTerminals(ByVal ChangesDT As DataTable)

    Dim original_modified_dv As DataView
    Dim idx_change As Integer

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

    For idx_change = 0 To ChangesDT.Rows.Count - 1

      If ChangesDT.Rows(idx_change).RowState = DataRowState.Added Or _
         ChangesDT.Rows(idx_change).RowState = DataRowState.Modified Then

        AuditoryRowChangeTerminal(TYPE_TERMINALS, ChangesDT.Rows(idx_change), original_modified_dv)

      End If
    Next

  End Sub ' GUI_WriteAuditoryChangesTerminals

  ' PURPOSE: Write auditory changes made in a Row, depending of Type (Global or 3gs terminal).
  '
  '  PARAMS:
  '     - INPUT:
  '           - Type As Integer: TYPE_GLOBAL, TYPE_3GS
  '           - Row As DataRow
  '           - OrigDV As DataView
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AuditoryRowChangeTerminal(ByVal Type As Integer, ByVal Row As DataRow, ByVal OrigDV As DataView)

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim has_changes As Boolean = False

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Select Case Type
      Case TYPE_TERMINALS
        ' If not added data... exit.
        If Row.RowState = DataRowState.Added And Not m_terms_added Then
          auditor_data = Nothing
          original_auditor_data = Nothing

          Exit Sub
        End If

        AuditoryRowTerminal(Row, auditor_data, original_auditor_data, OrigDV, has_changes)
    End Select

    Select Case Row.RowState
      Case DataRowState.Modified
        If has_changes Then
          If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                     GLB_CurrentUser.Id, _
                                     GLB_CurrentUser.Name, _
                                     CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                     0, _
                                     original_auditor_data) Then
            ' Logger message 
          End If
        End If

      Case DataRowState.Added
        If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                   GLB_CurrentUser.Id, _
                                   GLB_CurrentUser.Name, _
                                   CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                   0) Then
          ' Logger message 
        End If
    End Select

    auditor_data = Nothing
    original_auditor_data = Nothing

  End Sub ' AuditoryRowChangeTerminal

  ' PURPOSE: Set auditory data for a Row related with 3gs terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow
  '           - AuditorData As CLASS_AUDITOR_DATA
  '           - AuditorDataOrig As CLASS_AUDITOR_DATA
  '           - OrigDV As DataView
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub AuditoryRowTerminal(ByVal Row As DataRow, ByVal AuditorData As CLASS_AUDITOR_DATA, _
                                  ByVal AuditorDataOrig As CLASS_AUDITOR_DATA, ByVal OrigDV As DataView, _
                                  ByRef HasChanges As Boolean)
    Dim idx_original As Integer
    Dim value_string As String
    Dim value_vendor As String
    Dim value_serial As String

    HasChanges = False

    ' Set Name and Identifier
    Call AuditorData.SetName(GLB_NLS_GUI_AUDITOR.Id(339), Row.Item("TE_BASE_NAME"))
    Call AuditorDataOrig.SetName(GLB_NLS_GUI_AUDITOR.Id(339), Row.Item("TE_BASE_NAME"))

    'Set Related type and related id
    Call AuditorData.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Row.Item("TE_TERMINAL_ID"))
    Call AuditorDataOrig.SetRelated(CLASS_AUDITOR_DATA.ENUM_AUDITOR_RELATED.TERMINAL, Row.Item("TE_TERMINAL_ID"))

    '
    ' Details for current rows
    '

    ' Vendor
    value_vendor = IIf(IsDBNull(Row.Item("TE_PROVIDER_ID")), "", Row.Item("TE_PROVIDER_ID"))
    ' 469 "Provider"
    Call AuditorData.SetField(GLB_NLS_GUI_CONFIGURATION.Id(469), IIf(value_vendor = "", AUDIT_NONE_STRING, value_vendor))

    ' Serial Number
    value_serial = IIf(IsDBNull(Row.Item("TE_EXTERNAL_ID")), "", Row.Item("TE_EXTERNAL_ID"))
    ' 339 "Terminal"
    Call AuditorData.SetField(GLB_NLS_GUI_AUDITOR.Id(339), IIf(value_serial = "", AUDIT_NONE_STRING, value_serial))

    ' Search original row
    If Row.RowState = DataRowState.Modified Then
      For idx_original = 0 To OrigDV.Count - 1
        If OrigDV(idx_original).Item("TE_TERMINAL_ID") = Row.Item("TE_TERMINAL_ID") Then
          Exit For
        End If
      Next

      ' Critical Error: exit
      If idx_original = OrigDV.Count Then
        ' Logger message 
        Exit Sub
      End If

      '
      ' Details for original rows
      '

      ' Vendor
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("TE_PROVIDER_ID")), "", OrigDV(idx_original).Item("TE_PROVIDER_ID"))
      If Not value_string.Equals(value_vendor) Then
        HasChanges = True
      End If

      ' 469 "Provider"
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_CONFIGURATION.Id(469), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

      ' Serial Number
      value_string = IIf(IsDBNull(OrigDV(idx_original).Item("TE_EXTERNAL_ID")), "", OrigDV(idx_original).Item("TE_EXTERNAL_ID"))

      If Not value_string.Equals(value_serial) Then
        HasChanges = True
      End If

      ' 339 "Terminal"
      Call AuditorDataOrig.SetField(GLB_NLS_GUI_AUDITOR.Id(339), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

    End If

  End Sub ' AuditoryRowTerminal3gs

  ' PURPOSE: Write auditory changes made in data table ChangesDT, that is related with pending terminals table.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_WriteAuditoryChangesPending(ByVal ChangesDT As DataTable)
    Dim original_modified_dv As DataView
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim original_auditor_data As CLASS_AUDITOR_DATA
    Dim idx_change As Integer
    Dim idx_original As Integer
    Dim value_string As String

    If IsNothing(ChangesDT) Then
      Exit Sub
    End If

    original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.ModifiedOriginal)

    For idx_change = 0 To ChangesDT.Rows.Count - 1

      If ChangesDT.Rows(idx_change).RowState = DataRowState.Modified Then

        auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
        original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

        ' Set Name
        value_string = ChangesDT.Rows(idx_change).Item("TP_VENDOR_ID") & _
                 "." & ChangesDT.Rows(idx_change).Item("TP_SERIAL_NUMBER")
        '"." & ChangesDT.Rows(idx_change).Item("TP_MACHINE_NUMBER")

        Call auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(345), value_string)
        Call original_auditor_data.SetName(GLB_NLS_GUI_AUDITOR.Id(345), value_string)

        '
        ' Details for current rows
        '

        ' Ignore
        value_string = IIf(IsDBNull(ChangesDT.Rows(idx_change).Item("TP_IGNORE")), "", ChangesDT.Rows(idx_change).Item("TP_IGNORE"))
        Call auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(352), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        ' Search original row
        For idx_original = 0 To original_modified_dv.Count - 1
          If original_modified_dv(idx_original).Item("TP_ID") = ChangesDT.Rows(idx_change).Item("TP_ID") Then

            Exit For
          End If
        Next

        ' Critical Error: exit
        If idx_original = original_modified_dv.Count Then
          ' Logger message 

          Exit Sub
        End If

        '
        ' Details for original rows
        '

        ' Ignore
        value_string = IIf(IsDBNull(original_modified_dv(idx_original).Item("TP_IGNORE")), "", original_modified_dv(idx_original).Item("TP_IGNORE"))
        Call original_auditor_data.SetField(GLB_NLS_GUI_AUDITOR.Id(352), IIf(value_string = "", AUDIT_NONE_STRING, value_string))

        If Not auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                       GLB_CurrentUser.Id, _
                                       GLB_CurrentUser.Name, _
                                       CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                       0, _
                                       original_auditor_data) Then
          ' Logger message 
        End If

        auditor_data = Nothing
        original_auditor_data = Nothing
      End If
    Next

  End Sub ' GUI_WriteAuditoryChangesPending

  ' PURPOSE: Replaces an existing terminal with a pending terminal
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Change was ok
  '     - False: Change was NOT ok

  Private Function GUI_ReplacePendingTerminal() As Boolean

    Try
      ReplacePendingTerminal()

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write

    Catch ex As Exception
      Debug.WriteLine(ex.Message)

      ' 113 "Error managing terminal: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Return False

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Function ' GUI_ReplacePendingTerminal

  ' PURPOSE: Change a pending terminal with a existing terminal to terminals in memory
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  Assignation was ok
  '     - False: Assignation was NOT ok

  Private Function ReplacePendingTerminal() As Boolean

    Dim row_pending As DataRowView
    Dim idx_row As Integer
    Dim has_errors As Boolean
    Dim sql_adap_terminals_temp As SqlDataAdapter
    Dim sql_query As String
    Dim terminal_rows As DataRow()
    Dim terminal_row As DataRow
    Dim frm_sel As frm_terminals
    Dim selected_terminal_id As Integer
    Dim sel_params As TYPE_TERMINAL_SEL_PARAMS

    has_errors = False
    row_pending = Nothing

    For idx_row = m_data_view_pending.Count - 1 To 0 Step -1
      If dg_terminals_pending.IsSelected(idx_row) Then
        row_pending = m_data_view_pending(idx_row)

        Exit For    ' Process only first selected row
      End If
    Next

    If row_pending Is Nothing Then
      ' 118 "You must select at least one terminal."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return True
    End If

    frm_sel = New frm_terminals

    sel_params = New TYPE_TERMINAL_SEL_PARAMS
    sel_params.FilterTypeDefault = False
    sel_params.FilterTypeEnable(0) = False    ' Disable all 
    ' Check the one corresponding to the selected type and don't allow unselection
    sel_params.FilterTypeValue(row_pending.Item("TP_TERMINAL_TYPE")) = True
    sel_params.FilterTypeEnable(row_pending.Item("TP_TERMINAL_TYPE")) = True
    sel_params.FilterTypeLocked(row_pending.Item("TP_TERMINAL_TYPE")) = True
    selected_terminal_id = frm_sel.ShowForSelect(sel_params)

    If selected_terminal_id = 0 Then
      ' 118 "You must select at least one terminal."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return True
    ElseIf selected_terminal_id = -1 Then
      Return True
    End If

    m_data_view_terminals.AllowNew = Me.Permissions.Write

    Try

      row_pending = m_data_view_pending(idx_row)

      ' Build SQL Query for selected terminal read
      sql_query = "SELECT TE_TERMINAL_ID,TE_PROVIDER_ID,TE_BASE_NAME,TE_TYPE,TE_BLOCKED,TE_ISO_CODE,TE_MASTER_ID,TE_CREATION_DATE,TE_REPLACEMENT_DATE FROM TERMINALS WHERE TE_TERMINAL_ID = " & selected_terminal_id

      ' Read selected terminal information
      sql_adap_terminals_temp = New SqlDataAdapter(sql_query, m_sql_conn)
      sql_adap_terminals_temp.FillSchema(m_sql_dataset_terminals, SchemaType.Source)
      sql_adap_terminals_temp.Fill(m_sql_dataset_terminals)

      ' Select row with selected terminal id
      terminal_rows = m_sql_dataset_terminals.Tables(0).Select("TE_TERMINAL_ID = " & selected_terminal_id)

      If terminal_rows.Length < 1 Then
        ' TODO: Check message!!
        ' 118 "You must select at least one terminal."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return True
      End If

      terminal_row = terminal_rows(0)

      ' Change TE_EXTERNAL_ID to read/write and modify
      m_sql_dataset_terminals.Tables(0).Columns("TE_EXTERNAL_ID").ReadOnly = False
      terminal_row.Item("TE_EXTERNAL_ID") = row_pending.Item("TP_SERIAL_NUMBER")
      m_sql_dataset_terminals.Tables(0).Columns("TE_EXTERNAL_ID").ReadOnly = True

      ' Set the replacement date to now
      m_sql_dataset_terminals.Tables(0).Columns("TE_REPLACEMENT_DATE").ReadOnly = False
      terminal_row.Item("TE_REPLACEMENT_DATE") = WSI.Common.WGDB.Now
      m_sql_dataset_terminals.Tables(0).Columns("TE_REPLACEMENT_DATE").ReadOnly = True

      ' Field TP_ID is necessary for undelete operation of pending terminals. See routine UndeleteAssignedRow().
      m_sql_dataset_terminals.Tables(0).Columns("TP_ID").ReadOnly = False
      terminal_row.Item("TP_ID") = row_pending.Item("TP_ID")

      ' Mark as Added
      ' terminal_row.AcceptChanges()
      ' terminal_row.SetAdded()

      ' Dispose the temporal adapter (maybe not needed)
      sql_adap_terminals_temp.Dispose()

      ' Delete the Pending terminal
      m_data_view_pending.AllowDelete = True
      row_pending.Delete()
      row_pending.EndEdit()

      m_replaced_pending_terminals.Add(selected_terminal_id)

      Return Not has_errors

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      has_errors = True

      Return False

    Finally
      m_data_view_pending.AllowDelete = False
      m_data_view_terminals.AllowNew = False

      If has_errors Then
        ' 120 "Could not replace all the terminals."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(120), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try

  End Function ' ReplacePendingTerminal

  ' PURPOSE: GUI Assign a pending terminal to terminals
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Assignation was ok
  '     - False: Assignation was NOT ok
  '
  Private Function GUI_AssignPendingTerminal() As Boolean

    Dim data_table_changes_terminals_pending As New DataTable
    Dim rc As Boolean

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Execute assign        
      rc = AssignPendingTerminal()

      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = m_data_view_pending.Count > 0 And Me.Permissions.Write

      Return rc

    Catch ex As Exception
      Debug.WriteLine(ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(113), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

      Return False

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Function 'GUI_AssignPendingTerminal

  ' PURPOSE: Assign a pending terminal to terminals in memory
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  Assignation was ok
  '     - False: Assignation was NOT ok

  Private Function AssignPendingTerminal() As Boolean

    Dim row_pending As DataRowView
    Dim new_row As DataRowView
    Dim idx_row As Integer
    Dim select_idx As Collection
    Dim has_errors As Boolean

    select_idx = New Collection()
    has_errors = False

    ' Need to save selected index before process them, if not, the selected property for all indexes is deleted after the
    ' first index processed. Also save them in reverse order (highest to lowest), because data view is modified while
    ' processing.
    For idx_row = m_data_view_pending.Count - 1 To 0 Step -1
      If dg_terminals_pending.IsSelected(idx_row) Then
        select_idx.Add(idx_row)
      End If
    Next

    If select_idx.Count = 0 Then
      ' 118 "You must select at least one terminal."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return True
    End If

    m_data_view_terminals.AllowNew = Me.Permissions.Write

    Try

      For Each idx_row In select_idx
        row_pending = m_data_view_pending(idx_row)

        new_row = m_data_view_terminals.AddNew()

        If row_pending.Item("TP_VENDOR_ID") = "CASINO" Then
          new_row.Item("TE_PROVIDER_ID") = row_pending.Item("TP_VENDOR_ID")
        Else
          new_row.Item("TE_PROVIDER_ID") = ""
        End If

        new_row.Item("TE_EXTERNAL_ID") = row_pending.Item("TP_SERIAL_NUMBER")
        new_row.Item("TE_BASE_NAME") = ""
        new_row.Item("TE_TYPE") = WSI.Common.TerminalTypes.WIN
        new_row.Item("TE_TERMINAL_TYPE") = row_pending.Item("TP_TERMINAL_TYPE")
        new_row.Item("TE_BLOCKED") = 0
        ' Field TP_ID is necessary for undelete operation of pending terminals. See routine UndeleteAssignedRow().
        new_row.Item("TP_ID") = row_pending.Item("TP_ID")
        new_row.Item("TE_ISO_CODE") = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

        If CheckDataRow(new_row.Row, True) Then
          new_row.EndEdit()

          m_data_view_pending.AllowDelete = True
          row_pending.Delete()
          row_pending.EndEdit()
        Else
          has_errors = True
          new_row.CancelEdit()
        End If
      Next

      Return Not has_errors

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
      has_errors = True

      Return False

    Finally
      m_data_view_pending.AllowDelete = False
      m_data_view_terminals.AllowNew = False

      If has_errors Then
        ' 117 "Could not add all the terminals."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
    End Try

  End Function 'AssignPendingTerminal

  ' PURPOSE: Check Data Row for constraints.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow: Row to check.
  '           - IsPendingRow As Boolean: Indicates if the checked row come from Pending Table.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckDataRow(ByVal Row As DataRow, ByVal IsPendingRow As Boolean) As Boolean
    Dim data_view As DataView
    Dim row_filter As String
    Dim count_plus As Integer
    Dim nls_string As String
    Dim _terminal_name As String
    Dim _provider_id As String
    Dim _external_id As String

    count_plus = 0
    If IsPendingRow Then
      count_plus = 1
    End If

    data_view = New DataView(Me.m_sql_dataset_terminals.Tables(0))

    _provider_id = Row.Item("TE_PROVIDER_ID")
    _external_id = Row.Item("TE_EXTERNAL_ID")
    _terminal_name = Row.Item("TE_BASE_NAME")

    ' Check couple VENDOR_ID / SERIAL_NUMBER
    row_filter = "TE_PROVIDER_ID = '" & _provider_id.Replace("'", "''") & "'" & _
             " AND TE_EXTERNAL_ID = '" & _external_id.Replace("'", "''") & "'"
    data_view.RowFilter = row_filter

    If data_view.Count + count_plus > 1 Then
      If Not IsPendingRow Then
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(115), _
                                   _provider_id, _external_id, _
                                   GLB_NLS_GUI_CONFIGURATION.GetString(205), _
                                   GLB_NLS_GUI_CONFIGURATION.GetString(214))
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(115), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If
      Return False
    End If

    ' Pending Rows have not TE_BASE_NAME, don't check it for them.
    If Not IsPendingRow Then
      row_filter = "TE_BASE_NAME = '" & _terminal_name.Replace("'", "''") & "'"
      data_view.RowFilter = row_filter

      If data_view.Count > 1 Then
        ' 116 "El %1 ya existe.\nEl %2 debe ser �nico."
        nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(116), _
                                   _terminal_name, GLB_NLS_GUI_CONFIGURATION.GetString(210))
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(116), nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE: Initialize data in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ClearGrid()

    dg_terminals_pending.DataSource = Nothing

  End Sub 'GUI_ClearGrid

  ' PURPOSE: Create Data Grids with SQL queries, and show generated info in screen.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub ExecuteQuery()
    Dim table_style As DataGridTableStyle
    Dim sql_query_pending As String
    Dim sql_query_terminals As String
    'Dim row_filter As String
    ' Dim need_add As Boolean
    Dim where_added As Boolean
    ' Dim str_filter As String

    ' Initialize DataView for database
    Try
      m_sql_conn = Nothing
      m_sql_conn = NewSQLConnection()

      If Not (m_sql_conn Is Nothing) Then
        ' Connected
        If m_sql_conn.State <> ConnectionState.Open Then
          ' Connecting ..
          m_sql_conn.Open()
        End If

        If Not m_sql_conn.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      m_sql_dataset_pending = New DataSet()
      m_sql_dataset_terminals = New DataSet()

      ' Build Query -------------------------------------------------

      '
      ' TERMINALS_PENDING
      '

      sql_query_pending = "SELECT TP_ID              " _
                             & ", TP_SOURCE          " _
                             & ", TP_VENDOR_ID       " _
                             & ", TP_SERIAL_NUMBER   " _
                             & ", TP_TERMINAL_TYPE   " _
                             & ", TP_REPORTED        " _
                             & ", TP_IGNORE          " _
                         & " FROM TERMINALS_PENDING  " _
                        & " WHERE TP_SOURCE = 1      "

      where_added = True

      If Len(ef_terminal_pending_vendor.Value) > 0 Then

        If where_added = False Then
          sql_query_pending = sql_query_pending & " WHERE "
          where_added = True
        Else
          sql_query_pending = sql_query_pending & " AND "
        End If

        sql_query_pending = sql_query_pending & _
                            GUI_FilterField("TP_VENDOR_ID", ef_terminal_pending_vendor.Value)
      End If

      If Len(ef_terminal_pending_serial_number.Value) > 0 Then

        If where_added = False Then
          sql_query_pending = sql_query_pending & " WHERE "
          where_added = True
        Else
          sql_query_pending = sql_query_pending & " AND "
        End If

        sql_query_pending = sql_query_pending & GUI_FilterField("TP_SERIAL_NUMBER", ef_terminal_pending_serial_number.Value, False, False, True)
      End If

      If Me.chk_hide_ignored.Checked Then

        If where_added = False Then
          sql_query_pending = sql_query_pending & " WHERE "
          where_added = True
        Else
          sql_query_pending = sql_query_pending & " AND "
        End If

        sql_query_pending = sql_query_pending & " TP_IGNORE = 0"
      End If

      '
      ' TERMINALS_3GS

      ' Field TP_ID is necessary for undelete operation of pending terminals. See routine UndeleteAssignedRow().

      sql_query_terminals = "SELECT TE_TERMINAL_ID       " & _
                                 ", TE_PROVIDER_ID       " & _
                                 ", TE_EXTERNAL_ID       " & _
                                 ", TE_NAME              " & _
                                 ", TE_TYPE              " & _
                                 ", TE_TERMINAL_TYPE     " & _
                                 ", TE_BLOCKED           " & _
                                 ", ( -1 ) TP_ID         " & _
                                 ", TE_BASE_NAME         " & _
                                 ", TE_CHANGE_ID         " & _
                                 ", TE_MASTER_ID         " & _
                                 ", TE_ISO_CODE          " & _
                             " FROM TERMINALS            "

      ' End Build Query -------------------------------------------------
      '
      ' TERMINALS_PENDING
      '

      If Not IsNothing(m_sql_adap_pending) Then
        m_sql_adap_pending.Dispose()
        m_sql_adap_pending = Nothing
      End If
      m_sql_adap_pending = New SqlDataAdapter(sql_query_pending, m_sql_conn)
      m_sql_adap_pending.FillSchema(m_sql_dataset_pending, SchemaType.Source)
      m_sql_adap_pending.Fill(m_sql_dataset_pending)

      m_sql_dataset_pending.Tables(0).Columns("TP_VENDOR_ID").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_SERIAL_NUMBER").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_TERMINAL_TYPE").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_REPORTED").ReadOnly = True
      m_sql_dataset_pending.Tables(0).Columns("TP_IGNORE").ReadOnly = False

      m_sql_commandbuilder_pending = New SqlCommandBuilder(m_sql_adap_pending)
      m_sql_adap_pending.UpdateCommand = UpdateCommandPending()
      m_sql_adap_pending.DeleteCommand = m_sql_commandbuilder_pending.GetDeleteCommand
      m_sql_adap_pending.InsertCommand = Nothing

      m_data_view_pending = New DataView(m_sql_dataset_pending.Tables(0))
      m_data_view_pending.AllowDelete = Me.Permissions.Write
      m_data_view_pending.AllowNew = False
      m_data_view_pending.AllowEdit = Me.Permissions.Write

      '
      ' TERMINALS_3GS
      '

      If Not IsNothing(m_sql_adap_terminals) Then
        m_sql_adap_terminals.Dispose()
        m_sql_adap_terminals = Nothing
      End If
      m_sql_adap_terminals = New SqlDataAdapter(sql_query_terminals, m_sql_conn)
      'm_sql_dataset_terminals.Tables(0).Columns("TE_NAME").ColumnName = "TE_BASE_NAME"
      m_sql_adap_terminals.FillSchema(m_sql_dataset_terminals, SchemaType.Source)
      ' m_sql_adap_terminals.Fill(m_sql_dataset_terminals)

      ' Remove constraints. Control them manually in function GUI_IsDataChangedOk().
      'm_sql_dataset_terminals.Tables(0).Constraints.Clear()

      ' Default Values for Table TERMINALS_3GS
      m_sql_dataset_terminals.Tables(0).Columns("TE_PROVIDER_ID").DefaultValue = ""
      m_sql_dataset_terminals.Tables(0).Columns("TE_EXTERNAL_ID").DefaultValue = ""
      m_sql_dataset_terminals.Tables(0).Columns("TE_NAME").DefaultValue = ""
      m_sql_dataset_terminals.Tables(0).Columns("TE_TYPE").DefaultValue = WSI.Common.TerminalTypes.WIN
      m_sql_dataset_terminals.Tables(0).Columns("TE_TERMINAL_TYPE").DefaultValue = -1
      m_sql_dataset_terminals.Tables(0).Columns("TE_BLOCKED").DefaultValue = 0
      m_sql_dataset_terminals.Tables(0).Columns("TE_BASE_NAME").DefaultValue = ""
      m_sql_dataset_terminals.Tables(0).Columns("TE_CHANGE_ID").DefaultValue = 0
      m_sql_dataset_terminals.Tables(0).Columns("TE_MASTER_ID").DefaultValue = 0

      m_sql_dataset_terminals.Tables(0).Columns("TE_PROVIDER_ID").ReadOnly = False
      m_sql_dataset_terminals.Tables(0).Columns("TE_EXTERNAL_ID").ReadOnly = True
      m_sql_dataset_terminals.Tables(0).Columns("TE_NAME").ReadOnly = True 'False
      m_sql_dataset_terminals.Tables(0).Columns("TE_TYPE").ReadOnly = True
      m_sql_dataset_terminals.Tables(0).Columns("TE_TERMINAL_TYPE").ReadOnly = True
      m_sql_dataset_terminals.Tables(0).Columns("TE_BLOCKED").ReadOnly = True
      m_sql_dataset_terminals.Tables(0).Columns("TE_BASE_NAME").ReadOnly = False
      m_sql_dataset_terminals.Tables(0).Columns("TE_CHANGE_ID").ReadOnly = True
      m_sql_dataset_terminals.Tables(0).Columns("TE_MASTER_ID").ReadOnly = True

      m_sql_commandbuilder_terminals = New SqlCommandBuilder(m_sql_adap_terminals)
      m_sql_adap_terminals.UpdateCommand = UpdateCommandTerminals()
      m_sql_adap_terminals.DeleteCommand = Nothing
      m_sql_adap_terminals.InsertCommand = InsertCommandTerminals()   ' m_sql_commandbuilder_terminals.GetInsertCommand    '

      If WSI.Common.Misc.SystemMode = SYSTEM_MODE.WASS Then
        m_sql_adap_terminals.InsertCommand = InsertCommandTerminals()
      End If

      m_data_view_terminals = New DataView(m_sql_dataset_terminals.Tables(0))
      'm_data_view_terminals.RowFilter = row_filter
      m_data_view_terminals.AllowDelete = Me.Permissions.Write
      m_data_view_terminals.AllowNew = False
      m_data_view_terminals.AllowEdit = Me.Permissions.Write

      '
      ' DATA GRIDS
      '

      ' Enable Data Grids
      dg_terminals_pending.Enabled = True
      dg_terminals_pending.DataSource = m_data_view_pending

      dg_terminals_added.Enabled = True
      dg_terminals_added.DataSource = m_data_view_terminals

      ' TODO: What does AllowNavigation???
      dg_terminals_pending.AllowNavigation = False
      dg_terminals_added.AllowNavigation = False

      ' Columns for TERMINALS_PENDING
      If IsNothing(dg_terminals_pending.TableStyles.Item(m_data_view_pending.Table.TableName)) Then

        table_style = NewTableStylePending(m_data_view_pending.Table.TableName, _
                                           m_sql_dataset_pending.Tables(0).Columns)
        dg_terminals_pending.TableStyles.Add(table_style)
      End If

      ' Columns for TERMINALS_PENDING
      If IsNothing(dg_terminals_added.TableStyles.Item(m_data_view_terminals.Table.TableName)) Then
        table_style = NewTableStyleTerminals(m_data_view_terminals.Table.TableName, _
                                             m_sql_dataset_terminals.Tables(0).Columns)
        dg_terminals_added.TableStyles.Add(table_style)
      End If

      ' Restore original RowHeaderWidth.
      If dg_terminals_added.TableStyles.Count > 0 Then
        dg_terminals_added.TableStyles(0).RowHeaderWidth = m_original_row_header_width
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
    End Try

  End Sub 'ExecuteQuery

  ' PURPOSE: Return the UPDATE SqlCommand for the SQL Adapter for table terminals_pending.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SqlCommand

  Private Function UpdateCommandTerminals() As SqlCommand

    Dim cmd As SqlCommand

    cmd = New SqlCommand("UPDATE TERMINALS                                                " & _
                           " SET TE_EXTERNAL_ID = @pExternalId                            " & _
                              ", TE_PROVIDER_ID = @pProviderId                            " & _
                              ", TE_BASE_NAME   = @pBaseName                              " & _
                              ", TE_STATUS      = @pStatusActive                          " & _
                              ", TE_MASTER_ID   = isNull(@pTerminalMasterId, @pTerminalId)" & _
                              ", TE_REPLACEMENT_DATE   = @pReplacementDate                " & _
                         " WHERE TE_TERMINAL_ID = @pTerminalId                            ")

    cmd.Parameters.Add("@pExternalId", SqlDbType.NVarChar).SourceColumn = "TE_EXTERNAL_ID"
    cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar).SourceColumn = "TE_PROVIDER_ID"
    cmd.Parameters.Add("@pBaseName", SqlDbType.NVarChar).SourceColumn = "TE_BASE_NAME"
    cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = WSI.Common.TerminalStatus.ACTIVE
    cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TE_TERMINAL_ID"
    cmd.Parameters.Add("@pTerminalMasterId", SqlDbType.Int).SourceColumn = "TE_MASTER_ID"
    cmd.Parameters.Add("@pReplacementDate", SqlDbType.DateTime).SourceColumn = "TE_REPLACEMENT_DATE"

    Return cmd
  End Function 'UpdateCommandTerminals

  Private Function InsertCommandTerminals() As SqlCommand

    Dim _str_bld As StringBuilder
    Dim cmd As SqlCommand
    Dim _parameter As SqlParameter

    _str_bld = New StringBuilder
    _str_bld.AppendLine("INSERT   INTO TERMINALS                     ")
    _str_bld.AppendLine("       ( TE_PROVIDER_ID                     ")
    _str_bld.AppendLine("       , TE_EXTERNAL_ID                     ")
    _str_bld.AppendLine("       , TE_BASE_NAME                       ")
    _str_bld.AppendLine("       , TE_TYPE                            ")
    _str_bld.AppendLine("       , TE_TERMINAL_TYPE                   ")
    _str_bld.AppendLine("       , TE_BLOCKED                         ")
    _str_bld.AppendLine("       , TE_MASTER_ID                       ")
    _str_bld.AppendLine("       , TE_ISO_CODE                        ")
    _str_bld.AppendLine("       , TE_CHANGE_ID                       ")
    _str_bld.AppendLine("       , TE_CREATION_DATE )                 ")
    _str_bld.AppendLine("VALUES                                      ")
    _str_bld.AppendLine("      ( @pProviderId                        ")
    _str_bld.AppendLine("      , @pExternalId                        ")
    _str_bld.AppendLine("      , @pBaseName                          ")
    _str_bld.AppendLine("      , @pType                              ")
    _str_bld.AppendLine("      , @pTerminalType                      ")
    _str_bld.AppendLine("      , @pBlocked                           ")
    _str_bld.AppendLine("      , @pMasterId                          ")
    _str_bld.AppendLine("      , @pCurrency                          ")
    _str_bld.AppendLine("      , @pChangeId                          ")
    _str_bld.AppendLine("      , @pCreationDate )                    ")
    _str_bld.AppendLine("  SET @pTerminalId = SCOPE_IDENTITY()       ")
    _str_bld.AppendLine("                                            ")
    _str_bld.AppendLine("UPDATE   TERMINALS                          ")
    _str_bld.AppendLine("   SET   TE_MASTER_ID = TE_TERMINAL_ID      ")
    _str_bld.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId      ")

    cmd = New SqlCommand(_str_bld.ToString())

    cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar).SourceColumn = "TE_PROVIDER_ID"
    cmd.Parameters.Add("@pExternalId", SqlDbType.NVarChar).SourceColumn = "TE_EXTERNAL_ID"
    cmd.Parameters.Add("@pBaseName", SqlDbType.NVarChar).SourceColumn = "TE_BASE_NAME"
    cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TE_TYPE"
    cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).SourceColumn = "TE_TERMINAL_TYPE"
    cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).SourceColumn = "TE_BLOCKED"
    cmd.Parameters.Add("@pMasterId", SqlDbType.Bit).SourceColumn = "TE_MASTER_ID"
    cmd.Parameters.Add("@pChangeId", SqlDbType.Int).SourceColumn = "TE_CHANGE_ID"
    cmd.Parameters.Add("@pCurrency", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
    cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now

    _parameter = cmd.Parameters.Add("@pTerminalId", SqlDbType.Int)
    _parameter.SourceColumn = "TE_TERMINAL_ID"
    _parameter.Direction = ParameterDirection.Output

    Return cmd
  End Function 'InsertCommandTerminals

  ' PURPOSE: Return the UPDATE SqlCommand for the SQL Adapter for table terminals_pending.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SqlCommand

  Private Function UpdateCommandPending() As SqlCommand

    Dim cmd As SqlCommand

    cmd = New SqlCommand("UPDATE TERMINALS_PENDING" & _
                           " SET TP_IGNORE = @Ignored" & _
                         " WHERE TP_ID = @TpId" & _
                          "  AND TP_IGNORE = @IgnoredOrig")

    cmd.Parameters.Add("@TpId", SqlDbType.Int, 0, "TP_ID")

    cmd.Parameters.Add("@Ignored", SqlDbType.Bit, 1, "TP_IGNORE")

    cmd.Parameters.Add("@IgnoredOrig", SqlDbType.Bit, 1, "TP_IGNORE")
    cmd.Parameters("@IgnoredOrig").SourceVersion = DataRowVersion.Original

    Return cmd
  End Function 'UpdateCommandPending

  ' PURPOSE: Create table style for pending terminals view.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableName As String
  '           - Columns As DataColumnCollection
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     - table_style As DataGridTableStyle

  Private Function NewTableStylePending(ByVal TableName As String, ByVal Columns As DataColumnCollection) As DataGridTableStyle


    Dim table_style As DataGridTableStyle
    Dim dgb_pvendor As DataGridTextBoxColumn
    Dim dgb_pserial As DataGridTextBoxColumn
    Dim dgb_ptype As DataGridTextBoxColumn
    Dim dgb_reported As DataGridTextBoxColumn
    Dim dgb_ignore As DataGridBoolColumn
    Dim idx_column As Integer

    table_style = New DataGridTableStyle()
    table_style.MappingName = TableName

    dgb_pvendor = New DataGridTextBoxColumn()
    dgb_pvendor.MappingName = "TP_VENDOR_ID"
    dgb_pvendor.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
    dgb_pvendor.Width = GRID_WIDTH_VENDOR_ID
    dgb_pvendor.ReadOnly = Columns(dgb_pvendor.MappingName).ReadOnly

    dgb_pserial = New DataGridTextBoxColumn()
    dgb_pserial.MappingName = "TP_SERIAL_NUMBER"
    dgb_pserial.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(331) 'ID PROTOCOLO
    dgb_pserial.Width = GRID_WIDTH_SERIAL_NUMBER
    dgb_pserial.ReadOnly = Columns(dgb_pserial.MappingName).ReadOnly

    dgb_ptype = New DataGridTextBoxColumn()
    dgb_ptype.MappingName = "TP_TERMINAL_TYPE"
    dgb_ptype.HeaderText = GLB_NLS_GUI_SW_DOWNLOAD.GetString(322)
    dgb_ptype.Width = 0
    dgb_ptype.ReadOnly = Columns(dgb_ptype.MappingName).ReadOnly

    dgb_reported = New DataGridTextBoxColumn()
    dgb_reported.MappingName = "TP_REPORTED"
    dgb_reported.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(351)
    dgb_reported.Width = GRID_WIDTH_TP_REPORTED
    dgb_reported.ReadOnly = Columns(dgb_reported.MappingName).ReadOnly
    ' Necessary to format date and show also the time.
    dgb_reported.Format = Format.DateTimeCustomFormatString(True)

    dgb_ignore = New DataGridBoolColumn()
    dgb_ignore.MappingName = "TP_IGNORE"
    dgb_ignore.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(352)
    dgb_ignore.Width = GRID_WIDTH_TP_IGNORE
    dgb_ignore.ReadOnly = Columns(dgb_ignore.MappingName).ReadOnly
    ' To disable null values in DataGridBoolColumn
    dgb_ignore.AllowNull = False

    ' The order added is the order shown
    For idx_column = 0 To GRID_PENDING_COLUMNS
      Select Case idx_column
        Case GRID_COLUMN_PENDING_VENDOR_ID
          table_style.GridColumnStyles.Add(dgb_pvendor)

        Case GRID_COLUMN_PENDING_SERIAL_NUMBER
          table_style.GridColumnStyles.Add(dgb_pserial)

        Case GRID_COLUMN_PENDING_TYPE
          table_style.GridColumnStyles.Add(dgb_ptype)

        Case GRID_COLUMN_PENDING_REPORTED
          table_style.GridColumnStyles.Add(dgb_reported)

        Case GRID_COLUMN_PENDING_IGNORE
          table_style.GridColumnStyles.Add(dgb_ignore)

      End Select
    Next

    Return table_style

  End Function 'NewTableStylePending

  ' PURPOSE: Create table style for added terminals view.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableName As String
  '           - Columns As DataColumnCollection
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     - table_style As DataGridTableStyle

  Private Function NewTableStyleTerminals(ByVal TableName As String, ByVal Columns As DataColumnCollection) As DataGridTableStyle
    Dim table_style As DataGridTableStyle
    Dim dgb_pvendor As DataGridTextBoxColumn
    Dim dgb_pserial As DataGridTextBoxColumn
    Dim dgb_pname As DataGridTextBoxColumn
    Dim dgb_cmbox_pvendor As DataGridComboBoxColumn
    Dim idx_column As Integer
    Dim str_sql As String

    table_style = New DataGridTableStyle()
    table_style.MappingName = TableName

    dgb_pvendor = New DataGridTextBoxColumn()
    dgb_pvendor.MappingName = "TE_PROVIDER_ID"
    dgb_pvendor.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
    dgb_pvendor.Width = GRID_WIDTH_VENDOR_ID
    dgb_pvendor.ReadOnly = Columns(dgb_pvendor.MappingName).ReadOnly

    str_sql = "SELECT DISTINCT PV_ID,PV_NAME FROM PROVIDERS WHERE PV_HIDE='0' ORDER BY PV_NAME"
    dgb_cmbox_pvendor = New DataGridComboBoxColumn()

    dgb_cmbox_pvendor.MappingName = "TE_PROVIDER_ID"
    dgb_cmbox_pvendor.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)   ' "Proveedor"
    dgb_cmbox_pvendor.Width = GRID_WIDTH_VENDOR_ID
    dgb_cmbox_pvendor.ColumnComboBox.Tag = Me.FormId
    dgb_cmbox_pvendor.ColumnComboBox.DataSource = New DataView(GUI_GetTableUsingSQL(str_sql, 1000))
    dgb_cmbox_pvendor.ColumnComboBox.DisplayMember = "PV_NAME"
    dgb_cmbox_pvendor.ColumnComboBox.ValueMember = "PV_NAME"
    dgb_cmbox_pvendor.ReadOnly = Columns(dgb_cmbox_pvendor.MappingName).ReadOnly

    dgb_pserial = New DataGridTextBoxColumn()
    dgb_pserial.MappingName = "TE_EXTERNAL_ID"
    dgb_pserial.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(331)
    dgb_pserial.Width = GRID_WIDTH_SERIAL_NUMBER
    dgb_pserial.ReadOnly = Columns(dgb_pserial.MappingName).ReadOnly

    dgb_pname = New DataGridTextBoxColumn()
    dgb_pname.MappingName = "TE_BASE_NAME"
    dgb_pname.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(330)
    dgb_pname.Width = GRID_WIDTH_SERIAL_NUMBER
    dgb_pname.ReadOnly = Columns(dgb_pname.MappingName).ReadOnly

    ' The order added is the order shown
    For idx_column = 0 To GRID_TERMINAL_COLUMNS
      Select Case idx_column
        Case GRID_COLUMN_TERMINAL_PROVIDER_ID
          table_style.GridColumnStyles.Add(dgb_cmbox_pvendor)
          'table_style.GridColumnStyles.Add(dgb_pvendor)

        Case GRID_COLUMN_TERMINAL_SERIAL_NUMBER
          table_style.GridColumnStyles.Add(dgb_pserial)

        Case GRID_COLUMN_TERMINAL_NAME
          table_style.GridColumnStyles.Add(dgb_pname)
      End Select
    Next

    Return table_style

  End Function 'NewTableStylePending

  ' PURPOSE: If there are pending changes in Data Grids, show a MsgBox asking user to discard changes or not.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If discard changes
  '     - False: If NOT discard changes

  Private Function DiscardChanges() As Boolean
    Dim user_msg As Boolean
    Dim data_table_changes As DataTable

    ' If pending changes MsgBox
    user_msg = False
    If Not IsNothing(m_data_view_pending) Then
      data_table_changes = m_data_view_pending.Table.GetChanges

      If Not IsNothing(data_table_changes) Then
        If data_table_changes.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
          user_msg = True
        End If
      End If
    End If

    If Not IsNothing(m_data_view_terminals) And Not user_msg Then
      data_table_changes = m_data_view_terminals.Table.GetChanges

      If Not IsNothing(data_table_changes) Then
        If data_table_changes.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
        End If
      End If
    End If

    Return True
  End Function ' DiscardChanges

#End Region  ' Private Functions 

End Class ' frm_terminals_pending

'
' PURPOSE: Own version of DataGrid to be able to process keys inside DataGrid.
'
Public Class DataGridWithDelete
  Inherits System.Windows.Forms.DataGrid

  Public m_hit_info As DataGrid.HitTestInfo

  ' PURPOSE: Own version for OnMouseDown event routine. Just get HitTest info.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '
  Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
    m_hit_info = Me.HitTest(e.X, e.Y)
    Call MyBase.OnMouseDown(e)
  End Sub ' OnMouseDown

  ' PURPOSE: Process pressed keys inside DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If the key was processed by the DataGrid
  '     - False: Otherwise
  '
  Protected Overrides Function ProcessDialogKey(ByVal keyData As System.Windows.Forms.Keys) As Boolean
    Dim rc As Boolean

    If m_hit_info Is Nothing Then
      Return MyBase.ProcessDialogKey(keyData)
    End If
    If m_hit_info.Type = HitTestType.None Then
      Return True
    End If

    If keyData <> Keys.Delete Then
      Return MyBase.ProcessDialogKey(keyData)
    End If

    Select Case m_hit_info.Type
      Case HitTestType.RowHeader
        rc = ProcessKeyDeleteRowHeader()
        If rc Then
          Return True
        End If
    End Select

    Return MyBase.ProcessDialogKey(keyData)
  End Function ' ProcessDialogKey

  ' PURPOSE: Process DEL key inside DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If the key was processed by the DataGrid
  '     - False: Otherwise
  '
  Private Function ProcessKeyDeleteRowHeader() As Boolean
    Dim data_view As DataView
    Dim data_row_view As DataRowView
    Dim form As frm_terminals_pending
    Dim idx_row As Integer
    Dim select_idx As Collection

    If CurrentRowIndex = -1 Then
      Return True
    End If

    select_idx = New Collection()
    data_view = Me.DataSource

    ' Need to save selected index before process them, if not, the selected property for all indexes is deleted after the
    ' first index processed. Also save them in reverse order (highest to lowest), because data view is modified while
    ' processing.
    For idx_row = data_view.Count - 1 To 0 Step -1
      If Me.IsSelected(idx_row) Then
        select_idx.Add(idx_row)
      End If
    Next
    If select_idx.Count = 0 Then
      select_idx.Add(CurrentRowIndex)
    End If

    form = Me.FindForm()

    For Each idx_row In select_idx
      data_row_view = data_view(idx_row)

      Select Case data_row_view.Row.RowState
        Case DataRowState.Added
          form.UndeleteAssignedRow(data_row_view)
          data_row_view.Row.RejectChanges()
        Case DataRowState.Detached
          data_row_view.CancelEdit()
      End Select
    Next

    ' Restore original RowHeaderWidth.
    If form.dg_terminals_added.TableStyles.Count > 0 Then
      form.dg_terminals_added.TableStyles(0).RowHeaderWidth = form.m_original_row_header_width
    End If

    Return True
  End Function ' ProcessKeyDeleteRowHeader

End Class ' MyDataGrid
