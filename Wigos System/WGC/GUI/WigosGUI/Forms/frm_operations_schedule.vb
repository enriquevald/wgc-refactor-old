'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_operations_schedule
' DESCRIPTION:   Operations Schedule configuration
' AUTHOR:        Quim Morales
' CREATION DATE: 18-FEB-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 18-FEB-2013 QMP    Initial version
' 17-APR-2013 QMP    Fixed bug #669: User can add multiple recipients at once
' 06-MAY-2013 NMR    Fixed Bug #768: In "Horario de Operaciones" form, in "Excepciones" tab, in the Grid, large text are cutted.
' 14-OCT-2013 ANG    Fix bug WIG-64
' 11-JUL-2014 JMM    Weeks days sorted depending on system's first day of week
' 19-FEB-2015 YNM    Fixed Bug WIG-2073: NSL Untranslated.
' 22-JUN-2015 SGB    Backlog item 2162: Added check for enabled machines
' 06-AUG-2015 ETP    Fixed BUG-3591 Modified btn_all_ClickEvent to use system configuration.
'-------------------------------------------------------------------

#Region " Imports "

Option Strict Off
Option Explicit On

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.OperationsSchedule
Imports System.Globalization

#End Region

Public Class frm_operations_schedule
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_EXCEPTIONS_COLUMN_ID = 0
  Private Const GRID_EXCEPTIONS_COLUMN_DESCRIPTION = 1

  Private Const GRID_EXCEPTIONS_COLUMNS = 2
  Private Const GRID_EXCEPTIONS_HEADER_ROWS = 0

  Private Const GRID_RECIPIENTS_COLUMN_EMAIL = 0

  Private Const GRID_RECIPIENTS_COLUMNS = 1
  Private Const GRID_RECIPIENTS_HEADER_ROWS = 0

  Private Const FORM_DB_MIN_VERSION As Short = 144

#End Region ' Constants

#Region " Members "

  Dim m_schedule_allowed(6) As CheckBox
  Dim m_schedule_time1_from(6) As uc_date_picker
  Dim m_schedule_time1_to(6) As uc_date_picker
  Dim m_schedule_time2_check(6) As CheckBox
  Dim m_schedule_time2_from(6) As uc_date_picker
  Dim m_schedule_time2_to(6) As uc_date_picker

  Dim m_exceptions_list As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
  Dim m_exceptions_deleted As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
  Dim m_exceptions_added_idx As Integer

  Dim m_calendar_selected_range As SelectionRange

  Dim m_recipients As List(Of String)
  Dim m_recipients_added As List(Of String)
  Dim m_recipients_deleted As List(Of String)

#End Region ' Members

#Region " Overrides "

  'PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox
    Dim _minutes_start As Int32
    Dim _minutes_end As Int32

    MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1663) ' Operations Schedule

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Call Me.ArrangeControls()

    m_exceptions_added_idx = -1

    'SCHEDULE TAB
    tab_schedule.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1676) ' Schedule

    chk_schedule_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6518) ' Enable Operations Schedule
    chk_disable_machines.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6574) ' Disable machines

    _minutes_start = GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.StartTrading")
    _minutes_end = GeneralParam.GetInt32("OperationsSchedule", "DisableMachine.EndTrading")

    lbl_disable_machines.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6575, Math.Abs(_minutes_end).ToString, IIf(_minutes_end >= 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6598), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6599)) _
                                                                          , Math.Abs(_minutes_start).ToString, IIf(_minutes_start >= 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6598), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6599))) ' "Machines will be locked 15 minutes before the end of the......

    cmb_closing_time.TextVisible = True
    cmb_closing_time.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1677) ' Workday Start

    For _idx As Integer = 0 To 23
      cmb_closing_time.Add(_idx, _idx.ToString("00") & ":00")
    Next

    cmb_closing_time.SelectedIndex = -1

    gb_schedule.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1676) ' Schedule

    chk_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301) ' Monday
    chk_tuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302) ' Tuesday
    chk_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303) ' Wednesday
    chk_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304) ' Thurdsay
    chk_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305) ' Friday
    chk_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306) ' Saturday
    chk_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307) ' Sunday

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    ' Monday
    dtp_monday_time1_from.Top = chk_monday.Top
    dtp_monday_time1_from.TabIndex = chk_monday.TabIndex + 1
    dtp_monday_time1_to.Top = chk_monday.Top
    dtp_monday_time1_to.TabIndex = chk_monday.TabIndex + 2
    chk_monday_time2.Top = chk_monday.Top
    chk_monday_time2.TabIndex = chk_monday.TabIndex + 3
    dtp_monday_time2_from.Top = chk_monday.Top
    dtp_monday_time2_from.TabIndex = chk_monday.TabIndex + 4
    dtp_monday_time2_to.Top = chk_monday.Top
    dtp_monday_time2_to.TabIndex = chk_monday.TabIndex + 5

    ' Tuesday
    dtp_tuesday_time1_from.Top = chk_tuesday.Top
    dtp_tuesday_time1_from.TabIndex = chk_tuesday.TabIndex + 1
    dtp_tuesday_time1_to.Top = chk_tuesday.Top
    dtp_tuesday_time1_to.TabIndex = chk_tuesday.TabIndex + 2
    chk_tuesday_time2.Top = chk_tuesday.Top
    chk_tuesday_time2.TabIndex = chk_tuesday.TabIndex + 3
    dtp_tuesday_time2_from.Top = chk_tuesday.Top
    dtp_tuesday_time2_from.TabIndex = chk_tuesday.TabIndex + 4
    dtp_tuesday_time2_to.Top = chk_tuesday.Top
    dtp_tuesday_time2_to.TabIndex = chk_tuesday.TabIndex + 5

    ' Wednesday
    dtp_wednesday_time1_from.Top = chk_wednesday.Top
    dtp_wednesday_time1_from.TabIndex = chk_wednesday.TabIndex + 1
    dtp_wednesday_time1_to.Top = chk_wednesday.Top
    dtp_wednesday_time1_to.TabIndex = chk_wednesday.TabIndex + 2
    chk_wednesday_time2.Top = chk_wednesday.Top
    chk_wednesday_time2.TabIndex = chk_wednesday.TabIndex + 3
    dtp_wednesday_time2_from.Top = chk_wednesday.Top
    dtp_wednesday_time2_from.TabIndex = chk_wednesday.TabIndex + 4
    dtp_wednesday_time2_to.Top = chk_wednesday.Top
    dtp_wednesday_time2_to.TabIndex = chk_wednesday.TabIndex + 5

    ' Thursday
    dtp_thursday_time1_from.Top = chk_thursday.Top
    dtp_thursday_time1_from.TabIndex = chk_thursday.TabIndex + 1
    dtp_thursday_time1_to.Top = chk_thursday.Top
    dtp_thursday_time1_to.TabIndex = chk_thursday.TabIndex + 2
    chk_thursday_time2.Top = chk_thursday.Top
    chk_thursday_time2.TabIndex = chk_thursday.TabIndex + 3
    dtp_thursday_time2_from.Top = chk_thursday.Top
    dtp_thursday_time2_from.TabIndex = chk_thursday.TabIndex + 4
    dtp_thursday_time2_to.Top = chk_thursday.Top
    dtp_thursday_time2_to.TabIndex = chk_thursday.TabIndex + 5

    ' Friday
    dtp_friday_time1_from.Top = chk_friday.Top
    dtp_friday_time1_from.TabIndex = chk_friday.TabIndex + 1
    dtp_friday_time1_to.Top = chk_friday.Top
    dtp_friday_time1_to.TabIndex = chk_friday.TabIndex + 2
    chk_friday_time2.Top = chk_friday.Top
    chk_friday_time2.TabIndex = chk_friday.TabIndex + 3
    dtp_friday_time2_from.Top = chk_friday.Top
    dtp_friday_time2_from.TabIndex = chk_friday.TabIndex + 4
    dtp_friday_time2_to.Top = chk_friday.Top
    dtp_friday_time2_to.TabIndex = chk_friday.TabIndex + 5

    ' Saturday
    dtp_saturday_time1_from.Top = chk_saturday.Top
    dtp_saturday_time1_from.TabIndex = chk_saturday.TabIndex + 1
    dtp_saturday_time1_to.Top = chk_saturday.Top
    dtp_saturday_time1_to.TabIndex = chk_saturday.TabIndex + 2
    chk_saturday_time2.Top = chk_saturday.Top
    chk_saturday_time2.TabIndex = chk_saturday.TabIndex + 3
    dtp_saturday_time2_from.Top = chk_saturday.Top
    dtp_saturday_time2_from.TabIndex = chk_saturday.TabIndex + 4
    dtp_saturday_time2_to.Top = chk_saturday.Top
    dtp_saturday_time2_to.TabIndex = chk_saturday.TabIndex + 5

    ' Sunday
    dtp_sunday_time1_from.Top = chk_sunday.Top
    dtp_sunday_time1_from.TabIndex = chk_sunday.TabIndex + 1
    dtp_sunday_time1_to.Top = chk_sunday.Top
    dtp_sunday_time1_to.TabIndex = chk_sunday.TabIndex + 2
    chk_sunday_time2.Top = chk_sunday.Top
    chk_sunday_time2.TabIndex = chk_sunday.TabIndex + 3
    dtp_sunday_time2_from.Top = chk_sunday.Top
    dtp_sunday_time2_from.TabIndex = chk_sunday.TabIndex + 4
    dtp_sunday_time2_to.Top = chk_sunday.Top
    dtp_sunday_time2_to.TabIndex = chk_sunday.TabIndex + 5

    For Each _dtp As uc_date_picker In m_schedule_time1_from
      _dtp.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Next

    For Each _dtp As uc_date_picker In m_schedule_time1_to
      _dtp.TextVisible = True
      _dtp.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1670) ' to
      _dtp.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Next

    For Each _chk As CheckBox In m_schedule_time2_check
      _chk.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1672) ' And
    Next

    For Each _dtp As uc_date_picker In m_schedule_time2_from
      _dtp.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Next

    For Each _dtp As uc_date_picker In m_schedule_time2_to
      _dtp.TextVisible = True
      _dtp.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1670) ' to
      _dtp.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Next

    btn_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1671) ' All
    btn_all.Type = uc_button.ENUM_BUTTON_TYPE.SMALL

    'EXCEPTIONS TAB
    tab_exceptions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1682) ' Exceptions
    btn_delete_exceptions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1683) ' Delete
    btn_add_exception.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684) ' Add
    gb_new_exception.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1685) ' New Exception

    dtp_new_exception_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_new_exception_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    dtp_new_exception_date_from.SetRange(Today, Today.AddYears(5))
    dtp_new_exception_date_to.SetRange(dtp_new_exception_date_from.Value, dtp_new_exception_date_from.Value.AddYears(5))

    chk_new_exception_allow.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1664) ' Allow operations
    chk_new_exception_date_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1670) ' to
    chk_new_exception_time2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1672) ' and

    dtp_new_exception_time1_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)

    dtp_new_exception_time1_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    dtp_new_exception_time1_to.TextVisible = True
    dtp_new_exception_time1_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1670) ' to

    dtp_new_exception_time2_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)

    dtp_new_exception_time2_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)
    dtp_new_exception_time2_to.TextVisible = True
    dtp_new_exception_time2_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1670) ' to

    With Me.dg_exceptions

      Call .Init(GRID_EXCEPTIONS_COLUMNS, GRID_EXCEPTIONS_HEADER_ROWS)
      .Counter(0).Visible = False
      .Sortable = False

      'Hidden ID column
      .Column(GRID_EXCEPTIONS_COLUMN_ID).Header.Text = ""
      .Column(GRID_EXCEPTIONS_COLUMN_ID).WidthFixed = 0

      'Description column
      .Column(GRID_EXCEPTIONS_COLUMN_DESCRIPTION).Header.Text = ""
      .Column(GRID_EXCEPTIONS_COLUMN_DESCRIPTION).WidthFixed = 8020

    End With

    'CALENDAR TAB
    tab_calendar.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1693) ' Calendar
    calendar.MinDate = Today
    lbl_calendar_schedule.Text = ""
    lbl_calendar_error.Text = ""
    lbl_calendar_instructions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1669) ' Select a date...

    m_calendar_selected_range = calendar.SelectionRange

    'NOTIFICATIONS TAB
    tab_notifications.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1689) ' Notifications
    chk_notifications_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1699) ' Send e-mail notifications...

    ef_subject.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1700) ' Subject
    ef_subject.TextVisible = True
    ef_subject.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)

    gb_notifications_recipients.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1701) ' Recipient(s)
    btn_delete_recipients.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1683) ' Delete
    gb_new_recipient.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1702) ' New recipient

    ef_new_recipient.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1703) ' E-mail
    ef_new_recipient.TextVisible = True
    ef_new_recipient.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)

    btn_add_recipient.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684) ' Add

    With Me.dg_recipients

      Call .Init(GRID_RECIPIENTS_COLUMNS, GRID_RECIPIENTS_HEADER_ROWS)
      .Counter(0).Visible = False
      .Sortable = False

      'E-mail column
      .Column(GRID_RECIPIENTS_COLUMN_EMAIL).Header.Text = ""
      .Column(GRID_RECIPIENTS_COLUMN_EMAIL).WidthFixed = 6250

    End With

    'SHIFTS TAB
    tab_shifts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2581) ' Shifts

    gb_work_shifts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2562) ' Work Shifts
    chk_enable_shift_duration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2563) ' Limit Work Shift Duration
    ef_work_shift_duration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2564) ' Maximum Duration
    ef_work_shift_duration.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2582) ' hours
    ef_work_shift_duration.TextVisible = True
    ef_work_shift_duration.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    gb_daily_cash_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2583) ' Daily Cash Sessions
    ef_max_daily_cash_openings.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2584) ' Maximum Daily Cash Openings per User
    ef_max_daily_cash_openings.TextVisible = True
    ef_max_daily_cash_openings.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 1)
    lbl_info_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2585, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2580)) ' To open additional cash sessions...

    'Add handlers
    For Each _chk As CheckBox In m_schedule_allowed
      AddHandler _chk.CheckedChanged, AddressOf DisableControlsEvent
    Next

    For Each _chk As CheckBox In m_schedule_time2_check
      AddHandler _chk.CheckedChanged, AddressOf DisableControlsEvent
    Next

    AddHandler chk_new_exception_allow.CheckedChanged, AddressOf DisableControlsEvent
    AddHandler chk_new_exception_date_range.CheckedChanged, AddressOf DisableControlsEvent
    AddHandler chk_new_exception_time2.CheckedChanged, AddressOf DisableControlsEvent

    'Select the appropriate tab depending on user permissions
    If Permissions.Write Then
      tab_control.SelectedTab = tab_schedule
    Else
      tab_control.SelectedTab = tab_calendar
    End If

  End Sub

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_OPERATIONS_SCHEDULE

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _operations_schedule As CLASS_OPERATIONS_SCHEDULE

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Min database version popup alerdy showed. Don't show any message again
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_OPERATIONS_SCHEDULE
        _operations_schedule = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then

          Me.DbStatus = ENUM_STATUS.STATUS_OK

        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub ' GUI_DB_Operation

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _operations_schedule As CLASS_OPERATIONS_SCHEDULE
    Dim _idx As Integer
    Dim _sched As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _recipient As String

    _operations_schedule = DbReadObject

    'SCHEDULE TAB
    chk_schedule_enabled.Checked = _operations_schedule.ScheduleEnabled
    chk_disable_machines.Checked = _operations_schedule.DisableMachines

    cmb_closing_time.SelectedIndex = _operations_schedule.ClosingHour

    For _idx = 0 To m_schedule_allowed.Length - 1
      m_schedule_allowed(_idx).Checked = _operations_schedule.DayOfWeek_OperationsAllowed(_idx)
      m_schedule_time1_from(_idx).Value = Today.Date.AddMinutes(_operations_schedule.DayOfWeek_Time1FromMinutes(_idx))
      m_schedule_time1_to(_idx).Value = Today.Date.AddMinutes(_operations_schedule.DayOfWeek_Time1ToMinutes(_idx))

      If _operations_schedule.DayOfWeek_Time2FromMinutes(_idx) = -1 Or _operations_schedule.DayOfWeek_Time2ToMinutes(_idx) = -1 Then
        m_schedule_time2_check(_idx).Checked = False
        m_schedule_time2_from(_idx).Value = Today.Date
        m_schedule_time2_to(_idx).Value = Today.Date
      Else
        m_schedule_time2_check(_idx).Checked = True
        m_schedule_time2_from(_idx).Value = Today.Date.AddMinutes(_operations_schedule.DayOfWeek_Time2FromMinutes(_idx))
        m_schedule_time2_to(_idx).Value = Today.Date.AddMinutes(_operations_schedule.DayOfWeek_Time2ToMinutes(_idx))
      End If
    Next

    'EXCEPTIONS TAB
    m_exceptions_list = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    For Each _sched In _operations_schedule.ExceptionsList
      m_exceptions_list.Add(_sched.Clone())
    Next

    m_exceptions_deleted = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    For Each _sched In _operations_schedule.DeletedExceptionsList
      m_exceptions_deleted.Add(_sched.Clone())
    Next

    Call Me.RefreshExceptionsList()

    If cmb_closing_time.SelectedIndex = -1 Then
      dtp_new_exception_time1_from.Value = Today.Date
      dtp_new_exception_time1_to.Value = Today.Date
      dtp_new_exception_time2_from.Value = Today.Date
      dtp_new_exception_time2_to.Value = Today.Date
    Else
      dtp_new_exception_time1_from.Value = Today.Date.AddHours(cmb_closing_time.SelectedIndex)
      dtp_new_exception_time1_to.Value = Today.Date.AddHours(cmb_closing_time.SelectedIndex)
      dtp_new_exception_time2_from.Value = Today.Date.AddHours(cmb_closing_time.SelectedIndex)
      dtp_new_exception_time2_to.Value = Today.Date.AddHours(cmb_closing_time.SelectedIndex)
    End If

    'CALENDAR TAB
    Call Me.RefreshCalendarSchedule()
    Call Me.RefreshCalendarBoldedDates()

    'NOTIFICATIONS TAB
    m_recipients = New List(Of String)
    m_recipients_added = New List(Of String)
    m_recipients_deleted = New List(Of String)

    For Each _recipient In _operations_schedule.Recipients
      m_recipients.Add(_recipient)
    Next

    chk_notifications_enabled.Checked = _operations_schedule.NotificationsEnabled
    ef_subject.Value = _operations_schedule.NotificationsSubject

    Call Me.RefreshRecipientsList()

    'SHIFTS TAB
    If _operations_schedule.WorkShiftDuration = 0 Then
      chk_enable_shift_duration.Checked = False
    Else
      chk_enable_shift_duration.Checked = True
      ef_work_shift_duration.Value = _operations_schedule.WorkShiftDuration
    End If

    Call chk_enable_shift_duration_CheckedChanged(Me, Nothing)

    ef_max_daily_cash_openings.Value = _operations_schedule.MaxDailyCashOpenings
    Call Me.ef_max_daily_cash_openings_EntryFieldValueChanged()

    'Disable controls when appropriate
    Call Me.DisableControls()

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _operations_schedule As CLASS_OPERATIONS_SCHEDULE
    Dim _idx As Integer

    _operations_schedule = DbEditedObject

    _operations_schedule.ScheduleEnabled = chk_schedule_enabled.Checked
    _operations_schedule.DisableMachines = chk_disable_machines.Checked

    _operations_schedule.ClosingHour = cmb_closing_time.SelectedIndex

    'Days of week
    For _idx = 0 To m_schedule_allowed.Length - 1
      _operations_schedule.DayOfWeek_OperationsAllowed(_idx) = m_schedule_allowed(_idx).Checked
      _operations_schedule.DayOfWeek_Time1FromMinutes(_idx) = m_schedule_time1_from(_idx).Value.TimeOfDay.TotalMinutes
      _operations_schedule.DayOfWeek_Time1ToMinutes(_idx) = m_schedule_time1_to(_idx).Value.TimeOfDay.TotalMinutes

      If m_schedule_time2_check(_idx).Checked = False Then
        _operations_schedule.DayOfWeek_Time2FromMinutes(_idx) = -1
        _operations_schedule.DayOfWeek_Time2ToMinutes(_idx) = -1
      Else
        _operations_schedule.DayOfWeek_Time2FromMinutes(_idx) = m_schedule_time2_from(_idx).Value.TimeOfDay.TotalMinutes
        _operations_schedule.DayOfWeek_Time2ToMinutes(_idx) = m_schedule_time2_to(_idx).Value.TimeOfDay.TotalMinutes
      End If
    Next

    'Exceptions
    _operations_schedule.ExceptionsList = m_exceptions_list
    _operations_schedule.DeletedExceptionsList = m_exceptions_deleted

    'Notifications
    _operations_schedule.NotificationsEnabled = chk_notifications_enabled.Checked
    _operations_schedule.NotificationsSubject = ef_subject.Value
    _operations_schedule.Recipients = m_recipients
    _operations_schedule.RecipientsAdded = m_recipients_added
    _operations_schedule.RecipientsDeleted = m_recipients_deleted

    'Shifts
    If chk_enable_shift_duration.Checked = True Then
      _operations_schedule.WorkShiftDuration = IIf(ef_work_shift_duration.Value = "", 0, ef_work_shift_duration.Value)
    Else
      _operations_schedule.WorkShiftDuration = 0
    End If

    _operations_schedule.MaxDailyCashOpenings = IIf(ef_max_daily_cash_openings.Value = "", 0, ef_max_daily_cash_openings.Value)

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _result As Boolean
    Dim _selected_days_week As Integer
    Dim _day_of_week As DayOfWeek
    Dim _day_of_week_nlsid As Integer
    Dim _idx As Integer
    Dim _sched As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _validation_reason As TYPE_OPERATIONS_SCHEDULE_ITEM.VALIDATION_RESULT
    Dim _validation_reason_string As String

    _validation_reason_string = ""

    'Check that Closing Time is configured
    If cmb_closing_time.SelectedIndex = -1 Then
      tab_control.SelectedTab = tab_schedule
      Call cmb_closing_time.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1742), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must select a Workday Start time

      Return False
    End If

    'Check that Operations Schedule is enabled
    If chk_schedule_enabled.Checked = True Then

      ' DAYS OF WEEK
      'No days of week selected
      _selected_days_week = 0

      For Each _chk As CheckBox In m_schedule_allowed
        If _chk.Checked Then
          _selected_days_week += 1
        End If
      Next

      If _selected_days_week = 0 Then
        tab_control.SelectedTab = tab_schedule
        Call m_schedule_allowed(DayOfWeek.Monday).Focus()

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1679), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Return False
        End If
      End If

      'Validate each Day of Week
      For _idx = 0 To m_schedule_allowed.Length - 1

        _day_of_week = _idx

        'Do not validate unchecked days of week
        If m_schedule_allowed(_idx).Checked = False Then

          Continue For
        End If

        _sched = New TYPE_OPERATIONS_SCHEDULE_ITEM
        _sched.time1_from_minutes = m_schedule_time1_from(_idx).Value.TimeOfDay.TotalMinutes
        _sched.time1_to_minutes = m_schedule_time1_to(_idx).Value.TimeOfDay.TotalMinutes

        If m_schedule_time2_check(_idx).Checked = False Then
          _sched.time2_from_minutes = -1
          _sched.time2_to_minutes = -1
        Else
          _sched.time2_from_minutes = m_schedule_time2_from(_idx).Value.TimeOfDay.TotalMinutes
          _sched.time2_to_minutes = m_schedule_time2_to(_idx).Value.TimeOfDay.TotalMinutes
        End If

        _result = _sched.IsValid(New Date().AddHours(cmb_closing_time.SelectedIndex), _validation_reason)

        If _result = False Then
          Select Case _day_of_week
            Case DayOfWeek.Monday
              _day_of_week_nlsid = 301 ' Monday
            Case DayOfWeek.Tuesday
              _day_of_week_nlsid = 302 ' Tuesday
            Case DayOfWeek.Wednesday
              _day_of_week_nlsid = 303 ' Wednesday
            Case DayOfWeek.Thursday
              _day_of_week_nlsid = 304 ' Thursday
            Case DayOfWeek.Friday
              _day_of_week_nlsid = 305 ' Friday
            Case DayOfWeek.Saturday
              _day_of_week_nlsid = 306 ' Saturday
            Case DayOfWeek.Sunday
              _day_of_week_nlsid = 307 ' Sunday
          End Select

          Select Case _validation_reason
            Case TYPE_OPERATIONS_SCHEDULE_ITEM.VALIDATION_RESULT.OUTSIDE_WORKDAY
              _validation_reason_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1665) ' schedule is beyond the workday limits
            Case TYPE_OPERATIONS_SCHEDULE_ITEM.VALIDATION_RESULT.OVERLAPPING_RANGES
              _validation_reason_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1666) ' overlapping time ranges
          End Select

          tab_control.SelectedTab = tab_schedule
          Call m_schedule_time1_from(_day_of_week).Focus()

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1681), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(_day_of_week_nlsid), _
                          _validation_reason_string)

          Return False
        End If
      Next

      'EXCEPTIONS
      For Each _sched In m_exceptions_list

        'Do not validate unchecked days
        If _sched.operations_allowed Then

          'Check that time ranges are valid
          If Not _sched.IsValid(New Date().AddHours(cmb_closing_time.SelectedIndex), _validation_reason) Then

            Select Case _validation_reason
              Case TYPE_OPERATIONS_SCHEDULE_ITEM.VALIDATION_RESULT.OUTSIDE_WORKDAY
                _validation_reason_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1665) ' schedule is beyond the workday limits
              Case TYPE_OPERATIONS_SCHEDULE_ITEM.VALIDATION_RESULT.OVERLAPPING_RANGES
                _validation_reason_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1666) ' overlapping time ranges
            End Select

            tab_control.SelectedTab = tab_exceptions

            For _idx = 0 To dg_exceptions.NumRows - 1
              If dg_exceptions.Cell(_idx, GRID_EXCEPTIONS_COLUMN_ID).Value = _sched.id Then
                dg_exceptions.Row(_idx).IsSelected = True
                dg_exceptions.RepaintRow(_idx)

                Exit For
              End If
            Next

            Call dtp_new_exception_time1_from.Focus()

            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1667), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _validation_reason_string)

            Return False
          End If

        End If
      Next

      'NOTIFICATIONS
      'Blank subject
      If chk_notifications_enabled.Checked = True And ef_subject.Value = "" Then
        tab_control.SelectedTab = tab_notifications
        Call ef_subject.Focus()

        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_subject.Text) ' You must enter a value for Subject

        Return False
      End If

      'Empty recipient list
      If chk_notifications_enabled.Checked = True And m_recipients.Count = 0 Then
        tab_control.SelectedTab = tab_notifications
        Call ef_new_recipient.Focus()

        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1704), ENUM_MB_TYPE.MB_TYPE_WARNING) ' You must enter at least a recipient

        Return False
      End If

      'Validate recipients
      For _idx = 0 To dg_recipients.NumRows - 1
        If Not WSI.Common.ValidateFormat.Email(dg_recipients.Cell(_idx, GRID_RECIPIENTS_COLUMN_EMAIL).Value) Then
          dg_recipients.Row(_idx).IsSelected = True
          dg_recipients.RepaintRow(_idx)
          tab_control.SelectedTab = tab_notifications
          Call dg_recipients.Focus()

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1705), ENUM_MB_TYPE.MB_TYPE_WARNING) ' The e-mail address entered is not valid

          Return False
        End If
      Next

    End If

    'SHIFTS
    'Blank work shift duration
    If chk_enable_shift_duration.Checked = True And ef_work_shift_duration.Value = "" Then
      tab_control.SelectedTab = tab_shifts
      Call ef_work_shift_duration.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_work_shift_duration.Text) ' You must enter a value for Work Shift Duration

      Return False
    End If

    'Blank max daily cash openings
    If ef_max_daily_cash_openings.Value = "" Then
      tab_control.SelectedTab = tab_shifts
      Call ef_max_daily_cash_openings.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_max_daily_cash_openings.Text) ' You must enter a value for Work Shift Duration

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Arranges form controls in collections for better management.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ArrangeControls()

    'Schedule: Allowed
    m_schedule_allowed(DayOfWeek.Monday) = chk_monday
    m_schedule_allowed(DayOfWeek.Tuesday) = chk_tuesday
    m_schedule_allowed(DayOfWeek.Wednesday) = chk_wednesday
    m_schedule_allowed(DayOfWeek.Thursday) = chk_thursday
    m_schedule_allowed(DayOfWeek.Friday) = chk_friday
    m_schedule_allowed(DayOfWeek.Saturday) = chk_saturday
    m_schedule_allowed(DayOfWeek.Sunday) = chk_sunday

    'Schedule: Time1From
    m_schedule_time1_from(DayOfWeek.Monday) = dtp_monday_time1_from
    m_schedule_time1_from(DayOfWeek.Tuesday) = dtp_tuesday_time1_from
    m_schedule_time1_from(DayOfWeek.Wednesday) = dtp_wednesday_time1_from
    m_schedule_time1_from(DayOfWeek.Thursday) = dtp_thursday_time1_from
    m_schedule_time1_from(DayOfWeek.Friday) = dtp_friday_time1_from
    m_schedule_time1_from(DayOfWeek.Saturday) = dtp_saturday_time1_from
    m_schedule_time1_from(DayOfWeek.Sunday) = dtp_sunday_time1_from

    'Schedule: Time1To
    m_schedule_time1_to(DayOfWeek.Monday) = dtp_monday_time1_to
    m_schedule_time1_to(DayOfWeek.Tuesday) = dtp_tuesday_time1_to
    m_schedule_time1_to(DayOfWeek.Wednesday) = dtp_wednesday_time1_to
    m_schedule_time1_to(DayOfWeek.Thursday) = dtp_thursday_time1_to
    m_schedule_time1_to(DayOfWeek.Friday) = dtp_friday_time1_to
    m_schedule_time1_to(DayOfWeek.Saturday) = dtp_saturday_time1_to
    m_schedule_time1_to(DayOfWeek.Sunday) = dtp_sunday_time1_to

    'Schedule: Time2 Checkbox
    m_schedule_time2_check(DayOfWeek.Monday) = chk_monday_time2
    m_schedule_time2_check(DayOfWeek.Tuesday) = chk_tuesday_time2
    m_schedule_time2_check(DayOfWeek.Wednesday) = chk_wednesday_time2
    m_schedule_time2_check(DayOfWeek.Thursday) = chk_thursday_time2
    m_schedule_time2_check(DayOfWeek.Friday) = chk_friday_time2
    m_schedule_time2_check(DayOfWeek.Saturday) = chk_saturday_time2
    m_schedule_time2_check(DayOfWeek.Sunday) = chk_sunday_time2

    'Schedule: Time2From
    m_schedule_time2_from(DayOfWeek.Monday) = dtp_monday_time2_from
    m_schedule_time2_from(DayOfWeek.Tuesday) = dtp_tuesday_time2_from
    m_schedule_time2_from(DayOfWeek.Wednesday) = dtp_wednesday_time2_from
    m_schedule_time2_from(DayOfWeek.Thursday) = dtp_thursday_time2_from
    m_schedule_time2_from(DayOfWeek.Friday) = dtp_friday_time2_from
    m_schedule_time2_from(DayOfWeek.Saturday) = dtp_saturday_time2_from
    m_schedule_time2_from(DayOfWeek.Sunday) = dtp_sunday_time2_from

    'Schedule: Time2To
    m_schedule_time2_to(DayOfWeek.Monday) = dtp_monday_time2_to
    m_schedule_time2_to(DayOfWeek.Tuesday) = dtp_tuesday_time2_to
    m_schedule_time2_to(DayOfWeek.Wednesday) = dtp_wednesday_time2_to
    m_schedule_time2_to(DayOfWeek.Thursday) = dtp_thursday_time2_to
    m_schedule_time2_to(DayOfWeek.Friday) = dtp_friday_time2_to
    m_schedule_time2_to(DayOfWeek.Saturday) = dtp_saturday_time2_to
    m_schedule_time2_to(DayOfWeek.Sunday) = dtp_sunday_time2_to

  End Sub ' ArrangeControls

  ' PURPOSE: Disables controls depending on checkboxes status.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DisableControls()

    Dim _idx As Integer

    'SCHEDULE TAB
    For _idx = 0 To m_schedule_allowed.Length - 1
      If m_schedule_allowed(_idx).Checked = False Then
        m_schedule_time1_from(_idx).Enabled = False
        m_schedule_time1_to(_idx).Enabled = False
        m_schedule_time2_check(_idx).Enabled = False
        m_schedule_time2_from(_idx).Enabled = False
        m_schedule_time2_to(_idx).Enabled = False
      Else
        m_schedule_time1_from(_idx).Enabled = True
        m_schedule_time1_to(_idx).Enabled = True
        m_schedule_time2_check(_idx).Enabled = True

        If m_schedule_time2_check(_idx).Checked = False Then
          m_schedule_time2_from(_idx).Enabled = False
          m_schedule_time2_to(_idx).Enabled = False
        Else
          m_schedule_time2_from(_idx).Enabled = True
          m_schedule_time2_to(_idx).Enabled = True
        End If
      End If
    Next

    'EXCEPTION TAB
    If chk_new_exception_allow.Checked = False Then
      dtp_new_exception_time1_from.Enabled = False
      dtp_new_exception_time1_to.Enabled = False
      chk_new_exception_time2.Enabled = False
      dtp_new_exception_time2_from.Enabled = False
      dtp_new_exception_time2_to.Enabled = False
    Else
      dtp_new_exception_time1_from.Enabled = True
      dtp_new_exception_time1_to.Enabled = True
      chk_new_exception_time2.Enabled = True

      If chk_new_exception_time2.Checked = False Then
        dtp_new_exception_time2_from.Enabled = False
        dtp_new_exception_time2_to.Enabled = False
      Else
        dtp_new_exception_time2_from.Enabled = True
        dtp_new_exception_time2_to.Enabled = True
      End If
    End If

    If chk_new_exception_date_range.Checked = False Then
      dtp_new_exception_date_to.Enabled = False
    Else
      dtp_new_exception_date_to.Enabled = True
    End If

  End Sub ' DisableControls

  ' PURPOSE: Refresh the exceptions list.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshExceptionsList()

    Dim _original_list As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    Dim _sorted_list As List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    Dim _min_item As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _min_idx As Integer
    Dim _idx As Integer
    Dim _exc As TYPE_OPERATIONS_SCHEDULE_ITEM

    _original_list = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)
    _sorted_list = New List(Of TYPE_OPERATIONS_SCHEDULE_ITEM)

    For Each _exc In m_exceptions_list
      _original_list.Add(_exc.Clone())
    Next

    'Sort items
    While _sorted_list.Count < m_exceptions_list.Count

      _min_item = New TYPE_OPERATIONS_SCHEDULE_ITEM
      _min_item.date_from = Date.MaxValue

      For _idx = 0 To _original_list.Count - 1
        If _original_list(_idx).date_from < _min_item.date_from Then
          _min_item = _original_list(_idx)
          _min_idx = _idx
        ElseIf _original_list(_idx).date_from = _min_item.date_from And _original_list(_idx).date_to > _min_item.date_to Then
          _min_item = _original_list(_idx)
          _min_idx = _idx
        End If
      Next

      _sorted_list.Add(_min_item)
      _original_list.RemoveAt(_min_idx)

    End While

    dg_exceptions.Clear()

    For _idx = 0 To _sorted_list.Count - 1
      dg_exceptions.AddRow()
      dg_exceptions.Cell(_idx, GRID_EXCEPTIONS_COLUMN_ID).Value = _sorted_list(_idx).id
      dg_exceptions.Cell(_idx, GRID_EXCEPTIONS_COLUMN_DESCRIPTION).Value = _sorted_list(_idx).ToString()
    Next

    If _sorted_list.Count = 0 Then
      btn_delete_exceptions.Enabled = False
    Else
      btn_delete_exceptions.Enabled = True
    End If

  End Sub ' RefreshExceptionsList

  ' PURPOSE: Adds an exception to the list with the entered data.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AddException()

    Dim _exception As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _exc As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _date_range As String

    _exception = New TYPE_OPERATIONS_SCHEDULE_ITEM

    'New exceptions must have ID < 0
    _exception.id = m_exceptions_added_idx
    m_exceptions_added_idx -= 1

    _exception.type = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE
    _exception.date_from = dtp_new_exception_date_from.Value.Date

    If chk_new_exception_date_range.Checked = False Then
      _exception.date_to = dtp_new_exception_date_from.Value.Date
    Else
      _exception.date_to = dtp_new_exception_date_to.Value.Date
    End If

    _exception.operations_allowed = chk_new_exception_allow.Checked

    _exception.time1_from_minutes = dtp_new_exception_time1_from.Value.TimeOfDay.TotalMinutes
    _exception.time1_to_minutes = dtp_new_exception_time1_to.Value.TimeOfDay.TotalMinutes

    If chk_new_exception_time2.Checked = False Then
      _exception.time2_from_minutes = -1
      _exception.time2_to_minutes = -1
    Else
      _exception.time2_from_minutes = dtp_new_exception_time2_from.Value.TimeOfDay.TotalMinutes
      _exception.time2_to_minutes = dtp_new_exception_time2_to.Value.TimeOfDay.TotalMinutes
    End If

    'Check that date_to is after or equal to date_from
    If _exception.date_to < _exception.date_from Then

      tab_control.SelectedTab = tab_exceptions
      Call dtp_new_exception_date_to.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1688), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    'Check that exception is not already on the list
    If _exception.date_from = _exception.date_to Then
      For Each _exc In m_exceptions_list
        If _exc.date_from = _exc.date_to And _exc.date_from = _exception.date_from Then

          tab_control.SelectedTab = tab_exceptions
          Call dtp_new_exception_date_from.Focus()

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1686), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _exc.date_from.ToShortDateString())

          Return
        End If
      Next
    Else
      For Each _exc In m_exceptions_list
        If _exc.date_from <> _exc.date_to Then
          If Date.Compare(_exc.date_from, _exception.date_to) <= 0 And Date.Compare(_exc.date_to, _exception.date_from) >= 0 Then
            _date_range = _exception.date_from.ToShortDateString() + " - " + _exception.date_to.ToShortDateString()

            tab_control.SelectedTab = tab_exceptions
            Call dtp_new_exception_date_from.Focus()

            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1687), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _date_range)

            Return
          End If
        End If
      Next
    End If

    m_exceptions_list.Add(_exception)
    Call Me.RefreshExceptionsList()
    Call Me.RefreshCalendarBoldedDates()

  End Sub ' AddException

  ' PURPOSE: Deletes one or multiple exceptions from the list.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DeleteExceptions()

    Dim _row_id As Integer
    Dim _idx As Integer
    Dim _exception As TYPE_OPERATIONS_SCHEDULE_ITEM

    If dg_exceptions.SelectedRows Is Nothing Then

      Return
    End If

    For Each _row_id In dg_exceptions.SelectedRows

      For _idx = 0 To m_exceptions_list.Count - 1
        If m_exceptions_list(_idx).id = dg_exceptions.Cell(_row_id, GRID_EXCEPTIONS_COLUMN_ID).Value Then
          _exception = m_exceptions_list(_idx).Clone()
          m_exceptions_list.RemoveAt(_idx)

          'Add to deleted list, only if it was not new
          If dg_exceptions.Cell(_row_id, GRID_EXCEPTIONS_COLUMN_ID).Value > 0 Then
            m_exceptions_deleted.Add(_exception)
          End If

          Exit For
        End If
      Next
    Next

    Call Me.RefreshExceptionsList()
    Call Me.RefreshCalendarBoldedDates()

  End Sub ' DeleteException

  ' PURPOSE: Refresh the schedule description for the selected day in the calendar.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshCalendarSchedule()

    Dim _sched As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _exception As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _day_of_week As DayOfWeek
    Dim _reason As TYPE_OPERATIONS_SCHEDULE_ITEM.VALIDATION_RESULT

    _sched = New TYPE_OPERATIONS_SCHEDULE_ITEM

    lbl_calendar_error.Text = ""

    'Check that Closing Time is configured
    If cmb_closing_time.SelectedIndex = -1 Then
      lbl_calendar_schedule.Text = ""
      lbl_calendar_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1742) ' You must select a Workday Start time

      Return
    End If

    'Find the appropriate schedule
    While True

      'Single day exceptions
      For Each _exception In m_exceptions_list
        If _exception.date_from = _exception.date_to And _exception.date_from = calendar.SelectionRange.Start Then
          _sched = _exception

          Exit While
        End If
      Next

      'Date range exceptions
      For Each _exception In m_exceptions_list
        If _exception.date_from <> _exception.date_to And _
           calendar.SelectionRange.Start >= _exception.date_from And calendar.SelectionRange.Start <= _exception.date_to Then
          _sched = _exception

          Exit While
        End If
      Next

      'Days of week
      _day_of_week = calendar.SelectionRange.Start.DayOfWeek

      _sched.type = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE
      _sched.date_from = calendar.SelectionRange.Start
      _sched.date_to = _sched.date_from
      _sched.operations_allowed = m_schedule_allowed(_day_of_week).Checked
      _sched.time1_from_minutes = m_schedule_time1_from(_day_of_week).Value.TimeOfDay.TotalMinutes
      _sched.time1_to_minutes = m_schedule_time1_to(_day_of_week).Value.TimeOfDay.TotalMinutes

      If m_schedule_time2_check(_day_of_week).Checked = False Then
        _sched.time2_from_minutes = -1
        _sched.time2_to_minutes = -1
      Else
        _sched.time2_from_minutes = m_schedule_time2_from(_day_of_week).Value.TimeOfDay.TotalMinutes
        _sched.time2_to_minutes = m_schedule_time2_to(_day_of_week).Value.TimeOfDay.TotalMinutes
      End If

      Exit While
    End While

    If Not _sched.IsValid(Today.Date.AddHours(cmb_closing_time.SelectedIndex), _reason) Then
      lbl_calendar_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1668) ' Schedule is not valid
    End If

    lbl_calendar_schedule.Text = _sched.ToString()

  End Sub ' RefreshCalendarSchedule

  ' PURPOSE: Refresh the bolded dates in the calendar.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshCalendarBoldedDates()

    Dim _sched As TYPE_OPERATIONS_SCHEDULE_ITEM
    Dim _bolded_dates As List(Of Date)
    Dim _date As Date

    _bolded_dates = New List(Of Date)

    For Each _sched In m_exceptions_list

      _date = _sched.date_from

      While _date <= _sched.date_to
        _bolded_dates.Add(_date)
        _date = _date.AddDays(1)
      End While

    Next

    calendar.BoldedDates = _bolded_dates.ToArray()

    Call calendar.Refresh()

  End Sub ' RefreshCalendarBoldedDates

  ' PURPOSE: Refresh the recipients list.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshRecipientsList()

    Dim _idx As Integer

    dg_recipients.Clear()

    For _idx = 0 To m_recipients.Count - 1
      dg_recipients.AddRow()
      dg_recipients.Cell(_idx, GRID_RECIPIENTS_COLUMN_EMAIL).Value = m_recipients(_idx)
    Next

    If m_recipients.Count = 0 Then
      btn_delete_recipients.Enabled = False
    Else
      btn_delete_recipients.Enabled = True
    End If

  End Sub ' RefreshRecipientsList

  ' PURPOSE: Adds a recipient to the list with the entered data.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AddRecipient()

    Dim _recipient As String
    Dim _recipient_aux As String
    Dim _recipients_string As String
    Dim _recipients_added As List(Of String)
    Dim _recipients_array() As String
    Dim _split_delimiters() As Char
    Dim _duplicates_found As Boolean
    Dim _duplicate As Boolean

    'Entry field must not be empty
    If ef_new_recipient.Value = "" Then
      tab_control.SelectedTab = tab_notifications
      Call ef_new_recipient.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_new_recipient.Text) ' You must enter a value for E-mail

      Return
    End If

    ' Parse entered e-mail addresses
    _split_delimiters = New Char() {";", ",", " "}
    _recipients_array = ef_new_recipient.Value.Split(_split_delimiters, System.StringSplitOptions.RemoveEmptyEntries)

    _recipients_added = New List(Of String)
    _duplicates_found = False

    ' Validate each e-mail address
    For Each _recipient In _recipients_array

      ' Validate format
      If Not WSI.Common.ValidateFormat.Email(_recipient) Then
        tab_control.SelectedTab = tab_notifications
        Call ef_new_recipient.Focus()

        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1705), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _recipient) ' Invalid e-mail address: %1

        Return
      End If

      _duplicate = False

      ' Check for duplicates in newly added recipients
      For Each _recipient_aux In _recipients_added
        If _recipient_aux = _recipient Then
          _duplicates_found = True
          _duplicate = True
        End If
      Next

      ' Check for duplicates in existing recipient list
      For Each _recipient_aux In m_recipients
        If _recipient_aux = _recipient Then
          _duplicates_found = True
          _duplicate = True
        End If
      Next

      ' Add recipient to newly added recipients (if it's not duplicate)
      If Not _duplicate Then
        _recipients_added.Add(_recipient)
      End If

    Next

    _recipients_string = String.Join(",", m_recipients.ToArray())
    _recipients_string &= "," & String.Join(",", _recipients_added.ToArray())

    'Check if total recipients length exceeds maximum characters (500) for GeneralParams
    If _recipients_string.Length > 500 Then
      tab_control.SelectedTab = tab_notifications
      Call ef_new_recipient.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1780), ENUM_MB_TYPE.MB_TYPE_WARNING) ' The recipient list is too long.

      Return
    End If

    'Add to list and added list
    For Each _recipient In _recipients_added
      m_recipients.Add(_recipient)
      m_recipients_added.Add(_recipient)
    Next

    'Refresh the list and clear the entry field
    ef_new_recipient.Value = ""
    Call ef_new_recipient.Focus()
    Call Me.RefreshRecipientsList()

    'Notify user of duplicate addresses
    If _duplicates_found Then
      tab_control.SelectedTab = tab_notifications
      Call ef_new_recipient.Focus()

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1706), ENUM_MB_TYPE.MB_TYPE_INFO) ' Duplicate e-mail address where found...

      Return
    End If

  End Sub ' AddRecipient

  ' PURPOSE: Deletes one or multiple recipients from the list.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DeleteRecipients()

    Dim _row_id As Integer
    Dim _recipient As String
    Dim _is_added_recipient As Boolean
    Dim _idx As Integer

    _is_added_recipient = False

    If dg_recipients.SelectedRows Is Nothing Then

      Return
    End If

    For Each _row_id In dg_recipients.SelectedRows

      _recipient = dg_recipients.Cell(_row_id, GRID_RECIPIENTS_COLUMN_EMAIL).Value

      'Check if it's a previously added recipient
      For _idx = 0 To m_recipients_added.Count - 1
        If m_recipients_added(_idx) = _recipient Then
          m_recipients_added.RemoveAt(_idx)
          _is_added_recipient = True

          Exit For
        End If
      Next

      'Add to deleted list if necessary
      If Not _is_added_recipient Then
        m_recipients_deleted.Add(_recipient)
      End If

      'Delete from recipients list
      For _idx = 0 To m_recipients.Count - 1
        If m_recipients(_idx) = _recipient Then
          m_recipients.RemoveAt(_idx)

          Exit For
        End If
      Next
    Next

    Call Me.RefreshRecipientsList()

  End Sub ' DeleteRecipients

#End Region ' Private Functions

#Region " Events "

  Private Sub DisableControlsEvent(ByVal sender As Object, ByVal e As EventArgs)

    Call Me.DisableControls()

  End Sub ' DisableControlsEvent

  ' PURPOSE: Copies first day of week (by system) values to the rest of days of week.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_all_ClickEvent() Handles btn_all.ClickEvent

    Dim _idx As Integer
    Dim _day_off_week As Integer

    _day_off_week = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek()

    For _idx = 0 To m_schedule_allowed.Length - 1
      m_schedule_allowed(_idx).Checked = m_schedule_allowed(_day_off_week).Checked
      m_schedule_time1_from(_idx).Value = m_schedule_time1_from(_day_off_week).Value
      m_schedule_time1_to(_idx).Value = m_schedule_time1_to(_day_off_week).Value
      m_schedule_time2_check(_idx).Checked = m_schedule_time2_check(_day_off_week).Checked
      m_schedule_time2_from(_idx).Value = m_schedule_time2_from(_day_off_week).Value
      m_schedule_time2_to(_idx).Value = m_schedule_time2_to(_day_off_week).Value
    Next

  End Sub ' btn_all_ClickEvent

  ' PURPOSE: Changes new exception date to range according to date from selected value.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub dtp_new_exception_date_from_DatePickerValueChanged() Handles dtp_new_exception_date_from.DatePickerValueChanged

    dtp_new_exception_date_to.SetRange(dtp_new_exception_date_from.Value, dtp_new_exception_date_from.Value.AddYears(5))

  End Sub ' dtp_new_exception_date_from_DatePickerValueChanged

  Private Sub btn_add_exception_ClickEvent() Handles btn_add_exception.ClickEvent

    Call Me.AddException()

  End Sub ' btn_add_exception_ClickEvent

  Private Sub btn_delete_exceptions_ClickEvent() Handles btn_delete_exceptions.ClickEvent

    Call Me.DeleteExceptions()

  End Sub ' btn_delete_exceptions_ClickEvent

  Private Sub calendar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles calendar.KeyUp

    If calendar.SelectionRange.Start <> m_calendar_selected_range.Start Or _
       calendar.SelectionRange.End <> m_calendar_selected_range.End Then

      m_calendar_selected_range = calendar.SelectionRange

      Call Me.RefreshCalendarSchedule()

    End If

  End Sub ' calendar_KeyUp

  Private Sub calendar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles calendar.MouseDown

    If calendar.SelectionRange.Start <> m_calendar_selected_range.Start Or _
       calendar.SelectionRange.End <> m_calendar_selected_range.End Then

      m_calendar_selected_range = calendar.SelectionRange

      Call Me.RefreshCalendarSchedule()

    End If

  End Sub ' calendar_MouseDown

  Private Sub tab_control_Selected(ByVal sender As Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tab_control.Selected
    If e.TabPage.Equals(tab_calendar) Then
      Call Me.RefreshCalendarSchedule()
    End If
  End Sub ' tab_control_Selected

  Private Sub btn_add_recipient_ClickEvent() Handles btn_add_recipient.ClickEvent

    Call Me.AddRecipient()

  End Sub ' btn_add_recipient_ClickEvent

  Private Sub btn_delete_recipients_ClickEvent() Handles btn_delete_recipients.ClickEvent

    Call Me.DeleteRecipients()

  End Sub ' btn_delete_recipients_ClickEvent

  Private Sub chk_enable_shift_duration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_enable_shift_duration.CheckedChanged

    If chk_enable_shift_duration.Checked Then
      ef_work_shift_duration.Enabled = True
    Else
      ef_work_shift_duration.Enabled = False
      ef_work_shift_duration.Value = ""
    End If

  End Sub ' chk_enable_shift_duration_CheckedChanged

  Private Sub ef_max_daily_cash_openings_EntryFieldValueChanged() Handles ef_max_daily_cash_openings.EntryFieldValueChanged

    Select Case ef_max_daily_cash_openings.Value

      Case ""
        Return

      Case "0"
        lbl_info_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5995)

      Case Else
        lbl_info_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2585, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2580)) ' To open additional cash sessions...

    End Select

  End Sub ' ef_max_daily_cash_openings_EntryFieldValueChanged

#End Region ' Events

End Class