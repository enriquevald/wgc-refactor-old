'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_multisite_configuration.vb
' DESCRIPTION:   Multisite configuration
' AUTHOR:        Javier Molina Moral
' CREATION DATE: 26-FEB-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-FEB-2013  JMM    Initial version
' 17-APR-2013  ANG    Add head group box
' 18-APR-2013  JMM    SiteID check added
' 22-APR-2013  ANG    Set MemeberCardsId as ReadOnly, Now it gets from center
' 29-SEP-2014  FJC    Add SAS_FLAG: SPECIAL_PROGRESSIVE_METER  
' 16-JAN-2016  JMM    PBI 19744:Pago automático de handpays
' 16-FEB-2016  FAV    PBI 24711: Machine on reserve.
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_multisite_configuration
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

#End Region ' Constants

#Region " Members "

#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MULTISITE_CONFIG

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1406) '1406 "Site Configuration"

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.lbl_multisite_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) '262 "Status"

    Me.cmb_multisite_status.Add(CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694)) '1694 "Not activated"
    Me.cmb_multisite_status.Add(CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696)) '1696 "Activated"

    Me.gb_multisite_params.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1697) '1697 "Multisite"

    Me.gb_head.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906) '1906  Site
    Me.lbl_site_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407) '1407 "Site ID"
    Me.ef_site_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    Me.lbl_site_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1410) '1410 "Nombre de sala"
    Me.ef_site_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.lbl_members_card_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1408) '1408 "Members Card ID"
    Me.ef_member_card_id.Enabled = False
    Me.ef_member_card_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.lbl_center_address_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1690) '1690 "Center Address 1"
    Me.ef_center_address_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)

    Me.lbl_center_address_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1691) '1691 "Center Address 2"
    Me.ef_center_address_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 500)

    Me.gb_sas.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096)

    'SAS Flags
    Me.uc_sas_flags.Init(GUI_Controls.uc_sas_flags.ENUM_SAS_FLAGS_TYPE.SITE_DEFAULT)

  End Sub 'GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _multisite_config As CLS_MULTISITE_CONFIGURATION
    Dim _multisite_member As Boolean
    Dim _font As Font

    _multisite_config = DbReadObject

    ' Site ID
    Me.ef_site_id.Value = _multisite_config.SiteID
    Me.ef_site_id.IsReadOnly = (_multisite_config.Status = CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_ACTIVATED)

    ' Site Name
    Me.ef_site_name.Value = _multisite_config.SiteName

    _multisite_member = (_multisite_config.Status = CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_ACTIVATED)

    ' MultiSite Member
    If _multisite_member Then
      lbl_multisite_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1839) '1839 "Attached to MultiSite"
      _font = lbl_multisite_status.Font
      lbl_multisite_status.Font = New Font(_font, FontStyle.Bold)
      lbl_multisite_status.Top += 10
    End If

    Me.cmb_multisite_status.Value = _multisite_config.Status
    Me.cmb_multisite_status.Visible = Not _multisite_member

    ' MultiSite Member Card ID
    Me.ef_member_card_id.Value = _multisite_config.MembersCardID

    ' MultiSite Center Address 1
    Me.ef_center_address_1.Value = _multisite_config.CenterAddress1
    Me.ef_center_address_1.Enabled = _multisite_member

    ' MultiSite Center Address 2
    Me.ef_center_address_2.Value = _multisite_config.CenterAddress2
    Me.ef_center_address_2.Enabled = _multisite_member

    If TITO.Utils.IsTitoMode() Then
      Me.uc_sas_flags.IsReadOnly(GUI_Controls.uc_sas_flags.ENUM_SAS_FLAGS_COMBOS.CMD1B_PROMOTIONAL_CREDITS)
    End If
    Me.uc_sas_flags.ExtendedMeters = IIf(_multisite_config.ExtendedMeters, ENUM_SAS_FLAGS.ACTIVATED, ENUM_SAS_FLAGS.NOT_ACTIVATED)

    If _multisite_config.PromotionalCredits = CLS_MULTISITE_CONFIGURATION.ENUM_SAS_PROMOTION_FLAGS.MACHINE_DEFINED Then
      Me.uc_sas_flags.PromotionalCredits = ENUM_SAS_FLAGS.MACHINE_DEFINED
    Else
      Me.uc_sas_flags.PromotionalCredits = ENUM_SAS_FLAGS.SYSTEM_DEFINED
    End If

    Me.uc_sas_flags.Cmd1BHandPays = _multisite_config.Cmd1BHandPays

    Me.uc_sas_flags.ProgressiveMeters = IIf(_multisite_config.ProgressiveMeters, ENUM_SAS_FLAGS.ACTIVATED, ENUM_SAS_FLAGS.NOT_ACTIVATED)
    If Not GeneralParam.GetBoolean("Progressives", "Enabled") Then
      Me.uc_sas_flags.IsReadOnly(GUI_Controls.uc_sas_flags.ENUM_SAS_FLAGS_COMBOS.PROGRESSIVE_METERS)
    End If

    Me.uc_sas_flags.EnableDisableNoteAcceptor = IIf(_multisite_config.EnableDisableNoteAcceptor, ENUM_SAS_FLAGS.ACTIVATED, ENUM_SAS_FLAGS.NOT_ACTIVATED)
    Me.uc_sas_flags.ReserveTerminal = IIf(_multisite_config.ReserveTerminal, ENUM_SAS_FLAGS.ACTIVATED, ENUM_SAS_FLAGS.NOT_ACTIVATED)
    Me.uc_sas_flags.LockEgmOnReserve = IIf(_multisite_config.LockEgmOnReserve, ENUM_SAS_FLAGS.ACTIVATED, ENUM_SAS_FLAGS.NOT_ACTIVATED)

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _multisite_config As CLS_MULTISITE_CONFIGURATION

    _multisite_config = DbEditedObject

    ' Site Configured
    _multisite_config.SiteConfigured = 1

    ' Site ID
    _multisite_config.SiteID = Me.ef_site_id.Value

    ' Site Name
    _multisite_config.SiteName = Me.ef_site_name.Value

    ' MultiStie Status
    _multisite_config.Status = Me.cmb_multisite_status.Value

    ' MultiStie Members Card ID
    _multisite_config.MembersCardID = Me.ef_member_card_id.Value

    ' MultiStie Members Card ID, enable?
    _multisite_config.MembersCardIDEnabled = Me.ef_member_card_id.Enabled

    ' MultiStie Members Center Address 1
    _multisite_config.CenterAddress1 = Me.ef_center_address_1.Value

    ' MultiStie Members Center Address 2
    _multisite_config.CenterAddress2 = Me.ef_center_address_2.Value


    If Me.uc_sas_flags.ExtendedMeters = ENUM_SAS_FLAGS.ACTIVATED Then
      _multisite_config.ExtendedMeters = True
    Else
      _multisite_config.ExtendedMeters = False
    End If

    If Me.uc_sas_flags.PromotionalCredits = ENUM_SAS_FLAGS.MACHINE_DEFINED Then
      _multisite_config.PromotionalCredits = CLS_MULTISITE_CONFIGURATION.ENUM_SAS_PROMOTION_FLAGS.MACHINE_DEFINED
    Else
      _multisite_config.PromotionalCredits = CLS_MULTISITE_CONFIGURATION.ENUM_SAS_PROMOTION_FLAGS.SYSTEM_DEFINED
    End If

    _multisite_config.Cmd1BHandPays = Me.uc_sas_flags.Cmd1BHandPays

    If Me.uc_sas_flags.ProgressiveMeters = ENUM_SAS_FLAGS.ACTIVATED Then
      _multisite_config.ProgressiveMeters = True
    Else
      _multisite_config.ProgressiveMeters = False
    End If

    If Me.uc_sas_flags.EnableDisableNoteAcceptor = ENUM_SAS_FLAGS.ACTIVATED Then
      _multisite_config.EnableDisableNoteAcceptor = True
    Else
      _multisite_config.EnableDisableNoteAcceptor = False
    End If

    If Me.uc_sas_flags.ReserveTerminal = ENUM_SAS_FLAGS.ACTIVATED Then
      _multisite_config.ReserveTerminal = True
    Else
      _multisite_config.ReserveTerminal = False
    End If

    If Me.uc_sas_flags.LockEgmOnReserve = ENUM_SAS_FLAGS.ACTIVATED Then
      _multisite_config.LockEgmOnReserve = True
    Else
      _multisite_config.LockEgmOnReserve = False
    End If

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

        Me.DbEditedObject = New CLS_MULTISITE_CONFIGURATION
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _site_id As Int32
    Dim _read_config As CLS_MULTISITE_CONFIGURATION

    Int32.TryParse(Me.ef_site_id.Value, _site_id)

    If _site_id = 0 Then
      '1190 "%1 field value must be between %2 and %3." 
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      , , _
                      Me.lbl_site_id.Text, _
                      "1", _
                      "999")

      Return False
    End If

    'MultiSite not activated: No changes, no multisite data check.
    If Me.cmb_multisite_status.Value = CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED Then
      Return True
    End If

    If Me.ef_member_card_id.Value = "" Then
      '118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , Me.lbl_members_card_id.Text)
      Call Me.ef_member_card_id.Focus()

      Return False
    End If

    If Me.ef_center_address_1.Value = "" And Me.ef_center_address_2.Value = "" Then
      '118 "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , Me.lbl_center_address_1.Text)
      Call Me.ef_center_address_1.Focus()

      Return False
    End If

    If Me.ef_center_address_1.Value = "" Then
      '1786 "Field %1 is empty.  Continue anyway?"
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1786), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , Me.lbl_center_address_1.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Call Me.ef_center_address_2.Focus()

        Return False
      End If
    End If

    If Me.ef_center_address_2.Value = "" Then
      '1786 "Field %1 is empty.  Continue anyway?"
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1786), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , Me.lbl_center_address_2.Text) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Call Me.ef_center_address_2.Focus()

        Return False
      End If
    End If

    _read_config = Me.DbReadObject  

    If Me.cmb_multisite_status.Value = CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_ACTIVATED _
    And Me.cmb_multisite_status.Value <> _read_config.Status Then
      '1848 "You must restart WigosGUI for changes to take effect."
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1848), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
    End If

    Return True
  End Function 'GUI_IsScreenDataOk()

  ' PURPOSE: Handle Form buttons cliks
  '
  '  PARAMS:
  '     - INPUT: Clicked button object
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        ' Save without close form
        MyBase.CloseOnOkClick = False
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        MyBase.CloseOnOkClick = True

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform any clean-up , release used memory in image cache
  '
  '  PARAMS:
  '     - INPUT:
  '         -  
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '
  Protected Overrides Sub GUI_Exit()
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

#End Region ' Private Functions

#Region " Events "

  Private Sub cmb_multisite_status_ValueChangedEvent() Handles cmb_multisite_status.ValueChangedEvent
    Dim _multisite_config As CLS_MULTISITE_CONFIGURATION
    Dim _parameters_enabled As Boolean

    _multisite_config = DbReadObject

    _parameters_enabled = (Me.cmb_multisite_status.Value = CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_ACTIVATED)

    If _parameters_enabled And _multisite_config.Status = CLS_MULTISITE_CONFIGURATION.ENUM_MULTISITE_STATUS.STATUS_NO_ACTIVATED Then
      Me.ef_member_card_id.Enabled = True
    Else
      Me.ef_member_card_id.Enabled = False
    End If

    Me.ef_center_address_1.Enabled = _parameters_enabled
    Me.ef_center_address_2.Enabled = _parameters_enabled
  End Sub

#End Region ' Events

End Class