<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_winup_settings
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub


  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_winup_settings))
    Me.tab_player_club = New System.Windows.Forms.TabPage()
    Me.gb_PreferentText = New System.Windows.Forms.GroupBox()
    Me.txt_link = New System.Windows.Forms.TextBox()
    Me.lbl_terms = New System.Windows.Forms.Label()
    Me.txt_terms = New System.Windows.Forms.TextBox()
    Me.lbl_link = New System.Windows.Forms.Label()
    Me.lbl_join = New System.Windows.Forms.Label()
    Me.lbl_about = New System.Windows.Forms.Label()
    Me.txt_join = New System.Windows.Forms.TextBox()
    Me.txt_about = New System.Windows.Forms.TextBox()
    Me.tab_control = New System.Windows.Forms.TabControl()
    Me.tab_footer = New System.Windows.Forms.TabPage()
    Me.gb_size = New System.Windows.Forms.GroupBox()
    Me.lbl_weight = New System.Windows.Forms.Label()
    Me.ef_height = New GUI_Controls.uc_entry_field()
    Me.ef_weight = New GUI_Controls.uc_entry_field()
    Me.lbl_height = New System.Windows.Forms.Label()
    Me.cmb_weight_unit = New GUI_Controls.uc_combo()
    Me.lbl_width = New System.Windows.Forms.Label()
    Me.ef_width = New GUI_Controls.uc_entry_field()
    Me.TabConfigFooter = New System.Windows.Forms.TabControl()
    Me.tab_menu_footer1 = New System.Windows.Forms.TabPage()
    Me.cmb_Section_footer1 = New GUI_Controls.uc_combo()
    Me.Uc_imageList_footer1 = New GUI_Controls.uc_image()
    Me.chk_visible_footer1 = New System.Windows.Forms.CheckBox()
    Me.ef_order_footer1 = New GUI_Controls.uc_entry_field()
    Me.lbl_section_footer1 = New System.Windows.Forms.Label()
    Me.tab_menu_footer2 = New System.Windows.Forms.TabPage()
    Me.cmb_section_footer2 = New GUI_Controls.uc_combo()
    Me.Uc_imageList_footer2 = New GUI_Controls.uc_image()
    Me.chk_visible_footer2 = New System.Windows.Forms.CheckBox()
    Me.ef_order_footer2 = New GUI_Controls.uc_entry_field()
    Me.lbl_section_footer2 = New System.Windows.Forms.Label()
    Me.tab_menu_footer3 = New System.Windows.Forms.TabPage()
    Me.cmb_section_footer3 = New GUI_Controls.uc_combo()
    Me.Uc_imageList_footer3 = New GUI_Controls.uc_image()
    Me.chk_visible_footer3 = New System.Windows.Forms.CheckBox()
    Me.ef_order_footer3 = New GUI_Controls.uc_entry_field()
    Me.lbl_section_footer3 = New System.Windows.Forms.Label()
    Me.tab_menu_footer4 = New System.Windows.Forms.TabPage()
    Me.cmb_section_footer4 = New GUI_Controls.uc_combo()
    Me.Uc_imageList_footer4 = New GUI_Controls.uc_image()
    Me.chk_visible_footer4 = New System.Windows.Forms.CheckBox()
    Me.ef_order_footer4 = New GUI_Controls.uc_entry_field()
    Me.lbl_section_footer4 = New System.Windows.Forms.Label()
    Me.tab_menu_footer5 = New System.Windows.Forms.TabPage()
    Me.cmb_section_footer5 = New GUI_Controls.uc_combo()
    Me.Uc_imageList_footer5 = New GUI_Controls.uc_image()
    Me.chk_visible_footer5 = New System.Windows.Forms.CheckBox()
    Me.ef_order_footer5 = New GUI_Controls.uc_entry_field()
    Me.lbl_section_footer5 = New System.Windows.Forms.Label()
    Me.tab_mailing = New System.Windows.Forms.TabPage()
    Me.gbCredentials = New System.Windows.Forms.GroupBox()
    Me.lbl_mailing_emaiAddress = New System.Windows.Forms.Label()
    Me.lbl_mailing_domain = New System.Windows.Forms.Label()
    Me.lbl_mailing_password = New System.Windows.Forms.Label()
    Me.lbl_mailing_username = New System.Windows.Forms.Label()
    Me.Uc_mailing_password = New GUI_Controls.uc_entry_field()
    Me.Uc_mailing_domain = New GUI_Controls.uc_entry_field()
    Me.Uc_mailing_username = New GUI_Controls.uc_entry_field()
    Me.Uc_mailing_emaiAddress = New GUI_Controls.uc_entry_field()
    Me.gbSMTPConfig = New System.Windows.Forms.GroupBox()
    Me.lbl_smtp_secured = New System.Windows.Forms.Label()
    Me.lbl_mailing_serverPort = New System.Windows.Forms.Label()
    Me.lbl_mailing_serverAdress = New System.Windows.Forms.Label()
    Me.cmb_smtp_secured = New GUI_Controls.uc_combo()
    Me.Uc_mailing_serverPort = New GUI_Controls.uc_entry_field()
    Me.Uc_mailing_serverAdress = New GUI_Controls.uc_entry_field()
    Me.chk_mailing_enabled = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.tab_player_club.SuspendLayout()
    Me.gb_PreferentText.SuspendLayout()
    Me.tab_control.SuspendLayout()
    Me.tab_footer.SuspendLayout()
    Me.gb_size.SuspendLayout()
    Me.TabConfigFooter.SuspendLayout()
    Me.tab_menu_footer1.SuspendLayout()
    Me.tab_menu_footer2.SuspendLayout()
    Me.tab_menu_footer3.SuspendLayout()
    Me.tab_menu_footer4.SuspendLayout()
    Me.tab_menu_footer5.SuspendLayout()
    Me.tab_mailing.SuspendLayout()
    Me.gbCredentials.SuspendLayout()
    Me.gbSMTPConfig.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.tab_control)
    Me.panel_data.Location = New System.Drawing.Point(5, 5)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(4)
    Me.panel_data.Size = New System.Drawing.Size(961, 608)
    '
    'tab_player_club
    '
    Me.tab_player_club.Controls.Add(Me.gb_PreferentText)
    Me.tab_player_club.Location = New System.Drawing.Point(4, 22)
    Me.tab_player_club.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_player_club.Name = "tab_player_club"
    Me.tab_player_club.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_player_club.Size = New System.Drawing.Size(949, 578)
    Me.tab_player_club.TabIndex = 0
    Me.tab_player_club.Text = "xTabPlayerClub"
    Me.tab_player_club.UseVisualStyleBackColor = True
    '
    'gb_PreferentText
    '
    Me.gb_PreferentText.BackColor = System.Drawing.Color.Transparent
    Me.gb_PreferentText.Controls.Add(Me.txt_link)
    Me.gb_PreferentText.Controls.Add(Me.lbl_terms)
    Me.gb_PreferentText.Controls.Add(Me.txt_terms)
    Me.gb_PreferentText.Controls.Add(Me.lbl_link)
    Me.gb_PreferentText.Controls.Add(Me.lbl_join)
    Me.gb_PreferentText.Controls.Add(Me.lbl_about)
    Me.gb_PreferentText.Controls.Add(Me.txt_join)
    Me.gb_PreferentText.Controls.Add(Me.txt_about)
    Me.gb_PreferentText.Location = New System.Drawing.Point(4, 0)
    Me.gb_PreferentText.Margin = New System.Windows.Forms.Padding(4)
    Me.gb_PreferentText.Name = "gb_PreferentText"
    Me.gb_PreferentText.Padding = New System.Windows.Forms.Padding(4)
    Me.gb_PreferentText.Size = New System.Drawing.Size(941, 568)
    Me.gb_PreferentText.TabIndex = 0
    Me.gb_PreferentText.TabStop = False
    Me.gb_PreferentText.Text = "xGb_PreferentText"
    '
    'txt_link
    '
    Me.txt_link.Location = New System.Drawing.Point(8, 323)
    Me.txt_link.Margin = New System.Windows.Forms.Padding(4)
    Me.txt_link.Multiline = True
    Me.txt_link.Name = "txt_link"
    Me.txt_link.Size = New System.Drawing.Size(925, 231)
    Me.txt_link.TabIndex = 0
    '
    'lbl_terms
    '
    Me.lbl_terms.AutoSize = True
    Me.lbl_terms.Location = New System.Drawing.Point(6, 432)
    Me.lbl_terms.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_terms.Name = "lbl_terms"
    Me.lbl_terms.Size = New System.Drawing.Size(40, 13)
    Me.lbl_terms.TabIndex = 1
    Me.lbl_terms.Text = "terms"
    Me.lbl_terms.Visible = False
    '
    'txt_terms
    '
    Me.txt_terms.Location = New System.Drawing.Point(8, 457)
    Me.txt_terms.Margin = New System.Windows.Forms.Padding(4)
    Me.txt_terms.Multiline = True
    Me.txt_terms.Name = "txt_terms"
    Me.txt_terms.Size = New System.Drawing.Size(925, 97)
    Me.txt_terms.TabIndex = 0
    Me.txt_terms.Visible = False
    '
    'lbl_link
    '
    Me.lbl_link.AutoSize = True
    Me.lbl_link.Location = New System.Drawing.Point(8, 298)
    Me.lbl_link.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_link.Name = "lbl_link"
    Me.lbl_link.Size = New System.Drawing.Size(27, 13)
    Me.lbl_link.TabIndex = 1
    Me.lbl_link.Text = "link"
    '
    'lbl_join
    '
    Me.lbl_join.AutoSize = True
    Me.lbl_join.Location = New System.Drawing.Point(8, 164)
    Me.lbl_join.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_join.Name = "lbl_join"
    Me.lbl_join.Size = New System.Drawing.Size(28, 13)
    Me.lbl_join.TabIndex = 1
    Me.lbl_join.Text = "join"
    Me.lbl_join.Visible = False
    '
    'lbl_about
    '
    Me.lbl_about.AutoSize = True
    Me.lbl_about.Location = New System.Drawing.Point(8, 30)
    Me.lbl_about.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_about.Name = "lbl_about"
    Me.lbl_about.Size = New System.Drawing.Size(39, 13)
    Me.lbl_about.TabIndex = 1
    Me.lbl_about.Text = "about"
    '
    'txt_join
    '
    Me.txt_join.Location = New System.Drawing.Point(8, 189)
    Me.txt_join.Margin = New System.Windows.Forms.Padding(4)
    Me.txt_join.Multiline = True
    Me.txt_join.Name = "txt_join"
    Me.txt_join.Size = New System.Drawing.Size(925, 97)
    Me.txt_join.TabIndex = 0
    Me.txt_join.Visible = False
    '
    'txt_about
    '
    Me.txt_about.Location = New System.Drawing.Point(8, 55)
    Me.txt_about.Margin = New System.Windows.Forms.Padding(4)
    Me.txt_about.Multiline = True
    Me.txt_about.Name = "txt_about"
    Me.txt_about.Size = New System.Drawing.Size(925, 231)
    Me.txt_about.TabIndex = 0
    '
    'tab_control
    '
    Me.tab_control.Controls.Add(Me.tab_player_club)
    Me.tab_control.Controls.Add(Me.tab_footer)
    Me.tab_control.Controls.Add(Me.tab_mailing)
    Me.tab_control.Cursor = System.Windows.Forms.Cursors.Default
    Me.tab_control.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.tab_control.Location = New System.Drawing.Point(0, 0)
    Me.tab_control.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_control.Multiline = True
    Me.tab_control.Name = "tab_control"
    Me.tab_control.SelectedIndex = 0
    Me.tab_control.Size = New System.Drawing.Size(957, 604)
    Me.tab_control.TabIndex = 7
    '
    'tab_footer
    '
    Me.tab_footer.Controls.Add(Me.gb_size)
    Me.tab_footer.Controls.Add(Me.TabConfigFooter)
    Me.tab_footer.Location = New System.Drawing.Point(4, 22)
    Me.tab_footer.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_footer.Name = "tab_footer"
    Me.tab_footer.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_footer.Size = New System.Drawing.Size(949, 578)
    Me.tab_footer.TabIndex = 3
    Me.tab_footer.Text = "xTabFooter"
    Me.tab_footer.UseVisualStyleBackColor = True
    '
    'gb_size
    '
    Me.gb_size.BackColor = System.Drawing.Color.Transparent
    Me.gb_size.Controls.Add(Me.lbl_weight)
    Me.gb_size.Controls.Add(Me.ef_height)
    Me.gb_size.Controls.Add(Me.ef_weight)
    Me.gb_size.Controls.Add(Me.lbl_height)
    Me.gb_size.Controls.Add(Me.cmb_weight_unit)
    Me.gb_size.Controls.Add(Me.lbl_width)
    Me.gb_size.Controls.Add(Me.ef_width)
    Me.gb_size.Location = New System.Drawing.Point(607, 8)
    Me.gb_size.Margin = New System.Windows.Forms.Padding(4)
    Me.gb_size.Name = "gb_size"
    Me.gb_size.Padding = New System.Windows.Forms.Padding(4)
    Me.gb_size.Size = New System.Drawing.Size(338, 562)
    Me.gb_size.TabIndex = 33
    Me.gb_size.TabStop = False
    Me.gb_size.Text = "xProperties (maximum values)"
    '
    'lbl_weight
    '
    Me.lbl_weight.AutoSize = True
    Me.lbl_weight.Location = New System.Drawing.Point(9, 104)
    Me.lbl_weight.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_weight.Name = "lbl_weight"
    Me.lbl_weight.Size = New System.Drawing.Size(45, 13)
    Me.lbl_weight.TabIndex = 33
    Me.lbl_weight.Text = "Weight"
    '
    'ef_height
    '
    Me.ef_height.DoubleValue = 0.0R
    Me.ef_height.IntegerValue = 0
    Me.ef_height.IsReadOnly = False
    Me.ef_height.Location = New System.Drawing.Point(70, 25)
    Me.ef_height.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_height.Name = "ef_height"
    Me.ef_height.PlaceHolder = Nothing
    Me.ef_height.Size = New System.Drawing.Size(172, 24)
    Me.ef_height.SufixText = "px"
    Me.ef_height.SufixTextVisible = True
    Me.ef_height.SufixTextWidth = 51
    Me.ef_height.TabIndex = 10
    Me.ef_height.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_height.TextValue = ""
    Me.ef_height.TextVisible = False
    Me.ef_height.TextWidth = 0
    Me.ef_height.Value = ""
    Me.ef_height.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_weight
    '
    Me.ef_weight.DoubleValue = 0.0R
    Me.ef_weight.IntegerValue = 0
    Me.ef_weight.IsReadOnly = False
    Me.ef_weight.Location = New System.Drawing.Point(70, 97)
    Me.ef_weight.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_weight.Name = "ef_weight"
    Me.ef_weight.PlaceHolder = Nothing
    Me.ef_weight.Size = New System.Drawing.Size(121, 24)
    Me.ef_weight.SufixText = "Sufix Text"
    Me.ef_weight.SufixTextVisible = True
    Me.ef_weight.TabIndex = 32
    Me.ef_weight.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_weight.TextValue = ""
    Me.ef_weight.TextVisible = False
    Me.ef_weight.TextWidth = 0
    Me.ef_weight.Value = ""
    Me.ef_weight.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_height
    '
    Me.lbl_height.AutoSize = True
    Me.lbl_height.Location = New System.Drawing.Point(9, 31)
    Me.lbl_height.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_height.Name = "lbl_height"
    Me.lbl_height.Size = New System.Drawing.Size(43, 13)
    Me.lbl_height.TabIndex = 9
    Me.lbl_height.Text = "Height"
    '
    'cmb_weight_unit
    '
    Me.cmb_weight_unit.AllowUnlistedValues = False
    Me.cmb_weight_unit.AutoCompleteMode = False
    Me.cmb_weight_unit.IsReadOnly = False
    Me.cmb_weight_unit.Location = New System.Drawing.Point(194, 97)
    Me.cmb_weight_unit.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_weight_unit.Name = "cmb_weight_unit"
    Me.cmb_weight_unit.SelectedIndex = -1
    Me.cmb_weight_unit.Size = New System.Drawing.Size(77, 24)
    Me.cmb_weight_unit.SufixText = "Sufix Text"
    Me.cmb_weight_unit.SufixTextVisible = True
    Me.cmb_weight_unit.TabIndex = 31
    Me.cmb_weight_unit.TextCombo = Nothing
    Me.cmb_weight_unit.TextVisible = False
    Me.cmb_weight_unit.TextWidth = 0
    '
    'lbl_width
    '
    Me.lbl_width.AutoSize = True
    Me.lbl_width.Location = New System.Drawing.Point(9, 67)
    Me.lbl_width.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_width.Name = "lbl_width"
    Me.lbl_width.Size = New System.Drawing.Size(39, 13)
    Me.lbl_width.TabIndex = 11
    Me.lbl_width.Text = "Width"
    '
    'ef_width
    '
    Me.ef_width.DoubleValue = 0.0R
    Me.ef_width.IntegerValue = 0
    Me.ef_width.IsReadOnly = False
    Me.ef_width.Location = New System.Drawing.Point(70, 61)
    Me.ef_width.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_width.Name = "ef_width"
    Me.ef_width.PlaceHolder = Nothing
    Me.ef_width.Size = New System.Drawing.Size(172, 24)
    Me.ef_width.SufixText = "px"
    Me.ef_width.SufixTextVisible = True
    Me.ef_width.SufixTextWidth = 51
    Me.ef_width.TabIndex = 12
    Me.ef_width.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_width.TextValue = ""
    Me.ef_width.TextVisible = False
    Me.ef_width.TextWidth = 0
    Me.ef_width.Value = ""
    Me.ef_width.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabConfigFooter
    '
    Me.TabConfigFooter.Controls.Add(Me.tab_menu_footer1)
    Me.TabConfigFooter.Controls.Add(Me.tab_menu_footer2)
    Me.TabConfigFooter.Controls.Add(Me.tab_menu_footer3)
    Me.TabConfigFooter.Controls.Add(Me.tab_menu_footer4)
    Me.TabConfigFooter.Controls.Add(Me.tab_menu_footer5)
    Me.TabConfigFooter.Cursor = System.Windows.Forms.Cursors.Default
    Me.TabConfigFooter.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.TabConfigFooter.Location = New System.Drawing.Point(1, 8)
    Me.TabConfigFooter.Margin = New System.Windows.Forms.Padding(4)
    Me.TabConfigFooter.Multiline = True
    Me.TabConfigFooter.Name = "TabConfigFooter"
    Me.TabConfigFooter.SelectedIndex = 0
    Me.TabConfigFooter.Size = New System.Drawing.Size(591, 562)
    Me.TabConfigFooter.TabIndex = 8
    '
    'tab_menu_footer1
    '
    Me.tab_menu_footer1.Controls.Add(Me.cmb_Section_footer1)
    Me.tab_menu_footer1.Controls.Add(Me.Uc_imageList_footer1)
    Me.tab_menu_footer1.Controls.Add(Me.chk_visible_footer1)
    Me.tab_menu_footer1.Controls.Add(Me.ef_order_footer1)
    Me.tab_menu_footer1.Controls.Add(Me.lbl_section_footer1)
    Me.tab_menu_footer1.Location = New System.Drawing.Point(4, 22)
    Me.tab_menu_footer1.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer1.Name = "tab_menu_footer1"
    Me.tab_menu_footer1.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer1.Size = New System.Drawing.Size(583, 536)
    Me.tab_menu_footer1.TabIndex = 0
    Me.tab_menu_footer1.Text = "xTabFoot1"
    Me.tab_menu_footer1.UseVisualStyleBackColor = True
    '
    'cmb_Section_footer1
    '
    Me.cmb_Section_footer1.AllowUnlistedValues = False
    Me.cmb_Section_footer1.AutoCompleteMode = False
    Me.cmb_Section_footer1.IsReadOnly = False
    Me.cmb_Section_footer1.Location = New System.Drawing.Point(69, 36)
    Me.cmb_Section_footer1.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_Section_footer1.Name = "cmb_Section_footer1"
    Me.cmb_Section_footer1.SelectedIndex = -1
    Me.cmb_Section_footer1.Size = New System.Drawing.Size(237, 24)
    Me.cmb_Section_footer1.SufixText = "Sufix Text"
    Me.cmb_Section_footer1.SufixTextVisible = True
    Me.cmb_Section_footer1.TabIndex = 27
    Me.cmb_Section_footer1.TextCombo = Nothing
    Me.cmb_Section_footer1.TextVisible = False
    Me.cmb_Section_footer1.TextWidth = 0
    '
    'Uc_imageList_footer1
    '
    Me.Uc_imageList_footer1.AutoSize = True
    Me.Uc_imageList_footer1.ButtonDeleteEnabled = True
    Me.Uc_imageList_footer1.FreeResize = False
    Me.Uc_imageList_footer1.Image = Nothing
    Me.Uc_imageList_footer1.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList_footer1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList_footer1.ImageName = Nothing
    Me.Uc_imageList_footer1.Location = New System.Drawing.Point(70, 95)
    Me.Uc_imageList_footer1.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList_footer1.Name = "Uc_imageList_footer1"
    Me.Uc_imageList_footer1.Size = New System.Drawing.Size(468, 371)
    Me.Uc_imageList_footer1.TabIndex = 26
    Me.Uc_imageList_footer1.Transparent = False
    '
    'chk_visible_footer1
    '
    Me.chk_visible_footer1.AutoSize = True
    Me.chk_visible_footer1.Location = New System.Drawing.Point(8, 8)
    Me.chk_visible_footer1.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_visible_footer1.Name = "chk_visible_footer1"
    Me.chk_visible_footer1.Size = New System.Drawing.Size(63, 17)
    Me.chk_visible_footer1.TabIndex = 25
    Me.chk_visible_footer1.Text = "Visible"
    Me.chk_visible_footer1.UseVisualStyleBackColor = True
    '
    'ef_order_footer1
    '
    Me.ef_order_footer1.DoubleValue = 0.0R
    Me.ef_order_footer1.IntegerValue = 0
    Me.ef_order_footer1.IsReadOnly = False
    Me.ef_order_footer1.Location = New System.Drawing.Point(315, 36)
    Me.ef_order_footer1.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_order_footer1.Name = "ef_order_footer1"
    Me.ef_order_footer1.PlaceHolder = Nothing
    Me.ef_order_footer1.Size = New System.Drawing.Size(158, 24)
    Me.ef_order_footer1.SufixText = "Sufix Text"
    Me.ef_order_footer1.SufixTextVisible = True
    Me.ef_order_footer1.TabIndex = 18
    Me.ef_order_footer1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_footer1.TextValue = ""
    Me.ef_order_footer1.TextVisible = False
    Me.ef_order_footer1.TextWidth = 51
    Me.ef_order_footer1.Value = ""
    Me.ef_order_footer1.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_section_footer1
    '
    Me.lbl_section_footer1.Location = New System.Drawing.Point(4, 35)
    Me.lbl_section_footer1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_section_footer1.Name = "lbl_section_footer1"
    Me.lbl_section_footer1.Size = New System.Drawing.Size(67, 27)
    Me.lbl_section_footer1.TabIndex = 17
    Me.lbl_section_footer1.Text = "xOptimumSize"
    Me.lbl_section_footer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_menu_footer2
    '
    Me.tab_menu_footer2.Controls.Add(Me.cmb_section_footer2)
    Me.tab_menu_footer2.Controls.Add(Me.Uc_imageList_footer2)
    Me.tab_menu_footer2.Controls.Add(Me.chk_visible_footer2)
    Me.tab_menu_footer2.Controls.Add(Me.ef_order_footer2)
    Me.tab_menu_footer2.Controls.Add(Me.lbl_section_footer2)
    Me.tab_menu_footer2.Location = New System.Drawing.Point(4, 22)
    Me.tab_menu_footer2.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer2.Name = "tab_menu_footer2"
    Me.tab_menu_footer2.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer2.Size = New System.Drawing.Size(583, 536)
    Me.tab_menu_footer2.TabIndex = 1
    Me.tab_menu_footer2.Text = "xTabFoot2"
    Me.tab_menu_footer2.UseVisualStyleBackColor = True
    '
    'cmb_section_footer2
    '
    Me.cmb_section_footer2.AllowUnlistedValues = False
    Me.cmb_section_footer2.AutoCompleteMode = False
    Me.cmb_section_footer2.IsReadOnly = False
    Me.cmb_section_footer2.Location = New System.Drawing.Point(69, 36)
    Me.cmb_section_footer2.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_section_footer2.Name = "cmb_section_footer2"
    Me.cmb_section_footer2.SelectedIndex = -1
    Me.cmb_section_footer2.Size = New System.Drawing.Size(237, 24)
    Me.cmb_section_footer2.SufixText = "Sufix Text"
    Me.cmb_section_footer2.TabIndex = 32
    Me.cmb_section_footer2.TextCombo = Nothing
    Me.cmb_section_footer2.TextVisible = False
    Me.cmb_section_footer2.TextWidth = 0
    '
    'Uc_imageList_footer2
    '
    Me.Uc_imageList_footer2.AutoSize = True
    Me.Uc_imageList_footer2.ButtonDeleteEnabled = True
    Me.Uc_imageList_footer2.FreeResize = False
    Me.Uc_imageList_footer2.Image = Nothing
    Me.Uc_imageList_footer2.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList_footer2.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList_footer2.ImageName = Nothing
    Me.Uc_imageList_footer2.Location = New System.Drawing.Point(70, 95)
    Me.Uc_imageList_footer2.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList_footer2.Name = "Uc_imageList_footer2"
    Me.Uc_imageList_footer2.Size = New System.Drawing.Size(468, 371)
    Me.Uc_imageList_footer2.TabIndex = 31
    Me.Uc_imageList_footer2.Transparent = False
    '
    'chk_visible_footer2
    '
    Me.chk_visible_footer2.AutoSize = True
    Me.chk_visible_footer2.Location = New System.Drawing.Point(8, 8)
    Me.chk_visible_footer2.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_visible_footer2.Name = "chk_visible_footer2"
    Me.chk_visible_footer2.Size = New System.Drawing.Size(63, 17)
    Me.chk_visible_footer2.TabIndex = 30
    Me.chk_visible_footer2.Text = "Visible"
    Me.chk_visible_footer2.UseVisualStyleBackColor = True
    '
    'ef_order_footer2
    '
    Me.ef_order_footer2.DoubleValue = 0.0R
    Me.ef_order_footer2.IntegerValue = 0
    Me.ef_order_footer2.IsReadOnly = False
    Me.ef_order_footer2.Location = New System.Drawing.Point(315, 36)
    Me.ef_order_footer2.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_order_footer2.Name = "ef_order_footer2"
    Me.ef_order_footer2.PlaceHolder = Nothing
    Me.ef_order_footer2.Size = New System.Drawing.Size(158, 25)
    Me.ef_order_footer2.SufixText = "Sufix Text"
    Me.ef_order_footer2.TabIndex = 29
    Me.ef_order_footer2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_footer2.TextValue = ""
    Me.ef_order_footer2.TextVisible = False
    Me.ef_order_footer2.TextWidth = 51
    Me.ef_order_footer2.Value = ""
    Me.ef_order_footer2.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_section_footer2
    '
    Me.lbl_section_footer2.Location = New System.Drawing.Point(4, 35)
    Me.lbl_section_footer2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_section_footer2.Name = "lbl_section_footer2"
    Me.lbl_section_footer2.Size = New System.Drawing.Size(67, 27)
    Me.lbl_section_footer2.TabIndex = 28
    Me.lbl_section_footer2.Text = "xOptimumSize"
    Me.lbl_section_footer2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_menu_footer3
    '
    Me.tab_menu_footer3.Controls.Add(Me.cmb_section_footer3)
    Me.tab_menu_footer3.Controls.Add(Me.Uc_imageList_footer3)
    Me.tab_menu_footer3.Controls.Add(Me.chk_visible_footer3)
    Me.tab_menu_footer3.Controls.Add(Me.ef_order_footer3)
    Me.tab_menu_footer3.Controls.Add(Me.lbl_section_footer3)
    Me.tab_menu_footer3.Location = New System.Drawing.Point(4, 22)
    Me.tab_menu_footer3.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer3.Name = "tab_menu_footer3"
    Me.tab_menu_footer3.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer3.Size = New System.Drawing.Size(583, 536)
    Me.tab_menu_footer3.TabIndex = 2
    Me.tab_menu_footer3.Text = "xTabFoot3"
    Me.tab_menu_footer3.UseVisualStyleBackColor = True
    '
    'cmb_section_footer3
    '
    Me.cmb_section_footer3.AllowUnlistedValues = False
    Me.cmb_section_footer3.AutoCompleteMode = False
    Me.cmb_section_footer3.IsReadOnly = False
    Me.cmb_section_footer3.Location = New System.Drawing.Point(69, 36)
    Me.cmb_section_footer3.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_section_footer3.Name = "cmb_section_footer3"
    Me.cmb_section_footer3.SelectedIndex = -1
    Me.cmb_section_footer3.Size = New System.Drawing.Size(237, 24)
    Me.cmb_section_footer3.SufixText = "Sufix Text"
    Me.cmb_section_footer3.TabIndex = 33
    Me.cmb_section_footer3.TextCombo = Nothing
    Me.cmb_section_footer3.TextVisible = False
    Me.cmb_section_footer3.TextWidth = 0
    '
    'Uc_imageList_footer3
    '
    Me.Uc_imageList_footer3.AutoSize = True
    Me.Uc_imageList_footer3.ButtonDeleteEnabled = True
    Me.Uc_imageList_footer3.FreeResize = False
    Me.Uc_imageList_footer3.Image = Nothing
    Me.Uc_imageList_footer3.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList_footer3.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList_footer3.ImageName = Nothing
    Me.Uc_imageList_footer3.Location = New System.Drawing.Point(70, 95)
    Me.Uc_imageList_footer3.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList_footer3.Name = "Uc_imageList_footer3"
    Me.Uc_imageList_footer3.Size = New System.Drawing.Size(468, 371)
    Me.Uc_imageList_footer3.TabIndex = 31
    Me.Uc_imageList_footer3.Transparent = False
    '
    'chk_visible_footer3
    '
    Me.chk_visible_footer3.AutoSize = True
    Me.chk_visible_footer3.Location = New System.Drawing.Point(8, 8)
    Me.chk_visible_footer3.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_visible_footer3.Name = "chk_visible_footer3"
    Me.chk_visible_footer3.Size = New System.Drawing.Size(63, 17)
    Me.chk_visible_footer3.TabIndex = 30
    Me.chk_visible_footer3.Text = "Visible"
    Me.chk_visible_footer3.UseVisualStyleBackColor = True
    '
    'ef_order_footer3
    '
    Me.ef_order_footer3.DoubleValue = 0.0R
    Me.ef_order_footer3.IntegerValue = 0
    Me.ef_order_footer3.IsReadOnly = False
    Me.ef_order_footer3.Location = New System.Drawing.Point(315, 36)
    Me.ef_order_footer3.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_order_footer3.Name = "ef_order_footer3"
    Me.ef_order_footer3.PlaceHolder = Nothing
    Me.ef_order_footer3.Size = New System.Drawing.Size(158, 25)
    Me.ef_order_footer3.SufixText = "Sufix Text"
    Me.ef_order_footer3.TabIndex = 29
    Me.ef_order_footer3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_footer3.TextValue = ""
    Me.ef_order_footer3.TextVisible = False
    Me.ef_order_footer3.TextWidth = 51
    Me.ef_order_footer3.Value = ""
    Me.ef_order_footer3.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_section_footer3
    '
    Me.lbl_section_footer3.Location = New System.Drawing.Point(4, 35)
    Me.lbl_section_footer3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_section_footer3.Name = "lbl_section_footer3"
    Me.lbl_section_footer3.Size = New System.Drawing.Size(67, 27)
    Me.lbl_section_footer3.TabIndex = 28
    Me.lbl_section_footer3.Text = "xOptimumSize"
    Me.lbl_section_footer3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_menu_footer4
    '
    Me.tab_menu_footer4.Controls.Add(Me.cmb_section_footer4)
    Me.tab_menu_footer4.Controls.Add(Me.Uc_imageList_footer4)
    Me.tab_menu_footer4.Controls.Add(Me.chk_visible_footer4)
    Me.tab_menu_footer4.Controls.Add(Me.ef_order_footer4)
    Me.tab_menu_footer4.Controls.Add(Me.lbl_section_footer4)
    Me.tab_menu_footer4.Location = New System.Drawing.Point(4, 22)
    Me.tab_menu_footer4.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer4.Name = "tab_menu_footer4"
    Me.tab_menu_footer4.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer4.Size = New System.Drawing.Size(583, 536)
    Me.tab_menu_footer4.TabIndex = 3
    Me.tab_menu_footer4.Text = "xTabFoot4"
    Me.tab_menu_footer4.UseVisualStyleBackColor = True
    '
    'cmb_section_footer4
    '
    Me.cmb_section_footer4.AllowUnlistedValues = False
    Me.cmb_section_footer4.AutoCompleteMode = False
    Me.cmb_section_footer4.IsReadOnly = False
    Me.cmb_section_footer4.Location = New System.Drawing.Point(69, 36)
    Me.cmb_section_footer4.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_section_footer4.Name = "cmb_section_footer4"
    Me.cmb_section_footer4.SelectedIndex = -1
    Me.cmb_section_footer4.Size = New System.Drawing.Size(237, 24)
    Me.cmb_section_footer4.SufixText = "Sufix Text"
    Me.cmb_section_footer4.TabIndex = 33
    Me.cmb_section_footer4.TextCombo = Nothing
    Me.cmb_section_footer4.TextVisible = False
    Me.cmb_section_footer4.TextWidth = 0
    '
    'Uc_imageList_footer4
    '
    Me.Uc_imageList_footer4.AutoSize = True
    Me.Uc_imageList_footer4.ButtonDeleteEnabled = True
    Me.Uc_imageList_footer4.FreeResize = False
    Me.Uc_imageList_footer4.Image = Nothing
    Me.Uc_imageList_footer4.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList_footer4.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList_footer4.ImageName = Nothing
    Me.Uc_imageList_footer4.Location = New System.Drawing.Point(70, 95)
    Me.Uc_imageList_footer4.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList_footer4.Name = "Uc_imageList_footer4"
    Me.Uc_imageList_footer4.Size = New System.Drawing.Size(468, 371)
    Me.Uc_imageList_footer4.TabIndex = 31
    Me.Uc_imageList_footer4.Transparent = False
    '
    'chk_visible_footer4
    '
    Me.chk_visible_footer4.AutoSize = True
    Me.chk_visible_footer4.Location = New System.Drawing.Point(8, 8)
    Me.chk_visible_footer4.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_visible_footer4.Name = "chk_visible_footer4"
    Me.chk_visible_footer4.Size = New System.Drawing.Size(63, 17)
    Me.chk_visible_footer4.TabIndex = 30
    Me.chk_visible_footer4.Text = "Visible"
    Me.chk_visible_footer4.UseVisualStyleBackColor = True
    '
    'ef_order_footer4
    '
    Me.ef_order_footer4.DoubleValue = 0.0R
    Me.ef_order_footer4.IntegerValue = 0
    Me.ef_order_footer4.IsReadOnly = False
    Me.ef_order_footer4.Location = New System.Drawing.Point(315, 36)
    Me.ef_order_footer4.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_order_footer4.Name = "ef_order_footer4"
    Me.ef_order_footer4.PlaceHolder = Nothing
    Me.ef_order_footer4.Size = New System.Drawing.Size(158, 24)
    Me.ef_order_footer4.SufixText = "Sufix Text"
    Me.ef_order_footer4.TabIndex = 29
    Me.ef_order_footer4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_footer4.TextValue = ""
    Me.ef_order_footer4.TextVisible = False
    Me.ef_order_footer4.TextWidth = 51
    Me.ef_order_footer4.Value = ""
    Me.ef_order_footer4.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_section_footer4
    '
    Me.lbl_section_footer4.Location = New System.Drawing.Point(4, 35)
    Me.lbl_section_footer4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_section_footer4.Name = "lbl_section_footer4"
    Me.lbl_section_footer4.Size = New System.Drawing.Size(67, 27)
    Me.lbl_section_footer4.TabIndex = 28
    Me.lbl_section_footer4.Text = "xOptimumSize"
    Me.lbl_section_footer4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_menu_footer5
    '
    Me.tab_menu_footer5.Controls.Add(Me.cmb_section_footer5)
    Me.tab_menu_footer5.Controls.Add(Me.Uc_imageList_footer5)
    Me.tab_menu_footer5.Controls.Add(Me.chk_visible_footer5)
    Me.tab_menu_footer5.Controls.Add(Me.ef_order_footer5)
    Me.tab_menu_footer5.Controls.Add(Me.lbl_section_footer5)
    Me.tab_menu_footer5.Location = New System.Drawing.Point(4, 22)
    Me.tab_menu_footer5.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_menu_footer5.Name = "tab_menu_footer5"
    Me.tab_menu_footer5.Size = New System.Drawing.Size(583, 536)
    Me.tab_menu_footer5.TabIndex = 4
    Me.tab_menu_footer5.Text = "xTabFoot5"
    Me.tab_menu_footer5.UseVisualStyleBackColor = True
    '
    'cmb_section_footer5
    '
    Me.cmb_section_footer5.AllowUnlistedValues = False
    Me.cmb_section_footer5.AutoCompleteMode = False
    Me.cmb_section_footer5.IsReadOnly = False
    Me.cmb_section_footer5.Location = New System.Drawing.Point(69, 36)
    Me.cmb_section_footer5.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_section_footer5.Name = "cmb_section_footer5"
    Me.cmb_section_footer5.SelectedIndex = -1
    Me.cmb_section_footer5.Size = New System.Drawing.Size(237, 24)
    Me.cmb_section_footer5.SufixText = "Sufix Text"
    Me.cmb_section_footer5.TabIndex = 33
    Me.cmb_section_footer5.TextCombo = Nothing
    Me.cmb_section_footer5.TextVisible = False
    Me.cmb_section_footer5.TextWidth = 0
    '
    'Uc_imageList_footer5
    '
    Me.Uc_imageList_footer5.AutoSize = True
    Me.Uc_imageList_footer5.ButtonDeleteEnabled = True
    Me.Uc_imageList_footer5.FreeResize = False
    Me.Uc_imageList_footer5.Image = Nothing
    Me.Uc_imageList_footer5.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList_footer5.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList_footer5.ImageName = Nothing
    Me.Uc_imageList_footer5.Location = New System.Drawing.Point(70, 95)
    Me.Uc_imageList_footer5.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList_footer5.Name = "Uc_imageList_footer5"
    Me.Uc_imageList_footer5.Size = New System.Drawing.Size(468, 371)
    Me.Uc_imageList_footer5.TabIndex = 31
    Me.Uc_imageList_footer5.Transparent = False
    '
    'chk_visible_footer5
    '
    Me.chk_visible_footer5.AutoSize = True
    Me.chk_visible_footer5.Location = New System.Drawing.Point(8, 8)
    Me.chk_visible_footer5.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_visible_footer5.Name = "chk_visible_footer5"
    Me.chk_visible_footer5.Size = New System.Drawing.Size(63, 17)
    Me.chk_visible_footer5.TabIndex = 30
    Me.chk_visible_footer5.Text = "Visible"
    Me.chk_visible_footer5.UseVisualStyleBackColor = True
    '
    'ef_order_footer5
    '
    Me.ef_order_footer5.DoubleValue = 0.0R
    Me.ef_order_footer5.IntegerValue = 0
    Me.ef_order_footer5.IsReadOnly = False
    Me.ef_order_footer5.Location = New System.Drawing.Point(315, 36)
    Me.ef_order_footer5.Margin = New System.Windows.Forms.Padding(4)
    Me.ef_order_footer5.Name = "ef_order_footer5"
    Me.ef_order_footer5.PlaceHolder = Nothing
    Me.ef_order_footer5.Size = New System.Drawing.Size(158, 25)
    Me.ef_order_footer5.SufixText = "Sufix Text"
    Me.ef_order_footer5.TabIndex = 29
    Me.ef_order_footer5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_footer5.TextValue = ""
    Me.ef_order_footer5.TextVisible = False
    Me.ef_order_footer5.TextWidth = 51
    Me.ef_order_footer5.Value = ""
    Me.ef_order_footer5.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_section_footer5
    '
    Me.lbl_section_footer5.Location = New System.Drawing.Point(4, 35)
    Me.lbl_section_footer5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_section_footer5.Name = "lbl_section_footer5"
    Me.lbl_section_footer5.Size = New System.Drawing.Size(67, 27)
    Me.lbl_section_footer5.TabIndex = 28
    Me.lbl_section_footer5.Text = "xOptimumSize"
    Me.lbl_section_footer5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_mailing
    '
    Me.tab_mailing.Controls.Add(Me.gbCredentials)
    Me.tab_mailing.Controls.Add(Me.gbSMTPConfig)
    Me.tab_mailing.Controls.Add(Me.chk_mailing_enabled)
    Me.tab_mailing.Location = New System.Drawing.Point(4, 22)
    Me.tab_mailing.Margin = New System.Windows.Forms.Padding(4)
    Me.tab_mailing.Name = "tab_mailing"
    Me.tab_mailing.Padding = New System.Windows.Forms.Padding(4)
    Me.tab_mailing.Size = New System.Drawing.Size(949, 578)
    Me.tab_mailing.TabIndex = 4
    Me.tab_mailing.Text = "xTabMailing"
    Me.tab_mailing.UseVisualStyleBackColor = True
    '
    'gbCredentials
    '
    Me.gbCredentials.Controls.Add(Me.lbl_mailing_emaiAddress)
    Me.gbCredentials.Controls.Add(Me.lbl_mailing_domain)
    Me.gbCredentials.Controls.Add(Me.lbl_mailing_password)
    Me.gbCredentials.Controls.Add(Me.lbl_mailing_username)
    Me.gbCredentials.Controls.Add(Me.Uc_mailing_password)
    Me.gbCredentials.Controls.Add(Me.Uc_mailing_domain)
    Me.gbCredentials.Controls.Add(Me.Uc_mailing_username)
    Me.gbCredentials.Controls.Add(Me.Uc_mailing_emaiAddress)
    Me.gbCredentials.Location = New System.Drawing.Point(7, 206)
    Me.gbCredentials.Name = "gbCredentials"
    Me.gbCredentials.Size = New System.Drawing.Size(429, 199)
    Me.gbCredentials.TabIndex = 35
    Me.gbCredentials.TabStop = False
    Me.gbCredentials.Text = "xGbCredentials"
    '
    'lbl_mailing_emaiAddress
    '
    Me.lbl_mailing_emaiAddress.AutoSize = True
    Me.lbl_mailing_emaiAddress.Location = New System.Drawing.Point(8, 160)
    Me.lbl_mailing_emaiAddress.Name = "lbl_mailing_emaiAddress"
    Me.lbl_mailing_emaiAddress.Size = New System.Drawing.Size(44, 13)
    Me.lbl_mailing_emaiAddress.TabIndex = 39
    Me.lbl_mailing_emaiAddress.Text = "Label7"
    '
    'lbl_mailing_domain
    '
    Me.lbl_mailing_domain.AutoSize = True
    Me.lbl_mailing_domain.Location = New System.Drawing.Point(8, 117)
    Me.lbl_mailing_domain.Name = "lbl_mailing_domain"
    Me.lbl_mailing_domain.Size = New System.Drawing.Size(44, 13)
    Me.lbl_mailing_domain.TabIndex = 38
    Me.lbl_mailing_domain.Text = "Label6"
    '
    'lbl_mailing_password
    '
    Me.lbl_mailing_password.AutoSize = True
    Me.lbl_mailing_password.Location = New System.Drawing.Point(8, 74)
    Me.lbl_mailing_password.Name = "lbl_mailing_password"
    Me.lbl_mailing_password.Size = New System.Drawing.Size(44, 13)
    Me.lbl_mailing_password.TabIndex = 37
    Me.lbl_mailing_password.Text = "Label5"
    '
    'lbl_mailing_username
    '
    Me.lbl_mailing_username.AutoSize = True
    Me.lbl_mailing_username.Location = New System.Drawing.Point(8, 31)
    Me.lbl_mailing_username.Name = "lbl_mailing_username"
    Me.lbl_mailing_username.Size = New System.Drawing.Size(44, 13)
    Me.lbl_mailing_username.TabIndex = 36
    Me.lbl_mailing_username.Text = "Label4"
    '
    'Uc_mailing_password
    '
    Me.Uc_mailing_password.DoubleValue = 0.0R
    Me.Uc_mailing_password.IntegerValue = 0
    Me.Uc_mailing_password.IsReadOnly = False
    Me.Uc_mailing_password.Location = New System.Drawing.Point(103, 71)
    Me.Uc_mailing_password.Margin = New System.Windows.Forms.Padding(4)
    Me.Uc_mailing_password.Name = "Uc_mailing_password"
    Me.Uc_mailing_password.PlaceHolder = Nothing
    Me.Uc_mailing_password.Size = New System.Drawing.Size(308, 24)
    Me.Uc_mailing_password.SufixText = "Sufix Text"
    Me.Uc_mailing_password.TabIndex = 35
    Me.Uc_mailing_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_mailing_password.TextValue = ""
    Me.Uc_mailing_password.TextVisible = False
    Me.Uc_mailing_password.TextWidth = 0
    Me.Uc_mailing_password.Value = ""
    Me.Uc_mailing_password.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_mailing_domain
    '
    Me.Uc_mailing_domain.DoubleValue = 0.0R
    Me.Uc_mailing_domain.IntegerValue = 0
    Me.Uc_mailing_domain.IsReadOnly = False
    Me.Uc_mailing_domain.Location = New System.Drawing.Point(103, 114)
    Me.Uc_mailing_domain.Margin = New System.Windows.Forms.Padding(4)
    Me.Uc_mailing_domain.Name = "Uc_mailing_domain"
    Me.Uc_mailing_domain.PlaceHolder = Nothing
    Me.Uc_mailing_domain.Size = New System.Drawing.Size(308, 24)
    Me.Uc_mailing_domain.SufixText = "Sufix Text"
    Me.Uc_mailing_domain.TabIndex = 32
    Me.Uc_mailing_domain.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_mailing_domain.TextValue = ""
    Me.Uc_mailing_domain.TextVisible = False
    Me.Uc_mailing_domain.TextWidth = 0
    Me.Uc_mailing_domain.Value = ""
    Me.Uc_mailing_domain.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_mailing_username
    '
    Me.Uc_mailing_username.DoubleValue = 0.0R
    Me.Uc_mailing_username.IntegerValue = 0
    Me.Uc_mailing_username.IsReadOnly = False
    Me.Uc_mailing_username.Location = New System.Drawing.Point(103, 28)
    Me.Uc_mailing_username.Margin = New System.Windows.Forms.Padding(4)
    Me.Uc_mailing_username.Name = "Uc_mailing_username"
    Me.Uc_mailing_username.PlaceHolder = Nothing
    Me.Uc_mailing_username.Size = New System.Drawing.Size(308, 24)
    Me.Uc_mailing_username.SufixText = "Sufix Text"
    Me.Uc_mailing_username.TabIndex = 34
    Me.Uc_mailing_username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_mailing_username.TextValue = ""
    Me.Uc_mailing_username.TextVisible = False
    Me.Uc_mailing_username.TextWidth = 0
    Me.Uc_mailing_username.Value = ""
    Me.Uc_mailing_username.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_mailing_emaiAddress
    '
    Me.Uc_mailing_emaiAddress.DoubleValue = 0.0R
    Me.Uc_mailing_emaiAddress.IntegerValue = 0
    Me.Uc_mailing_emaiAddress.IsReadOnly = False
    Me.Uc_mailing_emaiAddress.Location = New System.Drawing.Point(103, 157)
    Me.Uc_mailing_emaiAddress.Margin = New System.Windows.Forms.Padding(4)
    Me.Uc_mailing_emaiAddress.Name = "Uc_mailing_emaiAddress"
    Me.Uc_mailing_emaiAddress.PlaceHolder = Nothing
    Me.Uc_mailing_emaiAddress.Size = New System.Drawing.Size(308, 24)
    Me.Uc_mailing_emaiAddress.SufixText = "Sufix Text"
    Me.Uc_mailing_emaiAddress.TabIndex = 33
    Me.Uc_mailing_emaiAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_mailing_emaiAddress.TextValue = ""
    Me.Uc_mailing_emaiAddress.TextVisible = False
    Me.Uc_mailing_emaiAddress.TextWidth = 0
    Me.Uc_mailing_emaiAddress.Value = ""
    Me.Uc_mailing_emaiAddress.ValueForeColor = System.Drawing.Color.Blue
    '
    'gbSMTPConfig
    '
    Me.gbSMTPConfig.Controls.Add(Me.lbl_smtp_secured)
    Me.gbSMTPConfig.Controls.Add(Me.lbl_mailing_serverPort)
    Me.gbSMTPConfig.Controls.Add(Me.lbl_mailing_serverAdress)
    Me.gbSMTPConfig.Controls.Add(Me.cmb_smtp_secured)
    Me.gbSMTPConfig.Controls.Add(Me.Uc_mailing_serverPort)
    Me.gbSMTPConfig.Controls.Add(Me.Uc_mailing_serverAdress)
    Me.gbSMTPConfig.Location = New System.Drawing.Point(5, 41)
    Me.gbSMTPConfig.Name = "gbSMTPConfig"
    Me.gbSMTPConfig.Size = New System.Drawing.Size(431, 157)
    Me.gbSMTPConfig.TabIndex = 34
    Me.gbSMTPConfig.TabStop = False
    Me.gbSMTPConfig.Text = "xGbSMTPConfig"
    '
    'lbl_smtp_secured
    '
    Me.lbl_smtp_secured.AutoSize = True
    Me.lbl_smtp_secured.Location = New System.Drawing.Point(8, 116)
    Me.lbl_smtp_secured.Name = "lbl_smtp_secured"
    Me.lbl_smtp_secured.Size = New System.Drawing.Size(44, 13)
    Me.lbl_smtp_secured.TabIndex = 37
    Me.lbl_smtp_secured.Text = "Label3"
    '
    'lbl_mailing_serverPort
    '
    Me.lbl_mailing_serverPort.AutoSize = True
    Me.lbl_mailing_serverPort.Location = New System.Drawing.Point(8, 74)
    Me.lbl_mailing_serverPort.Name = "lbl_mailing_serverPort"
    Me.lbl_mailing_serverPort.Size = New System.Drawing.Size(44, 13)
    Me.lbl_mailing_serverPort.TabIndex = 36
    Me.lbl_mailing_serverPort.Text = "Label2"
    '
    'lbl_mailing_serverAdress
    '
    Me.lbl_mailing_serverAdress.AutoSize = True
    Me.lbl_mailing_serverAdress.Location = New System.Drawing.Point(8, 32)
    Me.lbl_mailing_serverAdress.Name = "lbl_mailing_serverAdress"
    Me.lbl_mailing_serverAdress.Size = New System.Drawing.Size(44, 13)
    Me.lbl_mailing_serverAdress.TabIndex = 35
    Me.lbl_mailing_serverAdress.Text = "Label1"
    '
    'cmb_smtp_secured
    '
    Me.cmb_smtp_secured.AllowUnlistedValues = False
    Me.cmb_smtp_secured.AutoCompleteMode = False
    Me.cmb_smtp_secured.IsReadOnly = False
    Me.cmb_smtp_secured.Location = New System.Drawing.Point(105, 112)
    Me.cmb_smtp_secured.Margin = New System.Windows.Forms.Padding(4)
    Me.cmb_smtp_secured.Name = "cmb_smtp_secured"
    Me.cmb_smtp_secured.SelectedIndex = -1
    Me.cmb_smtp_secured.Size = New System.Drawing.Size(53, 24)
    Me.cmb_smtp_secured.SufixText = "Sufix Text"
    Me.cmb_smtp_secured.TabIndex = 34
    Me.cmb_smtp_secured.TextCombo = Nothing
    Me.cmb_smtp_secured.TextVisible = False
    Me.cmb_smtp_secured.TextWidth = 0
    '
    'Uc_mailing_serverPort
    '
    Me.Uc_mailing_serverPort.DoubleValue = 0.0R
    Me.Uc_mailing_serverPort.IntegerValue = 0
    Me.Uc_mailing_serverPort.IsReadOnly = False
    Me.Uc_mailing_serverPort.Location = New System.Drawing.Point(105, 70)
    Me.Uc_mailing_serverPort.Margin = New System.Windows.Forms.Padding(4)
    Me.Uc_mailing_serverPort.Name = "Uc_mailing_serverPort"
    Me.Uc_mailing_serverPort.PlaceHolder = Nothing
    Me.Uc_mailing_serverPort.Size = New System.Drawing.Size(53, 24)
    Me.Uc_mailing_serverPort.SufixText = "Sufix Text"
    Me.Uc_mailing_serverPort.TabIndex = 31
    Me.Uc_mailing_serverPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_mailing_serverPort.TextValue = ""
    Me.Uc_mailing_serverPort.TextVisible = False
    Me.Uc_mailing_serverPort.TextWidth = 0
    Me.Uc_mailing_serverPort.Value = ""
    Me.Uc_mailing_serverPort.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_mailing_serverAdress
    '
    Me.Uc_mailing_serverAdress.DoubleValue = 0.0R
    Me.Uc_mailing_serverAdress.IntegerValue = 0
    Me.Uc_mailing_serverAdress.IsReadOnly = False
    Me.Uc_mailing_serverAdress.Location = New System.Drawing.Point(105, 29)
    Me.Uc_mailing_serverAdress.Margin = New System.Windows.Forms.Padding(4)
    Me.Uc_mailing_serverAdress.Name = "Uc_mailing_serverAdress"
    Me.Uc_mailing_serverAdress.PlaceHolder = Nothing
    Me.Uc_mailing_serverAdress.Size = New System.Drawing.Size(308, 24)
    Me.Uc_mailing_serverAdress.SufixText = "Sufix Text"
    Me.Uc_mailing_serverAdress.TabIndex = 30
    Me.Uc_mailing_serverAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_mailing_serverAdress.TextValue = ""
    Me.Uc_mailing_serverAdress.TextVisible = False
    Me.Uc_mailing_serverAdress.TextWidth = 0
    Me.Uc_mailing_serverAdress.Value = ""
    Me.Uc_mailing_serverAdress.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_mailing_enabled
    '
    Me.chk_mailing_enabled.AutoSize = True
    Me.chk_mailing_enabled.Location = New System.Drawing.Point(5, 14)
    Me.chk_mailing_enabled.Margin = New System.Windows.Forms.Padding(4)
    Me.chk_mailing_enabled.Name = "chk_mailing_enabled"
    Me.chk_mailing_enabled.Size = New System.Drawing.Size(117, 17)
    Me.chk_mailing_enabled.TabIndex = 26
    Me.chk_mailing_enabled.Text = "xMailingEnabled"
    Me.chk_mailing_enabled.UseVisualStyleBackColor = True
    '
    'frm_winup_settings
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1066, 621)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Margin = New System.Windows.Forms.Padding(4)
    Me.Name = "frm_winup_settings"
    Me.Padding = New System.Windows.Forms.Padding(5)
    Me.Text = "frm_winup_settings"
    Me.panel_data.ResumeLayout(False)
    Me.tab_player_club.ResumeLayout(False)
    Me.gb_PreferentText.ResumeLayout(False)
    Me.gb_PreferentText.PerformLayout()
    Me.tab_control.ResumeLayout(False)
    Me.tab_footer.ResumeLayout(False)
    Me.gb_size.ResumeLayout(False)
    Me.gb_size.PerformLayout()
    Me.TabConfigFooter.ResumeLayout(False)
    Me.tab_menu_footer1.ResumeLayout(False)
    Me.tab_menu_footer1.PerformLayout()
    Me.tab_menu_footer2.ResumeLayout(False)
    Me.tab_menu_footer2.PerformLayout()
    Me.tab_menu_footer3.ResumeLayout(False)
    Me.tab_menu_footer3.PerformLayout()
    Me.tab_menu_footer4.ResumeLayout(False)
    Me.tab_menu_footer4.PerformLayout()
    Me.tab_menu_footer5.ResumeLayout(False)
    Me.tab_menu_footer5.PerformLayout()
    Me.tab_mailing.ResumeLayout(False)
    Me.tab_mailing.PerformLayout()
    Me.gbCredentials.ResumeLayout(False)
    Me.gbCredentials.PerformLayout()
    Me.gbSMTPConfig.ResumeLayout(False)
    Me.gbSMTPConfig.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_control As System.Windows.Forms.TabControl
  Friend WithEvents tab_player_club As System.Windows.Forms.TabPage
  Friend WithEvents gb_PreferentText As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_terms As System.Windows.Forms.Label
  Friend WithEvents txt_terms As System.Windows.Forms.TextBox
  Friend WithEvents lbl_link As System.Windows.Forms.Label
  Friend WithEvents lbl_join As System.Windows.Forms.Label
  Friend WithEvents lbl_about As System.Windows.Forms.Label
  Friend WithEvents txt_link As System.Windows.Forms.TextBox
  Friend WithEvents txt_join As System.Windows.Forms.TextBox
  Friend WithEvents txt_about As System.Windows.Forms.TextBox
  Friend WithEvents tab_footer As System.Windows.Forms.TabPage
  Friend WithEvents TabConfigFooter As System.Windows.Forms.TabControl
  Friend WithEvents tab_menu_footer1 As System.Windows.Forms.TabPage
  Friend WithEvents tab_menu_footer2 As System.Windows.Forms.TabPage
  Friend WithEvents tab_menu_footer3 As System.Windows.Forms.TabPage
  Friend WithEvents tab_menu_footer4 As System.Windows.Forms.TabPage
  Friend WithEvents tab_menu_footer5 As System.Windows.Forms.TabPage
  Friend WithEvents lbl_section_footer1 As System.Windows.Forms.Label
  Friend WithEvents ef_order_footer1 As GUI_Controls.uc_entry_field
  Friend WithEvents chk_visible_footer1 As System.Windows.Forms.CheckBox
  Friend WithEvents Uc_imageList_footer1 As GUI_Controls.uc_image
  Friend WithEvents Uc_imageList_footer2 As GUI_Controls.uc_image
  Friend WithEvents chk_visible_footer2 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_order_footer2 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_section_footer2 As System.Windows.Forms.Label
  Friend WithEvents Uc_imageList_footer3 As GUI_Controls.uc_image
  Friend WithEvents chk_visible_footer3 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_order_footer3 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_section_footer3 As System.Windows.Forms.Label
  Friend WithEvents Uc_imageList_footer4 As GUI_Controls.uc_image
  Friend WithEvents chk_visible_footer4 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_order_footer4 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_section_footer4 As System.Windows.Forms.Label
  Friend WithEvents Uc_imageList_footer5 As GUI_Controls.uc_image
  Friend WithEvents chk_visible_footer5 As System.Windows.Forms.CheckBox
  Friend WithEvents ef_order_footer5 As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_section_footer5 As System.Windows.Forms.Label
  Friend WithEvents cmb_Section_footer1 As GUI_Controls.uc_combo
  Friend WithEvents cmb_section_footer2 As GUI_Controls.uc_combo
  Friend WithEvents cmb_section_footer3 As GUI_Controls.uc_combo
  Friend WithEvents cmb_section_footer4 As GUI_Controls.uc_combo
  Friend WithEvents cmb_section_footer5 As GUI_Controls.uc_combo
  Friend WithEvents tab_mailing As System.Windows.Forms.TabPage
  Friend WithEvents chk_mailing_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_size As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_weight As System.Windows.Forms.Label
  Friend WithEvents ef_height As GUI_Controls.uc_entry_field
  Friend WithEvents ef_weight As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_height As System.Windows.Forms.Label
  Friend WithEvents cmb_weight_unit As GUI_Controls.uc_combo
  Friend WithEvents lbl_width As System.Windows.Forms.Label
  Friend WithEvents ef_width As GUI_Controls.uc_entry_field
  Friend WithEvents gbCredentials As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_mailing_password As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_mailing_username As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_mailing_emaiAddress As GUI_Controls.uc_entry_field
  Friend WithEvents gbSMTPConfig As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_mailing_domain As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_mailing_serverPort As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_mailing_serverAdress As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_smtp_secured As GUI_Controls.uc_combo
  Friend WithEvents lbl_mailing_emaiAddress As System.Windows.Forms.Label
  Friend WithEvents lbl_mailing_domain As System.Windows.Forms.Label
  Friend WithEvents lbl_mailing_password As System.Windows.Forms.Label
  Friend WithEvents lbl_mailing_username As System.Windows.Forms.Label
  Friend WithEvents lbl_smtp_secured As System.Windows.Forms.Label
  Friend WithEvents lbl_mailing_serverPort As System.Windows.Forms.Label
  Friend WithEvents lbl_mailing_serverAdress As System.Windows.Forms.Label

  'Friend WithEvents gb_promotion_taxes As System.Windows.Forms.GroupBox
  'Friend WithEvents gb_prize_taxes As System.Windows.Forms.GroupBox
  'Friend WithEvents gb_taxes As System.Windows.Forms.GroupBox

End Class
