'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bonuses
' DESCRIPTION:   Bonuses report 
' AUTHOR:        Javier Molina
' CREATION DATE: 13-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-DEC-2013  JMM    Initial version
' 13-MAR-2014  JMM    Fixed bug WIG-727: Unhandled exception on clicking on 'Confirm' or 'Cancel' button with no bonus on the grid
' 17-MAR-2014  JMM    Fixed bug WIG-738: Changed cancel button text to 'Exit'
' 30-MAY-2014  JCOR   Added to Enum SAS_HOST_STATUS 12, 13, 14, 15. 
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 30-DIC-2014  SGB    Fixed bug WIG-1901: Enter the bonus status in query
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 16-FEB-2016  RAB    Bug 9417: Bonus screen: Error in filter amount "from/to"
' 28-SEP-2016  RAB    Bug 12537: Dual currency - Bonus: currency shown in "jackpot" room in a currency matching terminal.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class frm_bonuses
  Inherits frm_base_sel

#Region "Enums"

  Public Enum SAS_HOST_STATUS
    BONUS_STATUS_UNKNOWN = 0
    BONUS_STATUS_PENDING = 5
    BONUS_STATUS_TIMEOUT = 10
    BONUS_STATUS_NO_CREDIT = 11
    BONUS_STATUS_NOT_SUPPORTED = 12
    BONUS_STATUS_AWARD_IN_PROGRESS = 13
    BONUS_STATUS_INVALID_BONUS_INFO = 14
    BONUS_STATUS_NO_FOUND = 15
    BONUS_STATUS_NOTIFIED_TO_EGM = 20
    BONUS_STATUS_NOTIFIED_TO_EGM_NACK = 21
    BONUS_STATUS_NOTIFIED_TO_EGM_ACK = 22
    BONUS_STATUS_NOTIFIED_TO_EGM_IN_DOUBT = 30
    BONUS_STATUS_NOTIFIED_TO_EGM_NACK_IN_DOUBT = 31
    BONUS_STATUS_NOTIFIED_TO_EGM_ACK_IN_DOUBT = 32
    BONUS_STATUS_AWARDED_ON_EGM = 99
    BONUS_STATUS_TRANSFERRED = 100
  End Enum

  Public Enum BONUS_STATUS
    UNKNOWN = -1
    PENDING = 0
    CONFIRMED = 1
    IN_DOUBT = 2
    CANCELED = 3
    WRONG_STATUS = 99
  End Enum
#End Region

#Region "Structures"

  Private Structure STATUS_TYPE
    Dim status_code As Integer
    Dim status_nls_id As Integer
  End Structure ' TYPE_OPERATION

#End Region ' Structures

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid configuration
  Private Const GRID_HEADER_ROWS_COUNT As Integer = 1

  Private GRID_COLUMN_SITES_SENTINEL As Integer = 0
  Private GRID_COLUMN_BONUS_ID As Integer = 1
  Private GRID_COLUMN_INSERTED_DATETIME As Integer = 2
  Private GRID_INIT_TERMINAL_DATA As Integer = 3
  Private GRID_COLUMN_TARGET_TERMINAL_NAME As Integer = 4
  Private GRID_COLUMN_TRANSFERRED_RE As Integer = 5
  Private GRID_COLUMN_TRANSFERRED_PROMO_RE As Integer = 6
  Private GRID_COLUMN_TRANSFERRED_PROMO_NR As Integer = 7
  Private GRID_COLUMN_TRANSFERRED_TOTAL As Integer = 8
  Private GRID_COLUMN_TRANSFER_STATUS_TEXT As Integer = 9
  Private GRID_COLUMN_SAS_HOST_STATUS As Integer = 10
  Private GRID_COLUMN_TO_TRANSFER_RE As Integer = 11
  Private GRID_COLUMN_TO_TRANSFER_PROMO_RE As Integer = 12
  Private GRID_COLUMN_TO_TRANSFER_PROMO_NR As Integer = 13
  Private GRID_COLUMN_TO_TRANSFER_TOTAL As Integer = 14
  Private GRID_COLUMN_TARGET_ACCOUNT_ID As Integer = 15
  Private GRID_COLUMN_PLAYSESSION_ID As Integer = 16

  Private GRID_COLUMNS_COUNT As Integer = 17

  'SQL Query Columns
  Private Const SQL_COL_BONUS_ID As Integer = 0
  Private Const SQL_COL_TO_TRANSFER_RE As Integer = 1
  Private Const SQL_COL_TO_TRANSFER_PROMO_RE As Integer = 2
  Private Const SQL_COL_TO_TRANSFER_PROMO_NR As Integer = 3
  Private Const SQL_COL_TARGET_PROVIDER As Integer = 4
  Private Const SQL_COL_TARGET_TERMINAL_NAME As Integer = 5
  Private Const SQL_COL_TARGET_ACCOUNT_ID As Integer = 6
  Private Const SQL_COL_TRANSFER_STATUS As Integer = 7
  Private Const SQL_COL_TRANSFER_STATUS_TEXT As Integer = 8
  Private Const SQL_COL_INSERTED_DATETIME As Integer = 9
  Private Const SQL_COL_TRANSFERRED_RE As Integer = 10
  Private Const SQL_COL_TRANSFERRED_PROMO_RE As Integer = 11
  Private Const SQL_COL_TRANSFERRED_PROMO_NR As Integer = 12
  Private Const SQL_COL_SAS_HOST_STATUS As Integer = 13
  Private Const SQL_COL_PLAYSESSION_ID As Integer = 14
  Private Const SQL_COL_TARGET_TERMINAL_ID As Integer = 15

  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2

  Private Const GRID_2_COLUMNS As Integer = 3
  Private Const GRID_2_HEADER_ROWS As Integer = 0
  Private Const GRID_2_TAB As String = "    "

  Private m_window_color As Color = System.Drawing.SystemColors.Window
  Private m_window_text_color As Color = System.Drawing.SystemColors.WindowText

  Private m_red_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_02)
  Private m_white_color As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

  Private m_refreshing_grid As Boolean

#End Region ' Constants

#Region " Members "

  ' Report filters 
  Private m_date_from_filter As String
  Private m_date_to_filter As String
  Private m_status_filter As String
  Private m_terminals As String
  Private m_amount_from_filter As String
  Private m_amount_to_filter As String

  Private m_status_list As List(Of STATUS_TYPE)

  ' Totals
  Private m_total As Decimal

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BONUSES

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3389) '3389 "Bonus"

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Enable/Disable buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4672) '4672 "Confirm"

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4679) '4679 "Cancel"

    Me.btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    Me.btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) '268 "Fecha"
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())
    Me.uc_pr_list.FilterByCurrency = False

    'Amount filter
    Me.gb_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)     '3372 "Amount"
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4698)     '4698 "Status"

    Me.ef_amount_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) '297 "From"
    Me.ef_amount_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    Me.ef_amount_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)   '298 "To"
    Me.ef_amount_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Grid
    Call GUI_StyleSheet()
    Call GUI_StyleSheet_Status()

    ' Status filter
    Call GUI_FillStatusFilter()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub 'GUI_InitControls


  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_from

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _from_amount As Int32
    Dim _to_amount As Int32

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Amount filter
    _from_amount = 0
    _to_amount = 0

    If Me.ef_amount_from.Value <> "" Then
      _from_amount = Me.ef_amount_from.Value
    End If

    If Me.ef_amount_to.Value <> "" Then
      _to_amount = Me.ef_amount_to.Value
    End If

    If _from_amount >= _to_amount And _from_amount <> 0 And _to_amount <> 0 Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3407), ENUM_MB_TYPE.MB_TYPE_WARNING) '3407 "Amount range error: From amount must be lower thna to amount."
      Call Me.ef_amount_from.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    ' Get Select and from
    _str_sql.AppendLine("SELECT   BNS_BONUS_ID                                              ")
    _str_sql.AppendLine("       , ISNULL (BNS_TO_TRANSFER_RE,       0)                      ")
    _str_sql.AppendLine("       , ISNULL (BNS_TO_TRANSFER_PROMO_RE, 0)                      ")
    _str_sql.AppendLine("       , ISNULL (BNS_TO_TRANSFER_PROMO_NR, 0)                      ")
    _str_sql.AppendLine("       , TE_PROVIDER_ID                                            ")
    _str_sql.AppendLine("       , TE_NAME                                                   ")
    _str_sql.AppendLine("       , BNS_TARGET_ACCOUNT_ID                                     ")
    _str_sql.AppendLine("       , BNS_TRANSFER_STATUS                                       ")
    _str_sql.AppendLine("       , BNS_TRANSFER_STATUS_TEXT                                  ")
    _str_sql.AppendLine("       , BNS_INSERTED                                              ")
    _str_sql.AppendLine("       , ISNULL (BNS_TRANSFERRED_RE,       0)                      ")
    _str_sql.AppendLine("       , ISNULL (BNS_TRANSFERRED_PROMO_RE, 0)                      ")
    _str_sql.AppendLine("       , ISNULL (BNS_TRANSFERRED_PROMO_NR, 0)                      ")
    _str_sql.AppendLine("       , BNS_SAS_HOST_STATUS                                       ")
    _str_sql.AppendLine("       , BNS_TRANSFERRED_PLAY_SESSION_ID                           ")
    _str_sql.AppendLine("       , BNS_TARGET_TERMINAL_ID                                    ")
    _str_sql.AppendLine("  FROM   BONUSES                                                   ")
    _str_sql.AppendLine(" INNER   JOIN TERMINALS ON TE_TERMINAL_ID = BNS_TARGET_TERMINAL_ID ")

    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine("ORDER BY   BNS_BONUS_ID DESC ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_ConfirmOrCancelBonus(BONUS_STATUS.CONFIRMED)

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call GUI_ConfirmOrCancelBonus(BONUS_STATUS.CANCELED)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_total = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _last_row As Integer

    _last_row = Me.Grid.NumRows
    Me.Grid.AddRow()
    Me.Grid.Row(_last_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.Grid.Cell(_last_row, GRID_COLUMN_TARGET_TERMINAL_NAME).Value = GLB_NLS_GUI_STATISTICS.GetString(203) '203 "TOTAL: "

    Me.Grid.Cell(_last_row, GRID_COLUMN_TRANSFERRED_TOTAL).Value = GUI_FormatCurrency(m_total)
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _idx_row As Int32
    Dim _session_id As Integer
    Dim _frm_sel As frm_plays_plays

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then

        Exit For
      End If
    Next

    If Not IsValidDataRow(_idx_row) Then

      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm_sel = New frm_plays_plays

    ' Get the complete session ID and session date and launch select form
    _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYSESSION_ID).Value

    _frm_sel.ShowForEdit(Me.MdiParent, _session_id, "", "")

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    ' If Row is Header, exit.
    If RowIndex < 0 Then
      Exit Sub
    End If

    If ColIndex <> GRID_COLUMN_TRANSFER_STATUS_TEXT Then
      Exit Sub
    End If

    If Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_HOST_STATUS).Value <> "" Then
      ToolTipTxt = TranslateSasHostStatus(Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_HOST_STATUS).Value)
    End If

  End Sub ' GUI_SetToolTipText

#End Region ' Overrides

#Region " GUI Reports "

  ' PURPOSE : Set Custom formats to excel
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  ' 
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    MyBase.GUI_ReportParams(ExcelData, FirstColIndex)

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _status_filter_desc As StringBuilder

    'First col
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from_filter)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to_filter)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'Second col
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(628) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_amount_from_filter)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(628) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_amount_to_filter)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'Third col
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'Fourth col
    _status_filter_desc = New StringBuilder

    For Each _status As STATUS_TYPE In m_status_list
      If m_status_filter.Contains(_status.status_code) Then

        If _status_filter_desc.Length > 0 Then
          _status_filter_desc.Append(", ")
        End If
        _status_filter_desc.Append(GLB_NLS_GUI_PLAYER_TRACKING.GetString(_status.status_nls_id))
      End If
    Next

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1747), _status_filter_desc.ToString())
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.FilterValueWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(3) = 3000
    PrintData.FilterHeaderWidth(4) = 300

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' Date filter
    m_date_from_filter = ""
    m_date_to_filter = ""

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from_filter = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to_filter = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Providers - Terminals
    m_terminals = ""

    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    'Status filter
    m_status_filter = GetStatusSelected()

    'Amount filter
    m_amount_from_filter = ""
    m_amount_to_filter = ""

    If (Me.ef_amount_from.Value <> "") Then
      m_amount_from_filter = GUI_FormatCurrency(Me.ef_amount_from.Value)
    End If

    If (Me.ef_amount_to.Value <> "") Then
      m_amount_to_filter = GUI_FormatCurrency(Me.ef_amount_to.Value)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS_COUNT)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Sentinel
      .Column(GRID_COLUMN_SITES_SENTINEL).Header(0).Text = " "
      .Column(GRID_COLUMN_SITES_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SITES_SENTINEL).HighLightWhenSelected = False

      ' Bonus ID
      .Column(GRID_COLUMN_BONUS_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_BONUS_ID).Width = 0
      .Column(GRID_COLUMN_BONUS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Inserted Datetime
      .Column(GRID_COLUMN_INSERTED_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268) '268 "Fecha"
      .Column(GRID_COLUMN_INSERTED_DATETIME).Width = 2500
      .Column(GRID_COLUMN_INSERTED_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TARGET_TERMINAL_NAME = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' Transferred RE
      .Column(GRID_COLUMN_TRANSFERRED_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) '2125 "Redeemable"
      .Column(GRID_COLUMN_TRANSFERRED_RE).Width = 0
      .Column(GRID_COLUMN_TRANSFERRED_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Transferred Promo RE
      .Column(GRID_COLUMN_TRANSFERRED_PROMO_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3351) '3351  "Promotion RE"
      .Column(GRID_COLUMN_TRANSFERRED_PROMO_RE).Width = 0
      .Column(GRID_COLUMN_TRANSFERRED_PROMO_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Transferred Promo NR
      .Column(GRID_COLUMN_TRANSFERRED_PROMO_NR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3352) '3352 "Promotion NR"
      .Column(GRID_COLUMN_TRANSFERRED_PROMO_NR).Width = 0
      .Column(GRID_COLUMN_TRANSFERRED_PROMO_NR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Transferred Total
      .Column(GRID_COLUMN_TRANSFERRED_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3406) '3406 "Transferred"
      .Column(GRID_COLUMN_TRANSFERRED_TOTAL).Width = 1550
      .Column(GRID_COLUMN_TRANSFERRED_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account ID
      .Column(GRID_COLUMN_TARGET_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202)  '1202 "Account"
      .Column(GRID_COLUMN_TARGET_ACCOUNT_ID).Width = 0
      .Column(GRID_COLUMN_TARGET_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Transfer Status
      .Column(GRID_COLUMN_TRANSFER_STATUS_TEXT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1747) '1747 "Status"
      .Column(GRID_COLUMN_TRANSFER_STATUS_TEXT).Width = 2050
      .Column(GRID_COLUMN_TRANSFER_STATUS_TEXT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' SasHost Status
      .Column(GRID_COLUMN_SAS_HOST_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4698)
      .Column(GRID_COLUMN_SAS_HOST_STATUS).Width = 0
      .Column(GRID_COLUMN_SAS_HOST_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' To Transfer RE
      .Column(GRID_COLUMN_TO_TRANSFER_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125) '2125 "Redeemable"
      .Column(GRID_COLUMN_TO_TRANSFER_RE).Width = 0
      .Column(GRID_COLUMN_TO_TRANSFER_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' To Transfer Promo RE
      .Column(GRID_COLUMN_TO_TRANSFER_PROMO_RE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3351) '3351  "Promotion RE"
      .Column(GRID_COLUMN_TO_TRANSFER_PROMO_RE).Width = 0
      .Column(GRID_COLUMN_TO_TRANSFER_PROMO_RE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' To Transfer Promo NR
      .Column(GRID_COLUMN_TO_TRANSFER_PROMO_NR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3352) '3352 "Promotion NR"
      .Column(GRID_COLUMN_TO_TRANSFER_PROMO_NR).Width = 0
      .Column(GRID_COLUMN_TO_TRANSFER_PROMO_NR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' To Transfer Total
      .Column(GRID_COLUMN_TO_TRANSFER_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3405) '3405 "To transfer"
      .Column(GRID_COLUMN_TO_TRANSFER_TOTAL).Width = 1550
      .Column(GRID_COLUMN_TO_TRANSFER_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' PlaySessionId
      .Column(GRID_COLUMN_PLAYSESSION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4699)
      .Column(GRID_COLUMN_PLAYSESSION_ID).Width = 0
      .Column(GRID_COLUMN_PLAYSESSION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  Private Sub GUI_StyleSheet_Status()
    With Me.dg_status
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 350
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 3150
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With
  End Sub

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_FillStatusFilter()
    Dim _idx_event As Integer
    Dim _prev_redraw As Boolean

    m_status_list = GetStatusFilters()

    Me.dg_status.Clear()
    _prev_redraw = Me.dg_status.Redraw
    Me.dg_status.Redraw = False

    Try
      _idx_event = 0
      For Each _status As STATUS_TYPE In m_status_list

        dg_status.AddRow()
        dg_status.Cell(dg_status.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        dg_status.Cell(dg_status.NumRows - 1, GRID_2_COLUMN_CODE).Value = _status.status_code
        dg_status.Cell(dg_status.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & GLB_NLS_GUI_PLAYER_TRACKING.GetString(_status.status_nls_id)

        _idx_event += 1
      Next

      If Me.dg_status.NumRows > 0 Then
        Me.dg_status.CurrentRow = 0
      End If

    Finally
      Me.dg_status.Redraw = _prev_redraw
    End Try
  End Sub ' GUI_FillStatusFilter

  ' PURPOSE: Obtain a SQL query with the selected operations
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  '
  Private Function GetStatusSelected() As String
    Dim _idx_rows As Integer
    Dim _str_selected_status As String
    Dim _empty As Boolean
    Dim _value As String

    _empty = True
    _str_selected_status = ""

    For _idx_rows = 0 To dg_status.NumRows - 1

      If dg_status.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then

        _value = dg_status.Cell(_idx_rows, GRID_2_COLUMN_CODE).Value

        If String.IsNullOrEmpty(_str_selected_status) Then
          _str_selected_status = _value
        Else
          _str_selected_status = _str_selected_status + ", " + _value
        End If

        _empty = False
      End If
    Next

    If _empty Then
      For _idx_rows = 0 To dg_status.NumRows - 1
        If String.IsNullOrEmpty(_str_selected_status) Then
          _str_selected_status = dg_status.Cell(_idx_rows, GRID_2_COLUMN_CODE).Value
        Else
          _str_selected_status = _str_selected_status + ", " + dg_status.Cell(_idx_rows, GRID_2_COLUMN_CODE).Value
        End If
      Next
    End If

    Return _str_selected_status
  End Function ' GetOperationSelected  

  Private Function GetStatusFilters() As List(Of STATUS_TYPE)
    Dim _status As STATUS_TYPE
    Dim _status_list As List(Of STATUS_TYPE)

    _status_list = New List(Of STATUS_TYPE)

    _status.status_code = BONUS_STATUS.PENDING
    _status.status_nls_id = GetStatusNLS(_status.status_code)
    _status_list.Add(_status)

    _status.status_code = BONUS_STATUS.CONFIRMED
    _status.status_nls_id = GetStatusNLS(_status.status_code)
    _status_list.Add(_status)

    _status.status_code = BONUS_STATUS.IN_DOUBT
    _status.status_nls_id = GetStatusNLS(_status.status_code)
    _status_list.Add(_status)

    _status.status_code = BONUS_STATUS.CANCELED
    _status.status_nls_id = GetStatusNLS(_status.status_code)
    _status_list.Add(_status)

    _status.status_code = BONUS_STATUS.WRONG_STATUS
    _status.status_nls_id = GetStatusNLS(_status.status_code)
    _status_list.Add(_status)

    Return _status_list
  End Function

  Private Function GetTransferStatusSelected() As String
    Dim _idx_rows As Integer
    Dim _value As Integer
    Dim _str_transfer_status As String = Nothing
    Dim _empty As Boolean = True

    ' IN of transfer status selected of dg_status
    For _idx_rows = 0 To dg_status.NumRows - 1

      If dg_status.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then

        _value = dg_status.Cell(_idx_rows, GRID_2_COLUMN_CODE).Value

        If String.IsNullOrEmpty(_str_transfer_status) Then
          _str_transfer_status = TranslateBonusStatusQuery(_value)
        Else
          _str_transfer_status = _str_transfer_status + ", " + TranslateBonusStatusQuery(_value)
        End If
        _empty = False
      End If
    Next

    'if not selected any, input IN all transfer status
    If _empty Then
      For _idx_rows = 0 To dg_status.NumRows - 1
        _value = dg_status.Cell(_idx_rows, GRID_2_COLUMN_CODE).Value
        If String.IsNullOrEmpty(_str_transfer_status) Then
          _str_transfer_status = TranslateBonusStatusQuery(_value)
        Else
          _str_transfer_status = _str_transfer_status + ", " + TranslateBonusStatusQuery(_value)
        End If
      Next
    End If

    Return _str_transfer_status
  End Function

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _back_color As Color
    Dim _fore_color As Color
    Dim _to_transfer_re As Double
    Dim _to_transfer_promo_re As Double
    Dim _to_transfer_promo_nr As Double
    Dim _to_transfer_total As Double
    Dim _transferred_re As Double
    Dim _transferred_promo_re As Double
    Dim _transferred_promo_nr As Double
    Dim _transferred_total As Double
    Dim _sas_host_status As Integer
    Dim _wcp_status As Integer
    Dim _str_amount As String
    Dim _terminal_data As List(Of Object)

    _to_transfer_re = 0
    _to_transfer_promo_re = 0
    _to_transfer_promo_nr = 0
    _to_transfer_total = 0
    _transferred_re = 0
    _transferred_promo_re = 0
    _transferred_promo_nr = 0
    _transferred_total = 0

    _back_color = m_window_color
    _fore_color = m_window_text_color

    ' Bonus ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BONUS_ID).Value = DbRow.Value(SQL_COL_BONUS_ID)

    If Not IsDBNull(DbRow.Value(SQL_COL_TO_TRANSFER_RE)) Then
      _to_transfer_re = DbRow.Value(SQL_COL_TO_TRANSFER_RE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_TO_TRANSFER_PROMO_RE)) Then
      _to_transfer_promo_re = DbRow.Value(SQL_COL_TO_TRANSFER_PROMO_RE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_TO_TRANSFER_PROMO_NR)) Then
      _to_transfer_promo_nr = DbRow.Value(SQL_COL_TO_TRANSFER_PROMO_NR)
    End If

    _to_transfer_total = _to_transfer_re + _to_transfer_promo_re + _to_transfer_promo_nr

    If Not IsDBNull(DbRow.Value(SQL_COL_TRANSFERRED_RE)) Then
      _transferred_re = DbRow.Value(SQL_COL_TRANSFERRED_RE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_TRANSFERRED_PROMO_RE)) Then
      _transferred_promo_re = DbRow.Value(SQL_COL_TRANSFERRED_PROMO_RE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_TRANSFERRED_PROMO_NR)) Then
      _transferred_promo_nr = DbRow.Value(SQL_COL_TRANSFERRED_PROMO_NR)
    End If

    _transferred_total = _transferred_re + _transferred_promo_re + _transferred_promo_nr

    m_total += _transferred_total

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITES_SENTINEL).Value = " "

    _str_amount = ""
    If _to_transfer_re > 0 Then
      _str_amount = GUI_FormatCurrency(_to_transfer_re)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_TRANSFER_RE).Value = _str_amount

    _str_amount = ""
    If _to_transfer_promo_re > 0 Then
      _str_amount = GUI_FormatCurrency(_to_transfer_promo_re)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_TRANSFER_PROMO_RE).Value = _str_amount

    _str_amount = ""
    If _to_transfer_promo_nr > 0 Then
      _str_amount = GUI_FormatCurrency(_to_transfer_promo_nr)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_TRANSFER_PROMO_NR).Value = _str_amount

    _str_amount = ""
    If _to_transfer_total > 0 Then
      _str_amount = GUI_FormatCurrency(_to_transfer_total)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TO_TRANSFER_TOTAL).Value = _str_amount

    _str_amount = ""
    If _transferred_re > 0 Then
      _str_amount = GUI_FormatCurrency(_transferred_re)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFERRED_RE).Value = _str_amount

    _str_amount = ""
    If _transferred_promo_re > 0 Then
      _str_amount = GUI_FormatCurrency(_transferred_promo_re)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFERRED_PROMO_RE).Value = _str_amount

    _str_amount = ""
    If _transferred_promo_nr > 0 Then
      _str_amount = GUI_FormatCurrency(_transferred_promo_nr)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFERRED_PROMO_NR).Value = _str_amount

    _str_amount = ""
    If _transferred_total > 0 Then
      _str_amount = GUI_FormatCurrency(_transferred_total)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFERRED_TOTAL).Value = _str_amount

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COL_TARGET_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    If Not IsDBNull(DbRow.Value(SQL_COL_TARGET_ACCOUNT_ID)) _
      And DbRow.Value(SQL_COL_TARGET_ACCOUNT_ID).ToString() <> "0" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TARGET_ACCOUNT_ID).Value = DbRow.Value(SQL_COL_TARGET_ACCOUNT_ID).ToString()
    End If

    _sas_host_status = vbNull
    If Not IsDBNull(DbRow.Value(SQL_COL_SAS_HOST_STATUS)) Then
      _sas_host_status = DbRow.Value(SQL_COL_SAS_HOST_STATUS)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SAS_HOST_STATUS).Value = _sas_host_status
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_TRANSFER_STATUS)) Then
      _wcp_status = TranslateWCPStatus(DbRow.Value(SQL_COL_TRANSFER_STATUS))

      If Not m_status_filter.Contains(_wcp_status) Then
        Return False
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFER_STATUS_TEXT).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetStatusNLS(_wcp_status))

      If _wcp_status = BONUS_STATUS.IN_DOUBT Then
        _back_color = m_red_color
        _fore_color = m_white_color
      End If
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_INSERTED_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INSERTED_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COL_INSERTED_DATETIME), _
                                                                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_PLAYSESSION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYSESSION_ID).Value = DbRow.Value(SQL_COL_PLAYSESSION_ID)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITES_SENTINEL).BackColor = _back_color
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITES_SENTINEL).ForeColor = _fore_color

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFER_STATUS_TEXT).BackColor = _back_color
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TRANSFER_STATUS_TEXT).ForeColor = _fore_color

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()
    Dim _final_time As Date
    Dim _closing_time As Integer
    Dim _now As Date
    Dim _idx_rows As Integer

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    Me.dtp_to.Value = _final_time
    Me.dtp_to.Checked = True

    Me.dtp_from.Value = _final_time.AddDays(-1)
    Me.dtp_from.Checked = True

    Me.ef_amount_from.Value = ""
    Me.ef_amount_to.Value = ""

    Call Me.uc_pr_list.SetDefaultValues()

    ' dg_operations
    For _idx_rows = 0 To dg_status.NumRows - 1
      dg_status.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues  

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As StringBuilder
    Dim _str_where_account As String = ""

    _str_where = New StringBuilder()

    ' Dates filter
    If Me.dtp_from.Checked = True Then
      _str_where.AppendLine(" AND (BNS_INSERTED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
    End If

    If Me.dtp_to.Checked = True Then
      _str_where.AppendLine(" AND (BNS_INSERTED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
    End If

    _str_where.AppendLine(" AND BNS_TRANSFER_STATUS IN (" & GetTransferStatusSelected() & ")")

    ' Amount filter
    If Me.ef_amount_from.Value <> String.Empty Then
      _str_where.AppendLine(" AND ISNULL(BNS_TO_TRANSFER_RE + BNS_TO_TRANSFER_PROMO_RE + BNS_TO_TRANSFER_PROMO_NR, 0) >= '" & Me.ef_amount_from.Value & "'")   '' Integer value don't work!!!
    End If

    If Me.ef_amount_to.Value <> String.Empty Then
      _str_where.AppendLine(" AND ISNULL(BNS_TO_TRANSFER_RE + BNS_TO_TRANSFER_PROMO_RE + BNS_TO_TRANSFER_PROMO_NR, 0) <= '" & Me.ef_amount_to.Value & "'")
    End If

    ' Filter Providers - Terminals
    _str_where.AppendLine(" AND BNS_TARGET_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    If Not String.IsNullOrEmpty(_str_where.ToString()) Then
      _str_where = New StringBuilder(" WHERE " & _str_where.Remove(_str_where.ToString().IndexOf("AND"), 3).ToString())
    End If

    Return _str_where.ToString()
  End Function ' GetSqlWhere

  Private Function TranslateSasHostStatus(ByVal SasHostStatus As SAS_HOST_STATUS) As String
    Select Case SasHostStatus
      Case SAS_HOST_STATUS.BONUS_STATUS_UNKNOWN
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4659)
      Case SAS_HOST_STATUS.BONUS_STATUS_PENDING
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4660)
      Case SAS_HOST_STATUS.BONUS_STATUS_TIMEOUT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4661)
      Case SAS_HOST_STATUS.BONUS_STATUS_NO_CREDIT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4662)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOT_SUPPORTED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4681)
      Case SAS_HOST_STATUS.BONUS_STATUS_AWARD_IN_PROGRESS
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4682)
      Case SAS_HOST_STATUS.BONUS_STATUS_INVALID_BONUS_INFO
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4683)
      Case SAS_HOST_STATUS.BONUS_STATUS_NO_FOUND
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4684)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOTIFIED_TO_EGM
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4662)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOTIFIED_TO_EGM_NACK
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4664)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOTIFIED_TO_EGM_ACK
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4665)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOTIFIED_TO_EGM_IN_DOUBT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4666)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOTIFIED_TO_EGM_NACK_IN_DOUBT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4667)
      Case SAS_HOST_STATUS.BONUS_STATUS_NOTIFIED_TO_EGM_ACK_IN_DOUBT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4668)
      Case SAS_HOST_STATUS.BONUS_STATUS_AWARDED_ON_EGM
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4669)
      Case SAS_HOST_STATUS.BONUS_STATUS_TRANSFERRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4670)
      Case Else
        Return ""
    End Select
  End Function

  ' PURPOSE: Uncheck all subrows (rows not headers)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer

    For _idx = FirstRow - 1 To 0 Step -1

      If Me.dg_status.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then

        Exit Sub
      End If
    Next
  End Sub ' TryToUncheckHeader

  ' PURPOSE: Checks whether the specified row has valid data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains valid data
  '     - FALSE: selected row does not exist or contains no valid data
  '
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column SESSION_ID.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_PLAYSESSION_ID).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None
  Protected Sub GUI_ConfirmOrCancelBonus(ByVal BonusStatus As BONUS_STATUS)
    Dim _idx_row As Integer
    Dim _str_in_doubt_status As String
    Dim _frm_bonuses_confirm As frm_bonuses_confirm
    Dim _bonus_id As Long

    ' Ensure there is any row on the grid
    If Me.Grid.NumRows = 0 Then

      Exit Sub
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then

        Exit For
      End If
    Next

    ' Ensure _idx_row refers a valid grid row.  Last row (Me.Grid.NumRows - 1) is the total row
    If _idx_row < 0 Or _idx_row > Me.Grid.NumRows - 2 Then

      Exit Sub
    End If

    _str_in_doubt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(GetStatusNLS(BONUS_STATUS.IN_DOUBT))

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_TRANSFER_STATUS_TEXT).Value <> _str_in_doubt_status Then

      If BonusStatus = BONUS_STATUS.CANCELED Then
        '4680 "Only '%1' bonuses can be canceled"
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4680), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, , _str_in_doubt_status)
      Else
        '4673 "Only '%1' bonuses can be confirmed"
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4673), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, , _str_in_doubt_status)
      End If

      Exit Sub
    End If

    _bonus_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_BONUS_ID).Value

    _frm_bonuses_confirm = New frm_bonuses_confirm

    _frm_bonuses_confirm.DefaultValue = BonusStatus

    _frm_bonuses_confirm.ShowEditItem(_bonus_id)

  End Sub

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SITES_SENTINEL = 0
    GRID_COLUMN_BONUS_ID = 1
    GRID_COLUMN_INSERTED_DATETIME = 2
    GRID_INIT_TERMINAL_DATA = 3
    GRID_COLUMN_TARGET_TERMINAL_NAME = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_TRANSFERRED_RE = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_TRANSFERRED_PROMO_RE = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_TRANSFERRED_PROMO_NR = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_TRANSFERRED_TOTAL = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_TRANSFER_STATUS_TEXT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_SAS_HOST_STATUS = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_TO_TRANSFER_RE = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_TO_TRANSFER_PROMO_RE = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_TO_TRANSFER_PROMO_NR = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_TO_TRANSFER_TOTAL = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_TARGET_ACCOUNT_ID = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_PLAYSESSION_ID = TERMINAL_DATA_COLUMNS + 14

    GRID_COLUMNS_COUNT = TERMINAL_DATA_COLUMNS + 15
  End Sub

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Gets the NLS id related to the given BONUS_STATUS
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer containing the NLS id
  Public Function GetStatusNLS(ByVal Status As BONUS_STATUS) As Integer
    Select Case Status
      Case BONUS_STATUS.PENDING
        Return 4660 '4660 "Pendiente"
      Case BONUS_STATUS.CONFIRMED
        Return 4653 '4653 "Confirmado"
      Case BONUS_STATUS.IN_DOUBT
        Return 4671 '4671 "Otorgado sin confirmar"
      Case BONUS_STATUS.CANCELED
        Return 2989 '2989 "Anulado"
      Case BONUS_STATUS.WRONG_STATUS
        Return 4658 '4658 "Error"
    End Select
  End Function ' GetStatusNLS

  ' PURPOSE: Translates the bonus status stored on DB to the values shown on the GUI
  '
  '  PARAMS:
  '     - INPUT:
  '           - WcpStatus
  '           - SASHostStatus
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - BONUS_STATUS value related the the WCP & SASHost status
  Public Function TranslateWCPStatus(ByVal WcpStatus As BonusTransferStatus) As BONUS_STATUS

    Select Case WcpStatus
      Case BonusTransferStatus.Pending
        Return BONUS_STATUS.PENDING
      Case BonusTransferStatus.Notified
        Return BONUS_STATUS.PENDING
      Case BonusTransferStatus.NotifiedInDoubt
        Return BONUS_STATUS.IN_DOUBT
      Case BonusTransferStatus.Confirmed
        Return BONUS_STATUS.CONFIRMED
      Case BonusTransferStatus.CanceledTimeout, BonusTransferStatus.NoCredits, BonusTransferStatus.NonZeroRestrictedCredits, _
            BonusTransferStatus.NonZeroRestrictedCredits, BonusTransferStatus.TrackdataMismatch, BonusTransferStatus.UnknownTransactionId
        Return BONUS_STATUS.CANCELED
      Case BonusTransferStatus.Error
        Return BONUS_STATUS.WRONG_STATUS
      Case Else
        Return BONUS_STATUS.WRONG_STATUS
    End Select
  End Function

  ' PURPOSE: Translates the bonus status stored on DB to the query of Bonus transfer status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Bonus status
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String of Bonus transfer status selected
  Public Function TranslateBonusStatusQuery(ByVal BonusStatus As BONUS_STATUS) As String

    Select Case BonusStatus
      Case BONUS_STATUS.PENDING
        Return BonusTransferStatus.Pending & ", " & BonusTransferStatus.Notified
      Case BONUS_STATUS.IN_DOUBT
        Return BonusTransferStatus.NotifiedInDoubt
      Case BONUS_STATUS.CONFIRMED
        Return BonusTransferStatus.Confirmed
      Case BONUS_STATUS.CANCELED
        Return BonusTransferStatus.CanceledTimeout & ", " & BonusTransferStatus.NoCredits & ", " & BonusTransferStatus.NonZeroRestrictedCredits & ", " & _
            BonusTransferStatus.TrackdataMismatch & ", " & BonusTransferStatus.UnknownTransactionId
      Case BONUS_STATUS.WRONG_STATUS
        Return BonusTransferStatus.Error
      Case Else
        Return ""
    End Select
  End Function

#End Region ' Public Functions

#Region " Events "

  Private Sub dg_status_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_status.CellDataChangedEvent
    Dim _current_row As Integer

    If m_refreshing_grid = True Then
      Exit Sub
    End If

    With Me.dg_status
      _current_row = .CurrentRow
      m_refreshing_grid = True

      If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        Call TryToUncheckHeader(.CurrentRow)
      End If

      m_refreshing_grid = False
      .CurrentRow = _current_row
    End With
  End Sub

  Private Sub btn_uncheck_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_uncheck_all.Click
    Dim _idx_rows As Integer

    For _idx_rows = 0 To dg_status.NumRows - 1
      dg_status.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub

  Private Sub btn_check_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_check_all.Click
    Dim _idx_rows As Integer

    For _idx_rows = 0 To dg_status.NumRows - 1
      dg_status.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

End Class