<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_money_laundering_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_account = New GUI_Controls.uc_account_sel
    Me.chk_show_account_details = New System.Windows.Forms.CheckBox
    Me.gb_date_range = New System.Windows.Forms.GroupBox
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.opt_month = New System.Windows.Forms.RadioButton
    Me.opt_daily = New System.Windows.Forms.RadioButton
    Me.gb_detail = New System.Windows.Forms.GroupBox
    Me.opt_movements = New System.Windows.Forms.RadioButton
    Me.gb_limits = New System.Windows.Forms.GroupBox
    Me.chk_show_not_identified = New System.Windows.Forms.CheckBox
    Me.opt_next_to_report = New System.Windows.Forms.RadioButton
    Me.opt_report_sat = New System.Windows.Forms.RadioButton
    Me.opt_next_to_identify = New System.Windows.Forms.RadioButton
    Me.opt_identified = New System.Windows.Forms.RadioButton
    Me.cmb_group = New GUI_Controls.uc_combo
    Me.chk_total_account = New System.Windows.Forms.CheckBox
    Me.chk_show_amounts_below_limit = New System.Windows.Forms.CheckBox
    Me.lbl_color_info = New System.Windows.Forms.Label
    Me.lbl_info = New System.Windows.Forms.Label
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date_range.SuspendLayout()
    Me.gb_detail.SuspendLayout()
    Me.gb_limits.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_info)
    Me.panel_filter.Controls.Add(Me.lbl_color_info)
    Me.panel_filter.Controls.Add(Me.chk_show_amounts_below_limit)
    Me.panel_filter.Controls.Add(Me.gb_detail)
    Me.panel_filter.Controls.Add(Me.chk_total_account)
    Me.panel_filter.Controls.Add(Me.cmb_group)
    Me.panel_filter.Controls.Add(Me.gb_date_range)
    Me.panel_filter.Controls.Add(Me.uc_account)
    Me.panel_filter.Controls.Add(Me.chk_show_account_details)
    Me.panel_filter.Controls.Add(Me.gb_limits)
    Me.panel_filter.Size = New System.Drawing.Size(1174, 215)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_limits, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_account_details, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date_range, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_group, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_total_account, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_detail, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_amounts_below_limit, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_color_info, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_info, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 219)
    Me.panel_data.Size = New System.Drawing.Size(1174, 347)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1168, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1168, 4)
    '
    'uc_account
    '
    Me.uc_account.Account = ""
    Me.uc_account.AccountText = ""
    Me.uc_account.Anon = False
    Me.uc_account.AutoSize = True
    Me.uc_account.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account.BirthDate = New Date(CType(0, Long))
    Me.uc_account.DisabledHolder = False
    Me.uc_account.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account.Holder = ""
    Me.uc_account.Location = New System.Drawing.Point(664, 1)
    Me.uc_account.MassiveSearchNumbers = ""
    Me.uc_account.MassiveSearchNumbersToEdit = ""
    Me.uc_account.MassiveSearchType = 0
    Me.uc_account.Name = "uc_account"
    Me.uc_account.SearchTrackDataAsInternal = True
    Me.uc_account.ShowMassiveSearch = False
    Me.uc_account.ShowVipClients = True
    Me.uc_account.Size = New System.Drawing.Size(307, 134)
    Me.uc_account.TabIndex = 3
    Me.uc_account.Telephone = ""
    Me.uc_account.TrackData = ""
    Me.uc_account.Vip = False
    Me.uc_account.WeddingDate = New Date(CType(0, Long))
    '
    'chk_show_account_details
    '
    Me.chk_show_account_details.AutoSize = True
    Me.chk_show_account_details.Location = New System.Drawing.Point(668, 144)
    Me.chk_show_account_details.Name = "chk_show_account_details"
    Me.chk_show_account_details.Size = New System.Drawing.Size(148, 17)
    Me.chk_show_account_details.TabIndex = 6
    Me.chk_show_account_details.Text = "xShowAccountDetails"
    Me.chk_show_account_details.UseVisualStyleBackColor = True
    '
    'gb_date_range
    '
    Me.gb_date_range.Controls.Add(Me.dtp_from)
    Me.gb_date_range.Controls.Add(Me.dtp_to)
    Me.gb_date_range.Location = New System.Drawing.Point(12, 48)
    Me.gb_date_range.Name = "gb_date_range"
    Me.gb_date_range.Size = New System.Drawing.Size(281, 92)
    Me.gb_date_range.TabIndex = 1
    Me.gb_date_range.TabStop = False
    Me.gb_date_range.Text = "xDateRange"
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(21, 21)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(201, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 40
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(21, 53)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(201, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 40
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'opt_month
    '
    Me.opt_month.AutoSize = True
    Me.opt_month.Location = New System.Drawing.Point(93, 17)
    Me.opt_month.Name = "opt_month"
    Me.opt_month.Size = New System.Drawing.Size(66, 17)
    Me.opt_month.TabIndex = 1
    Me.opt_month.TabStop = True
    Me.opt_month.Text = "xMonth"
    Me.opt_month.UseVisualStyleBackColor = True
    '
    'opt_daily
    '
    Me.opt_daily.AutoSize = True
    Me.opt_daily.Location = New System.Drawing.Point(16, 17)
    Me.opt_daily.Name = "opt_daily"
    Me.opt_daily.Size = New System.Drawing.Size(61, 17)
    Me.opt_daily.TabIndex = 0
    Me.opt_daily.TabStop = True
    Me.opt_daily.Text = "xDaily"
    Me.opt_daily.UseVisualStyleBackColor = True
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.opt_movements)
    Me.gb_detail.Controls.Add(Me.opt_month)
    Me.gb_detail.Controls.Add(Me.opt_daily)
    Me.gb_detail.Location = New System.Drawing.Point(12, 4)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Size = New System.Drawing.Size(281, 39)
    Me.gb_detail.TabIndex = 0
    Me.gb_detail.TabStop = False
    Me.gb_detail.Text = "xDetail"
    '
    'opt_movements
    '
    Me.opt_movements.AutoSize = True
    Me.opt_movements.Location = New System.Drawing.Point(177, 16)
    Me.opt_movements.Name = "opt_movements"
    Me.opt_movements.Size = New System.Drawing.Size(91, 17)
    Me.opt_movements.TabIndex = 2
    Me.opt_movements.TabStop = True
    Me.opt_movements.Text = "xMovement"
    Me.opt_movements.UseVisualStyleBackColor = True
    '
    'gb_limits
    '
    Me.gb_limits.Controls.Add(Me.chk_show_not_identified)
    Me.gb_limits.Controls.Add(Me.opt_next_to_report)
    Me.gb_limits.Controls.Add(Me.opt_report_sat)
    Me.gb_limits.Controls.Add(Me.opt_next_to_identify)
    Me.gb_limits.Controls.Add(Me.opt_identified)
    Me.gb_limits.Location = New System.Drawing.Point(299, 4)
    Me.gb_limits.Name = "gb_limits"
    Me.gb_limits.Size = New System.Drawing.Size(359, 136)
    Me.gb_limits.TabIndex = 2
    Me.gb_limits.TabStop = False
    Me.gb_limits.Text = "xUserFilter"
    '
    'chk_show_not_identified
    '
    Me.chk_show_not_identified.AutoSize = True
    Me.chk_show_not_identified.Location = New System.Drawing.Point(40, 42)
    Me.chk_show_not_identified.Name = "chk_show_not_identified"
    Me.chk_show_not_identified.Size = New System.Drawing.Size(201, 17)
    Me.chk_show_not_identified.TabIndex = 1
    Me.chk_show_not_identified.Text = "xMostrar el titular de la cuenta"
    Me.chk_show_not_identified.UseVisualStyleBackColor = True
    '
    'opt_next_to_report
    '
    Me.opt_next_to_report.AutoSize = True
    Me.opt_next_to_report.Location = New System.Drawing.Point(8, 89)
    Me.opt_next_to_report.Name = "opt_next_to_report"
    Me.opt_next_to_report.Size = New System.Drawing.Size(147, 17)
    Me.opt_next_to_report.TabIndex = 3
    Me.opt_next_to_report.TabStop = True
    Me.opt_next_to_report.Text = "xPr�ximos a reportar"
    Me.opt_next_to_report.UseVisualStyleBackColor = True
    '
    'opt_report_sat
    '
    Me.opt_report_sat.AutoSize = True
    Me.opt_report_sat.Location = New System.Drawing.Point(8, 114)
    Me.opt_report_sat.Name = "opt_report_sat"
    Me.opt_report_sat.Size = New System.Drawing.Size(132, 17)
    Me.opt_report_sat.TabIndex = 4
    Me.opt_report_sat.TabStop = True
    Me.opt_report_sat.Text = "xA reportar al SAT"
    Me.opt_report_sat.UseVisualStyleBackColor = True
    '
    'opt_next_to_identify
    '
    Me.opt_next_to_identify.AutoSize = True
    Me.opt_next_to_identify.Location = New System.Drawing.Point(8, 16)
    Me.opt_next_to_identify.Name = "opt_next_to_identify"
    Me.opt_next_to_identify.Size = New System.Drawing.Size(156, 17)
    Me.opt_next_to_identify.TabIndex = 0
    Me.opt_next_to_identify.TabStop = True
    Me.opt_next_to_identify.Text = "xProximos a identificar"
    Me.opt_next_to_identify.UseVisualStyleBackColor = True
    '
    'opt_identified
    '
    Me.opt_identified.AutoSize = True
    Me.opt_identified.Location = New System.Drawing.Point(8, 65)
    Me.opt_identified.Name = "opt_identified"
    Me.opt_identified.Size = New System.Drawing.Size(105, 17)
    Me.opt_identified.TabIndex = 2
    Me.opt_identified.TabStop = True
    Me.opt_identified.Text = "xIdentificados"
    Me.opt_identified.UseVisualStyleBackColor = True
    '
    'cmb_group
    '
    Me.cmb_group.AllowUnlistedValues = False
    Me.cmb_group.AutoCompleteMode = False
    Me.cmb_group.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
    Me.cmb_group.IsReadOnly = False
    Me.cmb_group.Location = New System.Drawing.Point(-15, 152)
    Me.cmb_group.Name = "cmb_group"
    Me.cmb_group.SelectedIndex = -1
    Me.cmb_group.Size = New System.Drawing.Size(463, 24)
    Me.cmb_group.SufixText = "Sufix Text"
    Me.cmb_group.SufixTextVisible = True
    Me.cmb_group.TabIndex = 4
    Me.cmb_group.TextWidth = 110
    '
    'chk_total_account
    '
    Me.chk_total_account.AutoSize = True
    Me.chk_total_account.Location = New System.Drawing.Point(97, 183)
    Me.chk_total_account.Name = "chk_total_account"
    Me.chk_total_account.Size = New System.Drawing.Size(152, 17)
    Me.chk_total_account.TabIndex = 5
    Me.chk_total_account.Text = "xShowTotalByAccount"
    Me.chk_total_account.UseVisualStyleBackColor = True
    '
    'chk_show_amounts_below_limit
    '
    Me.chk_show_amounts_below_limit.AutoSize = True
    Me.chk_show_amounts_below_limit.Location = New System.Drawing.Point(668, 167)
    Me.chk_show_amounts_below_limit.Name = "chk_show_amounts_below_limit"
    Me.chk_show_amounts_below_limit.Size = New System.Drawing.Size(169, 17)
    Me.chk_show_amounts_below_limit.TabIndex = 7
    Me.chk_show_amounts_below_limit.Text = "xHideAmountsUnderLimit"
    Me.chk_show_amounts_below_limit.UseVisualStyleBackColor = True
    '
    'lbl_color_info
    '
    Me.lbl_color_info.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_info.Location = New System.Drawing.Point(687, 196)
    Me.lbl_color_info.Name = "lbl_color_info"
    Me.lbl_color_info.Size = New System.Drawing.Size(13, 12)
    Me.lbl_color_info.TabIndex = 8
    '
    'lbl_info
    '
    Me.lbl_info.AutoSize = True
    Me.lbl_info.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_info.Location = New System.Drawing.Point(704, 192)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.Size = New System.Drawing.Size(87, 13)
    Me.lbl_info.TabIndex = 9
    Me.lbl_info.Text = "xInfoAmounts"
    '
    'frm_money_laundering_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1182, 570)
    Me.Name = "frm_money_laundering_report"
    Me.Text = "frm_money_laundering_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date_range.ResumeLayout(False)
    Me.gb_detail.ResumeLayout(False)
    Me.gb_detail.PerformLayout()
    Me.gb_limits.ResumeLayout(False)
    Me.gb_limits.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account As GUI_Controls.uc_account_sel
  Friend WithEvents chk_show_account_details As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date_range As System.Windows.Forms.GroupBox
  Protected WithEvents dtp_from As GUI_Controls.uc_date_picker
  Protected WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents opt_month As System.Windows.Forms.RadioButton
  Friend WithEvents opt_daily As System.Windows.Forms.RadioButton
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Friend WithEvents gb_limits As System.Windows.Forms.GroupBox
  Friend WithEvents opt_report_sat As System.Windows.Forms.RadioButton
  Friend WithEvents opt_next_to_identify As System.Windows.Forms.RadioButton
  Friend WithEvents opt_identified As System.Windows.Forms.RadioButton
  Friend WithEvents opt_next_to_report As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_not_identified As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_group As GUI_Controls.uc_combo
  Friend WithEvents chk_total_account As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_amounts_below_limit As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_info As System.Windows.Forms.Label
  Friend WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents opt_movements As System.Windows.Forms.RadioButton
End Class
