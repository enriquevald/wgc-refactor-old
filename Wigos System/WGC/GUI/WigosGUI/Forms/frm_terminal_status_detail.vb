'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_status_detail.vb
' DESCRIPTION:   Terminal Status Detail
' AUTHOR:        Jaume Barn�s
' CREATION DATE: 14-JAN-2014
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 14-JAN-2014 JBC    Initial version
' 20-JAN-2014 AMF    Changes
' 20-JAN-2014 JBC    Now audited buttons
' 08-APR-2014 DHA    Added validation software
' 15-APR-2014 JBC    Added flowlayoutpanel to resize form
' 22-APR-2014 JBC    Added New flags.
' 29-APR-2014 AMF    Added Machine Status.
' 26-MAY-2014 JBC    Fixed Bug WIG-911: Added Payout ACK button
' 12-FEB-2015 FOS    Added WCP and WC2 server Name
' 10-APR-2015 FOS    Fixwed Bug WIG-2200: Protected DBNull error
' 17-APR-2015 FOS    TASK 975: Changes from review
' 20-OCT-2015 GMV    TFS 5277 : HighRollers
' 11-NOV-2015 GMV    TFS 5277 : HighRollers Extended Definition
' 29-FEB-2016 RAB    Bug 9356:En la pantalla de Detalle de Estado de terminales, el texto en jugado/ganado es ilegible
'--------------------------------------------------------------------


#Region " Imports "

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports WSI.Common.TITO

#End Region

Public Class frm_terminal_status_detail
  Inherits frm_base_edit

#Region "Constants"
  Private Const FORM_DATABASE_MIN_VERISON As Short = 203

  'Button ID
  Private Const BTN_SLOT_DOOR As Integer = 1
  Private Const BTN_DROP_DOOR As Integer = 2
  Private Const BTN_CARD_CAGE As Integer = 3
  Private Const BTN_BELLY_DOOR As Integer = 4
  Private Const BTN_CASHBOX_DOOR As Integer = 5
  Private Const BTN_PLAYS As Integer = 6
  Private Const BTN_CANCELED_CREDITS As Integer = 7
  Private Const BTN_CMOS_RAM_ERROR_DATA_RECOVERED As Integer = 8
  Private Const BTN_CMOS_RAM_ERROR_NO_DATA_RECOVERED As Integer = 9
  Private Const BTN_CMOS_RAM_ERROR_BAD_DEVICE As Integer = 10
  Private Const BTN_CMOS_RAM_ERROR_DATA_ERROR As Integer = 11
  Private Const BTN_BILL_JAM As Integer = 12
  Private Const BTN_BILL_ACCEPTOR_HARDWARE_FAILURE As Integer = 13
  Private Const BTN_EXCESS_COUNTERFEITS As Integer = 14
  Private Const BTN_PRINTER_COMMUNITACION_ERROR As Integer = 15
  Private Const BTN_PRINTER_PAPER_OUT_ERROR As Integer = 16
  Private Const BTN_REPLACE_PRINTER_RIBBON As Integer = 17
  Private Const BTN_PRINTER_PAPER_LOW As Integer = 18
  Private Const BTN_PRINTER_CARRIAGE_JAMMED As Integer = 19
  Private Const BTN_PRINTER_POWER_ON As Integer = 20
  Private Const BTN_JACKPOT As Integer = 21
  Private Const BTN_WON As Integer = 22
  Private Const BTN_EPROM_ERROR_BAD_DEVICE As Integer = 23
  Private Const BTN_EPROM_ERROR_DIFFERENT_CHECKSUM_VERSION_CHANGED As Integer = 24
  Private Const BTN_EPROM_ERROR_BAD_CHECKSUM_COMPARE As Integer = 25
  Private Const BTN_PARTITIONED_EPROM_ERROR_CHECKSUM_VERSION_CHANGED As Integer = 26
  Private Const BTN_PARTITIONED_EPROM_ERROR_BAD_CHECKSUM_COMPARE As Integer = 27
  Private Const BTN_LOW_BACKUP_BATTERY_DETECTED As Integer = 28
  Private Const BTN_CALL_ATTENDANT As Integer = 29
  Private Const BTN_MACHINE As Integer = 30
  Private Const BTN_PAYOUT As Integer = 31
  Private Const BTN_MANUALLY As Integer = 32
  Private Const BTN_WHITOUT_PLAYS As Integer = 33
  'JML 16-SEP-2015
  Private Const BTN_COIN_IN_TILT As Integer = 34
  Private Const BTN_REVERSE_COIN_IN As Integer = 35
  Private Const BTN_COIN_IN_LOCKOUT_MALFUNCTION As Integer = 36
  'GMV 23-OCT-2015
  Private Const BTN_HIGHROLLER As Integer = 37
  'GMV 11-NOV-2015
  Private Const BTN_HIGHROLLER_ANONYMOUS As Integer = 38
  Private Const BTN_INTRUSION As Integer = 39

  ' Session Status
  Private Const TERM_STATUS_FREE = 1
  Private Const TERM_STATUS_BUSY = 2

  ' WCP & WC2 status
  Private Const TERM_STATUS_DISCONNECTED = 0
  Private Const TERM_STATUS_CONNECTED = 1

  Private Const PLAY_SESSION_STATUS_OPEN = 0

  ' Time since last message to consider a terminal as stopped
  Private Const ACTIVITY_TIMEOUT = 180

  Private Const TIMER_INTERVAL = 5000

  'SQL COLUMNS
  Private Const SQL_TE_NAME As Integer = 0
  Private Const SQL_TE_TERMINAL_TYPE As Integer = 1
  Private Const SQL_TE_PROVIDER_ID As Integer = 2
  Private Const SQL_SESSION_STATE As Integer = 3
  Private Const SQL_WCP_STATE As Integer = 4
  Private Const SQL_WC2_STATE As Integer = 5
  Private Const SQL_TE_TERMINAL_ID As Integer = 6
  Private Const SQL_TS_BILL_FLAGS As Integer = 7

  'JML 16-SEP-2015
  Private Const SQL_TS_COIN_FLAGS As Integer = 8

  Private Const SQL_TS_EGM_FLAGS As Integer = 9
  Private Const SQL_TS_PLAYED_WON_FLAGS As Integer = 10
  Private Const SQL_TS_JACKPOT_FLAGS As Integer = 11
  Private Const SQL_TS_DOOR_FLAGS As Integer = 12
  Private Const SQL_TS_STACKER_COUNTER As Integer = 13
  Private Const SQL_TS_SAS_HOST_ERROR As Integer = 14
  Private Const SQL_CURRENT_PAYOUT As Integer = 15
  Private Const SQL_TS_PRINTER_FLAGS As Integer = 16
  Private Const SQL_TS_STACKER_STATUS As Integer = 17
  Private Const SQL_CALL_ATTENDANT_FLAGS As Integer = 18
  'FOS 12-FEB-2015 
  Private Const WS_SERVER_NAME As Integer = 19
  Private Const W2S_SERVER_NAME As Integer = 20

  Private Const SQL_AUTHENTICATION_STATUS As Integer = 21
  Private Const SQL_AUTHENTICATION_LAST_CHECKED As Integer = 22
  Private Const SQL_TS_MACHINE_FLAGS As Integer = 23


  Private Const SQL_ALARM_PLAYED As Integer = 0
  Private Const SQL_ALARM_WON As Integer = 1
  Private Const SQL_ALARM_JACKPOT As Integer = 2
  Private Const SQL_ALARM_CANCELED_CREDITS As Integer = 3
  Private Const SQL_ALARM_COUNTERFEITS As Integer = 4
  Private Const SQL_ALARM_WITHOUT_PLAYS As Integer = 5
  Private Const SQL_ALARM_HIGHROLLER As Integer = 6
  Private Const SQL_ALARM_HIGHROLLER_ANONYMOUS As Integer = 7
  Private Const SQL_ALARM_PAYOUT As Integer = 8

#End Region

#Region "Enums "

  Private Enum GROUP_FLAG
    DOOR_FLAG
    BILL_FLAG
    EGM_FLAG
    PRINTER_FLAG
    PLAYS_FLAG
    JACKPOT_FLAG
  End Enum

#End Region ' Enums

#Region "Members"

  Private m_terminal_id As Integer
  Private m_flags_doors As Integer
  Private m_flags_played_won As Integer
  Private m_flags_EGM As Integer
  Private m_flags_bills As Integer
  Private m_flags_coins As Integer
  Private m_flags_printers As Integer
  Private m_flags_jackpot As Integer
  Private m_flags_machine As Integer
  Private m_flags_call As Boolean
  Private m_plays_per_second As Decimal
  Private m_cents_per_second As Decimal
  Private m_won_amount_excedeed As Decimal
  Private m_str_button_audit As String

  Private m_first_time As Boolean

  Private WithEvents m_timer As Timer

  Private m_system_mode As SYSTEM_MODE
  Private m_NLS_list As Dictionary(Of String, String)

#End Region ' Members

#Region "Overrides"

  'PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _size As New Size

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4485)

    m_system_mode = WSI.Common.Misc.SystemMode()

    m_timer = New Timer()

    m_timer.Interval = TIMER_INTERVAL

    m_str_button_audit = String.Empty

    m_first_time = True

    Me.btn_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_5.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_6.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_7.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_8.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_9.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_10.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_11.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_12.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_13.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_14.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_15.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_16.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_17.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_18.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_19.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_20.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_21.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_22.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_23.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_24.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_25.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_26.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_27.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_28.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_29.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_30.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_31.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_32.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_33.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_34.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_35.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_36.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_37.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_38.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)
    Me.btn_39.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4484)

    'Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    'TabControl
    '    Me.tab_now.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486)

    'GroupBox Service
    Me.gb_service.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4487)
    Me.lbl_WCP_WC2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4488)
    Me.lbl_SAS_HOST.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4489)
    'FOS 12-FEB-2015
    Me.lbl_WCP_Name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5894) & ":"
    Me.lbl_WC2_Name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5895) & ":"
    Me.lbl_WCP_ServerName.Text = AUDIT_NONE_STRING
    Me.lbl_WC2_ServerName.Text = AUDIT_NONE_STRING


    'GroupBox Doors
    Me.gb_doors.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4490)
    Me.lbl_belly_door.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4491)
    Me.lbl_slot_door.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4492)
    Me.lbl_card_cage.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4495)
    Me.lbl_cashbox_door.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4494)
    Me.lbl_drop_door.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4493)

    'GroupBox Plays
    Me.gb_plays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4496)
    Me.lbl_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
    Me.lbl_current_payout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
    Me.lbl_current_payout_value.Text = AUDIT_NONE_STRING
    Me.lbl_wonamount_exceeded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
    Me.lbl_wonamount_alarms_message.Text = AUDIT_NONE_STRING
    Me.lbl_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4525)
    Me.lbl_jackpot_alarm_message.Text = AUDIT_NONE_STRING
    Me.lbl_wonamount_alarms_message.Text = AUDIT_NONE_STRING
    Me.lbl_played_alarms_message.Text = AUDIT_NONE_STRING
    Me.lbl_c_credits_alarm_message.Text = AUDIT_NONE_STRING
    Me.lbl_counterfeits_alarm_message.Text = AUDIT_NONE_STRING
    Me.lbl_c_credits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4851)
    Me.lbl_without_plays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4853)
    Me.lbl_without_plays_value.Text = AUDIT_NONE_STRING
    Me.lbl_highroller.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892)
    Me.lbl_highroller_value.Text = AUDIT_NONE_STRING
    Me.lbl_highroller_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
    Me.lbl_highroller_anonymous_value.Text = AUDIT_NONE_STRING


    'GroupBox CPU
    Me.gb_egm.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4501)
    Me.lbl_cmos_error_data_recovered.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4049)
    Me.lbl_cmos_error_no_data_recovered.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4050)
    Me.lbl_cmos_ram_error_bad_device.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4051)
    Me.lbl_eprom_error_data_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4052)
    Me.lbl_eprom_error_bad_device.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4053)
    Me.lbl_eprom_error_different_checksum_version_changed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4054)
    Me.lbl_eprom_error_bad_checksum_compare.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4055)
    Me.lbl_partitioned_eprom_error_checksum_version_changed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4056)
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4057)
    Me.lbl_backup_battery_detected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4059)

    'GroupBox Bills
    Me.gb_bills.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4514)
    Me.lbl_bill_acceptor_hardware_failure.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4516)
    Me.lbl_bill_jam.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4515)
    Me.lbl_excess_counterfeits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843)
    Me.lbl_stacker_full.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4502)
    Me.lbl_stacker_counter.Text = AUDIT_NONE_STRING
    Me.lbl_stacker_counter_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4503)

    'GroupBox Coins
    Me.gb_coins.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6716)
    Me.lbl_coin_in_tilt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6717)
    Me.lbl_reverse_coin_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6718)
    Me.lbl_coin_in_lockout_malfunction.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6719)

    'GroupBox Printers
    Me.gb_printers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4518)
    Me.lbl_printer_carriage_jammed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4521)
    Me.lbl_printer_communication_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4519)
    Me.lbl_printer_paper_low.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4520)
    Me.lbl_printer_paper_out_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4522)
    Me.lbl_printer_power_on.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4523)
    Me.lbl_replace_printer_ribbon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4524)

    'GroupBox Call Attendant
    Me.gb_call_attendant.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4541)
    Me.lbl_call_attendant.Text = GLB_NLS_GUI_ALARMS.GetString(361)

    'GroupBox Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4527)
    Me.tf_error.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4516)
    Me.tf_warning.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4528)

    'Groupbox HandPays
    Me.gb_handpays.Text = Resource.String("STR_UC_BANK_HANDPAYS")

    'Terminal Info
    Me.ef_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4529)
    Me.ef_terminal_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4530)
    Me.ef_terminal_session.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)
    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.ef_terminal_name.IsReadOnly = True
    Me.ef_terminal_provider.IsReadOnly = True
    Me.ef_terminal_session.IsReadOnly = True

    ' DHA 08-APR-2014
    Me.gb_machine_status.Visible = False
    Me.gb_software_validation.Visible = False

    Me.gb_bills.Visible = True


    'TODO JCA visible el grupbox
    'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
    Me.gb_machine_status.Visible = True
    Me.gb_machine_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4912)
    Me.lbl_locked_by_intrusion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7115)

    If m_system_mode = SYSTEM_MODE.WASS Then

      Me.gb_machine_status.Size = New Size(432, 87)

      Me.lbl_locked_by_signature.Visible = True
      Me.lbl_locked_by_manually.Visible = True
      Me.btn_30.Visible = True
      Me.btn_32.Visible = True

      Me.lbl_locked_by_signature.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4813)
      Me.lbl_locked_by_manually.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4913)

      Me.gb_software_validation.Visible = True
      Me.gb_software_validation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4812)
      Me.lbl_authentication_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4914)
      Me.lbl_authentication_last_checked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4915)

    End If

    Call setDefaultValues()
    Call SetNLSValuesFromDB()

    m_timer.Start()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_STATUS_DETAIL

    Call GUI_SetMinDbVersion(FORM_DATABASE_MIN_VERISON)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Call setScreenTerminalInfo()
    Call UpdateScreenData()
    Call setDefaultValues()

  End Sub ' GUI_SetScreenData

  ' PURPOSE: First Activation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_FirstActivation()

    Call GUI_SetScreenData(0)

  End Sub ' GUI_FirstActivation

  ' PURPOSE: Exit form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Exit()
    Dim _auditor_data As CLASS_AUDITOR_DATA

    m_timer.Stop()

    Call setScreenTerminalInfo()
    Call UpdateScreenData()
    Call setDefaultValues()

    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    If m_str_button_audit <> String.Empty Then
      _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
      _auditor_data.SetName(0, GLB_NLS_GUI_SW_DOWNLOAD.GetString(213) & " - " & GLB_NLS_GUI_STATISTICS.GetString(377) & " - " & ef_terminal_name.Value)

      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(4484), m_str_button_audit.Substring(2))

      If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                           GLB_CurrentUser.Id, _
                           GLB_CurrentUser.Name, _
                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                           0) Then
        ' Logger message 
      End If

      _auditor_data = Nothing
    End If

    Me.Close()

    MyBase.GUI_Exit()

  End Sub ' GUI_Exit

  ' PURPOSE: Dispose form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub Finalize()

    MyBase.Finalize()

  End Sub ' Finalize

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Set members default values 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub setDefaultValues()

    m_flags_doors = 0
    m_flags_played_won = 0
    m_flags_EGM = 0
    m_flags_bills = 0
    m_flags_coins = 0
    m_flags_printers = 0
    m_flags_jackpot = 0
    m_flags_machine = 0

  End Sub ' setDefaultValues

  ' PURPOSE: returns red/transparent backcolor, also enable/disable Validate button.
  '
  '  PARAMS:
  '     - INPUT:  Enabled Boolean
  '               ValidateButton  Button
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '               Color
  Private Function getEnabledColor(ByVal Enabled As Boolean, ByVal IsWarning As Boolean, Optional ByRef ValidateButton As Button = Nothing) As Color
    Dim _color As Color

    If Enabled Then
      If IsWarning Then
        _color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Else
        _color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      End If
    Else
      _color = Color.Transparent
    End If

    If Not IsNothing(ValidateButton) Then
      ValidateButton.Enabled = Enabled
    End If

    Return _color

  End Function ' getEnabledColor

  ' PURPOSE: Update database bits and return updated info.
  '
  '  PARAMS:
  '     - INPUT:  Enabled Boolean
  '               ValidateButton  Button
  '
  ' RETURNS:
  '               DataRow
  Private Function getDataRowFromTerminalStatus() As DataSet
    Dim _str_sql As StringBuilder
    Dim _data_table As DataTable
    Dim _sql_command As SqlCommand
    Dim _dataset As DataSet

    Try
      _dataset = New DataSet()

      Using _db_trx As WSI.Common.DB_TRX = New WSI.Common.DB_TRX()

        _str_sql = New StringBuilder()

        _str_sql.AppendLine("         UPDATE    TERMINAL_STATUS ")
        _str_sql.AppendLine("            SET    TS_EGM_FLAGS             = TS_EGM_FLAGS & ~@pFlagsEGM")
        _str_sql.AppendLine(",                  TS_BILL_FLAGS            = TS_BILL_FLAGS & ~@pFlagsBills")
        _str_sql.AppendLine(",                  TS_COIN_FLAGS            = TS_COIN_FLAGS & ~@pFlagsCoins")
        _str_sql.AppendLine(",                  TS_PLAYED_WON_FLAGS      = TS_PLAYED_WON_FLAGS & ~@pFlagsPlays")
        _str_sql.AppendLine(",                  TS_JACKPOT_FLAGS         = TS_JACKPOT_FLAGS & ~@pFlagsJackpot")
        _str_sql.AppendLine(",                  TS_DOOR_FLAGS            = TS_DOOR_FLAGS & ~@pFlagsDoor")
        _str_sql.AppendLine(",                  TS_PRINTER_FLAGS         = TS_PRINTER_FLAGS & ~@pFlagsPrinter")

        'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
        'If m_system_mode = SYSTEM_MODE.WASS Then
        _str_sql.AppendLine(",                  TS_MACHINE_FLAGS         = TS_MACHINE_FLAGS & ~@pFlagsMachine")
        'End If

        If Not m_first_time Then
          _str_sql.AppendLine(",                  TS_CALL_ATTENDANT_FLAGS  = @pFlagsCall")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.PLAYED) Then
          _str_sql.AppendLine(",                    TS_PLAYED_ALARM_ID = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.WON) Then
          _str_sql.AppendLine(",                  TS_WON_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_jackpot, TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS) Then
          _str_sql.AppendLine(",                  TS_JACKPOT_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_jackpot, TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT) Then
          _str_sql.AppendLine(",                  TS_CANCELED_CREDIT_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_bills, TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED) Then
          _str_sql.AppendLine(",                  TS_COUNTERFEIT_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.WITHOUT_PLAYS) Then
          _str_sql.AppendLine(",                  TS_WITHOUT_PLAYS_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER) Then
          _str_sql.AppendLine(",                  TS_HIGHROLLER_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS) Then
          _str_sql.AppendLine(",                  TS_HIGHROLLER_ANONYMOUS_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT) Then
          _str_sql.AppendLine(",                  TS_CURRENT_PAYOUT_ALARM_ID  = NULL")
        End If

        If TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT) Then
          _str_sql.AppendLine(",                  TS_CURRENT_PAYOUT  = 0")
        End If

        _str_sql.AppendLine("          WHERE    TS_TERMINAL_ID           = @pTerminalID   ")

        _sql_command = New SqlClient.SqlCommand(_str_sql.ToString())

        _sql_command.Transaction = _db_trx.SqlTransaction
        _sql_command.Connection = _db_trx.SqlTransaction.Connection

        _sql_command.Parameters.Add("@pFlagsDoor", SqlDbType.BigInt).Value = m_flags_doors
        _sql_command.Parameters.Add("@pFlagsPlays", SqlDbType.BigInt).Value = m_flags_played_won
        _sql_command.Parameters.Add("@pFlagsEGM", SqlDbType.BigInt).Value = m_flags_EGM
        _sql_command.Parameters.Add("@pFlagsBills", SqlDbType.BigInt).Value = m_flags_bills
        _sql_command.Parameters.Add("@pFlagsCoins", SqlDbType.BigInt).Value = m_flags_coins
        _sql_command.Parameters.Add("@pFlagsPrinter", SqlDbType.BigInt).Value = m_flags_printers
        _sql_command.Parameters.Add("@pFlagsJackpot", SqlDbType.BigInt).Value = m_flags_jackpot

        'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
        ''If m_system_mode = SYSTEM_MODE.WASS Then
        _sql_command.Parameters.Add("@pFlagsMachine", SqlDbType.BigInt).Value = m_flags_machine
        ''End If
        If Not m_first_time Then
          _sql_command.Parameters.Add("@pFlagsCall", SqlDbType.Bit).Value = m_flags_call
        End If
        _sql_command.Parameters.Add("@pTerminalID", SqlDbType.BigInt).Value = m_terminal_id

        _sql_command.ExecuteNonQuery()

        _str_sql = New StringBuilder()

        _str_sql.AppendLine("         SELECT    TE_NAME , TE_TERMINAL_TYPE , TE_PROVIDER_ID , ")
        _str_sql.AppendLine("                   SESSION_STATE = ")
        _str_sql.AppendLine("                     CASE  WHEN (PS_PLAY_SESSION_ID IS NOT NULL) THEN @pTermStatusBusy ")
        _str_sql.AppendLine("                     WHEN (PS_PLAY_SESSION_ID IS NULL) THEN @pTermStatusFree  ELSE 0  END ")
        _str_sql.AppendLine(",                  WCP_STATE = CASE  WHEN (DATEDIFF(SECOND,WS_LAST_RCVD_MSG,GETDATE()) <= @pActivityTimeout) THEN @pTermStatusConnected  ELSE @pTermStatusDisconnected  END ")
        _str_sql.AppendLine(",                  WC2_STATE = CASE  WHEN (DATEDIFF(SECOND,W2S_LAST_RCVD_MSG,GETDATE()) <= @pActivityTimeout) THEN @pTermStatusConnected  ELSE @pTermStatusDisconnected  END ")
        _str_sql.AppendLine(",                  TE_TERMINAL_ID, TS_BILL_FLAGS, TS_COIN_FLAGS,  TS_EGM_FLAGS, TS_PLAYED_WON_FLAGS, TS_JACKPOT_FLAGS, TS_DOOR_FLAGS ")
        _str_sql.AppendLine(",                  TS_STACKER_COUNTER, TS_SAS_HOST_ERROR,TS_CURRENT_PAYOUT, TS_PRINTER_FLAGS ")
        _str_sql.AppendLine(",                  TS_STACKER_STATUS, TS_CALL_ATTENDANT_FLAGS ")
        ' FOS 12-FEB-2015
        _str_sql.AppendLine(",                  WS_SERVER_NAME, W2S_SERVER_NAME ")
        ' DHA 08-APR-2014
        'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
        'If m_system_mode = SYSTEM_MODE.WASS Then
        _str_sql.AppendLine(",                TE_AUTHENTICATION_STATUS, TE_AUTHENTICATION_LAST_CHECKED, TS_MACHINE_FLAGS ")
        'End If
        _str_sql.AppendLine("           FROM    TERMINALS ")
        _str_sql.AppendLine("LEFT OUTER JOIN    (")
        _str_sql.AppendLine("         SELECT    PS_TERMINAL_ID, MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID   ")
        _str_sql.AppendLine("           FROM    PLAY_SESSIONS")
        _str_sql.AppendLine("          WHERE    PS_STATUS = @pPlaySessionStatusOpen")
        _str_sql.AppendLine("       GROUP BY    PS_TERMINAL_ID) LAST_PLAY_SESSIONS ON TE_TERMINAL_ID = PS_TERMINAL_ID")
        _str_sql.AppendLine("LEFT OUTER JOIN    (")
        _str_sql.AppendLine("         SELECT    WS_TERMINAL_ID, MAX(WS_LAST_RCVD_MSG) AS WS_LAST_RCVD_MSG ,WS_SERVER_NAME  ")
        _str_sql.AppendLine("           FROM    WCP_SESSIONS  WITH(INDEX(IX_wcp_status)) ")
        _str_sql.AppendLine("          WHERE    WS_STATUS = @pPlaySessionStatusOpen ")
        _str_sql.AppendLine("       GROUP BY    WS_TERMINAL_ID,WS_SERVER_NAME) LAST_WCP_SESSIONS ON TE_TERMINAL_ID = WS_TERMINAL_ID ")
        _str_sql.AppendLine("LEFT OUTER JOIN    (")
        _str_sql.AppendLine("         SELECT    W2S_TERMINAL_ID, MAX(W2S_LAST_RCVD_MSG) AS W2S_LAST_RCVD_MSG ,W2S_SERVER_NAME   ")
        _str_sql.AppendLine("           FROM    WC2_SESSIONS   WITH(INDEX(IX_wc2_status))")
        _str_sql.AppendLine("          WHERE    W2S_STATUS = @pPlaySessionStatusOpen")
        _str_sql.AppendLine("       GROUP BY    W2S_TERMINAL_ID,W2S_SERVER_NAME ) LAST_WC2_SESSIONS ON TE_TERMINAL_ID = W2S_TERMINAL_ID ")
        _str_sql.AppendLine("LEFT OUTER JOIN    TERMINAL_STATUS ON TE_TERMINAL_ID = TS_TERMINAL_ID  ")
        _str_sql.AppendLine("          WHERE    TE_TERMINAL_ID = @pTerminalID")
        _str_sql.AppendLine("       ORDER BY    TE_NAME, WCP_STATE,WC2_STATE, SESSION_STATE ")

        _sql_command = New SqlClient.SqlCommand(_str_sql.ToString())

        _sql_command.Transaction = _db_trx.SqlTransaction
        _sql_command.Connection = _db_trx.SqlTransaction.Connection

        _sql_command.Parameters.Add("@pTermStatusBusy", SqlDbType.BigInt).Value = TERM_STATUS_BUSY
        _sql_command.Parameters.Add("@pTermStatusFree", SqlDbType.BigInt).Value = TERM_STATUS_FREE
        _sql_command.Parameters.Add("@pActivityTimeout", SqlDbType.BigInt).Value = ACTIVITY_TIMEOUT
        _sql_command.Parameters.Add("@pTermStatusConnected", SqlDbType.BigInt).Value = TERM_STATUS_CONNECTED
        _sql_command.Parameters.Add("@pTermStatusDisconnected", SqlDbType.BigInt).Value = TERM_STATUS_DISCONNECTED
        _sql_command.Parameters.Add("@pPlaySessionStatusOpen", SqlDbType.BigInt).Value = PLAY_SESSION_STATUS_OPEN
        _sql_command.Parameters.Add("@pTerminalID", SqlDbType.BigInt).Value = m_terminal_id

        _dataset = New DataSet()
        _data_table = New DataTable("Flags")

        Using _sql_da As New SqlClient.SqlDataAdapter(_sql_command)
          _sql_da.Fill(_data_table)
        End Using

        _dataset.Tables.Add(_data_table)

        _data_table = New DataTable("Alarms")
        _str_sql = New StringBuilder()

        _str_sql.AppendLine("        SELECT   COLUMN_NAME,ALARMS.AL_ALARM_DESCRIPTION                                     ")
        _str_sql.AppendLine("          FROM                                                                               ")
        _str_sql.AppendLine("             (                                                                               ")
        _str_sql.AppendLine("        SELECT   ISNULL(T.TS_PLAYED_ALARM_ID,'')             AS TS_PLAYED_ALARM_ID,          ")
        _str_sql.AppendLine("                 ISNULL(T.TS_WON_ALARM_ID,'')                AS TS_WON_ALARM_ID   ,          ")
        _str_sql.AppendLine("                 ISNULL(T.TS_JACKPOT_ALARM_ID,'')            AS TS_JACKPOT_ALARM_ID,         ")
        _str_sql.AppendLine("                 ISNULL(T.TS_CANCELED_CREDIT_ALARM_ID,'')    AS TS_CANCELED_CREDIT_ALARM_ID, ")
        _str_sql.AppendLine("                 ISNULL(T.TS_COUNTERFEIT_ALARM_ID,'')        AS TS_COUNTERFEIT_ALARM_ID,     ")
        _str_sql.AppendLine("                 ISNULL(T.TS_WITHOUT_PLAYS_ALARM_ID,'')      AS TS_WITHOUT_PLAYS_ALARM_ID,   ")
        _str_sql.AppendLine("                 ISNULL(T.TS_HIGHROLLER_ALARM_ID,'')         AS TS_HIGHROLLER_ALARM_ID,      ")
        _str_sql.AppendLine("                 ISNULL(T.TS_HIGHROLLER_ANONYMOUS_ALARM_ID,'')         AS TS_HIGHROLLER_ANONYMOUS_ALARM_ID,      ")
        _str_sql.AppendLine("                 ISNULL(T.TS_CURRENT_PAYOUT_ALARM_ID,'')     AS TS_CURRENT_PAYOUT_ALARM_ID   ")
        _str_sql.AppendLine("          FROM   TERMINAL_STATUS T                                                           ")
        _str_sql.AppendLine("         WHERE   TS_TERMINAL_ID = @pTerminalID                                               ")
        _str_sql.AppendLine("             ) P                                                                             ")
        _str_sql.AppendLine("       UNPIVOT                                                                               ")
        _str_sql.AppendLine("        (                                                                                    ")
        _str_sql.AppendLine("                 ALARM_VALUE FOR COLUMN_NAME IN (P.TS_PLAYED_ALARM_ID,                       ")
        _str_sql.AppendLine("                 P.TS_WON_ALARM_ID,P.TS_JACKPOT_ALARM_ID,                                    ")
        _str_sql.AppendLine("                 P.TS_CANCELED_CREDIT_ALARM_ID,                                              ")
        _str_sql.AppendLine("                 P.TS_COUNTERFEIT_ALARM_ID,                                                  ")
        _str_sql.AppendLine("                 P.TS_WITHOUT_PLAYS_ALARM_ID,                                                ")
        _str_sql.AppendLine("                 P.TS_HIGHROLLER_ALARM_ID,                                                   ")
        _str_sql.AppendLine("                 P.TS_HIGHROLLER_ANONYMOUS_ALARM_ID,                                         ")
        _str_sql.AppendLine("                 P.TS_CURRENT_PAYOUT_ALARM_ID)                                               ")
        _str_sql.AppendLine("                 ) PVT                                                                       ")
        _str_sql.AppendLine("    LEFT JOIN   ALARMS                                                                       ")
        _str_sql.AppendLine("            ON   PVT.ALARM_VALUE = ALARMS.AL_ALARM_ID                                        ")

        _sql_command = New SqlClient.SqlCommand(_str_sql.ToString())
        _sql_command.Transaction = _db_trx.SqlTransaction
        _sql_command.Connection = _db_trx.SqlTransaction.Connection

        _sql_command.Parameters.Add("@pTerminalID", SqlDbType.BigInt).Value = m_terminal_id

        Using _sql_da As New SqlClient.SqlDataAdapter(_sql_command)
          _sql_da.Fill(_data_table)
        End Using

        _dataset.Tables.Add(_data_table)

        _db_trx.SqlTransaction.Commit()

      End Using

      m_first_time = False

      Return _dataset

    Catch ex As Exception

      Return Nothing
    End Try
  End Function ' getDataRowFromTerminalStatus

  ' PURPOSE: Refresh Screen Terminal Data and update screen bitmask data info
  '
  '  PARAMS:
  '     - INPUT:  Enabled Boolean
  '               ValidateButton  Button
  '
  ' RETURNS:
  '               DataRow
  Private Sub setScreenTerminalInfo()
    Dim _data_row As DataRow
    Dim _data_set As DataSet
    Dim _stacker_counter As Integer
    Dim _stacker_status As Integer
    Dim _current_payout As Decimal

    Try

      _data_set = getDataRowFromTerminalStatus()

      'Load Terminal Data
      _data_row = _data_set.Tables(0).Rows(0)

      If IsNothing(_data_row) Then

        Return
      End If

      'TERMINAL INFO
      Me.ef_terminal_name.Value = _data_row(SQL_TE_NAME).ToString() & "  -  " & CLASS_SW_PACKAGE.GetTerminalTypeName(CInt(_data_row(SQL_TE_TERMINAL_TYPE)))
      Me.ef_terminal_provider.Value = _data_row(SQL_TE_PROVIDER_ID).ToString()

      If _data_row(SQL_SESSION_STATE) = TERM_STATUS_FREE Then
        Me.ef_terminal_session.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(218)
      Else
        Me.ef_terminal_session.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)
      End If

      'SERVICE INFO
      If _data_row(SQL_WCP_STATE) = TERM_STATUS_CONNECTED AndAlso _data_row(SQL_WC2_STATE) = TERM_STATUS_CONNECTED Then
        Me.lbl_WCP_WC2.BackColor = Color.Transparent
        Me.lbl_WCP_ServerName.BackColor = Color.Transparent
        Me.lbl_WCP_ServerName.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_03)
        Me.lbl_WC2_ServerName.BackColor = Color.Transparent
        Me.lbl_WC2_ServerName.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_03)
        'SAS-HOST
        If Not IsDBNull(_data_row(SQL_TS_SAS_HOST_ERROR)) AndAlso _data_row(SQL_TS_SAS_HOST_ERROR) Then
          Me.lbl_SAS_HOST.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
        Else
          Me.lbl_SAS_HOST.BackColor = Color.Transparent
        End If
      Else
        'FOS 12-FEB-2015
        If _data_row(SQL_WCP_STATE) = TERM_STATUS_CONNECTED AndAlso _data_row(SQL_WC2_STATE) = TERM_STATUS_DISCONNECTED Then
          Me.lbl_WCP_WC2.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Me.lbl_WCP_ServerName.BackColor = Color.Transparent
          Me.lbl_WC2_ServerName.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Me.lbl_WC2_ServerName.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        End If

        If _data_row(SQL_WCP_STATE) = TERM_STATUS_DISCONNECTED Then 'AndAlso _data_row(SQL_WC2_STATE) = TERM_STATUS_DISCONNECTED Then
          Me.lbl_WCP_WC2.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          Me.lbl_WCP_ServerName.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          Me.lbl_WCP_ServerName.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
          Me.lbl_WC2_ServerName.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Me.lbl_WC2_ServerName.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

        End If

        Me.lbl_SAS_HOST.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If

      If _data_row(SQL_WCP_STATE) = TERM_STATUS_CONNECTED Then
        Me.lbl_WCP_ServerName.Text = IIf(_data_row(WS_SERVER_NAME) Is Global.System.Convert.DBNull, AUDIT_NONE_STRING, _data_row(WS_SERVER_NAME))
      End If
      If _data_row(SQL_WC2_STATE) = TERM_STATUS_CONNECTED Then
        Me.lbl_WC2_ServerName.Text = IIf(_data_row(W2S_SERVER_NAME) Is Global.System.Convert.DBNull, AUDIT_NONE_STRING, _data_row(W2S_SERVER_NAME))
      End If

      If Me.lbl_WCP_ServerName.Text.Length > 15 Then
        Me.lbl_WCP_ServerName.Text = Me.lbl_WCP_ServerName.Text.Substring(0, 15)
      End If
      If Me.lbl_WC2_ServerName.Text.Length > 15 Then
        Me.lbl_WC2_ServerName.Text = Me.lbl_WC2_ServerName.Text.Substring(0, 15)
      End If

      'EGM STACKER INFO
      If IsDBNull(_data_row(SQL_TS_STACKER_COUNTER)) Then
        _stacker_counter = 0
      Else
        _stacker_counter = _data_row(SQL_TS_STACKER_COUNTER)
      End If

      Dim _stacker_capacity As Int32

      _stacker_capacity = WSI.Common.GeneralParam.GetInt32("TITO", "StackerCapacity", 0)
      If _stacker_capacity > 0 And _stacker_counter > _stacker_capacity Then
        Me.lbl_stacker_full.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Else
        Me.lbl_stacker_full.BackColor = Color.Transparent
      End If
      lbl_stacker_counter.Text = IIf(_stacker_counter = 0, AUDIT_NONE_STRING, _stacker_counter.ToString())

      'CURRENT PAYOUT
      _current_payout = 0
      If Not IsDBNull(_data_row(SQL_CURRENT_PAYOUT)) Then
        _current_payout = _data_row(SQL_CURRENT_PAYOUT)
      End If

      If _current_payout > 0 Then
        Me.lbl_current_payout_value.Text = GUI_FormatNumber(_current_payout, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
      Else
        Me.lbl_current_payout_value.Text = AUDIT_NONE_STRING
      End If

      'EGM STACKER STATUS
      If Not IsDBNull(_data_row(SQL_TS_STACKER_STATUS)) Then
        _stacker_status = _data_row(SQL_TS_STACKER_STATUS)
        Select Case _stacker_status
          Case TerminalStatusFlags.STACKER_STATUS.FULL
            lbl_stacker_counter_value.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          Case TerminalStatusFlags.STACKER_STATUS.ALMOST_FULL
            lbl_stacker_counter_value.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Case TerminalStatusFlags.STACKER_STATUS.OK
            lbl_stacker_counter_value.BackColor = Color.Transparent
        End Select
      End If

      'FLAGS INFO
      m_flags_bills = IIf(IsDBNull(_data_row(SQL_TS_BILL_FLAGS)), 0, _data_row(SQL_TS_BILL_FLAGS))
      m_flags_coins = IIf(IsDBNull(_data_row(SQL_TS_COIN_FLAGS)), 0, _data_row(SQL_TS_COIN_FLAGS))
      m_flags_doors = IIf(IsDBNull(_data_row(SQL_TS_DOOR_FLAGS)), 0, _data_row(SQL_TS_DOOR_FLAGS))
      m_flags_EGM = IIf(IsDBNull(_data_row(SQL_TS_EGM_FLAGS)), 0, _data_row(SQL_TS_EGM_FLAGS))
      m_flags_jackpot = IIf(IsDBNull(_data_row(SQL_TS_JACKPOT_FLAGS)), 0, _data_row(SQL_TS_JACKPOT_FLAGS))
      m_flags_played_won = IIf(IsDBNull(_data_row(SQL_TS_PLAYED_WON_FLAGS)), 0, _data_row(SQL_TS_PLAYED_WON_FLAGS))
      m_flags_printers = IIf(IsDBNull(_data_row(SQL_TS_PRINTER_FLAGS)), 0, _data_row(SQL_TS_PRINTER_FLAGS))
      m_flags_call = IIf(IsDBNull(_data_row(SQL_CALL_ATTENDANT_FLAGS)), 0, _data_row(SQL_CALL_ATTENDANT_FLAGS))

      ' DHA 08-APR-2014
      'JCA PBI 9306:Instrusi�n de m�quinas: Edici�n del estado del terminal
      'If m_system_mode = SYSTEM_MODE.WASS Then
      ' Machine flags
      m_flags_machine = IIf(IsDBNull(_data_row(SQL_TS_MACHINE_FLAGS)), 0, _data_row(SQL_TS_MACHINE_FLAGS))
      ' Authentication last checked
      If Not _data_row(SQL_AUTHENTICATION_LAST_CHECKED) Is DBNull.Value Then
        Me.lbl_last_checked_detail.Text = GUI_FormatDate(_data_row(SQL_AUTHENTICATION_LAST_CHECKED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Else
        Me.lbl_last_checked_detail.Text = AUDIT_NONE_STRING
      End If

      ' Authentication status
      If Not _data_row(SQL_AUTHENTICATION_STATUS) Is DBNull.Value Then
        Select Case (_data_row(SQL_AUTHENTICATION_STATUS))
          Case ENUM_AUTHENTICATION_STATUS.ERROR
            lbl_authentication_status.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          Case ENUM_AUTHENTICATION_STATUS.OK
          Case Else
            lbl_authentication_status.BackColor = Color.Transparent
        End Select
      End If
      'End If

      If _data_set.Tables(1).Rows.Count > 0 Then
        'ALARM MESSAGES
        Me.lbl_played_alarms_message.Text = getAlarmMessage(SQL_ALARM_PLAYED, _data_set.Tables(1))
        Me.lbl_wonamount_alarms_message.Text = getAlarmMessage(SQL_ALARM_WON, _data_set.Tables(1))
        Me.lbl_jackpot_alarm_message.Text = getAlarmMessage(SQL_ALARM_JACKPOT, _data_set.Tables(1))
        Me.lbl_c_credits_alarm_message.Text = getAlarmMessage(SQL_ALARM_CANCELED_CREDITS, _data_set.Tables(1))
        Me.lbl_counterfeits_alarm_message.Text = getAlarmMessage(SQL_ALARM_COUNTERFEITS, _data_set.Tables(1))
        Me.lbl_without_plays_value.Text = getAlarmMessage(SQL_ALARM_WITHOUT_PLAYS, _data_set.Tables(1))
        Me.lbl_highroller_value.Text = getAlarmMessage(SQL_ALARM_HIGHROLLER, _data_set.Tables(1))
        Me.lbl_highroller_anonymous_value.Text = getAlarmMessage(SQL_ALARM_HIGHROLLER_ANONYMOUS, _data_set.Tables(1))

        If Not getAlarmMessage(SQL_ALARM_PAYOUT, _data_set.Tables(1)) = AUDIT_NONE_STRING Then
          Me.lbl_current_payout_value.Text = getAlarmMessage(SQL_ALARM_PAYOUT, _data_set.Tables(1))
        End If

      End If

    Catch ex As Exception
      Log.Exception(ex)
    End Try
  End Sub ' setScreenTerminalInfo

  ' PURPOSE: Return Alarm message from DataTable
  '
  '  PARAMS:
  '     - INPUT:  Id Integer
  '               DataTableAlarms DataTable
  '
  ' RETURNS:
  '               String
  Private Function getAlarmMessage(ByVal Id As Integer, ByVal DataTableAlarms As DataTable) As String
    Dim _data_row As DataRow

    Try
      'Load Terminal Data
      _data_row = DataTableAlarms.Rows(Id)

      If IsNothing(_data_row) Then

        Return AUDIT_NONE_STRING
      End If

      If String.IsNullOrEmpty(_data_row(1).ToString()) Then

        Return AUDIT_NONE_STRING
      End If

      Return _data_row(1).ToString()

    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return AUDIT_NONE_STRING
  End Function

  ' PURPOSE: Update BackColor by terminal status flags
  '
  '  PARAMS:
  '
  '
  ' RETURNS:
  '
  Private Sub UpdateScreenData()

    'GroupBox Doors
    Me.lbl_belly_door.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_doors, TerminalStatusFlags.DOOR_FLAG.BELLY_DOOR), True, btn_4)
    Me.lbl_card_cage.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_doors, TerminalStatusFlags.DOOR_FLAG.CARD_CAGE), True, btn_3)
    Me.lbl_cashbox_door.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_doors, TerminalStatusFlags.DOOR_FLAG.CASHBOX_DOOR), True, btn_5)
    Me.lbl_drop_door.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_doors, TerminalStatusFlags.DOOR_FLAG.DROP_DOOR), True, btn_2)
    Me.lbl_slot_door.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_doors, TerminalStatusFlags.DOOR_FLAG.SLOT_DOOR), True, btn_1)

    'GroupBox Played/won
    Me.lbl_wonamount_exceeded.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.WON), True, btn_22)
    Me.lbl_played.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.PLAYED), True, btn_6)
    Me.lbl_current_payout.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT), True, btn_31)
    Me.lbl_without_plays.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.WITHOUT_PLAYS), True, btn_33)
    Me.lbl_highroller.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER), True, btn_37)
    Me.lbl_highroller_anonymous.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_played_won, TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS), True, btn_38)

    'Call
    Me.lbl_call_attendant.BackColor = getEnabledColor(m_flags_call, True, btn_29)

    'GroupBox Egm
    Me.lbl_cmos_error_data_recovered.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.CMOS_RAM_DATA_RECOVERED), False, btn_8)
    Me.lbl_cmos_error_no_data_recovered.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.CMOS_RAM_NO_DATA_RECOVERED), False, btn_9)
    Me.lbl_cmos_ram_error_bad_device.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.CMOS_RAM_BAD_DEVICE), False, btn_10)
    Me.lbl_eprom_error_data_error.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.EPROM_DATA_ERROR), False, btn_11)
    Me.lbl_eprom_error_bad_device.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.EPROM_BAD_DEVICE), False, btn_23)
    Me.lbl_eprom_error_different_checksum_version_changed.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.EPROM_DIFF_CHECKSUM), False, btn_24)
    Me.lbl_eprom_error_bad_checksum_compare.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.EPROM_BAD_CHECKSUM), False, btn_25)
    Me.lbl_partitioned_eprom_error_checksum_version_changed.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.PART_EPROM_DIFF_CHECKSUM), False, btn_26)
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.PART_EPROM_BAD_CHECKSUM), False, btn_27)
    Me.lbl_backup_battery_detected.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_EGM, TerminalStatusFlags.EGM_FLAG.LOW_BACKUP_BATTERY), True, btn_28)

    'GroupBox Bills
    Me.lbl_bill_acceptor_hardware_failure.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_bills, TerminalStatusFlags.BILL_FLAG.HARD_FAILURE), True, btn_13)
    Me.lbl_bill_jam.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_bills, TerminalStatusFlags.BILL_FLAG.BILL_JAM), True, btn_12)
    Me.lbl_excess_counterfeits.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_bills, TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED), True, btn_14)

    'GroupBox coins
    Me.lbl_coin_in_tilt.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_coins, TerminalStatusFlags.COIN_FLAG.COIN_IN_TILT), True, btn_34)
    Me.lbl_reverse_coin_in.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_coins, TerminalStatusFlags.COIN_FLAG.REVERSE_COIN), True, btn_35)
    Me.lbl_coin_in_lockout_malfunction.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_coins, TerminalStatusFlags.COIN_FLAG.COIN_IN_LOCKOUT), True, btn_36)

    'GroupBox Printers
    Me.lbl_printer_carriage_jammed.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_printers, TerminalStatusFlags.PRINTER_FLAG.CARRIAGE_JAM), True, btn_19)
    Me.lbl_printer_communication_error.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_printers, TerminalStatusFlags.PRINTER_FLAG.COMM_ERROR), True, btn_15)
    Me.lbl_printer_paper_low.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_printers, TerminalStatusFlags.PRINTER_FLAG.PAPER_LOW), True, btn_18)
    Me.lbl_printer_paper_out_error.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_printers, TerminalStatusFlags.PRINTER_FLAG.PAPER_OUT_ERROR), True, btn_16)
    Me.lbl_printer_power_on.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_printers, TerminalStatusFlags.PRINTER_FLAG.POWER_ERROR), True, btn_20)
    Me.lbl_replace_printer_ribbon.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_printers, TerminalStatusFlags.PRINTER_FLAG.REPLACE_RIBBON), True, btn_17)

    'GroupBox HandPays
    Me.lbl_jackpot.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_jackpot, TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS), True, btn_21)
    Me.lbl_c_credits.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_jackpot, TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT), True, btn_7)

    'GroupBox Software Validation
    Me.lbl_locked_by_intrusion.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_machine, TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_INTRUSION), False, btn_39)

    If m_system_mode = SYSTEM_MODE.WASS Then
      Me.lbl_locked_by_signature.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_machine, TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE), False, btn_32)
      Me.lbl_locked_by_manually.BackColor = getEnabledColor(TerminalStatusFlags.IsFlagActived(m_flags_machine, TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY), True, btn_30)
    End If

  End Sub ' UpdateScreenData

  ' PURPOSE: Get Terminal Payout
  '
  '  PARAMS:
  '     - INPUT:  TerminalId
  '     - OUTPUT: TerminalPayout
  '
  ' RETURNS:
  '
  Private Sub GetTerminalPayout(ByVal TerminalId As Integer, ByRef TerminalPayout As Integer)
    Dim _sb As StringBuilder
    Dim _data As Object

    _sb = New StringBuilder()

    Try
      _sb.AppendLine("SELECT   TE_THEORETICAL_PAYOUT         ")
      _sb.AppendLine("  FROM   TERMINALS                     ")
      _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ")

      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId
          _data = _cmd.ExecuteScalar()
          If Not IsNothing(_data) AndAlso IsNumeric(_data) Then
            TerminalPayout = CType(_data, Integer)
          Else
            TerminalPayout = WSI.Common.GeneralParam.GetInt32("PlayerTracking", "TerminalDefaultPayout", 0)
          End If
        End Using
      End Using

    Catch ex As Exception
      TerminalPayout = WSI.Common.GeneralParam.GetInt32("PlayerTracking", "TerminalDefaultPayout", 0)
    End Try
  End Sub ' GetTerminalPayout

  ' PURPOSE: Loop controls finding ID=Tag
  '
  '  PARAMS:
  '
  ' RETURNS:
  '
  Private Sub SetNLSValuesFromDB()

    Dim _has_three_points As Boolean

    Try
      m_NLS_list = frm_terminal_status.LoadMetersByGroup()

      For Each _groupbox_control As Control In Me.panel_info_terminal.Controls
        If TypeOf (_groupbox_control) Is System.Windows.Forms.GroupBox Then
          For Each _label_control As Control In _groupbox_control.Controls
            If TypeOf (_label_control) Is System.Windows.Forms.Label Then
              If Not String.IsNullOrEmpty(_label_control.Tag) AndAlso m_NLS_list.ContainsKey(_label_control.Tag) Then

                If Not String.IsNullOrEmpty(m_NLS_list(_label_control.Tag)) Then

                  _label_control.Text = m_NLS_list(_label_control.Tag)

                End If

                _has_three_points = False
                While _label_control.Width >= 333
                  If Not _has_three_points Then
                    Me.ToolTip1.SetToolTip(_label_control, _label_control.Text)
                  End If
                  _label_control.Text = _label_control.Text.Remove(_label_control.Text.Length - 1, 1)
                  _has_three_points = True
                End While

                If _has_three_points Then
                  _label_control.Text = _label_control.Text.Remove(_label_control.Text.Length - 3, 3)
                  _label_control.Text &= "..."
                End If

              End If 'If Not Stri
            End If 'If TypeOf (_control_2)
          Next
        End If ' If TypeOf (_control)
      Next

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub

  Private Function getNLSbyTag(ByVal Tag As String, ByVal NLSId As Integer) As String

    If Not String.IsNullOrEmpty(m_NLS_list(Tag)) Then
      Return m_NLS_list(Tag)
    Else
      Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(NLSId)
    End If

  End Function

#End Region ' Private Functions

#Region "Public Functions"

  ' PURPOSE: Show terminal info.
  '
  '  PARAMS:
  '     - INPUT: TerminalId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub ShowItem(ByVal TerminalId As Integer)

    m_terminal_id = TerminalId

    Me.ShowDialog()

  End Sub ' showItem

#End Region ' Public Functions

#Region "Events"

  ' PURPOSE: Execute the command associated to the button ButtonId.
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender: System.Object.
  '           - e: System.EventArgs.
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub ProcessButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_1.Click, btn_2.Click, btn_3.Click, btn_4.Click, btn_5.Click, btn_6.Click, _
                                                                                                     btn_7.Click, btn_8.Click, btn_9.Click, btn_10.Click, btn_11.Click, btn_12.Click, _
                                                                                                    btn_13.Click, btn_14.Click, btn_15.Click, btn_16.Click, btn_17.Click, btn_18.Click, _
                                                                                                    btn_19.Click, btn_20.Click, btn_21.Click, btn_22.Click, btn_23.Click, btn_24.Click, _
                                                                                                    btn_25.Click, btn_26.Click, btn_27.Click, btn_28.Click, btn_29.Click, btn_31.Click, _
                                                                                                    btn_30.Click, btn_32.Click, btn_33.Click, btn_34.Click, btn_35.Click, btn_36.Click, _
                                                                                                    btn_37.Click, btn_38.Click, btn_39.Click
    Dim _button_id As Integer
    Dim _str_button As String

    _str_button = sender.Name.ToString()

    If Integer.TryParse(_str_button.Substring(4), _button_id) Then

      Select Case _button_id
        'DOORS
        Case BTN_SLOT_DOOR
          m_flags_doors += TerminalStatusFlags.DOOR_FLAG.SLOT_DOOR
          Me.lbl_slot_door.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4492)

        Case BTN_DROP_DOOR
          m_flags_doors += TerminalStatusFlags.DOOR_FLAG.DROP_DOOR
          Me.lbl_drop_door.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4493)

        Case BTN_CARD_CAGE
          m_flags_doors += TerminalStatusFlags.DOOR_FLAG.CARD_CAGE
          Me.lbl_card_cage.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4495)

        Case BTN_BELLY_DOOR
          m_flags_doors += TerminalStatusFlags.DOOR_FLAG.BELLY_DOOR
          Me.lbl_belly_door.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4491)

        Case BTN_CASHBOX_DOOR
          m_flags_doors += TerminalStatusFlags.DOOR_FLAG.CASHBOX_DOOR
          Me.lbl_cashbox_door.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4494)

        Case BTN_PLAYS
          m_flags_played_won += TerminalStatusFlags.PLAYS_FLAG.PLAYED
          Me.lbl_played.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(537)
          Me.lbl_played_alarms_message.Text = AUDIT_NONE_STRING

        Case BTN_PAYOUT
          m_flags_played_won += TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT
          Me.lbl_current_payout.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
          Me.lbl_current_payout_value.Text = AUDIT_NONE_STRING

        Case BTN_WHITOUT_PLAYS
          m_flags_played_won += TerminalStatusFlags.PLAYS_FLAG.WITHOUT_PLAYS
          Me.lbl_without_plays.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4852)
          Me.lbl_without_plays_value.Text = AUDIT_NONE_STRING

        Case BTN_HIGHROLLER
          m_flags_played_won += TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER
          Me.lbl_highroller.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892)
          Me.lbl_highroller_value.Text = AUDIT_NONE_STRING

        Case BTN_HIGHROLLER_ANONYMOUS
          m_flags_played_won += TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS
          Me.lbl_highroller_anonymous.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6892) + " " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6897)
          Me.lbl_highroller_anonymous_value.Text = AUDIT_NONE_STRING

        Case BTN_WON
          m_flags_played_won += TerminalStatusFlags.PLAYS_FLAG.WON
          Me.lbl_wonamount_exceeded.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(798)
          Me.lbl_wonamount_alarms_message.Text = AUDIT_NONE_STRING

        Case BTN_JACKPOT
          m_flags_jackpot += TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS
          Me.lbl_jackpot.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4525)
          Me.lbl_jackpot_alarm_message.Text = AUDIT_NONE_STRING

        Case BTN_CANCELED_CREDITS
          m_flags_jackpot += TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT
          Me.lbl_c_credits.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4857)
          Me.lbl_c_credits_alarm_message.Text = AUDIT_NONE_STRING

          'CALL
        Case BTN_CALL_ATTENDANT
          m_flags_call = False
          Me.lbl_call_attendant.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_ALARMS.GetString(361)

          'CPU
        Case BTN_CMOS_RAM_ERROR_DATA_RECOVERED
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.CMOS_RAM_DATA_RECOVERED
          Me.lbl_cmos_error_data_recovered.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_cmos_error_data_recovered.Tag, 4504)

        Case BTN_CMOS_RAM_ERROR_NO_DATA_RECOVERED
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.CMOS_RAM_NO_DATA_RECOVERED
          Me.lbl_cmos_error_no_data_recovered.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_cmos_error_no_data_recovered.Tag, 4505)

        Case BTN_CMOS_RAM_ERROR_BAD_DEVICE
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.CMOS_RAM_BAD_DEVICE
          Me.lbl_cmos_ram_error_bad_device.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_cmos_ram_error_bad_device.Tag, 4508)

        Case BTN_CMOS_RAM_ERROR_DATA_ERROR
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.EPROM_DATA_ERROR
          Me.lbl_eprom_error_data_error.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_eprom_error_data_error.Tag, 4507)

        Case BTN_EPROM_ERROR_BAD_DEVICE
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.EPROM_BAD_DEVICE
          Me.lbl_eprom_error_bad_device.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_eprom_error_bad_device.Tag, 4506)

        Case BTN_EPROM_ERROR_DIFFERENT_CHECKSUM_VERSION_CHANGED
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.EPROM_DIFF_CHECKSUM
          Me.lbl_eprom_error_different_checksum_version_changed.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_eprom_error_different_checksum_version_changed.Tag, 4509)

        Case BTN_EPROM_ERROR_BAD_CHECKSUM_COMPARE
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.EPROM_BAD_CHECKSUM
          Me.lbl_eprom_error_bad_checksum_compare.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_eprom_error_bad_checksum_compare.Tag, 4510)

        Case BTN_PARTITIONED_EPROM_ERROR_CHECKSUM_VERSION_CHANGED
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.PART_EPROM_DIFF_CHECKSUM
          Me.lbl_partitioned_eprom_error_checksum_version_changed.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_partitioned_eprom_error_checksum_version_changed.Tag, 4511)

        Case BTN_PARTITIONED_EPROM_ERROR_BAD_CHECKSUM_COMPARE
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.PART_EPROM_BAD_CHECKSUM
          Me.lbl_partitioned_eprom_error_bad_checksum_compare.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_partitioned_eprom_error_bad_checksum_compare.Tag, 4512)

        Case BTN_LOW_BACKUP_BATTERY_DETECTED
          m_flags_EGM += TerminalStatusFlags.EGM_FLAG.LOW_BACKUP_BATTERY
          Me.lbl_backup_battery_detected.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_backup_battery_detected.Tag, 4513)

          'BILLS
        Case BTN_BILL_JAM
          m_flags_bills += TerminalStatusFlags.BILL_FLAG.BILL_JAM
          Me.lbl_bill_jam.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_bill_jam.Tag, 4515)

        Case BTN_BILL_ACCEPTOR_HARDWARE_FAILURE
          m_flags_bills += TerminalStatusFlags.BILL_FLAG.HARD_FAILURE
          Me.lbl_bill_acceptor_hardware_failure.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_bill_acceptor_hardware_failure.Tag, 4516)

        Case BTN_EXCESS_COUNTERFEITS
          m_flags_bills += TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED
          Me.lbl_excess_counterfeits.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4843)
          Me.lbl_counterfeits_alarm_message.Text = AUDIT_NONE_STRING

          'COINS_IN
        Case BTN_COIN_IN_TILT
          m_flags_coins += TerminalStatusFlags.COIN_FLAG.COIN_IN_TILT
          Me.lbl_coin_in_tilt.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_coin_in_tilt.Tag, 6717)

        Case BTN_REVERSE_COIN_IN
          m_flags_coins += TerminalStatusFlags.COIN_FLAG.REVERSE_COIN
          Me.lbl_reverse_coin_in.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_reverse_coin_in.Tag, 6718)

        Case BTN_COIN_IN_LOCKOUT_MALFUNCTION
          m_flags_coins += TerminalStatusFlags.COIN_FLAG.COIN_IN_LOCKOUT
          Me.lbl_coin_in_lockout_malfunction.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_coin_in_lockout_malfunction.Tag, 6719)

          'PRINTER
        Case BTN_PRINTER_COMMUNITACION_ERROR
          m_flags_printers += TerminalStatusFlags.PRINTER_FLAG.COMM_ERROR
          Me.lbl_printer_communication_error.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_printer_communication_error.Tag, 4519)

        Case BTN_PRINTER_PAPER_OUT_ERROR
          m_flags_printers += TerminalStatusFlags.PRINTER_FLAG.PAPER_OUT_ERROR
          Me.lbl_printer_paper_out_error.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_printer_paper_out_error.Tag, 4522)

        Case BTN_REPLACE_PRINTER_RIBBON
          m_flags_printers += TerminalStatusFlags.PRINTER_FLAG.REPLACE_RIBBON
          Me.lbl_replace_printer_ribbon.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_replace_printer_ribbon.Tag, 4524)

        Case BTN_PRINTER_PAPER_LOW
          m_flags_printers += TerminalStatusFlags.PRINTER_FLAG.PAPER_LOW
          Me.lbl_printer_paper_low.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_printer_paper_low.Tag, 4520)

        Case BTN_PRINTER_CARRIAGE_JAMMED
          m_flags_printers += TerminalStatusFlags.PRINTER_FLAG.CARRIAGE_JAM
          Me.lbl_printer_carriage_jammed.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_printer_carriage_jammed.Tag, 4521)

        Case BTN_PRINTER_POWER_ON
          m_flags_printers += TerminalStatusFlags.PRINTER_FLAG.POWER_ERROR
          Me.lbl_printer_power_on.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & getNLSbyTag(Me.lbl_printer_power_on.Tag, 4523)

          'MACHINE
        Case BTN_MACHINE
          m_flags_machine += TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE
          Me.lbl_locked_by_signature.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4813)

        Case BTN_MANUALLY
          m_flags_machine += TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY
          Me.lbl_locked_by_manually.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4913)

        Case BTN_INTRUSION
          m_flags_machine += TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_INTRUSION
          Me.lbl_locked_by_intrusion.BackColor = getEnabledColor(False, False)
          m_str_button_audit &= ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7115)

      End Select

      sender.Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Focus()
    End If

  End Sub ' ProcessButtonClick

  ' PURPOSE: Execute proper actions when programmed timer elapses
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender: System.Object.
  '           - e: System.EventArgs.
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub m_timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles m_timer.Tick

    Call setScreenTerminalInfo()
    Call UpdateScreenData()
    Call setDefaultValues()

    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

  End Sub ' m_timer_Tick

  ' PURPOSE: Change the fore color of the labels
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender: System.Object.
  '           - e: System.EventArgs.
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub Labels_BackColorChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbl_SAS_HOST.BackColorChanged, lbl_belly_door.BackColorChanged, _
                                                                                                        lbl_card_cage.BackColorChanged, lbl_cashbox_door.BackColorChanged, lbl_drop_door.BackColorChanged, _
                                                                                                        lbl_slot_door.BackColorChanged, lbl_wonamount_exceeded.BackColorChanged, _
                                                                                                        lbl_played.BackColorChanged, lbl_current_payout.BackColorChanged, lbl_call_attendant.BackColorChanged, _
                                                                                                        lbl_cmos_error_data_recovered.BackColorChanged, lbl_cmos_error_no_data_recovered.BackColorChanged, _
                                                                                                        lbl_cmos_ram_error_bad_device.BackColorChanged, lbl_eprom_error_data_error.BackColorChanged, _
                                                                                                        lbl_eprom_error_bad_device.BackColorChanged, lbl_eprom_error_different_checksum_version_changed.BackColorChanged, _
                                                                                                        lbl_eprom_error_bad_checksum_compare.BackColorChanged, lbl_partitioned_eprom_error_checksum_version_changed.BackColorChanged, _
                                                                                                        lbl_partitioned_eprom_error_bad_checksum_compare.BackColorChanged, lbl_backup_battery_detected.BackColorChanged, _
                                                                                                        lbl_bill_acceptor_hardware_failure.BackColorChanged, lbl_bill_jam.BackColorChanged, _
                                                                                                        lbl_excess_counterfeits.BackColorChanged, lbl_printer_carriage_jammed.BackColorChanged, lbl_printer_communication_error.BackColorChanged, _
                                                                                                        lbl_printer_paper_low.BackColorChanged, lbl_printer_paper_out_error.BackColorChanged, lbl_printer_power_on.BackColorChanged, lbl_replace_printer_ribbon.BackColorChanged, _
                                                                                                        lbl_jackpot.BackColorChanged, lbl_c_credits.BackColorChanged, lbl_WCP_WC2.BackColorChanged, _
                                                                                                        lbl_stacker_full.BackColorChanged, lbl_stacker_counter_value.BackColorChanged, lbl_locked_by_signature.BackColorChanged, lbl_authentication_status.BackColorChanged, lbl_highroller_anonymous.BackColorChanged, lbl_highroller.BackColorChanged

    If sender.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00) Then
      sender.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    Else
      sender.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    End If
  End Sub 'Labels_BackColorChanged
#End Region ' Events

End Class



