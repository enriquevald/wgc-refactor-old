<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_flash_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dt_date_from = New GUI_Controls.uc_date_picker
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1216, 66)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 70)
    Me.panel_data.Size = New System.Drawing.Size(1216, 648)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1210, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1210, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dt_date_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(200, 58)
    Me.gb_date.TabIndex = 11
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDATE FROM"
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(7, 20)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = False
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(180, 25)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 1
    Me.dt_date_from.Value = New Date(2012, 3, 15, 10, 57, 5, 200)
    '
    'frm_flash_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1224, 722)
    Me.Name = "frm_flash_report"
    Me.Text = "frm_flash_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
End Class
