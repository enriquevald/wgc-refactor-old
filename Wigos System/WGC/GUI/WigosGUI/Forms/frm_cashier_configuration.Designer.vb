<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cashier_configuration
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cashier_configuration))
    Me.uc_cmb_cashier_type = New GUI_Controls.uc_combo()
    Me.lbl_cashier_type = New System.Windows.Forms.Label()
    Me.tab_control = New System.Windows.Forms.TabControl()
    Me.TabWin = New System.Windows.Forms.TabPage()
    Me.uc_wcp_cashier_config = New GUI_Controls.uc_wcp_cashier_config()
    Me.TabAlesis = New System.Windows.Forms.TabPage()
    Me.uc_alesis_cashier_config = New GUI_Controls.uc_alesis_cashier_config()
    Me.TabPoints = New System.Windows.Forms.TabPage()
    Me.pnl_player_tracking = New System.Windows.Forms.Panel()
    Me.gb_lower_panel = New System.Windows.Forms.GroupBox()
    Me.lbl_points_expiration_msg = New System.Windows.Forms.Label()
    Me.gb_gift_expiration = New System.Windows.Forms.GroupBox()
    Me.uc_entry_gift_voucher_expiration = New GUI_Controls.uc_entry_field()
    Me.gb_providers = New System.Windows.Forms.GroupBox()
    Me.dg_providers = New GUI_Controls.uc_grid()
    Me.gb_playsession = New System.Windows.Forms.GroupBox()
    Me.chk_max_points = New System.Windows.Forms.CheckBox()
    Me.chk_max_points_per_minute = New System.Windows.Forms.CheckBox()
    Me.chk_has_mismatch = New System.Windows.Forms.CheckBox()
    Me.chk_has_zero_plays = New System.Windows.Forms.CheckBox()
    Me.uc_max_points_per_minute = New GUI_Controls.uc_entry_field()
    Me.uc_max_points = New GUI_Controls.uc_entry_field()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.tab_control_points = New System.Windows.Forms.TabControl()
    Me.TabPointsDef = New System.Windows.Forms.TabPage()
    Me.lbl_points_program = New System.Windows.Forms.Label()
    Me.uc_entry_04_points_to_enter = New GUI_Controls.uc_entry_field()
    Me.uc_entry_03_points_to_enter = New GUI_Controls.uc_entry_field()
    Me.uc_entry_02_points_to_enter = New GUI_Controls.uc_entry_field()
    Me.lbl_points_to_enter = New System.Windows.Forms.Label()
    Me.TabLock = New System.Windows.Forms.TabPage()
    Me.uc_cmb_Level4 = New GUI_Controls.uc_combo()
    Me.uc_cmb_Level3 = New GUI_Controls.uc_combo()
    Me.uc_cmb_Level2 = New GUI_Controls.uc_combo()
    Me.uc_cmb_Level1 = New GUI_Controls.uc_combo()
    Me.TabPlayerLevelIcons = New System.Windows.Forms.TabPage()
    Me.pb_level_04 = New System.Windows.Forms.PictureBox()
    Me.pb_level_03 = New System.Windows.Forms.PictureBox()
    Me.pb_level_02 = New System.Windows.Forms.PictureBox()
    Me.pb_level_01 = New System.Windows.Forms.PictureBox()
    Me.cmb_player_level_icon_04 = New GUI_Controls.uc_combo()
    Me.cmb_player_level_icon_03 = New GUI_Controls.uc_combo()
    Me.cmb_player_level_icon_02 = New GUI_Controls.uc_combo()
    Me.cmb_player_level_icon_01 = New GUI_Controls.uc_combo()
    Me.uc_entry_levels_days_counting_points = New GUI_Controls.uc_entry_field()
    Me.uc_entry_04_name = New GUI_Controls.uc_entry_field()
    Me.lbl_04_name = New System.Windows.Forms.Label()
    Me.lbl_name = New System.Windows.Forms.Label()
    Me.uc_entry_03_name = New GUI_Controls.uc_entry_field()
    Me.lbl_suffix_01 = New System.Windows.Forms.Label()
    Me.uc_entry_02_name = New GUI_Controls.uc_entry_field()
    Me.uc_entry_01_name = New GUI_Controls.uc_entry_field()
    Me.lbl_03_name = New System.Windows.Forms.Label()
    Me.lbl_02_name = New System.Windows.Forms.Label()
    Me.lbl_01_name = New System.Windows.Forms.Label()
    Me.gb_permanency = New System.Windows.Forms.GroupBox()
    Me.lbl_points_to_keep_information = New System.Windows.Forms.Label()
    Me.lbl_expiration_day_month = New System.Windows.Forms.Label()
    Me.lbl_points_to_keep = New System.Windows.Forms.Label()
    Me.dtp_expiration_day_month = New System.Windows.Forms.DateTimePicker()
    Me.uc_entry_04_points_to_keep = New GUI_Controls.uc_entry_field()
    Me.uc_entry_03_points_to_keep = New GUI_Controls.uc_entry_field()
    Me.chk_expiration_day_month = New System.Windows.Forms.CheckBox()
    Me.uc_entry_02_points_to_keep = New GUI_Controls.uc_entry_field()
    Me.uc_entry_03_days_on_level = New GUI_Controls.uc_entry_field()
    Me.lbl_days_on_level = New System.Windows.Forms.Label()
    Me.uc_entry_04_days_on_level = New GUI_Controls.uc_entry_field()
    Me.uc_entry_02_days_on_level = New GUI_Controls.uc_entry_field()
    Me.gb_inactivity = New System.Windows.Forms.GroupBox()
    Me.chk_max_days_no_activity = New System.Windows.Forms.CheckBox()
    Me.uc_entry_03_max_days_no_activity = New GUI_Controls.uc_entry_field()
    Me.lbl_max_days_no_activity = New System.Windows.Forms.Label()
    Me.uc_entry_02_max_days_no_activity = New GUI_Controls.uc_entry_field()
    Me.uc_entry_04_max_days_no_activity = New GUI_Controls.uc_entry_field()
    Me.TabTaxes = New System.Windows.Forms.TabPage()
    Me.pnl_taxes = New System.Windows.Forms.Panel()
    Me.Uc_entry_tax_3_on_won_name = New GUI_Controls.uc_entry_field()
    Me.lbl_taxes_name = New System.Windows.Forms.Label()
    Me.tab_business = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.uc_business_a = New GUI_Controls.uc_taxes_config()
    Me.TabPage2 = New System.Windows.Forms.TabPage()
    Me.uc_business_b = New GUI_Controls.uc_taxes_config()
    Me.chk_split_enabled = New System.Windows.Forms.CheckBox()
    Me.tab_taxes = New System.Windows.Forms.TabControl()
    Me.TabTaxesOnPrize = New System.Windows.Forms.TabPage()
    Me.pnl_taxes_on_prize = New System.Windows.Forms.Panel()
    Me.Uc_entry_tax_3_apply_if = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_tax_3_on_won_pct = New GUI_Controls.uc_entry_field()
    Me.lbl_tax3_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.Uc_entry_tax_2_apply_if = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_tax_1_apply_if = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_tax_2_on_won_pct = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_tax_1_on_won_pct = New GUI_Controls.uc_entry_field()
    Me.lbl_state_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.lbl_federal_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.TabTaxesOnPromotions = New System.Windows.Forms.TabPage()
    Me.pnl_taxes_on_promotions = New System.Windows.Forms.Panel()
    Me.chk_enable_taxes_redeem_promotions = New System.Windows.Forms.CheckBox()
    Me.Uc_entry_promo_tax_3_apply_if = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_promo_tax_2_apply_if = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_promo_tax_3_on_won_pct = New GUI_Controls.uc_entry_field()
    Me.lbl_tax3_promo_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.Uc_entry_promo_tax_1_apply_if = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_promo_tax_2_on_won_pct = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_promo_tax_1_on_won_pct = New GUI_Controls.uc_entry_field()
    Me.lbl_state_promo_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.lbl_federal_promo_tax_on_won_suffix = New System.Windows.Forms.Label()
    Me.Uc_entry_tax_2_on_won_name = New GUI_Controls.uc_entry_field()
    Me.Uc_entry_tax_1_on_won_name = New GUI_Controls.uc_entry_field()
    Me.TabEvidence = New System.Windows.Forms.TabPage()
    Me.id_evidence_configuration = New GUI_Controls.uc_identity_data()
    Me.TabCFDI = New System.Windows.Forms.TabPage()
    Me.uc_entry_cfdi_active = New System.Windows.Forms.CheckBox()
    Me.gb_cfdi_resume = New System.Windows.Forms.GroupBox()
    Me.dg_cfdi_resume = New GUI_Controls.uc_grid()
    Me.gb_cfdi_configuration = New System.Windows.Forms.GroupBox()
    Me.gb_special_account_data = New System.Windows.Forms.GroupBox()
    Me.uc_entry_cfdi_ef_anonymous = New GUI_Controls.uc_entry_field()
    Me.uc_entry_cfdi_rfc_anonymous = New GUI_Controls.uc_entry_field()
    Me.lbl_special_account_ef = New System.Windows.Forms.Label()
    Me.lbl_special_account_rfc = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_ef_foreign = New GUI_Controls.uc_entry_field()
    Me.uc_entry_cfdi_rfc_foreign = New GUI_Controls.uc_entry_field()
    Me.lbl_special_account_foreign = New System.Windows.Forms.Label()
    Me.lbl_special_account_anonymous = New System.Windows.Forms.Label()
    Me.gb_scheduling = New System.Windows.Forms.GroupBox()
    Me.lbl_info_load = New System.Windows.Forms.Label()
    Me.lbl_info_process = New System.Windows.Forms.Label()
    Me.lbl_cfdi_minutes_load = New System.Windows.Forms.Label()
    Me.lbl_cfdi_minutes_process = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_retries = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_retries = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_scheduling_load = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_scheduling_load = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_scheduling_process = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_scheduling_process = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_site = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_site = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_password = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_password = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_user = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_user = New System.Windows.Forms.Label()
    Me.uc_entry_cfdi_url = New GUI_Controls.uc_entry_field()
    Me.lbl_cfdi_url = New System.Windows.Forms.Label()
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.panel_data.SuspendLayout()
    Me.tab_control.SuspendLayout()
    Me.TabWin.SuspendLayout()
    Me.TabAlesis.SuspendLayout()
    Me.TabPoints.SuspendLayout()
    Me.pnl_player_tracking.SuspendLayout()
    Me.gb_lower_panel.SuspendLayout()
    Me.gb_gift_expiration.SuspendLayout()
    Me.gb_providers.SuspendLayout()
    Me.gb_playsession.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.tab_control_points.SuspendLayout()
    Me.TabPointsDef.SuspendLayout()
    Me.TabLock.SuspendLayout()
    Me.TabPlayerLevelIcons.SuspendLayout()
    CType(Me.pb_level_04, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pb_level_03, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pb_level_02, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pb_level_01, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.gb_permanency.SuspendLayout()
    Me.gb_inactivity.SuspendLayout()
    Me.TabTaxes.SuspendLayout()
    Me.pnl_taxes.SuspendLayout()
    Me.tab_business.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.TabPage2.SuspendLayout()
    Me.tab_taxes.SuspendLayout()
    Me.TabTaxesOnPrize.SuspendLayout()
    Me.pnl_taxes_on_prize.SuspendLayout()
    Me.TabTaxesOnPromotions.SuspendLayout()
    Me.pnl_taxes_on_promotions.SuspendLayout()
    Me.TabEvidence.SuspendLayout()
    Me.TabCFDI.SuspendLayout()
    Me.gb_cfdi_resume.SuspendLayout()
    Me.gb_cfdi_configuration.SuspendLayout()
    Me.gb_special_account_data.SuspendLayout()
    Me.gb_scheduling.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tab_control)
    Me.panel_data.Controls.Add(Me.lbl_cashier_type)
    Me.panel_data.Controls.Add(Me.uc_cmb_cashier_type)
    Me.panel_data.Size = New System.Drawing.Size(1094, 764)
    '
    'uc_cmb_cashier_type
    '
    Me.uc_cmb_cashier_type.AllowUnlistedValues = False
    Me.uc_cmb_cashier_type.AutoCompleteMode = False
    Me.uc_cmb_cashier_type.IsReadOnly = False
    Me.uc_cmb_cashier_type.Location = New System.Drawing.Point(215, 11)
    Me.uc_cmb_cashier_type.Name = "uc_cmb_cashier_type"
    Me.uc_cmb_cashier_type.SelectedIndex = -1
    Me.uc_cmb_cashier_type.Size = New System.Drawing.Size(184, 24)
    Me.uc_cmb_cashier_type.SufixText = "Sufix Text"
    Me.uc_cmb_cashier_type.SufixTextVisible = True
    Me.uc_cmb_cashier_type.TabIndex = 0
    Me.uc_cmb_cashier_type.TextCombo = Nothing
    Me.uc_cmb_cashier_type.TextVisible = False
    Me.uc_cmb_cashier_type.TextWidth = 0
    '
    'lbl_cashier_type
    '
    Me.lbl_cashier_type.AutoSize = True
    Me.lbl_cashier_type.Location = New System.Drawing.Point(117, 19)
    Me.lbl_cashier_type.Name = "lbl_cashier_type"
    Me.lbl_cashier_type.Size = New System.Drawing.Size(85, 13)
    Me.lbl_cashier_type.TabIndex = 6
    Me.lbl_cashier_type.Text = "xCashierType"
    '
    'tab_control
    '
    Me.tab_control.Controls.Add(Me.TabWin)
    Me.tab_control.Controls.Add(Me.TabAlesis)
    Me.tab_control.Controls.Add(Me.TabPoints)
    Me.tab_control.Controls.Add(Me.TabTaxes)
    Me.tab_control.Controls.Add(Me.TabEvidence)
    Me.tab_control.Controls.Add(Me.TabCFDI)
    Me.tab_control.Cursor = System.Windows.Forms.Cursors.Default
    Me.tab_control.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.tab_control.Location = New System.Drawing.Point(11, 54)
    Me.tab_control.Multiline = True
    Me.tab_control.Name = "tab_control"
    Me.tab_control.SelectedIndex = 0
    Me.tab_control.Size = New System.Drawing.Size(1081, 704)
    Me.tab_control.TabIndex = 7
    '
    'TabWin
    '
    Me.TabWin.Controls.Add(Me.uc_wcp_cashier_config)
    Me.TabWin.Location = New System.Drawing.Point(4, 22)
    Me.TabWin.Name = "TabWin"
    Me.TabWin.Padding = New System.Windows.Forms.Padding(3)
    Me.TabWin.Size = New System.Drawing.Size(1073, 678)
    Me.TabWin.TabIndex = 0
    Me.TabWin.Text = "xTabWin"
    Me.TabWin.UseVisualStyleBackColor = True
    '
    'uc_wcp_cashier_config
    '
    Me.uc_wcp_cashier_config.AllowAnon = "0"
    Me.uc_wcp_cashier_config.CardPlayerAmountToCompanyB = False
    Me.uc_wcp_cashier_config.CardPlayerConcept = ""
    Me.uc_wcp_cashier_config.CardPrice = ""
    Me.uc_wcp_cashier_config.CardPrintBold = "0"
    Me.uc_wcp_cashier_config.CardPrintBottomX = ""
    Me.uc_wcp_cashier_config.CardPrintBottomY = ""
    Me.uc_wcp_cashier_config.CardPrinterName = ""
    Me.uc_wcp_cashier_config.CardPrintFontSize = ""
    Me.uc_wcp_cashier_config.CardPrintTopX = ""
    Me.uc_wcp_cashier_config.CardPrintTopY = ""
    Me.uc_wcp_cashier_config.CardRefundable = "0"
    Me.uc_wcp_cashier_config.CashierMode = "#3734"
    Me.uc_wcp_cashier_config.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_wcp_cashier_config.HandPayCancelTime = ""
    Me.uc_wcp_cashier_config.HandPayTimePeriod = ""
    Me.uc_wcp_cashier_config.Language = "#3733"
    Me.uc_wcp_cashier_config.Location = New System.Drawing.Point(6, 6)
    Me.uc_wcp_cashier_config.MinTimeToCloseSession = ""
    Me.uc_wcp_cashier_config.Name = "uc_wcp_cashier_config"
    Me.uc_wcp_cashier_config.PersonalCardPrice = ""
    Me.uc_wcp_cashier_config.PersonalCardReplacementChargeAfterTimes = ""
    Me.uc_wcp_cashier_config.PersonalCardReplacementPrice = ""
    Me.uc_wcp_cashier_config.PersonalCardReplacementPriceInPoints = ""
    Me.uc_wcp_cashier_config.Size = New System.Drawing.Size(956, 608)
    Me.uc_wcp_cashier_config.TabIndex = 0
    Me.uc_wcp_cashier_config.VoucherFooter = ""
    Me.uc_wcp_cashier_config.VoucherFooterAccountsCustomization = ""
    Me.uc_wcp_cashier_config.VoucherHeader = ""
    '
    'TabAlesis
    '
    Me.TabAlesis.Controls.Add(Me.uc_alesis_cashier_config)
    Me.TabAlesis.Location = New System.Drawing.Point(4, 22)
    Me.TabAlesis.Name = "TabAlesis"
    Me.TabAlesis.Padding = New System.Windows.Forms.Padding(3)
    Me.TabAlesis.Size = New System.Drawing.Size(1073, 678)
    Me.TabAlesis.TabIndex = 1
    Me.TabAlesis.Text = "xTabAlesis"
    Me.TabAlesis.UseVisualStyleBackColor = True
    '
    'uc_alesis_cashier_config
    '
    Me.uc_alesis_cashier_config.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_alesis_cashier_config.DataBaseIp = ""
    Me.uc_alesis_cashier_config.DataBaseName = ""
    Me.uc_alesis_cashier_config.Location = New System.Drawing.Point(6, 5)
    Me.uc_alesis_cashier_config.Name = "uc_alesis_cashier_config"
    Me.uc_alesis_cashier_config.Password = ""
    Me.uc_alesis_cashier_config.Size = New System.Drawing.Size(550, 615)
    Me.uc_alesis_cashier_config.TabIndex = 0
    Me.uc_alesis_cashier_config.Username = ""
    Me.uc_alesis_cashier_config.VendorId = ""
    '
    'TabPoints
    '
    Me.TabPoints.Controls.Add(Me.pnl_player_tracking)
    Me.TabPoints.Location = New System.Drawing.Point(4, 22)
    Me.TabPoints.Name = "TabPoints"
    Me.TabPoints.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPoints.Size = New System.Drawing.Size(1073, 678)
    Me.TabPoints.TabIndex = 2
    Me.TabPoints.Text = "xTabPuntos"
    Me.TabPoints.UseVisualStyleBackColor = True
    '
    'pnl_player_tracking
    '
    Me.pnl_player_tracking.Controls.Add(Me.gb_lower_panel)
    Me.pnl_player_tracking.Controls.Add(Me.gb_level)
    Me.pnl_player_tracking.Location = New System.Drawing.Point(3, 3)
    Me.pnl_player_tracking.Name = "pnl_player_tracking"
    Me.pnl_player_tracking.Size = New System.Drawing.Size(1062, 596)
    Me.pnl_player_tracking.TabIndex = 0
    '
    'gb_lower_panel
    '
    Me.gb_lower_panel.Controls.Add(Me.lbl_points_expiration_msg)
    Me.gb_lower_panel.Controls.Add(Me.gb_gift_expiration)
    Me.gb_lower_panel.Controls.Add(Me.gb_providers)
    Me.gb_lower_panel.Controls.Add(Me.gb_playsession)
    Me.gb_lower_panel.Location = New System.Drawing.Point(3, 299)
    Me.gb_lower_panel.Name = "gb_lower_panel"
    Me.gb_lower_panel.Size = New System.Drawing.Size(1052, 294)
    Me.gb_lower_panel.TabIndex = 14
    Me.gb_lower_panel.TabStop = False
    '
    'lbl_points_expiration_msg
    '
    Me.lbl_points_expiration_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_points_expiration_msg.ForeColor = System.Drawing.Color.Navy
    Me.lbl_points_expiration_msg.Location = New System.Drawing.Point(308, 78)
    Me.lbl_points_expiration_msg.Name = "lbl_points_expiration_msg"
    Me.lbl_points_expiration_msg.Size = New System.Drawing.Size(392, 23)
    Me.lbl_points_expiration_msg.TabIndex = 22
    Me.lbl_points_expiration_msg.Text = "x* La caducidad de puntos se debe realizar en los Buckets"
    Me.lbl_points_expiration_msg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'gb_gift_expiration
    '
    Me.gb_gift_expiration.Controls.Add(Me.uc_entry_gift_voucher_expiration)
    Me.gb_gift_expiration.Location = New System.Drawing.Point(310, 20)
    Me.gb_gift_expiration.Name = "gb_gift_expiration"
    Me.gb_gift_expiration.Size = New System.Drawing.Size(254, 47)
    Me.gb_gift_expiration.TabIndex = 21
    Me.gb_gift_expiration.TabStop = False
    Me.gb_gift_expiration.Text = "xGiftExpiration"
    '
    'uc_entry_gift_voucher_expiration
    '
    Me.uc_entry_gift_voucher_expiration.DoubleValue = 0.0R
    Me.uc_entry_gift_voucher_expiration.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_entry_gift_voucher_expiration.IntegerValue = 0
    Me.uc_entry_gift_voucher_expiration.IsReadOnly = False
    Me.uc_entry_gift_voucher_expiration.Location = New System.Drawing.Point(24, 16)
    Me.uc_entry_gift_voucher_expiration.Name = "uc_entry_gift_voucher_expiration"
    Me.uc_entry_gift_voucher_expiration.PlaceHolder = Nothing
    Me.uc_entry_gift_voucher_expiration.Size = New System.Drawing.Size(227, 24)
    Me.uc_entry_gift_voucher_expiration.SufixText = "xdays"
    Me.uc_entry_gift_voucher_expiration.SufixTextVisible = True
    Me.uc_entry_gift_voucher_expiration.SufixTextWidth = 125
    Me.uc_entry_gift_voucher_expiration.TabIndex = 0
    Me.uc_entry_gift_voucher_expiration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_gift_voucher_expiration.TextValue = ""
    Me.uc_entry_gift_voucher_expiration.TextVisible = False
    Me.uc_entry_gift_voucher_expiration.TextWidth = 50
    Me.uc_entry_gift_voucher_expiration.Value = ""
    Me.uc_entry_gift_voucher_expiration.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_providers
    '
    Me.gb_providers.Controls.Add(Me.dg_providers)
    Me.gb_providers.Location = New System.Drawing.Point(20, 13)
    Me.gb_providers.Name = "gb_providers"
    Me.gb_providers.Size = New System.Drawing.Size(280, 276)
    Me.gb_providers.TabIndex = 17
    Me.gb_providers.TabStop = False
    Me.gb_providers.Text = "xPointsMultiplierPerProvider"
    '
    'dg_providers
    '
    Me.dg_providers.CurrentCol = -1
    Me.dg_providers.CurrentRow = -1
    Me.dg_providers.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_providers.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_providers.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_providers.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_providers.Location = New System.Drawing.Point(3, 17)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.PanelRightVisible = False
    Me.dg_providers.Redraw = True
    Me.dg_providers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_providers.Size = New System.Drawing.Size(274, 256)
    Me.dg_providers.Sortable = False
    Me.dg_providers.SortAscending = True
    Me.dg_providers.SortByCol = 0
    Me.dg_providers.TabIndex = 0
    Me.dg_providers.ToolTipped = True
    Me.dg_providers.TopRow = -2
    Me.dg_providers.WordWrap = False
    '
    'gb_playsession
    '
    Me.gb_playsession.Controls.Add(Me.chk_max_points)
    Me.gb_playsession.Controls.Add(Me.chk_max_points_per_minute)
    Me.gb_playsession.Controls.Add(Me.chk_has_mismatch)
    Me.gb_playsession.Controls.Add(Me.chk_has_zero_plays)
    Me.gb_playsession.Controls.Add(Me.uc_max_points_per_minute)
    Me.gb_playsession.Controls.Add(Me.uc_max_points)
    Me.gb_playsession.Location = New System.Drawing.Point(713, 20)
    Me.gb_playsession.Name = "gb_playsession"
    Me.gb_playsession.Size = New System.Drawing.Size(326, 137)
    Me.gb_playsession.TabIndex = 16
    Me.gb_playsession.TabStop = False
    Me.gb_playsession.Text = "xPlaySession"
    '
    'chk_max_points
    '
    Me.chk_max_points.AutoSize = True
    Me.chk_max_points.Location = New System.Drawing.Point(6, 28)
    Me.chk_max_points.Name = "chk_max_points"
    Me.chk_max_points.Size = New System.Drawing.Size(15, 14)
    Me.chk_max_points.TabIndex = 1
    Me.chk_max_points.UseVisualStyleBackColor = True
    '
    'chk_max_points_per_minute
    '
    Me.chk_max_points_per_minute.AutoSize = True
    Me.chk_max_points_per_minute.Location = New System.Drawing.Point(6, 58)
    Me.chk_max_points_per_minute.Name = "chk_max_points_per_minute"
    Me.chk_max_points_per_minute.Size = New System.Drawing.Size(15, 14)
    Me.chk_max_points_per_minute.TabIndex = 3
    Me.chk_max_points_per_minute.UseVisualStyleBackColor = True
    '
    'chk_has_mismatch
    '
    Me.chk_has_mismatch.AutoSize = True
    Me.chk_has_mismatch.Location = New System.Drawing.Point(6, 114)
    Me.chk_has_mismatch.Name = "chk_has_mismatch"
    Me.chk_has_mismatch.Size = New System.Drawing.Size(107, 17)
    Me.chk_has_mismatch.TabIndex = 5
    Me.chk_has_mismatch.Text = "xHasMismatch"
    Me.chk_has_mismatch.UseVisualStyleBackColor = True
    '
    'chk_has_zero_plays
    '
    Me.chk_has_zero_plays.AutoSize = True
    Me.chk_has_zero_plays.Location = New System.Drawing.Point(6, 88)
    Me.chk_has_zero_plays.Name = "chk_has_zero_plays"
    Me.chk_has_zero_plays.Size = New System.Drawing.Size(111, 17)
    Me.chk_has_zero_plays.TabIndex = 4
    Me.chk_has_zero_plays.Text = "xHasZeroPlays"
    Me.chk_has_zero_plays.UseVisualStyleBackColor = True
    '
    'uc_max_points_per_minute
    '
    Me.uc_max_points_per_minute.DoubleValue = 0.0R
    Me.uc_max_points_per_minute.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_max_points_per_minute.IntegerValue = 0
    Me.uc_max_points_per_minute.IsReadOnly = False
    Me.uc_max_points_per_minute.Location = New System.Drawing.Point(20, 51)
    Me.uc_max_points_per_minute.Name = "uc_max_points_per_minute"
    Me.uc_max_points_per_minute.PlaceHolder = Nothing
    Me.uc_max_points_per_minute.Size = New System.Drawing.Size(300, 24)
    Me.uc_max_points_per_minute.SufixText = "xpoints/min"
    Me.uc_max_points_per_minute.SufixTextVisible = True
    Me.uc_max_points_per_minute.SufixTextWidth = 75
    Me.uc_max_points_per_minute.TabIndex = 2
    Me.uc_max_points_per_minute.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_max_points_per_minute.TextValue = ""
    Me.uc_max_points_per_minute.TextVisible = False
    Me.uc_max_points_per_minute.TextWidth = 150
    Me.uc_max_points_per_minute.Value = ""
    Me.uc_max_points_per_minute.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_max_points
    '
    Me.uc_max_points.DoubleValue = 0.0R
    Me.uc_max_points.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_max_points.IntegerValue = 0
    Me.uc_max_points.IsReadOnly = False
    Me.uc_max_points.Location = New System.Drawing.Point(20, 20)
    Me.uc_max_points.Name = "uc_max_points"
    Me.uc_max_points.PlaceHolder = Nothing
    Me.uc_max_points.Size = New System.Drawing.Size(300, 24)
    Me.uc_max_points.SufixText = "xpoints"
    Me.uc_max_points.SufixTextVisible = True
    Me.uc_max_points.SufixTextWidth = 75
    Me.uc_max_points.TabIndex = 0
    Me.uc_max_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_max_points.TextValue = ""
    Me.uc_max_points.TextVisible = False
    Me.uc_max_points.TextWidth = 150
    Me.uc_max_points.Value = ""
    Me.uc_max_points.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.tab_control_points)
    Me.gb_level.Controls.Add(Me.uc_entry_levels_days_counting_points)
    Me.gb_level.Controls.Add(Me.uc_entry_04_name)
    Me.gb_level.Controls.Add(Me.lbl_04_name)
    Me.gb_level.Controls.Add(Me.lbl_name)
    Me.gb_level.Controls.Add(Me.uc_entry_03_name)
    Me.gb_level.Controls.Add(Me.lbl_suffix_01)
    Me.gb_level.Controls.Add(Me.uc_entry_02_name)
    Me.gb_level.Controls.Add(Me.uc_entry_01_name)
    Me.gb_level.Controls.Add(Me.lbl_03_name)
    Me.gb_level.Controls.Add(Me.lbl_02_name)
    Me.gb_level.Controls.Add(Me.lbl_01_name)
    Me.gb_level.Controls.Add(Me.gb_permanency)
    Me.gb_level.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_level.Location = New System.Drawing.Point(3, 3)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(1052, 296)
    Me.gb_level.TabIndex = 0
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevels"
    '
    'tab_control_points
    '
    Me.tab_control_points.Controls.Add(Me.TabPointsDef)
    Me.tab_control_points.Controls.Add(Me.TabLock)
    Me.tab_control_points.Controls.Add(Me.TabPlayerLevelIcons)
    Me.tab_control_points.Location = New System.Drawing.Point(288, 60)
    Me.tab_control_points.Name = "tab_control_points"
    Me.tab_control_points.SelectedIndex = 0
    Me.tab_control_points.Size = New System.Drawing.Size(325, 194)
    Me.tab_control_points.TabIndex = 6
    '
    'TabPointsDef
    '
    Me.TabPointsDef.Controls.Add(Me.lbl_points_program)
    Me.TabPointsDef.Controls.Add(Me.uc_entry_04_points_to_enter)
    Me.TabPointsDef.Controls.Add(Me.uc_entry_03_points_to_enter)
    Me.TabPointsDef.Controls.Add(Me.uc_entry_02_points_to_enter)
    Me.TabPointsDef.Controls.Add(Me.lbl_points_to_enter)
    Me.TabPointsDef.Location = New System.Drawing.Point(4, 22)
    Me.TabPointsDef.Name = "TabPointsDef"
    Me.TabPointsDef.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPointsDef.Size = New System.Drawing.Size(317, 168)
    Me.TabPointsDef.TabIndex = 0
    Me.TabPointsDef.Text = "xPuntos"
    Me.TabPointsDef.UseVisualStyleBackColor = True
    '
    'lbl_points_program
    '
    Me.lbl_points_program.AutoSize = True
    Me.lbl_points_program.Location = New System.Drawing.Point(15, 44)
    Me.lbl_points_program.Name = "lbl_points_program"
    Me.lbl_points_program.Size = New System.Drawing.Size(146, 13)
    Me.lbl_points_program.TabIndex = 63
    Me.lbl_points_program.Text = "xMessagePointsProgram"
    '
    'uc_entry_04_points_to_enter
    '
    Me.uc_entry_04_points_to_enter.DoubleValue = 0.0R
    Me.uc_entry_04_points_to_enter.IntegerValue = 0
    Me.uc_entry_04_points_to_enter.IsReadOnly = False
    Me.uc_entry_04_points_to_enter.Location = New System.Drawing.Point(238, 134)
    Me.uc_entry_04_points_to_enter.Name = "uc_entry_04_points_to_enter"
    Me.uc_entry_04_points_to_enter.PlaceHolder = Nothing
    Me.uc_entry_04_points_to_enter.Size = New System.Drawing.Size(76, 24)
    Me.uc_entry_04_points_to_enter.SufixText = "Sufix Text"
    Me.uc_entry_04_points_to_enter.TabIndex = 14
    Me.uc_entry_04_points_to_enter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_04_points_to_enter.TextValue = ""
    Me.uc_entry_04_points_to_enter.TextVisible = False
    Me.uc_entry_04_points_to_enter.TextWidth = 0
    Me.uc_entry_04_points_to_enter.Value = ""
    Me.uc_entry_04_points_to_enter.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_03_points_to_enter
    '
    Me.uc_entry_03_points_to_enter.DoubleValue = 0.0R
    Me.uc_entry_03_points_to_enter.IntegerValue = 0
    Me.uc_entry_03_points_to_enter.IsReadOnly = False
    Me.uc_entry_03_points_to_enter.Location = New System.Drawing.Point(238, 104)
    Me.uc_entry_03_points_to_enter.Name = "uc_entry_03_points_to_enter"
    Me.uc_entry_03_points_to_enter.PlaceHolder = Nothing
    Me.uc_entry_03_points_to_enter.Size = New System.Drawing.Size(76, 24)
    Me.uc_entry_03_points_to_enter.SufixText = "Sufix Text"
    Me.uc_entry_03_points_to_enter.TabIndex = 10
    Me.uc_entry_03_points_to_enter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_03_points_to_enter.TextValue = ""
    Me.uc_entry_03_points_to_enter.TextVisible = False
    Me.uc_entry_03_points_to_enter.TextWidth = 0
    Me.uc_entry_03_points_to_enter.Value = ""
    Me.uc_entry_03_points_to_enter.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_02_points_to_enter
    '
    Me.uc_entry_02_points_to_enter.DoubleValue = 0.0R
    Me.uc_entry_02_points_to_enter.IntegerValue = 0
    Me.uc_entry_02_points_to_enter.IsReadOnly = False
    Me.uc_entry_02_points_to_enter.Location = New System.Drawing.Point(238, 74)
    Me.uc_entry_02_points_to_enter.Name = "uc_entry_02_points_to_enter"
    Me.uc_entry_02_points_to_enter.PlaceHolder = Nothing
    Me.uc_entry_02_points_to_enter.Size = New System.Drawing.Size(76, 24)
    Me.uc_entry_02_points_to_enter.SufixText = "Sufix Text"
    Me.uc_entry_02_points_to_enter.TabIndex = 6
    Me.uc_entry_02_points_to_enter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_02_points_to_enter.TextValue = ""
    Me.uc_entry_02_points_to_enter.TextVisible = False
    Me.uc_entry_02_points_to_enter.TextWidth = 0
    Me.uc_entry_02_points_to_enter.Value = ""
    Me.uc_entry_02_points_to_enter.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_points_to_enter
    '
    Me.lbl_points_to_enter.Location = New System.Drawing.Point(238, 6)
    Me.lbl_points_to_enter.Name = "lbl_points_to_enter"
    Me.lbl_points_to_enter.Size = New System.Drawing.Size(72, 33)
    Me.lbl_points_to_enter.TabIndex = 62
    Me.lbl_points_to_enter.Text = "xPuntos Entrada"
    Me.lbl_points_to_enter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'TabLock
    '
    Me.TabLock.Controls.Add(Me.uc_cmb_Level4)
    Me.TabLock.Controls.Add(Me.uc_cmb_Level3)
    Me.TabLock.Controls.Add(Me.uc_cmb_Level2)
    Me.TabLock.Controls.Add(Me.uc_cmb_Level1)
    Me.TabLock.Location = New System.Drawing.Point(4, 22)
    Me.TabLock.Name = "TabLock"
    Me.TabLock.Padding = New System.Windows.Forms.Padding(3)
    Me.TabLock.Size = New System.Drawing.Size(317, 168)
    Me.TabLock.TabIndex = 1
    Me.TabLock.Text = "xBloqueoAutomatico"
    Me.TabLock.UseVisualStyleBackColor = True
    '
    'uc_cmb_Level4
    '
    Me.uc_cmb_Level4.AllowUnlistedValues = False
    Me.uc_cmb_Level4.AutoCompleteMode = False
    Me.uc_cmb_Level4.IsReadOnly = False
    Me.uc_cmb_Level4.Location = New System.Drawing.Point(6, 134)
    Me.uc_cmb_Level4.Name = "uc_cmb_Level4"
    Me.uc_cmb_Level4.SelectedIndex = -1
    Me.uc_cmb_Level4.Size = New System.Drawing.Size(260, 24)
    Me.uc_cmb_Level4.SufixText = "Sufix Text"
    Me.uc_cmb_Level4.TabIndex = 3
    Me.uc_cmb_Level4.TextCombo = Nothing
    Me.uc_cmb_Level4.TextVisible = False
    Me.uc_cmb_Level4.TextWidth = 0
    '
    'uc_cmb_Level3
    '
    Me.uc_cmb_Level3.AllowUnlistedValues = False
    Me.uc_cmb_Level3.AutoCompleteMode = False
    Me.uc_cmb_Level3.IsReadOnly = False
    Me.uc_cmb_Level3.Location = New System.Drawing.Point(6, 104)
    Me.uc_cmb_Level3.Name = "uc_cmb_Level3"
    Me.uc_cmb_Level3.SelectedIndex = -1
    Me.uc_cmb_Level3.Size = New System.Drawing.Size(260, 24)
    Me.uc_cmb_Level3.SufixText = "Sufix Text"
    Me.uc_cmb_Level3.TabIndex = 2
    Me.uc_cmb_Level3.TextCombo = Nothing
    Me.uc_cmb_Level3.TextVisible = False
    Me.uc_cmb_Level3.TextWidth = 0
    '
    'uc_cmb_Level2
    '
    Me.uc_cmb_Level2.AllowUnlistedValues = False
    Me.uc_cmb_Level2.AutoCompleteMode = False
    Me.uc_cmb_Level2.IsReadOnly = False
    Me.uc_cmb_Level2.Location = New System.Drawing.Point(6, 74)
    Me.uc_cmb_Level2.Name = "uc_cmb_Level2"
    Me.uc_cmb_Level2.SelectedIndex = -1
    Me.uc_cmb_Level2.Size = New System.Drawing.Size(260, 24)
    Me.uc_cmb_Level2.SufixText = "Sufix Text"
    Me.uc_cmb_Level2.TabIndex = 1
    Me.uc_cmb_Level2.TextCombo = Nothing
    Me.uc_cmb_Level2.TextVisible = False
    Me.uc_cmb_Level2.TextWidth = 0
    '
    'uc_cmb_Level1
    '
    Me.uc_cmb_Level1.AllowUnlistedValues = False
    Me.uc_cmb_Level1.AutoCompleteMode = False
    Me.uc_cmb_Level1.IsReadOnly = False
    Me.uc_cmb_Level1.Location = New System.Drawing.Point(6, 44)
    Me.uc_cmb_Level1.Name = "uc_cmb_Level1"
    Me.uc_cmb_Level1.SelectedIndex = -1
    Me.uc_cmb_Level1.Size = New System.Drawing.Size(260, 24)
    Me.uc_cmb_Level1.SufixText = "Sufix Text"
    Me.uc_cmb_Level1.TabIndex = 0
    Me.uc_cmb_Level1.TextCombo = Nothing
    Me.uc_cmb_Level1.TextVisible = False
    Me.uc_cmb_Level1.TextWidth = 0
    '
    'TabPlayerLevelIcons
    '
    Me.TabPlayerLevelIcons.Controls.Add(Me.pb_level_04)
    Me.TabPlayerLevelIcons.Controls.Add(Me.pb_level_03)
    Me.TabPlayerLevelIcons.Controls.Add(Me.pb_level_02)
    Me.TabPlayerLevelIcons.Controls.Add(Me.pb_level_01)
    Me.TabPlayerLevelIcons.Controls.Add(Me.cmb_player_level_icon_04)
    Me.TabPlayerLevelIcons.Controls.Add(Me.cmb_player_level_icon_03)
    Me.TabPlayerLevelIcons.Controls.Add(Me.cmb_player_level_icon_02)
    Me.TabPlayerLevelIcons.Controls.Add(Me.cmb_player_level_icon_01)
    Me.TabPlayerLevelIcons.Location = New System.Drawing.Point(4, 22)
    Me.TabPlayerLevelIcons.Name = "TabPlayerLevelIcons"
    Me.TabPlayerLevelIcons.Size = New System.Drawing.Size(317, 168)
    Me.TabPlayerLevelIcons.TabIndex = 2
    Me.TabPlayerLevelIcons.Text = "xColorFondoIconos"
    Me.TabPlayerLevelIcons.UseVisualStyleBackColor = True
    '
    'pb_level_04
    '
    Me.pb_level_04.Location = New System.Drawing.Point(222, 136)
    Me.pb_level_04.Name = "pb_level_04"
    Me.pb_level_04.Size = New System.Drawing.Size(29, 29)
    Me.pb_level_04.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_level_04.TabIndex = 5
    Me.pb_level_04.TabStop = False
    '
    'pb_level_03
    '
    Me.pb_level_03.Location = New System.Drawing.Point(222, 104)
    Me.pb_level_03.Name = "pb_level_03"
    Me.pb_level_03.Size = New System.Drawing.Size(29, 29)
    Me.pb_level_03.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_level_03.TabIndex = 5
    Me.pb_level_03.TabStop = False
    '
    'pb_level_02
    '
    Me.pb_level_02.Location = New System.Drawing.Point(222, 72)
    Me.pb_level_02.Name = "pb_level_02"
    Me.pb_level_02.Size = New System.Drawing.Size(29, 29)
    Me.pb_level_02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_level_02.TabIndex = 5
    Me.pb_level_02.TabStop = False
    '
    'pb_level_01
    '
    Me.pb_level_01.Location = New System.Drawing.Point(222, 40)
    Me.pb_level_01.Name = "pb_level_01"
    Me.pb_level_01.Size = New System.Drawing.Size(29, 29)
    Me.pb_level_01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.pb_level_01.TabIndex = 5
    Me.pb_level_01.TabStop = False
    '
    'cmb_player_level_icon_04
    '
    Me.cmb_player_level_icon_04.AllowUnlistedValues = False
    Me.cmb_player_level_icon_04.AutoCompleteMode = False
    Me.cmb_player_level_icon_04.IsReadOnly = False
    Me.cmb_player_level_icon_04.Location = New System.Drawing.Point(17, 135)
    Me.cmb_player_level_icon_04.Name = "cmb_player_level_icon_04"
    Me.cmb_player_level_icon_04.SelectedIndex = -1
    Me.cmb_player_level_icon_04.Size = New System.Drawing.Size(184, 24)
    Me.cmb_player_level_icon_04.SufixText = "Sufix Text"
    Me.cmb_player_level_icon_04.SufixTextVisible = True
    Me.cmb_player_level_icon_04.TabIndex = 4
    Me.cmb_player_level_icon_04.TextCombo = Nothing
    Me.cmb_player_level_icon_04.TextVisible = False
    Me.cmb_player_level_icon_04.TextWidth = 0
    '
    'cmb_player_level_icon_03
    '
    Me.cmb_player_level_icon_03.AllowUnlistedValues = False
    Me.cmb_player_level_icon_03.AutoCompleteMode = False
    Me.cmb_player_level_icon_03.IsReadOnly = False
    Me.cmb_player_level_icon_03.Location = New System.Drawing.Point(17, 105)
    Me.cmb_player_level_icon_03.Name = "cmb_player_level_icon_03"
    Me.cmb_player_level_icon_03.SelectedIndex = -1
    Me.cmb_player_level_icon_03.Size = New System.Drawing.Size(184, 24)
    Me.cmb_player_level_icon_03.SufixText = "Sufix Text"
    Me.cmb_player_level_icon_03.SufixTextVisible = True
    Me.cmb_player_level_icon_03.TabIndex = 3
    Me.cmb_player_level_icon_03.TextCombo = Nothing
    Me.cmb_player_level_icon_03.TextVisible = False
    Me.cmb_player_level_icon_03.TextWidth = 0
    '
    'cmb_player_level_icon_02
    '
    Me.cmb_player_level_icon_02.AllowUnlistedValues = False
    Me.cmb_player_level_icon_02.AutoCompleteMode = False
    Me.cmb_player_level_icon_02.IsReadOnly = False
    Me.cmb_player_level_icon_02.Location = New System.Drawing.Point(17, 74)
    Me.cmb_player_level_icon_02.Name = "cmb_player_level_icon_02"
    Me.cmb_player_level_icon_02.SelectedIndex = -1
    Me.cmb_player_level_icon_02.Size = New System.Drawing.Size(184, 24)
    Me.cmb_player_level_icon_02.SufixText = "Sufix Text"
    Me.cmb_player_level_icon_02.SufixTextVisible = True
    Me.cmb_player_level_icon_02.TabIndex = 2
    Me.cmb_player_level_icon_02.TextCombo = Nothing
    Me.cmb_player_level_icon_02.TextVisible = False
    Me.cmb_player_level_icon_02.TextWidth = 0
    '
    'cmb_player_level_icon_01
    '
    Me.cmb_player_level_icon_01.AllowUnlistedValues = False
    Me.cmb_player_level_icon_01.AutoCompleteMode = False
    Me.cmb_player_level_icon_01.IsReadOnly = False
    Me.cmb_player_level_icon_01.Location = New System.Drawing.Point(17, 43)
    Me.cmb_player_level_icon_01.Name = "cmb_player_level_icon_01"
    Me.cmb_player_level_icon_01.SelectedIndex = -1
    Me.cmb_player_level_icon_01.Size = New System.Drawing.Size(184, 24)
    Me.cmb_player_level_icon_01.SufixText = "Sufix Text"
    Me.cmb_player_level_icon_01.SufixTextVisible = True
    Me.cmb_player_level_icon_01.TabIndex = 1
    Me.cmb_player_level_icon_01.TextCombo = Nothing
    Me.cmb_player_level_icon_01.TextVisible = False
    Me.cmb_player_level_icon_01.TextWidth = 0
    '
    'uc_entry_levels_days_counting_points
    '
    Me.uc_entry_levels_days_counting_points.DoubleValue = 0.0R
    Me.uc_entry_levels_days_counting_points.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_entry_levels_days_counting_points.IntegerValue = 0
    Me.uc_entry_levels_days_counting_points.IsReadOnly = False
    Me.uc_entry_levels_days_counting_points.Location = New System.Drawing.Point(99, 266)
    Me.uc_entry_levels_days_counting_points.Name = "uc_entry_levels_days_counting_points"
    Me.uc_entry_levels_days_counting_points.PlaceHolder = Nothing
    Me.uc_entry_levels_days_counting_points.Size = New System.Drawing.Size(499, 24)
    Me.uc_entry_levels_days_counting_points.SufixText = "xdays (study period)"
    Me.uc_entry_levels_days_counting_points.SufixTextVisible = True
    Me.uc_entry_levels_days_counting_points.SufixTextWidth = 170
    Me.uc_entry_levels_days_counting_points.TabIndex = 7
    Me.uc_entry_levels_days_counting_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_levels_days_counting_points.TextValue = ""
    Me.uc_entry_levels_days_counting_points.TextVisible = False
    Me.uc_entry_levels_days_counting_points.TextWidth = 270
    Me.uc_entry_levels_days_counting_points.Value = ""
    Me.uc_entry_levels_days_counting_points.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_04_name
    '
    Me.uc_entry_04_name.DoubleValue = 0.0R
    Me.uc_entry_04_name.IntegerValue = 0
    Me.uc_entry_04_name.IsReadOnly = False
    Me.uc_entry_04_name.Location = New System.Drawing.Point(96, 216)
    Me.uc_entry_04_name.Name = "uc_entry_04_name"
    Me.uc_entry_04_name.PlaceHolder = Nothing
    Me.uc_entry_04_name.Size = New System.Drawing.Size(186, 24)
    Me.uc_entry_04_name.SufixText = "Sufix Text"
    Me.uc_entry_04_name.SufixTextVisible = True
    Me.uc_entry_04_name.TabIndex = 5
    Me.uc_entry_04_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_04_name.TextValue = ""
    Me.uc_entry_04_name.TextVisible = False
    Me.uc_entry_04_name.TextWidth = 0
    Me.uc_entry_04_name.Value = ""
    Me.uc_entry_04_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_04_name
    '
    Me.lbl_04_name.Location = New System.Drawing.Point(23, 216)
    Me.lbl_04_name.Name = "lbl_04_name"
    Me.lbl_04_name.Size = New System.Drawing.Size(67, 25)
    Me.lbl_04_name.TabIndex = 38
    Me.lbl_04_name.Text = "xLevel 04"
    Me.lbl_04_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_name
    '
    Me.lbl_name.Location = New System.Drawing.Point(96, 98)
    Me.lbl_name.Name = "lbl_name"
    Me.lbl_name.Size = New System.Drawing.Size(186, 24)
    Me.lbl_name.TabIndex = 1
    Me.lbl_name.Text = "xNombre"
    Me.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'uc_entry_03_name
    '
    Me.uc_entry_03_name.DoubleValue = 0.0R
    Me.uc_entry_03_name.IntegerValue = 0
    Me.uc_entry_03_name.IsReadOnly = False
    Me.uc_entry_03_name.Location = New System.Drawing.Point(96, 186)
    Me.uc_entry_03_name.Name = "uc_entry_03_name"
    Me.uc_entry_03_name.PlaceHolder = Nothing
    Me.uc_entry_03_name.Size = New System.Drawing.Size(186, 24)
    Me.uc_entry_03_name.SufixText = "Sufix Text"
    Me.uc_entry_03_name.SufixTextVisible = True
    Me.uc_entry_03_name.TabIndex = 4
    Me.uc_entry_03_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_03_name.TextValue = ""
    Me.uc_entry_03_name.TextVisible = False
    Me.uc_entry_03_name.TextWidth = 0
    Me.uc_entry_03_name.Value = ""
    Me.uc_entry_03_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_suffix_01
    '
    Me.lbl_suffix_01.ForeColor = System.Drawing.Color.Navy
    Me.lbl_suffix_01.Location = New System.Drawing.Point(112, 17)
    Me.lbl_suffix_01.Name = "lbl_suffix_01"
    Me.lbl_suffix_01.Size = New System.Drawing.Size(405, 24)
    Me.lbl_suffix_01.TabIndex = 0
    Me.lbl_suffix_01.Text = "xSe obtienen puntos por cada una de las cantidades siguientes."
    Me.lbl_suffix_01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'uc_entry_02_name
    '
    Me.uc_entry_02_name.DoubleValue = 0.0R
    Me.uc_entry_02_name.IntegerValue = 0
    Me.uc_entry_02_name.IsReadOnly = False
    Me.uc_entry_02_name.Location = New System.Drawing.Point(96, 156)
    Me.uc_entry_02_name.Name = "uc_entry_02_name"
    Me.uc_entry_02_name.PlaceHolder = Nothing
    Me.uc_entry_02_name.Size = New System.Drawing.Size(186, 24)
    Me.uc_entry_02_name.SufixText = "Sufix Text"
    Me.uc_entry_02_name.SufixTextVisible = True
    Me.uc_entry_02_name.TabIndex = 3
    Me.uc_entry_02_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_02_name.TextValue = ""
    Me.uc_entry_02_name.TextVisible = False
    Me.uc_entry_02_name.TextWidth = 0
    Me.uc_entry_02_name.Value = ""
    Me.uc_entry_02_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_01_name
    '
    Me.uc_entry_01_name.DoubleValue = 0.0R
    Me.uc_entry_01_name.IntegerValue = 0
    Me.uc_entry_01_name.IsReadOnly = False
    Me.uc_entry_01_name.Location = New System.Drawing.Point(96, 126)
    Me.uc_entry_01_name.Name = "uc_entry_01_name"
    Me.uc_entry_01_name.PlaceHolder = Nothing
    Me.uc_entry_01_name.Size = New System.Drawing.Size(186, 24)
    Me.uc_entry_01_name.SufixText = "Sufix Text"
    Me.uc_entry_01_name.SufixTextVisible = True
    Me.uc_entry_01_name.TabIndex = 2
    Me.uc_entry_01_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_01_name.TextValue = ""
    Me.uc_entry_01_name.TextVisible = False
    Me.uc_entry_01_name.TextWidth = 0
    Me.uc_entry_01_name.Value = ""
    Me.uc_entry_01_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_03_name
    '
    Me.lbl_03_name.Location = New System.Drawing.Point(26, 186)
    Me.lbl_03_name.Name = "lbl_03_name"
    Me.lbl_03_name.Size = New System.Drawing.Size(64, 25)
    Me.lbl_03_name.TabIndex = 27
    Me.lbl_03_name.Text = "xLevel 03"
    Me.lbl_03_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_02_name
    '
    Me.lbl_02_name.Location = New System.Drawing.Point(26, 156)
    Me.lbl_02_name.Name = "lbl_02_name"
    Me.lbl_02_name.Size = New System.Drawing.Size(64, 25)
    Me.lbl_02_name.TabIndex = 26
    Me.lbl_02_name.Text = "xLevel 02"
    Me.lbl_02_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_01_name
    '
    Me.lbl_01_name.Location = New System.Drawing.Point(26, 126)
    Me.lbl_01_name.Name = "lbl_01_name"
    Me.lbl_01_name.Size = New System.Drawing.Size(64, 25)
    Me.lbl_01_name.TabIndex = 25
    Me.lbl_01_name.Text = "xLevel 01"
    Me.lbl_01_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_permanency
    '
    Me.gb_permanency.Controls.Add(Me.lbl_points_to_keep_information)
    Me.gb_permanency.Controls.Add(Me.lbl_expiration_day_month)
    Me.gb_permanency.Controls.Add(Me.lbl_points_to_keep)
    Me.gb_permanency.Controls.Add(Me.dtp_expiration_day_month)
    Me.gb_permanency.Controls.Add(Me.uc_entry_04_points_to_keep)
    Me.gb_permanency.Controls.Add(Me.uc_entry_03_points_to_keep)
    Me.gb_permanency.Controls.Add(Me.chk_expiration_day_month)
    Me.gb_permanency.Controls.Add(Me.uc_entry_02_points_to_keep)
    Me.gb_permanency.Controls.Add(Me.uc_entry_03_days_on_level)
    Me.gb_permanency.Controls.Add(Me.lbl_days_on_level)
    Me.gb_permanency.Controls.Add(Me.uc_entry_04_days_on_level)
    Me.gb_permanency.Controls.Add(Me.uc_entry_02_days_on_level)
    Me.gb_permanency.Controls.Add(Me.gb_inactivity)
    Me.gb_permanency.Location = New System.Drawing.Point(628, 15)
    Me.gb_permanency.Name = "gb_permanency"
    Me.gb_permanency.Size = New System.Drawing.Size(418, 275)
    Me.gb_permanency.TabIndex = 8
    Me.gb_permanency.TabStop = False
    Me.gb_permanency.Text = "xPermanencia"
    '
    'lbl_points_to_keep_information
    '
    Me.lbl_points_to_keep_information.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_points_to_keep_information.ForeColor = System.Drawing.Color.Navy
    Me.lbl_points_to_keep_information.Location = New System.Drawing.Point(6, 246)
    Me.lbl_points_to_keep_information.Name = "lbl_points_to_keep_information"
    Me.lbl_points_to_keep_information.Size = New System.Drawing.Size(405, 23)
    Me.lbl_points_to_keep_information.TabIndex = 12
    Me.lbl_points_to_keep_information.Text = "x* Evaluado en el per�odo de estudio."
    Me.lbl_points_to_keep_information.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_expiration_day_month
    '
    Me.lbl_expiration_day_month.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_expiration_day_month.ForeColor = System.Drawing.Color.Navy
    Me.lbl_expiration_day_month.Location = New System.Drawing.Point(6, 45)
    Me.lbl_expiration_day_month.Name = "lbl_expiration_day_month"
    Me.lbl_expiration_day_month.Size = New System.Drawing.Size(405, 27)
    Me.lbl_expiration_day_month.TabIndex = 2
    Me.lbl_expiration_day_month.Text = "xCuando el cliente accede al nivel y su permanencia en �l vaya a ser inferior al " & _
    "m�nimo configurado se le prolongar� la estancia 1 a�o adicional."
    '
    'lbl_points_to_keep
    '
    Me.lbl_points_to_keep.Location = New System.Drawing.Point(106, 108)
    Me.lbl_points_to_keep.Name = "lbl_points_to_keep"
    Me.lbl_points_to_keep.Size = New System.Drawing.Size(76, 28)
    Me.lbl_points_to_keep.TabIndex = 4
    Me.lbl_points_to_keep.Text = "xM�nimo Puntos *"
    Me.lbl_points_to_keep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'dtp_expiration_day_month
    '
    Me.dtp_expiration_day_month.CustomFormat = "dd MMMM"
    Me.dtp_expiration_day_month.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dtp_expiration_day_month.Location = New System.Drawing.Point(235, 21)
    Me.dtp_expiration_day_month.Name = "dtp_expiration_day_month"
    Me.dtp_expiration_day_month.ShowUpDown = True
    Me.dtp_expiration_day_month.Size = New System.Drawing.Size(123, 21)
    Me.dtp_expiration_day_month.TabIndex = 1
    Me.dtp_expiration_day_month.Value = New Date(2001, 1, 1, 0, 0, 0, 0)
    '
    'uc_entry_04_points_to_keep
    '
    Me.uc_entry_04_points_to_keep.DoubleValue = 0.0R
    Me.uc_entry_04_points_to_keep.IntegerValue = 0
    Me.uc_entry_04_points_to_keep.IsReadOnly = False
    Me.uc_entry_04_points_to_keep.Location = New System.Drawing.Point(106, 202)
    Me.uc_entry_04_points_to_keep.Name = "uc_entry_04_points_to_keep"
    Me.uc_entry_04_points_to_keep.PlaceHolder = Nothing
    Me.uc_entry_04_points_to_keep.Size = New System.Drawing.Size(76, 24)
    Me.uc_entry_04_points_to_keep.SufixText = "Sufix Text"
    Me.uc_entry_04_points_to_keep.SufixTextVisible = True
    Me.uc_entry_04_points_to_keep.TabIndex = 10
    Me.uc_entry_04_points_to_keep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_04_points_to_keep.TextValue = ""
    Me.uc_entry_04_points_to_keep.TextVisible = False
    Me.uc_entry_04_points_to_keep.TextWidth = 0
    Me.uc_entry_04_points_to_keep.Value = ""
    Me.uc_entry_04_points_to_keep.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_03_points_to_keep
    '
    Me.uc_entry_03_points_to_keep.DoubleValue = 0.0R
    Me.uc_entry_03_points_to_keep.IntegerValue = 0
    Me.uc_entry_03_points_to_keep.IsReadOnly = False
    Me.uc_entry_03_points_to_keep.Location = New System.Drawing.Point(106, 172)
    Me.uc_entry_03_points_to_keep.Name = "uc_entry_03_points_to_keep"
    Me.uc_entry_03_points_to_keep.PlaceHolder = Nothing
    Me.uc_entry_03_points_to_keep.Size = New System.Drawing.Size(76, 24)
    Me.uc_entry_03_points_to_keep.SufixText = "Sufix Text"
    Me.uc_entry_03_points_to_keep.SufixTextVisible = True
    Me.uc_entry_03_points_to_keep.TabIndex = 8
    Me.uc_entry_03_points_to_keep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_03_points_to_keep.TextValue = ""
    Me.uc_entry_03_points_to_keep.TextVisible = False
    Me.uc_entry_03_points_to_keep.TextWidth = 0
    Me.uc_entry_03_points_to_keep.Value = ""
    Me.uc_entry_03_points_to_keep.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_expiration_day_month
    '
    Me.chk_expiration_day_month.AutoSize = True
    Me.chk_expiration_day_month.Location = New System.Drawing.Point(77, 22)
    Me.chk_expiration_day_month.Name = "chk_expiration_day_month"
    Me.chk_expiration_day_month.Size = New System.Drawing.Size(145, 17)
    Me.chk_expiration_day_month.TabIndex = 0
    Me.chk_expiration_day_month.Text = "xFecha de caducidad"
    Me.chk_expiration_day_month.UseVisualStyleBackColor = True
    '
    'uc_entry_02_points_to_keep
    '
    Me.uc_entry_02_points_to_keep.DoubleValue = 0.0R
    Me.uc_entry_02_points_to_keep.IntegerValue = 0
    Me.uc_entry_02_points_to_keep.IsReadOnly = False
    Me.uc_entry_02_points_to_keep.Location = New System.Drawing.Point(106, 142)
    Me.uc_entry_02_points_to_keep.Name = "uc_entry_02_points_to_keep"
    Me.uc_entry_02_points_to_keep.PlaceHolder = Nothing
    Me.uc_entry_02_points_to_keep.Size = New System.Drawing.Size(76, 24)
    Me.uc_entry_02_points_to_keep.SufixText = "Sufix Text"
    Me.uc_entry_02_points_to_keep.SufixTextVisible = True
    Me.uc_entry_02_points_to_keep.TabIndex = 6
    Me.uc_entry_02_points_to_keep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_02_points_to_keep.TextValue = ""
    Me.uc_entry_02_points_to_keep.TextVisible = False
    Me.uc_entry_02_points_to_keep.TextWidth = 0
    Me.uc_entry_02_points_to_keep.Value = ""
    Me.uc_entry_02_points_to_keep.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_03_days_on_level
    '
    Me.uc_entry_03_days_on_level.DoubleValue = 0.0R
    Me.uc_entry_03_days_on_level.IntegerValue = 0
    Me.uc_entry_03_days_on_level.IsReadOnly = False
    Me.uc_entry_03_days_on_level.Location = New System.Drawing.Point(20, 172)
    Me.uc_entry_03_days_on_level.Name = "uc_entry_03_days_on_level"
    Me.uc_entry_03_days_on_level.PlaceHolder = Nothing
    Me.uc_entry_03_days_on_level.Size = New System.Drawing.Size(73, 24)
    Me.uc_entry_03_days_on_level.SufixText = "Sufix Text"
    Me.uc_entry_03_days_on_level.SufixTextVisible = True
    Me.uc_entry_03_days_on_level.TabIndex = 7
    Me.uc_entry_03_days_on_level.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_03_days_on_level.TextValue = ""
    Me.uc_entry_03_days_on_level.TextVisible = False
    Me.uc_entry_03_days_on_level.TextWidth = 0
    Me.uc_entry_03_days_on_level.Value = ""
    Me.uc_entry_03_days_on_level.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_days_on_level
    '
    Me.lbl_days_on_level.Location = New System.Drawing.Point(5, 108)
    Me.lbl_days_on_level.Name = "lbl_days_on_level"
    Me.lbl_days_on_level.Size = New System.Drawing.Size(108, 28)
    Me.lbl_days_on_level.TabIndex = 3
    Me.lbl_days_on_level.Text = "xD�as de Permanencia"
    Me.lbl_days_on_level.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'uc_entry_04_days_on_level
    '
    Me.uc_entry_04_days_on_level.DoubleValue = 0.0R
    Me.uc_entry_04_days_on_level.IntegerValue = 0
    Me.uc_entry_04_days_on_level.IsReadOnly = False
    Me.uc_entry_04_days_on_level.Location = New System.Drawing.Point(20, 202)
    Me.uc_entry_04_days_on_level.Name = "uc_entry_04_days_on_level"
    Me.uc_entry_04_days_on_level.PlaceHolder = Nothing
    Me.uc_entry_04_days_on_level.Size = New System.Drawing.Size(73, 24)
    Me.uc_entry_04_days_on_level.SufixText = "Sufix Text"
    Me.uc_entry_04_days_on_level.SufixTextVisible = True
    Me.uc_entry_04_days_on_level.TabIndex = 9
    Me.uc_entry_04_days_on_level.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_04_days_on_level.TextValue = ""
    Me.uc_entry_04_days_on_level.TextVisible = False
    Me.uc_entry_04_days_on_level.TextWidth = 0
    Me.uc_entry_04_days_on_level.Value = ""
    Me.uc_entry_04_days_on_level.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_02_days_on_level
    '
    Me.uc_entry_02_days_on_level.DoubleValue = 0.0R
    Me.uc_entry_02_days_on_level.IntegerValue = 0
    Me.uc_entry_02_days_on_level.IsReadOnly = False
    Me.uc_entry_02_days_on_level.Location = New System.Drawing.Point(20, 142)
    Me.uc_entry_02_days_on_level.Name = "uc_entry_02_days_on_level"
    Me.uc_entry_02_days_on_level.PlaceHolder = Nothing
    Me.uc_entry_02_days_on_level.Size = New System.Drawing.Size(73, 24)
    Me.uc_entry_02_days_on_level.SufixText = "Sufix Text"
    Me.uc_entry_02_days_on_level.SufixTextVisible = True
    Me.uc_entry_02_days_on_level.TabIndex = 5
    Me.uc_entry_02_days_on_level.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_02_days_on_level.TextValue = ""
    Me.uc_entry_02_days_on_level.TextVisible = False
    Me.uc_entry_02_days_on_level.TextWidth = 0
    Me.uc_entry_02_days_on_level.Value = ""
    Me.uc_entry_02_days_on_level.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_inactivity
    '
    Me.gb_inactivity.Controls.Add(Me.chk_max_days_no_activity)
    Me.gb_inactivity.Controls.Add(Me.uc_entry_03_max_days_no_activity)
    Me.gb_inactivity.Controls.Add(Me.lbl_max_days_no_activity)
    Me.gb_inactivity.Controls.Add(Me.uc_entry_02_max_days_no_activity)
    Me.gb_inactivity.Controls.Add(Me.uc_entry_04_max_days_no_activity)
    Me.gb_inactivity.Location = New System.Drawing.Point(209, 75)
    Me.gb_inactivity.Name = "gb_inactivity"
    Me.gb_inactivity.Size = New System.Drawing.Size(200, 168)
    Me.gb_inactivity.TabIndex = 11
    Me.gb_inactivity.TabStop = False
    Me.gb_inactivity.Text = "xInactivity"
    '
    'chk_max_days_no_activity
    '
    Me.chk_max_days_no_activity.AutoSize = True
    Me.chk_max_days_no_activity.Location = New System.Drawing.Point(12, 17)
    Me.chk_max_days_no_activity.Name = "chk_max_days_no_activity"
    Me.chk_max_days_no_activity.Size = New System.Drawing.Size(188, 17)
    Me.chk_max_days_no_activity.TabIndex = 0
    Me.chk_max_days_no_activity.Text = "xPierde nivel por inactividad"
    Me.chk_max_days_no_activity.UseVisualStyleBackColor = True
    '
    'uc_entry_03_max_days_no_activity
    '
    Me.uc_entry_03_max_days_no_activity.DoubleValue = 0.0R
    Me.uc_entry_03_max_days_no_activity.IntegerValue = 0
    Me.uc_entry_03_max_days_no_activity.IsReadOnly = False
    Me.uc_entry_03_max_days_no_activity.Location = New System.Drawing.Point(65, 97)
    Me.uc_entry_03_max_days_no_activity.Name = "uc_entry_03_max_days_no_activity"
    Me.uc_entry_03_max_days_no_activity.PlaceHolder = Nothing
    Me.uc_entry_03_max_days_no_activity.Size = New System.Drawing.Size(73, 24)
    Me.uc_entry_03_max_days_no_activity.SufixText = "Sufix Text"
    Me.uc_entry_03_max_days_no_activity.SufixTextVisible = True
    Me.uc_entry_03_max_days_no_activity.TabIndex = 3
    Me.uc_entry_03_max_days_no_activity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_03_max_days_no_activity.TextValue = ""
    Me.uc_entry_03_max_days_no_activity.TextVisible = False
    Me.uc_entry_03_max_days_no_activity.TextWidth = 0
    Me.uc_entry_03_max_days_no_activity.Value = ""
    Me.uc_entry_03_max_days_no_activity.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_max_days_no_activity
    '
    Me.lbl_max_days_no_activity.Location = New System.Drawing.Point(6, 46)
    Me.lbl_max_days_no_activity.Name = "lbl_max_days_no_activity"
    Me.lbl_max_days_no_activity.Size = New System.Drawing.Size(190, 15)
    Me.lbl_max_days_no_activity.TabIndex = 1
    Me.lbl_max_days_no_activity.Text = "xM�ximo d�as sin actividad"
    Me.lbl_max_days_no_activity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'uc_entry_02_max_days_no_activity
    '
    Me.uc_entry_02_max_days_no_activity.DoubleValue = 0.0R
    Me.uc_entry_02_max_days_no_activity.IntegerValue = 0
    Me.uc_entry_02_max_days_no_activity.IsReadOnly = False
    Me.uc_entry_02_max_days_no_activity.Location = New System.Drawing.Point(65, 67)
    Me.uc_entry_02_max_days_no_activity.Name = "uc_entry_02_max_days_no_activity"
    Me.uc_entry_02_max_days_no_activity.PlaceHolder = Nothing
    Me.uc_entry_02_max_days_no_activity.Size = New System.Drawing.Size(73, 24)
    Me.uc_entry_02_max_days_no_activity.SufixText = "Sufix Text"
    Me.uc_entry_02_max_days_no_activity.SufixTextVisible = True
    Me.uc_entry_02_max_days_no_activity.TabIndex = 2
    Me.uc_entry_02_max_days_no_activity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_02_max_days_no_activity.TextValue = ""
    Me.uc_entry_02_max_days_no_activity.TextVisible = False
    Me.uc_entry_02_max_days_no_activity.TextWidth = 0
    Me.uc_entry_02_max_days_no_activity.Value = ""
    Me.uc_entry_02_max_days_no_activity.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_04_max_days_no_activity
    '
    Me.uc_entry_04_max_days_no_activity.DoubleValue = 0.0R
    Me.uc_entry_04_max_days_no_activity.IntegerValue = 0
    Me.uc_entry_04_max_days_no_activity.IsReadOnly = False
    Me.uc_entry_04_max_days_no_activity.Location = New System.Drawing.Point(65, 127)
    Me.uc_entry_04_max_days_no_activity.Name = "uc_entry_04_max_days_no_activity"
    Me.uc_entry_04_max_days_no_activity.PlaceHolder = Nothing
    Me.uc_entry_04_max_days_no_activity.Size = New System.Drawing.Size(73, 24)
    Me.uc_entry_04_max_days_no_activity.SufixText = "Sufix Text"
    Me.uc_entry_04_max_days_no_activity.SufixTextVisible = True
    Me.uc_entry_04_max_days_no_activity.TabIndex = 4
    Me.uc_entry_04_max_days_no_activity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_04_max_days_no_activity.TextValue = ""
    Me.uc_entry_04_max_days_no_activity.TextVisible = False
    Me.uc_entry_04_max_days_no_activity.TextWidth = 0
    Me.uc_entry_04_max_days_no_activity.Value = ""
    Me.uc_entry_04_max_days_no_activity.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabTaxes
    '
    Me.TabTaxes.Controls.Add(Me.pnl_taxes)
    Me.TabTaxes.Location = New System.Drawing.Point(4, 22)
    Me.TabTaxes.Name = "TabTaxes"
    Me.TabTaxes.Padding = New System.Windows.Forms.Padding(3)
    Me.TabTaxes.Size = New System.Drawing.Size(1073, 678)
    Me.TabTaxes.TabIndex = 3
    Me.TabTaxes.Text = "xTabTaxes"
    Me.TabTaxes.UseVisualStyleBackColor = True
    '
    'pnl_taxes
    '
    Me.pnl_taxes.Controls.Add(Me.Uc_entry_tax_3_on_won_name)
    Me.pnl_taxes.Controls.Add(Me.lbl_taxes_name)
    Me.pnl_taxes.Controls.Add(Me.tab_business)
    Me.pnl_taxes.Controls.Add(Me.chk_split_enabled)
    Me.pnl_taxes.Controls.Add(Me.tab_taxes)
    Me.pnl_taxes.Controls.Add(Me.Uc_entry_tax_2_on_won_name)
    Me.pnl_taxes.Controls.Add(Me.Uc_entry_tax_1_on_won_name)
    Me.pnl_taxes.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_taxes.Location = New System.Drawing.Point(3, 3)
    Me.pnl_taxes.Name = "pnl_taxes"
    Me.pnl_taxes.Size = New System.Drawing.Size(1067, 672)
    Me.pnl_taxes.TabIndex = 2
    '
    'Uc_entry_tax_3_on_won_name
    '
    Me.Uc_entry_tax_3_on_won_name.DoubleValue = 0.0R
    Me.Uc_entry_tax_3_on_won_name.IntegerValue = 0
    Me.Uc_entry_tax_3_on_won_name.IsReadOnly = False
    Me.Uc_entry_tax_3_on_won_name.Location = New System.Drawing.Point(15, 129)
    Me.Uc_entry_tax_3_on_won_name.Name = "Uc_entry_tax_3_on_won_name"
    Me.Uc_entry_tax_3_on_won_name.PlaceHolder = Nothing
    Me.Uc_entry_tax_3_on_won_name.Size = New System.Drawing.Size(283, 24)
    Me.Uc_entry_tax_3_on_won_name.SufixText = "Sufix Text"
    Me.Uc_entry_tax_3_on_won_name.TabIndex = 2
    Me.Uc_entry_tax_3_on_won_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_3_on_won_name.TextValue = ""
    Me.Uc_entry_tax_3_on_won_name.TextVisible = False
    Me.Uc_entry_tax_3_on_won_name.Value = ""
    Me.Uc_entry_tax_3_on_won_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_taxes_name
    '
    Me.lbl_taxes_name.Location = New System.Drawing.Point(99, 49)
    Me.lbl_taxes_name.Name = "lbl_taxes_name"
    Me.lbl_taxes_name.Size = New System.Drawing.Size(186, 24)
    Me.lbl_taxes_name.TabIndex = 8
    Me.lbl_taxes_name.Text = "xNombre"
    Me.lbl_taxes_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'tab_business
    '
    Me.tab_business.Controls.Add(Me.TabPage1)
    Me.tab_business.Controls.Add(Me.TabPage2)
    Me.tab_business.Location = New System.Drawing.Point(0, 200)
    Me.tab_business.Name = "tab_business"
    Me.tab_business.SelectedIndex = 0
    Me.tab_business.Size = New System.Drawing.Size(920, 470)
    Me.tab_business.TabIndex = 4
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.uc_business_a)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(912, 444)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'uc_business_a
    '
    Me.uc_business_a.BusinessName = ""
    Me.uc_business_a.CashInPct = ""
    Me.uc_business_a.ConceptName = ""
    Me.uc_business_a.DevTitle = ""
    Me.uc_business_a.HidePromo = False
    Me.uc_business_a.HideTotalCashIn = False
    Me.uc_business_a.Location = New System.Drawing.Point(7, 7)
    Me.uc_business_a.Name = "uc_business_a"
    Me.uc_business_a.PayTitle = ""
    Me.uc_business_a.PrizeCoupon = False
    Me.uc_business_a.PromoAndCouponTogether = False
    Me.uc_business_a.PromoAndCouponTogetherText = ""
    Me.uc_business_a.Size = New System.Drawing.Size(882, 429)
    Me.uc_business_a.SplitB = False
    Me.uc_business_a.TabIndex = 0
    Me.uc_business_a.TaxName = ""
    Me.uc_business_a.TaxPct = ""
    Me.uc_business_a.TextPrizeCoupon = ""
    Me.uc_business_a.TextPromotion = ""
    '
    'TabPage2
    '
    Me.TabPage2.Controls.Add(Me.uc_business_b)
    Me.TabPage2.Location = New System.Drawing.Point(4, 22)
    Me.TabPage2.Name = "TabPage2"
    Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage2.Size = New System.Drawing.Size(912, 444)
    Me.TabPage2.TabIndex = 1
    Me.TabPage2.Text = "TabPage2"
    Me.TabPage2.UseVisualStyleBackColor = True
    '
    'uc_business_b
    '
    Me.uc_business_b.BusinessName = ""
    Me.uc_business_b.CashInPct = ""
    Me.uc_business_b.ConceptName = ""
    Me.uc_business_b.DevTitle = ""
    Me.uc_business_b.HidePromo = False
    Me.uc_business_b.HideTotalCashIn = False
    Me.uc_business_b.Location = New System.Drawing.Point(7, 7)
    Me.uc_business_b.Name = "uc_business_b"
    Me.uc_business_b.PayTitle = ""
    Me.uc_business_b.PrizeCoupon = False
    Me.uc_business_b.PromoAndCouponTogether = False
    Me.uc_business_b.PromoAndCouponTogetherText = ""
    Me.uc_business_b.Size = New System.Drawing.Size(882, 434)
    Me.uc_business_b.SplitB = False
    Me.uc_business_b.TabIndex = 0
    Me.uc_business_b.TaxName = ""
    Me.uc_business_b.TaxPct = ""
    Me.uc_business_b.TextPrizeCoupon = ""
    Me.uc_business_b.TextPromotion = ""
    '
    'chk_split_enabled
    '
    Me.chk_split_enabled.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_split_enabled.Location = New System.Drawing.Point(4, 177)
    Me.chk_split_enabled.Name = "chk_split_enabled"
    Me.chk_split_enabled.Size = New System.Drawing.Size(201, 17)
    Me.chk_split_enabled.TabIndex = 3
    Me.chk_split_enabled.Text = "xSplitEnabled"
    Me.chk_split_enabled.UseVisualStyleBackColor = True
    '
    'tab_taxes
    '
    Me.tab_taxes.Controls.Add(Me.TabTaxesOnPrize)
    Me.tab_taxes.Controls.Add(Me.TabTaxesOnPromotions)
    Me.tab_taxes.Cursor = System.Windows.Forms.Cursors.Default
    Me.tab_taxes.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.tab_taxes.Location = New System.Drawing.Point(304, 3)
    Me.tab_taxes.Multiline = True
    Me.tab_taxes.Name = "tab_taxes"
    Me.tab_taxes.SelectedIndex = 0
    Me.tab_taxes.Size = New System.Drawing.Size(500, 161)
    Me.tab_taxes.TabIndex = 5
    '
    'TabTaxesOnPrize
    '
    Me.TabTaxesOnPrize.Controls.Add(Me.pnl_taxes_on_prize)
    Me.TabTaxesOnPrize.Location = New System.Drawing.Point(4, 22)
    Me.TabTaxesOnPrize.Name = "TabTaxesOnPrize"
    Me.TabTaxesOnPrize.Padding = New System.Windows.Forms.Padding(3)
    Me.TabTaxesOnPrize.Size = New System.Drawing.Size(492, 135)
    Me.TabTaxesOnPrize.TabIndex = 3
    Me.TabTaxesOnPrize.Text = "xTabTaxesOnPrize"
    Me.TabTaxesOnPrize.UseVisualStyleBackColor = True
    '
    'pnl_taxes_on_prize
    '
    Me.pnl_taxes_on_prize.Controls.Add(Me.Uc_entry_tax_3_apply_if)
    Me.pnl_taxes_on_prize.Controls.Add(Me.Uc_entry_tax_3_on_won_pct)
    Me.pnl_taxes_on_prize.Controls.Add(Me.lbl_tax3_tax_on_won_suffix)
    Me.pnl_taxes_on_prize.Controls.Add(Me.Uc_entry_tax_2_apply_if)
    Me.pnl_taxes_on_prize.Controls.Add(Me.Uc_entry_tax_1_apply_if)
    Me.pnl_taxes_on_prize.Controls.Add(Me.Uc_entry_tax_2_on_won_pct)
    Me.pnl_taxes_on_prize.Controls.Add(Me.Uc_entry_tax_1_on_won_pct)
    Me.pnl_taxes_on_prize.Controls.Add(Me.lbl_state_tax_on_won_suffix)
    Me.pnl_taxes_on_prize.Controls.Add(Me.lbl_federal_tax_on_won_suffix)
    Me.pnl_taxes_on_prize.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_taxes_on_prize.Location = New System.Drawing.Point(3, 3)
    Me.pnl_taxes_on_prize.Name = "pnl_taxes_on_prize"
    Me.pnl_taxes_on_prize.Size = New System.Drawing.Size(486, 129)
    Me.pnl_taxes_on_prize.TabIndex = 0
    '
    'Uc_entry_tax_3_apply_if
    '
    Me.Uc_entry_tax_3_apply_if.DoubleValue = 0.0R
    Me.Uc_entry_tax_3_apply_if.IntegerValue = 0
    Me.Uc_entry_tax_3_apply_if.IsReadOnly = False
    Me.Uc_entry_tax_3_apply_if.Location = New System.Drawing.Point(157, 101)
    Me.Uc_entry_tax_3_apply_if.Name = "Uc_entry_tax_3_apply_if"
    Me.Uc_entry_tax_3_apply_if.PlaceHolder = Nothing
    Me.Uc_entry_tax_3_apply_if.Size = New System.Drawing.Size(280, 24)
    Me.Uc_entry_tax_3_apply_if.SufixText = "Sufix Text"
    Me.Uc_entry_tax_3_apply_if.TabIndex = 7
    Me.Uc_entry_tax_3_apply_if.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_3_apply_if.TextValue = ""
    Me.Uc_entry_tax_3_apply_if.TextVisible = False
    Me.Uc_entry_tax_3_apply_if.TextWidth = 170
    Me.Uc_entry_tax_3_apply_if.Value = ""
    Me.Uc_entry_tax_3_apply_if.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_tax_3_on_won_pct
    '
    Me.Uc_entry_tax_3_on_won_pct.DoubleValue = 0.0R
    Me.Uc_entry_tax_3_on_won_pct.IntegerValue = 0
    Me.Uc_entry_tax_3_on_won_pct.IsReadOnly = False
    Me.Uc_entry_tax_3_on_won_pct.Location = New System.Drawing.Point(19, 101)
    Me.Uc_entry_tax_3_on_won_pct.Name = "Uc_entry_tax_3_on_won_pct"
    Me.Uc_entry_tax_3_on_won_pct.PlaceHolder = Nothing
    Me.Uc_entry_tax_3_on_won_pct.Size = New System.Drawing.Size(115, 24)
    Me.Uc_entry_tax_3_on_won_pct.SufixText = "Sufix Text"
    Me.Uc_entry_tax_3_on_won_pct.TabIndex = 6
    Me.Uc_entry_tax_3_on_won_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_3_on_won_pct.TextValue = ""
    Me.Uc_entry_tax_3_on_won_pct.TextVisible = False
    Me.Uc_entry_tax_3_on_won_pct.TextWidth = 65
    Me.Uc_entry_tax_3_on_won_pct.Value = ""
    Me.Uc_entry_tax_3_on_won_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_tax3_tax_on_won_suffix
    '
    Me.lbl_tax3_tax_on_won_suffix.Location = New System.Drawing.Point(136, 101)
    Me.lbl_tax3_tax_on_won_suffix.Name = "lbl_tax3_tax_on_won_suffix"
    Me.lbl_tax3_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_tax3_tax_on_won_suffix.TabIndex = 21
    Me.lbl_tax3_tax_on_won_suffix.Text = "%"
    Me.lbl_tax3_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Uc_entry_tax_2_apply_if
    '
    Me.Uc_entry_tax_2_apply_if.DoubleValue = 0.0R
    Me.Uc_entry_tax_2_apply_if.IntegerValue = 0
    Me.Uc_entry_tax_2_apply_if.IsReadOnly = False
    Me.Uc_entry_tax_2_apply_if.Location = New System.Drawing.Point(157, 73)
    Me.Uc_entry_tax_2_apply_if.Name = "Uc_entry_tax_2_apply_if"
    Me.Uc_entry_tax_2_apply_if.PlaceHolder = Nothing
    Me.Uc_entry_tax_2_apply_if.Size = New System.Drawing.Size(280, 24)
    Me.Uc_entry_tax_2_apply_if.SufixText = "Sufix Text"
    Me.Uc_entry_tax_2_apply_if.TabIndex = 5
    Me.Uc_entry_tax_2_apply_if.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_2_apply_if.TextValue = ""
    Me.Uc_entry_tax_2_apply_if.TextVisible = False
    Me.Uc_entry_tax_2_apply_if.TextWidth = 170
    Me.Uc_entry_tax_2_apply_if.Value = ""
    Me.Uc_entry_tax_2_apply_if.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_tax_1_apply_if
    '
    Me.Uc_entry_tax_1_apply_if.DoubleValue = 0.0R
    Me.Uc_entry_tax_1_apply_if.IntegerValue = 0
    Me.Uc_entry_tax_1_apply_if.IsReadOnly = False
    Me.Uc_entry_tax_1_apply_if.Location = New System.Drawing.Point(157, 46)
    Me.Uc_entry_tax_1_apply_if.Name = "Uc_entry_tax_1_apply_if"
    Me.Uc_entry_tax_1_apply_if.PlaceHolder = Nothing
    Me.Uc_entry_tax_1_apply_if.Size = New System.Drawing.Size(280, 24)
    Me.Uc_entry_tax_1_apply_if.SufixText = "Sufix Text"
    Me.Uc_entry_tax_1_apply_if.TabIndex = 2
    Me.Uc_entry_tax_1_apply_if.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_1_apply_if.TextValue = ""
    Me.Uc_entry_tax_1_apply_if.TextVisible = False
    Me.Uc_entry_tax_1_apply_if.TextWidth = 170
    Me.Uc_entry_tax_1_apply_if.Value = ""
    Me.Uc_entry_tax_1_apply_if.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_tax_2_on_won_pct
    '
    Me.Uc_entry_tax_2_on_won_pct.DoubleValue = 0.0R
    Me.Uc_entry_tax_2_on_won_pct.IntegerValue = 0
    Me.Uc_entry_tax_2_on_won_pct.IsReadOnly = False
    Me.Uc_entry_tax_2_on_won_pct.Location = New System.Drawing.Point(19, 73)
    Me.Uc_entry_tax_2_on_won_pct.Name = "Uc_entry_tax_2_on_won_pct"
    Me.Uc_entry_tax_2_on_won_pct.PlaceHolder = Nothing
    Me.Uc_entry_tax_2_on_won_pct.Size = New System.Drawing.Size(115, 24)
    Me.Uc_entry_tax_2_on_won_pct.SufixText = "Sufix Text"
    Me.Uc_entry_tax_2_on_won_pct.TabIndex = 4
    Me.Uc_entry_tax_2_on_won_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_2_on_won_pct.TextValue = ""
    Me.Uc_entry_tax_2_on_won_pct.TextVisible = False
    Me.Uc_entry_tax_2_on_won_pct.TextWidth = 65
    Me.Uc_entry_tax_2_on_won_pct.Value = ""
    Me.Uc_entry_tax_2_on_won_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_tax_1_on_won_pct
    '
    Me.Uc_entry_tax_1_on_won_pct.DoubleValue = 0.0R
    Me.Uc_entry_tax_1_on_won_pct.IntegerValue = 0
    Me.Uc_entry_tax_1_on_won_pct.IsReadOnly = False
    Me.Uc_entry_tax_1_on_won_pct.Location = New System.Drawing.Point(19, 46)
    Me.Uc_entry_tax_1_on_won_pct.Name = "Uc_entry_tax_1_on_won_pct"
    Me.Uc_entry_tax_1_on_won_pct.PlaceHolder = Nothing
    Me.Uc_entry_tax_1_on_won_pct.Size = New System.Drawing.Size(115, 24)
    Me.Uc_entry_tax_1_on_won_pct.SufixText = "Sufix Text"
    Me.Uc_entry_tax_1_on_won_pct.TabIndex = 1
    Me.Uc_entry_tax_1_on_won_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_1_on_won_pct.TextValue = ""
    Me.Uc_entry_tax_1_on_won_pct.TextVisible = False
    Me.Uc_entry_tax_1_on_won_pct.TextWidth = 65
    Me.Uc_entry_tax_1_on_won_pct.Value = ""
    Me.Uc_entry_tax_1_on_won_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_state_tax_on_won_suffix
    '
    Me.lbl_state_tax_on_won_suffix.Location = New System.Drawing.Point(136, 73)
    Me.lbl_state_tax_on_won_suffix.Name = "lbl_state_tax_on_won_suffix"
    Me.lbl_state_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_state_tax_on_won_suffix.TabIndex = 18
    Me.lbl_state_tax_on_won_suffix.Text = "%"
    Me.lbl_state_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_federal_tax_on_won_suffix
    '
    Me.lbl_federal_tax_on_won_suffix.Location = New System.Drawing.Point(136, 46)
    Me.lbl_federal_tax_on_won_suffix.Name = "lbl_federal_tax_on_won_suffix"
    Me.lbl_federal_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_federal_tax_on_won_suffix.TabIndex = 17
    Me.lbl_federal_tax_on_won_suffix.Text = "%"
    Me.lbl_federal_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'TabTaxesOnPromotions
    '
    Me.TabTaxesOnPromotions.Controls.Add(Me.pnl_taxes_on_promotions)
    Me.TabTaxesOnPromotions.Location = New System.Drawing.Point(4, 22)
    Me.TabTaxesOnPromotions.Name = "TabTaxesOnPromotions"
    Me.TabTaxesOnPromotions.Padding = New System.Windows.Forms.Padding(3)
    Me.TabTaxesOnPromotions.Size = New System.Drawing.Size(492, 135)
    Me.TabTaxesOnPromotions.TabIndex = 3
    Me.TabTaxesOnPromotions.Text = "xTabTaxesOnPromotions"
    Me.TabTaxesOnPromotions.UseVisualStyleBackColor = True
    '
    'pnl_taxes_on_promotions
    '
    Me.pnl_taxes_on_promotions.Controls.Add(Me.chk_enable_taxes_redeem_promotions)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.Uc_entry_promo_tax_3_apply_if)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.Uc_entry_promo_tax_2_apply_if)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.Uc_entry_promo_tax_3_on_won_pct)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.lbl_tax3_promo_tax_on_won_suffix)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.Uc_entry_promo_tax_1_apply_if)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.Uc_entry_promo_tax_2_on_won_pct)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.Uc_entry_promo_tax_1_on_won_pct)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.lbl_state_promo_tax_on_won_suffix)
    Me.pnl_taxes_on_promotions.Controls.Add(Me.lbl_federal_promo_tax_on_won_suffix)
    Me.pnl_taxes_on_promotions.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_taxes_on_promotions.Location = New System.Drawing.Point(3, 3)
    Me.pnl_taxes_on_promotions.Name = "pnl_taxes_on_promotions"
    Me.pnl_taxes_on_promotions.Size = New System.Drawing.Size(486, 129)
    Me.pnl_taxes_on_promotions.TabIndex = 0
    '
    'chk_enable_taxes_redeem_promotions
    '
    Me.chk_enable_taxes_redeem_promotions.AutoSize = True
    Me.chk_enable_taxes_redeem_promotions.Location = New System.Drawing.Point(19, 8)
    Me.chk_enable_taxes_redeem_promotions.Name = "chk_enable_taxes_redeem_promotions"
    Me.chk_enable_taxes_redeem_promotions.Size = New System.Drawing.Size(141, 17)
    Me.chk_enable_taxes_redeem_promotions.TabIndex = 1
    Me.chk_enable_taxes_redeem_promotions.Text = "xEnablePromoTaxes"
    Me.chk_enable_taxes_redeem_promotions.UseVisualStyleBackColor = True
    '
    'Uc_entry_promo_tax_3_apply_if
    '
    Me.Uc_entry_promo_tax_3_apply_if.DoubleValue = 0.0R
    Me.Uc_entry_promo_tax_3_apply_if.IntegerValue = 0
    Me.Uc_entry_promo_tax_3_apply_if.IsReadOnly = False
    Me.Uc_entry_promo_tax_3_apply_if.Location = New System.Drawing.Point(157, 101)
    Me.Uc_entry_promo_tax_3_apply_if.Name = "Uc_entry_promo_tax_3_apply_if"
    Me.Uc_entry_promo_tax_3_apply_if.PlaceHolder = Nothing
    Me.Uc_entry_promo_tax_3_apply_if.Size = New System.Drawing.Size(280, 24)
    Me.Uc_entry_promo_tax_3_apply_if.SufixText = "Sufix Text"
    Me.Uc_entry_promo_tax_3_apply_if.TabIndex = 7
    Me.Uc_entry_promo_tax_3_apply_if.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_promo_tax_3_apply_if.TextValue = ""
    Me.Uc_entry_promo_tax_3_apply_if.TextVisible = False
    Me.Uc_entry_promo_tax_3_apply_if.TextWidth = 170
    Me.Uc_entry_promo_tax_3_apply_if.Value = ""
    Me.Uc_entry_promo_tax_3_apply_if.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_promo_tax_2_apply_if
    '
    Me.Uc_entry_promo_tax_2_apply_if.DoubleValue = 0.0R
    Me.Uc_entry_promo_tax_2_apply_if.IntegerValue = 0
    Me.Uc_entry_promo_tax_2_apply_if.IsReadOnly = False
    Me.Uc_entry_promo_tax_2_apply_if.Location = New System.Drawing.Point(157, 73)
    Me.Uc_entry_promo_tax_2_apply_if.Name = "Uc_entry_promo_tax_2_apply_if"
    Me.Uc_entry_promo_tax_2_apply_if.PlaceHolder = Nothing
    Me.Uc_entry_promo_tax_2_apply_if.Size = New System.Drawing.Size(280, 24)
    Me.Uc_entry_promo_tax_2_apply_if.SufixText = "Sufix Text"
    Me.Uc_entry_promo_tax_2_apply_if.TabIndex = 5
    Me.Uc_entry_promo_tax_2_apply_if.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_promo_tax_2_apply_if.TextValue = ""
    Me.Uc_entry_promo_tax_2_apply_if.TextVisible = False
    Me.Uc_entry_promo_tax_2_apply_if.TextWidth = 170
    Me.Uc_entry_promo_tax_2_apply_if.Value = ""
    Me.Uc_entry_promo_tax_2_apply_if.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_promo_tax_3_on_won_pct
    '
    Me.Uc_entry_promo_tax_3_on_won_pct.DoubleValue = 0.0R
    Me.Uc_entry_promo_tax_3_on_won_pct.IntegerValue = 0
    Me.Uc_entry_promo_tax_3_on_won_pct.IsReadOnly = False
    Me.Uc_entry_promo_tax_3_on_won_pct.Location = New System.Drawing.Point(19, 101)
    Me.Uc_entry_promo_tax_3_on_won_pct.Name = "Uc_entry_promo_tax_3_on_won_pct"
    Me.Uc_entry_promo_tax_3_on_won_pct.PlaceHolder = Nothing
    Me.Uc_entry_promo_tax_3_on_won_pct.Size = New System.Drawing.Size(115, 24)
    Me.Uc_entry_promo_tax_3_on_won_pct.SufixText = "Sufix Text"
    Me.Uc_entry_promo_tax_3_on_won_pct.TabIndex = 6
    Me.Uc_entry_promo_tax_3_on_won_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_promo_tax_3_on_won_pct.TextValue = ""
    Me.Uc_entry_promo_tax_3_on_won_pct.TextVisible = False
    Me.Uc_entry_promo_tax_3_on_won_pct.TextWidth = 65
    Me.Uc_entry_promo_tax_3_on_won_pct.Value = ""
    Me.Uc_entry_promo_tax_3_on_won_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_tax3_promo_tax_on_won_suffix
    '
    Me.lbl_tax3_promo_tax_on_won_suffix.Location = New System.Drawing.Point(136, 101)
    Me.lbl_tax3_promo_tax_on_won_suffix.Name = "lbl_tax3_promo_tax_on_won_suffix"
    Me.lbl_tax3_promo_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_tax3_promo_tax_on_won_suffix.TabIndex = 22
    Me.lbl_tax3_promo_tax_on_won_suffix.Text = "%"
    Me.lbl_tax3_promo_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Uc_entry_promo_tax_1_apply_if
    '
    Me.Uc_entry_promo_tax_1_apply_if.DoubleValue = 0.0R
    Me.Uc_entry_promo_tax_1_apply_if.IntegerValue = 0
    Me.Uc_entry_promo_tax_1_apply_if.IsReadOnly = False
    Me.Uc_entry_promo_tax_1_apply_if.Location = New System.Drawing.Point(157, 46)
    Me.Uc_entry_promo_tax_1_apply_if.Name = "Uc_entry_promo_tax_1_apply_if"
    Me.Uc_entry_promo_tax_1_apply_if.PlaceHolder = Nothing
    Me.Uc_entry_promo_tax_1_apply_if.Size = New System.Drawing.Size(280, 24)
    Me.Uc_entry_promo_tax_1_apply_if.SufixText = "Sufix Text"
    Me.Uc_entry_promo_tax_1_apply_if.TabIndex = 2
    Me.Uc_entry_promo_tax_1_apply_if.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_promo_tax_1_apply_if.TextValue = ""
    Me.Uc_entry_promo_tax_1_apply_if.TextVisible = False
    Me.Uc_entry_promo_tax_1_apply_if.TextWidth = 170
    Me.Uc_entry_promo_tax_1_apply_if.Value = ""
    Me.Uc_entry_promo_tax_1_apply_if.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_promo_tax_2_on_won_pct
    '
    Me.Uc_entry_promo_tax_2_on_won_pct.DoubleValue = 0.0R
    Me.Uc_entry_promo_tax_2_on_won_pct.IntegerValue = 0
    Me.Uc_entry_promo_tax_2_on_won_pct.IsReadOnly = False
    Me.Uc_entry_promo_tax_2_on_won_pct.Location = New System.Drawing.Point(19, 73)
    Me.Uc_entry_promo_tax_2_on_won_pct.Name = "Uc_entry_promo_tax_2_on_won_pct"
    Me.Uc_entry_promo_tax_2_on_won_pct.PlaceHolder = Nothing
    Me.Uc_entry_promo_tax_2_on_won_pct.Size = New System.Drawing.Size(115, 24)
    Me.Uc_entry_promo_tax_2_on_won_pct.SufixText = "Sufix Text"
    Me.Uc_entry_promo_tax_2_on_won_pct.TabIndex = 4
    Me.Uc_entry_promo_tax_2_on_won_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_promo_tax_2_on_won_pct.TextValue = ""
    Me.Uc_entry_promo_tax_2_on_won_pct.TextVisible = False
    Me.Uc_entry_promo_tax_2_on_won_pct.TextWidth = 65
    Me.Uc_entry_promo_tax_2_on_won_pct.Value = ""
    Me.Uc_entry_promo_tax_2_on_won_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_promo_tax_1_on_won_pct
    '
    Me.Uc_entry_promo_tax_1_on_won_pct.DoubleValue = 0.0R
    Me.Uc_entry_promo_tax_1_on_won_pct.IntegerValue = 0
    Me.Uc_entry_promo_tax_1_on_won_pct.IsReadOnly = False
    Me.Uc_entry_promo_tax_1_on_won_pct.Location = New System.Drawing.Point(19, 46)
    Me.Uc_entry_promo_tax_1_on_won_pct.Name = "Uc_entry_promo_tax_1_on_won_pct"
    Me.Uc_entry_promo_tax_1_on_won_pct.PlaceHolder = Nothing
    Me.Uc_entry_promo_tax_1_on_won_pct.Size = New System.Drawing.Size(115, 24)
    Me.Uc_entry_promo_tax_1_on_won_pct.SufixText = "Sufix Text"
    Me.Uc_entry_promo_tax_1_on_won_pct.TabIndex = 1
    Me.Uc_entry_promo_tax_1_on_won_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_promo_tax_1_on_won_pct.TextValue = ""
    Me.Uc_entry_promo_tax_1_on_won_pct.TextVisible = False
    Me.Uc_entry_promo_tax_1_on_won_pct.TextWidth = 65
    Me.Uc_entry_promo_tax_1_on_won_pct.Value = ""
    Me.Uc_entry_promo_tax_1_on_won_pct.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_state_promo_tax_on_won_suffix
    '
    Me.lbl_state_promo_tax_on_won_suffix.Location = New System.Drawing.Point(136, 73)
    Me.lbl_state_promo_tax_on_won_suffix.Name = "lbl_state_promo_tax_on_won_suffix"
    Me.lbl_state_promo_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_state_promo_tax_on_won_suffix.TabIndex = 18
    Me.lbl_state_promo_tax_on_won_suffix.Text = "%"
    Me.lbl_state_promo_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_federal_promo_tax_on_won_suffix
    '
    Me.lbl_federal_promo_tax_on_won_suffix.Location = New System.Drawing.Point(136, 46)
    Me.lbl_federal_promo_tax_on_won_suffix.Name = "lbl_federal_promo_tax_on_won_suffix"
    Me.lbl_federal_promo_tax_on_won_suffix.Size = New System.Drawing.Size(15, 24)
    Me.lbl_federal_promo_tax_on_won_suffix.TabIndex = 17
    Me.lbl_federal_promo_tax_on_won_suffix.Text = "%"
    Me.lbl_federal_promo_tax_on_won_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Uc_entry_tax_2_on_won_name
    '
    Me.Uc_entry_tax_2_on_won_name.DoubleValue = 0.0R
    Me.Uc_entry_tax_2_on_won_name.IntegerValue = 0
    Me.Uc_entry_tax_2_on_won_name.IsReadOnly = False
    Me.Uc_entry_tax_2_on_won_name.Location = New System.Drawing.Point(15, 101)
    Me.Uc_entry_tax_2_on_won_name.Name = "Uc_entry_tax_2_on_won_name"
    Me.Uc_entry_tax_2_on_won_name.PlaceHolder = Nothing
    Me.Uc_entry_tax_2_on_won_name.Size = New System.Drawing.Size(283, 24)
    Me.Uc_entry_tax_2_on_won_name.SufixText = "Sufix Text"
    Me.Uc_entry_tax_2_on_won_name.TabIndex = 1
    Me.Uc_entry_tax_2_on_won_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_2_on_won_name.TextValue = ""
    Me.Uc_entry_tax_2_on_won_name.TextVisible = False
    Me.Uc_entry_tax_2_on_won_name.Value = ""
    Me.Uc_entry_tax_2_on_won_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_entry_tax_1_on_won_name
    '
    Me.Uc_entry_tax_1_on_won_name.DoubleValue = 0.0R
    Me.Uc_entry_tax_1_on_won_name.IntegerValue = 0
    Me.Uc_entry_tax_1_on_won_name.IsReadOnly = False
    Me.Uc_entry_tax_1_on_won_name.Location = New System.Drawing.Point(15, 74)
    Me.Uc_entry_tax_1_on_won_name.Name = "Uc_entry_tax_1_on_won_name"
    Me.Uc_entry_tax_1_on_won_name.PlaceHolder = Nothing
    Me.Uc_entry_tax_1_on_won_name.Size = New System.Drawing.Size(283, 24)
    Me.Uc_entry_tax_1_on_won_name.SufixText = "Sufix Text"
    Me.Uc_entry_tax_1_on_won_name.TabIndex = 0
    Me.Uc_entry_tax_1_on_won_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.Uc_entry_tax_1_on_won_name.TextValue = ""
    Me.Uc_entry_tax_1_on_won_name.TextVisible = False
    Me.Uc_entry_tax_1_on_won_name.Value = ""
    Me.Uc_entry_tax_1_on_won_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'TabEvidence
    '
    Me.TabEvidence.Controls.Add(Me.id_evidence_configuration)
    Me.TabEvidence.Location = New System.Drawing.Point(4, 22)
    Me.TabEvidence.Name = "TabEvidence"
    Me.TabEvidence.Padding = New System.Windows.Forms.Padding(3)
    Me.TabEvidence.Size = New System.Drawing.Size(1073, 678)
    Me.TabEvidence.TabIndex = 5
    Me.TabEvidence.Text = "xTabEvidence"
    Me.TabEvidence.UseVisualStyleBackColor = True
    '
    'id_evidence_configuration
    '
    Me.id_evidence_configuration.AutoSize = True
    Me.id_evidence_configuration.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.id_evidence_configuration.CompanyID1 = ""
    Me.id_evidence_configuration.CompanyID2 = ""
    Me.id_evidence_configuration.EditableMinutes = 0
    Me.id_evidence_configuration.EvidenceEnabled = False
    Me.id_evidence_configuration.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.id_evidence_configuration.GeneratedWitholdingDocument = 0
    Me.id_evidence_configuration.IdentityID1 = ""
    Me.id_evidence_configuration.IdentityID2 = ""
    Me.id_evidence_configuration.IdentityLastName1 = ""
    Me.id_evidence_configuration.IdentityLastName2 = ""
    Me.id_evidence_configuration.IdentityName = ""
    Me.id_evidence_configuration.Location = New System.Drawing.Point(15, 15)
    Me.id_evidence_configuration.Margin = New System.Windows.Forms.Padding(0)
    Me.id_evidence_configuration.Name = "id_evidence_configuration"
    Me.id_evidence_configuration.PrizeGratherThan = New Decimal(New Integer() {0, 0, 0, 131072})
    Me.id_evidence_configuration.RepresentationMode = GUI_Controls.uc_identity_data.Mode.PARAMETERS_CONFIGURATION
    Me.id_evidence_configuration.RepresentativeID1 = ""
    Me.id_evidence_configuration.RepresentativeID2 = ""
    Me.id_evidence_configuration.RepresentativeName = ""
    Me.id_evidence_configuration.RepresentativeSignature = Nothing
    Me.id_evidence_configuration.Size = New System.Drawing.Size(876, 414)
    Me.id_evidence_configuration.TabIndex = 0
    Me.id_evidence_configuration.TextGroupBox = ""
    '
    'TabCFDI
    '
    Me.TabCFDI.Controls.Add(Me.uc_entry_cfdi_active)
    Me.TabCFDI.Controls.Add(Me.gb_cfdi_resume)
    Me.TabCFDI.Controls.Add(Me.gb_cfdi_configuration)
    Me.TabCFDI.Location = New System.Drawing.Point(4, 22)
    Me.TabCFDI.Name = "TabCFDI"
    Me.TabCFDI.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCFDI.Size = New System.Drawing.Size(1073, 678)
    Me.TabCFDI.TabIndex = 6
    Me.TabCFDI.Text = "xTabCFDI"
    Me.TabCFDI.UseVisualStyleBackColor = True
    '
    'uc_entry_cfdi_active
    '
    Me.uc_entry_cfdi_active.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.uc_entry_cfdi_active.Location = New System.Drawing.Point(22, 20)
    Me.uc_entry_cfdi_active.Name = "uc_entry_cfdi_active"
    Me.uc_entry_cfdi_active.Size = New System.Drawing.Size(201, 17)
    Me.uc_entry_cfdi_active.TabIndex = 0
    Me.uc_entry_cfdi_active.Text = "xActive"
    Me.uc_entry_cfdi_active.UseVisualStyleBackColor = True
    '
    'gb_cfdi_resume
    '
    Me.gb_cfdi_resume.Controls.Add(Me.dg_cfdi_resume)
    Me.gb_cfdi_resume.Location = New System.Drawing.Point(22, 314)
    Me.gb_cfdi_resume.Name = "gb_cfdi_resume"
    Me.gb_cfdi_resume.Size = New System.Drawing.Size(387, 88)
    Me.gb_cfdi_resume.TabIndex = 2
    Me.gb_cfdi_resume.TabStop = False
    Me.gb_cfdi_resume.Text = "xResume"
    '
    'dg_cfdi_resume
    '
    Me.dg_cfdi_resume.CurrentCol = -1
    Me.dg_cfdi_resume.CurrentRow = -1
    Me.dg_cfdi_resume.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_cfdi_resume.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_cfdi_resume.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_cfdi_resume.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_cfdi_resume.Location = New System.Drawing.Point(3, 17)
    Me.dg_cfdi_resume.Name = "dg_cfdi_resume"
    Me.dg_cfdi_resume.PanelRightVisible = False
    Me.dg_cfdi_resume.Redraw = True
    Me.dg_cfdi_resume.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_cfdi_resume.Size = New System.Drawing.Size(381, 68)
    Me.dg_cfdi_resume.Sortable = False
    Me.dg_cfdi_resume.SortAscending = True
    Me.dg_cfdi_resume.SortByCol = 0
    Me.dg_cfdi_resume.TabIndex = 0
    Me.dg_cfdi_resume.ToolTipped = True
    Me.dg_cfdi_resume.TopRow = -2
    Me.dg_cfdi_resume.WordWrap = False
    '
    'gb_cfdi_configuration
    '
    Me.gb_cfdi_configuration.Controls.Add(Me.gb_special_account_data)
    Me.gb_cfdi_configuration.Controls.Add(Me.gb_scheduling)
    Me.gb_cfdi_configuration.Controls.Add(Me.uc_entry_cfdi_site)
    Me.gb_cfdi_configuration.Controls.Add(Me.lbl_cfdi_site)
    Me.gb_cfdi_configuration.Controls.Add(Me.uc_entry_cfdi_password)
    Me.gb_cfdi_configuration.Controls.Add(Me.lbl_cfdi_password)
    Me.gb_cfdi_configuration.Controls.Add(Me.uc_entry_cfdi_user)
    Me.gb_cfdi_configuration.Controls.Add(Me.lbl_cfdi_user)
    Me.gb_cfdi_configuration.Controls.Add(Me.uc_entry_cfdi_url)
    Me.gb_cfdi_configuration.Controls.Add(Me.lbl_cfdi_url)
    Me.gb_cfdi_configuration.Location = New System.Drawing.Point(22, 45)
    Me.gb_cfdi_configuration.Name = "gb_cfdi_configuration"
    Me.gb_cfdi_configuration.Size = New System.Drawing.Size(666, 263)
    Me.gb_cfdi_configuration.TabIndex = 1
    Me.gb_cfdi_configuration.TabStop = False
    Me.gb_cfdi_configuration.Text = "xConfiguration"
    '
    'gb_special_account_data
    '
    Me.gb_special_account_data.Controls.Add(Me.uc_entry_cfdi_ef_anonymous)
    Me.gb_special_account_data.Controls.Add(Me.uc_entry_cfdi_rfc_anonymous)
    Me.gb_special_account_data.Controls.Add(Me.lbl_special_account_ef)
    Me.gb_special_account_data.Controls.Add(Me.lbl_special_account_rfc)
    Me.gb_special_account_data.Controls.Add(Me.uc_entry_cfdi_ef_foreign)
    Me.gb_special_account_data.Controls.Add(Me.uc_entry_cfdi_rfc_foreign)
    Me.gb_special_account_data.Controls.Add(Me.lbl_special_account_foreign)
    Me.gb_special_account_data.Controls.Add(Me.lbl_special_account_anonymous)
    Me.gb_special_account_data.Location = New System.Drawing.Point(284, 48)
    Me.gb_special_account_data.Name = "gb_special_account_data"
    Me.gb_special_account_data.Size = New System.Drawing.Size(374, 89)
    Me.gb_special_account_data.TabIndex = 8
    Me.gb_special_account_data.TabStop = False
    Me.gb_special_account_data.Text = "xSpecialAccountData"
    '
    'uc_entry_cfdi_ef_anonymous
    '
    Me.uc_entry_cfdi_ef_anonymous.DoubleValue = 0.0R
    Me.uc_entry_cfdi_ef_anonymous.IntegerValue = 0
    Me.uc_entry_cfdi_ef_anonymous.IsReadOnly = False
    Me.uc_entry_cfdi_ef_anonymous.Location = New System.Drawing.Point(262, 29)
    Me.uc_entry_cfdi_ef_anonymous.Name = "uc_entry_cfdi_ef_anonymous"
    Me.uc_entry_cfdi_ef_anonymous.PlaceHolder = Nothing
    Me.uc_entry_cfdi_ef_anonymous.Size = New System.Drawing.Size(78, 24)
    Me.uc_entry_cfdi_ef_anonymous.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_ef_anonymous.TabIndex = 2
    Me.uc_entry_cfdi_ef_anonymous.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_ef_anonymous.TextValue = ""
    Me.uc_entry_cfdi_ef_anonymous.TextVisible = False
    Me.uc_entry_cfdi_ef_anonymous.TextWidth = 0
    Me.uc_entry_cfdi_ef_anonymous.Value = ""
    Me.uc_entry_cfdi_ef_anonymous.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_cfdi_rfc_anonymous
    '
    Me.uc_entry_cfdi_rfc_anonymous.DoubleValue = 0.0R
    Me.uc_entry_cfdi_rfc_anonymous.IntegerValue = 0
    Me.uc_entry_cfdi_rfc_anonymous.IsReadOnly = False
    Me.uc_entry_cfdi_rfc_anonymous.Location = New System.Drawing.Point(124, 29)
    Me.uc_entry_cfdi_rfc_anonymous.Name = "uc_entry_cfdi_rfc_anonymous"
    Me.uc_entry_cfdi_rfc_anonymous.PlaceHolder = Nothing
    Me.uc_entry_cfdi_rfc_anonymous.Size = New System.Drawing.Size(108, 24)
    Me.uc_entry_cfdi_rfc_anonymous.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_rfc_anonymous.TabIndex = 1
    Me.uc_entry_cfdi_rfc_anonymous.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_rfc_anonymous.TextValue = ""
    Me.uc_entry_cfdi_rfc_anonymous.TextVisible = False
    Me.uc_entry_cfdi_rfc_anonymous.TextWidth = 0
    Me.uc_entry_cfdi_rfc_anonymous.Value = ""
    Me.uc_entry_cfdi_rfc_anonymous.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_special_account_ef
    '
    Me.lbl_special_account_ef.Location = New System.Drawing.Point(236, 12)
    Me.lbl_special_account_ef.Name = "lbl_special_account_ef"
    Me.lbl_special_account_ef.Size = New System.Drawing.Size(130, 19)
    Me.lbl_special_account_ef.TabIndex = 48
    Me.lbl_special_account_ef.Text = "xEntidadFederativa"
    Me.lbl_special_account_ef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_special_account_rfc
    '
    Me.lbl_special_account_rfc.Location = New System.Drawing.Point(161, 15)
    Me.lbl_special_account_rfc.Name = "lbl_special_account_rfc"
    Me.lbl_special_account_rfc.Size = New System.Drawing.Size(32, 13)
    Me.lbl_special_account_rfc.TabIndex = 47
    Me.lbl_special_account_rfc.Text = "RFC"
    Me.lbl_special_account_rfc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'uc_entry_cfdi_ef_foreign
    '
    Me.uc_entry_cfdi_ef_foreign.DoubleValue = 0.0R
    Me.uc_entry_cfdi_ef_foreign.IntegerValue = 0
    Me.uc_entry_cfdi_ef_foreign.IsReadOnly = False
    Me.uc_entry_cfdi_ef_foreign.Location = New System.Drawing.Point(262, 59)
    Me.uc_entry_cfdi_ef_foreign.Name = "uc_entry_cfdi_ef_foreign"
    Me.uc_entry_cfdi_ef_foreign.PlaceHolder = Nothing
    Me.uc_entry_cfdi_ef_foreign.Size = New System.Drawing.Size(78, 24)
    Me.uc_entry_cfdi_ef_foreign.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_ef_foreign.TabIndex = 5
    Me.uc_entry_cfdi_ef_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_ef_foreign.TextValue = ""
    Me.uc_entry_cfdi_ef_foreign.TextVisible = False
    Me.uc_entry_cfdi_ef_foreign.TextWidth = 0
    Me.uc_entry_cfdi_ef_foreign.Value = ""
    Me.uc_entry_cfdi_ef_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_cfdi_rfc_foreign
    '
    Me.uc_entry_cfdi_rfc_foreign.DoubleValue = 0.0R
    Me.uc_entry_cfdi_rfc_foreign.IntegerValue = 0
    Me.uc_entry_cfdi_rfc_foreign.IsReadOnly = False
    Me.uc_entry_cfdi_rfc_foreign.Location = New System.Drawing.Point(124, 59)
    Me.uc_entry_cfdi_rfc_foreign.Name = "uc_entry_cfdi_rfc_foreign"
    Me.uc_entry_cfdi_rfc_foreign.PlaceHolder = Nothing
    Me.uc_entry_cfdi_rfc_foreign.Size = New System.Drawing.Size(108, 24)
    Me.uc_entry_cfdi_rfc_foreign.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_rfc_foreign.TabIndex = 4
    Me.uc_entry_cfdi_rfc_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_rfc_foreign.TextValue = ""
    Me.uc_entry_cfdi_rfc_foreign.TextVisible = False
    Me.uc_entry_cfdi_rfc_foreign.TextWidth = 0
    Me.uc_entry_cfdi_rfc_foreign.Value = ""
    Me.uc_entry_cfdi_rfc_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_special_account_foreign
    '
    Me.lbl_special_account_foreign.Location = New System.Drawing.Point(6, 58)
    Me.lbl_special_account_foreign.Name = "lbl_special_account_foreign"
    Me.lbl_special_account_foreign.Size = New System.Drawing.Size(112, 25)
    Me.lbl_special_account_foreign.TabIndex = 3
    Me.lbl_special_account_foreign.Text = "xforeign"
    Me.lbl_special_account_foreign.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_special_account_anonymous
    '
    Me.lbl_special_account_anonymous.Location = New System.Drawing.Point(6, 28)
    Me.lbl_special_account_anonymous.Name = "lbl_special_account_anonymous"
    Me.lbl_special_account_anonymous.Size = New System.Drawing.Size(112, 25)
    Me.lbl_special_account_anonymous.TabIndex = 0
    Me.lbl_special_account_anonymous.Text = "xAnonymous"
    Me.lbl_special_account_anonymous.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'gb_scheduling
    '
    Me.gb_scheduling.Controls.Add(Me.lbl_info_load)
    Me.gb_scheduling.Controls.Add(Me.lbl_info_process)
    Me.gb_scheduling.Controls.Add(Me.lbl_cfdi_minutes_load)
    Me.gb_scheduling.Controls.Add(Me.lbl_cfdi_minutes_process)
    Me.gb_scheduling.Controls.Add(Me.uc_entry_cfdi_retries)
    Me.gb_scheduling.Controls.Add(Me.lbl_cfdi_retries)
    Me.gb_scheduling.Controls.Add(Me.uc_entry_cfdi_scheduling_load)
    Me.gb_scheduling.Controls.Add(Me.lbl_cfdi_scheduling_load)
    Me.gb_scheduling.Controls.Add(Me.uc_entry_cfdi_scheduling_process)
    Me.gb_scheduling.Controls.Add(Me.lbl_cfdi_scheduling_process)
    Me.gb_scheduling.Location = New System.Drawing.Point(6, 138)
    Me.gb_scheduling.Name = "gb_scheduling"
    Me.gb_scheduling.Size = New System.Drawing.Size(652, 116)
    Me.gb_scheduling.TabIndex = 9
    Me.gb_scheduling.TabStop = False
    Me.gb_scheduling.Text = "xScheduling"
    '
    'lbl_info_load
    '
    Me.lbl_info_load.AutoSize = True
    Me.lbl_info_load.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_info_load.Location = New System.Drawing.Point(102, 44)
    Me.lbl_info_load.Name = "lbl_info_load"
    Me.lbl_info_load.Size = New System.Drawing.Size(64, 13)
    Me.lbl_info_load.TabIndex = 42
    Me.lbl_info_load.Text = "xInfoLoad"
    '
    'lbl_info_process
    '
    Me.lbl_info_process.AutoSize = True
    Me.lbl_info_process.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_info_process.Location = New System.Drawing.Point(102, 90)
    Me.lbl_info_process.Name = "lbl_info_process"
    Me.lbl_info_process.Size = New System.Drawing.Size(81, 13)
    Me.lbl_info_process.TabIndex = 43
    Me.lbl_info_process.Text = "xInfoProcess"
    '
    'lbl_cfdi_minutes_load
    '
    Me.lbl_cfdi_minutes_load.Location = New System.Drawing.Point(173, 17)
    Me.lbl_cfdi_minutes_load.Name = "lbl_cfdi_minutes_load"
    Me.lbl_cfdi_minutes_load.Size = New System.Drawing.Size(42, 25)
    Me.lbl_cfdi_minutes_load.TabIndex = 47
    Me.lbl_cfdi_minutes_load.Text = "xMin."
    Me.lbl_cfdi_minutes_load.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_cfdi_minutes_process
    '
    Me.lbl_cfdi_minutes_process.Location = New System.Drawing.Point(173, 63)
    Me.lbl_cfdi_minutes_process.Name = "lbl_cfdi_minutes_process"
    Me.lbl_cfdi_minutes_process.Size = New System.Drawing.Size(42, 25)
    Me.lbl_cfdi_minutes_process.TabIndex = 48
    Me.lbl_cfdi_minutes_process.Text = "xMin."
    Me.lbl_cfdi_minutes_process.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'uc_entry_cfdi_retries
    '
    Me.uc_entry_cfdi_retries.DoubleValue = 0.0R
    Me.uc_entry_cfdi_retries.IntegerValue = 0
    Me.uc_entry_cfdi_retries.IsReadOnly = False
    Me.uc_entry_cfdi_retries.Location = New System.Drawing.Point(315, 63)
    Me.uc_entry_cfdi_retries.Name = "uc_entry_cfdi_retries"
    Me.uc_entry_cfdi_retries.PlaceHolder = Nothing
    Me.uc_entry_cfdi_retries.Size = New System.Drawing.Size(66, 24)
    Me.uc_entry_cfdi_retries.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_retries.TabIndex = 41
    Me.uc_entry_cfdi_retries.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_retries.TextValue = ""
    Me.uc_entry_cfdi_retries.TextVisible = False
    Me.uc_entry_cfdi_retries.TextWidth = 0
    Me.uc_entry_cfdi_retries.Value = ""
    Me.uc_entry_cfdi_retries.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_retries
    '
    Me.lbl_cfdi_retries.Location = New System.Drawing.Point(221, 62)
    Me.lbl_cfdi_retries.Name = "lbl_cfdi_retries"
    Me.lbl_cfdi_retries.Size = New System.Drawing.Size(88, 25)
    Me.lbl_cfdi_retries.TabIndex = 4
    Me.lbl_cfdi_retries.Text = "xRetries"
    Me.lbl_cfdi_retries.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_entry_cfdi_scheduling_load
    '
    Me.uc_entry_cfdi_scheduling_load.DoubleValue = 0.0R
    Me.uc_entry_cfdi_scheduling_load.IntegerValue = 0
    Me.uc_entry_cfdi_scheduling_load.IsReadOnly = False
    Me.uc_entry_cfdi_scheduling_load.Location = New System.Drawing.Point(105, 17)
    Me.uc_entry_cfdi_scheduling_load.Name = "uc_entry_cfdi_scheduling_load"
    Me.uc_entry_cfdi_scheduling_load.PlaceHolder = Nothing
    Me.uc_entry_cfdi_scheduling_load.Size = New System.Drawing.Size(66, 24)
    Me.uc_entry_cfdi_scheduling_load.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_scheduling_load.TabIndex = 1
    Me.uc_entry_cfdi_scheduling_load.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_scheduling_load.TextValue = ""
    Me.uc_entry_cfdi_scheduling_load.TextVisible = False
    Me.uc_entry_cfdi_scheduling_load.TextWidth = 0
    Me.uc_entry_cfdi_scheduling_load.Value = ""
    Me.uc_entry_cfdi_scheduling_load.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_scheduling_load
    '
    Me.lbl_cfdi_scheduling_load.Location = New System.Drawing.Point(9, 17)
    Me.lbl_cfdi_scheduling_load.Name = "lbl_cfdi_scheduling_load"
    Me.lbl_cfdi_scheduling_load.Size = New System.Drawing.Size(90, 25)
    Me.lbl_cfdi_scheduling_load.TabIndex = 0
    Me.lbl_cfdi_scheduling_load.Text = "xSchLoad"
    Me.lbl_cfdi_scheduling_load.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_entry_cfdi_scheduling_process
    '
    Me.uc_entry_cfdi_scheduling_process.DoubleValue = 0.0R
    Me.uc_entry_cfdi_scheduling_process.IntegerValue = 0
    Me.uc_entry_cfdi_scheduling_process.IsReadOnly = False
    Me.uc_entry_cfdi_scheduling_process.Location = New System.Drawing.Point(105, 63)
    Me.uc_entry_cfdi_scheduling_process.Name = "uc_entry_cfdi_scheduling_process"
    Me.uc_entry_cfdi_scheduling_process.PlaceHolder = Nothing
    Me.uc_entry_cfdi_scheduling_process.Size = New System.Drawing.Size(66, 24)
    Me.uc_entry_cfdi_scheduling_process.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_scheduling_process.TabIndex = 3
    Me.uc_entry_cfdi_scheduling_process.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_scheduling_process.TextValue = ""
    Me.uc_entry_cfdi_scheduling_process.TextVisible = False
    Me.uc_entry_cfdi_scheduling_process.TextWidth = 0
    Me.uc_entry_cfdi_scheduling_process.Value = ""
    Me.uc_entry_cfdi_scheduling_process.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_scheduling_process
    '
    Me.lbl_cfdi_scheduling_process.Location = New System.Drawing.Point(6, 63)
    Me.lbl_cfdi_scheduling_process.Name = "lbl_cfdi_scheduling_process"
    Me.lbl_cfdi_scheduling_process.Size = New System.Drawing.Size(93, 25)
    Me.lbl_cfdi_scheduling_process.TabIndex = 2
    Me.lbl_cfdi_scheduling_process.Text = "xSchProcess"
    Me.lbl_cfdi_scheduling_process.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_entry_cfdi_site
    '
    Me.uc_entry_cfdi_site.DoubleValue = 0.0R
    Me.uc_entry_cfdi_site.IntegerValue = 0
    Me.uc_entry_cfdi_site.IsReadOnly = False
    Me.uc_entry_cfdi_site.Location = New System.Drawing.Point(111, 112)
    Me.uc_entry_cfdi_site.Name = "uc_entry_cfdi_site"
    Me.uc_entry_cfdi_site.PlaceHolder = Nothing
    Me.uc_entry_cfdi_site.Size = New System.Drawing.Size(167, 24)
    Me.uc_entry_cfdi_site.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_site.TabIndex = 7
    Me.uc_entry_cfdi_site.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_site.TextValue = ""
    Me.uc_entry_cfdi_site.TextVisible = False
    Me.uc_entry_cfdi_site.TextWidth = 0
    Me.uc_entry_cfdi_site.Value = ""
    Me.uc_entry_cfdi_site.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_site
    '
    Me.lbl_cfdi_site.Location = New System.Drawing.Point(17, 112)
    Me.lbl_cfdi_site.Name = "lbl_cfdi_site"
    Me.lbl_cfdi_site.Size = New System.Drawing.Size(88, 25)
    Me.lbl_cfdi_site.TabIndex = 6
    Me.lbl_cfdi_site.Text = "xSite"
    Me.lbl_cfdi_site.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_entry_cfdi_password
    '
    Me.uc_entry_cfdi_password.DoubleValue = 0.0R
    Me.uc_entry_cfdi_password.IntegerValue = 0
    Me.uc_entry_cfdi_password.IsReadOnly = False
    Me.uc_entry_cfdi_password.Location = New System.Drawing.Point(111, 82)
    Me.uc_entry_cfdi_password.Name = "uc_entry_cfdi_password"
    Me.uc_entry_cfdi_password.PlaceHolder = Nothing
    Me.uc_entry_cfdi_password.Size = New System.Drawing.Size(167, 24)
    Me.uc_entry_cfdi_password.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_password.TabIndex = 5
    Me.uc_entry_cfdi_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_password.TextValue = ""
    Me.uc_entry_cfdi_password.TextVisible = False
    Me.uc_entry_cfdi_password.TextWidth = 0
    Me.uc_entry_cfdi_password.Value = ""
    Me.uc_entry_cfdi_password.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_password
    '
    Me.lbl_cfdi_password.Location = New System.Drawing.Point(17, 82)
    Me.lbl_cfdi_password.Name = "lbl_cfdi_password"
    Me.lbl_cfdi_password.Size = New System.Drawing.Size(88, 25)
    Me.lbl_cfdi_password.TabIndex = 4
    Me.lbl_cfdi_password.Text = "xPassword"
    Me.lbl_cfdi_password.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_entry_cfdi_user
    '
    Me.uc_entry_cfdi_user.DoubleValue = 0.0R
    Me.uc_entry_cfdi_user.IntegerValue = 0
    Me.uc_entry_cfdi_user.IsReadOnly = False
    Me.uc_entry_cfdi_user.Location = New System.Drawing.Point(111, 52)
    Me.uc_entry_cfdi_user.Name = "uc_entry_cfdi_user"
    Me.uc_entry_cfdi_user.PlaceHolder = Nothing
    Me.uc_entry_cfdi_user.Size = New System.Drawing.Size(167, 24)
    Me.uc_entry_cfdi_user.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_user.TabIndex = 3
    Me.uc_entry_cfdi_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_user.TextValue = ""
    Me.uc_entry_cfdi_user.TextVisible = False
    Me.uc_entry_cfdi_user.TextWidth = 0
    Me.uc_entry_cfdi_user.Value = ""
    Me.uc_entry_cfdi_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_user
    '
    Me.lbl_cfdi_user.Location = New System.Drawing.Point(17, 52)
    Me.lbl_cfdi_user.Name = "lbl_cfdi_user"
    Me.lbl_cfdi_user.Size = New System.Drawing.Size(88, 25)
    Me.lbl_cfdi_user.TabIndex = 2
    Me.lbl_cfdi_user.Text = "xUser"
    Me.lbl_cfdi_user.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'uc_entry_cfdi_url
    '
    Me.uc_entry_cfdi_url.DoubleValue = 0.0R
    Me.uc_entry_cfdi_url.IntegerValue = 0
    Me.uc_entry_cfdi_url.IsReadOnly = False
    Me.uc_entry_cfdi_url.Location = New System.Drawing.Point(111, 18)
    Me.uc_entry_cfdi_url.Name = "uc_entry_cfdi_url"
    Me.uc_entry_cfdi_url.PlaceHolder = Nothing
    Me.uc_entry_cfdi_url.Size = New System.Drawing.Size(549, 24)
    Me.uc_entry_cfdi_url.SufixText = "Sufix Text"
    Me.uc_entry_cfdi_url.TabIndex = 1
    Me.uc_entry_cfdi_url.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_cfdi_url.TextValue = ""
    Me.uc_entry_cfdi_url.TextVisible = False
    Me.uc_entry_cfdi_url.TextWidth = 0
    Me.uc_entry_cfdi_url.Value = ""
    Me.uc_entry_cfdi_url.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_cfdi_url
    '
    Me.lbl_cfdi_url.Location = New System.Drawing.Point(17, 18)
    Me.lbl_cfdi_url.Name = "lbl_cfdi_url"
    Me.lbl_cfdi_url.Size = New System.Drawing.Size(88, 25)
    Me.lbl_cfdi_url.TabIndex = 0
    Me.lbl_cfdi_url.Text = "xURL"
    Me.lbl_cfdi_url.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Timer1
    '
    '
    'frm_cashier_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1196, 775)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cashier_configuration"
    Me.Text = "frm_cashier_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.tab_control.ResumeLayout(False)
    Me.TabWin.ResumeLayout(False)
    Me.TabAlesis.ResumeLayout(False)
    Me.TabPoints.ResumeLayout(False)
    Me.pnl_player_tracking.ResumeLayout(False)
    Me.gb_lower_panel.ResumeLayout(False)
    Me.gb_gift_expiration.ResumeLayout(False)
    Me.gb_providers.ResumeLayout(False)
    Me.gb_playsession.ResumeLayout(False)
    Me.gb_playsession.PerformLayout()
    Me.gb_level.ResumeLayout(False)
    Me.tab_control_points.ResumeLayout(False)
    Me.TabPointsDef.ResumeLayout(False)
    Me.TabPointsDef.PerformLayout()
    Me.TabLock.ResumeLayout(False)
    Me.TabPlayerLevelIcons.ResumeLayout(False)
    CType(Me.pb_level_04, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pb_level_03, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pb_level_02, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pb_level_01, System.ComponentModel.ISupportInitialize).EndInit()
    Me.gb_permanency.ResumeLayout(False)
    Me.gb_permanency.PerformLayout()
    Me.gb_inactivity.ResumeLayout(False)
    Me.gb_inactivity.PerformLayout()
    Me.TabTaxes.ResumeLayout(False)
    Me.pnl_taxes.ResumeLayout(False)
    Me.tab_business.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.TabPage2.ResumeLayout(False)
    Me.tab_taxes.ResumeLayout(False)
    Me.TabTaxesOnPrize.ResumeLayout(False)
    Me.pnl_taxes_on_prize.ResumeLayout(False)
    Me.TabTaxesOnPromotions.ResumeLayout(False)
    Me.pnl_taxes_on_promotions.ResumeLayout(False)
    Me.pnl_taxes_on_promotions.PerformLayout()
    Me.TabEvidence.ResumeLayout(False)
    Me.TabEvidence.PerformLayout()
    Me.TabCFDI.ResumeLayout(False)
    Me.gb_cfdi_resume.ResumeLayout(False)
    Me.gb_cfdi_configuration.ResumeLayout(False)
    Me.gb_special_account_data.ResumeLayout(False)
    Me.gb_scheduling.ResumeLayout(False)
    Me.gb_scheduling.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

  Friend WithEvents uc_cmb_cashier_type As GUI_Controls.uc_combo
  Friend WithEvents lbl_cashier_type As System.Windows.Forms.Label
  Friend WithEvents tab_control As System.Windows.Forms.TabControl
  Friend WithEvents TabWin As System.Windows.Forms.TabPage
  Friend WithEvents TabAlesis As System.Windows.Forms.TabPage
  Friend WithEvents uc_alesis_cashier_config As GUI_Controls.uc_alesis_cashier_config
  Friend WithEvents TabPoints As System.Windows.Forms.TabPage
  Friend WithEvents pnl_player_tracking As System.Windows.Forms.Panel
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_01_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_03_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_02_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_name As System.Windows.Forms.Label
  Friend WithEvents lbl_03_name As System.Windows.Forms.Label
  Friend WithEvents lbl_02_name As System.Windows.Forms.Label
  Friend WithEvents lbl_01_name As System.Windows.Forms.Label
  Friend WithEvents lbl_suffix_01 As System.Windows.Forms.Label
  Friend WithEvents TabTaxes As System.Windows.Forms.TabPage
  Friend WithEvents TabTaxesOnPrize As System.Windows.Forms.TabPage
  Friend WithEvents TabTaxesOnPromotions As System.Windows.Forms.TabPage
  Friend WithEvents tab_taxes As System.Windows.Forms.TabControl
  Friend WithEvents chk_split_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents pnl_taxes_on_prize As System.Windows.Forms.Panel
  Friend WithEvents pnl_taxes_on_promotions As System.Windows.Forms.Panel
  Friend WithEvents pnl_taxes As System.Windows.Forms.Panel
  'Friend WithEvents gb_promotion_taxes As System.Windows.Forms.GroupBox
  'Friend WithEvents gb_prize_taxes As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_tax_2_on_won_pct As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_tax_1_on_won_pct As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_state_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents lbl_federal_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents Uc_entry_tax_2_apply_if As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_tax_1_apply_if As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_promo_tax_2_on_won_pct As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_promo_tax_1_on_won_pct As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_state_promo_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents lbl_federal_promo_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents Uc_entry_promo_tax_2_apply_if As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_promo_tax_1_apply_if As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enable_taxes_redeem_promotions As System.Windows.Forms.CheckBox
  Friend WithEvents uc_entry_04_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_04_name As System.Windows.Forms.Label
  Friend WithEvents uc_entry_04_days_on_level As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_03_days_on_level As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_02_days_on_level As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_days_on_level As System.Windows.Forms.Label
  Friend WithEvents uc_entry_levels_days_counting_points As GUI_Controls.uc_entry_field
  Friend WithEvents tab_business As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
  Friend WithEvents uc_business_a As GUI_Controls.uc_taxes_config
  Friend WithEvents uc_business_b As GUI_Controls.uc_taxes_config
  Friend WithEvents TabEvidence As System.Windows.Forms.TabPage
  Friend WithEvents id_evidence_configuration As GUI_Controls.uc_identity_data
  Friend WithEvents uc_entry_04_points_to_keep As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_03_points_to_keep As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_02_points_to_keep As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_points_to_keep As System.Windows.Forms.Label
  Friend WithEvents chk_expiration_day_month As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_expiration_day_month As System.Windows.Forms.DateTimePicker
  Friend WithEvents gb_permanency As System.Windows.Forms.GroupBox
  Friend WithEvents chk_max_days_no_activity As System.Windows.Forms.CheckBox
  Friend WithEvents uc_entry_03_max_days_no_activity As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_04_max_days_no_activity As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_02_max_days_no_activity As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_max_days_no_activity As System.Windows.Forms.Label
  Friend WithEvents lbl_expiration_day_month As System.Windows.Forms.Label
  Friend WithEvents lbl_points_to_keep_information As System.Windows.Forms.Label
  Friend WithEvents gb_inactivity As System.Windows.Forms.GroupBox
  Friend WithEvents tab_control_points As System.Windows.Forms.TabControl
  Friend WithEvents TabPointsDef As System.Windows.Forms.TabPage
  Friend WithEvents uc_entry_04_points_to_enter As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_03_points_to_enter As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_02_points_to_enter As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_points_to_enter As System.Windows.Forms.Label
  Friend WithEvents TabLock As System.Windows.Forms.TabPage
  Friend WithEvents uc_cmb_Level1 As GUI_Controls.uc_combo
  Friend WithEvents uc_cmb_Level4 As GUI_Controls.uc_combo
  Friend WithEvents uc_cmb_Level3 As GUI_Controls.uc_combo
  Friend WithEvents uc_cmb_Level2 As GUI_Controls.uc_combo
  Friend WithEvents uc_wcp_cashier_config As GUI_Controls.uc_wcp_cashier_config
  Friend WithEvents lbl_points_program As System.Windows.Forms.Label
  'Friend WithEvents gb_taxes As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_tax_2_on_won_name As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_tax_1_on_won_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_taxes_name As System.Windows.Forms.Label
  Friend WithEvents gb_lower_panel As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_points_expiration_msg As System.Windows.Forms.Label
  Friend WithEvents gb_gift_expiration As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_gift_voucher_expiration As GUI_Controls.uc_entry_field
  Friend WithEvents gb_providers As System.Windows.Forms.GroupBox
  Friend WithEvents dg_providers As GUI_Controls.uc_grid
  Friend WithEvents gb_playsession As System.Windows.Forms.GroupBox
  Friend WithEvents chk_max_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_max_points_per_minute As System.Windows.Forms.CheckBox
  Friend WithEvents chk_has_mismatch As System.Windows.Forms.CheckBox
  Friend WithEvents chk_has_zero_plays As System.Windows.Forms.CheckBox
  Friend WithEvents uc_max_points_per_minute As GUI_Controls.uc_entry_field
  Friend WithEvents uc_max_points As GUI_Controls.uc_entry_field
  Friend WithEvents TabCFDI As System.Windows.Forms.TabPage
  Friend WithEvents gb_cfdi_configuration As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_cfdi_password As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_password As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_user As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_user As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_url As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_url As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_site As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_site As System.Windows.Forms.Label
  Friend WithEvents gb_cfdi_resume As System.Windows.Forms.GroupBox
  Friend WithEvents dg_cfdi_resume As GUI_Controls.uc_grid
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents gb_scheduling As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_info_load As System.Windows.Forms.Label
  Friend WithEvents lbl_info_process As System.Windows.Forms.Label
  Friend WithEvents lbl_cfdi_minutes_load As System.Windows.Forms.Label
  Friend WithEvents lbl_cfdi_minutes_process As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_retries As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_retries As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_scheduling_load As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_scheduling_load As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_scheduling_process As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cfdi_scheduling_process As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_active As System.Windows.Forms.CheckBox
  Friend WithEvents gb_special_account_data As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_cfdi_ef_anonymous As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_cfdi_rfc_anonymous As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_special_account_ef As System.Windows.Forms.Label
  Friend WithEvents lbl_special_account_rfc As System.Windows.Forms.Label
  Friend WithEvents uc_entry_cfdi_ef_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_cfdi_rfc_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_special_account_foreign As System.Windows.Forms.Label
  Friend WithEvents lbl_special_account_anonymous As System.Windows.Forms.Label
  Friend WithEvents Uc_entry_tax_3_on_won_name As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_promo_tax_3_apply_if As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_promo_tax_3_on_won_pct As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_tax3_promo_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents Uc_entry_tax_3_apply_if As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_tax_3_on_won_pct As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_tax3_tax_on_won_suffix As System.Windows.Forms.Label
  Friend WithEvents TabPlayerLevelIcons As System.Windows.Forms.TabPage
  Friend WithEvents cmb_player_level_icon_04 As GUI_Controls.uc_combo
  Friend WithEvents cmb_player_level_icon_03 As GUI_Controls.uc_combo
  Friend WithEvents cmb_player_level_icon_02 As GUI_Controls.uc_combo
  Friend WithEvents cmb_player_level_icon_01 As GUI_Controls.uc_combo
  Friend WithEvents pb_level_04 As System.Windows.Forms.PictureBox
  Friend WithEvents pb_level_03 As System.Windows.Forms.PictureBox
  Friend WithEvents pb_level_02 As System.Windows.Forms.PictureBox
  Friend WithEvents pb_level_01 As System.Windows.Forms.PictureBox

End Class
