<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_export_cash_cage_count
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.btn_acept = New GUI_Controls.uc_button
    Me.chk_print_option = New System.Windows.Forms.CheckBox
    Me.chk_excel_option = New System.Windows.Forms.CheckBox
    Me.lb_confirmation_message = New System.Windows.Forms.Label
    Me.lb_acceptornot = New System.Windows.Forms.Label
    Me.SuspendLayout()
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_cancel.Location = New System.Drawing.Point(322, 115)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 0
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_acept
    '
    Me.btn_acept.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_acept.Location = New System.Drawing.Point(208, 115)
    Me.btn_acept.Name = "btn_acept"
    Me.btn_acept.Size = New System.Drawing.Size(90, 30)
    Me.btn_acept.TabIndex = 1
    Me.btn_acept.ToolTipped = False
    Me.btn_acept.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'chk_print_option
    '
    Me.chk_print_option.AutoSize = True
    Me.chk_print_option.Location = New System.Drawing.Point(51, 40)
    Me.chk_print_option.Name = "chk_print_option"
    Me.chk_print_option.Size = New System.Drawing.Size(152, 17)
    Me.chk_print_option.TabIndex = 2
    Me.chk_print_option.Text = "xPrintCashCageCount"
    Me.chk_print_option.UseVisualStyleBackColor = True
    '
    'chk_excel_option
    '
    Me.chk_excel_option.AutoSize = True
    Me.chk_excel_option.Location = New System.Drawing.Point(51, 70)
    Me.chk_excel_option.Name = "chk_excel_option"
    Me.chk_excel_option.Size = New System.Drawing.Size(163, 17)
    Me.chk_excel_option.TabIndex = 3
    Me.chk_excel_option.Text = "xExportCashCageCount"
    Me.chk_excel_option.UseVisualStyleBackColor = True
    '
    'lb_confirmation_message
    '
    Me.lb_confirmation_message.AutoSize = True
    Me.lb_confirmation_message.Location = New System.Drawing.Point(19, 14)
    Me.lb_confirmation_message.Name = "lb_confirmation_message"
    Me.lb_confirmation_message.Size = New System.Drawing.Size(137, 13)
    Me.lb_confirmation_message.TabIndex = 4
    Me.lb_confirmation_message.Text = "xConfirmationMessage"
    Me.lb_confirmation_message.Visible = False
    '
    'lb_acceptornot
    '
    Me.lb_acceptornot.AutoSize = True
    Me.lb_acceptornot.Location = New System.Drawing.Point(19, 100)
    Me.lb_acceptornot.Name = "lb_acceptornot"
    Me.lb_acceptornot.Size = New System.Drawing.Size(79, 13)
    Me.lb_acceptornot.TabIndex = 5
    Me.lb_acceptornot.Text = "xAceptOrNot"
    Me.lb_acceptornot.Visible = False
    '
    'frm_export_cash_cage_count
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(432, 156)
    Me.Controls.Add(Me.lb_acceptornot)
    Me.Controls.Add(Me.lb_confirmation_message)
    Me.Controls.Add(Me.chk_excel_option)
    Me.Controls.Add(Me.chk_print_option)
    Me.Controls.Add(Me.btn_acept)
    Me.Controls.Add(Me.btn_cancel)
    Me.Name = "frm_export_cash_cage_count"
    Me.Text = "frm_export_cash_cage_count"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents btn_acept As GUI_Controls.uc_button
  Friend WithEvents chk_print_option As System.Windows.Forms.CheckBox
  Friend WithEvents chk_excel_option As System.Windows.Forms.CheckBox
  Friend WithEvents lb_confirmation_message As System.Windows.Forms.Label
  Friend WithEvents lb_acceptornot As System.Windows.Forms.Label
End Class
