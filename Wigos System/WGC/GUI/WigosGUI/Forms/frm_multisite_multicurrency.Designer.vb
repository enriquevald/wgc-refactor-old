<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_multisite_multicurrency
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.pn_separator = New System.Windows.Forms.Panel
    Me.uc_search = New GUI_Controls.uc_button
    Me.ef_main_currency = New GUI_Controls.uc_entry_field
    Me.dtp_day = New GUI_Controls.uc_date_picker
    Me.lbl_day_note = New GUI_Controls.uc_text_field
    Me.dg_coins = New GUI_Controls.uc_grid
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_day_note)
    Me.panel_data.Controls.Add(Me.dg_coins)
    Me.panel_data.Controls.Add(Me.ef_main_currency)
    Me.panel_data.Controls.Add(Me.dtp_day)
    Me.panel_data.Size = New System.Drawing.Size(350, 309)
    '
    'pn_separator
    '
    Me.pn_separator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pn_separator.Location = New System.Drawing.Point(16, 94)
    Me.pn_separator.Name = "pn_separator"
    Me.pn_separator.Size = New System.Drawing.Size(433, 5)
    Me.pn_separator.TabIndex = 0
    '
    'uc_search
    '
    Me.uc_search.DialogResult = System.Windows.Forms.DialogResult.None
    Me.uc_search.Location = New System.Drawing.Point(358, 44)
    Me.uc_search.Name = "uc_search"
    Me.uc_search.Size = New System.Drawing.Size(90, 30)
    Me.uc_search.TabIndex = 1
    Me.uc_search.ToolTipped = False
    Me.uc_search.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_main_currency
    '
    Me.ef_main_currency.DoubleValue = 0
    Me.ef_main_currency.IntegerValue = 0
    Me.ef_main_currency.IsReadOnly = False
    Me.ef_main_currency.Location = New System.Drawing.Point(61, 15)
    Me.ef_main_currency.Name = "ef_main_currency"
    Me.ef_main_currency.PlaceHolder = Nothing
    Me.ef_main_currency.Size = New System.Drawing.Size(199, 24)
    Me.ef_main_currency.SufixText = "Sufix Text"
    Me.ef_main_currency.SufixTextVisible = True
    Me.ef_main_currency.TabIndex = 3
    Me.ef_main_currency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_main_currency.TextValue = ""
    Me.ef_main_currency.TextWidth = 60
    Me.ef_main_currency.Value = ""
    Me.ef_main_currency.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_day
    '
    Me.dtp_day.Checked = True
    Me.dtp_day.IsReadOnly = False
    Me.dtp_day.Location = New System.Drawing.Point(61, 45)
    Me.dtp_day.Name = "dtp_day"
    Me.dtp_day.ShowCheckBox = False
    Me.dtp_day.ShowUpDown = False
    Me.dtp_day.Size = New System.Drawing.Size(199, 24)
    Me.dtp_day.SufixText = "Sufix Text"
    Me.dtp_day.SufixTextVisible = True
    Me.dtp_day.TabIndex = 2
    Me.dtp_day.TextWidth = 60
    Me.dtp_day.Value = New Date(2015, 4, 11, 0, 0, 0, 0)
    '
    'lbl_day_note
    '
    Me.lbl_day_note.IsReadOnly = True
    Me.lbl_day_note.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_day_note.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_day_note.Location = New System.Drawing.Point(26, 105)
    Me.lbl_day_note.Name = "lbl_day_note"
    Me.lbl_day_note.Size = New System.Drawing.Size(132, 24)
    Me.lbl_day_note.SufixText = "Sufix Text"
    Me.lbl_day_note.SufixTextVisible = True
    Me.lbl_day_note.TabIndex = 33
    Me.lbl_day_note.TextVisible = False
    Me.lbl_day_note.TextWidth = 0
    Me.lbl_day_note.Value = "xDayNote"
    '
    'dg_coins
    '
    Me.dg_coins.CurrentCol = -1
    Me.dg_coins.CurrentRow = -1
    Me.dg_coins.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_coins.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_coins.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_coins.Location = New System.Drawing.Point(10, 135)
    Me.dg_coins.Name = "dg_coins"
    Me.dg_coins.PanelRightVisible = False
    Me.dg_coins.Redraw = True
    Me.dg_coins.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_coins.Size = New System.Drawing.Size(332, 174)
    Me.dg_coins.Sortable = False
    Me.dg_coins.SortAscending = True
    Me.dg_coins.SortByCol = 0
    Me.dg_coins.TabIndex = 32
    Me.dg_coins.ToolTipped = True
    Me.dg_coins.TopRow = -2
    '
    'frm_multisite_multicurrency
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(452, 317)
    Me.Controls.Add(Me.pn_separator)
    Me.Controls.Add(Me.uc_search)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_multisite_multicurrency"
    Me.Text = "frm_jackpot_configuration"
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.Controls.SetChildIndex(Me.uc_search, 0)
    Me.Controls.SetChildIndex(Me.pn_separator, 0)
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents pn_separator As System.Windows.Forms.Panel
  Friend WithEvents uc_search As GUI_Controls.uc_button
  Friend WithEvents lbl_day_note As GUI_Controls.uc_text_field
  Friend WithEvents dg_coins As GUI_Controls.uc_grid
  Friend WithEvents ef_main_currency As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_day As GUI_Controls.uc_date_picker
End Class
