'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_about_us_sel
' DESCRIPTION:   About Us
' AUTHOR:        Gustavo Al�
' CREATION DATE: 14-SEP-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-SEP-2012  ANG    Initial version
' 23-FEB-2017  PDM    Bug 24976:Review Sprint 42 - About Us
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports System.Net
Imports System.Text
Imports System.IO
Imports Newtonsoft.Json

Public Class frm_about_us_sel
  Inherits GUI_Controls.frm_base_sel


#Region "Members"

#End Region

#Region " CONSTANT "


  Private Const GRID_COLUMNS As Integer = 4
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_WIDTH_TITLE As Integer = 2700
  Private Const GRID_WIDTH_CONTENT_TEXT As Integer = 8000

  Private Const GRID_COL_INDEX As Integer = 0
  Private Const GRID_COL_ID As Integer = 1
  Private Const GRID_COL_TITLE As Integer = 2
  Private Const GRID_COL_CONTENT_TEXT As Integer = 3


  Private Const SQL_COL_ID As Integer = 1
  Private Const SQL_COL_TITLE As Integer = 2
  Private Const SQL_COL_CONTENT_TEXT As Integer = 3

#End Region

#Region " OVERRIDES "


  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Me.Grid.Cell(RowIndex, GRID_COL_ID).Value = DbRow.Value(SQL_COL_ID).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_TITLE).Value = DbRow.Value(SQL_COL_TITLE).ToString()
    Me.Grid.Cell(RowIndex, GRID_COL_CONTENT_TEXT).Value = DbRow.Value(SQL_COL_CONTENT_TEXT).ToString

    Return True


  End Function

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ABOUT_US_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()


    ' Advertisement
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7592) + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7590)  ' Label form caption

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False


    ' INITIALIZE LABEL CAPTIONS


    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    '
    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls




  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetCustomDataTable() As DataTable
    Dim url As String = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/aboutus/getallbylanguage/1"
    Dim api As New CLS_API(url)
    Return JsonConvert.DeserializeObject(Of DataTable)(api.sanitizeData(api.getData()))
  End Function 'GUI_GetCustomDataTable

  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer
    Dim _has_changed As Boolean

    _table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"
    _has_changed = False
    _max_rows = GUI_MaxRows()

    Try

      'SaveSelectedGridIds()

      _table = GUI_GetCustomDataTable()

      Call GUI_ReportUpdateFilters()

      If _table Is Nothing Then

        Return
      End If

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      For _idx_row = 0 To _table.Rows.Count - 1

        If _idx_row >= Me.Grid.NumRows Then
          Me.Grid.AddRow()
        End If

        _db_row = New CLASS_DB_ROW(_table.Rows(_idx_row))
        Me.Grid.Redraw = False

        If GUI_SetupRow(_idx_row, _db_row) Then
          Me.Grid.Redraw = True
        End If

      Next

      While _idx_row < Me.Grid.NumRows
        Me.Grid.DeleteRow(_idx_row)
      End While

      Call GUI_AfterLastRow()

      'SelectLastSelectedRow()

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cash Monitor", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True
    Finally

    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Date selection 

    Return True
  End Function ' GUI_FilterCheck  


  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _au_id As Integer
    Dim _frm_edit As frm_about_us_edit 'GUI_Controls.frm_base_edit
    Dim _rows_to_move() As Integer

    _rows_to_move = Me.Grid.SelectedRows()

    ' ICS 18-DEC-2012: Check if there is a row selected
    If _rows_to_move Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(130), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return
    End If

    _idx_row = _rows_to_move(0)
    _au_id = Me.Grid.Cell(_idx_row, GRID_COL_ID).Value

    _frm_edit = New frm_about_us_edit

    Call _frm_edit.ShowEditItem(_au_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem


  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call ShowNewAboutUsForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE



  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _enabled As String = ""




    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterValueWidth(1) = 10000


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    ' Name


    ' Date


  End Sub ' GUI_ReportUpdateFilters

#End Region

#End Region

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False


      With .Column(GRID_COL_INDEX)
        .Header(0).Text = " "
        .Header(1).Text = " "
        .Width = 0
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' ID
      With .Column(GRID_COL_ID)
        .Width = 0
      End With
      'NAME
      With .Column(GRID_COL_TITLE)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7593) ' Label Name
        .Width = GRID_WIDTH_TITLE
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End With

      'DESCRIPTION
      With .Column(GRID_COL_CONTENT_TEXT)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7594) ' Header description
        .Width = GRID_WIDTH_CONTENT_TEXT
      End With

    End With
  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()


    Dim _current_date As Date
    Dim _initial_date As Date
    Dim _final_date As Date

    _current_date = WSI.Common.WGDB.Now

    _initial_date = New Date(_current_date.Year, _current_date.Month, 1)
    _final_date = _initial_date.AddMonths(1)




  End Sub


  ' PURPOSE: Get SQL WHERE 
  '     
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Sub ShowNewAboutUsForm()

    Dim frm_edit As frm_about_us_edit

    frm_edit = New frm_about_us_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewAboutUsForm

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

#End Region

End Class