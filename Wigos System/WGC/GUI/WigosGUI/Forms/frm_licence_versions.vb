'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_licence_versions
'
' DESCRIPTION : This screen allows to browse the installed software licences
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-APR-2009  APB    Initial version
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_licence_versions
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents cb_latest As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.cb_latest = New System.Windows.Forms.CheckBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(529, 107)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 111)
    Me.panel_data.Size = New System.Drawing.Size(529, 495)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(523, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(523, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.cb_latest)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(256, 96)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'cb_latest
    '
    Me.cb_latest.Location = New System.Drawing.Point(62, 19)
    Me.cb_latest.Name = "cb_latest"
    Me.cb_latest.Size = New System.Drawing.Size(140, 17)
    Me.cb_latest.TabIndex = 4
    Me.cb_latest.Text = "xLatest"
    Me.cb_latest.UseVisualStyleBackColor = True
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 66)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 42)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'frm_licence_versions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(537, 610)
    Me.Name = "frm_licence_versions"
    Me.Text = "frm_licence_versions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_IDENTIFIER As Integer = 0
  Private Const SQL_COLUMN_INSERTION_DATE As Integer = 1
  Private Const SQL_COLUMN_EXPIRATION_DATE As Integer = 2

  Private Const GRID_COLUMN_IDENTIFIER As Integer = 0
  Private Const GRID_COLUMN_INSERTION_DATE As Integer = 1
  Private Const GRID_COLUMN_EXPIRATION_DATE As Integer = 2

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Members "

  Private m_form_licence As New CLASS_LICENCE

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_latest As String


#End Region ' Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_LICENCE_VERSIONS

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(229)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    ' Cancel button text =>  "Exit"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)
    ' New button text =>  "New"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(3)

    ' Insertion Date
    Me.gb_date.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(205)
    Me.cb_latest.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(228)
    Me.dtp_from.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(206)
    Me.dtp_to.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(207)

    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT
        Call ShowLicenceDetails()

      Case ENUM_BUTTON.BUTTON_NEW
        Call AddNewLicence()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked And Me.cb_latest.Checked = False Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        ' 101 - "Error in dates interval: Initial date must be earlier than the final date."
        Call NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    If Me.cb_latest.Checked = True Then
      str_sql = "select TOP(1) WL_ID " _
                  & " , WL_INSERTION_DATE " _
                  & " , WL_EXPIRATION_DATE " _
               & " from LICENCES"
      str_sql = str_sql & GetSqlWhere()
      str_sql = str_sql & " order by WL_INSERTION_DATE DESC"
    Else
      str_sql = "select WL_ID " _
                  & " , WL_INSERTION_DATE " _
                  & " , WL_EXPIRATION_DATE " _
               & " from LICENCES"
      str_sql = str_sql & GetSqlWhere()
      str_sql = str_sql & " order by WL_INSERTION_DATE DESC"

    End If

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_IDENTIFIER).Value = DbRow.Value(SQL_COLUMN_IDENTIFIER)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INSERTION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_INSERTION_DATE), , ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    If Not DbRow.IsNull(SQL_COLUMN_EXPIRATION_DATE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXPIRATION_DATE), , ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATION_DATE).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(249)
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.cb_latest

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(250) & " " & GLB_NLS_GUI_SW_DOWNLOAD.GetString(206), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(250) & " " & GLB_NLS_GUI_SW_DOWNLOAD.GetString(207), m_date_to)

    PrintData.SetFilter(cb_latest.Text, m_latest)

    PrintData.FilterHeaderWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 1500

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_SW_DOWNLOAD.GetString(229)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_latest = ""

    If cb_latest.Checked = True Then
      m_latest = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_latest = GLB_NLS_GUI_INVOICING.GetString(480)
    End If


    ' Insertion Date
    If Me.dtp_from.Enabled = True And Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    End If

    If Me.dtp_to.Enabled = True And Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Internal identifier
      .Column(GRID_COLUMN_IDENTIFIER).Header.Text = ""
      .Column(GRID_COLUMN_IDENTIFIER).Width = 0

      ' Licence Insertion
      .Column(GRID_COLUMN_INSERTION_DATE).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(234)
      .Column(GRID_COLUMN_INSERTION_DATE).Width = 2500
      .Column(GRID_COLUMN_INSERTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Licence Expiration
      .Column(GRID_COLUMN_EXPIRATION_DATE).Header.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(255)
      .Column(GRID_COLUMN_EXPIRATION_DATE).Width = 2500
      .Column(GRID_COLUMN_EXPIRATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Set default values to filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = False

    Me.dtp_to.Value = New DateTime(Now.Year, Now.Month, Now.Day, 23, 59, 59)
    Me.dtp_to.Checked = False

    Me.cb_latest.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE : Get the SQL WHERE
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function GetSqlWhere() As String

    Dim str_where As String
    Dim aux_date As DateTime

    str_where = ""

    ' Insertion Date 
    If Me.dtp_from.Checked = True And Me.cb_latest.Checked = False Then
      str_where = str_where & " AND (WL_INSERTION_DATE >= " & GUI_FormatDateDB(dtp_from.Value) & ")"
    End If

    If Me.dtp_to.Checked = True And Me.cb_latest.Checked = False Then
      aux_date = dtp_to.Value
      aux_date = aux_date.AddSeconds(1)
      str_where = str_where & " AND (WL_INSERTION_DATE < " & GUI_FormatDateDB(aux_date) & ")"
    End If

    ' Final processing
    If Len(str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where

  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new software version package to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowLicenceDetails()

    Dim idx_row As Short
    Dim row_selected As Boolean
    Dim frm_edit As Object
    Dim licence As New CLASS_LICENCE

    ' Get the selected row
    row_selected = False
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        row_selected = True

        Exit For
      End If
    Next

    If Not row_selected Then
      ' No selected row 
      Call Me.Grid.Focus()

      Exit Sub
    End If

    frm_edit = New frm_licence_details

    ' Show the selected Licence
    licence.Identifier = Me.Grid.Cell(idx_row, GRID_COLUMN_IDENTIFIER).Value

    Call frm_edit.ShowEditItem(licence)

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowPackageDetails

  ' PURPOSE : Adds a new licence to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub AddNewLicence()

    Dim frm_edit As Object

    frm_edit = New frm_licence_details

    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' AddNewLicence

#End Region ' Private Functions

#Region "Events"

  Private Sub cb_latest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_latest.CheckedChanged
    If Me.cb_latest.Checked = True Then
      Me.dtp_from.Enabled = False
      Me.dtp_to.Enabled = False
    Else
      Me.dtp_from.Enabled = True
      Me.dtp_to.Enabled = True
    End If
  End Sub ' cb_latest_CheckedChanged

#End Region  ' Events

End Class
