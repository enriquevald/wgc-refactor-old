<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_lcd_message_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_lcd_message_edit))
    Me.cmb_categories = New GUI_Controls.uc_combo
    Me.uc_schedule = New GUI_Controls.uc_schedule
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.uc_terminals_group_filter = New GUI_Controls.uc_terminals_group_filter
    Me.uc_site_select = New GUI_Controls.uc_sites_sel
    Me.ef_message_line_1 = New GUI_Controls.uc_entry_field
    Me.ef_message_line_2 = New GUI_Controls.uc_entry_field
    Me.gb_message = New System.Windows.Forms.GroupBox
    Me.panel_data.SuspendLayout()
    Me.gb_message.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_message)
    Me.panel_data.Controls.Add(Me.uc_site_select)
    Me.panel_data.Controls.Add(Me.uc_terminals_group_filter)
    Me.panel_data.Controls.Add(Me.uc_schedule)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(443, 573)
    '
    'cmb_categories
    '
    Me.cmb_categories.AllowUnlistedValues = False
    Me.cmb_categories.AutoCompleteMode = False
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(6, 68)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(268, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 3
    Me.cmb_categories.TextWidth = 87
    '
    'uc_schedule
    '
    Me.uc_schedule.CheckedDateTo = True
    Me.uc_schedule.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_schedule.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_schedule.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_schedule.Location = New System.Drawing.Point(4, 166)
    Me.uc_schedule.Name = "uc_schedule"
    Me.uc_schedule.SecondTime = False
    Me.uc_schedule.SecondTimeFrom = 0
    Me.uc_schedule.SecondTimeTo = 0
    Me.uc_schedule.ShowCheckBoxDateTo = False
    Me.uc_schedule.Size = New System.Drawing.Size(410, 239)
    Me.uc_schedule.TabIndex = 8
    Me.uc_schedule.TimeFrom = 0
    Me.uc_schedule.TimeTo = 0
    Me.uc_schedule.Weekday = 127
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(303, 74)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 15
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'uc_terminals_group_filter
    '
    Me.uc_terminals_group_filter.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_filter.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_filter.HeightOnExpanded = 300
    Me.uc_terminals_group_filter.Location = New System.Drawing.Point(6, 105)
    Me.uc_terminals_group_filter.Name = "uc_terminals_group_filter"
    Me.uc_terminals_group_filter.SelectedIndex = 0
    Me.uc_terminals_group_filter.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_filter.SetGroupBoxText = "#5477"
    Me.uc_terminals_group_filter.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_filter.ShowGroupBoxLine = True
    Me.uc_terminals_group_filter.ShowGroups = Nothing
    Me.uc_terminals_group_filter.Size = New System.Drawing.Size(402, 54)
    Me.uc_terminals_group_filter.TabIndex = 17
    Me.uc_terminals_group_filter.ThreeStateCheckMode = True
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(6, 401)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(334, 168)
    Me.uc_site_select.TabIndex = 2
    '
    'ef_message_line_1
    '
    Me.ef_message_line_1.DoubleValue = 0
    Me.ef_message_line_1.IntegerValue = 0
    Me.ef_message_line_1.IsReadOnly = False
    Me.ef_message_line_1.Location = New System.Drawing.Point(13, 13)
    Me.ef_message_line_1.Name = "ef_message_line_1"
    Me.ef_message_line_1.Size = New System.Drawing.Size(262, 25)
    Me.ef_message_line_1.SufixText = "Sufix Text"
    Me.ef_message_line_1.SufixTextVisible = True
    Me.ef_message_line_1.TabIndex = 18
    Me.ef_message_line_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_message_line_1.TextValue = ""
    Me.ef_message_line_1.Value = ""
    Me.ef_message_line_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_message_line_2
    '
    Me.ef_message_line_2.DoubleValue = 0
    Me.ef_message_line_2.IntegerValue = 0
    Me.ef_message_line_2.IsReadOnly = False
    Me.ef_message_line_2.Location = New System.Drawing.Point(13, 40)
    Me.ef_message_line_2.Name = "ef_message_line_2"
    Me.ef_message_line_2.Size = New System.Drawing.Size(262, 25)
    Me.ef_message_line_2.SufixText = "Sufix Text"
    Me.ef_message_line_2.SufixTextVisible = True
    Me.ef_message_line_2.TabIndex = 19
    Me.ef_message_line_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_message_line_2.TextValue = ""
    Me.ef_message_line_2.Value = ""
    Me.ef_message_line_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_message
    '
    Me.gb_message.Controls.Add(Me.ef_message_line_1)
    Me.gb_message.Controls.Add(Me.ef_message_line_2)
    Me.gb_message.Controls.Add(Me.chk_enabled)
    Me.gb_message.Controls.Add(Me.cmb_categories)
    Me.gb_message.Location = New System.Drawing.Point(9, 5)
    Me.gb_message.Name = "gb_message"
    Me.gb_message.Size = New System.Drawing.Size(399, 99)
    Me.gb_message.TabIndex = 20
    Me.gb_message.TabStop = False
    Me.gb_message.Text = "xMessage"
    '
    'frm_lcd_message_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(547, 580)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_lcd_message_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_lcd_message_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_message.ResumeLayout(False)
    Me.gb_message.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents uc_schedule As GUI_Controls.uc_schedule
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents uc_terminals_group_filter As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents ef_message_line_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_message_line_1 As GUI_Controls.uc_entry_field
  Friend WithEvents gb_message As System.Windows.Forms.GroupBox
End Class
