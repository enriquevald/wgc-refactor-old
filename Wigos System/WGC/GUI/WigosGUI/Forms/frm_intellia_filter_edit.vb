﻿
'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_intellia_filter_edit
' DESCRIPTION:   Intellia filter editor
' AUTHOR:        Pablo Molina
' CREATION DATE: 22-JUN-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-JUN-2017  PDM    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class frm_intellia_filter_edit
  Inherits frm_base_edit

#Region ""
  Private m_class_base As New CLASS_INTELLIA_FILTERS
  Private m_id_filter_selected As Integer
  Private m_num_rows_add_sub As Integer
  Private m_max_num_rows As Integer
  Private m_id_temp As Integer
  Private m_operators As Dictionary(Of Integer, String)
  Private m_press_enter As Boolean
  Private m_intents As Integer
#End Region

#Region "Contructor"

  ' PURPOSE: Constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    m_num_rows_add_sub = 0
    m_id_temp = 0
    m_max_num_rows = 15

    DbEditedObject = m_class_base

    DbReadObject = New CLASS_INTELLIA_FILTERS

  End Sub

#End Region

#Region "Overrides"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_INTELLIA_FILTER_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub              ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8427)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.btn_add_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1684)
    Me.btn_del_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1683)
    Me.grb_filter_information.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8451)
    Me.btn_color.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8445)
    SetStyleGridFilters()
    FillGridFilters()

    AddHandler uc_grid_filters.RowSelectedEvent, AddressOf uc_grid_filters_RowSelectedEvent
    AddHandler uc_grid_filters.CellDataChangedEvent, AddressOf uc_grid_filters_CellDataChangedEvent

  End Sub        ' GUI_InitControls

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

        If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND Then
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), ENUM_MB_TYPE.MB_TYPE_ERROR)

        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub        ' GUI_DB_Operation

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '         TRUE: if everything goes ok
  '         FALSE: if fails
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean


    If Not HasOneUnknown() Then
      Return False
    End If

    If Not IsRangesValid() Then
      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region

#Region "Private Functions"

  Private Sub SetStyleGridFilters()

    With Me.uc_grid_filters

      Call .Init(5, 1, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False
      .EditableCellShowMode = uc_grid.GRID_SHOW_EDIT_MODE.SHOW_BORDER

      ' ID
      With .Column(0)
        .Width = 0
      End With

      'NAME
      With .Column(1)
        .Width = 7000
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428)
      End With

      'USED
      With .Column(2)
        .Width = 1300
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8429)
        .ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_BOOLEAN
        .Editable = True
        .EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      End With

      With .Column(3)
        .Width = 1300
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8430)
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      With .Column(4)
        .Width = 0
      End With

    End With

  End Sub

  Private Sub SetStyleGridRanges(ByVal AddUnknown As Boolean)

    m_operators = ListOperators(AddUnknown)

    With Me.uc_grid_ranges

      Call .Init(8, 1, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False
      .EditableCellShowMode = uc_grid.GRID_SHOW_EDIT_MODE.SHOW_BORDER

      ' ID
      With .Column(0)
        .Width = 0
      End With

      'NAME
      With .Column(1)
        .Width = 2100
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428)
        .ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_STRING
        .EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20, 0)
        .Editable = True
      End With

      'USED
      With .Column(2)
        .Width = 1600
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8442)
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_COMBO
        .EditionControl.ComboBox.IsReadOnly = False
        .EditionControl.ComboBox.Add(m_operators)
        .Editable = True
      End With

      With .Column(3)
        .Width = 1500
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8443)
        .ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_DECIMAL
        .EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 7, 2)
        .Editable = True
      End With

      With .Column(4)
        .Width = 1500
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8444)
        .ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_DECIMAL
        .EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES, 7, 2)
        .Editable = True
      End With

      With .Column(5)
        .Width = 1200
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8445)
        .Editable = False
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      ' ID
      With .Column(6)
        .Width = 0
      End With

      ' ID
      With .Column(7)
        .Width = 0
      End With

    End With


  End Sub


  Private Sub FillGridFilters()
    Dim _index As Integer

    _index = 0

    Me.uc_grid_filters.Clear()

    Me.uc_grid_filters.AddRow()

    Me.uc_grid_filters.Cell(_index, 0).Value = -1
    Me.uc_grid_filters.Cell(_index, 1).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8431)
    Me.uc_grid_filters.Cell(_index, 2).Value = False
    Me.uc_grid_filters.Cell(_index, 3).Value = ""
    Me.uc_grid_filters.Row(_index).BackColor = Color.Gray

    _index = _index + 1

    If m_class_base.Filters.Count > 0 Then

      For Each _item As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS In m_class_base.Filters.FindAll(Function(x) x.Type = 60)

        Me.uc_grid_filters.AddRow()
        Me.uc_grid_filters.Cell(_index, 0).Value = _item.Id
        Me.uc_grid_filters.Cell(_index, 1).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_item.Nls)
        Me.uc_grid_filters.Cell(_index, 2).Value = _item.Visible
        Me.uc_grid_filters.Cell(_index, 3).Value = IIf(_item.Editable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))
        Me.uc_grid_filters.Cell(_index, 4).Value = _item.HasUnknown
        Me.uc_grid_filters.Cell(_index, 3).BackColor = Color.LightGray

        If _item.Editable = 0 Then
          Me.uc_grid_filters.Row(_index).BackColor = Color.LightGray
        End If

        _index = _index + 1

      Next

      Me.uc_grid_filters.AddRow()

      Me.uc_grid_filters.Cell(_index, 0).Value = -1
      Me.uc_grid_filters.Cell(_index, 1).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8439)
      Me.uc_grid_filters.Cell(_index, 2).Value = False
      Me.uc_grid_filters.Cell(_index, 3).Value = ""
      Me.uc_grid_filters.Row(_index).BackColor = Color.Gray

      _index = _index + 1

      For Each _item As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS In m_class_base.Filters.FindAll(Function(x) x.Type = 70)

        Me.uc_grid_filters.AddRow()
        Me.uc_grid_filters.Cell(_index, 0).Value = _item.Id
        Me.uc_grid_filters.Cell(_index, 1).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_item.Nls)
        Me.uc_grid_filters.Cell(_index, 2).Value = _item.Visible
        Me.uc_grid_filters.Cell(_index, 3).Value = IIf(_item.Editable, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305))
        Me.uc_grid_filters.Cell(_index, 4).Value = _item.HasUnknown
        Me.uc_grid_filters.Cell(_index, 3).BackColor = Color.LightGray

        If _item.Editable = 0 Then
          Me.uc_grid_filters.Row(_index).BackColor = Color.LightGray
        End If

        _index = _index + 1

      Next

    End If

    SelectFirstRow()

    uc_grid_filters_RowSelectedEvent(1)

    uc_grid_ranges_RowSelect(0)

  End Sub

  Private Sub SelectFirstRow()

    Dim redraw_state As Boolean

    redraw_state = Me.uc_grid_filters.Redraw
    Me.uc_grid_filters.Redraw = False

    If Me.uc_grid_filters.NumRows > 0 Then
      Me.uc_grid_filters.IsSelected(1) = True
    End If

    Me.uc_grid_filters.Redraw = redraw_state

  End Sub

  Private Sub FillGridRanges(ByVal Id As Integer)

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    Dim _index As Integer
    Dim _ranges As List(Of CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES)

    _filter = m_class_base.Filters.Find(Function(x) x.Id = Id)
    _index = 0
    m_num_rows_add_sub = 0

    SetStyleGridRanges(_filter.HasUnknown)

    Me.uc_grid_ranges.Clear()

    _ranges = _filter.Ranges.FindAll(Function(x) x.Deleted = False)
    _ranges.Sort(AddressOf Comparer)

    If Not _filter Is Nothing Then

      For Each _range As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES In _ranges

        Me.uc_grid_ranges.AddRow()
        Me.uc_grid_ranges.Cell(_index, 0).Value = _range.Id
        Me.uc_grid_ranges.Cell(_index, 1).Value = _range.Title
        Me.uc_grid_ranges.Cell(_index, 2).Value = TransformComboValueToText(_range.OperatorValue)
        Me.uc_grid_ranges.Cell(_index, 3).Value = _range.Field1
        Me.uc_grid_ranges.Cell(_index, 4).Value = _range.Field2
        Me.uc_grid_ranges.Cell(_index, 5).Value = ""
        Me.uc_grid_ranges.Cell(_index, 5).BackColor = ColorTranslator.FromHtml(_range.Color)
        Me.uc_grid_ranges.Cell(_index, 6).Value = _range.IdTemp
        Me.uc_grid_ranges.Cell(_index, 7).Value = _range.Color

        _index = _index + 1
        m_num_rows_add_sub = m_num_rows_add_sub + 1
      Next

    End If

    If uc_grid_ranges.NumRows > 0 Then
      uc_grid_ranges.SelectFirstRow(True)
    End If

    If m_num_rows_add_sub >= m_max_num_rows Then
      Me.btn_add_range.Enabled = False
    End If

  End Sub

  Private Function Comparer(ByVal x As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES, ByVal y As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES) As Integer
    Dim result As Integer = x.OperatorValue.CompareTo(y.OperatorValue)
    If result = 0 Then
      result = x.Field1.CompareTo(y.Field1)
    End If
    Return result
  End Function

  Private Sub UpdateOperatorField(ByVal IdRange As Integer, ByVal IdTempRange As Integer, ByVal Value As Integer)

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      _filter.Ranges.Find(Function(x) x.Id = IdRange).OperatorField = CLASS_INTELLIA_FILTERS.IntToOperator(Value)
      _filter.Ranges.Find(Function(x) x.Id = IdRange).OperatorValue = Value
    ElseIf IdTempRange > 0 Then
      _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).OperatorField = CLASS_INTELLIA_FILTERS.IntToOperator(Value)
      _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).OperatorValue = Value
    End If

  End Sub

  Private Sub UpdateTitle(ByVal IdRange As Integer, ByVal IdTempRange As Integer, ByVal Value As String)

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      _filter.Ranges.Find(Function(x) x.Id = IdRange).Title = Value
    ElseIf IdTempRange > 0 Then
      _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).Title = Value
    End If

  End Sub

  Private Sub UpdateField1(ByVal IdRange As Integer, ByVal IdTempRange As Integer, ByVal Value As Decimal)

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      _filter.Ranges.Find(Function(x) x.Id = IdRange).Field1 = Value
    ElseIf IdTempRange > 0 Then
      _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).Field1 = Value
    End If

  End Sub

  Private Sub UpdateField2(ByVal IdRange As Integer, ByVal IdTempRange As Integer, ByVal Value As Decimal)

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      _filter.Ranges.Find(Function(x) x.Id = IdRange).Field2 = Value
    ElseIf IdTempRange > 0 Then
      _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).Field2 = Value
    End If

  End Sub

  Private Sub UpdateColor(ByVal IdRange As Integer, ByVal IdTempRange As Integer, ByVal Value As String)

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      _filter.Ranges.Find(Function(x) x.Id = IdRange).Color = Value
    ElseIf IdTempRange > 0 Then
      _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).Color = Value
    End If

  End Sub

  Private Function GetLastTitle(ByVal IdRange As Integer, ByVal IdTempRange As Integer) As String

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      Return _filter.Ranges.Find(Function(x) x.Id = IdRange).Title
    ElseIf IdTempRange > 0 Then
      Return _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).Title
    Else
      Return String.Empty
    End If

  End Function


  Private Function GetLastOperator(ByVal IdRange As Integer, ByVal IdTempRange As Integer) As String

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If IdRange > 0 Then
      Return _filter.Ranges.Find(Function(x) x.Id = IdRange).OperatorValue
    ElseIf IdTempRange > 0 Then
      Return _filter.Ranges.Find(Function(x) x.IdTemp = IdTempRange).OperatorValue
    Else
      Return String.Empty
    End If

  End Function

  Function IsColorSelected(ByVal Color As String) As Boolean

    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    For Each _range As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES In _filter.Ranges.FindAll(Function(x) x.Deleted = False)
      If _range.Color = Color Then
        Return True
      End If
    Next

    Return False

  End Function

  ' PURPOSE:  Return the list of operators
  '
  '  PARAMS:
  '     - INPUT:
  '           - AddUnknown AS Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Dictionary(Of Integer, String)
  Private Function ListOperators(Optional ByVal AddUnknown As Boolean = False) As Dictionary(Of Integer, String)
    Dim _list As Dictionary(Of Integer, String)
    _list = New Dictionary(Of Integer, String)

    If AddUnknown Then
      _list.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8446))
    End If

    _list.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8449))
    _list.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8447))
    _list.Add(3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8448))
    _list.Add(4, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8450))

    Return _list

  End Function


  ' PURPOSE:  Transform Combo Value to Text
  '
  '  PARAMS:
  '     - INPUT:
  '           - value AS Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - string
  Private Function TransformComboValueToText(ByVal value As Integer) As String
    Dim resp As String = ""
    For Each entry As KeyValuePair(Of Integer, String) In m_operators
      If entry.Key = value Then
        resp = entry.Value
      End If
    Next
    Return resp
  End Function ' TransformComboValueToText

  ' PURPOSE:  Transform Combo Text to Value
  '
  '  PARAMS:
  '     - INPUT:
  '           - text AS String
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer
  Private Function TransformComboTextToValue(ByVal text As String) As Integer
    Dim value As Integer
    For Each entry As KeyValuePair(Of Integer, String) In m_operators
      If entry.Value = text Then
        value = entry.Key
      End If
    Next

    Return value
  End Function ' TransformValueToText

  ' PURPOSE: Delete a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - _idx_row As Integer 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DeleteRow(ByVal _idx_row As Integer)

    Dim _id As Integer
    Dim _id_temp As Integer
    Dim _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS
    Dim _range As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES

    _id = uc_grid_ranges.Cell(_idx_row, 0).Value
    _id_temp = uc_grid_ranges.Cell(_idx_row, 6).Value
    _filter = m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected)

    If _id > 0 Then
      _filter.Ranges.Find(Function(x) x.Id = _id).Deleted = True
    ElseIf _id_temp > 0 Then
      _range = _filter.Ranges.Find(Function(x) x.IdTemp = _id_temp)
      _filter.Ranges.Remove(_range)
    End If

    Me.uc_grid_ranges.IsSelected(_idx_row) = False
    Me.uc_grid_ranges.DeleteRow(_idx_row)

  End Sub ' DeleteRow

  ' PURPOSE: Insert a new row in grid and in BD
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function InsertRow() As Integer

    Dim _idx_row As Integer
    Dim _range As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES
    _range = New CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES

    m_id_temp = m_id_temp + 1

    _range.Id = 0
    _range.IdTemp = m_id_temp
    _range.Title = ""
    _range.OperatorValue = 2
    _range.OperatorField = CLASS_INTELLIA_FILTERS.IntToOperator(2)
    _range.Field1 = 0
    _range.Field2 = 0
    _range.Color = CLASS_INTELLIA_FILTERS.RandomColor()
    m_class_base.Filters.Find(Function(x) x.Id = m_id_filter_selected).Ranges.Add(_range)

    _idx_row = uc_grid_ranges.AddRow()

    Me.uc_grid_ranges.Cell(_idx_row, 0).Value = 0
    Me.uc_grid_ranges.Cell(_idx_row, 1).Value = ""
    Me.uc_grid_ranges.Cell(_idx_row, 2).Value = TransformComboValueToText(_range.OperatorValue)
    Me.uc_grid_ranges.Cell(_idx_row, 3).Value = 0
    Me.uc_grid_ranges.Cell(_idx_row, 4).Value = 0
    Me.uc_grid_ranges.Cell(_idx_row, 5).Value = ""
    Me.uc_grid_ranges.Cell(_idx_row, 5).BackColor = ColorTranslator.FromHtml(_range.Color)
    Me.uc_grid_ranges.Cell(_idx_row, 6).Value = m_id_temp
    Me.uc_grid_ranges.Cell(_idx_row, 7).Value = _range.Color

    Return _idx_row

  End Function ' InsertRow

  Function IsRangesValid() As Boolean

    Dim _ranges As List(Of CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES)
    Dim _name_range As String

    Try

      For Each _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS In m_class_base.Filters

        _name_range = _filter.Title.Trim()

        If String.IsNullOrEmpty(_name_range) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8351), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428)) ' "Field '%1' must not be left blank." param %1-> "Name"
          SelectFilter(_filter.Id)
          Return False
        End If

        _ranges = _filter.Ranges.FindAll(Function(x) x.Deleted = False)
        _ranges.Sort(AddressOf Comparer)

        If _ranges.Count > 0 And Not ValidateRanges(_ranges) Then
          SelectFilter(_filter.Id)
          Return False
        End If

      Next

    Catch ex As Exception
      Return False
    End Try

    Return True
  End Function

  Function ValidateRanges(ByVal Ranges As List(Of CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES)) As Boolean

    Dim _verify As Boolean
    Dim _valid As Boolean

    _verify = True
    _valid = True

    For Each _range As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES In Ranges

      _verify = True
      _valid = True

      'Verify the range title
      If String.IsNullOrEmpty(_range.Title.Trim()) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8351), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428)) ' "Field '%1' must not be left blank." param %1-> "Name"
        Return False
      End If

      'Compare ranges values.
      If _range.OperatorValue = 2 Then
        If _range.Field2 < _range.Field1 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8444), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8443))
          Return False
        End If
      End If

      'Compare overlap
      For Each _range_to_comparer As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES In Ranges

        _verify = True

        If _range.Id > 0 And _range_to_comparer.Id > 0 Then
          If _range.Id = _range_to_comparer.Id Then
            _verify = False
          End If
        ElseIf _range.IdTemp > 0 And _range_to_comparer.IdTemp > 0 Then
          If _range.IdTemp = _range_to_comparer.IdTemp Then
            _verify = False
          End If
        End If

        If _verify Then

          Select Case _range.OperatorValue
            Case 1
              _valid = CompareWithLessThan(_range.Field1, _range_to_comparer.OperatorValue, _range_to_comparer.Field1, _range_to_comparer.Field2)
            Case 2
              _valid = CompareWithBetween(_range.Field1, _range.Field2, _range_to_comparer.OperatorValue, _range_to_comparer.Field1, _range_to_comparer.Field2)
            Case 3
              _valid = CompareWithMoreThan(_range.Field1, _range_to_comparer.OperatorValue, _range_to_comparer.Field1, _range_to_comparer.Field2)
            Case 4
              _valid = CompareWithEqualThan(_range.Field1, _range_to_comparer.OperatorValue, _range_to_comparer.Field1, _range_to_comparer.Field2)
          End Select

        End If

        If Not _valid Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8456), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        End If
      Next

    Next

    Return True

  End Function

  Function CompareWithLessThan(ByVal MaxValue As Decimal, ByVal OperatorToCompare As Integer, ByVal Value1ToComparer As Decimal, Optional ByVal Value2ToComparer As Decimal = 0) As Boolean

    Select Case OperatorToCompare
      Case 1
        Return False
      Case 2, 3, 4
        If MaxValue > Value1ToComparer Then
          Return False
        End If
    End Select

    Return True
  End Function

  Function CompareWithBetween(ByVal MinValue As Decimal, ByVal MaxValue As Decimal, ByVal OperatorToCompare As Integer, ByVal Value1ToComparer As Decimal, Optional ByVal Value2ToComparer As Decimal = 0) As Boolean

    Select Case OperatorToCompare

      Case 1

        If Value1ToComparer > MinValue Then
          Return False
        End If

      Case 2

        If Value1ToComparer > MaxValue Then
          Return True
        End If

        If Value2ToComparer < MinValue Then
          Return True
        End If

        If Value1ToComparer >= MinValue And Value1ToComparer <= MaxValue Then
          Return False
        End If

        If Value2ToComparer >= MinValue And Value2ToComparer <= MaxValue Then
          Return False
        End If

        If Value1ToComparer <= MinValue And Value2ToComparer >= MaxValue Then
          Return False
        End If

      Case 3

        If Value1ToComparer < MaxValue Then
          Return False
        End If

      Case 4
        If Value1ToComparer >= MinValue And Value1ToComparer <= MaxValue Then
          Return False
        End If
    End Select

    Return True

  End Function

  Function CompareWithMoreThan(ByVal MinValue As Decimal, ByVal OperatorToCompare As Integer, ByVal Value1ToComparer As Decimal, Optional ByVal Value2ToComparer As Decimal = 0) As Boolean

    Select Case OperatorToCompare
      Case 1, 4
        If MinValue < Value1ToComparer Then
          Return False
        End If
      Case 2
        If MinValue < Value2ToComparer Then
          Return False
        End If
      Case 3
        Return False
    End Select

    Return True

  End Function

  Function CompareWithEqualThan(ByVal Value As Decimal, ByVal OperatorToCompare As Integer, ByVal Value1ToComparer As Decimal, Optional ByVal Value2ToComparer As Decimal = 0) As Boolean

    Select Case OperatorToCompare
      Case 1
        If Value < Value1ToComparer Then
          Return False
        End If
      Case 2
        If Value >= Value1ToComparer And Value <= Value2ToComparer Then
          Return False
        End If
      Case 3
        If Value > Value1ToComparer Then
          Return False
        End If
      Case 4
        If Value = Value1ToComparer Then
          Return False
        End If
    End Select

    Return True

  End Function


  Function HasOneUnknown() As Boolean
    Dim _ranges As List(Of CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS_RANGES)
    Dim _name_range As String

    Try

      For Each _filter As CLASS_INTELLIA_FILTERS.INTELLIA_FILTERS In m_class_base.Filters.FindAll(Function(x) x.HasUnknown = True)
        _name_range = _filter.Title
        _ranges = _filter.Ranges.FindAll(Function(x) x.Deleted = False And x.OperatorValue = 0)

        If _ranges.Count > 1 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8455), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, TransformComboValueToText(0))
          SelectFilter(_filter.Id)
          Return False
        End If

      Next

    Catch ex As Exception
      Return False
    End Try

    Return True
  End Function



  Sub SelectFilter(ByVal IdFilter As Integer)

    Dim _row As Integer
    RemoveHandler uc_grid_filters.RowSelectedEvent, AddressOf uc_grid_filters_RowSelectedEvent

    For _index As Integer = 0 To uc_grid_filters.NumRows - 1
      If uc_grid_filters.Cell(_index, 0).Value = IdFilter.ToString() Then
        Me.uc_grid_filters.IsSelected(_index) = True
        _row = _index
      Else
        Me.uc_grid_filters.IsSelected(_index) = False
      End If
    Next

    uc_grid_filters_RowSelectedEvent(_row)

    AddHandler uc_grid_filters.RowSelectedEvent, AddressOf uc_grid_filters_RowSelectedEvent

  End Sub


  Sub ShowEditionMode(ByVal Row As Integer, ByVal Column As Integer)

    uc_grid_ranges.Focus()
    uc_grid_ranges.Select()
    uc_grid_ranges.CurrentRow = Row
    uc_grid_ranges.CurrentCol = Column
    SendKeys.Send("{ENTER}")

  End Sub

  Function HasUnknownRow(ByVal Row As Integer) As Boolean
    Return True
  End Function

#End Region


#Region "Public Functions"

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Call Me.Display(False)

  End Sub

#End Region

#Region "Events"

  ' PURPOSE: Handler to control key pressed intro in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If uc_grid_ranges.ContainsFocus Then
          m_press_enter = True
          m_intents = 1
          uc_grid_ranges.KeyPressed(sender, e)
          m_press_enter = False
        End If

        Return True
      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function ' GUI_KeyPress


  Private Sub uc_grid_filters_RowSelectedEvent(SelectedRow As Integer)

    Dim _editable As Boolean
    Dim _id As Integer

    _id = CType(uc_grid_filters.Cell(SelectedRow, 0).Value, Integer)

    If _id = -1 Then
      _editable = False
    Else
      _editable = m_class_base.Filters.Find(Function(x) x.Id = _id).Editable
    End If

    Me.btn_add_range.Enabled = True

    Me.grb_filter_information.Enabled = _editable

    m_id_filter_selected = _id

    If _id = -1 Then
      Me.grb_filter_information.Enabled = False
      Me.uc_grid_ranges.Clear()
    Else
      FillGridRanges(m_id_filter_selected)
    End If


  End Sub


  Private Sub uc_grid_filters_CellDataChangedEvent(Row As Integer, Column As Integer)

    Dim _id As Integer
    Dim _value As Boolean

    _id = uc_grid_filters.Cell(Row, 0).Value
    _value = CType(uc_grid_filters.Cell(Row, Column).Value, Boolean)

    If _id = -1 Then
      uc_grid_filters.Cell(Row, Column).Value = False
    Else
      m_class_base.Filters.Find(Function(x) x.Id = _id).Visible = _value
    End If

  End Sub

  Private Sub uc_grid_ranges_RowSelect(SelectedRow As Integer) Handles uc_grid_ranges.RowSelectedEvent

    Dim _operator As Integer

    If uc_grid_ranges.NumRows = 0 Then
      Exit Sub
    End If

    _operator = TransformComboTextToValue(uc_grid_ranges.Cell(SelectedRow, 2).Value)

    If _operator = 0 Then
      btn_del_range.Enabled = False
    Else
      btn_del_range.Enabled = True
    End If

  End Sub

  Private Sub uc_grid_ranges_CellDataChangedEvent(Row As Integer, Column As Integer) Handles uc_grid_ranges.CellDataChangedEvent

    CellDataChange(Row, Column)

  End Sub

  Sub CellDataChange(Row As Integer, Column As Integer)

    Dim _id As Integer
    Dim _id_temp As Integer
    Dim _value As String
    Dim _field As Decimal
    Dim _operator As Integer


    Try

      _id = uc_grid_ranges.Cell(Row, 0).Value
      _id_temp = uc_grid_ranges.Cell(Row, 6).Value

      RemoveHandler uc_grid_ranges.CellDataChangedEvent, AddressOf uc_grid_ranges_CellDataChangedEvent

      Select Case Column

        Case 1

          _value = uc_grid_ranges.Cell(Row, Column).Value

          If String.IsNullOrEmpty(_value.Trim()) Then

            If m_press_enter And m_intents = 1 Or Not m_press_enter Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8351), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8428)) ' "Field '%1' must not be left blank." param %1-> "Name"
              m_intents = 0
            End If

            uc_grid_ranges.Cell(Row, Column).Value = GetLastTitle(_id, _id_temp)

            ShowEditionMode(Row, Column)

          Else
            UpdateTitle(_id, _id_temp, _value)
          End If

        Case 2

          Dim _update As Boolean
          _update = True

          _value = TransformComboTextToValue(uc_grid_ranges.Cell(Row, Column).Value)

          _operator = CInt(_value)

          If _operator = 0 Then

            If (HasUnknownRow(Row)) Then
              uc_grid_ranges.Cell(Row, Column).Value = TransformComboValueToText(GetLastOperator(_id, _id_temp).Trim())
              If m_press_enter And m_intents = 1 Or Not m_press_enter Then
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8455), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, TransformComboValueToText(0))
                m_intents = 0
              End If
              _update = False
            Else
            uc_grid_ranges.Cell(Row, 3).Value = ""
            uc_grid_ranges.Cell(Row, 4).Value = ""
            UpdateField2(_id, _id_temp, 0)
            UpdateField1(_id, _id_temp, 0)
            End If

          ElseIf _operator = 2 Then

          Else
            uc_grid_ranges.Cell(Row, 4).Value = ""
            UpdateField1(_id, _id_temp, 0)
          End If

          If _update Then
            UpdateOperatorField(_id, _id_temp, _value)
          End If


        Case 3

          _operator = TransformComboTextToValue(uc_grid_ranges.Cell(Row, 2).Value)

          If _operator = 0 Then
            uc_grid_ranges.Cell(Row, Column).Value = ""
            UpdateField1(_id, _id_temp, 0)
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8454), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8443))
          Else

            If uc_grid_ranges.Cell(Row, Column).Value = "" Then
              _field = 0
              uc_grid_ranges.Cell(Row, Column).Value = 0
            Else
              _field = CType(uc_grid_ranges.Cell(Row, Column).Value, Decimal)
            End If

            UpdateField1(_id, _id_temp, _field)

          End If

        Case 4

          _operator = TransformComboTextToValue(uc_grid_ranges.Cell(Row, 2).Value)

          If _operator = 2 Then

            If uc_grid_ranges.Cell(Row, Column).Value = "" Then
              _field = 0
            Else
              _field = CType(uc_grid_ranges.Cell(Row, Column).Value, Decimal)
            End If

            Dim _field1 As Decimal

            If uc_grid_ranges.Cell(Row, 3).Value = "" Then
              _field1 = 0
            Else
              _field1 = CType(uc_grid_ranges.Cell(Row, 3).Value, Decimal)
            End If

            If _field < _field1 Then
              _field = _field1
              If m_press_enter And m_intents = 1 Or Not m_press_enter Then
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8444), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8443))
                m_intents = 0
              End If
            End If

            UpdateField2(_id, _id_temp, _field)
            uc_grid_ranges.Cell(Row, Column).Value = _field

          Else
            uc_grid_ranges.Cell(Row, Column).Value = ""
            UpdateField2(_id, _id_temp, 0)
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8454), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8444))
          End If

      End Select

      AddHandler uc_grid_ranges.CellDataChangedEvent, AddressOf uc_grid_ranges_CellDataChangedEvent

    Catch ex As Exception

    Finally

    End Try

  End Sub


  Private Sub btn_del_range_Click(sender As Object, e As EventArgs) Handles btn_del_range.Click

    Dim _idx_row As Integer
    Dim _select_row() As Integer

    _select_row = Me.uc_grid_ranges.SelectedRows()

    If _select_row Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    If _select_row.Length = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _idx_row = _select_row(0)

    Me.DeleteRow(_idx_row)

    m_num_rows_add_sub = m_num_rows_add_sub - 1

    ' Check Max chips
    If Me.uc_grid_ranges.NumRows < m_max_num_rows Then
      btn_add_range.Enabled = True
    End If

  End Sub

  Private Sub btn_add_range_Click(sender As Object, e As EventArgs) Handles btn_add_range.Click

    Dim _idx_row As Integer

    m_num_rows_add_sub = m_num_rows_add_sub + 1

    _idx_row = Me.InsertRow()

    ' Focus on Row added
    uc_grid_ranges.ClearSelection()
    uc_grid_ranges.IsSelected(_idx_row) = True
    uc_grid_ranges.Redraw = True
    uc_grid_ranges.TopRow = _idx_row

    ' Check Max chips
    If m_num_rows_add_sub >= m_max_num_rows Then
      btn_add_range.Enabled = False
    End If

    uc_grid_ranges.Cell(uc_grid_ranges.NumRows - 1, 1).Value = "---"
    UpdateTitle(0, m_id_temp, "---")
    ShowEditionMode(uc_grid_ranges.NumRows - 1, 1)

  End Sub

  Private Sub btn_color_Click(sender As Object, e As EventArgs) Handles btn_color.Click

    Dim _idx_row As Integer
    Dim _select_row() As Integer
    Dim _color As String
    Dim _colorString As String
    Dim _id As Integer
    Dim _id_temp As Integer

    _select_row = Me.uc_grid_ranges.SelectedRows()

    If _select_row Is Nothing Then
      Return
    End If

    If _select_row.Length = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6926), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _idx_row = _select_row(0)

    _id = uc_grid_ranges.Cell(_idx_row, 0).Value
    _id_temp = uc_grid_ranges.Cell(_idx_row, 6).Value
    _color = uc_grid_ranges.Cell(_idx_row, 7).Value()

    If _color <> String.Empty Then
      cd_button_color.Color = ColorTranslator.FromHtml(_color)
    Else
      cd_button_color.Color = Color.Gray
    End If

    If cd_button_color.ShowDialog() = Windows.Forms.DialogResult.OK Then

      _colorString = String.Format("#{0:X2}{1:X2}{2:X2}", cd_button_color.Color.R, cd_button_color.Color.G, cd_button_color.Color.B)

      If IsColorSelected(_colorString) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8457), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return
      End If

      uc_grid_ranges.Cell(_idx_row, 5).BackColor = cd_button_color.Color
      UpdateColor(_id, _id_temp, _colorString)

    End If

  End Sub


  Private Sub uc_grid_ranges_BeforeStartEditionEvent(Row As Integer, Column As Integer, ByRef EditionCanceled As Boolean) Handles uc_grid_ranges.BeforeStartEditionEvent

    Dim _operator As Integer

    _operator = TransformComboTextToValue(uc_grid_ranges.Cell(Row, 2).Value)

    Select Case _operator
      Case 0
        If Column = 4 Or Column = 3 Or Column = 2 Then
          EditionCanceled = True
        End If
      Case 1, 3, 4
        If Column = 4 Then
          EditionCanceled = True
        End If
      Case Else
        'Nothing
    End Select

  End Sub

  Private Sub uc_grid_filters_BeforeStartEditionEvent(Row As Integer, Column As Integer, ByRef EditionCanceled As Boolean) Handles uc_grid_filters.BeforeStartEditionEvent

    Dim _id As Integer

    _id = uc_grid_filters.Cell(Row, 0).Value

    If _id = -1 And Column = 2 Or Column = 3 Then
      EditionCanceled = True
    End If

  End Sub

#End Region




End Class