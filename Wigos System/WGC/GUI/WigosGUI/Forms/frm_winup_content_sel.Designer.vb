﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_winup_content_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_enable = New System.Windows.Forms.GroupBox()
    Me.chk_disable = New System.Windows.Forms.CheckBox()
    Me.chk_enable = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.ef_title = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_enable.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_enable)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.ef_title)
    Me.panel_filter.Size = New System.Drawing.Size(896, 92)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_title, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enable, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 96)
    Me.panel_data.Size = New System.Drawing.Size(896, 486)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(890, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(890, 4)
    '
    'gb_enable
    '
    Me.gb_enable.Controls.Add(Me.chk_disable)
    Me.gb_enable.Controls.Add(Me.chk_enable)
    Me.gb_enable.Location = New System.Drawing.Point(288, 1)
    Me.gb_enable.Name = "gb_enable"
    Me.gb_enable.Size = New System.Drawing.Size(95, 90)
    Me.gb_enable.TabIndex = 20
    Me.gb_enable.TabStop = False
    Me.gb_enable.Text = "xBlocked"
    '
    'chk_disable
    '
    Me.chk_disable.Location = New System.Drawing.Point(12, 50)
    Me.chk_disable.Name = "chk_disable"
    Me.chk_disable.Size = New System.Drawing.Size(72, 16)
    Me.chk_disable.TabIndex = 1
    Me.chk_disable.Text = "No"
    '
    'chk_enable
    '
    Me.chk_enable.Location = New System.Drawing.Point(12, 30)
    Me.chk_enable.Name = "chk_enable"
    Me.chk_enable.Size = New System.Drawing.Size(72, 16)
    Me.chk_enable.TabIndex = 0
    Me.chk_enable.Text = "Yes"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(5, 1)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(272, 90)
    Me.gb_date.TabIndex = 22
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = False
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 49)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(258, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = False
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 21)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(258, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_title
    '
    Me.ef_title.AllowDrop = True
    Me.ef_title.DoubleValue = 0.0R
    Me.ef_title.IntegerValue = 0
    Me.ef_title.IsReadOnly = False
    Me.ef_title.Location = New System.Drawing.Point(390, 5)
    Me.ef_title.Name = "ef_title"
    Me.ef_title.PlaceHolder = Nothing
    Me.ef_title.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.ef_title.Size = New System.Drawing.Size(350, 24)
    Me.ef_title.SufixText = "Sufix Text"
    Me.ef_title.SufixTextVisible = True
    Me.ef_title.TabIndex = 21
    Me.ef_title.TabStop = False
    Me.ef_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_title.TextValue = ""
    Me.ef_title.TextWidth = 50
    Me.ef_title.Value = ""
    Me.ef_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_winup_content_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(904, 586)
    Me.Name = "frm_winup_content_sel"
    Me.Text = "frm_winup_content_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_enable.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_enable As System.Windows.Forms.GroupBox
  Friend WithEvents chk_disable As System.Windows.Forms.CheckBox
  Friend WithEvents chk_enable As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents ef_title As GUI_Controls.uc_entry_field
End Class
