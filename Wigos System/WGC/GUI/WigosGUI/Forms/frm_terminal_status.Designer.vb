<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_status
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_terminal_status))
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.cb_warning = New System.Windows.Forms.CheckBox()
    Me.cb_ok = New System.Windows.Forms.CheckBox()
    Me.cb_error = New System.Windows.Forms.CheckBox()
    Me.lbl_warning = New System.Windows.Forms.Label()
    Me.lbl_error = New System.Windows.Forms.Label()
    Me.lbl_ok = New System.Windows.Forms.Label()
    Me.gb_session = New System.Windows.Forms.GroupBox()
    Me.cb_session_free = New System.Windows.Forms.CheckBox()
    Me.cb_session_oc = New System.Windows.Forms.CheckBox()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.gb_alerts = New System.Windows.Forms.GroupBox()
    Me.btn_check_all = New GUI_Controls.uc_button()
    Me.btn_uncheck_all = New GUI_Controls.uc_button()
    Me.dg_alerts = New GUI_Controls.uc_grid()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.chk_show_all_alarms = New System.Windows.Forms.CheckBox()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_session.SuspendLayout()
    Me.gb_alerts.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Controls.Add(Me.chk_show_all_alarms)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.tf_last_update)
    Me.panel_filter.Controls.Add(Me.gb_alerts)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_session)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Size = New System.Drawing.Size(1240, 220)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_session, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_alerts, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_last_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_all_alarms, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 224)
    Me.panel_data.Size = New System.Drawing.Size(1240, 534)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1234, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1234, 4)
    '
    'tmr_monitor
    '
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.cb_warning)
    Me.gb_status.Controls.Add(Me.cb_ok)
    Me.gb_status.Controls.Add(Me.cb_error)
    Me.gb_status.Controls.Add(Me.lbl_warning)
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.lbl_ok)
    Me.gb_status.Location = New System.Drawing.Point(9, 7)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(130, 106)
    Me.gb_status.TabIndex = 0
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'cb_warning
    '
    Me.cb_warning.AutoSize = True
    Me.cb_warning.Checked = True
    Me.cb_warning.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_warning.Location = New System.Drawing.Point(30, 45)
    Me.cb_warning.Name = "cb_warning"
    Me.cb_warning.Size = New System.Drawing.Size(80, 17)
    Me.cb_warning.TabIndex = 3
    Me.cb_warning.Text = "xWarning"
    Me.cb_warning.UseVisualStyleBackColor = True
    '
    'cb_ok
    '
    Me.cb_ok.AutoSize = True
    Me.cb_ok.Location = New System.Drawing.Point(30, 19)
    Me.cb_ok.Name = "cb_ok"
    Me.cb_ok.Size = New System.Drawing.Size(49, 17)
    Me.cb_ok.TabIndex = 1
    Me.cb_ok.Text = "xOk"
    Me.cb_ok.UseVisualStyleBackColor = True
    '
    'cb_error
    '
    Me.cb_error.AutoSize = True
    Me.cb_error.Checked = True
    Me.cb_error.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_error.Location = New System.Drawing.Point(30, 74)
    Me.cb_error.Name = "cb_error"
    Me.cb_error.Size = New System.Drawing.Size(62, 17)
    Me.cb_error.TabIndex = 5
    Me.cb_error.Text = "xError"
    Me.cb_error.UseVisualStyleBackColor = True
    '
    'lbl_warning
    '
    Me.lbl_warning.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_warning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_warning.Location = New System.Drawing.Point(8, 45)
    Me.lbl_warning.Name = "lbl_warning"
    Me.lbl_warning.Size = New System.Drawing.Size(16, 16)
    Me.lbl_warning.TabIndex = 2
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(8, 74)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 4
    '
    'lbl_ok
    '
    Me.lbl_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_ok.Location = New System.Drawing.Point(8, 19)
    Me.lbl_ok.Name = "lbl_ok"
    Me.lbl_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_ok.TabIndex = 0
    '
    'gb_session
    '
    Me.gb_session.Controls.Add(Me.cb_session_free)
    Me.gb_session.Controls.Add(Me.cb_session_oc)
    Me.gb_session.Location = New System.Drawing.Point(9, 119)
    Me.gb_session.Name = "gb_session"
    Me.gb_session.Size = New System.Drawing.Size(130, 68)
    Me.gb_session.TabIndex = 1
    Me.gb_session.TabStop = False
    Me.gb_session.Text = "xSession"
    '
    'cb_session_free
    '
    Me.cb_session_free.AutoSize = True
    Me.cb_session_free.Checked = True
    Me.cb_session_free.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_session_free.Location = New System.Drawing.Point(8, 41)
    Me.cb_session_free.Name = "cb_session_free"
    Me.cb_session_free.Size = New System.Drawing.Size(58, 17)
    Me.cb_session_free.TabIndex = 1
    Me.cb_session_free.Text = "xFree"
    Me.cb_session_free.UseVisualStyleBackColor = True
    '
    'cb_session_oc
    '
    Me.cb_session_oc.AutoSize = True
    Me.cb_session_oc.Checked = True
    Me.cb_session_oc.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_session_oc.Location = New System.Drawing.Point(8, 20)
    Me.cb_session_oc.Name = "cb_session_oc"
    Me.cb_session_oc.Size = New System.Drawing.Size(83, 17)
    Me.cb_session_oc.TabIndex = 0
    Me.cb_session_oc.Text = "xOcupado"
    Me.cb_session_oc.UseVisualStyleBackColor = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(450, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 183)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'gb_alerts
    '
    Me.gb_alerts.Controls.Add(Me.btn_check_all)
    Me.gb_alerts.Controls.Add(Me.btn_uncheck_all)
    Me.gb_alerts.Controls.Add(Me.dg_alerts)
    Me.gb_alerts.Location = New System.Drawing.Point(783, 7)
    Me.gb_alerts.Name = "gb_alerts"
    Me.gb_alerts.Size = New System.Drawing.Size(290, 180)
    Me.gb_alerts.TabIndex = 3
    Me.gb_alerts.TabStop = False
    Me.gb_alerts.Text = "xAlerts"
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_check_all.Location = New System.Drawing.Point(6, 145)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 1
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_uncheck_all.Location = New System.Drawing.Point(112, 145)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 2
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'dg_alerts
    '
    Me.dg_alerts.CurrentCol = -1
    Me.dg_alerts.CurrentRow = -1
    Me.dg_alerts.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_alerts.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_alerts.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_alerts.Location = New System.Drawing.Point(6, 19)
    Me.dg_alerts.Name = "dg_alerts"
    Me.dg_alerts.PanelRightVisible = False
    Me.dg_alerts.Redraw = True
    Me.dg_alerts.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_alerts.Size = New System.Drawing.Size(278, 122)
    Me.dg_alerts.Sortable = False
    Me.dg_alerts.SortAscending = True
    Me.dg_alerts.SortByCol = 0
    Me.dg_alerts.TabIndex = 0
    Me.dg_alerts.ToolTipped = True
    Me.dg_alerts.TopRow = -2
    Me.dg_alerts.WordWrap = False
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(9, 192)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(250, 25)
    Me.tf_last_update.SufixText = "xSeconds"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 6
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 130
    Me.tf_last_update.Value = "-"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(450, 196)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(221, 17)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'chk_show_all_alarms
    '
    Me.chk_show_all_alarms.AutoSize = True
    Me.chk_show_all_alarms.Location = New System.Drawing.Point(789, 196)
    Me.chk_show_all_alarms.Name = "chk_show_all_alarms"
    Me.chk_show_all_alarms.Size = New System.Drawing.Size(124, 17)
    Me.chk_show_all_alarms.TabIndex = 5
    Me.chk_show_all_alarms.Text = "xShow all alarms"
    Me.chk_show_all_alarms.UseVisualStyleBackColor = True
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(157, 3)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(288, 179)
    Me.uc_site_select.TabIndex = 12
    '
    'frm_terminal_status
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1248, 762)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_terminal_status"
    Me.Text = "frm_terminal_status"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_session.ResumeLayout(False)
    Me.gb_session.PerformLayout()
    Me.gb_alerts.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_warning As System.Windows.Forms.Label
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents lbl_ok As System.Windows.Forms.Label
  Friend WithEvents gb_session As System.Windows.Forms.GroupBox
  Friend WithEvents cb_session_free As System.Windows.Forms.CheckBox
  Friend WithEvents cb_session_oc As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents cb_ok As System.Windows.Forms.CheckBox
  Friend WithEvents gb_alerts As System.Windows.Forms.GroupBox
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents dg_alerts As GUI_Controls.uc_grid
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents btn_check_all As GUI_Controls.uc_button
  Friend WithEvents cb_warning As System.Windows.Forms.CheckBox
  Friend WithEvents cb_error As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_all_alarms As System.Windows.Forms.CheckBox
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
End Class
