'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_winup_settings.vb
'
' DESCRIPTION : Settings for App Mobile
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-OCT-2016  SDS    Initial Draft.
' 07-NOV-2016  RDS    PBI 19342:Feedback Module - Api - Envio de correos
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports WSI.Common
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class frm_winup_settings
  Inherits frm_base_edit

#Region " Constants "


#End Region 'Constants
#Region " Members "
  Private m_winup_settings As CLASS_WINUP_SETTINGS
  Private m_status_changed As Integer

  Public Class ucComboBox
    Public Sub New(ByVal NewValue As Integer, ByVal NewDescription As String)
      Value = NewValue
      Description = NewDescription
    End Sub
    Public Value As Integer
    Public Description As String
    Public Overrides Function ToString() As String
      Return Description
    End Function
  End Class
#End Region
#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_WINAP_SETTINGS


    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _imageMaxSize As Size
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7694)
    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    'btn_custom_0 is used as the 'Apply/Save' button
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    Me.lbl_about.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7697)
    Me.lbl_join.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7698)
    Me.lbl_link.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7699)
    Me.lbl_terms.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7700)
    Me.tab_player_club.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7904)
    Me.gb_PreferentText.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7695)

    'RDS 02-DIC-2016
    Me.gb_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7798)
    Me.lbl_height.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7799)
    Me.lbl_width.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7800)
    Me.lbl_weight.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7801)
    Me.ef_height.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_width.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    Me.ef_weight.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4)
    CargaComboWeightUnit()

    _imageMaxSize = Me.m_winup_settings.GetImageMaxSize()
    Me.Uc_imageList_footer1.Init(_imageMaxSize.Height, _imageMaxSize.Width)
    Me.Uc_imageList_footer1.Transparent = True

    Me.Uc_imageList_footer2.Init(_imageMaxSize.Height, _imageMaxSize.Width)
    Me.Uc_imageList_footer2.Transparent = True

    Me.Uc_imageList_footer3.Init(_imageMaxSize.Height, _imageMaxSize.Width)
    Me.Uc_imageList_footer3.Transparent = True

    Me.Uc_imageList_footer4.Init(_imageMaxSize.Height, _imageMaxSize.Width)
    Me.Uc_imageList_footer4.Transparent = True

    'SDS 08-OCT-2016 Footer
    Me.tab_footer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7748) 'Pie de la App'
    Me.tab_menu_footer1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7749) + " 1" 'Bot�n 1'
    Me.tab_menu_footer2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7749) + " 2"  'Bot�n 2'
    Me.tab_menu_footer3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7749) + " 3" 'Bot�n 3'
    Me.tab_menu_footer4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7749) + " 4" 'Bot�n 4'
    Me.tab_menu_footer5.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7749) + " 5" 'Bot�n 5'
    Me.lbl_section_footer1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7750) 'Secci�n'
    Me.lbl_section_footer2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7750) 'Secci�n'
    Me.lbl_section_footer3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7750) 'Secci�n'
    Me.lbl_section_footer4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7750) 'Secci�n'
    Me.lbl_section_footer5.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7750) 'Secci�n'
    Me.ef_order_footer1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) 'Orden'
    Me.ef_order_footer2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) 'Orden'
    Me.ef_order_footer3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) 'Orden'
    Me.ef_order_footer4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) 'Orden'
    Me.ef_order_footer5.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7751) 'Orden'
    Me.ef_order_footer1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_order_footer2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_order_footer3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_order_footer4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_order_footer5.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.chk_visible_footer1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752) 'Visible'
    Me.chk_visible_footer2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752) 'Visible'
    Me.chk_visible_footer3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752) 'Visible'
    Me.chk_visible_footer4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752) 'Visible'
    Me.chk_visible_footer5.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752) 'Visible'

    'SDS 18-OCT-2016 Footer Settings
    CargaComboSchema()

    Me.cmb_Section_footer1.TextVisible = True
    Me.cmb_section_footer2.TextVisible = True
    Me.cmb_section_footer3.TextVisible = True
    Me.cmb_section_footer4.TextVisible = True
    Me.cmb_section_footer5.TextVisible = True
    Me.ef_order_footer1.TextVisible = True
    Me.ef_order_footer2.TextVisible = True
    Me.ef_order_footer3.TextVisible = True
    Me.ef_order_footer4.TextVisible = True
    Me.ef_order_footer5.TextVisible = True

    GLB_NLS_GUI_CONFIGURATION.GetString(405)
    'SDS 29-NOV-2016 App Mailing

    Me.gbSMTPConfig.Text = GLB_NLS_GUI_CONFIGURATION.GetString(398)
    Me.gbCredentials.Text = GLB_NLS_GUI_CONFIGURATION.GetString(399)

    Me.Uc_mailing_domain.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7789) 'Visible'
    Me.lbl_mailing_domain.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7789) 'Visible'
    Me.Uc_mailing_emaiAddress.Text = GLB_NLS_GUI_CONFIGURATION.GetString(406) 'Buz�n'
    Me.lbl_mailing_emaiAddress.Text = GLB_NLS_GUI_CONFIGURATION.GetString(406) 'Buz�n'
    Me.Uc_mailing_password.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7787) 'Password'
    Me.lbl_mailing_password.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7787) 'Password'
    Me.Uc_mailing_serverAdress.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7783) 'Server Address'
    Me.lbl_mailing_serverAdress.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7783) 'Server Address'
    Me.Uc_mailing_serverPort.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7784) 'Server Port'
    Me.lbl_mailing_serverPort.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7784) 'Server Port'
    Me.Uc_mailing_username.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786) 'User Name'
    Me.lbl_mailing_username.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786) 'User Name'
    Me.tab_mailing.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7788) 'Mailing'
    Me.chk_mailing_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(396) ' Enabled
    Me.cmb_smtp_secured.Text = GLB_NLS_GUI_CONFIGURATION.GetString(402) 'Smtp
    Me.lbl_smtp_secured.Text = GLB_NLS_GUI_CONFIGURATION.GetString(402) 'Smtp

    Me.Uc_mailing_domain.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)
    Me.Uc_mailing_emaiAddress.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)
    Me.Uc_mailing_password.Password = True
    Me.Uc_mailing_password.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)
    Me.Uc_mailing_serverAdress.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)
    Me.Uc_mailing_serverPort.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.Uc_mailing_username.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 200)

    Me.cmb_smtp_secured.Add(1, GLB_NLS_GUI_CONFIGURATION.GetString(264))
    Me.cmb_smtp_secured.Add(0, GLB_NLS_GUI_CONFIGURATION.GetString(265))

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_Exit()
    ' Perform any clean-up
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

  Protected Overrides Sub GUI_GetScreenData()
    Dim _footerList As New List(Of CLASS_WINUP_SETTINGS.TYPE_FOOTER)
    Dim _footer As New CLASS_WINUP_SETTINGS.TYPE_FOOTER
    Dim _mailing As New CLASS_WINUP_SETTINGS.TYPE_MAILING

    Try
      m_winup_settings = Me.DbEditedObject

      m_winup_settings.About = Me.txt_about.Text
      m_winup_settings.Join = Me.txt_join.Text
      m_winup_settings.Link = Me.txt_link.Text
      m_winup_settings.Terms = Me.txt_terms.Text

      m_winup_settings.Height = Me.ef_height.Value
      m_winup_settings.Width = Me.ef_width.Value
      m_winup_settings.Weight = Me.ef_weight.Value
      m_winup_settings.Weight_Unit = Me.cmb_weight_unit.TextValue

      '14/11/2016 SDS Footer Configuration


      _footer.sectionSchemaId = Me.cmb_Section_footer1.Value
      _footer.footerOrder = Me.ef_order_footer1.Value
      _footer.footerActive = Me.chk_visible_footer1.Checked
      _footer.footerImage = Me.Uc_imageList_footer1.Image
      If Me.cmb_Section_footer1.Tag <> Me.cmb_Section_footer1.Value Or Me.Uc_imageList_footer1.Tag = 0 Or ef_order_footer1.Tag <> ef_order_footer1.Value Or Me.chk_visible_footer1.Tag <> Me.chk_visible_footer1.Checked.ToString() Then
        _footer.footerStatus = 1
      Else
        _footer.footerStatus = 0
      End If
      _footerList.Add(_footer)
      _footer = New CLASS_WINUP_SETTINGS.TYPE_FOOTER

      _footer.sectionSchemaId = Me.cmb_section_footer2.Value
      _footer.footerOrder = Me.ef_order_footer2.Value
      _footer.footerActive = Me.chk_visible_footer2.Checked
      _footer.footerImage = Me.Uc_imageList_footer2.Image
      If Me.cmb_section_footer2.Tag <> Me.cmb_section_footer2.Value Or Me.Uc_imageList_footer2.Tag = 0 Or ef_order_footer2.Tag <> ef_order_footer2.Value Or Me.chk_visible_footer2.Tag <> Me.chk_visible_footer2.Checked.ToString() Then
        _footer.footerStatus = 1
      Else
        _footer.footerStatus = 0
      End If
      _footerList.Add(_footer)
      _footer = New CLASS_WINUP_SETTINGS.TYPE_FOOTER

      _footer.sectionSchemaId = Me.cmb_section_footer3.Value
      _footer.footerOrder = Me.ef_order_footer3.Value
      _footer.footerActive = Me.chk_visible_footer3.Checked
      _footer.footerImage = Me.Uc_imageList_footer3.Image
      If Me.cmb_section_footer3.Tag <> Me.cmb_section_footer3.Value Or Me.Uc_imageList_footer3.Tag = 0 Or ef_order_footer3.Tag <> ef_order_footer3.Value Or Me.chk_visible_footer3.Tag <> Me.chk_visible_footer3.Checked.ToString() Then
        _footer.footerStatus = 1
      Else
        _footer.footerStatus = 0
      End If
      _footerList.Add(_footer)
      _footer = New CLASS_WINUP_SETTINGS.TYPE_FOOTER


      _footer.sectionSchemaId = Me.cmb_section_footer4.Value
      _footer.footerOrder = Me.ef_order_footer4.Value
      _footer.footerActive = Me.chk_visible_footer4.Checked
      _footer.footerImage = Me.Uc_imageList_footer4.Image
      If Me.cmb_section_footer4.Tag <> Me.cmb_section_footer4.Value Or Me.Uc_imageList_footer4.Tag = 0 Or ef_order_footer4.Tag <> ef_order_footer4.Value Or Me.chk_visible_footer4.Tag <> Me.chk_visible_footer4.Checked.ToString() Then
        _footer.footerStatus = 1
      Else
        _footer.footerStatus = 0
      End If
      _footerList.Add(_footer)
      _footer = New CLASS_WINUP_SETTINGS.TYPE_FOOTER

      _footer.sectionSchemaId = Me.cmb_section_footer5.Value
      _footer.footerOrder = Me.ef_order_footer5.Value
      _footer.footerActive = Me.chk_visible_footer5.Checked
      _footer.footerImage = Me.Uc_imageList_footer5.Image
      If Me.cmb_section_footer5.Tag <> Me.cmb_section_footer5.Value Or Me.Uc_imageList_footer5.Tag = 0 Or ef_order_footer5.Tag <> ef_order_footer5.Value Or Me.chk_visible_footer5.Tag <> Me.chk_visible_footer5.Checked.ToString() Then
        _footer.footerStatus = 1
      Else
        _footer.footerStatus = 0
      End If
      _footerList.Add(_footer)

      m_winup_settings.FooterList = _footerList

      'SDS 29-NOv-2016 Mailing Settings
      m_winup_settings.Enabled = Me.chk_mailing_enabled.Checked
      m_winup_settings.ServerAddress = Me.Uc_mailing_serverAdress.Value
      m_winup_settings.ServerPort = Me.Uc_mailing_serverPort.Value
      m_winup_settings.Domain = Me.Uc_mailing_domain.Value
      m_winup_settings.UserName = Me.Uc_mailing_username.Value
      m_winup_settings.Password = Me.Uc_mailing_password.Value
      m_winup_settings.EmailAddress = Me.Uc_mailing_emaiAddress.Value
      m_winup_settings.Secured = cmb_smtp_secured.Value

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_GetScreenData", _
                            ex.Message)
    Finally

    End Try

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Try
      ' m_winup_settings = Me.DbReadObject

      txt_about.Text = m_winup_settings.About
      txt_link.Text = m_winup_settings.Link
      txt_join.Text = m_winup_settings.Join
      txt_terms.Text = m_winup_settings.Terms

      ef_height.TextValue = m_winup_settings.Height
      ef_width.TextValue = m_winup_settings.Width
      ef_weight.TextValue = m_winup_settings.Weight
      cmb_weight_unit.TextValue = m_winup_settings.Weight_Unit

      For Each footer As CLASS_WINUP_SETTINGS.TYPE_FOOTER In m_winup_settings.FooterList
        If footer.footerActive = True Then
          Select Case footer.footerOrder
            Case 1
              Me.cmb_Section_footer1.Value = footer.sectionSchemaId
              Me.cmb_Section_footer1.Text = footer.footerName
              Me.ef_order_footer1.Value = footer.footerOrder.ToString()
              Me.chk_visible_footer1.Checked = footer.footerActive
              Me.Uc_imageList_footer1.Image = footer.footerImage


              Me.ef_order_footer1.Tag = footer.footerOrder.ToString()
              Me.chk_visible_footer1.Tag = footer.footerActive.ToString()
              Me.cmb_Section_footer1.Tag = footer.sectionSchemaId.ToString()
              Me.Uc_imageList_footer1.Tag = footer.footerImageId

            Case 2
              Me.cmb_section_footer2.Value = footer.sectionSchemaId
              Me.ef_order_footer2.Value = footer.footerOrder.ToString()
              Me.chk_visible_footer2.Checked = footer.footerActive
              Me.Uc_imageList_footer2.Image = footer.footerImage

              Me.ef_order_footer2.Tag = footer.footerOrder.ToString()
              Me.chk_visible_footer2.Tag = footer.footerActive.ToString()
              Me.cmb_section_footer2.Tag = footer.sectionSchemaId.ToString()
              Me.Uc_imageList_footer2.Tag = footer.footerImageId
            Case 3
              Me.cmb_section_footer3.Value = footer.sectionSchemaId
              Me.ef_order_footer3.Value = footer.footerOrder.ToString()
              Me.chk_visible_footer3.Checked = footer.footerActive
              Me.Uc_imageList_footer3.Image = footer.footerImage

              Me.ef_order_footer3.Tag = footer.footerOrder.ToString()
              Me.chk_visible_footer3.Tag = footer.footerActive.ToString()
              Me.cmb_section_footer3.Tag = footer.sectionSchemaId.ToString()
              Me.Uc_imageList_footer3.Tag = footer.footerImageId
            Case 4
              Me.cmb_section_footer4.Value = footer.sectionSchemaId
              Me.ef_order_footer4.Value = footer.footerOrder.ToString()
              Me.chk_visible_footer4.Checked = footer.footerActive
              Me.Uc_imageList_footer4.Image = footer.footerImage

              Me.ef_order_footer4.Tag = footer.footerOrder.ToString()
              Me.chk_visible_footer4.Tag = footer.footerActive.ToString()
              Me.cmb_section_footer4.Tag = footer.sectionSchemaId.ToString()
              Me.Uc_imageList_footer4.Tag = footer.footerImageId
            Case 5
              Me.cmb_section_footer5.Value = footer.sectionSchemaId
              Me.ef_order_footer5.Value = footer.footerOrder.ToString()
              Me.chk_visible_footer5.Checked = footer.footerActive
              Me.Uc_imageList_footer5.Image = footer.footerImage

              Me.ef_order_footer5.Tag = footer.footerOrder.ToString()
              Me.chk_visible_footer5.Tag = footer.footerActive.ToString()
              Me.cmb_section_footer5.Tag = footer.sectionSchemaId.ToString()
              Me.Uc_imageList_footer5.Tag = footer.footerImageId

            Case Else
          End Select
        End If
      Next

      'SDS 29-NOv-2016 Mailing Settings
      Me.chk_mailing_enabled.Checked = m_winup_settings.Enabled
      Me.Uc_mailing_serverAdress.Value = m_winup_settings.ServerAddress
      Me.Uc_mailing_serverPort.Value = m_winup_settings.ServerPort
      Me.Uc_mailing_emaiAddress.Value = m_winup_settings.EmailAddress
      Me.Uc_mailing_domain.Value = m_winup_settings.Domain
      Me.Uc_mailing_username.Value = m_winup_settings.UserName
      Me.Uc_mailing_password.Value = m_winup_settings.Password

      If m_winup_settings.Secured Then
        Me.cmb_smtp_secured.Value = 1
      Else
        Me.cmb_smtp_secured.Value = 0
      End If

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub  'GUI_Permissions

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)


    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_WINUP_SETTINGS
        m_winup_settings = DbEditedObject


      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

        m_winup_settings = Me.DbEditedObject


      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_SUPPORTED Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          ' Call Me.DB_CheckAllCounters() DO NOTHING
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          m_winup_settings = Me.DbEditedObject
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    m_winup_settings = DbEditedObject
    Dim _image_maxsize = Me.m_winup_settings.GetImageMaxWeightKB()


    If (Me.Uc_imageList_footer1.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageList_footer1.GetImageKBWeight()


      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7803), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If
    If (Me.Uc_imageList_footer2.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageList_footer2.GetImageKBWeight()

      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7803), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If
    If (Me.Uc_imageList_footer3.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageList_footer3.GetImageKBWeight()

      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7803), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If
    If (Me.Uc_imageList_footer4.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageList_footer4.GetImageKBWeight()

      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7803), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If
    If (Me.Uc_imageList_footer5.Image IsNot Nothing) Then
      Dim _image_weight = Me.Uc_imageList_footer5.GetImageKBWeight()

      If (_image_weight > 0) And (_image_weight > _image_maxsize) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7803), ENUM_MB_TYPE.MB_TYPE_WARNING, , , " " & _image_maxsize & " KB", , )
        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk



  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    If ButtonId = ENUM_BUTTON.BUTTON_CUSTOM_0 Then
      ApplyButton()
    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If

  End Sub ' GUI_ButtonClick

#End Region ' Overrides

#Region "Private Functions"

  Private Sub ApplyButton()

    ' 193 "Are you sure you want to save changes?"  
    If NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(193), _
                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
      Exit Sub
    End If

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to cahnge the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
      Exit Sub
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

  End Sub ' ApplyButton

  Private Sub CargaComboWeightUnit()

    Me.cmb_weight_unit.Add(CLASS_WINUP_SETTINGS.IMAGE_WEIGHT_UNIT_KB)
    Me.cmb_weight_unit.Add(CLASS_WINUP_SETTINGS.IMAGE_WEIGHT_UNIT_MB)

  End Sub

  Private Sub CargaComboSchema()
    Dim _url As String
    Dim _response, _responseSanitized As String
    Dim _data As DataTable
    _url = WSI.Common.GeneralParam.GetString("WinUP", "Backend.Url") + "/sectionsschema/getallbylanguage"
    Dim _items() As String = {"childs"}
    Dim _jarray As JArray
    Dim _api As New CLS_API(_url)
    _response = _api.getData()
    _responseSanitized = _api.sanitizeData(_response)
    _jarray = _api.removeItems(JArray.Parse(_responseSanitized), _items)
    _data = JsonConvert.DeserializeObject(Of DataTable)(_jarray.ToString())
    Me.cmb_Section_footer1.Clear()

    Dim _lista As New List(Of ucComboBox)
    For Each row As DataRow In _data.Rows
      Dim _list As New ucComboBox(row(0), row(2)) 'sectionschemaid,name
      _lista.Add(_list)
    Next

    cmb_Section_footer1.Add(_data, 0, 2)
    cmb_section_footer2.Add(_data, 0, 2)
    cmb_section_footer3.Add(_data, 0, 2)
    cmb_section_footer4.Add(_data, 0, 2)
    cmb_section_footer5.Add(_data, 0, 2)


  End Sub

#End Region ' Private Functions

#Region "Events"

#End Region ' Events


  Private Sub Uc_imageList_footer1_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageList_footer1.ImageSelected
    Dim _section As CLASS_WINUP_SETTINGS = Me.DbEditedObject
    If Not IsNothing(_section) Then
      Uc_imageList_footer1.Tag = 0
      _section.FooterStatus = 1
    End If
  End Sub
  Private Sub Uc_imageList_footer2_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageList_footer1.ImageSelected, Uc_imageList_footer2.ImageSelected
    Dim _section As CLASS_WINUP_SETTINGS = Me.DbEditedObject

    If Not IsNothing(_section) Then
      Uc_imageList_footer2.Tag = 0
      _section.FooterStatus = 1
    End If
  End Sub
  Private Sub Uc_imageList_footer3_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageList_footer1.ImageSelected, Uc_imageList_footer3.ImageSelected
    Dim _section As CLASS_WINUP_SETTINGS = Me.DbEditedObject

    If Not IsNothing(_section) Then
      Uc_imageList_footer3.Tag = 0
      _section.FooterStatus = 1
    End If
  End Sub
  Private Sub Uc_imageList_footer4_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageList_footer1.ImageSelected, Uc_imageList_footer4.ImageSelected
    Dim _section As CLASS_WINUP_SETTINGS = Me.DbEditedObject

    If Not IsNothing(_section) Then
      Uc_imageList_footer4.Tag = 0
      _section.FooterStatus = 1
    End If
  End Sub
  Private Sub Uc_imageList_footer5_ImageSelected(ByRef [Continue] As Boolean, ImageSelected As Image) Handles Uc_imageList_footer1.ImageSelected, Uc_imageList_footer5.ImageSelected
    Dim _section As CLASS_WINUP_SETTINGS = Me.DbEditedObject

    If Not IsNothing(_section) Then
      Uc_imageList_footer5.Tag = 0
      _section.FooterStatus = 1
    End If
  End Sub

  Private Sub chk_visible_footer1_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visible_footer1.CheckedChanged
    cmb_Section_footer1.Enabled = chk_visible_footer1.Checked
    ef_order_footer1.Enabled = chk_visible_footer1.Checked
    Uc_imageList_footer1.Enabled = chk_visible_footer1.Checked
  End Sub

  Private Sub chk_visible_footer2_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visible_footer2.CheckedChanged
    cmb_section_footer2.Enabled = chk_visible_footer2.Checked
    ef_order_footer2.Enabled = chk_visible_footer2.Checked
    Uc_imageList_footer2.Enabled = chk_visible_footer2.Checked
  End Sub

  Private Sub chk_visible_footer3_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visible_footer3.CheckedChanged
    cmb_section_footer3.Enabled = chk_visible_footer3.Checked
    ef_order_footer3.Enabled = chk_visible_footer3.Checked
    Uc_imageList_footer3.Enabled = chk_visible_footer3.Checked
  End Sub

  Private Sub chk_visible_footer4_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visible_footer4.CheckedChanged
    cmb_section_footer4.Enabled = chk_visible_footer4.Checked
    ef_order_footer4.Enabled = chk_visible_footer4.Checked
    Uc_imageList_footer4.Enabled = chk_visible_footer4.Checked
  End Sub

  Private Sub chk_visible_footer5_CheckedChanged(sender As Object, e As EventArgs) Handles chk_visible_footer5.CheckedChanged
    cmb_section_footer5.Enabled = chk_visible_footer5.Checked
    ef_order_footer5.Enabled = chk_visible_footer5.Checked
    Uc_imageList_footer5.Enabled = chk_visible_footer5.Checked
  End Sub
End Class ' frm_winup_settings