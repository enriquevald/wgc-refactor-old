﻿'-------------------------------------------------------------------
' Copyright © 2018 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_report_profit_machine_and_day
' DESCRIPTION:   Form to show the profit reports for AGG
' AUTHOR:        Alberto Marcos
' CREATION DATE: 09-GEN-2018
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-GEN-2018  AMF    Initial version
'--------------------------------------------------------------------

Imports GUI_Controls
Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_report_profit_machine_and_day
  Inherits frm_base_sel

#Region " Members "

  Private m_is_multisite_center As Boolean

  Private m_site_id As Int32
  Private m_num_rows_group As Int32
  Private m_num_rows_total As Int32
  Private m_num_plays_group As Int64
  Private m_num_plays_total As Int64

  Private m_sum_subtotal_bets As Double
  Private m_sum_subtotal_payments As Double
  Private m_sum_subtotal_profit As Double
  Private m_sum_subtotal_jackpots As Double

  Private m_sum_total_bets As Double
  Private m_sum_total_payments As Double
  Private m_sum_total_profit As Double
  Private m_sum_total_jackpots As Double

  Private m_filter_date_to As String
  Private m_filter_date_from As String
  Private m_filter_terminal As String
  Private m_filter_site As String
  Private m_filter_close_only As String
  Private m_filter_increment As String

#End Region ' Members

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_SITE As Integer = 1
  Private Const GRID_COLUMN_WORKING_DAY As Integer = 2
  Private Const GRID_COLUMN_TERMINAL As Integer = 3
  Private Const GRID_COLUMN_GAME As Integer = 4
  Private Const GRID_COLUMN_TERMINAL_TYPE As Integer = 5
  Private Const GRID_COLUMN_PROFIT As Integer = 6
  Private Const GRID_COLUMN_PROFIT_AVG As Integer = 7
  Private Const GRID_COLUMN_BET_AVG As Integer = 8
  Private Const GRID_COLUMN_BETS As Integer = 9
  Private Const GRID_COLUMN_PAYMENTS As Integer = 10
  Private Const GRID_COLUMN_JACKPOTS As Integer = 11
  Private Const GRID_COLUMN_PAYOUT As Integer = 12
  Private Const GRID_COLUMN_THEORICAL_PAYOUT As Integer = 13

  ' SQL Columns
  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_GAME As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 3
  Private Const SQL_COLUMN_BET_AVG As Integer = 4
  Private Const SQL_COLUMN_BETS As Integer = 5
  Private Const SQL_COLUMN_PAYMENTS As Integer = 6
  Private Const SQL_COLUMN_JACKPOTS As Integer = 7
  Private Const SQL_COLUMN_WORKING_DAY As Integer = 8
  Private Const SQL_COLUMN_PROFIT As Integer = 9
  Private Const SQL_COLUMN_PROFIT_AVG As Integer = 10
  Private Const SQL_COLUMN_PAYOUT As Integer = 11
  Private Const SQL_COLUMN_THEORICAL_PAYOUT As Integer = 12
  Private Const SQL_COLUMN_NUM_PLAYS As Integer = 13

  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_COLUMN_WIDTH_INDEX As Integer = 200
  Private Const GRID_COLUMN_WIDTH_SITE As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_TERMINAL As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_GAME As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TERMINAL_TYPE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BET_AVG As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_BETS As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_PAYMENTS As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_JACKPOTS As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_WORKING_DAY As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_PROFIT As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_PROFIT_AVG As Integer = 1600
  Private Const GRID_COLUMN_WIDTH_PAYOUT As Integer = 1800
  Private Const GRID_COLUMN_WIDTH_THEORICAL_PAYOUT As Integer = 2500

  Private Const EXCEL_COLUMN_WORKING_DAY As Integer = 8

#End Region ' Constants

#Region " Overrides "

  ''' <summary>
  ''' Initializes the form id.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MACHINE_DAY

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId 

  ''' <summary>
  ''' Show form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ''' <summary>
  ''' Init controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Dim _terminal_types As List(Of Int32)

    Call MyBase.GUI_InitControls()

    m_is_multisite_center = WSI.Common.Misc.IsCenter()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8976)

    Me.lbl_profits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8978)
    Me.lbl_profits_avg.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8979)
    Me.lbl_payouts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8969)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.gb_init_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
    Me.dtp_init_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_init_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)

    Me.dtp_init_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_init_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Me.chk_only_closed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8957)
    Me.ef_win_increment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8958)
    Me.ef_win_increment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES, 9)

    _terminal_types = New List(Of Int32)
    _terminal_types.Add(TerminalTypes.WIN)
    _terminal_types.Add(TerminalTypes.T3GS)
    _terminal_types.Add(TerminalTypes.SAS_HOST)

    m_site_id = -1

    If m_is_multisite_center Then
      Me.uc_site_select.Visible = True

      Me.pnl_filters.Location = New Point(305, 8)

      Me.uc_site_select.ShowMultisiteRow = False
      Me.uc_site_select.MultiSelect = True
      Me.uc_site_select.Init()

      Call Me.uc_pr_list.Init(_terminal_types.ToArray(), , , True)
    Else
      Call Me.uc_pr_list.Init(_terminal_types.ToArray())
    End If

    Call SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ''' <summary>
  ''' Set command
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_command As SqlCommand
    Dim _with_increment As Boolean
    Dim _increment As Decimal

    _with_increment = Decimal.TryParse(ef_win_increment.Value, _increment)

    _sql_command = New SqlCommand()

    _sql_command.CommandText = "SP_AGG_ReportProfitByMachineDay"

    _sql_command.CommandType = CommandType.StoredProcedure

    If m_is_multisite_center AndAlso Me.uc_site_select.GetSitesIdListSelectedAll <> String.Empty Then
      _sql_command.Parameters.Add("@pSites", SqlDbType.NVarChar).Value = Me.uc_site_select.GetSitesIdListSelectedAll
    Else
      _sql_command.Parameters.Add("@pSites", SqlDbType.NVarChar).Value = GetSiteId().ToString()
    End If

    _sql_command.Parameters.Add("@pTerminals", SqlDbType.NChar).Value = Me.uc_pr_list.GetProviderIdListSelected()
    _sql_command.Parameters.Add("@pFrom", SqlDbType.BigInt).Value = cls_lottery.DateToBigInt(Me.dtp_init_from.Value)
    _sql_command.Parameters.Add("@pTo", SqlDbType.BigInt).Value = cls_lottery.DateToBigInt(Me.dtp_init_to.Value)
    _sql_command.Parameters.Add("@pOnlyClosed", SqlDbType.Bit).Value = chk_only_closed.Checked

    If _with_increment Then
      _sql_command.Parameters.Add("@pIncrement", SqlDbType.Money).Value = _increment
    End If

    Return _sql_command
  End Function ' GUI_GetSqlCommand

  ''' <summary>
  ''' Set rows
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    m_num_rows_group += 1
    m_num_rows_total += 1

    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      m_site_id = DbRow.Value(SQL_COLUMN_SITE_ID)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE).Value = DbRow.Value(SQL_COLUMN_SITE_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_GAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME).Value = DbRow.Value(SQL_COLUMN_GAME)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_TYPE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = Strings.Trim(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_BETS) AndAlso Not DbRow.IsNull(SQL_COLUMN_NUM_PLAYS) Then
      If DbRow.Value(SQL_COLUMN_NUM_PLAYS) > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BET_AVG).Value = GUI_FormatCurrency((DbRow.Value(SQL_COLUMN_BETS) / DbRow.Value(SQL_COLUMN_NUM_PLAYS)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BET_AVG).Value = 0
      End If
    End If

    If Not DbRow.IsNull(SQL_COLUMN_BETS) Then
      m_sum_total_bets += DbRow.Value(SQL_COLUMN_BETS)
      m_sum_subtotal_bets += DbRow.Value(SQL_COLUMN_BETS)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BETS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BETS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PAYMENTS) Then
      m_sum_total_payments += DbRow.Value(SQL_COLUMN_PAYMENTS)
      m_sum_subtotal_payments += DbRow.Value(SQL_COLUMN_PAYMENTS)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAYMENTS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PAYMENTS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_JACKPOTS) Then
      m_sum_total_jackpots += DbRow.Value(SQL_COLUMN_JACKPOTS)
      m_sum_subtotal_jackpots += DbRow.Value(SQL_COLUMN_JACKPOTS)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOTS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = GUI_FormatDate(cls_lottery.BigIntToDate(DbRow.Value(SQL_COLUMN_WORKING_DAY)), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PROFIT) Then
      m_sum_total_profit += DbRow.Value(SQL_COLUMN_PROFIT)
      m_sum_subtotal_profit += DbRow.Value(SQL_COLUMN_PROFIT)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROFIT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROFIT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PROFIT_AVG) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROFIT_AVG).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROFIT_AVG), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PAYOUT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PAYOUT).Value = String.Format("{0}%", GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PAYOUT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_THEORICAL_PAYOUT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_PAYOUT).Value = String.Format("{0}%", GUI_FormatNumber(DbRow.Value(SQL_COLUMN_THEORICAL_PAYOUT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_NUM_PLAYS) Then
      m_num_plays_group += DbRow.Value(SQL_COLUMN_NUM_PLAYS)
      m_num_plays_total += DbRow.Value(SQL_COLUMN_NUM_PLAYS)
    End If

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Reset filters
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()
  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Set subtotals
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If m_site_id = -1 Then

      Return True
    End If

    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then

      If m_site_id <> DbRow.Value(SQL_COLUMN_SITE_ID) Then

        AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), m_sum_subtotal_bets, m_sum_subtotal_payments, m_sum_subtotal_profit, m_sum_subtotal_jackpots, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, m_num_plays_group)

        m_sum_subtotal_bets = 0
        m_sum_subtotal_payments = 0
        m_sum_subtotal_profit = 0
        m_sum_subtotal_jackpots = 0
        m_num_rows_group = 0
        m_num_plays_group = 0

      End If

    End If

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ''' <summary>
  ''' Set the las rows
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    If Not m_is_multisite_center Then
      AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047), m_sum_subtotal_bets, m_sum_subtotal_payments, m_sum_subtotal_profit, m_sum_subtotal_jackpots, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, m_num_plays_total)
    Else
      AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), m_sum_subtotal_bets, m_sum_subtotal_payments, m_sum_subtotal_profit, m_sum_subtotal_jackpots, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, m_num_plays_group)
      AddRowTotal(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047), m_sum_total_bets, m_sum_total_payments, m_sum_total_profit, m_sum_total_jackpots, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, m_num_plays_total)
    End If

  End Sub ' GUI_AfterLastRow

  ''' <summary>
  ''' Filter check
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.dtp_init_from.Value > Me.dtp_init_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_init_to.Focus()

      Return False
    End If

    ' If Multisite then selected site needed
    If m_is_multisite_center Then
      If Not uc_site_select.FilterCheckSites() Then
        Return False
      End If
    End If

    m_site_id = -1

    m_sum_subtotal_bets = 0
    m_sum_subtotal_payments = 0
    m_sum_subtotal_profit = 0
    m_sum_subtotal_jackpots = 0

    m_sum_total_bets = 0
    m_sum_total_payments = 0
    m_sum_total_profit = 0
    m_sum_total_jackpots = 0

    m_num_plays_total = 0
    m_num_plays_group = 0
    m_num_rows_group = 0

    Return True

  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Action before first row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirstRow

  ''' <summary>
  ''' Report filters.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_filter_date_from = String.Empty
    m_filter_date_to = String.Empty
    m_filter_terminal = String.Empty
    m_filter_site = String.Empty
    m_filter_close_only = String.Empty
    m_filter_increment = String.Empty

    If Not String.IsNullOrEmpty(dtp_init_from.Value) Then
      m_filter_date_from = dtp_init_from.Value
    End If

    If Not String.IsNullOrEmpty(dtp_init_to.Value) Then
      m_filter_date_to = dtp_init_to.Value
    End If

    m_filter_terminal = Me.uc_pr_list.GetTerminalReportText()

    If m_is_multisite_center Then
      If Not String.IsNullOrEmpty(Me.uc_site_select.GetSitesIdListSelected()) Then
        m_filter_site = Me.uc_site_select.GetSitesIdListSelected()
      Else
        m_filter_site = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337)
      End If
    End If

    If chk_only_closed.Checked Then
      m_filter_close_only = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    Else
      m_filter_close_only = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
    End If

    If Not String.IsNullOrEmpty(ef_win_increment.Value) Then
      m_filter_increment = GUI_FormatCurrency(ef_win_increment.Value)
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Sets the report Filter values for Excel and Print
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5846), m_filter_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5847), m_filter_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8957), m_filter_close_only)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8958), m_filter_increment)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4895), m_filter_terminal)

    If m_is_multisite_center Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803), m_filter_site)
    End If

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' Set format for excel columns
  ''' </summary>
  ''' <param name="ExcelData"></param>
  ''' <param name="FirstColIndex"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_WORKING_DAY, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams

#End Region ' Overrides

#Region " Private methods "

  ''' <summary>
  ''' Set style grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_COLUMN_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Site Id
      .Column(GRID_COLUMN_SITE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' Site ID
      .Column(GRID_COLUMN_SITE).Width = GRID_COLUMN_WIDTH_SITE
      .Column(GRID_COLUMN_SITE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492) ' Terminal
      .Column(GRID_COLUMN_TERMINAL).Width = GRID_COLUMN_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Game
      .Column(GRID_COLUMN_GAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008) ' Provider id
      .Column(GRID_COLUMN_GAME).Width = GRID_COLUMN_WIDTH_GAME
      .Column(GRID_COLUMN_GAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2115) ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = GRID_COLUMN_WIDTH_TERMINAL_TYPE
      .Column(GRID_COLUMN_TERMINAL_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bets AVG
      .Column(GRID_COLUMN_BET_AVG).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8435) 'Bets AVG
      .Column(GRID_COLUMN_BET_AVG).Width = GRID_COLUMN_WIDTH_BET_AVG
      .Column(GRID_COLUMN_BET_AVG).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Bets
      .Column(GRID_COLUMN_BETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8963) ' Bet
      .Column(GRID_COLUMN_BETS).Width = GRID_COLUMN_WIDTH_BETS
      .Column(GRID_COLUMN_BETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Payments
      .Column(GRID_COLUMN_PAYMENTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8964) ' Payments
      .Column(GRID_COLUMN_PAYMENTS).Width = GRID_COLUMN_WIDTH_PAYMENTS
      .Column(GRID_COLUMN_PAYMENTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpots
      .Column(GRID_COLUMN_JACKPOTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5849) ' Jackpots
      .Column(GRID_COLUMN_JACKPOTS).Width = GRID_COLUMN_WIDTH_JACKPOTS
      .Column(GRID_COLUMN_JACKPOTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' WorkDay
      .Column(GRID_COLUMN_WORKING_DAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) ' Workday
      .Column(GRID_COLUMN_WORKING_DAY).Width = GRID_COLUMN_WIDTH_WORKING_DAY
      .Column(GRID_COLUMN_WORKING_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Profit
      .Column(GRID_COLUMN_PROFIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8965) ' Profits
      .Column(GRID_COLUMN_PROFIT).Width = GRID_COLUMN_WIDTH_PROFIT
      .Column(GRID_COLUMN_PROFIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Profit AVG
      .Column(GRID_COLUMN_PROFIT_AVG).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8974) ' Profits avg
      .Column(GRID_COLUMN_PROFIT_AVG).Width = GRID_COLUMN_WIDTH_PROFIT_AVG
      .Column(GRID_COLUMN_PROFIT_AVG).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Payout
      .Column(GRID_COLUMN_PAYOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8966) ' Payout
      .Column(GRID_COLUMN_PAYOUT).Width = GRID_COLUMN_WIDTH_PAYOUT
      .Column(GRID_COLUMN_PAYOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical payout
      .Column(GRID_COLUMN_THEORICAL_PAYOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8975) ' Theoretical Payout
      .Column(GRID_COLUMN_THEORICAL_PAYOUT).Width = GRID_COLUMN_WIDTH_THEORICAL_PAYOUT
      .Column(GRID_COLUMN_THEORICAL_PAYOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Set defaults values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _str_closing_time As String

    _closing_time = GetDefaultClosingTime()
    _str_closing_time = GUI_FormatDate(New DateTime(TimeSpan.FromHours(_closing_time).Ticks), ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.lbl_from_hour.Text = _str_closing_time
    Me.lbl_to_hour.Text = _str_closing_time

    Me.dtp_init_to.Value = WGDB.Now.Date.AddHours(_closing_time)
    Me.dtp_init_to.Checked = False
    Me.dtp_init_from.Value = Me.dtp_init_to.Value.AddDays(-1)
    Me.dtp_init_from.Checked = True

    Me.chk_only_closed.Checked = True
    Me.ef_win_increment.Value = String.Empty

    If m_is_multisite_center Then
      Me.uc_site_select.SetFirstValue()
    End If

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Add sobtotal and total row
  ''' </summary>
  ''' <param name="TextType"></param>
  ''' <param name="TotalBets"></param>
  ''' <param name="TotalPayments"></param>
  ''' <param name="TotalProfit"></param>
  ''' <param name="TotalJackpots"></param>
  ''' <param name="ColorCell"></param>
  ''' <remarks></remarks>
  Private Sub AddRowTotal(ByVal TextType As String,
                          ByVal TotalBets As Double,
                          ByVal TotalPayments As Double,
                          ByVal TotalProfit As Double,
                          ByVal TotalJackpots As Double,
                          ByRef ColorCell As ENUM_GUI_COLOR,
                          ByVal Num As Int32)

    Dim _prev_redraw As Boolean
    Dim _payout As Double
    Dim _bets_avg As Double

    _payout = 0
    _bets_avg = 0

    _prev_redraw = Me.Grid.Redraw

    Me.Grid.Redraw = False
    Me.Grid.AddRow()
    Me.Grid.Redraw = False

    If TotalBets > 0 Then
      _payout = (TotalPayments / TotalBets) * 100
      _bets_avg = TotalBets / Num
    End If

    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_INDEX).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_SITE).Value = TextType
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_TERMINAL).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_GAME).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_TERMINAL_TYPE).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_WORKING_DAY).Value = String.Empty
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_THEORICAL_PAYOUT).Value = String.Empty

    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_BETS).Value = GUI_FormatCurrency(TotalBets, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PAYMENTS).Value = GUI_FormatCurrency(TotalPayments , ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(TotalJackpots, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PROFIT).Value = GUI_FormatCurrency(TotalProfit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_BET_AVG).Value = GUI_FormatCurrency(_bets_avg, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PROFIT_AVG).Value = GUI_FormatCurrency(TotalProfit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(Grid.NumRows - 1, GRID_COLUMN_PAYOUT).Value = String.Format("{0}%", GUI_FormatNumber(_payout, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Me.Grid.Row(Grid.NumRows - 1).BackColor = GetColor(ColorCell)

    Me.Grid.Redraw = _prev_redraw

  End Sub ' AddRowTotal

#End Region ' Private methods

End Class