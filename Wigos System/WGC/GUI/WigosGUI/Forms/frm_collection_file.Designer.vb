<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_collection_file
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_collection_file))
    Me.gb_collections = New System.Windows.Forms.GroupBox()
    Me.dg_collection_sel = New GUI_Controls.uc_collection_sel()
    Me.gb_detail = New System.Windows.Forms.GroupBox()
    Me.dg_collection_detail = New GUI_Controls.uc_collection_detail()
    Me.cmb_cage_sessions = New GUI_Controls.uc_combo()
    Me.gb_dates = New System.Windows.Forms.GroupBox()
    Me.ef_working_day = New GUI_Controls.uc_entry_field()
    Me.ef_cage_session = New GUI_Controls.uc_entry_field()
    Me.gb_collection_status = New System.Windows.Forms.GroupBox()
    Me.lbl_only_file_color = New System.Windows.Forms.Label()
    Me.lbl_only_system_color = New System.Windows.Forms.Label()
    Me.lbl_unbalanced_color = New System.Windows.Forms.Label()
    Me.lbl_balanced_color = New System.Windows.Forms.Label()
    Me.chk_only_file = New System.Windows.Forms.CheckBox()
    Me.chk_only_system = New System.Windows.Forms.CheckBox()
    Me.chk_unbalanced = New System.Windows.Forms.CheckBox()
    Me.chk_balanced = New System.Windows.Forms.CheckBox()
    Me.gb_totals = New System.Windows.Forms.GroupBox()
    Me.ef_total_bills = New GUI_Controls.uc_entry_field()
    Me.ef_total_tickets = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gb_collections.SuspendLayout()
    Me.gb_detail.SuspendLayout()
    Me.gb_dates.SuspendLayout()
    Me.gb_collection_status.SuspendLayout()
    Me.gb_totals.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_totals)
    Me.panel_data.Controls.Add(Me.gb_collection_status)
    Me.panel_data.Controls.Add(Me.gb_detail)
    Me.panel_data.Controls.Add(Me.gb_collections)
    Me.panel_data.Controls.Add(Me.gb_dates)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(797, 724)
    '
    'gb_collections
    '
    Me.gb_collections.Controls.Add(Me.dg_collection_sel)
    Me.gb_collections.Location = New System.Drawing.Point(3, 159)
    Me.gb_collections.Name = "gb_collections"
    Me.gb_collections.Size = New System.Drawing.Size(784, 281)
    Me.gb_collections.TabIndex = 1
    Me.gb_collections.TabStop = False
    Me.gb_collections.Text = "xCollections"
    '
    'dg_collection_sel
    '
    Me.dg_collection_sel.Collected = True
    Me.dg_collection_sel.CollectionMode = WSI.Common.CollectionImport.COLLECTION_MODE.MODE_COLLECT
    Me.dg_collection_sel.Completed = True
    Me.dg_collection_sel.CountIdLoad = 0
    Me.dg_collection_sel.CountIdOk = 0
    Me.dg_collection_sel.HaveChanges = False
    Me.dg_collection_sel.ImportIndex = 0
    Me.dg_collection_sel.Location = New System.Drawing.Point(4, 16)
    Me.dg_collection_sel.MachineIdType = WSI.Common.CollectionImport.ImportId.StackerId
    Me.dg_collection_sel.Name = "dg_collection_sel"
    Me.dg_collection_sel.NewValue = Nothing
    Me.dg_collection_sel.OldValue = Nothing
    Me.dg_collection_sel.Pending = True
    Me.dg_collection_sel.RaiseCellDataChangedEvent = False
    Me.dg_collection_sel.Size = New System.Drawing.Size(774, 254)
    Me.dg_collection_sel.TabIndex = 6
    Me.dg_collection_sel.TotalBills = 0.0R
    Me.dg_collection_sel.TotalTickets = New Decimal(New Integer() {0, 0, 0, 0})
    Me.dg_collection_sel.Unbalanced = True
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.dg_collection_detail)
    Me.gb_detail.Location = New System.Drawing.Point(3, 442)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Size = New System.Drawing.Size(784, 279)
    Me.gb_detail.TabIndex = 2
    Me.gb_detail.TabStop = False
    Me.gb_detail.Text = "xDetail"
    '
    'dg_collection_detail
    '
    Me.dg_collection_detail.Location = New System.Drawing.Point(2, 13)
    Me.dg_collection_detail.Name = "dg_collection_detail"
    Me.dg_collection_detail.Size = New System.Drawing.Size(775, 265)
    Me.dg_collection_detail.TabIndex = 0
    '
    'cmb_cage_sessions
    '
    Me.cmb_cage_sessions.AllowUnlistedValues = False
    Me.cmb_cage_sessions.AutoCompleteMode = False
    Me.cmb_cage_sessions.IsReadOnly = False
    Me.cmb_cage_sessions.Location = New System.Drawing.Point(6, 41)
    Me.cmb_cage_sessions.Name = "cmb_cage_sessions"
    Me.cmb_cage_sessions.SelectedIndex = -1
    Me.cmb_cage_sessions.Size = New System.Drawing.Size(358, 24)
    Me.cmb_cage_sessions.SufixText = "Sufix Text"
    Me.cmb_cage_sessions.SufixTextVisible = True
    Me.cmb_cage_sessions.TabIndex = 0
    Me.cmb_cage_sessions.TextWidth = 120
    '
    'gb_dates
    '
    Me.gb_dates.Controls.Add(Me.ef_working_day)
    Me.gb_dates.Controls.Add(Me.cmb_cage_sessions)
    Me.gb_dates.Controls.Add(Me.ef_cage_session)
    Me.gb_dates.Location = New System.Drawing.Point(5, 3)
    Me.gb_dates.Name = "gb_dates"
    Me.gb_dates.Size = New System.Drawing.Size(386, 75)
    Me.gb_dates.TabIndex = 0
    Me.gb_dates.TabStop = False
    '
    'ef_working_day
    '
    Me.ef_working_day.DoubleValue = 0.0R
    Me.ef_working_day.IntegerValue = 0
    Me.ef_working_day.IsReadOnly = False
    Me.ef_working_day.Location = New System.Drawing.Point(6, 13)
    Me.ef_working_day.Name = "ef_working_day"
    Me.ef_working_day.PlaceHolder = Nothing
    Me.ef_working_day.Size = New System.Drawing.Size(357, 24)
    Me.ef_working_day.SufixText = "Sufix Text"
    Me.ef_working_day.SufixTextVisible = True
    Me.ef_working_day.TabIndex = 12
    Me.ef_working_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_working_day.TextValue = ""
    Me.ef_working_day.TextWidth = 120
    Me.ef_working_day.Value = ""
    Me.ef_working_day.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cage_session
    '
    Me.ef_cage_session.DoubleValue = 0.0R
    Me.ef_cage_session.IntegerValue = 0
    Me.ef_cage_session.IsReadOnly = False
    Me.ef_cage_session.Location = New System.Drawing.Point(6, 43)
    Me.ef_cage_session.Name = "ef_cage_session"
    Me.ef_cage_session.PlaceHolder = Nothing
    Me.ef_cage_session.Size = New System.Drawing.Size(357, 24)
    Me.ef_cage_session.SufixText = "Sufix Text"
    Me.ef_cage_session.SufixTextVisible = True
    Me.ef_cage_session.TabIndex = 11
    Me.ef_cage_session.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cage_session.TextValue = ""
    Me.ef_cage_session.TextWidth = 120
    Me.ef_cage_session.Value = ""
    Me.ef_cage_session.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_collection_status
    '
    Me.gb_collection_status.Controls.Add(Me.lbl_only_file_color)
    Me.gb_collection_status.Controls.Add(Me.lbl_only_system_color)
    Me.gb_collection_status.Controls.Add(Me.lbl_unbalanced_color)
    Me.gb_collection_status.Controls.Add(Me.lbl_balanced_color)
    Me.gb_collection_status.Controls.Add(Me.chk_only_file)
    Me.gb_collection_status.Controls.Add(Me.chk_only_system)
    Me.gb_collection_status.Controls.Add(Me.chk_unbalanced)
    Me.gb_collection_status.Controls.Add(Me.chk_balanced)
    Me.gb_collection_status.Location = New System.Drawing.Point(401, 3)
    Me.gb_collection_status.Name = "gb_collection_status"
    Me.gb_collection_status.Size = New System.Drawing.Size(234, 154)
    Me.gb_collection_status.TabIndex = 1
    Me.gb_collection_status.TabStop = False
    Me.gb_collection_status.Text = "xCollectionStatus"
    '
    'lbl_only_file_color
    '
    Me.lbl_only_file_color.BackColor = System.Drawing.Color.Red
    Me.lbl_only_file_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_only_file_color.Location = New System.Drawing.Point(205, 117)
    Me.lbl_only_file_color.Name = "lbl_only_file_color"
    Me.lbl_only_file_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_only_file_color.TabIndex = 0
    '
    'lbl_only_system_color
    '
    Me.lbl_only_system_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_only_system_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_only_system_color.Location = New System.Drawing.Point(205, 88)
    Me.lbl_only_system_color.Name = "lbl_only_system_color"
    Me.lbl_only_system_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_only_system_color.TabIndex = 7
    '
    'lbl_unbalanced_color
    '
    Me.lbl_unbalanced_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_unbalanced_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_unbalanced_color.Location = New System.Drawing.Point(205, 59)
    Me.lbl_unbalanced_color.Name = "lbl_unbalanced_color"
    Me.lbl_unbalanced_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_unbalanced_color.TabIndex = 6
    '
    'lbl_balanced_color
    '
    Me.lbl_balanced_color.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_balanced_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_balanced_color.Location = New System.Drawing.Point(205, 30)
    Me.lbl_balanced_color.Name = "lbl_balanced_color"
    Me.lbl_balanced_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_balanced_color.TabIndex = 5
    '
    'chk_only_file
    '
    Me.chk_only_file.AutoSize = True
    Me.chk_only_file.Location = New System.Drawing.Point(19, 117)
    Me.chk_only_file.Name = "chk_only_file"
    Me.chk_only_file.Size = New System.Drawing.Size(78, 17)
    Me.chk_only_file.TabIndex = 4
    Me.chk_only_file.Text = "xOnlyFile"
    Me.chk_only_file.UseVisualStyleBackColor = True
    '
    'chk_only_system
    '
    Me.chk_only_system.AutoSize = True
    Me.chk_only_system.Location = New System.Drawing.Point(19, 88)
    Me.chk_only_system.Name = "chk_only_system"
    Me.chk_only_system.Size = New System.Drawing.Size(102, 17)
    Me.chk_only_system.TabIndex = 3
    Me.chk_only_system.Text = "xOnlySystem"
    Me.chk_only_system.UseVisualStyleBackColor = True
    '
    'chk_unbalanced
    '
    Me.chk_unbalanced.AutoSize = True
    Me.chk_unbalanced.Location = New System.Drawing.Point(19, 59)
    Me.chk_unbalanced.Name = "chk_unbalanced"
    Me.chk_unbalanced.Size = New System.Drawing.Size(99, 17)
    Me.chk_unbalanced.TabIndex = 2
    Me.chk_unbalanced.Text = "xUnbalanced"
    Me.chk_unbalanced.UseVisualStyleBackColor = True
    '
    'chk_balanced
    '
    Me.chk_balanced.AutoSize = True
    Me.chk_balanced.Location = New System.Drawing.Point(19, 30)
    Me.chk_balanced.Name = "chk_balanced"
    Me.chk_balanced.Size = New System.Drawing.Size(85, 17)
    Me.chk_balanced.TabIndex = 1
    Me.chk_balanced.Text = "xBalanced"
    Me.chk_balanced.UseVisualStyleBackColor = True
    '
    'gb_totals
    '
    Me.gb_totals.Controls.Add(Me.ef_total_bills)
    Me.gb_totals.Controls.Add(Me.ef_total_tickets)
    Me.gb_totals.Location = New System.Drawing.Point(5, 79)
    Me.gb_totals.Name = "gb_totals"
    Me.gb_totals.Size = New System.Drawing.Size(386, 78)
    Me.gb_totals.TabIndex = 3
    Me.gb_totals.TabStop = False
    Me.gb_totals.Text = "xTotals"
    '
    'ef_total_bills
    '
    Me.ef_total_bills.DoubleValue = 0.0R
    Me.ef_total_bills.IntegerValue = 0
    Me.ef_total_bills.IsReadOnly = False
    Me.ef_total_bills.Location = New System.Drawing.Point(6, 13)
    Me.ef_total_bills.Name = "ef_total_bills"
    Me.ef_total_bills.PlaceHolder = Nothing
    Me.ef_total_bills.Size = New System.Drawing.Size(357, 24)
    Me.ef_total_bills.SufixText = "Sufix Text"
    Me.ef_total_bills.SufixTextVisible = True
    Me.ef_total_bills.TabIndex = 14
    Me.ef_total_bills.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_total_bills.TextValue = ""
    Me.ef_total_bills.TextWidth = 120
    Me.ef_total_bills.Value = ""
    Me.ef_total_bills.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_total_tickets
    '
    Me.ef_total_tickets.DoubleValue = 0.0R
    Me.ef_total_tickets.IntegerValue = 0
    Me.ef_total_tickets.IsReadOnly = False
    Me.ef_total_tickets.Location = New System.Drawing.Point(6, 41)
    Me.ef_total_tickets.Name = "ef_total_tickets"
    Me.ef_total_tickets.PlaceHolder = Nothing
    Me.ef_total_tickets.Size = New System.Drawing.Size(357, 24)
    Me.ef_total_tickets.SufixText = "Sufix Text"
    Me.ef_total_tickets.SufixTextVisible = True
    Me.ef_total_tickets.TabIndex = 13
    Me.ef_total_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_total_tickets.TextValue = ""
    Me.ef_total_tickets.TextWidth = 120
    Me.ef_total_tickets.Value = ""
    Me.ef_total_tickets.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_collection_file
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(901, 732)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_collection_file"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_collection_file"
    Me.panel_data.ResumeLayout(False)
    Me.gb_collections.ResumeLayout(False)
    Me.gb_detail.ResumeLayout(False)
    Me.gb_dates.ResumeLayout(False)
    Me.gb_collection_status.ResumeLayout(False)
    Me.gb_collection_status.PerformLayout()
    Me.gb_totals.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_collections As System.Windows.Forms.GroupBox
  Friend WithEvents dg_collection_sel As GUI_Controls.uc_collection_sel
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Friend WithEvents dg_collection_detail As GUI_Controls.uc_collection_detail
  Friend WithEvents gb_collection_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_only_file_color As System.Windows.Forms.Label
  Friend WithEvents lbl_only_system_color As System.Windows.Forms.Label
  Friend WithEvents lbl_unbalanced_color As System.Windows.Forms.Label
  Friend WithEvents lbl_balanced_color As System.Windows.Forms.Label
  Friend WithEvents chk_only_file As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_system As System.Windows.Forms.CheckBox
  Friend WithEvents chk_unbalanced As System.Windows.Forms.CheckBox
  Friend WithEvents chk_balanced As System.Windows.Forms.CheckBox
  Friend WithEvents gb_dates As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_cage_sessions As GUI_Controls.uc_combo
  Friend WithEvents ef_cage_session As GUI_Controls.uc_entry_field
  Friend WithEvents ef_working_day As GUI_Controls.uc_entry_field
  Friend WithEvents gb_totals As System.Windows.Forms.GroupBox
  Friend WithEvents ef_total_bills As GUI_Controls.uc_entry_field
  Friend WithEvents ef_total_tickets As GUI_Controls.uc_entry_field
End Class
