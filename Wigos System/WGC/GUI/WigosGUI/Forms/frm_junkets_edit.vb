﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_edit.vb
' DESCRIPTION:   Create and edit jankets
' AUTHOR:        David Perelló
' CREATION DATE: 03-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2017  DPC    Initial version
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports GUI_CommonMisc
Imports WSI.Common.Junkets

Public Class frm_junkets_edit
  Inherits frm_base_edit

#Region " Members "

  Private m_junket_read As CLASS_JUNKETS
  Private m_junket_edit As CLASS_JUNKETS
  Private m_dt_flyers As DataTable

#End Region ' Members

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_CODE As Integer = 1
  Private Const GRID_COLUMN_COMMENTS As Integer = 2

  Private Const GRID_WIDTH_CODE As Integer = 100
  Private Const GRID_WIDTH_COMMENT As Integer = 400

  Private Const DATATABLE_COLUMN_NAME_ID As String = "ID"
  Private Const DATATABLE_COLUMN_NAME_CODE As String = "CODE"
  Private Const DATATABLE_COLUMN_NAME_COMMENTS As String = "COMMENTS"

#End Region ' Constants

#Region " Overrides "

  ''' <summary>
  ''' GUI_SetFormId
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKETS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' GUI_InitControls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8037)

    Me.ef_code_rep.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710) ' Code
    Call Me.ef_code_rep.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 10)
    Me.ef_code_rep.IsReadOnly = True

    Me.ef_name_rep.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399) ' Name
    Call Me.ef_name_rep.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_name_rep.IsReadOnly = True

    Me.ef_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710) ' Code
    Call Me.ef_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 10)

    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399) ' Name
    Call Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) ' Date from
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) ' Date to
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Me.ef_com_cutomers.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8007) ' By number of customers
    Call Me.ef_com_cutomers.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.ef_com_visits.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8008) ' By number of visits
    Call Me.ef_com_visits.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.ef_com_enrolled.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8009) ' By Enrolled customers
    Call Me.ef_com_enrolled.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.ef_com_coinin.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8010) ' By coin in
    Call Me.ef_com_coinin.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

    Me.ef_com_netwin.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8011) ' By theorical netwin
    Call Me.ef_com_netwin.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

    Me.ef_com_buyin.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8012) ' By buy in
    Call Me.ef_com_buyin.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

    Me.ef_com_points.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8013) ' By points
    Call Me.ef_com_points.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)

    Me.ef_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6506) ' Creation date
    Call Me.ef_creation.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_creation.IsReadOnly = True

    txt_comments.MaxLength = 512

    Me.gb_comissions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2149) ' Commissions
    Me.gb_representative_junket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8565) ' Junkets representatives
    Me.gb_comments.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1716) ' Comments
    Me.gb_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474) ' Details
    Me.gb_data_range.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) ' Data range
    Me.gb_flyers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8062) ' Flyers

    Me.bt_search_rep.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(723) ' Junkets representatives
    Me.bt_new.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2) ' New
    Me.bt_select.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1894) ' JSelect

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    InitDataTableFlyers()

    If m_junket_edit Is Nothing Then
      m_junket_edit = DbEditedObject
    End If

    If m_junket_read Is Nothing Then
      m_junket_read = DbReadObject
    End If

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then

      ef_code.IsReadOnly = True
      ef_code.TabStop = False
      bt_search_rep.Enabled = False
      bt_search_rep.TabStop = False

    End If

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' GUI_SetScreenData
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then

      ef_com_cutomers.Value = GUI_FormatCurrency(0)
      ef_com_visits.Value = GUI_FormatCurrency(0)
      ef_com_enrolled.Value = GUI_FormatCurrency(0)
      ef_com_points.Value = GUI_FormatCurrency(0)
      ef_com_coinin.Value = GUI_FormatNumber(0)
      ef_com_netwin.Value = GUI_FormatNumber(0)
      ef_com_buyin.Value = GUI_FormatNumber(0)

      dtp_from.Value = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      dtp_to.Value = GUI_FormatDate(WGDB.Now.AddMonths(1), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      ef_creation.Value = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Else

      ef_code.Value = m_junket_read.Code
      ef_name.Value = m_junket_read.Name
      dtp_from.Value = GUI_FormatDate(m_junket_read.DateFrom, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      dtp_to.Value = GUI_FormatDate(m_junket_read.DateTo, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
      ef_creation.Value = GUI_FormatDate(m_junket_read.Creation, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

      ef_code_rep.Value = m_junket_read.JunketRepresentative.code
      ef_name_rep.Value = m_junket_read.JunketRepresentative.name

      ef_com_cutomers.Value = GUI_FormatCurrency(m_junket_read.getValueComission(CommissionType.ByCostumer))
      ef_com_visits.Value = GUI_FormatCurrency(m_junket_read.getValueComission(CommissionType.ByVisit))
      ef_com_enrolled.Value = GUI_FormatCurrency(m_junket_read.getValueComission(CommissionType.ByEnrolled))
      ef_com_points.Value = GUI_FormatCurrency(m_junket_read.getValueComission(CommissionType.ByPoints))
      ef_com_coinin.Value = GUI_FormatNumber(m_junket_read.getValueComission(CommissionType.ByCoinIn))
      ef_com_netwin.Value = GUI_FormatNumber(m_junket_read.getValueComission(CommissionType.ByTheoricalNetwin))
      ef_com_buyin.Value = GUI_FormatNumber(m_junket_read.getValueComission(CommissionType.ByBuyIn))

      txt_comments.Text = m_junket_read.Comment

    End If

    LoadGridFlyers()

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' GUI_GetScreenData
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    m_junket_edit.Code = ef_code.Value
    m_junket_edit.Name = ef_name.Value
    m_junket_edit.DateFrom = dtp_from.Value.AddHours(GetDefaultClosingTime())
    m_junket_edit.DateTo = dtp_to.Value.AddHours(GetDefaultClosingTime())
    m_junket_edit.Comment = txt_comments.Text

    m_junket_edit.SetComission(CommissionType.ByCostumer, ef_com_cutomers.Value)
    m_junket_edit.SetComission(CommissionType.ByVisit, ef_com_visits.Value)
    m_junket_edit.SetComission(CommissionType.ByEnrolled, ef_com_enrolled.Value)
    m_junket_edit.SetComission(CommissionType.ByCoinIn, ef_com_coinin.Value)
    m_junket_edit.SetComission(CommissionType.ByTheoricalNetwin, ef_com_netwin.Value)
    m_junket_edit.SetComission(CommissionType.ByBuyIn, ef_com_buyin.Value)
    m_junket_edit.SetComission(CommissionType.ByPoints, ef_com_points.Value)

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' GUI_DB_Operation
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim ctx As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_JUNKETS
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        DbStatus = DbEditedObject.DB_Insert(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        DbStatus = DbEditedObject.DB_Update(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' GUI_IsScreenDataOk
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _date_from As DateTime
    Dim _date_to As DateTime

    If String.IsNullOrEmpty(ef_code.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710))
      ef_code.Focus()

      Return False
    End If

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      If CLASS_JUNKETS.ExistJunket(ef_code.Value) = ENUM_STATUS.STATUS_DUPLICATE_KEY Then

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8036), ENUM_MB_TYPE.MB_TYPE_WARNING)
        ef_code.Focus()

        Return False
      End If
    End If

    If String.IsNullOrEmpty(ef_name.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399))
      ef_name.Focus()

      Return False
    End If

    If m_junket_edit.JunketRepresentative.id = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(8089))
      bt_search_rep.Focus()

      Return False
    End If

    _date_from = DateTime.Parse(dtp_from.Value)
    _date_to = DateTime.Parse(dtp_to.Value)

    If _date_from > _date_to Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1688), ENUM_MB_TYPE.MB_TYPE_WARNING)
      dtp_from.Focus()

      Return False
    End If

    If _date_from.ToShortDateString.Equals(_date_to.ToShortDateString) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8157), ENUM_MB_TYPE.MB_TYPE_WARNING, , , _date_to.AddDays(1).ToShortDateString)
      dtp_to.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' GUI_ButtonClick
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub 'GUI_ButtonClick

#End Region ' Overrides

#Region " Overloads "

  ''' <summary>
  ''' Overload of button ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Overloads Function ButtonOkClick() As Boolean
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Function
    End If

    Me.ActiveControl = Me.AcceptButton

    If Not GUI_IsScreenDataOk() Then
      Exit Function
    End If

    Call GUI_GetScreenData()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_INSERT)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_INSERT)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

      Return True
    End If

    Return False

  End Function ' ButtonOkClick

#End Region ' Overloads

#Region " Events "

  Private Sub bt_search_rep_Click(sender As Object, e As EventArgs) Handles bt_search_rep.Click


    'Dim a As New WSI.Common.Junkets.JunketsCommissionsMovements
    'Dim b As New WSI.Common.Junkets.Commission

    'a.FlyerId = 29
    'a.Commissions.Add(New WSI.Common.Junkets.Commission(WSI.Common.Junkets.CommissionType.ByCostumer))
    'a.Commissions.Add(New WSI.Common.Junkets.Commission(WSI.Common.Junkets.CommissionType.ByVisit))
    'a.Commissions.Add(New WSI.Common.Junkets.Commission(WSI.Common.Junkets.CommissionType.ByEnrolled))

    'a.Save()



    Dim _frm As frm_junkets_representatives_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_junkets_representatives_sel()

    _frm.IsSelectMode = True

    Call _frm.ShowDialog(Me)

    m_junket_edit.JunketRepresentative = _frm.JunketRepresentative

    ef_code_rep.Value = m_junket_edit.JunketRepresentative.code
    ef_name_rep.Value = m_junket_edit.JunketRepresentative.name

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' bt_search_rep_Click

  Private Sub bt_new_Click(sender As Object, e As EventArgs) Handles bt_new.Click

    GUI_ShowNewFlyer()

  End Sub ' bt_new_Click

  Private Sub bt_select_Click(sender As Object, e As EventArgs) Handles bt_select.Click

    GUI_EditSelectedItem()

  End Sub ' bt_select_Click

  Private Sub dgv_flyers_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_flyers.CellDoubleClick

    If e.RowIndex > -1 Then
      GUI_EditSelectedItem()
    End If

  End Sub ' dgv_flyers_CellDoubleClick

#End Region ' Events

#Region " Private Methods "

  ''' <summary>
  ''' Set style to grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    dgv_flyers.Columns(0).Visible = False
    dgv_flyers.Columns(1).Width = GRID_WIDTH_CODE
    dgv_flyers.Columns(2).Width = GRID_WIDTH_COMMENT

    dgv_flyers.Columns(1).HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710)
    dgv_flyers.Columns(2).HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1716)

    dgv_flyers.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

    dgv_flyers.RowTemplate.Height = 15

    For Each _col As DataGridViewColumn In dgv_flyers.Columns
      _col.SortMode = DataGridViewColumnSortMode.NotSortable
    Next

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Load grid flyers
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadGridFlyers()

    Dim _row As DataRow
    Dim dataView As DataView

    m_dt_flyers.Rows.Clear()

    For Each _flyer As CLASS_JUNKETS_FLYER.TYPE_FLYER In m_junket_read.Flyers

      _row = m_dt_flyers.NewRow
      _row(DATATABLE_COLUMN_NAME_ID) = _flyer.id
      _row(DATATABLE_COLUMN_NAME_CODE) = _flyer.code
      _row(DATATABLE_COLUMN_NAME_COMMENTS) = _flyer.comment

      m_dt_flyers.Rows.Add(_row)

    Next

    dataView = New DataView(m_dt_flyers)
    dataView.Sort = String.Format("{0} DESC", DATATABLE_COLUMN_NAME_ID)
    m_dt_flyers = dataView.ToTable()

    dgv_flyers.DataSource = m_dt_flyers

    If m_dt_flyers.Rows.Count > 0 Then
      bt_select.Enabled = True
      bt_select.TabStop = True
    End If

    GUI_StyleSheet()

  End Sub ' LoadGridFlyers

  ''' <summary>
  ''' Open form for create a flyer
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_ShowNewFlyer()

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8079), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
        If Not ButtonOkClick() Then

          Return
        End If
      Else

        Return
      End If
    End If

    OpenFormFlyer(True)

    ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

  End Sub ' GUI_ShowNewFlyer

  ''' <summary>
  ''' Select a flyer and open form for edit
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_EditSelectedItem()

    OpenFormFlyer(False)

    Call Me.dgv_flyers.Focus()

  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  ''' Ini datatable flyers
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitDataTableFlyers()

    m_dt_flyers = New DataTable

    m_dt_flyers.Columns.Add(DATATABLE_COLUMN_NAME_ID, GetType(Int64))
    m_dt_flyers.Columns.Add(DATATABLE_COLUMN_NAME_CODE, GetType(String))
    m_dt_flyers.Columns.Add(DATATABLE_COLUMN_NAME_COMMENTS, GetType(String))

  End Sub ' InitDataTableFlyers

  ''' <summary>
  ''' Open form flyers in mode new or edit
  ''' </summary>
  ''' <param name="IsNew"></param>
  ''' <remarks></remarks>
  Private Sub OpenFormFlyer(ByVal IsNew As Boolean)

    Dim _index As Int32
    Dim _frm As frm_junkets_flyer_edit
    Dim _info_form_flyer As CLASS_JUNKETS_FLYER.InfoFormFlyer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _index = 0

    If Not dgv_flyers.CurrentRow Is Nothing Then
      _index = dgv_flyers.CurrentRow.Index
    End If

    _frm = New frm_junkets_flyer_edit()
    _info_form_flyer = New CLASS_JUNKETS_FLYER.InfoFormFlyer

    _info_form_flyer.junket_id = Me.m_junket_edit.Id
    _info_form_flyer.junket_name = Me.m_junket_edit.Name
    _info_form_flyer.junket_code = Me.m_junket_edit.Code
    _info_form_flyer.junket_expiration_date = Me.m_junket_edit.DateTo
    _info_form_flyer.flyer_code = CLASS_JUNKETS_FLYER.GenerateUniqueCode(String.Format("{0}{1}", Me.m_junket_edit.Code(0), Me.m_junket_edit.JunketRepresentative.code(0)), String.Empty, 6)

    If IsNew Then
      _info_form_flyer.flyer_id = -1
    Else
      If dgv_flyers.SelectedRows.Count > 0 Then
        _info_form_flyer.flyer_id = dgv_flyers.SelectedRows(0).Cells(0).Value
      End If
    End If

    _frm.InfoFormFlyer = _info_form_flyer

    If IsNew Then
      _frm.ShowNewItem()
    Else
      If dgv_flyers.SelectedRows.Count Then
        _frm.ShowEditItem(Nothing)
      End If
    End If

    m_junket_edit.SetFlyersToJunket()
    m_junket_read.Flyers.Clear()
    m_junket_read.Flyers.AddRange(m_junket_edit.Flyers)

    LoadGridFlyers()

    If dgv_flyers.Rows.Count > 0 Then
      dgv_flyers.Rows(_index).Selected = True
    End If

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' OpenFormFlyer

#End Region ' Private Methods

End Class