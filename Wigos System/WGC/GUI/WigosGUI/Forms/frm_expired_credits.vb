'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_expired_credits
' DESCRIPTION:   Expired Credits Form
'
' CREATION DATE: 01-MAR-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-MAR-2010  AAV    Initial version
' 27-FEB-2012  JMM    Added not redeemable 2
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 22-APR-2013  LEM    Fixed Bug #741: Did not show Excel filter for Show Account Details option
' 20-JUN-2013  RRR    Fixed Bug #889: Delay on search when "Mostrar Detalles" not selected (solved by David Hern�ndez)
' 21-JUN-2013  RRR    Change Request #892: Added Credit Type filter
' 11-NOV-2013  JRM    Added TITO Functionallity to display expired tickets
' 17-DEC-2013  RMS    In TITO Mode virtual accounts should be hidden
' 28-ENE-2014  JFC    In TITO mode all accounts must be consider. Furthermore, check box for consider only personal accounts has been added.
' 07-FEB-2014  JFC    Filter error in TITO mode with credit types solved.
' 21-FEB-2014  AMF    Fixed Bug WIG-661: Apply account sel filter
' 25-MAR-2014  HBB    Fixed Bug WIGOSTITO-1170: Virtual account aren't shown
' 28-MAR-2014  ICS    Fixed Bug WIGOSTITO-1177: Incongruence of data in the GUI for the expiration of tickets
' 15-APR-2014  LEM    Fixed Bug WIGOSTITO-1200: Bad format data (last_action_datetime null)
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
' 28-NOV-2014  FJC    Changes in form
'                     � Change NLS_ID_GUI_INVOICE_SYSTEM(476) "Mostrar Detalle de Cuentas" by "Mostrar detalle".
'                     � Mode TITO:      Filter "Tipo Credito" all options checked by default.
'                     � Mode CASHLESS:  Hide option promo when mode is cashless.
'                     � Mode TITO:      Use NLS "Promocional Redimible" in GRID (detail & subtotals / totals).
' 28-MAY-2018  LQB    Fixed Bug WIGOS-12147 [Ticket #14455] Fallo- Reporte de Cr�ditos caducados - No se totalia no redimibles.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text

Public Class frm_expired_credits
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_details = New System.Windows.Forms.CheckBox()
    Me.gb_credit_type = New System.Windows.Forms.GroupBox()
    Me.chk_type_promo = New System.Windows.Forms.CheckBox()
    Me.chk_type_non_redeemeable = New System.Windows.Forms.CheckBox()
    Me.chk_type_price = New System.Windows.Forms.CheckBox()
    Me.chk_type_redeemeable = New System.Windows.Forms.CheckBox()
    Me.chk_only_personal_accounts = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_credit_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_only_personal_accounts)
    Me.panel_filter.Controls.Add(Me.gb_credit_type)
    Me.panel_filter.Controls.Add(Me.chk_details)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1096, 144)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_details, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_credit_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_personal_accounts, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 148)
    Me.panel_data.Size = New System.Drawing.Size(1096, 459)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1090, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1090, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(6, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 0
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(321, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(255, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xCreationDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_details
    '
    Me.chk_details.AutoSize = True
    Me.chk_details.Location = New System.Drawing.Point(321, 87)
    Me.chk_details.Name = "chk_details"
    Me.chk_details.Size = New System.Drawing.Size(72, 17)
    Me.chk_details.TabIndex = 2
    Me.chk_details.Text = "xDetails"
    Me.chk_details.UseVisualStyleBackColor = True
    '
    'gb_credit_type
    '
    Me.gb_credit_type.Controls.Add(Me.chk_type_promo)
    Me.gb_credit_type.Controls.Add(Me.chk_type_non_redeemeable)
    Me.gb_credit_type.Controls.Add(Me.chk_type_price)
    Me.gb_credit_type.Controls.Add(Me.chk_type_redeemeable)
    Me.gb_credit_type.Location = New System.Drawing.Point(582, 8)
    Me.gb_credit_type.Name = "gb_credit_type"
    Me.gb_credit_type.Size = New System.Drawing.Size(200, 100)
    Me.gb_credit_type.TabIndex = 4
    Me.gb_credit_type.TabStop = False
    Me.gb_credit_type.Text = "xCredirType"
    '
    'chk_type_promo
    '
    Me.chk_type_promo.AutoSize = True
    Me.chk_type_promo.Location = New System.Drawing.Point(7, 77)
    Me.chk_type_promo.Name = "chk_type_promo"
    Me.chk_type_promo.Size = New System.Drawing.Size(70, 17)
    Me.chk_type_promo.TabIndex = 3
    Me.chk_type_promo.Text = "xPromo"
    Me.chk_type_promo.UseVisualStyleBackColor = True
    '
    'chk_type_non_redeemeable
    '
    Me.chk_type_non_redeemeable.AutoSize = True
    Me.chk_type_non_redeemeable.Location = New System.Drawing.Point(7, 57)
    Me.chk_type_non_redeemeable.Name = "chk_type_non_redeemeable"
    Me.chk_type_non_redeemeable.Size = New System.Drawing.Size(137, 17)
    Me.chk_type_non_redeemeable.TabIndex = 2
    Me.chk_type_non_redeemeable.Text = "xNon Redeemeable"
    Me.chk_type_non_redeemeable.UseVisualStyleBackColor = True
    '
    'chk_type_price
    '
    Me.chk_type_price.AutoSize = True
    Me.chk_type_price.Location = New System.Drawing.Point(7, 37)
    Me.chk_type_price.Name = "chk_type_price"
    Me.chk_type_price.Size = New System.Drawing.Size(61, 17)
    Me.chk_type_price.TabIndex = 1
    Me.chk_type_price.Text = "xPrice"
    Me.chk_type_price.UseVisualStyleBackColor = True
    '
    'chk_type_redeemeable
    '
    Me.chk_type_redeemeable.AutoSize = True
    Me.chk_type_redeemeable.Location = New System.Drawing.Point(7, 18)
    Me.chk_type_redeemeable.Name = "chk_type_redeemeable"
    Me.chk_type_redeemeable.Size = New System.Drawing.Size(111, 17)
    Me.chk_type_redeemeable.TabIndex = 0
    Me.chk_type_redeemeable.Text = "xRedeemeable"
    Me.chk_type_redeemeable.UseVisualStyleBackColor = True
    '
    'chk_only_personal_accounts
    '
    Me.chk_only_personal_accounts.AutoSize = True
    Me.chk_only_personal_accounts.Location = New System.Drawing.Point(321, 110)
    Me.chk_only_personal_accounts.Name = "chk_only_personal_accounts"
    Me.chk_only_personal_accounts.Size = New System.Drawing.Size(159, 17)
    Me.chk_only_personal_accounts.TabIndex = 3
    Me.chk_only_personal_accounts.Text = "xOnlyPersonalAccounts"
    Me.chk_only_personal_accounts.UseVisualStyleBackColor = True
    '
    'frm_expired_credits
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1104, 611)
    Me.Name = "frm_expired_credits"
    Me.Text = "frm_account_summary"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_credit_type.ResumeLayout(False)
    Me.gb_credit_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' Num grid columns / headers
  Private Const GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const SQL_COLUMN_CARD_ACCOUNT As Integer = 0
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 1
  Private Const SQL_COLUMN_CARD_HOLDER As Integer = 2
  Private Const SQL_COLUMN_CARD_EXPIRATION_DATE As Integer = 3
  Private Const SQL_COLUMN_BALANCE_CREDITS_TYPE As Integer = 4
  Private Const SQL_COLUMN_BALANCE_EXPIRATION_CREDITS As Integer = 5
  Private Const SQL_COLUMN_BALANCE_NON_REDEEMABLE_CREDITS As Integer = 6
  Private Const SQL_COLUMN_BALANCE_PROMO_CREDITS As Integer = 7
  Private Const SQL_COLUMN_BALANCE_PRIZE_CREDITS As Integer = 8

  Private Const SQL_COLUMN_DETAIL_TYPE = 5
  Private Const SQL_COLUMN_DETAIL_AMOUNT = 4
  Private Const SQL_COLUMN_TICKET_VALIDATION_TYPE = 6
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 7

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_EXPIRATION_DATE As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT As Integer = 2
  Private Const GRID_COLUMN_TRACK_DATA As Integer = 3
  Private Const GRID_COLUMN_HOLDER As Integer = 4
  Private Const GRID_COLUMN_CREDITS_TYPE As Integer = 5
  Private Const GRID_COLUMN_EXPIRATION_CREDITS As Integer = 6

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TIME As Integer = 1300
  Private Const GRID_WIDTH_USER As Integer = 1000
  Private Const GRID_WIDTH_ACCOUNT_NONE_TITO As Integer = 1150
  Private Const GRID_WIDTH_ACCOUNT_IS_TITO As Integer = 2300
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1900
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2700
  Private Const GRID_WIDTH_CREDITS_TYPE As Integer = 2350

  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_CARD_BLOCKED As Integer = 1400

#End Region ' Constants

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_track_data As String
  Private m_account As String
  Private m_state As String
  Private m_balance As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_date_to As String
  Private m_date_from As String
  Private m_show_detail As String
  Private m_only_personal_accounts As String
  Private m_credit_types As String

  ' For amount grid
  Private m_total_redeemable_amount As Double
  Private m_total_non_redeemable_amount As Double
  Private m_total_promo_amount As Double
  Private m_total_prize_amount As Double

  ' Date grouping data
  Private m_current_date As Date

  Private m_date_redeemable_amount As Double
  Private m_date_non_redeemable_amount As Double
  Private m_date_promo_amount As Double
  Private m_date_prize_amount As Double

  Private m_date_redeemable_amount_aux As Double
  Private m_date_non_redeemable_amount_aux As Double
  Private m_date_promo_amount_aux As Double
  Private m_date_prize_amount_aux As Double

  Private m_first_time As Boolean

  Private m_is_tito_mode As Boolean

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_EXPIRED_CREDITS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()
    Dim _size_height_gb_type As Integer

    _size_height_gb_type = 0

    Call MyBase.GUI_InitControls()

    ' Set TitoMode if active
    Call SetIsTitoMode()

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(471)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Credit Type Filter
    Me.gb_credit_type.Text = GLB_NLS_GUI_INVOICING.GetString(336)
    Me.chk_type_redeemeable.Text = GLB_NLS_GUI_INVOICING.GetString(473)
    Me.chk_type_redeemeable.Checked = True
    Me.chk_type_price.Text = GLB_NLS_GUI_INVOICING.GetString(158)
    Me.chk_type_non_redeemeable.Text = GLB_NLS_GUI_INVOICING.GetString(474)
    Me.chk_type_non_redeemeable.Checked = True

    If m_is_tito_mode Then
      Me.chk_type_promo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2333)
    Else
      Me.chk_type_promo.Text = GLB_NLS_GUI_INVOICING.GetString(475)
    End If

    Me.chk_type_promo.Checked = False

    ' Expired Credits
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(469)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Changes in case this form runs on Tito mode
    If m_is_tito_mode = True Then
      Me.chk_type_price.Visible = False
      Me.chk_type_promo.Location = Me.chk_type_non_redeemeable.Location
      Me.chk_type_non_redeemeable.Location = Me.chk_type_price.Location
      _size_height_gb_type = Me.chk_type_promo.Location.Y + _
                             Me.chk_type_promo.Size.Height + 5
    End If

    If WSI.Common.Misc.IsCashlessMode Then
      Me.chk_type_promo.Visible = False
      _size_height_gb_type = Me.chk_type_non_redeemeable.Location.Y + _
                             Me.chk_type_non_redeemeable.Size.Height + 5
    End If

    'Adjust height size of the group box
    Me.gb_credit_type.Size = New System.Drawing.Size(Me.gb_credit_type.Size.Width, _size_height_gb_type)

    Me.chk_type_price.Checked = Not m_is_tito_mode
    Me.chk_only_personal_accounts.Visible = m_is_tito_mode

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Show Details
    Me.chk_details.Text = GLB_NLS_GUI_INVOICING.GetString(476)
    Me.chk_only_personal_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4555)

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()
    m_first_time = True
    m_total_redeemable_amount = 0
    m_total_non_redeemable_amount = 0
    m_total_promo_amount = 0
    m_total_prize_amount = 0

    ' Reset subtotals
    ResetSubTotals()
    ResetSubTotalsAux()

    m_current_date = dtp_from.Value

    'if sle: 25-MAY-2010 el "if" provocaba errores al compilar... 

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _show_all As Boolean
    Dim _insert_new_row As Boolean
    Dim _idx_row As Integer

    _show_all = MustShowAllCreditTypes()

    _insert_new_row = False

    ' Insert SubTotals
    Me.Grid.AddRow()

    Call UpdateSubTotalsFromAux()
    Call InsertDateTotal()

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATION_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(205)
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    ' Redeemable
    If Me.chk_type_redeemeable.Checked = True Or _show_all = True Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(473) & ": "
      Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_total_redeemable_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      _insert_new_row = True
      _idx_row += 1
    End If

    ' Prize
    If Not m_is_tito_mode AndAlso (Me.chk_type_price.Checked = True OrElse _show_all = True) Then
      If _insert_new_row Then
        Me.Grid.AddRow()
      End If

      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(158) & ": "
      Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_total_prize_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      _insert_new_row = True
      _idx_row += 1
    End If

    ' Non-Redeemable
    If (Me.chk_type_non_redeemeable.Checked = True OrElse _show_all = True) Then

      If _insert_new_row Then
        Me.Grid.AddRow()
      End If

      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(474) & ": "
      Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_total_non_redeemable_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      _insert_new_row = True
      _idx_row += 1
    End If

    ' Promo
    If (Me.chk_type_promo.Checked = True OrElse _show_all = True) AndAlso _
        m_is_tito_mode = True Then

      If _insert_new_row Then
        Me.Grid.AddRow()
      End If

      ''Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(475) & ": "
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2333) & ": "
      Me.Grid.Cell(_idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_total_promo_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

  End Sub ' GUI_AfterLastRow


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters and based on none titoMode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Function GUI_GetSqlCommandNoneTitoMode() As String
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    If Not chk_details.Checked Then
      _sb.AppendLine("     SELECT   AC_ACCOUNT_ID     ")
      _sb.AppendLine("            , AC_TRACK_DATA     ")
      _sb.AppendLine("            , AC_HOLDER_NAME    ")
      _sb.AppendLine("            , AM_DATETIME       ")
      _sb.AppendLine("            , AM_TYPE           ")
      _sb.AppendLine("            , SUM ( CASE WHEN AM_TYPE = 26 THEN AM_SUB_AMOUNT ELSE 0 END ) AS TOT_RDM                  ")
      _sb.AppendLine("            , SUM ( CASE WHEN AM_TYPE = 27 OR AM_TYPE = 53 THEN AM_SUB_AMOUNT ELSE 0 END ) AS TOT_NRDM ")
      _sb.AppendLine("            , SUM ( CASE WHEN AM_TYPE = 22 THEN AM_SUB_AMOUNT ELSE 0 END ) AS TOT_PROM                 ")
      _sb.AppendLine("            , SUM ( CASE WHEN AM_TYPE = 47 THEN AM_SUB_AMOUNT ELSE 0 END ) AS TOT_PRIZE                ")
      _sb.AppendLine("       FROM   ACCOUNTS          ")
      _sb.AppendLine(" INNER JOIN   ACCOUNT_MOVEMENTS ")
      _sb.AppendLine("         ON   AC_ACCOUNT_ID = AM_ACCOUNT_ID ")

      _sb.AppendLine(GetSqlWhere())

      _sb.AppendLine("  GROUP BY   AC_ACCOUNT_ID    ")
      _sb.AppendLine("           , AC_TRACK_DATA    ")
      _sb.AppendLine("           , AC_HOLDER_NAME   ")
      _sb.AppendLine("           , AM_DATETIME      ")
      _sb.AppendLine("           , AM_TYPE          ")
      _sb.AppendLine("  ORDER BY   AM_DATETIME DESC ")

    Else
      _sb.AppendLine("     SELECT   AC_ACCOUNT_ID     ")
      _sb.AppendLine("            , AC_TRACK_DATA     ")
      _sb.AppendLine("            , AC_HOLDER_NAME    ")
      _sb.AppendLine("            , AM_DATETIME       ")
      _sb.AppendLine("            , AM_SUB_AMOUNT     ")
      _sb.AppendLine("            , AM_TYPE           ")
      _sb.AppendLine("            , -1                ")
      _sb.AppendLine("            , AC_TYPE           ")
      _sb.AppendLine("       FROM   ACCOUNTS          ")
      _sb.AppendLine(" INNER JOIN   ACCOUNT_MOVEMENTS ")
      _sb.AppendLine("         ON   AC_ACCOUNT_ID = AM_ACCOUNT_ID ")

      _sb.AppendLine(GetSqlWhere())

      _sb.AppendLine(" ORDER BY   AM_DATETIME DESC ")

    End If

    Return _sb.ToString()

  End Function


  ' PURPOSE: Build an SQL Command query from conditions set in the filters and based on tito mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Function GUI_GetSqlCommandTitoMode() As String
    Dim _sb As System.Text.StringBuilder
    Dim _cashable As String
    Dim _promo_no_redeem As String
    Dim _promo_redeem As String

    _cashable = Convert.ToInt32(TITO_TICKET_TYPE.CASHABLE)
    _promo_no_redeem = Convert.ToInt32(TITO_TICKET_TYPE.PROMO_NONREDEEM)
    _promo_redeem = Convert.ToInt32(TITO_TICKET_TYPE.PROMO_REDEEM)

    _sb = New System.Text.StringBuilder()

    If Not chk_details.Checked Then
      _sb.AppendLine("      SELECT  TI_VALIDATION_NUMBER    ")
      _sb.AppendLine("            , AC_TRACK_DATA           ")
      _sb.AppendLine("            , AC_HOLDER_NAME          ")
      _sb.AppendLine("            , TI_LAST_ACTION_DATETIME ")
      _sb.AppendLine("            , TI_TYPE_ID              ")
      _sb.AppendLine("            , SUM ( CASE WHEN TI_TYPE_ID = " + _cashable + " THEN TI_AMOUNT ELSE 0 END ) AS TOT_RDM         ")
      _sb.AppendLine("            , SUM ( CASE WHEN TI_TYPE_ID = " + _promo_no_redeem + " THEN TI_AMOUNT ELSE 0 END ) AS TOT_NRDM ")
      _sb.AppendLine("            , SUM ( CASE WHEN TI_TYPE_ID = " + _promo_redeem + " THEN TI_AMOUNT ELSE 0 END ) AS TOT_PROM    ")
      _sb.AppendLine("            , SUM ( TI_AMOUNT ) AS TOT_PRIZE                                                                ")
      _sb.AppendLine("       FROM   ACCOUNTS                ")
      _sb.AppendLine(" INNER JOIN   TICKETS                 ")
      _sb.AppendLine("         ON   AC_ACCOUNT_ID = TI_CREATED_ACCOUNT_ID ")

      _sb.AppendLine(GetSqlWhereTitoMode())

      _sb.AppendLine(" GROUP BY   TI_VALIDATION_NUMBER         ")
      _sb.AppendLine("          , AC_TRACK_DATA                ")
      _sb.AppendLine("          , AC_HOLDER_NAME               ")
      _sb.AppendLine("          , TI_LAST_ACTION_DATETIME      ")
      _sb.AppendLine("          , TI_TYPE_ID                   ")
      _sb.AppendLine("          , AC_TYPE                      ")
      _sb.AppendLine(" ORDER BY   TI_LAST_ACTION_DATETIME DESC ")

    Else
      _sb.AppendLine("      SELECT  TI_VALIDATION_NUMBER    ")
      _sb.AppendLine("            , AC_TRACK_DATA           ")
      _sb.AppendLine("            , AC_HOLDER_NAME          ")
      _sb.AppendLine("            , TI_LAST_ACTION_DATETIME ")
      _sb.AppendLine("            , TI_AMOUNT               ")
      _sb.AppendLine("            , TI_TYPE_ID              ")
      _sb.AppendLine("            , TI_VALIDATION_TYPE      ")
      _sb.AppendLine("            , AC_TYPE                 ")
      _sb.AppendLine("       FROM   ACCOUNTS                ")
      _sb.AppendLine(" INNER JOIN   TICKETS                 ")
      _sb.AppendLine("         ON   AC_ACCOUNT_ID = TI_CREATED_ACCOUNT_ID ")

      _sb.AppendLine(GetSqlWhereTitoMode())

      _sb.AppendLine(" ORDER BY   TI_LAST_ACTION_DATETIME DESC ")

    End If

    Return _sb.ToString()

  End Function

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _sql_str As String

    If (m_is_tito_mode = False) Then
      _sql_str = GUI_GetSqlCommandNoneTitoMode()

    Else
      _sql_str = GUI_GetSqlCommandTitoMode()

    End If

    _sql_command = New SqlCommand(_sql_str)

    Return _sql_command

  End Function ' GUI_GetSqlCommand

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)

  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If DbRow.IsNull(SQL_COLUMN_CARD_EXPIRATION_DATE) Then

      Return False
    End If

    If m_first_time Then
      ' Process first row 
      m_current_date = DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE)
      m_first_time = False
    End If

    UpdateSubTotalsFromAux()
    ResetSubTotalsAux()

    '' Sub totals
    If m_is_tito_mode = False Then
      CalculateTotalsNoneTitoMode(DbRow)
    Else
      CalculateTotalsTitoMode(DbRow)
    End If

    If GUI_FormatDate(m_current_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) _
        <> GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) Or chk_details.Checked Then
      Return True
    End If

    Return False

  End Function ' GUI_CheckOutRowBeforeAdd

  Private Function CalculateTotalsNoneTitoMode(ByVal DbRow As CLASS_DB_ROW) As Boolean
    ' Sub totals
    If Not chk_details.Checked Then

      AcumulateSubTotalsNoneTitoMode(MovementType.CreditsExpired, DbRow.Value(SQL_COLUMN_BALANCE_EXPIRATION_CREDITS))
      AcumulateSubTotalsNoneTitoMode(MovementType.CreditsNotRedeemableExpired, DbRow.Value(SQL_COLUMN_BALANCE_NON_REDEEMABLE_CREDITS))
      AcumulateSubTotalsNoneTitoMode(MovementType.PromoExpired, DbRow.Value(SQL_COLUMN_BALANCE_PROMO_CREDITS))
      AcumulateSubTotalsNoneTitoMode(MovementType.PrizeExpired, DbRow.Value(SQL_COLUMN_BALANCE_PRIZE_CREDITS))

      AcumulateTotalsNoneTitoMode(MovementType.CreditsExpired, DbRow.Value(SQL_COLUMN_BALANCE_EXPIRATION_CREDITS))
      AcumulateTotalsNoneTitoMode(MovementType.CreditsNotRedeemableExpired, DbRow.Value(SQL_COLUMN_BALANCE_NON_REDEEMABLE_CREDITS))
      AcumulateTotalsNoneTitoMode(MovementType.PromoExpired, DbRow.Value(SQL_COLUMN_BALANCE_PROMO_CREDITS))
      AcumulateTotalsNoneTitoMode(MovementType.PrizeExpired, DbRow.Value(SQL_COLUMN_BALANCE_PRIZE_CREDITS))
    Else

      AcumulateSubTotalsNoneTitoMode(CInt(DbRow.Value(SQL_COLUMN_DETAIL_TYPE)), DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT))
      AcumulateTotalsNoneTitoMode(CInt(DbRow.Value(SQL_COLUMN_DETAIL_TYPE)), DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT))

    End If

  End Function

  Private Function CalculateTotalsTitoMode(ByVal DbRow As CLASS_DB_ROW) As Boolean
    If Not chk_details.Checked Then

      AcumulateSubTotalsTitoMode(TITO_TICKET_TYPE.CASHABLE, DbRow.Value(SQL_COLUMN_BALANCE_EXPIRATION_CREDITS))
      AcumulateSubTotalsTitoMode(TITO_TICKET_TYPE.PROMO_NONREDEEM, DbRow.Value(SQL_COLUMN_BALANCE_NON_REDEEMABLE_CREDITS))
      AcumulateSubTotalsTitoMode(TITO_TICKET_TYPE.PROMO_REDEEM, DbRow.Value(SQL_COLUMN_BALANCE_PROMO_CREDITS))

      AcumulateTotalsTitoMode(TITO_TICKET_TYPE.CASHABLE, DbRow.Value(SQL_COLUMN_BALANCE_EXPIRATION_CREDITS))
      AcumulateTotalsTitoMode(TITO_TICKET_TYPE.PROMO_NONREDEEM, DbRow.Value(SQL_COLUMN_BALANCE_NON_REDEEMABLE_CREDITS))
      AcumulateTotalsTitoMode(TITO_TICKET_TYPE.PROMO_REDEEM, DbRow.Value(SQL_COLUMN_BALANCE_PROMO_CREDITS))
    Else

      AcumulateSubTotalsTitoMode(CInt(DbRow.Value(SQL_COLUMN_DETAIL_TYPE)), DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT))
      AcumulateTotalsTitoMode(CInt(DbRow.Value(SQL_COLUMN_DETAIL_TYPE)), DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT))

    End If
  End Function


  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _track_data As String
    Dim _add_row As Boolean
    Dim _current_row As Int32
    Dim _show_all As Boolean
    Dim _validation_type As Int32

    _add_row = False

    If GUI_FormatDate(m_current_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) _
    <> GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) Then

      InsertDateTotal()
      ResetSubTotals()

      _show_all = MustShowAllCreditTypes()

      m_current_date = DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE)

      _current_row = 0
      If Me.chk_type_redeemeable.Checked = True Or _show_all Then
        _current_row += 1
      End If
      If Not m_is_tito_mode AndAlso (Me.chk_type_price.Checked = True OrElse _show_all) Then
        _current_row += 1
      End If
      If Me.chk_type_non_redeemeable.Checked = True Or _show_all Then
        _current_row += 1
      End If
      If Me.chk_type_promo.Checked = True Or _show_all Then
        _current_row += 1
      End If

      ' Offest the number of extra rows added
      RowIndex += _current_row
      _add_row = True

    End If

    If chk_details.Checked Then

      If _add_row Then
        Me.Grid.AddRow()
        _add_row = False
      End If

      ' Card Account
      If Not m_is_tito_mode Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)
      Else
        _validation_type = DbRow.Value(SQL_COLUMN_TICKET_VALIDATION_TYPE)

        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT).Value = _
                        WSI.Common.TITO.ValidationNumberManager.FormatValidationNumber(DbRow.Value(SQL_COLUMN_CARD_ACCOUNT), _
                        _validation_type, False)
      End If

      ' Card Track Data
      _track_data = ""
      If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
        CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                        CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TRACK_DATA).Value = _track_data
      End If

      ' Holder Name
      If Not DbRow.IsNull(SQL_COLUMN_CARD_HOLDER) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER).Value = DbRow.Value(SQL_COLUMN_CARD_HOLDER)
      End If

      ' Expiration Date
      If Not DbRow.IsNull(SQL_COLUMN_CARD_EXPIRATION_DATE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CARD_EXPIRATION_DATE), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                  ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Credits Type
      If Not DbRow.IsNull(SQL_COLUMN_DETAIL_TYPE) Then
        If m_is_tito_mode = False Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDITS_TYPE).Value = GetStringCode(CInt(DbRow.Value(SQL_COLUMN_DETAIL_TYPE)))
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDITS_TYPE).Value = GetTitoTicketType(CInt(DbRow.Value(SQL_COLUMN_DETAIL_TYPE)))
        End If

        ' TODO: Convert TYPE value to readable format
      End If


      ' Expired Credits
      If Not DbRow.IsNull(SQL_COLUMN_DETAIL_AMOUNT) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DETAIL_AMOUNT), _
                                                                                          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

    End If


    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_account_sel1

  End Sub ' GUI_SetInitialFocus


  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Int32
    Dim account_number As Int64
    Dim frm_sel As frm_account_movements

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      Return
    End If

    ' Get the complete account number and launch select form 
    If Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT).Value = "" Then
      account_number = 0
    Else
      account_number = Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm_sel = New frm_account_movements

    frm_sel.ShowForEdit(Me.MdiParent, account_number)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditSelectedItem

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(chk_details.Text, m_show_detail)
    If m_is_tito_mode Then
      PrintData.SetFilter(chk_only_personal_accounts.Text, m_only_personal_accounts)
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.SetFilter(Me.gb_credit_type.Text, m_credit_types)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(469)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _all_credit_types As Boolean

    m_track_data = ""
    m_account = ""
    m_state = ""
    m_balance = ""
    m_holder = ""
    m_holder_is_vip = ""
    m_date_from = ""
    m_date_to = ""
    m_show_detail = ""
    m_only_personal_accounts = ""
    m_credit_types = ""

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If


    ' Card Account number
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Show account detail and only personal accounts  | 479 "Yes"  | 480 "No"
    m_show_detail = IIf(chk_details.Checked, GLB_NLS_GUI_INVOICING.GetString(479), GLB_NLS_GUI_INVOICING.GetString(480))
    m_only_personal_accounts = IIf(chk_only_personal_accounts.Checked, GLB_NLS_GUI_INVOICING.GetString(479), GLB_NLS_GUI_INVOICING.GetString(480))

    ' Credit Types
    _all_credit_types = MustShowAllCreditTypes()

    If Me.chk_type_redeemeable.Checked Or _all_credit_types = True Then
      If Not String.IsNullOrEmpty(m_credit_types) Then
        m_credit_types = m_credit_types & ", "
      End If
      m_credit_types = m_credit_types & GLB_NLS_GUI_INVOICING.GetString(473)
    End If
    If Me.chk_type_price.Checked Or _all_credit_types = True Then
      If Not String.IsNullOrEmpty(m_credit_types) Then
        m_credit_types = m_credit_types & ", "
      End If
      m_credit_types = m_credit_types & GLB_NLS_GUI_INVOICING.GetString(158)
    End If
    If Me.chk_type_non_redeemeable.Checked Or _all_credit_types = True Then
      If Not String.IsNullOrEmpty(m_credit_types) Then
        m_credit_types = m_credit_types & ", "
      End If
      m_credit_types = m_credit_types & GLB_NLS_GUI_INVOICING.GetString(474)
    End If
    If Me.chk_type_promo.Checked Or _all_credit_types = True Then
      If Not String.IsNullOrEmpty(m_credit_types) Then
        m_credit_types = m_credit_types & ", "
      End If
      m_credit_types = m_credit_types & GLB_NLS_GUI_INVOICING.GetString(475)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Account - Number
      '  Account
      .Column(GRID_COLUMN_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)

      ' Number
      .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT).Width = GRID_WIDTH_ACCOUNT_NONE_TITO

      ' Account - Card
      ' Account
      .Column(GRID_COLUMN_TRACK_DATA).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Card
      .Column(GRID_COLUMN_TRACK_DATA).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_TRACK_DATA).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_TRACK_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Holder Name
      ' Account
      .Column(GRID_COLUMN_HOLDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      ' Holder Name
      .Column(GRID_COLUMN_HOLDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Account - Expiration Date
      ' Account
      .Column(GRID_COLUMN_EXPIRATION_DATE).Header(0).Text = ""
      ' Expiration Date
      .Column(GRID_COLUMN_EXPIRATION_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(471)
      .Column(GRID_COLUMN_EXPIRATION_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_EXPIRATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Balance - Type
      ' Balance
      .Column(GRID_COLUMN_CREDITS_TYPE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' Type 
      .Column(GRID_COLUMN_CREDITS_TYPE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(472)
      .Column(GRID_COLUMN_CREDITS_TYPE).Width = GRID_WIDTH_CREDITS_TYPE
      .Column(GRID_COLUMN_CREDITS_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Balance - Expired Credits
      ' Balance
      .Column(GRID_COLUMN_EXPIRATION_CREDITS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(240)
      ' Type 
      .Column(GRID_COLUMN_EXPIRATION_CREDITS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(469)
      .Column(GRID_COLUMN_EXPIRATION_CREDITS).Width = GRID_WIDTH_AMOUNT_LONG
      .Column(GRID_COLUMN_EXPIRATION_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' If the system is running on Tito mode then change the look of some columns here
      If m_is_tito_mode = True Then
        ' Account Number -> ValidationNumber
        .Column(GRID_COLUMN_ACCOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2920) ' "ticket number "
        .Column(GRID_COLUMN_ACCOUNT).Width = GRID_WIDTH_ACCOUNT_IS_TITO
        .Column(GRID_COLUMN_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End If
    End With

  End Sub ' GUI_StyleSheet

  Private Sub InsertDateTotal()

    Dim idx_row As Integer = Me.Grid.NumRows - 1
    Dim _insert_new_row As Boolean
    Dim _show_all As Boolean

    _insert_new_row = False

    _show_all = MustShowAllCreditTypes()

    ' Label - TOTAL:
    Me.Grid.Cell(idx_row, GRID_COLUMN_EXPIRATION_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(205) & _
    GUI_FormatDate(m_current_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    ' Redeemable
    If Me.chk_type_redeemeable.Checked = True Or _show_all = True Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(473) & ": "
      Me.Grid.Cell(idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_date_redeemable_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      _insert_new_row = True
      idx_row += 1
    End If

    ' Prize
    If Not m_is_tito_mode AndAlso (Me.chk_type_price.Checked = True OrElse _show_all = True) Then
      If _insert_new_row Then
        Me.Grid.AddRow()
      End If

      Me.Grid.Cell(idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(158) & ": "
      Me.Grid.Cell(idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_date_prize_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      _insert_new_row = True
      idx_row += 1
    End If

    ' Non-Redeemable
    If Me.chk_type_non_redeemeable.Checked = True Or _show_all = True Then
      If _insert_new_row Then
        Me.Grid.AddRow()
      End If

      Me.Grid.Cell(idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(474) & ": "
      Me.Grid.Cell(idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_date_non_redeemable_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      _insert_new_row = True
      idx_row += 1
    End If

    ' Promo
    If (Me.chk_type_promo.Checked = True OrElse _show_all = True) AndAlso _
        Me.m_is_tito_mode Then

      If _insert_new_row Then
        Me.Grid.AddRow()
      End If

      ''Me.Grid.Cell(idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(475) & ": "
      Me.Grid.Cell(idx_row, GRID_COLUMN_CREDITS_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2333) & ": "
      Me.Grid.Cell(idx_row, GRID_COLUMN_EXPIRATION_CREDITS).Value = GUI_FormatCurrency(m_date_promo_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    End If

  End Sub

  Private Sub AcumulateTotalsNoneTitoMode(ByVal Type As MovementType, ByVal Amount As Double)

    Select Case Type
      Case MovementType.PromoExpired
        m_total_promo_amount += Amount

      Case MovementType.CreditsExpired
        m_total_redeemable_amount += Amount

      Case MovementType.CreditsNotRedeemableExpired
        m_total_non_redeemable_amount += Amount

      Case MovementType.PrizeExpired
        m_total_prize_amount += Amount

      Case MovementType.CreditsNotRedeemable2Expired
        m_total_non_redeemable_amount += Amount

    End Select
  End Sub

  Private Sub AcumulateSubTotalsNoneTitoMode(ByVal Type As MovementType, ByVal Amount As Double)

    Select Case Type
      Case MovementType.PromoExpired
        m_date_promo_amount_aux = Amount

      Case MovementType.CreditsExpired
        m_date_redeemable_amount_aux = Amount

      Case MovementType.CreditsNotRedeemableExpired
        m_date_non_redeemable_amount_aux = Amount

      Case MovementType.PrizeExpired
        m_date_prize_amount_aux = Amount

      Case MovementType.CreditsNotRedeemable2Expired
        m_date_non_redeemable_amount_aux = Amount

    End Select
  End Sub

  Private Sub AcumulateTotalsTitoMode(ByVal Type As TITO_TICKET_TYPE, ByVal Amount As Double)

    Select Case Type
      Case TITO_TICKET_TYPE.PROMO_REDEEM
        m_total_promo_amount += Amount

      Case TITO_TICKET_TYPE.CASHABLE
        m_total_redeemable_amount += Amount

      Case TITO_TICKET_TYPE.PROMO_NONREDEEM
        m_total_non_redeemable_amount += Amount

    End Select
  End Sub

  Private Sub AcumulateSubTotalsTitoMode(ByVal Type As TITO_TICKET_TYPE, ByVal Amount As Double)

    Select Case Type
      Case TITO_TICKET_TYPE.CASHABLE
        m_date_redeemable_amount_aux = Amount

      Case TITO_TICKET_TYPE.PROMO_REDEEM
        m_date_promo_amount_aux = Amount

      Case TITO_TICKET_TYPE.PROMO_NONREDEEM
        m_date_non_redeemable_amount_aux = Amount
    End Select
  End Sub

  Private Sub ResetSubTotals()

    m_date_promo_amount = 0
    m_date_redeemable_amount = 0
    m_date_non_redeemable_amount = 0
    m_date_prize_amount = 0

  End Sub

  Private Sub ResetSubTotalsAux()

    m_date_promo_amount_aux = 0
    m_date_redeemable_amount_aux = 0
    m_date_non_redeemable_amount_aux = 0
    m_date_prize_amount_aux = 0

  End Sub

  Private Sub UpdateSubTotalsFromAux()
    m_date_promo_amount += m_date_promo_amount_aux

    m_date_redeemable_amount += m_date_redeemable_amount_aux
    m_date_non_redeemable_amount += m_date_non_redeemable_amount_aux
    m_date_prize_amount += m_date_prize_amount_aux
  End Sub

  ' PURPOSE: Set member variable m_is_tito_mode to determine overall form functionallity
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS: 
  '     - None
  Private Sub SetIsTitoMode()

    m_is_tito_mode = TITO.Utils.IsTitoMode()
    uc_account_sel1.InitExtendedQuery(m_is_tito_mode)

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Me.chk_details.Checked = False
    Me.chk_only_personal_accounts.Checked = False

    Me.chk_type_redeemeable.Checked = True
    Me.chk_type_price.Checked = Not m_is_tito_mode
    Me.chk_type_non_redeemeable.Checked = True
    Me.chk_type_promo.Checked = m_is_tito_mode

    Me.uc_account_sel1.Clear()

  End Sub ' SetDefaultValues

  Private Function MustShowAllCreditTypes() As Boolean
    Dim _result As Boolean
    _result = False

    If Me.chk_type_redeemeable.Checked = False _
         And (m_is_tito_mode Or Me.chk_type_price.Checked = False) _
         And Me.chk_type_non_redeemeable.Checked = False _
         And Me.chk_type_promo.Checked = False Then
      _result = True
    End If

    Return _result

  End Function

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String

    Dim _sb_where As System.Text.StringBuilder
    Dim _ls_types As List(Of String)
    Dim _str_where_account As String = ""

    _sb_where = New System.Text.StringBuilder()

    _sb_where.AppendLine(" WHERE   AC_TYPE = " & AccountType.ACCOUNT_WIN)
    _sb_where.AppendLine("         AND AM_TYPE IN (#CREDIT_TYPES_LIST#) ")

    ' Account sel
    _str_where_account = Me.uc_account_sel1.GetFilterSQL()
    If _str_where_account <> "" Then
      _sb_where.AppendLine("         AND " & _str_where_account)
    End If

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      _sb_where.AppendLine("         AND (AM_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
    End If

    If Me.dtp_to.Checked = True Then
      _sb_where.AppendLine("         AND (AM_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
    End If

    ' Set credit type filter
    _ls_types = New List(Of String)

    If Me.chk_type_redeemeable.Checked = True Then
      _ls_types.Add(Me.MovToStr(MovementType.CreditsExpired))
    End If

    If Me.chk_type_price.Checked = True Then
      _ls_types.Add(Me.MovToStr(MovementType.PrizeExpired))
    End If

    If Me.chk_type_non_redeemeable.Checked = True Then
      _ls_types.Add(Me.MovToStr(MovementType.CreditsNotRedeemableExpired))
      _ls_types.Add(Me.MovToStr(MovementType.CreditsNotRedeemable2Expired))
    End If

    If Me.chk_type_promo.Checked = True Then
      _ls_types.Add(Me.MovToStr(MovementType.PromoExpired))
    End If

    ' If no check selected: show all
    If _ls_types.Count = 0 Then
      _ls_types.Add(Me.MovToStr(MovementType.CreditsExpired))
      _ls_types.Add(Me.MovToStr(MovementType.PrizeExpired))
      _ls_types.Add(Me.MovToStr(MovementType.CreditsNotRedeemableExpired))
      _ls_types.Add(Me.MovToStr(MovementType.CreditsNotRedeemable2Expired))
      _ls_types.Add(Me.MovToStr(MovementType.PromoExpired))
    End If

    _sb_where.Replace("#CREDIT_TYPES_LIST#", String.Join(",", _ls_types.ToArray()))

    Return _sb_where.ToString()
  End Function ' GetSqlWhere
  ' PURPOSE: Build the variable part of the WHERE clause in TITO mode (the one that depends on filter values) for the main SQL Query TITO mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhereTitoMode() As String

    Dim _sb_where As StringBuilder
    Dim _ls_types As List(Of String)
    Dim _str_where_account As String = ""

    _sb_where = New StringBuilder()

    _sb_where.AppendLine(" WHERE ")
    If chk_only_personal_accounts.Checked Then
      _sb_where.AppendLine("       AC_TYPE = " & AccountType.ACCOUNT_WIN)
    Else
      _sb_where.AppendLine("       AC_TYPE IN(" & AccountType.ACCOUNT_WIN & ", " & AccountType.ACCOUNT_VIRTUAL_CASHIER & ", " & AccountType.ACCOUNT_VIRTUAL_TERMINAL & ")")
    End If

    _sb_where.AppendLine("   AND   TI_TYPE_ID IN (#CREDIT_TYPES_LIST#)   ")
    _sb_where.AppendLine("   AND   TI_STATUS =                           " & TITO_TICKET_STATUS.EXPIRED)

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      _sb_where.AppendLine(" AND (TI_LAST_ACTION_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
    End If

    If Me.dtp_to.Checked = True Then
      _sb_where.AppendLine(" AND (TI_LAST_ACTION_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
    End If

    ' Account sel
    _str_where_account = Me.uc_account_sel1.GetFilterSQL()
    If _str_where_account <> "" Then
      _sb_where.AppendLine("         AND " & _str_where_account)
    End If

    ' Set credit type filter
    _ls_types = New List(Of String)

    If Me.chk_type_redeemeable.Checked = True Then
      _ls_types.Add(Me.MovToStr(TITO_TICKET_TYPE.CASHABLE))
    End If

    If Me.chk_type_promo.Checked = True Then
      _ls_types.Add(Me.MovToStr(TITO_TICKET_TYPE.PROMO_REDEEM))
    End If

    If Me.chk_type_non_redeemeable.Checked = True Then
      _ls_types.Add(Me.MovToStr(TITO_TICKET_TYPE.PROMO_NONREDEEM))
    End If

    ' If no check selected: show all
    If _ls_types.Count = 0 Then
      _ls_types.Add(Me.MovToStr(TITO_TICKET_TYPE.CASHABLE))
      _ls_types.Add(Me.MovToStr(TITO_TICKET_TYPE.PROMO_REDEEM))
      _ls_types.Add(Me.MovToStr(TITO_TICKET_TYPE.PROMO_NONREDEEM))
    End If

    _sb_where.Replace("#CREDIT_TYPES_LIST#", String.Join(",", _ls_types.ToArray()))

    Return _sb_where.ToString()
  End Function ' GetSqlWhere
  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    If IdxRow < 0 Or IdxRow >= (Me.Grid.NumRows - 1) Then
      Return False
    Else
      Return True
    End If

  End Function ' SetDefaultValues

  Private Function GetTitoTicketType(ByVal Code As TITO_TICKET_TYPE) As String
    Select Case Code
      Case TITO_TICKET_TYPE.CASHABLE
        Return GLB_NLS_GUI_INVOICING.GetString(473)

      Case TITO_TICKET_TYPE.PROMO_REDEEM
        ''Return GLB_NLS_GUI_INVOICING.GetString(475)
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2333)

      Case TITO_TICKET_TYPE.PROMO_NONREDEEM
        Return GLB_NLS_GUI_INVOICING.GetString(474)



      Case Else
        Return CStr(Code)
    End Select

  End Function

  Private Function GetStringCode(ByVal Code As MovementType) As String
    Select Case Code
      Case MovementType.PromoExpired
        Return GLB_NLS_GUI_INVOICING.GetString(475)

      Case MovementType.CreditsExpired
        Return GLB_NLS_GUI_INVOICING.GetString(473)

      Case MovementType.CreditsNotRedeemableExpired
        Return GLB_NLS_GUI_INVOICING.GetString(474)

      Case MovementType.PrizeExpired
        Return GLB_NLS_GUI_INVOICING.GetString(158)

      Case MovementType.CreditsNotRedeemable2Expired
        Return GLB_NLS_GUI_INVOICING.GetString(474)

      Case Else
        Return CStr(Code)
    End Select

  End Function

  ' PURPOSE : Converts a MovementType to its id
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovementType
  '
  '     - OUTPUT :
  '
  ' RETURNS : The id of the mOvement converted to Sring

  Private Function MovToStr(ByVal MovementType As MovementType) As String

    Return CInt(MovementType).ToString()

  End Function

  ' PURPOSE : Converts a TITO_TICKET_TYPE to its id
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovementType
  '
  '     - OUTPUT :
  '
  ' RETURNS : The id of the mOvement converted to Sring

  Private Function MovToStr(ByVal TitoTicketType As TITO_TICKET_TYPE) As String

    Return CInt(TitoTicketType).ToString()

  End Function

#End Region ' Private Functions

#Region " Events "

#End Region ' Events

End Class


