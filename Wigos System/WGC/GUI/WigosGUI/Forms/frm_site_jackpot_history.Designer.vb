<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_site_jackpot_history
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_jackpot_type = New System.Windows.Forms.GroupBox
    Me.chk_jackpot3 = New System.Windows.Forms.CheckBox
    Me.chk_jackpot2 = New System.Windows.Forms.CheckBox
    Me.chk_jackpot1 = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.uc_sites_sel = New GUI_Controls.uc_sites_sel
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.chk_level_totals = New System.Windows.Forms.CheckBox
    Me.chk_exclude_inactivity = New System.Windows.Forms.CheckBox
    Me.gb_options = New System.Windows.Forms.GroupBox
    Me.uc_multi_currency_sel = New GUI_Controls.uc_multi_currency_sel
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_jackpot_type.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multi_currency_sel)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.uc_sites_sel)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.gb_jackpot_type)
    Me.panel_filter.Size = New System.Drawing.Size(1242, 137)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_jackpot_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_sites_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multi_currency_sel, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 141)
    Me.panel_data.Size = New System.Drawing.Size(1242, 382)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1236, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1236, 4)
    '
    'gb_jackpot_type
    '
    Me.gb_jackpot_type.Controls.Add(Me.chk_jackpot3)
    Me.gb_jackpot_type.Controls.Add(Me.chk_jackpot2)
    Me.gb_jackpot_type.Controls.Add(Me.chk_jackpot1)
    Me.gb_jackpot_type.Location = New System.Drawing.Point(274, 8)
    Me.gb_jackpot_type.Name = "gb_jackpot_type"
    Me.gb_jackpot_type.Size = New System.Drawing.Size(167, 101)
    Me.gb_jackpot_type.TabIndex = 3
    Me.gb_jackpot_type.TabStop = False
    Me.gb_jackpot_type.Text = "xJackpot Type"
    '
    'chk_jackpot3
    '
    Me.chk_jackpot3.AutoSize = True
    Me.chk_jackpot3.Location = New System.Drawing.Point(18, 72)
    Me.chk_jackpot3.Name = "chk_jackpot3"
    Me.chk_jackpot3.Size = New System.Drawing.Size(83, 17)
    Me.chk_jackpot3.TabIndex = 2
    Me.chk_jackpot3.Text = "xJackpot3"
    Me.chk_jackpot3.UseVisualStyleBackColor = True
    '
    'chk_jackpot2
    '
    Me.chk_jackpot2.AutoSize = True
    Me.chk_jackpot2.Location = New System.Drawing.Point(18, 46)
    Me.chk_jackpot2.Name = "chk_jackpot2"
    Me.chk_jackpot2.Size = New System.Drawing.Size(83, 17)
    Me.chk_jackpot2.TabIndex = 1
    Me.chk_jackpot2.Text = "xJackpot2"
    Me.chk_jackpot2.UseVisualStyleBackColor = True
    '
    'chk_jackpot1
    '
    Me.chk_jackpot1.AutoSize = True
    Me.chk_jackpot1.Location = New System.Drawing.Point(18, 21)
    Me.chk_jackpot1.Name = "chk_jackpot1"
    Me.chk_jackpot1.Size = New System.Drawing.Size(83, 17)
    Me.chk_jackpot1.TabIndex = 0
    Me.chk_jackpot1.Text = "xJackpot1"
    Me.chk_jackpot1.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 2
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_sites_sel
    '
    Me.uc_sites_sel.Location = New System.Drawing.Point(670, 8)
    Me.uc_sites_sel.MultiSelect = True
    Me.uc_sites_sel.Name = "uc_sites_sel"
    Me.uc_sites_sel.ShowMultisiteRow = True
    Me.uc_sites_sel.Size = New System.Drawing.Size(289, 166)
    Me.uc_sites_sel.TabIndex = 0
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(6, 70)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(197, 19)
    Me.chk_terminal_location.TabIndex = 2
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'chk_level_totals
    '
    Me.chk_level_totals.Location = New System.Drawing.Point(6, 20)
    Me.chk_level_totals.Name = "chk_level_totals"
    Me.chk_level_totals.Size = New System.Drawing.Size(197, 19)
    Me.chk_level_totals.TabIndex = 0
    Me.chk_level_totals.Text = "xShowDetail"
    '
    'chk_exclude_inactivity
    '
    Me.chk_exclude_inactivity.Location = New System.Drawing.Point(6, 45)
    Me.chk_exclude_inactivity.Name = "chk_exclude_inactivity"
    Me.chk_exclude_inactivity.Size = New System.Drawing.Size(197, 19)
    Me.chk_exclude_inactivity.TabIndex = 1
    Me.chk_exclude_inactivity.Text = "xExclude inactivity"
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.chk_level_totals)
    Me.gb_options.Controls.Add(Me.chk_terminal_location)
    Me.gb_options.Controls.Add(Me.chk_exclude_inactivity)
    Me.gb_options.Location = New System.Drawing.Point(451, 8)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(213, 101)
    Me.gb_options.TabIndex = 4
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'uc_multi_currency_sel
    '
    Me.uc_multi_currency_sel.BackColor = System.Drawing.SystemColors.Control
    Me.uc_multi_currency_sel.CurrenciesTable = Nothing
    Me.uc_multi_currency_sel.DateFrom = New Date(CType(0, Long))
    Me.uc_multi_currency_sel.DateTo = New Date(CType(0, Long))
    Me.uc_multi_currency_sel.HasSitesWithoutCurrency = False
    Me.uc_multi_currency_sel.IsGridApplyChanges = False
    Me.uc_multi_currency_sel.Location = New System.Drawing.Point(965, 8)
    Me.uc_multi_currency_sel.MultiSelect = True
    Me.uc_multi_currency_sel.MultiSiteCurrency = Nothing
    Me.uc_multi_currency_sel.Name = "uc_multi_currency_sel"
    Me.uc_multi_currency_sel.ShowMultisiteRow = True
    Me.uc_multi_currency_sel.Size = New System.Drawing.Size(170, 123)
    Me.uc_multi_currency_sel.TabIndex = 1
    '
    'frm_site_jackpot_history
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1250, 527)
    Me.Name = "frm_site_jackpot_history"
    Me.Text = "frm_jackpot_history"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_jackpot_type.ResumeLayout(False)
    Me.gb_jackpot_type.PerformLayout()
    Me.gb_options.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_jackpot_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_jackpot3 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot2 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot1 As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_sites_sel As GUI_Controls.uc_sites_sel
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_totals As System.Windows.Forms.CheckBox
  Friend WithEvents chk_exclude_inactivity As System.Windows.Forms.CheckBox
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents uc_multi_currency_sel As GUI_Controls.uc_multi_currency_sel
End Class
