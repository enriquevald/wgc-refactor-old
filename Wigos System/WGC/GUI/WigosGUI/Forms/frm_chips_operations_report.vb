'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_chips_operations_report
' DESCRIPTION:   Gaming tables chips sales and purchases report
' AUTHOR:        Ram�n Moncl�s
' CREATION DATE: 09-JAN-2014
'
' REVISION HISTORY:
'
' Date         Author   Description
' -----------  ------   -----------------------------------------------
' 09-JAN-2014  RMS      First Release
' 14-FEB-2014  RMS      Show message when no data found
' 27-MAR-2014  DLL      Fixed Bug WIG-772: Wrong Excel filters
' 12-MAY-2014  DLL      Fixed Bug WIG-914: Exception error when close frm
' 22-MAY-2014  SMN      Modify some NLS, change order of a columns, change default value of checkbox 
' 20-NOV-2014  SGB      Fixed Bug WIG-1714: Error show subtotal in datagrid 
' 12-MAY-2015  DHA      Fixed Bug WIG-2323: Error show subtotal in datagrid field 'Difference'
' 15-OCT-2015  CPC      Rename column 'Difference' to 'Win Cajas'
' 15-OCT-2015  CPC      Add column Table name 
' 15-OCT-2015  CPC      Move column "Cahier" to first place
' 15-OCT-2015  CPC      Add column 'Delta'
' 18-NOV-2015  CPC      Rename columns Ventas -> Ventas (A), Compras -> Compras (B), Win Cajas -> A-B
' 18-NOV-2015  CPC      Hide column Delta
' 08-JUL-2016  RAB      GamingTables - MultiCurrency (Phase 4): Adapt them reports of table to MultiCurrency
' 12-JUL-2017  JML      PBI 28424:WIGOS-2957 Rounding - Reports - Add Remaining amount in two reports
' 25-JUL-2017  RAB      PBI 28424:WIGOS-2957 Rounding - Reports - Add Remaining amount in two reports
' 19-JUN-2018  FJC      WIGOS-13041 When filtering from a date to a previous date in 'Chip purchase/sale report', there is no warning about that. In all other forms there is a warning
' 27-JUN-2018  DLM      Bug 33364: WIGOS 13030 : Form 'Reporte de compra/venta de fichas' does not show the gambling tables name, it shows the cashier's name repeated.
' 27-JUN-2018  DLM      Bug 33366: WIGOS 13035 : Filters by table type and location on 'Reporte de compra/venta de fichas' does not filter.
'----------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient

#End Region

Public Class frm_chips_operations_report
  Inherits frm_base_sel

#Region " Constants "

  ' SQL Columnds
  Private Const SQL_COLUMN_TYPE As Integer = 0
  Private Const SQL_COLUMN_SESSION_ID As Integer = 1
  Private Const SQL_COLUMN_GAMING_TABLE_NAME As Integer = 2
  Private Const SQL_COLUMN_GAMING_TABLE_DATE As Integer = 3
  Private Const SQL_COLUMN_GAMING_TABLE_TYPE As Integer = 4
  Private Const SQL_COLUMN_SALES_TOTAL As Integer = 5
  Private Const SQL_COLUMN_PURCHASES_TOTAL As Integer = 6
  Private Const SQL_COLUMN_DELTA As Integer = 7
  Private Const SQL_COLUMN_REMAINING_TOTAL As Integer = 8
  Private Const SQL_COLUMN_CURRENCY_ISO_CODE As Integer = 9
  Private Const SQL_COLUMN_SESSION_NAME As Integer = 10
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 11  'DLM Bug 33364: WIGOS 13030

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CASHIER As Integer = 1
  Private Const GRID_COLUMN_TYPE As Integer = 2
  Private Const GRID_COLUMN_SESSION_ID As Integer = 3
  Private Const GRID_COLUMN_GAMING_TABLE_TYPE As Integer = 4
  Private Const GRID_COLUMN_GAMING_TABLE_NAME As Integer = 5
  Private Const GRID_COLUMN_GAMING_TABLE_DATE As Integer = 6
  Private Const GRID_COLUMN_SALES_TOTAL As Integer = 7
  Private Const GRID_COLUMN_PURCHASES_TOTAL As Integer = 8
  Private Const GRID_COLUMN_WINCAJAS As Integer = 9
  Private Const GRID_COLUMN_DELTA As Integer = 10
  Private Const GRID_COLUMN_REMAINING_AMOUNT As Integer = 11
  Private Const GRID_COLUMN_CURRENCY_ISO_CODE As Integer = 12
  Private Const GRID_COLUMN_SESSION_NAME As Integer = 13

  ' Grid Columns widths
  Private Const WIDTH_GRID_COLUMN_INDEX As Integer = 200
  Private Const WIDTH_GRID_COLUMN_GAMING_TABLE_NAME As Integer = 3250
  Private Const WIDTH_GRID_COLUMN_GAMING_TABLE_DATE As Integer = 1700
  Private Const WIDTH_GRID_COLUMN_GAMING_TABLE_TYPE As Integer = 1503
  Private Const WIDTH_GRID_COLUMN_SALES_TOTAL As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_PURCHASES_TOTAL As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_WINCAJAS As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_DELTA As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_REMAINING_AMOUNT As Integer = 1900
  Private Const WIDTH_GRID_COLUMN_CURRENCY_ISO_CODE As Integer = 0
  Private Const WIDTH_GRID_COLUMN_SESSION_NAME As Integer = 0

#End Region

#Region " Enums "

  Private Enum GAMING_TABLE_STATUS
    GT_STATUS_ALL = -1
    GT_STATUS_DISABLED = 0
    GT_STATUS_ENABLED = 1
  End Enum

  Private Enum OPERATIONS_TYPES
    OPERATIONS_ALL = -1
    OPERATIONS_SALES = 0
    OPERATIONS_PURCHASES = 1
  End Enum

#End Region

#Region " Members "

  ' Filter variables
  Private m_show_tables_only As Boolean
  Private m_date_from As String
  Private m_date_to As String
  Private m_gaming_table_status As GAMING_TABLE_STATUS
  Private m_operations As OPERATIONS_TYPES
  Private m_table_types_list As String
  Private m_area_id As String
  Private m_bank_id As String
  Private m_area_name As String
  Private m_bank_name As String
  Private m_cashiers_group_name As String
  Private m_multicurrency_selected_option As String

  ' For totals
  Private m_subtotal_name As String
  Private m_subtotal_type As String
  Private m_subtotal_sales As Decimal
  Private m_subtotal_purchases As Decimal
  Private m_subtotal_wincajas As Decimal
  Private m_subtotal_remaining As Decimal
  Private m_is_cashier As Boolean

  ' For totals
  Private m_total_sales As Decimal
  Private m_total_purchases As Decimal
  Private m_total_wincajas As Decimal
  Private m_total_remaining As Decimal

#End Region

#Region " OVERRIDES "


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _operations As String
    Dim _status As String

    _status = String.Empty
    _operations = String.Empty

    ' Dates filter checks
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & Space(1) & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_date_to)

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Status
    Select Case m_gaming_table_status
      Case GAMING_TABLE_STATUS.GT_STATUS_ALL
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
      Case GAMING_TABLE_STATUS.GT_STATUS_DISABLED
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(247)
      Case GAMING_TABLE_STATUS.GT_STATUS_ENABLED
        _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(246)
    End Select
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(262), _status)

    ' Movements
    Select Case m_operations
      Case OPERATIONS_TYPES.OPERATIONS_ALL
        _operations = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
      Case OPERATIONS_TYPES.OPERATIONS_PURCHASES
        _operations = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2343)
      Case OPERATIONS_TYPES.OPERATIONS_SALES
        _operations = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1177)
    End Select
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460), _operations)

    ' Gaming table type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409), m_table_types_list)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    ' Area & Bank
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(435), m_area_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(430), m_bank_name)

    '
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4574), IIf(m_show_tables_only, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4581), IIf(chk_grouped_by_type.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    ' Space
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)
    PrintData.SetFilter(String.Empty, String.Empty, True)

    ' Currency type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_multicurrency_selected_option)

    ' Width from header filter
    PrintData.Settings.FilterHeaderWidth(1) = 1600
    PrintData.Settings.FilterHeaderWidth(2) = 1000
    PrintData.Settings.FilterHeaderWidth(3) = 1400

  End Sub

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - 
  Protected Overrides Sub GUI_AfterLastRow()
    ' Draw Sub Total
    Call DrawRow(False)

    ' Draw totals
    Call DrawRow(True)

  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _purchase As Decimal
    Dim _sales As Decimal
    Dim _remaining As Decimal

    _purchase = DbRow.Value(SQL_COLUMN_PURCHASES_TOTAL)
    _sales = DbRow.Value(SQL_COLUMN_SALES_TOTAL)
    _remaining = DbRow.Value(SQL_COLUMN_REMAINING_TOTAL)

    If Not String.Equals(m_subtotal_name, DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)) Then
      If Not String.IsNullOrEmpty(m_subtotal_name) Then
        DrawRow(False)
      End If
      m_subtotal_name = DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)
      m_subtotal_type = DbRow.Value(SQL_COLUMN_GAMING_TABLE_TYPE)
      m_is_cashier = DbRow.Value(SQL_COLUMN_TYPE) = 1

      m_subtotal_purchases = 0
      m_subtotal_sales = 0
      m_subtotal_wincajas = 0
      m_subtotal_remaining = 0
    End If

    m_subtotal_purchases += _purchase
    m_subtotal_sales += _sales
    m_subtotal_wincajas = m_subtotal_sales - m_subtotal_purchases
    m_subtotal_remaining += _remaining

    ' Acum Total
    m_total_purchases += _purchase
    m_total_sales += _sales
    m_total_wincajas = m_total_sales - m_total_purchases
    m_total_remaining += _remaining

    Return True
  End Function

  ' PURPOSE: Hide columns depending selected mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_subtotal_sales = 0
    m_subtotal_purchases = 0
    m_subtotal_wincajas = 0
    m_subtotal_remaining = 0

    m_subtotal_name = ""
    m_subtotal_type = ""

    m_total_sales = 0
    m_total_purchases = 0
    m_total_wincajas = 0
    m_total_remaining = 0

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Set member values for filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim all_checked As Boolean

    m_date_from = ""
    m_date_to = ""

    ' Dates filter checks
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Operations to show
    If rb_movements_all.Checked Then
      m_operations = OPERATIONS_TYPES.OPERATIONS_ALL
    ElseIf rb_movements_sales.Checked Then
      m_operations = OPERATIONS_TYPES.OPERATIONS_SALES
    ElseIf rb_movements_purchases.Checked Then
      m_operations = OPERATIONS_TYPES.OPERATIONS_PURCHASES
    End If

    ' Gaming table status
    If rb_status_all.Checked Then
      m_gaming_table_status = GAMING_TABLE_STATUS.GT_STATUS_ALL
    ElseIf rb_status_disabled.Checked Then
      m_gaming_table_status = GAMING_TABLE_STATUS.GT_STATUS_DISABLED
    ElseIf rb_status_enabled.Checked Then
      m_gaming_table_status = GAMING_TABLE_STATUS.GT_STATUS_ENABLED
    End If

    ' Include cashiers
    m_show_tables_only = chb_show_tables_only.Checked

    ' Selected table types
    all_checked = True
    If uc_checked_list_table_type.SelectedIndexes.Length <> uc_checked_list_table_type.Count() Then
      all_checked = False
    End If

    If all_checked = True Then
      m_table_types_list = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_table_types_list = uc_checked_list_table_type.SelectedValuesList()
    End If

    ' Location filters update
    m_area_name = String.Empty
    m_bank_name = String.Empty
    If Me.cmb_area.SelectedIndex > 0 Then
      m_area_name = Me.cmb_area.TextValue
      m_area_id = Me.cmb_area.Value
      m_bank_name = Me.cmb_bank.TextValue
      m_bank_id = Me.cmb_bank.Value
    Else
      m_area_id = String.Empty
      m_bank_id = String.Empty
    End If

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _sales As Decimal
    Dim _purchases As Decimal
    Dim _delta As Decimal
    Dim _remaining As Decimal

    If chk_grouped_by_type.Checked Then

      Return False
    End If


    _sales = 0
    _purchases = 0
    _delta = 0
    _remaining = 0

    ' DLM      Bug 33364: WIGOS 13030
    With Me.Grid

      .Cell(RowIndex, GRID_COLUMN_INDEX).Value = ""

      ' Type
      .Cell(RowIndex, GRID_COLUMN_TYPE).Value = DbRow.Value(SQL_COLUMN_TYPE)

      ' Session Id
      .Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

      ' DLM 27-JUN-2018 Bug 33366: WIGOS 13035

      ' Gaming Table Type
      .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_TYPE).Value = DbRow.Value(SQL_COLUMN_GAMING_TABLE_TYPE) 'DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)

      If DbRow.Value(SQL_COLUMN_TYPE) = 1 Then
        ' Table name
        .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_NAME).Value = ""

      Else
        ' Table name
        .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_NAME).Value = DbRow.Value(SQL_COLUMN_GAMING_TABLE_NAME)

      End If

      ' Cashier Name
      .Cell(RowIndex, GRID_COLUMN_CASHIER).Value = DbRow.Value(SQL_COLUMN_CASHIER_NAME)

      ' DLM 27-JUN-2018 Bug 33366: WIGOS 13035

      ' Date
      .Cell(RowIndex, GRID_COLUMN_GAMING_TABLE_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_GAMING_TABLE_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

      ' Total Sales
      _sales = DbRow.Value(SQL_COLUMN_SALES_TOTAL)
      .Cell(RowIndex, GRID_COLUMN_SALES_TOTAL).Value = GUI_FormatCurrency(_sales, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      ' Total Purchases
      _purchases = DbRow.Value(SQL_COLUMN_PURCHASES_TOTAL)
      .Cell(RowIndex, GRID_COLUMN_PURCHASES_TOTAL).Value = GUI_FormatCurrency(_purchases, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      ' Win Cajas
      .Cell(RowIndex, GRID_COLUMN_WINCAJAS).Value = GUI_FormatCurrency(_sales - _purchases, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      ' Delta
      _delta = DbRow.Value(SQL_COLUMN_DELTA)
      .Cell(RowIndex, GRID_COLUMN_DELTA).Value = GUI_FormatCurrency(_delta, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      ' Total remaining
      _remaining = DbRow.Value(SQL_COLUMN_REMAINING_TOTAL)
      .Cell(RowIndex, GRID_COLUMN_REMAINING_AMOUNT).Value = GUI_FormatCurrency(_remaining, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

      ' Session Name
      .Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_SESSION_NAME), String.Empty, DbRow.Value(SQL_COLUMN_SESSION_NAME))
    End With


    Return True
  End Function

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = WIDTH_GRID_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Cashier
      .Column(GRID_COLUMN_CASHIER).Header(0).Text = ""
      .Column(GRID_COLUMN_CASHIER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) ' Name
      .Column(GRID_COLUMN_CASHIER).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_NAME

      ' Type
      .Column(GRID_COLUMN_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_TYPE).Width = 0
      .Column(GRID_COLUMN_TYPE).HighLightWhenSelected = False
      .Column(GRID_COLUMN_TYPE).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0
      .Column(GRID_COLUMN_SESSION_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SESSION_ID).IsColumnPrintable = False

      ' Table Name
      .Column(GRID_COLUMN_GAMING_TABLE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410) ' Table
      .Column(GRID_COLUMN_GAMING_TABLE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419) ' Name
      .Column(GRID_COLUMN_GAMING_TABLE_NAME).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_NAME

      ' Date
      .Column(GRID_COLUMN_GAMING_TABLE_DATE).Header(0).Text = ""
      .Column(GRID_COLUMN_GAMING_TABLE_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4452) ' Opening date
      .Column(GRID_COLUMN_GAMING_TABLE_DATE).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_DATE

      ' Table Type
      .Column(GRID_COLUMN_GAMING_TABLE_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3410) ' Table
      .Column(GRID_COLUMN_GAMING_TABLE_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4544) ' Type
      .Column(GRID_COLUMN_GAMING_TABLE_TYPE).Width = WIDTH_GRID_COLUMN_GAMING_TABLE_TYPE

      ' TOTAL Sales
      .Column(GRID_COLUMN_SALES_TOTAL).Header(0).Text = ""
      .Column(GRID_COLUMN_SALES_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6936)
      .Column(GRID_COLUMN_SALES_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If rb_movements_purchases.Checked Then
        .Column(GRID_COLUMN_SALES_TOTAL).Width = 0
        .Column(GRID_COLUMN_SALES_TOTAL).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_SALES_TOTAL).Width = WIDTH_GRID_COLUMN_SALES_TOTAL
      End If


      ' TOTAL Purchases
      .Column(GRID_COLUMN_PURCHASES_TOTAL).Header(0).Text = ""
      .Column(GRID_COLUMN_PURCHASES_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6937)
      .Column(GRID_COLUMN_PURCHASES_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If rb_movements_sales.Checked Then
        .Column(GRID_COLUMN_PURCHASES_TOTAL).Width = 0
        .Column(GRID_COLUMN_PURCHASES_TOTAL).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_PURCHASES_TOTAL).Width = WIDTH_GRID_COLUMN_PURCHASES_TOTAL
      End If

      ' Win Cajas ( A - B )
      .Column(GRID_COLUMN_WINCAJAS).Header(0).Text = ""
      .Column(GRID_COLUMN_WINCAJAS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6825)
      .Column(GRID_COLUMN_WINCAJAS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If rb_movements_all.Checked Then
        .Column(GRID_COLUMN_WINCAJAS).Width = WIDTH_GRID_COLUMN_WINCAJAS
      Else
        .Column(GRID_COLUMN_WINCAJAS).Width = 0
        .Column(GRID_COLUMN_WINCAJAS).IsColumnPrintable = False
      End If

      ' Delta
      .Column(GRID_COLUMN_DELTA).Width = 0
      .Column(GRID_COLUMN_DELTA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DELTA).Header(0).Text = ""
      .Column(GRID_COLUMN_DELTA).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6848)
      .Column(GRID_COLUMN_DELTA).Width = WIDTH_GRID_COLUMN_DELTA

      ' Currency iso code
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Header(1).Text = ""
      .Column(GRID_COLUMN_CURRENCY_ISO_CODE).Width = WIDTH_GRID_COLUMN_CURRENCY_ISO_CODE

      ' TOTAL Remaining
      .Column(GRID_COLUMN_REMAINING_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_REMAINING_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8477)
      .Column(GRID_COLUMN_REMAINING_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If rb_movements_sales.Checked Or Not GamingTableBusinessLogic.IsEnableRoundingSaleChips() Then
        .Column(GRID_COLUMN_REMAINING_AMOUNT).Width = 0
        .Column(GRID_COLUMN_REMAINING_AMOUNT).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_REMAINING_AMOUNT).Width = WIDTH_GRID_COLUMN_REMAINING_AMOUNT
      End If

      'Session Name
      .Column(GRID_COLUMN_SESSION_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_SESSION_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_SESSION_NAME).Header(1).Text = ""
      .Column(GRID_COLUMN_SESSION_NAME).Width = WIDTH_GRID_COLUMN_SESSION_NAME

    End With
  End Sub

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow

    Call GUI_StyleSheet()

    _sql_cmd = New SqlCommand("GT_Chips_Operations")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = IIf(Not Me.dtp_from.Checked, System.DBNull.Value, m_date_from)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = IIf(Not Me.dtp_to.Checked, System.DBNull.Value, m_date_to)
    _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = m_gaming_table_status
    _sql_cmd.Parameters.Add("@pArea", SqlDbType.VarChar).Value = m_area_id
    _sql_cmd.Parameters.Add("@pBank", SqlDbType.VarChar).Value = m_bank_id
    _sql_cmd.Parameters.Add("@pOnlyTables", SqlDbType.Int).Value = m_show_tables_only
    _sql_cmd.Parameters.Add("@pCashierGroupName", SqlDbType.VarChar).Value = m_cashiers_group_name
    _sql_cmd.Parameters.Add("@pValidTypes", SqlDbType.VarChar).Value = uc_checked_list_table_type.SelectedIndexesList()
    _sql_cmd.Parameters.Add("@pSelectedCurrency", SqlDbType.VarChar).Value = uc_multicurrency.SelectedCurrency
    _sql_cmd.Parameters.Add("@pSelectedOptionForm", SqlDbType.Int).Value = Convert.ToInt32(uc_multicurrency.SelectedOption)

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    ' Fill the grid only if there is data
    If _table.Rows.Count > 0 Then

      For Each _row In _table.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_chips_operations_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

    End If
    Call GUI_AfterLastRow()

    'Set m_multicurrency_selected_option when GUI_ExecuteQueryCustom function its ok.
    If (uc_multicurrency.SelectedOption = uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode) Then
      m_multicurrency_selected_option = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7397, CurrencyExchange.GetNationalCurrency())
    ElseIf (uc_multicurrency.SelectedOption = uc_multi_currency_site_sel.MULTICURRENCY_OPTION.ByCurrency) Then
      m_multicurrency_selected_option = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7398) & ": " & uc_multicurrency.SelectedCurrency
    End If
  End Sub

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4443)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4452) 'Opening date
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' TODO: Set NLS for all controls

    ' -- Location --
    '   � Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)                                ' 1166 "Ubicaci�n"
    Me.cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                                         ' 435  "Area"
    '   � Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)                                       ' 430  "Zona"
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False
    '   � Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                                         ' 431  "Isla"

    ' Movimientos
    Me.gb_movement_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2460)
    Me.rb_movements_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    Me.rb_movements_sales.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1177)
    Me.rb_movements_purchases.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4472)

    ' Estado
    Me.gb_tables_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5731)
    Me.rb_status_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
    Me.rb_status_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(247)
    Me.rb_status_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(246)

    ' Incluir cajeros
    Me.chb_show_tables_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4574)

    ' Agrupado
    Me.chk_grouped_by_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4581)

    m_cashiers_group_name = String.Empty

    ' Fill operators
    Me.uc_checked_list_table_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3409)
    Call Me.uc_checked_list_table_type.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)
    Call FillTableTypeFilterGrid()

    ' Fill combos
    Call AreaComboFill()
    Call BankComboFill()

    ' Add Handles
    Call AddHandles()

    Call SetDefaultValues()

    Call GUI_StyleSheet()
  End Sub


  ' PURPOSE: Call to child window to show details for the selected row
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim idx_row As Integer
    Dim session_id As Integer
    Dim session_name As String

    ' Check that it can show the information
    If Not GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled Then

      Return
    End If

    Dim frm As frm_cashier_movements

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then

      Return
    Else
      session_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value
      session_name = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_NAME).Value
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_cashier_movements

    frm.ShowForEdit(Me.MdiParent, session_id, session_name)


  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

    If ButtonId <> ENUM_BUTTON.BUTTON_CANCEL Then
      GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = CBool(Me.Grid.NumRows > 0) And Not Me.chk_grouped_by_type.Checked
    End If

  End Sub ' GUI_ButtonClick


  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CHIPS_OPERATIONS_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub ' Finalize

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private "

  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '          - LastTotal: Indicates the kind of total
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - True
  Private Function DrawRow(ByVal IsTotal As Boolean) As Boolean
    Dim _idx_row As Integer

    ' Add the totals row & data
    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      .Cell(_idx_row, GRID_COLUMN_INDEX).Value = String.Empty

      .Cell(_idx_row, GRID_COLUMN_TYPE).Value = String.Empty

      .Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value = String.Empty

      .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_TYPE).Value = String.Empty

      If IsTotal Then
        .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

        .Cell(_idx_row, GRID_COLUMN_SALES_TOTAL).Value = GUI_FormatCurrency(m_total_sales, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Cell(_idx_row, GRID_COLUMN_PURCHASES_TOTAL).Value = GUI_FormatCurrency(m_total_purchases, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Cell(_idx_row, GRID_COLUMN_WINCAJAS).Value = GUI_FormatCurrency(m_total_wincajas, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Cell(_idx_row, GRID_COLUMN_REMAINING_AMOUNT).Value = GUI_FormatCurrency(m_total_remaining, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Else

        If Not m_is_cashier Then
          .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) + " " + m_subtotal_name + ":"
        Else
          If chk_grouped_by_type.Checked Then
            .Cell(_idx_row, GRID_COLUMN_CASHIER).Value = m_subtotal_name
          Else
            .Cell(_idx_row, GRID_COLUMN_CASHIER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046) + " " + m_subtotal_name + ":"
          End If
        End If

        If chk_grouped_by_type.Checked AndAlso Not m_is_cashier Then
          .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_TYPE).Value = m_subtotal_type

          .Cell(_idx_row, GRID_COLUMN_GAMING_TABLE_NAME).Value = m_subtotal_name
        End If

        .Cell(_idx_row, GRID_COLUMN_SALES_TOTAL).Value = GUI_FormatCurrency(m_subtotal_sales, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Cell(_idx_row, GRID_COLUMN_PURCHASES_TOTAL).Value = GUI_FormatCurrency(m_subtotal_purchases, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Cell(_idx_row, GRID_COLUMN_WINCAJAS).Value = GUI_FormatCurrency(m_subtotal_wincajas, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Cell(_idx_row, GRID_COLUMN_REMAINING_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_remaining, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, CurrencySymbol:=False)

        .Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      End If
    End With

    Return True
  End Function ' DrawRow

  ' DLM 27-JUN-2018 Bug 33366: WIGOS 13035
  Private Sub ShowTablesOnly()
    If Me.chb_show_tables_only.Checked = False Then

      Call Me.uc_checked_list_table_type.SetDefaultValue(True)

      ' Location
      cmb_area.SelectedIndex = 0
      cmb_bank.SelectedIndex = 0
      ef_smoking.Value = String.Empty

      'uc_multi_currency_site_sel
      uc_multicurrency.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382)
      uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.AllControls

      Me.uc_checked_list_table_type.Enabled = False
      Me.gb_area_island.Enabled = False

    Else
      Me.uc_checked_list_table_type.Enabled = True
      Me.gb_area_island.Enabled = True
    End If


  End Sub
  '
  Private Sub SetDefaultValues()
    Dim _current_date As DateTime
    Dim _closing_time As Integer
    Dim _initial_time As Date

    _current_date = WSI.Common.WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_current_date.Year, _current_date.Month, _current_date.Day, _closing_time, 0, 0)

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    Me.rb_status_all.Checked = True
    Me.rb_movements_all.Checked = True
    Me.chb_show_tables_only.Checked = False

    'DLM 27-JUN-2018 Bug 33366: WIGOS 13035
    'ShowTablesOnly()

    Me.chk_grouped_by_type.Checked = False

    Call Me.uc_checked_list_table_type.SetDefaultValue(True)

    ' Location
    cmb_area.SelectedIndex = 0
    cmb_bank.SelectedIndex = 0
    ef_smoking.Value = String.Empty

    'uc_multi_currency_site_sel
    uc_multicurrency.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382)
    uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.AllControls

    'DLM 27-JUN-2018 Bug 33366: WIGOS 13035
    ShowTablesOnly()

    Me.uc_multicurrency.Init()


  End Sub

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTableTypeFilterGrid()
    Dim _data_table_tables_type As DataTable

    Call Me.uc_checked_list_table_type.Clear()
    Call Me.uc_checked_list_table_type.ReDraw(False)

    _data_table_tables_type = GamingTablesType_DBRead()
    Me.uc_checked_list_table_type.Add(_data_table_tables_type, "ID", "NAME")

    If Me.uc_checked_list_table_type.Count() > 0 Then
      Call Me.uc_checked_list_table_type.CurrentRow(0)
    End If

    Call Me.uc_checked_list_table_type.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Read the data from gaming tables types
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable: collected data
  Private Function GamingTablesType_DBRead() As DataTable
    Dim _str_sql As String
    Dim _data_table_tables_type As DataTable

    _str_sql = "SELECT   gtt_gaming_table_type_id AS ID" & _
              "        , GTT_NAME  AS NAME" & _
              "   FROM   GAMING_TABLES_TYPES " & _
              "  WHERE   GTT_ENABLED =  " & 1 & ""

    _data_table_tables_type = GUI_GetTableUsingSQL(_str_sql, 5000)

    Return _data_table_tables_type
  End Function

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub

  ' PURPOSE: Returns zone description
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  '
  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AreaComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)

    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub     ' AreaComboFill

  ' PURPOSE: Init BANKS combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub BankComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)

    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub     ' BankComboFill

  ' PURPOSE: Add Handles
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

#End Region

  'DLM 27-JUN-2018 Bug 33366: WIGOS 13035
  Private Sub chb_show_tables_only_CheckedChanged(sender As Object, e As EventArgs) Handles chb_show_tables_only.CheckedChanged
    ShowTablesOnly()

  End Sub
End Class