<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bonuses_meters_comparation
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_date = New GUI_Controls.uc_daily_session_selector
    Me.uc_provider = New GUI_Controls.uc_provider
    Me.chk_only_unbalanced = New System.Windows.Forms.CheckBox
    Me.chk_with_activity = New System.Windows.Forms.CheckBox
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.gb_group_by = New System.Windows.Forms.GroupBox
    Me.chk_show_detail = New System.Windows.Forms.CheckBox
    Me.chk_group_by_provider = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.chk_with_activity)
    Me.panel_filter.Controls.Add(Me.chk_only_unbalanced)
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Controls.Add(Me.uc_date)
    Me.panel_filter.Size = New System.Drawing.Size(1141, 196)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_unbalanced, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_with_activity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 200)
    Me.panel_data.Size = New System.Drawing.Size(1141, 368)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1135, 23)
    Me.pn_separator_line.TabIndex = 2
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1135, 4)
    '
    'uc_date
    '
    Me.uc_date.ClosingTime = 0
    Me.uc_date.ClosingTimeEnabled = True
    Me.uc_date.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.FromDateSelected = True
    Me.uc_date.Location = New System.Drawing.Point(6, 10)
    Me.uc_date.Name = "uc_date"
    Me.uc_date.ShowBorder = True
    Me.uc_date.Size = New System.Drawing.Size(260, 82)
    Me.uc_date.TabIndex = 0
    Me.uc_date.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.ToDateSelected = True
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(273, 6)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(325, 179)
    Me.uc_provider.TabIndex = 4
    '
    'chk_only_unbalanced
    '
    Me.chk_only_unbalanced.AutoSize = True
    Me.chk_only_unbalanced.Location = New System.Drawing.Point(7, 99)
    Me.chk_only_unbalanced.Name = "chk_only_unbalanced"
    Me.chk_only_unbalanced.Size = New System.Drawing.Size(125, 17)
    Me.chk_only_unbalanced.TabIndex = 1
    Me.chk_only_unbalanced.Text = "xOnlyUnbalanced"
    Me.chk_only_unbalanced.UseVisualStyleBackColor = True
    '
    'chk_with_activity
    '
    Me.chk_with_activity.AutoSize = True
    Me.chk_with_activity.Location = New System.Drawing.Point(7, 122)
    Me.chk_with_activity.Name = "chk_with_activity"
    Me.chk_with_activity.Size = New System.Drawing.Size(100, 17)
    Me.chk_with_activity.TabIndex = 2
    Me.chk_with_activity.Text = "xWithActivity"
    Me.chk_with_activity.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chk_terminal_location.Location = New System.Drawing.Point(7, 145)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(250, 19)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.chk_show_detail)
    Me.gb_group_by.Controls.Add(Me.chk_group_by_provider)
    Me.gb_group_by.Location = New System.Drawing.Point(604, 10)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(197, 82)
    Me.gb_group_by.TabIndex = 5
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xOptions"
    '
    'chk_show_detail
    '
    Me.chk_show_detail.AutoSize = True
    Me.chk_show_detail.Location = New System.Drawing.Point(20, 45)
    Me.chk_show_detail.Name = "chk_show_detail"
    Me.chk_show_detail.Size = New System.Drawing.Size(98, 17)
    Me.chk_show_detail.TabIndex = 3
    Me.chk_show_detail.Text = "Show details"
    Me.chk_show_detail.UseVisualStyleBackColor = True
    '
    'chk_group_by_provider
    '
    Me.chk_group_by_provider.AutoSize = True
    Me.chk_group_by_provider.Location = New System.Drawing.Point(6, 20)
    Me.chk_group_by_provider.Name = "chk_group_by_provider"
    Me.chk_group_by_provider.Size = New System.Drawing.Size(131, 17)
    Me.chk_group_by_provider.TabIndex = 0
    Me.chk_group_by_provider.Text = "xGroupByProvider"
    Me.chk_group_by_provider.UseVisualStyleBackColor = True
    '
    'frm_bonuses_meters_comparation
    '
    Me.ClientSize = New System.Drawing.Size(1149, 572)
    Me.Name = "frm_bonuses_meters_comparation"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents uc_date As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_only_unbalanced As System.Windows.Forms.CheckBox
  Friend WithEvents chk_with_activity As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_detail As System.Windows.Forms.CheckBox
  Friend WithEvents chk_group_by_provider As System.Windows.Forms.CheckBox

End Class
