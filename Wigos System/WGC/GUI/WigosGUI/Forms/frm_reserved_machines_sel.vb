﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_reserved_machines_sel.vb
'
' DESCRIPTION:   Report maquines reservedActivity.
'
' AUTHOR:        Ervin Olvera
'
' CREATION DATE: 08-AUG-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-AUG-2016  EOR    Initial version
' --------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml
#End Region ' Imports

Public Class frm_reserved_machines_sel
  Inherits frm_base_sel

#Region " Members"

  Private m_report_terminals As String
  Private m_from_date As Date
  Private m_to_date As Date
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_from_date_checked As Boolean
  Private m_to_date_checked As Boolean
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region

#Region " GRID"

  Private GRID_COLUMNS As Integer
  Private GRID_COLUMN_PROVIDER As Integer

  Private GRID_COLUMN_DENOMINATION As Integer
  Private GRID_COLUMN_ACCT_NUMBER As Integer
  Private GRID_COLUMN_CARD_TRACK As Integer
  Private GRID_COLUMN_HOLDER_NAME As Integer
  Private GRID_COLUMN_DATE_START_RESERVED As Integer
  Private GRID_COLUMN_DATE_END_RESERVED As Integer
  Private GRID_COLUMN_DURATION_RESERVED As Integer

#End Region


#Region " Constants"

  'SQL COLUMNS
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_DENOMINATION As Integer = 1
  Private Const SQL_COLUMN_ACCT_NUMBER As Integer = 2
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 3
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 4
  Private Const SQL_COLUMN_DATE_START_RESERVED As Integer = 5
  Private Const SQL_COLUMN_DATE_END_RESERVED As Integer = 6
  Private Const SQL_COLUMN_DURATION_RESERVED As Integer = 7

  'GRID COLUMNS
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_INIT_TERMINAL_DATA As Integer = 1

  'GRID SETUP
  Private Const GRID_HEADER_ROWS As Integer = 2

  'WIDTH GRID COLUMNS
  Private Const WIDTH_COLUMN_INDEX As Integer = 200
  Private Const WIDTH_COLUMN_DENOMINATION As Integer = 2300
  Private Const WIDTH_COLUMN_ACCT_NUMBER As Integer = 1150
  Private Const WIDTH_COLUMN_CARD_TRACK As Integer = 2300
  Private Const WIDTH_COLUMN_HOLDER_NAME As Integer = 2150
  Private Const WIDTH_COLUMN_DATE_START_RESERVED As Integer = 2300
  Private Const WIDTH_COLUMN_DATE_END_RESERVED As Integer = 2300
  Private Const WIDTH_COLUMN_DURATION_RESERVED As Integer = 1200

#End Region

#Region " Private Functions"

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      ' Initialize Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      ' GRID INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = WIDTH_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False


      'Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)

      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        'Set the main header
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)

        'In order to put in the second header 'MACHINE' instead of 'NAME'
        If _idx = 1 Then
          'set the second header
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(592)
        Else
          'set the second header
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        End If

        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment

        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      'Denomination
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_COLUMN_DENOMINATION).Width = WIDTH_COLUMN_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'CUSTOMERS 
      'Account number
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCT_NUMBER).Width = WIDTH_COLUMN_ACCT_NUMBER
      .Column(GRID_COLUMN_ACCT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Card Track
      .Column(GRID_COLUMN_CARD_TRACK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CARD_TRACK).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_CARD_TRACK).Width = WIDTH_COLUMN_CARD_TRACK
      .Column(GRID_COLUMN_CARD_TRACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = WIDTH_COLUMN_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'RESERVED
      'Reserved Date Start
      .Column(GRID_COLUMN_DATE_START_RESERVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7498)
      .Column(GRID_COLUMN_DATE_START_RESERVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3355)
      .Column(GRID_COLUMN_DATE_START_RESERVED).Width = WIDTH_COLUMN_DATE_START_RESERVED
      .Column(GRID_COLUMN_DATE_START_RESERVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Reserved Date End
      .Column(GRID_COLUMN_DATE_END_RESERVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7498)
      .Column(GRID_COLUMN_DATE_END_RESERVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356)
      .Column(GRID_COLUMN_DATE_END_RESERVED).Width = WIDTH_COLUMN_DATE_END_RESERVED
      .Column(GRID_COLUMN_DATE_END_RESERVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Duration Reserved
      .Column(GRID_COLUMN_DURATION_RESERVED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7498)
      .Column(GRID_COLUMN_DURATION_RESERVED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7500)
      .Column(GRID_COLUMN_DURATION_RESERVED).Width = WIDTH_COLUMN_DURATION_RESERVED
      .Column(GRID_COLUMN_DURATION_RESERVED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With
  End Sub

  Private Sub SetDefaultValues()
    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.chk_terminals_location.Checked = False

    Me.uc_accounts.Clear()

    Me.uc_terminals.SetDefaultValues()
  End Sub

  Private Sub GridColumnReIndex()

    'The number an position of columns depends on "chk_terminal_location" 
    If chk_terminals_location.Checked Then
      GRID_COLUMNS = 14

      GRID_COLUMN_DENOMINATION = 7
      GRID_COLUMN_ACCT_NUMBER = 8
      GRID_COLUMN_CARD_TRACK = 9
      GRID_COLUMN_HOLDER_NAME = 10
      GRID_COLUMN_DATE_START_RESERVED = 11
      GRID_COLUMN_DATE_END_RESERVED = 12
      GRID_COLUMN_DURATION_RESERVED = 13
    Else
      GRID_COLUMNS = 11

      GRID_COLUMN_DENOMINATION = 4
      GRID_COLUMN_ACCT_NUMBER = 5
      GRID_COLUMN_CARD_TRACK = 6
      GRID_COLUMN_HOLDER_NAME = 7
      GRID_COLUMN_DATE_START_RESERVED = 8
      GRID_COLUMN_DATE_END_RESERVED = 9
      GRID_COLUMN_DURATION_RESERVED = 10
    End If

  End Sub

  Private Function GetSqlWhere() As String
    Dim str_where As String = ""

    If Me.uc_accounts.Account() <> "" Then
      str_where += " AND (tb.ac_account_id = " & Me.uc_accounts.Account().Replace(",", "") & ")"
    End If

    If Me.uc_accounts.TrackData() <> "" Then
      str_where += " AND (tb.ac_card_data = '" & Me.uc_accounts.TrackData() & "')"
    End If

    If Me.uc_accounts.Holder() <> "" Then
      str_where += " AND (UPPER(tb.AC_HOLDER_NAME) LIKE '" & Me.uc_accounts.Holder() & "%' OR UPPER(tb.AC_HOLDER_NAME) LIKE '% " & Me.uc_accounts.Holder() & "%') "
    End If

    If str_where <> "" Then
      str_where = "WHERE " & str_where.Substring(5, str_where.Length - 5)
    End If

    Return str_where
  End Function

#End Region

#Region " Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region

#Region " Overrides"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    'Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7497)

    'Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'Terminals
    Call uc_terminals.Init(WSI.Common.Misc.AllTerminalTypes())

    'Accounts
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_accounts.InitExtendedQuery(False)
    End If

    'Show
    Me.chk_terminals_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Initialize Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub 'GUI_InitControls

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_RESERVED_MACHINES
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Call function to reset Filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub 'GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()
        Return False
      End If
    End If

    If Not Me.uc_accounts.ValidateFormat() Then
      Return False
    End If

    Return True

  End Function

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_query As New System.Text.StringBuilder


    _str_query.AppendLine(" SELECT * ")
    _str_query.AppendLine(" FROM ( ")
    _str_query.AppendLine("       SELECT rtt_terminal_id te_terminal_id,  ")
    _str_query.AppendLine("             te_denomination,")
    _str_query.AppendLine("             rtt_account_id ac_account_id, ")
    _str_query.AppendLine("             dbo.TrackDataToExternal(ac_track_data) ac_card_data,")
    _str_query.AppendLine("             ac_holder_name, ")
    _str_query.AppendLine("             rtt_start_reserved, ")
    _str_query.AppendLine("             rtt_end_reserved, ")
    _str_query.AppendLine("             rtt_total_minutes, ")
    _str_query.AppendLine("             ac_track_data, ")
    _str_query.AppendLine("             ac_type ")
    _str_query.AppendLine("       FROM reserved_terminal_transaction ")
    _str_query.AppendLine("       LEFT JOIN terminals  ON te_terminal_id = rtt_terminal_id ")
    _str_query.AppendLine("       LEFT JOIN accounts ON ac_account_id = rtt_account_id ")
    _str_query.AppendLine("       WHERE  rtt_terminal_id IN " & uc_terminals.GetProviderIdListSelected())

    If Me.dtp_from.Checked Then
      _str_query.AppendLine("     AND rtt_start_reserved >= CAST('" & dtp_from.Value.ToString("yyyy-MM-dd HH:mm:ss") & "' AS DATETIME) ")
    End If

    If Me.dtp_to.Checked Then
      _str_query.AppendLine("     AND rtt_start_reserved < CAST('" & dtp_to.Value.ToString("yyyy-MM-dd HH:mm:ss") & "' AS DATETIME) ")
    Else
      _str_query.AppendLine("     AND rtt_start_reserved < CAST('" & WSI.Common.Misc.TodayOpening().AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") & "' AS DATETIME) ")
    End If

    _str_query.AppendLine("      ) tb ")

    If WSI.Common.TITO.Utils.IsTitoMode() Then
      _str_query.AppendLine(GetSqlWhere())
    Else
      If uc_accounts.GetFilterSQL() <> "" Then
        _str_query.AppendLine(" WHERE " & uc_accounts.GetFilterSQL())
      End If
    End If

    Return _str_query.ToString()
  End Function

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    'Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)

    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      Else
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = ""
      End If
    Next

    If Not DbRow.IsNull(SQL_COLUMN_DENOMINATION) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DENOMINATION))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = "---"
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCT_NUMBER).Value = IIf(DbRow.IsNull(SQL_COLUMN_ACCT_NUMBER), "", DbRow.Value(SQL_COLUMN_ACCT_NUMBER))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = IIf(DbRow.IsNull(SQL_COLUMN_CARD_TRACK), "", DbRow.Value(SQL_COLUMN_CARD_TRACK))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = IIf(DbRow.IsNull(SQL_COLUMN_HOLDER_NAME), "", DbRow.Value(SQL_COLUMN_HOLDER_NAME))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_START_RESERVED).Value = IIf(DbRow.IsNull(SQL_COLUMN_DATE_START_RESERVED), "", DbRow.Value(SQL_COLUMN_DATE_START_RESERVED))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_END_RESERVED).Value = IIf(DbRow.IsNull(SQL_COLUMN_DATE_END_RESERVED), "", DbRow.Value(SQL_COLUMN_DATE_END_RESERVED))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DURATION_RESERVED).Value = IIf(DbRow.IsNull(SQL_COLUMN_DURATION_RESERVED), "", DbRow.Value(SQL_COLUMN_DURATION_RESERVED))

    Return True
  End Function

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_from_date_checked = Me.dtp_from.Checked
    If Me.dtp_from.Checked Then
      Me.m_from_date = Me.dtp_from.Value
    End If

    m_to_date_checked = Me.dtp_to.Checked
    If Me.dtp_to.Checked Then
      Me.m_to_date = Me.dtp_to.Value
    End If

    m_account = Me.uc_accounts.Account()
    m_track_data = Me.uc_accounts.TrackData()
    m_holder = Me.uc_accounts.Holder()

    m_report_terminals = Me.uc_terminals.GetTerminalReportText()

  End Sub 'GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    If m_from_date_checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(Me.m_from_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), "---")
    End If

    If m_to_date_checked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(Me.m_to_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), "---")
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_report_terminals)
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional FirstColIndex As Integer = 0)
    MyBase.GUI_ReportParams(ExcelData, FirstColIndex)

    ExcelData.SetColumnFormat(GRID_COLUMN_ACCT_NUMBER - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
    ExcelData.SetColumnFormat(GRID_COLUMN_CARD_TRACK - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
  End Sub

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub
#End Region

#Region "Events"

  Private Sub chk_terminals_location_CheckedChanged(sender As Object, e As EventArgs) Handles chk_terminals_location.CheckedChanged
    If chk_terminals_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True

  End Sub

#End Region

End Class