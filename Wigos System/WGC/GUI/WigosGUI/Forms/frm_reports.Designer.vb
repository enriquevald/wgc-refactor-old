<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_reports
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.uc_multi_currency = New GUI_Controls.uc_multi_currency_sel
    Me.uc_account_mov_sites_sel = New GUI_Controls.uc_sites_sel
    Me.gb_controls = New System.Windows.Forms.GroupBox
    Me.btn_exit = New GUI_Controls.uc_button
    Me.btn_excel = New GUI_Controls.uc_button
    Me.btn_xml = New GUI_Controls.uc_button
    Me.btn_visualize = New GUI_Controls.uc_button
    Me.lbl_filename = New GUI_Controls.uc_text_field
    Me.lnk_filename = New System.Windows.Forms.LinkLabel
    Me.lbl_num_records = New GUI_Controls.uc_text_field
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.rad_gaming_day = New System.Windows.Forms.RadioButton
    Me.rad_cashier_movements = New System.Windows.Forms.RadioButton
    Me.dt_date_to = New GUI_Controls.uc_date_picker
    Me.dt_date_from = New GUI_Controls.uc_date_picker
    Me.cmb_form = New GUI_Controls.uc_combo
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.gb_controls.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.IsSplitterFixed = True
    Me.SplitContainer1.Location = New System.Drawing.Point(5, 4)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.uc_multi_currency)
    Me.SplitContainer1.Panel1.Controls.Add(Me.uc_account_mov_sites_sel)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.gb_controls)
    Me.SplitContainer1.Panel2.Controls.Add(Me.cmb_form)
    Me.SplitContainer1.Size = New System.Drawing.Size(337, 489)
    Me.SplitContainer1.SplitterDistance = 171
    Me.SplitContainer1.TabIndex = 9
    '
    'uc_multi_currency
    '
    Me.uc_multi_currency.BackColor = System.Drawing.SystemColors.Control
    Me.uc_multi_currency.CurrenciesTable = Nothing
    Me.uc_multi_currency.DateFrom = New Date(CType(0, Long))
    Me.uc_multi_currency.DateTo = New Date(CType(0, Long))
    Me.uc_multi_currency.HasSitesWithoutCurrency = False
    Me.uc_multi_currency.IsGridApplyChanges = False
    Me.uc_multi_currency.IsoCodeSelected = Nothing
    Me.uc_multi_currency.Location = New System.Drawing.Point(3, 4)
    Me.uc_multi_currency.MultiSelect = False
    Me.uc_multi_currency.MultiSiteCurrency = Nothing
    Me.uc_multi_currency.Name = "uc_multi_currency"
    Me.uc_multi_currency.ShowIsoCode = True
    Me.uc_multi_currency.ShowMultisiteRow = False
    Me.uc_multi_currency.Size = New System.Drawing.Size(629, 284)
    Me.uc_multi_currency.TabIndex = 1
    '
    'uc_account_mov_sites_sel
    '
    Me.uc_account_mov_sites_sel.Location = New System.Drawing.Point(3, 3)
    Me.uc_account_mov_sites_sel.MultiSelect = True
    Me.uc_account_mov_sites_sel.Name = "uc_account_mov_sites_sel"
    Me.uc_account_mov_sites_sel.ShowIsoCode = False
    Me.uc_account_mov_sites_sel.ShowMultisiteRow = True
    Me.uc_account_mov_sites_sel.Size = New System.Drawing.Size(342, 166)
    Me.uc_account_mov_sites_sel.TabIndex = 0
    '
    'gb_controls
    '
    Me.gb_controls.Controls.Add(Me.btn_exit)
    Me.gb_controls.Controls.Add(Me.btn_excel)
    Me.gb_controls.Controls.Add(Me.btn_xml)
    Me.gb_controls.Controls.Add(Me.btn_visualize)
    Me.gb_controls.Controls.Add(Me.lbl_filename)
    Me.gb_controls.Controls.Add(Me.lnk_filename)
    Me.gb_controls.Controls.Add(Me.lbl_num_records)
    Me.gb_controls.Controls.Add(Me.gb_date)
    Me.gb_controls.Location = New System.Drawing.Point(3, 40)
    Me.gb_controls.Name = "gb_controls"
    Me.gb_controls.Size = New System.Drawing.Size(329, 267)
    Me.gb_controls.TabIndex = 9
    Me.gb_controls.TabStop = False
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(231, 231)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 8
    Me.btn_exit.Tag = ""
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_excel
    '
    Me.btn_excel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_excel.Location = New System.Drawing.Point(15, 140)
    Me.btn_excel.Name = "btn_excel"
    Me.btn_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_excel.TabIndex = 2
    Me.btn_excel.ToolTipped = False
    Me.btn_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_xml
    '
    Me.btn_xml.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_xml.Location = New System.Drawing.Point(119, 140)
    Me.btn_xml.Name = "btn_xml"
    Me.btn_xml.Size = New System.Drawing.Size(90, 30)
    Me.btn_xml.TabIndex = 3
    Me.btn_xml.ToolTipped = False
    Me.btn_xml.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_visualize
    '
    Me.btn_visualize.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_visualize.Location = New System.Drawing.Point(231, 140)
    Me.btn_visualize.Name = "btn_visualize"
    Me.btn_visualize.Size = New System.Drawing.Size(90, 30)
    Me.btn_visualize.TabIndex = 4
    Me.btn_visualize.ToolTipped = False
    Me.btn_visualize.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_filename
    '
    Me.lbl_filename.AutoSize = True
    Me.lbl_filename.IsReadOnly = True
    Me.lbl_filename.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_filename.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_filename.Location = New System.Drawing.Point(11, 171)
    Me.lbl_filename.Name = "lbl_filename"
    Me.lbl_filename.Size = New System.Drawing.Size(77, 24)
    Me.lbl_filename.SufixText = "Sufix Text"
    Me.lbl_filename.SufixTextVisible = True
    Me.lbl_filename.TabIndex = 5
    Me.lbl_filename.TabStop = False
    Me.lbl_filename.TextWidth = 0
    Me.lbl_filename.Value = "xFilename:"
    '
    'lnk_filename
    '
    Me.lnk_filename.AutoSize = True
    Me.lnk_filename.Location = New System.Drawing.Point(116, 175)
    Me.lnk_filename.Name = "lnk_filename"
    Me.lnk_filename.Size = New System.Drawing.Size(66, 13)
    Me.lnk_filename.TabIndex = 6
    Me.lnk_filename.TabStop = True
    Me.lnk_filename.Text = "xFileName"
    '
    'lbl_num_records
    '
    Me.lbl_num_records.AutoSize = True
    Me.lbl_num_records.IsReadOnly = True
    Me.lbl_num_records.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_num_records.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_num_records.Location = New System.Drawing.Point(40, 201)
    Me.lbl_num_records.Name = "lbl_num_records"
    Me.lbl_num_records.Size = New System.Drawing.Size(264, 24)
    Me.lbl_num_records.SufixText = "Sufix Text"
    Me.lbl_num_records.SufixTextVisible = True
    Me.lbl_num_records.TabIndex = 7
    Me.lbl_num_records.TabStop = False
    Me.lbl_num_records.TextWidth = 0
    Me.lbl_num_records.Value = "xNumRecords"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.rad_gaming_day)
    Me.gb_date.Controls.Add(Me.rad_cashier_movements)
    Me.gb_date.Controls.Add(Me.dt_date_to)
    Me.gb_date.Controls.Add(Me.dt_date_from)
    Me.gb_date.Location = New System.Drawing.Point(9, 13)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(315, 123)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "fra_date"
    '
    'rad_gaming_day
    '
    Me.rad_gaming_day.AutoSize = True
    Me.rad_gaming_day.Location = New System.Drawing.Point(28, 42)
    Me.rad_gaming_day.Name = "rad_gaming_day"
    Me.rad_gaming_day.Size = New System.Drawing.Size(99, 17)
    Me.rad_gaming_day.TabIndex = 3
    Me.rad_gaming_day.TabStop = True
    Me.rad_gaming_day.Text = "xGamingDay"
    Me.rad_gaming_day.UseVisualStyleBackColor = True
    '
    'rad_cashier_movements
    '
    Me.rad_cashier_movements.AutoSize = True
    Me.rad_cashier_movements.Location = New System.Drawing.Point(28, 19)
    Me.rad_cashier_movements.Name = "rad_cashier_movements"
    Me.rad_cashier_movements.Size = New System.Drawing.Size(141, 17)
    Me.rad_cashier_movements.TabIndex = 2
    Me.rad_cashier_movements.TabStop = True
    Me.rad_cashier_movements.Text = "xCashierMovements"
    Me.rad_cashier_movements.UseVisualStyleBackColor = True
    '
    'dt_date_to
    '
    Me.dt_date_to.Checked = True
    Me.dt_date_to.IsReadOnly = False
    Me.dt_date_to.Location = New System.Drawing.Point(2, 94)
    Me.dt_date_to.Name = "dt_date_to"
    Me.dt_date_to.ShowCheckBox = False
    Me.dt_date_to.ShowUpDown = False
    Me.dt_date_to.Size = New System.Drawing.Size(236, 24)
    Me.dt_date_to.SufixText = "Sufix Text"
    Me.dt_date_to.SufixTextVisible = True
    Me.dt_date_to.TabIndex = 1
    Me.dt_date_to.Value = New Date(2012, 3, 15, 10, 57, 5, 184)
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(2, 62)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = False
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(236, 24)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 0
    Me.dt_date_from.Value = New Date(2012, 3, 15, 10, 57, 5, 200)
    '
    'cmb_form
    '
    Me.cmb_form.AllowUnlistedValues = False
    Me.cmb_form.AutoCompleteMode = False
    Me.cmb_form.IsReadOnly = False
    Me.cmb_form.Location = New System.Drawing.Point(11, 10)
    Me.cmb_form.Name = "cmb_form"
    Me.cmb_form.SelectedIndex = -1
    Me.cmb_form.Size = New System.Drawing.Size(313, 24)
    Me.cmb_form.SufixText = "Sufix Text"
    Me.cmb_form.SufixTextVisible = True
    Me.cmb_form.TabIndex = 0
    '
    'frm_reports
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(347, 497)
    Me.Controls.Add(Me.SplitContainer1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_reports"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_forms_report"
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.gb_controls.ResumeLayout(False)
    Me.gb_controls.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents cmb_form As GUI_Controls.uc_combo
  Friend WithEvents uc_account_mov_sites_sel As GUI_Controls.uc_sites_sel
  Friend WithEvents gb_controls As System.Windows.Forms.GroupBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents btn_excel As GUI_Controls.uc_button
  Friend WithEvents btn_xml As GUI_Controls.uc_button
  Friend WithEvents btn_visualize As GUI_Controls.uc_button
  Friend WithEvents lbl_filename As GUI_Controls.uc_text_field
  Friend WithEvents lnk_filename As System.Windows.Forms.LinkLabel
  Friend WithEvents lbl_num_records As GUI_Controls.uc_text_field
  Friend WithEvents rad_gaming_day As System.Windows.Forms.RadioButton
  Friend WithEvents rad_cashier_movements As System.Windows.Forms.RadioButton
  Friend WithEvents uc_multi_currency As GUI_Controls.uc_multi_currency_sel
End Class
