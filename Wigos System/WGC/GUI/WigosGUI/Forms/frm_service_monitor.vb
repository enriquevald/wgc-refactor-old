'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_system_monitor.vb
' DESCRIPTION:   Monitor for WCP and WC2 services
' AUTHOR:        Agust� Poch
' CREATION DATE: 26-NOV-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-NOV-2008  APB    Initial Draft.
' 11-JUN-2010  RCI    Added column Version in Database Grid.
' 03-MAY-2012  JAB    Defect #288: Service Monitor without witness
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_CommonMisc
Imports WSI.Common

Public Class frm_service_monitor
  Inherits frm_base

#Region "Constants"

  ' Grid Columns
  Private Const SVC_GRID_COLUMNS As Integer = 8
  Private Const SVC_GRID_HEADER_ROWS As Integer = 1

  Private Const SVC_GRID_COLUMN_SENTINEL = 0
  Private Const SVC_GRID_COLUMN_PROTOCOL = 1
  Private Const SVC_GRID_COLUMN_COMPUTER = 2
  Private Const SVC_GRID_COLUMN_IP_ADDRESS = 3
  Private Const SVC_GRID_COLUMN_STATUS = 4
  Private Const SVC_GRID_COLUMN_STATUS_CODE = 5
  Private Const SVC_GRID_COLUMN_LAST_ACCESS = 6
  Private Const SVC_GRID_COLUMN_VERSION = 7

  Private Const SVC_SQL_COLUMN_PROTOCOL = 0
  Private Const SVC_SQL_COLUMN_COMPUTER = 1
  Private Const SVC_SQL_COLUMN_IP_ADDRESS = 2
  Private Const SVC_SQL_COLUMN_STATUS = 3
  Private Const SVC_SQL_COLUMN_LAST_ACCESS = 4
  Private Const SVC_SQL_COLUMN_VERSION = 5

  Private Const DB_GRID_COLUMNS As Integer = 5
  Private Const DB_GRID_HEADER_ROWS As Integer = 1

  Private Const DB_GRID_COLUMN_SENTINEL = 0
  Private Const DB_GRID_COLUMN_COMPUTER = 1
  Private Const DB_GRID_COLUMN_ROLE = 2
  Private Const DB_GRID_COLUMN_STATUS = 3
  Private Const DB_GRID_COLUMN_VERSION = 4

  Private Const DB_SQL_COLUMN_SERVER_NAME = 0
  Private Const DB_SQL_COLUMN_MIRRORING_STATE = 1
  Private Const DB_SQL_COLUMN_MIRRORING_PARTNER = 2
  Private Const DB_SQL_COLUMN_MIRRORING_WITNESS = 3
  Private Const DB_SQL_COLUMN_MIRRORING_WITNESS_STATE = 4
  Private Const DB_SQL_COLUMN_CLIENT_ID = 5
  Private Const DB_SQL_COLUMN_BUILD_ID = 6
  Private Const DB_SQL_COLUMN_RELEASE_ID = 7

  ' Service status as reported in the database
  Private Const DB_STATUS_ACTIVE = "RUNNING"
  Private Const DB_STATUS_STANDBY = "STANDBY"
  Private Const DB_STATUS_INACTIVE = "UNKNOWN"

  ' Maximum amount of seconds elapsed since last refresh to consider that a service is alive
  Private Const MAX_SERVICE_TIMEOUT = 10

  ' Timer interval to refresh services status (in milliseconds)
  Private Const SERVICE_REFRESH_TIMEOUT = 3000

  Private Const WINSTATS_NAME = "Winstats"
  Private Const WINSTATS_PROXY = "Winstats.Proxy"
  Private Const WINSTATS_API = "Winstats.API"
  Private Const WINSTATS_ERROR = "error"
  Private Const WINSTATS_STATUS_STOPPED = "STOPPED"
  Private Const WINSTATS_STATUS_RUNNING = "RUNNING"
  Private Const WINSTATS_DELAY = 5

#End Region

#Region "Enums"
  Private Enum ENUM_SERVICE_STATUS
    STATUS_INACTIVE = 0
    STATUS_ACTIVE = 1
    STATUS_STANDBY = 2
  End Enum

  Private Enum ENUM_WITNESS_STATUS
    STATUS_UKNOWN = 0
    STATUS_CONNECTED = 1
    STATUS_DISCONNECTED = 2
  End Enum

  Private Enum ENUM_MIRROR_STATUS
    STATUS_SUSPENDED = 0
    STATUS_DISCONNECTED = 1
    STATUS_SYNCHRONIZING = 2
    STATUS_PENDING_FAILOVER = 3
    STATUS_SYNCRONIZED = 4
    STATUS_NOT_SYNCHRONIZED = 5
    STATUS_PARTNERS_SYNCHRONIZED = 6
    STATUS_STANDALONE = 9
  End Enum
#End Region ' Enums

#Region "Overrides"

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SERVICE_MONITOR

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize all form controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    ' Form Title
    Me.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(201)

    Me.btn_exit.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(1)
    Me.gb_status.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(205)

    Me.tf_running.Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(206)
    GetServiceStatusColor(ENUM_SERVICE_STATUS.STATUS_ACTIVE, _
                          fore_color, _
                          back_color)
    lbl_running.BackColor = back_color
    lbl_running.ForeColor = fore_color

    Me.tf_standby.Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(207)
    GetServiceStatusColor(ENUM_SERVICE_STATUS.STATUS_STANDBY, _
                          fore_color, _
                          back_color)
    lbl_standby.BackColor = back_color
    lbl_standby.ForeColor = fore_color

    Me.tf_inactive.Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(208)
    GetServiceStatusColor(ENUM_SERVICE_STATUS.STATUS_INACTIVE, _
                          fore_color, _
                          back_color)
    lbl_inactive.BackColor = back_color
    lbl_inactive.ForeColor = fore_color

    Me.Timer1.Interval = SERVICE_REFRESH_TIMEOUT

    GUI_DbStyleSheet()
    RefreshDbGrid()
    GUI_SvcStyleSheet()
    RefreshSvcGrid()

    Me.Timer1.Start()

  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub RefreshSvcGrid()

    Dim data_table As DataTable
    Dim row_index As Integer
    Dim data_row As DataRow
    Dim fore_color As Color
    Dim back_color As Color
    Dim version_info_available As Boolean

    'SDS 2017-12-27  Winstats Aplicattions: Status & Version

    Dim _responseAPI, _responseProxy As String
    Dim _winstatsActive, _checkStatus As Boolean
    Dim _urlApi, _urlProxy As String

    _winstatsActive = (WSI.Common.GeneralParam.GetBoolean(WINSTATS_NAME, "Winstats.Active"))
    If Date.Now.Second < WINSTATS_DELAY Then _checkStatus = True Else _checkStatus = False




    If _winstatsActive And _checkStatus Then
      'update Api services Status

      Dim _api As CLS_API


      _urlProxy = WSI.Common.GeneralParam.GetString(WINSTATS_NAME, WINSTATS_PROXY) + "/api/setting/getsetting/"
      _api = New CLS_API(_urlProxy)
      _responseProxy = _api.getData()
      _urlApi = WSI.Common.GeneralParam.GetString(WINSTATS_NAME, WINSTATS_API) + "/api/setting/getsetting/"
      _api = New CLS_API(_urlApi)
      _responseAPI = _api.getData()

    End If


    ' Database may contain version information or not
    ' Query database schema to find out if relevant field is present or not
    data_table = GUI_GetTableUsingSQL(GetVersionInfoPresent(), 100)
    If IsNothing(data_table) Then
      Exit Sub
    End If

    version_info_available = data_table.Rows.Item(0).Item(0) > 0

    If version_info_available Then
      data_table = GUI_GetTableUsingSQL(GetFullSqlSvcQuery(), 100)
    Else
      data_table = GUI_GetTableUsingSQL(GetSqlSvcQuery(), 100)
    End If

    If IsNothing(data_table) Then
      Exit Sub
    End If

    If (data_table.Rows.Count <> Me.Service_Grid.NumRows) Then
      Me.Service_Grid.Clear()
    End If

    row_index = 0
    For Each data_row In data_table.Rows

      'SDS 2017-12-27  Winstats Aplicattions: Status & Version

      If _winstatsActive And _checkStatus Then
        If data_row.Item("SVC_PROTOCOL") = WINSTATS_PROXY And _responseProxy = WINSTATS_ERROR And data_row.Item("SVC_STATUS") <> WINSTATS_STATUS_STOPPED Then
          Using _db_trx As New DB_TRX
            WSI.Common.Services.DB_UpdateService(WINSTATS_PROXY, data_row.Item("SVC_IP_ADDRESS"), WINSTATS_STATUS_STOPPED, data_row.Item("SVC_VERSION"), _db_trx.SqlTransaction)
            _db_trx.Commit()
          End Using
          data_row.Item("SVC_STATUS") = WINSTATS_STATUS_STOPPED
        ElseIf _responseProxy <> WINSTATS_ERROR And data_row.Item("SVC_STATUS") = WINSTATS_STATUS_STOPPED Then
          Using _db_trx As New DB_TRX
            WSI.Common.Services.DB_UpdateService(WINSTATS_PROXY, data_row.Item("SVC_IP_ADDRESS"), WINSTATS_STATUS_RUNNING, data_row.Item("SVC_VERSION"), _db_trx.SqlTransaction)
            _db_trx.Commit()
          End Using
        End If

        If data_row.Item("SVC_PROTOCOL") = WINSTATS_API And _responseAPI = WINSTATS_ERROR And data_row.Item("SVC_STATUS") <> WINSTATS_STATUS_STOPPED Then
          Dim version As String
          version = "" & data_row.Item("SVC_VERSION")

          Using _db_trx As New DB_TRX
            WSI.Common.Services.DB_UpdateService(WINSTATS_API, data_row.Item("SVC_IP_ADDRESS"), WINSTATS_STATUS_STOPPED, version, _db_trx.SqlTransaction)
            _db_trx.Commit()
          End Using
          data_row.Item("SVC_STATUS") = WINSTATS_STATUS_STOPPED
        End If
      End If



      If (row_index >= Me.Service_Grid.NumRows) Then
        Me.Service_Grid.AddRow()
      End If

      Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_PROTOCOL).Value = data_row.Item("SVC_PROTOCOL")
      Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_COMPUTER).Value = data_row.Item("SVC_MACHINE")
      Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_IP_ADDRESS).Value = data_row.Item("SVC_IP_ADDRESS")
      Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_LAST_ACCESS).Value = data_row.Item("SVC_LAST_ACCESS")
      If version_info_available Then
        ' Add version info
        If Not IsDBNull(data_row.Item("SVC_VERSION")) Then
          Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_VERSION).Value = data_row.Item("SVC_VERSION")
        Else
          Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_VERSION).Value = ""
        End If
      Else
        ' Set a dummy value
        Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_VERSION).Value = ""
      End If

      ' Check service current status based on data from database and last access timestamps
      If (IsServiceDead(data_row.Item("SVC_LAST_ACCESS")) And data_row.Item("SVC_PROTOCOL") <> WINSTATS_API And data_row.Item("SVC_PROTOCOL") <> WINSTATS_PROXY) Then
        Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS_CODE).Value = ENUM_SERVICE_STATUS.STATUS_INACTIVE
        Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(208)
      Else
        ' Select color depending on service status
        Select Case data_row.Item("SVC_STATUS")

          Case DB_STATUS_ACTIVE
            Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS_CODE).Value = ENUM_SERVICE_STATUS.STATUS_ACTIVE
            Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(206)

          Case DB_STATUS_STANDBY
            Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS_CODE).Value = ENUM_SERVICE_STATUS.STATUS_STANDBY
            Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(207)

          Case Else ' DB_STATUS_INACTIVE and others
            Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS_CODE).Value = ENUM_SERVICE_STATUS.STATUS_INACTIVE
            Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(208)

        End Select
      End If

      GetServiceStatusColor(Me.Service_Grid.Cell(row_index, SVC_GRID_COLUMN_STATUS_CODE).Value, fore_color, back_color)
      Me.Service_Grid.Row(row_index).ForeColor = fore_color
      Me.Service_Grid.Row(row_index).BackColor = back_color

      row_index += 1
    Next

  End Sub ' RefreshSvcGrid

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub RefreshDbGrid()

    Dim data_table As DataTable
    Dim principal_name As String
    Dim mirror_name As String
    Dim witness_name As String
    Dim mirror_state As Integer
    Dim witness_state As Integer
    Dim fore_color As Color
    Dim back_color As Color
    Dim idx_rows As Integer
    Dim version As String
    Dim _dt_3gs_version As DataTable
    Dim _num_rows As Integer

    Try

      data_table = GUI_GetTableUsingSQL(GetSqlDbQuery(), 100)
      ' MPO 12-MAR-2013
      ' only refresh when is a site
      _dt_3gs_version = Nothing
      If Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) AndAlso Not WSI.Common.GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw", False) Then
        _dt_3gs_version = GUI_GetTableUsingSQL("SELECT DB_RELEASE_ID FROM DB_VERSION_INTERFACE_3GS", 1)
      End If

      If IsNothing(data_table) Then
        Exit Sub
      End If

      principal_name = ""
      mirror_name = ""
      witness_name = ""

      ' At least the main server must be there
      If (IsDBNull(data_table.Rows(0).Item(0))) Then
        Exit Sub
      End If
      principal_name = NullTrim(data_table.Rows(0).Item(0).ToString)

      If (Not IsDBNull(data_table.Rows(0).Item(2))) Then
        mirror_name = NullTrim(data_table.Rows(0).Item(2).ToString)
      End If

      If (Not IsDBNull(data_table.Rows(0).Item(3))) Then
        witness_name = NullTrim(data_table.Rows(0).Item(3).ToString)
      End If

      If (Not IsDBNull(data_table.Rows(0).Item(1))) Then
        mirror_state = NullTrim(data_table.Rows(0).Item(1).ToString)
      End If

      If (Not IsDBNull(data_table.Rows(0).Item(4))) Then
        witness_state = NullTrim(data_table.Rows(0).Item(4).ToString)
      End If

      ' RCI 11-JUN-2010: Get Version String
      If _dt_3gs_version IsNot Nothing Then
        If _dt_3gs_version.Rows.Count > 0 Then
          If Not IsDBNull(_dt_3gs_version.Rows(0).Item(0)) Then
            data_table.Rows(0).Item("RELEASE_3GS_ID") = _dt_3gs_version.Rows(0).Item(0)
          End If
        End If
      End If
      version = BuildVersionString(data_table.Rows(0))

      ' JAB 03-MAY-2012: Service Monitor without witness
      _num_rows = 0
      If Not String.IsNullOrEmpty(principal_name) Then
        _num_rows += 1
      End If
      If Not String.IsNullOrEmpty(mirror_name) Then
        _num_rows += 1
      End If
      If Not String.IsNullOrEmpty(witness_name) Then
        _num_rows += 1
      End If

      If Me.Database_Grid.NumRows < _num_rows Then
        For idx_rows = Me.Database_Grid.NumRows + 1 To _num_rows
          Me.Database_Grid.AddRow()
        Next
      Else
        For idx_rows = Me.Database_Grid.NumRows - 1 To _num_rows Step -1
          Me.Database_Grid.DeleteRowFast(idx_rows)
        Next
      End If

      ' Determine number of rows in the grid: 1 (standalone) or 3 (fully mirrored system)
      If (Not String.IsNullOrEmpty(principal_name) And _
          Not String.IsNullOrEmpty(mirror_name)) Then

        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_COMPUTER).Value = principal_name
        Me.Database_Grid.Cell(1, DB_GRID_COLUMN_COMPUTER).Value = mirror_name

        ' Mirroring roles
        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_ROLE).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(219)
        Me.Database_Grid.Cell(1, DB_GRID_COLUMN_ROLE).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(220)

        ' Mirroring state
        Select Case mirror_state
          Case ENUM_MIRROR_STATUS.STATUS_SUSPENDED
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(211)

          Case ENUM_MIRROR_STATUS.STATUS_DISCONNECTED
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(212)

          Case ENUM_MIRROR_STATUS.STATUS_SYNCHRONIZING
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(213)

          Case ENUM_MIRROR_STATUS.STATUS_PENDING_FAILOVER
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(214)

          Case ENUM_MIRROR_STATUS.STATUS_SYNCRONIZED
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(215)

          Case ENUM_MIRROR_STATUS.STATUS_NOT_SYNCHRONIZED
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(216)

          Case ENUM_MIRROR_STATUS.STATUS_PARTNERS_SYNCHRONIZED
            Me.Database_Grid.Cell(1, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(217)
        End Select

        ' Primary server (the one answering) is always supposed to be OK
        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(206)
        GetMirrorStatusColor(ENUM_MIRROR_STATUS.STATUS_STANDALONE, fore_color, back_color)
        Me.Database_Grid.Row(0).ForeColor = fore_color
        Me.Database_Grid.Row(0).BackColor = back_color

        GetMirrorStatusColor(mirror_state, fore_color, back_color)
        Me.Database_Grid.Row(1).ForeColor = fore_color
        Me.Database_Grid.Row(1).BackColor = back_color

        ' RCI 11-JUN-2010: New column in Database Grid
        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_VERSION).Value = version
        Me.Database_Grid.Cell(1, DB_GRID_COLUMN_VERSION).Value = version

        ' JAB 03-MAY-2012: Service Monitor without witness
        If Not String.IsNullOrEmpty(witness_name) Then
          ' Witness name is not present: it must be extracted from the Instance name
          Me.Database_Grid.Cell(2, DB_GRID_COLUMN_COMPUTER).Value = ExtractServerFromInstanceName(witness_name)

          ' Witness role and state cells
          Me.Database_Grid.Cell(2, DB_GRID_COLUMN_ROLE).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(221)
          Select Case witness_state

            Case ENUM_WITNESS_STATUS.STATUS_CONNECTED
              Me.Database_Grid.Cell(2, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(223)

            Case ENUM_WITNESS_STATUS.STATUS_DISCONNECTED
              Me.Database_Grid.Cell(2, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(224)

            Case Else
              Me.Database_Grid.Cell(2, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(222)
          End Select

          GetWitnessStatusColor(witness_state, fore_color, back_color)
          Me.Database_Grid.Row(2).ForeColor = fore_color
          Me.Database_Grid.Row(2).BackColor = back_color

          ' RCI 11-JUN-2010: New column in Database Grid
          Me.Database_Grid.Cell(2, DB_GRID_COLUMN_VERSION).Value = version

        End If

      Else

        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_COMPUTER).Value = principal_name
        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_ROLE).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(218)
        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(206)

        GetMirrorStatusColor(ENUM_MIRROR_STATUS.STATUS_STANDALONE, fore_color, back_color)
        Me.Database_Grid.Row(0).ForeColor = fore_color
        Me.Database_Grid.Row(0).BackColor = back_color

        ' RCI 11-JUN-2010: New column in Database Grid
        Me.Database_Grid.Cell(0, DB_GRID_COLUMN_VERSION).Value = version
      End If

    Catch ex As Exception

    End Try

  End Sub ' RefreshDbGrid

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetSqlSvcQuery() As String
    Dim sql_str As String

    sql_str = " SELECT   SVC_PROTOCOL " & _
              "        , SVC_MACHINE " & _
              "        , SVC_IP_ADDRESS " & _
              "        , SVC_STATUS " & _
              "        , SVC_LAST_ACCESS " & _
              "   FROM   SERVICES " & _
              " ORDER BY SVC_PROTOCOL, SVC_MACHINE "

    Return sql_str
  End Function ' GetSqlSvcQuery

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetFullSqlSvcQuery() As String
    Dim sql_str As String

    sql_str = " SELECT   SVC_PROTOCOL " & _
              "        , SVC_MACHINE " & _
              "        , SVC_IP_ADDRESS " & _
              "        , SVC_STATUS " & _
              "        , SVC_LAST_ACCESS " & _
              "        , SVC_VERSION " & _
              "   FROM   SERVICES " & _
              " ORDER BY SVC_PROTOCOL, SVC_MACHINE "

    Return sql_str
  End Function ' GetFullSqlSvcQuery

  ' PURPOSE: Build the SQL query to find out if version information is available for services
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetVersionInfoPresent() As String
    Dim sql_str As String

    sql_str = " SELECT   COUNT(*) " & _
              "   FROM   INFORMATION_SCHEMA.COLUMNS " & _
               " WHERE   TABLE_NAME = 'SERVICES'" & _
                   " AND COLUMN_NAME = 'SVC_VERSION' "

    Return sql_str
  End Function ' GetVersionInfoPresent

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Function GetSqlDbQuery() As String
    Dim sql_str As String

    sql_str = " SELECT   ServerProperty ('servername') PRINCIPAL " & _
              "        , m.mirroring_state MIRROR_STATUS " & _
              "        , m.mirroring_partner_instance PARTNER_NAME " & _
              "        , m.mirroring_witness_name WITNESS_NAME " & _
              "        , m.mirroring_witness_state WITNESS_STATE " & _
              "        , v.DB_CLIENT_ID  CLIENT_ID" & _
              "        , v.DB_RELEASE_ID RELEASE_ID" & _
              "        , 0               RELEASE_3GS_ID" & _
              "   FROM   sys.database_mirroring m JOIN sys.databases d " & _
              "   ON     m.database_id = d.database_id " & _
              "        , DB_VERSION v " & _
              "   WHERE  d.name = DB_Name() "

    Return sql_str
  End Function ' GetSqlDbQuery

  ' PURPOSE: Format appearance of the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_SvcStyleSheet()

    With Me.Service_Grid
      Call .Init(SVC_GRID_COLUMNS, SVC_GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      ' Header Initialization

      ' Sentinel
      .Column(SVC_GRID_COLUMN_SENTINEL).Header.Text = ""
      .Column(SVC_GRID_COLUMN_SENTINEL).Width = 200
      .Column(SVC_GRID_COLUMN_SENTINEL).HighLightWhenSelected = False

      ' Protocol
      .Column(SVC_GRID_COLUMN_PROTOCOL).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(SVC_GRID_COLUMN_PROTOCOL).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(202)
      .Column(SVC_GRID_COLUMN_PROTOCOL).Width = 1400
      .Column(SVC_GRID_COLUMN_PROTOCOL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Computer
      .Column(SVC_GRID_COLUMN_COMPUTER).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(SVC_GRID_COLUMN_COMPUTER).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(203)
      .Column(SVC_GRID_COLUMN_COMPUTER).Width = 2000
      .Column(SVC_GRID_COLUMN_COMPUTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' IP Address
      .Column(SVC_GRID_COLUMN_IP_ADDRESS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(SVC_GRID_COLUMN_IP_ADDRESS).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(204)
      .Column(SVC_GRID_COLUMN_IP_ADDRESS).Width = 1600
      .Column(SVC_GRID_COLUMN_IP_ADDRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(SVC_GRID_COLUMN_STATUS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(SVC_GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(205)
      .Column(SVC_GRID_COLUMN_STATUS).Width = 2000
      .Column(SVC_GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status Code
      .Column(SVC_GRID_COLUMN_STATUS_CODE).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(205)
      .Column(SVC_GRID_COLUMN_STATUS_CODE).Width = 0

      ' Last Access
      .Column(SVC_GRID_COLUMN_LAST_ACCESS).Header.Text = ""
      .Column(SVC_GRID_COLUMN_LAST_ACCESS).Width = 0

      ' Version
      .Column(SVC_GRID_COLUMN_VERSION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(SVC_GRID_COLUMN_VERSION).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(225)
      .Column(SVC_GRID_COLUMN_VERSION).Width = 1300
      .Column(SVC_GRID_COLUMN_VERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_SvcStyleSheet

  ' PURPOSE: Format appearance of the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_DbStyleSheet()

    With Me.Database_Grid
      Call .Init(DB_GRID_COLUMNS, DB_GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      ' Header Initialization

      ' Sentinel
      .Column(DB_GRID_COLUMN_SENTINEL).Header.Text = ""
      .Column(DB_GRID_COLUMN_SENTINEL).Width = 200
      .Column(DB_GRID_COLUMN_SENTINEL).HighLightWhenSelected = False

      ' Computer
      .Column(DB_GRID_COLUMN_COMPUTER).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(DB_GRID_COLUMN_COMPUTER).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(203)
      .Column(DB_GRID_COLUMN_COMPUTER).Width = 2000
      .Column(DB_GRID_COLUMN_COMPUTER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Role
      .Column(DB_GRID_COLUMN_ROLE).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(DB_GRID_COLUMN_ROLE).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(209)
      .Column(DB_GRID_COLUMN_ROLE).Width = 2200
      .Column(DB_GRID_COLUMN_ROLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(DB_GRID_COLUMN_STATUS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(DB_GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(205)
      .Column(DB_GRID_COLUMN_STATUS).Width = 2700
      .Column(DB_GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' RCI 11-JUN-2010: New column in Database Grid
      ' Version
      .Column(DB_GRID_COLUMN_VERSION).Header.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(225)
      .Column(DB_GRID_COLUMN_VERSION).Width = 1400
      .Column(DB_GRID_COLUMN_VERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub 'GUI_DbStyleSheet

  ' PURPOSE: Get color by SERVICE status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_SERVICE_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '     - 
  Private Sub GetServiceStatusColor(ByVal Status As ENUM_SERVICE_STATUS, _
                                    ByRef ForeColor As Color, _
                                    ByRef BackColor As Color)
    Select Case Status

      Case ENUM_SERVICE_STATUS.STATUS_ACTIVE
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      Case ENUM_SERVICE_STATUS.STATUS_STANDBY
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

      Case ENUM_SERVICE_STATUS.STATUS_INACTIVE
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_00)

    End Select

  End Sub 'GetServiceStatusColor

  ' PURPOSE: Get color by WITNESS status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_WITNESS_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '     - 
  Private Sub GetWitnessStatusColor(ByVal Status As ENUM_WITNESS_STATUS, _
                                    ByRef ForeColor As Color, _
                                    ByRef BackColor As Color)
    Select Case Status

      Case ENUM_WITNESS_STATUS.STATUS_CONNECTED
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      Case ENUM_WITNESS_STATUS.STATUS_DISCONNECTED
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

    End Select

  End Sub 'GetWitnessStatusColor

  ' PURPOSE: Get color by WITNESS status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_WITNESS_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '     - 
  Private Sub GetMirrorStatusColor(ByVal Status As ENUM_MIRROR_STATUS, _
                                   ByRef ForeColor As Color, _
                                   ByRef BackColor As Color)
    Select Case Status

      ' OK
      Case ENUM_MIRROR_STATUS.STATUS_STANDALONE
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

        ' OK
      Case ENUM_MIRROR_STATUS.STATUS_SYNCRONIZED, _
           ENUM_MIRROR_STATUS.STATUS_PARTNERS_SYNCHRONIZED, _
           ENUM_MIRROR_STATUS.STATUS_SYNCHRONIZING
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

        ' Error
      Case ENUM_MIRROR_STATUS.STATUS_DISCONNECTED, _
           ENUM_MIRROR_STATUS.STATUS_SUSPENDED, _
           ENUM_MIRROR_STATUS.STATUS_PENDING_FAILOVER, _
           ENUM_MIRROR_STATUS.STATUS_NOT_SYNCHRONIZED
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

    End Select

  End Sub 'GetMirrorStatusColor

  ' PURPOSE: Determine if a service can be marked as dead or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - LastAccess: Last date and time the service status was updated in the DB
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     - TRUE: Service is considered to be dead
  '     - FALSE: Service is considered to be active

  Private Function IsServiceDead(ByVal LastAccess As DateTime) As Boolean
    Dim inactivity As TimeSpan
    Dim now As DateTime

    now = GUI_GetDBDateTime()
    inactivity = now.Subtract(LastAccess)

    If (inactivity.TotalSeconds() >= MAX_SERVICE_TIMEOUT) Then
      Return True
    Else
      Return False
    End If

  End Function ' IsServiceDead

  ' PURPOSE: Extract the server name from the SQL Server instance name
  '
  '  PARAMS:
  '     - INPUT:
  '           - InstanceName: complete SQL Server instance Name
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     - Server name
  '     - "" if name cannot be extracted

  Private Function ExtractServerFromInstanceName(ByVal InstanceName As String) As String
    Dim slash_pos As Integer
    Dim dot_pos As Integer

    slash_pos = InstanceName.IndexOf("//")
    dot_pos = InstanceName.IndexOf(".")

    If (dot_pos > slash_pos And _
        slash_pos > -1) Then
      Return InstanceName.Substring(slash_pos + 2, (dot_pos - slash_pos - 2)).ToUpper
    Else
      Return ""
    End If

  End Function ' ExtractServerFromInstance

  ' PURPOSE: Build the Database Version string: client_id.build_id.release_id.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow: A row containing the data needed: client_id, build_id and release_id
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     - String with the Database Version.
  '     

  Private Function BuildVersionString(ByVal Row As DataRow) As String
    Dim _client As Integer
    Dim _db_ver As Integer
    Dim _db_3gs As Integer

    _client = CInt(Row.Item("CLIENT_ID"))
    _db_ver = CInt(Row.Item("RELEASE_ID"))
    _db_3gs = CInt(Row.Item("RELEASE_3GS_ID"))

    If _db_3gs > 0 Then
      Return _client.ToString() & "." & _db_ver.ToString("000") & "." & _db_3gs.ToString("000")
    Else
      Return _client.ToString() & "." & _db_ver.ToString("000")
    End If

  End Function ' BuildVersionString



#End Region ' Private Functions

#Region "Events"

  ' PURPOSE: Execute proper actions when user presses the 'Exit' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub btn_cancel_Click() Handles btn_exit.ClickEvent
    Me.Close()
  End Sub

  ' PURPOSE: Execute proper actions when programmed timer elapses
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Me.RefreshDbGrid()
    Me.RefreshSvcGrid()
  End Sub

#End Region ' Events

#Region "Public Functions"

  ' PURPOSE: Entry point for the screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub

#End Region ' Public Functions

End Class ' frm_service_monitor