<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_alarm_execute_patterns
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_checked_list_patterns = New GUI_Controls.uc_checked_list
    Me.cb_show_open_patterns = New System.Windows.Forms.CheckBox
    Me.lbl_in_process_pattern_color = New System.Windows.Forms.Label
    Me.lbl_closed_pattern_color = New System.Windows.Forms.Label
    Me.lbl_in_process_pattern = New GUI_Controls.uc_text_field
    Me.lbl_closed_pattern = New GUI_Controls.uc_text_field
    Me.gb_labels = New System.Windows.Forms.GroupBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_labels.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_labels)
    Me.panel_filter.Controls.Add(Me.cb_show_open_patterns)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_patterns)
    Me.panel_filter.Size = New System.Drawing.Size(1240, 185)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_patterns, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_show_open_patterns, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_labels, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 189)
    Me.panel_data.Size = New System.Drawing.Size(1240, 469)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1234, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1234, 4)
    '
    'uc_checked_list_patterns
    '
    Me.uc_checked_list_patterns.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_patterns.Location = New System.Drawing.Point(6, 6)
    Me.uc_checked_list_patterns.m_resize_width = 451
    Me.uc_checked_list_patterns.multiChoice = True
    Me.uc_checked_list_patterns.Name = "uc_checked_list_patterns"
    Me.uc_checked_list_patterns.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_patterns.SelectedIndexesList = ""
    Me.uc_checked_list_patterns.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_patterns.SelectedValuesList = ""
    Me.uc_checked_list_patterns.SetLevels = 2
    Me.uc_checked_list_patterns.Size = New System.Drawing.Size(451, 173)
    Me.uc_checked_list_patterns.TabIndex = 11
    Me.uc_checked_list_patterns.ValuesArray = New String(-1) {}
    '
    'cb_show_open_patterns
    '
    Me.cb_show_open_patterns.AutoSize = True
    Me.cb_show_open_patterns.Location = New System.Drawing.Point(463, 18)
    Me.cb_show_open_patterns.Name = "cb_show_open_patterns"
    Me.cb_show_open_patterns.Size = New System.Drawing.Size(141, 17)
    Me.cb_show_open_patterns.TabIndex = 12
    Me.cb_show_open_patterns.Text = "xShowOpenPatterns"
    Me.cb_show_open_patterns.UseVisualStyleBackColor = True
    '
    'lbl_in_process_pattern_color
    '
    Me.lbl_in_process_pattern_color.AutoSize = True
    Me.lbl_in_process_pattern_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_in_process_pattern_color.Location = New System.Drawing.Point(9, 55)
    Me.lbl_in_process_pattern_color.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_in_process_pattern_color.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_in_process_pattern_color.Name = "lbl_in_process_pattern_color"
    Me.lbl_in_process_pattern_color.Size = New System.Drawing.Size(15, 15)
    Me.lbl_in_process_pattern_color.TabIndex = 28
    Me.lbl_in_process_pattern_color.Text = "P"
    '
    'lbl_closed_pattern_color
    '
    Me.lbl_closed_pattern_color.AutoSize = True
    Me.lbl_closed_pattern_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_closed_pattern_color.Location = New System.Drawing.Point(9, 25)
    Me.lbl_closed_pattern_color.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_closed_pattern_color.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_closed_pattern_color.Name = "lbl_closed_pattern_color"
    Me.lbl_closed_pattern_color.Size = New System.Drawing.Size(15, 15)
    Me.lbl_closed_pattern_color.TabIndex = 27
    Me.lbl_closed_pattern_color.Text = "C"
    '
    'lbl_in_process_pattern
    '
    Me.lbl_in_process_pattern.AutoSize = True
    Me.lbl_in_process_pattern.IsReadOnly = True
    Me.lbl_in_process_pattern.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_in_process_pattern.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_in_process_pattern.Location = New System.Drawing.Point(30, 50)
    Me.lbl_in_process_pattern.Name = "lbl_in_process_pattern"
    Me.lbl_in_process_pattern.Size = New System.Drawing.Size(168, 25)
    Me.lbl_in_process_pattern.SufixText = "Sufix Text"
    Me.lbl_in_process_pattern.SufixTextVisible = True
    Me.lbl_in_process_pattern.TabIndex = 26
    Me.lbl_in_process_pattern.TabStop = False
    Me.lbl_in_process_pattern.TextWidth = 0
    Me.lbl_in_process_pattern.Value = "x In process pattern"
    '
    'lbl_closed_pattern
    '
    Me.lbl_closed_pattern.AutoSize = True
    Me.lbl_closed_pattern.IsReadOnly = True
    Me.lbl_closed_pattern.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_closed_pattern.LabelForeColor = System.Drawing.SystemColors.WindowText
    Me.lbl_closed_pattern.Location = New System.Drawing.Point(30, 20)
    Me.lbl_closed_pattern.Name = "lbl_closed_pattern"
    Me.lbl_closed_pattern.Size = New System.Drawing.Size(168, 25)
    Me.lbl_closed_pattern.SufixText = "Sufix Text"
    Me.lbl_closed_pattern.SufixTextVisible = True
    Me.lbl_closed_pattern.TabIndex = 25
    Me.lbl_closed_pattern.TabStop = False
    Me.lbl_closed_pattern.TextWidth = 0
    Me.lbl_closed_pattern.Value = "x closed pattern"
    '
    'gb_labels
    '
    Me.gb_labels.Controls.Add(Me.lbl_closed_pattern)
    Me.gb_labels.Controls.Add(Me.lbl_in_process_pattern_color)
    Me.gb_labels.Controls.Add(Me.lbl_in_process_pattern)
    Me.gb_labels.Controls.Add(Me.lbl_closed_pattern_color)
    Me.gb_labels.Location = New System.Drawing.Point(463, 94)
    Me.gb_labels.Name = "gb_labels"
    Me.gb_labels.Size = New System.Drawing.Size(204, 85)
    Me.gb_labels.TabIndex = 29
    Me.gb_labels.TabStop = False
    '
    'frm_alarm_execute_patterns
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1248, 662)
    Me.Name = "frm_alarm_execute_patterns"
    Me.Text = "frm_alarm_execute_patterns"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_labels.ResumeLayout(False)
    Me.gb_labels.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_checked_list_patterns As GUI_Controls.uc_checked_list
  Friend WithEvents cb_show_open_patterns As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_in_process_pattern_color As System.Windows.Forms.Label
  Friend WithEvents lbl_closed_pattern_color As System.Windows.Forms.Label
  Friend WithEvents lbl_in_process_pattern As GUI_Controls.uc_text_field
  Friend WithEvents lbl_closed_pattern As GUI_Controls.uc_text_field
  Friend WithEvents gb_labels As System.Windows.Forms.GroupBox
End Class
