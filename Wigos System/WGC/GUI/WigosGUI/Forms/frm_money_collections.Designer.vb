<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_money_collections
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.cmb_report_type = New GUI_Controls.uc_combo
    Me.gb_Notes = New System.Windows.Forms.GroupBox
    Me.lbl_color_profile = New System.Windows.Forms.Label
    Me.opt_money_lost = New System.Windows.Forms.RadioButton
    Me.opt_no_collected = New System.Windows.Forms.RadioButton
    Me.opt_all_notes = New System.Windows.Forms.RadioButton
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_Notes.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_Notes)
    Me.panel_filter.Controls.Add(Me.cmb_report_type)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Size = New System.Drawing.Size(752, 100)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_report_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_Notes, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 104)
    Me.panel_data.Size = New System.Drawing.Size(752, 529)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(746, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(746, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(625, 6)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(407, 197)
    Me.uc_pr_list.TabIndex = 3
    Me.uc_pr_list.Visible = False
    '
    'cmb_report_type
    '
    Me.cmb_report_type.IsReadOnly = False
    Me.cmb_report_type.Location = New System.Drawing.Point(292, 135)
    Me.cmb_report_type.Name = "cmb_report_type"
    Me.cmb_report_type.SelectedIndex = -1
    Me.cmb_report_type.Size = New System.Drawing.Size(277, 24)
    Me.cmb_report_type.SufixText = "Sufix Text"
    Me.cmb_report_type.SufixTextVisible = True
    Me.cmb_report_type.TabIndex = 11
    Me.cmb_report_type.TextWidth = 90
    Me.cmb_report_type.Visible = False
    '
    'gb_Notes
    '
    Me.gb_Notes.Controls.Add(Me.lbl_color_profile)
    Me.gb_Notes.Controls.Add(Me.opt_money_lost)
    Me.gb_Notes.Controls.Add(Me.opt_no_collected)
    Me.gb_Notes.Controls.Add(Me.opt_all_notes)
    Me.gb_Notes.Enabled = False
    Me.gb_Notes.Location = New System.Drawing.Point(292, 11)
    Me.gb_Notes.Name = "gb_Notes"
    Me.gb_Notes.Size = New System.Drawing.Size(318, 102)
    Me.gb_Notes.TabIndex = 2
    Me.gb_Notes.TabStop = False
    Me.gb_Notes.Text = "xNotes"
    Me.gb_Notes.Visible = False
    '
    'lbl_color_profile
    '
    Me.lbl_color_profile.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_color_profile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_profile.Location = New System.Drawing.Point(143, 74)
    Me.lbl_color_profile.Name = "lbl_color_profile"
    Me.lbl_color_profile.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_profile.TabIndex = 106
    '
    'opt_money_lost
    '
    Me.opt_money_lost.Location = New System.Drawing.Point(6, 69)
    Me.opt_money_lost.Name = "opt_money_lost"
    Me.opt_money_lost.Size = New System.Drawing.Size(256, 24)
    Me.opt_money_lost.TabIndex = 2
    Me.opt_money_lost.Text = "xMoneyLost"
    '
    'opt_no_collected
    '
    Me.opt_no_collected.Checked = True
    Me.opt_no_collected.Location = New System.Drawing.Point(6, 19)
    Me.opt_no_collected.Name = "opt_no_collected"
    Me.opt_no_collected.Size = New System.Drawing.Size(256, 24)
    Me.opt_no_collected.TabIndex = 0
    Me.opt_no_collected.TabStop = True
    Me.opt_no_collected.Text = "xNoCollected"
    '
    'opt_all_notes
    '
    Me.opt_all_notes.Location = New System.Drawing.Point(6, 43)
    Me.opt_all_notes.Name = "opt_all_notes"
    Me.opt_all_notes.Size = New System.Drawing.Size(256, 24)
    Me.opt_all_notes.TabIndex = 1
    Me.opt_all_notes.Text = "xAllNotes"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2003, 5, 20, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(15, 11)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(260, 80)
    Me.uc_dsl.TabIndex = 1
    Me.uc_dsl.ToDate = New Date(2003, 5, 20, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'frm_money_collections
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(760, 637)
    Me.Name = "frm_money_collections"
    Me.Text = "frm_money_collections"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_Notes.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents cmb_report_type As GUI_Controls.uc_combo
  Friend WithEvents gb_Notes As System.Windows.Forms.GroupBox
  Friend WithEvents opt_no_collected As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_notes As System.Windows.Forms.RadioButton
  Friend WithEvents opt_money_lost As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_color_profile As System.Windows.Forms.Label
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
End Class
