<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_payment_order_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector
    Me.uc_account_filter = New GUI_Controls.uc_account_sel
    Me.gb_bank_accounts = New System.Windows.Forms.GroupBox
    Me.dg_bank_account = New GUI_Controls.uc_grid
    Me.ef_barcode = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_bank_accounts.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_barcode)
    Me.panel_filter.Controls.Add(Me.gb_bank_accounts)
    Me.panel_filter.Controls.Add(Me.uc_account_filter)
    Me.panel_filter.Controls.Add(Me.ds_from_to)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 146)
    Me.panel_filter.Controls.SetChildIndex(Me.ds_from_to, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_filter, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_bank_accounts, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_barcode, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 150)
    Me.panel_data.Size = New System.Drawing.Size(1226, 540)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = False
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.Location = New System.Drawing.Point(24, 9)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.ShowBorder = True
    Me.ds_from_to.Size = New System.Drawing.Size(265, 84)
    Me.ds_from_to.TabIndex = 0
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    '
    'uc_account_filter
    '
    Me.uc_account_filter.Account = ""
    Me.uc_account_filter.AccountText = ""
    Me.uc_account_filter.Anon = False
    Me.uc_account_filter.AutoSize = True
    Me.uc_account_filter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_filter.BirthDate = New Date(CType(0, Long))
    Me.uc_account_filter.DisabledHolder = False
    Me.uc_account_filter.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_filter.Holder = ""
    Me.uc_account_filter.Location = New System.Drawing.Point(584, 6)
    Me.uc_account_filter.MassiveSearchNumbers = ""
    Me.uc_account_filter.MassiveSearchNumbersToEdit = ""
    Me.uc_account_filter.MassiveSearchType = 0
    Me.uc_account_filter.Name = "uc_account_filter"
    Me.uc_account_filter.SearchTrackDataAsInternal = True
    Me.uc_account_filter.ShowMassiveSearch = False
    Me.uc_account_filter.ShowVipClients = True
    Me.uc_account_filter.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_filter.TabIndex = 2
    Me.uc_account_filter.Telephone = ""
    Me.uc_account_filter.TrackData = ""
    Me.uc_account_filter.Vip = False
    Me.uc_account_filter.WeddingDate = New Date(CType(0, Long))
    '
    'gb_bank_accounts
    '
    Me.gb_bank_accounts.Controls.Add(Me.dg_bank_account)
    Me.gb_bank_accounts.Location = New System.Drawing.Point(296, 9)
    Me.gb_bank_accounts.Name = "gb_bank_accounts"
    Me.gb_bank_accounts.Size = New System.Drawing.Size(283, 101)
    Me.gb_bank_accounts.TabIndex = 1
    Me.gb_bank_accounts.TabStop = False
    Me.gb_bank_accounts.Text = "xBankAccount"
    '
    'dg_bank_account
    '
    Me.dg_bank_account.CurrentCol = -1
    Me.dg_bank_account.CurrentRow = -1
    Me.dg_bank_account.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_bank_account.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_bank_account.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_bank_account.Location = New System.Drawing.Point(11, 20)
    Me.dg_bank_account.Name = "dg_bank_account"
    Me.dg_bank_account.PanelRightVisible = True
    Me.dg_bank_account.Redraw = True
    Me.dg_bank_account.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_bank_account.Size = New System.Drawing.Size(266, 69)
    Me.dg_bank_account.Sortable = False
    Me.dg_bank_account.SortAscending = True
    Me.dg_bank_account.SortByCol = 0
    Me.dg_bank_account.TabIndex = 0
    Me.dg_bank_account.ToolTipped = True
    Me.dg_bank_account.TopRow = -2
    '
    'ef_barcode
    '
    Me.ef_barcode.AllowDrop = True
    Me.ef_barcode.DoubleValue = 0
    Me.ef_barcode.IntegerValue = 0
    Me.ef_barcode.IsReadOnly = False
    Me.ef_barcode.Location = New System.Drawing.Point(3, 103)
    Me.ef_barcode.Name = "ef_barcode"
    Me.ef_barcode.PlaceHolder = Nothing
    Me.ef_barcode.Size = New System.Drawing.Size(286, 24)
    Me.ef_barcode.SufixText = "Sufix Text"
    Me.ef_barcode.SufixTextVisible = True
    Me.ef_barcode.TabIndex = 13
    Me.ef_barcode.TabStop = False
    Me.ef_barcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_barcode.TextValue = ""
    Me.ef_barcode.TextWidth = 120
    Me.ef_barcode.Value = ""
    Me.ef_barcode.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_payment_order_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1234, 694)
    Me.Name = "frm_payment_order_sel"
    Me.Text = "frm_payment_order_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_bank_accounts.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_account_filter As GUI_Controls.uc_account_sel
  Friend WithEvents gb_bank_accounts As System.Windows.Forms.GroupBox
  Friend WithEvents dg_bank_account As GUI_Controls.uc_grid
  Friend WithEvents ef_barcode As GUI_Controls.uc_entry_field
End Class
