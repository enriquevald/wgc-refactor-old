<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_draw_summary
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_site_id = New GUI_Controls.uc_entry_field()
    Me.rb_opt_site_day = New System.Windows.Forms.RadioButton()
    Me.rb_opt_day_site = New System.Windows.Forms.RadioButton()
    Me.gb_group_by = New System.Windows.Forms.GroupBox()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.ef_site_id)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(666, 128)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_site_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 132)
    Me.panel_data.Size = New System.Drawing.Size(666, 520)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(660, 16)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(660, 4)
    '
    'ef_site_id
    '
    Me.ef_site_id.DoubleValue = 0.0R
    Me.ef_site_id.IntegerValue = 0
    Me.ef_site_id.IsReadOnly = False
    Me.ef_site_id.Location = New System.Drawing.Point(9, 95)
    Me.ef_site_id.Name = "ef_site_id"
    Me.ef_site_id.PlaceHolder = Nothing
    Me.ef_site_id.Size = New System.Drawing.Size(176, 24)
    Me.ef_site_id.SufixText = "Sufix Text"
    Me.ef_site_id.SufixTextVisible = True
    Me.ef_site_id.TabIndex = 3
    Me.ef_site_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_id.TextValue = ""
    Me.ef_site_id.Value = ""
    Me.ef_site_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'rb_opt_site_day
    '
    Me.rb_opt_site_day.AutoSize = True
    Me.rb_opt_site_day.Location = New System.Drawing.Point(26, 20)
    Me.rb_opt_site_day.Name = "rb_opt_site_day"
    Me.rb_opt_site_day.Size = New System.Drawing.Size(77, 17)
    Me.rb_opt_site_day.TabIndex = 0
    Me.rb_opt_site_day.TabStop = True
    Me.rb_opt_site_day.Text = "xSiteDay"
    Me.rb_opt_site_day.UseVisualStyleBackColor = True
    '
    'rb_opt_day_site
    '
    Me.rb_opt_day_site.AutoSize = True
    Me.rb_opt_day_site.Location = New System.Drawing.Point(26, 51)
    Me.rb_opt_day_site.Name = "rb_opt_day_site"
    Me.rb_opt_day_site.Size = New System.Drawing.Size(77, 17)
    Me.rb_opt_day_site.TabIndex = 1
    Me.rb_opt_day_site.TabStop = True
    Me.rb_opt_day_site.Text = "xDaySite"
    Me.rb_opt_day_site.UseVisualStyleBackColor = True
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.rb_opt_site_day)
    Me.gb_group_by.Controls.Add(Me.rb_opt_day_site)
    Me.gb_group_by.Location = New System.Drawing.Point(290, 6)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(154, 83)
    Me.gb_group_by.TabIndex = 2
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xGroupBy"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(24, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(260, 83)
    Me.uc_dsl.TabIndex = 1
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'frm_terminal_draw_summary
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(676, 656)
    Me.Name = "frm_terminal_draw_summary"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_draws_cashdesk_summary_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_site_id As GUI_Controls.uc_entry_field
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents rb_opt_site_day As System.Windows.Forms.RadioButton
  Friend WithEvents rb_opt_day_site As System.Windows.Forms.RadioButton
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
End Class
