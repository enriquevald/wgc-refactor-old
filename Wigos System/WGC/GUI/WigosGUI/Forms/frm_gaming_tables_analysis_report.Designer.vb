﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_analysis_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_checked_list_table_type = New GUI_Controls.uc_checked_list()
    Me.lbl_hour = New System.Windows.Forms.Label()
    Me.dtp_date = New GUI_Controls.uc_date_picker()
    Me.lbl_accumulated = New System.Windows.Forms.Label()
    Me.chk_type_table_subtotal = New System.Windows.Forms.CheckBox()
    Me.opt_acum_monthly = New System.Windows.Forms.RadioButton()
    Me.opt_acum_yearly = New System.Windows.Forms.RadioButton()
    Me.uc_multicurrency = New GUI_Controls.uc_multi_currency_site_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_multicurrency)
    Me.panel_filter.Controls.Add(Me.opt_acum_yearly)
    Me.panel_filter.Controls.Add(Me.opt_acum_monthly)
    Me.panel_filter.Controls.Add(Me.chk_type_table_subtotal)
    Me.panel_filter.Controls.Add(Me.lbl_accumulated)
    Me.panel_filter.Controls.Add(Me.lbl_hour)
    Me.panel_filter.Controls.Add(Me.dtp_date)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_table_type)
    Me.panel_filter.Size = New System.Drawing.Size(1297, 189)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_table_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.dtp_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_hour, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_accumulated, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_type_table_subtotal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.opt_acum_monthly, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.opt_acum_yearly, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_multicurrency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 193)
    Me.panel_data.Size = New System.Drawing.Size(1297, 415)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1291, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1291, 4)
    '
    'uc_checked_list_table_type
    '
    Me.uc_checked_list_table_type.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_table_type.Location = New System.Drawing.Point(260, 6)
    Me.uc_checked_list_table_type.m_resize_width = 351
    Me.uc_checked_list_table_type.multiChoice = True
    Me.uc_checked_list_table_type.Name = "uc_checked_list_table_type"
    Me.uc_checked_list_table_type.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_table_type.SelectedIndexesList = ""
    Me.uc_checked_list_table_type.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_table_type.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_table_type.SelectedValuesList = ""
    Me.uc_checked_list_table_type.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_table_type.SetLevels = 2
    Me.uc_checked_list_table_type.Size = New System.Drawing.Size(351, 173)
    Me.uc_checked_list_table_type.TabIndex = 5
    Me.uc_checked_list_table_type.ValuesArray = New String(-1) {}
    '
    'lbl_hour
    '
    Me.lbl_hour.AutoSize = True
    Me.lbl_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_hour.Location = New System.Drawing.Point(198, 22)
    Me.lbl_hour.Name = "lbl_hour"
    Me.lbl_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_hour.TabIndex = 0
    Me.lbl_hour.Text = "00:00"
    '
    'dtp_date
    '
    Me.dtp_date.Checked = True
    Me.dtp_date.IsReadOnly = False
    Me.dtp_date.Location = New System.Drawing.Point(6, 16)
    Me.dtp_date.Name = "dtp_date"
    Me.dtp_date.ShowCheckBox = False
    Me.dtp_date.ShowUpDown = False
    Me.dtp_date.Size = New System.Drawing.Size(188, 24)
    Me.dtp_date.SufixText = "Sufix Text"
    Me.dtp_date.SufixTextVisible = True
    Me.dtp_date.TabIndex = 1
    Me.dtp_date.TextWidth = 50
    Me.dtp_date.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'lbl_accumulated
    '
    Me.lbl_accumulated.AutoSize = True
    Me.lbl_accumulated.Location = New System.Drawing.Point(26, 63)
    Me.lbl_accumulated.Name = "lbl_accumulated"
    Me.lbl_accumulated.Size = New System.Drawing.Size(87, 13)
    Me.lbl_accumulated.TabIndex = 0
    Me.lbl_accumulated.Text = "xAccumulated"
    '
    'chk_type_table_subtotal
    '
    Me.chk_type_table_subtotal.AutoSize = True
    Me.chk_type_table_subtotal.Location = New System.Drawing.Point(29, 145)
    Me.chk_type_table_subtotal.Name = "chk_type_table_subtotal"
    Me.chk_type_table_subtotal.Size = New System.Drawing.Size(136, 17)
    Me.chk_type_table_subtotal.TabIndex = 4
    Me.chk_type_table_subtotal.Text = "xTypeTableSubtotal"
    Me.chk_type_table_subtotal.UseVisualStyleBackColor = True
    '
    'opt_acum_monthly
    '
    Me.opt_acum_monthly.AutoSize = True
    Me.opt_acum_monthly.Location = New System.Drawing.Point(58, 86)
    Me.opt_acum_monthly.Name = "opt_acum_monthly"
    Me.opt_acum_monthly.Size = New System.Drawing.Size(76, 17)
    Me.opt_acum_monthly.TabIndex = 2
    Me.opt_acum_monthly.TabStop = True
    Me.opt_acum_monthly.Text = "xMonthly"
    Me.opt_acum_monthly.UseVisualStyleBackColor = True
    '
    'opt_acum_yearly
    '
    Me.opt_acum_yearly.AutoSize = True
    Me.opt_acum_yearly.Location = New System.Drawing.Point(58, 108)
    Me.opt_acum_yearly.Name = "opt_acum_yearly"
    Me.opt_acum_yearly.Size = New System.Drawing.Size(67, 17)
    Me.opt_acum_yearly.TabIndex = 3
    Me.opt_acum_yearly.TabStop = True
    Me.opt_acum_yearly.Text = "xYearly"
    Me.opt_acum_yearly.UseVisualStyleBackColor = True
    '
    'uc_multicurrency
    '
    Me.uc_multicurrency.GroupBoxText = Nothing
    Me.uc_multicurrency.HeightControl = 0
    Me.uc_multicurrency.Location = New System.Drawing.Point(617, 6)
    Me.uc_multicurrency.Name = "uc_multicurrency"
    Me.uc_multicurrency.OpenModeControl = GUI_Controls.uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.SelectedCurrency = ""
    Me.uc_multicurrency.SelectedOption = GUI_Controls.uc_multi_currency_site_sel.MULTICURRENCY_OPTION.TotalToIsoCode
    Me.uc_multicurrency.Size = New System.Drawing.Size(202, 71)
    Me.uc_multicurrency.TabIndex = 11
    Me.uc_multicurrency.WidthControl = 0
    '
    'frm_gaming_tables_analysis_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1305, 612)
    Me.Name = "frm_gaming_tables_analysis_report"
    Me.Text = "frm_gaming_tables_analysis_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_checked_list_table_type As GUI_Controls.uc_checked_list
  Friend WithEvents opt_acum_yearly As System.Windows.Forms.RadioButton
  Friend WithEvents opt_acum_monthly As System.Windows.Forms.RadioButton
  Friend WithEvents chk_type_table_subtotal As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_accumulated As System.Windows.Forms.Label
  Public WithEvents lbl_hour As System.Windows.Forms.Label
  Protected WithEvents dtp_date As GUI_Controls.uc_date_picker
  Friend WithEvents uc_multicurrency As GUI_Controls.uc_multi_currency_site_sel
End Class
