'-------------------------------------------------------------------
' Copyright � 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_lottery_meters_adjustment_working_day
'
' DESCRIPTION : This screen allows to view the working dat status
'
' CREATION DATE : 15-DEC-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-DEC-2017  JBM    Initial version.
' 06-MAR-2018  DPC    Bug 31819:[WIGOS-8689]: AGG - MULTISITE - Billing: When enter in the form Billing and click Enter key without touch the filters, the search not show the message "without results"
' 06-MAR-2018  DPC    Bug 31820:[WIGOS-8690]: AGG - MULTISITE - Billing: In the form Billing the sites control, the buttons size and location are wrong
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Text
Imports System.Data.SqlClient
Imports System.Globalization

Public Class frm_lottery_meters_adjustment_working_day
  Inherits frm_base_sel_edit

#Region " Constants "

  Private Const GRID_LEVELS_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS_COUNT As Integer = 12

  ' GRID
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_NOTICE As Integer = 1
  Private Const GRID_COLUMN_SITE_ID As Integer = 2
  Private Const GRID_COLUMN_WORKING_DAY_INT As Integer = 3
  Private Const GRID_COLUMN_WORKING_DAY As Integer = 4
  Private Const GRID_COLUMN_STATUS As Integer = 5
  Private Const GRID_COLUMN_USER As Integer = 6
  Private Const GRID_COLUMN_LAST_UPDATE_USER_DATETIME As Integer = 7
  Private Const GRID_COLUMN_LAST_UPDATE_METERS_DATETIME As Integer = 8
  Private Const GRID_COLUMN_HAS_NEW_METERS As Integer = 9
  Private Const GRID_COLUMN_STATUS_INT As Integer = 10
  Private Const GRID_COLUMN_SITE_NAME As Integer = 11

  ' SQL
  Private Const SQL_COLUMN_WORKING_DAY_INT As Integer = 0
  Private Const SQL_COLUMN_SITE_ID As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_HAS_NEW_METERS As Integer = 3
  Private Const SQL_COLUMN_LAST_UPDATED_METERS As Integer = 4
  Private Const SQL_COLUMN_LAST_UPDATED_USER As Integer = 5
  Private Const SQL_COLUMN_LAST_UPDATED_USER_DATETIME As Integer = 6
  Private Const SQL_COLUMN_SITE_NAME As Integer = 7

  ' WIDTH GRID COLUMNS
  Private Const GRID_WIDTH_INDEX As Integer = 0
  Private Const GRID_WIDTH_NOTICE As Integer = 200
  Private Const GRID_WIDTH_SITE_ID As Integer = 0
  Private Const GRID_WIDTH_SITE_ID_MULTISITE As Integer = 1000
  Private Const GRID_WIDTH_WORKING_DAY_INT As Integer = 0
  Private Const GRID_WIDTH_WORKING_DAY As Integer = 3500
  Private Const GRID_WIDTH_STATUS As Integer = 2000
  Private Const GRID_WIDTH_USER As Integer = 3500
  Private Const GRID_WIDTH_LAST_UPDATE_USER_DATETIME As Integer = 3500
  Private Const GRID_WIDTH_LAST_UPDATE_METERS_DATETIME As Integer = 0
  Private Const GRID_WIDTH_HAS_NEW_METERS As Integer = 0
  Private Const GRID_WIDTH_STATUS_INT As Integer = 0
  Private Const GRID_WIDTH_SITE_NAME As Integer = 0

#End Region ' Constants

#Region " Enumerates "

#End Region

#Region " Members "
  Private m_is_multisite As Boolean

  Private m_site_name As String

  Private m_query_as_datatable As DataTable

  Private m_closing_time As Int32

#End Region ' Members

#Region "Overrides"
  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Get values
    Call GetInitialValues()

    ' Initialize Form Controls
    ' - Form Text
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8932)

    ' - Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)  'EXIT
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8943)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Sites
    If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      'Call Me.FindSitesName()
      Me.uc_sites_sel1.ShowMultisiteRow = False
      Me.uc_sites_sel1.Init()
    Else
      Me.uc_sites_sel1.Visible = False

    End If

    ' Dates
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True, False)
    Me.uc_dsl.FromDateText = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.uc_dsl.ToDateText = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.uc_dsl.ClosingTime = m_closing_time

    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(830) '830 "Date Range"
    Me.rd_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8944)
    Me.rd_last_update.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8945)

    Me.opt_all_users.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    Me.opt_one_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8946)
    Me.gb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)

    ' Shows
    Me.gb_shows.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(702)
    Me.cb_closed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8947)
    Me.cb_opened.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8948)

    ' Change location site mode
    If (Not m_is_multisite) Then
      Me.uc_sites_sel1.Visible = False    ' Sites not visible

      Me.gb_date.Location = New System.Drawing.Point(6, 6)
      Me.gb_shows.Location = New System.Drawing.Point(403, 101)
      Me.gb_user.Location = New System.Drawing.Point(403, 6)
      Me.panel_filter.Size = New Size(1100, 150)
    End If

    SetDefaultValues()

    GUI_StyleSheet()
  End Sub 'GUI_InitControls

  'Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

  'End Sub ' GUI_DB_Operation  
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        ' Reopen working day if the user have permissions
        If CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_REOPEN_WORKING_DAY).Read Then
          If ReopenGamingDay() Then
            AuditChanges()
            MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
          End If
        Else
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8949), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case ENUM_BUTTON.BUTTON_SELECT
        If Not Me.Grid.SelectedRows Is Nothing AndAlso Me.Grid.SelectedRows.Length > 0 Then
          ShowTerminalForm()
        End If

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        MyBase.GUI_ButtonClick(ButtonId)
        If Me.Grid.NumRows > 0 Then
          Me.GUI_RowSelectedEvent(0)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter
    If rd_working_day.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830), " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8944))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(830), " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8945))
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(311), Me.uc_dsl.FromDate)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(312), Me.uc_dsl.ToDate)

    If Not Me.cb_opened.Checked And Not Me.cb_closed.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(702), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1313))
    ElseIf Me.cb_opened.Checked And Me.cb_closed.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(702), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1313))
    ElseIf Me.cb_opened.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(702), Me.cb_opened.Text)
    ElseIf Me.cb_closed.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(702), Me.cb_closed.Text)
    End If

    If Me.opt_one_user.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(603), Me.cmb_user.TextValue)
    ElseIf Me.opt_all_users.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(603), Me.opt_all_users.Text)
    End If

  End Sub

  ''' <summary>
  ''' Set texts corresponding to the provided filter values for the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' put here your code
  End Sub

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("    SELECT    ED_WORKING_DAY                    ")
    _sb.AppendLine("            , ED_SITE_ID                        ")
    _sb.AppendLine("            , ED_STATUS                         ")
    _sb.AppendLine("            , ED_HAS_NEW_METERS                 ")
    _sb.AppendLine("            , ED_LAST_UPDATED_METERS            ")
    _sb.AppendLine("            , GU_USERNAME                       ")
    _sb.AppendLine("            , ED_LAST_UPDATED_USER_DATETIME     ")

    If (m_is_multisite) Then
      _sb.AppendLine("          , ST_NAME                           ")
    End If

    _sb.AppendLine("      FROM    EGM_DAILY                         ")
    _sb.AppendLine(" LEFT JOIN    GUI_USERS                         ")
    _sb.AppendLine("        ON    ED_LAST_UPDATED_USER = GU_USER_ID ")

    If (m_is_multisite) Then
      _sb.AppendLine("INNER JOIN  SITES")
      _sb.AppendLine("        ON  ED_SITE_ID = ST_SITE_ID")
    End If

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine(GetSqlOrderBy())

    ' Set data table
    Me.m_query_as_datatable = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Return _sb.ToString()

  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean ' GUI_SetupRow

    Dim _time As DateTime
    Dim _sql_column_working_day As String
    'Dim _sql_column_last_update_meter_datetime As String

    If IsDBNull(DbRow.Value(SQL_COLUMN_WORKING_DAY_INT)) Or IsDBNull(DbRow.Value(SQL_COLUMN_SITE_ID)) Then
      Return False
    End If

    If Not DbRow.IsNull(SQL_COLUMN_WORKING_DAY_INT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY_INT).Value = DbRow.Value(SQL_COLUMN_WORKING_DAY_INT)
      _sql_column_working_day = DbRow.Value(SQL_COLUMN_WORKING_DAY_INT).ToString()
      _time = DateTime.ParseExact(_sql_column_working_day, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_WORKING_DAY).Value = GUI_FormatDate(_time, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_SITE_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = DbRow.Value(SQL_COLUMN_SITE_ID)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS_INT).Value = DbRow.Value(SQL_COLUMN_STATUS)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GetStatusDescription(DbRow.Value(SQL_COLUMN_STATUS))
    End If

    If Not DbRow.IsNull(SQL_COLUMN_HAS_NEW_METERS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HAS_NEW_METERS).Value = DbRow.Value(SQL_COLUMN_HAS_NEW_METERS)
      'If Me.Grid.Cell(RowIndex, GRID_COLUMN_HAS_NEW_METERS).Value = True Then
      '  Me.Grid.Row(RowIndex).BackColor = Color.IndianRed

      'End If
    End If

    If Not DbRow.IsNull(SQL_COLUMN_LAST_UPDATED_METERS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_UPDATE_METERS_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_UPDATED_METERS), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_LAST_UPDATED_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER).Value = DbRow.Value(SQL_COLUMN_LAST_UPDATED_USER)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_LAST_UPDATED_USER_DATETIME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_UPDATE_USER_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_UPDATED_USER_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                                                                               ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If (m_is_multisite) Then
      If Not DbRow.IsNull(SQL_COLUMN_SITE_NAME) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_NAME).Value = DbRow.Value(SQL_COLUMN_SITE_NAME)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_NAME).Value = m_site_name
    End If

    Return True

  End Function

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    If (SelectedRow < 0) Then
      Return
    End If

    If Me.Grid.Cell(SelectedRow, GRID_COLUMN_STATUS_INT).Value = WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    End If
  End Sub

  ' PURPOSE: Select first row only if grid has more than one row
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Return Me.Grid.NumRows > 0
  End Function ' GUI_SelectFirstRow

#End Region ' Overrides

#Region " Private Functions"
  ''' <summary>
  ''' Get form initial values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetInitialValues()
    ' Is multisite?
    m_is_multisite = WSI.Common.Misc.IsCenter

    ' Get site name if not is center
    m_site_name = String.Empty
    If (Not m_is_multisite) Then
      m_site_name = WSI.Common.Misc.SiteName
    End If

    m_closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1)
  End Sub

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Me.Grid.Sortable = True
      .Init(GRID_COLUMNS_COUNT, GRID_LEVELS_HEADER_ROWS, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' NOTICE
      .Column(GRID_COLUMN_NOTICE).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_NOTICE).Width = GRID_WIDTH_NOTICE
      .Column(GRID_COLUMN_NOTICE).IsColumnSortable = True
      .Column(GRID_COLUMN_NOTICE).IsColumnPrintable = False

      ' WORKING DAY INT
      .Column(GRID_COLUMN_WORKING_DAY_INT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_WORKING_DAY_INT).Width = GRID_WIDTH_WORKING_DAY_INT

      ' WORKING DAY
      .Column(GRID_COLUMN_WORKING_DAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
      .Column(GRID_COLUMN_WORKING_DAY).Width = GRID_WIDTH_WORKING_DAY
      .Column(GRID_COLUMN_WORKING_DAY).IsColumnSortable = True
      .Column(GRID_COLUMN_WORKING_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' STATUS
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).IsColumnSortable = True

      ' USER
      .Column(GRID_COLUMN_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
      .Column(GRID_COLUMN_USER).Width = GRID_WIDTH_USER
      .Column(GRID_COLUMN_USER).IsColumnSortable = True

      ' LAST UPDATE DATETIME
      .Column(GRID_COLUMN_LAST_UPDATE_USER_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_COLUMN_LAST_UPDATE_USER_DATETIME).Width = GRID_WIDTH_LAST_UPDATE_USER_DATETIME
      .Column(GRID_COLUMN_LAST_UPDATE_USER_DATETIME).IsColumnSortable = True
      .Column(GRID_COLUMN_LAST_UPDATE_USER_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' LAST UPDATE METERS DATETIME
      .Column(GRID_COLUMN_LAST_UPDATE_METERS_DATETIME).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_LAST_UPDATE_METERS_DATETIME).Width = GRID_WIDTH_LAST_UPDATE_METERS_DATETIME
      .Column(GRID_COLUMN_LAST_UPDATE_METERS_DATETIME).IsColumnSortable = True

      ' SITE ID
      .Column(GRID_COLUMN_SITE_ID).IsColumnSortable = True
      .Column(GRID_COLUMN_SITE_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773)     'Site

      If (m_is_multisite) Then
        .Column(GRID_COLUMN_SITE_ID).Width = GRID_WIDTH_SITE_ID_MULTISITE
      Else
        .Column(GRID_COLUMN_SITE_ID).Width = GRID_WIDTH_SITE_ID
      End If

      ' HAS NEW METERS
      .Column(GRID_COLUMN_HAS_NEW_METERS).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_HAS_NEW_METERS).Width = GRID_WIDTH_HAS_NEW_METERS
      .Column(GRID_COLUMN_HAS_NEW_METERS).IsColumnSortable = True

      ' STATUS INT
      .Column(GRID_COLUMN_STATUS_INT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_STATUS_INT).Width = GRID_WIDTH_STATUS_INT

      ' SITE NAME
      .Column(GRID_COLUMN_SITE_NAME).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SITE_NAME).Width = GRID_WIDTH_SITE_NAME
    End With
  End Sub

  ' PURPOSE: Sets the default date values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ' Users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_FULL_NAME + ' - [' + GU_USERNAME + ']' AS GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_TYPE = " & WSI.Common.GU_USER_TYPE.USER & " ORDER BY GU_USERNAME")
    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False

    ' Date
    Me.rd_working_day.Checked = True

    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDate = Today
    Me.uc_dsl.FromDate = New Date(Today.Year, Today.Month, 1)
    Me.uc_dsl.ClosingTime = m_closing_time

    ' Shows
    Me.cb_closed.Checked = False
    Me.cb_opened.Checked = False

    Me.uc_dsl.ClosingTime = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1)

    Uc_sites_sel1.SetFirstValue()

  End Sub

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _sb_where As StringBuilder
    Dim _sites As String

    _sb_where = New StringBuilder()
    _sb_where.AppendLine("WHERE ED_WORKING_DAY >= " & Integer.Parse(Me.uc_dsl.FromDate.ToString("yyyyMMdd")))
    _sb_where.AppendLine("  AND ED_WORKING_DAY <= " & Integer.Parse(Me.uc_dsl.ToDate.ToString("yyyyMMdd")))

    If opt_one_user.Checked Then
      _sb_where.AppendLine("  AND ED_LAST_UPDATED_USER = " & Me.cmb_user.Value)
    End If

    If (m_is_multisite) Then
      _sites = Me.uc_sites_sel1.GetSitesIdListSelected()

      If (Not String.IsNullOrEmpty(_sites)) Then
        _sb_where.AppendLine("  AND ED_SITE_ID IN (" & _sites & ")")
      End If
    End If

    If ((Me.cb_opened.Checked) And (Not Me.cb_closed.Checked)) Then
      _sb_where.AppendLine("  AND ED_STATUS IN (0, 2)")       '0: Open; 2: In progress
    ElseIf ((Me.cb_closed.Checked) And (Not Me.cb_opened.Checked)) Then
      _sb_where.AppendLine("  AND ED_STATUS = 1")             '1: Closed
    End If

    If Me.rd_last_update.Checked Then
      _sb_where.AppendLine("  AND ED_LAST_UPDATED_USER_DATETIME IS NOT NULL")
    End If

    Return _sb_where.ToString()
  End Function

  ''' <summary>
  ''' Returns the SQL order by
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlOrderBy() As String
    Dim _sql_order As String

    If Me.rd_working_day.Checked Then
      _sql_order = "   ORDER BY   ED_WORKING_DAY  ASC"
    ElseIf Me.rd_last_update.Checked Then
      _sql_order = "   ORDER BY   ED_LAST_UPDATED_USER_DATETIME  ASC"
    Else
      _sql_order = ""
    End If

    Return _sql_order

  End Function

  Private Sub ShowTerminalForm()
    Dim _frm As frm_lottery_meters_adjustment_terminals

    _frm = New frm_lottery_meters_adjustment_terminals(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TERMINALS)

    _frm.ShowForEdit(Integer.Parse(Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_SITE_ID).Value), Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_SITE_ID).Value + _
                     " - " + Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_SITE_NAME).Value, Integer.Parse(Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_WORKING_DAY_INT).Value))

    If _frm.SaveAlmostOnce Then
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Function ReopenGamingDay() As Boolean
    Dim _row_aux As DataRow
    Dim _datetime_from As DateTime
    Dim _list_days As List(Of Int64())
    Dim _dt_by_period As New DataTable

    _list_days = Nothing

    Try
      _datetime_from = CType(Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_WORKING_DAY).Value, DateTime).AddHours(m_closing_time)

      Using _db_trx As DB_TRX = New DB_TRX()
        If Not cls_lottery.GetMetersPeriod(_datetime_from, EGMMeterAdjustment.EGMWorkingDayStatus.OPEN, Int32.Parse(Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_SITE_ID).Value),
                                           _db_trx.SqlTransaction, _dt_by_period) Then

          Return False
        End If

        If _dt_by_period.Rows.Count > 0 Then
          _list_days = New List(Of Int64())

          For i As Int32 = 0 To _dt_by_period.Rows.Count - 1
            _row_aux = _dt_by_period.NewRow
            _row_aux.ItemArray = _dt_by_period.Rows(i).ItemArray.Clone

            _list_days.Add({_row_aux.Item("EMP_WORKING_DAY"), _row_aux.Item("emp_terminal_id"), _row_aux.Item("emp_site_id")})

            _row_aux.Item("EMP_WORKING_DAY") = CType(WSI.Common.Misc.Opening(_dt_by_period.Rows(i).Item("EMP_DATETIME")).ToString("yyyyMMdd"), Integer)

            _list_days.Add({_row_aux.Item("EMP_WORKING_DAY"), _row_aux.Item("emp_terminal_id"), _row_aux.Item("emp_site_id")})

            _dt_by_period.Rows.Add(_row_aux)
            _dt_by_period.Rows(i).Delete()
          Next

          If Not cls_lottery.DB_SavePendingChangesByPeriod(_dt_by_period, _db_trx.SqlTransaction) Then

            Return False
          End If

          For Each _days As Int64() In _list_days
            If Not cls_lottery.RecalculatedMetersDay(_days, _db_trx.SqlTransaction) Then

              Return False
            End If
          Next

        End If

        If Not cls_lottery.UpdateStatusWorkingDay(EGMMeterAdjustment.EGMWorkingDayStatus.OPEN, CurrentUser.Id,
                                                  Int64.Parse(Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_WORKING_DAY_INT).Value),
                                                  Int64.Parse(Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_SITE_ID).Value), 0, _db_trx.SqlTransaction) Then

          Return False
        End If

        If ((Not _list_days Is Nothing) AndAlso (_list_days.Count > 0)) Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8988), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_WORKING_DAY).Value)
        End If

        TerminalsRecount(_db_trx.SqlTransaction)

        _db_trx.Commit()

        m_query_as_datatable.Rows(Grid.CurrentRow)(SQL_COLUMN_STATUS) = WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus.OPEN

      End Using

      Return True

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return False
  End Function

  ' PURPOSE: Recount terminals when a working day is reopened
  '
  '  PARAMS:
  '     - INPUT:
  '           - DB_TRX
  '     - OUTPUT:
  '           - Boolean with the result of transaction
  '
  ' RETURNS:
  '     - None 
  '
  Private Function TerminalsRecount(ByVal db_transaction As SqlTransaction) As Boolean

    Dim _sb As StringBuilder

    _sb = New StringBuilder

    Try
      _sb.AppendLine(" UPDATE    EGM_DAILY                                                                                                                                            ")
      _sb.AppendLine("    SET    ED_TERMINALS_NUMBER =                                                                                                                                ")
      _sb.AppendLine("        (                                                                                                                                                       ")
      _sb.AppendLine("           SELECT    COUNT(TE_NAME)                                                                                                                             ")
      _sb.AppendLine("             FROM    TERMINALS                                                                                                                                  ")
      _sb.AppendLine("            WHERE    TE_TYPE = 1                                                                                                                                ")
      _sb.AppendLine("              AND    TE_TERMINAL_TYPE IN (1, 3, 5)                                                                                                              ")
      _sb.AppendLine("              AND    TE_ACTIVATION_DATE < DATEADD(DAY, 1, DBO.OPENING(0, CONVERT(DATETIME, CONVERT(DATETIME, (CONVERT(VARCHAR(8), @PARAM_INT_DATETIME))))))     ")
      _sb.AppendLine("              AND  ( TE_RETIREMENT_DATE IS NULL                                                                                                                 ")
      _sb.AppendLine("                   OR  TE_RETIREMENT_DATE >= DATEADD(DAY, 1, DBO.OPENING(0, CONVERT(DATETIME, CONVERT(DATETIME, (CONVERT(VARCHAR(8), @PARAM_INT_DATETIME))))))  ")
      _sb.AppendLine("                   )                                                                                                                                            ")
      _sb.AppendLine("              AND    TE_TERMINAL_ID IN (SELECT DISTINCT EMD_TERMINAL_ID FROM EGM_METERS_BY_DAY WHERE EMD_WORKING_DAY = @PARAM_INT_DATETIME)                     ")

      If (m_is_multisite) Then
        _sb.AppendLine("              AND     TE_SITE_ID = @pSiteId                                                                                                                   ")
      End If

      _sb.AppendLine("       )                                                                                                                                                        ")

      _sb.AppendLine("  WHERE    ED_WORKING_DAY = @PARAM_INT_DATETIME                                                                                                                 ")

      If (m_is_multisite) Then
        _sb.AppendLine("    AND    ED_SITE_ID = @pSiteId                                                                                                                              ")
      End If

      Using _sql_cmd As SqlCommand = New SqlCommand(_sb.ToString(), db_transaction.Connection, db_transaction)
        _sql_cmd.Parameters.Add("@PARAM_INT_DATETIME", SqlDbType.Int).Value = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_WORKING_DAY_INT).Value
        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_SITE_ID).Value

        If _sql_cmd.ExecuteNonQuery() = 1 Then
          Return True
        End If
      End Using
    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return False
  End Function

  Private Function GetStatusDescription(Status As Integer) As String
    Select Case Status
      Case WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus.OPEN
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2742)

      Case WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus.IN_PROGRESS
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(8950)

      Case WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4350)

      Case Else
        Return String.Empty
    End Select
  End Function

  ' PURPOSE: Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None 
  '
  Private Function AuditChanges() As Boolean
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _old_status As Integer
    Dim _current_status As Integer
    Dim _original_status As String
    Dim _final_status As String

    _original_status = String.Empty
    _final_status = String.Empty

    Try
      _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_EGM_METERS)

      '' Set Name and Identifier
      _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8932), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8813))

      For _idx_row As Integer = 0 To m_query_as_datatable.Rows.Count - 1
        ' Old status
        _old_status = IIf(IsDBNull(m_query_as_datatable.Rows(_idx_row)(SQL_COLUMN_STATUS, DataRowVersion.Original)), _
            AUDIT_NONE_STRING, m_query_as_datatable.Rows(_idx_row)(SQL_COLUMN_STATUS, DataRowVersion.Original))

        ' Old status name
        _original_status = GetStatusDescription(m_query_as_datatable.Rows(_idx_row)(SQL_COLUMN_STATUS, DataRowVersion.Original))

        ' Final status
        _current_status = IIf(IsDBNull(m_query_as_datatable.Rows(_idx_row)(SQL_COLUMN_STATUS)), _
            AUDIT_NONE_STRING, m_query_as_datatable.Rows(_idx_row)(SQL_COLUMN_STATUS))

        ' Final ststus name
        _final_status = GetStatusDescription(m_query_as_datatable.Rows(_idx_row)(SQL_COLUMN_STATUS))

        If (_old_status <> _current_status) Then
          _curr_auditor.SetField(0, Me.Grid.Cell(_idx_row, GRID_COLUMN_WORKING_DAY).Value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)) ' Working Day
          '_curr_auditor.SetField(0, _current_status & "(xAntes :" & _old_status & ")", "xStatus")
          '_curr_auditor.SetField(0, CurrentUser.Name & "(xAntes : " & Me.Grid.Cell(_idx_row, SQL_COLUMN_LAST_UPDATED_USER).Value & ")", "xUser")
          '_curr_auditor.SetField(0, GUI_FormatDate(Date.Today, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) & "(xAntes : " & Me.Grid.Cell(_idx_row, SQL_COLUMN_LAST_UPDATED_USER_DATETIME).Value & ")", "xDate")
          _curr_auditor.SetField(0, _final_status & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548) & ": " & _original_status & ")", GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)) ' Status
        End If
      Next

      ' Notify
      _curr_auditor.Notify(CurrentUser.GuiId, _
                           CurrentUser.Id, _
                           CurrentUser.Name, _
                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                            0)

      Return True
    Catch ex As Exception
      Log.Exception(ex)

    End Try

    Return False

  End Function ' AuditChanges

  ''' <summary>
  ''' Update emd_connected when the session it is closing
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateTerminalCloseSesion(ByVal WorkingDay As DateTime, ByVal Site As Int32, ByVal SqlTrans As SqlTransaction) As Boolean

    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try

      _query.AppendLine(" UPDATE   EGM_METERS_BY_DAY              ")
      _query.AppendLine("    SET   EMD_CONNECTED   = @pConnected  ")
      _query.AppendLine("  WHERE   EMD_WORKING_DAY = @pWorkingDay ")
      _query.AppendLine("    AND   EMD_SITE_ID     = @pSite       ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)

        _cmd.Parameters.Add("@pConnected", SqlDbType.Bit).Value = False
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = Int32.Parse(WorkingDay.ToString("yyyyMMdd"))
        _cmd.Parameters.Add("@pSite", SqlDbType.Int).Value = Site

        _cmd.ExecuteNonQuery()

        Return True
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateTerminalCloseSesion

  ''' <summary>
  ''' Set terminals connected
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="TerminalsConencted"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateTerminalsConnectedDay(ByVal CurrentTerminalRow As DataRow, ByVal SqlTrans As SqlTransaction) As Boolean

    Dim _query As StringBuilder
    Dim _count As Int32

    _query = New StringBuilder()

    Try

      _query.AppendLine(" UPDATE EGM_DAILY                                     ")
      _query.AppendLine("    SET ED_TERMINALS_CONNECTED = @pTerminalsConnected ")
      _query.AppendLine("  WHERE ED_WORKING_DAY         = @pWorkingDay         ")
      _query.AppendLine("    AND ED_SITE_ID             = @pSite               ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)


        _cmd.Parameters.Add("@pTerminalsConnected", SqlDbType.Int).Value = 0
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_WORKING_DAY_INT)
        _cmd.Parameters.Add("@pSite", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_SITE_ID)

        _count = _cmd.ExecuteNonQuery()

        If _count > 1 Then

          Return False
        End If

        Return True
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateTerminalsConnectedDay

#End Region ' Private Functions

#Region " Public Functions "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent() ' Set combo with Users

    MyBase.GUI_SetFormId()
  End Sub

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_WORKING_DAY

    MyBase.GUI_SetFormId()

  End Sub

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub

#End Region ' Public Functions

#Region " Events "
  Private Sub opt_one_user_CheckedChanged(sender As Object, e As EventArgs) Handles opt_one_user.CheckedChanged
    Me.cmb_user.Enabled = True
  End Sub

  Private Sub opt_all_users_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_users.CheckedChanged
    Me.cmb_user.Enabled = False
  End Sub
#End Region ' Events

End Class