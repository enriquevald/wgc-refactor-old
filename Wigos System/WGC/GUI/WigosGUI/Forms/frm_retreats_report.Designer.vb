<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_retreats_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_retreats_report))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_only_report_of_decimals_rounding = New System.Windows.Forms.CheckBox()
    Me.chk_show_details = New System.Windows.Forms.CheckBox()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel()
    Me.ef_winnings_amount = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_winnings_amount)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.chk_show_details)
    Me.panel_filter.Controls.Add(Me.chk_only_report_of_decimals_rounding)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1217, 113)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_report_of_decimals_rounding, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_details, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_winnings_amount, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 117)
    Me.panel_data.Size = New System.Drawing.Size(1217, 545)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1211, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1211, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(242, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_only_report_of_decimals_rounding
    '
    Me.chk_only_report_of_decimals_rounding.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_only_report_of_decimals_rounding.Location = New System.Drawing.Point(840, 20)
    Me.chk_only_report_of_decimals_rounding.Name = "chk_only_report_of_decimals_rounding"
    Me.chk_only_report_of_decimals_rounding.Size = New System.Drawing.Size(234, 17)
    Me.chk_only_report_of_decimals_rounding.TabIndex = 6
    Me.chk_only_report_of_decimals_rounding.Text = "xOnlyReportOfDecimalsRounding"
    Me.chk_only_report_of_decimals_rounding.UseVisualStyleBackColor = True
    Me.chk_only_report_of_decimals_rounding.Visible = False
    '
    'chk_show_details
    '
    Me.chk_show_details.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_show_details.Location = New System.Drawing.Point(6, 89)
    Me.chk_show_details.Name = "chk_show_details"
    Me.chk_show_details.Size = New System.Drawing.Size(234, 18)
    Me.chk_show_details.TabIndex = 2
    Me.chk_show_details.Text = "xShowDetails"
    Me.chk_show_details.UseVisualStyleBackColor = True
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(253, 6)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(272, 72)
    Me.gb_user.TabIndex = 3
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(83, 42)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 38)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(80, 14)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(527, 3)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 5
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'ef_winnings_amount
    '
    Me.ef_winnings_amount.DoubleValue = 0.0R
    Me.ef_winnings_amount.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_winnings_amount.IntegerValue = 0
    Me.ef_winnings_amount.IsReadOnly = False
    Me.ef_winnings_amount.Location = New System.Drawing.Point(251, 82)
    Me.ef_winnings_amount.Name = "ef_winnings_amount"
    Me.ef_winnings_amount.PlaceHolder = Nothing
    Me.ef_winnings_amount.Size = New System.Drawing.Size(272, 25)
    Me.ef_winnings_amount.SufixText = "Sufix Text"
    Me.ef_winnings_amount.SufixTextVisible = True
    Me.ef_winnings_amount.TabIndex = 4
    Me.ef_winnings_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_winnings_amount.TextValue = ""
    Me.ef_winnings_amount.TextWidth = 165
    Me.ef_winnings_amount.Value = ""
    Me.ef_winnings_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_retreats_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1225, 666)
    Me.Name = "frm_retreats_report"
    Me.Text = "frm_retreats_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_show_details As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_report_of_decimals_rounding As System.Windows.Forms.CheckBox
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents ef_winnings_amount As GUI_Controls.uc_entry_field
End Class
