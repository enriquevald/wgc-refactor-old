'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_catalogs_sel
' DESCRIPTION:   Catalog Selection
' AUTHOR:        Sergio Soria
' CREATION DATE: 24-AUG-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-AUG-2015  SDS    Initial version
' 04-NOV-2015  DLL    Bug 6173: Incorrect columns order
' 04-NOV-2015  DLL    Bug 6174: Incorrect NLS
' 11-DEC-2015  GMV    Product Backlog Item 6857:Review Iteración 15: Catalogos
' 08-FEB-2016  DDS    Bug 7820:En el excel generado, se muestran string not found
'-------------------------------------------------------------------- 
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_catalogs_sel
  Inherits frm_base_sel

#Region " Constants "

  Private Const LEN_CATALOG_NAME As Integer = 50

  ' DB Columns
  Private Const SQL_COLUMN_CAT_ID As Integer = 0
  Private Const SQL_COLUMN_TYPE As Integer = 1
  Private Const SQL_COLUMN_NAME As Integer = 2
  Private Const SQL_COLUMN_DESCRIPTION As Integer = 3
  Private Const SQL_COLUMN_ENABLED As Integer = 4

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CAT_ID As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_TYPE As Integer = 3
  Private Const GRID_COLUMN_ENABLED As Integer = 4
  Private Const GRID_COLUMN_DESCRIPTION As Integer = 5

  Private Const GRID_COLUMNS As Integer = 6
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_TYPE As Integer = 3100
  Private Const GRID_WIDTH_NAME As Integer = 3200
  Private Const GRID_WIDTH_DESCRIPTION As Integer = 5800
  Private Const GRID_WIDTH_ENABLED As Integer = 2000


  ' Database codes
  Private Const CATALOG_STATUS_ENABLED As Integer = 1
  Private Const CATALOG_STATUS_DISABLED As Integer = 0

  Private Const CATALOG_TYPE_SYSTEM As Integer = 1
  Private Const CATALOG_TYPE_USER As Integer = 0

  Private Const FORM_DB_MIN_VERSION As Short = 204 'consultar que esto  SDS 24/08/2015

#End Region ' Constants

#Region " Members "

  ' For report filters
  Private m_catalog_name As String
  Private m_catalog_type As String
  Private m_catalog_status As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CATALOGS


    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_AUDITOR.GetString(484)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)

    ' Type
    Me.gb_catalogs_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)

    ' Catalog Type Values
    Me.chk_type_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
    Me.chk_type_system.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2268)

    ' Status
    Me.gb_catalogs_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2539)

    ' Catalog Status Values
    Me.chk_status_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008)
    Me.chk_status_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359)

    'Catalog Name
    Me.ef_catalogs_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)


    ' filters
    Me.ef_catalogs_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_CATALOG_NAME)


    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim catalog_id As Integer
    Dim catalog_name As String
    Dim frm_edit As Object
    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    catalog_id = Me.Grid.Cell(idx_row, GRID_COLUMN_CAT_ID).Value
    catalog_name = Me.Grid.Cell(idx_row, GRID_COLUMN_NAME).Value

    frm_edit = New frm_catalog_edit

    Call frm_edit.ShowEditItem(catalog_id)

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT " & _
              "         [CAT_ID] " & _
              "         ,[CAT_TYPE] " & _
              "         ,[CAT_NAME] " & _
              "         ,[CAT_DESCRIPTION] " & _
              "         ,[CAT_ENABLED] " & _
              "  FROM [DBO].[CATALOGS] "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY CAT_ID ASC"
    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _cat_status As Integer
    Dim _cat_type As Integer
    Dim _str_status As String = ""
    Dim _str_type As String = ""



    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Status
    _cat_status = IIf(DbRow.Value(SQL_COLUMN_ENABLED) = True, 1, 0)

    ' Status names
    Select Case _cat_status
      Case CATALOG_STATUS_ENABLED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6677)

      Case CATALOG_STATUS_DISABLED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6678)

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ENABLED).Value = _str_status

    ' Type
    _cat_type = DbRow.Value(SQL_COLUMN_TYPE)

    ' type names
    Select Case _cat_type
      Case Is >= CATALOG_TYPE_SYSTEM
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6675)

      Case CATALOG_TYPE_USER
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6676)

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = _str_type

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    ' Description
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = DbRow.Value(SQL_COLUMN_DESCRIPTION)



    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_catalogs_name
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call GUI_ShowNewCatalogForm()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2539), m_catalog_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(254), m_catalog_type)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419), m_catalog_name)

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_catalog_name = ""
    m_catalog_status = ""
    m_catalog_type = ""

    ' Filter Catalog STATUS
    ' STATUS Enabled
    If Me.chk_status_enabled.Checked Then
      m_catalog_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6677)
    End If

    ' STATUS Disabled
    If Me.chk_status_disabled.Checked Then
      If m_catalog_status.Length > 0 Then
        m_catalog_status = m_catalog_status & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6678)
      Else
        m_catalog_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6678)
      End If
    End If

    ' Filter Catalog TYPE
    ' TYPE System
    If Me.chk_type_system.Checked Then
      m_catalog_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6675)
    End If

    ' TYPE User
    If Me.chk_type_user.Checked Then
      If m_catalog_type.Length > 0 Then
        m_catalog_type = m_catalog_type & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6676)
      Else
        m_catalog_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6676)
      End If
    End If

    ' Catalog NAME
    m_catalog_name = ef_catalogs_name.Value

  End Sub 'GUI_ReportUpdateFilters

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "



  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX 
      .Column(GRID_COLUMN_INDEX).Header.Text = ""
      .Column(GRID_COLUMN_INDEX).WidthFixed = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' CATALOG Id
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_CAT_ID).Width = 0
      .Column(GRID_COLUMN_CAT_ID).Mapping = SQL_COLUMN_CAT_ID

      ' CATALOG Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6679)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CATALOG Type
      .Column(GRID_COLUMN_TYPE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6680)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CATALOG Status
      .Column(GRID_COLUMN_ENABLED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6681)
      .Column(GRID_COLUMN_ENABLED).Width = GRID_WIDTH_ENABLED
      .Column(GRID_COLUMN_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CATALOG Description
      .Column(GRID_COLUMN_DESCRIPTION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6682)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_type_user.Checked = True
    Me.chk_type_system.Checked = False

    Me.chk_status_enabled.Checked = True
    Me.chk_status_disabled.Checked = False

    Me.ef_catalogs_name.Value = ""

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_status As String = ""
    Dim str_type As String = ""
    Dim str_where_account As String = ""
    Dim where_added As Boolean

    where_added = False

    ' Filter Catalog NAME
    If Me.ef_catalogs_name.Value <> "" Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & GUI_FilterField("CAT_NAME", Me.ef_catalogs_name.Value, False, False, True)
    End If


    ' Filter Catalog STATUS
    str_status = ""
    ' STATUS Enabled
    If Me.chk_status_enabled.Checked Then
      str_status = str_status & " OR CAT_ENABLED = 1 "
    End If

    ' STATUS Disabled
    If Me.chk_status_disabled.Checked Then
      str_status = str_status & " OR CAT_ENABLED = 0 "
    End If

    If str_status.Length > 0 Then
      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If
      str_status = Strings.Right(str_status, Len(str_status) - 4)
      str_where = str_where & " ( " & str_status & " ) "
    End If

    ' Filter Catalog TYPE
    str_type = ""
    ' TYPE Enabled
    If Me.chk_type_system.Checked Then
      str_type = str_type & " OR CAT_TYPE > 0 "
    End If
    ' TYPE Disabled
    If Me.chk_type_user.Checked Then
      str_type = str_type & " OR CAT_TYPE = 0 "
    End If

    If str_type.Length > 0 Then
      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_type = Strings.Right(str_type, Len(str_type) - 4)
      str_where = str_where & " ( " & str_type & " ) "
    End If
    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new draw to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_ShowNewCatalogForm()

    Dim frm_edit As frm_catalog_edit

    frm_edit = New frm_catalog_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_ShowNewCatalogForm

#End Region ' Private Functions

#Region "Events"

#End Region ' Events

End Class ' frm_catalogs_sel
