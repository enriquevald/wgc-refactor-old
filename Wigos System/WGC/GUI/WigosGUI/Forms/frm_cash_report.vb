'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_cash_report
' DESCRIPTION:   Comparison of different cash meters by day
' AUTHOR:        Jos� Mart�nez
' CREATION DATE: 23-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-MAY-2012  JML    Initial version.
' 15-JUN-2012  RCI    Error in date conversion.
' 24-AUG-2012  JAB    DoEvents run each second.
' 29-APR-2013  RBG    Fixed Bug #755: Reset button only clean the filter, not the grid.
' 08-AUG-2013  ICS & RCI    Fixed Bug WIG-103: Imbalance due bank card commissions
' 19-FEB-2014  AMF    Fixed Bug WIG-647: Control minimum version
' 23-OCT-2014  FJC    Fixed Bug WIG-1566: Add Provision to Progressive in statistics (column L)
' 31-OCT-2014  SGB    Fixed Bug WIG-1611: Failed to filter by dates on future activity providers
' 15-DEC-2014  XCD    ONLY IN CASHLESS -- Delete D column (play sessions opened)
' 19-DEC-2014  RCI    Fixed Bug WIG-1860: Open PlaySessions (column D) still in SQL query (must remove in Cashless)
' 05-AUG-2015  ETP    Fixed BUG-3567 Update of filters to report.
' 16-FEB-2015  FAV    Fixed Bug 9347 Added column with machine tickets voided
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_cash_report
  Inherits frm_base_sel

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL As Integer = 2
  Private Const GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE As Integer = 3
  Private Const GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE As Integer = 4
  Private Const GRID_COLUMN_PLAY_SESSIONS_OPEN As Integer = 5
  Private Const GRID_COLUMN_CASH_RESULTS As Integer = 6
  Private Const GRID_COLUMN_OTHER_INCOME As Integer = 7
  Private Const GRID_COLUMN_CREDITS_EXPIRED As Integer = 8
  Private Const GRID_COLUMN_CONVERTED_CREDIT As Integer = 9
  Private Const GRID_COLUMN_HALL_PAYMENTS As Integer = 10
  Private Const GRID_COLUMN_GAMING_TABLE_OPEN As Integer = 11
  Private Const GRID_COLUMN_GAMING_TABLE_CIRCULATION As Integer = 12
  Private Const GRID_COLUMN_PROVISION_TO_PROGRESSIVE As Integer = 13
  Private Const GRID_COLUMN_MACHINE_TICKET_VOIDED As Integer = 14
  Private Const GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE As Integer = 15


  Private Const GRID_COLUMNS As Integer = 16
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const FORM_DB_MIN_VERSION As Short = 183

#End Region ' Constants

#Region " Enums "

#End Region ' Enums 

#Region " Structures "

#End Region ' Structures

#Region " Member "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

  Private m_total_redeemable_value As String
  Private m_total_no_redeemable_value As String

  Private m_date_filter As Date

  Private m_gaming_table_enabled As Boolean
  Private m_is_tito_mode As Boolean

#End Region ' Member

#Region " Overrides "

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASH_REPORT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _str_calcul As String

    Call MyBase.GUI_InitControls()

    m_gaming_table_enabled = GamingTableBusinessLogic.IsGamingTablesEnabled()
    m_is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode()

    Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(486)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True)

    Me.ef_total_redeemable.IsReadOnly = True
    Me.ef_total_no_redeemable.IsReadOnly = True

    Call GUI_StyleSheet()

    Me.ef_total_redeemable.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 15, 2)
    Me.ef_total_no_redeemable.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 15, 2)

    Me.ef_total_redeemable.Text = GLB_NLS_GUI_STATISTICS.GetString(340)
    Me.ef_total_no_redeemable.Text = GLB_NLS_GUI_STATISTICS.GetString(341)

    Me.ef_total_redeemable.Value = ""
    Me.ef_total_no_redeemable.Value = ""

    Me.lbl_captions_a.Text = "A: " & GLB_NLS_GUI_STATISTICS.GetString(330)
    Me.lbl_captions_b.Text = "B: " & GLB_NLS_GUI_STATISTICS.GetString(329)
    Me.lbl_captions_c.Text = "C: " & GLB_NLS_GUI_STATISTICS.GetString(327)

    If m_is_tito_mode Then
      Me.lbl_captions_d.Text = "D: " & GLB_NLS_GUI_STATISTICS.GetString(343)
      Me.lbl_captions_e.Text = "E: " & GLB_NLS_GUI_STATISTICS.GetString(326)
      Me.lbl_captions_f.Text = "F: " & GLB_NLS_GUI_STATISTICS.GetString(342)
      Me.lbl_captions_g.Text = "G: " & GLB_NLS_GUI_STATISTICS.GetString(325)
      Me.lbl_captions_h.Text = "H: " & GLB_NLS_GUI_STATISTICS.GetString(328)
      Me.lbl_captions_i.Text = "I: " & GLB_NLS_GUI_STATISTICS.GetString(324)
      Me.lbl_captions_j.Text = "J: " & GLB_NLS_GUI_STATISTICS.GetString(348)
      Me.lbl_captions_k.Text = "K: " & GLB_NLS_GUI_STATISTICS.GetString(349)
      Me.lbl_captions_l.Text = "L: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799)
      Me.lbl_captions_l.Text = "M: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7123)


      Me.lbl_note.Visible = False

      _str_calcul = "-C-D+E-F-G+H+I"
      If m_gaming_table_enabled Then
        _str_calcul &= "+J-K-L"
      Else
        _str_calcul &= "-L"
        Me.lbl_captions_j.Visible = False
        Me.lbl_captions_k.Visible = False
      End If

      ' FAV 16-FEB-2016
      _str_calcul &= "-M"
    Else
      Me.lbl_captions_d.Text = "D: " & GLB_NLS_GUI_STATISTICS.GetString(326)
      Me.lbl_captions_e.Text = "E: " & GLB_NLS_GUI_STATISTICS.GetString(342)
      Me.lbl_captions_f.Text = "F: " & GLB_NLS_GUI_STATISTICS.GetString(325)
      Me.lbl_captions_g.Text = "G: " & GLB_NLS_GUI_STATISTICS.GetString(328)
      Me.lbl_captions_h.Text = "H: " & GLB_NLS_GUI_STATISTICS.GetString(324)
      Me.lbl_captions_i.Text = "I: " & GLB_NLS_GUI_STATISTICS.GetString(348)
      Me.lbl_captions_j.Text = "J: " & GLB_NLS_GUI_STATISTICS.GetString(349)
      Me.lbl_captions_k.Text = "K: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799)
      'Me.lbl_captions_l.Text = "L: "
      Me.lbl_captions_l.Text = " "
      Me.lbl_captions_l.Visible = False

      Me.lbl_note.Visible = True
      Me.lbl_note.Text = "*: " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5798)

      _str_calcul = "-C+D-E-F+G+H"
      If m_gaming_table_enabled Then
        _str_calcul &= "+I-J-K"
      Else
        _str_calcul &= "-K"
        Me.lbl_captions_j.Visible = False
        Me.lbl_captions_k.Visible = False
      End If
    End If

    _str_calcul &= ": "
    Me.lbl_captions_t.Text = _str_calcul & GLB_NLS_GUI_STATISTICS.GetString(323)

    m_total_redeemable_value = 0
    m_total_no_redeemable_value = 0

    ' Set filter default values
    Call SetDefaultValues()

    Call Me.Grid.Clear()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim to_date As DateTime

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    Else
      to_date = WSI.Common.WGDB.Now
      to_date = New DateTime(to_date.Year, to_date.Month, to_date.Day, Me.uc_dsl.ClosingTime, 0, 0)
      If Me.uc_dsl.FromDate > to_date Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _cash_days_data As TYPE_PROVIDER_CASH_ITEM
    Dim _total_item As TYPE_PROVIDER_CASH_ITEM
    Dim _color As Integer

    Dim _function_name As String
    Dim _date_from_custom As Date
    Dim _date_to_custom As Date
    Dim _total_days As Integer
    Dim _closing_time As String

    Dim _total_redeemable As Decimal
    Dim _totat_not_redeemable As Decimal

    _closing_time = Me.uc_dsl.ClosingTime.ToString("00")

    _function_name = "GUI_ExecuteQueryCustom"

    _date_from_custom = m_date_from
    _date_to_custom = m_date_to
    m_date_filter = m_date_from

    Try

      Call Me.Grid.Clear()
      Me.Grid.Redraw = False

      _total_days = DateDiff(DateInterval.Day, _date_from_custom, _date_to_custom) - 1

      _total_item.cash_date = GLB_NLS_GUI_STATISTICS.GetString(203)
      _total_item.provider_activity_total = 0
      _total_item.provider_activity_no_redeemable = 0
      _total_item.provider_activity_redeemable = 0
      _total_item.play_sessions_open = 0
      _total_item.cash_results = 0
      _total_item.other_income = 0
      _total_item.credits_expired = 0
      _total_item.converted_credit = 0
      _total_item.hall_payments = 0
      _total_item.progressive_provision = 0
      _total_item.redeemable_circulation_increase = 0
      _total_item.machine_tickets_voided = 0

      Using _db_trx As New DB_TRX()

        For _num_day As Integer = 0 To _total_days

          ' JAB 24-AUG-2012: DoEvents run each second.
          If Not GUI_DoEvents(Me.Grid) Then
            Exit Sub
          End If

          m_date_filter = _date_from_custom.AddDays(_num_day)

          _cash_days_data.cash_date = GUI_FormatDate(m_date_filter, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

          GetProvCashData(m_date_filter, m_date_filter.AddDays(1), _closing_time, _db_trx.SqlTransaction, _cash_days_data)

          _total_item.provider_activity_total += _cash_days_data.provider_activity_total
          _total_item.provider_activity_no_redeemable += _cash_days_data.provider_activity_no_redeemable
          _total_item.provider_activity_redeemable += _cash_days_data.provider_activity_redeemable
          _total_item.play_sessions_open += _cash_days_data.play_sessions_open
          _total_item.cash_results += _cash_days_data.cash_results
          _total_item.other_income += _cash_days_data.other_income
          _total_item.credits_expired += _cash_days_data.credits_expired
          _total_item.converted_credit += _cash_days_data.converted_credit
          _total_item.hall_payments += _cash_days_data.hall_payments
          _total_item.progressive_provision += _cash_days_data.progressive_provision
          _total_item.redeemable_circulation_increase += _cash_days_data.redeemable_circulation_increase
          _total_item.machine_tickets_voided += _cash_days_data.machine_tickets_voided

          _color = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
          DrawRow(_cash_days_data, _color)

        Next

        CalculateTotalAssets(_total_redeemable, _totat_not_redeemable, _db_trx.SqlTransaction)
      End Using ' Using _db_trx

      _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
      DrawRow(_total_item, _color)

      ef_total_redeemable.Text = GLB_NLS_GUI_STATISTICS.GetString(340) & " (" & GUI_FormatDate(WSI.Common.WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & ")  "
      ef_total_redeemable.Value = _total_redeemable
      ef_total_no_redeemable.Text = GLB_NLS_GUI_STATISTICS.GetString(341) & " (" & GUI_FormatDate(WSI.Common.WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & ")  "
      ef_total_no_redeemable.Value = _totat_not_redeemable

      m_total_redeemable_value = ef_total_redeemable.Value
      m_total_no_redeemable_value = ef_total_no_redeemable.Value

      Me.Grid.Redraw = True

    Catch _ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(_ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "frm_cash_report", _function_name, _ex.Message)
      Me.Grid.Redraw = True
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(ef_total_redeemable.Text, GUI_FormatCurrency(m_total_redeemable_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))
    PrintData.SetFilter(ef_total_no_redeemable.Text, GUI_FormatCurrency(m_total_no_redeemable_value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_a)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_b)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_c)

    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_d)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_e)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_f)

    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_g)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_h)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_i)

    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_j)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_k)
    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_l)

    Call GUI_FilterByLabel(PrintData, Me.lbl_captions_t)

    PrintData.FilterHeaderWidth(1) = 1000
    PrintData.FilterValueWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 4000
    PrintData.FilterValueWidth(2) = 1300
    PrintData.FilterHeaderWidth(3) = 2100
    PrintData.FilterValueWidth(3) = 200
    PrintData.FilterHeaderWidth(4) = 3500
    PrintData.FilterValueWidth(4) = 1300

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim aux_date As Date

    m_date_from = ""
    m_date_to = ""

    'Date
    m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      If Now.Hour >= Me.uc_dsl.ClosingTime Then
        aux_date = New Date(Now.Year, Now.Month, Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
        aux_date = aux_date.AddDays(1)
      Else
        aux_date = New Date(Now.Year, Now.Month, Now.Day, Me.uc_dsl.ClosingTime, 0, 0)
      End If
      m_date_to = GUI_FormatDate(aux_date, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _str_calcul As String
    Dim _width_total As Integer

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .PanelRightVisible = False

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE).Header(1).Text = "  "
      .Column(GRID_COLUMN_DATE).Width = 1100
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DATE).PrintWidth = 1100

      ' Provider activity total
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL).Header(0).Text = "A"
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(330)
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL).Width = 1550
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Provider activity no redeemable
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE).Header(0).Text = "B"
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(329)
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE).Width = 1450
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Provider activity redeemable
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE).Header(0).Text = "C"
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(327)
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE).Header(1).Alignment() = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE).Width = 1550
      .Column(GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Play sessions open
      .Column(GRID_COLUMN_PLAY_SESSIONS_OPEN).Header(0).Text = "D"
      .Column(GRID_COLUMN_PLAY_SESSIONS_OPEN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(343)
      .Column(GRID_COLUMN_PLAY_SESSIONS_OPEN).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PLAY_SESSIONS_OPEN).Width = IIf(m_is_tito_mode, 1450, 0)
      .Column(GRID_COLUMN_PLAY_SESSIONS_OPEN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cash results
      .Column(GRID_COLUMN_CASH_RESULTS).Header(0).Text = IIf(m_is_tito_mode, "E", "D")
      .Column(GRID_COLUMN_CASH_RESULTS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(326)
      .Column(GRID_COLUMN_CASH_RESULTS).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CASH_RESULTS).Width = 1650
      .Column(GRID_COLUMN_CASH_RESULTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Card deposits
      .Column(GRID_COLUMN_OTHER_INCOME).Header(0).Text = IIf(m_is_tito_mode, "F", "E")
      .Column(GRID_COLUMN_OTHER_INCOME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(342)
      .Column(GRID_COLUMN_OTHER_INCOME).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_OTHER_INCOME).Width = 1450
      .Column(GRID_COLUMN_OTHER_INCOME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Credits expired
      .Column(GRID_COLUMN_CREDITS_EXPIRED).Header(0).Text = IIf(m_is_tito_mode, "G", "F")
      .Column(GRID_COLUMN_CREDITS_EXPIRED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(325)
      .Column(GRID_COLUMN_CREDITS_EXPIRED).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CREDITS_EXPIRED).Width = 1650
      .Column(GRID_COLUMN_CREDITS_EXPIRED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Converted credit
      .Column(GRID_COLUMN_CONVERTED_CREDIT).Header(0).Text = IIf(m_is_tito_mode, "H", "G")
      .Column(GRID_COLUMN_CONVERTED_CREDIT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(328)
      .Column(GRID_COLUMN_CONVERTED_CREDIT).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CONVERTED_CREDIT).Width = 1450
      .Column(GRID_COLUMN_CONVERTED_CREDIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Hall payments
      .Column(GRID_COLUMN_HALL_PAYMENTS).Header(0).Text = IIf(m_is_tito_mode, "I", "H")
      .Column(GRID_COLUMN_HALL_PAYMENTS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(324)
      .Column(GRID_COLUMN_HALL_PAYMENTS).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_HALL_PAYMENTS).Width = 1550
      .Column(GRID_COLUMN_HALL_PAYMENTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Gaming table open
      .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Header(0).Text = IIf(m_is_tito_mode, "J", "I")
      .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(323)
      .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Width = 1800
      .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Gaming table Circulation
      .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Header(0).Text = IIf(m_is_tito_mode, "K", "J")
      .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(323)
      .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Width = 1800
      .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Provision to Progressive
      .Column(GRID_COLUMN_PROVISION_TO_PROGRESSIVE).Header(0).Text = IIf(m_is_tito_mode, "L", "K")
      .Column(GRID_COLUMN_PROVISION_TO_PROGRESSIVE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5799)
      .Column(GRID_COLUMN_PROVISION_TO_PROGRESSIVE).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PROVISION_TO_PROGRESSIVE).Width = 1800
      .Column(GRID_COLUMN_PROVISION_TO_PROGRESSIVE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Machine tickets voided
      .Column(GRID_COLUMN_MACHINE_TICKET_VOIDED).Header(0).Text = "M"
      .Column(GRID_COLUMN_MACHINE_TICKET_VOIDED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(323)
      .Column(GRID_COLUMN_MACHINE_TICKET_VOIDED).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MACHINE_TICKET_VOIDED).Width = 1800
      .Column(GRID_COLUMN_MACHINE_TICKET_VOIDED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If m_is_tito_mode Then
        _str_calcul = "-C-D+E-F-G+H+I"
        _width_total = 1800
        If m_gaming_table_enabled Then
          _str_calcul &= "+J-K-L"
          _width_total = 2100
        Else
          _str_calcul &= "-L"
          .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Width = 0
          .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Width = 0
        End If

        ' FAV 16-FEB-2016
        _str_calcul &= "-M"
      Else
        _str_calcul = "-C+D-E-F+G+H"
        _width_total = 1800
        If m_gaming_table_enabled Then
          _str_calcul &= "+I-J-K"
          _width_total = 2100
        Else
          _str_calcul &= "-K"
          .Column(GRID_COLUMN_GAMING_TABLE_OPEN).Width = 0
          .Column(GRID_COLUMN_GAMING_TABLE_CIRCULATION).Width = 0

        End If
        .Column(GRID_COLUMN_MACHINE_TICKET_VOIDED).Width = 0
      End If

      ' Redeemable circulation increase
      .Column(GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE).Header(0).Text = _str_calcul
      .Column(GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(323)
      .Column(GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE).Header(1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE).Width = 2400
      .Column(GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Sortable = False

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(1 - _final_time.Day)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

  End Sub ' SetDefaultValues 

  ' PURPOSE: Show results to screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - Provider items with data
  '           - color of row
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function DrawRow(ByVal ProviderItem As TYPE_PROVIDER_CASH_ITEM, ByVal Color As Integer) As Boolean

    Dim idx_row As Integer

    With Me.Grid
      .AddRow()
      idx_row = Me.Grid.NumRows - 1

      .Cell(idx_row, GRID_COLUMN_DATE).Value = ProviderItem.cash_date

      .Cell(idx_row, GRID_COLUMN_PROVIDER_ACTIVITY_TOTAL).Value = GUI_FormatCurrency(ProviderItem.provider_activity_total, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_PROVIDER_ACTIVITY_NO_REDEEMABLE).Value = GUI_FormatCurrency(ProviderItem.provider_activity_no_redeemable, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_PROVIDER_ACTIVITY_REDEEMABLE).Value = GUI_FormatCurrency(ProviderItem.provider_activity_redeemable, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_PLAY_SESSIONS_OPEN).Value = GUI_FormatCurrency(ProviderItem.play_sessions_open, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_CASH_RESULTS).Value = GUI_FormatCurrency(ProviderItem.cash_results, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_OTHER_INCOME).Value = GUI_FormatCurrency(ProviderItem.other_income, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_CREDITS_EXPIRED).Value = GUI_FormatCurrency(ProviderItem.credits_expired, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_CONVERTED_CREDIT).Value = GUI_FormatCurrency(ProviderItem.converted_credit, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_HALL_PAYMENTS).Value = GUI_FormatCurrency(ProviderItem.hall_payments, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_GAMING_TABLE_OPEN).Value = GUI_FormatCurrency(ProviderItem.gaming_table_open, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_GAMING_TABLE_CIRCULATION).Value = GUI_FormatCurrency(ProviderItem.gaming_table_circulation, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_PROVISION_TO_PROGRESSIVE).Value = GUI_FormatCurrency(ProviderItem.progressive_provision, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_MACHINE_TICKET_VOIDED).Value = GUI_FormatCurrency(ProviderItem.machine_tickets_voided, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(idx_row, GRID_COLUMN_REDEEMABLE_CIRCULATION_INCREASE).Value = GUI_FormatCurrency(ProviderItem.redeemable_circulation_increase, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Row(idx_row).BackColor = GetColor(Color)

    End With

    Return True

  End Function ' DrawRow

  ' PURPOSE: Set filters to report by label
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - Label
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Sub GUI_FilterByLabel(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA,
                                ByVal Label As System.Windows.Forms.Label
                                )
    Dim _str_filtered() As String

    If Label.Visible Then
      _str_filtered = Label.Text.Split(":")
      PrintData.SetFilter(_str_filtered(1).Trim(), _str_filtered(0))
    End If

  End Sub ' GUI_SetFilter

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

#End Region ' Events

End Class  ' frm_cash_report

