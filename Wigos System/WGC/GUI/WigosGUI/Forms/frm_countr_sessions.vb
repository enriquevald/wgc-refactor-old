﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_countr_sessions.vb
' DESCRIPTION:  Countr Session Report
' AUTHOR:        Alberto Dios Ibáñez
' CREATION DATE: 31-mar-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-MAR-2016  ADI    Initial version
' 07-JUL-2016  AMF    Bug 15249:CountR: El filtro de fecha no es correcto dentro de "Sesiones de kiosko de redención"
' 14-JUL-2016  FJC    Bug 15596:CountR: Falta filtro de terminal en pantalla de sesiones de CountR
' 15-JUL-2016  JBP    Bug 15409:CountR: No se insertan los registros en "Faltantes y sobrantes"
'                     Bug 15626:CountR: No se actualiza la sesión de CountR
' 12-AGO-2016  FJC    Bug 16720:CountR: En las sesiones de Kiosco restar los Tickets en vez de sumar al Esperado
' 18-AGO-2016  FJC    (reopened) Bug 16720:CountR: En las sesiones de Kiosco restar los Tickets en vez de sumar al Esperado. (Faltaba modificar la diferencia)
' 04-JAN-2017  JBP    Bug 22316:CountR: Sesión de CountR duplicada
' 03-JAN-2017  CCG    Bug 22319:CountR: Columna Deposits - Withdrawal demasiado pequeña en formulario de sesiones
'-------------------------------------------------------------------- 

Imports GUI_Controls
Imports GUI_CommonOperations.ModuleDateTimeFormats
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient
Imports System.Globalization

Public Class frm_countr_sessions
  Inherits GUI_Controls.frm_base_sel

#Region "Private properties"

  Private m_report_terminal_name As String
  Private m_report_terminal_code As String
  Private m_terminal_show_location As Boolean
  Private m_report_enabled As String
  Private m_report_status As String
  Private m_report_date_type As String
  Private m_report_from As Date
  Private m_report_from_text As String
  Private m_report_to As Date
  Private m_report_to_text As String
  Private m_report_area_name As String
  Private m_report_zone_name As String
  Private m_report_floor_id As String
  Private m_report_bank_name As String
  Private m_is_from_movements As Boolean
  Private m_date_openning_session As Boolean
  Private m_dbrow As GUI_Controls.frm_base_sel.CLASS_DB_ROW
  Private m_row_index As Integer
  Private m_tickets As Double
  Private m_deposits_amount As Double
  Private m_withdrawals_amount As Double
  Private m_final_amount As Double
  Private m_expected_amount As Double
  Private m_collected_amount As Double
  Private m_difference_amount As Double

  'Params
  Private m_param_enabled As Boolean
  Private m_param_status As Boolean

  Private m_frm_cashier_session_detail As frm_cashier_session_detail
#End Region

#Region " Constants "

  ' Grid Setup
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 19


  ' Terminal
  Private Const GRID_COLUMN_ROW_INDEX As Integer = 0
  Private Const GRID_COLUMN_COUNTR_NAME As Integer = 1
  Private Const GRID_COLUMN_COUNTR_CODE As Integer = 2

  ' Ubication
  Private Const GRID_COLUMN_COUNTR_AREA_ID As Integer = 3
  Private Const GRID_COLUMN_COUNTR_BANK_ID As Integer = 4
  Private Const GRID_COLUMN_COUNTR_FLOOR_ID As Integer = 5
  Private Const GRID_COLUMN_COUNTR_IS_ENABLED As Integer = 6

  Private Const GRID_COLUMN_SESSION_NAME As Integer = 7
  Private Const GRID_COLUMN_OPENING_DATE As Integer = 8
  Private Const GRID_COLUMN_CLOSING_DATE As Integer = 9
  Private Const GRID_COLUMN_STATUS As Integer = 10
  Private Const GRID_COLUMN_DEPOSITS As Integer = 11
  Private Const GRID_COLUMN_CASH_OUT As Integer = 12
  Private Const GRID_COLUMN_FINAL_BALANCE As Integer = 13
  Private Const GRID_COLUMN_TICKETS As Integer = 14
  Private Const GRID_COLUMN_EXPECTED As Integer = 15
  Private Const GRID_COLUMN_COLLECTED As Integer = 16
  Private Const GRID_COLUMN_DIFFERENCE As Integer = 17
  Private Const GRID_COLUMN_SESSION_ID As Integer = 18

  ' Grid Columns Width
  Private Const GRID_COLUMN_ROW_INDEX_WIDTH As Integer = 200
  Private Const GRID_COLUMN_SESSION_NAME_WIDTH As Integer = 3200
  Private Const GRID_COLUMN_OPENING_DATE_WIDTH As Integer = 1900
  Private Const GRID_COLUMN_CLOSING_DATE_WIDTH As Integer = 1900
  Private Const GRID_COLUMN_STATUS_WIDTH As Integer = 1300
  Private Const GRID_COLUMN_COUNTR_NAME_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_COUNTR_CODE_WIDTH As Integer = 800
  Private Const GRID_COLUMN_COUNTR_IS_ENABLED_WIDTH As Integer = 1000

  Private Const GRID_COLUMN_DEPOSITS_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_CASH_OUT_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_COUNTR_AREA_ID_WIDTH As Integer = 1200
  Private Const GRID_COLUMN_COUNTR_BANK_ID_WIDTH As Integer = 1200
  Private Const GRID_COLUMN_COUNTR_FLOOR_ID_WIDTH As Integer = 1200
  Private Const GRID_COLUMN_FINAL_BALANCE_WIDTH As Integer = 2100
  Private Const GRID_COLUMN_TICKETS_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_EXPECTED_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_COLLECTED_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_DIFFERENCE_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_SESSION_ID_WIDTH As Integer = 0


  ' SQL Columns
  Private Const SQL_GRID_COLUMN_ROW_INDEX As Integer = 0
  Private Const SQL_GRID_COLUMN_COUNTR_NAME As Integer = 1
  Private Const SQL_GRID_COLUMN_COUNTR_CODE As Integer = 2
  Private Const SQL_GRID_COLUMN_COUNTR_IS_ENABLED As Integer = 3
  Private Const SQL_GRID_COLUMN_SESSION_ID As Integer = 4
  Private Const SQL_GRID_COLUMN_OPENING_DATE As Integer = 5
  Private Const SQL_GRID_COLUMN_CLOSING_DATE As Integer = 6
  Private Const SQL_GRID_COLUMN_STATUS As Integer = 7
  Private Const SQL_GRID_COLUMN_DEPOSITS As Integer = 8
  Private Const SQL_GRID_COLUMN_CASH_OUT As Integer = 9
  Private Const SQL_GRID_COLUMN_FINAL_BALANCE As Integer = 10
  Private Const SQL_GRID_COLUMN_TICKETS As Integer = 11
  Private Const SQL_GRID_COLUMN_EXPECTED As Integer = 12
  Private Const SQL_GRID_COLUMN_COLLECTED As Integer = 13
  Private Const SQL_GRID_COLUMN_DIFFERENCE As Integer = 14
  'Ubication
  Private Const SQL_GRID_COLUMN_COUNTR_AREA_ID As Integer = 15
  Private Const SQL_GRID_COLUMN_COUNTR_AREA_NAME As Integer = 16
  Private Const SQL_GRID_COLUMN_COUNTR_BANK_ID As Integer = 17
  Private Const SQL_GRID_COLUMN_COUNTR_BANK_NAME As Integer = 18
  Private Const SQL_GRID_COLUMN_COUNTR_FLOOR_ID As Integer = 19

  'CASHIER SESSIONS
  Private Const SQL_GRID_COLUMN_CASHIER_SESSION_NAME As Integer = 20

  'Const SQL Where
  Private Const SQL_WHERE_AND As String = " AND "

#End Region ' Constants

#Region "Constructors"

  ''' <summary>
  ''' New form
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub ' New

#End Region

#Region " Public Methods "

  ''' <summary>
  ''' Show form
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region

#Region "Overrides"

  ''' <summary>
  ''' Open additional form to show details for the select row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _idx_row As Int32
    Dim _session_name As String
    Dim _session_id As Integer
    Dim _frm_cashier_movements As frm_cashier_movements

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next
    _session_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_NAME).Value
    _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value
    If (Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01) Or
         Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)) Then
      Return
    End If

    _frm_cashier_movements = New frm_cashier_movements
    _frm_cashier_movements.ShowForEdit(Me.MdiParent, _session_id, _session_name)


  End Sub ' GUI_EditSelectedItem

  ''' <summary>
  '''  Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    'Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7244)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7287)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Size = New System.Drawing.Size(90, 45)

    ' -- Terminal --
    fra_terminal.Text = GLB_NLS_GUI_AUDITOR.GetString(339)
    ef_terminal_name.Text = GLB_NLS_GUI_AUDITOR.GetString(330)
    ef_terminal_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)
    Call Me.ef_terminal_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Call Me.ef_terminal_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3, 0)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' -- Date --
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.opt_opening_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101)
    Me.opt_movement_session.Text = GLB_NLS_GUI_INVOICING.GetString(454)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.Value = WGDB.Now.AddDays(-1)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.Value = WGDB.Now
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.opt_opening_session.Checked = True

    ' -- Enabled -- 
    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422)
    Me.chk_yes.Text = GLB_NLS_GUI_INVOICING.GetString(479)
    Me.chk_no.Text = GLB_NLS_GUI_INVOICING.GetString(480)

    ' -- State --
    fra_session_state.Text = GLB_NLS_GUI_INVOICING.GetString(152)
    chk_session_open.Text = GLB_NLS_GUI_INVOICING.GetString(153)
    chk_session_closed.Text = GLB_NLS_GUI_INVOICING.GetString(154)

    ' -- Location --
    '   · Area
    Me.gb_area_island.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1166)
    Me.cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)
    '   · Zone
    Me.ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)
    Me.ef_smoking.IsReadOnly = True
    Me.ef_smoking.TabStop = False
    ef_floor_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233)
    '   · Bank
    Me.cmb_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)

    Call Me.ef_floor_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)


    ' Fill combos
    Call AreaComboFill()
    Call BankComboFill()

    Call GUI_StyleSheet()

    ' Add Handles
    Call AddHandles()

    Call SetDefaultValues()
  End Sub 'GUI_InitControls

  ''' <summary>
  ''' Set Focus in a control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_terminal_name
  End Sub 'GUI_SetInitialFocus

  ''' <summary>
  ''' Set form id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COUNTR_SESSIONS
    MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Reset Filters to inital values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()

    m_tickets = 0
    m_deposits_amount = 0
    m_withdrawals_amount = 0
    m_final_amount = 0
    m_expected_amount = 0
    m_collected_amount = 0
    m_difference_amount = 0

  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_RowSelectedEvent(SelectedRow As Integer)
    'MyBase.GUI_RowSelectedEvent(SelectedRow)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = IsValidDataRow(SelectedRow)

  End Sub
  ''' <summary>
  '''Inserts total and subtotal
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow()

    If Me.Grid.NumRows > 1 Then
      With Me.Grid
        m_row_index = m_row_index + 1
        .AddRow()
        .Row(m_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
        .Cell(m_row_index, GRID_COLUMN_SESSION_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047)
        .Cell(m_row_index, GRID_COLUMN_TICKETS).Value = GUI_FormatCurrency(m_tickets, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(m_row_index, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(m_deposits_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(m_row_index, GRID_COLUMN_CASH_OUT).Value = GUI_FormatCurrency(m_withdrawals_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(m_row_index, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(m_final_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(m_row_index, GRID_COLUMN_EXPECTED).Value = GUI_FormatCurrency(m_expected_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(m_row_index, GRID_COLUMN_COLLECTED).Value = GUI_FormatCurrency(m_collected_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(m_row_index, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(m_difference_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End With
    End If

    MyBase.GUI_AfterLastRow()
  End Sub 'GUI_AfterLastRow

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _index As Integer
    Dim _aux As Double
    Dim _session_closed As Boolean
    _index = DbRow.Value(SQL_GRID_COLUMN_ROW_INDEX)
    _session_closed = False
    m_dbrow = DbRow
    m_row_index = RowIndex

    With Me.Grid

      .Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = CInt(DbRow.Value(SQL_GRID_COLUMN_SESSION_ID))
      .Cell(RowIndex, GRID_COLUMN_SESSION_NAME).Value = DbRow.Value(SQL_GRID_COLUMN_CASHIER_SESSION_NAME)

      ' Date 
      .Cell(RowIndex, GRID_COLUMN_OPENING_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_GRID_COLUMN_OPENING_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      If Not String.IsNullOrEmpty(DbRow.Value(SQL_GRID_COLUMN_CLOSING_DATE).ToString()) Then
        .Cell(RowIndex, GRID_COLUMN_CLOSING_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_GRID_COLUMN_CLOSING_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Status
      If Not String.IsNullOrEmpty(DbRow.Value(SQL_GRID_COLUMN_CLOSING_DATE).ToString()) Then
        If DbRow.Value(SQL_GRID_COLUMN_STATUS) = DbRow.Value(SQL_GRID_COLUMN_CLOSING_DATE) Then
          _session_closed = True
          .Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(315) 'Closed
        End If
      Else
        .Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(314)  'Open
      End If

      ' Terminal 
      .Cell(RowIndex, GRID_COLUMN_COUNTR_NAME).Value = DbRow.Value(SQL_GRID_COLUMN_COUNTR_NAME)

      ' Code
      .Cell(RowIndex, GRID_COLUMN_COUNTR_CODE).Value = DbRow.Value(SQL_GRID_COLUMN_COUNTR_CODE)

      ' Area
      If Not String.IsNullOrEmpty(DbRow.Value(SQL_GRID_COLUMN_COUNTR_AREA_ID)) Then
        .Cell(RowIndex, GRID_COLUMN_COUNTR_AREA_ID).Value = DbRow.Value(SQL_GRID_COLUMN_COUNTR_AREA_ID)
      End If

      ' Bank
      If Not String.IsNullOrEmpty(DbRow.Value(SQL_GRID_COLUMN_COUNTR_BANK_ID)) Then
        .Cell(RowIndex, GRID_COLUMN_COUNTR_BANK_ID).Value = DbRow.Value(SQL_GRID_COLUMN_COUNTR_BANK_ID)
      End If

      ' FloorId
      If Not String.IsNullOrEmpty(DbRow.Value(SQL_GRID_COLUMN_COUNTR_FLOOR_ID)) Then
        .Cell(RowIndex, GRID_COLUMN_COUNTR_FLOOR_ID).Value = DbRow.Value(SQL_GRID_COLUMN_COUNTR_FLOOR_ID)
      End If


      ' Enabled
      If DbRow.Value(SQL_GRID_COLUMN_COUNTR_IS_ENABLED) = True Then
        .Cell(RowIndex, GRID_COLUMN_COUNTR_IS_ENABLED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      ElseIf DbRow.Value(SQL_GRID_COLUMN_COUNTR_IS_ENABLED) = False Then
        .Cell(RowIndex, GRID_COLUMN_COUNTR_IS_ENABLED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
      End If

      'Balance Operations
      _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_TICKETS)), 0, DbRow.Value(SQL_GRID_COLUMN_TICKETS))
      m_tickets = m_tickets + _aux
      .Cell(RowIndex, GRID_COLUMN_TICKETS).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


      _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_DEPOSITS)), 0, DbRow.Value(SQL_GRID_COLUMN_DEPOSITS))
      m_deposits_amount = m_deposits_amount + _aux
      .Cell(RowIndex, GRID_COLUMN_DEPOSITS).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_CASH_OUT)), 0, DbRow.Value(SQL_GRID_COLUMN_CASH_OUT))
      m_withdrawals_amount = m_withdrawals_amount + _aux
      .Cell(RowIndex, GRID_COLUMN_CASH_OUT).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_FINAL_BALANCE)), 0, DbRow.Value(SQL_GRID_COLUMN_FINAL_BALANCE))
      m_final_amount = m_final_amount + _aux
      .Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_EXPECTED)), 0, DbRow.Value(SQL_GRID_COLUMN_EXPECTED))
      m_expected_amount = m_expected_amount + _aux
      .Cell(RowIndex, GRID_COLUMN_EXPECTED).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Only when session closed
      If _session_closed Then
        _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_COLLECTED)), 0, DbRow.Value(SQL_GRID_COLUMN_COLLECTED))
        m_collected_amount = m_collected_amount + _aux
        .Cell(RowIndex, GRID_COLUMN_COLLECTED).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        _aux = IIf(IsDBNull(DbRow.Value(SQL_GRID_COLUMN_DIFFERENCE)), 0, DbRow.Value(SQL_GRID_COLUMN_DIFFERENCE))
        m_difference_amount = m_difference_amount + _aux
        .Cell(RowIndex, GRID_COLUMN_DIFFERENCE).Value = GUI_FormatCurrency(_aux, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

    End With

    Return True

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Set texts corresponding to the provided filter values for the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_report_terminal_name = String.Empty
    m_report_terminal_code = String.Empty
    m_report_enabled = String.Empty
    m_report_status = String.Empty
    m_report_date_type = String.Empty
    m_report_area_name = String.Empty
    m_report_zone_name = String.Empty
    m_report_bank_name = String.Empty
    m_report_floor_id = String.Empty
    m_report_from_text = String.Empty
    m_report_to_text = String.Empty

    m_report_from = Nothing
    m_report_to = Nothing

    ' Terminal
    If Not String.IsNullOrEmpty(Me.ef_terminal_name.TextValue) Then
      m_report_terminal_name = Me.ef_terminal_name.TextValue
    Else
      m_report_terminal_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    End If

    If Not String.IsNullOrEmpty(Me.ef_terminal_code.TextValue) Then
      m_report_terminal_code = Me.ef_terminal_code.Value
    Else
      m_report_terminal_code = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    End If

    ' Enabled filter
    If ((Me.chk_yes.Checked And Me.chk_no.Checked) Or (Not Me.chk_yes.Checked And Not Me.chk_no.Checked)) Then
      m_report_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    ElseIf Me.chk_yes.Checked Then
      m_report_enabled = Me.chk_yes.Text
    ElseIf Me.chk_no.Checked Then
      If String.IsNullOrEmpty(m_report_enabled) Then
        m_report_enabled = Me.chk_no.Text
      Else
        m_report_enabled = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    ' Status filter
    If ((Me.chk_session_open.Checked And Me.chk_session_closed.Checked) Or (Not Me.chk_session_open.Checked And Not Me.chk_session_closed.Checked)) Then
      m_report_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    ElseIf Me.chk_session_open.Checked Then
      m_report_status = Me.chk_session_open.Text
    ElseIf Me.chk_session_closed.Checked Then
      If String.IsNullOrEmpty(m_report_status) Then
        m_report_status = Me.chk_session_closed.Text
      Else
        m_report_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
      End If
    End If

    ' Date
    If Me.dtp_from.Checked Or Me.dtp_to.Checked Then

      ' Type
      If Me.opt_opening_session.Checked Then
        m_report_date_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101)
      Else
        m_report_date_type = GLB_NLS_GUI_INVOICING.GetString(454)
      End If

      ' From
      If Me.dtp_from.Checked Then
        m_report_from = Me.dtp_from.Value
        m_report_from_text = GUI_FormatDate(Me.dtp_from.Value, _
                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' To
      If Me.dtp_to.Checked Then
        m_report_to = Me.dtp_to.Value
        m_report_to_text = GUI_FormatDate(Me.dtp_to.Value, _
                                     ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                     ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
    End If

    ' Location filter
    m_report_area_name = Me.cmb_area.TextValue
    m_report_zone_name = Me.ef_smoking.Value
    m_report_bank_name = Me.cmb_bank.TextValue

    'FloorID
    m_report_floor_id = Me.ef_floor_id.Value

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(339), m_report_terminal_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418), m_report_terminal_code)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), m_terminal_show_location)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201), m_report_date_type)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(202), m_report_from_text)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(203), m_report_to_text)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(152), m_report_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3422), m_report_enabled)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), IIf(m_terminal_show_location,
                                                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5304),
                                                                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(5305)))
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(435), m_report_area_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(430), m_report_zone_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(431), m_report_bank_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233), m_report_floor_id)

  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' Get the defined Query Type
  ''' </summary>
  ''' <returns>ENUM_QUERY_TYPE</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ''' <summary>
  ''' Process clicks on data grid (double-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY

        Call GUI_StyleSheet()

        Call MyBase.GUI_ButtonClick(ButtonId)
      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call CloseSession()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ''' <summary>
  '''  SQL Command query ready to send to the database
  ''' </summary>
  ''' <returns>Build an SQL Command query from conditions set in the filters</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _str_sql As StringBuilder

    _sql_command = New SqlCommand()
    _str_sql = New StringBuilder()

    _str_sql.AppendLine("     SELECT   ROW_NUMBER() OVER (ORDER BY CRS_OPENNING_DATE) AS CTRS_ROW_INDEX                                       ")
    _str_sql.AppendLine("            , CR_NAME AS CTRS_NAME                                                                                   ")
    _str_sql.AppendLine("            , CR_CODE AS CTRS_CODE                                                                                   ")
    _str_sql.AppendLine("            , CR_ENABLED AS CTRS_ENABLED                                                                             ")
    _str_sql.AppendLine("            , ISNULL(CRS_CASHIER_SESSION_ID,-1) AS CTRS_SESSION_ID                                                   ")
    _str_sql.AppendLine("            , CRS_OPENNING_DATE AS CTRS_OPENNING_DATE                                                                ")
    _str_sql.AppendLine("            , CRS_CLOSED_DATE AS CRTS_CLOSED_DATE                                                                    ")
    _str_sql.AppendLine("            , ISNULL(CRS_CLOSED_DATE,0) AS CTRS_STATUS                                                               ")
    _str_sql.AppendLine("            , CRS_DEPOSITS_AMOUNT AS CTRS_DEPOSITS_AMOUNT                                                            ")
    _str_sql.AppendLine("            , CRS_WITHDRAWALS_AMOUNT AS CTRS_WITHDRAWALS_AMOUNT                                                      ")
    _str_sql.AppendLine("            , CRS_FINAL_AMOUNT AS CTRS_FINAL_AMOUNT                                                                  ")
    _str_sql.AppendLine("            , CRS_TICKET_AMOUNT AS CTRS_TICKET_AMOUNT                                                                ")
    _str_sql.AppendLine("            , (CRS_FINAL_AMOUNT - CRS_TICKET_AMOUNT) AS CTRS_EXPECTED_AMOUNT                                         ")
    _str_sql.AppendLine("            , CRS_COLLECTED_AMOUNT AS CTRS_COLLECTED_AMOUNT                                                          ")
    _str_sql.AppendLine("            , (CRS_COLLECTED_AMOUNT - (CRS_FINAL_AMOUNT - CRS_TICKET_AMOUNT)) AS CTRS_DIFFERENCE_AMOUNT              ")
    _str_sql.AppendLine("            , AR_AREA_ID AS CTRS_AREA                                                                                ")
    _str_sql.AppendLine("            , AR_NAME AS CTRS_AREA_NAME                                                                              ")
    _str_sql.AppendLine("            , BK_BANK_ID  AS CTRS_BANK_ID                                                                            ")
    _str_sql.AppendLine("            , BK_NAME AS CTRS_BANK_NAME                                                                              ")
    _str_sql.AppendLine("            , CR_FLOOR_ID AS CTRS_FLOOR_ID                                                                           ")
    _str_sql.AppendLine("            , CASHIER_SESSIONS.CS_NAME                                                                               ")
    _str_sql.AppendLine("       FROM   COUNTR                                                                                                 ")
    _str_sql.AppendLine(" INNER JOIN   COUNTR_SESSIONS ON COUNTR.CR_COUNTR_ID = COUNTR_SESSIONS.CRS_COUNTR_ID                                 ")
    _str_sql.AppendLine(" INNER JOIN   AREAS  ON COUNTR.CR_AREA_ID = AREAS.AR_AREA_ID                                                         ")
    _str_sql.AppendLine(" INNER JOIN   BANKS  ON COUNTR.CR_BANK_ID = BANKS.BK_BANK_ID                                                         ")
    _str_sql.AppendLine(" INNER JOIN   CASHIER_SESSIONS ON CRS_CASHIER_SESSION_ID = CASHIER_SESSIONS.CS_SESSION_ID                            ")
    If m_is_from_movements Then
      _str_sql.AppendLine(" INNER JOIN   CASHIER_MOVEMENTS ON COUNTR_SESSIONS.CRS_CASHIER_SESSION_ID = CASHIER_MOVEMENTS.CM_SESSION_ID        ")
    End If
    _str_sql.AppendLine(GetSqlWhere())
    _str_sql.AppendLine("   GROUP BY   CRS_COUNTR_ID                ")
    _str_sql.AppendLine("            , CRS_OPENNING_DATE            ")
    _str_sql.AppendLine("            , CRS_CLOSED_DATE              ")
    _str_sql.AppendLine("            , ISNULL(CRS_CLOSED_DATE,0)    ")
    _str_sql.AppendLine("            , CRS_INITIAL_AMOUNT           ")
    _str_sql.AppendLine("            , CRS_DEPOSITS_AMOUNT          ")
    _str_sql.AppendLine("            , CRS_WITHDRAWALS_AMOUNT       ")
    _str_sql.AppendLine("            , CRS_FINAL_AMOUNT             ")
    _str_sql.AppendLine("            , CRS_TICKET_AMOUNT            ")
    _str_sql.AppendLine("            , CRS_COLLECTED_AMOUNT         ")
    _str_sql.AppendLine("            , CR_NAME                      ")
    _str_sql.AppendLine("            , CR_CODE                      ")
    _str_sql.AppendLine("            , AR_AREA_ID                   ")
    _str_sql.AppendLine("            , AR_NAME                      ")
    _str_sql.AppendLine("            , BK_BANK_ID                   ")
    _str_sql.AppendLine("            , BK_NAME                      ")
    _str_sql.AppendLine("            , CR_ENABLED                   ")
    _str_sql.AppendLine("            , CRS_CASHIER_SESSION_ID       ")
    _str_sql.AppendLine("            , CR_FLOOR_ID                  ")
    _str_sql.AppendLine("            , CASHIER_SESSIONS.CS_NAME     ")
    _str_sql.AppendLine("   ORDER BY   CRS_OPENNING_DATE DESC       ")

    _sql_command.CommandType = CommandType.Text
    _sql_command.CommandText = _str_sql.ToString()

    'Terminal
    If Not String.IsNullOrEmpty(Me.ef_terminal_name.TextValue) Then
      Me.m_report_terminal_name = Me.ef_terminal_name.TextValue
    End If

    If Not String.IsNullOrEmpty(Me.ef_terminal_code.TextValue) Then
      Me.m_report_terminal_code = Me.ef_terminal_code.Value
      _sql_command.Parameters.Add("@pCode", SqlDbType.Int).Value = Int32.Parse(Me.m_report_terminal_code)   'GUI_FormatNumber(Me.m_report_terminal_code)  'Convert.ToInt32(Me.m_report_terminal_code)
    End If

    'Dates
    If (Not Me.dtp_from.Checked) Then
      m_report_from = WGDB.Now.AddMonths(-2)
    Else
      m_report_from = Me.dtp_from.Value
    End If

    If (Not Me.dtp_to.Checked) Then
      m_report_to = WGDB.Now.AddMonths(2)
    Else
      m_report_to = Me.dtp_to.Value
    End If

    _sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = m_report_from
    _sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = m_report_to

    'Area, Bank, etc. 
    If Me.cmb_area.Value > 0 Then
      _sql_command.Parameters.Add("@pAreaID", SqlDbType.Int).Value = Me.cmb_area.Value
    End If

    If Me.cmb_area.Value > 0 Then
      _sql_command.Parameters.Add("@pBankID", SqlDbType.Int).Value = Me.cmb_bank.Value
    End If

    Return _sql_command

  End Function ' GUI_GetSqlCommand

#End Region

#Region "Private Methods"

  ''' <summary>
  ''' Add Handles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddHandles()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' AddHandles

  ''' <summary>
  ''' Area selection
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub cmb_area_ValueChangedEvent()

    'Zone (smoking - no smoking)
    ef_smoking.Value = GetZoneDescription(cmb_area.Value)

    'Banks
    Call BankComboFill()

  End Sub ' cmb_area_ValueChangedEvent

  ''' <summary>
  '''  Returns zone description
  ''' </summary>
  ''' <param name="AreaId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Private Function GetZoneDescription(ByVal AreaId As Integer) As String
    Dim _table As DataTable
    Dim _returnValue As String

    _table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = " & AreaId.ToString(), Integer.MaxValue)

    _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        If _table.Rows.Item(0).Item("AR_SMOKING") Then
          _returnValue = GLB_NLS_GUI_CONFIGURATION.GetString(436)
        End If
      End If
    End If

    Return _returnValue

  End Function ' GetZoneDescription

  ''' <summary>
  ''' Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlWhere() As String

    Dim _str_where As StringBuilder
    Dim _index As Integer
    Dim _str_where_ini As String


    _index = 0

    _str_where = New StringBuilder()
    _str_where_ini = " WHERE "

    _str_where.Append(_str_where_ini)


    ' Terminal
    If (Not String.IsNullOrEmpty(Me.ef_terminal_name.TextValue)) Then
      _str_where.Append(GUI_FilterField("CR_NAME", Me.ef_terminal_name.TextValue, True, False, True))
    End If
    If (Not String.IsNullOrEmpty(Me.ef_terminal_code.TextValue)) Then
      _str_where.Append(IIf(_str_where.ToString() = _str_where_ini, String.Empty, SQL_WHERE_AND) & "CR_CODE = @pCode")
    End If

    _str_where.Append(IIf(_str_where.ToString() = _str_where_ini, String.Empty, SQL_WHERE_AND))

    ' Dates
    If (m_date_openning_session) Then
      If (Me.dtp_from.Checked And Me.dtp_to.Checked) Then
        _str_where.Append("(CRS_OPENNING_DATE >= @pDateFrom AND CRS_OPENNING_DATE <= @pDateTo )")
      ElseIf (Not Me.dtp_from.Checked And Not Me.dtp_to.Checked) Then
        _str_where.Append("(CRS_OPENNING_DATE >= @pDateFrom OR CRS_OPENNING_DATE <= @pDateTo )")
        m_report_from = Me.dtp_from.Value.AddYears(-1)
        m_report_to = Me.dtp_to.Value.AddYears(1)
      ElseIf Me.dtp_from.Checked Then
        _str_where.Append("(CRS_OPENNING_DATE >= @pDateFrom )")
      ElseIf Me.dtp_to.Checked Then
        _str_where.Append("(CRS_OPENNING_DATE <= @pDateTo )")
      End If
    ElseIf (opt_movement_session.Checked) Then
      If (Me.dtp_from.Checked And Me.dtp_to.Checked) Or (Not Me.dtp_from.Checked And Not Me.dtp_to.Checked) Then
        _str_where.Append("(CM_DATE >= @pDateFrom AND CM_DATE <= @pDateTo )")
      ElseIf Me.dtp_from.Checked Then
        _str_where.Append("(CM_DATE >= @pDateFrom )")
      ElseIf Me.dtp_to.Checked Then
        _str_where.Append("(CM_DATE <= @pDateTo )")
      End If
    End If

    'session Enabled
    If (Me.chk_yes.Checked And Me.chk_no.Checked) Or (Not Me.chk_yes.Checked And Not Me.chk_no.Checked) Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " CR_ENABLED IN(0,1)"))
    ElseIf Me.chk_yes.Checked Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " CR_ENABLED =1 "))
    ElseIf Me.chk_no.Checked Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " CR_ENABLED = 0 "))
    End If

    'm_param_status
    If (Me.chk_session_closed.Checked And Me.chk_session_open.Checked) Or (Not Me.chk_session_closed.Checked And Not Me.chk_session_open.Checked) Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " (CRS_CLOSED_DATE  IS NOT NULL OR CRS_CLOSED_DATE IS NULL )"))
    ElseIf (Me.chk_session_closed.Checked) Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " (CRS_CLOSED_DATE  IS NOT NULL) "))
    ElseIf (Me.chk_session_open.Checked) Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " ( CRS_CLOSED_DATE  IS NULL) "))
    End If

    'area
    If Me.cmb_area.Value > 0 Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " ( AR_AREA_ID = @pAreaID) "))
    End If

    ' bank
    If Me.cmb_bank.Value > 0 Then
      _str_where.Append(String.Format("{0} {1}", SQL_WHERE_AND, " ( BK_BANK_ID = @pBankID) "))
    End If

    ' -- Floor Id --
    If Not String.IsNullOrEmpty(Me.ef_floor_id.Value) Then
      _str_where.Append(SQL_WHERE_AND & GUI_FilterField("CR_FLOOR_ID", Me.ef_floor_id.Value, False, False, True))
    End If

    Return _str_where.ToString()

  End Function ' GetSqlWhere

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>.
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    m_report_terminal_name = String.Empty
    m_report_terminal_code = String.Empty
    m_report_from = Nothing
    m_report_to = Nothing
    m_date_openning_session = True
    m_tickets = 0
    m_final_amount = 0
    m_expected_amount = 0
    m_collected_amount = 0
    m_difference_amount = 0
    m_deposits_amount = 0
    m_withdrawals_amount = 0
    m_report_enabled = String.Empty
    m_report_area_name = String.Empty
    m_report_bank_name = String.Empty
    m_report_zone_name = String.Empty
    If Me.opt_movement_session.Checked Then
      m_is_from_movements = True
    Else
      m_is_from_movements = False
    End If

    ' Terminal 
    ef_terminal_name.Value = String.Empty
    ef_terminal_code.Value = String.Empty

    ' Fields
    ef_floor_id.Value = String.Empty

    ' Enabled
    chk_yes.Checked = True
    chk_no.Checked = False

    'Status
    chk_session_open.Checked = True
    chk_session_closed.Checked = False

    ' Location
    cmb_area.SelectedIndex = 0
    cmb_bank.SelectedIndex = 0
    ef_smoking.Value = String.Empty
    ef_smoking.Value = String.Empty
    chk_terminal_location.Checked = False

    'Dates
    _now = WGDB.Now
    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If
    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    ' Opening session
    Me.opt_opening_session.Checked = True

  End Sub 'SetDefaultValues

  ''' <summary>
  ''' Init Area combobox
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AreaComboFill()

    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID, AR_NAME FROM AREAS INNER JOIN BANKS ON BK_AREA_ID = AR_AREA_ID GROUP BY AR_AREA_ID, AR_NAME ORDER BY AR_NAME", Integer.MaxValue)
    Me.cmb_area.Clear()
    Me.cmb_area.Add(_table)

  End Sub ' AreaComboFill

  ''' <summary>
  ''' Init BANKS combo box
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BankComboFill()

    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT BK_BANK_ID, BK_NAME FROM BANKS WHERE BK_AREA_ID = " & cmb_area.Value & " GROUP BY BK_BANK_ID, BK_NAME ORDER BY BK_NAME", Integer.MaxValue)
    Me.cmb_bank.Clear()
    Me.cmb_bank.Add(_table)

  End Sub     ' BankComboFill

  ''' <summary>
  ''' Define layout of all Main Grid Columns 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      ' Index
      .Column(GRID_COLUMN_ROW_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_ROW_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_ROW_INDEX).Width = GRID_COLUMN_ROW_INDEX_WIDTH
      .Column(GRID_COLUMN_ROW_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_ROW_INDEX).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605)
      .Column(GRID_COLUMN_SESSION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(353)
      If Debugger.IsAttached Then
        .Column(GRID_COLUMN_SESSION_ID).Width = GRID_COLUMN_SESSION_NAME_WIDTH
      Else
        .Column(GRID_COLUMN_SESSION_ID).Width = GRID_COLUMN_SESSION_ID_WIDTH
      End If
      .Column(GRID_COLUMN_SESSION_ID).IsColumnPrintable = False

      ' Session Name
      .Column(GRID_COLUMN_SESSION_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605)
      .Column(GRID_COLUMN_SESSION_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(353)
      .Column(GRID_COLUMN_SESSION_NAME).Width = GRID_COLUMN_SESSION_NAME_WIDTH
      .Column(GRID_COLUMN_SESSION_NAME).IsColumnPrintable = True


      ' OPEN DATETIME
      .Column(GRID_COLUMN_OPENING_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605)
      .Column(GRID_COLUMN_OPENING_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(244)
      .Column(GRID_COLUMN_OPENING_DATE).Width = GRID_COLUMN_OPENING_DATE_WIDTH
      .Column(GRID_COLUMN_OPENING_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' CLOSE DATETIME
      .Column(GRID_COLUMN_CLOSING_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605)
      .Column(GRID_COLUMN_CLOSING_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(266)
      .Column(GRID_COLUMN_CLOSING_DATE).Width = GRID_COLUMN_CLOSING_DATE_WIDTH
      .Column(GRID_COLUMN_CLOSING_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' SESSION STATUS
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(605)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(280)
      .Column(GRID_COLUMN_COUNTR_IS_ENABLED).Width = GRID_COLUMN_COUNTR_IS_ENABLED_WIDTH
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' COUNTR NAME
      .Column(GRID_COLUMN_COUNTR_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_COUNTR_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3419)
      .Column(GRID_COLUMN_COUNTR_NAME).Width = GRID_COLUMN_COUNTR_NAME_WIDTH
      .Column(GRID_COLUMN_COUNTR_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' COUNTR CODE
      .Column(GRID_COLUMN_COUNTR_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_COUNTR_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)
      .Column(GRID_COLUMN_COUNTR_CODE).Width = GRID_COLUMN_COUNTR_CODE_WIDTH
      .Column(GRID_COLUMN_COUNTR_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER

      ' COUNTR IS ENABLED
      .Column(GRID_COLUMN_COUNTR_IS_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_COUNTR_IS_ENABLED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      .Column(GRID_COLUMN_COUNTR_IS_ENABLED).Width = GRID_COLUMN_COUNTR_IS_ENABLED_WIDTH
      .Column(GRID_COLUMN_COUNTR_IS_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER_CENTER

      ' COUNTR UBICATION
      ' Area 
      .Column(GRID_COLUMN_COUNTR_AREA_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_COUNTR_AREA_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7230)
      .Column(GRID_COLUMN_COUNTR_AREA_ID).Width = IIf(m_terminal_show_location, GRID_COLUMN_COUNTR_AREA_ID_WIDTH, 0)
      .Column(GRID_COLUMN_COUNTR_AREA_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank
      .Column(GRID_COLUMN_COUNTR_BANK_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_COUNTR_BANK_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7232)
      .Column(GRID_COLUMN_COUNTR_BANK_ID).Width = IIf(m_terminal_show_location, GRID_COLUMN_COUNTR_BANK_ID_WIDTH, 0)
      .Column(GRID_COLUMN_COUNTR_BANK_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' FloorId
      .Column(GRID_COLUMN_COUNTR_FLOOR_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7226)
      .Column(GRID_COLUMN_COUNTR_FLOOR_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7233)
      .Column(GRID_COLUMN_COUNTR_FLOOR_ID).Width = IIf(m_terminal_show_location, GRID_COLUMN_COUNTR_FLOOR_ID_WIDTH, 0)
      .Column(GRID_COLUMN_COUNTR_FLOOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DEPOSITS
      .Column(GRID_COLUMN_DEPOSITS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_DEPOSITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1342)
      .Column(GRID_COLUMN_DEPOSITS).Width = GRID_COLUMN_DEPOSITS_WIDTH
      .Column(GRID_COLUMN_DEPOSITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' CASH OUT
      .Column(GRID_COLUMN_CASH_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_CASH_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(608)
      .Column(GRID_COLUMN_CASH_OUT).Width = GRID_COLUMN_CASH_OUT_WIDTH
      .Column(GRID_COLUMN_CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' DEPOSITS - WITHDRAWALS
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1117)
      .Column(GRID_COLUMN_FINAL_BALANCE).Width = GRID_COLUMN_FINAL_BALANCE_WIDTH
      .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' TICKETS
      .Column(GRID_COLUMN_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_TICKETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7485)
      .Column(GRID_COLUMN_TICKETS).Width = GRID_COLUMN_TICKETS_WIDTH
      .Column(GRID_COLUMN_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' EXPECTED
      .Column(GRID_COLUMN_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7424)
      .Column(GRID_COLUMN_EXPECTED).Width = GRID_COLUMN_EXPECTED_WIDTH
      .Column(GRID_COLUMN_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' COLLECTED
      .Column(GRID_COLUMN_COLLECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7425)
      .Column(GRID_COLUMN_COLLECTED).Width = GRID_COLUMN_COLLECTED_WIDTH
      .Column(GRID_COLUMN_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      ' DIFFERENCE
      .Column(GRID_COLUMN_DIFFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4707)
      .Column(GRID_COLUMN_DIFFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7426)
      .Column(GRID_COLUMN_DIFFERENCE).Width = GRID_COLUMN_DIFFERENCE_WIDTH
      .Column(GRID_COLUMN_DIFFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then

      Return (Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_STATUS).Value) AndAlso Me.Grid.Cell(IdxRow, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(314))
    End If
    Return False
  End Function ' IsValidDataRow

  ''' <summary>
  ''' Open frm_cashier_session_detailm to Close Session for the select row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Sub CloseSession()
    Dim _idx_row As Int32
    Dim _session_name As String
    Dim _session_id As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next
    _session_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_NAME).Value
    _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value
    If (Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01) Or
         Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)) Then
      Return
    End If

    If m_frm_cashier_session_detail Is Nothing Then
      m_frm_cashier_session_detail = New frm_cashier_session_detail()
    End If

    m_frm_cashier_session_detail.m_cashier_session_id = _session_id
    m_frm_cashier_session_detail.m_cage_session_id = 0

    Call m_frm_cashier_session_detail.CloseCountRSession()

  End Sub ' GUI_EditSelectedItem

#End Region

#Region " Events "

  Private Sub opt_movement_session_CheckedChanged(sender As Object, e As EventArgs) Handles opt_movement_session.CheckedChanged
    IIf(Me.opt_movement_session.Checked, m_is_from_movements = True, m_is_from_movements = False)
  End Sub ' opt_movement_session_CheckedChanged

  Private Sub opt_opening_session_CheckedChanged(sender As Object, e As EventArgs) Handles opt_opening_session.CheckedChanged
    IIf(Me.opt_opening_session.Checked, m_date_openning_session = True, m_date_openning_session = False)
  End Sub ' opt_opening_session_CheckedChanged

  Private Sub chk_terminal_location_CheckedChanged(sender As Object, e As EventArgs) Handles chk_terminal_location.CheckedChanged
    m_terminal_show_location = chk_terminal_location.Checked
  End Sub

#End Region



End Class