'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_draws_sel
' DESCRIPTION:   Draws Selection
' AUTHOR:        Raul Cervera
' CREATION DATE: 09-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-JUN-2010  RCI    Initial version
' 02-JAN-2012  JMM    Cash-in 'a posteriori' draw type added
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 04-APR-2013  RRB    Added new column "Number Points" and fixed Defect #682: Added ReportFilter
' 10-APR-2013  RRB    Changed columns Number Prize and Points when null or 0 now will show empty value.
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 03-MAY-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 22-MAY-2013  ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters
' 04-SEP-2013  CCG    Fixed Bug WIG-166: Make visible the print button
' 20-SEP-2013  DRV    Added points per level
' 07-APR-2014  JBP    Added only VIP filter.
' 07-MAY-2014  DRV    Fixed Bug WIG-902: Theres's no selection column and the date column is too small
'-------------------------------------------------------------------- 
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_draws_sel
  Inherits frm_base_sel

#Region " Constants "

  Private Const LEN_DRAW_NAME As Integer = 50

  ' DB Columns
  Private Const SQL_COLUMN_DRAW_ID As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_ENDING_DATE As Integer = 2
  Private Const SQL_COLUMN_LAST_NUMBER As Integer = 3
  Private Const SQL_COLUMN_NUMBER_PRICE As Integer = 4
  Private Const SQL_COLUMN_POINTS_LEVEL1 As Integer = 5
  Private Const SQL_COLUMN_POINTS_LEVEL2 As Integer = 6
  Private Const SQL_COLUMN_POINTS_LEVEL3 As Integer = 7
  Private Const SQL_COLUMN_POINTS_LEVEL4 As Integer = 8
  Private Const SQL_COLUMN_STATUS As Integer = 9
  Private Const SQL_COLUMN_LIMITED As Integer = 10
  Private Const SQL_COLUMN_LIMIT As Integer = 11
  Private Const SQL_COLUMN_TYPE As Integer = 12
  Private Const SQL_COLUMN_VIP As Integer = 13

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DRAW_ID As Integer = 1
  Private Const GRID_COLUMN_ENDING_DATE As Integer = 2
  Private Const GRID_COLUMN_STATUS As Integer = 3
  Private Const GRID_COLUMN_NAME As Integer = 4
  Private Const GRID_COLUMN_TYPE As Integer = 5
  Private Const GRID_COLUMN_NUMBER_PRICE As Integer = 6
  Private Const GRID_COLUMN_LAST_NUMBER As Integer = 7
  Private Const GRID_COLUMN_LIMIT As Integer = 8
  Private Const GRID_COLUMN_POINTS_LEVEL1 As Integer = 9
  Private Const GRID_COLUMN_POINTS_LEVEL2 As Integer = 10
  Private Const GRID_COLUMN_POINTS_LEVEL3 As Integer = 11
  Private Const GRID_COLUMN_POINTS_LEVEL4 As Integer = 12
  Private Const GRID_COLUMN_VIP As Integer = 13

  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_NAME As Integer = 3200
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_NUMBER As Integer = 1500
  Private Const GRID_WIDTH_AMOUNT As Integer = 1400
  Private Const GRID_WIDTH_STATUS As Integer = 1300
  Private Const GRID_WIDTH_LIMIT As Integer = 1100
  Private Const GRID_WIDTH_TYPE As Integer = 3100

  ' Database codes
  Private Const DR_STATUS_OPENED As Integer = 0
  Private Const DR_STATUS_CLOSED As Integer = 1
  Private Const DR_STATUS_DISABLED As Integer = 2

  Private Const FORM_DB_MIN_VERSION As Short = 204

#End Region ' Constants

#Region " Members "

  ' For report filters
  Private m_date_end_from As String
  Private m_date_end_to As String
  Private m_status As String
  Private m_draw_name As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DRAWS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(210)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)

    ' Ending Date
    Me.gb_ending_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356)
    Me.dtp_end_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_end_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_end_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_end_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Status
    Me.gb_draw_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)

    ' Draw Status Values
    Me.chk_draw_opened.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(314)
    Me.chk_draw_closed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(315)
    Me.chk_draw_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)

    ' Draw Name
    Me.ef_draw_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
    Me.ef_draw_name.TextVisible = True

    ' filters
    Me.ef_draw_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_DRAW_NAME)

    ' Only VIP
    Me.chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4804)         ' 4804 "Exclusivo para clientes VIP"

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim draw_id As Integer
    Dim draw_name As String
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    draw_id = Me.Grid.Cell(idx_row, GRID_COLUMN_DRAW_ID).Value
    draw_name = Me.Grid.Cell(idx_row, GRID_COLUMN_NAME).Value

    frm_edit = New frm_draw_edit

    Call frm_edit.ShowEditItem(draw_id, draw_name)

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_end_from.Checked And Me.dtp_end_to.Checked Then
      If Me.dtp_end_from.Value > Me.dtp_end_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_end_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT " & _
              "   DR_ID " & _
              " , DR_NAME " & _
              " , DR_ENDING_DATE " & _
              " , DR_LAST_NUMBER " & _
              " , DR_NUMBER_PRICE " & _
              " , DR_POINTS_LEVEL1 " & _
              " , DR_POINTS_LEVEL2 " & _
              " , DR_POINTS_LEVEL3 " & _
              " , DR_POINTS_LEVEL4 " & _
              " , DR_STATUS " & _
              " , DR_LIMITED " & _
              " , DR_LIMIT " & _
              " , DR_CREDIT_TYPE " & _
              " , DR_VIP " & _
              " FROM DRAWS "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY DR_ENDING_DATE ASC, DR_STARTING_DATE ASC, DR_ID ASC"
    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _dr_status As Integer
    Dim _str_status As String = ""
    Dim _sched_enabled As Boolean = False
    Dim _expiration_enabled As Boolean = False
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Ending Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ENDING_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ENDING_DATE), _
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Status
    _dr_status = DbRow.Value(SQL_COLUMN_STATUS)

    ' Status names
    Select Case _dr_status
      Case DR_STATUS_OPENED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(314)

      Case DR_STATUS_CLOSED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(315)

      Case DR_STATUS_DISABLED
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = _str_status

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    ' Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GetTypeString(DbRow.Value(SQL_COLUMN_TYPE))

    ' Number Price
    If DbRow.Value(SQL_COLUMN_TYPE) = 3 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUMBER_PRICE).Value = ""

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUMBER_PRICE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NUMBER_PRICE), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Number Points level 1
    If IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL1)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL1).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL1).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS_LEVEL1), 0)
    End If
    ' Number Points level 2
    If IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL2)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL2).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL2).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS_LEVEL2), 0)
    End If
    ' Number Points level 3
    If IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL3)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL3).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL3).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS_LEVEL3), 0)
    End If
    ' Number Points level 4
    If IsDBNull(DbRow.Value(SQL_COLUMN_POINTS_LEVEL4)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL4).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS_LEVEL4).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS_LEVEL4), 0)
    End If

    ' VIP
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VIP).Value = _no_text

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_VIP)) Then
      If DbRow.Value(SQL_COLUMN_VIP) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VIP).Value = _yes_text
      End If
    End If

    ' Last Number
    If DbRow.Value(SQL_COLUMN_LAST_NUMBER) = -1 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_NUMBER).Value = ""
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_NUMBER).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_LAST_NUMBER), 0)
    End If

    ' Limit
    If DbRow.Value(SQL_COLUMN_LIMITED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LIMIT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_LIMIT), 0)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LIMIT).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(333)
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_end_from
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call GUI_ShowNewDrawForm()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_end_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_end_to)

    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(261), m_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(319), m_draw_name)

    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_end_from = ""
    m_date_end_to = ""
    m_status = ""
    m_draw_name = ""

    ' ENDING Dates
    If Me.dtp_end_from.Checked Then
      m_date_end_from = GUI_FormatDate(Me.dtp_end_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_end_to.Checked Then
      m_date_end_to = GUI_FormatDate(Me.dtp_end_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Filter Draw STATUS
    ' STATUS Opened
    If Me.chk_draw_opened.Checked Then
      m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(314)
    End If

    ' STATUS Closed
    If Me.chk_draw_closed.Checked Then
      If m_status.Length > 0 Then
        m_status = m_status & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(315)
      Else
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(315)
      End If
    End If

    ' STATUS Disabled
    If Me.chk_draw_disabled.Checked Then
      If m_status.Length > 0 Then
        m_status = m_status & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
      Else
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
      End If
    End If

    ' Draw NAME
    m_draw_name = ef_draw_name.Value

  End Sub 'GUI_ReportUpdateFilters

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  Private Function GetTypeString(ByVal Type As Integer) As String

    Select Case Type
      Case WSI.Common.DrawType.BY_PLAYED_CREDIT
        Return GLB_NLS_GUI_CONFIGURATION.GetString(302)
      Case WSI.Common.DrawType.BY_REDEEMABLE_SPENT
        Return GLB_NLS_GUI_CONFIGURATION.GetString(303)
      Case WSI.Common.DrawType.BY_TOTAL_SPENT
        Return GLB_NLS_GUI_CONFIGURATION.GetString(313)
      Case WSI.Common.DrawType.BY_POINTS
        Return GLB_NLS_GUI_CONFIGURATION.GetString(329)
      Case WSI.Common.DrawType.BY_CASH_IN
        Return GLB_NLS_GUI_CONFIGURATION.GetString(330)
      Case WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
        Return GLB_NLS_GUI_CONFIGURATION.GetString(422)
      Case Else
        Return ""
    End Select

  End Function

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).WidthFixed = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' DRAW Id
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_DRAW_ID).Width = 0
      .Column(GRID_COLUMN_DRAW_ID).Mapping = SQL_COLUMN_DRAW_ID

      ' DRAW Name
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(353)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Draw Type
      .Column(GRID_COLUMN_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_TYPE).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(299)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DRAW Ending Date
      .Column(GRID_COLUMN_ENDING_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_ENDING_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356)
      .Column(GRID_COLUMN_ENDING_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_ENDING_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DRAW Last Number
      .Column(GRID_COLUMN_LAST_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_LAST_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(317)
      .Column(GRID_COLUMN_LAST_NUMBER).Width = GRID_WIDTH_NUMBER
      .Column(GRID_COLUMN_LAST_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DRAW Number Price
      .Column(GRID_COLUMN_NUMBER_PRICE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_NUMBER_PRICE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(318)
      .Column(GRID_COLUMN_NUMBER_PRICE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NUMBER_PRICE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DRAW Points Level 1
      .Column(GRID_COLUMN_POINTS_LEVEL1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)
      .Column(GRID_COLUMN_POINTS_LEVEL1).Header(1).Text = GetCashierPlayerTrackingData("Level01.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL1).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_POINTS_LEVEL1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DRAW Points Level 2
      .Column(GRID_COLUMN_POINTS_LEVEL2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)
      .Column(GRID_COLUMN_POINTS_LEVEL2).Header(1).Text = GetCashierPlayerTrackingData("Level02.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL2).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_POINTS_LEVEL2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DRAW Points Level 3
      .Column(GRID_COLUMN_POINTS_LEVEL3).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)
      .Column(GRID_COLUMN_POINTS_LEVEL3).Header(1).Text = GetCashierPlayerTrackingData("Level03.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL3).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_POINTS_LEVEL3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DRAW Points Level 1
      .Column(GRID_COLUMN_POINTS_LEVEL4).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2622)
      .Column(GRID_COLUMN_POINTS_LEVEL4).Header(1).Text = GetCashierPlayerTrackingData("Level04.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL4).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_POINTS_LEVEL4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' DRAW Vip
      .Column(GRID_COLUMN_VIP).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_VIP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4805) ' 4805 "VIP"
      .Column(GRID_COLUMN_VIP).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_VIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DRAW Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(261)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DRAW Limit
      .Column(GRID_COLUMN_LIMIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      .Column(GRID_COLUMN_LIMIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(334)
      .Column(GRID_COLUMN_LIMIT).Width = GRID_WIDTH_LIMIT
      .Column(GRID_COLUMN_LIMIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_end_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_end_from.Checked = False
    Me.dtp_end_to.Value = Me.dtp_end_from.Value.AddDays(1)
    Me.dtp_end_to.Checked = False

    Me.chk_draw_opened.Checked = True
    Me.chk_draw_closed.Checked = False
    Me.chk_draw_disabled.Checked = False

    Me.ef_draw_name.Value = ""

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_status As String = ""
    Dim str_where_account As String = ""
    Dim where_added As Boolean

    where_added = False

    ' Filter ENDING Dates
    If Me.dtp_end_from.Checked Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & " (DR_ENDING_DATE >= " & GUI_FormatDateDB(dtp_end_from.Value) & ") "
    End If

    If Me.dtp_end_to.Checked Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & " (DR_ENDING_DATE < " & GUI_FormatDateDB(dtp_end_to.Value) & ") "
    End If

    ' Filter Draw NAME
    If Me.ef_draw_name.Value <> "" Then

      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where & GUI_FilterField("DR_NAME", Me.ef_draw_name.Value, False, False, True)
    End If


    ' Filter Draw STATUS
    str_status = ""
    ' STATUS Closed
    If Me.chk_draw_opened.Checked Then
      str_status = str_status & " OR DR_STATUS = 0 "
    End If
    ' STATUS Closed
    If Me.chk_draw_closed.Checked Then
      str_status = str_status & " OR DR_STATUS = 1 "
    End If
    ' STATUS Disabled
    If Me.chk_draw_disabled.Checked Then
      str_status = str_status & " OR DR_STATUS = 2 "
    End If

    If str_status.Length > 0 Then
      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_status = Strings.Right(str_status, Len(str_status) - 4)
      str_where = str_where & " ( " & str_status & " ) "
    End If

    '   - Vip
    If Me.chk_only_vip.Checked Then
      If where_added = False Then
        str_where = str_where & " WHERE "
        where_added = True
      Else
        str_where = str_where & " AND "
      End If

      str_where = str_where + " DR_VIP = 1"
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new draw to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_ShowNewDrawForm()

    Dim frm_edit As frm_draw_edit

    frm_edit = New frm_draw_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_ShowNewDrawForm

#End Region ' Private Functions

#Region "Events"

#End Region ' Events

End Class ' frm_draws_sel
