<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gift_history
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_gift_history))
        Me.gb_request_date = New System.Windows.Forms.GroupBox()
        Me.dtp_to_rd = New GUI_Controls.uc_date_picker()
        Me.dtp_from_rd = New GUI_Controls.uc_date_picker()
        Me.gb_type = New System.Windows.Forms.GroupBox()
        Me.chk_type_redeem_credit = New System.Windows.Forms.CheckBox()
        Me.chk_type_services = New System.Windows.Forms.CheckBox()
        Me.chk_type_draw_numbers = New System.Windows.Forms.CheckBox()
        Me.chk_type_nrc = New System.Windows.Forms.CheckBox()
        Me.chk_type_object = New System.Windows.Forms.CheckBox()
        Me.gb_delivery_date = New System.Windows.Forms.GroupBox()
        Me.dtp_to_dd = New GUI_Controls.uc_date_picker()
        Me.dtp_from_dd = New GUI_Controls.uc_date_picker()
        Me.gb_expire_date = New System.Windows.Forms.GroupBox()
        Me.dtp_to_ed = New GUI_Controls.uc_date_picker()
        Me.dtp_from_ed = New GUI_Controls.uc_date_picker()
        Me.gb_status = New System.Windows.Forms.GroupBox()
        Me.chk_status_canceled = New System.Windows.Forms.CheckBox()
        Me.chk_status_pending = New System.Windows.Forms.CheckBox()
        Me.chk_status_expired = New System.Windows.Forms.CheckBox()
        Me.chk_status_delivered = New System.Windows.Forms.CheckBox()
        Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
        Me.ef_gift_name = New GUI_Controls.uc_entry_field()
        Me.gb_user_request = New System.Windows.Forms.GroupBox()
        Me.chk_show_all_request = New System.Windows.Forms.CheckBox()
        Me.opt_one_user_request = New System.Windows.Forms.RadioButton()
        Me.opt_all_users_request = New System.Windows.Forms.RadioButton()
        Me.cmb_user_request = New GUI_Controls.uc_combo()
        Me.gb_user_delivery = New System.Windows.Forms.GroupBox()
        Me.chk_show_all_delivery = New System.Windows.Forms.CheckBox()
        Me.opt_one_user_delivery = New System.Windows.Forms.RadioButton()
        Me.opt_all_users_delivery = New System.Windows.Forms.RadioButton()
        Me.cmb_user_delivery = New GUI_Controls.uc_combo()
        Me.panel_filter.SuspendLayout()
        Me.panel_data.SuspendLayout()
        Me.pn_separator_line.SuspendLayout()
        Me.gb_request_date.SuspendLayout()
        Me.gb_type.SuspendLayout()
        Me.gb_delivery_date.SuspendLayout()
        Me.gb_expire_date.SuspendLayout()
        Me.gb_status.SuspendLayout()
        Me.gb_user_request.SuspendLayout()
        Me.gb_user_delivery.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_filter
        '
        Me.panel_filter.Controls.Add(Me.gb_user_request)
        Me.panel_filter.Controls.Add(Me.gb_expire_date)
        Me.panel_filter.Controls.Add(Me.gb_user_delivery)
        Me.panel_filter.Controls.Add(Me.gb_delivery_date)
        Me.panel_filter.Controls.Add(Me.ef_gift_name)
        Me.panel_filter.Controls.Add(Me.gb_request_date)
        Me.panel_filter.Controls.Add(Me.gb_type)
        Me.panel_filter.Controls.Add(Me.uc_account_sel1)
        Me.panel_filter.Controls.Add(Me.gb_status)
        Me.panel_filter.Size = New System.Drawing.Size(1219, 221)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_request_date, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.ef_gift_name, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_delivery_date, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_user_delivery, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_expire_date, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_user_request, 0)
        '
        'panel_data
        '
        Me.panel_data.Location = New System.Drawing.Point(4, 225)
        Me.panel_data.Size = New System.Drawing.Size(1219, 420)
        '
        'pn_separator_line
        '
        Me.pn_separator_line.Size = New System.Drawing.Size(1213, 23)
        '
        'pn_line
        '
        Me.pn_line.Size = New System.Drawing.Size(1213, 4)
        '
        'gb_request_date
        '
        Me.gb_request_date.Controls.Add(Me.dtp_to_rd)
        Me.gb_request_date.Controls.Add(Me.dtp_from_rd)
        Me.gb_request_date.Location = New System.Drawing.Point(6, 6)
        Me.gb_request_date.Name = "gb_request_date"
        Me.gb_request_date.Size = New System.Drawing.Size(261, 72)
        Me.gb_request_date.TabIndex = 0
        Me.gb_request_date.TabStop = False
        Me.gb_request_date.Text = "xRequest Date"
        '
        'dtp_to_rd
        '
        Me.dtp_to_rd.Checked = True
        Me.dtp_to_rd.IsReadOnly = False
        Me.dtp_to_rd.Location = New System.Drawing.Point(6, 38)
        Me.dtp_to_rd.Name = "dtp_to_rd"
        Me.dtp_to_rd.ShowCheckBox = True
        Me.dtp_to_rd.ShowUpDown = False
        Me.dtp_to_rd.Size = New System.Drawing.Size(240, 24)
        Me.dtp_to_rd.SufixText = "Sufix Text"
        Me.dtp_to_rd.SufixTextVisible = True
        Me.dtp_to_rd.TabIndex = 1
        Me.dtp_to_rd.TextWidth = 42
        Me.dtp_to_rd.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'dtp_from_rd
        '
        Me.dtp_from_rd.Checked = True
        Me.dtp_from_rd.IsReadOnly = False
        Me.dtp_from_rd.Location = New System.Drawing.Point(6, 14)
        Me.dtp_from_rd.Name = "dtp_from_rd"
        Me.dtp_from_rd.ShowCheckBox = True
        Me.dtp_from_rd.ShowUpDown = False
        Me.dtp_from_rd.Size = New System.Drawing.Size(240, 24)
        Me.dtp_from_rd.SufixText = "Sufix Text"
        Me.dtp_from_rd.SufixTextVisible = True
        Me.dtp_from_rd.TabIndex = 0
        Me.dtp_from_rd.TextWidth = 42
        Me.dtp_from_rd.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'gb_type
        '
        Me.gb_type.Controls.Add(Me.chk_type_redeem_credit)
        Me.gb_type.Controls.Add(Me.chk_type_services)
        Me.gb_type.Controls.Add(Me.chk_type_draw_numbers)
        Me.gb_type.Controls.Add(Me.chk_type_nrc)
        Me.gb_type.Controls.Add(Me.chk_type_object)
        Me.gb_type.Location = New System.Drawing.Point(540, 84)
        Me.gb_type.Name = "gb_type"
        Me.gb_type.Size = New System.Drawing.Size(255, 131)
        Me.gb_type.TabIndex = 6
        Me.gb_type.TabStop = False
        Me.gb_type.Text = "xType"
        '
        'chk_type_redeem_credit
        '
        Me.chk_type_redeem_credit.AutoSize = True
        Me.chk_type_redeem_credit.Location = New System.Drawing.Point(19, 64)
        Me.chk_type_redeem_credit.Name = "chk_type_redeem_credit"
        Me.chk_type_redeem_credit.Size = New System.Drawing.Size(149, 17)
        Me.chk_type_redeem_credit.TabIndex = 2
        Me.chk_type_redeem_credit.Text = "xRedeemable Credits"
        Me.chk_type_redeem_credit.UseVisualStyleBackColor = True
        '
        'chk_type_services
        '
        Me.chk_type_services.AutoSize = True
        Me.chk_type_services.Location = New System.Drawing.Point(19, 110)
        Me.chk_type_services.Name = "chk_type_services"
        Me.chk_type_services.Size = New System.Drawing.Size(82, 17)
        Me.chk_type_services.TabIndex = 4
        Me.chk_type_services.Text = "xServices"
        Me.chk_type_services.UseVisualStyleBackColor = True
        '
        'chk_type_draw_numbers
        '
        Me.chk_type_draw_numbers.AutoSize = True
        Me.chk_type_draw_numbers.Location = New System.Drawing.Point(19, 87)
        Me.chk_type_draw_numbers.Name = "chk_type_draw_numbers"
        Me.chk_type_draw_numbers.Size = New System.Drawing.Size(118, 17)
        Me.chk_type_draw_numbers.TabIndex = 3
        Me.chk_type_draw_numbers.Text = "xDraw Numbers"
        Me.chk_type_draw_numbers.UseVisualStyleBackColor = True
        '
        'chk_type_nrc
        '
        Me.chk_type_nrc.AutoSize = True
        Me.chk_type_nrc.Location = New System.Drawing.Point(19, 41)
        Me.chk_type_nrc.Name = "chk_type_nrc"
        Me.chk_type_nrc.Size = New System.Drawing.Size(172, 17)
        Me.chk_type_nrc.TabIndex = 1
        Me.chk_type_nrc.Text = "xNot Redeemable Credits"
        Me.chk_type_nrc.UseVisualStyleBackColor = True
        '
        'chk_type_object
        '
        Me.chk_type_object.AutoSize = True
        Me.chk_type_object.Location = New System.Drawing.Point(19, 18)
        Me.chk_type_object.Name = "chk_type_object"
        Me.chk_type_object.Size = New System.Drawing.Size(70, 17)
        Me.chk_type_object.TabIndex = 0
        Me.chk_type_object.Text = "xObject"
        Me.chk_type_object.UseVisualStyleBackColor = True
        '
        'gb_delivery_date
        '
        Me.gb_delivery_date.Controls.Add(Me.dtp_to_dd)
        Me.gb_delivery_date.Controls.Add(Me.dtp_from_dd)
        Me.gb_delivery_date.Location = New System.Drawing.Point(273, 6)
        Me.gb_delivery_date.Name = "gb_delivery_date"
        Me.gb_delivery_date.Size = New System.Drawing.Size(261, 72)
        Me.gb_delivery_date.TabIndex = 3
        Me.gb_delivery_date.TabStop = False
        Me.gb_delivery_date.Text = "xDelivery Date"
        '
        'dtp_to_dd
        '
        Me.dtp_to_dd.Checked = True
        Me.dtp_to_dd.IsReadOnly = False
        Me.dtp_to_dd.Location = New System.Drawing.Point(6, 38)
        Me.dtp_to_dd.Name = "dtp_to_dd"
        Me.dtp_to_dd.ShowCheckBox = True
        Me.dtp_to_dd.ShowUpDown = False
        Me.dtp_to_dd.Size = New System.Drawing.Size(240, 24)
        Me.dtp_to_dd.SufixText = "Sufix Text"
        Me.dtp_to_dd.SufixTextVisible = True
        Me.dtp_to_dd.TabIndex = 1
        Me.dtp_to_dd.TextWidth = 42
        Me.dtp_to_dd.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'dtp_from_dd
        '
        Me.dtp_from_dd.Checked = True
        Me.dtp_from_dd.IsReadOnly = False
        Me.dtp_from_dd.Location = New System.Drawing.Point(6, 14)
        Me.dtp_from_dd.Name = "dtp_from_dd"
        Me.dtp_from_dd.ShowCheckBox = True
        Me.dtp_from_dd.ShowUpDown = False
        Me.dtp_from_dd.Size = New System.Drawing.Size(240, 24)
        Me.dtp_from_dd.SufixText = "Sufix Text"
        Me.dtp_from_dd.SufixTextVisible = True
        Me.dtp_from_dd.TabIndex = 0
        Me.dtp_from_dd.TextWidth = 42
        Me.dtp_from_dd.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'gb_expire_date
        '
        Me.gb_expire_date.Controls.Add(Me.dtp_to_ed)
        Me.gb_expire_date.Controls.Add(Me.dtp_from_ed)
        Me.gb_expire_date.Location = New System.Drawing.Point(540, 6)
        Me.gb_expire_date.Name = "gb_expire_date"
        Me.gb_expire_date.Size = New System.Drawing.Size(255, 72)
        Me.gb_expire_date.TabIndex = 5
        Me.gb_expire_date.TabStop = False
        Me.gb_expire_date.Text = "xExpire Date"
        '
        'dtp_to_ed
        '
        Me.dtp_to_ed.Checked = True
        Me.dtp_to_ed.IsReadOnly = False
        Me.dtp_to_ed.Location = New System.Drawing.Point(6, 38)
        Me.dtp_to_ed.Name = "dtp_to_ed"
        Me.dtp_to_ed.ShowCheckBox = True
        Me.dtp_to_ed.ShowUpDown = False
        Me.dtp_to_ed.Size = New System.Drawing.Size(240, 24)
        Me.dtp_to_ed.SufixText = "Sufix Text"
        Me.dtp_to_ed.SufixTextVisible = True
        Me.dtp_to_ed.TabIndex = 1
        Me.dtp_to_ed.TextWidth = 42
        Me.dtp_to_ed.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'dtp_from_ed
        '
        Me.dtp_from_ed.Checked = True
        Me.dtp_from_ed.IsReadOnly = False
        Me.dtp_from_ed.Location = New System.Drawing.Point(6, 14)
        Me.dtp_from_ed.Name = "dtp_from_ed"
        Me.dtp_from_ed.ShowCheckBox = True
        Me.dtp_from_ed.ShowUpDown = False
        Me.dtp_from_ed.Size = New System.Drawing.Size(240, 24)
        Me.dtp_from_ed.SufixText = "Sufix Text"
        Me.dtp_from_ed.SufixTextVisible = True
        Me.dtp_from_ed.TabIndex = 0
        Me.dtp_from_ed.TextWidth = 42
        Me.dtp_from_ed.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'gb_status
        '
        Me.gb_status.Controls.Add(Me.chk_status_canceled)
        Me.gb_status.Controls.Add(Me.chk_status_pending)
        Me.gb_status.Controls.Add(Me.chk_status_expired)
        Me.gb_status.Controls.Add(Me.chk_status_delivered)
        Me.gb_status.Location = New System.Drawing.Point(801, 6)
        Me.gb_status.Name = "gb_status"
        Me.gb_status.Size = New System.Drawing.Size(304, 72)
        Me.gb_status.TabIndex = 7
        Me.gb_status.TabStop = False
        Me.gb_status.Text = "xStatus"
        '
        'chk_status_canceled
        '
        Me.chk_status_canceled.AutoSize = True
        Me.chk_status_canceled.Location = New System.Drawing.Point(176, 43)
        Me.chk_status_canceled.Name = "chk_status_canceled"
        Me.chk_status_canceled.Size = New System.Drawing.Size(86, 17)
        Me.chk_status_canceled.TabIndex = 3
        Me.chk_status_canceled.Text = "xCanceled"
        Me.chk_status_canceled.UseVisualStyleBackColor = True
        '
        'chk_status_pending
        '
        Me.chk_status_pending.AutoSize = True
        Me.chk_status_pending.Location = New System.Drawing.Point(19, 20)
        Me.chk_status_pending.Name = "chk_status_pending"
        Me.chk_status_pending.Size = New System.Drawing.Size(78, 17)
        Me.chk_status_pending.TabIndex = 0
        Me.chk_status_pending.Text = "xPending"
        Me.chk_status_pending.UseVisualStyleBackColor = True
        '
        'chk_status_expired
        '
        Me.chk_status_expired.AutoSize = True
        Me.chk_status_expired.Location = New System.Drawing.Point(176, 20)
        Me.chk_status_expired.Name = "chk_status_expired"
        Me.chk_status_expired.Size = New System.Drawing.Size(76, 17)
        Me.chk_status_expired.TabIndex = 2
        Me.chk_status_expired.Text = "xExpired"
        Me.chk_status_expired.UseVisualStyleBackColor = True
        '
        'chk_status_delivered
        '
        Me.chk_status_delivered.AutoSize = True
        Me.chk_status_delivered.Location = New System.Drawing.Point(19, 43)
        Me.chk_status_delivered.Name = "chk_status_delivered"
        Me.chk_status_delivered.Size = New System.Drawing.Size(88, 17)
        Me.chk_status_delivered.TabIndex = 1
        Me.chk_status_delivered.Text = "xDelivered"
        Me.chk_status_delivered.UseVisualStyleBackColor = True
        '
        'uc_account_sel1
        '
        Me.uc_account_sel1.Account = ""
        Me.uc_account_sel1.AccountText = ""
        Me.uc_account_sel1.Anon = False
        Me.uc_account_sel1.AutoSize = True
        Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
        Me.uc_account_sel1.DisabledHolder = False
        Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.uc_account_sel1.Holder = ""
        Me.uc_account_sel1.Location = New System.Drawing.Point(798, 81)
        Me.uc_account_sel1.Margin = New System.Windows.Forms.Padding(0)
        Me.uc_account_sel1.MassiveSearchNumbers = ""
        Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
        Me.uc_account_sel1.MassiveSearchType = 0
        Me.uc_account_sel1.Name = "uc_account_sel1"
        Me.uc_account_sel1.SearchTrackDataAsInternal = True
        Me.uc_account_sel1.ShowMassiveSearch = False
        Me.uc_account_sel1.ShowVipClients = True
        Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
        Me.uc_account_sel1.TabIndex = 8
        Me.uc_account_sel1.Telephone = ""
        Me.uc_account_sel1.TrackData = ""
        Me.uc_account_sel1.Vip = False
        Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
        '
        'ef_gift_name
        '
        Me.ef_gift_name.DoubleValue = 0.0R
        Me.ef_gift_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ef_gift_name.IntegerValue = 0
        Me.ef_gift_name.IsReadOnly = False
        Me.ef_gift_name.Location = New System.Drawing.Point(6, 168)
        Me.ef_gift_name.Name = "ef_gift_name"
        Me.ef_gift_name.PlaceHolder = Nothing
        Me.ef_gift_name.ShortcutsEnabled = True
        Me.ef_gift_name.Size = New System.Drawing.Size(261, 25)
        Me.ef_gift_name.SufixText = "Sufix Text"
        Me.ef_gift_name.SufixTextVisible = True
        Me.ef_gift_name.TabIndex = 2
        Me.ef_gift_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.ef_gift_name.TextValue = ""
        Me.ef_gift_name.TextWidth = 70
        Me.ef_gift_name.Value = ""
        Me.ef_gift_name.ValueForeColor = System.Drawing.Color.Blue
        '
        'gb_user_request
        '
        Me.gb_user_request.Controls.Add(Me.chk_show_all_request)
        Me.gb_user_request.Controls.Add(Me.opt_one_user_request)
        Me.gb_user_request.Controls.Add(Me.opt_all_users_request)
        Me.gb_user_request.Controls.Add(Me.cmb_user_request)
        Me.gb_user_request.Location = New System.Drawing.Point(6, 84)
        Me.gb_user_request.Name = "gb_user_request"
        Me.gb_user_request.Size = New System.Drawing.Size(261, 67)
        Me.gb_user_request.TabIndex = 1
        Me.gb_user_request.TabStop = False
        Me.gb_user_request.Text = "xRequest User"
        '
        'chk_show_all_request
        '
        Me.chk_show_all_request.AutoSize = True
        Me.chk_show_all_request.Location = New System.Drawing.Point(73, 43)
        Me.chk_show_all_request.Name = "chk_show_all_request"
        Me.chk_show_all_request.Size = New System.Drawing.Size(102, 17)
        Me.chk_show_all_request.TabIndex = 4
        Me.chk_show_all_request.Text = "chk_show_all"
        Me.chk_show_all_request.UseVisualStyleBackColor = True
        '
        'opt_one_user_request
        '
        Me.opt_one_user_request.AutoSize = True
        Me.opt_one_user_request.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.opt_one_user_request.Location = New System.Drawing.Point(10, 18)
        Me.opt_one_user_request.Name = "opt_one_user_request"
        Me.opt_one_user_request.Size = New System.Drawing.Size(55, 17)
        Me.opt_one_user_request.TabIndex = 0
        Me.opt_one_user_request.Text = "xOne"
        '
        'opt_all_users_request
        '
        Me.opt_all_users_request.AutoSize = True
        Me.opt_all_users_request.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.opt_all_users_request.Location = New System.Drawing.Point(10, 40)
        Me.opt_all_users_request.Name = "opt_all_users_request"
        Me.opt_all_users_request.Size = New System.Drawing.Size(46, 17)
        Me.opt_all_users_request.TabIndex = 2
        Me.opt_all_users_request.Text = "xAll"
        '
        'cmb_user_request
        '
        Me.cmb_user_request.AllowUnlistedValues = False
        Me.cmb_user_request.AutoCompleteMode = False
        Me.cmb_user_request.IsReadOnly = False
        Me.cmb_user_request.Location = New System.Drawing.Point(69, 14)
        Me.cmb_user_request.Name = "cmb_user_request"
        Me.cmb_user_request.SelectedIndex = -1
        Me.cmb_user_request.Size = New System.Drawing.Size(184, 24)
        Me.cmb_user_request.SufixText = "Sufix Text"
        Me.cmb_user_request.SufixTextVisible = True
        Me.cmb_user_request.TabIndex = 3
        Me.cmb_user_request.TextCombo = Nothing
        Me.cmb_user_request.TextVisible = False
        Me.cmb_user_request.TextWidth = 0
        '
        'gb_user_delivery
        '
        Me.gb_user_delivery.Controls.Add(Me.chk_show_all_delivery)
        Me.gb_user_delivery.Controls.Add(Me.opt_one_user_delivery)
        Me.gb_user_delivery.Controls.Add(Me.opt_all_users_delivery)
        Me.gb_user_delivery.Controls.Add(Me.cmb_user_delivery)
        Me.gb_user_delivery.Location = New System.Drawing.Point(273, 84)
        Me.gb_user_delivery.Name = "gb_user_delivery"
        Me.gb_user_delivery.Size = New System.Drawing.Size(261, 67)
        Me.gb_user_delivery.TabIndex = 4
        Me.gb_user_delivery.TabStop = False
        Me.gb_user_delivery.Text = "xDelivery User"
        '
        'chk_show_all_delivery
        '
        Me.chk_show_all_delivery.AutoSize = True
        Me.chk_show_all_delivery.Location = New System.Drawing.Point(73, 43)
        Me.chk_show_all_delivery.Name = "chk_show_all_delivery"
        Me.chk_show_all_delivery.Size = New System.Drawing.Size(102, 17)
        Me.chk_show_all_delivery.TabIndex = 4
        Me.chk_show_all_delivery.Text = "chk_show_all"
        Me.chk_show_all_delivery.UseVisualStyleBackColor = True
        '
        'opt_one_user_delivery
        '
        Me.opt_one_user_delivery.AutoSize = True
        Me.opt_one_user_delivery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.opt_one_user_delivery.Location = New System.Drawing.Point(10, 18)
        Me.opt_one_user_delivery.Name = "opt_one_user_delivery"
        Me.opt_one_user_delivery.Size = New System.Drawing.Size(55, 17)
        Me.opt_one_user_delivery.TabIndex = 0
        Me.opt_one_user_delivery.Text = "xOne"
        '
        'opt_all_users_delivery
        '
        Me.opt_all_users_delivery.AutoSize = True
        Me.opt_all_users_delivery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.opt_all_users_delivery.Location = New System.Drawing.Point(10, 40)
        Me.opt_all_users_delivery.Name = "opt_all_users_delivery"
        Me.opt_all_users_delivery.Size = New System.Drawing.Size(46, 17)
        Me.opt_all_users_delivery.TabIndex = 2
        Me.opt_all_users_delivery.Text = "xAll"
        '
        'cmb_user_delivery
        '
        Me.cmb_user_delivery.AllowUnlistedValues = False
        Me.cmb_user_delivery.AutoCompleteMode = False
        Me.cmb_user_delivery.IsReadOnly = False
        Me.cmb_user_delivery.Location = New System.Drawing.Point(69, 14)
        Me.cmb_user_delivery.Name = "cmb_user_delivery"
        Me.cmb_user_delivery.SelectedIndex = -1
        Me.cmb_user_delivery.Size = New System.Drawing.Size(184, 24)
        Me.cmb_user_delivery.SufixText = "Sufix Text"
        Me.cmb_user_delivery.SufixTextVisible = True
        Me.cmb_user_delivery.TabIndex = 3
        Me.cmb_user_delivery.TextCombo = Nothing
        Me.cmb_user_delivery.TextVisible = False
        Me.cmb_user_delivery.TextWidth = 0
        '
        'frm_gift_history
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1227, 649)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_gift_history"
        Me.Text = "frm_gift_history"
        Me.panel_filter.ResumeLayout(False)
        Me.panel_filter.PerformLayout()
        Me.panel_data.ResumeLayout(False)
        Me.pn_separator_line.ResumeLayout(False)
        Me.gb_request_date.ResumeLayout(False)
        Me.gb_type.ResumeLayout(False)
        Me.gb_type.PerformLayout()
        Me.gb_delivery_date.ResumeLayout(False)
        Me.gb_expire_date.ResumeLayout(False)
        Me.gb_status.ResumeLayout(False)
        Me.gb_status.PerformLayout()
        Me.gb_user_request.ResumeLayout(False)
        Me.gb_user_request.PerformLayout()
        Me.gb_user_delivery.ResumeLayout(False)
        Me.gb_user_delivery.PerformLayout()
        Me.ResumeLayout(False)

End Sub
  Friend WithEvents gb_request_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to_rd As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_rd As GUI_Controls.uc_date_picker
  Friend WithEvents gb_expire_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to_ed As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_ed As GUI_Controls.uc_date_picker
  Friend WithEvents gb_delivery_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to_dd As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_dd As GUI_Controls.uc_date_picker
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_type_services As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_draw_numbers As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_nrc As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_object As System.Windows.Forms.CheckBox
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_status_expired As System.Windows.Forms.CheckBox
  Friend WithEvents chk_status_delivered As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents chk_status_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_redeem_credit As System.Windows.Forms.CheckBox
  Friend WithEvents ef_gift_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_user_request As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_user_request As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users_request As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user_request As GUI_Controls.uc_combo
  Friend WithEvents chk_show_all_request As System.Windows.Forms.CheckBox
  Friend WithEvents gb_user_delivery As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all_delivery As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user_delivery As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users_delivery As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user_delivery As GUI_Controls.uc_combo
  Friend WithEvents chk_status_canceled As System.Windows.Forms.CheckBox
End Class
