<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_machine_alias
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_application = New GUI_Controls.uc_grid
    Me.panel_grids.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.dg_application)
    Me.panel_grids.Location = New System.Drawing.Point(5, 81)
    Me.panel_grids.Size = New System.Drawing.Size(835, 267)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_application, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(744, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(91, 267)
    Me.panel_buttons.TabIndex = 0
    '
    'panel_filter
    '
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(835, 67)
    Me.panel_filter.TabStop = True
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 71)
    Me.pn_separator_line.Size = New System.Drawing.Size(835, 10)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(835, 10)
    '
    'dg_application
    '
    Me.dg_application.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_application.CurrentCol = -1
    Me.dg_application.CurrentRow = -1
    Me.dg_application.Location = New System.Drawing.Point(6, 6)
    Me.dg_application.Name = "dg_application"
    Me.dg_application.PanelRightVisible = True
    Me.dg_application.Redraw = True
    Me.dg_application.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_application.Size = New System.Drawing.Size(739, 260)
    Me.dg_application.Sortable = False
    Me.dg_application.SortAscending = True
    Me.dg_application.SortByCol = 0
    Me.dg_application.TabIndex = 9
    Me.dg_application.ToolTipped = True
    Me.dg_application.TopRow = -2
    '
    'frm_machine_alias
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(845, 352)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_machine_alias"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.Text = "frm_machine_alias"
    Me.panel_grids.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_application As GUI_Controls.uc_grid
End Class
