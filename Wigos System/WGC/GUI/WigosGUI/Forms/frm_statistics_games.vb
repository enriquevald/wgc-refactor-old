'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statistics_games
' DESCRIPTION:   This screen allows to view the games statistics between:
'                           - two dates 
'                           - for all terminals or one terminal
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 13-APR-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-APR-2007  MTM    Initial Version
' 03-DEC-2012  RRB    Add Average Bet column.
' 28-MAR-2013  RCI & HBB    Added column SPH_THEORETICAL_WON_AMOUNT in table SALES_PER_HOUR. Needed to calculate the theoretical payout %. Also added a column to the grid with the theoretical payout %
' 25-MAY-2013  JCA    Change to use new table providers_games
' 18-AUG-2014  AMF    Progressive Jackpot
' 17-OCT-2014  JMV    Fixed WIG-1431
' 14-NOV-2014  OPC    Fixed WIG-1687
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 13-APR-2015  AMF    Backlog Item 966
' 10-JUN-2015  DHA    Error when bigger numbers of money type
' 17-JUN-2015  FAV    WIG-2461: Errors in the log
' 23-JUN-2015  DCS    Fixed Bug #WIG-2454: Statistics reports: Error displaying cloned terminals
' 08-JUL-2015  FOS    Fixed Bug #WIG-2557: Error filter by provider
' 28-JUL-2015  DCS    Fixed Bug #WIG-2615: Query it is not properly grouped by provider
' 28-JUL-2015  DCS    Fixed Bug #TFS-3648: Error Calculating TERMINALS_CONNECTED, the date used included closing hour 
' 11-NOV-2015 FOS    Product Backlog Item 5839:Garcia River change NLS
' 15-JUN-2018 GDA    Bug 33063 - WIGOS-12243 - [Ticket #14517] Fallo � Reporte de Estad�stica agrupada por fecha, proveedor, terminal- Jugadas Version V03.08.0001
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text

Public Class frm_statistics_games
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents lbl_netwin As System.Windows.Forms.Label
  Friend WithEvents lbl_payout As System.Windows.Forms.Label
  Friend WithEvents gb_group As System.Windows.Forms.GroupBox
  Friend WithEvents opt_provider_game As System.Windows.Forms.RadioButton
  Friend WithEvents opt_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents uc_combo_games As GUI_Controls.uc_combo_games
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.lbl_netwin = New System.Windows.Forms.Label
    Me.lbl_payout = New System.Windows.Forms.Label
    Me.gb_group = New System.Windows.Forms.GroupBox
    Me.chk_show_terminals = New System.Windows.Forms.CheckBox
    Me.opt_provider_game = New System.Windows.Forms.RadioButton
    Me.opt_terminal = New System.Windows.Forms.RadioButton
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.uc_combo_games = New GUI_Controls.uc_combo_games
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_combo_games)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_group)
    Me.panel_filter.Controls.Add(Me.lbl_payout)
    Me.panel_filter.Controls.Add(Me.lbl_netwin)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 229)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_netwin, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_payout, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_combo_games, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 233)
    Me.panel_data.Size = New System.Drawing.Size(1226, 475)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(9, 9)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 110)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(273, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 1
    '
    'lbl_netwin
    '
    Me.lbl_netwin.AutoSize = True
    Me.lbl_netwin.ForeColor = System.Drawing.Color.Navy
    Me.lbl_netwin.Location = New System.Drawing.Point(6, 211)
    Me.lbl_netwin.Name = "lbl_netwin"
    Me.lbl_netwin.Size = New System.Drawing.Size(52, 13)
    Me.lbl_netwin.TabIndex = 17
    Me.lbl_netwin.Text = "xNetwin"
    '
    'lbl_payout
    '
    Me.lbl_payout.AutoSize = True
    Me.lbl_payout.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payout.Location = New System.Drawing.Point(6, 192)
    Me.lbl_payout.Name = "lbl_payout"
    Me.lbl_payout.Size = New System.Drawing.Size(53, 13)
    Me.lbl_payout.TabIndex = 18
    Me.lbl_payout.Text = "xPayout"
    '
    'gb_group
    '
    Me.gb_group.Controls.Add(Me.chk_show_terminals)
    Me.gb_group.Controls.Add(Me.opt_provider_game)
    Me.gb_group.Controls.Add(Me.opt_terminal)
    Me.gb_group.Location = New System.Drawing.Point(608, 81)
    Me.gb_group.Name = "gb_group"
    Me.gb_group.Size = New System.Drawing.Size(267, 82)
    Me.gb_group.TabIndex = 3
    Me.gb_group.TabStop = False
    Me.gb_group.Text = "xGroup"
    '
    'chk_show_terminals
    '
    Me.chk_show_terminals.AutoSize = True
    Me.chk_show_terminals.Location = New System.Drawing.Point(19, 36)
    Me.chk_show_terminals.Name = "chk_show_terminals"
    Me.chk_show_terminals.Size = New System.Drawing.Size(120, 17)
    Me.chk_show_terminals.TabIndex = 1
    Me.chk_show_terminals.Text = "xShowTerminals"
    Me.chk_show_terminals.UseVisualStyleBackColor = True
    '
    'opt_provider_game
    '
    Me.opt_provider_game.Location = New System.Drawing.Point(8, 13)
    Me.opt_provider_game.Name = "opt_provider_game"
    Me.opt_provider_game.Size = New System.Drawing.Size(211, 24)
    Me.opt_provider_game.TabIndex = 0
    Me.opt_provider_game.Text = "xProvider-Game"
    '
    'opt_terminal
    '
    Me.opt_terminal.Location = New System.Drawing.Point(8, 56)
    Me.opt_terminal.Name = "opt_terminal"
    Me.opt_terminal.Size = New System.Drawing.Size(211, 24)
    Me.opt_terminal.TabIndex = 2
    Me.opt_terminal.Text = "xTerminal"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(608, 169)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'uc_combo_games
    '
    Me.uc_combo_games.Location = New System.Drawing.Point(603, 5)
    Me.uc_combo_games.Name = "uc_combo_games"
    Me.uc_combo_games.Size = New System.Drawing.Size(272, 72)
    Me.uc_combo_games.TabIndex = 2
    '
    'frm_statistics_games
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1234, 712)
    Me.Name = "frm_statistics_games"
    Me.Text = "frm_statistics_games"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group.ResumeLayout(False)
    Me.gb_group.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const SQL_COLUMN_GAME_ID As Integer = 0
  Private Const SQL_COLUMN_GAME_NAME As Integer = 1
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_AMOUNT_PER_CENT As Integer = 4
  Private Const SQL_COLUMN_NETWIN As Integer = 5
  Private Const SQL_COLUMN_NETWIN_PER_CENT As Integer = 6
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 7
  Private Const SQL_COLUMN_WON_COUNT As Integer = 8
  Private Const SQL_COLUMN_COUNT_PER_CENT As Integer = 9
  Private Const SQL_COLUMN_PROVIDER As Integer = 10
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 11
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 12
  Private Const SQL_COLUMN_FLOOR_ID As Integer = 13
  Private Const SQL_COLUMN_NUM_TERMINALS As Integer = 14
  Private Const SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT As Integer = 15
  Private Const SQL_COLUMN_THEORICAL_AMOUNT As Integer = 16
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer = 17
  Private Const SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 18
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 19
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0 As Integer = 20
  Private Const SQL_COLUMN_NUM_MASTER_TERMINALS As Integer = 21

  Private TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATE_FROM As Integer = 1
  Private Const GRID_COLUMN_DATE_TO As Integer = 2
  Private Const GRID_COLUMN_PROVIDER As Integer = 3

  Private GRID_COLUMN_GAME_NAME As Integer
  Private GRID_COLUMN_TERMINAL_NAME As Integer
  Private GRID_COLUMN_FLOOR_ID As Integer

  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_NUM_TERMINALS As Integer
  Private GRID_COLUMN_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_WON_AMOUNT As Integer
  Private GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer
  Private GRID_COLUMN_AMOUNT_PER_CENT As Integer
  Private GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT As Integer
  Private GRID_COLUMN_NETWIN As Integer
  Private GRID_COLUMN_NW_TOTAL_PER_CENT As Integer
  Private GRID_COLUMN_PLAYED_COUNT As Integer
  Private GRID_COLUMN_WON_COUNT As Integer
  Private GRID_COLUMN_COUNT_PER_CENT As Integer
  Private GRID_COLUMN_AVERAGE_BET As Integer
  Private GRID_COLUMN_NW_PER_TERMINAL As Integer
  Private GRID_COLUMN_PLAYED_PER_TERMINAL As Integer

  Private GRID_COLUMNS As Integer = 23
  Private Const GRID_HEADER_ROWS As Integer = 2

  'Counters
  Private Const COUNTER_DEVICE_ERROR As Integer = 1
  Private Const COUNTER_DEVICE_WARNING As Integer = 2
  Private Const COUNTER_DEVICE_OK As Integer = 3
  Private Const COUNTER_OPERATIONS As Integer = 4

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_DEVICE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_DEVICE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_DEVICE_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_DEVICE_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_OPERATION_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_OPERATION_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region ' Constants

#Region " Enums "

  Enum GROUP_MODE
    PROVIDER_GAME_NON_TERMINAL = 0
    PROVIDER_GAME_WITH_TERMINAL = 1
    TERMINAL = 2
  End Enum ' GROUP_MODE

#End Region ' Enums

#Region " Members "

  ' For report filters 
  Private m_game As String
  Private m_terminal As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_group As String
  Private m_show_terminals As String
  Private m_show_terminal_location As String

  Private m_terminal_report_type As ReportType = ReportType.Provider

  ' For total amount grid
  Dim total_played_amount As Decimal
  Dim total_won_amount As Decimal
  Dim total_played_count As Decimal
  Dim total_won_count As Decimal
  Dim total_terminal_count As Integer
  Dim total_theoretical_won_amount As Decimal
  Dim total_progressive_provision_amount As Decimal
  Dim total_non_progressive_jackpot_amount As Decimal
  Dim total_progressive_jackpot_amount As Decimal
  Dim total_progressive_jackpot_amount_0 As Decimal

  ' For subtotal amount grid
  Dim subtotal_provider As String
  Dim subtotal_game As String
  Dim subtotal_terminal As String
  Dim subtotal_played_amount As Decimal
  Dim subtotal_won_amount As Decimal
  Dim subtotal_played_count As Decimal
  Dim subtotal_won_count As Decimal
  Dim subtotal_terminal_count As Integer
  Dim subtotal_theoretical_won_amount As Decimal
  Dim subtotal_progressive_provision_amount As Decimal
  Dim subtotal_non_progressive_jackpot_amount As Decimal
  Dim subtotal_progressive_jackpot_amount As Decimal
  Dim subtotal_progressive_jackpot_amount_0 As Decimal

  Private m_group_mode As GROUP_MODE

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Set FormId
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STATISTICS_GAMES

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  ' PURPOSE: Init controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(201) + " " + GLB_NLS_GUI_STATISTICS.GetString(351) ' Estad�sticas Agrupadas por Juego

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_STATISTICS.Id(202), True)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Games
    Me.uc_combo_games.Init()

    ' Progressives
    Me.lbl_payout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5400)
    Me.lbl_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5349)

    ' Group
    Me.gb_group.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2984)
    Me.opt_provider_game.Text = GLB_NLS_GUI_STATISTICS.GetString(315) + "-" + GLB_NLS_GUI_STATISTICS.GetString(215)
    Me.opt_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968) + "-" + GLB_NLS_GUI_STATISTICS.GetString(215)
    Me.chk_show_terminals.Text = GLB_NLS_GUI_STATISTICS.GetString(472)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Group mode
    m_group_mode = GROUP_MODE.PROVIDER_GAME_NON_TERMINAL

  End Sub ' GUI_InitControls

  ' PURPOSE: Filter reset
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_BeforeFirstRow()

    'Subtotal
    subtotal_provider = String.Empty
    subtotal_game = String.Empty
    subtotal_terminal = String.Empty
    subtotal_played_amount = 0
    subtotal_won_amount = 0
    subtotal_played_count = 0
    subtotal_won_count = 0
    subtotal_terminal_count = 0
    subtotal_theoretical_won_amount = 0
    subtotal_progressive_provision_amount = 0
    subtotal_non_progressive_jackpot_amount = 0
    subtotal_progressive_jackpot_amount = 0
    subtotal_progressive_jackpot_amount_0 = 0

    'Total
    total_played_amount = 0
    total_won_amount = 0
    total_played_count = 0
    total_won_count = 0
    total_terminal_count = 0
    total_theoretical_won_amount = 0
    total_progressive_provision_amount = 0
    total_non_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount_0 = 0

    ' Show terminal location
    If Not chk_terminal_location.Enabled Then
      m_terminal_report_type = ReportType.Provider
    ElseIf chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.AddRow()

    If (m_group_mode = GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL Or m_group_mode = GROUP_MODE.TERMINAL) AndAlso Me.Grid.NumRows > 1 Then
      Call InsertSubtotal()
    End If

    Dim _idx_row As Integer = Me.Grid.NumRows - 1
    Dim _amount_per_cent As Decimal
    Dim _netwin As Decimal = total_played_amount - (total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount_0)
    Dim _count_per_cent As Decimal
    Dim _total_average_bet As Decimal
    Dim _theoretical_amount_per_cent As Decimal

    _total_average_bet = 0

    ' Total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE_FROM).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_NUM_TERMINALS).Value = GUI_FormatNumber(total_terminal_count, 0)

    ' Played Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(total_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Won Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(total_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Amount Per cent
    If total_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    Else
      _amount_per_cent = ((total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount_0) / total_played_amount) * 100
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_amount_per_cent, 2) & "%"
    End If

    ' Theoretical Amount Per cent
    If total_theoretical_won_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = ""
    Else
      If total_played_amount = 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = ""
      Else
        _theoretical_amount_per_cent = (total_theoretical_won_amount / total_played_amount) * 100
        Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_theoretical_amount_per_cent, 2) & "%"
      End If
    End If

    ' Non progressive Jackpot amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(total_non_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive Jackpot amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(total_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive provision amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(total_progressive_provision_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Played Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(total_played_count, 0)

    ' Won Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(total_won_count, 0)

    ' Count per cent
    If total_played_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    Else
      _count_per_cent = (total_won_count / total_played_count) * 100
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(_count_per_cent, 2) & "%"
    End If

    ' Count Netwin per terminal
    If total_terminal_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(0)
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(_netwin / total_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Count Played per terminal
    If total_terminal_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = String.Empty
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(total_played_amount / total_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    ' Netwin Total Per cent
    For i_row As Integer = 0 To Me.Grid.NumRows - 1
      Dim total_played_amount As Long = GUI_FormatNumber(Me.Grid.Cell(i_row, GRID_COLUMN_PLAYED_AMOUNT).Value)
      Dim netwin_row As Long = GUI_FormatNumber(Me.Grid.Cell(i_row, GRID_COLUMN_NETWIN).Value)

      If total_played_amount = 0 Then
        Me.Grid.Cell(i_row, GRID_COLUMN_NW_TOTAL_PER_CENT).Value = String.Empty
      Else
        Me.Grid.Cell(i_row, GRID_COLUMN_NW_TOTAL_PER_CENT).Value = GUI_FormatNumber(netwin_row * 100 / total_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
      End If
    Next

    ' Average bet
    If total_played_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
    Else
      _total_average_bet = Math.Round(total_played_amount / total_played_count, 2, MidpointRounding.AwayFromZero)

      If _total_average_bet = 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    ' Select group mode
    If Me.opt_provider_game.Checked Then
      If chk_show_terminals.Checked Then
        m_group_mode = GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL

        Return GetSqlQueryProviderGameWithTerminal()
      Else
        m_group_mode = GROUP_MODE.PROVIDER_GAME_NON_TERMINAL

        Return GetSqlQueryProviderGameNonTerminal()
      End If
    Else
      m_group_mode = GROUP_MODE.TERMINAL

      Return GetSqlQueryTerminal()
    End If

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Change subtotal 

    ' PROVIDER_GAME_WITH_TERMINAL
    If m_group_mode = GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL Then
      If subtotal_provider = String.Empty Then
        subtotal_provider = DbRow.Value(SQL_COLUMN_PROVIDER)
      End If
      If subtotal_game = String.Empty Then
        subtotal_game = DbRow.Value(SQL_COLUMN_GAME_NAME)
      End If

      If subtotal_provider <> DbRow.Value(SQL_COLUMN_PROVIDER) Then
        Call InsertSubtotal()
        subtotal_provider = DbRow.Value(SQL_COLUMN_PROVIDER)
        subtotal_game = DbRow.Value(SQL_COLUMN_GAME_NAME)
        RowIndex = RowIndex + 1
      ElseIf subtotal_game <> DbRow.Value(SQL_COLUMN_GAME_NAME) Then
        Call InsertSubtotal()
        subtotal_game = DbRow.Value(SQL_COLUMN_GAME_NAME)
        RowIndex = RowIndex + 1
      End If
    End If

    ' TERMINAL
    If m_group_mode = GROUP_MODE.TERMINAL Then
      If subtotal_terminal = String.Empty Then
        subtotal_terminal = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      End If

      If subtotal_terminal <> DbRow.Value(SQL_COLUMN_TERMINAL_NAME) Then
        Call InsertSubtotal()
        subtotal_terminal = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
        RowIndex = RowIndex + 1
      End If
    End If

    Call GUI_AddDataRow(RowIndex, DbRow)

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set initial focus
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(215), m_game)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2984), m_group)

    If m_group = Me.opt_provider_game.Text Then
      PrintData.SetFilter(chk_show_terminals.Text, m_show_terminals)
      If Me.chk_show_terminals.Checked Then
        PrintData.SetFilter(chk_terminal_location.Text, m_show_terminal_location)
      End If
    Else
      PrintData.SetFilter(chk_terminal_location.Text, m_show_terminal_location)
    End If

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(360)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_game = ""
    m_terminal = ""
    m_date_from = ""
    m_date_to = ""
    m_group = ""
    m_show_terminals = ""
    m_show_terminal_location = ""

    ' Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' Games
    If Me.uc_combo_games.GetSelectedId() = String.Empty Then
      m_game = Me.uc_combo_games.opt_all_game.Text
    Else
      m_game = Me.uc_combo_games.GetSelectedValue
    End If

    ' Group
    If Me.opt_provider_game.Checked Then
      m_group = Me.opt_provider_game.Text
    Else
      m_group = Me.opt_terminal.Text
    End If

    ' Show terminales
    If Me.chk_show_terminals.Checked Then
      m_show_terminals = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_terminals = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Show Terminal Location
    If Me.chk_terminal_location.Checked Then
      m_show_terminal_location = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_terminal_location = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      .Counter(COUNTER_DEVICE_ERROR).Visible = False
      .Counter(COUNTER_DEVICE_ERROR).BackColor = GetColor(COLOR_DEVICE_ERROR_BACK)
      .Counter(COUNTER_DEVICE_ERROR).ForeColor = GetColor(COLOR_DEVICE_ERROR_FORE)

      .Counter(COUNTER_DEVICE_WARNING).Visible = False
      .Counter(COUNTER_DEVICE_WARNING).BackColor = GetColor(COLOR_DEVICE_WARNING_BACK)

      .Counter(COUNTER_OPERATIONS).Visible = False
      .Counter(COUNTER_OPERATIONS).BackColor = GetColor(COLOR_OPERATION_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date from
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(309)
      .Column(GRID_COLUMN_DATE_FROM).Width = 1700
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date to
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(310)
      .Column(GRID_COLUMN_DATE_TO).Width = 1700
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Provider 
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = ""
      .Column(GRID_COLUMN_PROVIDER).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER).Width = 2000
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Game Name
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COLUMN_GAME_NAME).Width = 1950
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(491)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = IIf(m_group_mode = GROUP_MODE.PROVIDER_GAME_NON_TERMINAL, 0, 2200)
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Floor ID
      .Column(GRID_COLUMN_FLOOR_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COLUMN_FLOOR_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5200)
      .Column(GRID_COLUMN_FLOOR_ID).Width = IIf(m_group_mode = GROUP_MODE.PROVIDER_GAME_NON_TERMINAL, 0, 1300)
      .Column(GRID_COLUMN_FLOOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Report
      If m_terminal_report_type = ReportType.Location Then
        _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_columns.Count - 1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        Next
      End If

      '  Num Terminals
      .Column(GRID_COLUMN_NUM_TERMINALS).Header(0).Text = ""
      .Column(GRID_COLUMN_NUM_TERMINALS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(269)
      .Column(GRID_COLUMN_NUM_TERMINALS).Width = IIf(m_group_mode = GROUP_MODE.TERMINAL, 0, 1250)
      .Column(GRID_COLUMN_NUM_TERMINALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '  Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6275) 'Coin in
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(302)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5391)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = 1950
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non progressive Jackpot amount
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5293)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1600
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive Jackpot amount
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5294)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive provision amount
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5292)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount Per Cent
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(303)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Width = 1250
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical Amount Per Cent
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1809)
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Width = GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
      .Column(GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NETWIN).Width = 1250
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin Total Per Cent
      .Column(GRID_COLUMN_NW_TOTAL_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NW_TOTAL_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(312)
      .Column(GRID_COLUMN_NW_TOTAL_PER_CENT).Width = 1250
      .Column(GRID_COLUMN_NW_TOTAL_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = 950
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = 950
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count Per Cent
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(316)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Width = 820
      .Column(GRID_COLUMN_COUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NetWin Per Terminal
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(346)
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(311)
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Width = IIf(m_group_mode = GROUP_MODE.TERMINAL, 0, 1250)
      .Column(GRID_COLUMN_NW_PER_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Per Terminal
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(346)
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(301)
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Width = IIf(m_group_mode = GROUP_MODE.TERMINAL, 0, 1250)
      .Column(GRID_COLUMN_PLAYED_PER_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average bet
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = 1400
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.uc_combo_games.SetDefaultValues()

    Me.opt_provider_game.Checked = True
    Me.chk_show_terminals.Checked = False
    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location.Enabled = False

    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    _str_where = Me.uc_dsl.GetSqlFilterCondition("SPH_BASE_HOUR")
    If _str_where.Length > 0 Then
      _str_where = " AND " & _str_where
    End If

    ' Filter Providers - Terminals
    _str_where = _str_where & " AND SPH_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Filter Game
    If Me.uc_combo_games.GetSelectedId <> String.Empty Then
      _str_where = _str_where & " AND SPH_GAME_ID = " & Me.uc_combo_games.GetSelectedId
    End If


    If _str_where.Length > 0 Then
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    'Group by
    _str_where = _str_where + " GROUP BY SPH_GAME_ID, TE_PROVIDER_ID"

    Select Case m_group_mode
      Case GROUP_MODE.PROVIDER_GAME_NON_TERMINAL
        _str_where = _str_where + ", TMP_NUM_CONNECTED, TE_TERMINAL_ID, TC.TMP_MASTER_ID ) x2 "

      Case GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL
        _str_where = _str_where + ", TMP_NUM_CONNECTED, SPH_TERMINAL_ID, SPH_TERMINAL_NAME, TE_FLOOR_ID, TC.TMP_MASTER_ID )"

      Case GROUP_MODE.TERMINAL
        _str_where = _str_where + ", SPH_TERMINAL_ID, SPH_TERMINAL_NAME, TE_FLOOR_ID )"

    End Select

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Set column index
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridColumnReIndex()

    If m_terminal_report_type = ReportType.Location Then
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)
    Else
      TERMINAL_DATA_COLUMNS = 0
    End If

    If m_group_mode = GROUP_MODE.TERMINAL Then
      GRID_COLUMN_TERMINAL_NAME = 4
      GRID_COLUMN_FLOOR_ID = 5
      GRID_INIT_TERMINAL_DATA = 6
      GRID_COLUMN_GAME_NAME = TERMINAL_DATA_COLUMNS + 6
    Else
      GRID_COLUMN_GAME_NAME = 4
      GRID_COLUMN_TERMINAL_NAME = 5
      GRID_COLUMN_FLOOR_ID = 6
      GRID_INIT_TERMINAL_DATA = 7
    End If

    GRID_COLUMN_NUM_TERMINALS = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_AMOUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_NW_TOTAL_PER_CENT = TERMINAL_DATA_COLUMNS + 16
    GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 18
    GRID_COLUMN_COUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 19
    GRID_COLUMN_AVERAGE_BET = TERMINAL_DATA_COLUMNS + 20
    GRID_COLUMN_NW_PER_TERMINAL = TERMINAL_DATA_COLUMNS + 21
    GRID_COLUMN_PLAYED_PER_TERMINAL = TERMINAL_DATA_COLUMNS + 22

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 23

  End Sub ' GridColumnReIndex

  ' PURPOSE: Get terminals connected
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlQueryTerminalsConnected() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(" CREATE TABLE #TMP_CONNECTED ")
    _str_sql.AppendLine(" ( ")
    _str_sql.AppendLine("    TMP_TERMINAL_ID BIGINT, ")
    _str_sql.AppendLine("    TMP_NUM_CONNECTED BIGINT, ")
    _str_sql.AppendLine("    TMP_MASTER_ID BIGINT ")
    _str_sql.AppendLine(" ) ")
    _str_sql.AppendLine(" CREATE NONCLUSTERED INDEX IX_TMP_TC_ID ON #TMP_CONNECTED(TMP_TERMINAL_ID) ")
    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" INSERT INTO   #TMP_CONNECTED ")
    _str_sql.AppendLine("      SELECT   TC_TERMINAL_ID, ")
    _str_sql.AppendLine("               SUM(1), ")
    _str_sql.AppendLine("               TC_MASTER_ID ")
    _str_sql.AppendLine("        FROM   TERMINALS_CONNECTED ")
    _str_sql.AppendLine("       WHERE ")

    ' Filter date
    _str_sql.AppendLine(Me.uc_dsl.GetSqlFilterCondition("TC_DATE", False))

    ' Filter Providers - Terminals
    _str_sql.AppendLine("   AND   TC_TERMINAL_ID IN  " & Me.uc_pr_list.GetProviderIdListSelected())

    _str_sql.AppendLine("   AND   TC_CONNECTED = 1 ")
    _str_sql.AppendLine("GROUP BY TC_TERMINAL_ID, TC_MASTER_ID ")

    Return _str_sql.ToString()

  End Function ' GetSqlQueryTerminalsConnected

  ' PURPOSE: Get Query mode Provider Game Non Terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlQueryProviderGameNonTerminal() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(GetSqlQueryTerminalsConnected())
    _str_sql.AppendLine(" ")
    _str_sql.AppendLine("SELECT   x.SPH_GAME_ID, ")
    _str_sql.AppendLine("         ISNULL(PG_GAME_NAME,'UNKNOWN') AS GM_NAME, ")
    _str_sql.AppendLine("         x.PlayedAmount, ")
    _str_sql.AppendLine("         x.WonAmount, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)*100/x.PlayedAmount) END AS AmountPc, ")
    _str_sql.AppendLine("         PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) AS Netwin, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))*100/x.PlayedAmount) END AS NetwinPc, ")
    _str_sql.AppendLine("         x.PlayedCount, ")
    _str_sql.AppendLine("         x.WonCount, ")
    _str_sql.AppendLine("         CASE x.PlayedCount WHEN 0 THEN 0 ELSE ((x.WonCount* 100.0) /x.PlayedCount) END AS CountPerCent, ")
    _str_sql.AppendLine("         x.Provider, ")
    _str_sql.AppendLine("         '' AS TerminalId, ")
    _str_sql.AppendLine("         '' AS Terminal, ")
    _str_sql.AppendLine("         '' AS FloorId, ")
    _str_sql.AppendLine("         x.TerminalCount, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc, ")
    _str_sql.AppendLine("         TheoreticalWonAmount, ")
    _str_sql.AppendLine("         ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("         NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("         ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("         ProgressiveJackpotAmount0,")
    _str_sql.AppendLine("         x.TerminalCount ") ' DCS: 28-JUL-2015: Added for post calculations (homogeneous with the other queries that use this column as TerminalCountMaster)
    _str_sql.AppendLine("  FROM   PROVIDERS_GAMES RIGHT JOIN ")
    _str_sql.AppendLine("         ( ")
    _str_sql.AppendLine("         SELECT   SPH_GAME_ID, ")
    _str_sql.AppendLine("                  sum(PlayedAmount) AS PlayedAmount, ")
    _str_sql.AppendLine("                  sum(WonAmount) AS WonAmount, ")
    _str_sql.AppendLine("                  sum(PlayedCount) AS PlayedCount, ")
    _str_sql.AppendLine("                  sum(WonCount) AS WonCount, ")
    _str_sql.AppendLine("                  sum(ProgressiveProvisionAmount) AS ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("                  sum(NonProgressiveJackpotAmount) AS NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                  sum(ProgressiveJackpotAmount) AS ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                  sum(ProgressiveJackpotAmount0) AS ProgressiveJackpotAmount0, ")
    _str_sql.AppendLine("                  Provider, ")
    _str_sql.AppendLine("                  sum(TerminalCount) AS TerminalCount, ")
    _str_sql.AppendLine("                  sum(TheoreticalWonAmount) AS TheoreticalWonAmount ")
    _str_sql.AppendLine("           FROM ")
    _str_sql.AppendLine("           ( ")
    _str_sql.AppendLine("           SELECT   SPH_GAME_ID, ")
    _str_sql.AppendLine("                    TE_TERMINAL_ID, ")
    _str_sql.AppendLine("                    sum(CAST(SPH_PLAYED_AMOUNT AS DECIMAL(22,4))) as PlayedAmount, ")
    _str_sql.AppendLine("                    sum(CAST(SPH_WON_AMOUNT AS DECIMAL(22,4))) AS WonAmount, ")
    _str_sql.AppendLine("                    CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END as PlayedCount, ")
    _str_sql.AppendLine("                    sum(SPH_WON_COUNT) AS WonCount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0, ")
    _str_sql.AppendLine("                    TE_PROVIDER_ID AS Provider, ")
    _str_sql.AppendLine("                    ISNULL(TMP_NUM_CONNECTED,0) AS TerminalCount, ")
    _str_sql.AppendLine("                    sum(SPH_theoretical_won_amount) AS TheoreticalWonAmount,       ")
    _str_sql.AppendLine("                    CASE WHEN TMP_NUM_CONNECTED IS NULL                                  ")
    _str_sql.AppendLine("                    	    THEN CASE WHEN TC.TMP_MASTER_ID IS NULL THEN 0 ELSE 1 END       ")
    _str_sql.AppendLine("                    	    ELSE TMP_NUM_CONNECTED                                          ")
    _str_sql.AppendLine("                    END AS TerminalCountMaster                                           ")
    _str_sql.AppendLine("             FROM   SALES_PER_HOUR_V2 JOIN TERMINALS ON SPH_TERMINAL_ID = TE_TERMINAL_ID ")
    _str_sql.AppendLine("        LEFT JOIN   #TMP_CONNECTED ON TE_TERMINAL_ID = TMP_TERMINAL_ID ")
    _str_sql.AppendLine("        LEFT JOIN	(SELECT	DISTINCT TMP_MASTER_ID FROM  #TMP_CONNECTED) AS TC ON TE_MASTER_ID = TC.TMP_MASTER_ID ")
    _str_sql.AppendLine(GetSqlWhere())
    _str_sql.AppendLine(" GROUP BY Provider, SPH_GAME_ID) x ON PG_GAME_ID = x.SPH_GAME_ID ")
    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" DROP TABLE #TMP_CONNECTED ")

    Return _str_sql.ToString()

  End Function ' GetSqlQueryProviderGameNonTerminal

  ' PURPOSE: Get Query mode Provider Game With Terminal
  ' 
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlQueryProviderGameWithTerminal() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(GetSqlQueryTerminalsConnected())
    _str_sql.AppendLine(" ")
    _str_sql.AppendLine("SELECT   x.SPH_GAME_ID, ")
    _str_sql.AppendLine("         ISNULL(PG_GAME_NAME,'UNKNOWN') AS GM_NAME, ")
    _str_sql.AppendLine("         x.PlayedAmount, ")
    _str_sql.AppendLine("         x.WonAmount, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)*100/x.PlayedAmount) END AS AmountPc, ")
    _str_sql.AppendLine("         PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) AS Netwin, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))*100/x.PlayedAmount) END AS NetwinPc, ")
    _str_sql.AppendLine("         x.PlayedCount, ")
    _str_sql.AppendLine("         x.WonCount, ")
    _str_sql.AppendLine("         CASE x.PlayedCount WHEN 0 THEN 0 ELSE ((x.WonCount* 100.0) /x.PlayedCount) END AS CountPerCent, ")
    _str_sql.AppendLine("         x.Provider, ")
    _str_sql.AppendLine("         x.TerminalId, ")
    _str_sql.AppendLine("         x.Terminal, ")
    _str_sql.AppendLine("         ISNULL(x.FloorId,'') AS FloorId, ")
    _str_sql.AppendLine("         x.TerminalCount, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc, ")
    _str_sql.AppendLine("         TheoreticalWonAmount, ")
    _str_sql.AppendLine("         ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("         NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("         ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("         ProgressiveJackpotAmount0,")
    _str_sql.AppendLine("         TerminalCountMaster ")
    _str_sql.AppendLine("  FROM   PROVIDERS_GAMES RIGHT JOIN ")
    _str_sql.AppendLine("         ( ")
    _str_sql.AppendLine("           SELECT   SPH_GAME_ID, ")
    _str_sql.AppendLine("                    sum(CAST(SPH_PLAYED_AMOUNT AS DECIMAL(22,4))) as PlayedAmount, ")
    _str_sql.AppendLine("                    sum(CAST(SPH_WON_AMOUNT AS DECIMAL(22,4))) AS WonAmount, ")
    _str_sql.AppendLine("                    CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END AS PlayedCount, ")
    _str_sql.AppendLine("                    sum(SPH_WON_COUNT) AS WonCount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0, ")
    _str_sql.AppendLine("                    TE_PROVIDER_ID AS Provider, ")
    _str_sql.AppendLine("                    SPH_TERMINAL_ID AS TerminalId,")
    _str_sql.AppendLine("                    SPH_TERMINAL_NAME AS Terminal,")
    _str_sql.AppendLine("                    TE_FLOOR_ID AS FloorId,")
    _str_sql.AppendLine("                    ISNULL(TMP_NUM_CONNECTED,0) AS TerminalCount, ")
    _str_sql.AppendLine("                    sum(SPH_theoretical_won_amount) AS TheoreticalWonAmount,       ")
    _str_sql.AppendLine("                    CASE WHEN TMP_NUM_CONNECTED IS NULL                                  ")
    _str_sql.AppendLine("                    	    THEN CASE WHEN TC.TMP_MASTER_ID IS NULL THEN 0 ELSE 1 END       ")
    _str_sql.AppendLine("                    	    ELSE TMP_NUM_CONNECTED                                          ")
    _str_sql.AppendLine("                    END AS TerminalCountMaster                                           ")
    _str_sql.AppendLine("             FROM   SALES_PER_HOUR_V2 JOIN TERMINALS ON SPH_TERMINAL_ID = TE_TERMINAL_ID ")
    _str_sql.AppendLine("        LEFT JOIN   #TMP_CONNECTED ON TE_TERMINAL_ID = TMP_TERMINAL_ID ")
    _str_sql.AppendLine("        LEFT JOIN	(SELECT	DISTINCT TMP_MASTER_ID FROM  #TMP_CONNECTED) AS TC ON TE_MASTER_ID = TC.TMP_MASTER_ID ")

    _str_sql.AppendLine(GetSqlWhere())
    _str_sql.AppendLine(" x ON PG_GAME_ID = x.SPH_GAME_ID ")
    _str_sql.AppendLine(" ORDER BY x.Provider, GM_NAME, x.Terminal ")

    _str_sql.AppendLine(" ")
    _str_sql.AppendLine(" DROP TABLE #TMP_CONNECTED ")

    Return _str_sql.ToString()

  End Function ' GetSqlQueryProviderGameWithTerminal

  ' PURPOSE: Get Query mode Terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlQueryTerminal() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("SELECT   x.SPH_GAME_ID, ")
    _str_sql.AppendLine("         ISNULL(PG_GAME_NAME,'UNKNOWN') AS GM_NAME, ")
    _str_sql.AppendLine("         x.PlayedAmount, ")
    _str_sql.AppendLine("         x.WonAmount, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0)*100/x.PlayedAmount) END AS AmountPc, ")
    _str_sql.AppendLine("         PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0) AS Netwin, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE ((PlayedAmount-(WonAmount +  ProgressiveProvisionAmount - ProgressiveJackpotAmount0))*100/x.PlayedAmount) END AS NetwinPc, ")
    _str_sql.AppendLine("         x.PlayedCount, ")
    _str_sql.AppendLine("         x.WonCount, ")
    _str_sql.AppendLine("         CASE x.PlayedCount WHEN 0 THEN 0 ELSE ((x.WonCount* 100.0) /x.PlayedCount) END AS CountPerCent, ")
    _str_sql.AppendLine("         x.Provider, ")
    _str_sql.AppendLine("         x.TerminalId, ")
    _str_sql.AppendLine("         x.Terminal, ")
    _str_sql.AppendLine("         ISNULL(x.FloorId,'') AS FloorId, ")
    _str_sql.AppendLine("         x.TerminalCount, ")
    _str_sql.AppendLine("         CASE x.PlayedAmount WHEN 0 THEN 0 ELSE(x.TheoreticalWonAmount*100/PlayedAmount) END AS TheoreticalAmountPc, ")
    _str_sql.AppendLine("         TheoreticalWonAmount, ")
    _str_sql.AppendLine("         ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("         NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("         ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("         ProgressiveJackpotAmount0")
    _str_sql.AppendLine("  FROM   PROVIDERS_GAMES RIGHT JOIN ")
    _str_sql.AppendLine("         ( ")
    _str_sql.AppendLine("           SELECT   SPH_GAME_ID, ")
    _str_sql.AppendLine("                    sum(CAST(SPH_PLAYED_AMOUNT AS DECIMAL(22,4))) as PlayedAmount, ")
    _str_sql.AppendLine("                    sum(CAST(SPH_WON_AMOUNT AS DECIMAL(22,4))) AS WonAmount, ")
    _str_sql.AppendLine("                    CASE WHEN SUM(SPH_WON_COUNT)>SUM(SPH_PLAYED_COUNT) THEN SUM(SPH_WON_COUNT) ELSE SUM(SPH_PLAYED_COUNT) END AS PlayedCount, ")
    _str_sql.AppendLine("                    sum(SPH_WON_COUNT) AS WonCount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_PROVISION_AMOUNT,0)) AS ProgressiveProvisionAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_JACKPOT_AMOUNT,0) - ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS NonProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT,0)) AS ProgressiveJackpotAmount, ")
    _str_sql.AppendLine("                    sum(ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)) AS ProgressiveJackpotAmount0, ")
    _str_sql.AppendLine("                    TE_PROVIDER_ID AS Provider, ")
    _str_sql.AppendLine("                    SPH_TERMINAL_ID AS TerminalId,")
    _str_sql.AppendLine("                    SPH_TERMINAL_NAME AS Terminal,")
    _str_sql.AppendLine("                    TE_FLOOR_ID AS FloorId,")
    _str_sql.AppendLine("                    count(DISTINCT SPH_TERMINAL_ID) AS TerminalCount, ")
    _str_sql.AppendLine("                    sum(SPH_theoretical_won_amount) AS TheoreticalWonAmount       ")
    _str_sql.AppendLine("             FROM   SALES_PER_HOUR_V2 JOIN TERMINALS ON SPH_TERMINAL_ID = TE_TERMINAL_ID ")

    _str_sql.AppendLine(GetSqlWhere())
    _str_sql.AppendLine(" x ON PG_GAME_ID = x.SPH_GAME_ID ")
    _str_sql.AppendLine(" ORDER BY x.Provider, x.Terminal, GM_NAME ")

    Return _str_sql.ToString()

  End Function ' GetSqlQueryTerminal

  ' PURPOSE: Add DataRow to grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex
  '           - DbRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_AddDataRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)
    Dim _average_bet As Decimal
    Dim _terminal_data As List(Of Object)

    _average_bet = 0

    ' Date from
    If m_date_from <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(m_date_from, _
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = ""
    End If

    ' Date to
    If m_date_to <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(m_date_to, _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = ""
    End If

    ' Provider
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)

    ' Game
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_GAME_NAME)

    ' Terminal name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)

    ' Floor ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FLOOR_ID).Value = DbRow.Value(SQL_COLUMN_FLOOR_ID)

    ' Terminal Report
    If m_terminal_report_type = ReportType.Location Then
      If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
        _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
      Else
        _terminal_data = New List(Of Object)()
      End If
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next
    End If

    ' Num Terminals
    If m_group_mode = GROUP_MODE.PROVIDER_GAME_NON_TERMINAL Or m_group_mode = GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL Then
      If DbRow.Value(SQL_COLUMN_NUM_TERMINALS) > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TERMINALS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_TERMINALS), 0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_TERMINALS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_MASTER_TERMINALS), 0)
      End If
    End If
    If DbRow.Value(SQL_COLUMN_NUM_TERMINALS) > 0 Then
      subtotal_terminal_count += DbRow.Value(SQL_COLUMN_NUM_TERMINALS)
      total_terminal_count += DbRow.Value(SQL_COLUMN_NUM_TERMINALS)
    Else
      If subtotal_terminal_count = 0 Then
        subtotal_terminal_count += DbRow.Value(SQL_COLUMN_NUM_MASTER_TERMINALS)
      Else
        subtotal_terminal_count += DbRow.Value(SQL_COLUMN_NUM_TERMINALS)
      End If

    End If



    ' PlayedAmount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT))
    subtotal_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
    total_played_amount += DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)

    ' WonAmount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_WON_AMOUNT))
    subtotal_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)
    total_won_amount += DbRow.Value(SQL_COLUMN_WON_AMOUNT)

    ' AmountPerCent
    If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AMOUNT_PER_CENT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    'TheoricalAmount
    subtotal_theoretical_won_amount += DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT)
    total_theoretical_won_amount += DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT)

    'TheoricalAmountPerCent
    If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_THEORICAL_AMOUNT_PER_CENT), _
                                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    ' Non progressive Jackpot amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT))
    subtotal_non_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT)
    total_non_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT)

    ' Progressive Jackpot amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
    subtotal_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT)
    subtotal_progressive_jackpot_amount_0 += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0)
    total_progressive_jackpot_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT)
    total_progressive_jackpot_amount_0 += DbRow.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT_0)

    ' Progressive provision amount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT))
    subtotal_progressive_provision_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT)
    total_progressive_provision_amount += DbRow.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT)

    ' NetWin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN))

    ' Netwin Total Per cent: GRID_COLUMN_NW_TOTAL_PER_CENT
    ' These values are assigned in GUI_AfterLastRow()

    ' PlayedCount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_COUNT), _
                                                                              ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    subtotal_played_count += DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
    total_played_count += DbRow.Value(SQL_COLUMN_PLAYED_COUNT)

    ' WonCount
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WON_COUNT), _
                                                                           ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    subtotal_won_count += DbRow.Value(SQL_COLUMN_WON_COUNT)
    total_won_count += DbRow.Value(SQL_COLUMN_WON_COUNT)

    ' CountPerCent
    If DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) = 0.0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COUNT_PER_CENT), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    End If

    If m_group_mode = GROUP_MODE.PROVIDER_GAME_NON_TERMINAL Or m_group_mode = GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL Then
      If DbRow.Value(SQL_COLUMN_NUM_TERMINALS) > 0 Then
        ' Netwin per terminals
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN) / DbRow.Value(SQL_COLUMN_NUM_TERMINALS))

        ' Played per terminals
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / DbRow.Value(SQL_COLUMN_NUM_TERMINALS))
      Else
        If DbRow.Value(SQL_COLUMN_NUM_MASTER_TERMINALS) > 0 Then
          ' Netwin per terminals
          Me.Grid.Cell(RowIndex, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NETWIN) / DbRow.Value(SQL_COLUMN_NUM_MASTER_TERMINALS))

          ' Played per terminals
          Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / DbRow.Value(SQL_COLUMN_NUM_MASTER_TERMINALS))
        End If
      End If
    End If

    ' Average bet
    If Not DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT) And Not DbRow.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      If DbRow.Value(SQL_COLUMN_PLAYED_COUNT) = 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        _average_bet = Math.Round(DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT) / DbRow.Value(SQL_COLUMN_PLAYED_COUNT), 2, MidpointRounding.AwayFromZero)

        If _average_bet = 0 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, _
                                                                                     ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AVERAGE_BET).Value = ""
    End If

  End Sub ' GUI_AddDataRow

  ' PURPOSE: Insert Subtotal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertSubtotal()

    Dim _idx_row As Integer = Me.Grid.NumRows - 1
    Dim _amount_per_cent As Decimal
    Dim _netwin As Decimal = subtotal_played_amount - (subtotal_won_amount + subtotal_progressive_provision_amount - subtotal_progressive_jackpot_amount_0)
    Dim _count_per_cent As Decimal
    Dim _total_average_bet As Decimal
    Dim _theoretical_amount_per_cent As Decimal

    _total_average_bet = 0

    ' Subtotal
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE_TO).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)

    If m_group_mode = GROUP_MODE.PROVIDER_GAME_WITH_TERMINAL Then
      ' Provider
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = subtotal_provider

      ' Game
      Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value = Me.Grid.Cell(_idx_row - 1, GRID_COLUMN_GAME_NAME).Value
    ElseIf m_group_mode = GROUP_MODE.TERMINAL Then
      ' Terminal
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = subtotal_terminal

      If subtotal_terminal_count > 1 Then
        ' Game
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634)
      Else
        ' Provider
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PROVIDER).Value = Me.Grid.Cell(_idx_row - 1, GRID_COLUMN_PROVIDER).Value

        ' Game
        Me.Grid.Cell(_idx_row, GRID_COLUMN_GAME_NAME).Value = Me.Grid.Cell(_idx_row - 1, GRID_COLUMN_GAME_NAME).Value
      End If
    End If

    Me.Grid.Cell(_idx_row, GRID_COLUMN_NUM_TERMINALS).Value = GUI_FormatNumber(subtotal_terminal_count, 0)

    ' Played Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(subtotal_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Won Amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(subtotal_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Amount Per cent
    If subtotal_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    Else
      _amount_per_cent = ((subtotal_won_amount + subtotal_progressive_provision_amount - subtotal_progressive_jackpot_amount_0) / subtotal_played_amount) * 100
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_amount_per_cent, 2) & "%"
    End If

    ' Theoretical Amount Per cent
    If subtotal_theoretical_won_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = String.Empty
    Else
      _theoretical_amount_per_cent = (subtotal_theoretical_won_amount / subtotal_played_amount) * 100
      Me.Grid.Cell(_idx_row, GRID_COLUMN_THEORICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(_theoretical_amount_per_cent, 2) & "%"
    End If

    ' Non progressive Jackpot amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(subtotal_non_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive Jackpot amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(subtotal_progressive_jackpot_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Progressive provision amount
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(subtotal_progressive_provision_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Netwin
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Played Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(subtotal_played_count, 0)

    ' Won Count
    Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(subtotal_won_count, 0)

    ' Count per cent
    If subtotal_played_count > 0 Then
      _count_per_cent = (subtotal_won_count / subtotal_played_count) * 100
    Else
      _count_per_cent = 0
    End If
    If subtotal_played_amount = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(_count_per_cent, 2) & "%"
    End If

    ' Count Netwin per terminal
    If subtotal_terminal_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NW_PER_TERMINAL).Value = String.Empty
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_NW_PER_TERMINAL).Value = GUI_FormatCurrency(_netwin / subtotal_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Count Played per terminal
    If subtotal_terminal_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = String.Empty
    Else
      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_PER_TERMINAL).Value = GUI_FormatCurrency(subtotal_played_amount / subtotal_terminal_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    ' Netwin Total Per cent
    ' These values are assigned in GUI_AfterLastRow()

    ' Average bet
    If subtotal_played_count = 0 Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
    Else
      _total_average_bet = Math.Round(subtotal_played_amount / subtotal_played_count, 2, MidpointRounding.AwayFromZero)

      If _total_average_bet = 0 Then
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_total_average_bet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    End If

    ' Inicialize counts
    subtotal_played_amount = 0
    subtotal_won_amount = 0
    subtotal_played_count = 0
    subtotal_won_count = 0
    subtotal_terminal_count = 0
    subtotal_theoretical_won_amount = 0
    subtotal_progressive_provision_amount = 0
    subtotal_non_progressive_jackpot_amount = 0
    subtotal_progressive_jackpot_amount = 0
    subtotal_progressive_jackpot_amount_0 = 0

    Me.Grid.AddRow()

  End Sub ' InsertSubtotal

#End Region  ' Private Functions

#Region " Events"

  Private Sub opt_terminal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_terminal.CheckedChanged
    Me.chk_show_terminals.Enabled = Not Me.opt_terminal.Checked
    Me.chk_terminal_location.Enabled = Me.opt_terminal.Checked Or Me.chk_show_terminals.Checked
  End Sub

  Private Sub chk_show_terminals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_terminals.CheckedChanged
    Me.chk_terminal_location.Enabled = Me.chk_show_terminals.Checked
  End Sub

#End Region 'Events

End Class
