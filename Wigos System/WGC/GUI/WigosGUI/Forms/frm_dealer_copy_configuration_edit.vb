﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_dealer_copy_configuration_edit.vb
' DESCRIPTION:   Dealer copy and TITO ticket configuration
' AUTHOR:        Rafa Arjona
' CREATION DATE: 05-JUL-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 05-JUL-2017  RAB     Initial version (PBI 28421: WIGOS-1425 MES10 Ticket validation - Dealer copy tickets configuration)
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports WSI.Common

#End Region

Public Class frm_dealer_copy_configuration_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const FORM_DB_MIN_VERSION As Short = 173

#End Region

#Region " Members "

  Private m_dealer_copy_configuration As CLASS_DEALER_COPY_CONFIGURATION

#End Region

#Region " Public funtions"

  ''' <summary>
  ''' Initializes the form id.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_DEALER_COPY_CONFIGURATION
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()
  End Sub

#End Region

#Region " Overrides functions "

  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8460)                                             ' Dealer copy / tickets TITO configuration

    Me.gb_dealer_copy.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8461)                              ' Dealer copy

    Me.gb_dealer_copy_gui.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819)                          ' WigosGUI
    Me.chk_dealer_copy_gui_cage_collection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8462)         ' Cage collection
    Me.chk_dealer_copy_gui_ticket_report.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2124)           ' Ticket report
    Me.chk_dealer_copy_gui_tickets_audit_report.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7674)    ' Tickets audit
    Me.chk_dealer_copy_gui_soft_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5638)              ' Soft Count

    Me.gb_dealer_copy_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616)                      ' Cashier
    Me.chk_dealer_copy_cashier_redeem_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8463)      ' Redeem tickets
    Me.chk_dealer_copy_cashier_validation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2710)          ' Validation
    Me.chk_dealer_copy_cashier_offline.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)             ' Offline

    Me.gb_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4600)                             ' Tickets TITO

    Me.gb_tickets_tito_gui.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1819)                         ' WigosGUI
    Me.chk_tickets_tito_gui_reconcile.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6525)              ' Reconcile

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = True

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_GetScreenData()
    Try
      If Me.IsDisposed Then
        Return
      End If

      m_dealer_copy_configuration = Me.DbEditedObject

      m_dealer_copy_configuration.CageCollectionEnabled = Me.chk_dealer_copy_gui_cage_collection.Checked
      m_dealer_copy_configuration.TicketReportEnabled = Me.chk_dealer_copy_gui_ticket_report.Checked
      m_dealer_copy_configuration.TicketsAuditEnabled = Me.chk_dealer_copy_gui_tickets_audit_report.Checked
      m_dealer_copy_configuration.SoftCountEnabled = Me.chk_dealer_copy_gui_soft_count.Checked
      m_dealer_copy_configuration.RedeemTicketsEnabled = Me.chk_dealer_copy_cashier_redeem_tickets.Checked
      m_dealer_copy_configuration.ValidationEnabled = Me.chk_dealer_copy_cashier_validation.Checked
      m_dealer_copy_configuration.OfflineEnabled = Me.chk_dealer_copy_cashier_offline.Checked
      m_dealer_copy_configuration.ReconcileEnabled = Me.chk_tickets_tito_gui_reconcile.Checked

    Catch ex As Exception
      ' Save error in log
      Log.Error("Dealer copy configuration: Error when save configuration.")
    End Try

  End Sub 'GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    If Me.IsDisposed Then
      Return
    End If

    Me.chk_dealer_copy_gui_cage_collection.Checked = m_dealer_copy_configuration.CageCollectionEnabled
    Me.chk_dealer_copy_gui_ticket_report.Checked = m_dealer_copy_configuration.TicketReportEnabled
    Me.chk_dealer_copy_gui_tickets_audit_report.Checked = m_dealer_copy_configuration.TicketsAuditEnabled
    Me.chk_dealer_copy_gui_soft_count.Checked = m_dealer_copy_configuration.SoftCountEnabled
    Me.chk_dealer_copy_cashier_redeem_tickets.Checked = m_dealer_copy_configuration.RedeemTicketsEnabled
    Me.chk_dealer_copy_cashier_validation.Checked = m_dealer_copy_configuration.ValidationEnabled
    Me.chk_dealer_copy_cashier_offline.Checked = m_dealer_copy_configuration.OfflineEnabled
    Me.chk_tickets_tito_gui_reconcile.Checked = m_dealer_copy_configuration.ReconcileEnabled
  End Sub 'GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      'Wrong DbVersion message already shown, do not show again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_DEALER_COPY_CONFIGURATION
        m_dealer_copy_configuration = DbEditedObject
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        Call Me.Close()

      Case ENUM_BUTTON.BUTTON_OK
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        '

    End Select

    If GUI_HasToBeAudited(ButtonId) Then
      Call MyBase.AuditFormClick(ButtonId)
    End If
  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Define the control which have the focus when the form is initially shown
  ''' </summary>
  ''' <remarks></remarks>
  'Protected Overrides Sub GUI_SetInitialFocus()
  '  Me.ActiveControl = Me.chk_dealer_copy_gui_cage_collection
  'End Sub ' GUI_SetInitialFocus

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return True
  End Function ' GUI_IsScreenDataOk

#End Region

End Class