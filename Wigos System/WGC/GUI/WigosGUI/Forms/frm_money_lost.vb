'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_money_lost
' DESCRIPTION:   Edit number of lost notes (terminal_money)
' AUTHOR:        Susana Sams�
' CREATION DATE: 26-ABR-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 26-ABR-2012 SSC    Initial version
'-------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

Public Class frm_money_lost
  Inherits frm_base_edit

#Region " Members "

  Private m_sql_dataset As DataSet
  Private m_data_view_amount_num As DataView
  Private m_ds_money_collections As DataSet = Nothing

#End Region ' Members

#Region " Constants "
  Private Const RELATION_COLLECTION_TERMINALMONEY As String = "Collection_TerminalMoney"
  Private Const RELATION_COLLECTION_DETAILS As String = "Collection_Details"

#End Region

#Region " Properties"


#End Region ' Properties

#Region " Enums "
  Private Enum ENUM_TABLES
    COLLECTION = 0
    COLLECTION_DETAILS = 1
    TERMINAL_MONEY = 2
  End Enum

  Private Enum ENUM_COLLECTION_TYPE
    NOT_ASSIGNED = -1
    COLLECTED = 0
    LOST = 1
  End Enum
#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MONEY_LOST

    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(675)                    ' 675 "Billetes perdidos"

    Me.gb_num_lost_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(675)
    Me.gb_num_lost_notes.Enabled = True

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
 
  End Sub ' GUI_InitControls

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dg_money_lost

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        Call SaveChanges()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - TableLostNotes
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal TableLostNotes As DataTable)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    TableLostNotes.Columns.Add("NUM_LOST", Type.GetType("System.Int32"))

    For Each _row As DataRow In TableLostNotes.Rows
      _row("NUM_LOST") = _row("NUM_REPORTED")
    Next

    m_sql_dataset = New DataSet
    m_sql_dataset.Tables.Add(TableLostNotes)

    Call Me.Display(True)

  End Sub 'ShowEditItem

  ' PURPOSE: Create DataGridViews with SQL queries, and show info in screen.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _table_style As DataGridTableStyle
    Dim _text_column As DataGridTextBoxColumn

    ' Initialize DataView for database
    Try
    
      m_sql_dataset.Tables(0).Columns("TERMINAL_NAME").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("AMOUNT").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("NUM_REPORTED").ReadOnly = True
      m_sql_dataset.Tables(0).Columns("NUM_LOST").ReadOnly = False

      m_data_view_amount_num = New DataView(m_sql_dataset.Tables(0))
      m_data_view_amount_num.AllowNew = False
      m_data_view_amount_num.AllowDelete = False

      dg_money_lost.Enabled = True
      dg_money_lost.DataSource = m_data_view_amount_num
      dg_money_lost.AllowNavigation = False
      dg_money_lost.CaptionVisible = False
      dg_money_lost.AllowDrop = False
      '570

      ' Columns Style
      If IsNothing(dg_money_lost.TableStyles.Item(m_data_view_amount_num.Table.TableName)) Then
        _table_style = New DataGridTableStyle
        _table_style.MappingName = m_data_view_amount_num.Table.TableName

        _text_column = New DataGridTextBoxColumn
        _text_column.MappingName = "TERMINAL_ID"
        _text_column.ReadOnly = True
        _text_column.HeaderText = GLB_NLS_GUI_STATISTICS.GetString(300)       ' 300 "Terminal"
        _text_column.Width = 0
        _table_style.GridColumnStyles.Add(_text_column)

        _text_column = New DataGridTextBoxColumn
        _text_column.MappingName = "TERMINAL_NAME"
        _text_column.ReadOnly = True
        _text_column.HeaderText = GLB_NLS_GUI_STATISTICS.GetString(300)       ' 300 "Terminal"
        _text_column.Width = 150
        _table_style.GridColumnStyles.Add(_text_column)

        _text_column = New DataGridTextBoxColumn
        _text_column.MappingName = "AMOUNT"
        _text_column.ReadOnly = True
        _text_column.HeaderText = GLB_NLS_GUI_INVOICING.GetString(443)        ' 443 "Nominal"
        _text_column.Width = 120
        _table_style.GridColumnStyles.Add(_text_column)

        _text_column = New DataGridTextBoxColumn
        _text_column.MappingName = "NUM_REPORTED"
        _text_column.ReadOnly = True
        _text_column.HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(677)    ' 445 "Aceptado"
        _text_column.Width = 120
        _table_style.GridColumnStyles.Add(_text_column)

        _text_column = New DataGridTextBoxColumn
        _text_column.MappingName = "NUM_LOST"
        _text_column.ReadOnly = False
        _text_column.HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(672)  ' 672 "Perdido"
        _text_column.Width = 120
        _table_style.GridColumnStyles.Add(_text_column)

        dg_money_lost.TableStyles.Add(_table_style)

      End If

    Catch _ex As Exception
      ' Do nothing
      Debug.WriteLine(_ex.Message)

    Finally
    End Try

  End Sub ' GUI_SetScreenData

#End Region ' Overrides

#Region " Public Functions "

#End Region

#Region " Private functions "

  ' PURPOSE: Save DataGridView data to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub SaveChanges()

    Dim _dt_changes As New DataTable
    Dim _data_row As DataRow
    Dim _dt_rows_not_lost As DataRow()
    Dim _already_updated As Boolean

    Try
      If IsNothing(m_data_view_amount_num) Or IsNothing(m_data_view_amount_num.Table) Then
        Exit Sub
      End If

      ' All num_collected from grid must have a value 
      If m_data_view_amount_num.Table.Select("NUM_LOST IS NULL ").Length > 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(640), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
        Exit Sub
      End If

      ' The number of lost notes cannot be greater than reported by Note Acceptor
      If m_data_view_amount_num.Table.Select("NUM_LOST > NUM_REPORTED").Length > 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(676), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
        Exit Sub
      End If

      Call TrimDataValues(m_data_view_amount_num.Table.Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added))
      Call TrimDataValues(m_sql_dataset.Tables(0).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added))

      '
      ' UPDATE 
      '
      _already_updated = False
      Using _db_trx As DB_TRX = New DB_TRX()

        'Register Money as lost
        If Not RegisterNotesAsLost(_db_trx.SqlTransaction, _already_updated) Then
          Throw New Exception("Error. RegisterNotesAsLost")
        End If

        If m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).Rows.Count = 0 And _already_updated Then
          Call Me.Close()
          Exit Sub
        End If

        _db_trx.Commit()
      End Using ' Using _db_trx

      ' Remove not lost notes (with 0 value) from dt_changes 
      _dt_changes = m_data_view_amount_num.Table
      _dt_rows_not_lost = m_data_view_amount_num.Table.Select("NUM_LOST = 0")

      For Each _data_row In _dt_rows_not_lost
        _dt_changes.Rows.Remove(_data_row)
      Next

      ' Auditory
      GUI_WriteAuditoryChanges(True, _dt_changes)

      m_sql_dataset.AcceptChanges()

      Call Me.Close()

    Catch _ex As Exception
      m_sql_dataset.AcceptChanges()
      Debug.WriteLine(_ex.Message)

      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _ex.Message)

    Finally
    End Try

  End Sub ' SaveChanges

  ' PURPOSE: RegisterNotesAsLost --> INSERT into MONEY_COLLECTIONS and MONEY_COLLECTION_DETAILS and UPDATE TERMINAL_MONEY
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if changes saved
  '
  ' NOTES :
  '  Steps:
  '        1. Create DataSet MoneyCollections with new tables: "MONEY_COLLECTIONS", "MONEY_COLLECTIONS_DETAILS" and "TERMINAL_MONEY"
  '        2. Get all terminals from dataview table
  '        3. For Each amount of current terminal
  '        4. Get values from grid
  '        5. Get older registers in the "terminal_money" table and fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables)
  '        6. Insert into MONEY_COLLECTIONS
  '        7. Update TERMINAL_MONEY
  '        8. Insert into MONEY_COLLECTION_DETAILS
  '
  Private Function RegisterNotesAsLost(ByVal SqlTrx As SqlTransaction, ByRef AlreadyUpdated As Boolean) As Boolean

    Dim _terminal_id As Int64
    Dim _current_amount As Int32
    Dim _num_lost As Int32
    Dim _num_reported As Int32

    Dim _terminals As List(Of Integer)
    Dim _collection_id As Long

    Try

      '1. Create DataSet MoneyCollections with new tables: "MONEY_COLLECTIONS", "MONEY_COLLECTIONS_DETAILS" and "TERMINAL_MONEY"
      CreateDataSetMoneyCollections()

      _collection_id = -1

      '2. Get all terminals from dataview table
      _terminals = New List(Of Integer)

      For Each _terminal_row As DataRow In m_data_view_amount_num.Table.Rows
        _terminal_id = _terminal_row("TERMINAL_ID")
        If Not _terminals.Contains(_terminal_id) Then
          _terminals.Add(_terminal_id)
        End If
      Next

      ' For Each terminal
      For Each _current_terminal_id As Int32 In _terminals

        ' 3. For Each amount of current terminal
        For Each _row_register As DataRow In m_data_view_amount_num.Table.Select("TERMINAL_ID = " & _current_terminal_id)

          '4. Get values from grid
          _num_lost = _row_register("NUM_LOST")
          If _num_lost = 0 Then
            Continue For
          End If

          _current_amount = GUI_ParseCurrency(_row_register("AMOUNT"))
          _num_reported = _row_register("NUM_REPORTED")

          '5. Get older registers in the "terminal_money" table and fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables)
          AlreadyUpdated = False
          If Not RegisterMoneyByFaceTerminal(_current_terminal_id, _current_amount, _collection_id, _num_reported, _num_lost, SqlTrx, AlreadyUpdated) Then
            Throw New Exception("Error. RegisterMoneyByFaceTerminal")
          End If
        Next

        If m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Select("MC_COLLECTION_ID = " & _collection_id).Length = 1 Then
          _collection_id -= 1

        End If 'For each amount

      Next 'For each terminal


      If m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Rows.Count = 0 Then
        Return True
      End If

      If m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).Rows.Count = 0 And AlreadyUpdated Then
        Return True
      End If

      '6. Insert into MONEY_COLLECTIONS
      If Not InsertMoneyCollection(SqlTrx) Then
        Throw New Exception("Error. InsertMoneyCollection")
      End If

      '7. Update TERMINAL_MONEY
      If Not UpdateTerminalMoney(SqlTrx) Then
        Throw New Exception("Error. UpdateTerminalMoney")
      End If

      '8. Insert into MONEY_COLLECTION_DETAILS
      If Not InsertMoneyCollectionDetails(SqlTrx) Then
        Throw New Exception("Error. InsertMoneyCollectionDetails")
      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(109), _
                             GLB_NLS_GUI_AUDITOR.GetString(109) & _ex.Message, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End Try

    Return True

  End Function ' RegisterNotesAsLost

  ' PURPOSE: Create a Dataset with tables MONEY_COLLECTION, MONEY_COLLECTION_DETAILS, TERMINAL_MONEY
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  '
  ' NOTES :
  '
  Public Sub CreateDataSetMoneyCollections()

    Dim _table As DataTable
    Dim _column_parent As DataColumn
    Dim _column_child As DataColumn
    Dim _data_relation As DataRelation

    m_ds_money_collections = New DataSet()

    ' Create Collection table
    _table = New DataTable("MONEY_COLLECTION")
    _table.Columns.Add("MC_COLLECTION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("MC_TERMINAL_ID", Type.GetType("System.Int32"))
    m_ds_money_collections.Tables.Add(_table)

    ' Create Collection Details table
    _table = New DataTable("MONEY_COLLECTION_DETAILS")
    _table.Columns.Add("MCD_COLLECTION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("MCD_FACE_VALUE", Type.GetType("System.Int32"))
    _table.Columns.Add("MCD_NUM_EXPECTED", Type.GetType("System.Int32"))
    _table.Columns.Add("MCD_NUM_COLLECTED", Type.GetType("System.Int32"))
    m_ds_money_collections.Tables.Add(_table)

    ' Create Terminal Money table
    _table = New DataTable("TERMINAL_MONEY")
    _table.Columns.Add("TM_TERMINAL_ID", Type.GetType("System.Int32"))
    _table.Columns.Add("TM_TRANSACTION_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("TM_STACKER_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("TM_AMOUNT", Type.GetType("System.Int32"))
    _table.Columns.Add("TM_COLLECTION_ID", Type.GetType("System.Int64"))
    m_ds_money_collections.Tables.Add(_table)

    ' Relations
    _column_parent = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Columns("MC_COLLECTION_ID")

    _column_child = m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).Columns("TM_COLLECTION_ID")
    _data_relation = New DataRelation(RELATION_COLLECTION_TERMINALMONEY, _column_parent, _column_child, True)
    m_ds_money_collections.Relations.Add(_data_relation)

    _column_child = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Columns("MCD_COLLECTION_ID")
    _data_relation = New DataRelation(RELATION_COLLECTION_DETAILS, _column_parent, _column_child, True)
    m_ds_money_collections.Relations.Add(_data_relation)

  End Sub ' CreateDataSetMoneyCollections

  ' PURPOSE: Fill dataset (MoneyCollection, MoneyCollectionDetails and TerminalMoney tables)
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalId
  '           - Amount
  '           - CollectionId
  '           - RowTerminal
  '           - StackerOpenId
  '           - OpenStackers
  '           - NumExpected
  '           - NumCollectedGrid
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: _min_open_stacker_found
  '
  ' NOTES :
  '     - Steps:
  '            1. Add to Collection table
  '            2. Add to Collection Detail table
  '            3. Select Terminal Money registers pending to update
  '            4. Add to Terminal Money table
  '
  Private Function RegisterMoneyByFaceTerminal(ByVal TerminalId As Int64, ByVal Amount As Decimal, ByVal CollectionId As Int64, _
                                              ByVal NumReported As Int32, ByVal NumLost As Int32, ByVal SqlTrx As SqlTransaction, ByRef AlreadyUpdated As Boolean) As Boolean

    Dim _sql_str As String
    Dim _note_lost As Integer
    Dim _transaction_id As Int64
    Dim _stacker_id As Int64
    Dim _row As DataRow

    _note_lost = 0
    _transaction_id = 0
    _stacker_id = 0

    Try

      '1. Add to Collection table
      If m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Select("MC_COLLECTION_ID = " & CollectionId).Length = 0 Then
        _row = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).NewRow()
        _row("MC_COLLECTION_ID") = CollectionId
        _row("MC_TERMINAL_ID") = TerminalId
        m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION).Rows.Add(_row)
      End If

      '2. Add to Collection Detail table
      _row = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).NewRow()
      _row("MCD_COLLECTION_ID") = CollectionId
      _row("MCD_FACE_VALUE") = Amount
      _row("MCD_NUM_COLLECTED") = NumLost
      _row("MCD_NUM_EXPECTED") = NumReported
      m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS).Rows.Add(_row)

      '3. Select Terminal Money registers to update
      While _note_lost < NumLost

        _sql_str = "SELECT   MIN(TM_REPORTED)                     " & _
                   "       , MIN(TM_TRANSACTION_ID)               " & _
                   "       , MIN(TM_STACKER_ID)                   " & _
                   "  FROM   TERMINAL_MONEY                       " & _
                   " WHERE   TM_TERMINAL_ID   = @pTerminalId      " & _
                   "   AND   TM_AMOUNT        = @pAmount          " & _
                   "   AND   TM_COLLECTION_ID IS NULL             " & _
                   "   AND   TM_TRANSACTION_ID > @pTransactionId  "

        Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount
          _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = _transaction_id

          Using _sql_reader As SqlDataReader = _sql_cmd.ExecuteReader()
            If Not _sql_reader.Read() Then
              Throw New Exception()
            End If
            If _sql_reader.IsDBNull(1) Then
              AlreadyUpdated = True

              Return True
            End If
            _transaction_id = _sql_reader.GetInt64(1)
            _stacker_id = _sql_reader.GetInt64(2)

          End Using 'using _sql_reader

        End Using 'using _cql_cmd 

        '4. Add to Terminal Money table
        _row = m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).NewRow()
        _row("TM_TERMINAL_ID") = TerminalId
        _row("TM_TRANSACTION_ID") = _transaction_id
        _row("TM_STACKER_ID") = _stacker_id
        _row("TM_AMOUNT") = Amount
        _row("TM_COLLECTION_ID") = CollectionId
        m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY).Rows.Add(_row)

        _note_lost = _note_lost + 1

      End While

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "money_lost", "RegisterMoneyByFaceTerminal", _ex.Message)

      Return False
    End Try

    Return True
  End Function 'RegisterMoneyByFaceTerminal

  ' PURPOSE: Insert values into MONEY_COLLECTIONS table
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if Collection inserted, false if not.
  '
  ' NOTES :
  '  
  Private Function InsertMoneyCollection(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _parameter As SqlParameter
    Dim _num_rows_inserted As Integer
    Dim _dt_collection As DataTable

    Try

      _dt_collection = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION)

      ' MONEY_COLLECTIONS
      _sql_str = " INSERT INTO   MONEY_COLLECTIONS                  " & _
                 "             ( MC_DATETIME                        " & _
                 "             , MC_TERMINAL_ID                     " & _
                 "             , MC_USER_ID                         " & _
                 "             , MC_MONEY_LOST                      " & _
                 "             )                                    " & _
                 "      VALUES ( @pDatetime                         " & _
                 "             , @pTerminalId                       " & _
                 "             , @pUserId                           " & _
                 "             , @pMoneyLost                        " & _
                 "             )                                    " & _
                 "         SET   @pCollectionId = SCOPE_IDENTITY()  "

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
        With _sql_adap
          .SelectCommand = Nothing
          .DeleteCommand = Nothing
          .UpdateCommand = Nothing

          Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MC_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = GLB_CurrentUser.Id
            _sql_cmd.Parameters.Add("@pMoneyLost", SqlDbType.Bit).Value = ENUM_COLLECTION_TYPE.LOST
            _parameter = _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt)
            _parameter.SourceColumn = "MC_COLLECTION_ID"
            _parameter.Direction = ParameterDirection.Output

            _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
            .InsertCommand = _sql_cmd

            .UpdateBatchSize = 500
            _num_rows_inserted = .Update(_dt_collection)

            If _num_rows_inserted = 0 Then
              Return False
            End If

          End Using

        End With
      End Using ' _sql_adap for TerminalMoney

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "money_lost", "InsertMoneyCollection", _ex.Message)

      Return False
    End Try

    Return True
  End Function 'InsertMoneyCollection

  ' PURPOSE: Update IdCollection value in TERMINAL_MONEY table
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: true if rows updated
  '
  ' NOTES :
  '  
  Private Function UpdateTerminalMoney(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _num_rows_updated As Integer
    Dim _dt_terminal_money As DataTable

    Try

      _dt_terminal_money = m_ds_money_collections.Tables(ENUM_TABLES.TERMINAL_MONEY)

      'TERMINAL_MONEY
      _sql_str = "UPDATE   TERMINAL_MONEY                       " & _
                 "   SET   TM_COLLECTION_ID  = @pCollectionId   " & _
                 " WHERE   TM_TERMINAL_ID    = @pTerminalId     " & _
                 "   AND   TM_TRANSACTION_ID = @pTransactionId  " & _
                 "   AND   TM_STACKER_ID     = @pStackerId      " & _
                 "   AND   TM_AMOUNT         = @pAmount         "

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()

        With _sql_adap
          .SelectCommand = Nothing
          .UpdateCommand = Nothing
          .DeleteCommand = Nothing

          Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TM_TERMINAL_ID"
            _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).SourceColumn = "TM_TRANSACTION_ID"
            _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).SourceColumn = "TM_STACKER_ID"
            _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).SourceColumn = "TM_AMOUNT"
            _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "TM_COLLECTION_ID"

            _sql_cmd.UpdatedRowSource = UpdateRowSource.None
            ' We use the InsertCommand because the RowState of the DataTable is Added.
            .InsertCommand = _sql_cmd

            .UpdateBatchSize = 500

            _num_rows_updated = .Update(_dt_terminal_money)

            If _num_rows_updated = 0 Then
              Return False
            End If

          End Using

        End With
      End Using  ' _sql_adap for TerminalMoney

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "money_lost", "UpdateTerminalMoney", _ex.Message)

      Return False
    End Try

    Return True
  End Function 'UpdateTerminalMoney

  ' PURPOSE: Insert values into MONEY_COLLECTION_DETAILS table
  '
  '  PARAMS:
  '     - INPUT:
  '           - SqlTrx
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if Collection inserted, false if not.
  '
  ' NOTES :
  '  
  Private Function InsertMoneyCollectionDetails(ByVal SqlTrx As SqlTransaction) As Boolean

    Dim _sql_str As String
    Dim _num_rows_inserted_details As Integer
    Dim _dt_collection_details As DataTable

    Try

      _dt_collection_details = m_ds_money_collections.Tables(ENUM_TABLES.COLLECTION_DETAILS)

      ' MONEY_COLLECTIONS
      _sql_str = "     INSERT INTO   MONEY_COLLECTION_DETAILS         " & _
                 "                 ( MCD_COLLECTION_ID                " & _
                 "                 , MCD_FACE_VALUE                   " & _
                 "                 , MCD_NUM_EXPECTED                 " & _
                 "                 , MCD_NUM_COLLECTED                " & _
                 "                 )                                  " & _
                 "          VALUES ( @pCollectionId                   " & _
                 "                 , @pFaceValue                      " & _
                 "                 , @pNumExpected                    " & _
                 "                 , @pNumCollected                   " & _
                 "                 )                                  "

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
        With _sql_adap
          .SelectCommand = Nothing
          .UpdateCommand = Nothing
          .DeleteCommand = Nothing

          Using _sql_cmd As SqlCommand = New SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx)

            _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MCD_COLLECTION_ID"
            _sql_cmd.Parameters.Add("@pFaceValue", SqlDbType.Money).SourceColumn = "MCD_FACE_VALUE"
            _sql_cmd.Parameters.Add("@pNumExpected", SqlDbType.Int).SourceColumn = "MCD_NUM_EXPECTED"
            _sql_cmd.Parameters.Add("@pNumCollected", SqlDbType.Int).SourceColumn = "MCD_NUM_COLLECTED"

            _sql_cmd.UpdatedRowSource = UpdateRowSource.None
            .InsertCommand = _sql_cmd

            .UpdateBatchSize = 500

            _num_rows_inserted_details = .Update(_dt_collection_details)

            If _num_rows_inserted_details = 0 Then
              Return False
            End If

          End Using

        End With
      End Using ' _sql_adap for TerminalMoney

    Catch _ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "money_lost", "InsertMoneyCollectionDetails", _ex.Message)

      Return False
    End Try

    Return True
  End Function 'InsertMoneyCollectionDetails

  ' PURPOSE: Write auditory changes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Server As Boolean
  '           - ChangesDT As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_WriteAuditoryChanges(ByVal Server As Boolean, ByVal ChangesDT As DataTable)

    Dim _original_modified_dv As DataView
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _original_auditor_data As CLASS_AUDITOR_DATA
    Dim _idx_change As Integer
    Dim _idx_original As Integer
    Dim _value_string As String

    If IsNothing(ChangesDT) Or ChangesDT.Rows.Count = 0 Then
      Exit Sub
    End If

    _original_modified_dv = New DataView(ChangesDT, "", "", DataViewRowState.CurrentRows)

    '---------------------------------------------
    ' Insert MODIFIED items
    '---------------------------------------------
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _original_auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    ' Set Parameter
    Call _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(675), " ")
    Call _original_auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(675), " ")

    For _idx_change = 0 To ChangesDT.Rows.Count - 1
      '
      ' Details for current rows
      '

      ' Value
      _value_string = IIf(IsDBNull(ChangesDT.Rows(_idx_change).Item("NUM_LOST")), "", ChangesDT.Rows(_idx_change).Item("NUM_LOST"))
      Call _auditor_data.SetField(0, IIf(_value_string = "", AUDIT_NONE_STRING, _value_string), GLB_NLS_GUI_PLAYER_TRACKING.GetString(680) + ChangesDT.Rows(_idx_change).Item("AMOUNT"))

      ' Search original row
      For _idx_original = 0 To _original_modified_dv.Count - 1
        If _original_modified_dv(_idx_original).Item("AMOUNT") = ChangesDT.Rows(_idx_change).Item("AMOUNT") Then
          Exit For
        End If
      Next

      ' Critical Error: exit
      If _idx_original = _original_modified_dv.Count Then
        ' Logger message 

        Exit Sub
      End If

      '
      ' Details for original rows
      '

      ' Value
      _value_string = 0
      Call _original_auditor_data.SetField(0, IIf(_value_string = "", AUDIT_NONE_STRING, _value_string), _original_modified_dv(_idx_original).Item("AMOUNT"))

    Next

    If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                               GLB_CurrentUser.Id, _
                               GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                               0, _
                               _original_auditor_data) Then
      ' Logger message 
    End If

    _auditor_data = Nothing
    _original_auditor_data = Nothing

  End Sub 'GUI_WriteAuditoryChanges

#End Region

End Class