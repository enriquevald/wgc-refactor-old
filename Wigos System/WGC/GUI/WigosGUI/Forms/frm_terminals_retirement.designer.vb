<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_retirement
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
Me.btn_ok = New GUI_Controls.uc_button
Me.btn_cancel = New GUI_Controls.uc_button
Me.uc_grid_terminals = New GUI_Controls.uc_grid
Me.dtp_retirement_date = New GUI_Controls.uc_date_picker
Me.panel_data.SuspendLayout()
Me.SuspendLayout()
'
'panel_data
'
Me.panel_data.Controls.Add(Me.dtp_retirement_date)
Me.panel_data.Controls.Add(Me.uc_grid_terminals)
Me.panel_data.Size = New System.Drawing.Size(499, 253)
'
'btn_ok
'
Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_ok.Location = New System.Drawing.Point(0, 12)
Me.btn_ok.Name = "btn_ok"
Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
Me.btn_ok.Size = New System.Drawing.Size(90, 30)
Me.btn_ok.TabIndex = 3
Me.btn_ok.ToolTipped = False
Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'btn_cancel
'
Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_cancel.Location = New System.Drawing.Point(0, 42)
Me.btn_cancel.Name = "btn_cancel"
Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
Me.btn_cancel.TabIndex = 4
Me.btn_cancel.ToolTipped = False
Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'uc_grid_terminals
'
Me.uc_grid_terminals.CurrentCol = -1
Me.uc_grid_terminals.CurrentRow = -1
Me.uc_grid_terminals.Location = New System.Drawing.Point(9, 17)
Me.uc_grid_terminals.Name = "uc_grid_terminals"
Me.uc_grid_terminals.PanelRightVisible = True
Me.uc_grid_terminals.Redraw = True
Me.uc_grid_terminals.Size = New System.Drawing.Size(488, 193)
Me.uc_grid_terminals.Sortable = False
Me.uc_grid_terminals.SortAscending = True
Me.uc_grid_terminals.SortByCol = 0
Me.uc_grid_terminals.TabIndex = 11
Me.uc_grid_terminals.ToolTipped = True
Me.uc_grid_terminals.TopRow = -2
'
'dtp_retirement_date
'
Me.dtp_retirement_date.Checked = True
Me.dtp_retirement_date.IsReadOnly = False
Me.dtp_retirement_date.Location = New System.Drawing.Point(200, 216)
Me.dtp_retirement_date.Name = "dtp_retirement_date"
Me.dtp_retirement_date.ShowCheckBox = True
Me.dtp_retirement_date.ShowUpDown = False
Me.dtp_retirement_date.Size = New System.Drawing.Size(232, 24)
Me.dtp_retirement_date.SufixText = "Sufix Text"
Me.dtp_retirement_date.SufixTextVisible = True
Me.dtp_retirement_date.TabIndex = 12
Me.dtp_retirement_date.TextWidth = 120
Me.dtp_retirement_date.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
'
'frm_terminals_retirement
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(602, 261)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.Name = "frm_terminals_retirement"
Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
Me.Text = "frm_terminals_edit_status"
Me.panel_data.ResumeLayout(False)
Me.ResumeLayout(False)

End Sub
  Private WithEvents btn_ok As GUI_Controls.uc_button
  Private WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents uc_grid_terminals As GUI_Controls.uc_grid
  Friend WithEvents dtp_retirement_date As GUI_Controls.uc_date_picker
End Class
