﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_flyers_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_representatives = New System.Windows.Forms.GroupBox()
    Me.opt_all_representatives = New System.Windows.Forms.RadioButton()
    Me.opt_several_representatives = New System.Windows.Forms.RadioButton()
    Me.dg_filter_representatives = New GUI_Controls.uc_grid()
    Me.gb_junkets = New System.Windows.Forms.GroupBox()
    Me.opt_all_junkets = New System.Windows.Forms.RadioButton()
    Me.opt_several_junkets = New System.Windows.Forms.RadioButton()
    Me.dg_filter_junkets = New GUI_Controls.uc_grid()
    Me.ef_flyer_code = New GUI_Controls.uc_entry_field()
    Me.gb_flyer = New System.Windows.Forms.GroupBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_representatives.SuspendLayout()
    Me.gb_junkets.SuspendLayout()
    Me.gb_flyer.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_flyer)
    Me.panel_filter.Controls.Add(Me.gb_junkets)
    Me.panel_filter.Controls.Add(Me.gb_representatives)
    Me.panel_filter.Size = New System.Drawing.Size(1138, 183)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_representatives, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_junkets, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_flyer, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 187)
    Me.panel_data.Size = New System.Drawing.Size(1138, 417)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1132, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1132, 4)
    '
    'gb_representatives
    '
    Me.gb_representatives.Controls.Add(Me.opt_all_representatives)
    Me.gb_representatives.Controls.Add(Me.opt_several_representatives)
    Me.gb_representatives.Controls.Add(Me.dg_filter_representatives)
    Me.gb_representatives.Location = New System.Drawing.Point(6, 6)
    Me.gb_representatives.Name = "gb_representatives"
    Me.gb_representatives.Size = New System.Drawing.Size(341, 170)
    Me.gb_representatives.TabIndex = 0
    Me.gb_representatives.TabStop = False
    Me.gb_representatives.Text = "xRepresentatives"
    '
    'opt_all_representatives
    '
    Me.opt_all_representatives.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_representatives.Name = "opt_all_representatives"
    Me.opt_all_representatives.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_representatives.TabIndex = 1
    Me.opt_all_representatives.Text = "xAll"
    '
    'opt_several_representatives
    '
    Me.opt_several_representatives.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_representatives.Name = "opt_several_representatives"
    Me.opt_several_representatives.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_representatives.TabIndex = 0
    Me.opt_several_representatives.Text = "xSeveral"
    '
    'dg_filter_representatives
    '
    Me.dg_filter_representatives.CurrentCol = -1
    Me.dg_filter_representatives.CurrentRow = -1
    Me.dg_filter_representatives.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter_representatives.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter_representatives.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter_representatives.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter_representatives.Name = "dg_filter_representatives"
    Me.dg_filter_representatives.PanelRightVisible = False
    Me.dg_filter_representatives.Redraw = True
    Me.dg_filter_representatives.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter_representatives.Size = New System.Drawing.Size(249, 148)
    Me.dg_filter_representatives.Sortable = False
    Me.dg_filter_representatives.SortAscending = True
    Me.dg_filter_representatives.SortByCol = 0
    Me.dg_filter_representatives.TabIndex = 2
    Me.dg_filter_representatives.ToolTipped = True
    Me.dg_filter_representatives.TopRow = -2
    Me.dg_filter_representatives.WordWrap = False
    '
    'gb_junkets
    '
    Me.gb_junkets.Controls.Add(Me.opt_all_junkets)
    Me.gb_junkets.Controls.Add(Me.opt_several_junkets)
    Me.gb_junkets.Controls.Add(Me.dg_filter_junkets)
    Me.gb_junkets.Location = New System.Drawing.Point(353, 6)
    Me.gb_junkets.Name = "gb_junkets"
    Me.gb_junkets.Size = New System.Drawing.Size(341, 170)
    Me.gb_junkets.TabIndex = 1
    Me.gb_junkets.TabStop = False
    Me.gb_junkets.Text = "xJunkets"
    '
    'opt_all_junkets
    '
    Me.opt_all_junkets.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_junkets.Name = "opt_all_junkets"
    Me.opt_all_junkets.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_junkets.TabIndex = 1
    Me.opt_all_junkets.Text = "xAll"
    '
    'opt_several_junkets
    '
    Me.opt_several_junkets.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_junkets.Name = "opt_several_junkets"
    Me.opt_several_junkets.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_junkets.TabIndex = 0
    Me.opt_several_junkets.Text = "xSeveral"
    '
    'dg_filter_junkets
    '
    Me.dg_filter_junkets.CurrentCol = -1
    Me.dg_filter_junkets.CurrentRow = -1
    Me.dg_filter_junkets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter_junkets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter_junkets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter_junkets.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter_junkets.Name = "dg_filter_junkets"
    Me.dg_filter_junkets.PanelRightVisible = False
    Me.dg_filter_junkets.Redraw = True
    Me.dg_filter_junkets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter_junkets.Size = New System.Drawing.Size(249, 148)
    Me.dg_filter_junkets.Sortable = False
    Me.dg_filter_junkets.SortAscending = True
    Me.dg_filter_junkets.SortByCol = 0
    Me.dg_filter_junkets.TabIndex = 2
    Me.dg_filter_junkets.ToolTipped = True
    Me.dg_filter_junkets.TopRow = -2
    Me.dg_filter_junkets.WordWrap = False
    '
    'ef_flyer_code
    '
    Me.ef_flyer_code.DoubleValue = 0.0R
    Me.ef_flyer_code.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_flyer_code.IntegerValue = 0
    Me.ef_flyer_code.IsReadOnly = False
    Me.ef_flyer_code.Location = New System.Drawing.Point(15, 14)
    Me.ef_flyer_code.Name = "ef_flyer_code"
    Me.ef_flyer_code.PlaceHolder = Nothing
    Me.ef_flyer_code.Size = New System.Drawing.Size(159, 25)
    Me.ef_flyer_code.SufixText = "Sufix Text"
    Me.ef_flyer_code.SufixTextVisible = True
    Me.ef_flyer_code.TabIndex = 0
    Me.ef_flyer_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_flyer_code.TextValue = ""
    Me.ef_flyer_code.TextWidth = 60
    Me.ef_flyer_code.Value = ""
    Me.ef_flyer_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_flyer
    '
    Me.gb_flyer.Controls.Add(Me.ef_flyer_code)
    Me.gb_flyer.Location = New System.Drawing.Point(700, 6)
    Me.gb_flyer.Name = "gb_flyer"
    Me.gb_flyer.Size = New System.Drawing.Size(182, 47)
    Me.gb_flyer.TabIndex = 2
    Me.gb_flyer.TabStop = False
    Me.gb_flyer.Text = "xFlyer"
    '
    'frm_junkets_flyers_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1146, 608)
    Me.Name = "frm_junkets_flyers_report"
    Me.Text = "frm_junkets_report_flyers"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_representatives.ResumeLayout(False)
    Me.gb_junkets.ResumeLayout(False)
    Me.gb_flyer.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_representatives As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_representatives As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_representatives As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter_representatives As GUI_Controls.uc_grid
  Friend WithEvents gb_junkets As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_junkets As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_junkets As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter_junkets As GUI_Controls.uc_grid
  Friend WithEvents gb_flyer As System.Windows.Forms.GroupBox
  Friend WithEvents ef_flyer_code As GUI_Controls.uc_entry_field
End Class
