'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_draw_edit
' DESCRIPTION:   New or Edit Draw
' AUTHOR:        Raul Cervera
' CREATION DATE: 09-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-JUN-2010  RCI    Initial version
' 02-MAY-2011  LCM    Special and Provider sections added.
' 20-OCT-2011  MPO    Limit per operation and first cash in constrained
' 21-DEC-2011  RCI    Added Bingo format flag
' 02-JAN-2012  JMM    Cash-in 'a posteriori' draw type added
' 03-JAN-2012  JMM    Check header, footer and details from ticket layout don't exceed 1024
' 19-JAN-2012  RCI    Provider filter now is a User Control
' 21-MAR-2012  JCM    User Control Reorganized, and Collapsed.
' 09-MAY-2012  JCM    Fixed Bug #113: Ask if draw end date is allowed to be before current date
' 23-MAY-2012  MPO    Added icon and image controls.
' 30-MAY-2012  RCI    Check header, footer and details from ticket layout don't exceed 10000 (MAX_TICKET_LAYOUT_FIELDS_LENGTH)
' 16-NOV-2012  RRB    Expanded bingo columns to 6.
' 26-FEB-2013  HBB    Fixed Bug #613: Exception when starting date greater than ending date.
' 01-MAR-2013  HBB    Fixed Bug #628: It is not allowed a begining date greater than the ending date.
' 04-APR-2013  QMP    Added check for New Offer unsaved data to IsScreenDataOk function.
' 11-APR-2013  JAR    If the raffle is closed, the ending time is set to now
' 16-APR-2013  JAR    The ending time is set to now unless it is a past date
' 23-APR-2013  QMP    Fixed Bug #715: Added specific error message when deleting a raffle with dependencies
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty 
' 24-MAY-2003  HBB    It is changed the holder account type to level filter
' 12-SEP-2013  ACM    Fixed Bug WIG-198: DrawType different than expected.
' 20-SEP-2013  DRV    Added points per level
' 07-FEB-2014  AMF    Fixed Bug WIG-516: In new draw, incorrect popup when exit without changes
' 03-MAR-2014  DLL    Fixed Bug WIG-688: In draw by points ignore providers
' 07-APR-2014  JBP    Added only VIP property.
' 20-MAY-2014  JMM    Fixed Bug WIG-931: Providers seletion dosen't show properly
' 11-JUL-2014  JMM    Weeks days sorted depending on system's first day of week
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 18-JUL-2017  DHA    Bug 28811:WIGOS-3702 Draws - the option "Minimo agotado" is autochecked when the user change the "Tipo de sorteo"
' 22-FEB-2018  EOR    Bug 31700: WIGOS-6823 [Ticket #10669] CONFIGURACI�N DE TICKET DE SORTEO
' 09-JUL-2018  FOS    Bug 31700: WIGOS-6823 [Ticket #10669] CONFIGURACI�N DE TICKET DE SORTEO
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_draw_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_DRAW_NAME As Integer = 50
  Private Const LEN_DRAW_PROMOBOX_TEXT_NAME As Integer = 50

  Private Const MAX_AMOUNT_DIGITS As Integer = 16
  Private Const MAX_NUMBER_DIGITS As Integer = 8
  Private Const MAX_LIMIT_PER_VOUCHER_DIGITS As Integer = 3
  Private Const MAX_LIMIT_PER_OPERATION_DIGITS As Integer = 3
  Private Const MAX_OFFER_LIMIT_DIGITS As Integer = 1

  ' Database codes
  Private Const DR_STATUS_OPENED As Integer = 0
  Private Const DR_STATUS_CLOSED As Integer = 1
  Private Const DR_STATUS_DISABLED As Integer = 2

  ' Grid columns
  Private Const DRAW_OFFER_GRID_COLUMNS As Integer = 2
  Private Const DRAW_OFFER_GRID_HEADER_ROWS As Integer = 0

  Private Const DRAW_OFFER_GRID_COLUMN_KEY = 0
  Private Const DRAW_OFFER_GRID_COLUMN_DESCRIPTION = 1

  Private Const BINGO_NUM_COLUMNS = 6

  Private Const MAX_TICKET_LAYOUT_FIELDS_LENGTH = 10000

#End Region

#Region " Members "

  Private m_input_draw_name As String
  Private DeleteOperation As Boolean
  Private m_changed_values As Boolean
  Private m_default_button As IButtonControl
  Private m_first_time As Boolean = True
  Private m_load_complete As Boolean = False

#End Region ' Members

#Region "Properties"

  Private Property DrawTypeSelection() As String
    Get
      If Me.cmb_draw_type.SelectedIndex < 0 Then
        Return -1
      End If

      Select Case Me.cmb_draw_type.Value

        Case 0
          Me.ef_number_price.Enabled = True
          Me.ef_number_price.Visible = True
          Me.ef_min_played.Enabled = False
          Me.ef_min_played.Value = 0
          Me.ef_min_played.TextValue = ""
          Me.chk_min_played.Checked = False
          Me.ef_min_spent.Enabled = False
          Me.ef_min_spent.Value = 0
          Me.ef_min_spent.TextValue = ""
          Me.chk_min_spent.Checked = False
          Me.ef_min_cash_in.Enabled = False
          Me.ef_min_cash_in.Value = 0
          Me.ef_min_cash_in.TextValue = ""

          Return WSI.Common.DrawType.BY_PLAYED_CREDIT
        Case 1
          Me.ef_number_price.Enabled = True
          Me.ef_number_price.Visible = True
          Me.ef_min_played.Enabled = False
          Me.ef_min_played.Value = 0
          Me.ef_min_played.TextValue = ""
          Me.chk_min_played.Checked = False
          Me.ef_min_spent.Enabled = False
          Me.ef_min_spent.Value = 0
          Me.ef_min_spent.TextValue = ""
          Me.chk_min_spent.Checked = False
          Me.ef_min_cash_in.Enabled = False
          Me.ef_min_cash_in.Value = 0
          Me.ef_min_cash_in.TextValue = ""

          Return WSI.Common.DrawType.BY_REDEEMABLE_SPENT
        Case 2
          Me.ef_number_price.Enabled = True
          Me.ef_number_price.Visible = True
          Me.ef_min_played.Enabled = False
          Me.ef_min_played.Value = 0
          Me.ef_min_played.TextValue = ""
          Me.chk_min_played.Checked = False
          Me.ef_min_spent.Enabled = False
          Me.ef_min_spent.Value = 0
          Me.ef_min_spent.TextValue = ""
          Me.chk_min_spent.Checked = False
          Me.ef_min_cash_in.Enabled = False
          Me.ef_min_cash_in.Value = 0
          Me.ef_min_cash_in.TextValue = ""

          Return WSI.Common.DrawType.BY_TOTAL_SPENT
        Case 3
          Me.ef_number_price.Value = 0
          Me.ef_number_price.TextValue = ""
          Me.ef_number_price.Enabled = False
          Me.ef_number_price.Visible = False
          Me.ef_min_played.Enabled = False
          Me.ef_min_played.Value = 0
          Me.ef_min_played.TextValue = ""
          Me.chk_min_played.Checked = False
          Me.ef_min_spent.Enabled = False
          Me.ef_min_spent.Value = 0
          Me.ef_min_spent.TextValue = ""
          Me.chk_min_spent.Checked = False
          Me.ef_min_cash_in.Enabled = False
          Me.ef_min_cash_in.Value = 0
          Me.ef_min_cash_in.TextValue = ""

          Return WSI.Common.DrawType.BY_POINTS
        Case 4
          Me.ef_number_price.Enabled = True
          Me.ef_number_price.Visible = True
          Me.ef_min_played.Enabled = chk_min_played.Checked
          Me.ef_min_spent.Enabled = chk_min_spent.Checked
          Me.ef_min_cash_in.Enabled = True

          Return WSI.Common.DrawType.BY_CASH_IN
        Case 5
          Me.ef_number_price.Enabled = True
          Me.ef_number_price.Visible = True
          Me.chk_min_spent.Checked = True
          Me.ef_min_played.Enabled = chk_min_played.Checked
          Me.ef_min_spent.Enabled = Me.chk_min_spent.Checked
          Me.ef_min_cash_in.Enabled = True

          Return WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
        Case Else
          Return -1

      End Select
    End Get

    Set(ByVal value As String)
      Select Case value
        Case WSI.Common.DrawType.BY_PLAYED_CREDIT
          Me.cmb_draw_type.Value = 0
        Case WSI.Common.DrawType.BY_REDEEMABLE_SPENT
          Me.cmb_draw_type.Value = 1
        Case WSI.Common.DrawType.BY_TOTAL_SPENT
          Me.cmb_draw_type.Value = 2
        Case WSI.Common.DrawType.BY_POINTS
          Me.cmb_draw_type.Value = 3
        Case WSI.Common.DrawType.BY_CASH_IN
          Me.cmb_draw_type.Value = 4
        Case WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
          Me.cmb_draw_type.Value = 5
        Case Else
          Me.cmb_draw_type.SelectedIndex = -1
          Me.cmb_draw_type.ResetLastIndex()
      End Select
    End Set

  End Property

#End Region ' Properties

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DRAW_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox
    Dim empty_placeholder As String
    empty_placeholder = "---"

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(336)

      ' Hide winner tab during draw creation
      tab_Extra.TabPages.RemoveByKey("tb_winner")

      'lbl_total_numbers_count.Visible = False
      ef_last_number.Visible = False
      lbl_last_number.Visible = False
      'Me.ClientSize = New System.Drawing.Size(Me.ClientSize.Width, Me.ClientSize.Height - Me.gb_winner.Height)
      m_changed_values = False
    Else
      ef_winning_number.TextVisible = True
      ef_winner_name.TextVisible = True
      ef_voucher.TextVisible = True
      ef_account.TextVisible = True
      ef_draw_ticket.TextVisible = True

      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(320)
      m_changed_values = True
    End If

    'Add any initialization after the MyBase.GUI_InitControls() call

    lbl_price_ratio.Text = ""
    lbl_info_points_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2647)
    lbl_total_numbers_count.Text = ""
    lbl_not_apply.Value = ""
    lbl_cash_in_conditions_1.Value = ""
    lbl_cash_in_conditions_2.Value = ""

    dtp_start.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(311)
    dtp_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(312)

    lbl_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)

    ef_draw_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)

    ef_number_price.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(318)
    ef_number_price.SufixText = ""

    gb_points_by_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2648)

    chk_bingo_format.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(560)

    lbl_price_ratio.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(504, empty_placeholder)

    tab_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(213)
    chk_by_gender_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_gender_male_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)
    opt_gender_female_draw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)
    chk_by_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(407)

    chk_level_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)

    Me.chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4804)   ' 4804 "Exclusivo para clientes VIP"

    InitCheckBoxTitles(chk_level_01, chk_points_level1, GetCashierPlayerTrackingData("Level01.Name"), 289)
    InitCheckBoxTitles(chk_level_02, chk_points_level2, GetCashierPlayerTrackingData("Level02.Name"), 290)
    InitCheckBoxTitles(chk_level_03, chk_points_level3, GetCashierPlayerTrackingData("Level03.Name"), 291)
    InitCheckBoxTitles(chk_level_04, chk_points_level4, GetCashierPlayerTrackingData("Level04.Name"), 332)

    ef_points_level1.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502)
    ef_points_level2.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502)
    ef_points_level3.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502)
    ef_points_level4.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502)

    tab_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(404)
    tab_cashin_conditions.Text = GLB_NLS_GUI_CONFIGURATION.GetString(338)
    tab_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(722) ' 722 "PromoBOX"
    ef_min_played.Text = GLB_NLS_GUI_CONFIGURATION.GetString(339)
    ef_min_spent.Text = GLB_NLS_GUI_CONFIGURATION.GetString(340)
    ef_min_cash_in.Text = GLB_NLS_GUI_CONFIGURATION.GetString(374)
    lbl_first_cash_in_constrained_1.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(533)
    lbl_first_cash_in_constrained_2.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(534)

    lbl_last_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(317)
    ef_last_number.IsReadOnly = True

    Me.gb_draws_numeration.Text = GLB_NLS_GUI_CONFIGURATION.GetString(316)
    Me.uc_entry_draw_numbers_initial.Text = GLB_NLS_GUI_CONFIGURATION.GetString(300)  ' "Primer n�mero"
    Me.uc_entry_draw_numbers_max.Text = GLB_NLS_GUI_CONFIGURATION.GetString(301) ' "�ltimo n�mero"

    lbl_total_numbers_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(467, empty_placeholder)

    ' Set combo with Draw Status
    cmb_status.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(314))
    cmb_status.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(315))
    cmb_status.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(316))

    gb_max_number_day_player.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(332)

    ef_limit_per_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(337)
    ef_limit_per_operation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(535)

    ef_limit.Enabled = False
    ef_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(511)

    'gb_winner.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(321)
    ef_winning_number.Text = GLB_NLS_GUI_CONTROLS.GetString(320)
    ef_winner_name.Text = GLB_NLS_GUI_CONTROLS.GetString(323)
    ef_account.Text = GLB_NLS_GUI_STATISTICS.GetString(207)
    ef_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(323)
    ef_draw_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2920)

    btn_check_winner.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(322)
    btn_voucher.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(485)

    Me.cmb_draw_type.Text = GLB_NLS_GUI_CONFIGURATION.GetString(299) ' N�meros de sorteo

    Call DrawTypeComboFill()

    Me.TabPage1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(372) ' "Cabecera"
    Me.TabPage2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(373) ' "Pie"
    Me.TabPage3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(392) ' "T�tulo"
    Me.TabPage4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(393) ' "Izquierda"
    Me.TabPage5.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(394) ' "Derecha"
    Me.TabPage6.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(486) ' "Vista previa" 

    ' RCI 30-MAY-2012: Set MaxLenght = MAX_TICKET_LAYOUT_FIELDS_LENGTH for header, footer and details from ticket layout
    ef_header.MaxLength = MAX_TICKET_LAYOUT_FIELDS_LENGTH
    ef_footer.MaxLength = MAX_TICKET_LAYOUT_FIELDS_LENGTH
    ef_detail1.MaxLength = MAX_TICKET_LAYOUT_FIELDS_LENGTH
    ef_detail2.MaxLength = MAX_TICKET_LAYOUT_FIELDS_LENGTH
    ef_detail3.MaxLength = MAX_TICKET_LAYOUT_FIELDS_LENGTH

    ' filters
    dtp_start.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_end.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ef_draw_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_DRAW_NAME)
    ef_number_price.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

    ef_points_level1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    ef_points_level2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    ef_points_level3.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    ef_points_level4.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)

    ef_min_played.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7, 2)
    ef_min_played.TextVisible = True
    ef_min_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
    ef_min_spent.TextVisible = True
    ef_min_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)
    ef_min_cash_in.TextVisible = True
    ef_winning_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_NUMBER_DIGITS)
    ef_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_NUMBER_DIGITS)
    ef_limit_per_voucher.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_LIMIT_PER_VOUCHER_DIGITS)
    ef_limit_per_operation.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_LIMIT_PER_OPERATION_DIGITS)

    uc_entry_draw_numbers_initial.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 0)
    uc_entry_draw_numbers_max.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 0)

    ef_offer_n.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_OFFER_LIMIT_DIGITS)
    ef_offer_m.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_OFFER_LIMIT_DIGITS)

    dtp_offer_day.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_offer_day.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    ' Detail
    gb_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)

    'Offers
    gb_new_offer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(478)

    lbl_msg_order_offers.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(520)

    ef_offer_n.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471)
    ef_offer_m.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(472)
    ef_offer_n.TextVisible = True
    ef_offer_m.TextVisible = True

    opt_offer_weekday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(476)
    opt_offer_single_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(477)

    lblFrom.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
    lblTo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)

    dtp_time_from.ShowUpDown = True
    dtp_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HOURS)
    dtp_time_to.ShowUpDown = True
    dtp_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HOURS)

    chk_offer_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(325)
    chk_offer_tuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(326)
    chk_offer_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(327)
    chk_offer_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(328)
    chk_offer_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(329)
    chk_offer_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(330)
    chk_offer_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(331)

    _week_days_chk_array(0) = chk_offer_monday
    _week_days_chk_array(1) = chk_offer_tuesday
    _week_days_chk_array(2) = chk_offer_wednesday
    _week_days_chk_array(3) = chk_offer_thursday
    _week_days_chk_array(4) = chk_offer_friday
    _week_days_chk_array(5) = chk_offer_saturday
    _week_days_chk_array(6) = chk_offer_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    btn_offer_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    btn_offer_remove.Text = GLB_NLS_GUI_CONTROLS.GetString(11)
    btn_check_all_days.Text = GLB_NLS_GUI_CONTROLS.GetString(276) ' Todos
    btn_reset.Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    ' RCI 07-JUL-2011: Offers gender filter.
    chk_by_gender_offer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(519)
    opt_gender_male_offer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)
    opt_gender_female_offer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)

    ' MPO 23-MAY-2012: Images
    lbl_icon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(719)
    lbl_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(687)
    Call img_icon.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)
    Call img_image.Init(uc_image.MAXIMUN_RESOLUTION.x400X300)

    ' Tab EXTRA
    tab_Extra.TabPages("tb_offer").Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471) ' Offer
    tab_Extra.TabPages("tb_layout").Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(468) ' Layout
    If tab_Extra.TabPages.ContainsKey("tb_winner") Then
      tab_Extra.TabPages("tb_winner").Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(321) ' Winner
    End If

    Call EnableControlsByDrawType()
    Call EnableWeekdayOrSingleDay()

    Call EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, -1)
    Call EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, -1)
    Call EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, -1)
    Call EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, -1)

    Me.lbl_icon_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "200", "150")

    Me.lbl_image_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "400", "300")

    ' PromoBox
    Me.chk_visible_on_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2868) ' 2868 "Mostrar en monitor superior"
    Me.chk_award_on_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2869)   ' 2869 "Allow request"
    Me.ef_text_on_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2870)     ' 2870 "Display text"
    Me.ef_text_on_promobox.TextVisible = True
    Me.ef_text_on_promobox.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_DRAW_PROMOBOX_TEXT_NAME)

    GUI_DG_OffersStyleSheet()

    Timer1.Start()

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _nls_param1 As String
    Dim _nls_param2 As String
    Dim _limit_per_voucher As Double
    Dim _str_levels As String
    Dim _levels_count As Integer

    Dim _draw As CLASS_DRAW

    _draw = DbEditedObject

    ' blank field Draw Name
    If ef_draw_name.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_draw_name.Focus()

      Return False
    End If

    ' blank combo draw type
    If cmb_draw_type.SelectedIndex = -1 Then
      _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(299)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_draw_name.Focus()

      Return False
    End If

    ' blank field Number Price
    If ef_number_price.Value = "" And DrawTypeSelection <> WSI.Common.DrawType.BY_POINTS Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(318)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_number_price.Focus()

      Return False
    End If

    ' Number Price > 0
    If GUI_ParseNumber(ef_number_price.Value) = 0 And DrawTypeSelection <> WSI.Common.DrawType.BY_POINTS Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(318)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_number_price.Focus()

      Return False
    End If

    'Number Price < 999.999.999.999.999
    If GUI_ParseNumber(ef_number_price.Value) > 999999999999999 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1421), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(318), "999.999.999.999.999")
      Call ef_number_price.Focus()

      Return False
    End If
    'Points Price for one level checked at least 
    If (DrawTypeSelection = WSI.Common.DrawType.BY_POINTS And Not chk_points_level1.Checked And Not chk_points_level2.Checked And Not chk_points_level3.Checked And Not chk_points_level4.Checked) Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(489)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_points_level1.Focus()

      Return False
    End If

    'Points Price > 0 if chk_points_levelN checked
    If GUI_ParseNumber(ef_points_level1.Value) = 0 And chk_points_level1.Checked = True Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2649, GetCashierPlayerTrackingData("Level01.Name"))
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_points_level1.Focus()

      Return False
    End If

    If GUI_ParseNumber(ef_points_level2.Value) = 0 And chk_points_level2.Checked = True Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2649, GetCashierPlayerTrackingData("Level02.Name"))
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_points_level2.Focus()

      Return False
    End If

    If GUI_ParseNumber(ef_points_level3.Value) = 0 And chk_points_level3.Checked = True Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2649, GetCashierPlayerTrackingData("Level03.Name"))
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_points_level3.Focus()

      Return False
    End If

    If GUI_ParseNumber(ef_points_level4.Value) = 0 And chk_points_level4.Checked = True Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2649, GetCashierPlayerTrackingData("Level04.Name"))
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_points_level4.Focus()

      Return False
    End If

    'DRV 30-SEP-2013: Check if points by level and level filter match.
    _str_levels = ""
    _levels_count = 0
    If chk_by_level.Checked Then
      If chk_points_level1.Checked And Not chk_level_01.Checked Then
        _str_levels = GetCashierPlayerTrackingData("Level01.Name")
        _levels_count = 1
      End If

      If chk_points_level2.Checked And Not chk_level_02.Checked Then
        If _levels_count > 0 Then
          _str_levels = _str_levels & ", "
        End If
        _str_levels = _str_levels & GetCashierPlayerTrackingData("Level02.Name")
        _levels_count = _levels_count + 1
      End If

      If chk_points_level3.Checked And Not chk_level_03.Checked Then
        If _levels_count > 0 Then
          _str_levels = _str_levels & ", "
        End If
        _str_levels = _str_levels & GetCashierPlayerTrackingData("Level03.Name")
        _levels_count = _levels_count + 1
      End If

      If chk_points_level4.Checked And Not chk_level_04.Checked Then
        If _levels_count > 0 Then
          _str_levels = _str_levels & ", "
        End If
        _str_levels = _str_levels & GetCashierPlayerTrackingData("Level04.Name")
        _levels_count = _levels_count + 1
      End If

      Select Case _levels_count
        Case 1
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2671), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _str_levels) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Call Me.gb_points_by_level.Focus()

            Return False
          End If

        Case 2, 3, 4
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2672), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _str_levels) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Call Me.gb_points_by_level.Focus()

            Return False
          End If
      End Select
    End If

    ' blank field Minimum Cash-In
    If ef_min_cash_in.Value = "" _
      And (DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN _
      Or DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI) Then

      _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(374)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_min_cash_in.Focus()

      Return False
    End If

    ' RCI 05-JAN-2012: If draw type = CASH_IN_POSTERIORI And Minimum Cash-In >= Price
    If DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI _
       And GUI_ParseNumber(ef_min_cash_in.Value) < GUI_ParseNumber(ef_number_price.Value) Then

      _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(374)
      _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(318)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call ef_min_cash_in.Focus()

      Return False
    End If

    ' Field Minimum Played
    If ef_min_played.Enabled Then
      ' blank field Minimum Played
      If ef_min_played.Value = "" _
        And (DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN _
        Or DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI) Then

        _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(339)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call ef_min_played.Focus()

        Return False

      End If

      ' Minimum Played > 0
      If GUI_ParseNumber(ef_min_played.Value) <= 0 _
        And (DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN _
        Or DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI) Then
        _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(339)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call ef_min_played.Focus()

        Return False
      End If

      ' Minimum Played <= 1000
      If GUI_ParseNumber(ef_min_played.Value) > 1000 Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Call ef_min_played.Focus()

        Return False
      End If
    End If

    ' Field Minimum Spent
    If ef_min_spent.Enabled Then
      ' blank field Minimum Spent
      If ef_min_spent.Value = "" _
        And (DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN _
        Or DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI) Then
        _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(340)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call ef_min_spent.Focus()

        Return False

      End If

      ' Minimum Spent > 0
      If GUI_ParseNumber(ef_min_spent.Value) <= 0 _
        And (DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN _
        Or DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI) Then
        _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(340)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call ef_min_spent.Focus()

        Return False
      End If

      ' Minimum Spent <= 100
      If GUI_ParseNumber(ef_min_spent.Value) > 100 Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(194), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Call ef_min_spent.Focus()

        Return False
      End If
    End If

    ' blank field initial
    If uc_entry_draw_numbers_initial.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(300)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call uc_entry_draw_numbers_initial.Focus()

      Return False
    End If

    ' blank field max
    If uc_entry_draw_numbers_max.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(301)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call uc_entry_draw_numbers_max.Focus()

      Return False
    End If

    ' Max number < last number
    If GUI_ParseNumber(uc_entry_draw_numbers_max.Value) < _draw.LastNumber Then
      _nls_param1 = uc_entry_draw_numbers_max.Text
      _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(317)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call uc_entry_draw_numbers_max.Focus()

      Return False
    End If

    ' Max number < Initial number
    If GUI_ParseNumber(uc_entry_draw_numbers_max.Value) < GUI_ParseNumber(uc_entry_draw_numbers_initial.Value) Then
      _nls_param1 = uc_entry_draw_numbers_max.Text
      _nls_param2 = uc_entry_draw_numbers_initial.Text
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call uc_entry_draw_numbers_max.Focus()

      Return False
    End If

    ' RCI 21-DEC-2011: Bingo Format
    If chk_bingo_format.Checked Then
      If uc_entry_draw_numbers_max.Value.Length > BINGO_NUM_COLUMNS Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(135), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call uc_entry_draw_numbers_max.Focus()

        Return False
      End If
    End If

    ' If field Limited in use
    If chk_limit_to.Checked Then

      ' blank field Limit
      If ef_limit.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(334)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_limit.Text)
        Call ef_limit.Focus()

        Return False
      End If

    End If

    ' blank field Limit Per Voucher
    If ef_limit_per_voucher.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(337)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_limit_per_voucher.Focus()

      Return False
    End If

    _limit_per_voucher = GUI_ParseNumber(ef_limit_per_voucher.Value)

    ' Limit Per Voucher > 0
    If _limit_per_voucher = 0 Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(337)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_limit_per_voucher.Focus()

      Return False
    End If

    ' Limit Per Voucher < CLASS_DRAW.DEFAULT_MAX_NUMBERS_PER_VOUCHER
    If _limit_per_voucher > CLASS_DRAW.MAX_NUMBERS_PER_VOUCHER Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(337)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(338), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
                      _nls_param1, CLASS_DRAW.MAX_NUMBERS_PER_VOUCHER)
      Call ef_limit_per_voucher.Focus()

      Return False
    End If

    ' Limit Per Voucher <= [Max per day and player]
    If chk_limit_to.Checked = True Then

      If GUI_ParseNumber(ef_limit_per_voucher.Value) > GUI_ParseNumber(ef_limit.Value) Then
        _nls_param1 = ef_limit_per_voucher.Text
        _nls_param2 = ef_limit.Text

        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(338), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
        Call ef_limit.Focus()
        Return False
      End If

    End If

    ' Start Date must be less than End Date
    If Me.dtp_start.Value >= Me.dtp_end.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_start.Focus()

      Return False
    End If

    ' end_draw_date can't be smaller than current time
    If WSI.Common.WGDB.Now >= Me.dtp_end.Value Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(691), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Call Me.dtp_end.Focus()

        Return False
      End If
    End If

    ' Check gender filter
    If Me.chk_by_gender_draw.Checked Then
      If Not Me.opt_gender_male_draw.Checked And Not Me.opt_gender_female_draw.Checked Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 0
        Call opt_gender_male_draw.Focus()

        Return False
      ElseIf Me.opt_gender_male_draw.Checked Then
        If _draw.OfferList.ExistGenderOffers(WSI.Common.DrawGenderFilter.WOMEN_ONLY) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(133), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If
      ElseIf Me.opt_gender_female_draw.Checked Then
        If _draw.OfferList.ExistGenderOffers(WSI.Common.DrawGenderFilter.MEN_ONLY) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(134), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If
      End If
    End If

    ' Check Level filter
    If chk_by_level.Checked Then
      If ((chk_level_anonymous.Checked = False Or cmb_draw_type.Value = WSI.Common.DrawType.BY_POINTS) _
         And chk_level_01.Checked = False _
         And chk_level_02.Checked = False _
         And chk_level_03.Checked = False _
         And chk_level_04.Checked = False) Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 1
        Call chk_level_anonymous.Focus()

        Return False
      End If
    End If

    ' Providers (if listed or not listed must have at least one selected)
    If Not uc_provider_filter1.CheckAtLeastOneSelected() Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(566)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call uc_provider_filter1.Focus()

      Return False
    End If

    ' Offers will be deleted if "By Points"
    If cmb_draw_type.Value = WSI.Common.DrawType.BY_POINTS And dg_offers.NumRows > 0 Then
      ' Are you sure you want to remove all offers?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(503), ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                    ef_draw_name.Value) <> ENUM_MB_RESULT.MB_RESULT_NO Then
        'Remove all offers and continue
        RemoveOffers(True)
      Else
        Return False
      End If
    End If

    ' Status CLOSED... warning!
    If Me.cmb_status.Value = DR_STATUS_CLOSED And Not Me.cmb_status.IsReadOnly Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(111), ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                    ef_draw_name.Value) <> ENUM_MB_RESULT.MB_RESULT_YES Then
        Return False
      End If
    End If

    If DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN _
      Or DrawTypeSelection = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI Then
      If Not (chk_min_played.Checked Or chk_min_spent.Checked) Then
        ' 550 A value bigger than 0 must be set for played or spent
        If WSI.Common.Misc.IsRaffleBlankMinFieldsAllowed And cmb_draw_type.Value = WSI.Common.DrawType.BY_CASH_IN Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then
            Return False
          End If
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(550), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        End If
      End If
    End If

    ' RCI 30-MAY-2012: Check header, footer and details from ticket layout don't exceed MAX_TICKET_LAYOUT_FIELDS_LENGTH
    If ef_header.Text.Length > MAX_TICKET_LAYOUT_FIELDS_LENGTH Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(372) ' "Cabecera"
      _nls_param2 = MAX_TICKET_LAYOUT_FIELDS_LENGTH

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call ef_header.Focus()

      Return False
    End If

    If ef_footer.Text.Length > MAX_TICKET_LAYOUT_FIELDS_LENGTH Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(373) ' "Pie"
      _nls_param2 = MAX_TICKET_LAYOUT_FIELDS_LENGTH

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call ef_footer.Focus()

      Return False
    End If

    If ef_detail1.Text.Length > MAX_TICKET_LAYOUT_FIELDS_LENGTH Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(392) ' "T�tulo"
      _nls_param2 = MAX_TICKET_LAYOUT_FIELDS_LENGTH

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call ef_detail1.Focus()

      Return False
    End If

    If ef_detail2.Text.Length > MAX_TICKET_LAYOUT_FIELDS_LENGTH Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(393) ' "Izquierda"
      _nls_param2 = MAX_TICKET_LAYOUT_FIELDS_LENGTH

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call ef_detail2.Focus()

      Return False
    End If

    If ef_detail3.Text.Length > MAX_TICKET_LAYOUT_FIELDS_LENGTH Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(394) ' "Derecha"
      _nls_param2 = MAX_TICKET_LAYOUT_FIELDS_LENGTH

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
      Call ef_detail3.Focus()

      Return False
    End If

    ' QMP 04-APR-2013: Check if there's New Offer unsaved data
    If ef_offer_n.Value <> "" Or _
       ef_offer_m.Value <> "" Or _
       chk_by_gender_offer.Checked Or _
       opt_offer_weekday.Checked Or _
       opt_offer_single_day.Checked Or _
       dtp_time_from.Value.Hour <> 0 Or _
       dtp_time_to.Value.Hour <> 0 Then

      ' You have entered data for a new offer, but you have not added it to the list. If you continue, this new offer will not be saved.
      ' Do you want to continue?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1880), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

        Return False
      End If

    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _draw As CLASS_DRAW

    _draw = DbEditedObject

    _draw.Name = ef_draw_name.Value
    _draw.StartingDate = dtp_start.Value
    _draw.EndingDate = dtp_end.Value

    If ef_last_number.Value.Equals("---") Then
      _draw.LastNumber = -1
    ElseIf ef_last_number.Value = "" Then
      _draw.LastNumber = -1
    Else
      _draw.LastNumber = GUI_ParseNumber(ef_last_number.Value)
    End If

    If ef_number_price.Value <> "" Then
      _draw.NumberPrice = GUI_ParseCurrency(ef_number_price.Value)
    Else
      _draw.NumberPrice = GUI_ParseCurrency(0)
    End If

    If ef_points_level1.Value <> "" And chk_points_level1.Checked Then
      _draw.PointsLevel1 = GUI_ParseCurrency(ef_points_level1.Value)
    Else
      _draw.PointsLevel1 = -1
    End If

    If ef_points_level2.Value <> "" And chk_points_level2.Checked Then
      _draw.PointsLevel2 = GUI_ParseCurrency(ef_points_level2.Value)
    Else
      _draw.PointsLevel2 = -1
    End If

    If ef_points_level3.Value <> "" And chk_points_level3.Checked Then
      _draw.PointsLevel3 = GUI_ParseCurrency(ef_points_level3.Value)
    Else
      _draw.PointsLevel3 = -1
    End If

    If ef_points_level4.Value <> "" And chk_points_level4.Checked Then
      _draw.PointsLevel4 = GUI_ParseCurrency(ef_points_level4.Value)
    Else
      _draw.PointsLevel4 = -1
    End If

    _draw.MinCashIn = GUI_ParseCurrency(ef_min_cash_in.Value)
    If chk_min_played.Checked Then
      _draw.MinPlayedPct = GUI_ParseCurrency(ef_min_played.Value)
    Else
      _draw.MinPlayedPct = 0
    End If
    If chk_min_spent.Checked Then
      _draw.MinSpentPct = GUI_ParseCurrency(ef_min_spent.Value)
    Else
      _draw.MinSpentPct = 0
    End If

    Select Case cmb_draw_type.Value
      Case 0
        'WSI.Common.DrawType.BY_PLAYED_CREDIT
      Case 1
        'WSI.Common.DrawType.BY_REDEEMABLE_SPENT
      Case 2
        'WSI.Common.DrawType.BY_TOTAL_SPENT
      Case 3
        'WSI.Common.DrawType.BY_POINTS
      Case 4
        'WSI.Common.DrawType.BY_CASH_IN
        _draw.FirstCashInConstrained = Not Me.chk_first_cash_in_not_constrained.Checked
      Case 5
        'WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
      Case Else
    End Select

    _draw.Status = cmb_status.Value

    If _draw.Status = DR_STATUS_CLOSED AndAlso Not cmb_status.IsReadOnly Then
      Dim _date_round As Date
      Dim _date_now As Date

      _date_round = _draw.EndingDate
      _date_now = WSI.Common.WGDB.Now

      If _draw.EndingDate > _date_now Then
        If _draw.StartingDate > _date_now Then
          _date_round = _draw.StartingDate
        Else
          _date_round = _date_now
        End If

        ' We round date
        If _date_round.Minute <> 0 OrElse _date_round.Second <> 0 Then
          _date_round = New Date(_date_round.Year, _date_round.Month, _date_round.Day, _date_round.Hour, 0, 0)
          _date_round = _date_round.AddHours(1)
        End If

        _draw.EndingDate = _date_round

      End If
    End If

    _draw.Limited = chk_limit_to.Checked
    _draw.Limit = GUI_ParseNumber(ef_limit.Value)
    _draw.LimitPerVoucher = GUI_ParseNumber(ef_limit_per_voucher.Value)
    If Me.chk_limit_per_operation.Checked Then
      _draw.LimitPerOperation = GUI_ParseNumber(Me.ef_limit_per_operation.Value)
    Else
      _draw.LimitPerOperation = 0
    End If

    If uc_entry_draw_numbers_initial.Value <> "" Then
      _draw.InitialNumber = GUI_ParseNumber(uc_entry_draw_numbers_initial.Value)
    Else
      _draw.InitialNumber = -1
    End If

    If uc_entry_draw_numbers_max.Value <> "" Then
      _draw.MaxNumber = GUI_ParseNumber(uc_entry_draw_numbers_max.Value)
    Else
      _draw.MaxNumber = GUI_ParseNumber(-1)
    End If

    _draw.CreditType = DrawTypeSelection

    _draw.Header = ef_header.Text
    _draw.Footer = ef_footer.Text
    _draw.Detail1 = ef_detail1.Text
    _draw.Detail2 = ef_detail2.Text
    _draw.Detail3 = ef_detail3.Text

    _draw.HasBingoFormat = chk_bingo_format.Checked

    ' Get the checked providers from the control and copy them in the _draw.ProviderList.
    uc_provider_filter1.GetProvidersFromControl(_draw.ProviderList)

    _draw.GenderFilter = WSI.Common.DrawGenderFilter.ALL
    If chk_by_gender_draw.Checked Then
      If opt_gender_male_draw.Checked Then
        _draw.GenderFilter = WSI.Common.DrawGenderFilter.MEN_ONLY
      ElseIf opt_gender_female_draw.Checked Then
        _draw.GenderFilter = WSI.Common.DrawGenderFilter.WOMEN_ONLY
      End If
    End If

    _draw.LevelFilter = GetLevelFilter()

    _draw.Icon = img_icon.Image
    _draw.Image = img_image.Image

    ' JMM 27-MAR-2014
    _draw.VisibleOnPromoBOX = Me.chk_visible_on_promobox.Checked
    _draw.AwardOnPromoBOX = Me.chk_award_on_promobox.Checked
    _draw.TextOnPromoBOX = Me.ef_text_on_promobox.Value

    ' JBP 07-APR-2014
    _draw.IsVip = Me.chk_only_vip.Checked

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _draw As CLASS_DRAW
    Dim _provider_list As ProviderList

    m_load_complete = False

    _draw = DbReadObject

    DrawTypeSelection = _draw.CreditType

    _provider_list = _draw.ProviderList

    ef_draw_name.Value = _draw.Name
    dtp_start.Value = _draw.StartingDate
    dtp_end.Value = _draw.EndingDate

    If _draw.LastNumber = -1 Then
      ef_last_number.Value = "---"
    Else
      'ef_last_number.Value = GUI_FormatNumber(_draw.LastNumber, 0)
      ef_last_number.Value = _draw.LastNumber.ToString.PadLeft(_draw.MaxNumber.ToString.Length, "0"c)
      uc_entry_draw_numbers_initial.Enabled = False
      cmb_draw_type.Enabled = False
      chk_bingo_format.Enabled = False
    End If

    If _draw.NumberPrice > 0 Then
      ef_number_price.Value = _draw.NumberPrice
    Else
      ef_number_price.Value = ""
    End If

    EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
    EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
    EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
    EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

    ef_min_cash_in.Value = _draw.MinCashIn
    If _draw.MinPlayedPct = 0 Then
      ef_min_played.Enabled = False
      chk_min_played.Checked = False
    Else
      ef_min_played.Enabled = True
      ef_min_played.Value = _draw.MinPlayedPct
      chk_min_played.Checked = True
    End If
    If _draw.MinSpentPct = 0 Then
      ef_min_spent.Enabled = False
      chk_min_spent.Checked = False
    Else
      ef_min_spent.Enabled = True
      ef_min_spent.Value = _draw.MinSpentPct
      chk_min_spent.Checked = True
    End If

    If cmb_draw_type.SelectedIndex <> -1 Then
      Select Case cmb_draw_type.Value
        Case 0
          'WSI.Common.DrawType.BY_PLAYED_CREDIT
        Case 1
          'WSI.Common.DrawType.BY_REDEEMABLE_SPENT
        Case 2
          'WSI.Common.DrawType.BY_TOTAL_SPENT
        Case 3
          'WSI.Common.DrawType.BY_POINTS
          ' DLL 03-MAR-2014: set povider list to all providers because with this draw ignore providers
          _provider_list = New ProviderList()
          _provider_list.ListType = ProviderList.ProviderListType.All
        Case 4
          'WSI.Common.DrawType.BY_CASH_IN
          Me.chk_first_cash_in_not_constrained.Checked = Not _draw.FirstCashInConstrained
        Case 5
          'WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
          ' RCI 05-JAN-2012: Disable minimum played entries
          Me.chk_min_played.Enabled = False
          Me.ef_min_played.Enabled = False
          Me.ef_min_played.Value = ""
        Case Else
      End Select
    End If

    cmb_status.Value = _draw.Status
    If _draw.Status = DR_STATUS_CLOSED Then
      cmb_status.IsReadOnly = True

      ' If the raffle is closed, we disable the OK button so the user can't make changes
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    End If

    If _draw.Limited Then
      chk_limit_to.Checked = True
      ef_limit.Enabled = True
      ef_limit.Value = _draw.Limit
    Else
      chk_limit_to.Checked = False
      ef_limit.Enabled = False
      ef_limit.Value = ""
    End If

    If _draw.LimitPerOperation = 0 Then
      Me.chk_limit_per_operation.Checked = False
      Me.ef_limit_per_operation.Enabled = False
    Else
      Me.chk_limit_per_operation.Checked = True
      Me.ef_limit_per_operation.Value = _draw.LimitPerOperation
    End If

    ef_limit_per_voucher.Value = _draw.LimitPerVoucher

    If _draw.InitialNumber = -1 Then
      uc_entry_draw_numbers_initial.Value = ""
    Else
      uc_entry_draw_numbers_initial.Value = _draw.InitialNumber
    End If

    If _draw.MaxNumber = -1 Then
      uc_entry_draw_numbers_max.Value = ""
    Else
      uc_entry_draw_numbers_max.Value = _draw.MaxNumber
    End If

    Me.chk_bingo_format.Checked = _draw.HasBingoFormat

    ' Offer
    RefreshOffers(_draw)
    btn_offer_remove.Enabled = _draw.OfferList.Count > 0

    dtp_end.SetRange(dtp_start.Value, dtp_start.Value.AddYears(5))
    SetOfferDayRange()

    'Gender Offer Filter
    ResetOfferFields()
    opt_gender_male_offer.Enabled = False
    opt_gender_male_offer.Checked = False
    opt_gender_female_offer.Enabled = False
    opt_gender_female_offer.Checked = False

    ' Set the provider filter control with the data from the _draw.ProviderList.
    uc_provider_filter1.SetProviderList(_provider_list)

    If m_first_time And ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _draw.CreditType = 0 Then
      m_first_time = False
      Call cmb_draw_type_ValueChanged()
    End If

    ef_header.Text = _draw.Header
    ef_footer.Text = _draw.Footer
    ef_detail1.Text = _draw.Detail1
    ef_detail2.Text = _draw.Detail2
    ef_detail3.Text = _draw.Detail3

    ' Level Filter
    SetLevelFilter(_draw.LevelFilter)

    ' Gender Filter
    Select Case _draw.GenderFilter
      Case WSI.Common.DrawGenderFilter.ALL
        chk_by_gender_draw.Checked = False
        opt_gender_male_draw.Enabled = False
        opt_gender_male_draw.Checked = False
        opt_gender_female_draw.Enabled = False
        opt_gender_female_draw.Checked = False

      Case WSI.Common.DrawGenderFilter.MEN_ONLY
        chk_by_gender_draw.Checked = True
        opt_gender_male_draw.Checked = True
        opt_gender_female_draw.Checked = False

      Case WSI.Common.DrawGenderFilter.WOMEN_ONLY
        chk_by_gender_draw.Checked = True
        opt_gender_male_draw.Checked = False
        opt_gender_female_draw.Checked = True

    End Select

    chk_visible_on_promobox.Checked = _draw.VisibleOnPromoBOX
    chk_award_on_promobox.Checked = _draw.AwardOnPromoBOX
    ef_text_on_promobox.Value = _draw.TextOnPromoBOX

    ' JBP 07-APR-2014
    Me.chk_only_vip.Checked = _draw.IsVip

    img_icon.Image = _draw.Icon
    img_image.Image = _draw.Image

    ShowVoucherPreview()

    m_load_complete = True

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim draw_class As CLASS_DRAW
    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_DRAW
        draw_class = DbEditedObject
        draw_class.DrawId = 0
        draw_class.Status = DR_STATUS_OPENED

        Dim startingDate As New DateTime()
        startingDate = New DateTime(Now.Year, Now.Month, 1, 0, 0, 0)

        draw_class.StartingDate = startingDate.AddMonths(1)
        draw_class.EndingDate = startingDate.AddMonths(2)
        draw_class.Limited = False
        draw_class.Limit = 0
        draw_class.LimitPerVoucher = CLASS_DRAW.DEFAULT_MAX_NUMBERS_PER_VOUCHER
        draw_class.LastNumber = -1
        draw_class.InitialNumber = -1
        draw_class.MaxNumber = -1
        draw_class.CreditType = -1
        draw_class.NumberPrice = 0
        draw_class.LevelFilter = WSI.Common.DrawAccountType.ALL
        draw_class.GenderFilter = WSI.Common.DrawGenderFilter.ALL
        draw_class.PointsLevel1 = -1
        draw_class.PointsLevel2 = -1
        draw_class.PointsLevel3 = -1
        draw_class.PointsLevel4 = -1

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          draw_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
          nls_param2 = m_input_draw_name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          draw_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
          nls_param2 = draw_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_draw_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          draw_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
          nls_param2 = draw_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          draw_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
          nls_param2 = draw_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_draw_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          draw_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
          nls_param2 = draw_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_draw_name.Focus()
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        ' QMP 23-APR-2013: Specific error message for raffle dependencies
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
          draw_class = Me.DbEditedObject
          nls_param1 = draw_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          draw_class = Me.DbEditedObject
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
          nls_param2 = draw_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(115), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        draw_class = Me.DbEditedObject
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(319)
        nls_param2 = draw_class.Name
        rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(111), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1, _
                        nls_param2)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_draw_name

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - DrawId
  '       - DrawName
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal DrawId As Integer, ByVal DrawName As String)
    'Dim _draw As CLASS_DRAW

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = DrawId
    Me.m_input_draw_name = DrawName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> ENUM_STATUS.STATUS_OK Then

      Return
    End If

    Call Me.Display(True)

  End Sub 'ShowEditItem

  ' PURPOSE: Ignore [ENTER] key when editing header/footer
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If e.KeyChar = Chr(Keys.Enter) Then
      If Me.ef_header.ContainsFocus Then
        Return True
      End If

      If Me.ef_footer.ContainsFocus Then
        Return True
      End If

      If Me.ef_detail1.ContainsFocus Then
        Return True
      End If

      If Me.ef_detail2.ContainsFocus Then
        Return True
      End If

      If Me.ef_detail3.ContainsFocus Then
        Return True
      End If

    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Init the checkBox text with value or default
  '
  '  PARAMS:
  '     - INPUT: checkbox1 
  '              checkbox2
  '              Value
  '              DefaultNls: checkbox.text settes with this value if Value is NULL or empty
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub InitCheckBoxTitles(ByRef ChkBox1 As CheckBox, ByRef ChkBox2 As CheckBox, ByVal Value As String, ByVal DefaultNLS As Integer)
    ChkBox1.Text = Value
    ChkBox2.Text = Value
    If Value = "" Then
      ChkBox1.Text = GLB_NLS_GUI_CONFIGURATION.GetString(DefaultNLS)
      ChkBox2.Text = GLB_NLS_GUI_CONFIGURATION.GetString(DefaultNLS)
    End If
  End Sub

  ' PURPOSE: Init credit type combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DrawTypeComboFill()

    Dim _draw As CLASS_DRAW
    _draw = DbReadObject

    ' WSI.Common.DrawType.BY_PLAYED_CREDIT
    Me.cmb_draw_type.Add(0, GLB_NLS_GUI_CONFIGURATION.GetString(302))
    ' WSI.Common.DrawType.BY_REDEEMABLE_SPENT
    Me.cmb_draw_type.Add(1, GLB_NLS_GUI_CONFIGURATION.GetString(303))
    ' WSI.Common.DrawType.BY_TOTAL_SPENT
    Me.cmb_draw_type.Add(2, GLB_NLS_GUI_CONFIGURATION.GetString(313))
    ' WSI.Common.DrawType.BY_POINTS
    Me.cmb_draw_type.Add(3, GLB_NLS_GUI_CONFIGURATION.GetString(329))
    ' WSI.Common.DrawType.BY_CASH_IN
    Me.cmb_draw_type.Add(4, GLB_NLS_GUI_CONFIGURATION.GetString(330))
    ' WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
    Me.cmb_draw_type.Add(5, GLB_NLS_GUI_CONFIGURATION.GetString(422))

    Me.cmb_draw_type.SelectedIndex = -1

  End Sub ' DrawTypeComboFill

  ' PURPOSE: Validate the data presented on the winner group box after clicking 'Check' button.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Private Function GUI_IsScreenWinnerOk() As Boolean
    Dim nls_param1 As String

    ' blank field Winning Number
    If ef_winning_number.Value = "" Then
      nls_param1 = GLB_NLS_GUI_CONTROLS.GetString(320)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_winning_number.Focus()

      Return False
    End If

    If ef_last_number.Value.Equals("---") Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(113), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call ef_winning_number.Focus()

      Return False
    End If

    If CInt(ef_winning_number.Value) > CInt(ef_last_number.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(112), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _
                      GUI_FormatNumber(ef_winning_number.Value, 0), GUI_FormatNumber(ef_last_number.Value, 0))
      Call ef_winning_number.Focus()

      Return False
    End If

    Return True
  End Function

  ' PURPOSE: Search for the winner ticket according to the entered number, and show winner data.
  '
  '  PARAMS:
  '     - INPUT: ShowVoucher As Boolean: Indicate if have to show voucher in a new window.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub ShowWinner(ByVal ShowVoucher As Boolean)
    Dim sql_connection As SqlConnection
    Dim sql_command As SqlCommand
    Dim str_sql As String
    Dim sql_reader As SqlDataReader
    Dim winner_found As Boolean
    Dim error_reader As Boolean
    Dim cashier_voucher_html As String
    Dim web_browser As frm_web_browser
    Dim count As Integer

    If Not GUI_IsScreenWinnerOk() Then
      Return
    End If

    winner_found = False
    error_reader = False
    cashier_voucher_html = ""

    Try
      sql_connection = NewSQLConnection()

      If Not (sql_connection Is Nothing) Then
        ' Connected
        If sql_connection.State <> ConnectionState.Open Then
          ' Connecting ..
          sql_connection.Open()
        End If

        If Not sql_connection.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      str_sql = "SELECT DT_ID " & _
                    " , AC_ACCOUNT_ID " & _
                    " , AC_HOLDER_NAME " & _
                    " , CV_VOUCHER_ID " & _
                    " , CV_HTML " & _
                " FROM DRAW_TICKETS, ACCOUNTS, CASHIER_VOUCHERS " & _
                " WHERE DT_ACCOUNT_ID    = AC_ACCOUNT_ID " & _
                  " AND DT_VOUCHER_ID    = CV_VOUCHER_ID " & _
                  " AND DT_DRAW_ID       = @draw_id " & _
                  " AND DT_FIRST_NUMBER <= @winning_number " & _
                  " AND DT_LAST_NUMBER  >= @winning_number "

      sql_command = New SqlCommand(str_sql)
      sql_command.Connection = sql_connection

      sql_command.Parameters.Add("@draw_id", SqlDbType.Int, 0).Value = DbObjectId
      sql_command.Parameters.Add("@winning_number", SqlDbType.Int, 0).Value = ef_winning_number.Value
      sql_reader = sql_command.ExecuteReader()

      count = 0
      While sql_reader.Read()
        If sql_reader.IsDBNull(2) Then
          ef_winner_name.Value = ""
        Else
          ef_winner_name.Value = sql_reader("AC_HOLDER_NAME")
        End If
        ef_voucher.Value = sql_reader("CV_VOUCHER_ID")
        ef_account.Value = sql_reader("AC_ACCOUNT_ID")
        ef_draw_ticket.Value = sql_reader("DT_ID")

        cashier_voucher_html = sql_reader("CV_HTML")

        winner_found = True
        count = count + 1
      End While

      sql_reader.Close()

      If count > 1 Then
        error_reader = True
        ef_winner_name.Value = ""
        ef_voucher.Value = ""
        ef_account.Value = ""
        ef_draw_ticket.Value = ""
      End If

      If ShowVoucher Then
        web_browser = New frm_web_browser()
        web_browser.wb_voucher.DocumentText = cashier_voucher_html
        web_browser.ShowDialog()
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    Finally
      If Not winner_found Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(113), ENUM_MB_TYPE.MB_TYPE_WARNING)
      End If
      If error_reader Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(115), ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If

    End Try

  End Sub ' ShowWinner

  Private Function GetWeekday() As Integer
    Dim bit_array As Char()

    bit_array = New String("0000000").ToCharArray()

    bit_array(6) = Convert.ToInt32(Me.chk_offer_sunday.Checked).ToString()  ' Sunday
    bit_array(5) = Convert.ToInt32(Me.chk_offer_monday.Checked).ToString()  ' Monday 
    bit_array(4) = Convert.ToInt32(Me.chk_offer_tuesday.Checked).ToString()  ' Tuesday
    bit_array(3) = Convert.ToInt32(Me.chk_offer_wednesday.Checked).ToString()  ' Wednesday
    bit_array(2) = Convert.ToInt32(Me.chk_offer_thursday.Checked).ToString()  ' Thursday
    bit_array(1) = Convert.ToInt32(Me.chk_offer_friday.Checked).ToString()  ' Friday
    bit_array(0) = Convert.ToInt32(Me.chk_offer_saturday.Checked).ToString()  ' Saturday

    Return Convert.ToInt32(bit_array, 2)

  End Function ' GetWeekday

  Private Sub cmb_draw_type_ValueChanged()

    Dim DbReadObjectCopy As CLASS_DRAW
    Dim DbEditedObjectCopy As CLASS_DRAW

    Call EnableControlsByDrawType()

    If Not m_changed_values Then

      GUI_GetScreenData()

      DbReadObjectCopy = DbReadObject.Duplicate
      DbEditedObjectCopy = DbEditedObject.Duplicate

      DbReadObjectCopy.CreditType = DbEditedObjectCopy.CreditType

      If DbReadObjectCopy.AuditorData.IsEqual(DbEditedObjectCopy.AuditorData) Then

        DbEditedObjectCopy.GetDrawDefaultValues()
        DbReadObject = DbEditedObjectCopy.Duplicate

        GUI_SetScreenData(0)

      Else
        m_changed_values = True
      End If


    End If




  End Sub

  ' PURPOSE: Enable/Disable and Reset draw features and values depending on draw type
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub EnableControlsByDrawType()

    Dim _draw As CLASS_DRAW
    _draw = DbReadObject

    ' RCI 05-JAN-2012: show minimum played and spent entries
    Me.chk_min_played.Visible = True
    Me.ef_min_played.Visible = True
    Me.chk_min_spent.Visible = True
    Me.uc_provider_filter1.Enabled = True

    Select Case Me.cmb_draw_type.Value

      Case 0 ' WSI.Common.DrawType.BY_PLAYED_CREDIT
        Me.tab_cashin_conditions.Enabled = False
        Me.ef_number_price.Enabled = True
        Me.ef_number_price.Visible = True
        Me.ef_min_played.Enabled = False
        Me.ef_min_played.Value = 0
        Me.chk_min_played.Checked = False
        Me.ef_min_spent.Enabled = False
        Me.ef_min_spent.Value = 0
        Me.chk_min_spent.Checked = False
        Me.ef_min_cash_in.Enabled = False
        Me.ef_min_cash_in.Value = 0

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.chk_by_level.Enabled = True

        Me.gb_new_offer.Enabled = True

        Me.chk_first_cash_in_not_constrained.Checked = False
        Me.chk_limit_per_operation.Enabled = False
        Me.chk_limit_per_operation.Checked = False
        Me.ef_limit_per_operation.Value = ""
        Me.ef_limit_per_operation.Enabled = False

        Me.chk_first_cash_in_not_constrained.Visible = True
        Me.lbl_first_cash_in_constrained_1.Visible = True
        Me.lbl_first_cash_in_constrained_2.Visible = True

      Case 1 ' WSI.Common.DrawType.BY_REDEEMABLE_SPENT
        Me.tab_cashin_conditions.Enabled = False
        Me.ef_number_price.Enabled = True
        Me.ef_number_price.Visible = True
        Me.ef_min_played.Enabled = False
        Me.ef_min_played.Value = 0
        Me.chk_min_played.Checked = False
        Me.ef_min_spent.Enabled = False
        Me.ef_min_spent.Value = 0
        Me.chk_min_spent.Checked = False
        Me.ef_min_cash_in.Enabled = False
        Me.ef_min_cash_in.Value = 0

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.chk_by_level.Enabled = True

        Me.gb_new_offer.Enabled = True

        Me.chk_first_cash_in_not_constrained.Checked = False
        Me.chk_limit_per_operation.Enabled = False
        Me.chk_limit_per_operation.Checked = False
        Me.ef_limit_per_operation.Value = ""
        Me.ef_limit_per_operation.Enabled = False

        Me.chk_first_cash_in_not_constrained.Visible = True
        Me.lbl_first_cash_in_constrained_1.Visible = True
        Me.lbl_first_cash_in_constrained_2.Visible = True

      Case 2 ' WSI.Common.DrawType.BY_TOTAL_SPENT
        Me.tab_cashin_conditions.Enabled = False
        Me.ef_number_price.Enabled = True
        Me.ef_number_price.Visible = True
        Me.ef_min_played.Enabled = False
        Me.ef_min_played.Value = 0
        Me.chk_min_played.Checked = False
        Me.ef_min_spent.Enabled = False
        Me.ef_min_spent.Value = 0
        Me.chk_min_spent.Checked = False
        Me.ef_min_cash_in.Enabled = False
        Me.ef_min_cash_in.Value = 0

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.chk_by_level.Enabled = True

        Me.gb_new_offer.Enabled = True

        Me.chk_first_cash_in_not_constrained.Checked = False
        Me.chk_limit_per_operation.Enabled = False
        Me.chk_limit_per_operation.Checked = False
        Me.ef_limit_per_operation.Value = ""
        Me.ef_limit_per_operation.Enabled = False

        Me.chk_first_cash_in_not_constrained.Visible = True
        Me.lbl_first_cash_in_constrained_1.Visible = True
        Me.lbl_first_cash_in_constrained_2.Visible = True

      Case 3 ' WSI.Common.DrawType.BY_POINTS
        Me.tab_cashin_conditions.Enabled = False
        Me.ef_number_price.Value = 0
        Me.ef_number_price.Enabled = False
        Me.ef_number_price.Visible = False
        Me.ef_min_played.Enabled = False
        Me.ef_min_played.Value = 0
        Me.chk_min_played.Checked = False
        Me.ef_min_spent.Enabled = False
        Me.ef_min_spent.Value = 0
        Me.chk_min_spent.Checked = False
        Me.ef_min_cash_in.Enabled = False
        Me.ef_min_cash_in.Value = 0
        ' DLL 03-MAR-2014: ignore providers
        Me.uc_provider_filter1.Enabled = False

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.gb_new_offer.Enabled = False

        Me.chk_first_cash_in_not_constrained.Checked = False
        Me.chk_limit_per_operation.Enabled = False
        Me.chk_limit_per_operation.Checked = False
        Me.ef_limit_per_operation.Enabled = False
        Me.ef_limit_per_operation.Value = ""

        Me.chk_first_cash_in_not_constrained.Visible = True
        Me.lbl_first_cash_in_constrained_1.Visible = True
        Me.lbl_first_cash_in_constrained_2.Visible = True

        Me.chk_by_level.Enabled = False
        Me.chk_by_level.Checked = False
        Me.chk_level_anonymous.Enabled = False
        Me.chk_level_01.Enabled = False
        Me.chk_level_02.Enabled = False
        Me.chk_level_03.Enabled = False
        Me.chk_level_04.Enabled = False

      Case 4 ' WSI.Common.DrawType.BY_CASH_IN
        Me.tab_cashin_conditions.Enabled = True
        Me.chk_min_played.Enabled = True
        Me.chk_min_spent.Enabled = True

        Me.ef_number_price.Enabled = True
        Me.ef_number_price.Visible = True
        Me.ef_min_played.Enabled = chk_min_played.Checked
        Me.ef_min_spent.Enabled = chk_min_spent.Checked
        Me.ef_min_cash_in.Enabled = True

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.chk_by_level.Enabled = True

        Me.gb_new_offer.Enabled = True

        Me.chk_limit_per_operation.Enabled = True

        Me.chk_first_cash_in_not_constrained.Visible = True
        Me.lbl_first_cash_in_constrained_1.Visible = True
        Me.lbl_first_cash_in_constrained_2.Visible = True

      Case 5 ' WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
        Me.tab_cashin_conditions.Enabled = True
        Me.ef_number_price.Enabled = True
        Me.ef_number_price.Visible = True
        Me.ef_min_played.Enabled = chk_min_played.Checked
        Me.ef_min_spent.Enabled = True
        Me.ef_min_cash_in.Enabled = True

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.chk_by_level.Enabled = True

        Me.gb_new_offer.Enabled = True

        Me.chk_limit_per_operation.Enabled = True

        Me.chk_first_cash_in_not_constrained.Visible = False
        Me.lbl_first_cash_in_constrained_1.Visible = False
        Me.lbl_first_cash_in_constrained_2.Visible = False

        ' RCI 05-JAN-2012: Disable minimum played entries
        Me.chk_min_played.Visible = False
        Me.ef_min_played.Visible = False
        Me.chk_min_spent.Visible = False

        Me.chk_min_played.Enabled = False
        Me.chk_min_played.Checked = False
        Me.ef_min_played.Enabled = False
        Me.ef_min_played.Value = 0
        Me.chk_min_spent.Enabled = False

      Case Else
        Me.ef_number_price.Enabled = True
        Me.ef_number_price.Visible = True
        Me.tab_cashin_conditions.Enabled = False
        Me.ef_min_played.Enabled = False
        Me.ef_min_played.Value = 0
        Me.chk_min_played.Checked = False
        Me.ef_min_spent.Enabled = False
        Me.ef_min_spent.Value = 0
        Me.chk_min_spent.Checked = False
        Me.ef_min_cash_in.Enabled = False
        Me.ef_min_cash_in.Value = 0

        EnableControlsByPointsPrice(chk_points_level1, ef_points_level1, _draw.PointsLevel1)
        EnableControlsByPointsPrice(chk_points_level2, ef_points_level2, _draw.PointsLevel2)
        EnableControlsByPointsPrice(chk_points_level3, ef_points_level3, _draw.PointsLevel3)
        EnableControlsByPointsPrice(chk_points_level4, ef_points_level4, _draw.PointsLevel4)

        Me.chk_by_level.Enabled = True

        Me.gb_new_offer.Enabled = False
        Me.chk_limit_per_operation.Enabled = False
        Me.ef_limit_per_operation.Enabled = False

        Me.chk_first_cash_in_not_constrained.Visible = True
        Me.lbl_first_cash_in_constrained_1.Visible = True
        Me.lbl_first_cash_in_constrained_2.Visible = True

    End Select

  End Sub ' EnableControlsByDrawType

  ' PURPOSE: Calculate values of text labels 
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -

  Private Sub RefreshLabelTexts()

    Dim empty_placeholder As String
    empty_placeholder = "---"

    Try
      ' Total numbers
      If uc_entry_draw_numbers_initial.Value <> "" And _
          GUI_ParseNumber(uc_entry_draw_numbers_max.Value) >= 0 And _
          (GUI_ParseNumber(uc_entry_draw_numbers_max.Value) >= GUI_ParseNumber(uc_entry_draw_numbers_initial.Value)) Then

        Dim _dummy As New GUI_Controls.uc_entry_field
        _dummy.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 0)
        _dummy.Value = GUI_ParseNumber(uc_entry_draw_numbers_max.Value) - GUI_ParseNumber(uc_entry_draw_numbers_initial.Value) + 1
        lbl_total_numbers_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(467, _dummy.GridValue)
        _dummy = Nothing

      Else
        lbl_total_numbers_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(467, empty_placeholder)
      End If

      ' Ratio Price generated
      If ef_number_price.Visible = True Then

        Dim _type As String = ""

        Select Case cmb_draw_type.Value
          Case 0
            'WSI.Common.DrawType.BY_PLAYED_CREDIT
            _type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(505)
          Case 1
            'WSI.Common.DrawType.BY_REDEEMABLE_SPENT
            _type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(506)
          Case 2
            'WSI.Common.DrawType.BY_TOTAL_SPENT
            _type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(507)
          Case 3
            'WSI.Common.DrawType.BY_POINTS
            _type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(508)
          Case 4
            'WSI.Common.DrawType.BY_CASH_IN
            _type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(509)
          Case 5
            'WSI.Common.DrawType.BY_CASH_IN_POSTERIORI
            _type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(509)
          Case Else

        End Select

        If GUI_ParseNumber(ef_number_price.Value) > 0 Then

          lbl_price_ratio.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(504, GUI_FormatCurrency(ef_number_price.Value), _type)
        Else
          lbl_price_ratio.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(504, empty_placeholder, _type)
        End If

      Else
        lbl_price_ratio.Text = ""
      End If

      ' Offers not apply warning
      If chk_points_level1.Checked Or chk_points_level2.Checked Or chk_points_level3.Checked Or chk_points_level4.Checked Then
        lbl_not_apply.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(487)
      Else
        lbl_not_apply.Value = ""
      End If

      If cmb_draw_type.Value <> WSI.Common.DrawType.BY_CASH_IN _
        And cmb_draw_type.Value <> WSI.Common.DrawType.BY_CASH_IN_POSTERIORI Then
        lbl_cash_in_conditions_1.Value = ""
        lbl_cash_in_conditions_2.Value = ""
      Else
        Dim str_and As String
        Dim str_played As String
        Dim str_spent As String
        Dim str_posteriori As String

        str_and = ""
        str_posteriori = ""

        If cmb_draw_type.Value = WSI.Common.DrawType.BY_CASH_IN_POSTERIORI Then
          str_posteriori = GLB_NLS_GUI_PLAYER_TRACKING.GetString(565)
        End If

        If (ef_min_played.Enabled And ef_min_played.Value > 0) Or (ef_min_spent.Enabled And ef_min_spent.Value > 0) Then
          str_and = GLB_NLS_GUI_PLAYER_TRACKING.GetString(513)
        End If
        lbl_cash_in_conditions_1.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(512, GUI_FormatCurrency(ef_min_cash_in.Value), str_and)

        str_played = GLB_NLS_GUI_PLAYER_TRACKING.GetString(514, GUI_FormatNumber(ef_min_played.Value, 2))
        str_spent = GLB_NLS_GUI_PLAYER_TRACKING.GetString(515, GUI_FormatNumber(ef_min_spent.Value, 2))

        If (ef_min_played.Enabled And ef_min_played.Value > 0) And (ef_min_spent.Enabled And ef_min_spent.Value > 0) Then
          lbl_cash_in_conditions_2.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(516, str_played, str_spent) & str_posteriori
        ElseIf ef_min_played.Enabled And ef_min_played.Value > 0 Then
          lbl_cash_in_conditions_2.Value = str_played & str_posteriori
        ElseIf ef_min_spent.Enabled And ef_min_spent.Value > 0 Then
          lbl_cash_in_conditions_2.Value = str_spent & str_posteriori
        Else
          lbl_cash_in_conditions_2.Value = ""
        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

  ' PURPOSE: Fills offers grid from draw item detail
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshOffers(ByVal Draw As CLASS_DRAW)

    dg_offers.Clear()

    If Not Draw.OfferList Is Nothing Then

      Dim _curOffer As WSI.Common.DrawOffer

      For _row_index As Integer = 0 To Draw.OfferList.Count - 1

        _curOffer = Draw.OfferList.GetOffer(_row_index)

        dg_offers.AddRow()

        dg_offers.Cell(_row_index, DRAW_OFFER_GRID_COLUMN_KEY).Value = _curOffer.Key
        dg_offers.Cell(_row_index, DRAW_OFFER_GRID_COLUMN_DESCRIPTION).Value = Draw.OfferAuditString(_curOffer, GLB_NLS_GUI_PLAYER_TRACKING.GetString(325), GLB_NLS_GUI_PLAYER_TRACKING.GetString(326), GLB_NLS_GUI_PLAYER_TRACKING.GetString(327), GLB_NLS_GUI_PLAYER_TRACKING.GetString(328), GLB_NLS_GUI_PLAYER_TRACKING.GetString(329), GLB_NLS_GUI_PLAYER_TRACKING.GetString(330), GLB_NLS_GUI_PLAYER_TRACKING.GetString(331), GLB_NLS_GUI_PLAYER_TRACKING.GetString(480), GLB_NLS_GUI_PLAYER_TRACKING.GetString(481), GLB_NLS_GUI_PLAYER_TRACKING.GetString(479), GLB_NLS_GUI_PLAYER_TRACKING.GetString(482), GLB_NLS_GUI_PLAYER_TRACKING.GetString(483), GLB_NLS_GUI_PLAYER_TRACKING.GetString(484))

      Next _row_index

    End If ' Not Draw.OfferList Is Nothing

  End Sub ' RefreshDbGrid

  ' PURPOSE: Enable/Disable offer day related controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub EnableWeekdayOrSingleDay()

    If opt_offer_single_day.Checked Then

      dtp_offer_day.Enabled = True

      chk_offer_monday.Enabled = False
      chk_offer_tuesday.Enabled = False
      chk_offer_wednesday.Enabled = False
      chk_offer_thursday.Enabled = False
      chk_offer_friday.Enabled = False
      chk_offer_saturday.Enabled = False
      chk_offer_sunday.Enabled = False

      chk_offer_monday.Checked = False
      chk_offer_tuesday.Checked = False
      chk_offer_wednesday.Checked = False
      chk_offer_thursday.Checked = False
      chk_offer_friday.Checked = False
      chk_offer_saturday.Checked = False
      chk_offer_sunday.Checked = False

      btn_check_all_days.Enabled = False

    ElseIf opt_offer_weekday.Checked Then

      dtp_offer_day.Enabled = False
      dtp_offer_day.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

      chk_offer_monday.Enabled = True
      chk_offer_tuesday.Enabled = True
      chk_offer_wednesday.Enabled = True
      chk_offer_thursday.Enabled = True
      chk_offer_friday.Enabled = True
      chk_offer_saturday.Enabled = True
      chk_offer_sunday.Enabled = True

      btn_check_all_days.Enabled = True

    Else

      dtp_offer_day.Enabled = False
      dtp_offer_day.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

      chk_offer_monday.Enabled = False
      chk_offer_tuesday.Enabled = False
      chk_offer_wednesday.Enabled = False
      chk_offer_thursday.Enabled = False
      chk_offer_friday.Enabled = False
      chk_offer_saturday.Enabled = False
      chk_offer_sunday.Enabled = False

      btn_check_all_days.Enabled = False

      chk_offer_monday.Checked = False
      chk_offer_tuesday.Checked = False
      chk_offer_wednesday.Checked = False
      chk_offer_thursday.Checked = False
      chk_offer_friday.Checked = False
      chk_offer_saturday.Checked = False
      chk_offer_sunday.Checked = False

    End If

  End Sub


  ' PURPOSE: Enable/Disable points price
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub EnableControlsByPointsPrice(ByRef ChkBox As CheckBox, ByRef EntField As uc_entry_field, ByVal Value As Double)
    If Value > 0 Then
      EntField.Value = Value
      ChkBox.Checked = True
      EntField.Enabled = True
    Else
      EntField.Value = ""
      ChkBox.Checked = False
      EntField.Enabled = False
    End If
  End Sub


  Public Overloads Property TimeFrom() As Integer
    Get
      Return Me.dtp_time_from.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_time_from.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Overloads Property TimeTo() As Integer
    Get
      Return Me.dtp_time_to.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_time_to.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property


  Private Function IsNewOfferOk() As Boolean
    Dim nls_param1 As String
    Dim nls_param2 As String

    nls_param1 = ""
    nls_param2 = ""

    ' N >= 0. Allow N to be 0.
    If ef_offer_n.Value = "" Then
      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_offer_n.Focus()

      Return False
    End If

    ' if N <> 0, M > 0
    If ef_offer_n.Value <> "0" And GUI_ParseNumber(ef_offer_m.Value) = 0 Then
      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(472)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_offer_m.Focus()

      Return False
    End If

    ' Must be N >= M
    If GUI_ParseNumber(ef_offer_n.Value) > 0 And GUI_ParseNumber(ef_offer_n.Value) < GUI_ParseNumber(ef_offer_m.Value) Then

      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471) & " " & "N"
      nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471) & " " & "M"

      'Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(130), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1, nls_param2)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(132), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , , )
      'NLS_ID_GUI_PLAYER_TRACKING(132)
      Call ef_offer_n.Focus()

      Return False
    End If

    If (GUI_ParseNumber(ef_offer_m.Value) = GUI_ParseNumber(ef_offer_n.Value)) And GUI_ParseNumber(ef_offer_n.Value) > 1 Then

      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471) & " " & "N"
      nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(471) & " " & "M"

      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(130), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1, nls_param2)
      Call ef_offer_n.Focus()

      Return False
    End If

    If Not Me.opt_offer_weekday.Checked And Not Me.opt_offer_single_day.Checked Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call opt_offer_weekday.Focus()

      Return False
    End If

    ' Weekday at least one day checked
    If Me.opt_offer_weekday.Checked Then
      If GetWeekday() = 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call opt_offer_weekday.Focus()

        Return False
      End If
    End If

    ' Check gender offer filter
    If Me.chk_by_gender_offer.Checked Then
      If Not Me.opt_gender_male_offer.Checked And Not Me.opt_gender_female_offer.Checked Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call opt_gender_male_offer.Focus()

        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE: Format appearance of the offers data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub GUI_DG_OffersStyleSheet()

    With dg_offers

      Call .Init(DRAW_OFFER_GRID_COLUMNS, DRAW_OFFER_GRID_HEADER_ROWS, False)

      .Sortable = False

      ' Key
      .Column(DRAW_OFFER_GRID_COLUMN_KEY).Width = 0
      .Column(DRAW_OFFER_GRID_COLUMN_KEY).HighLightWhenSelected = True
      .Column(DRAW_OFFER_GRID_COLUMN_KEY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(DRAW_OFFER_GRID_COLUMN_DESCRIPTION).Width = 5840
      .Column(DRAW_OFFER_GRID_COLUMN_DESCRIPTION).HighLightWhenSelected = True
      .Column(DRAW_OFFER_GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_DbStyleSheet


  ' PURPOSE: Re-set offer day depending on start and end draw values
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '    

  Private Sub SetOfferDayRange()

    ' dtp_start < dtp_end
    If DateTime.Compare(dtp_start.Value, dtp_end.Value) < 0 Then
      dtp_offer_day.SetRange(dtp_start.Value, dtp_end.Value)
    Else
      dtp_offer_day.SetRange(dtp_start.Value, dtp_start.Value)
    End If

  End Sub

  Private Sub RemoveOffers(Optional ByVal RemoveAll As Boolean = False)

    Dim _index As Integer
    Dim _draw As CLASS_DRAW
    Dim _topRow As Integer

    _draw = DbEditedObject
    _topRow = dg_offers.TopRow

    If RemoveAll = True Then

      For _index = 0 To dg_offers.NumRows - 1
        _draw.OfferList.RemoveOffer(dg_offers.Cell(_index, DRAW_OFFER_GRID_COLUMN_KEY).Value)
      Next

      dg_offers.Clear()

    Else

      If Not dg_offers.SelectedRows Is Nothing Then

        ' Do inverse loop, because we are deleting rows.
        For _index = dg_offers.NumRows - 1 To 0 Step -1
          If dg_offers.Row(_index).IsSelected Then

            _draw.OfferList.RemoveOffer(dg_offers.Cell(_index, DRAW_OFFER_GRID_COLUMN_KEY).Value)
            dg_offers.DeleteRowFast(_index)

          End If
        Next

      Else

        'Delete last one
        If dg_offers.NumRows > 0 Then
          _index = dg_offers.NumRows - 1
          _draw.OfferList.RemoveOffer(dg_offers.Cell(_index, DRAW_OFFER_GRID_COLUMN_KEY).Value)
          dg_offers.DeleteRowFast(_index)
        End If

      End If

    End If '  (RemoveAll = True) Then

    If dg_offers.NumRows > 8 Then
      dg_offers.TopRow = _topRow
    ElseIf (dg_offers.NumRows > 0) And (dg_offers.NumRows <= 8) Then
      dg_offers.TopRow = 0
    Else

    End If

    btn_offer_remove.Enabled = _draw.OfferList.Count > 0

  End Sub

  ' PURPOSE: Reset all the input fields needed to create an offer.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub ResetOfferFields()
    ef_offer_n.Value = ""
    ef_offer_n.Enabled = True
    ef_offer_m.Value = ""
    ef_offer_m.Enabled = True
    chk_by_gender_offer.Checked = False
    opt_offer_weekday.Checked = False
    opt_offer_single_day.Checked = False
    Call EnableWeekdayOrSingleDay()
    TimeFrom = 0
    TimeTo = 0

  End Sub ' ResetOfferFields

  Private Sub SetLevelFilter(ByVal LevelFilter As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    chk_by_level.Checked = LevelFilter <> 0
    chk_by_level_CheckedChanged(Nothing, Nothing)

    str_binary = Convert.ToString(LevelFilter, 2)
    str_binary = New String("0", 5 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(4) = "0") Then chk_level_anonymous.Checked = False Else chk_level_anonymous.Checked = True ' Anonymous Player
    If (bit_array(3) = "0") Then chk_level_01.Checked = False Else chk_level_01.Checked = True ' Level 01
    If (bit_array(2) = "0") Then chk_level_02.Checked = False Else chk_level_02.Checked = True ' Level 02
    If (bit_array(1) = "0") Then chk_level_03.Checked = False Else chk_level_03.Checked = True ' Level 03
    If (bit_array(0) = "0") Then chk_level_04.Checked = False Else chk_level_04.Checked = True ' Level 04

  End Sub ' SetLevelFilter

  Private Function GetLevelFilter() As Integer
    Dim bit_array As Char()

    If Not chk_by_level.Checked Then
      Return 0
    End If

    bit_array = New String("00000").ToCharArray()

    If (Not (cmb_draw_type.Value = WSI.Common.DrawType.BY_POINTS)) Then
      bit_array(4) = Convert.ToInt32(chk_level_anonymous.Checked).ToString()  ' Anonymous Player
    End If

    bit_array(3) = Convert.ToInt32(chk_level_01.Checked).ToString()  ' Level 01
    bit_array(2) = Convert.ToInt32(chk_level_02.Checked).ToString()  ' Level 02
    bit_array(1) = Convert.ToInt32(chk_level_03.Checked).ToString()  ' Level 03
    bit_array(0) = Convert.ToInt32(chk_level_04.Checked).ToString()  ' Level 04

    Return Convert.ToInt32(bit_array, 2)

  End Function ' GetLevelFilter


#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Timer operation to refresh labels.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Call RefreshLabelTexts()
  End Sub


  ' PURPOSE: Event handler routine for the Custom CHECK Button.
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub btn_check_winner_ClickEvent() Handles btn_check_winner.ClickEvent
    Call ShowWinner(False)
  End Sub ' btn_check_winner_ClickEvent

  ' PURPOSE: Event handler routine for the Custom VOUCHER Button.
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub btn_voucher_ClickEvent() Handles btn_voucher.ClickEvent
    Call ShowWinner(True)
  End Sub ' btn_check_winner_ClickEvent

  ' PURPOSE: Event handler routine for making CHECK button the default button when entering winning_number entry field.
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub ef_winning_number_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_winning_number.Enter
    m_default_button = Me.AcceptButton
    Me.AcceptButton = Me.btn_check_winner
  End Sub ' ef_winning_number_Enter

  ' PURPOSE: Event handler routine for restoring the form default button.
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub ef_winning_number_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_winning_number.Leave
    Me.AcceptButton = m_default_button
  End Sub ' ef_winning_number_Leave

  ' PURPOSE: Event handler routine for cleaning entries for the winner.
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub ef_winning_number_EntryFieldValueChanged() Handles ef_winning_number.EntryFieldValueChanged

    ef_winner_name.Value = ""
    ef_voucher.Value = ""
    ef_account.Value = ""
    ef_draw_ticket.Value = ""

  End Sub ' ef_winning_number_EntryFieldValueChanged

  Private Sub opt_offer_weekday_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_offer_weekday.CheckedChanged
    If opt_offer_weekday.Checked Then
      Call EnableWeekdayOrSingleDay()
    End If
  End Sub

  Private Sub opt_offer_single_day_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_offer_single_day.CheckedChanged
    If opt_offer_single_day.Checked Then
      Call EnableWeekdayOrSingleDay()
    End If
  End Sub

  ' PURPOSE: ' Event handler routine for the Custom add new offer button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub btn_offer_add_ClickEvent() Handles btn_offer_add.ClickEvent

    If IsNewOfferOk() Then

      Dim _draw As CLASS_DRAW
      Dim _offer_key As String
      Dim _offer_day As Date
      Dim _gender_offer As WSI.Common.DrawGenderFilter

      _draw = DbEditedObject

      If opt_offer_single_day.Checked Then
        _offer_day = New DateTime(dtp_offer_day.Value.Year, dtp_offer_day.Value.Month, dtp_offer_day.Value.Day, 0, 0, 0)
      Else
        _offer_day = Nothing
      End If

      ' Get gender offer filter
      _gender_offer = WSI.Common.DrawGenderFilter.ALL
      If chk_by_gender_offer.Checked Then
        If opt_gender_male_offer.Checked Then
          _gender_offer = WSI.Common.DrawGenderFilter.MEN_ONLY
        ElseIf opt_gender_female_offer.Checked Then
          _gender_offer = WSI.Common.DrawGenderFilter.WOMEN_ONLY
        End If
      End If

      'Add offer to collection
      _offer_key = _draw.OfferList.AddOffer(GUI_ParseNumber(ef_offer_n.Value), GUI_ParseNumber(ef_offer_m.Value), _gender_offer, GetWeekday(), _offer_day, TimeFrom, TimeTo)

      btn_offer_remove.Enabled = True

      'Add offer to grid
      If _offer_key <> String.Empty Then
        dg_offers.AddRow()
        dg_offers.Cell(dg_offers.NumRows - 1, DRAW_OFFER_GRID_COLUMN_KEY).Value = _offer_key
        dg_offers.Cell(dg_offers.NumRows - 1, DRAW_OFFER_GRID_COLUMN_DESCRIPTION).Value = _draw.OfferAuditString(_draw.OfferList.GetOffer(_draw.OfferList.Count - 1), GLB_NLS_GUI_PLAYER_TRACKING.GetString(325), GLB_NLS_GUI_PLAYER_TRACKING.GetString(326), GLB_NLS_GUI_PLAYER_TRACKING.GetString(327), GLB_NLS_GUI_PLAYER_TRACKING.GetString(328), GLB_NLS_GUI_PLAYER_TRACKING.GetString(329), GLB_NLS_GUI_PLAYER_TRACKING.GetString(330), GLB_NLS_GUI_PLAYER_TRACKING.GetString(331), GLB_NLS_GUI_PLAYER_TRACKING.GetString(480), GLB_NLS_GUI_PLAYER_TRACKING.GetString(481), GLB_NLS_GUI_PLAYER_TRACKING.GetString(479), GLB_NLS_GUI_PLAYER_TRACKING.GetString(482), GLB_NLS_GUI_PLAYER_TRACKING.GetString(483), GLB_NLS_GUI_PLAYER_TRACKING.GetString(484))
        dg_offers.TopRow = Math.Max(0, dg_offers.NumRows - 8)

        ResetOfferFields()
      Else
        ' Offer already exists
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(131), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(471))
        Call ef_offer_m.Focus()
      End If
    End If

  End Sub ' btn_offer_add_ClickEvent


  ' PURPOSE: ' Event handler routine for the Custom check all offer days button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub btn_check_all_days_ClickEvent() Handles btn_check_all_days.ClickEvent

    chk_offer_monday.Checked = True
    chk_offer_tuesday.Checked = True
    chk_offer_wednesday.Checked = True
    chk_offer_thursday.Checked = True
    chk_offer_friday.Checked = True
    chk_offer_saturday.Checked = True
    chk_offer_sunday.Checked = True

  End Sub ' btn_check_all_days

  ' PURPOSE: Event handler routine for the draw type 
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub cmb_draw_type_ValueChangedEvent() Handles cmb_draw_type.ValueChangedEvent

    EnableControlsByDrawType()

  End Sub

  ' PURPOSE: Event handler routine for the Custom remove offer button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub btn_offer_remove_ClickEvent() Handles btn_offer_remove.ClickEvent

    RemoveOffers(False)
  End Sub ' btn_offer_remove_ClickEvent

  ' PURPOSE: Event handler routine for the draw start date value changed
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub dtp_start_DatePickerValueChanged() Handles dtp_start.DatePickerValueChanged

    If m_load_complete = True Then
      dtp_end.SetRange(dtp_start.Value, dtp_start.Value.AddYears(5))
      Call SetOfferDayRange()
    End If
  End Sub

  ' PURPOSE: Event handler routine for the draw end date value changed
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub dtp_end_DatePickerValueChanged() Handles dtp_end.DatePickerValueChanged

    If m_load_complete = True Then
      Call SetOfferDayRange()
    End If
  End Sub

  ' PURPOSE: Event handler routine for enabling/ disabling number points price field 
  '
  '  PARAMS:
  '     - INPUT:
  '           -
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_points_level4.CheckedChanged, chk_points_level3.CheckedChanged, chk_points_level2.CheckedChanged, chk_points_level1.CheckedChanged

    Select Case sender.Name
      Case "chk_points_level1"
        If chk_points_level1.Checked Then
          ef_points_level1.Enabled = True
          ef_points_level1.Focus()
        Else
          ef_points_level1.Value = ""
          ef_points_level1.Enabled = False
        End If
      Case "chk_points_level2"
        If chk_points_level2.Checked Then
          ef_points_level2.Enabled = True
          ef_points_level2.Focus()
        Else
          ef_points_level2.Value = ""
          ef_points_level2.Enabled = False
        End If
      Case "chk_points_level3"
        If chk_points_level3.Checked Then
          ef_points_level3.Enabled = True
          ef_points_level3.Focus()
        Else
          ef_points_level3.Value = ""
          ef_points_level3.Enabled = False
        End If
      Case "chk_points_level4"
        If chk_points_level4.Checked Then
          ef_points_level4.Enabled = True
          ef_points_level4.Focus()
        Else
          ef_points_level4.Value = ""
          ef_points_level4.Enabled = False
        End If

    End Select
  End Sub

  Private Sub ShowVoucherPreview()
    Dim _preview As String
    Dim _fontSize As String
    Dim _numbers As String
    Dim _number1 As String
    Dim _number2 As String
    Dim _styles As String
    Dim _num_iters As Integer
    Dim _hide_borders As Boolean

    If tab_voucher.SelectedIndex = 0 Then

      _hide_borders = GeneralParam.GetBoolean("Cashier.DrawTicket", "BingoFormat.HideBorders", False)

      ' Can't put directly _styles in parameter {23}. Have to do it with variable _styles.
      _styles = "<style type='text/css'>" & _
                "  table {font: normal 14px arial}" & _
                "  td.padded {padding-left: 10px; padding-right: 10px;" & IIf(_hide_borders, "", "border: solid 2px black;") & "}" & _
                "</style>"

      _preview = "<html><head>" & _
                 "{23}" & _
                 "</head>" & _
                 "<body topmargin='0' leftmargin='0'>" & _
                 "<table border='0' align='left' cellpadding='0' cellspacing='0' width='270'>" & _
                 "<tr><td><div align='center' style='font-size: 150%'>{0}<br><br/></div>" & _
                 "<hr noshade='' size='4'><div align='right'>{1}</div>{2}{3}<br>{4}{5}<br>{6}{7}<br></td></tr>" & _
                 "<tr><td><table border='0' cellpadding='0' cellspacing='0'>" & _
                 "<!-- <td align='center' style='width: 321px'><p><B>{8}</B></p></td> -->" & _
                 "<tr align='center'><td style='width: 321px'><hr noshade='' size='4'>" & _
                 "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" & _
                 "<tr><td align='center'>{9}</td></tr><tr><td align='center'>" & _
                 "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" & _
                 "<tr><td align='center' style='font-size: 75%'>{10}<br></td>" & _
                 "<td align='center' style='font-size: 75%'>{11}<br></td></tr></table></td></tr></table>" & _
                 "<hr noshade='' size='4'></td></tr></table>{12}{13}<br>{14}{15} - {16}<br>{17}{18}<br>" & _
                 "<hr noshade='' size='4'>" & _
                 "{19}" & _
                 "<hr noshade='' size='4'></td></tr><tr><td>{20}{21}<br /><br /></td></tr>" & _
                 "<tr><td align='center' style='font-size: 75%'>{22}<br><br /><br />" & _
                 "</td></tr></table>" & _
                 "</body></html>"

      If uc_entry_draw_numbers_max.Value.Length > 7 Then
        _fontSize = "100"
      Else
        _fontSize = "150"
      End If

      If chk_bingo_format.Checked Then

        _number1 = WSI.Common.DrawBingoFormat.NumberToBingo(GUI_ParseNumber(uc_entry_draw_numbers_initial.Value), GUI_ParseNumber(uc_entry_draw_numbers_max.Value))

        If GUI_ParseNumber(ef_limit_per_voucher.Value) = 1 Then
          _numbers = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" & _
                     "<tr><td>" & _
                     "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-size: {0}%'>" & _
                     "<tr><td align='center'>{1}</td></tr></table></td></tr>" & _
                     "</table>"
          _numbers = String.Format(_numbers, _fontSize, _number1)
        Else
          _numbers = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" & _
                     "<tr><td>" & _
                     "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-size: {0}%'>" & _
                     "<tr><td align='center'>{1}</td></tr>" & _
                     "<tr><td><table><tr><td>&nbsp;</td></tr></table></td></tr>" & _
                     "<tr><td align='center'>{2}</td></tr></table></td></tr>" & _
                     "</table>"
          _number2 = WSI.Common.DrawBingoFormat.NumberToBingo(GUI_ParseNumber(uc_entry_draw_numbers_initial.Value) + 1, GUI_ParseNumber(uc_entry_draw_numbers_max.Value))
          _numbers = String.Format(_numbers, _fontSize, _number1, _number2)
        End If

      Else
        _number1 = GUI_ParseNumber(uc_entry_draw_numbers_initial.Value).ToString.PadLeft(uc_entry_draw_numbers_max.Value.Length, "0"c)

        If GUI_ParseNumber(ef_limit_per_voucher.Value) = 1 Then
          _numbers = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" & _
                     "<tr><td style='font-size: 150%'>" & _
                     "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-size: {0}%'>" & _
                     "<tr><td align='left'>{1}</td></tr></table></td></tr>" & _
                     "</table>"
          _numbers = String.Format(_numbers, _fontSize, _number1)
        Else
          _numbers = "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" & _
                     "<tr><td style='font-size: 150%'>" & _
                     "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='font-size: {0}%'>" & _
                     "<tr><td align='left'>{1}</td><td align='right'>{2}</td></tr></table></td></tr>" & _
                     "</table>"
          _number2 = (GUI_ParseNumber(uc_entry_draw_numbers_initial.Value) + 1).ToString.PadLeft(uc_entry_draw_numbers_max.Value.Length, "0"c)
          _numbers = String.Format(_numbers, _fontSize, _number1, _number2)
        End If

      End If

      '{0} [#CABECERA#]
      '{1} [#FECHA_HORA#]
      '{2} [#STR_USUARIO#]	"Usuario: "
      '{3} [#USUARIO#]
      '{4} [#STR_CAJERO#]	"Cajero: "
      '{5} [#CAJERO#]
      '{6} [#STR_RECIBO#]	"Recibo: "
      '{7} [#RECIBO#]
      '{8} [#STR_SORTEO#]	"Sorteo"
      '{9} [#TITULO#]
      '{10} [#DETALLE1#]
      '{11} [#DETALLE2#]
      '{12} [#STR_CLIENTE#]	"Cliente: "
      '{13} [#CLIENTE#]
      '{14} [#STR_CUENTA#]	"Cuenta: "
      '{15} [#CUENTA#]
      '{16} [#NIVEL#]
      '{17} [#STR_TARJETA#]	"Tarjeta: "
      '{18} [#TARJETA#]
      '{19} { _numbers }
      '{20} [#STR_NUMTICKET#]	"N&#250;mero de Ticket: "
      '{21} [#NUMTICKET#]
      '{22} [#PIE#]
      '{23} { _styles }

      _preview = String.Format(_preview, _
                               ef_header.Text, _
                               Format.CustomFormatDateTime(WGDB.Now, True), _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(491) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(491).ToLower & "]", _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(492) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(492).ToLower & "]", _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(493) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(493).ToLower & "]", _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(494) & ": ", _
                               ef_detail1.Text, ef_detail2.Text, ef_detail3.Text, _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(495) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(495).ToLower & "]", _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(496) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(496).ToLower & "]", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(499).ToLower & "]", _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(497) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(497).ToLower & "]", _
                               _numbers, _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(498) & ": ", _
                               "[" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(498).ToLower & "]", _
                               ef_footer.Text, _
                               _styles)

      wb_voucher.DocumentText = _preview

      ' Wait until the document is completely loaded. Needed for Bingo format tickets.
      _num_iters = 0
      While wb_voucher.ReadyState <> WebBrowserReadyState.Complete
        System.Threading.Thread.Sleep(10)
        Application.DoEvents()

        _num_iters += 1
        ' Maximum 1 second of iterations
        If _num_iters > 100 Then
          Exit While
        End If
      End While

    End If
  End Sub

  ' PURPOSE: Event handler routine when user clicks on voucher preview tab
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub tab_voucher_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_voucher.SelectedIndexChanged

    ShowVoucherPreview()
  End Sub

  Private Sub uc_entry_draw_numbers_max_EntryFieldValueChanged() Handles uc_entry_draw_numbers_max.EntryFieldValueChanged

    ShowVoucherPreview()
  End Sub

  Private Sub ef_limit_per_voucher_EntryFieldValueChanged() Handles ef_limit_per_voucher.EntryFieldValueChanged

    ShowVoucherPreview()
  End Sub

  Private Sub chk_bingo_format_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_bingo_format.CheckedChanged

    ShowVoucherPreview()
  End Sub

  Private Sub chk_limit_to_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_limit_to.CheckedChanged

    If chk_limit_to.Checked Then
      ef_limit.Enabled = True
    Else
      ef_limit.Enabled = False
      ef_limit.Value = ""
    End If
  End Sub

  Private Sub chk_min_played_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_min_played.CheckedChanged
    ef_min_played.Enabled = chk_min_played.Checked
  End Sub

  Private Sub chk_min_spent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_min_spent.CheckedChanged
    ef_min_spent.Enabled = chk_min_spent.Checked
  End Sub

  Private Sub chk_by_gender_draw_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_gender_draw.CheckedChanged
    opt_gender_male_draw.Enabled = chk_by_gender_draw.Checked
    opt_gender_female_draw.Enabled = chk_by_gender_draw.Checked
    If chk_by_gender_draw.Checked Then
      chk_by_gender_offer.Checked = False
      chk_by_gender_offer.Enabled = False
    Else
      chk_by_gender_offer.Enabled = True
    End If
  End Sub

  Private Sub chk_by_gender_offer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_gender_offer.CheckedChanged
    opt_gender_male_offer.Enabled = chk_by_gender_offer.Checked
    opt_gender_female_offer.Enabled = chk_by_gender_offer.Checked
    If Not chk_by_gender_offer.Checked Then
      opt_gender_male_offer.Checked = False
      opt_gender_female_offer.Checked = False
    End If
  End Sub

  Private Sub ef_offer_n_EntryFieldValueChanged() Handles ef_offer_n.EntryFieldValueChanged
    If ef_offer_n.Value = "0" Then
      ef_offer_m.Enabled = False
    Else
      ef_offer_m.Enabled = True
    End If
  End Sub

  Private Sub btn_reset_ClickEvent() Handles btn_reset.ClickEvent
    ResetOfferFields()
  End Sub

  Private Sub chk_limit_per_operation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_limit_per_operation.CheckedChanged

    If chk_limit_per_operation.Checked Then
      ef_limit_per_operation.Enabled = True
    Else
      ef_limit_per_operation.Enabled = False
      ef_limit_per_operation.Value = ""
    End If
  End Sub



  Private Sub chk_by_level_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_level.CheckedChanged

    If chk_by_level.Enabled Then
      chk_level_anonymous.Enabled = chk_by_level.Checked
      chk_level_01.Enabled = chk_by_level.Checked
      chk_level_02.Enabled = chk_by_level.Checked
      chk_level_03.Enabled = chk_by_level.Checked
      chk_level_04.Enabled = chk_by_level.Checked
    End If
  End Sub

#End Region ' Events

End Class ' frm_draw_edit


