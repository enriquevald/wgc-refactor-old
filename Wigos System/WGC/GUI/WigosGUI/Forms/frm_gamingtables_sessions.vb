'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gamingtables_sessions
' DESCRIPTION:   This screen allows to view gaming tables sessions:
' AUTHOR:        Sergi Mart�nez
' CREATION DATE: 16-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-DEC-2013  SMN    Initial version.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports WSI.Common

Public Class frm_gamingtables_sessions
  Inherits frm_base_sel

End Class