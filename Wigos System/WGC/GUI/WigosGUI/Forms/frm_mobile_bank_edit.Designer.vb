<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mobile_bank_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_pin = New GUI_Controls.uc_entry_field
    Me.ef_limit_by_session = New GUI_Controls.uc_entry_field
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.ef_limit_recharge = New GUI_Controls.uc_entry_field
    Me.chk_disabled = New System.Windows.Forms.CheckBox
    Me.ef_limit_num_recharges = New GUI_Controls.uc_entry_field
    Me.gb_edit_mb = New System.Windows.Forms.GroupBox
    Me.ef_mb_id = New GUI_Controls.uc_entry_field
    Me.ef_employee_code = New GUI_Controls.uc_entry_field
    Me.gb_limits = New System.Windows.Forms.GroupBox
    Me.chk_limit_num_recharges = New System.Windows.Forms.CheckBox
    Me.chk_limit_recharge = New System.Windows.Forms.CheckBox
    Me.chk_limit_by_session = New System.Windows.Forms.CheckBox
    Me.panel_data.SuspendLayout()
    Me.gb_edit_mb.SuspendLayout()
    Me.gb_limits.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_edit_mb)
    Me.panel_data.Size = New System.Drawing.Size(350, 267)
    '
    'ef_pin
    '
    Me.ef_pin.DoubleValue = 0
    Me.ef_pin.IntegerValue = 0
    Me.ef_pin.IsReadOnly = False
    Me.ef_pin.Location = New System.Drawing.Point(33, 119)
    Me.ef_pin.Name = "ef_pin"
    Me.ef_pin.Size = New System.Drawing.Size(297, 24)
    Me.ef_pin.SufixText = "Sufix Text"
    Me.ef_pin.SufixTextVisible = True
    Me.ef_pin.TabIndex = 4
    Me.ef_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_pin.TextValue = ""
    Me.ef_pin.TextWidth = 140
    Me.ef_pin.Value = ""
    Me.ef_pin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_limit_by_session
    '
    Me.ef_limit_by_session.DoubleValue = 0
    Me.ef_limit_by_session.IntegerValue = 0
    Me.ef_limit_by_session.IsReadOnly = False
    Me.ef_limit_by_session.Location = New System.Drawing.Point(169, 16)
    Me.ef_limit_by_session.Name = "ef_limit_by_session"
    Me.ef_limit_by_session.Size = New System.Drawing.Size(155, 24)
    Me.ef_limit_by_session.SufixText = "Sufix Text"
    Me.ef_limit_by_session.SufixTextVisible = True
    Me.ef_limit_by_session.TabIndex = 1
    Me.ef_limit_by_session.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_by_session.TextValue = ""
    Me.ef_limit_by_session.TextWidth = 0
    Me.ef_limit_by_session.Value = ""
    Me.ef_limit_by_session.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(33, 43)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.Size = New System.Drawing.Size(297, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 140
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_limit_recharge
    '
    Me.ef_limit_recharge.DoubleValue = 0
    Me.ef_limit_recharge.IntegerValue = 0
    Me.ef_limit_recharge.IsReadOnly = False
    Me.ef_limit_recharge.Location = New System.Drawing.Point(169, 47)
    Me.ef_limit_recharge.Name = "ef_limit_recharge"
    Me.ef_limit_recharge.Size = New System.Drawing.Size(155, 24)
    Me.ef_limit_recharge.SufixText = "Sufix Text"
    Me.ef_limit_recharge.SufixTextVisible = True
    Me.ef_limit_recharge.TabIndex = 3
    Me.ef_limit_recharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_recharge.TextValue = ""
    Me.ef_limit_recharge.TextWidth = 0
    Me.ef_limit_recharge.Value = ""
    Me.ef_limit_recharge.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_disabled
    '
    Me.chk_disabled.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_disabled.Location = New System.Drawing.Point(111, 100)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(79, 17)
    Me.chk_disabled.TabIndex = 3
    Me.chk_disabled.Text = "xDisabed"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'ef_limit_num_recharges
    '
    Me.ef_limit_num_recharges.DoubleValue = 0
    Me.ef_limit_num_recharges.IntegerValue = 0
    Me.ef_limit_num_recharges.IsReadOnly = False
    Me.ef_limit_num_recharges.Location = New System.Drawing.Point(169, 77)
    Me.ef_limit_num_recharges.Name = "ef_limit_num_recharges"
    Me.ef_limit_num_recharges.Size = New System.Drawing.Size(155, 24)
    Me.ef_limit_num_recharges.SufixText = "Sufix Text"
    Me.ef_limit_num_recharges.SufixTextVisible = True
    Me.ef_limit_num_recharges.TabIndex = 5
    Me.ef_limit_num_recharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_limit_num_recharges.TextValue = ""
    Me.ef_limit_num_recharges.TextWidth = 0
    Me.ef_limit_num_recharges.Value = ""
    Me.ef_limit_num_recharges.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_edit_mb
    '
    Me.gb_edit_mb.Controls.Add(Me.ef_mb_id)
    Me.gb_edit_mb.Controls.Add(Me.ef_employee_code)
    Me.gb_edit_mb.Controls.Add(Me.gb_limits)
    Me.gb_edit_mb.Controls.Add(Me.ef_name)
    Me.gb_edit_mb.Controls.Add(Me.ef_pin)
    Me.gb_edit_mb.Controls.Add(Me.chk_disabled)
    Me.gb_edit_mb.Location = New System.Drawing.Point(3, 3)
    Me.gb_edit_mb.Name = "gb_edit_mb"
    Me.gb_edit_mb.Size = New System.Drawing.Size(342, 261)
    Me.gb_edit_mb.TabIndex = 18
    Me.gb_edit_mb.TabStop = False
    '
    'ef_mb_id
    '
    Me.ef_mb_id.DoubleValue = 0
    Me.ef_mb_id.IntegerValue = 0
    Me.ef_mb_id.IsReadOnly = False
    Me.ef_mb_id.Location = New System.Drawing.Point(33, 13)
    Me.ef_mb_id.Name = "ef_mb_id"
    Me.ef_mb_id.Size = New System.Drawing.Size(297, 24)
    Me.ef_mb_id.SufixText = "Sufix Text"
    Me.ef_mb_id.SufixTextVisible = True
    Me.ef_mb_id.TabIndex = 0
    Me.ef_mb_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_mb_id.TextValue = ""
    Me.ef_mb_id.TextWidth = 140
    Me.ef_mb_id.Value = ""
    Me.ef_mb_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_employee_code
    '
    Me.ef_employee_code.DoubleValue = 0
    Me.ef_employee_code.IntegerValue = 0
    Me.ef_employee_code.IsReadOnly = False
    Me.ef_employee_code.Location = New System.Drawing.Point(33, 72)
    Me.ef_employee_code.Name = "ef_employee_code"
    Me.ef_employee_code.Size = New System.Drawing.Size(297, 24)
    Me.ef_employee_code.SufixText = "Sufix Text"
    Me.ef_employee_code.SufixTextVisible = True
    Me.ef_employee_code.TabIndex = 2
    Me.ef_employee_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_employee_code.TextValue = ""
    Me.ef_employee_code.TextWidth = 140
    Me.ef_employee_code.Value = ""
    Me.ef_employee_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_limits
    '
    Me.gb_limits.Controls.Add(Me.chk_limit_num_recharges)
    Me.gb_limits.Controls.Add(Me.chk_limit_recharge)
    Me.gb_limits.Controls.Add(Me.chk_limit_by_session)
    Me.gb_limits.Controls.Add(Me.ef_limit_recharge)
    Me.gb_limits.Controls.Add(Me.ef_limit_num_recharges)
    Me.gb_limits.Controls.Add(Me.ef_limit_by_session)
    Me.gb_limits.Location = New System.Drawing.Point(6, 144)
    Me.gb_limits.Name = "gb_limits"
    Me.gb_limits.Size = New System.Drawing.Size(330, 108)
    Me.gb_limits.TabIndex = 18
    Me.gb_limits.TabStop = False
    Me.gb_limits.Text = "xLimits"
    '
    'chk_limit_num_recharges
    '
    Me.chk_limit_num_recharges.AutoSize = True
    Me.chk_limit_num_recharges.Location = New System.Drawing.Point(7, 82)
    Me.chk_limit_num_recharges.Name = "chk_limit_num_recharges"
    Me.chk_limit_num_recharges.Size = New System.Drawing.Size(79, 17)
    Me.chk_limit_num_recharges.TabIndex = 4
    Me.chk_limit_num_recharges.Text = "xDisabed"
    Me.chk_limit_num_recharges.UseVisualStyleBackColor = True
    '
    'chk_limit_recharge
    '
    Me.chk_limit_recharge.AutoSize = True
    Me.chk_limit_recharge.Location = New System.Drawing.Point(7, 52)
    Me.chk_limit_recharge.Name = "chk_limit_recharge"
    Me.chk_limit_recharge.Size = New System.Drawing.Size(79, 17)
    Me.chk_limit_recharge.TabIndex = 2
    Me.chk_limit_recharge.Text = "xDisabed"
    Me.chk_limit_recharge.UseVisualStyleBackColor = True
    '
    'chk_limit_by_session
    '
    Me.chk_limit_by_session.AutoSize = True
    Me.chk_limit_by_session.Location = New System.Drawing.Point(7, 20)
    Me.chk_limit_by_session.Name = "chk_limit_by_session"
    Me.chk_limit_by_session.Size = New System.Drawing.Size(79, 17)
    Me.chk_limit_by_session.TabIndex = 0
    Me.chk_limit_by_session.Text = "xDisabed"
    Me.chk_limit_by_session.UseVisualStyleBackColor = True
    '
    'frm_mobile_bank_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(454, 275)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_mobile_bank_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_mobile_bank_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_edit_mb.ResumeLayout(False)
    Me.gb_edit_mb.PerformLayout()
    Me.gb_limits.ResumeLayout(False)
    Me.gb_limits.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_pin As GUI_Controls.uc_entry_field
  Friend WithEvents ef_limit_by_session As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_limit_recharge As GUI_Controls.uc_entry_field
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents ef_limit_num_recharges As GUI_Controls.uc_entry_field
  Friend WithEvents gb_edit_mb As System.Windows.Forms.GroupBox
  Friend WithEvents gb_limits As System.Windows.Forms.GroupBox
  Friend WithEvents chk_limit_num_recharges As System.Windows.Forms.CheckBox
  Friend WithEvents chk_limit_recharge As System.Windows.Forms.CheckBox
  Friend WithEvents chk_limit_by_session As System.Windows.Forms.CheckBox
  Friend WithEvents ef_employee_code As GUI_Controls.uc_entry_field
  Friend WithEvents ef_mb_id As GUI_Controls.uc_entry_field
End Class
