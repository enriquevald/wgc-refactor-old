<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_safe_keeping_movement_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_id = New GUI_Controls.uc_entry_field()
    Me.ef_name_account = New GUI_Controls.uc_entry_field()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_cage_session = New System.Windows.Forms.RadioButton()
    Me.opt_operation = New System.Windows.Forms.RadioButton()
    Me.gb_count = New System.Windows.Forms.GroupBox()
    Me.ef_document = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_count.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_count)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1099, 139)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_count, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 143)
    Me.panel_data.Size = New System.Drawing.Size(1099, 385)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1093, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1093, 4)
    '
    'ef_id
    '
    Me.ef_id.DoubleValue = 0.0R
    Me.ef_id.IntegerValue = 0
    Me.ef_id.IsReadOnly = False
    Me.ef_id.Location = New System.Drawing.Point(6, 20)
    Me.ef_id.Name = "ef_id"
    Me.ef_id.PlaceHolder = Nothing
    Me.ef_id.Size = New System.Drawing.Size(185, 24)
    Me.ef_id.SufixText = "Sufix Text"
    Me.ef_id.SufixTextVisible = True
    Me.ef_id.TabIndex = 0
    Me.ef_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_id.TextValue = ""
    Me.ef_id.Value = ""
    Me.ef_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name_account
    '
    Me.ef_name_account.DoubleValue = 0.0R
    Me.ef_name_account.IntegerValue = 0
    Me.ef_name_account.IsReadOnly = False
    Me.ef_name_account.Location = New System.Drawing.Point(6, 50)
    Me.ef_name_account.Name = "ef_name_account"
    Me.ef_name_account.PlaceHolder = Nothing
    Me.ef_name_account.Size = New System.Drawing.Size(260, 24)
    Me.ef_name_account.SufixText = "Sufix Text"
    Me.ef_name_account.SufixTextVisible = True
    Me.ef_name_account.TabIndex = 1
    Me.ef_name_account.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name_account.TextValue = ""
    Me.ef_name_account.Value = ""
    Me.ef_name_account.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(17, 67)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2015, 4, 14, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(17, 97)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2015, 4, 14, 0, 0, 0, 0)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_cage_session)
    Me.gb_date.Controls.Add(Me.opt_operation)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Location = New System.Drawing.Point(284, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(256, 130)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xgbDate"
    '
    'opt_cage_session
    '
    Me.opt_cage_session.AutoSize = True
    Me.opt_cage_session.Location = New System.Drawing.Point(7, 44)
    Me.opt_cage_session.Name = "opt_cage_session"
    Me.opt_cage_session.Size = New System.Drawing.Size(76, 17)
    Me.opt_cage_session.TabIndex = 1
    Me.opt_cage_session.Text = "xSession"
    Me.opt_cage_session.UseVisualStyleBackColor = True
    '
    'opt_operation
    '
    Me.opt_operation.AutoSize = True
    Me.opt_operation.Checked = True
    Me.opt_operation.Location = New System.Drawing.Point(7, 21)
    Me.opt_operation.Name = "opt_operation"
    Me.opt_operation.Size = New System.Drawing.Size(88, 17)
    Me.opt_operation.TabIndex = 0
    Me.opt_operation.TabStop = True
    Me.opt_operation.Text = "xOperation"
    Me.opt_operation.UseVisualStyleBackColor = True
    '
    'gb_count
    '
    Me.gb_count.Controls.Add(Me.ef_document)
    Me.gb_count.Controls.Add(Me.ef_id)
    Me.gb_count.Controls.Add(Me.ef_name_account)
    Me.gb_count.Location = New System.Drawing.Point(6, 6)
    Me.gb_count.Name = "gb_count"
    Me.gb_count.Size = New System.Drawing.Size(272, 111)
    Me.gb_count.TabIndex = 1
    Me.gb_count.TabStop = False
    Me.gb_count.Text = "xCount"
    '
    'ef_document
    '
    Me.ef_document.DoubleValue = 0.0R
    Me.ef_document.IntegerValue = 0
    Me.ef_document.IsReadOnly = False
    Me.ef_document.Location = New System.Drawing.Point(6, 80)
    Me.ef_document.Name = "ef_document"
    Me.ef_document.PlaceHolder = Nothing
    Me.ef_document.Size = New System.Drawing.Size(260, 24)
    Me.ef_document.SufixText = "Sufix Text"
    Me.ef_document.SufixTextVisible = True
    Me.ef_document.TabIndex = 2
    Me.ef_document.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_document.TextValue = ""
    Me.ef_document.Value = ""
    Me.ef_document.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_cage_safe_keeping_movement_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1107, 532)
    Me.Name = "frm_cage_safe_keeping_movement_sel"
    Me.Text = "frm_cage_safe_keeping_movement_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.gb_count.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_name_account As GUI_Controls.uc_entry_field
  Friend WithEvents ef_id As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents opt_cage_session As System.Windows.Forms.RadioButton
  Friend WithEvents opt_operation As System.Windows.Forms.RadioButton
  Friend WithEvents gb_count As System.Windows.Forms.GroupBox
  Friend WithEvents ef_document As GUI_Controls.uc_entry_field
End Class
