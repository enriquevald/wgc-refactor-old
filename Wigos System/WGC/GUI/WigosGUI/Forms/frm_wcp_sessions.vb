'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_wcp_sessions
' DESCRIPTION:   This screen allows to view the sessions between:
'                           - two dates 
'                           - for all terminals or one terminal
'                           - for all status or one status
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 13-APR-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-APR-2007  MTM    Initial version
' 01-FEB-2012  RCI & MPO    Routine TimeSpanToString() is now in module mdl_misc.vb.
'                           Also use WGDB.Now, not Date.Now.
' 18-NOV-2013  LJM    Changes for auditing the form.
' 06-MAR-2014  JAB    Update "GUI_ReportUpdateFilters" function to prevent exception if "m_terminals" is nothing.
' 30-SEP-2014  LEM    Fixed Bug WIG-1340: Option "Show terminal location" do not work correctly.
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 07-MAR-2018  RGR    Bug 31840:WIGOS-3642 Error in list and Machine selection criterion in Monitor - Sessions screen 
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_wcp_sessions
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_several_status As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_status As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_status As GUI_Controls.uc_combo
  Friend WithEvents cb_timer As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents cb_last_session As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents tmr_refresh As System.Windows.Forms.Timer
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_wcp_sessions))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.opt_several_status = New System.Windows.Forms.RadioButton()
    Me.opt_all_status = New System.Windows.Forms.RadioButton()
    Me.cmb_status = New GUI_Controls.uc_combo()
    Me.cb_timer = New System.Windows.Forms.CheckBox()
    Me.tmr_refresh = New System.Windows.Forms.Timer(Me.components)
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.cb_last_session = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.cb_last_session)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.cb_timer)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(962, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_timer, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_last_session, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(962, 514)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(956, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(956, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(252, 72)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_several_status)
    Me.gb_status.Controls.Add(Me.opt_all_status)
    Me.gb_status.Controls.Add(Me.cmb_status)
    Me.gb_status.Location = New System.Drawing.Point(596, 5)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(237, 72)
    Me.gb_status.TabIndex = 3
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'opt_several_status
    '
    Me.opt_several_status.Location = New System.Drawing.Point(8, 14)
    Me.opt_several_status.Name = "opt_several_status"
    Me.opt_several_status.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_status.TabIndex = 0
    Me.opt_several_status.Text = "xOne"
    '
    'opt_all_status
    '
    Me.opt_all_status.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_status.Name = "opt_all_status"
    Me.opt_all_status.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_status.TabIndex = 1
    Me.opt_all_status.Text = "xAll"
    '
    'cmb_status
    '
    Me.cmb_status.AllowUnlistedValues = False
    Me.cmb_status.AutoCompleteMode = False
    Me.cmb_status.IsReadOnly = False
    Me.cmb_status.Location = New System.Drawing.Point(80, 14)
    Me.cmb_status.Name = "cmb_status"
    Me.cmb_status.SelectedIndex = -1
    Me.cmb_status.Size = New System.Drawing.Size(148, 24)
    Me.cmb_status.SufixText = "Sufix Text"
    Me.cmb_status.SufixTextVisible = True
    Me.cmb_status.TabIndex = 2
    Me.cmb_status.TextCombo = Nothing
    Me.cmb_status.TextVisible = False
    Me.cmb_status.TextWidth = 0
    '
    'cb_timer
    '
    Me.cb_timer.AutoSize = True
    Me.cb_timer.Location = New System.Drawing.Point(68, 95)
    Me.cb_timer.Name = "cb_timer"
    Me.cb_timer.Size = New System.Drawing.Size(66, 17)
    Me.cb_timer.TabIndex = 1
    Me.cb_timer.Text = "xTimer"
    Me.cb_timer.UseVisualStyleBackColor = True
    '
    'tmr_refresh
    '
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(259, 2)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'cb_last_session
    '
    Me.cb_last_session.AutoSize = True
    Me.cb_last_session.Checked = True
    Me.cb_last_session.CheckState = System.Windows.Forms.CheckState.Checked
    Me.cb_last_session.Location = New System.Drawing.Point(604, 83)
    Me.cb_last_session.Name = "cb_last_session"
    Me.cb_last_session.Size = New System.Drawing.Size(98, 17)
    Me.cb_last_session.TabIndex = 4
    Me.cb_last_session.Text = "xMostRecent"
    Me.cb_last_session.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(604, 115)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(221, 17)
    Me.chk_terminal_location.TabIndex = 5
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_wcp_sessions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(970, 712)
    Me.Name = "frm_wcp_sessions"
    Me.Text = "frm_plays_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS As Int32

  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_SERVER_NAME As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 3
  Private Const SQL_COLUMN_STARTED As Integer = 4
  Private Const SQL_COLUMN_STATUS As Integer = 5
  Private Const SQL_COLUMN_FINISHED As Integer = 6
  Private Const SQL_COLUMN_LAST_MESSAGE As Integer = 7
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 8
  Private Const SQL_COLUMN_SERVER_ID As Integer = 9

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_SESSION_ID As Integer
  Private GRID_COLUMN_SERVER_NAME As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_STARTED As Integer
  Private GRID_COLUMN_STATUS As Integer
  Private GRID_COLUMN_TIME As Integer
  Private GRID_COLUMN_LAST_MESSAGE As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 1

  'Width
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TIME As Integer = 1300
  Private Const GRID_WIDTH_STATUS As Integer = 1500
  Private Const GRID_WIDTH_TERMINAL As Integer = 2290

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean = False

  ' For report filters 
  Private m_terminals As String = ""
  Private m_date_from As String = ""
  Private m_date_to As String = ""
  Private m_status As String = ""
  Private m_refreshing_grid_filter = ""

  Private bl_where As Boolean = True

  Private m_first_time_timer_checked As Boolean
  Private m_first_time_timer_nodatafound As Boolean

  Private m_num_of_refreshes As Integer = 0
  Private m_refresh_grid As Boolean = False

  Private m_last_session As String
  Private m_terminal_location As String

#End Region ' Members

#Region " Overrides "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_WS_SESSIONS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------ 
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(355)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(381)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypesExcept3GS())

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_STATISTICS.GetString(394)
    Me.opt_several_status.Text = GLB_NLS_GUI_ALARMS.GetString(276)
    Me.opt_all_status.Text = GLB_NLS_GUI_ALARMS.GetString(277)

    ' Timer
    Me.cb_timer.Text = GLB_NLS_GUI_STATISTICS.GetString(415)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Status
    Me.cmb_status.Add(0, GLB_NLS_GUI_STATISTICS.GetString(400))
    Me.cmb_status.Add(1, GLB_NLS_GUI_STATISTICS.GetString(401))
    Me.cmb_status.Add(2, GLB_NLS_GUI_STATISTICS.GetString(402))
    Me.cmb_status.Add(3, GLB_NLS_GUI_STATISTICS.GetString(403))
    Me.cmb_status.Add(4, GLB_NLS_GUI_STATISTICS.GetString(405))

    Me.cmb_status.Enabled = False

    Me.cb_last_session.Text = GLB_NLS_GUI_STATISTICS.GetString(423)


  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub ' GUI_BeforeFirsRow

  Protected Overrides Sub GUI_AfterLastRow()

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        If Not cb_timer.Checked Or m_first_time_timer_checked Then
          m_first_time_timer_checked = False
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        End If
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: What to do when no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  '
  Protected Overrides Sub GUI_NoDataFound()
    If Not cb_timer.Checked Or m_first_time_timer_nodatafound Then
      m_first_time_timer_nodatafound = False
      Call MyBase.GUI_NoDataFound()
    End If
  End Sub ' GUI_NoDataFound

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    ' Get Select and from
    str_sql = "SELECT " & _
                  "WS_SESSION_ID," & _
                  "(SELECT TE_NAME FROM TERMINALS WHERE " & _
                        "TE_TERMINAL_ID IN " & _
                                "(SELECT TE_SERVER_ID FROM TERMINALS WHERE TE_TERMINAL_ID=WS_TERMINAL_ID )) AS SERV," & _
                  "(SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID=WS_TERMINAL_ID) AS TERMINAL," & _
                  "(SELECT TE_TYPE FROM TERMINALS WHERE TE_TERMINAL_ID=WS_TERMINAL_ID) AS TYPE," & _
                  "WS_STARTED," & _
                  "WS_STATUS," & _
                  "WS_FINISHED," & _
                  "WS_LAST_RCVD_MSG, " & _
                  "WS_TERMINAL_ID " & _
              "FROM WCP_SESSIONS "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY WS_SESSION_ID DESC"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS: True (the row should be added)
  '          False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim time As TimeSpan = Nothing
    Dim date_now As Object
    Dim status As Integer
    Dim str_status As String = ""

    Dim _terminal_data As List(Of Object)

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

    ' Server Name
    If Not DbRow.IsNull(SQL_COLUMN_SERVER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVER_NAME).Value = DbRow.Value(SQL_COLUMN_SERVER_NAME)
    Else
      ' RRT 10-DEC-2008 - Terminals without an associated server goes to terminal column.
      If DbRow.Value(SQL_COLUMN_TERMINAL_TYPE) = 0 AndAlso Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SERVER_NAME).Value = TerminalReport.GetReportData(DbRow.Value(SQL_COLUMN_TERMINAL_ID), ReportColumn.Terminal)
      End If
    End If

    'Terminal Report
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next
    End If

    ' Started
    If Not DbRow.IsNull(SQL_COLUMN_STARTED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STARTED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_STARTED), _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STARTED).Value = ""
    End If

    ' Status
    status = DbRow.Value(SQL_COLUMN_STATUS)
    Select Case status
      Case 0
        str_status = GLB_NLS_GUI_STATISTICS.GetString(400)
      Case 1
        str_status = GLB_NLS_GUI_STATISTICS.GetString(401)
      Case 2
        str_status = GLB_NLS_GUI_STATISTICS.GetString(402)
      Case 3
        str_status = GLB_NLS_GUI_STATISTICS.GetString(403)
      Case 4
        str_status = GLB_NLS_GUI_STATISTICS.GetString(405)
    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = str_status

    ' Time
    If Not DbRow.IsNull(SQL_COLUMN_FINISHED) Then

      If Not DbRow.IsNull(SQL_COLUMN_STARTED) Then

        time = DbRow.Value(SQL_COLUMN_FINISHED) - DbRow.Value(SQL_COLUMN_STARTED)

      End If

    Else
      If status = 0 Then
        date_now = WSI.Common.WGDB.Now
        time = date_now - DbRow.Value(SQL_COLUMN_STARTED)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME).Value = ""
      End If
    End If

    If time.TotalSeconds <> 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME).Value = TimeSpanToString(time)
    End If

    ' Last Message
    If Not DbRow.IsNull(SQL_COLUMN_LAST_MESSAGE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_MESSAGE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_LAST_MESSAGE), _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_MESSAGE).Value = ""
    End If


    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_GRID_INFO
        Call GUI_ShowSelectedItem()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(415), m_refreshing_grid_filter)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(394), m_status)
    PrintData.SetFilter(cb_last_session.Text, m_last_session)
    PrintData.SetFilter(chk_terminal_location.Text, m_terminal_location)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_STATISTICS.GetString(358)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _terminals As String = m_terminals
    Dim _date_from As String = m_date_from
    Dim _date_to As String = m_date_to
    Dim _status As String = m_status
    Dim _refresh As Boolean = m_refreshing_grid

    m_terminals = ""
    m_date_from = ""
    m_date_to = ""
    m_status = ""

    If _terminals Is Nothing Then
      _terminals = ""
    End If

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Status
    If Me.opt_all_status.Checked Then
      m_status = Me.opt_all_status.Text
    Else
      m_status = Me.cmb_status.TextValue
    End If

    If Me.cb_timer.Checked Then
      m_refreshing_grid_filter = GLB_NLS_GUI_CONTROLS.GetString(3)
    Else
      m_refreshing_grid_filter = GLB_NLS_GUI_CONTROLS.GetString(4)
    End If

    m_refreshing_grid = Me.cb_timer.Checked

    If Not _terminals.Equals(m_terminals) _
       Or Not _date_from.Equals(m_date_from) _
       Or Not _date_to.Equals(m_date_to) _
       Or Not _status.Equals(m_status) _
       Or Not _refresh.Equals(m_refreshing_grid) Then
      m_num_of_refreshes = 0
    Else
      Me.m_num_of_refreshes = Me.m_num_of_refreshes + 1
    End If

    m_last_session = IIf(Me.cb_last_session.Checked, GLB_NLS_GUI_CONTROLS.GetString(3), GLB_NLS_GUI_CONTROLS.GetString(4))
    m_terminal_location = IIf(Me.chk_terminal_location.Checked, GLB_NLS_GUI_CONTROLS.GetString(3), GLB_NLS_GUI_CONTROLS.GetString(4))

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean
    Select Case AuditType
      Case AUDIT_FLAGS.SEARCH
        Return Me.m_refreshing_grid.Equals(False) Or (Me.m_refreshing_grid.Equals(True) And Me.m_num_of_refreshes < 1)
      Case Else
        Return MyBase.GUI_HasToBeAudited(AuditType)
    End Select
  End Function

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    If Not GridColumnReIndex() Then
      Return
    End If

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      '  Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0

      '  Server Name
      .Column(GRID_COLUMN_SERVER_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(413)
      .Column(GRID_COLUMN_SERVER_NAME).Width = GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_SERVER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      '  Started date
      .Column(GRID_COLUMN_STARTED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(381)
      .Column(GRID_COLUMN_STARTED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_STARTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(394)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Time
      .Column(GRID_COLUMN_TIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(382)
      .Column(GRID_COLUMN_TIME).Width = GRID_WIDTH_TIME
      .Column(GRID_COLUMN_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Last message
      .Column(GRID_COLUMN_LAST_MESSAGE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(414)
      .Column(GRID_COLUMN_LAST_MESSAGE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_LAST_MESSAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_from.Checked = False

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.cmb_status.Enabled = False

    Me.opt_all_status.Checked = True
    Me.cb_timer.Checked = False
    m_first_time_timer_checked = False
    m_first_time_timer_nodatafound = False

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim str_where As String = ""
    Dim str_terminal_id As String

    ' Filter Providers - Terminals
    str_terminal_id = " WS_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    If (Me.cb_last_session.Checked) Then
      str_where = " WHERE WS_SESSION_ID IN (SELECT   MAX(WS_SESSION_ID) " & _
                  "                           FROM   WCP_SESSIONS " & _
                  "                          WHERE   " & str_terminal_id & _
                  "                         GROUP BY WS_TERMINAL_ID)"
    Else
      str_where = " WHERE " & str_terminal_id
    End If

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (WS_STARTED >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
      Me.bl_where = False
    End If

    If Me.dtp_to.Checked = True Then

      str_where = str_where & " AND (WS_STARTED < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    ' Filter Status
    If Me.opt_several_status.Checked = True Then
      str_where = str_where & " AND WS_STATUS = " & Me.cmb_status.Value
    End If

    Return str_where
  End Function ' GetSqlWhere

  Private Function GridColumnReIndex() As Boolean
    Dim _column_count As Int32

    _column_count = TerminalReport.NumColumns(m_terminal_report_type)

    If _column_count <> TERMINAL_DATA_COLUMNS Then
      TERMINAL_DATA_COLUMNS = _column_count

      GRID_COLUMN_INDEX = 0
      GRID_COLUMN_SESSION_ID = 1
      GRID_COLUMN_SERVER_NAME = 2
      GRID_INIT_TERMINAL_DATA = 3
      GRID_COLUMN_STARTED = TERMINAL_DATA_COLUMNS + 3
      GRID_COLUMN_STATUS = TERMINAL_DATA_COLUMNS + 4
      GRID_COLUMN_TIME = TERMINAL_DATA_COLUMNS + 5
      GRID_COLUMN_LAST_MESSAGE = TERMINAL_DATA_COLUMNS + 6

      GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 7

      Return True
    End If

    Return False

  End Function

#End Region  ' Private Functions

#Region "Events"

  Private Sub opt_several_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_status.Click
    Me.cmb_status.Enabled = True
  End Sub

  Private Sub opt_all_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_status.Click
    Me.cmb_status.Enabled = False
  End Sub

  Private Sub tmr_refresh_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmr_refresh.Tick
    ' OK

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    tmr_refresh.Interval = 5000
  End Sub

  Private Sub cb_timer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_timer.CheckedChanged
    If cb_timer.Checked = True Then
      m_first_time_timer_checked = True
      m_first_time_timer_nodatafound = True
      tmr_refresh.Start()
    Else
      tmr_refresh.Stop()
    End If
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

End Class
