'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpot_history.vb
'
' DESCRIPTION:   Jackpot History selection form
'
' AUTHOR:        Armando Alva
'
' CREATION DATE: 02-SEP-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-SEP-2008  AAV    Initial Draft.
' 11-MAY-2012  JCM    Fixed Bug #136: Error in Jackpot monitor when DB connection fails
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_jackpot_monitor
  Inherits frm_base

#Region "Constants"

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_INDEX = 0
  Private Const GRID_COLUMN_NAME = 1
  Private Const GRID_COLUMN_ACCUMULATED = 2

  Private Const SQL_COLUMN_INDEX = 0
  Private Const SQL_COLUMN_NAME = 1
  Private Const SQL_COLUMN_CURRENT_AMOUNT = 2
  Private Const SQL_COLUMN_COMPENSATION_AMOUNT = 3
  Private Const SQL_COLUMN_SECOND_AMOUNT = 4


#End Region

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CLASS_II_JACKPOT_MONITOR

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(478)

    '- Buttons
    btn_exit.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(452)

    ' - Labels
    '   - Status
    lbl_status.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(215)
    '   - Contribution from bet
    lbl_jackpot_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(479)

    ' Total labels
    Me.lbl_accumulated_total.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(335)
    Me.lbl_to_compensate.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(336)

    Me.Timer1.Interval = 1000

    GUI_StyleSheet()

    Me.RefreshGrid()

    Me.Timer1.Start()

  End Sub ' GUI_InitControls


  ' PURPOSE: Close form.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent

#End Region

#Region "Private Functions"

  ' PURPOSE: Grid initial settings.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshGrid()

    Dim data_table As DataTable
    Dim RowIndex As Integer
    Dim data_row As DataRow
    Dim total_accumulated As Double
    Dim total_to_compensate As Double

    total_accumulated = 0
    total_to_compensate = 0

    Try

      data_table = GUI_GetTableUsingSQL(GetSqlQuery(), 5000)

      If (data_table.Rows.Count <> Me.Grid.NumRows) Then
        Me.Grid.Clear()
      End If

      RowIndex = 0
      For Each data_row In data_table.Rows

        If (RowIndex >= Me.Grid.NumRows) Then
          Me.Grid.AddRow()
        End If

        Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).Value = data_row.Item("C2JC_INDEX")
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = data_row.Item("C2JI_NAME")
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCUMULATED).Value = GUI_FormatCurrency(data_row.Item("C2JC_ACCUMULATED"))
        total_accumulated += Math.Round(data_row.Item("C2JC_ACCUMULATED"), 2, MidpointRounding.AwayFromZero)
        total_to_compensate += Math.Round(data_row.Item("C2JC_TO_COMPENSATE"), 2, MidpointRounding.AwayFromZero)
        RowIndex += 1

      Next

      'SetRefreshTime()

      lbl_status.Value = SetJackpotStatus()

      lbl_jackpot_contribution.Value = SetJackpotContribution()

      Me.lbl_accumulated_total.Value = GUI_FormatCurrency(total_accumulated)

      ' To Compensate
      If total_to_compensate < 0 Then
        Me.lbl_to_compensate.Value = GUI_FormatCurrency(Math.Abs(total_to_compensate))
        Me.lbl_to_compensate.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(337)
        Me.lbl_to_compensate.LabelForeColor = System.Drawing.Color.Blue
      Else
        Me.lbl_to_compensate.Value = GUI_FormatCurrency(total_to_compensate)
        Me.lbl_to_compensate.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(336)
        Me.lbl_to_compensate.LabelForeColor = System.Drawing.Color.Red
      End If

    Catch
    End Try
  End Sub

  ' PURPOSE: Obtain the jackpot status.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Function SetJackpotStatus() As String
    Dim str_status As String = ""
    Dim sql_str As String
    Dim data_table As DataTable

    sql_str = " SELECT C2JP_ENABLED FROM C2_JACKPOT_PARAMETERS "

    data_table = GUI_GetTableUsingSQL(sql_str, 1)

    If (data_table.Rows(0).Item("C2JP_ENABLED") = 0) Then
      str_status = GLB_NLS_GUI_JACKPOT_MGR.GetString(487) ' Disabled
    Else
      str_status = GLB_NLS_GUI_JACKPOT_MGR.GetString(486) ' Enabled
    End If

    Return str_status
  End Function

  ' PURPOSE: Obtain the jackpot contribution percentage.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Function SetJackpotContribution() As String
    Dim str_status As String = ""
    Dim sql_str As String
    Dim data_table As DataTable

    sql_str = " SELECT C2JP_CONTRIBUTION_PCT FROM C2_JACKPOT_PARAMETERS "

    data_table = GUI_GetTableUsingSQL(sql_str, 1)

    Return GUI_FormatNumber(data_table.Rows(0).Item("C2JP_CONTRIBUTION_PCT"), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 3) & " %"
  End Function

  ' PURPOSE: Return the general query.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Function GetSqlQuery() As String
    Dim sql_str As String

    sql_str = " SELECT   C2JC_INDEX " & _
              "        , C2JI_NAME  " & _
              "        , C2JC_ACCUMULATED " & _
              "        , C2JC_TO_COMPENSATE " & _
              "        , C2JI_MAXIMUM " & _
              "   FROM   C2_JACKPOT_COUNTERS " & _
              "        , C2_JACKPOT_INSTANCES " & _
              "  WHERE   C2JC_INDEX = C2JI_INDEX "

    Return sql_str
  End Function


  ' PURPOSE: Grid initial settings.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      ' Header Initialization

      'Index
      .Column(GRID_COLUMN_INDEX).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_INDEX).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(457)
      .Column(GRID_COLUMN_INDEX).Width = 800
      .Column(GRID_COLUMN_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Name
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(214)
      .Column(GRID_COLUMN_NAME).Width = 1400
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Accumulated (Amount)
      .Column(GRID_COLUMN_ACCUMULATED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_ACCUMULATED).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(234)
      .Column(GRID_COLUMN_ACCUMULATED).Width = 2400
      .Column(GRID_COLUMN_ACCUMULATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

  End Sub 'GUI_StyleSheet

#End Region

#Region "Event"

  ' PURPOSE: Timer operation to refresh the grid.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Me.RefreshGrid()
  End Sub

#End Region

#Region "Public Functions"

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub

#End Region

End Class