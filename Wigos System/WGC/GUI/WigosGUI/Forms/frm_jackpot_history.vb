'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_jackpot_history.vb
' DESCRIPTION:   Jackpot History selection form
' AUTHOR:        Armando Alva
' CREATION DATE: 02-SEP-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-SEP-2008  AAV    Initial Draft.
' 27-JAN-2012  RCI    Remove the button "Selection". Draw Class II is not used now.
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports WSI.Common
Imports System.Threading
Imports System.Data
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_jackpot_history
  Inherits frm_base_sel

#Region "Constants"
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_INTERNAL_ID As Integer = 0 ' temp

  Private GRID_COLUMN_DATE_AWARDED As Integer
  Private GRID_COLUMN_NAME As Integer
  Private GRID_COLUMN_AMOUNT As Integer
  Private GRID_COLUMN_STATUS As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_GAME_NAME As Integer
  Private GRID_COLUMN_PLAY_ID As Integer

  Private Const SQL_COLUMN_INDEX As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_DATE_AWARDED As Integer = 4
  Private Const SQL_COLUMN_PLAY_ID As Integer = 5
  Private Const SQL_TERMINAL_ID As Integer = 6
  Private Const SQL_TERMINAL_NAME As Integer = 7
  Private Const SQL_COLUMN_GAME_ID As Integer = 8
  Private Const SQL_COLUMN_GAME_NAME As Integer = 9

  Private Const TOTAL_JACKPOT_INSTANCES As Integer = 3

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_jackpot_type As String

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region "Overrides"

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(206)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_JACKPOT_MGR.Id(207))

    gb_jackpot_type.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(210)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CLASS_II_JACKPOT_HISTORY

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT C2JH_INDEX " & _
              "     , C2JH_NAME " & _
              "     , C2JH_STATUS " & _
              "     , C2JH_AMOUNT " & _
              "     , C2JH_AWARDED " & _
              "     , C2JH_PLAY_ID " & _
              "     , C2JH_TERMINAL_ID " & _
              "     , C2JH_TERMINAL_NAME " & _
              "     , C2JH_GAME_ID " & _
              "     , C2JH_GAME_NAME " & _
              "  FROM C2_JACKPOT_HISTORY "

    str_sql = str_sql & GetSqlWhere()

    str_sql = str_sql & " ORDER BY C2JH_AWARDED DESC "

    Return str_sql
  End Function ' GUI_FilterGetSqlQuery

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    If DbRow.Value(SQL_COLUMN_STATUS) = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_JACKPOT_MGR.GetString(216)
      'sle 08/01/09 "Awarded" ya existia el mensaje
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = DbRow.Value(SQL_COLUMN_STATUS)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_AWARDED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_AWARDED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_GAME_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAY_ID).Value = DbRow.Value(SQL_COLUMN_PLAY_ID)

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    Return True
  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(208), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(209), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(210), m_jackpot_type)

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_JACKPOT_MGR.GetString(206)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_jackpot_type = ""

    ' Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.chk_jackpot1.Checked And Me.chk_jackpot2.Checked And Me.chk_jackpot3.Checked Then
      m_jackpot_type = GLB_NLS_GUI_JACKPOT_MGR.GetString(201)
    Else
      If Me.chk_jackpot1.Checked Then
        m_jackpot_type = chk_jackpot1.Text
      End If

      If Me.chk_jackpot2.Checked Then
        If m_jackpot_type.Length > 0 Then
          m_jackpot_type = m_jackpot_type & ", " & chk_jackpot2.Text
        Else
          m_jackpot_type = chk_jackpot2.Text
        End If
      End If

      If Me.chk_jackpot3.Checked Then
        If m_jackpot_type.Length > 0 Then
          m_jackpot_type = m_jackpot_type & ", " & chk_jackpot3.Text
        Else
          m_jackpot_type = chk_jackpot3.Text
        End If
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region "Public Functions"

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region "Private Functions"

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = wgdb.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_jackpot1.Checked = True
    Me.chk_jackpot2.Checked = True
    Me.chk_jackpot3.Checked = True

    Me.chk_terminal_location.Checked = False

    SetJackpotValues()

  End Sub ' SetDefaultValues

  Private Sub SetJackpotValues()

    Dim str_sql As String
    Dim data_table As DataTable
    Dim idx As Integer = 0

    str_sql = "SELECT C2JI_INDEX, C2JI_NAME FROM C2_JACKPOT_INSTANCES"

    data_table = GUI_GetTableUsingSQL(str_sql, TOTAL_JACKPOT_INSTANCES)

    chk_jackpot1.Text = data_table.Rows(0).Item(1)
    chk_jackpot2.Text = data_table.Rows(1).Item(1)
    chk_jackpot3.Text = data_table.Rows(2).Item(1)

    chk_jackpot1.Tag = data_table.Rows(0).Item(0)
    chk_jackpot2.Tag = data_table.Rows(1).Item(0)
    chk_jackpot3.Tag = data_table.Rows(2).Item(0)

  End Sub ' SetJackpotValues

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      ' Header Initialization

      ' Date awarded
      .Column(GRID_COLUMN_DATE_AWARDED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_DATE_AWARDED).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(203)
      .Column(GRID_COLUMN_DATE_AWARDED).Width = 2000
      .Column(GRID_COLUMN_DATE_AWARDED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Jackpot Type
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(214)
      .Column(GRID_COLUMN_NAME).Width = 1400
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Amount
      .Column(GRID_COLUMN_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(211)
      .Column(GRID_COLUMN_AMOUNT).Width = 1800
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Status
      .Column(GRID_COLUMN_STATUS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(215)
      .Column(GRID_COLUMN_STATUS).Width = 1400
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      ' Game
      .Column(GRID_COLUMN_GAME_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_GAME_NAME).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(213)
      .Column(GRID_COLUMN_GAME_NAME).Width = 3200
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Play Id
      .Column(GRID_COLUMN_PLAY_ID).Width = 0
    End With

  End Sub ' GUI_StyleSheet

  Private Function GetSqlWhere() As String

    Dim sql_where As String
    Dim isfirst As Boolean = True

    sql_where = ""

    sql_where = Me.uc_dsl.GetSqlFilterCondition("C2JH_AWARDED")
    If sql_where.Length > 0 Then
      sql_where = " AND " & sql_where
    End If

    If Me.chk_jackpot1.Checked = True Then
      If isfirst Then
        sql_where = sql_where & " AND ( C2JH_INDEX = " & CStr(chk_jackpot1.Tag) & " "
        isfirst = False
      Else
        sql_where = sql_where & " OR C2JH_INDEX = " & CStr(chk_jackpot1.Tag) & " "
      End If
    End If

    If Me.chk_jackpot2.Checked = True Then
      If isfirst Then
        sql_where = sql_where & " AND ( C2JH_INDEX = " & CStr(chk_jackpot2.Tag) & " "
        isfirst = False
      Else
        sql_where = sql_where & " OR C2JH_INDEX = " & CStr(chk_jackpot2.Tag) & " "
      End If
    End If

    If Me.chk_jackpot3.Checked = True Then
      If isfirst Then
        sql_where = sql_where & " AND ( C2JH_INDEX = " & CStr(chk_jackpot3.Tag) & " "
        isfirst = False
      Else
        sql_where = sql_where & " OR C2JH_INDEX = " & CStr(chk_jackpot3.Tag) & " "
      End If
    End If

    If isfirst = False Then
      sql_where = sql_where & ") "
    End If

    sql_where = " WHERE C2JH_INDEX IS NOT NULL " & sql_where & " "

    Return sql_where
  End Function ' GetSqlWhere

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_DATE_AWARDED = 0
    GRID_COLUMN_NAME = 1
    GRID_COLUMN_AMOUNT = 2
    GRID_COLUMN_STATUS = 3
    GRID_INIT_TERMINAL_DATA = 4
    GRID_COLUMN_GAME_NAME = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_PLAY_ID = TERMINAL_DATA_COLUMNS + 5

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 6
  End Sub

#End Region ' Private Functions

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region

End Class