<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gift_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_gift_status = New System.Windows.Forms.GroupBox
    Me.chk_status_not_available = New System.Windows.Forms.CheckBox
    Me.chk_status_available = New System.Windows.Forms.CheckBox
    Me.ef_gift_name = New GUI_Controls.uc_entry_field
    Me.gb_points = New System.Windows.Forms.GroupBox
    Me.ef_points_to = New GUI_Controls.uc_entry_field
    Me.ef_points_from = New GUI_Controls.uc_entry_field
    Me.gb_gift_type = New System.Windows.Forms.GroupBox
    Me.chk_type_redeem_credit = New System.Windows.Forms.CheckBox
    Me.chk_type_services = New System.Windows.Forms.CheckBox
    Me.chk_type_nrc = New System.Windows.Forms.CheckBox
    Me.chk_type_object = New System.Windows.Forms.CheckBox
    Me.gb_stock = New System.Windows.Forms.GroupBox
    Me.ef_stock_to = New GUI_Controls.uc_entry_field
    Me.ef_stock_from = New GUI_Controls.uc_entry_field
    Me.lbl_legend_01 = New System.Windows.Forms.Label
    Me.lbl_legend_02 = New System.Windows.Forms.Label
    Me.ef_redemption_table_name = New GUI_Controls.uc_entry_field
    Me.chk_only_vip = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_gift_status.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.gb_gift_type.SuspendLayout()
    Me.gb_stock.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_only_vip)
    Me.panel_filter.Controls.Add(Me.ef_gift_name)
    Me.panel_filter.Controls.Add(Me.ef_redemption_table_name)
    Me.panel_filter.Controls.Add(Me.lbl_legend_02)
    Me.panel_filter.Controls.Add(Me.gb_points)
    Me.panel_filter.Controls.Add(Me.lbl_legend_01)
    Me.panel_filter.Controls.Add(Me.gb_gift_type)
    Me.panel_filter.Controls.Add(Me.gb_stock)
    Me.panel_filter.Controls.Add(Me.gb_gift_status)
    Me.panel_filter.Size = New System.Drawing.Size(1219, 194)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gift_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_stock, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gift_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_legend_01, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_points, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_legend_02, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_redemption_table_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_gift_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_vip, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 198)
    Me.panel_data.Size = New System.Drawing.Size(1219, 361)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1213, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1213, 4)
    '
    'gb_gift_status
    '
    Me.gb_gift_status.Controls.Add(Me.chk_status_not_available)
    Me.gb_gift_status.Controls.Add(Me.chk_status_available)
    Me.gb_gift_status.Location = New System.Drawing.Point(249, 77)
    Me.gb_gift_status.Name = "gb_gift_status"
    Me.gb_gift_status.Size = New System.Drawing.Size(166, 72)
    Me.gb_gift_status.TabIndex = 4
    Me.gb_gift_status.TabStop = False
    Me.gb_gift_status.Text = "xStatus"
    '
    'chk_status_not_available
    '
    Me.chk_status_not_available.AutoSize = True
    Me.chk_status_not_available.Location = New System.Drawing.Point(19, 45)
    Me.chk_status_not_available.Name = "chk_status_not_available"
    Me.chk_status_not_available.Size = New System.Drawing.Size(108, 17)
    Me.chk_status_not_available.TabIndex = 1
    Me.chk_status_not_available.Text = "xNot Available"
    Me.chk_status_not_available.UseVisualStyleBackColor = True
    '
    'chk_status_available
    '
    Me.chk_status_available.AutoSize = True
    Me.chk_status_available.Location = New System.Drawing.Point(19, 21)
    Me.chk_status_available.Name = "chk_status_available"
    Me.chk_status_available.Size = New System.Drawing.Size(85, 17)
    Me.chk_status_available.TabIndex = 0
    Me.chk_status_available.Text = "xAvailable"
    Me.chk_status_available.UseVisualStyleBackColor = True
    '
    'ef_gift_name
    '
    Me.ef_gift_name.DoubleValue = 0
    Me.ef_gift_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_gift_name.IntegerValue = 0
    Me.ef_gift_name.IsReadOnly = False
    Me.ef_gift_name.Location = New System.Drawing.Point(41, 46)
    Me.ef_gift_name.Name = "ef_gift_name"
    Me.ef_gift_name.Size = New System.Drawing.Size(190, 25)
    Me.ef_gift_name.SufixText = "Sufix Text"
    Me.ef_gift_name.SufixTextVisible = True
    Me.ef_gift_name.TabIndex = 1
    Me.ef_gift_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_gift_name.TextValue = ""
    Me.ef_gift_name.TextWidth = 69
    Me.ef_gift_name.Value = ""
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.ef_points_to)
    Me.gb_points.Controls.Add(Me.ef_points_from)
    Me.gb_points.Location = New System.Drawing.Point(249, 3)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(166, 72)
    Me.gb_points.TabIndex = 2
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPoints"
    '
    'ef_points_to
    '
    Me.ef_points_to.DoubleValue = 0
    Me.ef_points_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_to.IntegerValue = 0
    Me.ef_points_to.IsReadOnly = False
    Me.ef_points_to.Location = New System.Drawing.Point(6, 43)
    Me.ef_points_to.Name = "ef_points_to"
    Me.ef_points_to.Size = New System.Drawing.Size(149, 25)
    Me.ef_points_to.SufixText = "Sufix Text"
    Me.ef_points_to.SufixTextVisible = True
    Me.ef_points_to.TabIndex = 1
    Me.ef_points_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_to.TextValue = ""
    Me.ef_points_to.TextWidth = 60
    Me.ef_points_to.Value = ""
    '
    'ef_points_from
    '
    Me.ef_points_from.DoubleValue = 0
    Me.ef_points_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_from.IntegerValue = 0
    Me.ef_points_from.IsReadOnly = False
    Me.ef_points_from.Location = New System.Drawing.Point(6, 16)
    Me.ef_points_from.Name = "ef_points_from"
    Me.ef_points_from.Size = New System.Drawing.Size(149, 25)
    Me.ef_points_from.SufixText = "Sufix Text"
    Me.ef_points_from.SufixTextVisible = True
    Me.ef_points_from.TabIndex = 0
    Me.ef_points_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_points_from.TextValue = ""
    Me.ef_points_from.TextWidth = 60
    Me.ef_points_from.Value = ""
    '
    'gb_gift_type
    '
    Me.gb_gift_type.Controls.Add(Me.chk_type_redeem_credit)
    Me.gb_gift_type.Controls.Add(Me.chk_type_services)
    Me.gb_gift_type.Controls.Add(Me.chk_type_nrc)
    Me.gb_gift_type.Controls.Add(Me.chk_type_object)
    Me.gb_gift_type.Location = New System.Drawing.Point(6, 77)
    Me.gb_gift_type.Name = "gb_gift_type"
    Me.gb_gift_type.Size = New System.Drawing.Size(237, 115)
    Me.gb_gift_type.TabIndex = 3
    Me.gb_gift_type.TabStop = False
    Me.gb_gift_type.Text = "xType"
    '
    'chk_type_redeem_credit
    '
    Me.chk_type_redeem_credit.AutoSize = True
    Me.chk_type_redeem_credit.Location = New System.Drawing.Point(19, 66)
    Me.chk_type_redeem_credit.Name = "chk_type_redeem_credit"
    Me.chk_type_redeem_credit.Size = New System.Drawing.Size(149, 17)
    Me.chk_type_redeem_credit.TabIndex = 2
    Me.chk_type_redeem_credit.Text = "xRedeemable Credits"
    Me.chk_type_redeem_credit.UseVisualStyleBackColor = True
    '
    'chk_type_services
    '
    Me.chk_type_services.AutoSize = True
    Me.chk_type_services.Location = New System.Drawing.Point(19, 89)
    Me.chk_type_services.Name = "chk_type_services"
    Me.chk_type_services.Size = New System.Drawing.Size(82, 17)
    Me.chk_type_services.TabIndex = 3
    Me.chk_type_services.Text = "xServices"
    Me.chk_type_services.UseVisualStyleBackColor = True
    '
    'chk_type_nrc
    '
    Me.chk_type_nrc.AutoSize = True
    Me.chk_type_nrc.Location = New System.Drawing.Point(19, 43)
    Me.chk_type_nrc.Name = "chk_type_nrc"
    Me.chk_type_nrc.Size = New System.Drawing.Size(172, 17)
    Me.chk_type_nrc.TabIndex = 1
    Me.chk_type_nrc.Text = "xNot Redeemable Credits"
    Me.chk_type_nrc.UseVisualStyleBackColor = True
    '
    'chk_type_object
    '
    Me.chk_type_object.AutoSize = True
    Me.chk_type_object.Location = New System.Drawing.Point(19, 20)
    Me.chk_type_object.Name = "chk_type_object"
    Me.chk_type_object.Size = New System.Drawing.Size(70, 17)
    Me.chk_type_object.TabIndex = 0
    Me.chk_type_object.Text = "xObject"
    Me.chk_type_object.UseVisualStyleBackColor = True
    '
    'gb_stock
    '
    Me.gb_stock.Controls.Add(Me.ef_stock_to)
    Me.gb_stock.Controls.Add(Me.ef_stock_from)
    Me.gb_stock.Location = New System.Drawing.Point(421, 77)
    Me.gb_stock.Name = "gb_stock"
    Me.gb_stock.Size = New System.Drawing.Size(168, 72)
    Me.gb_stock.TabIndex = 6
    Me.gb_stock.TabStop = False
    Me.gb_stock.Text = "xAvailable Stock"
    '
    'ef_stock_to
    '
    Me.ef_stock_to.DoubleValue = 0
    Me.ef_stock_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_stock_to.IntegerValue = 0
    Me.ef_stock_to.IsReadOnly = False
    Me.ef_stock_to.Location = New System.Drawing.Point(8, 43)
    Me.ef_stock_to.Name = "ef_stock_to"
    Me.ef_stock_to.Size = New System.Drawing.Size(149, 25)
    Me.ef_stock_to.SufixText = "Sufix Text"
    Me.ef_stock_to.SufixTextVisible = True
    Me.ef_stock_to.TabIndex = 1
    Me.ef_stock_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_stock_to.TextValue = ""
    Me.ef_stock_to.TextWidth = 60
    Me.ef_stock_to.Value = ""
    '
    'ef_stock_from
    '
    Me.ef_stock_from.DoubleValue = 0
    Me.ef_stock_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_stock_from.IntegerValue = 0
    Me.ef_stock_from.IsReadOnly = False
    Me.ef_stock_from.Location = New System.Drawing.Point(8, 16)
    Me.ef_stock_from.Name = "ef_stock_from"
    Me.ef_stock_from.Size = New System.Drawing.Size(149, 25)
    Me.ef_stock_from.SufixText = "Sufix Text"
    Me.ef_stock_from.SufixTextVisible = True
    Me.ef_stock_from.TabIndex = 0
    Me.ef_stock_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_stock_from.TextValue = ""
    Me.ef_stock_from.TextWidth = 60
    Me.ef_stock_from.Value = ""
    '
    'lbl_legend_01
    '
    Me.lbl_legend_01.Location = New System.Drawing.Point(600, 102)
    Me.lbl_legend_01.Name = "lbl_legend_01"
    Me.lbl_legend_01.Size = New System.Drawing.Size(261, 19)
    Me.lbl_legend_01.TabIndex = 7
    Me.lbl_legend_01.Text = "x* Available Stock = Stock - Requested"
    '
    'lbl_legend_02
    '
    Me.lbl_legend_02.Location = New System.Drawing.Point(600, 124)
    Me.lbl_legend_02.Name = "lbl_legend_02"
    Me.lbl_legend_02.Size = New System.Drawing.Size(261, 19)
    Me.lbl_legend_02.TabIndex = 8
    Me.lbl_legend_02.Text = "x** Requested = Pending to be delivered"
    '
    'ef_redemption_table_name
    '
    Me.ef_redemption_table_name.DoubleValue = 0
    Me.ef_redemption_table_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_redemption_table_name.IntegerValue = 0
    Me.ef_redemption_table_name.IsReadOnly = False
    Me.ef_redemption_table_name.Location = New System.Drawing.Point(-5, 19)
    Me.ef_redemption_table_name.Name = "ef_redemption_table_name"
    Me.ef_redemption_table_name.Size = New System.Drawing.Size(236, 25)
    Me.ef_redemption_table_name.SufixText = "Sufix Text"
    Me.ef_redemption_table_name.SufixTextVisible = True
    Me.ef_redemption_table_name.TabIndex = 0
    Me.ef_redemption_table_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_redemption_table_name.TextValue = ""
    Me.ef_redemption_table_name.TextWidth = 115
    Me.ef_redemption_table_name.Value = ""
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(268, 166)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(126, 17)
    Me.chk_only_vip.TabIndex = 5
    Me.chk_only_vip.Text = "xOnly VIP Clients"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'frm_gift_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1227, 563)
    Me.Name = "frm_gift_sel"
    Me.Text = "frm_gift_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_gift_status.ResumeLayout(False)
    Me.gb_gift_status.PerformLayout()
    Me.gb_points.ResumeLayout(False)
    Me.gb_gift_type.ResumeLayout(False)
    Me.gb_gift_type.PerformLayout()
    Me.gb_stock.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_gift_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_status_available As System.Windows.Forms.CheckBox
  Friend WithEvents ef_gift_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_status_not_available As System.Windows.Forms.CheckBox
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents ef_points_from As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_to As GUI_Controls.uc_entry_field
  Friend WithEvents gb_gift_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_type_nrc As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_object As System.Windows.Forms.CheckBox
  Friend WithEvents gb_stock As System.Windows.Forms.GroupBox
  Friend WithEvents ef_stock_to As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stock_from As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_legend_02 As System.Windows.Forms.Label
  Friend WithEvents lbl_legend_01 As System.Windows.Forms.Label
  Friend WithEvents chk_type_services As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_redeem_credit As System.Windows.Forms.CheckBox
  Friend WithEvents ef_redemption_table_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
End Class
