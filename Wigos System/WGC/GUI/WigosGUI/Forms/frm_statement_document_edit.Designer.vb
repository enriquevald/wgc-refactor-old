<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_statement_document_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.img_preview = New GUI_Controls.uc_image()
    Me.ef_title = New GUI_Controls.uc_entry_field()
    Me.cmb_paper_size = New GUI_Controls.uc_combo()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_config_statement = New System.Windows.Forms.GroupBox()
    Me.ef_fiscal_year_start_date = New System.Windows.Forms.Label()
    Me.dp_fiscal_year_start_date = New System.Windows.Forms.DateTimePicker()
    Me.ef_number_prev_year = New GUI_Controls.uc_entry_field()
    Me.gb_config_page_format = New System.Windows.Forms.GroupBox()
    Me.ef_footer = New System.Windows.Forms.Label()
    Me.txt_footer = New System.Windows.Forms.TextBox()
    Me.ef_header = New System.Windows.Forms.Label()
    Me.ef_logo = New System.Windows.Forms.Label()
    Me.txt_header = New System.Windows.Forms.TextBox()
    Me.lb_img_description = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_config_statement.SuspendLayout()
    Me.gb_config_page_format.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_config_page_format)
    Me.panel_data.Controls.Add(Me.gb_config_statement)
    Me.panel_data.Size = New System.Drawing.Size(529, 539)
    '
    'img_preview
    '
    Me.img_preview.AutoSize = True
    Me.img_preview.BackColor = System.Drawing.SystemColors.Control
    Me.img_preview.ButtonDeleteEnabled = True
    Me.img_preview.FreeResize = False
    Me.img_preview.Image = Nothing
    Me.img_preview.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_preview.ImageLayout = System.Windows.Forms.ImageLayout.None
    Me.img_preview.ImageName = Nothing
    Me.img_preview.Location = New System.Drawing.Point(143, 158)
    Me.img_preview.Margin = New System.Windows.Forms.Padding(0)
    Me.img_preview.Name = "img_preview"
    Me.img_preview.Size = New System.Drawing.Size(236, 188)
    Me.img_preview.TabIndex = 3
    '
    'ef_title
    '
    Me.ef_title.DoubleValue = 0.0R
    Me.ef_title.IntegerValue = 0
    Me.ef_title.IsReadOnly = False
    Me.ef_title.Location = New System.Drawing.Point(1, 17)
    Me.ef_title.Name = "ef_title"
    Me.ef_title.PlaceHolder = Nothing
    Me.ef_title.Size = New System.Drawing.Size(507, 24)
    Me.ef_title.SufixText = "Sufix Text"
    Me.ef_title.SufixTextVisible = True
    Me.ef_title.TabIndex = 1
    Me.ef_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_title.TextValue = ""
    Me.ef_title.TextWidth = 140
    Me.ef_title.Value = ""
    Me.ef_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_paper_size
    '
    Me.cmb_paper_size.AllowUnlistedValues = False
    Me.cmb_paper_size.AutoCompleteMode = False
    Me.cmb_paper_size.IsReadOnly = False
    Me.cmb_paper_size.Location = New System.Drawing.Point(1, 32)
    Me.cmb_paper_size.Name = "cmb_paper_size"
    Me.cmb_paper_size.SelectedIndex = -1
    Me.cmb_paper_size.Size = New System.Drawing.Size(230, 24)
    Me.cmb_paper_size.SufixText = "Sufix Text"
    Me.cmb_paper_size.SufixTextVisible = True
    Me.cmb_paper_size.TabIndex = 0
    Me.cmb_paper_size.TextWidth = 140
    Me.cmb_paper_size.Visible = False
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(13, 20)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(125, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xFunctionEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'gb_config_statement
    '
    Me.gb_config_statement.Controls.Add(Me.ef_fiscal_year_start_date)
    Me.gb_config_statement.Controls.Add(Me.dp_fiscal_year_start_date)
    Me.gb_config_statement.Controls.Add(Me.chk_enabled)
    Me.gb_config_statement.Controls.Add(Me.ef_number_prev_year)
    Me.gb_config_statement.Location = New System.Drawing.Point(4, 4)
    Me.gb_config_statement.Name = "gb_config_statement"
    Me.gb_config_statement.Size = New System.Drawing.Size(519, 76)
    Me.gb_config_statement.TabIndex = 0
    Me.gb_config_statement.TabStop = False
    Me.gb_config_statement.Text = "xConfigStatement"
    '
    'ef_fiscal_year_start_date
    '
    Me.ef_fiscal_year_start_date.Location = New System.Drawing.Point(256, 15)
    Me.ef_fiscal_year_start_date.Name = "ef_fiscal_year_start_date"
    Me.ef_fiscal_year_start_date.Size = New System.Drawing.Size(140, 25)
    Me.ef_fiscal_year_start_date.TabIndex = 15
    Me.ef_fiscal_year_start_date.Text = "xDate"
    Me.ef_fiscal_year_start_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'dp_fiscal_year_start_date
    '
    Me.dp_fiscal_year_start_date.CustomFormat = "dd/MM HH:00"
    Me.dp_fiscal_year_start_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dp_fiscal_year_start_date.Location = New System.Drawing.Point(399, 17)
    Me.dp_fiscal_year_start_date.Name = "dp_fiscal_year_start_date"
    Me.dp_fiscal_year_start_date.Size = New System.Drawing.Size(109, 21)
    Me.dp_fiscal_year_start_date.TabIndex = 1
    Me.dp_fiscal_year_start_date.Value = New Date(2015, 4, 23, 17, 18, 46, 0)
    '
    'ef_number_prev_year
    '
    Me.ef_number_prev_year.DoubleValue = 0.0R
    Me.ef_number_prev_year.IntegerValue = 0
    Me.ef_number_prev_year.IsReadOnly = False
    Me.ef_number_prev_year.Location = New System.Drawing.Point(209, 43)
    Me.ef_number_prev_year.Name = "ef_number_prev_year"
    Me.ef_number_prev_year.PlaceHolder = Nothing
    Me.ef_number_prev_year.Size = New System.Drawing.Size(300, 24)
    Me.ef_number_prev_year.SufixText = "Sufix Text"
    Me.ef_number_prev_year.SufixTextVisible = True
    Me.ef_number_prev_year.TabIndex = 2
    Me.ef_number_prev_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_number_prev_year.TextValue = ""
    Me.ef_number_prev_year.TextWidth = 188
    Me.ef_number_prev_year.Value = ""
    Me.ef_number_prev_year.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_config_page_format
    '
    Me.gb_config_page_format.Controls.Add(Me.ef_footer)
    Me.gb_config_page_format.Controls.Add(Me.txt_footer)
    Me.gb_config_page_format.Controls.Add(Me.ef_header)
    Me.gb_config_page_format.Controls.Add(Me.ef_logo)
    Me.gb_config_page_format.Controls.Add(Me.txt_header)
    Me.gb_config_page_format.Controls.Add(Me.lb_img_description)
    Me.gb_config_page_format.Controls.Add(Me.img_preview)
    Me.gb_config_page_format.Controls.Add(Me.cmb_paper_size)
    Me.gb_config_page_format.Controls.Add(Me.ef_title)
    Me.gb_config_page_format.Location = New System.Drawing.Point(4, 86)
    Me.gb_config_page_format.Name = "gb_config_page_format"
    Me.gb_config_page_format.Size = New System.Drawing.Size(519, 448)
    Me.gb_config_page_format.TabIndex = 1
    Me.gb_config_page_format.TabStop = False
    Me.gb_config_page_format.Text = "xConfigPageFormat"
    '
    'ef_footer
    '
    Me.ef_footer.Location = New System.Drawing.Point(4, 369)
    Me.ef_footer.Name = "ef_footer"
    Me.ef_footer.Size = New System.Drawing.Size(136, 25)
    Me.ef_footer.TabIndex = 17
    Me.ef_footer.Text = "xFooter"
    Me.ef_footer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'txt_footer
    '
    Me.txt_footer.Location = New System.Drawing.Point(143, 369)
    Me.txt_footer.MaxLength = 90
    Me.txt_footer.Name = "txt_footer"
    Me.txt_footer.Size = New System.Drawing.Size(363, 21)
    Me.txt_footer.TabIndex = 4
    Me.txt_footer.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
    '
    'ef_header
    '
    Me.ef_header.Location = New System.Drawing.Point(4, 48)
    Me.ef_header.Name = "ef_header"
    Me.ef_header.Size = New System.Drawing.Size(136, 25)
    Me.ef_header.TabIndex = 15
    Me.ef_header.Text = "xHeader"
    Me.ef_header.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_logo
    '
    Me.ef_logo.Location = New System.Drawing.Point(1, 166)
    Me.ef_logo.Name = "ef_logo"
    Me.ef_logo.Size = New System.Drawing.Size(139, 25)
    Me.ef_logo.TabIndex = 14
    Me.ef_logo.Text = "xLogo"
    Me.ef_logo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'txt_header
    '
    Me.txt_header.Location = New System.Drawing.Point(143, 48)
    Me.txt_header.MaxLength = 700
    Me.txt_header.Multiline = True
    Me.txt_header.Name = "txt_header"
    Me.txt_header.Size = New System.Drawing.Size(363, 70)
    Me.txt_header.TabIndex = 2
    '
    'lb_img_description
    '
    Me.lb_img_description.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_img_description.ForeColor = System.Drawing.Color.Navy
    Me.lb_img_description.Location = New System.Drawing.Point(146, 138)
    Me.lb_img_description.Name = "lb_img_description"
    Me.lb_img_description.Size = New System.Drawing.Size(248, 17)
    Me.lb_img_description.TabIndex = 9
    Me.lb_img_description.Text = "xSAMPLE"
    '
    'frm_statement_document_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(631, 548)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_statement_document_edit"
    Me.Text = "frm_statement_document_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_config_statement.ResumeLayout(False)
    Me.gb_config_statement.PerformLayout()
    Me.gb_config_page_format.ResumeLayout(False)
    Me.gb_config_page_format.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents img_preview As GUI_Controls.uc_image
  Friend WithEvents cmb_paper_size As GUI_Controls.uc_combo
  Friend WithEvents ef_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_config_statement As System.Windows.Forms.GroupBox
  Friend WithEvents gb_config_page_format As System.Windows.Forms.GroupBox
  Friend WithEvents lb_img_description As System.Windows.Forms.Label
  Friend WithEvents txt_header As System.Windows.Forms.TextBox
  Friend WithEvents dp_fiscal_year_start_date As System.Windows.Forms.DateTimePicker
  Friend WithEvents ef_logo As System.Windows.Forms.Label
  Friend WithEvents ef_header As System.Windows.Forms.Label
  Friend WithEvents ef_fiscal_year_start_date As System.Windows.Forms.Label
  Friend WithEvents ef_number_prev_year As GUI_Controls.uc_entry_field
  Friend WithEvents ef_footer As System.Windows.Forms.Label
  Friend WithEvents txt_footer As System.Windows.Forms.TextBox
End Class
