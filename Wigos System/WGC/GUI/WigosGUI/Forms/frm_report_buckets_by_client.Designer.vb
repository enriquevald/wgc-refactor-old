﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_report_buckets_by_client
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.Uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.Uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1298, 122)
    Me.panel_filter.Controls.SetChildIndex(Me.Uc_account_sel1, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 126)
    Me.panel_data.Size = New System.Drawing.Size(1298, 526)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1292, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1292, 4)
    '
    'Uc_account_sel1
    '
    Me.Uc_account_sel1.Account = ""
    Me.Uc_account_sel1.AccountText = ""
    Me.Uc_account_sel1.Anon = False
    Me.Uc_account_sel1.AutoSize = True
    Me.Uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.Uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.Uc_account_sel1.DisabledHolder = False
    Me.Uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_account_sel1.Holder = ""
    Me.Uc_account_sel1.Location = New System.Drawing.Point(6, 5)
    Me.Uc_account_sel1.MassiveSearchNumbers = ""
    Me.Uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.Uc_account_sel1.MassiveSearchType = 0
    Me.Uc_account_sel1.Name = "Uc_account_sel1"
    Me.Uc_account_sel1.SearchTrackDataAsInternal = True
    Me.Uc_account_sel1.ShowMassiveSearch = False
    Me.Uc_account_sel1.ShowVipClients = False
    Me.Uc_account_sel1.Size = New System.Drawing.Size(307, 108)
    Me.Uc_account_sel1.TabIndex = 11
    Me.Uc_account_sel1.Telephone = ""
    Me.Uc_account_sel1.TrackData = ""
    Me.Uc_account_sel1.Vip = False
    Me.Uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'frm_report_buckets_by_client
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1306, 656)
    Me.Name = "frm_report_buckets_by_client"
    Me.Text = "frm_report_buckets_by_client"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Uc_account_sel1 As GUI_Controls.uc_account_sel
End Class
