'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_software_validations_send
' DESCRIPTION:   Send Software validations commands
'                   
' AUTHOR:        David Rigal Vall
' CREATION DATE: 07-ABR-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-ABR-2014  DRV    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_software_validations_send
  Inherits frm_base

#Region " Enums "

#End Region ' Enums

#Region " Constants "

  ' Indicates if Counters are visible or not.
  Private Const COUNTERS_VISIBLE As Boolean = False

#End Region ' Constants

#Region " Members "

  Private m_sv_list() As TYPE_SV_ITEM
  Private m_sv_list_count As Integer = 0

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SOFTWARE_VALIDATION_SEND

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Init gui controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4814)

    Call StyleSheet()

    Me.btn_validate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4818)
    Me.btn_clear.Text = GLB_NLS_GUI_CONTROLS.GetString(7)
    Me.btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Execute Permissions
    Me.btn_validate.Enabled = Me.Permissions.Execute

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)

    ' Text Status Labels
    Me.lbl_pending_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4839)
    Me.lbl_validation_ok_text.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.ConfirmedOK)
    Me.lbl_validation_error_text.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.ConfirmedError)
    Me.lbl_canceled_or_timeout_text.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.CanceledTimeout)
    Me.lbl_error_text.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.Error)
    Me.lbl_unknown_text.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.Unknown)

    ' Color Status Labels
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.Pending, fore_color, back_color)
    Me.lbl_pending.BackColor = back_color
    Me.lbl_pending.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(SoftwareValidationStatus.ConfirmedOK, fore_color, back_color)
    Me.lbl_validation_ok.BackColor = back_color
    Me.lbl_validation_ok.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(SoftwareValidationStatus.ConfirmedError, fore_color, back_color)
    Me.lbl_validation_error.BackColor = back_color
    Me.lbl_validation_error.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.CanceledTimeout, fore_color, back_color)
    Me.lbl_canceled_or_timeout.BackColor = back_color
    Me.lbl_canceled_or_timeout.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.Error, fore_color, back_color)
    Me.lbl_error.BackColor = back_color
    Me.lbl_error.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.Unknown, fore_color, back_color)
    Me.lbl_unknown.BackColor = back_color
    Me.lbl_unknown.ForeColor = fore_color

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.WCPTerminalTypes(), uc_provider.UC_FILTER_TYPE.ALL_ACTIVE)

    ' Set filter default values
    Call SetDefaultValues()

    Me.gd_software_validation.Clear()

    Me.tmr_refresh.Interval = 5000
    Me.tmr_refresh.Start()

  End Sub ' GUI_InitControls


#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Initialize all form filters with their default values and
  '          clear the data showed in the data grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ClearAllData()

    Call SetDefaultValues()

    Me.gd_software_validation.Clear()
    m_sv_list = Nothing
    m_sv_list_count = 0

  End Sub ' ClearAllData

  ' PURPOSE: Insert commands for each selected terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - CommandType
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertSoftwareValidation()

    Dim _list_terminal_id() As Long
    Dim _soft_validation_table As DataTable
    Dim _data_row As DataRow
    Dim _sql_trans As SqlTransaction = Nothing
    Dim _commit_trx As Boolean
    Dim _cursor As Cursor

    ' Get terminals id list from control
    _list_terminal_id = Me.uc_pr_list.GetTerminalIdListSelected()

    If Not Me.uc_pr_list.TerminalListHasValues Then
      Exit Sub
    End If

    _cursor = Windows.Forms.Cursor.Current
    _commit_trx = False
    _soft_validation_table = InitSoftwareValidationDataTable()

    Try
      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        Exit Sub
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      For Each _terminal_id As Long In _list_terminal_id
        _data_row = _soft_validation_table.NewRow()
        _data_row.Item("SVAL_TERMINAL_ID") = _terminal_id
        _data_row.Item("SVAL_RANDOM_SEED") = SoftwareValidations.GenerateRandomSeed()

        _soft_validation_table.Rows.Add(_data_row)
      Next

      Using _db_trx As New DB_TRX()

        If Not SoftwareValidations.InsertSoftwareValidation(_soft_validation_table, _db_trx.SqlTransaction) Then
          Return
        End If

        _db_trx.Commit()
      End Using

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      Windows.Forms.Cursor.Current = _cursor
    End Try

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Add software valdiation to software validations list
    ReDim Preserve m_sv_list(0 To m_sv_list_count + _soft_validation_table.Rows.Count - 1)

    For Each _row As DataRow In _soft_validation_table.Select()
      m_sv_list(m_sv_list_count).id = _row.Item("SVAL_VALIDATION_ID")
      m_sv_list(m_sv_list_count).terminal_id = _row.Item("SVAL_TERMINAL_ID")
      m_sv_list(m_sv_list_count).status = SoftwareValidationStatus.Pending

      m_sv_list_count = m_sv_list_count + 1
    Next

    Me.gd_software_validation.Redraw = False

    UpdateGrid()

    Me.gd_software_validation.Redraw = True

    Windows.Forms.Cursor.Current = _cursor

  End Sub ' InsertCommand

  ' PURPOSE: Create a DataTable for the Software Validations to send to the selected terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  Private Function InitSoftwareValidationDataTable() As DataTable
    Dim _soft_validation_table As DataTable
    Dim _column As DataColumn

    _soft_validation_table = New DataTable("SOFTWARE_VALIDATIONS")

    _column = New DataColumn("SVAL_VALIDATION_ID")
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ReadOnly = False
    _soft_validation_table.Columns.Add(_column)

    _column = New DataColumn("SVAL_TERMINAL_ID")
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ReadOnly = False
    _soft_validation_table.Columns.Add(_column)

    _column = New DataColumn("SVAL_RANDOM_SEED")
    _column.DataType = System.Type.GetType("System.String")
    _column.ReadOnly = False
    _soft_validation_table.Columns.Add(_column)

    Return _soft_validation_table
  End Function ' InitCommandDataTable

  ' PURPOSE: Update software validations list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateGrid()

    Dim _sv_row_idx As Integer

    UpdateSoftwareValidationsDatabase()

    For _sv_row_idx = 0 To m_sv_list_count - 1

      ' Check if new row
      If gd_software_validation.NumRows = _sv_row_idx Then
        ' First, add new row
        gd_software_validation.AddRow()
      End If

      If m_sv_list(_sv_row_idx).previous_status <> m_sv_list(_sv_row_idx).status Then

        m_sv_list(_sv_row_idx).previous_status = m_sv_list(_sv_row_idx).status
        UpdateGridRow(_sv_row_idx)

      End If


    Next

  End Sub ' UpdateGrid

  ' PURPOSE: Read command individually from DB and update status 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateSoftwareValidationsDatabase()

    Dim _str_sql As StringBuilder
    Dim _str_sv_ids As String = ""
    Dim _table As DataTable
    Dim _num_db_rows As Integer
    Dim _idx_db_row As Integer
    Dim _idx_sv_list As Integer
    Dim _last_idx_sv As Integer
    Dim _found As Boolean
    Dim _select_enabled As Boolean
    Dim _data_row As DataRow

    If m_sv_list Is Nothing Then
      Exit Sub
    End If

    _select_enabled = False

    Try

      For Each _sv As TYPE_SV_ITEM In m_sv_list

        If _sv.status = SoftwareValidationStatus.Pending Or _
           _sv.status = SoftwareValidationStatus.Notified Or _
           _sv.status = SoftwareValidationStatus.ConfirmedNonValidated Then

          If (_str_sv_ids <> "") Then
            _str_sv_ids = _str_sv_ids & ", "
          End If
          _str_sv_ids = _str_sv_ids & _sv.id

        End If

      Next

      If _str_sv_ids = "" Then
        Exit Sub
      End If
      _str_sql = New StringBuilder()

      _str_sql.AppendLine("   SELECT   SVAL_VALIDATION_ID                            ")
      _str_sql.AppendLine("          , SVAL_STATUS                                   ")
      _str_sql.AppendLine("          , SVAL_INSERTED                                 ")
      _str_sql.AppendLine("          , SVAL_LAST_REQUEST                             ")
      _str_sql.AppendLine("          , SVAL_RECEIVED_DATETIME                        ")
      _str_sql.AppendLine("          , SVAL_RECEIVED_STATUS                          ")
      _str_sql.AppendLine("          , SVAL_EXPECTED_SIGNATURE                       ")
      _str_sql.AppendLine("          , TE_NAME                                       ")
      _str_sql.AppendLine("          , TE_PROVIDER_ID                                ")
      _str_sql.AppendLine("     FROM   SOFTWARE_VALIDATIONS, TERMINALS               ")
      _str_sql.AppendLine("    WHERE   SVAL_VALIDATION_ID IN ( " & _str_sv_ids & " ) ")
      _str_sql.AppendLine("      AND   TE_TERMINAL_ID = SVAL_TERMINAL_ID             ")
      _str_sql.AppendLine(" ORDER BY   SVAL_VALIDATION_ID                            ")

      _table = GUI_GetTableUsingSQL(_str_sql.ToString(), m_sv_list_count)



      _num_db_rows = _table.Rows.Count()
      _last_idx_sv = 0

      For _idx_db_row = 0 To _num_db_rows - 1
        _found = False
        For _idx_sv_list = 0 To m_sv_list_count - 1

          _data_row = _table.Rows(_idx_db_row)

          If m_sv_list(_last_idx_sv).id = _data_row.Item("SVAL_VALIDATION_ID") Then
            m_sv_list(_last_idx_sv).status = _data_row.Item("SVAL_STATUS")
            m_sv_list(_last_idx_sv).inserted_datetime = _data_row.Item("SVAL_INSERTED")
            If Not _data_row.IsNull("SVAL_LAST_REQUEST") Then
              m_sv_list(_last_idx_sv).last_request = _data_row.Item("SVAL_LAST_REQUEST")
            End If
            If Not _data_row.IsNull("SVAL_RECEIVED_DATETIME") Then
              m_sv_list(_last_idx_sv).received = _data_row.Item("SVAL_RECEIVED_DATETIME")
            End If
            m_sv_list(_last_idx_sv).terminal_name = _data_row.Item("TE_NAME")
            If _data_row.IsNull("TE_PROVIDER_ID") Then
              m_sv_list(_last_idx_sv).provider_name = ""
            Else
              m_sv_list(_last_idx_sv).provider_name = _data_row.Item("TE_PROVIDER_ID")
            End If
            If _data_row.IsNull("SVAL_RECEIVED_STATUS") Then
              m_sv_list(_last_idx_sv).sas_host_error = 0
            Else
              m_sv_list(_last_idx_sv).sas_host_error = _data_row.Item("SVAL_RECEIVED_STATUS")
            End If
            If _data_row.IsNull("SVAL_EXPECTED_SIGNATURE") Then
              m_sv_list(_last_idx_sv).expected_signature = ""
            Else
              m_sv_list(_last_idx_sv).expected_signature = _data_row.Item("SVAL_EXPECTED_SIGNATURE")
            End If

            _found = True

          End If

          _last_idx_sv = (_last_idx_sv + 1) Mod m_sv_list_count
          If _found Then
            Exit For
          End If

        Next
      Next


    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' UpdateSoftwareValidationsDatabase

  ' PURPOSE: Update the grid, insert row if not exists or change row if it exists 
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '     - OUTPUT
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateGridRow(ByVal RowIndex As Integer)

    SoftwareValidationStatus_SetupRow(Me.gd_software_validation, RowIndex, m_sv_list(RowIndex))

  End Sub ' UpdateGridRow


  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Private Sub StyleSheet()

    SoftwareValidationStatus_StyleSheet(Me.gd_software_validation, COUNTERS_VISIBLE)
    Me.gd_software_validation.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

  End Sub ' StyleSheet


#End Region ' Private Functions

#Region " Events "

#Region " Timer Events "

  ' PURPOSE: Update software validation list of the grid, every interval ms of the tmr_refresh.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub tmr_refresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_refresh.Tick

    Me.gd_software_validation.Redraw = False

    UpdateGrid()

    Me.gd_software_validation.Redraw = True

  End Sub ' tmr_refresh_Tick

#End Region ' Timer Events

#Region " Button Events "


  ' PURPOSE: Event handler routine for the Validate Button.
  '          Send software validation request to the selected terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_validate_ClickEvent() Handles btn_validate.ClickEvent

    Call InsertSoftwareValidation()

  End Sub ' btn_logger_ClickEvent

  ' PURPOSE: Event handler routine for the Clear Button.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_clear_ClickEvent() Handles btn_clear.ClickEvent

    Call ClearAllData()

  End Sub ' btn_clear_ClickEvent


  ' PURPOSE: Close form.
  '
  '    - INPUT:
  '           - None
  '
  '    - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent

#End Region ' Button Events

#Region " Sql Server Events "

#End Region ' Sql Server Events

#Region " Grid Events "
  Private Sub dg_list_SetToolTipTextEvent(ByVal GridRowas As Integer, ByVal GridCol As Integer, ByRef Txt As String) Handles gd_software_validation.SetToolTipTextEvent
    Txt = SoftwareValidationStatus_GetToolTipText(gd_software_validation, GridRowas, GridCol)
  End Sub
#End Region

#End Region 'Events

End Class