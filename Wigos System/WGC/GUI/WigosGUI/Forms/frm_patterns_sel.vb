'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_patterns_sel.vb
' DESCRIPTION:   Patterns selection
' AUTHOR:        Javier Molina
' CREATION DATE: 07-MAY-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2014  JMM    Initial version
' 05-SEP-2014  JPJ    Mailing alarm functionality
' 21-OCT-2014  DLL    Fixed Bug WIG-1237:Hide columns GRID_COLUMN_DETECTIONS And GRID_COLUMN_LAST_FIND when is Multisite. Only show when is Site
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class frm_patterns_sel
  Inherits frm_base_sel

#Region "Enums"

#End Region

#Region "Structures"

#End Region ' Structures

#Region " Constants "

  ' Grid configuration
  Private Const GRID_HEADER_ROWS_COUNT As Integer = 1

  Private Const GRID_COLUMN_PATTERN_ID As Integer = 0
  Private Const GRID_COLUMN_PATTERN_NAME As Integer = 1
  Private Const GRID_COLUMN_ACTIVE As Integer = 2
  Private Const GRID_COLUMN_LAST_FIND As Integer = 3
  Private Const GRID_COLUMN_DETECTIONS As Integer = 4
  Private Const GRID_COLUMN_MAILING As Integer = 5

  Private Const GRID_COLUMNS_COUNT As Integer = 6

  'SQL Query Columns
  Private Const SQL_COL_PATTERN_ID As Integer = 0
  Private Const SQL_COL_PATTERN_NAME As Integer = 1
  Private Const SQL_COL_ACTIVE As Integer = 2
  Private Const SQL_COL_LAST_FIND As Integer = 3
  Private Const SQL_COL_DETECTIONS As Integer = 4
  Private Const SQL_COL_MAILING As Integer = 5

#End Region ' Constants

#Region " Members "

  ' Report filters 
  Private m_name_filter As String
  Private m_enabled_filter As String

#End Region ' Members

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PATTERNS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4924) '4924 "Patterns"

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Enable/Disable buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = Not GeneralParam.GetBoolean("Site", "MultiSiteMember", False)

    'Name
    Me.ef_name.TextVisible = True
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(693) '693 "Nombre"
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    'Active group box
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)     '262 "Status"
    Me.opt_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4941)  '4941 "Enabled"
    Me.opt_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4942) '4942 "Disabled"
    Me.opt_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)      '1353 "All"

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub 'GUI_InitControls


  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck  

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    ' Get Select and from
    _str_sql.AppendLine("SELECT   PT_ID                                       ")
    _str_sql.AppendLine("       , PT_NAME                                     ")
    _str_sql.AppendLine("       , PT_ACTIVE                                   ")
    _str_sql.AppendLine("       , PT_LAST_FIND                                ")
    _str_sql.AppendLine("       , ISNULL (PT_DETECTIONS, 0) AS PT_DETECTIONS  ")
    _str_sql.AppendLine("       , PT_ACTIVE_MAILING ")
    _str_sql.AppendLine("  FROM   PATTERNS                                    ")

    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine("ORDER BY   PT_NAME ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case ENUM_BUTTON.BUTTON_NEW
        Call ShowNewPatternForm()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _idx_row As Int32
    Dim _pattern_id As Integer
    Dim _pattern_name As String
    Dim _frm_edit As frm_pattern_edit

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then

        Exit For
      End If
    Next

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm_edit = New frm_pattern_edit

    ' Get the complete pattern ID and pattern name and launch edition form
    _pattern_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_PATTERN_ID).Value
    _pattern_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_PATTERN_NAME).Value

    _frm_edit.ShowEditItem(_pattern_id, _pattern_name)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_ShowSelectedItem

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

  End Sub ' GUI_SetToolTipText

#End Region ' Overrides

#Region " GUI Reports "

  ' PURPOSE : Set Custom formats to excel
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  ' 
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    MyBase.GUI_ReportParams(ExcelData, FirstColIndex)

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'First col
    PrintData.SetFilter(Me.ef_name.Text, Me.m_name_filter)
    PrintData.SetFilter(Me.gb_status.Text, Me.m_enabled_filter)

    PrintData.FilterValueWidth(1) = 1500
    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(3) = 3000
    PrintData.FilterHeaderWidth(4) = 300

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' Name filter
    m_name_filter = Me.ef_name.Value

    'Enabled filter
    m_enabled_filter = ""

    If Me.opt_enabled.Checked Then
      m_enabled_filter = Me.opt_enabled.Text
    ElseIf Me.opt_disabled.Checked Then
      m_enabled_filter = Me.opt_disabled.Text
    ElseIf Me.opt_all.Checked Then
      m_enabled_filter = Me.opt_all.Text
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS_COUNT)
      .Sortable = True
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Pattern ID
      .Column(GRID_COLUMN_PATTERN_ID).Header(0).Text = " ID "
      .Column(GRID_COLUMN_PATTERN_ID).Width = 0

      ' Name
      .Column(GRID_COLUMN_PATTERN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253) '253 "Nombre"
      .Column(GRID_COLUMN_PATTERN_NAME).Width = 3860
      .Column(GRID_COLUMN_PATTERN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Active
      .Column(GRID_COLUMN_ACTIVE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008) '3008 "Habilitado"
      .Column(GRID_COLUMN_ACTIVE).Width = 1000
      .Column(GRID_COLUMN_ACTIVE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Last Find
      .Column(GRID_COLUMN_LAST_FIND).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4961) '4961 "Last occurrence"
      .Column(GRID_COLUMN_LAST_FIND).Width = 2400
      .Column(GRID_COLUMN_LAST_FIND).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Detections
      .Column(GRID_COLUMN_DETECTIONS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4962) '4962 "Occurrences"
      .Column(GRID_COLUMN_DETECTIONS).Width = 1500
      .Column(GRID_COLUMN_DETECTIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Mailing active
      .Column(GRID_COLUMN_MAILING).Header(0).Text = GLB_NLS_GUI_AUDITOR.GetString(77) '4962 "Mailing active"
      .Column(GRID_COLUMN_MAILING).Width = 1000
      .Column(GRID_COLUMN_MAILING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        .Column(GRID_COLUMN_LAST_FIND).Width = 0
        .Column(GRID_COLUMN_DETECTIONS).Width = 0
      End If
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Pattern ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PATTERN_ID).Value = DbRow.Value(SQL_COL_PATTERN_ID)

    If Not IsDBNull(DbRow.Value(SQL_COL_PATTERN_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PATTERN_NAME).Value = DbRow.Value(SQL_COL_PATTERN_NAME)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_ACTIVE)) Then
      If DbRow.Value(SQL_COL_ACTIVE) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACTIVE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACTIVE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
      End If
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_LAST_FIND)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_FIND).Value = DbRow.Value(SQL_COL_LAST_FIND)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LAST_FIND).Value = "---"
    End If

    If Not IsDBNull(DbRow.Value(SQL_COL_DETECTIONS)) Then
      If DbRow.Value(SQL_COL_DETECTIONS) <> 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETECTIONS).Value = GUI_FormatNumber(DbRow.Value(SQL_COL_DETECTIONS), 0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETECTIONS).Value = "---"
      End If
    End If

    ' Mailing enabled
    If Not IsDBNull(DbRow.Value(SQL_COL_MAILING)) Then
      If DbRow.Value(SQL_COL_MAILING) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MAILING).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MAILING).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)
      End If
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Me.ef_name.Value = ""
    Me.opt_enabled.Checked = True

  End Sub ' SetDefaultValues  

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As StringBuilder
    Dim _str_where_account As String = ""

    _str_where = New StringBuilder()

    ' Name filter
    If Me.ef_name.Value <> "" Then
      _str_where.AppendLine(" AND PT_NAME like '%" & Me.ef_name.Value & "%'")
    End If

    ' Enabled filter
    If Me.opt_enabled.Checked Then
      _str_where.AppendLine(" AND PT_ACTIVE = 1 ")
    ElseIf Me.opt_disabled.Checked Then
      _str_where.AppendLine(" AND PT_ACTIVE = 0 ")
    End If

    If Not String.IsNullOrEmpty(_str_where.ToString()) Then
      _str_where = New StringBuilder(" WHERE " & _str_where.Remove(_str_where.ToString().IndexOf("AND"), 3).ToString())
    End If

    Return _str_where.ToString()
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new pattern to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub ShowNewPatternForm()

    Dim frm_edit As frm_pattern_edit

    frm_edit = New frm_pattern_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' ShowNewPatternForm

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

#End Region ' Events

End Class