'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:    frm_ticket_configuration
' DESCRIPTION:    Configure tickets impression
' AUTHOR:         Ferran Ortner
' CREATION DATE:  21-JUL-2015
'
' REVISION HISTORY:
'
' Date         Author  Description
' ----------   ------  -----------------------------------------------
' 21-JUL-2015  FOS     First Release.
' 07-AUG-2015  DCS     Refactoring
' 18-AUG-2015  FOS     Add reprint funcionality.
' 26-AUG-2015  FOS     Add info label when general params change
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common

Public Class frm_ticket_configuration_edit
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_VOUCHER_ID As Integer = 1
  Private Const GRID_COLUMN_GENERATE_TICKET As Integer = 2
  Private Const GRID_COLUMN_PRINT_TICKET As Integer = 3
  Private Const GRID_COLUMN_NUM_COPY_TICKET As Integer = 4
  Private Const GRID_COLUMN_REPRINT_TICKET As Integer = 5
  Private Const GRID_COLUMN_HIDDEN_VOUCHER_ID As Integer = 6

  Private Const GRID_COLUMNS As Integer = 7

  Private Const GRID_ROW_SENTINEL As Integer = 0
  Private Const GRID_ROW_VOUCHER_ID As Integer = 1
  Private Const GRID_ROW_GENERATE_TICKET As Integer = 2
  Private Const GRID_ROW_PRINT_TICKET As Integer = 3
  Private Const GRID_ROW_NUM_COPY_TICKET As Integer = 4
  Private Const GRID_ROW_REPRINT_TICKET As Integer = 5
  Private Const GRID_ROW_HIDDEN_VOUCHER_ID As Integer = 6

  Private Const MAX_COPY As Integer = 9

  Private m_num_copy_limit As Integer
  Private m_disable_grid_clr As Color

#End Region ' Constants

#Region " Properties "

  Public Overloads Property VoucherReprintText() As String
    Get
      Return uc_entry_reprint.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_reprint.Value = Value
    End Set

  End Property ' VoucherReprintText

  Public Overloads Property VoucherPrint() As Boolean
    Get
      Return cb_print.Checked
    End Get

    Set(ByVal Value As Boolean)
      cb_print.Checked = Value
    End Set

  End Property ' VoucherPrint

  Public Overloads Property VoucherReprint() As Boolean
    Get
      Return cb_reprint.Checked
    End Get

    Set(ByVal Value As Boolean)
      cb_reprint.Checked = Value
    End Set

  End Property ' VoucherReprint

  Public Overloads Property VoucherGenerate() As Boolean
    Get
      Return cb_generate.Checked
    End Get

    Set(ByVal Value As Boolean)
      cb_generate.Checked = Value
    End Set

  End Property ' VoucherGenerate


#End Region ' Properties

#Region "Overrides"

  ' PURPOSE: Controls the form button click events
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Handler keyboard press
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    ' The keypress event is done in uc_grid control
    If Me.dg_principal.ContainsFocus Then
      Return Me.dg_principal.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    m_num_copy_limit = WSI.Common.GeneralParam.GetInt32("Cashier.Voucher", "MaxCopy", MAX_COPY)

#If DEBUG Then
    Call WSI.Common.GeneralParam.ReloadGP()
#End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6618) '"Configuración de impresión de ticket caja por operación"

    Call StyleSheet()

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    uc_entry_reprint.Text = GLB_NLS_GUI_CONFIGURATION.GetString(450)
    uc_entry_reprint.SetFilter(GUI_Controls.CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    m_disable_grid_clr = ColorTranslator.FromHtml("#F0F0F0")

    cb_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6620)
    cb_generate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6619)
    cb_reprint.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6277)
    gp_general_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6666)

    lbl_message_info.Text = String.Empty

  End Sub ' GUI_InitControls

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TICKET_PRINT_CONFIGURATION
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TICKET_PRINT_CONFIGURATION()

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2479), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  '  PURPOSE: Set the screen data based on Database object
  '
  '  PARAMS:
  '     - INPUT:
  '               -Sql Context
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _tickect_print_configuration As CLASS_TICKET_PRINT_CONFIGURATION

    _tickect_print_configuration = DbReadObject

    Call RedrawGrid()

    uc_entry_reprint.Value = _tickect_print_configuration.VoucherReprintText
    cb_print.Checked = _tickect_print_configuration.VoucherPrint
    cb_generate.Checked = _tickect_print_configuration.VoucherGenerate
    cb_reprint.Checked = _tickect_print_configuration.VoucherReprint

    Call SetMessageInfo()

  End Sub ' GUI_SetScreenData

  '  PURPOSE: Get data in the form
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim _idx As Int16
    Dim _key As String
    Dim _tickect_print_configuration_Edited As New CLASS_TICKET_PRINT_CONFIGURATION

    _tickect_print_configuration_Edited = Me.DbEditedObject

    For _idx = 0 To dg_principal.NumRows - 1
      _key = dg_principal.Cell(_idx, GRID_ROW_HIDDEN_VOUCHER_ID).Value
      _tickect_print_configuration_Edited.TicketDicPrintConfiguration.Item(CType(_key, WSI.Common.OperationCode)).Generate = IIf(dg_principal.Cell(_idx, GRID_ROW_GENERATE_TICKET).Value, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
      _tickect_print_configuration_Edited.TicketDicPrintConfiguration.Item(CType(_key, WSI.Common.OperationCode)).Reprint = IIf(dg_principal.Cell(_idx, GRID_ROW_REPRINT_TICKET).Value, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)

      If String.IsNullOrEmpty(dg_principal.Cell(_idx, GRID_ROW_NUM_COPY_TICKET).Value) Then
        _tickect_print_configuration_Edited.TicketDicPrintConfiguration.Item(CType(_key, WSI.Common.OperationCode)).PrintCopy = 0
      Else
        _tickect_print_configuration_Edited.TicketDicPrintConfiguration.Item(CType(_key, WSI.Common.OperationCode)).PrintCopy = GUI_FormatNumber(dg_principal.Cell(_idx, GRID_ROW_NUM_COPY_TICKET).Value)
      End If

      _tickect_print_configuration_Edited.TicketDicPrintConfiguration.Item(CType(_key, WSI.Common.OperationCode)).Print = _tickect_print_configuration_Edited.TicketDicPrintConfiguration.Item(CType(_key, WSI.Common.OperationCode)).PrintCopy > 0
    Next

    _tickect_print_configuration_Edited.VoucherReprintText = uc_entry_reprint.Value
    _tickect_print_configuration_Edited.VoucherPrint = cb_print.Checked
    _tickect_print_configuration_Edited.VoucherReprint = cb_reprint.Checked
    _tickect_print_configuration_Edited.VoucherGenerate = cb_generate.Checked

  End Sub ' GUI_GetScreenData

  '  PURPOSE: Validate screen data before OK
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _idx As Integer

    For _idx = 0 To dg_principal.NumRows - 1
      With dg_principal

        If String.IsNullOrEmpty(.Cell(_idx, GRID_COLUMN_NUM_COPY_TICKET).Value) Then
          .Cell(_idx, GRID_COLUMN_NUM_COPY_TICKET).Value = 0
        Else

          'if it's negative
          If .Cell(_idx, GRID_COLUMN_NUM_COPY_TICKET).Value < 0 Then
            Return False
          End If

          'if it's greater than maxlimit
          If .Cell(_idx, GRID_COLUMN_NUM_COPY_TICKET).Value > m_num_copy_limit Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1421), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6623), m_num_copy_limit)
            Return False
          End If

          'if it's no numeric value
          If Not IsNumeric(.Cell(_idx, GRID_COLUMN_NUM_COPY_TICKET).Value) Then
            Return False
          End If
        End If

      End With
    Next

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region "Public Functions"

  Public Sub New()
    ' This call is required by the designer.
    InitializeComponent()
  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheet()
    Try

      With Me.dg_principal

        Call .Init(GRID_COLUMNS, 2, True)

        .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
        .Sortable = True
        .SortAscending = True
        .SortByCol = 1

        ' Sentinel
        .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
        .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
        .Column(GRID_COLUMN_SENTINEL).Width = 150
        .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False
        .Column(GRID_COLUMN_SENTINEL).Editable = False

        ' GRID VOUCHER_ID
        .Column(GRID_COLUMN_VOUCHER_ID).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_VOUCHER_ID).Header(0).Text = ""
        .Column(GRID_COLUMN_VOUCHER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6621) '"Voucher ID"
        .Column(GRID_COLUMN_VOUCHER_ID).Width = 4000
        .Column(GRID_COLUMN_VOUCHER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_VOUCHER_ID).IsColumnSortable = True

        ' GRID GENERATE TICKET
        .Column(GRID_COLUMN_GENERATE_TICKET).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
        .Column(GRID_COLUMN_GENERATE_TICKET).Header(0).Text = ""
        .Column(GRID_COLUMN_GENERATE_TICKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6619) '"generate"
        .Column(GRID_COLUMN_GENERATE_TICKET).Width = 1500
        .Column(GRID_COLUMN_GENERATE_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_GENERATE_TICKET).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_COLUMN_GENERATE_TICKET).Editable = True

        ' GRID PRINT TICKET
        .Column(GRID_COLUMN_PRINT_TICKET).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
        .Column(GRID_COLUMN_PRINT_TICKET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6620) '"Print"
        .Column(GRID_COLUMN_PRINT_TICKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2525) '"habilitar"
        .Column(GRID_COLUMN_PRINT_TICKET).Width = 1500
        .Column(GRID_COLUMN_PRINT_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_PRINT_TICKET).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_COLUMN_PRINT_TICKET).Editable = True

        ' GRID NUM COPY
        .Column(GRID_COLUMN_NUM_COPY_TICKET).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_NUM_COPY_TICKET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6620) '"Print"
        .Column(GRID_COLUMN_NUM_COPY_TICKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6623) '"Numero de copias"
        .Column(GRID_COLUMN_NUM_COPY_TICKET).Width = 1500
        .Column(GRID_COLUMN_NUM_COPY_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_NUM_COPY_TICKET).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_NUM_COPY_TICKET).Editable = True
        .Column(GRID_COLUMN_NUM_COPY_TICKET).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, m_num_copy_limit.ToString().Length, 0)

        ' GRID REPRINT COPY
        .Column(GRID_COLUMN_REPRINT_TICKET).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_REPRINT_TICKET).Header(0).Text = ""
        .Column(GRID_COLUMN_REPRINT_TICKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6277) '"Reimprimir"
        .Column(GRID_COLUMN_REPRINT_TICKET).Width = 1500
        .Column(GRID_COLUMN_REPRINT_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_REPRINT_TICKET).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
        .Column(GRID_COLUMN_REPRINT_TICKET).Editable = True

        ' GRID HIDDEN VOUCHER ID
        .Column(GRID_COLUMN_HIDDEN_VOUCHER_ID).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_HIDDEN_VOUCHER_ID).Width = 0
        .Column(GRID_COLUMN_HIDDEN_VOUCHER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
        .Column(GRID_COLUMN_HIDDEN_VOUCHER_ID).Editable = True

      End With

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub 'StyleSheet

  ' PURPOSE: Repaint grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RedrawGrid()
    Dim _operation_str As String
    Dim _idx_row As Integer
    Dim _voucher_parameters As WSI.Common.OperationVoucherParams.VoucherParameters
    Dim _tickect_print_configuration As CLASS_TICKET_PRINT_CONFIGURATION

    _tickect_print_configuration = DbReadObject
    _operation_str = String.Empty

    dg_principal.Redraw = False
    dg_principal.Clear()

    For Each ticket_configuration As KeyValuePair(Of WSI.Common.OperationCode, WSI.Common.OperationVoucherParams.VoucherParameters) In _tickect_print_configuration.TicketDicPrintConfiguration
      With Me.dg_principal
        _idx_row = Me.dg_principal.AddRow()
        _operation_str = _tickect_print_configuration.TranslateOperationType(ticket_configuration.Key)
        .Cell(_idx_row, GRID_COLUMN_VOUCHER_ID).Value = _operation_str
        .Cell(_idx_row, GRID_COLUMN_HIDDEN_VOUCHER_ID).Value = ticket_configuration.Key
        _voucher_parameters = ticket_configuration.Value
        .Cell(_idx_row, GRID_COLUMN_GENERATE_TICKET).Value = IIf(_voucher_parameters.Generate, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
        .Cell(_idx_row, GRID_COLUMN_REPRINT_TICKET).Value = IIf(_voucher_parameters.Reprint, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)

        If (_voucher_parameters.PrintCopy = 0) Then
          .Cell(_idx_row, GRID_COLUMN_NUM_COPY_TICKET).Value = String.Empty
          .Cell(_idx_row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_UNCHECKED
        Else
          .Cell(_idx_row, GRID_COLUMN_NUM_COPY_TICKET).Value = _voucher_parameters.PrintCopy
          .Cell(_idx_row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_CHECKED
        End If
      End With
    Next

    dg_principal.SortByCol = GRID_COLUMN_VOUCHER_ID
    dg_principal.Redraw = True

  End Sub ' RedrawGrid

  ' PURPOSE: Set message when general params change
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetMessageInfo()

    lbl_message_info.Text = String.Empty

    If cb_generate.Checked = False Then
      lbl_message_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6673)
    ElseIf cb_print.Checked = False Then
      lbl_message_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6672)
    End If

  End Sub 'SetMessageInfo

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Handler for change data in a cell in grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '           - Col
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub dg_principal_CellDataChangedEvent(Row As Integer, Column As Integer) Handles dg_principal.CellDataChangedEvent

    Dim _num_copy As Integer
    _num_copy = 0

    If Column = GRID_COLUMN_GENERATE_TICKET Then

      If dg_principal.Cell(Row, Column).Value = uc_grid.GRID_CHK_UNCHECKED Then
        dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value = String.Empty
        dg_principal.Cell(Row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_UNCHECKED
        dg_principal.Cell(Row, GRID_COLUMN_REPRINT_TICKET).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value = 1
        dg_principal.Cell(Row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_CHECKED
        dg_principal.Cell(Row, GRID_COLUMN_REPRINT_TICKET).Value = uc_grid.GRID_CHK_CHECKED
      End If

    End If

    If Column = GRID_COLUMN_NUM_COPY_TICKET Then

      If String.IsNullOrEmpty(dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value) OrElse dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value <= 0 Then
        dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value = String.Empty
        dg_principal.Cell(Row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_UNCHECKED
      End If

      If Not String.IsNullOrEmpty(dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value) AndAlso dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value > 0 Then
        dg_principal.Cell(Row, GRID_COLUMN_GENERATE_TICKET).Value = uc_grid.GRID_CHK_CHECKED
        dg_principal.Cell(Row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_CHECKED
        _num_copy = dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value
      End If

    End If

    If Column = GRID_COLUMN_PRINT_TICKET Then
      If dg_principal.Cell(Row, GRID_COLUMN_PRINT_TICKET).Value = uc_grid.GRID_CHK_CHECKED Then
        dg_principal.Cell(Row, GRID_COLUMN_GENERATE_TICKET).Value = uc_grid.GRID_CHK_CHECKED
        If _num_copy = 0 Then
          _num_copy = 1
        End If
        dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value = _num_copy
      Else
        dg_principal.Cell(Row, GRID_COLUMN_NUM_COPY_TICKET).Value = String.Empty
      End If
    End If

    If Column = GRID_COLUMN_REPRINT_TICKET Then
      If dg_principal.Cell(Row, GRID_COLUMN_REPRINT_TICKET).Value = uc_grid.GRID_CHK_CHECKED Then
        dg_principal.Cell(Row, GRID_COLUMN_GENERATE_TICKET).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If
  End Sub ' dg_principal_CellDataChangedEvent




  Private Sub cb_print_Click(sender As Object, e As EventArgs) Handles cb_print.Click
    If cb_print.Checked = True AndAlso cb_generate.Checked = False Then
      cb_generate.Checked = True
    End If

    Call SetMessageInfo()
  End Sub

  Private Sub cb_reprint_Click(sender As Object, e As EventArgs) Handles cb_reprint.Click
    If cb_reprint.Checked = True AndAlso cb_generate.Checked = False Then
      cb_generate.Checked = True
    End If

    Call SetMessageInfo()
  End Sub

  Private Sub cb_generate_Click(sender As Object, e As EventArgs) Handles cb_generate.Click
    If cb_generate.Checked = True Then
      cb_print.Checked = True
      cb_reprint.Checked = True
    Else
      cb_print.Checked = False
      cb_reprint.Checked = False
    End If

    Call SetMessageInfo()
  End Sub

#End Region ' Events
End Class