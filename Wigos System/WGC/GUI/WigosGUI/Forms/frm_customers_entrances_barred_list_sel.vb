﻿'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customers_entrances_barred_list_sel
' DESCRIPTION:   Customer Entrances Barred List Report
' AUTHOR:        Jorge Concheyro
' CREATION DATE: 13-JUL-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-JUL-2016  JRC    Initial version
' 20-SEP-2016  JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)
' 19-SEP-2016  XGJ    Eliminar contador de filas de la grid y substutuilo por una etiqueta con el número de entradas
' 29-SEP-2016  FJC    PBI 17681:Visitas / Recepción: MEJORAS II - Agrupar menú (GUI)
' 23-DEC-2016  MS     Bug 22012:Wigos GUI: Blacklist Search Failing 
' 14-JUL-2017  RGR    Bug 28727:WIGOS-3635 Name not visible in Barred list report when pressing Message button
' 24-JUL-2017  MS     Bug 28908:WIGOS-3886 Minor errors in selection criteria in Barred list report
' 07-AUG-2017  MS     Bug 28908:WIGOS-3886 Added Missing Report Filters
' 18-AUG-2017  MS     Bug 28908:[WIGOS-3886] Minor errors in selection criteria in Barred list report
'-------------------------------------------------------------------- 

Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports System.Globalization
Imports GUI_CommonOperations
Imports WSI.Common
Imports WSI.Common.Entrances.Enums
Imports WSI.Common.Entrances
Imports WSI.Common.Utilities
Imports WSI.Common.Utilities.Extensions
Imports GUI_CommonMisc
Imports GUI_CommonMisc.ControlColor
Imports GUI_CommonMisc.mdl_NLS
Imports System.Collections.Generic

Public Class frm_customers_entrances_barred_list_sel
  Inherits GUI_Controls.frm_base_sel

#Region "Private Properties"


  Dim m_list_type As Integer
  Dim m_list_subtype As Integer
  Dim m_entrance_denied As Boolean
  Dim m_name As String
  Dim m_block_type() As Integer  ' list of ENUM_BLACKLIST_REASON
  Dim m_document_number As String
  Dim m_black_list_types As Dictionary(Of Integer, String)
  Dim m_cls_blacklist_type As cls_blacklist_import_type
  Dim m_sql_where As String
  Dim _enum_values As Dictionary(Of [Enum], String)

  Private m_account As String
  Private m_track_data As String
  Private m_holder As String

  'Counters
  Private Const COUNTER_ERROR As Integer = 1
  Private m_counter_list(MAX_COUNTERS) As Integer
  Private Const MAX_COUNTERS As Integer = 1

  Private Const COUNTER_ALL_VISIBLE As Boolean = True

  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

#End Region

#Region " Constants "

  ' SQL Columns
  Private Const SQL_COLUMN_LIST_TYPE As Integer = 0
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 1
  Private Const SQL_COLUMN_HOLDER_LASTNAME As Integer = 2
  Private Const SQL_COLUMN_DOCUMENT_TYPE As Integer = 3
  Private Const SQL_COLUMN_DOCUMENT_NUMBER As Integer = 4
  Private Const SQL_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED As Integer = 5
  Private Const SQL_COLUMN_IMPORTED_TYPE_LIST As Integer = 6
  Private Const SQL_COLUMN_EXCLUSION_REASON As Integer = 7
  Private Const SQL_COLUMN_IMPORTED_TYPE_LIST_MESSAGE As Integer = 8

  ' Num grid columns / headers
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 11

  ' Grid Columns

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 1
  Private Const GRID_COLUMN_HOLDER_LASTNAME As Integer = 2
  Private Const GRID_COLUMN_DOCUMENT_TYPE As Integer = 3
  Private Const GRID_COLUMN_DOCUMENT_NUMBER As Integer = 4
  Private Const GRID_COLUMN_LIST_TYPE As Integer = 5
  Private Const GRID_COLUMN_EXCLUSION_REASON As Integer = 6
  Private Const GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED As Integer = 7
  Private Const GRID_COLUMN_IMPORTED_TYPE_LIST As Integer = 8
  Private Const GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE As Integer = 9
  Private Const GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT As Integer = 10

  'Width Columns
  Private Const GRID_WIDTH_COLUMN_INDEX As Integer = 210
  Private Const GRID_WIDTH_COLUMN_HOLDER_NAME As Integer = 1995
  Private Const GRID_WIDTH_COLUMN_HOLDER_LASTNAME As Integer = 2505
  Private Const GRID_WIDTH_COLUMN_DOCUMENT_TYPE As Integer = 1815
  Private Const GRID_WIDTH_COLUMN_DOCUMENT_NUMBER As Integer = 2130
  Private Const GRID_WIDTH_COLUMN_LIST_TYPE As Integer = 780
  Private Const GRID_WIDTH_COLUMN_EXCLUSION_REASON As Integer = 1785
  Private Const GRID_WIDTH_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED As Integer = 1710
  Private Const GRID_WIDTH_COLUMN_IMPORTED_TYPE_LIST As Integer = 1365
  Private Const GRID_WIDTH_COLUMN_IMPORTED_TYPE_LIST_MESSAGE As Integer = 1005

  ' Type of card for where clause
  'Private Const CARD_TYPE As Integer = 2
#End Region

#Region "Private Methods"

  ''' <summary>
  ''' Set default values to filters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()

    uc_account_sel1.Account = ""
    uc_account_sel1.TrackData = ""
    uc_account_sel1.Holder = ""
    chk_list_type_imported.Checked = False
    chk_list_type_internal.Checked = False
    chk_entrance_denied.Checked = False
    edtDocumentNumber.Text = ""

    chklst_Block_type.Enabled = True
    chklst_Block_type.btn_uncheck_all.PerformClick()
    chk_list_type_imported_CheckedChanged(Nothing, Nothing)
    cmb_blacklist_type.SelectedIndex = 0



  End Sub 'SetDefaultValues

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ExportBlacklistExported()

    Dim _sql_command As SqlCommand
    Dim _sb As System.Text.StringBuilder
    Dim table As DataTable


    If (uc_account_sel1.TrackData = "" And GUI_ParseNumber(uc_account_sel1.Account) = 0) _
          And (chk_list_type_imported.Checked Or BothListTypesSelected()) Then

      If MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7484), MsgBoxStyle.YesNo, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7472)) = MsgBoxResult.No Then
        Exit Sub
      End If

      _sql_command = New SqlCommand()
      _sb = New StringBuilder

      _sb.Append("SELECT * FROM BLACKLIST_FILE_IMPORTED ")

      Try

        If Not m_sql_where Is Nothing Then
          If m_sql_where.Length > 0 Then
            _sb.AppendLine("        WHERE")
            _sb.AppendLine(m_sql_where.ToString)
          End If
        End If

        _sql_command.CommandType = CommandType.Text
        _sql_command.CommandText = _sb.ToString

        table = GUI_GetTableUsingCommand(_sql_command, Integer.MaxValue, , )
        WSI.Common.BlacklistExport.Export(table, True)

        RegisterAuditExport()

      Catch ex As Exception
        Log.Exception(ex)
      End Try
    End If

  End Sub

  Private Sub RegisterAuditExport()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _audit_object As CLASS_AUDITOR_DATA
    Dim _filter As GUI_Reports.PrintDataset.PrintFilterRow
    Dim _offset As Integer
    Dim _text As New System.Text.StringBuilder

    _print_data = New GUI_Reports.CLASS_PRINT_DATA

    Call GUI_ReportFilter(_print_data)

    _filter = _print_data.Filter()
    _offset = _print_data.GetMaxNumFilters()

    _text.Append(GLB_NLS_GUI_AUDITOR.GetString(487))
    _text.Append(" ")
    _text.Append(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7459))

    _audit_object = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    _audit_object.SetName(0, _text.ToString())
    _text.Length = 0

    For _cursor As Integer = 0 To _offset - 1
      If Not FilterIsNull(_filter.ItemArray(_cursor + _offset)) Then

        _text.Append(_filter.ItemArray(_cursor))
        _text.Append(" ")
        _text.Append(_filter.ItemArray(_cursor + _offset))

        _audit_object.SetField(0, _text.ToString(), GLB_NLS_GUI_AUDITOR.GetString(477))
        _text.Length = 0

      End If
    Next

    _audit_object.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                         0)

  End Sub 'RegisterAudit


  Private Function BothListTypesSelected() As Boolean
    Return chk_list_type_internal.Checked = chk_list_type_imported.Checked
  End Function


  ''' <summary>
  ''' LoadComboBlacklistTypes 
  ''' </summary>
  ''' <param name="LoadComboBlacklistTypes"></param>
  ''' <remarks></remarks>
  Private Sub LoadComboBlacklistTypes()
    m_cls_blacklist_type = New cls_blacklist_import_type()
    cmb_blacklist_type.Clear()
    cmb_blacklist_type.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))
    For Each _dr As DataRow In m_cls_blacklist_type.GetTypes().Rows
      cmb_blacklist_type.Add(_dr("BKLT_ID_TYPE"), _dr("BKLT_NAME"))
    Next
  End Sub ' LoadComboBlacklistTypes

  Private Sub LoadDictionaryBlackListTypes()
    m_black_list_types = New Dictionary(Of Integer, String)
    For Each _dr As DataRow In m_cls_blacklist_type.GetTypes().Rows
      m_black_list_types.Add(_dr("BKLT_ID_TYPE"), _dr("BKLT_NAME"))
    Next
  End Sub

  ''' <summary>
  ''' LoadListBlockType
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadListBlockType()
    _enum_values = EnumHelper.ToDictionary2(Of ENUM_BLACKLIST_REASON)()
    chklst_Block_type.Clear()
    For Each pair As KeyValuePair(Of [Enum], String) In _enum_values
      chklst_Block_type.Add(Convert.ToInt32(pair.Key), pair.Value)
    Next
  End Sub ' LoadListBlockType


  ' PURPOSE: Increment the indicated Severity counter.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Severity
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub UpdateCounters(AccessAllowed As Boolean)

    If Not AccessAllowed Then
      m_counter_list(COUNTER_ERROR) = m_counter_list(COUNTER_ERROR) + 1
    End If


  End Sub ' UpdateCounters


  ''' <summary>
  ''' Define layout of all Main Grid Columns 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    Dim _total_columns As Integer

    _total_columns = GRID_COLUMNS

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)


      .Counter(COUNTER_ERROR).Visible = COUNTER_ALL_VISIBLE
      .Counter(COUNTER_ERROR).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_ERROR).ForeColor = GetColor(COLOR_ERROR_FORE)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      'Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)



      ' Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7481)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7462)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_COLUMN_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_HOLDER_NAME).IsColumnPrintable = True

      ' Lastname
      .Column(GRID_COLUMN_HOLDER_LASTNAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7481)
      .Column(GRID_COLUMN_HOLDER_LASTNAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7477)
      .Column(GRID_COLUMN_HOLDER_LASTNAME).Width = GRID_WIDTH_COLUMN_HOLDER_LASTNAME
      .Column(GRID_COLUMN_HOLDER_LASTNAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_HOLDER_LASTNAME).IsColumnPrintable = True


      ' document  type
      .Column(GRID_COLUMN_DOCUMENT_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7481)
      .Column(GRID_COLUMN_DOCUMENT_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7466)
      .Column(GRID_COLUMN_DOCUMENT_TYPE).Width = GRID_WIDTH_COLUMN_DOCUMENT_TYPE
      .Column(GRID_COLUMN_DOCUMENT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_DOCUMENT_TYPE).IsColumnPrintable = True

      ' document number
      .Column(GRID_COLUMN_DOCUMENT_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7481)
      .Column(GRID_COLUMN_DOCUMENT_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7469)

      .Column(GRID_COLUMN_DOCUMENT_NUMBER).Width = GRID_WIDTH_COLUMN_DOCUMENT_NUMBER
      .Column(GRID_COLUMN_DOCUMENT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_DOCUMENT_NUMBER).IsColumnPrintable = True

      ' List Type
      .Column(GRID_COLUMN_LIST_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7468)
      .Column(GRID_COLUMN_LIST_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7474)
      .Column(GRID_COLUMN_LIST_TYPE).Width = GRID_WIDTH_COLUMN_LIST_TYPE

      .Column(GRID_COLUMN_LIST_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_LIST_TYPE).IsColumnPrintable = True

      ' razon de exclusion
      .Column(GRID_COLUMN_EXCLUSION_REASON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7468)
      .Column(GRID_COLUMN_EXCLUSION_REASON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7475)

      .Column(GRID_COLUMN_EXCLUSION_REASON).Width = GRID_WIDTH_COLUMN_EXCLUSION_REASON
      .Column(GRID_COLUMN_EXCLUSION_REASON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_EXCLUSION_REASON).IsColumnPrintable = True


      ' entrance allowed
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7468)
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7473)
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).Width = GRID_WIDTH_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).IsColumnPrintable = True

      ' tipo de lista importada
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7468)
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7479)
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST).Width = GRID_WIDTH_COLUMN_IMPORTED_TYPE_LIST
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST).IsColumnPrintable = True


      ' duration
      '.Column(GRID_COLUMN_DURATION).Header(0).Text = "Duración"
      '.Column(GRID_COLUMN_DURATION).Width = GRID_WIDTH_COLUMN_DURATION
      '.Column(GRID_COLUMN_DURATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER
      '.Column(GRID_COLUMN_DURATION).IsColumnPrintable = True

      ' mensaje
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7468)
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7467)
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).Width = GRID_WIDTH_COLUMN_IMPORTED_TYPE_LIST_MESSAGE
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).IsColumnPrintable = True


      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Header(0).Text = ""
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Header(1).Text = ""
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Width = 0
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).IsColumnPrintable = False


    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Checks if the filter is empty
  '
  '  PARAMS:
  '     - INPUT:
  '           - FilterValue
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: If the filter has any value
  '     - False: If the filter is empty
  Private Function FilterIsNull(ByVal FilterValue As Object) As Boolean

    If FilterValue Is Nothing OrElse IsDBNull(FilterValue) Then
      Return True
    ElseIf String.IsNullOrEmpty(FilterValue) Or FilterValue = "---" Then
      Return True
    End If

    Return False

  End Function

#End Region

#Region "Overrides"

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_list_type = 0
    m_list_subtype = 0
    m_entrance_denied = False
    m_name = ""
    ReDim m_block_type(0)
    m_document_number = ""
    m_account = ""
    m_track_data = ""
    m_holder = ""

    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()

    If chk_list_type_imported.Checked = chk_list_type_internal.Checked Then
      m_list_type = 0
    ElseIf chk_list_type_imported.Checked Then
      m_list_type = 1
    Else
      m_list_type = 2
    End If

    m_list_subtype = cmb_blacklist_type.Value
    m_entrance_denied = chk_entrance_denied.Checked
    m_name = uc_account_sel1.Holder
    m_block_type = chklst_Block_type.SelectedIndexes()
    m_document_number = edtDocumentNumber.Text
  End Sub 'GUI_ReportUpdateFilters

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.FilterHeaderWidth(2) = 1600
    PrintData.FilterValueWidth(2) = 2000
    PrintData.FilterHeaderWidth(4) = 500
    PrintData.FilterValueWidth(4) = 1800

    If BothListTypesSelected() Then
      PrintData.SetFilter(grp_box_list_type.Text, chk_list_type_internal.Text + " " + chk_list_type_imported.Text)
    ElseIf chk_list_type_imported.Checked Then
      PrintData.SetFilter(grp_box_list_type.Text, chk_list_type_imported.Text)
    Else
      PrintData.SetFilter(grp_box_list_type.Text, chk_list_type_internal.Text)
    End If

    If m_entrance_denied Then
      PrintData.SetFilter(chk_entrance_denied.Text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7460))
    Else
      PrintData.SetFilter(chk_entrance_denied.Text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7461))
    End If

    PrintData.SetFilter(lblDocNumber.Text, edtDocumentNumber.Text)

    If Not chklst_Block_type.SelectedValuesList.IsNullOrWhiteSpace Then
      PrintData.SetFilter(chklst_Block_type.GroupBoxText, chklst_Block_type.SelectedValuesList)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)

  End Sub ' GUI_ReportFilter

  ''' <summary>
  '''  Establish Form Id, according to the enumeration in the application
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_CUSTOMER_ENTRANCES_BARRED_LIST
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ''' <summary>
  ''' Sets the values of a row in the data grid
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns>True: the row could be added</returns>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _result As Boolean
    Dim _dt_scan_id_types As DataTable
    Dim _doc_type As Int32
    Dim _message As String


    _dt_scan_id_types = IdentificationTypes.GetIdentificationTypes(False)
    _result = True

    'Entrance Allowed
    If Not DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED) = 0 And chk_entrance_denied.Checked Then
      Return False
    End If

    With Me.Grid

      'List type
      If DbRow.Value(SQL_COLUMN_LIST_TYPE) = 1 Then
        .Cell(RowIndex, GRID_COLUMN_LIST_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7463)
        .Cell(RowIndex, GRID_COLUMN_EXCLUSION_REASON).Value = DbRow.Value(SQL_COLUMN_EXCLUSION_REASON)
      Else
        .Cell(RowIndex, GRID_COLUMN_LIST_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7464)

        Dim _enumval = Int32.Parse(DbRow.Value(SQL_COLUMN_EXCLUSION_REASON))

        Dim _enum As ENUM_BLACKLIST_REASON
        _enum = [Enum].Parse(GetType(ENUM_BLACKLIST_REASON), _enumval)

        .Cell(RowIndex, GRID_COLUMN_EXCLUSION_REASON).Value = _enum_values(_enum)
      End If

      ' Name
      .Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)

      ' Lastname
      .Cell(RowIndex, GRID_COLUMN_HOLDER_LASTNAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_LASTNAME)


      'Document Type
      Int32.TryParse(DbRow.Value(SQL_COLUMN_DOCUMENT_TYPE), _doc_type)

      .Cell(RowIndex, GRID_COLUMN_DOCUMENT_TYPE).Value = IdentificationTypes.DocIdTypeString(_doc_type)

      'Document Number
      .Cell(RowIndex, GRID_COLUMN_DOCUMENT_NUMBER).Value = DbRow.Value(SQL_COLUMN_DOCUMENT_NUMBER)


      'Entrance Allowed
      If DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED) = 0 Then
        .Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_02)
        .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_02)
        .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

        .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST_ENTRANCE_ALLOWED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7465)
        Call UpdateCounters(False)

      End If

      'black list imported type
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST)) Then
        'm_black_list_types.TryGetValue(DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST), .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST).Value)
        .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST).Value = DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST)
      End If

      'Razon de exclusion


      If Not IsDBNull(DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST_MESSAGE)) Then
        _message = DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).ToString().Trim()

        If Not String.IsNullOrEmpty(_message) Then
          .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7465)
          .Cell(RowIndex, GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Value = DbRow.Value(SQL_COLUMN_IMPORTED_TYPE_LIST_MESSAGE)
        End If

      End If


      'duracion
      '.Cell(RowIndex, GRID_COLUMN_DURATION).Value = DbRow.Value(SQL_COLUMN_DURATION)

    End With

    Return True

  End Function 'GUI_SetupRow

  ''' <summary>
  ''' Get the defined Query Type
  ''' </summary>
  ''' <returns>ENUM_QUERY_TYPE</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Not String.IsNullOrEmpty(Me.Grid.Cell(SelectedRow, GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Value)

  End Sub  ' GUI_RowSelectedEvent

  ''' <summary>
  ''' Open additional form to show details for the select row
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim _list_type As String
    Dim _message As String
    Dim _idx_row As Integer
    Dim _msg As String

    _msg = ""
    _list_type = ""
    _message = ""

    If Me.Grid.NumRows = 0 Then
      Return
    End If

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Me.Grid.SelectedRows.Length < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    If Not String.IsNullOrEmpty(Me.Grid.Cell(_idx_row, GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Value) Then

      _list_type = String.Format("{0} {1}", Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_NAME).Value, Me.Grid.Cell(_idx_row, GRID_COLUMN_HOLDER_LASTNAME).Value)
      _message = Me.Grid.Cell(_idx_row, GRID_COLUMN_IMPORTED_TYPE_LIST_MESSAGE_TEXT).Value
      _msg = "   " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7462) + ": " + _list_type + Environment.NewLine + "   " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7467) + ": " + _message
      MsgBox(_msg, ENUM_MB_TYPE.MB_TYPE_INFO, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7468))

    End If







  End Sub ' GUI_ShowSelectedItem


  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck  

  ''' <summary>
  '''  SQL Command query ready to send to the database
  ''' </summary>
  ''' <returns>Build an SQL Command query from conditions set in the filters</returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand
    Dim _sb As System.Text.StringBuilder
    Dim _sb_WHERE As System.Text.StringBuilder
    Dim _idx As Integer
    Dim _account_id As Long
    Dim _internal_card_id As String
    Dim _card_type As Integer
    Dim _rc As Boolean

    _sql_command = New SqlCommand()
    _sb = New System.Text.StringBuilder()
    _sb_WHERE = New System.Text.StringBuilder()
    _account_id = 0
    _internal_card_id = ""
    _card_type = 0
    _rc = False


    ' Imported Lists Query
    If (uc_account_sel1.TrackData = "" And GUI_ParseNumber(uc_account_sel1.Account) = 0) _
          And (chk_list_type_imported.Checked Or BothListTypesSelected()) Then

      _sb.AppendLine("        SELECT                                                                                              ")
      _sb.AppendLine("                 1 TIPO                                                                                     ")

      _sb.AppendLine("                ,CASE WHEN BLKF_NAME IS NULL THEN '' ELSE BLKF_NAME END                                     ")
      _sb.AppendLine("                 +                                                                                          ")
      _sb.AppendLine("                 CASE WHEN BLKF_MIDDLE_NAME IS NOT NULL AND BLKF_NAME IS NOT NULL THEN ' ' ELSE '' END      ")
      _sb.AppendLine("                 +                                                                                          ")
      _sb.AppendLine("                 CASE WHEN BLKF_MIDDLE_NAME IS NULL THEN '' ELSE BLKF_MIDDLE_NAME END AS NAME               ")


      _sb.AppendLine("                ,CASE WHEN BLKF_LASTNAME_1 IS NULL THEN '' ELSE BLKF_LASTNAME_1 END                         ")
      _sb.AppendLine("                 +                                                                                          ")
      _sb.AppendLine("                 CASE WHEN BLKF_LASTNAME_2 IS NOT NULL AND BLKF_LASTNAME_1 IS NOT NULL THEN ' ' ELSE '' END ")
      _sb.AppendLine("                 +                                                                                          ")
      _sb.AppendLine("                 CASE WHEN BLKF_LASTNAME_2 IS NULL THEN '' ELSE BLKF_LASTNAME_2 END AS LASTNAME             ")


      _sb.AppendLine("                ,BLKF_DOCUMENT_TYPE  DOC_TYPE                                               ")
      _sb.AppendLine("                ,BLKF_DOCUMENT DOC_NUM                                                                      ")
      _sb.AppendLine("                ,ISNULL(BKLT_ENTRANCE_ALLOWED,0) AS BKLT_ENTRANCE_ALLOWED                                   ")
      _sb.AppendLine("                ,BKLT_NAME                                                                                  ")
      _sb.AppendLine("                ,BKLF_REASON_DESCRIPTION                                                                    ")
      '_sb.AppendLine("                ,BKLF_EXCLUSION_DURATION                                                                    ")
      _sb.AppendLine("                ,BKLT_MESSAGE                                                                               ")
      _sb.AppendLine("          FROM  BLACKLIST_FILE_IMPORTED                                                                     ")
      _sb.AppendLine("    INNER JOIN  BLACKLIST_FILE_IMPORTED_TYPE ON BLKF_ID_TYPE = BKLT_ID_TYPE                                 ")


      If Not edtDocumentNumber.Text.IsNullOrWhiteSpace Then
        _sb_WHERE.AppendLine("                BLKF_DOCUMENT LIKE '%" + DataTable_FilterValue(edtDocumentNumber.Text) + "%'")
      End If


      ' Entrance allowed filter
      If chk_entrance_denied.Checked Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        _sb_WHERE.AppendLine("                BKLT_ENTRANCE_ALLOWED = 0")
      End If


      ' Imported subtype List filter
      If cmb_blacklist_type.SelectedIndex <> 0 And chk_list_type_imported.Checked Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        _sb_WHERE.AppendLine("                BLKF_ID_TYPE = " + cmb_blacklist_type.Value.ToString)
      End If

      ' Name Filter
      If Not uc_account_sel1.Holder.IsNullOrWhiteSpace Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        _sb_WHERE.AppendLine("                (BLKF_NAME LIKE'%" + DataTable_FilterValue(uc_account_sel1.Holder) + "%'")
        _sb_WHERE.AppendLine("                OR BLKF_MIDDLE_NAME LIKE'%" + DataTable_FilterValue(uc_account_sel1.Holder) + "%'")
        _sb_WHERE.AppendLine("                OR BLKF_LASTNAME_1 LIKE'%" + DataTable_FilterValue(uc_account_sel1.Holder) + "%'")
        _sb_WHERE.AppendLine("                OR BLKF_LASTNAME_2 LIKE'%" + DataTable_FilterValue(uc_account_sel1.Holder) + "%'")
        _sb_WHERE.AppendLine("                )")

      End If


      If _sb_WHERE.Length > 0 Then
        _sb.AppendLine("        WHERE")
        _sb.AppendLine(_sb_WHERE.ToString)
        m_sql_where = _sb_WHERE.ToString()
      End If



    End If

    _sb_WHERE.Length = 0
    _sb_WHERE.Capacity = 0

    ' Imported Lists Query
    ' 2016-DEC-23 MS  Bug 22012:Wigos GUI: Blacklist Search Failing - added CAST for blk_Reason
    If chk_list_type_internal.Checked Or BothListTypesSelected() Then

      If _sb.Length > 0 Then
        _sb.AppendLine("        UNION ALL                                                                                           ")
      End If

      _sb.AppendLine("        SELECT                                                                                              ")
      _sb.AppendLine("                 2 TIPO                                                                                     ")
      _sb.AppendLine("                ,AC_HOLDER_NAME3 NAME                                                                        ")

      _sb.AppendLine("                ,CASE WHEN AC_HOLDER_NAME1 IS NULL THEN '' ELSE AC_HOLDER_NAME1 END                         ")
      _sb.AppendLine("                 +                                                                                          ")
      _sb.AppendLine("                 CASE WHEN AC_HOLDER_NAME2 IS NOT NULL AND AC_HOLDER_NAME1 IS NOT NULL THEN ' ' ELSE '' END ")
      _sb.AppendLine("                 +                                                                                          ")
      _sb.AppendLine("                 CASE WHEN AC_HOLDER_NAME2 IS NULL THEN '' ELSE AC_HOLDER_NAME2 END AS AC_HOLDER_NAME12     ")




      _sb.AppendLine("                ,IT.IDT_ID    DOC_TYPE                                                                      ")
      _sb.AppendLine("                ,AC_HOLDER_ID DOC_NUM                                                                       ")
      _sb.AppendLine("                ,BKLT_ENTRANCE_ALLOWED = 1                                                                  ")
      _sb.AppendLine("                ,BKLT_NAME = ''                                                                             ")
      _sb.AppendLine("                ,BKL_REASON_DESCRIPTION  =  CAST(bkl_reason AS nvarchar)                                    ")
      _sb.AppendLine("                ,BKLT_MESSAGE  = bkl_reason_description                                                     ")
      _sb.AppendLine("          FROM  BLACKLIST_INTERNAL_BLOCK_LIST  BL                                                           ")
      _sb.AppendLine("    INNER JOIN  ACCOUNTS AC ON AC.AC_ACCOUNT_ID = BL.BKL_ACCOUNT_ID                                         ")
      _sb.AppendLine("     LEFT JOIN  IDENTIFICATION_TYPES  IT ON AC.AC_HOLDER_ID_TYPE = IT.IDT_ID                                ")


      ' Document Number Filter
      If Not edtDocumentNumber.Text.IsNullOrWhiteSpace Then
        _sb_WHERE.AppendLine("                AC_HOLDER_ID LIKE '%" + DataTable_FilterValue(edtDocumentNumber.Text) + "%'")   ' unclosed quoted
      End If

      ' Name Filter
      If Not uc_account_sel1.Holder.IsNullOrWhiteSpace Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        _sb_WHERE.AppendLine("                AC_HOLDER_NAME LIKE'%" + DataTable_FilterValue(uc_account_sel1.Holder) + "%'")
      End If

      'Get account id filter
      _account_id = GUI_ParseNumber(uc_account_sel1.Account)
      If _account_id > 0 Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        _sb_WHERE.AppendLine("                AC_ACCOUNT_ID = " + _account_id.ToString)
      End If

      'trackdata filtering
      If uc_account_sel1.TrackData <> "" Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        If uc_account_sel1.TrackData.Length = TRACK_DATA_LENGTH_WIN Then

          _internal_card_id = ""

          _rc = WSI.Common.CardNumber.TrackDataToInternal(uc_account_sel1.TrackData, _internal_card_id, _card_type)
          If _rc Then
            _sb_WHERE.AppendLine("                AC_TRACK_DATA = '" & _internal_card_id & "'")
          Else
            _sb_WHERE.AppendLine("                AC_TRACK_DATA = '" & uc_account_sel1.TrackData & "'")
          End If
        Else
          _sb_WHERE.AppendLine("                AC_TRACK_DATA = '" & uc_account_sel1.TrackData & "'")
        End If
      End If



      ' Reasons list filter
      If chklst_Block_type.SelectedIndexes.Length > 0 Then
        If _sb_WHERE.Length > 0 Then
          _sb_WHERE.AppendLine("                AND")
        End If
        _sb_WHERE.AppendLine("                (")

        For _idx = 0 To chklst_Block_type.SelectedIndexes.Length - 1
          If _idx > 0 Then
            _sb_WHERE.AppendLine("                OR")
          End If
          _sb_WHERE.AppendLine("                BKL_REASON = " + chklst_Block_type.SelectedIndexes(_idx).ToString())
        Next
        _sb_WHERE.AppendLine("                )")
      End If
      If _sb_WHERE.Length > 0 Then
        _sb.AppendLine("        WHERE")
        _sb.AppendLine(_sb_WHERE.ToString)
      End If

    End If
    If _sb.Length > 0 Then
      _sb.AppendLine("        ORDER BY NAME")
    Else
      ' if the query gets empty something have to send
      _sb.AppendLine("        SELECT * FROM BLACKLIST_FILE_IMPORTED_TYPE WHERE 1=2")
    End If



    _sql_command.CommandType = CommandType.Text
    _sql_command.CommandText = _sb.ToString


    Return _sql_command

  End Function 'GUI_GetSqlCommand

  ''' <summary>
  '''  Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7036) & ": " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7459)

    chk_list_type_imported.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7463)
    chk_list_type_internal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7464)
    lblDocNumber.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7469)
    grp_box_list_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7470)
    cmb_blacklist_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7479)
    chklst_Block_type.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7471)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7472)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7467)
    chk_entrance_denied.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7473)

    LoadComboBlacklistTypes()
    LoadDictionaryBlackListTypes()
    LoadListBlockType()
    chklst_Block_type.btn_uncheck_all.PerformClick()
    cmb_blacklist_type.Enabled = False
    chk_entrance_denied.Enabled = False
    chklst_Block_type.Enabled = False
    'chklst_Block_type.btn_check_all.Width = 100
    'chklst_Block_type.btn_uncheck_all.Width = 100
    chklst_Block_type.ResizeGrid()

    ' Hidden counter of rows
    Me.Grid.PanelRightVisible = False



    Me.GUI_StyleSheet()


  End Sub 'GUI_InitControls





  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_counter_list(COUNTER_ERROR) = 0
  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If Grid.Counter(COUNTER_ERROR).Value <> m_counter_list(COUNTER_ERROR) Then
      Grid.Counter(COUNTER_ERROR).Value = m_counter_list(COUNTER_ERROR)
    End If
  End Sub ' GUI_AfterLastRow




  ''' <summary>
  '''  Process clicks on data grid (double-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)
        Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = True
      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_ShowSelectedItem()
      Case ENUM_BUTTON.BUTTON_NEW
        Call ExportBlacklistExported()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub 'GUI_ButtonClick

  ''' <summary>
  ''' Initialize all form filters with their default values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

#End Region

#Region " Public Methods"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Events"

  Private Sub chk_list_type_imported_CheckedChanged(sender As Object, e As EventArgs) Handles chk_list_type_imported.CheckedChanged, chk_list_type_internal.CheckedChanged
    cmb_blacklist_type.Enabled = chk_list_type_imported.Checked
    If Not cmb_blacklist_type.Enabled Then
      cmb_blacklist_type.SelectedIndex = 0
    End If

    chklst_Block_type.Enabled = chk_list_type_internal.Checked
    If Not chklst_Block_type.Enabled Then
      chklst_Block_type.btn_uncheck_all.PerformClick()
    End If

    For Each _dr As DataRow In m_cls_blacklist_type.GetTypes().Rows
      If cmb_blacklist_type.Value(cmb_blacklist_type.SelectedIndex) = _dr("BKLT_ID_TYPE") Then
        chk_entrance_denied.Checked = (_dr("BKLT_ENTRANCE_ALLOWED") = 0)
      End If
    Next


  End Sub

#End Region

  Private Sub cmb_blacklist_type_ValueChangedEvent() Handles cmb_blacklist_type.ValueChangedEvent
    For Each _dr As DataRow In m_cls_blacklist_type.GetTypes().Rows
      If cmb_blacklist_type.Value(cmb_blacklist_type.SelectedIndex) = _dr("BKLT_ID_TYPE") Then
        chk_entrance_denied.Checked = Not _dr("BKLT_ENTRANCE_ALLOWED")
      End If
    Next

    If cmb_blacklist_type.Value(cmb_blacklist_type.SelectedIndex) = 0 Then
      chk_entrance_denied.Checked = False
    End If

  End Sub
End Class