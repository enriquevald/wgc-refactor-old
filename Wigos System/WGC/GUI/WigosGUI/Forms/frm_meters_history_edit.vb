'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_meters_history_edit
' DESCRIPTION:   Meters adjustment
' AUTHOR:        Jos� Mart�nez & David Hern�ndez
' CREATION DATE: 08-APR-2015

'
' REVISION HISTORY:
'
' Date        Author       Description
' ----------  ---------    -----------------------------------------------
' 08-APR-2015 JML & DHA    Initial version.
' 17-JUN-2015 YNM          Fixed Bug WIG-2452: Terminal Filter only have to list terminals with type SAS-HOST 
' 01-MAR-2016 DDS          Fixed Bug 10124: Select one terminal message is missing
' 16-DEC-2016 FGB          Fixed Bug 7119: Audit: It isn't translated (to spanish) the name of the meter 'Total credits from coin acceptor' when adjusting meters
' 29-OCT-2017 LTC          [WIGOS-6134] Multisite Terminal meter adjustment
' 31-OCT-2017 LTC          Bug 30512:WIGOS-6315 Multisite Terminal meter adjustment - reset button
' 07-NOV-2017 EOR          Bug 30635:WIGOS-6443 Cannot see data in "Meters adjustment" MultisiteGUI window when there is only one site
'-------------------------------------------------------------------

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.frm_base_sel
Imports System.Data.SqlClient

Public Class frm_meters_history_edit
  Inherits GUI_Controls.frm_base_print

#Region " Constants "
  Private Const LENGTH_METER As Integer = 15
  Private Const LENGTH_DECIMAL_METER As Integer = 12
  Private Const LENGTH_DECIMAL As Integer = 2
  Private Const MAX_DESC_LEN As Integer = 512
  Private Const SEPARATOR As String = " - "
  Private Const VISIBLE_CODE_LENGTH As Integer = 4
  Private Const EVENT_START_BASE As Integer = &H10000
  Private Const NLS_EVENTS As Integer = 4000

  Private Const GRID_M_COLUMN_INDEX As Integer = 0
  Private Const GRID_M_COLUMN_GAMING_DAY As Integer = 1
  Private Const GRID_M_COLUMN_TERMINAL_ID As Integer = 2
  Private Const GRID_M_COLUMN_TERMINAL_METER_CODE As Integer = 3
  Private Const GRID_M_COLUMN_TERMINAL_METER_NAME As Integer = 4
  Private Const GRID_M_COLUMN_TERMINAL_GAME_ID As Integer = 5
  Private Const GRID_M_COLUMN_TERMINAL_DENOMINATION As Integer = 6
  Private Const GRID_M_COLUMN_TERMINAL_TYPE As Integer = 7
  Private Const GRID_M_COLUMN_TERMINAL_SUB_TYPE As Integer = 8
  Private Const GRID_M_COLUMN_TERMINAL_INITIAL_METER As Integer = 9
  Private Const GRID_M_COLUMN_TERMINAL_FINAL_METER As Integer = 10
  Private Const GRID_M_COLUMN_TERMINAL_METER_MAX_VALUE As Integer = 11
  Private Const GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER As Integer = 12
  Private Const GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER As Integer = 13
  Private Const GRID_M_COLUMN_TERMINAL_USER_ID As Integer = 14
  Private Const GRID_M_COLUMN_TERMINAL_REASON As Integer = 15
  Private Const GRID_M_COLUMN_TERMINAL_REMARKS As Integer = 16
  Private Const GRID_M_COLUMN_TERMINAL_HAS_ERRORS As Integer = 17

  Private Const GRID_M_COLUMNS As Integer = 18
  Private Const GRID_M_HEADER_ROWS As Integer = 2

  Private Const GRID_M_COLUMN_NONE_WIDTH As Integer = 0
  Private Const GRID_M_COLUMN_INDEX_WIDTH As Integer = 150
  Private Const GRID_M_COLUMN_TERMINAL_METER_NAME_WIDTH As Integer = 5700
  Private Const GRID_M_COLUMN_TERMINAL_INITIAL_METER_WIDTH As Integer = 2160
  Private Const GRID_M_COLUMN_TERMINAL_FINAL_METER_WIDTH As Integer = 2160
  Private Const GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER_WIDTH As Integer = 2160
  Private Const GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER_WIDTH As Integer = 2160

  Private Const GRID_MH_COLUMN_INDEX As Integer = 0
  Private Const GRID_MH_COLUMN_TERMINAL_DATETIME As Integer = 1
  Private Const GRID_MH_COLUMN_TERMINAL_INITIAL_METER As Integer = 2
  Private Const GRID_MH_COLUMN_TERMINAL_FINAL_METER As Integer = 3
  Private Const GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER As Integer = 4
  Private Const GRID_MH_COLUMN_TERMINAL_REASON As Integer = 5
  Private Const GRID_MH_COLUMN_TERMINAL_REMARKS As Integer = 6
  Private Const GRID_MH_COLUMN_TERMINAL_USER_ID As Integer = 7

  Private Const GRID_MH_COLUMNS As Integer = 8
  Private Const GRID_MH_HEADER_ROWS As Integer = 1

  Private Const GRID_MH_COLUMN_INDEX_WIDTH As Integer = 150
  Private Const GRID_MH_COLUMN_TERMINAL_DATETIME_WIDTH As Integer = 2000
  Private Const GRID_MH_COLUMN_TERMINAL_INITIAL_METER_WIDTH As Integer = 2000
  Private Const GRID_MH_COLUMN_TERMINAL_FINAL_METER_WIDTH As Integer = 2000
  Private Const GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER_WIDTH As Integer = 2000
  Private Const GRID_MH_COLUMN_TERMINAL_REASON_WIDTH As Integer = 2000
  Private Const GRID_MH_COLUMN_TERMINAL_REMARKS_WIDTH As Integer = 2620
  Private Const GRID_MH_COLUMN_TERMINAL_USER_ID_WIDTH As Integer = 1500

#End Region ' Constants

#Region " Members "
  Dim m_terminals_meters_data As WSI.Common.ITerminalMeterGroup
  Dim m_prev_selected_meter As Int32
  Dim m_jouney_filter As String
  Dim m_terminal_selected As Int64
  Dim m_terminal_name_filter As String
  Dim m_calculated_system_delta As Int64

  Dim m_working_day_filtered As DateTime
  Dim m_meter_group_filtered As WSI.Common.TerminalMeterGroup.MeterGroups
  Dim m_meter_group_filtered_text As String
  Dim m_terminal_filtered As Int64()
  Dim m_reload_data As Boolean

  ' Can't define color as constant
  Dim COLOR_CELL_HAS_CHANGES As Color = Color.FromArgb(255, 180, 255, 187)
  Dim COLOR_WITHOUT_CHANGES As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
  Dim COLOR_WITHOUT_CHANGES_ENTRY_FIELD As Color = Color.FromArgb(255, 240, 240, 240)
  Dim COLOR_ROW_HAS_ERRORS As Color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_02)
  'LTC  29-OCT-2017
  Private m_current_site As String = ""

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_SAS_METERS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    m_reload_data = False

    Call MyBase.GUI_InitControls()
    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6144)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6151)

    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Me.Permissions.Write

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Type = uc_button.ENUM_BUTTON_TYPE.USER
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Height += 10

    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Journey
    dtp_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)

    ' Meters Group
    Me.cmb_meters_group.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6145)
    LoadComboMetersGroup()

    lbl_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) & ": "
    lbl_meter_group.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) & ": "
    lbl_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(592) & ": "

    'LTC  29-OCT-2017
    If (WSI.Common.Misc.IsMultisiteCenter()) Then
      lbl_site.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773) & ": "
    Else
      lbl_site.Text = ""
    End If

    ' Edition Tab
    tp_sel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6158)
    tp_edit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6159)

    ' Meter
    cmb_meters.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6152)

    ' MaxValue
    Me.lbl_max_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6203, "")

    ' BigIncrement
    Me.lbl_max_increment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6204, "")

    ' Labels
    ef_initial_meter_current.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6215)
    ef_initial_meter_new.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6156)
    lbl_initial.Text = GLB_NLS_GUI_INVOICING.GetString(213)
    lbl_final.Text = GLB_NLS_GUI_INVOICING.GetString(216)
    lbl_rollover_num.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6146)
    lbl_delta.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6147)
    lbl_delta_calculated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6148)
    lbl_delta_system.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6149)

    ef_initial_meter_current.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_initial_meter_new.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_final_meter_current.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_final_meter_new.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_delta_calculated_current.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_delta_calculated_new.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_delta_system_current.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)
    ef_delta_system_new.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, LENGTH_METER)

    ef_initial_meter_current.TextValue = "1"

    btn_reset.Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    ' Reason
    cmb_reason.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6150)
    LoadComboReason()

    lbl_description.Text = GLB_NLS_GUI_CONTROLS.GetString(312)
    Me.txt_remark.MaxLength = MAX_DESC_LEN

    lbl_history_meters_changes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6202)

    GUI_FilterReset()

    ' Load grid style
    Call Me.GUI_StyleSheet()

    ' Load history grid Style
    GUI_StyleSheet_HistoryMeters()

    ' Disable edition controls
    cmb_meters.Enabled = False

    tc_meters.SelectTab(tp_edit)
    tc_meters.SelectTab(tp_sel)

    DisableEditionTabControls(False)
    Call ClearEditionTab()

    'LTC  29-OCT-2017
    ' Sites
    If WSI.Common.Misc.IsMultisiteCenter() Then
      uc_site_select.Visible = True
      uc_site_select.ShowMultisiteRow = False
      uc_site_select.MultiSelect = False
      uc_site_select.Init()
      uc_site_select.SetDefaultValues(False)
    Else
      uc_site_select.Visible = False
    End If

  End Sub

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    ' Journey
    Me.dtp_working_day.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)

    ' Terminals
    ' The 3GS terminals only  must be showed is the S2S feature is enabled
    If WSI.Common.Misc.IsWS2SEnabled Then
      Call Me.uc_provider.Init({WSI.Common.TerminalTypes.SAS_HOST, WSI.Common.TerminalTypes.OFFLINE, WSI.Common.TerminalTypes.T3GS}, GUI_Controls.uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL, IIf(WSI.Common.Misc.IsMultisiteCenter(), "", Nothing))
    Else
      Call Me.uc_provider.Init({WSI.Common.TerminalTypes.SAS_HOST, WSI.Common.TerminalTypes.OFFLINE}, GUI_Controls.uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL, IIf(WSI.Common.Misc.IsMultisiteCenter(), "", Nothing))
    End If


    ' Sites
    ' LTC  31-OCT-2017
    If (WSI.Common.Misc.IsMultisiteCenter()) Then
      Call uc_site_select.Init()
      uc_site_select.SetDefaultValues(False)
    End If

    ' Selected group
    cmb_meters_group.Value = -1

  End Sub ' SetDefaultValues

  ' PURPOSE: Handler to control key pressed intro
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar

      Case Chr(Keys.Enter)

        Return True
    End Select
  End Function

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ExecuteQuery()
    Dim _idx_row As Integer
    Dim _str_filter_site As String

    Try
      m_jouney_filter = String.Empty
      m_terminal_name_filter = String.Empty

      ' Check the filter 
      If Not m_reload_data AndAlso Not GUI_FilterCheck() Then
        Return
      End If

      Me.dg_meters_data.Clear()

      If Not m_reload_data Then
        ' NOTE JML & DHA & DDM: To analyze: in case for a journey have more than one terminal active (clonation). 
        ' You should select which terminal want to work...
        m_terminal_selected = uc_provider.GetTerminalIdListSelected().GetValue(0)

        m_working_day_filtered = dtp_working_day.Value
        m_terminal_filtered = uc_provider.GetTerminalIdListSelected()
        m_meter_group_filtered = cmb_meters_group.Value
        m_meter_group_filtered_text = cmb_meters_group.TextValue

        'LTC  29-OCT-2017
        ' Sites
        m_current_site = uc_site_select.GetSiteIdSelected()

      End If

      m_reload_data = False

      _str_filter_site = String.Empty

      If WSI.Common.Misc.IsMultisiteCenter() Then
        _str_filter_site = uc_site_select.GetSitesIdListSelected()
      End If

      m_terminals_meters_data = WSI.Common.TerminalMeterGroup.LoadMeterData(m_working_day_filtered, m_terminal_filtered, m_meter_group_filtered, _str_filter_site)

      If m_terminals_meters_data.MetersData.Rows.Count > 0 Then

        ' Update filters
        m_jouney_filter = GUI_FormatDate(dtp_working_day.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
        m_terminal_name_filter = uc_provider.GetTerminalName(m_terminal_filtered(0))

        If Not frm_base_sel.ShowHugeNumberOfRows(m_terminals_meters_data.MetersData.Rows.Count) Then
          Exit Sub
        End If

        If Not GUI_DoEvents(Me.dg_meters_data) Then
          Exit Sub
        End If

        For Each _dr As DataRow In m_terminals_meters_data.MetersData.Rows

          If GUI_CheckOutRowBeforeAdd(_dr) Then

            ' add new row
            Me.dg_meters_data.AddRow()
            _idx_row = Me.dg_meters_data.NumRows - 1

            If Not GUI_SetupRow(_idx_row, _dr) Then
              Me.dg_meters_data.DeleteRowFast(_idx_row)
            End If

            ' Added to process the row color whe have changes
            _dr(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX) = _idx_row
            _dr.AcceptChanges()

            'DoEvents run each second.
            If Not GUI_DoEvents(Me.dg_meters_data) Then
              Exit Sub
            End If

          End If

        Next

      End If

      ' Update label
      ' NOTE JML & DHA & DDM: To analyze: in case for a journey have more than one terminal active (clonation). 
      ' You should select which terminal want to work...
      lbl_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723) & ": " & GUI_FormatDate(m_working_day_filtered, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      lbl_meter_group.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420) & ": " & m_meter_group_filtered_text
      lbl_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(592) & ": " & uc_provider.GetTerminalName(m_terminal_selected)
      'LTC  29-OCT-2017
      If (WSI.Common.Misc.IsMultisiteCenter()) Then
        lbl_site.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773) & ": " & uc_site_select.GetSiteIdSelected()
      End If

    Catch

    Finally

      cmb_meters.Value = -1
      ClearEditionTab()
      LoadComboMeters()

    End Try

  End Sub

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        ' Save meters
        If Not m_terminals_meters_data Is Nothing AndAlso m_terminals_meters_data.MetersData.Rows.Count > 0 Then
          ' Validate datatable
          If CheckRowsData(m_terminals_meters_data.MetersData, True, True) Then
            SaveMeterData()
          End If
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_0

        ' "Se van a deshacer todos los ajustes realizados en los contadores. �Desea continuar?"
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6201), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
          ResetAllMetersData()
        End If

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        If DiscardChanges(False) Then
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _max_working_day_allowed As Int32

    _max_working_day_allowed = WSI.Common.GeneralParam.GetInt32("SasMeter", "MetersHistory.MaxWorkingDaysAllowedEdit", 7)

    ' Validate date range
    If WSI.Common.Misc.TodayOpening().AddDays(-_max_working_day_allowed) > dtp_working_day.Value Or dtp_working_day.Value >= WSI.Common.Misc.TodayOpening() Then
      ' "Solo se pueden ajustar los contadores de las �ltimas %1 jornadas (excluyendo la jornada actual)."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6214), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _max_working_day_allowed)
      Call Me.dtp_working_day.Focus()

      Return False
    End If

    ' Meter Group
    If Me.cmb_meters_group.Value = -1 Then
      ' "Se debe seleccionar un grupo de contadores"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6157), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.cmb_meters_group.Focus()

      Return False
    End If

    'LTC  29-OCT-2017
    ' Site Selected
    If WSI.Common.Misc.IsMultisiteCenter() Then
      ' Debe seleccionar un site."
      If uc_site_select.GetSiteIdSelected() = "" Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4921), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_site_select.Focus()
        Return False
      End If
    End If

    ' Terminal Selected
    If Me.uc_provider.GetTerminalIdListSelected().GetValue(0) = -1 Then
      ' Debe seleccionar un terminal."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3451), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.uc_provider.Focus()

      Return False
    End If

    Return True
  End Function  ' GUI_FilterCheck

  'PURPOSE: Executed just before closing form.
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    '' Force to grid to lost focus
    '' to allow to detect changes on uncommited cells
    Me.ActiveControl = Nothing

    If DiscardChanges(True) Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If

  End Sub 'GUI_Closing


#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Load meters group combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub LoadComboMetersGroup()

    Dim _meters As List(Of WSI.Common.TerminalMeterGroup.MeterGroups)

    _meters = New List(Of WSI.Common.TerminalMeterGroup.MeterGroups)

    cmb_meters_group.Clear()

    WSI.Common.TerminalMeterGroup.GetMeterGroups(_meters)

    cmb_meters_group.Add(-1, "") 'Empty combo entry

    For Each _meter As WSI.Common.TerminalMeterGroup.MeterGroups In _meters

      Select Case _meter
        Case WSI.Common.TerminalMeterGroup.MeterGroups.GroupStatistics
          cmb_meters_group.Add(_meter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6288))

        Case WSI.Common.TerminalMeterGroup.MeterGroups.GroupElectronicsFundsTransfer
          cmb_meters_group.Add(_meter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6289))

        Case WSI.Common.TerminalMeterGroup.MeterGroups.GroupTickets
          cmb_meters_group.Add(_meter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6290))

        Case WSI.Common.TerminalMeterGroup.MeterGroups.GroupBills
          cmb_meters_group.Add(_meter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6291))

      End Select

    Next

  End Sub

  ' PURPOSE: Load meters combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub LoadComboMeters()

    RemoveHandler cmb_meters.ValueChangedEvent, AddressOf cmb_meters_ValueChangedEvent

    cmb_meters.Clear()

    cmb_meters.Add(-1, "") 'Empty combo entry

    If Not m_terminals_meters_data Is Nothing Then
      For Each _meter As WSI.Common.Meter In m_terminals_meters_data.Meters
        ' Hidde meters
        If _meter.IsVisible Then
          cmb_meters.Add(Convert.ToInt32(_meter.Code), GetMeterDescription(_meter.Code))
        End If
      Next
    End If

    ' Default selected meter
    cmb_meters.SelectedIndex = -1
    cmb_meters.ResetLastIndex()

    AddHandler cmb_meters.ValueChangedEvent, AddressOf cmb_meters_ValueChangedEvent

    cmb_meters.Enabled = True
    DisableEditionTabControls(False)

  End Sub

  ' PURPOSE: Load reasons combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub LoadComboReason()
    Dim _reasons As List(Of WSI.Common.TerminalMeterGroup.MeterAjustmentReason)

    _reasons = New List(Of WSI.Common.TerminalMeterGroup.MeterAjustmentReason)

    cmb_reason.Clear()

    WSI.Common.TerminalMeterGroup.GetMeterAjustmentReason(_reasons)

    For Each _reason As WSI.Common.TerminalMeterGroup.MeterAjustmentReason In _reasons

      cmb_reason.Add(_reason, GetReasonDescription(_reason))

    Next

  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Private Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As DataRow) As Boolean

    ' Hidde meters
    If Not DbRow.IsNull(WSI.Common.TerminalMeterGroup.COL_METERS.IS_VISIBLE) AndAlso DbRow(WSI.Common.TerminalMeterGroup.COL_METERS.IS_VISIBLE) = 0 Then

      Return False

    End If

    Return True

  End Function

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_BeforeFirstRow_Edit()

    m_calculated_system_delta = 0

    lbl_system_delta_adjustment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6221) & ": " & FormatMeterScreenStringValue(cmb_meters.Value, m_calculated_system_delta, Nothing)

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_AfterLastRow_Edit()

    lbl_system_delta_adjustment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6221) & ": " & FormatMeterScreenStringValue(cmb_meters.Value, m_calculated_system_delta, Nothing)

  End Sub

  ' PURPOSE: Clear edition tab
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ClearEditionTab()

    lbl_new_register.Text = String.Empty
    ef_initial_meter_current.TextValue = String.Empty
    ef_initial_meter_new.TextValue = String.Empty
    ef_final_meter_current.TextValue = String.Empty
    ef_final_meter_new.TextValue = String.Empty
    txt_rollover_current.Minimum = 0
    txt_rollover_new.Minimum = 0
    txt_rollover_current.Value = 0
    txt_rollover_new.Value = 0
    ef_delta_calculated_current.TextValue = String.Empty
    ef_delta_calculated_new.TextValue = String.Empty
    ef_delta_system_current.TextValue = String.Empty
    ef_delta_system_new.TextValue = String.Empty
    cmb_reason.SelectedIndex = 0
    txt_remark.Text = String.Empty
    dg_history_meters.Clear()

    ' Set default colors
    ef_initial_meter_new.TextBackColor = COLOR_WITHOUT_CHANGES
    ef_final_meter_new.TextBackColor = COLOR_WITHOUT_CHANGES
    txt_rollover_current.BackColor = COLOR_WITHOUT_CHANGES
    txt_rollover_new.BackColor = COLOR_WITHOUT_CHANGES
    ef_delta_calculated_new.BackColor = COLOR_WITHOUT_CHANGES_ENTRY_FIELD
    ef_delta_system_new.TextBackColor = COLOR_WITHOUT_CHANGES

    Me.lbl_max_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6203, "")
    Me.lbl_max_increment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6204, "")
    lbl_system_delta_adjustment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6221) & ": "

  End Sub

  ' PURPOSE: Set data to edition tab
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetScreenEditionData()
    Dim _rows_filtered As DataRow()
    Dim _filter As String
    Dim _meter_max_value As Int64
    Dim _big_increment As Int64
    Dim _control_format As GUI_Controls.CLASS_FILTER.ENUM_FORMAT

    ClearEditionTab()

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    _control_format = CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER

    ' Get selected meter data
    _filter = String.Format("meter_code = {0}", cmb_meters.Value)
    _rows_filtered = m_terminals_meters_data.MetersData.Select(_filter)

    If _rows_filtered.Length > 0 Then
      ' Set data grid row selected
      dg_meters_data.ClearSelection()
      dg_meters_data.Row(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX)).IsSelected = True

      ' Enable edition controls
      DisableEditionTabControls(True)

      ' Edit only System Delta
      If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is Nothing AndAlso _
        _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 1 Then
        ef_initial_meter_new.IsReadOnly = True
        ef_final_meter_new.IsReadOnly = True
      End If

      If _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.IS_NEW_ROW) = 1 Then
        lbl_new_register.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6238)
      End If

      Int64.TryParse(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.METER_MAX_VALUE), _meter_max_value)
      Int64.TryParse(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.MAX_INCREMENT), _big_increment)

      'If _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.IS_NEW_ROW) = 1 Then
      If _meter_max_value = 0 Then
        Me.lbl_max_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6203, "---")
      Else
        Me.lbl_max_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6203, FormatMeterScreenStringValue(cmb_meters.Value, _meter_max_value, _control_format))
      End If

      Me.lbl_max_increment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6204, FormatMeterScreenStringValue(cmb_meters.Value, _big_increment, _control_format))

      If _control_format = CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER Then
        ef_initial_meter_current.SetFilter(_control_format, LENGTH_METER, 0)
        ef_initial_meter_new.SetFilter(_control_format, LENGTH_METER, 0)
        ef_final_meter_current.SetFilter(_control_format, LENGTH_METER, 0)
        ef_final_meter_new.SetFilter(_control_format, LENGTH_METER, 0)
        ef_delta_calculated_current.SetFilter(_control_format, LENGTH_METER, 0)
        ef_delta_calculated_new.SetFilter(_control_format, LENGTH_METER, 0)
        ef_delta_system_current.SetFilter(_control_format, LENGTH_METER, 0)
        ef_delta_system_new.SetFilter(_control_format, LENGTH_METER, 0)
      Else
        ef_initial_meter_current.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_initial_meter_new.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_final_meter_current.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_final_meter_new.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_delta_calculated_current.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_delta_calculated_new.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_delta_system_current.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
        ef_delta_system_new.SetFilter(_control_format, LENGTH_DECIMAL_METER, LENGTH_DECIMAL)
      End If

      If _rows_filtered(0).HasVersion(DataRowVersion.Original) Then

        ' When is not Only delta set values initial/final
        If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
            _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then
          ef_initial_meter_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE, DataRowVersion.Original), _control_format)
          ef_final_meter_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE, DataRowVersion.Original), _control_format)
        End If

        ef_delta_system_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM, DataRowVersion.Original), _control_format)
        txt_rollover_current.Value = FormatMeterScreenValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER, DataRowVersion.Original), _control_format)
      Else
        ' When is not Only delta set values initial/final
        If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
            _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then
          ef_initial_meter_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), _control_format)
          ef_final_meter_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), _control_format)
        End If

        ef_delta_system_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), _control_format)
        txt_rollover_current.Value = FormatMeterScreenValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER), _control_format)

      End If

      ' When is not Only delta set values initial/final
      If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then
        If _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) Is DBNull.Value Then
          ef_initial_meter_new.TextValue = ""
        Else
          ef_initial_meter_new.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), _control_format)
        End If

        If _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) Is DBNull.Value Then
          ef_final_meter_new.TextValue = ""
        Else
          ef_final_meter_new.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), _control_format)
        End If

      End If

      txt_rollover_new.Value = _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER)

      If _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) Is DBNull.Value Then
        ef_delta_system_new.TextValue = ""
      Else
        ef_delta_system_new.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), _control_format)
      End If

      cmb_reason.SelectedIndex = _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.REASON)
      txt_remark.Text = _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS)

      CalculateDeltaMeters()

    Else
      ' Disable edition controls
      DisableEditionTabControls(False)

    End If

    SetControlsBackgroundColor()

    GUI_ExecuteQuery_History()

  End Sub

  ' PURPOSE: Get data from edition tab
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GetScreenMeterData()
    Dim _rows_filtered As DataRow()
    Dim _filter As String

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    _filter = String.Format("meter_code = {0}", cmb_meters.Value)
    _rows_filtered = m_terminals_meters_data.MetersData.Select(_filter)

    If _rows_filtered.Length > 0 Then

      ' When is not Only delta set values initial/final
      If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

        If String.IsNullOrEmpty(ef_initial_meter_new.TextValue) Then
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) = DBNull.Value
        Else
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) = CastFormatMeterScreenValue(cmb_meters.Value, ef_initial_meter_new.TextValue)
        End If

        If String.IsNullOrEmpty(ef_final_meter_new.TextValue) Then
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) = DBNull.Value
        Else
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) = CastFormatMeterScreenValue(cmb_meters.Value, ef_final_meter_new.TextValue)
        End If

        _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER) = txt_rollover_new.Value

        If String.IsNullOrEmpty(ef_delta_calculated_new.TextValue) Then
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) = DBNull.Value
        Else
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) = CastFormatMeterScreenValue(cmb_meters.Value, ef_delta_calculated_new.TextValue)
        End If

      End If

      If String.IsNullOrEmpty(ef_delta_system_new.TextValue) Then
        _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) = DBNull.Value
      Else
        _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) = CastFormatMeterScreenValue(cmb_meters.Value, ef_delta_system_new.TextValue)
      End If

      _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.REASON) = cmb_reason.SelectedIndex
      _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS) = txt_remark.Text
      _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.USER_ID) = CurrentUser.Id

      If Not CheckRowData(_rows_filtered(0), False) Then
        Me.dg_meters_data.Cell(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX), GRID_M_COLUMN_TERMINAL_HAS_ERRORS).Value = 1
      Else
        Me.dg_meters_data.Cell(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX), GRID_M_COLUMN_TERMINAL_HAS_ERRORS).Value = 0
      End If

    End If
  End Sub

  ' PURPOSE: Undo selected meter changes
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ResetSelectedMeterData()
    Dim _rows_filtered As DataRow()
    Dim _filter As String
    Dim _idx_row As Int32

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    _filter = String.Format("meter_code = {0}", cmb_meters.Value)
    _rows_filtered = m_terminals_meters_data.MetersData.Select(_filter)

    If _rows_filtered.Length > 0 Then

      _rows_filtered(0).RejectChanges()

      ' Update grid data
      _idx_row = _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX)
      UpdateGridMetersData(_idx_row, _rows_filtered(0))

    End If
  End Sub

  ' PURPOSE: Undo meters changes
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ResetAllMetersData()
    Dim _data_rows As DataRow()
    Dim _filter As String

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    m_terminals_meters_data.MetersData.RejectChanges()

    ' Update grid rows data and color
    For _idx_row As Int32 = 0 To dg_meters_data.NumRows - 1
      _filter = "grid_index = " & _idx_row
      _data_rows = m_terminals_meters_data.MetersData.Select(_filter)

      If _data_rows.Length > 0 Then
        UpdateGridMetersData(_idx_row, _data_rows(0))
      End If
    Next

    SetScreenEditionData()

  End Sub

  ' PURPOSE: Calculate delta meters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CalculateDeltaMeters()
    Dim _rows_filtered As DataRow()
    Dim _filter As String
    Dim _terminal_id As Int32
    Dim _initial_meter_current_int64 As Int64
    Dim _final_meter_current_int64 As Int64
    Dim _delta_calculated_meter_current As Int64
    Dim _delta_system_meter_current As Int64
    Dim _rollover_current As Int32
    Dim _initial_meter_new_int64 As Int64
    Dim _final_meter_new_int64 As Int64
    Dim _delta_calculated_meter_new_int64 As Int64
    Dim _rollover_new As Int32
    Dim _max_value_meter As Decimal
    Dim _max_value_meter_int64 As Int64
    Dim _has_rollover_current As Boolean
    Dim _has_rollover_new As Boolean
    Dim _max_units_increment As Int64
    Dim _unit_increment As Int32

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    _unit_increment = 1
    _filter = String.Format("meter_code = {0}", cmb_meters.Value)
    _rows_filtered = m_terminals_meters_data.MetersData.Select(_filter)

    If _rows_filtered.Length > 0 Then

      ' When is not Only delta set values initial/final
      If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

        ' NOTE JML & DHA & DDM: To analyze: in case for a journey have more than one terminal active (clonation).
        ' You should select which terminal want to work...
        _terminal_id = m_terminal_selected
        _initial_meter_current_int64 = 0
        _final_meter_current_int64 = 0
        _delta_calculated_meter_current = 0
        _delta_system_meter_current = 0
        _rollover_current = 0
        _initial_meter_new_int64 = 0
        _final_meter_new_int64 = 0
        _delta_calculated_meter_new_int64 = 0
        _rollover_new = 0
        _max_units_increment = 0

        _max_value_meter_int64 = _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.METER_MAX_VALUE)
        _max_value_meter = FormatMeterScreenValue(cmb_meters.Value, _max_value_meter_int64, Nothing)

        ' Validate max value for initial and final meter
        If Not String.IsNullOrEmpty(ef_initial_meter_new.TextValue) AndAlso _max_value_meter > 0 AndAlso ef_initial_meter_new.TextValue >= _max_value_meter - _unit_increment Then
          ef_initial_meter_new.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _max_value_meter_int64 - _unit_increment, Nothing)
        End If

        If Not String.IsNullOrEmpty(ef_final_meter_new.TextValue) AndAlso _max_value_meter > 0 AndAlso ef_final_meter_new.TextValue >= _max_value_meter - _unit_increment Then
          ef_final_meter_new.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _max_value_meter_int64 - _unit_increment, Nothing)
        End If

        _initial_meter_current_int64 = CastFormatMeterScreenValue(cmb_meters.Value, ef_initial_meter_current.TextValue)
        _final_meter_current_int64 = CastFormatMeterScreenValue(cmb_meters.Value, ef_final_meter_current.TextValue)
        Int32.TryParse(txt_rollover_current.Value, _rollover_current)

        ' Get deltas values
        WSI.Common.TerminalMeterGroup.GetDeltaMeter(_terminal_id, cmb_meters.Value, "", _initial_meter_current_int64, _final_meter_current_int64, _max_value_meter_int64, _delta_calculated_meter_current, _has_rollover_current, _max_units_increment)

        ' Calculate current rollover
        If _has_rollover_current And _rollover_current = 0 Then
          _rollover_current = 1
        End If

        ' Define minium range current rollover
        If _has_rollover_current Then
          txt_rollover_current.Minimum = 1
        Else
          txt_rollover_current.Minimum = 0
        End If

        txt_rollover_current.Value = _rollover_current

        If _has_rollover_current Then
          _rollover_current = Math.Max(0, _rollover_current - 1)
        End If

        ' Calculate current delta
        ef_delta_calculated_current.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _delta_calculated_meter_current + _rollover_current * _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.METER_MAX_VALUE), Nothing)

        If Not String.IsNullOrEmpty(ef_initial_meter_new.TextValue) And Not String.IsNullOrEmpty(ef_final_meter_new.TextValue) Then
          _initial_meter_new_int64 = CastFormatMeterScreenValue(cmb_meters.Value, ef_initial_meter_new.TextValue)
          _final_meter_new_int64 = CastFormatMeterScreenValue(cmb_meters.Value, ef_final_meter_new.TextValue)
          Int32.TryParse(txt_rollover_new.Value, _rollover_new)

          ' Get deltas values
          WSI.Common.TerminalMeterGroup.GetDeltaMeter(_terminal_id, cmb_meters.Value, "", _initial_meter_new_int64, _final_meter_new_int64, _max_value_meter_int64, _delta_calculated_meter_new_int64, _has_rollover_new, _max_units_increment)

          ' Calculate current rollover
          If _has_rollover_new And _rollover_new = 0 Then
            _rollover_new = 1
          End If

          ' Define minium range new rollover
          If _has_rollover_new Then
            txt_rollover_new.Minimum = 1
          Else
            txt_rollover_new.Minimum = 0
          End If

          txt_rollover_new.Value = _rollover_new

          ' Only update when fields are differents
          If _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER) <> _rollover_new Then
            _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER) = _rollover_new
          End If

          If _has_rollover_new Then
            _rollover_new = Math.Max(0, _rollover_new - 1)
          End If

          ' Calculate new delta
          ef_delta_calculated_new.TextValue = FormatMeterScreenStringValue(cmb_meters.Value, _delta_calculated_meter_new_int64 + _rollover_new * _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.METER_MAX_VALUE), Nothing)

          If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) Is DBNull.Value AndAlso _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) <> _delta_calculated_meter_new_int64 + _rollover_new * _max_value_meter Then
            _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) = _delta_calculated_meter_new_int64 + _rollover_new * _max_value_meter
          End If

        Else
          ef_delta_calculated_new.TextValue = String.Empty
          _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) = DBNull.Value
        End If

        UpdateGridMetersData(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX), _rows_filtered(0))

      End If
    End If

  End Sub

  ' PURPOSE: Add data rows to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None 
  '
  Private Function GUI_SetupRow(ByVal IdxRow As Integer, _
                                ByVal DRow As DataRow) As Boolean

    Dim _terminal_id As Int32
    Dim _meter_code As Int64
    Dim _meter_max_value As Int64
    Dim _delta_meter As Int64
    Dim _has_rollover As Boolean
    Dim _max_units_increment As Int64

    _terminal_id = 0
    _meter_code = 0
    _meter_max_value = 0
    _delta_meter = 0
    _has_rollover = False
    _max_units_increment = 0

    ' GAMING_DAY
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_GAMING_DAY).Value = GUI_FormatDate(DRow(WSI.Common.TerminalMeterGroup.COL_METERS.GAMING_DAY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' TERMINAL ID
    Int32.TryParse(DRow(WSI.Common.TerminalMeterGroup.COL_METERS.TERMINAL_ID), _terminal_id)
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_ID).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.TERMINAL_ID)

    ' METER CODE
    Int64.TryParse(DRow(WSI.Common.TerminalMeterGroup.COL_METERS.METER_CODE), _meter_code)
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_METER_CODE).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.METER_CODE)

    ' METER NAME
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_METER_NAME).Value = GetMeterDescription(_meter_code)

    ' GAME ID
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_GAME_ID).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.GAME_ID)

    ' DENOMINATION
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DENOMINATION).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.DENOMINATION)

    ' TYPE
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_TYPE).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.TYPE)

    ' SUB TYPE
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_SUB_TYPE).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.SUB_TYPE)

    ' When is not Only delta set values initial/final/calculate delta
    If Not DRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
        DRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

      ' INITIAL METER
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_INITIAL_METER).Value = FormatMeterScreenStringValue(_meter_code, DRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), Nothing)

      ' FINAL METER
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_FINAL_METER).Value = FormatMeterScreenStringValue(_meter_code, DRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), Nothing)

      ' DELTA CALCULATED METER
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Value = FormatMeterScreenStringValue(_meter_code, DRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE), Nothing)

    End If

    ' METER MAX VALUE
    Int64.TryParse(DRow(WSI.Common.TerminalMeterGroup.COL_METERS.METER_MAX_VALUE), _meter_max_value)
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_METER_MAX_VALUE).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.METER_MAX_VALUE)

    ' DELTA SYSTEM METER
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Value = FormatMeterScreenStringValue(_meter_code, DRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), Nothing)

    ' USER ID
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_USER_ID).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.USER_ID)

    ' REASON
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_REASON).Value = GetReasonDescription(DRow(WSI.Common.TerminalMeterGroup.COL_METERS.REASON))

    ' REMARK
    If Not IsDBNull(DRow(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS)) Then
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_REMARKS).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS)
    End If

    ' HAS ERRORS
    Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_HAS_ERRORS).Value = 0

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Add data rows to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None 
  '
  Private Function GUI_SetupRow_History(ByVal IdxRow As Integer, _
                                ByVal DRow As DataRow) As Boolean

    Dim _rows_filtered As DataRow()
    Dim _filter As String


    _filter = String.Format("meter_code = {0}", cmb_meters.Value)
    _rows_filtered = m_terminals_meters_data.MetersData.Select(_filter)

    If _rows_filtered.Length = 0 Then
      Return False
    End If

    ' When is not Only delta set values initial/final
    If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
        _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

      ' INICIAL
      Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_INITIAL_METER).Value = HistoryCellFormatOldNewValue(cmb_meters.Value, DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.OLD_INITIAL_VALUE), DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.NEW_INITIAL_VALUE))

      ' FINAL
      Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_FINAL_METER).Value = HistoryCellFormatOldNewValue(cmb_meters.Value, DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.OLD_FINAL_VALUE), DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.NEW_FINAL_VALUE))
    End If

    ' SYSTEM DELTA
    Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER).Value = HistoryCellFormatOldNewValue(cmb_meters.Value, DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.OLD_DELTA_CALCULATE), DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.NEW_DELTA_CALCULATE))

    ' REASON
    Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_REASON).Value = GetReasonDescription(DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.REASON))

    ' DESCRIPTION
    Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_REMARKS).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.REMARKS)

    ' USER
    Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_USER_ID).Value = DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.USER_NAME)

    ' DATETIME
    Me.dg_history_meters.Cell(IdxRow, GRID_MH_COLUMN_TERMINAL_DATETIME).Value = GUI_FormatDate(DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    If (DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.TYPE) = WSI.Common.TerminalMeterGroup.MetersAdjustmentsType.System) _
      AndAlso (DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.SUB_TYPE) = WSI.Common.TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement) Then

      m_calculated_system_delta += DRow(WSI.Common.TerminalMeterGroup.COL_METERS_HISTORY.OLD_DELTA_CALCULATE)
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set cell format for history grid values
  '
  '  PARAMS:
  '     - INPUT:
  '           - Int64: MeterCode
  '           - Object: OldValue
  '           - Object: NewValue
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String: formated string 
  '
  Private Function HistoryCellFormatOldNewValue(ByVal MeterCode As Int64, ByVal OldValue As Object, ByVal NewValue As Object) As String
    Dim _cell_data As String

    _cell_data = String.Empty

    If OldValue <> NewValue Then
      _cell_data = String.Format("{0} ({1} {2})", FormatMeterScreenStringValue(MeterCode, NewValue, Nothing), GLB_NLS_GUI_CONFIGURATION.GetString(83), FormatMeterScreenStringValue(MeterCode, OldValue, Nothing))
    Else
      _cell_data = FormatMeterScreenStringValue(MeterCode, NewValue, Nothing)
    End If

    Return _cell_data

  End Function

  ' PURPOSE: Get meters description
  '
  '  PARAMS:
  '     - INPUT:
  '           - Int64: MeterCode
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - String: MeterDescription
  '
  Private Function GetMeterDescription(ByVal MeterCode As Int32) As String
    Dim _meter_description As String

    _meter_description = String.Empty

    If MeterCode > EVENT_START_BASE Then
      _meter_description = GLB_NLS_GUI_PLAYER_TRACKING.GetString((MeterCode - EVENT_START_BASE) + NLS_EVENTS)
    Else
      _meter_description = WSI.Common.Resource.String(String.Format("STR_SAS_METER_DESC_{0:00000}", MeterCode))
    End If

    Return VisibleMeterCode(Hex(MeterCode)) + SEPARATOR + _meter_description

  End Function ' GetMeterDescription

  ' PURPOSE: Get reason description
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalMeterGroup.MeterAjustmentReason: ReasonCode
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - String: MeterDescription
  '
  Private Function GetReasonDescription(ByVal ReasonCode As WSI.Common.TerminalMeterGroup.MeterAjustmentReason) As String
    Dim _reason_description As String

    _reason_description = String.Empty

    Select Case ReasonCode

      Case WSI.Common.TerminalMeterGroup.MeterAjustmentReason.others
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(6292)

    End Select

    Return String.Empty

  End Function ' GetReasonDescription

  ' PURPOSE: Get meter formated to hexadecimal
  '
  '  PARAMS:
  '     - INPUT:
  '           - String: HexCode
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - String: hexadecimal value
  Private Function VisibleMeterCode(ByVal HexCode As String) As String
    Dim _code As String

    _code = HexCode.PadLeft(5, "0").Trim()
    _code = _code.Substring(_code.Length - VISIBLE_CODE_LENGTH)

    Return _code
  End Function

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.dg_meters_data
      Call .Init(GRID_M_COLUMNS, GRID_M_HEADER_ROWS)

      ' INDEX
      .Column(GRID_M_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_M_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_M_COLUMN_INDEX).Width = GRID_M_COLUMN_INDEX_WIDTH
      .Column(GRID_M_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_M_COLUMN_INDEX).IsColumnPrintable = False

      ' GAMING DAY
      .Column(GRID_M_COLUMN_GAMING_DAY).Header(0).Text = ""
      .Column(GRID_M_COLUMN_GAMING_DAY).Header(1).Text = " "
      .Column(GRID_M_COLUMN_GAMING_DAY).Width = 0
      .Column(GRID_M_COLUMN_GAMING_DAY).HighLightWhenSelected = False
      .Column(GRID_M_COLUMN_GAMING_DAY).IsColumnPrintable = False

      ' TERMINAL ID
      .Column(GRID_M_COLUMN_TERMINAL_ID).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_ID).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      ' METER CODE
      .Column(GRID_M_COLUMN_TERMINAL_METER_CODE).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_METER_CODE).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_METER_CODE).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_METER_CODE).HighLightWhenSelected = False
      .Column(GRID_M_COLUMN_TERMINAL_METER_CODE).IsColumnPrintable = False

      ' METER DESCRIPTION
      .Column(GRID_M_COLUMN_TERMINAL_METER_NAME).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_METER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6152)
      .Column(GRID_M_COLUMN_TERMINAL_METER_NAME).Width = GRID_M_COLUMN_TERMINAL_METER_NAME_WIDTH
      .Column(GRID_M_COLUMN_TERMINAL_METER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GAME ID
      .Column(GRID_M_COLUMN_TERMINAL_GAME_ID).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_GAME_ID).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_GAME_ID).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_GAME_ID).IsColumnPrintable = False

      ' DENOMINATION
      .Column(GRID_M_COLUMN_TERMINAL_DENOMINATION).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_DENOMINATION).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_DENOMINATION).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_DENOMINATION).IsColumnPrintable = False

      ' TYPE
      .Column(GRID_M_COLUMN_TERMINAL_TYPE).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_TYPE).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_TYPE).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_TYPE).IsColumnPrintable = False

      ' SUB TYPE
      .Column(GRID_M_COLUMN_TERMINAL_SUB_TYPE).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_SUB_TYPE).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_SUB_TYPE).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_SUB_TYPE).IsColumnPrintable = False

      ' INITIAL METER
      .Column(GRID_M_COLUMN_TERMINAL_INITIAL_METER).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_INITIAL_METER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(213)
      .Column(GRID_M_COLUMN_TERMINAL_INITIAL_METER).Width = GRID_M_COLUMN_TERMINAL_INITIAL_METER_WIDTH
      .Column(GRID_M_COLUMN_TERMINAL_INITIAL_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_M_COLUMN_TERMINAL_INITIAL_METER).HighLightWhenSelected = False

      ' FINAL METER
      .Column(GRID_M_COLUMN_TERMINAL_FINAL_METER).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_FINAL_METER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(216)
      .Column(GRID_M_COLUMN_TERMINAL_FINAL_METER).Width = GRID_M_COLUMN_TERMINAL_INITIAL_METER_WIDTH
      .Column(GRID_M_COLUMN_TERMINAL_FINAL_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_M_COLUMN_TERMINAL_FINAL_METER).HighLightWhenSelected = False

      ' METER MAX VALUE
      .Column(GRID_M_COLUMN_TERMINAL_METER_MAX_VALUE).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_METER_MAX_VALUE).Header(1).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_METER_MAX_VALUE).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_METER_MAX_VALUE).IsColumnPrintable = False

      ' DELTA CALCULATED METER
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6147)
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6148)
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Width = GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER_WIDTH
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).HighLightWhenSelected = False

      ' DELTA SYSTEM METER
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6147)
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6149)
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Width = GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER_WIDTH
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).HighLightWhenSelected = False

      ' USER ID
      .Column(GRID_M_COLUMN_TERMINAL_USER_ID).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_USER_ID).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_USER_ID).IsColumnPrintable = False

      ' REASON
      .Column(GRID_M_COLUMN_TERMINAL_REASON).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_REASON).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_REASON).IsColumnPrintable = False

      ' REMARKS
      .Column(GRID_M_COLUMN_TERMINAL_REMARKS).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_REMARKS).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_REMARKS).IsColumnPrintable = False

      ' HAS ERRORS
      .Column(GRID_M_COLUMN_TERMINAL_HAS_ERRORS).Header(0).Text = " "
      .Column(GRID_M_COLUMN_TERMINAL_HAS_ERRORS).Width = 0
      .Column(GRID_M_COLUMN_TERMINAL_HAS_ERRORS).IsColumnPrintable = False

    End With

  End Sub                    ' GUI_StyleSheet

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet_HistoryMeters()

    With Me.dg_history_meters
      Call .Init(GRID_MH_COLUMNS, GRID_MH_HEADER_ROWS)

      ' Index
      .Column(GRID_MH_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_MH_COLUMN_INDEX).Width = GRID_MH_COLUMN_INDEX_WIDTH
      .Column(GRID_MH_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_MH_COLUMN_INDEX).IsColumnPrintable = False

      ' Date
      .Column(GRID_MH_COLUMN_TERMINAL_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6155)
      .Column(GRID_MH_COLUMN_TERMINAL_DATETIME).Width = GRID_MH_COLUMN_TERMINAL_DATETIME_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Initial
      .Column(GRID_MH_COLUMN_TERMINAL_INITIAL_METER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(213)
      .Column(GRID_MH_COLUMN_TERMINAL_INITIAL_METER).Width = GRID_MH_COLUMN_TERMINAL_INITIAL_METER_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_INITIAL_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Final
      .Column(GRID_MH_COLUMN_TERMINAL_FINAL_METER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(216)
      .Column(GRID_MH_COLUMN_TERMINAL_FINAL_METER).Width = GRID_MH_COLUMN_TERMINAL_FINAL_METER_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_FINAL_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' System Delta
      .Column(GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6149)
      .Column(GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER).Width = GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_CALCULATED_DELTA_METER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Reason
      .Column(GRID_MH_COLUMN_TERMINAL_REASON).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6150)
      .Column(GRID_MH_COLUMN_TERMINAL_REASON).Width = GRID_MH_COLUMN_TERMINAL_REASON_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_REASON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(GRID_MH_COLUMN_TERMINAL_REMARKS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(312)
      .Column(GRID_MH_COLUMN_TERMINAL_REMARKS).Width = GRID_MH_COLUMN_TERMINAL_REMARKS_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_REMARKS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' User
      .Column(GRID_MH_COLUMN_TERMINAL_USER_ID).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(326)
      .Column(GRID_MH_COLUMN_TERMINAL_USER_ID).Width = GRID_MH_COLUMN_TERMINAL_USER_ID_WIDTH
      .Column(GRID_MH_COLUMN_TERMINAL_USER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub                    ' GUI_StyleSheet

  ' PURPOSE: Save meters data 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SaveMeterData()
    Dim _dt_changes As DataTable
    Dim _prev_selected_meter As Int32
    Dim _error_code As WSI.Common.TerminalMeterGroup.ResultCode

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    _dt_changes = m_terminals_meters_data.MetersData.GetChanges()
    If (Not _dt_changes Is Nothing) AndAlso (_dt_changes.Rows.Count > 0) Then

      'LTC  29-OCT-2017
      m_terminals_meters_data.SiteId = m_current_site
      _error_code = m_terminals_meters_data.UpdateMeters()

      If _error_code = WSI.Common.TerminalMeterGroup.ResultCode.ERROR_GENERIC Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853), _
                      ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        Return
      ElseIf _error_code = WSI.Common.TerminalMeterGroup.ResultCode.ERROR_NEVER_ACTIVITY Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6245), _
                      ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                      m_terminal_name_filter)
        Return
      End If

      AuditChanges(_dt_changes)

      ' Refresh form
      _prev_selected_meter = cmb_meters.Value
      m_reload_data = True
      Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
      cmb_meters.Value = _prev_selected_meter

    End If

  End Sub

  ' PURPOSE: If there are pending changes in Data Grids, show a MsgBox asking user to discard changes or not.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True:  If discard changes
  '     - False: If NOT discard changes
  Private Function DiscardChanges(ByVal ExitForm As Boolean) As Boolean
    Dim _data_table_changes As DataTable
    Dim _msg_id As Int32

    If ExitForm Then
      _msg_id = 101
    Else
      _msg_id = 122
    End If

    ' If pending changes MsgBox
    If Not m_terminals_meters_data Is Nothing AndAlso Not IsNothing(m_terminals_meters_data.MetersData) Then
      _data_table_changes = m_terminals_meters_data.MetersData.GetChanges

      If Not IsNothing(_data_table_changes) Then
        If _data_table_changes.Rows.Count > 0 Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(_msg_id), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return False
          End If
        End If
      End If
    End If

    Return True
  End Function ' DiscardChanges

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_ExecuteQuery_History()
    Dim _idx_row As Integer
    Dim _redraw As Boolean
    Dim _rows As DataRow()
    Dim _filter As String

    Try

      _redraw = Me.dg_history_meters.Redraw
      Me.dg_history_meters.Redraw = False

      Me.dg_history_meters.Clear()

      Call GUI_BeforeFirstRow_Edit()

      If Not m_terminals_meters_data.HistoryMetersData Is Nothing AndAlso m_terminals_meters_data.HistoryMetersData.Rows.Count > 0 Then

        _filter = String.Format("METER_CODE = {0}", cmb_meters.Value)
        _rows = m_terminals_meters_data.HistoryMetersData.Select(_filter)

        If Not GUI_DoEvents(Me.dg_history_meters) Then
          Exit Sub
        End If

        For Each _dr As DataRow In _rows
          ' add new row
          Me.dg_history_meters.AddRow()
          _idx_row = Me.dg_history_meters.NumRows - 1

          If Not GUI_SetupRow_History(_idx_row, _dr) Then
            Me.dg_history_meters.DeleteRowFast(_idx_row)
          End If

          ' JAB 12-JUN-2012: DoEvents run each second.
          If Not GUI_DoEvents(Me.dg_history_meters) Then
            Exit Sub
          End If

        Next

        GUI_AfterLastRow_Edit()

      End If
    Catch

    Finally
      If Not Me.IsDisposed Then
        Me.dg_history_meters.Redraw = _redraw
      End If
    End Try

  End Sub

  ' PURPOSE: Update meters grid with modified data
  '
  '  PARAMS:
  '     - INPUT:
  '           - Int32: IdxRow
  '           - DataRow: DataRow
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub UpdateGridMetersData(ByVal IdxRow As Int32, ByVal DataRow As DataRow)
    Dim _has_changes As Boolean
    Dim _meter_code As Int64

    _meter_code = DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.METER_CODE)
    _has_changes = False

    If DataRow.RowState <> DataRowState.Unchanged Then

      ' When is not Only delta set values initial/final
      If Not DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
          DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_INITIAL_METER).BackColor = COLOR_WITHOUT_CHANGES
        If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) Is DBNull.Value OrElse DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) Then
          _has_changes = True
          Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_INITIAL_METER).BackColor = COLOR_CELL_HAS_CHANGES
        End If

        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_FINAL_METER).BackColor = COLOR_WITHOUT_CHANGES
        If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) Is DBNull.Value OrElse DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) Then
          _has_changes = True
          Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_FINAL_METER).BackColor = COLOR_CELL_HAS_CHANGES
        End If

        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).BackColor = COLOR_WITHOUT_CHANGES
        If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) Is DBNull.Value OrElse DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) Then
          _has_changes = True
          Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).BackColor = COLOR_CELL_HAS_CHANGES
        End If

      End If

      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).BackColor = COLOR_WITHOUT_CHANGES
      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) Is DBNull.Value OrElse DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) Then
        _has_changes = True
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).BackColor = COLOR_CELL_HAS_CHANGES
      End If

      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER) Then
        _has_changes = True
      End If

      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.REASON, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.REASON) Then
        _has_changes = True
      End If

      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS, DataRowVersion.Original) <> DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS) Then
        _has_changes = True
      End If
    End If

    ' If all cells have the same value but the RowState is different "Unchanged" undo changes
    If Not _has_changes Then
      Me.dg_meters_data.Row(IdxRow).BackColor = COLOR_WITHOUT_CHANGES
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_INDEX).BackColor = COLOR_WITHOUT_CHANGES
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_INITIAL_METER).BackColor = COLOR_WITHOUT_CHANGES
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_FINAL_METER).BackColor = COLOR_WITHOUT_CHANGES
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).BackColor = COLOR_WITHOUT_CHANGES
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).BackColor = COLOR_WITHOUT_CHANGES

      DataRow.RejectChanges()
    Else
      If Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_HAS_ERRORS).Value = 1 Then
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_INDEX).BackColor = COLOR_ROW_HAS_ERRORS
      Else
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_INDEX).BackColor = COLOR_CELL_HAS_CHANGES
      End If
    End If

    ' When is not Only delta set values initial/final
    If Not DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
        DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) Is DBNull.Value Then
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_INITIAL_METER).Value = String.Empty
      Else
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_INITIAL_METER).Value = FormatMeterScreenStringValue(_meter_code, DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), Nothing)
      End If

      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) Is DBNull.Value Then
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_FINAL_METER).Value = String.Empty
      Else
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_FINAL_METER).Value = FormatMeterScreenStringValue(_meter_code, DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), Nothing)
      End If

      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE) Is DBNull.Value Then
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Value = String.Empty
      Else
        Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_CALCULATED_METER).Value = FormatMeterScreenStringValue(_meter_code, DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE), Nothing)
      End If

    End If

    If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) Is DBNull.Value Then
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Value = String.Empty
    Else
      Me.dg_meters_data.Cell(IdxRow, GRID_M_COLUMN_TERMINAL_DELTA_SYSTEM_METER).Value = FormatMeterScreenStringValue(_meter_code, DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), Nothing)
    End If

  End Sub

  ' PURPOSE: Set control background
  '
  '  PARAMS:
  '     - INPUT:
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetControlsBackgroundColor()
    Dim _rows_filtered As DataRow()
    Dim _filter As String
    Dim _cast_int32 As Int32
    Dim _cast_meters As Double
    Dim _idx_row As Int32

    _idx_row = -1

    ef_initial_meter_new.TextBackColor = COLOR_WITHOUT_CHANGES
    ef_final_meter_new.TextBackColor = COLOR_WITHOUT_CHANGES
    txt_rollover_current.BackColor = COLOR_WITHOUT_CHANGES
    txt_rollover_new.BackColor = COLOR_WITHOUT_CHANGES
    ef_delta_calculated_new.BackColor = COLOR_WITHOUT_CHANGES_ENTRY_FIELD
    ef_delta_system_new.TextBackColor = COLOR_WITHOUT_CHANGES

    If m_terminals_meters_data Is Nothing Then
      Return
    End If

    _filter = String.Format("meter_code = {0}", cmb_meters.Value)
    _rows_filtered = m_terminals_meters_data.MetersData.Select(_filter)

    If _rows_filtered.Length > 0 Then

      _idx_row = _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.GRID_INDEX)

      If _rows_filtered(0).HasVersion(DataRowVersion.Original) Then

        ' When is not Only delta set values initial/final
        If Not _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
            _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then

          _cast_meters = FormatMeterScreenValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE, DataRowVersion.Original), Nothing)
          If String.IsNullOrEmpty(ef_initial_meter_new.TextValue) OrElse Not String.IsNullOrEmpty(ef_initial_meter_new.TextValue) AndAlso _cast_meters <> ef_initial_meter_new.TextValue Then
            ef_initial_meter_new.TextBackColor = COLOR_CELL_HAS_CHANGES
          End If

          _cast_meters = FormatMeterScreenValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE, DataRowVersion.Original), Nothing)
          If String.IsNullOrEmpty(ef_final_meter_new.TextValue) OrElse Not String.IsNullOrEmpty(ef_final_meter_new.TextValue) AndAlso _cast_meters <> ef_final_meter_new.TextValue Then
            ef_final_meter_new.TextBackColor = COLOR_CELL_HAS_CHANGES
          End If

          _cast_meters = FormatMeterScreenValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_CALCULATE, DataRowVersion.Original), Nothing)
          If String.IsNullOrEmpty(ef_delta_calculated_new.TextValue) OrElse Not String.IsNullOrEmpty(ef_delta_calculated_new.TextValue) AndAlso _cast_meters <> ef_delta_calculated_new.TextValue Then
            ef_delta_calculated_new.BackColor = COLOR_CELL_HAS_CHANGES
          End If

        End If

        Int32.TryParse(_rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.ROLL_OVER, DataRowVersion.Original), _cast_int32)
        If Not String.IsNullOrEmpty(txt_rollover_new.Value) AndAlso _cast_int32 <> txt_rollover_new.Value Then
          txt_rollover_new.BackColor = COLOR_CELL_HAS_CHANGES
        End If

        _cast_meters = FormatMeterScreenValue(cmb_meters.Value, _rows_filtered(0).Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM, DataRowVersion.Original), Nothing)
        If String.IsNullOrEmpty(ef_delta_system_new.Value) OrElse Not String.IsNullOrEmpty(ef_delta_system_new.TextValue) AndAlso _cast_meters <> ef_delta_system_new.TextValue Then
          ef_delta_system_new.TextBackColor = COLOR_CELL_HAS_CHANGES
        End If
      End If

      ' Set Grid row color and data if there are changes
      UpdateGridMetersData(_idx_row, _rows_filtered(0))

    End If

  End Sub

  ' PURPOSE: Audit changes
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataTable: TerminalsMetersData
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AuditChanges(ByVal TerminalsMetersData As DataTable)
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _original_value As String
    Dim _current_value As String
    Dim _field_text As String
    Dim _meter_code As Int32
    Dim _meter_name As String

    If IsNothing(TerminalsMetersData) Then
      Return
    End If

    _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_METERS)
    _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_METERS)

    ' Set Name and Identifier
    _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6144), "")
    _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6144), "")

    For Each _row As DataRow In TerminalsMetersData.Rows

      _meter_code = _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.METER_CODE)
      _meter_name = GetMeterDescription(_meter_code)

      ' Initial meter
      If _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE, DataRowVersion.Original) <> _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) Then
        _field_text = GLB_NLS_GUI_INVOICING.GetString(213)
        _original_value = FormatMeterScreenStringValue(_meter_code, _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE, DataRowVersion.Original), Nothing)
        _current_value = FormatMeterScreenStringValue(_meter_code, _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), Nothing)

        'LTC  29-OCT-2017
        If m_current_site <> "" Then
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
        Else
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
        End If

      End If

      ' Final meter
      If _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE, DataRowVersion.Original) <> _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) Then
        _field_text = GLB_NLS_GUI_INVOICING.GetString(216)
        _original_value = FormatMeterScreenStringValue(_meter_code, _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE, DataRowVersion.Original), Nothing)
        _current_value = FormatMeterScreenStringValue(_meter_code, _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), Nothing)

        'LTC  29-OCT-2017
        If m_current_site <> "" Then
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
        Else
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
        End If


      End If

      ' System delta
      If _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM, DataRowVersion.Original) <> _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) Then
        _field_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6154)

        _original_value = FormatMeterScreenStringValue(_meter_code, _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM, DataRowVersion.Original), Nothing)
        _current_value = FormatMeterScreenStringValue(_meter_code, _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), Nothing)

        'LTC  29-OCT-2017
        If m_current_site <> "" Then
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
        Else
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
        End If

      End If

      ' Reason
      If _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REASON, DataRowVersion.Original) <> _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REASON) Then
        _original_value = _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REASON, DataRowVersion.Original)
        _current_value = _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REASON)

        _field_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6150)

        'LTC  29-OCT-2017
        If m_current_site <> "" Then
          _orig_auditor.SetField(0, GetReasonDescription(_original_value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, GetReasonDescription(_current_value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
        Else
          _orig_auditor.SetField(0, GetReasonDescription(_original_value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, GetReasonDescription(_current_value), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
        End If

      End If

      ' Remarks
      If _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS, DataRowVersion.Original) <> _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS) Then
        _original_value = _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS, DataRowVersion.Original)
        _current_value = _row.Item(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS)

        _field_text = GLB_NLS_GUI_CONTROLS.GetString(312)

        'LTC  29-OCT-2017
        If m_current_site <> "" Then
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8756, m_jouney_filter, m_current_site, m_terminal_name_filter, _meter_name, _field_text))
        Else
          _orig_auditor.SetField(0, _original_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
          _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6161, m_jouney_filter, m_terminal_name_filter, _meter_name, _field_text))
        End If

      End If

    Next

    ' Notify
    _curr_auditor.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                         0, _
                         _orig_auditor)

  End Sub

  ' PURPOSE: Enable/Disable tab edition controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - Boolean: Enable
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DisableEditionTabControls(ByVal Enable As Boolean)

    txt_rollover_current.Enabled = Enable
    ef_initial_meter_new.Enabled = Enable
    ef_final_meter_new.Enabled = Enable
    txt_rollover_new.Enabled = Enable
    ef_delta_system_new.Enabled = Enable
    cmb_reason.Enabled = Enable
    txt_remark.Enabled = Enable

    ef_initial_meter_current.IsReadOnly = True
    ef_final_meter_current.IsReadOnly = True
    ef_delta_calculated_current.IsReadOnly = True
    ef_delta_system_current.IsReadOnly = True
    ef_delta_calculated_new.IsReadOnly = True

    ef_initial_meter_new.IsReadOnly = False
    ef_final_meter_new.IsReadOnly = False

    btn_reset.Enabled = Enable

  End Sub

  ' PURPOSE: Check data rows values
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - Boolean
  Private Function CheckRowsData(ByVal DataTable As DataTable, ByVal ReturnFirstError As Boolean, ByVal ShowMessageAndFocusControl As Boolean) As Boolean
    Dim _has_errors As Boolean

    _has_errors = True

    For Each _row As DataRow In DataTable.Rows
      If Not CheckRowData(_row, ShowMessageAndFocusControl) Then

        _has_errors = False

        If ReturnFirstError Then
          Exit For
        End If
      End If

    Next

    Return _has_errors

  End Function

  ' PURPOSE: Check data row values
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - Boolean
  Private Function CheckRowData(ByVal DataRow As DataRow, ByVal ShowMessageAndFocusControl As Boolean) As Boolean
    Dim _value_int64 As Int64
    Dim _control_focus As Control
    Dim _message_id As Int32
    Dim _message_parameter As String
    Dim _meter_code As Int64

    _message_id = -1
    _control_focus = Nothing
    _message_parameter = String.Empty
    _meter_code = -1

    Try

      If DataRow.RowState = DataRowState.Unchanged Then
        Return True
      End If

      ' Validate when is NOT only delta
      If Not DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) Is DBNull.Value AndAlso _
          DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.ONLY_DELTA) = 0 Then
        ' New initial meter
        If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE) Is DBNull.Value OrElse Not Int64.TryParse(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), _value_int64) Then
          ' Debe introducir un valor para %1.
          _message_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(118)
          _control_focus = ef_initial_meter_new
          _message_parameter = String.Format("'{0}.{1}'", ef_initial_meter_new.Text, lbl_initial.Text)

          Return False
        End If

        If Int64.TryParse(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.INITIAL_VALUE), _value_int64) AndAlso _value_int64 < 0 Then
          ' Debe indicar un valor mayor que 0 para %1.
          _message_id = GLB_NLS_GUI_CONFIGURATION.Id(125)
          _control_focus = ef_initial_meter_new
          _message_parameter = String.Format("'{0}.{1}'", ef_initial_meter_new.Text, lbl_initial.Text)

          Return False
        End If

        ' New final meter
        If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE) Is DBNull.Value OrElse Not Int64.TryParse(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), _value_int64) Then
          ' Debe introducir un valor para %1.
          _message_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(118)
          _control_focus = ef_final_meter_new
          _message_parameter = String.Format("'{0}.{1}'", ef_initial_meter_new.Text, lbl_final.Text)

          Return False
        End If

        If Int64.TryParse(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.FINAL_VALUE), _value_int64) AndAlso _value_int64 < 0 Then
          ' Debe indicar un valor mayor que 0 para %1.
          _message_id = GLB_NLS_GUI_CONFIGURATION.Id(125)
          _control_focus = ef_final_meter_new
          _message_parameter = String.Format("'{0}.{1}'", ef_initial_meter_new.Text, lbl_final.Text)

          Return False
        End If
      End If

      ' New system delta
      If DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM) Is DBNull.Value OrElse Not Int64.TryParse(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), _value_int64) Then
        ' Debe introducir un valor para %1.
        _message_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(118)
        _control_focus = ef_delta_system_new
        _message_parameter = String.Format("'{0}.{1}'", ef_initial_meter_new.Text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6154))

        Return False
      End If

      If Int64.TryParse(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.DELTA_SYSTEM), _value_int64) AndAlso _value_int64 < 0 Then
        ' Debe indicar un valor mayor que 0 para %1.
        _message_id = GLB_NLS_GUI_CONFIGURATION.Id(125)
        _control_focus = ef_delta_system_new
        _message_parameter = String.Format("'{0}.{1}'", ef_initial_meter_new.Text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6154))

        Return False
      End If

      ' Description
      If String.IsNullOrEmpty(DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.REMARKS)) And DataRow(WSI.Common.TerminalMeterGroup.COL_METERS.REASON) = WSI.Common.TerminalMeterGroup.MeterAjustmentReason.others Then
        ' Se debe introducir una descripci�n para la raz�n '%1'
        _message_id = GLB_NLS_GUI_PLAYER_TRACKING.Id(1302)
        _control_focus = txt_remark
        _message_parameter = lbl_description.Text

        Return False
      End If

      Return True

    Catch ex As Exception

    Finally

      If ShowMessageAndFocusControl And Not _control_focus Is Nothing Then

        ' Load meter in edition tab
        _meter_code = DataRow.Item(WSI.Common.TerminalMeterGroup.COL_METERS.METER_CODE)
        tc_meters.SelectTab(tp_edit)
        cmb_meters.Value = _meter_code

        ' Show message
        Call NLS_MsgBox(_message_id, ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _message_parameter)
        _control_focus.Focus()

      End If
    End Try


  End Function

  ' PURPOSE: Get meter format Numeric/Currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - Int32: MeterCode
  '           - Decimal: MeterValue
  '           - GUI_Controls.CLASS_FILTER.ENUM_FORMAT: EnumFormat
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - String: value formated
  Private Function FormatMeterScreenStringValue(ByVal MeterCode As Int32, ByVal MeterValue As Decimal, ByRef EnumFormat As GUI_Controls.CLASS_FILTER.ENUM_FORMAT) As String
    Dim _value As String
    Dim _meter As WSI.Common.Meter

    _value = String.Empty
    _meter = WSI.Common.TerminalMeterGroup.GetMeter(MeterCode, m_terminals_meters_data.Meters)

    If _meter Is Nothing Then
      Return _value
    End If

    If _meter.type.Equals(GetType(WSI.Common.Currency)) Then
      _value = GUI_FormatCurrency(MeterValue / 100, 2)
      EnumFormat = CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY
    ElseIf _meter.type.Equals(GetType(Int64)) Then
      _value = GUI_FormatNumber(MeterValue, 0)
      EnumFormat = CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER
    End If

    Return _value
  End Function

  ' PURPOSE: Get meter format Numeric/Currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - Int32: MeterCode
  '           - Decimal: MeterValue
  '           - GUI_Controls.CLASS_FILTER.ENUM_FORMAT: EnumFormat
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - Decimal: value formated
  Private Function FormatMeterScreenValue(ByVal MeterCode As Int32, ByVal MeterValue As Decimal, ByRef EnumFormat As GUI_Controls.CLASS_FILTER.ENUM_FORMAT) As Decimal
    Dim _value As Decimal
    Dim _meter As WSI.Common.Meter

    _value = 0
    _meter = WSI.Common.TerminalMeterGroup.GetMeter(MeterCode, m_terminals_meters_data.Meters)

    If _meter.type.Equals(GetType(WSI.Common.Currency)) Then
      _value = MeterValue / 100
      EnumFormat = CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY
    ElseIf _meter.type.Equals(GetType(Int64)) Then
      _value = MeterValue
      EnumFormat = CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER
    End If

    Return _value
  End Function

  ' PURPOSE: Cast meter format from Numeric/Currency to Int64
  '
  '  PARAMS:
  '     - INPUT:
  '           - Int32: MeterCode
  '           - Decimal: MeterValue
  '        
  '     - OUTPUT:
  '
  ' RETURNS:
  '           - Int64: casted value
  Private Function CastFormatMeterScreenValue(ByVal MeterCode As Int32, ByVal MeterValue As Decimal) As Int64
    Dim _value As Int64
    Dim _meter As WSI.Common.Meter

    _value = 0
    _meter = WSI.Common.TerminalMeterGroup.GetMeter(MeterCode, m_terminals_meters_data.Meters)

    If _meter.type.Equals(GetType(WSI.Common.Currency)) Then
      _value = MeterValue * 100
    ElseIf _meter.type.Equals(GetType(Int64)) Then
      _value = MeterValue
    End If

    Return _value

  End Function

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub

#End Region ' Public Functions

#Region " Events "

  Private Sub cmb_meters_ValueChangedEvent()

    ' Load selected meter data
    SetScreenEditionData()

  End Sub

  Private Sub cmb_meters_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    ' Save current meter
    m_prev_selected_meter = cmb_meters.Value

  End Sub

  Private Sub controls_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_initial_meter_new.Leave, ef_final_meter_new.Leave, txt_rollover_current.Leave, txt_rollover_current.Click, txt_rollover_new.Leave, txt_rollover_new.Click, ef_delta_system_new.Leave, txt_remark.Leave, cmb_reason.Leave

    CalculateDeltaMeters()
    GetScreenMeterData()
    SetControlsBackgroundColor()

  End Sub

  Private Sub btn_reset_ClickEvent() Handles btn_reset.ClickEvent
    ResetSelectedMeterData()
    SetScreenEditionData()
  End Sub

  Private Sub dg_meters_data_DataSelectedEvent() Handles dg_meters_data.DataSelectedEvent
    Dim _selected_rows As Int32()
    Dim _meter_code As Int32

    _selected_rows = dg_meters_data.SelectedRows()

    ' Grid double click load selected meter data in edition tab
    If _selected_rows.Length > 0 AndAlso _selected_rows(0) >= 0 And _selected_rows(0) < dg_meters_data.NumRows Then
      _meter_code = dg_meters_data.Cell(_selected_rows(0), GRID_M_COLUMN_TERMINAL_METER_CODE).Value
      cmb_meters.Value = _meter_code
      tc_meters.SelectTab(tp_edit)

    End If

  End Sub

  Private Sub tc_meters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tc_meters.SelectedIndexChanged

    If tc_meters.SelectedTab.Name = tp_edit.Name Then
      ef_initial_meter_current.IsReadOnly = True
      ef_final_meter_current.IsReadOnly = True
      ef_delta_calculated_current.IsReadOnly = True
      ef_delta_system_current.IsReadOnly = True
      ef_delta_calculated_new.IsReadOnly = True

      If ef_initial_meter_new.IsReadOnly Then
        ef_initial_meter_new.IsReadOnly = True
      End If

      If ef_final_meter_new.IsReadOnly Then
        ef_final_meter_new.IsReadOnly = True
      End If

    End If

  End Sub

  'LTC  29-OCT-2017
  Private Sub CellDataChangedEvent() Handles uc_site_select.CellDataChangedEvent

    'Clean Terminals
    Call Me.uc_provider.Init({WSI.Common.TerminalTypes.SAS_HOST, WSI.Common.TerminalTypes.OFFLINE}, GUI_Controls.uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL, "")

    'Populate Terminals
    m_current_site = uc_site_select.GetSiteIdSelected()
    Call Me.uc_provider.Init({WSI.Common.TerminalTypes.SAS_HOST, WSI.Common.TerminalTypes.OFFLINE}, GUI_Controls.uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL, m_current_site)

  End Sub

#End Region ' Events

End Class