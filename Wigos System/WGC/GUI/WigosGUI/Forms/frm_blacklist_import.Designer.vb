﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_blacklist_import
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_file_path = New GUI_Controls.uc_entry_field()
    Me.btn_open_file_dialog = New System.Windows.Forms.Button()
    Me.gb_output = New System.Windows.Forms.GroupBox()
    Me.pgb_import_progress = New System.Windows.Forms.ProgressBar()
    Me.tb_output = New System.Windows.Forms.TextBox()
    Me.btn_blacklist_type = New System.Windows.Forms.Button()
    Me.cmb_blacklist_type = New GUI_Controls.uc_combo()
    Me.panel_data.SuspendLayout()
    Me.gb_output.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_blacklist_type)
    Me.panel_data.Controls.Add(Me.gb_output)
    Me.panel_data.Controls.Add(Me.ef_file_path)
    Me.panel_data.Controls.Add(Me.btn_blacklist_type)
    Me.panel_data.Controls.Add(Me.btn_open_file_dialog)
    Me.panel_data.Location = New System.Drawing.Point(8, 4)
    Me.panel_data.Size = New System.Drawing.Size(784, 434)
    '
    'ef_file_path
    '
    Me.ef_file_path.DoubleValue = 0.0R
    Me.ef_file_path.IntegerValue = 0
    Me.ef_file_path.IsReadOnly = False
    Me.ef_file_path.Location = New System.Drawing.Point(3, 25)
    Me.ef_file_path.Name = "ef_file_path"
    Me.ef_file_path.PlaceHolder = Nothing
    Me.ef_file_path.Size = New System.Drawing.Size(540, 24)
    Me.ef_file_path.SufixText = "Sufix Text"
    Me.ef_file_path.SufixTextVisible = True
    Me.ef_file_path.TabIndex = 2
    Me.ef_file_path.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_file_path.TextValue = ""
    Me.ef_file_path.TextWidth = 120
    Me.ef_file_path.Value = ""
    Me.ef_file_path.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_open_file_dialog
    '
    Me.btn_open_file_dialog.Location = New System.Drawing.Point(549, 25)
    Me.btn_open_file_dialog.Name = "btn_open_file_dialog"
    Me.btn_open_file_dialog.Size = New System.Drawing.Size(27, 24)
    Me.btn_open_file_dialog.TabIndex = 3
    Me.btn_open_file_dialog.Text = "..."
    Me.btn_open_file_dialog.UseVisualStyleBackColor = True
    '
    'gb_output
    '
    Me.gb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_output.Controls.Add(Me.pgb_import_progress)
    Me.gb_output.Controls.Add(Me.tb_output)
    Me.gb_output.Location = New System.Drawing.Point(3, 110)
    Me.gb_output.Name = "gb_output"
    Me.gb_output.Size = New System.Drawing.Size(778, 322)
    Me.gb_output.TabIndex = 8
    Me.gb_output.TabStop = False
    Me.gb_output.Text = "xOutput"
    '
    'pgb_import_progress
    '
    Me.pgb_import_progress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.pgb_import_progress.Location = New System.Drawing.Point(6, 292)
    Me.pgb_import_progress.Name = "pgb_import_progress"
    Me.pgb_import_progress.Size = New System.Drawing.Size(766, 23)
    Me.pgb_import_progress.TabIndex = 1
    '
    'tb_output
    '
    Me.tb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tb_output.Location = New System.Drawing.Point(6, 21)
    Me.tb_output.Multiline = True
    Me.tb_output.Name = "tb_output"
    Me.tb_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_output.Size = New System.Drawing.Size(766, 271)
    Me.tb_output.TabIndex = 0
    '
    'btn_blacklist_type
    '
    Me.btn_blacklist_type.Location = New System.Drawing.Point(549, 59)
    Me.btn_blacklist_type.Name = "btn_blacklist_type"
    Me.btn_blacklist_type.Size = New System.Drawing.Size(27, 24)
    Me.btn_blacklist_type.TabIndex = 3
    Me.btn_blacklist_type.Text = "..."
    Me.btn_blacklist_type.UseVisualStyleBackColor = True
    '
    'cmb_blacklist_type
    '
    Me.cmb_blacklist_type.AllowUnlistedValues = False
    Me.cmb_blacklist_type.AutoCompleteMode = False
    Me.cmb_blacklist_type.IsReadOnly = False
    Me.cmb_blacklist_type.Location = New System.Drawing.Point(23, 58)
    Me.cmb_blacklist_type.Name = "cmb_blacklist_type"
    Me.cmb_blacklist_type.SelectedIndex = -1
    Me.cmb_blacklist_type.Size = New System.Drawing.Size(520, 24)
    Me.cmb_blacklist_type.SufixText = "Sufix Text"
    Me.cmb_blacklist_type.SufixTextVisible = True
    Me.cmb_blacklist_type.TabIndex = 9
    Me.cmb_blacklist_type.TextCombo = Nothing
    Me.cmb_blacklist_type.TextWidth = 100
    '
    'frm_blacklist_import
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(906, 442)
    Me.Name = "frm_blacklist_import"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_blacklist_import"
    Me.panel_data.ResumeLayout(False)
    Me.gb_output.ResumeLayout(False)
    Me.gb_output.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_file_path As GUI_Controls.uc_entry_field
  Friend WithEvents btn_open_file_dialog As System.Windows.Forms.Button
  Friend WithEvents gb_output As System.Windows.Forms.GroupBox
  Friend WithEvents pgb_import_progress As System.Windows.Forms.ProgressBar
  Friend WithEvents tb_output As System.Windows.Forms.TextBox
  Friend WithEvents btn_blacklist_type As System.Windows.Forms.Button
  Friend WithEvents cmb_blacklist_type As GUI_Controls.uc_combo
End Class
