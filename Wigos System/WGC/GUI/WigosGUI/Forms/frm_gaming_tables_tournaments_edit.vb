﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_tournament_edit.vb
'
' DESCRIPTION : Window for edit and create tournaments
'               In this window the user can create and edit a selected tournament.
'
' AUTHOR:        Toni Téllez Barja
'
' CREATION DATE: 03-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-APR-2017  ATB    Initial version (PBI 26467: Gaming table tournament - New and Edition)
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl
Imports WigosGUI.frm_gaming_tables_sel
Imports System.Text
Imports System.Xml

#End Region  ' Imports

Public Class frm_gaming_tables_tournaments_edit
  Inherits frm_base_edit
#Region " Constants "
  Private Const MAX_TEXT_LENGTH As Integer = 100
#End Region

#Region " Enums "
  Private Enum TOURNAMENT_STATUS
    UNDEFINED = 0
    NEW_TOURNAMENT = 1
    INSCRIPTION = 2
    STARTED = 3
    FINISHED = 4
  End Enum

  Public Structure TYPE_GAME_TABLE
    Dim id As Integer
    Dim code As String
    Dim name As String
    Dim game_type As Integer
    Dim num_seats As Integer
    Dim selected As Boolean
  End Structure
#End Region

#Region " Members "

  Private m_tournament_read As CLASS_TOURNAMENT
  Private m_tournament_edit As CLASS_TOURNAMENT
  Private m_changed_values As Boolean
  Private m_new_count_id As Integer
  Private m_new_items As Stack

  Dim _gaming_table_list As List(Of TYPE_GAME_TABLE)
#End Region ' Members

#Region " Overrides "
  Public Overloads Sub ShowEditItem(Optional ByVal OperationId As Int64 = 0)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    If OperationId = 0 Then
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    End If

    Me.DbObjectId = OperationId


    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_TOURNAMENTS_EDIT

    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ''' <summary>
  ''' Set all the fields at their default value
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button 
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' SCREEN MODE CONFIG
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8038)
      m_changed_values = False
    Else
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8039)
      m_changed_values = True
    End If

    ''''''''''''''
    ''' FIELDS '''
    ''''''''''''''

    'Groupboxes titles
    Me.groupb_general.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8040) ' "Tournament general data"
    Me.groupb_dates.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8041) ' "Tournament dates"
    Me.groupb_amounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8042) ' "Tournament amount settings"
    Me.groupb_game_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8043) ' "Tournament game specific data"

    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008) ' "Enabled"


    'General data
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.txt_tournament_status.Value = m_tournament_edit.TournamentStatus
    Else
      Me.txt_tournament_status.Value = TOURNAMENT_STATUS.NEW_TOURNAMENT.ToString()
    End If

    Me.txt_tournament_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8044) ' "Tournament Status"
    Me.txt_tournament_status.IsReadOnly = True

    Me.txt_tournament_name.IsReadOnly = False
    Me.txt_tournament_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    Me.txt_tournament_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_TEXT_LENGTH)

    'Dates
    Me.date_inscription_start.IsReadOnly = False
    Me.date_inscription_start.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8056) ' "Inscription start"
    Me.date_inscription_start.Value = Date.Today.AddMonths(1).AddDays(-Date.Today.Day + 1)
    Me.date_inscription_start_time.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.date_inscription_end.IsReadOnly = False
    Me.date_inscription_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8057) ' "Inscription end"
    Me.date_inscription_end.Value = Me.date_inscription_start.Value.AddMonths(1).AddDays(-1)
    Me.date_inscription_end_time.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.date_tournament_start.IsReadOnly = False
    Me.date_tournament_start.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8052) ' "Tournament start"
    Me.date_tournament_start.Value = Me.date_inscription_end.Value.AddDays(1)
    Me.date_tournament_start_time.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.date_tournament_end.IsReadOnly = False
    Me.date_tournament_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8053) ' "Tournament end"
    Me.date_tournament_end.Value = Me.date_tournament_start.Value.AddMonths(1).AddDays(-1)
    Me.date_tournament_end_time.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_MMYEAR, ENUM_FORMAT_TIME.FORMAT_HHMM)

    'Amount settings
    Me.chk_admission_increment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8045) ' "Admission increment"

    Me.txt_admission.IsReadOnly = False
    Me.txt_admission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8046) ' "Admission"
    Me.txt_admission.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS)
    Me.txt_admission.Value = 0

    Me.txt_prize_contribution.IsReadOnly = False
    Me.txt_prize_contribution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8047) ' "Contribution"
    Me.txt_prize_contribution.SetFilter(FORMAT_NUMBER, 3)
    Me.txt_prize_contribution.Value = 0
    EnablePrizeContribution()

    Me.chk_rebuy.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8050) ' "Rebuy"

    Me.txt_number_rebuys.IsReadOnly = False
    Me.txt_number_rebuys.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795) ' "Number"
    Me.txt_number_rebuys.SetFilter(FORMAT_NUMBER, MAX_AMOUNT_DIGITS)

    Me.txt_rebuy.IsReadOnly = False
    Me.txt_rebuy.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8051) ' "Amount"
    Me.txt_rebuy.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS)

    Me.txt_prize_contribution_rebuy.IsReadOnly = False
    Me.txt_prize_contribution_rebuy.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8047) ' "Contribution"
    Me.txt_prize_contribution_rebuy.SetFilter(FORMAT_NUMBER, 3)
    Me.txt_prize_contribution_rebuy.Value = 0
    EnableRebuy()

    Me.txt_fixed_amount.IsReadOnly = False
    Me.txt_fixed_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8049) ' "Fixed amount"
    Me.txt_fixed_amount.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS)

    Me.txt_tournament_total.IsReadOnly = True
    Me.txt_tournament_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047) ' "TOTAL"
    Me.txt_tournament_total.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS)

    'Specific data
    Me.chkl_available_gaming_tables_at_tournament.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8054) ' "Available gaming tables at the tournament"
    Me.chkl_available_gaming_tables_at_tournament.btn_check_all.PerformClick()

    Me.lbl_rules.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8058) ' "Rules"
    Me.txtb_rules.ReadOnly = False
    Me.txtb_rules.ScrollBars = ScrollBars.Vertical

    Me.txt_rounds_number.IsReadOnly = False
    Me.txt_rounds_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8055) ' "Number of rounds"
    Me.txt_rounds_number.SetFilter(FORMAT_NUMBER, 2)
    Me.txt_rounds_number.Value = 1

    Me.txt_capacity.IsReadOnly = False
    Me.txt_capacity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8059) ' "Seating"
    Me.txt_capacity.SetFilter(FORMAT_NUMBER, 4)
    Me.txt_capacity.Value = 0

    Me.txt_number_seats.IsReadOnly = True
    Me.txt_number_seats.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8060) ' "Number of seats"
    Me.txt_number_seats.SetFilter(FORMAT_NUMBER, 4)
    Me.txt_number_seats.Value = 0

    Me.chkl_available_gaming_tables_at_tournament.Enabled = True

    Dim _row As TYPE_GAME_TABLE
    _gaming_table_list = New List(Of TYPE_GAME_TABLE)

    For Each _dr As DataRow In GamingTableRows()
      _row = New TYPE_GAME_TABLE
      _row.id = _dr("GT_GAMING_TABLE_ID")
      _row.code = _dr("GT_CODE")
      _row.name = _dr("GT_NAME")
      _row.game_type = _dr("GT_TYPE_ID")
      _row.num_seats = _dr("GT_NUM_SEATS")
      _row.selected = True
      ' A list will be saved to keep the xml data in memory
      _gaming_table_list.Add(_row)
      ' The checkbox list will be filled by default
      Me.chkl_available_gaming_tables_at_tournament.Add(0, _row.id, _row.name, _row.selected)
    Next

    'At booting the Enabled check will be checked
    EnableAll()

    'Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Set the form's info
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    m_tournament_read = Me.DbReadObject
    ' Name
    With m_tournament_read
      Me.chk_enabled.Checked = .Enabled
      Me.txt_tournament_status.Value = GetStatus(.TournamentStatus)
      Me.txt_tournament_name.Value = .TournamentName
      Me.date_inscription_start.Value = .InscriptionStartDate
      Me.date_inscription_start_time.Value = .InscriptionStartDate
      Me.date_inscription_end.Value = .InscriptionFinishDate
      Me.date_inscription_end_time.Value = .InscriptionFinishDate
      Me.date_tournament_start.Value = .EventStartDate
      Me.date_tournament_start_time.Value = .EventStartDate
      Me.date_tournament_end.Value = .EventFinishDate
      Me.date_tournament_end_time.Value = .EventFinishDate
      Me.txt_admission.Value = .AdmissionAmount
      Me.chk_admission_increment.Checked = .AdmissionIncrement
      Me.txt_prize_contribution.Value = .IncrementPrizeAmount
      Me.chk_rebuy.Checked = .Rebuy > 0
      Me.txt_number_rebuys.Value = .Rebuy
      Me.txt_rebuy.Value = .RebuyAmount
      Me.txt_prize_contribution_rebuy.Value = .RebuyPctjContribution
      Me.txt_fixed_amount.Value = .FixedPrizeAmount
      Me.txt_tournament_total.Value = .TotalPrizeAmount
      Me.txt_rounds_number.Value = .GameRounds
      Me.txt_capacity.Value = .Capacity
      Me.txt_number_seats.Value = .Seats
      Me.txtb_rules.Text = .Rules

      If (Not String.IsNullOrEmpty(.AvailableTables)) Then
        'If there are saved data, the checkbox list will be cleared and filled again
        Me.chkl_available_gaming_tables_at_tournament.Clear()
        _gaming_table_list = XmlToList(.AvailableTables)
        For Each game As TYPE_GAME_TABLE In _gaming_table_list
          Me.chkl_available_gaming_tables_at_tournament.Add(0, game.id, game.name, game.selected)
        Next
      End If
    End With
  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Sets the database connection
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TOURNAMENT
        m_tournament_edit = DbEditedObject

        m_tournament_edit.Enabled = True
        'm_tournament_edit.TournamentStatus = TOURNAMENT_STATUS.NEW_TOURNAMENT
        m_tournament_edit.TournamentId = 0
        DbStatus = ENUM_STATUS.STATUS_OK
      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE _
       , ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        m_tournament_read = DbReadObject
        m_tournament_edit = DbEditedObject
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' Sets the button behaviour
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        'ACCEPT
        If GUI_IsScreenDataOk() Then
          'Accept changes
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub  ' GUI_ButtonClick

#End Region '  Overrides 

#Region " Private Functions "
  ''' <summary>
  ''' Do the calculations to the amounts area
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CalculateAmount()
    Dim zero As Integer = 0
    Dim admission As Decimal
    Dim prize_contrib As Decimal
    Dim rebuy As Decimal
    Dim rebuy_contrib As Decimal
    Dim rebuy_times As Integer
    Dim fixed_amount As Decimal
    Dim total As Decimal
    admission = IIf(String.IsNullOrEmpty(Me.txt_admission.Value), 0, Me.txt_admission.Value)
    prize_contrib = IIf(String.IsNullOrEmpty(Me.txt_prize_contribution.Value), 0, Me.txt_prize_contribution.Value)
    If (Not Me.chk_admission_increment.Checked) Then
      prize_contrib = 0
    End If
    rebuy = IIf(String.IsNullOrEmpty(Me.txt_rebuy.Value), 0, Me.txt_rebuy.Value)
    rebuy_contrib = IIf(String.IsNullOrEmpty(Me.txt_prize_contribution_rebuy.Value), 0, Me.txt_prize_contribution_rebuy.Value)
    rebuy_times = IIf(String.IsNullOrEmpty(Me.txt_number_rebuys.Value), 0, Me.txt_number_rebuys.Value)
    If (Not Me.chk_rebuy.Checked) Then
      rebuy_contrib = 0
      rebuy_times = 0
    End If
    fixed_amount = IIf(String.IsNullOrEmpty(Me.txt_fixed_amount.Value), 0, Me.txt_fixed_amount.Value)

    total = (GetPercentage(admission, prize_contrib) + (GetPercentage(rebuy, rebuy_contrib) * rebuy_times) + fixed_amount)

    Me.txt_tournament_total.Value = total

  End Sub

  ''' <summary>
  ''' Calculate the percentage value of a number
  ''' </summary>
  ''' <param name="amount"></param>
  ''' <param name="percentage"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetPercentage(ByVal amount As Decimal, ByVal percentage As Decimal) As Decimal
    Return (amount * percentage) / 100
  End Function

  ''' <summary>
  ''' Sets the rebuy check's behaviour
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnableRebuy()
    If Me.chk_rebuy.Checked Then
      Me.txt_rebuy.Enabled = True
      Me.txt_number_rebuys.Enabled = True
      Me.txt_prize_contribution_rebuy.Enabled = True
    Else
      Me.txt_rebuy.Enabled = False
      Me.txt_number_rebuys.Enabled = False
      Me.txt_prize_contribution_rebuy.Enabled = False
    End If
    CalculateAmount()
  End Sub

  ''' <summary>
  ''' Sets the prize contribution check's behaviour
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnablePrizeContribution()
    If Me.chk_admission_increment.Checked Then
      Me.txt_prize_contribution.Enabled = True
    Else
      Me.txt_prize_contribution.Enabled = False
    End If
    CalculateAmount()
  End Sub

  ''' <summary>
  ''' Sets the enabled check's behaviour
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnableAll()
    If Me.chk_enabled.Checked Then
      Me.groupb_general.Enabled = True
      Me.groupb_dates.Enabled = True
      Me.groupb_amounts.Enabled = True
      Me.groupb_game_data.Enabled = True
    Else
      Me.groupb_general.Enabled = False
      Me.groupb_dates.Enabled = False
      Me.groupb_amounts.Enabled = False
      Me.groupb_game_data.Enabled = False
    End If
  End Sub

  ''' <summary>
  ''' Set the status field text
  ''' </summary>
  ''' <param name="status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetStatus(ByVal status As TOURNAMENT_STATUS) As String
    Select Case status
      Case TOURNAMENT_STATUS.NEW_TOURNAMENT
        Return "New"
      Case TOURNAMENT_STATUS.INSCRIPTION
        Return "Inscription"
      Case TOURNAMENT_STATUS.STARTED
        Return "Started"
      Case TOURNAMENT_STATUS.FINISHED
        Return "Finished"
      Case Else
        Return "Undefined"
    End Select
  End Function

  ''' <summary>
  ''' Set the status enum value
  ''' </summary>
  ''' <param name="status"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SetStatus(ByVal status As String) As TOURNAMENT_STATUS
    Select Case status
      Case "New"
        Return TOURNAMENT_STATUS.NEW_TOURNAMENT
      Case "Inscription"
        Return TOURNAMENT_STATUS.INSCRIPTION
      Case "Started"
        Return TOURNAMENT_STATUS.STARTED
      Case "Finished"
        Return TOURNAMENT_STATUS.FINISHED
      Case Else
        Return TOURNAMENT_STATUS.UNDEFINED
    End Select
  End Function

  ''' <summary>
  ''' Parses a list of type_game_table to XML
  ''' </summary>
  ''' <param name="list"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ToXml(ByVal list As List(Of TYPE_GAME_TABLE)) As String
    Dim xml_document As XmlDocument
    Dim xml_node_main As XmlNode
    xml_document = New XmlDocument
    xml_node_main = xml_document.CreateElement("GAMES")
    For Each element As TYPE_GAME_TABLE In list
      Dim xml_node_parent As XmlNode
      Dim xml_node_son As XmlNode

      xml_node_parent = xml_document.CreateElement("TYPE_GAME_TABLE")

      xml_node_son = xml_document.CreateElement("ID")
      xml_node_son.InnerText = element.id
      xml_node_parent.AppendChild(xml_node_son)

      xml_node_son = xml_document.CreateElement("CODE")
      xml_node_son.InnerText = element.code
      xml_node_parent.AppendChild(xml_node_son)

      xml_node_son = xml_document.CreateElement("GAME_TYPE")
      xml_node_son.InnerText = element.game_type
      xml_node_parent.AppendChild(xml_node_son)

      xml_node_son = xml_document.CreateElement("NAME")
      xml_node_son.InnerText = element.name
      xml_node_parent.AppendChild(xml_node_son)

      xml_node_son = xml_document.CreateElement("NUM_SEATS")
      xml_node_son.InnerText = element.num_seats
      xml_node_parent.AppendChild(xml_node_son)

      xml_node_son = xml_document.CreateElement("SELECTED")
      xml_node_son.InnerText = element.selected
      xml_node_parent.AppendChild(xml_node_son)

      xml_node_main.AppendChild(xml_node_parent)
    Next
    xml_document.AppendChild(xml_node_main)

    Return xml_document.OuterXml
  End Function

  ''' <summary>
  ''' Parses a xml string to a list of type_game_table
  ''' </summary>
  ''' <param name="xmlString"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function XmlToList(ByVal xmlString As String) As List(Of TYPE_GAME_TABLE)
    Dim type_game_table As TYPE_GAME_TABLE
    Dim xml_document As XmlDocument
    xml_document = New XmlDocument()
    xml_document.LoadXml(xmlString)
    Dim id_list As List(Of Integer)
    id_list = New List(Of Integer)
    _gaming_table_list.Clear()
    For Each game As XmlNode In xml_document.SelectSingleNode("GAMES").SelectNodes("TYPE_GAME_TABLE")
      If (Boolean.Parse(game.SelectSingleNode("SELECTED").InnerText)) Then
        id_list.Add(game.SelectSingleNode("ID").InnerText)
      End If
      type_game_table = New TYPE_GAME_TABLE
      type_game_table.id = game.SelectSingleNode("ID").InnerText
      type_game_table.code = game.SelectSingleNode("CODE").InnerText
      type_game_table.name = game.SelectSingleNode("NAME").InnerText
      type_game_table.game_type = game.SelectSingleNode("GAME_TYPE").InnerText
      type_game_table.num_seats = game.SelectSingleNode("NUM_SEATS").InnerText
      type_game_table.selected = game.SelectSingleNode("SELECTED").InnerText
      _gaming_table_list.Add(type_game_table)
    Next
    Return _gaming_table_list
  End Function

  ''' <summary>
  ''' Load the games from database and returns them in a row format
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GamingTableRows() As DataRowCollection
    ' Reset dg selections
    Dim _table_types As DataTable
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("   SELECT  GT_GAMING_TABLE_ID ")
    _str_sql.AppendLine("           ,GT_CODE           ")
    _str_sql.AppendLine("           , GT_NAME          ")
    _str_sql.AppendLine("           , GT_TYPE_ID       ")
    _str_sql.AppendLine("           , GT_NUM_SEATS     ")
    _str_sql.AppendLine("     FROM    GAMING_TABLES    ")
    _str_sql.AppendLine("    WHERE    GT_ENABLED = 1   ")

    _table_types = GUI_GetTableUsingSQL(_str_sql.ToString(), 5000)
    Return _table_types.Rows
  End Function

#End Region ' private Functions

#Region " Events "
  Private Sub chk_rebuy_CheckedChanged(sender As Object, e As EventArgs) Handles chk_rebuy.CheckedChanged
    EnableRebuy()
  End Sub

  Private Sub chk_enabled_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled.CheckedChanged
    EnableAll()
  End Sub

  Private Sub chk_admission_increment_CheckedChanged(sender As Object, e As EventArgs) Handles chk_admission_increment.CheckedChanged
    EnablePrizeContribution()
  End Sub

  Private Sub txt_admission_EntryFieldValueChanged() Handles txt_admission.EntryFieldValueChanged
    CalculateAmount()
  End Sub

  Private Sub txt_prize_contribution_EntryFieldValueChanged() Handles txt_prize_contribution.EntryFieldValueChanged
    If (Not String.IsNullOrEmpty(Me.txt_prize_contribution.Value) AndAlso Me.txt_prize_contribution.Value > 100) Then
      Me.txt_prize_contribution.Value = 100
    End If
    CalculateAmount()
  End Sub

  Private Sub txt_number_rebuys_EntryFieldValueChanged() Handles txt_number_rebuys.EntryFieldValueChanged
    CalculateAmount()
  End Sub

  Private Sub txt_rebuy_EntryFieldValueChanged() Handles txt_rebuy.EntryFieldValueChanged
    CalculateAmount()
  End Sub

  Private Sub txt_prize_contribution_rebuy_EntryFieldValueChanged() Handles txt_prize_contribution_rebuy.EntryFieldValueChanged
    If (Not String.IsNullOrEmpty(Me.txt_prize_contribution_rebuy.Value) AndAlso Me.txt_prize_contribution_rebuy.Value > 100) Then
      Me.txt_prize_contribution_rebuy.Value = 100
    End If
    CalculateAmount()
  End Sub

  Private Sub txt_fixed_amount_EntryFieldValueChanged() Handles txt_fixed_amount.EntryFieldValueChanged
    CalculateAmount()
  End Sub

#End Region

  'Events
End Class