'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bank_account_sel
' DESCRIPTION:   This screen allows to view the bank accounts.
' AUTHOR:        Humberto Braojos
' CREATION DATE: 14-JAN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-JAN-2013  HBB    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text


Public Class frm_bank_account_sel
  Inherits frm_base_sel


#Region "Constants"

  Private Const GRID_COLUMNS_COUNT As Integer = 7

  ' DB Columns
  Private Const SQL_COLUMN_BANK_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_BANK_NAME As Integer = 1
  Private Const SQL_COLUMN_ACCOUNT_INFO As Integer = 2
  Private Const SQL_COLUMN_CLIENT_NUMBER As Integer = 3
  Private Const SQL_COLUMN_ACCOUNT_NUMBER As Integer = 4
  Private Const SQL_COLUMN_VALID_DAYS As Integer = 5

  'Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_BANK_ACCOUNT_ID As Integer = 1
  Private Const GRID_COLUMN_BANK_NAME As Integer = 2
  Private Const GRID_COLUMN_ACCOUNT_INFO As Integer = 3
  Private Const GRID_COLUMN_CLIENT_NUMBER As Integer = 4
  Private Const GRID_COLUMN_ACCOUNT_NUMBER As Integer = 5
  Private Const GRID_COLUMN_VALID_DAYS As Integer = 6

  'Width
  Private Const GRID_WIDTH_BANK_ACCOUNT_ID As Integer = 0
  Private Const GRID_WIDTH_BANK_NAME As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT_INFO As Integer = 2500
  Private Const GRID_WIDTH_VALID_DAYS As Integer = 2000
  Private Const GRID_WIDTH_CLIENT_NUMBER As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT_NUMBER As Integer = 2000

  'Filter Length
  Private Const LENGTH_BANK_NAME_FILTER As Integer = 50
  Private Const LENGTH_ACCOUNT_INFO_FILTER As Integer = 50
  Private Const LENGTH_CUSTOMER_NUMBER_FILTER As Integer = 50
  Private Const LENGTH_ACCOUNT_NUMBER_FILTER As Integer = 50

  Private Const COUNTER_DEVICE_OK As Integer = 1

  Private Const COLOR_DEVICE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

#End Region

#Region "OVERRIDES"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PAYMENT_ORDER_CONFIG

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("SELECT   BA_ACCOUNT_ID                 ")
    _sb.AppendLine("        ,BA_BANK_NAME                  ")
    _sb.AppendLine("        ,BA_DESCRIPTION                ")
    _sb.AppendLine("        ,BA_CUSTOMER_NUMBER            ")
    _sb.AppendLine("        ,BA_ACCOUNT_NUMBER             ")
    _sb.AppendLine("        ,BA_EFFECTIVE_DAYS             ")
    _sb.AppendLine("  FROM   BANK_ACCOUNTS                 ")

    _sb.Append(GetSqlWhere())

    Return _sb.ToString()
  End Function

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    ef_account_number.Value = ""
    ef_bank_name.Value = ""
    ef_customer_number.Value = ""
    ef_account_info.Value = ""

  End Sub ' GUI_FilterReset

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1553)

    ef_bank_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554)
    ef_account_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1555)
    ef_customer_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1557)
    ef_account_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1558)

    'setting format and length to the entry fields
    Me.ef_account_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, LENGTH_BANK_NAME_FILTER)
    Me.ef_bank_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, LENGTH_BANK_NAME_FILTER)
    Me.ef_customer_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, LENGTH_CUSTOMER_NUMBER_FILTER)
    Me.ef_account_info.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, LENGTH_ACCOUNT_INFO_FILTER)

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE : Fills the rows
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                        ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' PROMO Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_BANK_ACCOUNT_ID)

    ' BANK NAME
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BANK_NAME).Value = DbRow.Value(SQL_COLUMN_BANK_NAME)

    ' ACCOUNT_INFO
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_INFO).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_INFO)

    ' VALID_DAYS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VALID_DAYS).Value = DbRow.Value(SQL_COLUMN_VALID_DAYS)

    ' CLIENT_NUMBER
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CLIENT_NUMBER).Value = DbRow.Value(SQL_COLUMN_CLIENT_NUMBER)

    ' ACCOUNT_CHARGE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_NUMBER).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_NUMBER)

    Return True

  End Function

  ' PURPOSE: Process button actions 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        GUI_ShowNewBankAccount()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()
    'If Me.Permissions.Write Then
    'Dim _idx_row As Short
    'Dim _account_id As Integer
    'Dim _frm_edit As frm_bank_accounts_edit

    'For _idx_row = 0 To Me.Grid.NumRows - 1
    '  If Me.Grid.Row(_idx_row).IsSelected Then
    '    Exit For
    '  End If
    'Next

    'If _idx_row = Me.Grid.NumRows Then
    '  Return
    'End If

    '' Get the BankAccount ID 
    '_account_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_BANK_ACCOUNT_ID).Value
    '_frm_edit = New frm_bank_accounts_edit()

    'Call _frm_edit.ShowSelectedItem(_account_id)

    '_frm_edit = Nothing

    'Call Me.Grid.Focus()
    ' End If
  End Sub

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region "Private Functions"

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT)

      .Counter(COUNTER_DEVICE_OK).Visible = False
      .Counter(COUNTER_DEVICE_OK).BackColor = GetColor(COLOR_DEVICE_OK_BACK)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' BANK ACCOUNT ID
      .Column(GRID_COLUMN_BANK_ACCOUNT_ID).Header.Text = " "
      .Column(GRID_COLUMN_BANK_ACCOUNT_ID).Width = GRID_WIDTH_BANK_ACCOUNT_ID
      .Column(GRID_COLUMN_BANK_ACCOUNT_ID).Mapping = SQL_COLUMN_BANK_ACCOUNT_ID

      ' BANK NAME
      .Column(GRID_COLUMN_BANK_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1554)
      .Column(GRID_COLUMN_BANK_NAME).Width = GRID_WIDTH_BANK_NAME
      .Column(GRID_COLUMN_BANK_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ACCOUNT_INFO
      .Column(GRID_COLUMN_ACCOUNT_INFO).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1555)
      .Column(GRID_COLUMN_ACCOUNT_INFO).Width = GRID_WIDTH_ACCOUNT_INFO
      .Column(GRID_COLUMN_ACCOUNT_INFO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CLIENT_NUMBER
      .Column(GRID_COLUMN_CLIENT_NUMBER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1557)
      .Column(GRID_COLUMN_CLIENT_NUMBER).Width = GRID_WIDTH_CLIENT_NUMBER
      .Column(GRID_COLUMN_CLIENT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ACCOUNT_CHARGE
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1558)
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Width = GRID_WIDTH_ACCOUNT_NUMBER
      .Column(GRID_COLUMN_ACCOUNT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' VALID_DAYS
      .Column(GRID_COLUMN_VALID_DAYS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1556)
      .Column(GRID_COLUMN_VALID_DAYS).Width = GRID_WIDTH_VALID_DAYS
      .Column(GRID_COLUMN_VALID_DAYS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub

  ' PURPOSE: Open a new BankAccount form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ShowNewBankAccount()

    'Dim _frm_edit As frm_bank_accounts_edit

    '_frm_edit = New frm_bank_accounts_edit()

    'Call _frm_edit.ShowNewItem()


    'Call Me.Grid.Focus()

  End Sub

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _sb_where As StringBuilder

    _sb_where = New StringBuilder()

    _sb_where.AppendLine("WHERE  BA_BANK_NAME         LIKE '%" & ef_bank_name.Value.Trim() & "%'")
    _sb_where.AppendLine("  AND  BA_DESCRIPTION       LIKE '%" & ef_account_info.Value.Trim() & "%'")
    _sb_where.AppendLine("  AND  BA_CUSTOMER_NUMBER   LIKE '%" & ef_customer_number.Value.Trim & "%'")
    _sb_where.AppendLine("  AND  BA_ACCOUNT_NUMBER    LIKE '%" & ef_account_number.Value.Trim & "%'")

    Return _sb_where.ToString()

  End Function

#End Region

End Class