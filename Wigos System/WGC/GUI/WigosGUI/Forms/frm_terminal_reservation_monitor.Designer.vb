<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_reservation_monitor
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.gp_order = New System.Windows.Forms.GroupBox()
    Me.rb_recent = New System.Windows.Forms.RadioButton()
    Me.rb_oldest = New System.Windows.Forms.RadioButton()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gp_order.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gp_order)
    Me.panel_filter.Controls.Add(Me.tf_last_update)
    Me.panel_filter.Size = New System.Drawing.Size(1224, 94)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_last_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gp_order, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 98)
    Me.panel_data.Size = New System.Drawing.Size(1224, 445)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1218, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1218, 4)
    '
    'tmr_monitor
    '
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(212, 52)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(250, 24)
    Me.tf_last_update.SufixText = "xSeconds"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 6
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 130
    Me.tf_last_update.Value = "-"
    '
    'gp_order
    '
    Me.gp_order.Controls.Add(Me.rb_recent)
    Me.gp_order.Controls.Add(Me.rb_oldest)
    Me.gp_order.Location = New System.Drawing.Point(28, 9)
    Me.gp_order.Name = "gp_order"
    Me.gp_order.Size = New System.Drawing.Size(142, 67)
    Me.gp_order.TabIndex = 11
    Me.gp_order.TabStop = False
    Me.gp_order.Text = "xOrder"
    '
    'rb_recent
    '
    Me.rb_recent.AutoSize = True
    Me.rb_recent.Location = New System.Drawing.Point(7, 21)
    Me.rb_recent.Name = "rb_recent"
    Me.rb_recent.Size = New System.Drawing.Size(71, 17)
    Me.rb_recent.TabIndex = 0
    Me.rb_recent.Tag = "radio_order"
    Me.rb_recent.Text = "xRecent"
    Me.rb_recent.UseVisualStyleBackColor = True
    '
    'rb_oldest
    '
    Me.rb_oldest.AutoSize = True
    Me.rb_oldest.Location = New System.Drawing.Point(7, 44)
    Me.rb_oldest.Name = "rb_oldest"
    Me.rb_oldest.Size = New System.Drawing.Size(68, 17)
    Me.rb_oldest.TabIndex = 0
    Me.rb_oldest.TabStop = True
    Me.rb_oldest.Tag = "radio_order"
    Me.rb_oldest.Text = "xOldest"
    Me.rb_oldest.UseVisualStyleBackColor = True
    '
    'frm_terminal_reservation_monitor
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1232, 547)
    Me.Name = "frm_terminal_reservation_monitor"
    Me.Text = "frm_terminal_reservation_monitor"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gp_order.ResumeLayout(False)
    Me.gp_order.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents gp_order As System.Windows.Forms.GroupBox
  Friend WithEvents rb_recent As System.Windows.Forms.RadioButton
  Friend WithEvents rb_oldest As System.Windows.Forms.RadioButton
End Class
