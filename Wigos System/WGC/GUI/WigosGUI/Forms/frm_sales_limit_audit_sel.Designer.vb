<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_sales_limit_audit_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.fra_date = New System.Windows.Forms.GroupBox
    Me.dt_date_to = New GUI_Controls.uc_date_picker
    Me.dt_date_from = New GUI_Controls.uc_date_picker
    Me.fra_user = New System.Windows.Forms.GroupBox
    Me.chk_show_all = New System.Windows.Forms.CheckBox
    Me.cmb_user = New GUI_Controls.uc_combo
    Me.opt_user_all = New System.Windows.Forms.RadioButton
    Me.opt_user_one = New System.Windows.Forms.RadioButton
    Me.fra_terminal_type = New System.Windows.Forms.GroupBox
    Me.chk_mobile_bank = New System.Windows.Forms.CheckBox
    Me.chk_cashier = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.fra_date.SuspendLayout()
    Me.fra_user.SuspendLayout()
    Me.fra_terminal_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.fra_user)
    Me.panel_filter.Controls.Add(Me.fra_terminal_type)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(846, 80)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_terminal_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.fra_user, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 84)
    Me.panel_data.Size = New System.Drawing.Size(846, 411)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(840, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(840, 4)
    '
    'fra_date
    '
    Me.fra_date.Controls.Add(Me.dt_date_to)
    Me.fra_date.Controls.Add(Me.dt_date_from)
    Me.fra_date.Location = New System.Drawing.Point(11, 4)
    Me.fra_date.Name = "fra_date"
    Me.fra_date.Size = New System.Drawing.Size(226, 80)
    Me.fra_date.TabIndex = 0
    Me.fra_date.TabStop = False
    Me.fra_date.Text = "fra_date"
    '
    'dt_date_to
    '
    Me.dt_date_to.Checked = True
    Me.dt_date_to.IsReadOnly = False
    Me.dt_date_to.Location = New System.Drawing.Point(2, 48)
    Me.dt_date_to.Name = "dt_date_to"
    Me.dt_date_to.ShowCheckBox = True
    Me.dt_date_to.ShowUpDown = False
    Me.dt_date_to.Size = New System.Drawing.Size(217, 24)
    Me.dt_date_to.SufixText = "Sufix Text"
    Me.dt_date_to.SufixTextVisible = True
    Me.dt_date_to.TabIndex = 1
    Me.dt_date_to.TextWidth = 50
    Me.dt_date_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(1, 17)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = True
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(218, 24)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 0
    Me.dt_date_from.TextWidth = 50
    Me.dt_date_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'fra_user
    '
    Me.fra_user.Controls.Add(Me.chk_show_all)
    Me.fra_user.Controls.Add(Me.cmb_user)
    Me.fra_user.Controls.Add(Me.opt_user_all)
    Me.fra_user.Controls.Add(Me.opt_user_one)
    Me.fra_user.Location = New System.Drawing.Point(238, 0)
    Me.fra_user.Name = "fra_user"
    Me.fra_user.Size = New System.Drawing.Size(242, 80)
    Me.fra_user.TabIndex = 1
    Me.fra_user.TabStop = False
    Me.fra_user.Text = "fra_user"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(73, 50)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'cmb_user
    '
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(70, 18)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(166, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 2
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'opt_user_all
    '
    Me.opt_user_all.Location = New System.Drawing.Point(8, 47)
    Me.opt_user_all.Name = "opt_user_all"
    Me.opt_user_all.Size = New System.Drawing.Size(59, 24)
    Me.opt_user_all.TabIndex = 1
    Me.opt_user_all.Text = "opt_user_all"
    '
    'opt_user_one
    '
    Me.opt_user_one.Location = New System.Drawing.Point(8, 19)
    Me.opt_user_one.Name = "opt_user_one"
    Me.opt_user_one.Size = New System.Drawing.Size(59, 24)
    Me.opt_user_one.TabIndex = 0
    Me.opt_user_one.Text = "opt_user_one"
    '
    'fra_terminal_type
    '
    Me.fra_terminal_type.Controls.Add(Me.chk_mobile_bank)
    Me.fra_terminal_type.Controls.Add(Me.chk_cashier)
    Me.fra_terminal_type.Location = New System.Drawing.Point(486, 0)
    Me.fra_terminal_type.Name = "fra_terminal_type"
    Me.fra_terminal_type.Size = New System.Drawing.Size(203, 80)
    Me.fra_terminal_type.TabIndex = 2
    Me.fra_terminal_type.TabStop = False
    Me.fra_terminal_type.Text = "xTerminalType"
    '
    'chk_mobile_bank
    '
    Me.chk_mobile_bank.AutoSize = True
    Me.chk_mobile_bank.Location = New System.Drawing.Point(12, 52)
    Me.chk_mobile_bank.Name = "chk_mobile_bank"
    Me.chk_mobile_bank.Size = New System.Drawing.Size(98, 17)
    Me.chk_mobile_bank.TabIndex = 1
    Me.chk_mobile_bank.Text = "xMobileBank"
    Me.chk_mobile_bank.UseVisualStyleBackColor = True
    '
    'chk_cashier
    '
    Me.chk_cashier.AutoSize = True
    Me.chk_cashier.Location = New System.Drawing.Point(12, 24)
    Me.chk_cashier.Name = "chk_cashier"
    Me.chk_cashier.Size = New System.Drawing.Size(77, 17)
    Me.chk_cashier.TabIndex = 0
    Me.chk_cashier.Text = "xCashier"
    Me.chk_cashier.UseVisualStyleBackColor = True
    '
    'frm_sales_limit_audit_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(856, 499)
    Me.Controls.Add(Me.fra_date)
    Me.Name = "frm_sales_limit_audit_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_sales_limit_audit_sel"
    Me.Controls.SetChildIndex(Me.panel_filter, 0)
    Me.Controls.SetChildIndex(Me.fra_date, 0)
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.fra_date.ResumeLayout(False)
    Me.fra_user.ResumeLayout(False)
    Me.fra_user.PerformLayout()
    Me.fra_terminal_type.ResumeLayout(False)
    Me.fra_terminal_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents fra_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
  Friend WithEvents fra_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents opt_user_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_user_one As System.Windows.Forms.RadioButton
  Friend WithEvents fra_terminal_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_mobile_bank As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashier As System.Windows.Forms.CheckBox
End Class
