<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_expired_credits
  Inherits GUI_Controls.frm_base_sel


  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer
  Friend WithEvents chk_details As System.Windows.Forms.CheckBox
  Friend WithEvents gb_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_type_promo As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_non_redeemeable As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_price As System.Windows.Forms.CheckBox
  Friend WithEvents chk_type_redeemeable As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_personal_accounts As System.Windows.Forms.CheckBox

End Class
