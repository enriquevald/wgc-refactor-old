﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customer_notices_priority_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_filter = New System.Windows.Forms.GroupBox()
    Me.gb_working_day_date = New GUI_Controls.uc_date_picker()
    Me.chk_promos = New System.Windows.Forms.CheckBox()
    Me.gb_priority = New System.Windows.Forms.GroupBox()
    Me.lb_name = New System.Windows.Forms.Label()
    Me.tb_name = New System.Windows.Forms.TextBox()
    Me.gb_notices_state = New System.Windows.Forms.GroupBox()
    Me.cb_disabled = New System.Windows.Forms.CheckBox()
    Me.cb_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_properties_priority = New System.Windows.Forms.GroupBox()
    Me.uc_grid_flags = New GUI_Controls.uc_grid()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_filter.SuspendLayout()
    Me.gb_priority.SuspendLayout()
    Me.gb_notices_state.SuspendLayout()
    Me.gb_properties_priority.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_properties_priority)
    Me.panel_filter.Controls.Add(Me.gb_notices_state)
    Me.panel_filter.Controls.Add(Me.gb_priority)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(993, 111)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_priority, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_notices_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_properties_priority, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 115)
    Me.panel_data.Size = New System.Drawing.Size(993, 421)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(987, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(987, 4)
    '
    'gb_filter
    '
    Me.gb_filter.Controls.Add(Me.gb_working_day_date)
    Me.gb_filter.Controls.Add(Me.chk_promos)
    Me.gb_filter.Location = New System.Drawing.Point(5, 4)
    Me.gb_filter.Name = "gb_filter"
    Me.gb_filter.Size = New System.Drawing.Size(317, 80)
    Me.gb_filter.TabIndex = 2
    Me.gb_filter.TabStop = False
    Me.gb_filter.Text = "GroupBox1"
    '
    'gb_working_day_date
    '
    Me.gb_working_day_date.Checked = True
    Me.gb_working_day_date.IsReadOnly = False
    Me.gb_working_day_date.Location = New System.Drawing.Point(10, 20)
    Me.gb_working_day_date.Name = "gb_working_day_date"
    Me.gb_working_day_date.ShowCheckBox = False
    Me.gb_working_day_date.ShowUpDown = False
    Me.gb_working_day_date.Size = New System.Drawing.Size(200, 24)
    Me.gb_working_day_date.SufixText = "Sufix Text"
    Me.gb_working_day_date.SufixTextVisible = True
    Me.gb_working_day_date.TabIndex = 11
    Me.gb_working_day_date.Value = New Date(2017, 8, 7, 0, 0, 0, 0)
    '
    'chk_promos
    '
    Me.chk_promos.AutoSize = True
    Me.chk_promos.Location = New System.Drawing.Point(70, 50)
    Me.chk_promos.Name = "chk_promos"
    Me.chk_promos.Size = New System.Drawing.Size(91, 17)
    Me.chk_promos.TabIndex = 0
    Me.chk_promos.Text = "CheckBox1"
    Me.chk_promos.UseVisualStyleBackColor = True
    '
    'gb_priority
    '
    Me.gb_priority.Controls.Add(Me.lb_name)
    Me.gb_priority.Controls.Add(Me.tb_name)
    Me.gb_priority.Location = New System.Drawing.Point(10, 6)
    Me.gb_priority.Name = "gb_priority"
    Me.gb_priority.Size = New System.Drawing.Size(285, 99)
    Me.gb_priority.TabIndex = 11
    Me.gb_priority.TabStop = False
    Me.gb_priority.Text = "gb_priority"
    '
    'lb_name
    '
    Me.lb_name.AutoSize = True
    Me.lb_name.Location = New System.Drawing.Point(10, 43)
    Me.lb_name.Name = "lb_name"
    Me.lb_name.Size = New System.Drawing.Size(47, 13)
    Me.lb_name.TabIndex = 3
    Me.lb_name.Text = "xName"
    '
    'tb_name
    '
    Me.tb_name.Location = New System.Drawing.Point(92, 40)
    Me.tb_name.MaxLength = 50
    Me.tb_name.Name = "tb_name"
    Me.tb_name.Size = New System.Drawing.Size(185, 21)
    Me.tb_name.TabIndex = 1
    '
    'gb_notices_state
    '
    Me.gb_notices_state.Controls.Add(Me.cb_disabled)
    Me.gb_notices_state.Controls.Add(Me.cb_enabled)
    Me.gb_notices_state.Location = New System.Drawing.Point(301, 6)
    Me.gb_notices_state.Name = "gb_notices_state"
    Me.gb_notices_state.Size = New System.Drawing.Size(157, 99)
    Me.gb_notices_state.TabIndex = 12
    Me.gb_notices_state.TabStop = False
    Me.gb_notices_state.Text = "gb_notices_state"
    '
    'cb_disabled
    '
    Me.cb_disabled.AutoSize = True
    Me.cb_disabled.Location = New System.Drawing.Point(20, 70)
    Me.cb_disabled.Name = "cb_disabled"
    Me.cb_disabled.Size = New System.Drawing.Size(82, 17)
    Me.cb_disabled.TabIndex = 1
    Me.cb_disabled.Text = "xDisabled"
    Me.cb_disabled.UseVisualStyleBackColor = True
    '
    'cb_enabled
    '
    Me.cb_enabled.AutoSize = True
    Me.cb_enabled.Location = New System.Drawing.Point(20, 30)
    Me.cb_enabled.Name = "cb_enabled"
    Me.cb_enabled.Size = New System.Drawing.Size(78, 17)
    Me.cb_enabled.TabIndex = 0
    Me.cb_enabled.Text = "xEnabled"
    Me.cb_enabled.UseVisualStyleBackColor = True
    '
    'gb_properties_priority
    '
    Me.gb_properties_priority.Controls.Add(Me.uc_grid_flags)
    Me.gb_properties_priority.Location = New System.Drawing.Point(464, 6)
    Me.gb_properties_priority.Name = "gb_properties_priority"
    Me.gb_properties_priority.Size = New System.Drawing.Size(278, 99)
    Me.gb_properties_priority.TabIndex = 15
    Me.gb_properties_priority.TabStop = False
    Me.gb_properties_priority.Text = "gb_properties_priority"
    '
    'uc_grid_flags
    '
    Me.uc_grid_flags.CurrentCol = -1
    Me.uc_grid_flags.CurrentRow = -1
    Me.uc_grid_flags.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_flags.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_flags.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_flags.Location = New System.Drawing.Point(6, 19)
    Me.uc_grid_flags.Name = "uc_grid_flags"
    Me.uc_grid_flags.PanelRightVisible = False
    Me.uc_grid_flags.Redraw = True
    Me.uc_grid_flags.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_grid_flags.Size = New System.Drawing.Size(264, 73)
    Me.uc_grid_flags.Sortable = False
    Me.uc_grid_flags.SortAscending = True
    Me.uc_grid_flags.SortByCol = 0
    Me.uc_grid_flags.TabIndex = 13
    Me.uc_grid_flags.ToolTipped = True
    Me.uc_grid_flags.TopRow = -2
    Me.uc_grid_flags.WordWrap = False
    '
    'frm_customer_notices_priority_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1003, 540)
    Me.Controls.Add(Me.gb_filter)
    Me.Name = "frm_customer_notices_priority_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "x_customer_notices_priority_sel"
    Me.Controls.SetChildIndex(Me.gb_filter, 0)
    Me.Controls.SetChildIndex(Me.panel_filter, 0)
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_filter.ResumeLayout(False)
    Me.gb_filter.PerformLayout()
    Me.gb_priority.ResumeLayout(False)
    Me.gb_priority.PerformLayout()
    Me.gb_notices_state.ResumeLayout(False)
    Me.gb_notices_state.PerformLayout()
    Me.gb_properties_priority.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_filter As System.Windows.Forms.GroupBox
  Friend WithEvents gb_working_day_date As GUI_Controls.uc_date_picker
  Friend WithEvents chk_promos As System.Windows.Forms.CheckBox
  Friend WithEvents gb_priority As System.Windows.Forms.GroupBox
  Friend WithEvents gb_notices_state As System.Windows.Forms.GroupBox
  Friend WithEvents lb_name As System.Windows.Forms.Label
  Friend WithEvents tb_name As System.Windows.Forms.TextBox
  Friend WithEvents cb_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents cb_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_properties_priority As System.Windows.Forms.GroupBox
  Friend WithEvents uc_grid_flags As GUI_Controls.uc_grid
End Class
