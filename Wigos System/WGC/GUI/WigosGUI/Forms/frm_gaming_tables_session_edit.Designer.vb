<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_session_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_table = New System.Windows.Forms.GroupBox()
    Me.lbl_table_session_status = New System.Windows.Forms.Label()
    Me.lbl_table_session_end = New System.Windows.Forms.Label()
    Me.lbl_table_session_start = New System.Windows.Forms.Label()
    Me.cmb_table_session = New GUI_Controls.uc_combo()
    Me.cmb_name = New GUI_Controls.uc_combo()
    Me.cmb_type = New GUI_Controls.uc_combo()
    Me.gb_account = New System.Windows.Forms.GroupBox()
    Me.btn_search_account = New System.Windows.Forms.Button()
    Me.lbl_holder_name = New System.Windows.Forms.Label()
    Me.ef_account_id = New GUI_Controls.uc_entry_field()
    Me.ef_card_track = New GUI_Controls.uc_entry_field()
    Me.chk_unknown = New System.Windows.Forms.CheckBox()
    Me.gb_gaming_session = New System.Windows.Forms.GroupBox()
    Me.cmb_isoCode = New GUI_Controls.uc_combo()
    Me.gb_points = New System.Windows.Forms.GroupBox()
    Me.ef_points = New GUI_Controls.uc_entry_field()
    Me.lbl_computed_points = New System.Windows.Forms.Label()
    Me.gb_chips = New System.Windows.Forms.GroupBox()
    Me.ef_chips_in = New GUI_Controls.uc_entry_field()
    Me.ef_chips_out = New GUI_Controls.uc_entry_field()
    Me.ef_buy_in = New GUI_Controls.uc_entry_field()
    Me.gb_bet = New System.Windows.Forms.GroupBox()
    Me.ef_plays = New GUI_Controls.uc_entry_field()
    Me.ef_played_amount = New GUI_Controls.uc_entry_field()
    Me.ef_current_bet = New GUI_Controls.uc_entry_field()
    Me.ef_min_bet = New GUI_Controls.uc_entry_field()
    Me.ef_max_bet = New GUI_Controls.uc_entry_field()
    Me.dtp_walk_time = New GUI_Controls.uc_date_picker()
    Me.dtp_end_session = New GUI_Controls.uc_date_picker()
    Me.dtp_start_session = New GUI_Controls.uc_date_picker()
    Me.cmb_skill = New GUI_Controls.uc_combo()
    Me.cmb_speed = New GUI_Controls.uc_combo()
    Me.panel_data.SuspendLayout()
    Me.gb_table.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.gb_gaming_session.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.gb_chips.SuspendLayout()
    Me.gb_bet.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_gaming_session)
    Me.panel_data.Controls.Add(Me.gb_account)
    Me.panel_data.Controls.Add(Me.gb_table)
    Me.panel_data.Size = New System.Drawing.Size(701, 400)
    '
    'gb_table
    '
    Me.gb_table.Controls.Add(Me.lbl_table_session_status)
    Me.gb_table.Controls.Add(Me.lbl_table_session_end)
    Me.gb_table.Controls.Add(Me.lbl_table_session_start)
    Me.gb_table.Controls.Add(Me.cmb_table_session)
    Me.gb_table.Controls.Add(Me.cmb_name)
    Me.gb_table.Controls.Add(Me.cmb_type)
    Me.gb_table.Location = New System.Drawing.Point(3, 3)
    Me.gb_table.Name = "gb_table"
    Me.gb_table.Size = New System.Drawing.Size(375, 173)
    Me.gb_table.TabIndex = 0
    Me.gb_table.TabStop = False
    Me.gb_table.Text = "xTable"
    '
    'lbl_table_session_status
    '
    Me.lbl_table_session_status.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_table_session_status.AutoSize = True
    Me.lbl_table_session_status.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_table_session_status.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_table_session_status.Location = New System.Drawing.Point(13, 112)
    Me.lbl_table_session_status.MaximumSize = New System.Drawing.Size(340, 0)
    Me.lbl_table_session_status.Name = "lbl_table_session_status"
    Me.lbl_table_session_status.Size = New System.Drawing.Size(112, 14)
    Me.lbl_table_session_status.TabIndex = 5
    Me.lbl_table_session_status.Text = "xStatusSession:"
    '
    'lbl_table_session_end
    '
    Me.lbl_table_session_end.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_table_session_end.AutoSize = True
    Me.lbl_table_session_end.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_table_session_end.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_table_session_end.Location = New System.Drawing.Point(13, 148)
    Me.lbl_table_session_end.MaximumSize = New System.Drawing.Size(340, 0)
    Me.lbl_table_session_end.Name = "lbl_table_session_end"
    Me.lbl_table_session_end.Size = New System.Drawing.Size(91, 14)
    Me.lbl_table_session_end.TabIndex = 4
    Me.lbl_table_session_end.Text = "xEndSession:"
    '
    'lbl_table_session_start
    '
    Me.lbl_table_session_start.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_table_session_start.AutoSize = True
    Me.lbl_table_session_start.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_table_session_start.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_table_session_start.Location = New System.Drawing.Point(13, 130)
    Me.lbl_table_session_start.MaximumSize = New System.Drawing.Size(340, 0)
    Me.lbl_table_session_start.Name = "lbl_table_session_start"
    Me.lbl_table_session_start.Size = New System.Drawing.Size(105, 14)
    Me.lbl_table_session_start.TabIndex = 3
    Me.lbl_table_session_start.Text = "xStartSession:"
    '
    'cmb_table_session
    '
    Me.cmb_table_session.AllowUnlistedValues = False
    Me.cmb_table_session.AutoCompleteMode = False
    Me.cmb_table_session.IsReadOnly = False
    Me.cmb_table_session.Location = New System.Drawing.Point(6, 80)
    Me.cmb_table_session.Name = "cmb_table_session"
    Me.cmb_table_session.SelectedIndex = -1
    Me.cmb_table_session.Size = New System.Drawing.Size(363, 24)
    Me.cmb_table_session.SufixText = "Sufix Text"
    Me.cmb_table_session.SufixTextVisible = True
    Me.cmb_table_session.TabIndex = 2
    Me.cmb_table_session.TextCombo = Nothing
    Me.cmb_table_session.TextWidth = 87
    '
    'cmb_name
    '
    Me.cmb_name.AllowUnlistedValues = False
    Me.cmb_name.AutoCompleteMode = False
    Me.cmb_name.IsReadOnly = False
    Me.cmb_name.Location = New System.Drawing.Point(6, 50)
    Me.cmb_name.Name = "cmb_name"
    Me.cmb_name.SelectedIndex = -1
    Me.cmb_name.Size = New System.Drawing.Size(281, 24)
    Me.cmb_name.SufixText = "Sufix Text"
    Me.cmb_name.SufixTextVisible = True
    Me.cmb_name.TabIndex = 1
    Me.cmb_name.TextCombo = Nothing
    Me.cmb_name.TextWidth = 87
    '
    'cmb_type
    '
    Me.cmb_type.AllowUnlistedValues = False
    Me.cmb_type.AutoCompleteMode = False
    Me.cmb_type.IsReadOnly = False
    Me.cmb_type.Location = New System.Drawing.Point(6, 20)
    Me.cmb_type.Name = "cmb_type"
    Me.cmb_type.SelectedIndex = -1
    Me.cmb_type.Size = New System.Drawing.Size(281, 24)
    Me.cmb_type.SufixText = "Sufix Text"
    Me.cmb_type.SufixTextVisible = True
    Me.cmb_type.TabIndex = 0
    Me.cmb_type.TextCombo = Nothing
    Me.cmb_type.TextWidth = 87
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.btn_search_account)
    Me.gb_account.Controls.Add(Me.lbl_holder_name)
    Me.gb_account.Controls.Add(Me.ef_account_id)
    Me.gb_account.Controls.Add(Me.ef_card_track)
    Me.gb_account.Controls.Add(Me.chk_unknown)
    Me.gb_account.Location = New System.Drawing.Point(384, 3)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(314, 173)
    Me.gb_account.TabIndex = 1
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xAccount"
    '
    'btn_search_account
    '
    Me.btn_search_account.Location = New System.Drawing.Point(229, 96)
    Me.btn_search_account.Name = "btn_search_account"
    Me.btn_search_account.Size = New System.Drawing.Size(75, 23)
    Me.btn_search_account.TabIndex = 3
    Me.btn_search_account.Text = "xSearch"
    Me.btn_search_account.UseVisualStyleBackColor = True
    '
    'lbl_holder_name
    '
    Me.lbl_holder_name.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_holder_name.AutoSize = True
    Me.lbl_holder_name.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_holder_name.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_holder_name.Location = New System.Drawing.Point(13, 124)
    Me.lbl_holder_name.MaximumSize = New System.Drawing.Size(291, 0)
    Me.lbl_holder_name.Name = "lbl_holder_name"
    Me.lbl_holder_name.Size = New System.Drawing.Size(84, 14)
    Me.lbl_holder_name.TabIndex = 4
    Me.lbl_holder_name.Text = "xHolderName"
    '
    'ef_account_id
    '
    Me.ef_account_id.DoubleValue = 0.0R
    Me.ef_account_id.IntegerValue = 0
    Me.ef_account_id.IsReadOnly = False
    Me.ef_account_id.Location = New System.Drawing.Point(13, 39)
    Me.ef_account_id.Name = "ef_account_id"
    Me.ef_account_id.PlaceHolder = Nothing
    Me.ef_account_id.Size = New System.Drawing.Size(181, 24)
    Me.ef_account_id.SufixText = "Sufix Text"
    Me.ef_account_id.SufixTextVisible = True
    Me.ef_account_id.TabIndex = 1
    Me.ef_account_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_id.TextValue = ""
    Me.ef_account_id.Value = ""
    Me.ef_account_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_card_track
    '
    Me.ef_card_track.DoubleValue = 0.0R
    Me.ef_card_track.IntegerValue = 0
    Me.ef_card_track.IsReadOnly = False
    Me.ef_card_track.Location = New System.Drawing.Point(13, 66)
    Me.ef_card_track.Name = "ef_card_track"
    Me.ef_card_track.PlaceHolder = Nothing
    Me.ef_card_track.Size = New System.Drawing.Size(291, 24)
    Me.ef_card_track.SufixText = "Sufix Text"
    Me.ef_card_track.SufixTextVisible = True
    Me.ef_card_track.TabIndex = 2
    Me.ef_card_track.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_track.TextValue = ""
    Me.ef_card_track.Value = ""
    Me.ef_card_track.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_unknown
    '
    Me.chk_unknown.AutoSize = True
    Me.chk_unknown.Location = New System.Drawing.Point(13, 20)
    Me.chk_unknown.Name = "chk_unknown"
    Me.chk_unknown.Size = New System.Drawing.Size(85, 17)
    Me.chk_unknown.TabIndex = 0
    Me.chk_unknown.Text = "xUnknown"
    Me.chk_unknown.UseVisualStyleBackColor = True
    '
    'gb_gaming_session
    '
    Me.gb_gaming_session.Controls.Add(Me.gb_points)
    Me.gb_gaming_session.Controls.Add(Me.gb_chips)
    Me.gb_gaming_session.Controls.Add(Me.gb_bet)
    Me.gb_gaming_session.Controls.Add(Me.dtp_walk_time)
    Me.gb_gaming_session.Controls.Add(Me.dtp_end_session)
    Me.gb_gaming_session.Controls.Add(Me.dtp_start_session)
    Me.gb_gaming_session.Controls.Add(Me.cmb_skill)
    Me.gb_gaming_session.Controls.Add(Me.cmb_speed)
    Me.gb_gaming_session.Location = New System.Drawing.Point(3, 182)
    Me.gb_gaming_session.Name = "gb_gaming_session"
    Me.gb_gaming_session.Size = New System.Drawing.Size(695, 215)
    Me.gb_gaming_session.TabIndex = 2
    Me.gb_gaming_session.TabStop = False
    Me.gb_gaming_session.Text = "xGamingSession"
    '
    'cmb_isoCode
    '
    Me.cmb_isoCode.AllowUnlistedValues = False
    Me.cmb_isoCode.AutoCompleteMode = False
    Me.cmb_isoCode.IsReadOnly = False
    Me.cmb_isoCode.Location = New System.Drawing.Point(3, 14)
    Me.cmb_isoCode.Name = "cmb_isoCode"
    Me.cmb_isoCode.SelectedIndex = -1
    Me.cmb_isoCode.Size = New System.Drawing.Size(200, 25)
    Me.cmb_isoCode.SufixText = "Sufix Text"
    Me.cmb_isoCode.SufixTextVisible = True
    Me.cmb_isoCode.TabIndex = 0
    Me.cmb_isoCode.TextCombo = Nothing
    Me.cmb_isoCode.TextWidth = 100
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.ef_points)
    Me.gb_points.Controls.Add(Me.lbl_computed_points)
    Me.gb_points.Location = New System.Drawing.Point(478, 125)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(211, 83)
    Me.gb_points.TabIndex = 7
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPoints"
    '
    'ef_points
    '
    Me.ef_points.DoubleValue = 0.0R
    Me.ef_points.IntegerValue = 0
    Me.ef_points.IsReadOnly = False
    Me.ef_points.Location = New System.Drawing.Point(3, 22)
    Me.ef_points.Name = "ef_points"
    Me.ef_points.PlaceHolder = Nothing
    Me.ef_points.Size = New System.Drawing.Size(200, 25)
    Me.ef_points.SufixText = "Sufix Text"
    Me.ef_points.SufixTextVisible = True
    Me.ef_points.TabIndex = 0
    Me.ef_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points.TextValue = ""
    Me.ef_points.TextWidth = 100
    Me.ef_points.Value = ""
    Me.ef_points.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_computed_points
    '
    Me.lbl_computed_points.AutoSize = True
    Me.lbl_computed_points.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_computed_points.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_computed_points.Location = New System.Drawing.Point(7, 58)
    Me.lbl_computed_points.MaximumSize = New System.Drawing.Size(270, 0)
    Me.lbl_computed_points.Name = "lbl_computed_points"
    Me.lbl_computed_points.Size = New System.Drawing.Size(168, 14)
    Me.lbl_computed_points.TabIndex = 1
    Me.lbl_computed_points.Text = "xCalculatedPoints: XXXX"
    '
    'gb_chips
    '
    Me.gb_chips.Controls.Add(Me.ef_chips_in)
    Me.gb_chips.Controls.Add(Me.ef_chips_out)
    Me.gb_chips.Controls.Add(Me.ef_buy_in)
    Me.gb_chips.Location = New System.Drawing.Point(478, 13)
    Me.gb_chips.Name = "gb_chips"
    Me.gb_chips.Size = New System.Drawing.Size(211, 112)
    Me.gb_chips.TabIndex = 6
    Me.gb_chips.TabStop = False
    Me.gb_chips.Text = "xChips"
    '
    'ef_chips_in
    '
    Me.ef_chips_in.DoubleValue = 0.0R
    Me.ef_chips_in.IntegerValue = 0
    Me.ef_chips_in.IsReadOnly = False
    Me.ef_chips_in.Location = New System.Drawing.Point(3, 14)
    Me.ef_chips_in.Name = "ef_chips_in"
    Me.ef_chips_in.PlaceHolder = Nothing
    Me.ef_chips_in.Size = New System.Drawing.Size(200, 24)
    Me.ef_chips_in.SufixText = "Sufix Text"
    Me.ef_chips_in.SufixTextVisible = True
    Me.ef_chips_in.TabIndex = 0
    Me.ef_chips_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_chips_in.TextValue = ""
    Me.ef_chips_in.TextWidth = 100
    Me.ef_chips_in.Value = ""
    Me.ef_chips_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_chips_out
    '
    Me.ef_chips_out.DoubleValue = 0.0R
    Me.ef_chips_out.IntegerValue = 0
    Me.ef_chips_out.IsReadOnly = False
    Me.ef_chips_out.Location = New System.Drawing.Point(3, 44)
    Me.ef_chips_out.Name = "ef_chips_out"
    Me.ef_chips_out.PlaceHolder = Nothing
    Me.ef_chips_out.Size = New System.Drawing.Size(200, 24)
    Me.ef_chips_out.SufixText = "Sufix Text"
    Me.ef_chips_out.SufixTextVisible = True
    Me.ef_chips_out.TabIndex = 1
    Me.ef_chips_out.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_chips_out.TextValue = ""
    Me.ef_chips_out.TextWidth = 100
    Me.ef_chips_out.Value = ""
    Me.ef_chips_out.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_buy_in
    '
    Me.ef_buy_in.DoubleValue = 0.0R
    Me.ef_buy_in.IntegerValue = 0
    Me.ef_buy_in.IsReadOnly = False
    Me.ef_buy_in.Location = New System.Drawing.Point(3, 74)
    Me.ef_buy_in.Name = "ef_buy_in"
    Me.ef_buy_in.PlaceHolder = Nothing
    Me.ef_buy_in.Size = New System.Drawing.Size(200, 24)
    Me.ef_buy_in.SufixText = "Sufix Text"
    Me.ef_buy_in.SufixTextVisible = True
    Me.ef_buy_in.TabIndex = 2
    Me.ef_buy_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_buy_in.TextValue = ""
    Me.ef_buy_in.TextWidth = 100
    Me.ef_buy_in.Value = ""
    Me.ef_buy_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_bet
    '
    Me.gb_bet.Controls.Add(Me.cmb_isoCode)
    Me.gb_bet.Controls.Add(Me.ef_plays)
    Me.gb_bet.Controls.Add(Me.ef_played_amount)
    Me.gb_bet.Controls.Add(Me.ef_current_bet)
    Me.gb_bet.Controls.Add(Me.ef_min_bet)
    Me.gb_bet.Controls.Add(Me.ef_max_bet)
    Me.gb_bet.Location = New System.Drawing.Point(261, 13)
    Me.gb_bet.Name = "gb_bet"
    Me.gb_bet.Size = New System.Drawing.Size(211, 195)
    Me.gb_bet.TabIndex = 5
    Me.gb_bet.TabStop = False
    Me.gb_bet.Text = "xBet"
    '
    'ef_plays
    '
    Me.ef_plays.DoubleValue = 0.0R
    Me.ef_plays.IntegerValue = 0
    Me.ef_plays.IsReadOnly = False
    Me.ef_plays.Location = New System.Drawing.Point(3, 104)
    Me.ef_plays.Name = "ef_plays"
    Me.ef_plays.PlaceHolder = Nothing
    Me.ef_plays.Size = New System.Drawing.Size(200, 24)
    Me.ef_plays.SufixText = "Sufix Text"
    Me.ef_plays.SufixTextVisible = True
    Me.ef_plays.TabIndex = 3
    Me.ef_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_plays.TextValue = ""
    Me.ef_plays.TextWidth = 100
    Me.ef_plays.Value = ""
    Me.ef_plays.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_played_amount
    '
    Me.ef_played_amount.DoubleValue = 0.0R
    Me.ef_played_amount.IntegerValue = 0
    Me.ef_played_amount.IsReadOnly = False
    Me.ef_played_amount.Location = New System.Drawing.Point(3, 74)
    Me.ef_played_amount.Name = "ef_played_amount"
    Me.ef_played_amount.PlaceHolder = Nothing
    Me.ef_played_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_played_amount.SufixText = "Sufix Text"
    Me.ef_played_amount.SufixTextVisible = True
    Me.ef_played_amount.TabIndex = 2
    Me.ef_played_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_played_amount.TextValue = ""
    Me.ef_played_amount.TextWidth = 100
    Me.ef_played_amount.Value = ""
    Me.ef_played_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_current_bet
    '
    Me.ef_current_bet.DoubleValue = 0.0R
    Me.ef_current_bet.IntegerValue = 0
    Me.ef_current_bet.IsReadOnly = False
    Me.ef_current_bet.Location = New System.Drawing.Point(3, 44)
    Me.ef_current_bet.Name = "ef_current_bet"
    Me.ef_current_bet.PlaceHolder = Nothing
    Me.ef_current_bet.Size = New System.Drawing.Size(200, 24)
    Me.ef_current_bet.SufixText = "Sufix Text"
    Me.ef_current_bet.SufixTextVisible = True
    Me.ef_current_bet.TabIndex = 1
    Me.ef_current_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_current_bet.TextValue = ""
    Me.ef_current_bet.TextWidth = 100
    Me.ef_current_bet.Value = ""
    Me.ef_current_bet.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_min_bet
    '
    Me.ef_min_bet.DoubleValue = 0.0R
    Me.ef_min_bet.IntegerValue = 0
    Me.ef_min_bet.IsReadOnly = False
    Me.ef_min_bet.Location = New System.Drawing.Point(3, 134)
    Me.ef_min_bet.Name = "ef_min_bet"
    Me.ef_min_bet.PlaceHolder = Nothing
    Me.ef_min_bet.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_bet.SufixText = "Sufix Text"
    Me.ef_min_bet.SufixTextVisible = True
    Me.ef_min_bet.TabIndex = 4
    Me.ef_min_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_bet.TextValue = ""
    Me.ef_min_bet.TextWidth = 100
    Me.ef_min_bet.Value = ""
    Me.ef_min_bet.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_max_bet
    '
    Me.ef_max_bet.DoubleValue = 0.0R
    Me.ef_max_bet.IntegerValue = 0
    Me.ef_max_bet.IsReadOnly = False
    Me.ef_max_bet.Location = New System.Drawing.Point(3, 164)
    Me.ef_max_bet.Name = "ef_max_bet"
    Me.ef_max_bet.PlaceHolder = Nothing
    Me.ef_max_bet.Size = New System.Drawing.Size(200, 24)
    Me.ef_max_bet.SufixText = "Sufix Text"
    Me.ef_max_bet.SufixTextVisible = True
    Me.ef_max_bet.TabIndex = 5
    Me.ef_max_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_bet.TextValue = ""
    Me.ef_max_bet.TextWidth = 100
    Me.ef_max_bet.Value = ""
    Me.ef_max_bet.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_walk_time
    '
    Me.dtp_walk_time.Checked = True
    Me.dtp_walk_time.IsReadOnly = False
    Me.dtp_walk_time.Location = New System.Drawing.Point(8, 88)
    Me.dtp_walk_time.Name = "dtp_walk_time"
    Me.dtp_walk_time.ShowCheckBox = False
    Me.dtp_walk_time.ShowUpDown = True
    Me.dtp_walk_time.Size = New System.Drawing.Size(205, 24)
    Me.dtp_walk_time.SufixText = "Sufix Text"
    Me.dtp_walk_time.SufixTextVisible = True
    Me.dtp_walk_time.TabIndex = 2
    Me.dtp_walk_time.TextWidth = 100
    Me.dtp_walk_time.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_end_session
    '
    Me.dtp_end_session.Checked = True
    Me.dtp_end_session.IsReadOnly = False
    Me.dtp_end_session.Location = New System.Drawing.Point(8, 57)
    Me.dtp_end_session.Name = "dtp_end_session"
    Me.dtp_end_session.ShowCheckBox = False
    Me.dtp_end_session.ShowUpDown = False
    Me.dtp_end_session.Size = New System.Drawing.Size(250, 24)
    Me.dtp_end_session.SufixText = "Sufix Text"
    Me.dtp_end_session.SufixTextVisible = True
    Me.dtp_end_session.TabIndex = 1
    Me.dtp_end_session.TextWidth = 100
    Me.dtp_end_session.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_start_session
    '
    Me.dtp_start_session.Checked = True
    Me.dtp_start_session.IsReadOnly = False
    Me.dtp_start_session.Location = New System.Drawing.Point(8, 28)
    Me.dtp_start_session.Name = "dtp_start_session"
    Me.dtp_start_session.ShowCheckBox = False
    Me.dtp_start_session.ShowUpDown = False
    Me.dtp_start_session.Size = New System.Drawing.Size(250, 24)
    Me.dtp_start_session.SufixText = "Sufix Text"
    Me.dtp_start_session.SufixTextVisible = True
    Me.dtp_start_session.TabIndex = 0
    Me.dtp_start_session.TextWidth = 100
    Me.dtp_start_session.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'cmb_skill
    '
    Me.cmb_skill.AllowUnlistedValues = False
    Me.cmb_skill.AutoCompleteMode = False
    Me.cmb_skill.IsReadOnly = False
    Me.cmb_skill.Location = New System.Drawing.Point(8, 148)
    Me.cmb_skill.Name = "cmb_skill"
    Me.cmb_skill.SelectedIndex = -1
    Me.cmb_skill.Size = New System.Drawing.Size(205, 24)
    Me.cmb_skill.SufixText = "Sufix Text"
    Me.cmb_skill.SufixTextVisible = True
    Me.cmb_skill.TabIndex = 4
    Me.cmb_skill.TextCombo = Nothing
    Me.cmb_skill.TextWidth = 100
    '
    'cmb_speed
    '
    Me.cmb_speed.AllowUnlistedValues = False
    Me.cmb_speed.AutoCompleteMode = False
    Me.cmb_speed.IsReadOnly = False
    Me.cmb_speed.Location = New System.Drawing.Point(8, 118)
    Me.cmb_speed.Name = "cmb_speed"
    Me.cmb_speed.SelectedIndex = -1
    Me.cmb_speed.Size = New System.Drawing.Size(205, 24)
    Me.cmb_speed.SufixText = "Sufix Text"
    Me.cmb_speed.SufixTextVisible = True
    Me.cmb_speed.TabIndex = 3
    Me.cmb_speed.TextCombo = Nothing
    Me.cmb_speed.TextWidth = 100
    '
    'frm_gaming_tables_session_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(803, 409)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_gaming_tables_session_edit"
    Me.Text = "frm_gaming_tables_session_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_table.ResumeLayout(False)
    Me.gb_table.PerformLayout()
    Me.gb_account.ResumeLayout(False)
    Me.gb_account.PerformLayout()
    Me.gb_gaming_session.ResumeLayout(False)
    Me.gb_points.ResumeLayout(False)
    Me.gb_points.PerformLayout()
    Me.gb_chips.ResumeLayout(False)
    Me.gb_bet.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_table As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_name As GUI_Controls.uc_combo
  Friend WithEvents cmb_type As GUI_Controls.uc_combo
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents ef_account_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_card_track As GUI_Controls.uc_entry_field
  Friend WithEvents chk_unknown As System.Windows.Forms.CheckBox
  Friend WithEvents btn_search_account As System.Windows.Forms.Button
  Friend WithEvents lbl_holder_name As System.Windows.Forms.Label
  Friend WithEvents gb_gaming_session As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_skill As GUI_Controls.uc_combo
  Friend WithEvents cmb_speed As GUI_Controls.uc_combo
  Friend WithEvents dtp_start_session As GUI_Controls.uc_date_picker
  Friend WithEvents ef_played_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_current_bet As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_computed_points As System.Windows.Forms.Label
  Friend WithEvents ef_points As GUI_Controls.uc_entry_field
  Friend WithEvents ef_buy_in As GUI_Controls.uc_entry_field
  Friend WithEvents ef_chips_out As GUI_Controls.uc_entry_field
  Friend WithEvents ef_chips_in As GUI_Controls.uc_entry_field
  Friend WithEvents ef_max_bet As GUI_Controls.uc_entry_field
  Friend WithEvents ef_min_bet As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_table_session As GUI_Controls.uc_combo
  Friend WithEvents dtp_end_session As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_walk_time As GUI_Controls.uc_date_picker
  Friend WithEvents gb_bet As System.Windows.Forms.GroupBox
  Friend WithEvents gb_chips As System.Windows.Forms.GroupBox
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents ef_plays As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_table_session_end As System.Windows.Forms.Label
  Friend WithEvents lbl_table_session_start As System.Windows.Forms.Label
  Friend WithEvents lbl_table_session_status As System.Windows.Forms.Label
  Friend WithEvents cmb_isoCode As GUI_Controls.uc_combo
End Class
