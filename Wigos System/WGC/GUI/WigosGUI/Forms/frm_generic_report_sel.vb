﻿'--------------------------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_generic_report_sel.vb
'
'   DESCRIPTION :  Generic Report From REPORT_TOOL_CONFIG Table
'
'        AUTHOR :  Rodrigo Gonzalez Rodriguez
'
' CREATION DATE :  09-JUN-2016
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 09-JUN-2016  RGR    Initial version 
' 11-JUL-2016  EOR    Product Backlog Item 13573 : Generic Report: GUI generation Screen
' 23-AUG-2016  EOR    Product Backlog Item 13573 : Generic Report: GUI generation Screen
' 30-AUG-2016  ESE    Bug 17161:Show maximized window
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
' 24-NOV-2016  EOR    PBI 19870: R.Franco: New report machines and games
' 08-SEP-2016  RGR    Bug 29652:WIGOS-4981 Accounting - Generic report: Monthly report window is deformed when is opened after maximized mode form
' 12-SEP-2017  EOR    Bug 29728:WIGOS-4977 Accounting - Generic report: The label "Months" or "Date" is changed by "Filters" on generic report form
' 28-SEP-2017  RGR    Bug 29898:WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
' 17-NOV-2017  LTC    Product Backlog Item 30849:Multisite: Reporte genérico terminales
' 05-JAN-2018  EOR    PBI 30849: Multisite: Reporte genérico terminales
' 08-MAR-2018  RGR    Bug 31855: WIGOS-3694 Wrong data file name when exporting Cage movements for currency generic report
'---------------------------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Data.SqlClient
Imports GUI_Reports
Imports WSI.Common.GenericReport
Imports System.Reflection

Public Class frm_generic_report_sel
  Inherits frm_base

#Region "Structures"

  'EOR 24-NOV-2016
  Private Structure GenericReportControls
    Public Control As Control
    Public Filter As WSI.Common.GenericReport.ReportToolDesignFilter
  End Structure

#End Region

#Region "Constans"
  Private Const outPutFolder As String = "Reports"
#End Region

#Region "Members"

  'Domain 
  Private m_reportToolConfig As ReportToolConfigResponse
  Private m_selectReport As ReportToolConfigDTO
  Private m_list_controls As IList(Of GenericReportControls)  'EOR 24-NOV-2016

  'Audit
  Private m_report_type As String = ""
  Private m_date_from As String = ""
  Private m_date_to As String = ""
  Private m_button_clicked As String = ""
  Private m_form_id As ENUM_FORM
  Private m_parameters_print As CLASS_PRINT_DATA  'EOR 05-JUL-2016
  Private m_month_year As String = ""             'EOR 24-NOV-2016

  Private m_params As Dictionary(Of String, String) 'EOR 22-NOV-2017

#End Region

#Region "Constructor"

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    Dim _service As IReportToolConfigService
    Dim _request As ReportToolConfigRequest = New ReportToolConfigRequest()
    Dim _cons_request As GenericReport.ConstructorRequest
    _cons_request = New ConstructorRequest()

    _request.LocationMenu = LocationMenu.CageMenu
    _cons_request.ProfileID = CurrentUser.ProfileId
    _cons_request.ShowAll = CurrentUser.IsSuperUser
    _cons_request.OnlyReportsMailing = False

    _cons_request.ModeType = GetVisibleProfileForms(CurrentUser.GuiId)

    _service = New ReportToolConfigService(_cons_request)
    Me.m_reportToolConfig = _service.GetMenuConfiguration(_request)
    _service.Dispose()

  End Sub

  Public Sub New(ByVal reportToolConfig As ReportToolConfigResponse, ByVal enumForm As ENUM_FORM)

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Me.m_reportToolConfig = reportToolConfig
    Me.m_form_id = enumForm

    'Set te enum form id
    Me.FormId = Me.m_form_id
    MyBase.GUI_SetFormId()

  End Sub

#End Region

#Region "Overrides"

  Public Overrides Sub GUI_SetFormId()

  End Sub

  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    'Combo
    Me.cmb_report.Text = GLB_NLS_GUI_STATISTICS.GetString(481)

    'Dates
    Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(204)
    Me.dt_date_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    Me.dt_date_to.Text = GLB_NLS_GUI_STATISTICS.GetString(310)
    Me.dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    'Buttons
    Me.btn_visualize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(485)
    Me.btn_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6088)
    Me.btn_excel.Text = GLB_NLS_GUI_STATISTICS.GetString(478)
    Me.btn_exit.Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    'Labels
    Me.lbl_filename.Value = GLB_NLS_GUI_STATISTICS.GetString(490)


    Call Me.SetDatesNow()
    'Add Items 
    Call Me.SetReportCombo()

  End Sub

#End Region

#Region "Public Methods"

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)
    Me.WindowState = FormWindowState.Normal
  End Sub

#End Region

#Region "Private Methods"

  Private Sub SetReportCombo()

    For Each item As ReportToolConfigDTO In Me.m_reportToolConfig.ReportToolConfigs
      Me.cmb_report.Add(item.ReportID, item.ReportName)
    Next

  End Sub

  Private Sub SetGenericFilters()
    If Not IsNothing(Me.m_selectReport) Then

      'EOR 24-NOV-2016
      If Not IsNothing(Me.m_selectReport.Filters) Then
        DeleteControlsDynamic()

        Select Case Me.m_selectReport.Filters.FilterType
          Case ReportToolFilterType.MonthYear
            Me.dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
            Me.dt_date_from.Visible = True
            Me.dt_date_to.Visible = False
            Me.dt_date_from.Text = String.Empty
            Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(204)

          Case ReportToolFilterType.FromToDate
            Me.dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
            Me.dt_date_from.Visible = True
            Me.dt_date_to.Visible = True
            Me.dt_date_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
            Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(204)

          Case ReportToolFilterType.CustomFilters
            Me.dt_date_from.Visible = False
            Me.dt_date_to.Visible = False

            'EOR 12-SEP-2017
            If m_selectReport.Filters.FilterText Is Nothing Then
              Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(212)
            Else
              Me.gb_date.Text = m_selectReport.Filters.FilterText.Label
            End If

            CreateControlsDynamic(m_selectReport.Filters.Filters)

        End Select
        ResizeAndRelocationScreenControls()

      End If
    End If
  End Sub

  'EOR 17-NOV-2016
  Private Sub DeleteControlsDynamic()
    If m_list_controls IsNot Nothing Then
      For Each gControl As GenericReportControls In m_list_controls
        For Each gbControl As Control In gb_date.Controls
          If gControl.Control.Name = gbControl.Name Then
            gb_date.Controls.Remove(gbControl)
            Exit For
          End If
        Next
      Next
    End If
  End Sub

  'EOR 24-NOV-2016
  Private Sub ResizeAndRelocationScreenControls()
    Dim _factor As Integer
    Dim _width As Integer
    Dim _height As Integer
    Dim _heightGB As Integer

    _width = 380
    _height = 300
    _heightGB = 95

    Me.Size = New Size(_width, _height)
    Me.gb_date.Size = New Size(330, _heightGB)
    Me.btn_print.Location = New Point(46, 155)
    Me.btn_excel.Location = New Point(151, 155)
    Me.btn_visualize.Location = New Point(256, 155)
    Me.btn_exit.Location = New Point(256, 217)
    Me.lbl_filename.Location = New Point(17, 189)
    Me.lnk_filename.Location = New Point(122, 193)

    If m_selectReport.Filters.FilterType = ReportToolFilterType.CustomFilters Then

      If m_selectReport.Filters.Filters.Length > 2 Then
        _factor = 35 * (m_selectReport.Filters.Filters.Length - 2)
      Else
        _factor = 0
      End If

      Me.Size = New Size(_width, _height + _factor)
      Me.gb_date.Size = New Size(330, _heightGB + _factor)
      Me.btn_print.Location = New Point(46, btn_print.Location.Y + _factor)
      Me.btn_excel.Location = New Point(151, btn_excel.Location.Y + _factor)
      Me.btn_visualize.Location = New Point(256, btn_visualize.Location.Y + _factor)
      Me.btn_exit.Location = New Point(256, btn_exit.Location.Y + _factor)
      Me.lbl_filename.Location = New Point(17, lbl_filename.Location.Y + _factor)
      Me.lnk_filename.Location = New Point(122, lnk_filename.Location.Y + _factor)

    End If
  End Sub

  'EOR 24-NOV-2016
  Private Sub CreateControlsDynamic(ByVal Filters() As ReportToolDesignFilter)
    Dim _ctrl_check_box As CheckBox
    Dim _ctrl_uc_combo As uc_combo
    Dim _ctrl_uc_date_picker As uc_date_picker
    Dim _ctrl_uc_entry_field As uc_entry_field
    Dim _ctrl_radio_button As RadioButton
    Dim _index As Integer
    Dim _structure As GenericReportControls
    Dim _x As Integer
    Dim _y As Integer
    Dim _tabIndex
    Dim _location As Point

    m_list_controls = New List(Of GenericReportControls)

    If Filters IsNot Nothing Then
      _index = 0
      _x = 15
      _y = 20
      _tabIndex = 0

      For Each Filter As WSI.Common.GenericReport.ReportToolDesignFilter In Filters
        _location = New Point(_x, _y)
        _structure = New GenericReportControls()

        Select Case Filter.TypeControl
          Case ReportToolTypeControl.CheckBox
            _ctrl_check_box = New CheckBox()
            _ctrl_check_box.Name = "chk_ctrl_" & _index.ToString("00")
            _ctrl_check_box.AutoSize = True
            _ctrl_check_box.TabIndex = _tabIndex
            _ctrl_check_box.UseVisualStyleBackColor = True
            _ctrl_check_box.Location = _location
            _ctrl_check_box.Size = New Size(280, 24)

            SetValueProperty(_ctrl_check_box, "Text", Filter.TextControl.Label)
            SetPropertys(_ctrl_check_box, Filter.Propertys)
            CallsMethods(_ctrl_check_box, Filter.Methods)
            SetValueProperty(_ctrl_check_box, "Checked", Filter.Value)
            gb_date.Controls.Add(_ctrl_check_box)

            _structure.Control = _ctrl_check_box
            _structure.Filter = Filter

          Case ReportToolTypeControl.uc_combo
            _ctrl_uc_combo = New uc_combo()
            _ctrl_uc_combo.Name = "cmb_ctrl_" & _index.ToString("00")
            _ctrl_uc_combo.AllowUnlistedValues = False
            _ctrl_uc_combo.AutoCompleteMode = False
            _ctrl_uc_combo.IsReadOnly = False
            _ctrl_uc_combo.Location = New System.Drawing.Point(7, 17)
            _ctrl_uc_combo.SelectedIndex = -1
            _ctrl_uc_combo.Size = New System.Drawing.Size(340, 25)
            _ctrl_uc_combo.SufixText = "Sufix Text"
            _ctrl_uc_combo.SufixTextVisible = True
            _ctrl_uc_combo.TabIndex = _tabIndex
            _ctrl_uc_combo.TextCombo = Nothing
            _ctrl_uc_combo.Location = _location
            _ctrl_uc_combo.Size = New Size(280, 24)

            SetValueProperty(_ctrl_uc_combo, "Text", Filter.TextControl.Label)
            SetPropertys(_ctrl_uc_combo, Filter.Propertys)
            CallsMethods(_ctrl_uc_combo, Filter.Methods)

            If Filter.TypeSource = TypeSourceItems.Items Then
              AddItemsCombo(_ctrl_uc_combo, Filter.Items)
            Else
              AddItemsCombo(_ctrl_uc_combo, Filter.Query)
            End If

            SetValueProperty(_ctrl_uc_combo, "Value", Filter.Value)
            gb_date.Controls.Add(_ctrl_uc_combo)

            _structure.Control = _ctrl_uc_combo
            _structure.Filter = Filter

          Case ReportToolTypeControl.uc_date_picker
            _ctrl_uc_date_picker = New uc_date_picker()
            _ctrl_uc_date_picker.Name = "dp_ctrl_" & _index.ToString("00")
            _ctrl_uc_date_picker.Checked = True
            _ctrl_uc_date_picker.IsReadOnly = False
            _ctrl_uc_date_picker.ShowCheckBox = False
            _ctrl_uc_date_picker.ShowUpDown = False
            _ctrl_uc_date_picker.SufixText = "Sufix Text"
            _ctrl_uc_date_picker.SufixTextVisible = True
            _ctrl_uc_date_picker.TabIndex = _tabIndex
            _ctrl_uc_date_picker.Value = WSI.Common.Misc.TodayOpening()
            _ctrl_uc_date_picker.Location = _location
            _ctrl_uc_date_picker.Size = New Size(236, 24)

            SetValueProperty(_ctrl_uc_date_picker, "Text", Filter.TextControl.Label)
            SetPropertys(_ctrl_uc_date_picker, Filter.Propertys)
            CallsMethods(_ctrl_uc_date_picker, Filter.Methods)
            SetValueProperty(_ctrl_uc_date_picker, "Value", WSI.Common.Misc.GetValueDateTimeReportTool(Filter.Value))
            gb_date.Controls.Add(_ctrl_uc_date_picker)

            _structure.Control = _ctrl_uc_date_picker
            _structure.Filter = Filter

          Case ReportToolTypeControl.uc_entry_field
            _ctrl_uc_entry_field = New uc_entry_field
            _ctrl_uc_entry_field.Name = "ef_ctrl_" & _index.ToString("00")
            _ctrl_uc_entry_field.IsReadOnly = False
            _ctrl_uc_entry_field.SufixText = "Sufix Text"
            _ctrl_uc_entry_field.SufixTextVisible = True
            _ctrl_uc_entry_field.TabIndex = _tabIndex
            _ctrl_uc_entry_field.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
            _ctrl_uc_entry_field.Location = _location
            _ctrl_uc_entry_field.Size = New Size(280, 24)

            SetValueProperty(_ctrl_uc_entry_field, "Text", Filter.TextControl.Label)
            SetPropertys(_ctrl_uc_entry_field, Filter.Propertys)
            CallsMethods(_ctrl_uc_entry_field, Filter.Methods)
            SetValueProperty(_ctrl_uc_entry_field, "Value", Filter.Value)
            gb_date.Controls.Add(_ctrl_uc_entry_field)

            _structure.Control = _ctrl_uc_entry_field
            _structure.Filter = Filter

          Case ReportToolTypeControl.RadioButton
            _ctrl_radio_button = New RadioButton
            _ctrl_radio_button.Name = "opt_ctrl_" & _index.ToString("00")
            _ctrl_radio_button.AutoSize = True
            _ctrl_radio_button.TabIndex = _tabIndex
            _ctrl_radio_button.TabStop = True
            _ctrl_radio_button.UseVisualStyleBackColor = True
            _ctrl_radio_button.Location = _location
            _ctrl_radio_button.Size = New Size(280, 24)

            SetValueProperty(_ctrl_radio_button, "Text", Filter.TextControl.Label)
            SetPropertys(_ctrl_radio_button, Filter.Propertys)
            CallsMethods(_ctrl_radio_button, Filter.Methods)
            SetValueProperty(_ctrl_radio_button, "Checked", Filter.Value)
            gb_date.Controls.Add(_ctrl_radio_button)

            _structure.Control = _ctrl_radio_button
            _structure.Filter = Filter

        End Select

        m_list_controls.Insert(_index, _structure)

        _index += 1
        _y += 35
        _tabIndex += 1
      Next
    End If
  End Sub

  'EOR 24-NOV-2016
  Private Sub AddItemsCombo(ByRef ctrl As uc_combo, ByVal items() As GenericReport.Item)
    If items IsNot Nothing Then
      For Each _item As GenericReport.Item In items
        ctrl.Add(_item.Id, _item.Text.Label)
      Next
    End If
  End Sub

  Private Sub AddItemsCombo(ByRef ctrl As uc_combo, ByVal query As String)
    Dim _data_table As DataTable

    _data_table = GUI_GetTableUsingSQL(query, Integer.MaxValue)

    If _data_table IsNot Nothing AndAlso _data_table.Rows.Count > 0 Then
      ctrl.Add(_data_table)
    End If
  End Sub


  'EOR 24-NOV-2016
  Private Sub SetPropertys(ByVal ctrl As Control, ByVal propertys() As GenericReport.Property)
    If propertys IsNot Nothing Then
      For Each _property As GenericReport.Property In propertys
        SetValueProperty(ctrl, _property.Name, _property.Value)
      Next

    End If
  End Sub

  'EOR 24-NOV-2016
  Private Sub SetValueProperty(ByVal ctrl As Control, ByVal propertyName As String, ByVal value As Object)
    Dim _propInfo As PropertyInfo

    Try
      If value.ToString().Trim() <> "" Then
        _propInfo = ctrl.GetType().GetProperty(propertyName)
        _propInfo.SetValue(ctrl, Convert.ChangeType(value, _propInfo.PropertyType), Nothing)
      End If
    Catch ex As Exception
      Log.Exception(ex)
    End Try
  End Sub

  'EOR 24-NOV-2016
  Private Sub CallsMethods(ByVal ctrl As Control, ByVal methods() As GenericReport.Method)
    If methods IsNot Nothing Then
      For Each _method As GenericReport.Method In methods
        CallMethodControl(ctrl, _method.Name, _method.Parameters)
      Next
    End If
  End Sub

  'EOR 24-NOV-2016
  Private Sub CallMethodControl(ByVal ctrl As Control, ByVal methodName As String, ByVal parametersMethod As String)
    Dim _method As MethodInfo
    Dim _objects() As Object
    Dim _index As Integer

    Try
      _method = ctrl.GetType().GetMethod(methodName)

      ReDim _objects(parametersMethod.Split(",").Length - 1)

      _index = 0
      For Each _parameter As String In parametersMethod.Split(",")
        _objects(_index) = _parameter
        _index += 1
      Next

      _index = 0
      For Each _parameterMethod As System.Reflection.ParameterInfo In _method.GetParameters()
        If _parameterMethod.ParameterType Is GetType(ENUM_FORMAT_DATE) Then
          _objects(_index) = CType(_objects(_index), ENUM_FORMAT_DATE)
        ElseIf _parameterMethod.ParameterType Is GetType(ENUM_FORMAT_TIME) Then
          _objects(_index) = CType(_objects(_index), ENUM_FORMAT_TIME)
        ElseIf _parameterMethod.ParameterType Is GetType(CLASS_FILTER.ENUM_FORMAT) Then
          _objects(_index) = CType(_objects(_index), CLASS_FILTER.ENUM_FORMAT)
        Else
          _objects(_index) = Convert.ChangeType(_objects(_index), _parameterMethod.ParameterType)
        End If

        _index += 1
      Next

      _method.Invoke(ctrl, _objects)
    Catch ex As Exception
      Log.Exception(ex)
    End Try
  End Sub

  'EOR 24-NOV-2016
  Private Function GUI_ReportFiltersOk() As Boolean

    Select Case Me.m_selectReport.Filters.FilterType
      Case ReportToolFilterType.FromToDate
        If Me.dt_date_from.Value > Me.dt_date_to.Value Then
          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call Me.dt_date_to.Focus()
          Return False
        End If

      Case ReportToolFilterType.CustomFilters
        For Each gReportControl As GenericReportControls In m_list_controls
          Select Case True
            Case TypeOf gReportControl.Control Is uc_combo
              If CType(gReportControl.Control, uc_combo).SelectedIndex = -1 Then
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7764), ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, GLB_NLS_GUI_PLAYER_TRACKING.GetString(123) & " " & gReportControl.Filter.TextControl.Label)
                gReportControl.Control.Focus()
                Return False
              End If

            Case TypeOf gReportControl.Control Is uc_entry_field
              If CType(gReportControl.Control, uc_entry_field).Value.Trim() = "" Then
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7764), ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, GLB_NLS_GUI_PLAYER_TRACKING.GetString(123) & " " & gReportControl.Filter.TextControl.Label)
                gReportControl.Control.Focus()
                Return False
              End If

          End Select
        Next

    End Select

    Return True
  End Function

  Private Function GetSelectReport(ByVal reportID As Integer) As ReportToolConfigDTO
    Dim _selectReport As ReportToolConfigDTO

    _selectReport = Nothing

    For Each item As ReportToolConfigDTO In Me.m_reportToolConfig.ReportToolConfigs
      If item.ReportID = reportID Then
        _selectReport = item
        Exit For
      End If
    Next

    Return _selectReport
  End Function

  Private Sub SetDatesNow()

    'EOR 24-NOV-2016
    Me.dt_date_from.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    Me.dt_date_to.Value = WSI.Common.Misc.TodayOpening()

  End Sub

  Private Sub AuditClick()
    Dim _print_data As CLASS_PRINT_DATA

    _print_data = New CLASS_PRINT_DATA

    Call GUI_ReportUpdateFilters()
    Call GUI_ReportFilter(_print_data)
    Call MyBase.AuditFormClick(ENUM_BUTTON.BUTTON_FILTER_APPLY, _print_data)

    'EOR 05-JUL-2016
    m_parameters_print = _print_data
  End Sub

  Private Sub GUI_ReportUpdateFilters()

    Me.m_report_type = Me.m_selectReport.ReportName

    Select Case Me.m_selectReport.Filters.FilterType
      Case ReportToolFilterType.MonthYear
        Me.m_month_year = Me.dt_date_from.Value.ToString("MMMM yyyy")
        Me.m_date_from = ""
        Me.m_date_to = ""

      Case ReportToolFilterType.FromToDate
        Me.m_month_year = ""
        Me.m_date_to = Me.dt_date_to.Value.ToString("G")
        Me.m_date_from = Me.dt_date_from.Value.ToString("G")

        'EOR 24-NOV-2016
      Case ReportToolFilterType.CustomFilters
        For Each gReportControl As GenericReportControls In m_list_controls
          Select Case True
            Case TypeOf gReportControl.Control Is RadioButton
              gReportControl.Filter.Value = CType(gReportControl.Control, RadioButton).Checked.ToString()

            Case TypeOf gReportControl.Control Is CheckBox
              gReportControl.Filter.Value = CType(gReportControl.Control, CheckBox).Checked.ToString()

            Case TypeOf gReportControl.Control Is uc_combo
              gReportControl.Filter.Value = CType(gReportControl.Control, uc_combo).Value.ToString()

            Case TypeOf gReportControl.Control Is uc_entry_field
              gReportControl.Filter.Value = CType(gReportControl.Control, uc_entry_field).Value

            Case TypeOf gReportControl.Control Is uc_date_picker
              gReportControl.Filter.Value = CType(gReportControl.Control, uc_date_picker).Value.ToString("yyyy-MM-dd HH:mm:ss")

          End Select

        Next
    End Select

  End Sub

  Private Sub GUI_ReportFilter(ByVal printData As GUI_Reports.CLASS_PRINT_DATA)
    'EOR 22-NOV-2017
    m_params = New Dictionary(Of String, String)
    m_params.Add("SITE", GetSiteOrMultiSiteName())

    Select Case Me.m_selectReport.Filters.FilterType
      Case ReportToolFilterType.MonthYear
        printData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204), m_month_year)

        m_params.Add(GLB_NLS_GUI_STATISTICS.GetString(204), m_month_year)
        m_params.Add("", "")

      Case ReportToolFilterType.FromToDate
        printData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
        printData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)

        m_params.Add(GLB_NLS_GUI_STATISTICS.GetString(309), GUI_FormatDate(dt_date_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))
        m_params.Add(GLB_NLS_GUI_STATISTICS.GetString(310), GUI_FormatDate(dt_date_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))
        m_params.Add("", "")

      Case ReportToolFilterType.CustomFilters
        For Each gReportControl As GenericReportControls In m_list_controls
          'EOR 12-SEP-2017
          If gReportControl.Filter.TypeControl = ReportToolTypeControl.uc_date_picker Then
            Try
              For Each _control_win As Control In gReportControl.Control.Controls("panel_control").Controls
                If _control_win.GetType() Is GetType(DateTimePicker) Then
                  printData.SetFilter(gReportControl.Filter.TextControl.Label, CDate(gReportControl.Filter.Value).ToString(CType(_control_win, DateTimePicker).CustomFormat))
                  m_params.Add(gReportControl.Filter.TextControl.Label, CDate(gReportControl.Filter.Value).ToString(CType(_control_win, DateTimePicker).CustomFormat))
                End If
              Next
            Catch ex As Exception
              printData.SetFilter(gReportControl.Filter.TextControl.Label, gReportControl.Filter.Value)
              m_params.Add(gReportControl.Filter.TextControl.Label, gReportControl.Filter.Value)
            End Try
          Else
            printData.SetFilter(gReportControl.Filter.TextControl.Label, gReportControl.Filter.Value)
            m_params.Add(gReportControl.Filter.TextControl.Label, gReportControl.Filter.Value)
          End If
        Next

        m_params.Add("", "")
    End Select

    m_params.Add(GLB_NLS_GUI_CONTROLS.GetString(377), GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    m_params.Add(GLB_NLS_GUI_CONTROLS.GetString(378), CurrentUser.Name & "@" & Environment.MachineName)

  End Sub

  Private Function GetReportData() As ReportToolGetDataResponse
    Dim _getDataService As IReportToolGetDataService
    Dim _data_request As GetDataBaseReportRequest
    Dim _response As ReportToolGetDataResponse
    Dim _show_print As Boolean
    Dim _dbParameters() As SqlParameter
    Dim _variables As VariableDTO()

    _getDataService = New ReportToolGetDataService()
    _data_request = New GetDataBaseReportRequest()
    _data_request.Report = Me.m_selectReport
    _show_print = Me.m_selectReport.Filters.ShowPrint
    _variables = Me.m_selectReport.Filters.Variables

    Select Case Me.m_selectReport.Filters.FilterType
      Case ReportToolFilterType.MonthYear
        Dim _filterMonthYear As ReportToolMonthYearFilter

        _filterMonthYear = New ReportToolMonthYearFilter()
        ReDim _dbParameters(2)

        _filterMonthYear.FilterType = _data_request.Report.Filters.FilterType
        _filterMonthYear.Month = Me.dt_date_from.Value.Month
        _filterMonthYear.Year = Me.dt_date_from.Value.Year

        _dbParameters(0) = New SqlParameter("@pMonth", Me.dt_date_from.Value)
        _dbParameters(1) = New SqlParameter("@pYear", Me.dt_date_from.Value)
        _filterMonthYear.dbParameters = _dbParameters

        _filterMonthYear.ShowPrint = _show_print
        _filterMonthYear.Variables = _variables

        _data_request.Report.Filters = _filterMonthYear
      Case ReportToolFilterType.FromToDate
        Dim _filterFromToDate As ReportToolFromToDateFilter

        _filterFromToDate = New ReportToolFromToDateFilter()
        ReDim _dbParameters(2)

        _filterFromToDate.FilterType = _data_request.Report.Filters.FilterType
        _filterFromToDate.From = Me.dt_date_from.Value
        _filterFromToDate.To = Me.dt_date_to.Value

        _dbParameters(0) = New SqlParameter("@pFrom", Me.dt_date_from.Value)
        _dbParameters(1) = New SqlParameter("@pTo", Me.dt_date_to.Value)
        _filterFromToDate.dbParameters = _dbParameters

        _filterFromToDate.ShowPrint = _show_print
        _filterFromToDate.Variables = _variables

        _data_request.Report.Filters = _filterFromToDate
        'EOR 24-NOV-2016
      Case ReportToolFilterType.CustomFilters
        Dim _filterCustom As ReportToolCustomFilter

        Dim _index As Integer

        _filterCustom = New ReportToolCustomFilter()
        ReDim _dbParameters(m_list_controls.Count - 1)

        _index = 0
        For Each gReportControl As GenericReportControls In m_list_controls
          _dbParameters(_index) = New SqlParameter()
          _dbParameters(_index).ParameterName = gReportControl.Filter.ParameterStoredProcedure.Name
          _dbParameters(_index).SqlDbType = gReportControl.Filter.ParameterStoredProcedure.Type

          Select Case True
            Case TypeOf gReportControl.Control Is RadioButton
              _dbParameters(_index).Value = CType(gReportControl.Control, RadioButton).Checked

            Case TypeOf gReportControl.Control Is CheckBox
              _dbParameters(_index).Value = CType(gReportControl.Control, CheckBox).Checked

            Case TypeOf gReportControl.Control Is uc_combo
              _dbParameters(_index).Value = CType(gReportControl.Control, uc_combo).Value

            Case TypeOf gReportControl.Control Is uc_entry_field
              _dbParameters(_index).Value = CType(gReportControl.Control, uc_entry_field).Value

            Case TypeOf gReportControl.Control Is uc_date_picker
              _dbParameters(_index).Value = CType(gReportControl.Control, uc_date_picker).Value

          End Select

          _index += 1

        Next

        _filterCustom.FilterType = _data_request.Report.Filters.FilterType
        _filterCustom.Filters = _data_request.Report.Filters.Filters
        _filterCustom.ShowPrint = _show_print
        _filterCustom.Variables = _variables

        _filterCustom.dbParameters = _dbParameters
        _data_request.Report.Filters = _filterCustom

    End Select

    _response = _getDataService.GetDataBaseReport(_data_request)

    Return _response
  End Function

  Private Sub SetLinkPathVisible(ByVal isVisible As Boolean)

    Me.lbl_filename.Visible = isVisible
    Me.lnk_filename.Visible = isVisible

  End Sub

  Private Function GetFileName() As String
    Dim _nameFile As String
    Dim _name As String

    _nameFile = String.Empty
    _name = Me.GetName(Me.m_selectReport.ReportName)
    Select Case Me.m_selectReport.Filters.FilterType

      Case ReportToolFilterType.MonthYear
        _nameFile = String.Format("{0}-{1}", _name, Me.dt_date_from.Value.ToString("yyyyMM"))

      Case ReportToolFilterType.FromToDate
        _nameFile = String.Format("{0}-{1}-{2}", _name, Me.dt_date_from.Value.ToString("yyyyMMdd"), Me.dt_date_to.Value.ToString("yyyyMMdd"))

        'EOR 24-NOV-2016
      Case ReportToolFilterType.CustomFilters
        _nameFile = String.Format("{0}-{1}", _name, WSI.Common.Misc.TodayOpening().ToString("yyyyMM"))

    End Select


    Return _nameFile

  End Function

  Private Function GetName(ByVal nameReport As String) As String
    Dim _site_id As Integer
    Dim _name As String
    Dim _words As String()

    Try

      If Not Integer.TryParse(WSI.Common.GeneralParam.GetInt32("Site", "Identifier"), _site_id) Then
        _site_id = 0
      End If

      _name = String.Empty
      _words = nameReport.Split(" ")

      If _words.Length = 1 Then
        _name = _words(0)
        If _name.Length > 2 Then
          _name = _name.Substring(0, 3)

        End If

        If String.IsNullOrEmpty(_name) Then
          _name = "REP"
        End If

        Return String.Format("{0}-{1}", _name, _site_id.ToString("000")).ToUpper()

      End If

      For Each _word As String In _words
        If _word.Length > 3 Then
          _name = String.Format("{0}{1}", _name, _word(0).ToString().ToUpper())
        End If

        If _name.Length = 3 Then
          Exit For

        End If
      Next

      If String.IsNullOrEmpty(_name) Then
        _name = "REP"
      End If

    Catch
      _name = "REP"
    End Try

    Return String.Format("{0}-{1}", _name, _site_id.ToString("000"))

  End Function

  Private Function GetPrint(ByVal fileName As String, ByVal sheets As IList(Of ReportToolDesignSheetsDataDTO)) As Boolean
    Dim _getDataService As IReportToolGetDataService
    Dim _request As CreatePDFRequest

    _getDataService = New ReportToolGetDataService()
    _request = New CreatePDFRequest()

    _request.FileName = fileName
    _request.Sheets = sheets
    _request.Filters = Me.m_selectReport.Filters
    _request.Header = Me.m_selectReport.Header
    _request.Footer = Me.m_selectReport.Footer
    _request.UserName = CurrentUser.Name
    _request.UserFullName = CurrentUser.FullName

    Return _getDataService.IsCreatePDF(_request)

  End Function


#End Region

#Region "Events"

  Private Sub cmb_report_ValueChangedEvent() Handles cmb_report.ValueChangedEvent

    If Me.m_reportToolConfig.ReportToolConfigs.Count > 0 Then
      Me.m_selectReport = Me.GetSelectReport(Me.cmb_report.Value)
      Call Me.SetGenericFilters()
      Call Me.SetDatesNow()

      If (Me.m_selectReport.Filters.ShowPrint) Then
        Me.btn_print.Visible = True
      Else
        Me.btn_print.Visible = False
      End If

    End If

    Call SetLinkPathVisible(False)

  End Sub

  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent
    Me.Close()
  End Sub

  Private Sub btn_excel_ClickEvent() Handles btn_excel.ClickEvent
    Dim _dataResponse As ReportToolGetDataResponse
    Dim _fileName As String
    Dim _has_permissions_over_directory As Boolean
    Dim _file_stream As FileStream
    Dim _num_records As Integer
    Dim _progress As frm_excel_progress

    Call SetLinkPathVisible(False)
    Me.btn_excel.Enabled = False

    _dataResponse = New ReportToolGetDataResponse()
    m_button_clicked = btn_excel.Text
    _has_permissions_over_directory = True
    _fileName = System.Environment.CurrentDirectory

    Call AuditClick()

    If Not GUI_ReportFiltersOk() Then
      Me.btn_excel.Enabled = True
      Return
    End If

    If ExcelConversion.HasPermissionsOverDirectory(_fileName) Then
      _fileName = System.Environment.CurrentDirectory & Path.DirectorySeparatorChar & ".." & Path.DirectorySeparatorChar & outPutFolder
      If Not Directory.Exists(_fileName) Then
        Directory.CreateDirectory(_fileName)
      Else
        If Not ExcelConversion.HasPermissionsOverDirectory(_fileName) Then
          _has_permissions_over_directory = False

        End If
      End If
    Else
      _has_permissions_over_directory = False
    End If

    If Not _has_permissions_over_directory Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2736), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _fileName)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                        GLB_ApplicationName, _
                                        "frm_generic_report_sel.vb", _
                                        "", _
                                        "", _
                                        "", _
                                       "Error, don't have permissions to create excel file in directory: " & _fileName & ".")
      Me.btn_excel.Enabled = True
      Return
    End If

    _fileName = String.Format("{0}{1}{2}", _fileName, Path.DirectorySeparatorChar, Me.GetFileName())
    _fileName = ExcelConversion.GetFullFileName(_fileName)

    'Check filename doesn't exists before open it.  If does, ask user
    If File.Exists(_fileName) Then

      ' 122 "The file %1 is in read-only mode.\n\nIt won't be possible to generate the file."
      If (File.GetAttributes(_fileName) And FileAttributes.ReadOnly) <> 0 Then
        NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(122), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
                   Path.GetFileName(_fileName))
        Me.btn_excel.Enabled = True
        Return
      End If

      ' 116 "File %1 already exists.  \n\nDo you want to overwrite it?"
      If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(116), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _
                    Path.GetFileName(_fileName)) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.btn_excel.Enabled = True
        Return
      End If

      'To know if the file is already open (it could be happen on Excel's files),
      'let's try to open it on exclusive mode.  If its not possible, ask user to close the file
      Try
        _file_stream = File.Open(_fileName, FileMode.Open, FileAccess.Write, FileShare.None)
        Call _file_stream.Close()
      Catch ex As Exception

        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(121), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
                        Path.GetFileName(_fileName))
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.m_selectReport.ReportName, "btn_excel.ClickEvent", ex.Message)
        Me.btn_excel.Enabled = True
        Return
      End Try
    End If

    _num_records = 0

    _progress = New frm_excel_progress()
    Try
      _dataResponse = Me.GetReportData()
      _progress.Show()
      For Each _item As ReportToolDesignSheetsDataDTO In _dataResponse.MetaDatas
        _num_records = _num_records + _item.DataReport.Rows.Count
      Next
      _progress.SetValues(0, _num_records)

      For _i As Integer = 0 To _num_records - 1
        _progress.SetValues(_i, _num_records)
      Next

      If _dataResponse.Success Then 'EOR 22-NOV-2017
        If (ExcelConversion.DataSetToExcel(_dataResponse.MetaDatas, _fileName, m_params)) Then

          Call SetLinkPathVisible(True)
          Me.lnk_filename.Text = Path.GetFileName(_fileName)
          Me.lnk_filename.Links(0).LinkData = _fileName
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2213), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call SetLinkPathVisible(False)
        End If

      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_dataResponse.Code), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Call SetLinkPathVisible(False)
      End If
    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(IIf(_dataResponse.Message <> "", 7764, _dataResponse.Code)), IIf(_dataResponse.Code = 7763, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO), mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _dataResponse.Message)
    Finally

      If _progress IsNot Nothing Then
        _progress.Close()
        _progress.Dispose()
      End If

    End Try


    Me.btn_excel.Enabled = True
  End Sub

  Private Sub btn_visualize_ClickEvent() Handles btn_visualize.ClickEvent
    Me.btn_visualize.Enabled = False

    Dim _dataResponse As ReportToolGetDataResponse
    Dim frm_view As frm_generic_report_view

    'EOR 24-NOV-2016
    Call AuditClick()

    If Not GUI_ReportFiltersOk() Then
      Me.btn_visualize.Enabled = True
      Return
    End If

    m_button_clicked = btn_visualize.Text
    _dataResponse = Me.GetReportData()

    If _dataResponse.Success Then
      'EOR 23-AUG-2016
      frm_view = New frm_generic_report_view(_dataResponse.MetaDatas, m_report_type, m_parameters_print, m_form_id)
      frm_view.ShowEditItem()
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(IIf(_dataResponse.Message <> "", 7764, _dataResponse.Code)),
                      IIf(_dataResponse.Code = 7763, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO), mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _dataResponse.Message)
    End If

    Me.btn_visualize.Enabled = True
  End Sub

  Private Sub lnk_filename_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnk_filename.LinkClicked
    Dim _target As String

    _target = e.Link.LinkData

    Try
      Process.Start(_target)

    Catch ex As Exception
      If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(130), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
                 Path.GetFileName(_target)) = ENUM_MB_RESULT.MB_RESULT_OK Then
        Call SetLinkPathVisible(False)
      End If

    End Try
  End Sub

  Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent
    Dim _dataResponse As ReportToolGetDataResponse
    Dim _fileName As String
    Dim _has_permissions_over_directory As Boolean
    Dim _file_stream As FileStream
    Dim _num_records As Integer
    Dim _progress As frm_excel_progress


    Call SetLinkPathVisible(False)
    Me.btn_print.Enabled = False

    _dataResponse = New ReportToolGetDataResponse()
    m_button_clicked = btn_print.Text
    _has_permissions_over_directory = True
    _fileName = System.Environment.CurrentDirectory

    Call AuditClick()

    If Not GUI_ReportFiltersOk() Then
      Me.btn_print.Enabled = True
      Return
    End If

    If ExcelConversion.HasPermissionsOverDirectory(_fileName) Then
      _fileName = System.Environment.CurrentDirectory & Path.DirectorySeparatorChar & ".." & Path.DirectorySeparatorChar & outPutFolder
      If Not Directory.Exists(_fileName) Then
        Directory.CreateDirectory(_fileName)
      Else
        If Not ExcelConversion.HasPermissionsOverDirectory(_fileName) Then
          _has_permissions_over_directory = False

        End If
      End If
    Else
      _has_permissions_over_directory = False
    End If

    If Not _has_permissions_over_directory Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2736), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _fileName)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                        GLB_ApplicationName, _
                                        "frm_generic_report_sel.vb", _
                                        "", _
                                        "", _
                                        "", _
                                       "Error, don't have permissions to create print file in directory: " & _fileName & ".")
      Me.btn_print.Enabled = True
      Return
    End If

    _fileName = String.Format("{0}{1}{2}{3}", _fileName, Path.DirectorySeparatorChar, Me.GetFileName(), ".pdf")

    'Check filename doesn't exists before open it.  If does, ask user
    If File.Exists(_fileName) Then

      ' 122 "The file %1 is in read-only mode.\n\nIt won't be possible to generate the file."
      If (File.GetAttributes(_fileName) And FileAttributes.ReadOnly) <> 0 Then
        NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(122), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
                   Path.GetFileName(_fileName))
        Me.btn_print.Enabled = True
        Return
      End If

      ' 116 "File %1 already exists.  \n\nDo you want to overwrite it?"
      If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(116), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _
                    Path.GetFileName(_fileName)) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.btn_print.Enabled = True
        Return
      End If

      'To know if the file is already open (it could be happen on Excel's files),
      'let's try to open it on exclusive mode.  If its not possible, ask user to close the file
      Try
        _file_stream = File.Open(_fileName, FileMode.Open, FileAccess.Write, FileShare.None)
        Call _file_stream.Close()
      Catch ex As Exception

        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(121), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _
                        Path.GetFileName(_fileName))
        Call Trace.WriteLine(ex.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.m_selectReport.ReportName, "btn_print.ClickEvent", ex.Message)
        Me.btn_print.Enabled = True
        Return
      End Try
    End If

    _num_records = 0

    _progress = New frm_excel_progress(btn_print.Text)
    Try
      _dataResponse = Me.GetReportData()
      _progress.Show()
      For Each _item As ReportToolDesignSheetsDataDTO In _dataResponse.MetaDatas
        _num_records = _num_records + _item.DataReport.Rows.Count
      Next
      _progress.SetValues(0, _num_records)

      For _i As Integer = 0 To _num_records - 1
        _progress.SetValues(_i, _num_records)
      Next

      If _dataResponse.Success Then
        If (Me.GetPrint(_fileName, _dataResponse.MetaDatas)) Then

          Call SetLinkPathVisible(True)
          Me.lnk_filename.Text = Path.GetFileName(_fileName)
          Me.lnk_filename.Links(0).LinkData = _fileName
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2213), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call SetLinkPathVisible(False)
        End If

      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(_dataResponse.Code), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Call SetLinkPathVisible(False)
      End If
    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(IIf(_dataResponse.Message <> "", 7764, _dataResponse.Code)), IIf(_dataResponse.Code = 7763, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO), mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _dataResponse.Message)
    Finally

      If _progress IsNot Nothing Then
        _progress.Close()
        _progress.Dispose()
      End If

    End Try

    Me.btn_print.Enabled = True

  End Sub

#End Region

End Class
