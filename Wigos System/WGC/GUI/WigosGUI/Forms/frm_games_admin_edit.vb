'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_games_admin_edit
' DESCRIPTION:   Games Admin
' AUTHOR:        Ariel Vasquez - Gustavo Al�
' CREATION DATE: 01-SEP-2016
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  --------   -----------------------------------------------
' 01-SEP-2016  AVZ GDA    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_games_admin_edit
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  Protected Const IMG_MAX_WIDTH As Integer = 1280
  Protected Const IMG_MAX_HEIGH As Integer = 1024

  Protected Const CAMPAIGN_DEFAULT_DURATION As Integer = 15 ' In days 
#End Region

#Region " Members "

  Private m_input_game_title As String
  Private m_delete_operation As Boolean
#End Region

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMES_ADMIN_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Dim _show_delete_button As Boolean
    _show_delete_button = False

    ' Set Label values
    Me.lbl_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7546) ' Label name
    Me.lbl_category.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7548)
    Me.lblVisible.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7752)
    Me.lbl_url.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7650)
    Me.lbl_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418)

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7532) + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7915)

    Me.Uc_imageList.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      _show_delete_button = False

 

    ElseIf Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      _show_delete_button = True
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = _show_delete_button
    CategoriesComboFill()
  End Sub

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _game As CLASS_GAMES_ADMIN

    _game = DbReadObject

    Me.txt_name.Text = _game.Title
    Me.cmb_category.Value = _game.Category

    If Not IsNothing(_game.Image) Then
      Me.Uc_imageList.Image = _game.Image
    Else
      Me.Uc_imageList.Image = Nothing
    End If

    Me.chk_app.Checked = _game.App_Visible

    Me.txt_url.Text = _game.Url
    Me.txt_code.Text = _game.Code

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim _game As CLASS_GAMES_ADMIN

    _game = DbEditedObject

    _game.Title = Me.txt_name.Text
    _game.Category = Me.cmb_category.Value

    If Not IsNothing(Me.Uc_imageList.Image) Then
      _game.Image = Me.Uc_imageList.Image
    Else
      _game.Image = Nothing
    End If

    _game.App_Visible = Me.chk_app.Checked

    _game.Url = Me.txt_url.Text
    _game.Code = Me.txt_code.Text

    ''_game.LastUpdate = WSI.Common.WGDB.Now

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _uri_result As System.Uri

    If String.IsNullOrEmpty(Me.txt_name.Text) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7546))
      Call txt_name.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.txt_code.Text) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(3418))
      Call txt_name.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.txt_url.Text) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7650))
      Call txt_name.Focus()

      Return False
    End If

    _uri_result = Nothing

    If Not txt_url.Text = String.Empty Then
      If Not Uri.IsWellFormedUriString(txt_url.Text, UriKind.Absolute) Then
        ' the url is valid
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7884), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call txt_url.Focus()
        Return False
      End If
    End If

    If cmb_category.SelectedIndex < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7548))
      Call cmb_category.Focus()

      Return False
    End If

    If (Me.Uc_imageList.Image Is Nothing) Then
      ' Must have picture
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7639), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem(ByVal gameCode As String, ByVal gameTitle As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = gameCode
    Me.m_input_game_title = gameTitle
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal gameCode As String, ByVal gameTitle As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = gameCode

    Me.m_input_game_title = gameTitle

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _game As CLASS_GAMES_ADMIN
    Dim _rc As Integer

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_GAMES_ADMIN
        _game = Me.DbEditedObject

        With _game
          .Title = Me.m_input_game_title
          .Code = Me.DbObjectId
        End With
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        m_delete_operation = False

        ' Delete confirmation
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(718), _
                                 ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                 ENUM_MB_BTN.MB_BTN_YES_NO, _
                                 ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                                 Me.m_input_game_title)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

  ' PURPOSE : Get string of play session
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - String: 
  '
  

  ' PURPOSE : Override default behavior when user press key
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :


#End Region

#Region "Privates"
  Private Sub CategoriesComboFill()
    Dim _table As DataTable

    _table = GUI_GetTableUsingSQL("SELECT gec_game_external_categories_id, gec_title FROM mapp_games_external_categories WHERE(gec_active = 1)", Integer.MaxValue)

    Me.cmb_category.Clear()
    Me.cmb_category.Add(_table)

  End Sub ' CategoriesComboFill
#End Region
End Class