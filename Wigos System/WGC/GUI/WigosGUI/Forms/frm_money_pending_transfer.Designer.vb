<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_money_pending_transfer
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.gb_account = New System.Windows.Forms.GroupBox
    Me.ef_trackdata = New GUI_Controls.uc_entry_field
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel
    Me.opt_trackdata = New System.Windows.Forms.RadioButton
    Me.opt_all_accounts = New System.Windows.Forms.RadioButton
    Me.opt_transfer_to = New System.Windows.Forms.RadioButton
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_account)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1216, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_account, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(1216, 393)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1210, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1210, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(242, 4)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(325, 189)
    Me.uc_pr_list.TabIndex = 2
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(17, 7)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(222, 72)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(208, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 12)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(208, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.ef_trackdata)
    Me.gb_account.Controls.Add(Me.uc_account_sel1)
    Me.gb_account.Controls.Add(Me.opt_trackdata)
    Me.gb_account.Controls.Add(Me.opt_all_accounts)
    Me.gb_account.Controls.Add(Me.opt_transfer_to)
    Me.gb_account.Location = New System.Drawing.Point(576, 7)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(467, 183)
    Me.gb_account.TabIndex = 3
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xCuenta"
    '
    'ef_trackdata
    '
    Me.ef_trackdata.DoubleValue = 0
    Me.ef_trackdata.IntegerValue = 0
    Me.ef_trackdata.IsReadOnly = False
    Me.ef_trackdata.Location = New System.Drawing.Point(153, 18)
    Me.ef_trackdata.Name = "ef_trackdata"
    Me.ef_trackdata.Size = New System.Drawing.Size(233, 24)
    Me.ef_trackdata.SufixText = "Sufix Text"
    Me.ef_trackdata.SufixTextVisible = True
    Me.ef_trackdata.TabIndex = 1
    Me.ef_trackdata.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_trackdata.TextValue = ""
    Me.ef_trackdata.TextWidth = 0
    Me.ef_trackdata.Value = ""
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(152, 43)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 3
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'opt_trackdata
    '
    Me.opt_trackdata.Location = New System.Drawing.Point(8, 18)
    Me.opt_trackdata.Name = "opt_trackdata"
    Me.opt_trackdata.Size = New System.Drawing.Size(138, 24)
    Me.opt_trackdata.TabIndex = 0
    Me.opt_trackdata.Text = "xTarjeta origen"
    '
    'opt_all_accounts
    '
    Me.opt_all_accounts.Location = New System.Drawing.Point(8, 78)
    Me.opt_all_accounts.Name = "opt_all_accounts"
    Me.opt_all_accounts.Size = New System.Drawing.Size(122, 24)
    Me.opt_all_accounts.TabIndex = 4
    Me.opt_all_accounts.Text = "xAll"
    '
    'opt_transfer_to
    '
    Me.opt_transfer_to.Location = New System.Drawing.Point(8, 48)
    Me.opt_transfer_to.Name = "opt_transfer_to"
    Me.opt_transfer_to.Size = New System.Drawing.Size(138, 24)
    Me.opt_transfer_to.TabIndex = 2
    Me.opt_transfer_to.Text = "xTransferido a"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(17, 92)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(222, 17)
    Me.chk_terminal_location.TabIndex = 1
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_money_pending_transfer
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1224, 591)
    Me.Name = "frm_money_pending_transfer"
    Me.Text = "frm_money_pending_transfer"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_account.ResumeLayout(False)
    Me.gb_account.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Public WithEvents opt_trackdata As System.Windows.Forms.RadioButton
  Public WithEvents opt_all_accounts As System.Windows.Forms.RadioButton
  Public WithEvents opt_transfer_to As System.Windows.Forms.RadioButton
  Friend WithEvents ef_trackdata As GUI_Controls.uc_entry_field
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
