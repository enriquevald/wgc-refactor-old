'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gift_history.vb
'
' DESCRIPTION:   Gifts history
'
' AUTHOR:        Ra�l Cervera
'
' CREATION DATE: 15-DEC-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-DEC-2010  RCI    Initial version.
' 30-DEC-2011  MPO    Add new columns (points).
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 09-APR-2013  DMR    Fixed Bug #685: Added a status for canceled gifts
' 15-MAY-2013  DHA    Add request user, delivery user and gift name filter
' 06-JUN-2013  DHA    Fixed Bug #831: Added columns Quantity and Spent Point. Added Total row
' 16-DEC-2013  RMS    In TITO Mode virtual accounts should be hidden
' 27-MAR-2014  DLL    Fixed Bug #WIGOSTITO-1173. The filter status and type when all selected shows ALL
' 30-APR-2014  JBP    Fixed Bug WIG-866. The filter "Only VIP" return error. 
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 27-JUN-2014  LEM & RCI    When card is recycled, the trackdata is shown empty and must be CardNumber.INVALID_TRACKDATA.
' 15-MAY-2018  GDA    Bug 32678 -- WIGOS-11758 --- Gift history: it is not possible to search for Cancelled status records
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports WSI.Common
Imports System.Data

Public Class frm_gift_history
  Inherits frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_REQUEST_DATE As Integer = 0
  Private Const SQL_COLUMN_DELIVERY_DATE As Integer = 1
  Private Const SQL_COLUMN_EXPIRE_DATE As Integer = 2
  Private Const SQL_COLUMN_NAME As Integer = 3
  Private Const SQL_COLUMN_TYPE As Integer = 4
  Private Const SQL_COLUMN_REQUEST_STATUS As Integer = 5
  Private Const SQL_COLUMN_CARD_ACCOUNT As Integer = 6
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 7
  Private Const SQL_COLUMN_CARD_HOLDER As Integer = 8
  Private Const SQL_COLUMN_CARD_ACCOUNT_TYPE As Integer = 9
  Private Const SQL_COLUMN_POINTS As Integer = 10
  Private Const SQL_COLUMN_REQUEST_USER As Integer = 11
  Private Const SQL_COLUMN_DELIVERY_USER As Integer = 12
  Private Const SQL_COLUMN_NUM_ITEMS As Integer = 13
  Private Const SQL_COLUMN_SPENT_POINTS As Integer = 14

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_REQUEST_DATE As Integer = 1
  Private Const GRID_COLUMN_REQUEST_USER As Integer = 2
  Private Const GRID_COLUMN_NAME As Integer = 3
  Private Const GRID_COLUMN_TYPE As Integer = 4
  Private Const GRID_COLUMN_REQUEST_STATUS As Integer = 5
  Private Const GRID_COLUMN_POINTS As Integer = 6
  Private Const GRID_COLUMN_NUM_ITEMS As Integer = 7
  Private Const GRID_COLUMN_SPENT_POINTS As Integer = 8
  Private Const GRID_COLUMN_DELIVERY_DATE As Integer = 9
  Private Const GRID_COLUMN_DELIVERY_USER As Integer = 10
  Private Const GRID_COLUMN_EXPIRE_DATE As Integer = 11
  Private Const GRID_COLUMN_CARD_ACCOUNT As Integer = 12
  Private Const GRID_COLUMN_CARD_TRACK As Integer = 13
  Private Const GRID_COLUMN_CARD_HOLDER As Integer = 14


  Private Const GRID_COLUMNS As Integer = 15
  Private Const GRID_HEADER_ROWS As Integer = 2

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_request_date_to As String
  Private m_request_date_from As String
  Private m_delivery_date_to As String
  Private m_delivery_date_from As String
  Private m_expire_date_to As String
  Private m_expire_date_from As String
  Private m_filter_type As String
  Private m_filter_status As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_filter_gift_name As String
  Private m_user_request As String
  Private m_user_delivery As String
  Dim total_spent_points As Double

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GIFTS_HISTORY

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(429)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Request Date
    Me.gb_request_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1995)
    Me.dtp_from_rd.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to_rd.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from_rd.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to_rd.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Delivery Date
    Me.gb_delivery_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1996)
    Me.dtp_from_dd.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to_dd.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from_dd.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to_dd.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Expire Date
    Me.gb_expire_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1997)
    Me.dtp_from_ed.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to_ed.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from_ed.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to_ed.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    '   - Type                                                            
    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)                 ' 343 "Tipo"
    Me.chk_type_object.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(344)         ' 344 "Objeto"
    Me.chk_type_nrc.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(357)            ' 357 "Cr�ditos No Redimibles"
    Me.chk_type_redeem_credit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(313)  ' 313 "Cr�ditos Redimibles"
    Me.chk_type_draw_numbers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(391)   ' 391 "Boletos de Sorteo"
    Me.chk_type_services.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(396)       ' 396 "Servicios"

    '   - Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(346)               ' 346 "Estado"
    Me.chk_status_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(433)      ' 433 "Solicitados"
    Me.chk_status_delivered.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(388)    ' 388 "Entregados"
    Me.chk_status_expired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(434)      ' 434 "Caducados"
    Me.chk_status_canceled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9080)    ' 434 "Cancelados"

    ' Gift
    Me.ef_gift_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)             ' 339 "Regalo"
    Me.ef_gift_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Request Users
    Me.gb_user_request.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1998)                 ' 1998 "Usuario Peticion"
    Me.opt_one_user_request.Text = GLB_NLS_GUI_INVOICING.GetString(218)                   ' 218 "Uno"
    Me.opt_all_users_request.Text = GLB_NLS_GUI_INVOICING.GetString(219)                  ' 219 "Todos"
    Me.chk_show_all_request.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)                ' 90  "Marcar todos"

    ' Delivery Users
    Me.gb_user_delivery.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1999)                 ' 1999 "Usuario Entrega"
    Me.opt_one_user_delivery.Text = GLB_NLS_GUI_INVOICING.GetString(218)                   ' 218 "Uno"
    Me.opt_all_users_delivery.Text = GLB_NLS_GUI_INVOICING.GetString(219)                  ' 219 "Todos"
    Me.chk_show_all_delivery.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)                ' 90  "Marcar todos"

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Set combo with Request Users
    Call SetCombo(Me.cmb_user_request, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                    WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    Me.cmb_user_request.Enabled = False
    Me.chk_show_all_request.Enabled = False

    ' Set combo with Delivery Users
    Call SetCombo(Me.cmb_user_delivery, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                    WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    Me.cmb_user_delivery.Enabled = False
    Me.chk_show_all_delivery.Enabled = False

  End Sub ' GUI_InitControls

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Request Dates selection 
    If Me.dtp_from_rd.Checked And Me.dtp_to_rd.Checked Then
      If Me.dtp_from_rd.Value > Me.dtp_to_rd.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to_rd.Focus()

        Return False
      End If
    End If

    ' Delivery Dates selection 
    If Me.dtp_from_dd.Checked And Me.dtp_to_dd.Checked Then
      If Me.dtp_from_dd.Value > Me.dtp_to_dd.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to_dd.Focus()

        Return False
      End If
    End If

    ' Expire Dates selection 
    If Me.dtp_from_ed.Checked And Me.dtp_to_ed.Checked Then
      If Me.dtp_from_ed.Value > Me.dtp_to_ed.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to_ed.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT GIN_REQUESTED " & _
              "       ,GIN_DELIVERED " & _
              "       ,GIN_EXPIRATION " & _
              "       ,GIN_GIFT_NAME " & _
              "       ,GIN_GIFT_TYPE " & _
              "       ,GIN_REQUEST_STATUS " & _
              "       ,GIN_ACCOUNT_ID " & _
              "       ,AC_TRACK_DATA " & _
              "       ,AC_HOLDER_NAME " & _
              "       ,AC_TYPE " & _
              "       ,GIN_POINTS " & _
              "       ,(  SELECT  GU_USERNAME " & _
              "             FROM  GUI_USERS " & _
              "            WHERE  GU_USER_ID = USER_REQUEST_ID " & _
              "       ) AS USER_REQUEST " & _
              "       ,(  SELECT  GU_USERNAME " & _
              "             FROM  GUI_USERS " & _
              "            WHERE  GU_USER_ID = USER_DELIVERY_ID " & _
              "       ) AS USER_DELIVERY " & _
              "       ,GIN_NUM_ITEMS " & _
              "       ,GIN_SPENT_POINTS " & _
              "FROM ( " & _
              "SELECT " & _
              "         GIN_REQUESTED " & _
              "       , GIN_DELIVERED " & _
              "       , GIN_EXPIRATION " & _
              "       , GIN_GIFT_NAME " & _
              "       , GIN_GIFT_TYPE " & _
              "       , GIN_REQUEST_STATUS " & _
              "       , GIN_ACCOUNT_ID " & _
              "       , AC_TRACK_DATA " & _
              "       , AC_HOLDER_NAME " & _
              "       , AC_HOLDER_IS_VIP " & _
              "       , AC_TYPE " & _
              "       , GIN_POINTS " & _
              "       ,(  SELECT  GU_USER_ID " & _
              "             FROM  GUI_USERS " & _
              "                   INNER JOIN CASHIER_SESSIONS     ON CS_USER_ID            = GU_USER_ID " & _
              "                   INNER JOIN ACCOUNT_OPERATIONS   ON AO_CASHIER_SESSION_ID = CS_SESSION_ID " & _
              "            WHERE  GIN_OPER_REQUEST_ID = AO_OPERATION_ID " & _
              "       ) AS USER_REQUEST_ID " & _
              "       ,(  SELECT  CASE " & _
              "	                  WHEN GIN_GIFT_TYPE IN (" & CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT & ", " & CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES & ") THEN " & _
              "	                  ( SELECT  GU_USER_ID " & _
              "                       FROM  GUI_USERS " & _
              "                             INNER JOIN CASHIER_SESSIONS     ON CS_USER_ID            = GU_USER_ID " & _
              "                             INNER JOIN ACCOUNT_OPERATIONS   ON AO_CASHIER_SESSION_ID = CS_SESSION_ID " & _
              "                      WHERE  GIN_OPER_DELIVERY_ID = AO_OPERATION_ID )" & _
              "                   ELSE " & _
              "                   ( SELECT  GU_USER_ID " & _
              "                       FROM  GUI_USERS " & _
              "                             INNER JOIN CASHIER_SESSIONS     ON CS_USER_ID            = GU_USER_ID " & _
              "                             INNER JOIN ACCOUNT_OPERATIONS   ON AO_CASHIER_SESSION_ID = CS_SESSION_ID " & _
              "                      WHERE  GIN_OPER_REQUEST_ID = AO_OPERATION_ID) " & _
              "                   END " & _
              "       ) AS USER_DELIVERY_ID " & _
              "       ,GIN_NUM_ITEMS " & _
              "       ,GIN_SPENT_POINTS " & _
              "       , AC_ACCOUNT_ID " & _
              "  FROM   GIFT_INSTANCES " & _
              "       , ACCOUNTS " & _
              " WHERE   GIN_ACCOUNT_ID = AC_ACCOUNT_ID " & _
              ") AS X "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY GIN_REQUESTED DESC "

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    total_spent_points = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _track_data As String
    Dim _req_status As GIFT_REQUEST_STATUS

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Date Request
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_REQUEST_DATE), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    ' Request User
    Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_USER).Value = IIf(DbRow.Value(SQL_COLUMN_REQUEST_USER) Is DBNull.Value, Nothing, DbRow.Value(SQL_COLUMN_REQUEST_USER))

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    ' Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GetTypeName(DbRow.Value(SQL_COLUMN_TYPE))

    ' Request Status
    _req_status = DbRow.Value(SQL_COLUMN_REQUEST_STATUS)
    Select Case _req_status
      Case WSI.Common.GIFT_REQUEST_STATUS.PENDING
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(435)
      Case WSI.Common.GIFT_REQUEST_STATUS.DELIVERED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(436)
      Case WSI.Common.GIFT_REQUEST_STATUS.EXPIRED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(437)
      Case WSI.Common.GIFT_REQUEST_STATUS.CANCELED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REQUEST_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1891)
    End Select

    ' Points
    Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS), 0)

    ' Quantity
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_ITEMS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_NUM_ITEMS), 0)

    ' Spent Points
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SPENT_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SPENT_POINTS), 0)

    ' Delivery Date
    If Not DbRow.Value(SQL_COLUMN_DELIVERY_DATE) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DELIVERY_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DELIVERY_DATE), _
                                                                      ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                      ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Delivery User
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DELIVERY_USER).Value = IIf(DbRow.Value(SQL_COLUMN_DELIVERY_USER) Is DBNull.Value, Nothing, DbRow.Value(SQL_COLUMN_DELIVERY_USER))

    ' Expire Date
    Select Case _req_status
      Case WSI.Common.GIFT_REQUEST_STATUS.PENDING
        If Not DbRow.Value(GRID_COLUMN_EXPIRE_DATE) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRE_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXPIRE_DATE), _
                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                    ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
      Case WSI.Common.GIFT_REQUEST_STATUS.EXPIRED
        If Not DbRow.Value(GRID_COLUMN_EXPIRE_DATE) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPIRE_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXPIRE_DATE), _
                                                                    ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                    ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
    End Select

    ' Card Account Number
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                     CType(DbRow.Value(SQL_COLUMN_CARD_ACCOUNT_TYPE), AccountType))
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = _track_data
    End If

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_CARD_HOLDER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_HOLDER).Value = DbRow.Value(SQL_COLUMN_CARD_HOLDER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_HOLDER).Value = ""
    End If


    ' Total

    If Not DbRow.IsNull(GRID_COLUMN_SPENT_POINTS) Then
      total_spent_points += GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SPENT_POINTS), 0)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SPENT_POINTS).Value = ""
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE : Performs some actions after all rows have been painted
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Protected Overrides Sub GUI_AfterLastRow()

    Dim idx_row As Integer

    ' "TOTAL POINTS SPENT" label
    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1
    Me.Grid.Cell(idx_row, GRID_COLUMN_REQUEST_DATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340) ' "xTotal" 

    ' Total Spent Points

    Me.Grid.Cell(idx_row, GRID_COLUMN_SPENT_POINTS).Value = GUI_FormatNumber(total_spent_points, 0)

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from_rd
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    Dim str_holder_name As String
    Dim str_card_id As String
    Dim str_account_id As String

    str_holder_name = Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_HOLDER).Value
    str_card_id = Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value
    str_account_id = Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ACCOUNT).Value

    If ColIndex = GRID_COLUMN_CARD_HOLDER Or ColIndex = GRID_COLUMN_CARD_ACCOUNT Then
      ToolTipTxt = GLB_NLS_GUI_STATISTICS.GetString(397, str_account_id, str_card_id, str_holder_name)
    End If

  End Sub ' GUI_SetToolTipText

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _filter_type As String
    Dim _filter_status As String

    _filter_type = ""
    _filter_status = ""

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(430) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_request_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(430) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_request_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1998), m_user_request)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1999), m_user_delivery)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(431) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_delivery_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(431) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_delivery_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(339), m_filter_gift_name)

    _filter_type = m_filter_type
    If String.IsNullOrEmpty(m_filter_type) Then
      _filter_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    End If
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(343), _filter_type)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(432) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), _
                        m_expire_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(432) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), _
                        m_expire_date_to)

    _filter_type = m_filter_status
    If String.IsNullOrEmpty(m_filter_status) Then
      _filter_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    End If
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(346), _filter_type)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    PrintData.FilterHeaderWidth(1) = 1500
    PrintData.FilterValueWidth(1) = 2000
    PrintData.FilterHeaderWidth(3) = 1500

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_request_date_to = ""
    m_request_date_from = ""
    m_delivery_date_to = ""
    m_delivery_date_from = ""
    m_expire_date_to = ""
    m_expire_date_from = ""
    m_filter_gift_name = ""
    m_user_request = ""
    m_user_delivery = ""

    ' Request Date 
    If Me.dtp_from_rd.Checked Then
      m_request_date_from = GUI_FormatDate(dtp_from_rd.Value, _
                                           ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to_rd.Checked Then
      m_request_date_to = GUI_FormatDate(dtp_to_rd.Value, _
                                         ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Delivery Date 
    If Me.dtp_from_dd.Checked Then
      m_delivery_date_from = GUI_FormatDate(dtp_from_dd.Value, _
                                            ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to_dd.Checked Then
      m_delivery_date_to = GUI_FormatDate(dtp_to_dd.Value, _
                                          ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Expire Date 
    If Me.dtp_from_ed.Checked Then
      m_expire_date_from = GUI_FormatDate(dtp_from_ed.Value, _
                                          ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to_ed.Checked Then
      m_expire_date_to = GUI_FormatDate(dtp_to_ed.Value, _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Type
    m_filter_type = SetGiftTypeFilter(False)

    ' Request Status
    m_filter_status = SetRequestStatusFilter(False)

    ' Account
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Gift Name
    If Me.ef_gift_name.Value <> "" Then
      m_filter_gift_name = Me.ef_gift_name.Value
    End If

    ' Request User
    If Me.opt_all_users_request.Checked Then
      m_user_request = Me.opt_all_users_request.Text
    End If
    If Me.opt_one_user_request.Checked Then
      m_user_request = Me.cmb_user_request.TextValue
    End If

    ' Delivery User
    If Me.opt_all_users_delivery.Checked Then
      m_user_delivery = Me.opt_all_users_delivery.Text
    End If
    If Me.opt_one_user_delivery.Checked Then
      m_user_delivery = Me.cmb_user_delivery.TextValue
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Request - Request Date
      '  Request
      .Column(GRID_COLUMN_REQUEST_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(430)
      '  Request Date
      .Column(GRID_COLUMN_REQUEST_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1050)
      .Column(GRID_COLUMN_REQUEST_DATE).Width = 2150
      .Column(GRID_COLUMN_REQUEST_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Request - Request User
      '  Request
      .Column(GRID_COLUMN_REQUEST_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(430)
      '  Request User
      .Column(GRID_COLUMN_REQUEST_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
      .Column(GRID_COLUMN_REQUEST_USER).Width = 1500
      .Column(GRID_COLUMN_REQUEST_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Gift - Name
      '  Gift
      .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      '  Name
      .Column(GRID_COLUMN_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(353)
      .Column(GRID_COLUMN_NAME).Width = 3100
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Gift - Type
      '  Gift
      .Column(GRID_COLUMN_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      '  Type
      .Column(GRID_COLUMN_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)
      .Column(GRID_COLUMN_TYPE).Width = 2150
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Gift - Status
      '  Gift
      .Column(GRID_COLUMN_REQUEST_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(339)
      '  Status
      .Column(GRID_COLUMN_REQUEST_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(346)
      .Column(GRID_COLUMN_REQUEST_STATUS).Width = 1500
      .Column(GRID_COLUMN_REQUEST_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Gift - Points Details
      '   - Points
      .Column(GRID_COLUMN_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2041)    ' Detalle Puntos Detail Points
      .Column(GRID_COLUMN_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)    ' 350 "Puntos"
      .Column(GRID_COLUMN_POINTS).Width = 1290
      .Column(GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Gift - Points Details
      '   - Quantity
      .Column(GRID_COLUMN_NUM_ITEMS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2041)    ' Detalle Puntos Detail Points
      .Column(GRID_COLUMN_NUM_ITEMS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)    ' 2042 "Cantidad"
      .Column(GRID_COLUMN_NUM_ITEMS).Width = 1290
      .Column(GRID_COLUMN_NUM_ITEMS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Gift - Points Details
      '   - Spent Points
      .Column(GRID_COLUMN_SPENT_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2041)    ' Detalle Puntos Detail Points
      .Column(GRID_COLUMN_SPENT_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2043)    ' 2043 "Puntos Gastados"
      .Column(GRID_COLUMN_SPENT_POINTS).Width = 1500
      .Column(GRID_COLUMN_SPENT_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' Delivery - Delivery Date
      '  Delivery
      .Column(GRID_COLUMN_DELIVERY_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(431)
      '  Delivery Date
      .Column(GRID_COLUMN_DELIVERY_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1050)
      .Column(GRID_COLUMN_DELIVERY_DATE).Width = 2150
      .Column(GRID_COLUMN_DELIVERY_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Delivery - Delivery User
      '  Delivery
      .Column(GRID_COLUMN_DELIVERY_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(431)
      '  Delivery User
      .Column(GRID_COLUMN_DELIVERY_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
      .Column(GRID_COLUMN_DELIVERY_USER).Width = 1500
      .Column(GRID_COLUMN_DELIVERY_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Expire - Expire Date
      '  Expire
      .Column(GRID_COLUMN_EXPIRE_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2000)
      '  Expire Date
      .Column(GRID_COLUMN_EXPIRE_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1050)
      .Column(GRID_COLUMN_EXPIRE_DATE).Width = 2150
      .Column(GRID_COLUMN_EXPIRE_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account - Number
      '  Account
      .Column(GRID_COLUMN_CARD_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      '  Number
      .Column(GRID_COLUMN_CARD_ACCOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_ACCOUNT).Width = 1150
      .Column(GRID_COLUMN_CARD_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account - Card
      '  Account
      .Column(GRID_COLUMN_CARD_TRACK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      '  Card
      .Column(GRID_COLUMN_CARD_TRACK).Header(1).Text = ""
      .Column(GRID_COLUMN_CARD_TRACK).Width = 0

      ' Account - Holder Name
      '  Account
      .Column(GRID_COLUMN_CARD_HOLDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      '  Holder Name
      .Column(GRID_COLUMN_CARD_HOLDER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_CARD_HOLDER).Width = 3000
      .Column(GRID_COLUMN_CARD_HOLDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    ' Request Date
    Me.dtp_from_rd.Value = _initial_time
    Me.dtp_from_rd.Checked = True
    Me.dtp_to_rd.Value = _initial_time.AddDays(1)
    Me.dtp_to_rd.Checked = False

    ' Delivery Date
    Me.dtp_from_dd.Value = _initial_time
    Me.dtp_from_dd.Checked = False
    Me.dtp_to_dd.Value = _initial_time.AddDays(1)
    Me.dtp_to_dd.Checked = False

    ' Expire Date
    Me.dtp_from_ed.Value = _initial_time
    Me.dtp_from_ed.Checked = False
    Me.dtp_to_ed.Value = _initial_time.AddDays(1)
    Me.dtp_to_ed.Checked = False

    ' Type
    Me.chk_type_object.Checked = True
    Me.chk_type_nrc.Checked = True
    Me.chk_type_redeem_credit.Checked = True
    Me.chk_type_draw_numbers.Checked = True
    Me.chk_type_services.Checked = True

    ' Request Status
    Me.chk_status_pending.Checked = True
    Me.chk_status_delivered.Checked = True
    Me.chk_status_expired.Checked = True
    Me.chk_status_canceled.Checked = True

    ' Account
    Me.uc_account_sel1.Clear()

    ' Gift
    Me.ef_gift_name.Value = ""

    ' Request Users
    Me.opt_all_users_request.Checked = True
    Me.cmb_user_request.Enabled = False
    Me.chk_show_all_request.Checked = False
    Me.chk_show_all_request.Enabled = False

    ' Delivery Users
    Me.opt_all_users_delivery.Checked = True
    Me.cmb_user_delivery.Enabled = False
    Me.chk_show_all_delivery.Checked = False
    Me.chk_show_all_delivery.Enabled = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_where_account As String
    Dim _str_aux As String

    ' Filter Request Dates
    If Me.dtp_from_rd.Checked Then
      str_where = str_where & " AND (GIN_REQUESTED >= " & GUI_FormatDateDB(dtp_from_rd.Value) & ") "
    End If
    If Me.dtp_to_rd.Checked Then
      str_where = str_where & " AND (GIN_REQUESTED < " & GUI_FormatDateDB(dtp_to_rd.Value) & ") "
    End If

    ' Filter Delivery Dates
    If Me.dtp_from_dd.Checked Then
      str_where = str_where & " AND (GIN_DELIVERED IS NULL OR GIN_DELIVERED >= " & GUI_FormatDateDB(dtp_from_dd.Value) & ") "
    End If
    If Me.dtp_to_dd.Checked Then
      str_where = str_where & " AND (GIN_DELIVERED IS NULL OR GIN_DELIVERED < " & GUI_FormatDateDB(dtp_to_dd.Value) & ") "
    End If

    ' Filter Expire Dates
    If Me.dtp_from_ed.Checked Then
      str_where = str_where & " AND (GIN_EXPIRATION >= " & GUI_FormatDateDB(dtp_from_ed.Value) & ") "
    End If
    If Me.dtp_to_ed.Checked Then
      str_where = str_where & " AND (GIN_EXPIRATION < " & GUI_FormatDateDB(dtp_to_ed.Value) & ") "
    End If

    ' Filter Type
    str_where = str_where & SetGiftTypeFilter(True)

    ' Filter Request Status
    str_where = str_where & SetRequestStatusFilter(True)

    ' Filter Account
    str_where_account = Me.uc_account_sel1.GetFilterSQL()
    If str_where_account <> "" Then
      str_where = str_where & " AND " & str_where_account
    End If

    ' Filter Gift
    If Me.ef_gift_name.Value <> "" Then
      _str_aux = Me.ef_gift_name.Value.Replace("'", "''")
      str_where = str_where + " AND UPPER(GIN_GIFT_NAME) LIKE '%" + _str_aux + "%'"
    End If

    ' Filter Request User
    If Me.opt_one_user_request.Checked = True Then
      str_where = str_where & " AND USER_REQUEST_ID = " & Me.cmb_user_request.Value & " "
    End If

    ' Filter Delivery User
    If Me.opt_one_user_delivery.Checked = True Then
      str_where = str_where & " AND USER_DELIVERY_ID = " & Me.cmb_user_delivery.Value & " "
    End If

    If String.IsNullOrEmpty(str_where) Then
      Return String.Empty
    ElseIf str_where.Trim().StartsWith("AND") Then
      Return " WHERE " & str_where.Trim().Remove(0, "AND".Length)
    End If
    Return " WHERE " & str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Set filter related to gift type
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function SetGiftTypeFilter(ByVal SqlFilter As Boolean) As String

    Dim idx_item As Int32
    Dim idx_count As Int32
    Dim filter_string(0 To 4) As String
    Dim result_string As String

    idx_count = 0

    If Me.chk_type_object.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_GIFT_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(344))      ' 344 "Objeto"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_nrc.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_GIFT_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(357))      ' 357 "Cr�ditos No Redimibles"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_redeem_credit.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_GIFT_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(313))      ' 313 "Cr�ditos Redimibles"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_draw_numbers.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_GIFT_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(391))      ' 391 "Boletos de Sorteo"
      idx_count = idx_count + 1
    End If

    If Me.chk_type_services.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_GIFT_TYPE = " + CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES.ToString("D") _
                                              , GLB_NLS_GUI_PLAYER_TRACKING.GetString(396))      ' 396 "Servicios"
      idx_count = idx_count + 1
    End If

    If idx_count < 1 Or idx_count > 4 Then
      Return ""
    End If

    If SqlFilter Then
      result_string = " AND (" + filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + " OR " + filter_string(idx_item)
      Next

      result_string = result_string + ")"
    Else
      result_string = filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + ", " + filter_string(idx_item)
      Next
    End If

    Return result_string

  End Function ' SetGiftTypeFilter

  ' PURPOSE : Set filter related to gift request status
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Function SetRequestStatusFilter(ByVal SqlFilter As Boolean) As String

    Dim idx_item As Int32
    Dim idx_count As Int32
    Dim filter_string(0 To 3) As String
    Dim result_string As String

    idx_count = 0

    If Me.chk_status_pending.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_REQUEST_STATUS = " & WSI.Common.GIFT_REQUEST_STATUS.PENDING _
                                               , GLB_NLS_GUI_PLAYER_TRACKING.GetString(433))
      idx_count = idx_count + 1
    End If

    If Me.chk_status_delivered.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_REQUEST_STATUS = " & WSI.Common.GIFT_REQUEST_STATUS.DELIVERED _
                                               , GLB_NLS_GUI_PLAYER_TRACKING.GetString(388))
      idx_count = idx_count + 1
    End If

    If Me.chk_status_expired.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_REQUEST_STATUS = " & WSI.Common.GIFT_REQUEST_STATUS.EXPIRED _
                                               , GLB_NLS_GUI_PLAYER_TRACKING.GetString(434))
      idx_count = idx_count + 1
    End If

    If Me.chk_status_canceled.Checked Then
      filter_string(idx_count) = IIf(SqlFilter, "GIN_REQUEST_STATUS = " & WSI.Common.GIFT_REQUEST_STATUS.CANCELED _
                                               , GLB_NLS_GUI_PLAYER_TRACKING.GetString(9080))
      idx_count = idx_count + 1
    End If

    If idx_count < 1 Or idx_count > 3 Then
      Return ""
    End If

    If SqlFilter Then
      result_string = " AND (" + filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + " OR " + filter_string(idx_item)
      Next

      result_string = result_string + ")"
    Else
      result_string = filter_string(0)

      For idx_item = 1 To idx_count - 1
        result_string = result_string + ", " + filter_string(idx_item)
      Next
    End If

    Return result_string
  End Function ' SetRequestStatusFilter

  ' PURPOSE : Return the type name string
  '
  '  PARAMS :
  '     - INPUT : Type enum
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - String with the type name

  Private Function GetTypeName(ByVal Type As CLASS_GIFT.ENUM_GIFT_TYPE) As String
    Dim type_name As String

    type_name = "UNKNOWN"

    Select Case Type
      Case CLASS_GIFT.ENUM_GIFT_TYPE.OBJECT_GIFT
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(344) ' 344 "Objeto"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.NOT_REDEEMABLE_CREDIT
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(357) ' 357 "Cr�ditos No Redimibles"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.REDEEMABLE_CREDIT
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(313) ' 313 "Cr�ditos Redimibles"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.DRAW_NUMBERS
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(391) ' 391 "Boletos de Sorteo"
      Case CLASS_GIFT.ENUM_GIFT_TYPE.SERVICES
        type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(396) ' 396 "Servicios"
    End Select

    Return type_name
  End Function ' GetTypeName

#End Region ' Private Functions

#Region "Events"

  Private Sub opt_one_user_request_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user_request.CheckedChanged
    Me.cmb_user_request.Enabled = opt_one_user_request.Checked
    Me.chk_show_all_request.Enabled = opt_one_user_request.Checked
  End Sub ' opt_one_user_request_CheckedChanged

  Private Sub opt_all_users_request_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users_request.CheckedChanged
    Me.cmb_user_request.Enabled = opt_one_user_request.Checked
    Me.chk_show_all_request.Enabled = opt_one_user_request.Checked
  End Sub ' opt_all_users_request_CheckedChanged

  Private Sub chk_show_all_request_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all_request.CheckedChanged
    If chk_show_all_request.Checked Then
      Call SetCombo(Me.cmb_user_request, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user_request, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub ' chk_show_all_request_CheckedChanged

  Private Sub opt_one_user_delivery_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user_delivery.CheckedChanged
    Me.cmb_user_delivery.Enabled = opt_one_user_delivery.Checked
    Me.chk_show_all_delivery.Enabled = opt_one_user_delivery.Checked
  End Sub ' opt_one_user_delivery_CheckedChanged

  Private Sub opt_all_users_delivery_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users_delivery.CheckedChanged
    Me.cmb_user_delivery.Enabled = opt_one_user_delivery.Checked
    Me.chk_show_all_delivery.Enabled = opt_one_user_delivery.Checked
  End Sub ' opt_all_users_delivery_CheckedChanged

  Private Sub chk_show_all_delivery_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all_delivery.CheckedChanged
    If chk_show_all_delivery.Checked Then
      Call SetCombo(Me.cmb_user_delivery, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user_delivery, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub ' chk_show_all_delivery_CheckedChanged

#End Region  ' Events

End Class ' frm_gift_history
