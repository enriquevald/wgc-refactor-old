﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gamegateway_terminals_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_gamegateway_terminals_edit))
    Me.dg_providers = New GUI_Controls.uc_terminals_group_filter()
    Me.chk_gp_gamegateway_enabled = New System.Windows.Forms.CheckBox()
    Me.panelEnableGameGateway = New System.Windows.Forms.Panel()
    Me.panelGameGatewayTerminals = New System.Windows.Forms.Panel()
    Me.panel_data.SuspendLayout()
    Me.panelEnableGameGateway.SuspendLayout()
    Me.panelGameGatewayTerminals.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.panelGameGatewayTerminals)
    Me.panel_data.Controls.Add(Me.panelEnableGameGateway)
    Me.panel_data.Size = New System.Drawing.Size(470, 461)
    '
    'dg_providers
    '
    Me.dg_providers.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_providers.ForbiddenGroups = CType(resources.GetObject("dg_providers.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.dg_providers.HeightOnExpanded = 300
    Me.dg_providers.Location = New System.Drawing.Point(0, 0)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.SelectedIndex = 0
    Me.dg_providers.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutCollapseButton
    Me.dg_providers.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.dg_providers.ShowGroupBoxLine = True
    Me.dg_providers.ShowGroups = Nothing
    Me.dg_providers.ShowNodeAll = True
    Me.dg_providers.Size = New System.Drawing.Size(470, 461)
    Me.dg_providers.TabIndex = 0
    Me.dg_providers.ThreeStateCheckMode = True
    '
    'chk_gp_gamegateway_enabled
    '
    Me.chk_gp_gamegateway_enabled.AutoSize = True
    Me.chk_gp_gamegateway_enabled.Location = New System.Drawing.Point(14, 13)
    Me.chk_gp_gamegateway_enabled.Name = "chk_gp_gamegateway_enabled"
    Me.chk_gp_gamegateway_enabled.Size = New System.Drawing.Size(87, 17)
    Me.chk_gp_gamegateway_enabled.TabIndex = 1
    Me.chk_gp_gamegateway_enabled.Text = "xcheckbox"
    Me.chk_gp_gamegateway_enabled.UseVisualStyleBackColor = True
    '
    'panelEnableGameGateway
    '
    Me.panelEnableGameGateway.Controls.Add(Me.chk_gp_gamegateway_enabled)
    Me.panelEnableGameGateway.Location = New System.Drawing.Point(0, 0)
    Me.panelEnableGameGateway.Name = "panelEnableGameGateway"
    Me.panelEnableGameGateway.Size = New System.Drawing.Size(470, 46)
    Me.panelEnableGameGateway.TabIndex = 2
    '
    'panelGameGatewayTerminals
    '
    Me.panelGameGatewayTerminals.Controls.Add(Me.dg_providers)
    Me.panelGameGatewayTerminals.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panelGameGatewayTerminals.Location = New System.Drawing.Point(0, 0)
    Me.panelGameGatewayTerminals.Name = "panelGameGatewayTerminals"
    Me.panelGameGatewayTerminals.Size = New System.Drawing.Size(470, 461)
    Me.panelGameGatewayTerminals.TabIndex = 3
    '
    'frm_gamegateway_terminals_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(572, 470)
    Me.Name = "frm_gamegateway_terminals_edit"
    Me.Text = "frm_gamegateway_terminals_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panelEnableGameGateway.ResumeLayout(False)
    Me.panelEnableGameGateway.PerformLayout()
    Me.panelGameGatewayTerminals.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_gp_gamegateway_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents dg_providers As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents panelGameGatewayTerminals As System.Windows.Forms.Panel
  Friend WithEvents panelEnableGameGateway As System.Windows.Forms.Panel
End Class
