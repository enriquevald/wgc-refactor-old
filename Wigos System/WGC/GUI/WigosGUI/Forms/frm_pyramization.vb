﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_pyramidal_values.vb
' DESCRIPTION:   Edit values playcash pyramidal distribution
' AUTHOR:        David Perelló
' CREATION DATE: 26-OCT-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-OCT-2017  DPC     Initial version
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.IO
Imports GUI_CommonMisc
Imports GUI_Controls

Public Class frm_pyramization
  Inherits frm_base_edit

#Region " Constants "

  Private Const m_const_DataSetName = "piramization"
  Private Const m_const_TableName = "level"
  Private Const m_const_column_probability = "probability"
  Private Const m_const_column_fix = "fix"
  Private Const m_const_column_recharge = "recharge"

  Private Const m_const_default_cell_value = "0"

#End Region ' Constants

#Region " Elements "

  Private m_xml As String
  Private m_result_xml As String
  Private m_dt_pyramid As DataTable
  Private m_parent_uc As uc_pyramidal

#End Region ' Elements

#Region " Properties "

  Public WriteOnly Property Xml() As String
    Set(ByVal value As String)
      m_xml = value
    End Set
  End Property

  Public ReadOnly Property ResultToDatatable() As DataTable
    Get
      Return m_dt_pyramid
    End Get
  End Property

  Public ReadOnly Property ResultToXml() As String
    Get
      If Not IsNothing(m_result_xml) Then
        Call CleanXml()
      End If

      Return m_result_xml
    End Get
  End Property

#End Region ' Properties

#Region " Constructor "

  Public Sub New(Parent As uc_pyramidal)
    Me.FormId = ENUM_FORM.FORM_PIRAMIZATION
    Me.m_parent_uc = Parent

    Call MyBase.GUI_SetFormId()
    ' Add any initialization after the InitializeComponent() call.
    Call Me.GUI_Init()
    ' This call is required by the designer.
    Call Me.InitializeComponent()
  End Sub 'New

#End Region ' Constructor

#Region " Overrides "

  ''' <summary>
  ''' GUI_Permissions
  ''' </summary>
  ''' <param name="AndPerm"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ''' <summary>
  ''' GUI_FirstActivation
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FirstActivation()
  End Sub ' GUI_FirstActivation

#End Region ' Overrides

#Region "Public methods"

  Public Sub SetFormInfo()
    Call SetGrid()
    Call CalculateSum()
  End Sub

#End Region

#Region " Private methods "

  ''' <summary>
  ''' Inizialitation controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8751)

    Me.lbl_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8750)
    Me.bt_accept.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4576)
    Me.bt_cancel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2903)

    Me.HidePanelButtons = True
    Me.Size = New Size(400, 372)
  End Sub ' InitializeControls

  ''' <summary>
  ''' Set grid and fill
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetGrid()
    Dim _reader As StringReader
    Dim _ds As DataSet

    _ds = New DataSet

    If Me.m_dt_pyramid Is Nothing Then

      _reader = New StringReader(m_xml)

      _ds.ReadXml(_reader)

      If _ds.Tables.Count > 0 Then
        Me.m_dt_pyramid = _ds.Tables(0)
      End If

    End If

    If m_dt_pyramid.Rows.Count > 0 Then

      Me.dgv_pyramid.DataSource = m_dt_pyramid
      Call SetStyleGrid()

    End If

  End Sub ' setGrid

  ''' <summary>
  ''' Set style grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetStyleGrid()

    Me.dgv_pyramid.Columns(0).Width = 100
    Me.dgv_pyramid.Columns(1).Width = 100
    Me.dgv_pyramid.Columns(2).Width = 100

    Me.dgv_pyramid.Columns(0).HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8747)
    Me.dgv_pyramid.Columns(1).HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8748)
    Me.dgv_pyramid.Columns(2).HeaderText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8749)

    Me.dgv_pyramid.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

    Me.dgv_pyramid.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    Me.dgv_pyramid.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    Me.dgv_pyramid.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

    For Each _column As DataGridViewColumn In Me.dgv_pyramid.Columns
      _column.SortMode = DataGridViewColumnSortMode.NotSortable
    Next

  End Sub ' SetStyleGrid

  ''' <summary>
  ''' Calculate the sum of probabilities
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CalculateSum()

    Dim _sum As Int32

    _sum = 0

    For Each _row As DataGridViewRow In Me.dgv_pyramid.Rows
      _sum += Int32.Parse(_row.Cells(0).Value)
    Next

    Me.lbl_sum.Text = String.Format("{0}%", _sum)

    If _sum <> 100 Then
      Me.bt_accept.Enabled = False
    Else
      Me.bt_accept.Enabled = True
    End If

  End Sub ' CalculateSum

  ''' <summary>
  ''' Datatable to xml to result
  ''' </summary>
  ''' <remarks></remarks>
  Private Function DatatbleToXml() As String

    Dim _sw As StringWriter = New StringWriter()
    Dim _ds As DataSet

    _ds = New DataSet
    _sw = New StringWriter()

    _ds = m_dt_pyramid.DataSet

    _ds.WriteXml(_sw)

    Return _sw.ToString()

  End Function ' DatatbleToXml

  ''' <summary>
  ''' To check and change if needed if the entered value is numeric
  ''' </summary>
  ''' <param name="Cell"></param>
  ''' <remarks></remarks>
  Private Sub CheckEnteredValue(ByRef Cell As DataGridViewCell)
    If Not Int32.TryParse(Cell.Value, 0) Then
      Cell.Value = m_const_default_cell_value
    End If
  End Sub ' CheckEnteredValue

  ''' <summary>
  ''' To clean the xml string
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CleanXml()
    Dim _str_temp As String

    _str_temp = Me.m_result_xml.Trim()
    _str_temp = _str_temp.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty)

    Do Until InStr(_str_temp, " ") = 0
      _str_temp = _str_temp.Replace(" ", "")
    Loop

    Me.m_result_xml = _str_temp
  End Sub ' CleanXml

#End Region ' Private methods

#Region " Events "

  ''' <summary>
  ''' Load page event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub frm_pyramidal_values_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    Call InitializeControls()

    ' Add SelectionChanged handler
    AddHandler Me.dgv_pyramid.CellEndEdit, AddressOf dgv_pyramid_CellEndEdit

  End Sub ' frm_pyramidal_values_Load

  ''' <summary>
  ''' Button accept event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_accept_Click(sender As Object, e As EventArgs) Handles bt_accept.Click
    Me.m_result_xml = DatatbleToXml()
    Me.Close()
  End Sub ' bt_accept_Click

  ''' <summary>
  ''' Button cancel event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_cancel_Click(sender As Object, e As EventArgs) Handles bt_cancel.Click
    Me.m_dt_pyramid = Nothing
    Me.Close()
  End Sub ' bt_cancel_Click

  ''' <summary>
  ''' Button add event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_add_Click(sender As Object, e As EventArgs) Handles bt_add.Click
    Dim _row As DataRow

    _row = m_dt_pyramid.NewRow

    _row(m_const_column_probability) = 0
    _row(m_const_column_fix) = 0
    _row(m_const_column_recharge) = 0

    Me.m_dt_pyramid.Rows.Add(_row)
  End Sub ' bt_add_Click

  ''' <summary>
  ''' Button remove event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub bt_remove_Click(sender As Object, e As EventArgs) Handles bt_remove.Click
    Dim _row As DataGridViewRow

    _row = dgv_pyramid.Rows(dgv_pyramid.CurrentCell.RowIndex)

    If Not ((_row.Cells(m_const_column_probability).Value = String.Empty AndAlso _row.Cells(m_const_column_fix).Value = String.Empty AndAlso _row.Cells(m_const_column_recharge).Value = String.Empty) OrElse _
           (_row.Cells(m_const_column_probability).Value = 0 AndAlso _row.Cells(m_const_column_fix).Value = 0 AndAlso _row.Cells(m_const_column_recharge).Value = 0)) Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8752), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return
      End If

    End If

    Me.dgv_pyramid.Rows.Remove(_row)
    Me.dgv_pyramid.CurrentCell.Selected = True

    Call CalculateSum()
  End Sub ' bt_remove_Click

  ''' <summary>
  ''' To close the form
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub frm_pyramization_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
    If Not IsNothing(Me.ResultToXml) Then
      Me.m_parent_uc.Xml = Me.ResultToXml
    End If    
  End Sub ' frm_pyramization_FormClosing

  ''' <summary>
  ''' To check if the cell value is ok
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub dgv_pyramid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs)
    If Not IsNothing(Me.dgv_pyramid.CurrentCell) Then
      Call CheckEnteredValue(Me.dgv_pyramid.CurrentCell)
    End If

    If Me.dgv_pyramid.CurrentCell.ColumnIndex = 0 Then
      Call CalculateSum()
    End If
  End Sub ' dgv_pyramid_CellEndEdit

#End Region ' Events

End Class