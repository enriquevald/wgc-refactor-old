<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_accounts_import
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_output = New System.Windows.Forms.GroupBox
    Me.pgb_import_progress = New System.Windows.Forms.ProgressBar
    Me.tb_output = New System.Windows.Forms.TextBox
    Me.ef_file_path = New GUI_Controls.uc_entry_field
    Me.btn_open_file_dialog = New System.Windows.Forms.Button
    Me.chk_update_balance = New System.Windows.Forms.CheckBox
    Me.chk_reset_balances = New System.Windows.Forms.CheckBox
    Me.gb_balacence = New System.Windows.Forms.GroupBox
    Me.chk_update_points = New System.Windows.Forms.CheckBox
    Me.chk_reset_points = New System.Windows.Forms.CheckBox
    Me.gb_points = New System.Windows.Forms.GroupBox
    Me.rb_account_id = New System.Windows.Forms.RadioButton
    Me.rb_external_reference = New System.Windows.Forms.RadioButton
    Me.gb_reference = New System.Windows.Forms.GroupBox
    Me.chk_add_accounts = New System.Windows.Forms.CheckBox
    Me.chk_update_clients = New System.Windows.Forms.CheckBox
    Me.gb_new_accounts = New System.Windows.Forms.GroupBox
    Me.chk_update_level = New System.Windows.Forms.CheckBox
    Me.gb_level = New System.Windows.Forms.GroupBox
    Me.panel_data.SuspendLayout()
    Me.gb_output.SuspendLayout()
    Me.gb_balacence.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.gb_reference.SuspendLayout()
    Me.gb_new_accounts.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_level)
    Me.panel_data.Controls.Add(Me.gb_output)
    Me.panel_data.Controls.Add(Me.gb_new_accounts)
    Me.panel_data.Controls.Add(Me.gb_reference)
    Me.panel_data.Controls.Add(Me.ef_file_path)
    Me.panel_data.Controls.Add(Me.gb_points)
    Me.panel_data.Controls.Add(Me.btn_open_file_dialog)
    Me.panel_data.Controls.Add(Me.gb_balacence)
    Me.panel_data.Size = New System.Drawing.Size(668, 521)
    '
    'gb_output
    '
    Me.gb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_output.Controls.Add(Me.pgb_import_progress)
    Me.gb_output.Controls.Add(Me.tb_output)
    Me.gb_output.Location = New System.Drawing.Point(3, 262)
    Me.gb_output.Name = "gb_output"
    Me.gb_output.Size = New System.Drawing.Size(654, 250)
    Me.gb_output.TabIndex = 7
    Me.gb_output.TabStop = False
    Me.gb_output.Text = "xOutput"
    '
    'pgb_import_progress
    '
    Me.pgb_import_progress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.pgb_import_progress.Location = New System.Drawing.Point(6, 220)
    Me.pgb_import_progress.Name = "pgb_import_progress"
    Me.pgb_import_progress.Size = New System.Drawing.Size(642, 23)
    Me.pgb_import_progress.TabIndex = 1
    '
    'tb_output
    '
    Me.tb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tb_output.Location = New System.Drawing.Point(6, 21)
    Me.tb_output.Multiline = True
    Me.tb_output.Name = "tb_output"
    Me.tb_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_output.Size = New System.Drawing.Size(642, 199)
    Me.tb_output.TabIndex = 0
    '
    'ef_file_path
    '
    Me.ef_file_path.DoubleValue = 0
    Me.ef_file_path.IntegerValue = 0
    Me.ef_file_path.IsReadOnly = False
    Me.ef_file_path.Location = New System.Drawing.Point(9, 26)
    Me.ef_file_path.Name = "ef_file_path"
    Me.ef_file_path.Size = New System.Drawing.Size(601, 24)
    Me.ef_file_path.SufixText = "Sufix Text"
    Me.ef_file_path.SufixTextVisible = True
    Me.ef_file_path.TabIndex = 0
    Me.ef_file_path.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_file_path.TextValue = ""
    Me.ef_file_path.TextWidth = 120
    Me.ef_file_path.Value = ""
    '
    'btn_open_file_dialog
    '
    Me.btn_open_file_dialog.Location = New System.Drawing.Point(616, 26)
    Me.btn_open_file_dialog.Name = "btn_open_file_dialog"
    Me.btn_open_file_dialog.Size = New System.Drawing.Size(27, 24)
    Me.btn_open_file_dialog.TabIndex = 1
    Me.btn_open_file_dialog.Text = "..."
    Me.btn_open_file_dialog.UseVisualStyleBackColor = True
    '
    'chk_update_balance
    '
    Me.chk_update_balance.AutoSize = True
    Me.chk_update_balance.Location = New System.Drawing.Point(24, 49)
    Me.chk_update_balance.Name = "chk_update_balance"
    Me.chk_update_balance.Size = New System.Drawing.Size(125, 17)
    Me.chk_update_balance.TabIndex = 1
    Me.chk_update_balance.Text = "xA�adir Balances"
    Me.chk_update_balance.UseVisualStyleBackColor = True
    '
    'chk_reset_balances
    '
    Me.chk_reset_balances.AutoSize = True
    Me.chk_reset_balances.Location = New System.Drawing.Point(24, 26)
    Me.chk_reset_balances.Name = "chk_reset_balances"
    Me.chk_reset_balances.Size = New System.Drawing.Size(120, 17)
    Me.chk_reset_balances.TabIndex = 0
    Me.chk_reset_balances.Text = "xResetear Antes"
    Me.chk_reset_balances.UseVisualStyleBackColor = True
    '
    'gb_balacence
    '
    Me.gb_balacence.Controls.Add(Me.chk_reset_balances)
    Me.gb_balacence.Controls.Add(Me.chk_update_balance)
    Me.gb_balacence.Location = New System.Drawing.Point(9, 165)
    Me.gb_balacence.Name = "gb_balacence"
    Me.gb_balacence.Size = New System.Drawing.Size(212, 89)
    Me.gb_balacence.TabIndex = 5
    Me.gb_balacence.TabStop = False
    Me.gb_balacence.Text = "xBalances"
    '
    'chk_update_points
    '
    Me.chk_update_points.AutoSize = True
    Me.chk_update_points.Location = New System.Drawing.Point(24, 49)
    Me.chk_update_points.Name = "chk_update_points"
    Me.chk_update_points.Size = New System.Drawing.Size(112, 17)
    Me.chk_update_points.TabIndex = 1
    Me.chk_update_points.Text = "xA�adir Puntos"
    Me.chk_update_points.UseVisualStyleBackColor = True
    '
    'chk_reset_points
    '
    Me.chk_reset_points.AutoSize = True
    Me.chk_reset_points.Location = New System.Drawing.Point(24, 26)
    Me.chk_reset_points.Name = "chk_reset_points"
    Me.chk_reset_points.Size = New System.Drawing.Size(120, 17)
    Me.chk_reset_points.TabIndex = 0
    Me.chk_reset_points.Text = "xResetear Antes"
    Me.chk_reset_points.UseVisualStyleBackColor = True
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.chk_reset_points)
    Me.gb_points.Controls.Add(Me.chk_update_points)
    Me.gb_points.Location = New System.Drawing.Point(227, 165)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(212, 89)
    Me.gb_points.TabIndex = 6
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPuntos"
    '
    'rb_account_id
    '
    Me.rb_account_id.AutoSize = True
    Me.rb_account_id.Location = New System.Drawing.Point(24, 46)
    Me.rb_account_id.Name = "rb_account_id"
    Me.rb_account_id.Size = New System.Drawing.Size(109, 17)
    Me.rb_account_id.TabIndex = 1
    Me.rb_account_id.TabStop = True
    Me.rb_account_id.Text = "xID de Cuenta"
    Me.rb_account_id.UseVisualStyleBackColor = True
    '
    'rb_external_reference
    '
    Me.rb_external_reference.AutoSize = True
    Me.rb_external_reference.Location = New System.Drawing.Point(24, 25)
    Me.rb_external_reference.Name = "rb_external_reference"
    Me.rb_external_reference.Size = New System.Drawing.Size(141, 17)
    Me.rb_external_reference.TabIndex = 0
    Me.rb_external_reference.TabStop = True
    Me.rb_external_reference.Text = "xReferencia Externa"
    Me.rb_external_reference.UseVisualStyleBackColor = True
    '
    'gb_reference
    '
    Me.gb_reference.Controls.Add(Me.rb_external_reference)
    Me.gb_reference.Controls.Add(Me.rb_account_id)
    Me.gb_reference.Location = New System.Drawing.Point(9, 70)
    Me.gb_reference.Name = "gb_reference"
    Me.gb_reference.Size = New System.Drawing.Size(212, 89)
    Me.gb_reference.TabIndex = 2
    Me.gb_reference.TabStop = False
    Me.gb_reference.Text = "xReferencia"
    '
    'chk_add_accounts
    '
    Me.chk_add_accounts.AutoSize = True
    Me.chk_add_accounts.Location = New System.Drawing.Point(24, 26)
    Me.chk_add_accounts.Name = "chk_add_accounts"
    Me.chk_add_accounts.Size = New System.Drawing.Size(167, 17)
    Me.chk_add_accounts.TabIndex = 0
    Me.chk_add_accounts.Text = "xA�adir Nuevas Cuentas"
    Me.chk_add_accounts.UseVisualStyleBackColor = True
    '
    'chk_update_clients
    '
    Me.chk_update_clients.AutoSize = True
    Me.chk_update_clients.Location = New System.Drawing.Point(24, 49)
    Me.chk_update_clients.Name = "chk_update_clients"
    Me.chk_update_clients.Size = New System.Drawing.Size(170, 17)
    Me.chk_update_clients.TabIndex = 1
    Me.chk_update_clients.Text = "xActualizar Datos Cliente"
    Me.chk_update_clients.UseVisualStyleBackColor = True
    '
    'gb_new_accounts
    '
    Me.gb_new_accounts.Controls.Add(Me.chk_update_clients)
    Me.gb_new_accounts.Controls.Add(Me.chk_add_accounts)
    Me.gb_new_accounts.Location = New System.Drawing.Point(227, 70)
    Me.gb_new_accounts.Name = "gb_new_accounts"
    Me.gb_new_accounts.Size = New System.Drawing.Size(212, 89)
    Me.gb_new_accounts.TabIndex = 3
    Me.gb_new_accounts.TabStop = False
    Me.gb_new_accounts.Text = "xCuentas"
    '
    'chk_update_level
    '
    Me.chk_update_level.AutoSize = True
    Me.chk_update_level.Location = New System.Drawing.Point(24, 26)
    Me.chk_update_level.Name = "chk_update_level"
    Me.chk_update_level.Size = New System.Drawing.Size(121, 17)
    Me.chk_update_level.TabIndex = 0
    Me.chk_update_level.Text = "xActualizar Nivel"
    Me.chk_update_level.UseVisualStyleBackColor = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_update_level)
    Me.gb_level.Location = New System.Drawing.Point(445, 70)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(212, 89)
    Me.gb_level.TabIndex = 4
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xNivel"
    '
    'frm_accounts_import
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(770, 532)
    Me.Name = "frm_accounts_import"
    Me.Text = "frm_accounts_import"
    Me.panel_data.ResumeLayout(False)
    Me.gb_output.ResumeLayout(False)
    Me.gb_output.PerformLayout()
    Me.gb_balacence.ResumeLayout(False)
    Me.gb_balacence.PerformLayout()
    Me.gb_points.ResumeLayout(False)
    Me.gb_points.PerformLayout()
    Me.gb_reference.ResumeLayout(False)
    Me.gb_reference.PerformLayout()
    Me.gb_new_accounts.ResumeLayout(False)
    Me.gb_new_accounts.PerformLayout()
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_output As System.Windows.Forms.GroupBox
  Friend WithEvents tb_output As System.Windows.Forms.TextBox
  Friend WithEvents pgb_import_progress As System.Windows.Forms.ProgressBar
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_update_level As System.Windows.Forms.CheckBox
  Friend WithEvents gb_new_accounts As System.Windows.Forms.GroupBox
  Friend WithEvents chk_update_clients As System.Windows.Forms.CheckBox
  Friend WithEvents chk_add_accounts As System.Windows.Forms.CheckBox
  Friend WithEvents gb_reference As System.Windows.Forms.GroupBox
  Friend WithEvents rb_external_reference As System.Windows.Forms.RadioButton
  Friend WithEvents rb_account_id As System.Windows.Forms.RadioButton
  Friend WithEvents ef_file_path As GUI_Controls.uc_entry_field
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents chk_reset_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_update_points As System.Windows.Forms.CheckBox
  Friend WithEvents btn_open_file_dialog As System.Windows.Forms.Button
  Friend WithEvents gb_balacence As System.Windows.Forms.GroupBox
  Friend WithEvents chk_reset_balances As System.Windows.Forms.CheckBox
  Friend WithEvents chk_update_balance As System.Windows.Forms.CheckBox
End Class
