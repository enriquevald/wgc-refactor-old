'-------------------------------------------------------------------
' Copyright � 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_plays_sessions
' DESCRIPTION:   This screen allows to view the sessions between:
'                           - two dates 
'                           - for all terminals or one terminal
'                           - for all status or one status
' AUTHOR:        Merc� Torrelles Minguella
' CREATION DATE: 13-APR-2007
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-APR-2007  MTM    Initial version
' 15-APR-2009  MBF    Close play session
' 03-AUG-2011  RCI    Group by provider or account and more
' 31-JAN-2011  AJQ & RCI    Computed duration at DB level
' 17-JUL-2012  MPO    Added string -> new type of handpays
' 10-OCT-2012  RRB    Added index in the query (IX_ps_started_terminal_id)
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 28-MAR-2013  ICS    Added new columns to grid (redeemable, non-redeemable, plays)
' 17-APR-2013  SMN    Fixed Bug #722: The total of the initial column doesn't contain the recharges
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 01-OCT-2013  JPJ    Defect WIG-242 When key "ESC" is pressed an error shows up
' 03-OCT-2013  AMF    Computed and Awarded Points
' 12-NOV-2013  RMS    Added index selection by started and status or started and terminal
' 14-NOV-2013  RMS    Hidded DateFrom checkbox to force a date in the query
' 14-NOV-2013  JFC    Added new TITO columns to grid
' 21-NOV-2013  MMG    Added call to new form (frm_play_session_close) when closing play session
' 27-NOV-2013  AMF    #754 TITO: incorrect warning
' 12-DEC-2013  AMF    #479 Error in search
' 12-DEC-2013  QMP    Fixed bug 486: 3GS plays are out of balance
' 13-DEC-2013  AMF    Change ps_initial_balance/ps_final_balance of ps_total_cash_in/ps_total_cash_out
' 17-DEC-2013  RMS    In TITO Mode virtual accounts should be hidden but accessible
' 27-DEC-2013  RRR    Added card ID to ToolTip in TITO Mode
' 03-FEB-2014  JBC    Fixed Bug WIGOSTITO-1027 If card is recycled, tooltip doesn't show card number
' 19-MAR-2014  DHA    Form modified to edit play sessions values (Played/Won fields): 'Gaming session adjustment'
' 25-MAR-2014  DHA    Fixed Bug WIGOSTITO-1169 Set values: Initial, Final and NetWin (Redeemable/Non-Redeemable)
' 01-APR-2014  DLL    Added new funcionality WIG-782: new totals to sessions and duration
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 08-SEP-2014  FJC    Play Sessions: 'Progressive Provision' and 'Progressive Reserve' should appear with a new NLS and another color.
'                     Also, if we check 'solo sesiones descuadradas', 'Progressive Provision' and 'Progressive Reserve' 
'                      should not appear on the report.
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 02-OCT-2014  LEM    Fixed Bug WIG-1384: Don't shows terminal location columns correctly.
' 21-OCT-2014  OPC    Fixed Bug WIG-1544: Don't group by account the virtual accounts.
' 26-NOV-2014  JMV    Changed color of subtotal rows to light yellow
' 02-DIC-2014  DCS    Modify date filter, adding end date
' 17-DIC-2014  DCS    Fixed Bug WIG-1856: Holder Name not visible
' 30-JAN-2015  FJC    Sum columns ps_aux_ft_re_cash_in and ps_aux_ft_nr_cash_in to column Cash-IN in the Grid (Redeemable and Not Redeemable) 
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 03-NOV-2015  FOS    Change Label Cash In to Total In and Bill In To "efectivo"
' 12-JAN-2016  AMF    PBI 8260: Changes NLS
' 20-JAN-2016  AMF    PBI 8267: Rollback
' 11-FEB-2016  JMV    Fixed Bug 9231:Actividad de proveedores
' 02-AGO-2016  JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
' 19-SEP-2016  XGJ    Prepare accounts for PariPlay 
' 16-JAN-2016  JMM    PBI 19744:Pago autom�tico de handpays
' 05-APR-2017  DHA    Fixed Bug 26413: Error on accumulated total for TOTAL_IN NON_REDEEMABLE
' 05-APR-2017  RAB    Bug 26586: Error opening form Gaming Session Adjustment
' 03-MAY-2017  RAB    Bug 26413: Gaming sesions report not sum correctly the column Total Out, Netwin, Computed points and Awarded points
' 21-FEB-2017  LTC    PBI 22283:Winpot - Change Promotions NLS
' 15-MAY-2017  JCA    PBI 27476:Sorteo de m�quina - A�adir filtro solo mostrar sesiones de sorteo de bienvenida
' 26-JUN-2017  AMF    Bug 27555:Filtro sesiones descuadradas no funciona correctamente
' 19-JUL-2017  DPC    PBI 28851:[WIGOS-3786] Changes in Gaming sessions form
' 28-JUL-2017  ETP    WIGOS-4011 - [Ticket #7414] Pago Manual Erroneo en laboratorio QA
' 28-AGU-2017  DHA    PBI 29471:Plays Sessions - Hide "Promotion" type on legend (Obsolete)
' 10-OCT-2017  AMF    Bug 30199:[WIGOS-4917][Ticket #8618] Consulta sobre el reporte de sesiones de juego en la versi�n v03.005.0081
' 02-FEB-2018  AGS    Bug 31374:WIGOS-7985 [Ticket #7978] An�lisis Reporte Sesiones de Juego
' 22-FEB-2018  EOR    Bug 31699: WIGOS-5493 Missing Close play session button in MICO2
' 01-MAR-2018  AGS    Fixed Bug 31374:WIGOS-7985 [Ticket #7978] An�lisis Reporte Sesiones de Juego
' 10-JUL-2018  JSG    Bug 33554:WIGOS - 13473 Sesiones: Campos Remaining & Found on EGM  
' 26-JUL-2018  AGS    Bug 33728:WIGOS-13888 [Ticket #15908] Fallo - Sesiones de Juego � Duplica el valor del Jackpot en Total Out Version V03.08.0027
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common.MultiPromos
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_plays_sessions
  Inherits frm_base_sel_edit

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_PLAYS_SESSIONS

    Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    Call MyBase.GUI_SetFormId()

    Select Case FormId
      Case ENUM_FORM.FORM_PLAYS_SESSIONS
        Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA

      Case ENUM_FORM.FORM_POINTS_ASSIGNMENT
        Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA

      Case ENUM_FORM.FORM_PLAYS_SESSIONS_EDIT
        Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT

      Case Else
        Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA
    End Select

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_several_status As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_status As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_status As GUI_Controls.uc_combo
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents chk_grouped As System.Windows.Forms.CheckBox
  Friend WithEvents opt_provider As System.Windows.Forms.RadioButton
  Friend WithEvents opt_account As System.Windows.Forms.RadioButton
  Friend WithEvents chk_subtotal As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_gap_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_handpay As System.Windows.Forms.Label
  Friend WithEvents gb_view_columns As System.Windows.Forms.GroupBox
  Friend WithEvents chk_view_column_nr As System.Windows.Forms.CheckBox
  Friend WithEvents chk_view_column_re As System.Windows.Forms.CheckBox
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents chk_only_pending_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_manual_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents rb_finish_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_ini_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_historic As System.Windows.Forms.RadioButton
  Friend WithEvents rb_open_sessions As System.Windows.Forms.RadioButton
  Friend WithEvents gp_caption As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_handpay_session As System.Windows.Forms.Label
  Friend WithEvents tf_handpay_session As GUI_Controls.uc_text_field
  Friend WithEvents lbl_unbalanced As System.Windows.Forms.Label
  Friend WithEvents tf_unbalanced As GUI_Controls.uc_text_field
  Friend WithEvents lbl_handpay_unpaid As System.Windows.Forms.Label
  Friend WithEvents tf_handpay_unpaid As GUI_Controls.uc_text_field
  Friend WithEvents chk_only_cdt_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents chk_without_cdt_sessions As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_plays_sessions))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.rb_finish_date = New System.Windows.Forms.RadioButton()
    Me.rb_ini_date = New System.Windows.Forms.RadioButton()
    Me.rb_historic = New System.Windows.Forms.RadioButton()
    Me.rb_open_sessions = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.opt_several_status = New System.Windows.Forms.RadioButton()
    Me.opt_all_status = New System.Windows.Forms.RadioButton()
    Me.cmb_status = New GUI_Controls.uc_combo()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.gb_options = New System.Windows.Forms.GroupBox()
    Me.chk_without_cdt_sessions = New System.Windows.Forms.CheckBox()
    Me.chk_only_cdt_sessions = New System.Windows.Forms.CheckBox()
    Me.chk_only_gap_sessions = New System.Windows.Forms.CheckBox()
    Me.chk_show_sessions = New System.Windows.Forms.CheckBox()
    Me.chk_subtotal = New System.Windows.Forms.CheckBox()
    Me.opt_account = New System.Windows.Forms.RadioButton()
    Me.opt_provider = New System.Windows.Forms.RadioButton()
    Me.chk_grouped = New System.Windows.Forms.CheckBox()
    Me.gb_points = New System.Windows.Forms.GroupBox()
    Me.chk_only_manual_points = New System.Windows.Forms.CheckBox()
    Me.chk_only_pending_points = New System.Windows.Forms.CheckBox()
    Me.lbl_handpay = New System.Windows.Forms.Label()
    Me.gb_view_columns = New System.Windows.Forms.GroupBox()
    Me.chk_view_column_nr = New System.Windows.Forms.CheckBox()
    Me.chk_view_column_re = New System.Windows.Forms.CheckBox()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.gp_caption = New System.Windows.Forms.GroupBox()
    Me.lbl_handpay_unpaid = New System.Windows.Forms.Label()
    Me.tf_handpay_unpaid = New GUI_Controls.uc_text_field()
    Me.lbl_handpay_session = New System.Windows.Forms.Label()
    Me.tf_handpay_session = New GUI_Controls.uc_text_field()
    Me.lbl_unbalanced = New System.Windows.Forms.Label()
    Me.tf_unbalanced = New GUI_Controls.uc_text_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.gb_view_columns.SuspendLayout()
    Me.gp_caption.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gp_caption)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_view_columns)
    Me.panel_filter.Controls.Add(Me.gb_points)
    Me.panel_filter.Controls.Add(Me.lbl_handpay)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1237, 250)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_handpay, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_points, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_view_columns, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gp_caption, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 254)
    Me.panel_data.Size = New System.Drawing.Size(1237, 455)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1231, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1231, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.rb_finish_date)
    Me.gb_date.Controls.Add(Me.rb_ini_date)
    Me.gb_date.Controls.Add(Me.rb_historic)
    Me.gb_date.Controls.Add(Me.rb_open_sessions)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(4, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(251, 147)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'rb_finish_date
    '
    Me.rb_finish_date.AutoCheck = False
    Me.rb_finish_date.Location = New System.Drawing.Point(58, 71)
    Me.rb_finish_date.Name = "rb_finish_date"
    Me.rb_finish_date.Size = New System.Drawing.Size(144, 24)
    Me.rb_finish_date.TabIndex = 6
    Me.rb_finish_date.Text = "xFinishDate"
    '
    'rb_ini_date
    '
    Me.rb_ini_date.AutoCheck = False
    Me.rb_ini_date.Location = New System.Drawing.Point(58, 52)
    Me.rb_ini_date.Name = "rb_ini_date"
    Me.rb_ini_date.Size = New System.Drawing.Size(144, 24)
    Me.rb_ini_date.TabIndex = 5
    Me.rb_ini_date.Text = "xIniDate"
    '
    'rb_historic
    '
    Me.rb_historic.AutoCheck = False
    Me.rb_historic.Location = New System.Drawing.Point(8, 33)
    Me.rb_historic.Name = "rb_historic"
    Me.rb_historic.Size = New System.Drawing.Size(144, 24)
    Me.rb_historic.TabIndex = 4
    Me.rb_historic.Text = "xHistoric"
    '
    'rb_open_sessions
    '
    Me.rb_open_sessions.AutoCheck = False
    Me.rb_open_sessions.Location = New System.Drawing.Point(8, 13)
    Me.rb_open_sessions.Name = "rb_open_sessions"
    Me.rb_open_sessions.Size = New System.Drawing.Size(144, 24)
    Me.rb_open_sessions.TabIndex = 3
    Me.rb_open_sessions.Text = "xOpenSessions"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 118)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 94)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_several_status)
    Me.gb_status.Controls.Add(Me.opt_all_status)
    Me.gb_status.Controls.Add(Me.cmb_status)
    Me.gb_status.Location = New System.Drawing.Point(4, 153)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(251, 62)
    Me.gb_status.TabIndex = 1
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'opt_several_status
    '
    Me.opt_several_status.Location = New System.Drawing.Point(8, 12)
    Me.opt_several_status.Name = "opt_several_status"
    Me.opt_several_status.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_status.TabIndex = 0
    Me.opt_several_status.Text = "xOne"
    '
    'opt_all_status
    '
    Me.opt_all_status.Location = New System.Drawing.Point(8, 36)
    Me.opt_all_status.Name = "opt_all_status"
    Me.opt_all_status.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_status.TabIndex = 1
    Me.opt_all_status.Text = "xAll"
    '
    'cmb_status
    '
    Me.cmb_status.AllowUnlistedValues = False
    Me.cmb_status.AutoCompleteMode = False
    Me.cmb_status.IsReadOnly = False
    Me.cmb_status.Location = New System.Drawing.Point(80, 12)
    Me.cmb_status.Name = "cmb_status"
    Me.cmb_status.SelectedIndex = -1
    Me.cmb_status.Size = New System.Drawing.Size(163, 24)
    Me.cmb_status.SufixText = "Sufix Text"
    Me.cmb_status.SufixTextVisible = True
    Me.cmb_status.TabIndex = 2
    Me.cmb_status.TextCombo = Nothing
    Me.cmb_status.TextVisible = False
    Me.cmb_status.TextWidth = 0
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(580, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 3
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(254, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(327, 192)
    Me.uc_pr_list.TabIndex = 2
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.chk_without_cdt_sessions)
    Me.gb_options.Controls.Add(Me.chk_only_cdt_sessions)
    Me.gb_options.Controls.Add(Me.chk_only_gap_sessions)
    Me.gb_options.Controls.Add(Me.chk_show_sessions)
    Me.gb_options.Controls.Add(Me.chk_subtotal)
    Me.gb_options.Controls.Add(Me.opt_account)
    Me.gb_options.Controls.Add(Me.opt_provider)
    Me.gb_options.Controls.Add(Me.chk_grouped)
    Me.gb_options.Location = New System.Drawing.Point(889, 6)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(252, 177)
    Me.gb_options.TabIndex = 6
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'chk_without_cdt_sessions
    '
    Me.chk_without_cdt_sessions.AutoSize = True
    Me.chk_without_cdt_sessions.Location = New System.Drawing.Point(46, 100)
    Me.chk_without_cdt_sessions.Name = "chk_without_cdt_sessions"
    Me.chk_without_cdt_sessions.Size = New System.Drawing.Size(189, 17)
    Me.chk_without_cdt_sessions.TabIndex = 2
    Me.chk_without_cdt_sessions.Text = "xOnly Without CDT Sessions"
    Me.chk_without_cdt_sessions.UseVisualStyleBackColor = True
    Me.chk_without_cdt_sessions.Visible = False
    '
    'chk_only_cdt_sessions
    '
    Me.chk_only_cdt_sessions.AutoSize = True
    Me.chk_only_cdt_sessions.Location = New System.Drawing.Point(46, 80)
    Me.chk_only_cdt_sessions.Name = "chk_only_cdt_sessions"
    Me.chk_only_cdt_sessions.Size = New System.Drawing.Size(142, 17)
    Me.chk_only_cdt_sessions.TabIndex = 1
    Me.chk_only_cdt_sessions.Text = "xOnly CDT Sessions"
    Me.chk_only_cdt_sessions.UseVisualStyleBackColor = True
    Me.chk_only_cdt_sessions.Visible = False
    '
    'chk_only_gap_sessions
    '
    Me.chk_only_gap_sessions.AutoSize = True
    Me.chk_only_gap_sessions.Location = New System.Drawing.Point(6, 20)
    Me.chk_only_gap_sessions.Name = "chk_only_gap_sessions"
    Me.chk_only_gap_sessions.Size = New System.Drawing.Size(140, 17)
    Me.chk_only_gap_sessions.TabIndex = 0
    Me.chk_only_gap_sessions.Text = "xOnly Gap Sessions"
    Me.chk_only_gap_sessions.UseVisualStyleBackColor = True
    '
    'chk_show_sessions
    '
    Me.chk_show_sessions.AutoSize = True
    Me.chk_show_sessions.Location = New System.Drawing.Point(20, 148)
    Me.chk_show_sessions.Name = "chk_show_sessions"
    Me.chk_show_sessions.Size = New System.Drawing.Size(116, 17)
    Me.chk_show_sessions.TabIndex = 7
    Me.chk_show_sessions.Text = "xShow sessions"
    '
    'chk_subtotal
    '
    Me.chk_subtotal.AutoSize = True
    Me.chk_subtotal.Location = New System.Drawing.Point(20, 123)
    Me.chk_subtotal.Name = "chk_subtotal"
    Me.chk_subtotal.Size = New System.Drawing.Size(80, 17)
    Me.chk_subtotal.TabIndex = 6
    Me.chk_subtotal.Text = "xSubtotal"
    '
    'opt_account
    '
    Me.opt_account.AutoSize = True
    Me.opt_account.Location = New System.Drawing.Point(20, 70)
    Me.opt_account.Name = "opt_account"
    Me.opt_account.Size = New System.Drawing.Size(77, 17)
    Me.opt_account.TabIndex = 4
    Me.opt_account.Text = "xAccount"
    '
    'opt_provider
    '
    Me.opt_provider.AutoSize = True
    Me.opt_provider.Location = New System.Drawing.Point(20, 95)
    Me.opt_provider.Name = "opt_provider"
    Me.opt_provider.Size = New System.Drawing.Size(80, 17)
    Me.opt_provider.TabIndex = 5
    Me.opt_provider.Text = "xProvider"
    '
    'chk_grouped
    '
    Me.chk_grouped.AutoSize = True
    Me.chk_grouped.Location = New System.Drawing.Point(6, 45)
    Me.chk_grouped.Name = "chk_grouped"
    Me.chk_grouped.Size = New System.Drawing.Size(82, 17)
    Me.chk_grouped.TabIndex = 3
    Me.chk_grouped.Text = "xGrouped"
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.chk_only_manual_points)
    Me.gb_points.Controls.Add(Me.chk_only_pending_points)
    Me.gb_points.Location = New System.Drawing.Point(907, 76)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(171, 74)
    Me.gb_points.TabIndex = 0
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPoints"
    '
    'chk_only_manual_points
    '
    Me.chk_only_manual_points.AutoSize = True
    Me.chk_only_manual_points.Location = New System.Drawing.Point(12, 46)
    Me.chk_only_manual_points.Name = "chk_only_manual_points"
    Me.chk_only_manual_points.Size = New System.Drawing.Size(141, 17)
    Me.chk_only_manual_points.TabIndex = 1
    Me.chk_only_manual_points.Text = "xOnly Manual Points"
    Me.chk_only_manual_points.UseVisualStyleBackColor = True
    '
    'chk_only_pending_points
    '
    Me.chk_only_pending_points.AutoSize = True
    Me.chk_only_pending_points.Location = New System.Drawing.Point(12, 23)
    Me.chk_only_pending_points.Name = "chk_only_pending_points"
    Me.chk_only_pending_points.Size = New System.Drawing.Size(146, 17)
    Me.chk_only_pending_points.TabIndex = 0
    Me.chk_only_pending_points.Text = "xOnly Pending Points"
    Me.chk_only_pending_points.UseVisualStyleBackColor = True
    '
    'lbl_handpay
    '
    Me.lbl_handpay.AutoSize = True
    Me.lbl_handpay.ForeColor = System.Drawing.Color.Navy
    Me.lbl_handpay.Location = New System.Drawing.Point(265, 186)
    Me.lbl_handpay.Name = "lbl_handpay"
    Me.lbl_handpay.Size = New System.Drawing.Size(90, 13)
    Me.lbl_handpay.TabIndex = 11
    Me.lbl_handpay.Text = "xHP.: Handpay"
    '
    'gb_view_columns
    '
    Me.gb_view_columns.Controls.Add(Me.chk_view_column_nr)
    Me.gb_view_columns.Controls.Add(Me.chk_view_column_re)
    Me.gb_view_columns.Location = New System.Drawing.Point(907, 6)
    Me.gb_view_columns.Name = "gb_view_columns"
    Me.gb_view_columns.Size = New System.Drawing.Size(171, 69)
    Me.gb_view_columns.TabIndex = 5
    Me.gb_view_columns.TabStop = False
    Me.gb_view_columns.Text = "xColumns"
    Me.gb_view_columns.Visible = False
    '
    'chk_view_column_nr
    '
    Me.chk_view_column_nr.AutoSize = True
    Me.chk_view_column_nr.Location = New System.Drawing.Point(10, 41)
    Me.chk_view_column_nr.Name = "chk_view_column_nr"
    Me.chk_view_column_nr.Size = New System.Drawing.Size(128, 17)
    Me.chk_view_column_nr.TabIndex = 2
    Me.chk_view_column_nr.Text = "xView Column NR"
    Me.chk_view_column_nr.UseVisualStyleBackColor = True
    '
    'chk_view_column_re
    '
    Me.chk_view_column_re.AutoSize = True
    Me.chk_view_column_re.Location = New System.Drawing.Point(10, 20)
    Me.chk_view_column_re.Name = "chk_view_column_re"
    Me.chk_view_column_re.Size = New System.Drawing.Size(127, 17)
    Me.chk_view_column_re.TabIndex = 1
    Me.chk_view_column_re.Text = "xView Column RE"
    Me.chk_view_column_re.UseVisualStyleBackColor = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(600, 143)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(221, 17)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'gp_caption
    '
    Me.gp_caption.Controls.Add(Me.lbl_handpay_unpaid)
    Me.gp_caption.Controls.Add(Me.tf_handpay_unpaid)
    Me.gp_caption.Controls.Add(Me.lbl_handpay_session)
    Me.gp_caption.Controls.Add(Me.tf_handpay_session)
    Me.gp_caption.Controls.Add(Me.lbl_unbalanced)
    Me.gp_caption.Controls.Add(Me.tf_unbalanced)
    Me.gp_caption.Location = New System.Drawing.Point(597, 164)
    Me.gp_caption.Name = "gp_caption"
    Me.gp_caption.Size = New System.Drawing.Size(162, 83)
    Me.gp_caption.TabIndex = 110
    Me.gp_caption.TabStop = False
    Me.gp_caption.Text = "xStatus"
    '
    'lbl_handpay_unpaid
    '
    Me.lbl_handpay_unpaid.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_handpay_unpaid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_handpay_unpaid.Location = New System.Drawing.Point(10, 59)
    Me.lbl_handpay_unpaid.Name = "lbl_handpay_unpaid"
    Me.lbl_handpay_unpaid.Size = New System.Drawing.Size(16, 16)
    Me.lbl_handpay_unpaid.TabIndex = 112
    '
    'tf_handpay_unpaid
    '
    Me.tf_handpay_unpaid.IsReadOnly = True
    Me.tf_handpay_unpaid.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_handpay_unpaid.LabelForeColor = System.Drawing.Color.Black
    Me.tf_handpay_unpaid.Location = New System.Drawing.Point(4, 55)
    Me.tf_handpay_unpaid.Name = "tf_handpay_unpaid"
    Me.tf_handpay_unpaid.Size = New System.Drawing.Size(154, 24)
    Me.tf_handpay_unpaid.SufixText = "Sufix Text"
    Me.tf_handpay_unpaid.SufixTextVisible = True
    Me.tf_handpay_unpaid.TabIndex = 111
    Me.tf_handpay_unpaid.TabStop = False
    Me.tf_handpay_unpaid.TextVisible = False
    Me.tf_handpay_unpaid.TextWidth = 30
    Me.tf_handpay_unpaid.Value = "xHandpayUnpaid"
    '
    'lbl_handpay_session
    '
    Me.lbl_handpay_session.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_handpay_session.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_handpay_session.Location = New System.Drawing.Point(10, 39)
    Me.lbl_handpay_session.Name = "lbl_handpay_session"
    Me.lbl_handpay_session.Size = New System.Drawing.Size(16, 16)
    Me.lbl_handpay_session.TabIndex = 107
    '
    'tf_handpay_session
    '
    Me.tf_handpay_session.IsReadOnly = True
    Me.tf_handpay_session.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_handpay_session.LabelForeColor = System.Drawing.Color.Black
    Me.tf_handpay_session.Location = New System.Drawing.Point(4, 35)
    Me.tf_handpay_session.Name = "tf_handpay_session"
    Me.tf_handpay_session.Size = New System.Drawing.Size(154, 24)
    Me.tf_handpay_session.SufixText = "Sufix Text"
    Me.tf_handpay_session.SufixTextVisible = True
    Me.tf_handpay_session.TabIndex = 108
    Me.tf_handpay_session.TabStop = False
    Me.tf_handpay_session.TextVisible = False
    Me.tf_handpay_session.TextWidth = 30
    Me.tf_handpay_session.Value = "xHandpaySession"
    '
    'lbl_unbalanced
    '
    Me.lbl_unbalanced.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_unbalanced.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_unbalanced.Location = New System.Drawing.Point(10, 19)
    Me.lbl_unbalanced.Name = "lbl_unbalanced"
    Me.lbl_unbalanced.Size = New System.Drawing.Size(16, 16)
    Me.lbl_unbalanced.TabIndex = 106
    '
    'tf_unbalanced
    '
    Me.tf_unbalanced.IsReadOnly = True
    Me.tf_unbalanced.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_unbalanced.LabelForeColor = System.Drawing.Color.Black
    Me.tf_unbalanced.Location = New System.Drawing.Point(4, 15)
    Me.tf_unbalanced.Name = "tf_unbalanced"
    Me.tf_unbalanced.Size = New System.Drawing.Size(152, 24)
    Me.tf_unbalanced.SufixText = "Sufix Text"
    Me.tf_unbalanced.SufixTextVisible = True
    Me.tf_unbalanced.TabIndex = 104
    Me.tf_unbalanced.TabStop = False
    Me.tf_unbalanced.TextVisible = False
    Me.tf_unbalanced.TextWidth = 30
    Me.tf_unbalanced.Value = "xUnbalanced"
    '
    'frm_plays_sessions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1245, 713)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_plays_sessions"
    Me.Text = "frm_plays_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.gb_points.ResumeLayout(False)
    Me.gb_points.PerformLayout()
    Me.gb_view_columns.ResumeLayout(False)
    Me.gb_view_columns.PerformLayout()
    Me.gp_caption.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS As Int32

  Private Const SQL_COLUMN_SESSION_ID As Integer = 0
  Private Const SQL_COLUMN_STARTED As Integer = 1
  Private Const SQL_COLUMN_FINISHED As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const SQL_COLUMN_TOTAL_CASH_IN As Integer = 4
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 5
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 7
  Private Const SQL_COLUMN_WON_COUNT As Integer = 8
  Private Const SQL_COLUMN_CASH_IN_PLAY_SESSION As Integer = 9
  Private Const SQL_COLUMN_TOTAL_CASH_OUT As Integer = 10
  Private Const SQL_COLUMN_STATUS As Integer = 11
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 12
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 13
  Private Const SQL_COLUMN_TRACKDATA As Integer = 14
  Private Const SQL_COLUMN_PROMOTIONAL As Integer = 15
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 16
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 17
  Private Const SQL_COLUMN_DURATION As Integer = 18
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 19
  Private Const SQL_COLUMN_TYPE As Integer = 20
  Private Const SQL_COLUMN_RE_CASH_IN As Integer = 21
  Private Const SQL_COLUMN_RE_PLAYED_AMOUNT As Integer = 22
  Private Const SQL_COLUMN_RE_WON_AMOUNT As Integer = 23
  Private Const SQL_COLUMN_RE_CASH_OUT As Integer = 24
  Private Const SQL_COLUMN_NRE_CASH_IN As Integer = 25
  Private Const SQL_COLUMN_NRE_PLAYED_AMOUNT As Integer = 26
  Private Const SQL_COLUMN_NRE_WON_AMOUNT As Integer = 27
  Private Const SQL_COLUMN_NRE_CASH_OUT As Integer = 28
  Private Const SQL_COLUMN_COMPUTED_POINTS As Integer = 29
  Private Const SQL_COLUMN_AWARDED_POINTS As Integer = 30
  Private Const SQL_COLUMN_AWARDED_POINTS_STATUS As Integer = 31

  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 32
  ' DHA 17-MAR-2014
  Private Const SQL_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL As Integer = 33
  Private Const SQL_COLUMN_RE_WON_AMOUNT_ORIGINAL As Integer = 34
  Private Const SQL_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL As Integer = 35
  Private Const SQL_COLUMN_NRE_WON_AMOUNT_ORIGINAL As Integer = 36
  Private Const SQL_COLUMN_PLAYED_COUNT_ORIGINAL As Integer = 37
  Private Const SQL_COLUMN_WON_COUNT_ORIGINAL As Integer = 38

  'JMM 20/10/2016
  Private Const SQL_COLUMN_HANDPAYS_AMOUNT As Integer = 39
  Private Const SQL_COLUMN_HANDPAYS_PAID_AMOUNT As Integer = 40

  Private Const SQL_COLUMN_RE_TICKET_IN As Integer = 41
  Private Const SQL_COLUMN_PROMO_RE_TICKET_IN As Integer = 42
  Private Const SQL_COLUMN_BILLS_IN_AMOUNT As Integer = 43
  Private Const SQL_COLUMN_PROMO_RE_TICKET_OUT As Integer = 44
  Private Const SQL_COLUMN_PROMO_NONRE_TICKET_IN As Integer = 45
  Private Const SQL_COLUMN_PROMO_NONRE_TICKET_OUT As Integer = 46

  'FJC 30/01/2015
  Private Const SQL_COLUMN_AUX_FT_RE_CASH_IN As Integer = 47
  Private Const SQL_COLUMN_AUX_FT_NR_CASH_IN As Integer = 48

  Private Const SQL_COLUMN_FOUND_IN_EGM As Integer = 49
  Private Const SQL_COLUMN_REMAINING_IN_EGM As Integer = 50

  Private Const GRID_COLUMN_GROUPED_A1 As Integer = 1
  Private Const GRID_COLUMN_GROUPED_A2 As Integer = 2
  Private Const GRID_COLUMN_GROUPED_A3 As Integer = 3
  Private Const GRID_COLUMN_GROUPED_A4 As Integer = 4
  Private Const GRID_COLUMN_GROUPED_A5 As Integer = 5
  Private Const GRID_COLUMN_GROUPED_A6 As Integer = 6
  Private Const GRID_COLUMN_GROUPED_A7 As Integer = 7
  Private Const GRID_COLUMN_GROUPED_A8 As Integer = 8
  Private Const GRID_COLUMN_GROUPED_A9 As Integer = 9

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER_ID As Integer
  Private GRID_COLUMN_TERMINAL_NAME As Integer
  Private GRID_COLUMN_TERMINAL_TYPE_NAME As Integer
  Private GRID_COLUMN_STARTED As Integer
  Private GRID_COLUMN_FINISHED As Integer
  Private GRID_COLUMN_STATUS As Integer
  Private GRID_COLUMN_TIME As Integer
  Private GRID_COLUMN_ACCOUNT_ID As Integer
  Private GRID_COLUMN_HOLDER_NAME As Integer
  Private GRID_COLUMN_RE_INITIAL_BALANCE As Integer
  Private GRID_COLUMN_RE_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_RE_WON_AMOUNT As Integer
  Private GRID_COLUMN_RE_FINAL_BALANCE As Integer
  Private GRID_COLUMN_RE_NETWIN As Integer
  Private GRID_COLUMN_REDEEMABLE_TICKET_IN As Integer
  Private GRID_COLUMN_PROMO_RE_TICKET_IN As Integer
  Private GRID_COLUMN_BILLS_IN_AMOUNT As Integer
  Private GRID_COLUMN_REDEEMABLE_TICKET_OUT As Integer
  Private GRID_COLUMN_NRE_INITIAL_BALANCE As Integer
  Private GRID_COLUMN_NRE_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_NRE_WON_AMOUNT As Integer
  Private GRID_COLUMN_NRE_FINAL_BALANCE As Integer
  Private GRID_COLUMN_NRE_NETWIN As Integer
  Private GRID_COLUMN_PROMO_NONRE_TICKET_IN As Integer
  Private GRID_COLUMN_PROMO_NONRE_TICKET_OUT As Integer
  Private GRID_COLUMN_CASH_IN_PLAY_SESSION As Integer
  Private GRID_COLUMN_INITIAL_BALANCE As Integer
  Private GRID_COLUMN_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_WON_AMOUNT As Integer
  Private GRID_COLUMN_FINAL_BALANCE As Integer
  Private GRID_COLUMN_HANDPAYS_AMOUNT As Integer
  Private GRID_COLUMN_HANDPAYS_PAID_AMOUNT As Integer
  Private GRID_COLUMN_NETWIN As Integer
  Private GRID_COLUMN_TOTAL_TICKET_IN As Integer
  Private GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL As Integer
  Private GRID_COLUMN_TOTAL_TICKET_OUT As Integer
  Private GRID_COLUMN_SESSION_ID As Integer
  Private GRID_COLUMN_CARD_ID As Integer
  Private GRID_COLUMN_TERMINAL_TYPE As Integer
  Private GRID_COLUMN_COMPUTED_POINTS As Integer
  Private GRID_COLUMN_AWARDED_POINTS As Integer
  Private GRID_COLUMN_PLAYED_COUNT As Integer
  Private GRID_COLUMN_WON_COUNT As Integer
  Private GRID_COLUMN_PROMOTIONAL As Integer
  Private GRID_COLUMN_AWARDED_POINTS_ORIGINAL As Integer
  Private GRID_COLUMN_AWARDED_POINTS_STATUS As Integer
  Private GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL As Integer
  Private GRID_COLUMN_TERMINAL_ID As Integer
  ' DHA 17-MAR-2014
  Private GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL As Integer
  Private GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL As Integer
  Private GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL As Integer
  Private GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL As Integer
  Private GRID_COLUMN_PLAYED_COUNT_ORIGINAL As Integer
  Private GRID_COLUMN_WON_COUNT_ORIGINAL As Integer
  ' DHA 17-MAR-2014
  Private GRID_COLUMN_RE_PLAYED_AMOUNT_OLD As Integer
  Private GRID_COLUMN_RE_WON_AMOUNT_OLD As Integer
  Private GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD As Integer
  Private GRID_COLUMN_NRE_WON_AMOUNT_OLD As Integer
  Private GRID_COLUMN_PLAYED_COUNT_OLD As Integer
  Private GRID_COLUMN_WON_COUNT_OLD As Integer
  Private GRID_COLUMN_EGM_CREDIT_FINAL As Integer
  Private GRID_COLUMN_EGM_CREDIT_INITIAL As Integer

  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 1900
  Private Const GRID_WIDTH_TIME As Integer = 1500
  Private Const GRID_WIDTH_AMOUNT As Integer = 1200
  Private Const GRID_WIDTH_AMOUNT_NON_REDEEMABLE As Integer = 1500
  Private Const GRID_WIDTH_PROMO As Integer = 700
  Private Const GRID_WIDTH_PROVIDER As Integer = 1850
  Private Const GRID_WIDTH_TERMINAL As Integer = 1710
  Private Const GRID_WIDTH_CARD_ACCOUNT As Integer = 1000
  Private Const GRID_WIDTH_CARD_ID As Integer = 2250
  Private Const GRID_WIDTH_PLAYS As Integer = 1000

  Private Const GRID_WIDTH_COLUMN_TICKETS_DEFAULT As Integer = 1200
  Private Const GRID_WIDTH_COLUMN_PROMO_TICKET As Integer = 1650

  ' Timeout to allow closing a session (in minutes)
  Private Const INACTIVITY_TIMEOUT As Integer = 60

  Private Const FORM_DB_MIN_VERSION As Short = 153

  Private GRID_NUM_COLUMNS As Short = TERMINAL_DATA_COLUMNS + 58

  Private Const STR_RECYCLED_CARD As String = "RECYCLED-VIRTUAL"

  Private Const PS_COLOR_UNBALANCED As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_RED_01
  Private Const PS_COLOR_HANDPAY_UNPAID As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00
  'Private Const PS_COLOR_PROMOTION As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_BLUE_01
  Private Const PS_COLOR_HANDPAY_PLAYSESSION As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00

#End Region ' Constants

#Region " Query Index Constants "

  Private Const INDEX_DEFAULT As String = " WITH( INDEX(IX_ps_started))"
  Private Const INDEX_TERMINALS As String = " WITH( INDEX(IX_ps_started_terminal_id))"
  Private Const INDEX_STATUS As String = " WITH( INDEX(IX_ps_status))"
  Private Const INDEX_FINISHED As String = " WITH( INDEX(IX_ps_finished_status))"
  Private Const INDEX_ACCOUNT_STARTED As String = " WITH( INDEX(IX_ps_account_id_started))"
  Private Const INDEX_ACCOUNT_FINISHED As String = " WITH( INDEX(IX_ps_account_id_finished))"

#End Region ' Query Index Constants

#Region "Enums"

  Private Enum ENUM_GROUPED
    NOT_GROUPED = 0
    BY_PROVIDER = 1
    BY_ACCOUNT = 2
  End Enum

  Private Enum ENUM_GRID_ROW_TYPE
    NORMAL = 0
    TOTAL_PROVIDER = 1
    TOTAL_TERMINAL = 2
    TOTAL_ACCOUNT = 3
    TOTAL = 4
  End Enum

  Public Enum ENUM_PLAY_SESSIONS_SCREEN_MODE
    MODE_SESSION_DATA = 0
    MODE_POINTS_DATA = 1
    MODE_FULL_DATA = 2
    MODE_SESSION_EDIT = 3
  End Enum

#End Region ' Enums

#Region " Structures "

  Private Structure TYPE_TOTAL_COUNTERS
    Dim key As String
    Dim played_amount As Double
    Dim won_amount As Double
    Dim initial_balance As Double
    Dim cash_in_play_session As Double
    Dim fin_balance As Double
    Dim redeemable_initial_balance As Double
    Dim redeemable_played_amount As Double
    Dim redeemable_won_amount As Double
    Dim redeemable_fin_balance As Double
    Dim not_redeemable_initial_balance As Double
    Dim not_redeemable_played_amount As Double
    Dim not_redeemable_won_amount As Double
    Dim not_redeemable_fin_balance As Double
    Dim computed_points As Double
    Dim awarded_points As Double
    Dim played_count As Integer
    Dim won_count As Integer
    Dim redeemable_ticket_in As Double
    Dim redeemable_ticket_in_promo As Double
    Dim bill_in As Double
    Dim redeemable_ticket_out As Double
    Dim not_redeemable_ticket_in As Double
    Dim not_redeemable_ticket_out As Double
    Dim total_ticket_in As Double
    Dim total_bill_in As Double
    Dim total_ticket_out As Double
    Dim handpay As Double
    Dim remaining As Double
    Dim found As Double
    Dim total_remaining As Double
    Dim total_found As Double
  End Structure

#End Region ' Structures

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_filter As String
  Private m_date_open_sessions As Boolean
  Private m_date_to As String
  Private m_status As String
  Private m_terminals As String
  Private m_card_account As String
  Private m_card_track_data As String
  Private m_card_holder_name As String
  Private m_holder_is_vip As String
  Private m_options_value As String
  Private m_points_value As String
  Private m_columns_value As String

  ' For totals
  Private m_first_time As Boolean
  Private m_grouped_by As ENUM_GROUPED
  Private m_terminal_total As TYPE_TOTAL_COUNTERS
  Private m_provider_total As TYPE_TOTAL_COUNTERS
  Private m_account_total As TYPE_TOTAL_COUNTERS

  Private m_global_total As TYPE_TOTAL_COUNTERS

  Private m_count_session As Integer
  Private m_duration_total As Int32

  Private m_last_grouped_by As ENUM_GROUPED

  Private m_point_awarded_changed As Double

  Private m_negative_accounts As List(Of String)

  Public m_screen_mode As Integer

  Private m_permission As CLASS_GUI_USER.TYPE_PERMISSIONS     ' Permission object

  Private m_is_tito_mode As Boolean
  Private m_number_of_columns As Integer

  Private m_terminal_columns As List(Of ColumnSettings)
  Private m_provider_col As Int32
  Private m_terminal_col As Int32
  Private m_current_terminal_data As List(Of Object)

  Private m_is_mico2 As Boolean

#End Region ' Members

#Region " OVERRIDES "

  'PURPOSE: Executed when cell data change
  '         
  ' PARAMS:
  '    - INPUT :
  '             Row
  '             Column
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

    Dim _idx_row As Integer
    Dim _points As Double
    Dim _color_back As ENUM_GUI_COLOR
    Dim _color_fore As ENUM_GUI_COLOR
    Dim _ratio As Decimal
    Dim _total_calculate As Double
    Dim _re_calculate As Double
    Dim _nre_calculate As Double

    If Column = GRID_COLUMN_AWARDED_POINTS Then
      With Me.Grid
        ' Not Allow negative points 
        _points = IIf(String.IsNullOrEmpty(.Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value), 0, .Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value)
        If _points < 0 Then
          .Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value = .Cell(Row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value

          Return
        End If
        ' Status
        If Not String.IsNullOrEmpty(.Cell(Row, Column).Value) Then
          If .Cell(Row, Column).Value = .Cell(Row, GRID_COLUMN_COMPUTED_POINTS).Value Then
            .Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = AwardPointsStatus.ManualCalculated
            _color_back = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
            _color_fore = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
          Else
            .Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = AwardPointsStatus.Manual
            _color_back = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
            _color_fore = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
          End If
        ElseIf .Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value = AwardPointsStatus.Pending Then
          .Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = AwardPointsStatus.Pending
          _color_back = ENUM_GUI_COLOR.GUI_COLOR_BLUE_01
        Else
          .Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value = .Cell(Row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value

          Return
        End If

        ' Set Colors
        .Cell(Row, GRID_COLUMN_AWARDED_POINTS).BackColor = GetColor(_color_back)
        .Cell(Row, GRID_COLUMN_AWARDED_POINTS).ForeColor = GetColor(_color_fore)
        .Cell(Row, GRID_COLUMN_COMPUTED_POINTS).BackColor = GetColor(_color_back)
        .Cell(Row, GRID_COLUMN_COMPUTED_POINTS).ForeColor = GetColor(_color_fore)

        If chk_grouped.Checked Then
          ' Modify Subtotal
          For _idx_row = Row To .NumRows - 1
            If Not IsValidDataRow(_idx_row) Then
              ' Subtotal group by account
              _points = GUI_ParseNumber(.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value) + _
                        GUI_ParseNumber(IIf(String.IsNullOrEmpty(.Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value), 0, .Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value)) - _
                        m_point_awarded_changed
              .Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value = GUI_FormatNumber(_points)
              ' Subtotal by provider
              If Not IsValidDataRow(_idx_row + 1) And _idx_row + 1 <> .NumRows - 1 Then
                _points = GUI_ParseNumber(.Cell(_idx_row + 1, GRID_COLUMN_AWARDED_POINTS).Value) + _
                          GUI_ParseNumber(IIf(String.IsNullOrEmpty(.Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value), 0, .Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value)) - _
                          m_point_awarded_changed
                .Cell(_idx_row + 1, GRID_COLUMN_AWARDED_POINTS).Value = GUI_FormatNumber(_points)
              End If

              Exit For
            End If
          Next
        End If
        ' Modify Total
        If _idx_row <> .NumRows - 1 Then
          _points = GUI_ParseNumber(.Cell(.NumRows - 1, GRID_COLUMN_AWARDED_POINTS).Value) + _
                    GUI_ParseNumber(IIf(String.IsNullOrEmpty(.Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value), 0, .Cell(Row, GRID_COLUMN_AWARDED_POINTS).Value)) - _
                    m_point_awarded_changed
          .Cell(.NumRows - 1, GRID_COLUMN_AWARDED_POINTS).Value = GUI_FormatNumber(_points)
        End If
      End With
    ElseIf Column = GRID_COLUMN_RE_PLAYED_AMOUNT Or Column = GRID_COLUMN_RE_WON_AMOUNT Or _
            Column = GRID_COLUMN_NRE_PLAYED_AMOUNT Or Column = GRID_COLUMN_NRE_WON_AMOUNT Then
      With Me.Grid

        .Cell(Row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(GUI_ParseCurrency(.Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value) + GUI_ParseCurrency(.Cell(Row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(Row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(GUI_ParseCurrency(.Cell(Row, GRID_COLUMN_RE_WON_AMOUNT).Value) + GUI_ParseCurrency(.Cell(Row, GRID_COLUMN_NRE_WON_AMOUNT).Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      End With
    ElseIf Column = GRID_COLUMN_PLAYED_AMOUNT Then
      With Me.Grid
        ' Ratio = RE Played / Total Played
        _total_calculate = (GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Value) + GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Value))
        If _total_calculate <> 0 Then
          _ratio = GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Value) / _total_calculate
        Else
          _ratio = 1
        End If

        _re_calculate = Math.Round(_ratio * GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_AMOUNT).Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        _nre_calculate = GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_AMOUNT).Value) - _re_calculate

        .Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_re_calculate, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(Row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_nre_calculate, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      End With
    ElseIf Column = GRID_COLUMN_WON_AMOUNT Then
      With Me.Grid
        ' Ratio = RE Won / Total Won
        _total_calculate = (GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Value) + GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Value))
        If _total_calculate <> 0 Then
          _ratio = GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Value) / _total_calculate
        Else
          _ratio = 1
        End If

        _re_calculate = Math.Round(_ratio * GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_WON_AMOUNT).Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        _nre_calculate = GUI_ParseCurrency(Me.Grid.Cell(Row, GRID_COLUMN_WON_AMOUNT).Value) - _re_calculate

        .Cell(Row, GRID_COLUMN_RE_WON_AMOUNT).Value = GUI_FormatCurrency(_re_calculate, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        .Cell(Row, GRID_COLUMN_NRE_WON_AMOUNT).Value = GUI_FormatCurrency(_nre_calculate, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      End With
    ElseIf Column = GRID_COLUMN_PLAYED_COUNT Or Column = GRID_COLUMN_WON_COUNT Then
      With Me.Grid
        If GUI_ParseNumber(Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_COUNT).Value) < 0 Then
          Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_COUNT).Value = Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_COUNT_OLD).Value
        End If
        If GUI_ParseNumber(Me.Grid.Cell(Row, GRID_COLUMN_WON_COUNT).Value) < 0 Then
          Me.Grid.Cell(Row, GRID_COLUMN_WON_COUNT).Value = Me.Grid.Cell(Row, GRID_COLUMN_WON_COUNT_OLD).Value
        End If
      End With
    End If
  End Sub ' GUI_CellDataChangedEvent

  'PURPOSE: Executed before start Edition
  '         
  ' PARAMS:
  '    - INPUT :
  '             Row
  '             Column
  '             EditionCanceled
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean)

    If Column = GRID_COLUMN_AWARDED_POINTS Then
      If Not IsValidDataRow(Row) Then
        EditionCanceled = True
      ElseIf Me.Grid.Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value <> AwardPointsStatus.Pending And _
             Me.Grid.Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value <> AwardPointsStatus.Manual And _
             Me.Grid.Cell(Row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value <> AwardPointsStatus.ManualCalculated Then
        EditionCanceled = True
      Else
        m_point_awarded_changed = GUI_ParseNumber(Me.Grid.Cell(Row, Column).Value)
      End If
      ' DHA 17-MAR-2014
    ElseIf Column = GRID_COLUMN_RE_PLAYED_AMOUNT Or Column = GRID_COLUMN_NRE_PLAYED_AMOUNT Or Column = GRID_COLUMN_PLAYED_AMOUNT Then
      If Not IsValidDataRow(Row) Then
        EditionCanceled = True
      End If
      If Me.Grid.Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value = "" Then
        Me.Grid.Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value = Me.Grid.Cell(Row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value
      End If
      If Me.Grid.Cell(Row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value = "" Then
        Me.Grid.Cell(Row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value = Me.Grid.Cell(Row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value
      End If
    ElseIf Column = GRID_COLUMN_RE_WON_AMOUNT Or Column = GRID_COLUMN_NRE_WON_AMOUNT Or Column = GRID_COLUMN_WON_AMOUNT Then
      If Not IsValidDataRow(Row) Then
        EditionCanceled = True
      End If
      If Me.Grid.Cell(Row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value = "" Then
        Me.Grid.Cell(Row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value = Me.Grid.Cell(Row, GRID_COLUMN_RE_WON_AMOUNT).Value
      End If
      If Me.Grid.Cell(Row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value = "" Then
        Me.Grid.Cell(Row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value = Me.Grid.Cell(Row, GRID_COLUMN_NRE_WON_AMOUNT).Value
      End If
    ElseIf Column = GRID_COLUMN_PLAYED_COUNT Then
      If Not IsValidDataRow(Row) Then
        EditionCanceled = True
      ElseIf Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_COUNT_OLD).Value = "" Then
        Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_COUNT_OLD).Value = Me.Grid.Cell(Row, GRID_COLUMN_PLAYED_COUNT).Value
      End If
    ElseIf Column = GRID_COLUMN_WON_COUNT Then
      If Not IsValidDataRow(Row) Then
        EditionCanceled = True
      ElseIf Me.Grid.Cell(Row, GRID_COLUMN_WON_COUNT_OLD).Value = "" Then
        Me.Grid.Cell(Row, GRID_COLUMN_WON_COUNT_OLD).Value = Me.Grid.Cell(Row, GRID_COLUMN_WON_COUNT).Value
      End If
    End If

  End Sub ' GUI_BeforeStartEditionEvent

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If

  End Sub ' GUI_Closing

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    ' Leave this sub empty, as the FormId is set now in the Constructor of the class.
    'Me.FormId = ENUM_FORM.FORM_PLAYS_SESSIONS
    'Call MyBase.GUI_SetFormId()

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_is_tito_mode = TITO.Utils.IsTitoMode

    Select Case Me.FormId
      Case ENUM_FORM.FORM_PLAYS_SESSIONS
        Me.Text = GLB_NLS_GUI_STATISTICS.GetString(357)
        ' Virtual accounts should be hidden
        Call Me.uc_account_sel1.InitExtendedQuery(True)

      Case ENUM_FORM.FORM_POINTS_ASSIGNMENT
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2814)
        ' Virtual accounts should be hidden
        Call Me.uc_account_sel1.InitExtendedQuery(False)

      Case ENUM_FORM.FORM_PLAYS_SESSIONS_EDIT
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4739)
        ' Virtual accounts should be hidden
        Call Me.uc_account_sel1.InitExtendedQuery(True)

      Case Else
        Me.Text = GLB_NLS_GUI_STATISTICS.GetString(357)
        ' Virtual accounts should be hidden
        Call Me.uc_account_sel1.InitExtendedQuery(False)
    End Select

    'Load Permisions
    m_permission = CurrentUser.Permissions(Me.FormId)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Text = GLB_NLS_GUI_STATISTICS.GetString(3)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_STATISTICS.GetString(1) ' "Close"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(26)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    Else
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2825)
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Size = New System.Drawing.Size(90, 45)
    End If

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3006)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Size = New System.Drawing.Size(90, 45)

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Or Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = False
      Me.lbl_handpay.Visible = False
    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    End If

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201) ' 201 - Date
    Me.rb_open_sessions.Text = GLB_NLS_GUI_STATISTICS.GetString(345) ' 345 - Open Sessions
    Me.rb_historic.Text = GLB_NLS_GUI_CONTROLS.GetString(331) ' 331 - History
    Me.rb_ini_date.Text = GLB_NLS_GUI_STATISTICS.GetString(381) ' 381 - Started
    Me.rb_finish_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704) ' 5704 - Finished
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    If Me.m_screen_mode <> ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA Then
      Me.rb_open_sessions.Checked = False
      Me.rb_open_sessions.Visible = False
      Me.rb_historic.Checked = True
      Me.rb_historic.Location = New Point(Me.rb_historic.Location.X, Me.rb_historic.Location.Y - 18)
      Me.rb_ini_date.Location = New Point(Me.rb_ini_date.Location.X, Me.rb_ini_date.Location.Y - 15)
      Me.rb_finish_date.Location = New Point(Me.rb_finish_date.Location.X, Me.rb_finish_date.Location.Y - 12)
      Me.dtp_from.Location = New Point(Me.dtp_from.Location.X, Me.dtp_from.Location.Y - 9)
      Me.dtp_to.Location = New Point(Me.dtp_to.Location.X, Me.dtp_to.Location.Y - 6)
    End If

    AddHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_STATISTICS.GetString(394)
    Me.opt_several_status.Text = GLB_NLS_GUI_ALARMS.GetString(276)
    Me.opt_all_status.Text = GLB_NLS_GUI_ALARMS.GetString(277)

    ' RCI 13-DEC-2010: Only Gap Sessions
    Me.chk_only_gap_sessions.Text = GLB_NLS_GUI_STATISTICS.GetString(424)

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA Or Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      Me.gb_points.Visible = False
    Else
      Me.gb_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205)
      Me.chk_only_pending_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2689)
      Me.chk_only_manual_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2797)
    End If

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
      ' Columns
      Me.gb_view_columns.Visible = True
      Me.gb_view_columns.Text = GLB_NLS_GUI_INVOICING.GetString(369)
      Me.chk_view_column_nr.Checked = False
      Me.chk_view_column_nr.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2801)
      Me.chk_view_column_re.Checked = False
      Me.chk_view_column_re.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2800)
      ' Options
      Me.gb_options.Visible = False
      Me.chk_grouped.Checked = True
      Me.opt_account.Checked = True
      Me.chk_show_sessions.Checked = True
    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      ' Options
      Me.gb_options.Visible = False
      Me.chk_grouped.Checked = True
      Me.opt_account.Checked = True
      Me.chk_show_sessions.Checked = True

      Me.gb_view_columns.Visible = False
      Me.chk_view_column_nr.Checked = True
      Me.chk_view_column_re.Checked = True
    Else
      Me.gb_view_columns.Visible = False
      Me.chk_view_column_nr.Checked = True
      Me.chk_view_column_re.Checked = True
    End If

    ' RCI 02-AUG-2011: Grouped by provider or account
    Me.gb_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    Me.chk_grouped.Text = GLB_NLS_GUI_INVOICING.GetString(458)
    Me.opt_provider.Text = GLB_NLS_GUI_INVOICING.GetString(421)
    Me.opt_account.Text = GLB_NLS_GUI_INVOICING.GetString(422)
    Me.chk_subtotal.Text = GLB_NLS_GUI_INVOICING.GetString(371, GLB_NLS_GUI_INVOICING.GetString(424))
    Me.chk_show_sessions.Text = GLB_NLS_GUI_INVOICING.GetString(425)

    Me.chk_only_cdt_sessions.Visible = False
    Me.chk_without_cdt_sessions.Visible = False

    Me.lbl_handpay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1153)

    Me.uc_account_sel1.ShowMassiveSearch = True

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      Call GUI_StyleSheetPlaySessionsEdit()
    Else
      Call GUI_StyleSheet()
    End If

    If (WSI.Common.Misc.IsCashlessMode And WSI.Common.Misc.IsFeatureTerminalDrawEnabled()) Then
      Me.chk_only_cdt_sessions.Visible = True
      Me.chk_without_cdt_sessions.Visible = True
      Me.chk_only_cdt_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8706)
      Me.chk_without_cdt_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8707)

      Me.gb_options.Size = New System.Drawing.Size(255, 196)

      Me.chk_only_gap_sessions.Location = New System.Drawing.Point(6, 20)
      Me.chk_only_cdt_sessions.Location = New System.Drawing.Point(6, 42)
      Me.chk_without_cdt_sessions.Location = New System.Drawing.Point(6, 64)
      Me.chk_grouped.Location = New System.Drawing.Point(6, 86)
      Me.opt_account.Location = New System.Drawing.Point(20, 108)
      Me.opt_provider.Location = New System.Drawing.Point(20, 130)
      Me.chk_subtotal.Location = New System.Drawing.Point(20, 152)
      Me.chk_show_sessions.Location = New System.Drawing.Point(20, 174)
    End If

    ' Set filter default values
    Call SetDefaultValues()

    For Each status As Integer In System.Enum.GetValues(GetType(WSI.Common.PlaySessionStatus))
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Or Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        If status = WSI.Common.PlaySessionStatus.Closed Or _
           status = WSI.Common.PlaySessionStatus.Abandoned Or _
           status = WSI.Common.PlaySessionStatus.ManualClosed Then
          Me.cmb_status.Add(status, PlaySessionStatusName(status, Integer.MinValue))
        End If
      Else
        Me.cmb_status.Add(status, PlaySessionStatusName(status, Integer.MinValue))
      End If
    Next

    Me.cmb_status.Enabled = False

    m_point_awarded_changed = 0

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Me.gp_caption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7809)            '7809 "Caption"
    Me.tf_unbalanced.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)        '1974 "Unbalanced"
    Me.tf_handpay_session.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7804)   '7804 "Handpay Session"
    'Me.tf_promotions.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211)         ' 211 "Promotions"
    Me.tf_handpay_unpaid.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7805)    '7805 "Handpay Unpaid"

    Me.lbl_unbalanced.ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    Me.lbl_unbalanced.BackColor = GetColor(PS_COLOR_UNBALANCED)
    Me.lbl_handpay_session.ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    Me.lbl_handpay_session.BackColor = GetColor(PS_COLOR_HANDPAY_PLAYSESSION)
    'Me.lbl_promotions.ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    'Me.lbl_promotions.BackColor = GetColor(PS_COLOR_PROMOTION)
    Me.lbl_handpay_unpaid.ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    Me.lbl_handpay_unpaid.BackColor = GetColor(PS_COLOR_HANDPAY_UNPAID)

    m_is_mico2 = WSI.Common.Misc.IsMico2Mode
  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    'If m_grouped_by <> m_last_grouped_by Then
    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      Call GUI_StyleSheetPlaySessionsEdit()
    Else
      Call GUI_StyleSheet()
    End If
    'End If

    m_first_time = True
    m_count_session = 0
    m_duration_total = 0

    ResetTotalCounters(m_provider_total, "")
    ResetTotalCounters(m_terminal_total, "")
    ResetTotalCounters(m_account_total, "")

    ResetTotalCounters(m_global_total, "")
  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Select Case m_grouped_by
      Case ENUM_GROUPED.BY_PROVIDER
        If Not String.IsNullOrEmpty(m_terminal_total.key) Then
          DrawTotalRow(m_terminal_total, ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL)
        End If
        If Not String.IsNullOrEmpty(m_provider_total.key) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
        End If

      Case ENUM_GROUPED.BY_ACCOUNT
        If Not String.IsNullOrEmpty(m_provider_total.key) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
        End If
        If Not String.IsNullOrEmpty(m_account_total.key) Then
          DrawTotalRow(m_account_total, ENUM_GRID_ROW_TYPE.TOTAL_ACCOUNT)
        End If

      Case ENUM_GROUPED.NOT_GROUPED
      Case Else
    End Select

    DrawTotalRow(m_global_total, ENUM_GRID_ROW_TYPE.TOTAL)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_MaxRows() As Integer
    Return Int32.MaxValue
  End Function ' GUI_MaxRows

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    Dim _order_by As String

    _str_sql = New StringBuilder()

    ' Massive Search
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel1.CreateAndInsertAccountData())
    End If

    ' Get Select and from
    _str_sql.AppendLine(" SELECT   PS_PLAY_SESSION_ID ")
    _str_sql.AppendLine("        , PS_STARTED ")
    _str_sql.AppendLine("        , ISNULL(PS_FINISHED, GETDATE()) ")
    _str_sql.AppendLine("        , TE_NAME ")
    _str_sql.AppendLine("        , PS_TOTAL_CASH_IN ")
    _str_sql.AppendLine("        , PS_PLAYED_AMOUNT ")
    _str_sql.AppendLine("        , PS_WON_AMOUNT ")
    _str_sql.AppendLine("        , PS_PLAYED_COUNT ")
    _str_sql.AppendLine("        , PS_WON_COUNT ")
    _str_sql.AppendLine("        , PS_CASH_IN ")
    _str_sql.AppendLine("        , PS_TOTAL_CASH_OUT ")
    _str_sql.AppendLine("        , PS_STATUS ")
    _str_sql.AppendLine("        , AC_ACCOUNT_ID ")
    _str_sql.AppendLine("        , AC_TYPE ")
    _str_sql.AppendLine("        , AC_TRACK_DATA ")
    _str_sql.AppendLine("        , PS_PROMO ")
    _str_sql.AppendLine("        , AC_HOLDER_NAME ")
    _str_sql.AppendLine("        , TE_PROVIDER_ID ")
    _str_sql.AppendLine("        , DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE())) PS_DURATION ")
    _str_sql.AppendLine("        , TE_TERMINAL_TYPE ")
    _str_sql.AppendLine("        , PS_TYPE ")
    _str_sql.AppendLine("        , PS_REDEEMABLE_CASH_IN ")
    _str_sql.AppendLine("        , PS_REDEEMABLE_PLAYED ")
    _str_sql.AppendLine("        , PS_REDEEMABLE_WON ")
    _str_sql.AppendLine("        , PS_REDEEMABLE_CASH_OUT ")
    _str_sql.AppendLine("        , PS_NON_REDEEMABLE_CASH_IN ")
    _str_sql.AppendLine("        , PS_NON_REDEEMABLE_PLAYED ")
    _str_sql.AppendLine("        , PS_NON_REDEEMABLE_WON ")
    _str_sql.AppendLine("        , PS_NON_REDEEMABLE_CASH_OUT ")
    _str_sql.AppendLine("        , PS_COMPUTED_POINTS ")
    _str_sql.AppendLine("        , PS_AWARDED_POINTS ")
    _str_sql.AppendLine("        , PS_AWARDED_POINTS_STATUS ")
    _str_sql.AppendLine("        , PS_TERMINAL_ID ")

    _str_sql.AppendLine("        , PS_REDEEMABLE_PLAYED_ORIGINAL")
    _str_sql.AppendLine("        , PS_REDEEMABLE_WON_ORIGINAL")
    _str_sql.AppendLine("        , PS_NON_REDEEMABLE_PLAYED_ORIGINAL")
    _str_sql.AppendLine("        , PS_NON_REDEEMABLE_WON_ORIGINAL")
    _str_sql.AppendLine("        , PS_PLAYED_COUNT_ORIGINAL")
    _str_sql.AppendLine("        , PS_WON_COUNT_ORIGINAL")

    ' JMM 20-OCT-2016
    _str_sql.AppendLine("       , ISNULL (PS_HANDPAYS_AMOUNT, 0) PS_HANDPAYS_AMOUNT ")
    _str_sql.AppendLine("       , ISNULL (PS_HANDPAYS_PAID_AMOUNT, 0) PS_HANDPAYS_PAID_AMOUNT ")

    If Me.m_is_tito_mode Then
      _str_sql.AppendLine("        , PS_RE_TICKET_IN ")
      _str_sql.AppendLine("        , PS_PROMO_RE_TICKET_IN ")
      _str_sql.AppendLine("        , PS_BILLS_IN_AMOUNT ")
      _str_sql.AppendLine("        , PS_RE_TICKET_OUT ")
      _str_sql.AppendLine("        , PS_PROMO_NR_TICKET_IN ")
      _str_sql.AppendLine("        , PS_PROMO_NR_TICKET_OUT ")
      _str_sql.AppendLine("        , PS_AUX_FT_RE_CASH_IN ")
      _str_sql.AppendLine("        , PS_AUX_FT_NR_CASH_IN  ")
    End If

    _str_sql.AppendLine("        , PS_FOUND ")
    _str_sql.AppendLine("        , PS_REMAINING ")

    _str_sql.AppendLine("   FROM ( ")

    _str_sql.AppendLine(GetSqlWhere())

    '''str_sql = str_sql & " ) X left join terminals on PS_TERMINAL_ID=TE_TERMINAL_ID" & _
    '''                        " ORDER BY PS_STARTED DESC"

    If Not Me.rb_finish_date.Checked Then
      _order_by = "PS_STARTED DESC"
    Else
      _order_by = "PS_FINISHED DESC"
    End If

    m_last_grouped_by = m_grouped_by
    m_grouped_by = ENUM_GROUPED.NOT_GROUPED

    If Me.chk_grouped.Checked Then
      If Me.opt_provider.Checked Then

        _order_by = "TE_PROVIDER_ID, TE_NAME, AC_ACCOUNT_ID, " + _order_by
        m_grouped_by = ENUM_GROUPED.BY_PROVIDER
      ElseIf Me.opt_account.Checked Then
        _order_by = "AC_ACCOUNT_ID, TE_PROVIDER_ID, TE_NAME,  " + _order_by
        m_grouped_by = ENUM_GROUPED.BY_ACCOUNT
      End If
    End If

    _str_sql.AppendLine(" ) X ORDER BY " & _order_by)

    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      _str_sql.AppendLine(Me.uc_account_sel1.DropTableAccountMassiveSearch())
    End If

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '           We use it here to add total data before adding "normal" data.
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  '
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    If Me.chk_grouped.Checked AndAlso Me.opt_account.Checked AndAlso _
          (DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE) = AccountType.ACCOUNT_VIRTUAL_CASHIER Or _
           DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE) = AccountType.ACCOUNT_VIRTUAL_TERMINAL) Then

      Return False
    End If

    m_current_terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)

    Call ProcessCounters(DbRow)

    ' Have to show sessions?
    Return Me.chk_show_sessions.Checked Or Not Me.chk_grouped.Checked
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Get string of play session
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - String: 
  '
  Private Function PlaySessionStatusName(ByVal Status As WSI.Common.PlaySessionStatus, ByVal Type As WSI.Common.PlaySessionType) As String

    Dim _str_status As String
    Dim _int_value As Int32
    Dim _handpay As Boolean
    Dim _nls_handpay As Int16

    _handpay = False
    _nls_handpay = -1
    _str_status = ""

    Select Case Status
      Case WSI.Common.PlaySessionStatus.Opened
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(400) ' Opened
      Case WSI.Common.PlaySessionStatus.Closed
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(401) ' Closed
      Case WSI.Common.PlaySessionStatus.Abandoned
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(402) ' Abandoned
      Case WSI.Common.PlaySessionStatus.ManualClosed
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(404) ' Manual Closed
      Case WSI.Common.PlaySessionStatus.Cancelled
        _str_status = GLB_NLS_GUI_STATISTICS.GetString(406) ' Cancelled
      Case WSI.Common.PlaySessionStatus.HandpayPayment
        If Type = Integer.MinValue Then
          _str_status = GLB_NLS_GUI_STATISTICS.GetString(407) ' HandpayPayment
        Else
          _handpay = True
          _nls_handpay = 1151
        End If
      Case WSI.Common.PlaySessionStatus.HandpayCancelPayment
        If Type = Integer.MinValue Then
          _str_status = GLB_NLS_GUI_STATISTICS.GetString(408) ' HandpayCancelPayment
        Else
          _handpay = True
          _nls_handpay = 1152
        End If
      Case WSI.Common.PlaySessionStatus.DrawWinner
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2606) ' DrawWinner
      Case WSI.Common.PlaySessionStatus.DrawLoser
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2607) ' DrawLoser
      Case WSI.Common.PlaySessionStatus.ProgressiveProvision
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5410) ' Progressive provision
      Case WSI.Common.PlaySessionStatus.ProgressiveReserve
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5411) ' Progressive reserve
      Case WSI.Common.PlaySessionStatus.GameGatewayBet
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))  ' %0 - Bet
      Case WSI.Common.PlaySessionStatus.GameGatewayPrize
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))  ' %0 - Prize
      Case WSI.Common.PlaySessionStatus.GameGatewayBetRollback
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))  ' %0 - Rollback
      Case WSI.Common.PlaySessionStatus.PariPlayBet
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))  ' %0 - Bet
      Case WSI.Common.PlaySessionStatus.PariPlayPrize
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))  ' %0 - Prize
      Case WSI.Common.PlaySessionStatus.PariPlayBetRollback
        _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, GeneralParam.GetString("PariPlay", "Name", "^PariPlay"))  ' %0 - Rollback
      Case Else
        _int_value = Status
        _str_status = "(" & _int_value.ToString() & ") - " & Status.ToString()
    End Select

    If _handpay Then
      Select Case Type
        Case WSI.Common.PlaySessionType.HANDPAY_CANCELLED_CREDITS
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(320))
        Case WSI.Common.PlaySessionType.HANDPAY_JACKPOT
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(321))
        Case WSI.Common.PlaySessionType.HANDPAY_MANUAL
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(330))
        Case WSI.Common.PlaySessionType.HANDPAY_ORPHAN_CREDITS
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(322))
        Case WSI.Common.PlaySessionType.HANDPAY_SITE_JACKPOT
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, " - " & GLB_NLS_GUI_INVOICING.GetString(323))
        Case Else
          _str_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_handpay, "")
      End Select

    End If

    Return _str_status

  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean

    Dim _time As TimeSpan
    Dim _status As Integer
    Dim _str_status As String = ""
    Dim _track_data As String

    Dim _played_amount As Double
    Dim _won_amount As Double
    Dim _initial_balance As Double
    Dim _cash_in As Double
    Dim _final_balance As Double
    Dim _cash_in_play_session As Double
    Dim _is_promo As Boolean = False
    Dim _played_count As Integer
    Dim _won_count As Integer

    Dim _re_ticket_in As Double
    Dim _re_promo_ticket_in As Double
    Dim _re_bill_in As Double
    Dim _re_ticket_out As Double

    Dim _nr_promo_ticket_in As Double
    Dim _nr_promo_ticket_out As Double

    Dim _total_ticket_in As Double
    Dim _total_bills_in As Double
    Dim _total_ticket_out As Double

    Dim _handpay As Double

    Dim _remaining As Double
    Dim _found As Double
    Dim _total_remaining As Double
    Dim _total_found As Double

    _played_amount = 0
    _won_amount = 0
    _initial_balance = 0
    _cash_in_play_session = 0
    _cash_in = 0
    _final_balance = 0
    _played_count = 0
    _won_count = 0

    _re_ticket_in = 0
    _re_promo_ticket_in = 0
    _re_bill_in = 0
    _re_ticket_out = 0

    _nr_promo_ticket_in = 0
    _nr_promo_ticket_out = 0

    _total_ticket_in = 0
    _total_bills_in = 0
    _total_ticket_out = 0

    _handpay = 0

    _remaining = 0
    _found = 0
    _total_remaining = 0
    _total_found = 0

    m_count_session += 1

    ' Started 
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STARTED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_STARTED), _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Status
    _status = DbRow.Value(SQL_COLUMN_STATUS)
    _str_status = PlaySessionStatusName(_status, DbRow.Value(SQL_COLUMN_TYPE))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = _str_status

    ' Time
    _time = New TimeSpan(0, 0, DbRow.Value(SQL_COLUMN_DURATION))
    If _time.TotalSeconds > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TIME).Value = TimeSpanToString(_time)
      m_duration_total += DbRow.Value(SQL_COLUMN_DURATION)
    End If

    ' Finished 
    If _status <> WSI.Common.PlaySessionStatus.Opened And _time.TotalSeconds > 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FINISHED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_FINISHED), _
                                                                             ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                             ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Account Id and Holder Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)

    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    ' Trackdata
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_TRACKDATA) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_TRACKDATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                  CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType))
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ID).Value = _track_data

    ' Terminal Report
    For _idx As Int32 = 0 To m_current_terminal_data.Count - 1
      If Not m_current_terminal_data(_idx) Is Nothing AndAlso Not m_current_terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = m_current_terminal_data(_idx)
      End If
    Next
    m_current_terminal_data = Nothing

    ' Terminal Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Terminal Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE_NAME).Value = GetTerminalTypeNls(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE))

    '
    ' REDEEMABLE
    '

    ' Redeemable - Initial Balance
    _initial_balance = DbRow.Value(SQL_COLUMN_RE_CASH_IN)
    If Me.m_is_tito_mode And Not m_is_mico2 Then
      If Not DbRow.IsNull(SQL_COLUMN_AUX_FT_RE_CASH_IN) Then

        _initial_balance += DbRow.Value(SQL_COLUMN_AUX_FT_RE_CASH_IN)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_INITIAL_BALANCE).Value = GUI_FormatCurrency(_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redeemable - Played amount
    _played_amount = DbRow.Value(SQL_COLUMN_RE_PLAYED_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redeemable - Won Amount
    _won_amount = DbRow.Value(SQL_COLUMN_RE_WON_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_WON_AMOUNT).Value = GUI_FormatCurrency(_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redeemable - Final Balance
    _final_balance = DbRow.Value(SQL_COLUMN_RE_CASH_OUT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_FINAL_BALANCE).Value = GUI_FormatCurrency(_final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redeemable - Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_NETWIN).Value = GUI_FormatCurrency(_initial_balance - _final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redeemable - Ticket In
    If Me.m_is_tito_mode Then
      If Not DbRow.IsNull(SQL_COLUMN_RE_TICKET_IN) Then
        _re_ticket_in = DbRow.Value(SQL_COLUMN_RE_TICKET_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_TICKET_IN).Value = GUI_FormatCurrency(_re_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Redeemable - Promo Redeemable Ticket In
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_IN) Then
        _re_promo_ticket_in = DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_RE_TICKET_IN).Value = GUI_FormatCurrency(_re_promo_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Redeemable - Bill In
      If Not DbRow.IsNull(SQL_COLUMN_BILLS_IN_AMOUNT) Then
        _re_bill_in = DbRow.Value(SQL_COLUMN_BILLS_IN_AMOUNT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_IN_AMOUNT).Value = GUI_FormatCurrency(_re_bill_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Redeemable - Promo Redeemable Ticket Out
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_OUT) Then
        _re_ticket_out = DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_OUT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_TICKET_OUT).Value = GUI_FormatCurrency(_re_ticket_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End If

    '
    ' NOT REDEEMABLE
    '

    ' Not Redeemable - Initial Balance
    _initial_balance = DbRow.Value(SQL_COLUMN_NRE_CASH_IN)
    If Me.m_is_tito_mode And Not m_is_mico2 Then
      If Not DbRow.IsNull(SQL_COLUMN_AUX_FT_NR_CASH_IN) Then

        _initial_balance += DbRow.Value(SQL_COLUMN_AUX_FT_NR_CASH_IN)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_INITIAL_BALANCE).Value = GUI_FormatCurrency(_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Not Redeemable - Played amount
    _played_amount = DbRow.Value(SQL_COLUMN_NRE_PLAYED_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Not Redeemable - Won Amount
    _won_amount = DbRow.Value(SQL_COLUMN_NRE_WON_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_WON_AMOUNT).Value = GUI_FormatCurrency(_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Not Redeemable - Final Balance
    _final_balance = DbRow.Value(SQL_COLUMN_NRE_CASH_OUT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_FINAL_BALANCE).Value = GUI_FormatCurrency(_final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Not Redeemable - Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_NETWIN).Value = GUI_FormatCurrency(_initial_balance - _final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Not Redeemable - Promo NR Ticket In
    If Me.m_is_tito_mode Then
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_IN) Then
        _nr_promo_ticket_in = DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_IN)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NONRE_TICKET_IN).Value = GUI_FormatCurrency(_nr_promo_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Not Redeemable - Promo NR Ticket Out
      If Not DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_OUT) Then
        _nr_promo_ticket_out = DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_OUT)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Value = GUI_FormatCurrency(_nr_promo_ticket_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    '
    ' TOTAL
    '

    ' TOTAL - Initial Balance
    _initial_balance = DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INITIAL_BALANCE).Value = GUI_FormatCurrency(_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TOTAL - Played amount
    _played_amount = DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TOTAL - Won Amount
    _won_amount = DbRow.Value(SQL_COLUMN_WON_AMOUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TOTAL - Final Balance
    _final_balance = DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(_final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_HANDPAYS_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS_PAID_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_HANDPAYS_PAID_AMOUNT), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TOTAL - Netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_initial_balance - _final_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' HANDPAYS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS_AMOUNT).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If Me.m_is_tito_mode Then
      ' TOTAL - Ticket In
      _total_ticket_in = _re_ticket_in
      _total_ticket_in += _re_promo_ticket_in
      _total_ticket_in += _nr_promo_ticket_in
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_IN).Value = GUI_FormatCurrency(_total_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' TOTAL - Bill In
      _total_bills_in = _re_bill_in
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Value = GUI_FormatCurrency(_total_bills_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' TOTAL - Ticket Out
      _total_ticket_out = _re_ticket_out
      _total_ticket_out += _nr_promo_ticket_out
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_TICKET_OUT).Value = GUI_FormatCurrency(_total_ticket_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' TOTAL - Remaining
      If Not DbRow.IsNull(SQL_COLUMN_REMAINING_IN_EGM) Then
        _remaining = DbRow.Value(SQL_COLUMN_REMAINING_IN_EGM)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EGM_CREDIT_FINAL).Value = GUI_FormatCurrency(_remaining, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' TOTAL - Found
      If Not DbRow.IsNull(SQL_COLUMN_FOUND_IN_EGM) Then
        _found = DbRow.Value(SQL_COLUMN_FOUND_IN_EGM)
      End If
      Me.Grid.Cell(RowIndex, GRID_COLUMN_EGM_CREDIT_INITIAL).Value = GUI_FormatCurrency(_found, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Else
      ' Cash In Play Session
      _cash_in_play_session = DbRow.Value(SQL_COLUMN_CASH_IN_PLAY_SESSION)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN_PLAY_SESSION).Value = GUI_FormatCurrency(_cash_in_play_session, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Session Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_SESSION_ID)

    ' Type
    Select Case DbRow.Value(SQL_COLUMN_TYPE)
      Case WSI.Common.PlaySessionType.PROGRESSIVE_PROVISION, _
           WSI.Common.PlaySessionType.PROGRESSIVE_RESERVE

        ' Set Colors
        Call SetRowBackColor(RowIndex, PS_COLOR_HANDPAY_PLAYSESSION)
      Case Else

        If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment _
            Or DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayCancelPayment Then
          ' Set Colors
          Call SetRowBackColor(RowIndex, PS_COLOR_HANDPAY_PLAYSESSION)

          ' Set Values
          If DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment Then
            _handpay = DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)
            If (DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_CANCELLED_CREDITS Or DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_MANUAL) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
              Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            End If
          Else
            _handpay = -DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)
            If (DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_CANCELLED_CREDITS Or DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_MANUAL) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_INITIAL_BALANCE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
              Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
            End If
          End If
          Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS_AMOUNT).Value = GUI_FormatCurrency(_handpay, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        Else

          Dim _total As Decimal

          _total = _played_amount - _won_amount

          If Math.Round(_total, 2) <> Math.Round((_initial_balance - DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) Then
            ' Set Colors
            Call SetRowBackColor(RowIndex, PS_COLOR_UNBALANCED)
          Else
            If DbRow.Value(SQL_COLUMN_HANDPAYS_AMOUNT) <> DbRow.Value(SQL_COLUMN_HANDPAYS_PAID_AMOUNT) Then
              Call SetRowBackColor(RowIndex, PS_COLOR_HANDPAY_UNPAID)
            End If
          End If

          ' Promotional
          _is_promo = DbRow.Value(SQL_COLUMN_PROMOTIONAL)

          If _is_promo Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTIONAL).Value = GLB_NLS_GUI_STATISTICS.GetString(392)
            ' Set Colors
            'Call SetRowBackColor(RowIndex, PS_COLOR_PROMOTION)
            'Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
          Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTIONAL).Value = ""
          End If

        End If
    End Select

    ' Points
    If Not DbRow.IsNull(SQL_COLUMN_COMPUTED_POINTS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COMPUTED_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_COMPUTED_POINTS))
      If DbRow.Value(SQL_COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.Pending Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COMPUTED_POINTS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COMPUTED_POINTS).Value = ""
    End If
    If Not DbRow.IsNull(SQL_COLUMN_AWARDED_POINTS) Then
      If DbRow.Value(SQL_COLUMN_AWARDED_POINTS_STATUS) = AwardPointsStatus.Pending Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS).Value = ""
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AWARDED_POINTS))
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AWARDED_POINTS))
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value = ""
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS_STATUS).Value = DbRow.Value(SQL_COLUMN_AWARDED_POINTS_STATUS)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value = DbRow.Value(SQL_COLUMN_AWARDED_POINTS_STATUS)

    ' Played Count
    _played_count = DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(_played_count, 0)

    ' Won Count
    _won_count = DbRow.Value(SQL_COLUMN_WON_COUNT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(_won_count, 0)

    ' DHA 17-MAR-2014
    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      ' RE Played Original
      If Not DbRow.IsNull(SQL_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' RE Won Original
      If Not DbRow.IsNull(SQL_COLUMN_RE_WON_AMOUNT_ORIGINAL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RE_WON_AMOUNT_ORIGINAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' NR Played Original
      If Not DbRow.IsNull(SQL_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' NR Won Original
      If Not DbRow.IsNull(SQL_COLUMN_NRE_WON_AMOUNT_ORIGINAL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NRE_WON_AMOUNT_ORIGINAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If

      ' Count Played Original
      If Not DbRow.IsNull(SQL_COLUMN_PLAYED_COUNT_ORIGINAL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PLAYED_COUNT_ORIGINAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      End If

      ' Count Won Original
      If Not DbRow.IsNull(SQL_COLUMN_WON_COUNT_ORIGINAL) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_WON_COUNT_ORIGINAL).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_WON_COUNT_ORIGINAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      End If

    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)
    Dim _holder_name As String
    Dim _track_data As String
    Dim _account_id As String
    Dim _handpay_amount As Decimal
    Dim _handpay_paid_amount As Decimal
    Dim _aux_handpay_amount As String
    Dim _aux_handpay_paid_amount As String

    If RowIndex < 0 Or RowIndex >= (Me.Grid.NumRows - 1) Then
      Return
    End If

    ' 27-DEC-2013  RRR    Added card ID to ToolTip in TITO Mode
    _holder_name = Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value
    _track_data = Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ID).Value
    '03-FEB-2014 JBC If card is recycled, tooltip doesn't show card number
    If _track_data.Contains(STR_RECYCLED_CARD) Then
      _track_data = String.Empty
    End If
    _account_id = Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value

    If String.IsNullOrEmpty(_account_id) Then
      Return
    End If

    If ColIndex = GRID_COLUMN_HOLDER_NAME Or ColIndex = GRID_COLUMN_ACCOUNT_ID Then
      ToolTipTxt = GLB_NLS_GUI_STATISTICS.GetString(397, _account_id, _track_data, _holder_name)
    End If

    If ColIndex = GRID_COLUMN_FINAL_BALANCE Then
      _handpay_amount = Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS_AMOUNT).Value
      _handpay_paid_amount = Me.Grid.Cell(RowIndex, GRID_COLUMN_HANDPAYS_PAID_AMOUNT).Value

      If _handpay_amount + _handpay_paid_amount > 0 Then
        _aux_handpay_amount = GUI_FormatCurrency(_handpay_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        _aux_handpay_paid_amount = GUI_FormatCurrency(_handpay_paid_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        ToolTipTxt = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7810, _aux_handpay_amount, _aux_handpay_paid_amount) '7810 "Handpay: %1 - Paid: %2"
      End If

    End If

  End Sub ' GUI_SetToolTipText

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_GRID_INFO
        Call GUI_ShowSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_DATA Then
          Call GUI_ShowSelectedItem()
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call ClosePlaySession()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        Call ViewLoggerPlaySession()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2
        If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
          Call SavePointsChanges()
        ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
          Call SaveAmountsChanges()
        End If


      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3
        Call PlaySessionData()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        ' If pending changes MsgBox
        If (Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Or Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT) AndAlso _
           PendingChanges() Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Return
          End If
        End If
        Call MyBase.GUI_ButtonClick(ButtonId)
        If Not Me.IsDisposed Then
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (Me.Grid.NumRows > 0)
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (Me.Grid.NumRows > 0)
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (Me.Grid.NumRows > 0) And (m_permission.Write)
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Enabled = (Me.Grid.NumRows > 0)
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
        ' 01-OCT-2013  JPJ While closing the form the Grid is not accesible
        If Not Me.IsDisposed Then
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (Me.Grid.NumRows > 0)
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (Me.Grid.NumRows > 0)
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = (Me.Grid.NumRows > 0) And (m_permission.Write)
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Enabled = (Me.Grid.NumRows > 0)
        End If
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Open additional form to show details for the select row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ShowSelectedItem()
    Dim idx_row As Int32
    Dim session_id As Integer
    Dim frm_sel As frm_plays_plays

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then

        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return
    End If

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    frm_sel = New frm_plays_plays

    ' Get the complete session ID and session date and launch select form
    If Me.m_is_tito_mode Then
      If Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
        session_id = 0
      Else
        session_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value
      End If

      frm_sel.ShowForEdit(Me.MdiParent, _
                          session_id, _
                          Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL_TYPE).Value, _
                          Me.Grid.Cell(idx_row, GRID_COLUMN_STARTED).Value, _
                          Me.Grid.Cell(idx_row, GRID_COLUMN_INITIAL_BALANCE).Value)
    Else
      If Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
        session_id = 0
      Else
        session_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value
      End If

      frm_sel.ShowForEdit(Me.MdiParent, _
                          session_id, _
                          Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL_TYPE).Value, _
                          Me.Grid.Cell(idx_row, GRID_COLUMN_STARTED).Value, _
                          Me.Grid.Cell(idx_row, GRID_COLUMN_INITIAL_BALANCE).Value)
    End If

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub ' GUI_EditSelectedItem

#End Region  ' Overrides

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_WON_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_RE_NETWIN).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).IsColumnPrintable = False
      .Column(GRID_COLUMN_NRE_NETWIN).IsColumnPrintable = False

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set form specific requirements/parameters for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_EXCEL_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_WON_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_RE_NETWIN).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).IsColumnPrintable = True
      .Column(GRID_COLUMN_NRE_NETWIN).IsColumnPrintable = True

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _info As String()
    Dim _info0 As String
    Dim _info1 As String

    _info = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1817).Split(":")
    _info0 = "* "
    _info1 = _info(0)
    If _info.Length > 1 Then
      _info0 = _info(0)
      _info1 = _info(1)
    End If

    If m_date_open_sessions Then
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261), GLB_NLS_GUI_STATISTICS.GetString(345))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261), GLB_NLS_GUI_CONTROLS.GetString(331) & " - " & m_date_filter)
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(394), m_status)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_card_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_card_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_card_holder_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(205), m_points_value)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2799), m_columns_value)
    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_FULL_DATA Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(205), m_points_value)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options_value)
    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      PrintData.SetFilter("", "", True)
      PrintData.SetFilter("", "", True)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options_value)
      PrintData.SetFilter("", "", True)
    End If

    If Not m_is_tito_mode Then
      PrintData.SetFilter(_info0, _info1)
    End If

    PrintData.FilterValueWidth(1) = 1800
    PrintData.FilterHeaderWidth(2) = 800
    PrintData.FilterValueWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 1000
    PrintData.FilterValueWidth(3) = 2500
    PrintData.FilterHeaderWidth(4) = 2000
    PrintData.FilterValueWidth(4) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_filter = ""
    m_date_open_sessions = False
    m_date_to = ""
    m_status = ""
    m_terminals = ""
    m_card_account = ""
    m_card_track_data = ""
    m_card_holder_name = ""
    m_holder_is_vip = ""
    m_options_value = ""
    m_points_value = ""
    m_columns_value = ""

    'Date 
    m_date_open_sessions = Me.rb_open_sessions.Checked
    If Me.rb_ini_date.Checked Then
      m_date_filter = GLB_NLS_GUI_STATISTICS.GetString(381)
    End If
    If Me.rb_finish_date.Checked Then
      m_date_filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704)
    End If
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Status
    If Me.opt_all_status.Checked Then
      m_status = Me.opt_all_status.Text
    Else
      m_status = Me.cmb_status.TextValue
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Account
    m_card_account = Me.uc_account_sel1.Account()
    m_card_track_data = Me.uc_account_sel1.TrackData()
    m_card_holder_name = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    ' Options
    ' Only Gap Sessions
    m_options_value = chk_only_gap_sessions.Text & ": "
    If chk_only_gap_sessions.Checked Then
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If (WSI.Common.Misc.IsCashlessMode And WSI.Common.Misc.IsFeatureTerminalDrawEnabled()) Then
      m_options_value = m_options_value & ", " & chk_only_cdt_sessions.Text & ": "
      If chk_only_cdt_sessions.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If

      m_options_value = m_options_value & ", " & chk_without_cdt_sessions.Text & ": "
      If chk_without_cdt_sessions.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    End If

    ' Grouped
    m_options_value = m_options_value & ", " & chk_grouped.Text & ": "
    If Me.chk_grouped.Checked Then
      ' By provider or account
      If Me.opt_provider.Checked Then
        m_options_value = m_options_value & Me.opt_provider.Text
      ElseIf Me.opt_account.Checked Then
        m_options_value = m_options_value & Me.opt_account.Text
      End If
      ' Show subtotal
      m_options_value = m_options_value & ", " & chk_subtotal.Text & ": "
      If chk_subtotal.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
      ' Show sessions
      m_options_value = m_options_value & ", " & chk_show_sessions.Text & ": "
      If chk_show_sessions.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Points
    ' Only Pending Points
    m_points_value = chk_only_pending_points.Text & ": "
    If chk_only_pending_points.Checked Then
      m_points_value = m_points_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_points_value = m_points_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If
    ' Only Manual Points
    m_points_value = m_points_value & ", " & chk_only_manual_points.Text & ": "
    If chk_only_manual_points.Checked Then
      m_points_value = m_points_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_points_value = m_points_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Columns
    ' RE
    m_columns_value = chk_view_column_re.Text & ": "
    If chk_view_column_re.Checked Then
      m_columns_value = m_columns_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_columns_value = m_columns_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If
    ' NR
    m_columns_value = m_columns_value & ", " & chk_view_column_nr.Text & ": "
    If chk_view_column_nr.Checked Then
      m_columns_value = m_columns_value & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_columns_value = m_columns_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:+
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Check if changes have been made in the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Changes have been made. False: Otherwise
  '
  Private Function PendingChanges() As Boolean

    Dim _idx_row As Integer

    For _idx_row = 0 To Grid.NumRows - 1
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
        If Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value <> _
            Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value And _
            IsValidDataRow(_idx_row) Then

          Return True
        End If
      ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        If Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value <> "" And _
          Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value <> _
          Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value And _
          IsValidDataRow(_idx_row) Then
          Return True
        ElseIf Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value <> "" And _
          Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value <> _
          Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value And _
          IsValidDataRow(_idx_row) Then
          Return True
        ElseIf Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value <> "" And _
          Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value <> _
          Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value And _
          IsValidDataRow(_idx_row) Then
          Return True
        ElseIf Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value <> "" And _
          Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value <> _
          Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value And _
          IsValidDataRow(_idx_row) Then
          Return True
        ElseIf Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value <> "" And _
          Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value <> _
          Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value And _
          IsValidDataRow(_idx_row) Then
          Return True
        ElseIf Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value <> "" And _
          Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value <> _
          Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value And _
          IsValidDataRow(_idx_row) Then
          Return True
        End If
      End If

    Next

    Return False

  End Function ' PendingChanges

  'End Function ' PendingChanges

  ' PURPOSE: Ask user to discard changes or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Discard changes. False: Otherwise
  '
  Private Function DiscardChanges() As Boolean

    ' If pending changes MsgBox
    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Or Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      If PendingChanges() Then
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101) _
                    , ENUM_MB_TYPE.MB_TYPE_WARNING _
                    , ENUM_MB_BTN.MB_BTN_YES_NO _
                    , ENUM_MB_DEF_BTN.MB_DEF_BTN_2) _
                    = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return False
        End If
      End If
    End If

    Return True

  End Function ' DiscardChanges

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Call SetColumnsIndex()

    With Me.Grid
      Call .Init(m_number_of_columns, GRID_HEADER_ROWS, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0

      ' Started date
      .Column(GRID_COLUMN_STARTED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STARTED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(381)
      .Column(GRID_COLUMN_STARTED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_STARTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(394)
      .Column(GRID_COLUMN_STATUS).Width = 2300
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Time
      .Column(GRID_COLUMN_TIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_TIME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(382)
      .Column(GRID_COLUMN_TIME).Width = GRID_WIDTH_TIME
      .Column(GRID_COLUMN_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Finish date
      .Column(GRID_COLUMN_FINISHED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_FINISHED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704)
      .Column(GRID_COLUMN_FINISHED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_FINISHED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1150
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_ID
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Card Id
      .Column(GRID_COLUMN_CARD_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_CARD_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_CARD_ID).Width = 0

      '  Terminal Report
      For _idx As Int32 = 0 To m_terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = m_terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = m_terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = m_terminal_columns(_idx).Alignment
      Next

      '  Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = 0

      '  Terminal Type Name
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1113)
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Width = 0 'GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Redeemable Initial Balance
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3136)     'Total In
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Played Amount
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Redeemable Won Amount
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_RE_WON_AMOUNT).Editable = True
        .Column(GRID_COLUMN_RE_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_RE_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Redeemable Final Balance
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)         'Total Out
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Netwin
      .Column(GRID_COLUMN_RE_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_RE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Ticket In
      If Me.m_is_tito_mode Then
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2921)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Redeemable Promotional Ticket In
        .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2922)
        .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Bill In
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Redeemable Ticket Out
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2924)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

      If Me.chk_view_column_re.Checked Then
        .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_NETWIN).Width = GRID_WIDTH_AMOUNT
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
          .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Width = GRID_WIDTH_COLUMN_PROMO_TICKET
          .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        End If
      Else
        .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Width = 0
        .Column(GRID_COLUMN_RE_WON_AMOUNT).Width = 0
        .Column(GRID_COLUMN_RE_FINAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_RE_NETWIN).Width = 0
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Width = 0
          .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Width = 0
          .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Width = 0
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Width = 0
        End If
      End If

      ' Not Redeemable Initial Balance
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3136)    'Total In
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Played Amount
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Not Redeemable Won Amount
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).Editable = True
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Not Redeemable Final Balance
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)    'Total Out
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Netwin
      .Column(GRID_COLUMN_NRE_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NRE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If Me.m_is_tito_mode Then
        ' Not Redeemable Promo Ticket In
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2922)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Not Redeemable Promo Ticket Out
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2925)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        ' Cash In on Play Session
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(387)
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Width = 1350
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

      If Me.chk_view_column_nr.Checked Then
        .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_NRE_NETWIN).Width = GRID_WIDTH_AMOUNT
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Width = GRID_WIDTH_COLUMN_PROMO_TICKET
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Width = GRID_WIDTH_COLUMN_PROMO_TICKET
        End If
      Else
        .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Width = 0
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).Width = 0
        .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_NRE_NETWIN).Width = 0
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Width = 0
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Width = 0
        End If
      End If

      ' Total Initial Balance
      .Column(GRID_COLUMN_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3136)       ' Total In  

      .Column(GRID_COLUMN_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Total Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_WON_AMOUNT).Editable = True
        .Column(GRID_COLUMN_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Total Final Balance
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)    'Total Out
      .Column(GRID_COLUMN_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Handpay amount
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834)
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Handpay paid amount
      .Column(GRID_COLUMN_HANDPAYS_PAID_AMOUNT).Width = 0

      ' Total Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Ticket In
      If Me.m_is_tito_mode Then
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2921)
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Bill In
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Total Ticket Out
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2924)
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Found
        .Column(GRID_COLUMN_EGM_CREDIT_INITIAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9123)
        .Column(GRID_COLUMN_EGM_CREDIT_INITIAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9121)
        .Column(GRID_COLUMN_EGM_CREDIT_INITIAL).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_EGM_CREDIT_INITIAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Remaining
        .Column(GRID_COLUMN_EGM_CREDIT_FINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9123)
        .Column(GRID_COLUMN_EGM_CREDIT_FINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9120)
        .Column(GRID_COLUMN_EGM_CREDIT_FINAL).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_EGM_CREDIT_FINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      End If

      ' Promotional
      .Column(GRID_COLUMN_PROMOTIONAL).Header(0).Text = "" ' GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PROMOTIONAL).Header(1).Text = "" ' GLB_NLS_GUI_STATISTICS.GetString(392)
      .Column(GRID_COLUMN_PROMOTIONAL).Width = 0 ' GRID_WIDTH_PROMO
      .Column(GRID_COLUMN_PROMOTIONAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Computed Points
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Width = 1250
      .Column(GRID_COLUMN_COMPUTED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Awarded Points
      .Column(GRID_COLUMN_AWARDED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_AWARDED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684)
      .Column(GRID_COLUMN_AWARDED_POINTS).Width = 1250
      .Column(GRID_COLUMN_AWARDED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
        .Column(GRID_COLUMN_AWARDED_POINTS).Editable = True
        .Column(GRID_COLUMN_AWARDED_POINTS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_AWARDED_POINTS).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 2)
      End If

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = GRID_WIDTH_PLAYS
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_PLAYED_COUNT).Editable = True
        .Column(GRID_COLUMN_PLAYED_COUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_COUNT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 0)
      End If

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = GRID_WIDTH_PLAYS
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_WON_COUNT).Editable = True
        .Column(GRID_COLUMN_WON_COUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_WON_COUNT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 0)
      End If

      ' DHA 17-MAR-2014
      ' Awarded Points Original
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).IsColumnPrintable = False

      ' Awarded Points Status
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Header(1).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Width = 0
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).IsColumnPrintable = False

      ' Awarded Points Status Original
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).IsColumnPrintable = False

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      ' RE Played Original
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' RE Won Original
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' NR Played Original
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' NR Won Original
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' Played Count Original
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).IsColumnPrintable = False

      ' Won Count Original
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).IsColumnPrintable = False

      ' DHA 17-MAR-2014
      ' RE Played Old
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).IsColumnPrintable = False

      ' RE Won Old
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).IsColumnPrintable = False

      ' NR Played Old
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).IsColumnPrintable = False

      ' NR Won Old
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).IsColumnPrintable = False

      ' Played Count Old
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).Width = 0
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).IsColumnPrintable = False

      ' Won Count Old
      .Column(GRID_COLUMN_WON_COUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_OLD).Width = 0
      .Column(GRID_COLUMN_WON_COUNT_OLD).IsColumnPrintable = False

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetPlaySessionsEdit()

    Call SetColumnsIndex()

    With Me.Grid
      Call .Init(m_number_of_columns, GRID_HEADER_ROWS, True)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Session Id
      .Column(GRID_COLUMN_SESSION_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_SESSION_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_SESSION_ID).Width = 0

      ' Started date
      .Column(GRID_COLUMN_STARTED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STARTED).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(381)
      .Column(GRID_COLUMN_STARTED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_STARTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(394)
      .Column(GRID_COLUMN_STATUS).Width = 2300
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Time
      .Column(GRID_COLUMN_TIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_TIME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(382)
      .Column(GRID_COLUMN_TIME).Width = GRID_WIDTH_TIME
      .Column(GRID_COLUMN_TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Finish date
      .Column(GRID_COLUMN_FINISHED).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(393)
      .Column(GRID_COLUMN_FINISHED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704)
      .Column(GRID_COLUMN_FINISHED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_FINISHED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1150
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_ID
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Card Id
      .Column(GRID_COLUMN_CARD_ID).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_CARD_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_CARD_ID).Width = 0

      '  Terminal Report
      For _idx As Int32 = 0 To m_terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = m_terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = m_terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = m_terminal_columns(_idx).Alignment
      Next

      '  Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = 0

      '  Terminal Type Name
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1113)
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Width = 0 'GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Redeemable Initial Balance
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1905) ' Initial*
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Initial Balance
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1905) ' Initial*
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Initial Balance
      .Column(GRID_COLUMN_INITIAL_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1905) ' Initial*
      .Column(GRID_COLUMN_INITIAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_INITIAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Played Amount
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Not Redeemable Played Amount
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Total Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_PLAYED_AMOUNT).Editable = True
        .Column(GRID_COLUMN_PLAYED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(304)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = GRID_WIDTH_PLAYS
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_PLAYED_COUNT).Editable = True
        .Column(GRID_COLUMN_PLAYED_COUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PLAYED_COUNT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 0)
      End If

      ' Redeemable Won Amount
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_RE_WON_AMOUNT).Editable = True
        .Column(GRID_COLUMN_RE_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_RE_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Not Redeemable Won Amount
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).Editable = True
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Total Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_WON_AMOUNT).Editable = True
        .Column(GRID_COLUMN_WON_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_WON_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 7, 2)
      End If

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(305)
      .Column(GRID_COLUMN_WON_COUNT).Width = GRID_WIDTH_PLAYS
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' DHA 17-MAR-2014
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        .Column(GRID_COLUMN_WON_COUNT).Editable = True
        .Column(GRID_COLUMN_WON_COUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_WON_COUNT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 0)
      End If


      ' Redeemable Final Balance
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Final Balance
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Final Balance
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(389)
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Handpay amount
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1834)
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_HANDPAYS_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Handpay paid amount
      .Column(GRID_COLUMN_HANDPAYS_PAID_AMOUNT).Width = 0

      ' Redeemable Netwin
      .Column(GRID_COLUMN_RE_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_RE_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_RE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Not Redeemable Netwin
      .Column(GRID_COLUMN_NRE_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NRE_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      .Column(GRID_COLUMN_NRE_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(390)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable Ticket In
      If Me.m_is_tito_mode Then
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2921)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Redeemable Promotional Ticket In
        .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2922)
        .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Bill In
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Redeemable Ticket Out
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2924)
        .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

      If Me.chk_view_column_re.Checked Then
        .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT
        .Column(GRID_COLUMN_RE_NETWIN).Width = GRID_WIDTH_AMOUNT
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
          .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Width = GRID_WIDTH_COLUMN_PROMO_TICKET
          .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        End If
      Else
        .Column(GRID_COLUMN_RE_INITIAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_RE_PLAYED_AMOUNT).Width = 0
        .Column(GRID_COLUMN_RE_WON_AMOUNT).Width = 0
        .Column(GRID_COLUMN_RE_FINAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_RE_NETWIN).Width = 0
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_IN).Width = 0
          .Column(GRID_COLUMN_PROMO_RE_TICKET_IN).Width = 0
          .Column(GRID_COLUMN_BILLS_IN_AMOUNT).Width = 0
          .Column(GRID_COLUMN_REDEEMABLE_TICKET_OUT).Width = 0
        End If
      End If


      If Me.m_is_tito_mode Then
        ' Not Redeemable Promo Ticket In
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2922)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Not Redeemable Promo Ticket Out
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(338)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2925)
        .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        ' Cash In on Play Session
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(387)
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Width = 1350
        .Column(GRID_COLUMN_CASH_IN_PLAY_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

      If Me.chk_view_column_nr.Checked Then
        .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Width = GRID_WIDTH_AMOUNT_NON_REDEEMABLE
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Width = GRID_WIDTH_AMOUNT_NON_REDEEMABLE
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).Width = GRID_WIDTH_AMOUNT_NON_REDEEMABLE
        .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Width = GRID_WIDTH_AMOUNT_NON_REDEEMABLE
        .Column(GRID_COLUMN_NRE_NETWIN).Width = GRID_WIDTH_AMOUNT_NON_REDEEMABLE
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Width = GRID_WIDTH_COLUMN_PROMO_TICKET
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Width = GRID_WIDTH_COLUMN_PROMO_TICKET
        End If
      Else
        .Column(GRID_COLUMN_NRE_INITIAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT).Width = 0
        .Column(GRID_COLUMN_NRE_WON_AMOUNT).Width = 0
        .Column(GRID_COLUMN_NRE_FINAL_BALANCE).Width = 0
        .Column(GRID_COLUMN_NRE_NETWIN).Width = 0
        If Me.m_is_tito_mode Then
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_IN).Width = 0
          .Column(GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Width = 0
        End If
      End If

      ' Total Ticket In
      If Me.m_is_tito_mode Then
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2921)
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        .Column(GRID_COLUMN_TOTAL_TICKET_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Bill In
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1570)
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        .Column(GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Total Ticket Out
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(391)
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2924)
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Width = GRID_WIDTH_COLUMN_TICKETS_DEFAULT
        .Column(GRID_COLUMN_TOTAL_TICKET_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

      ' Promotional
      .Column(GRID_COLUMN_PROMOTIONAL).Header(0).Text = "" ' GLB_NLS_GUI_STATISTICS.GetString(391)
      .Column(GRID_COLUMN_PROMOTIONAL).Header(1).Text = "" ' GLB_NLS_GUI_STATISTICS.GetString(392)
      .Column(GRID_COLUMN_PROMOTIONAL).Width = 0 ' GRID_WIDTH_PROMO
      .Column(GRID_COLUMN_PROMOTIONAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Computed Points
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683)
      .Column(GRID_COLUMN_COMPUTED_POINTS).Width = 1250
      .Column(GRID_COLUMN_COMPUTED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Awarded Points
      .Column(GRID_COLUMN_AWARDED_POINTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)
      .Column(GRID_COLUMN_AWARDED_POINTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684)
      .Column(GRID_COLUMN_AWARDED_POINTS).Width = 1250
      .Column(GRID_COLUMN_AWARDED_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
        .Column(GRID_COLUMN_AWARDED_POINTS).Editable = True
        .Column(GRID_COLUMN_AWARDED_POINTS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_AWARDED_POINTS).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7, 2)
      End If

      ' DHA 17-MAR-2014
      ' Awarded Points Original
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_AWARDED_POINTS_ORIGINAL).IsColumnPrintable = False

      ' Awarded Points Status
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Header(1).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).Width = 0
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS).IsColumnPrintable = False

      ' Awarded Points Status Original
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).IsColumnPrintable = False

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      ' RE Played Original
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' RE Won Original
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' NR Played Original
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' NR Won Original
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL).IsColumnPrintable = False

      ' Played Count Original
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_PLAYED_COUNT_ORIGINAL).IsColumnPrintable = False

      ' Won Count Original
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).Header(0).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).Header(1).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_WON_COUNT_ORIGINAL).IsColumnPrintable = False

      ' DHA 17-MAR-2014
      ' RE Played Old
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).IsColumnPrintable = False

      ' RE Won Old
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_RE_WON_AMOUNT_OLD).IsColumnPrintable = False

      ' NR Played Old
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).IsColumnPrintable = False

      ' NR Won Old
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).Width = 0
      .Column(GRID_COLUMN_NRE_WON_AMOUNT_OLD).IsColumnPrintable = False

      ' Played Count Old
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).Width = 0
      .Column(GRID_COLUMN_PLAYED_COUNT_OLD).IsColumnPrintable = False

      ' Won Count Old
      .Column(GRID_COLUMN_WON_COUNT_OLD).Header(0).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_OLD).Header(1).Text = ""
      .Column(GRID_COLUMN_WON_COUNT_OLD).Width = 0
      .Column(GRID_COLUMN_WON_COUNT_OLD).IsColumnPrintable = False

    End With

  End Sub ' GUI_StyleSheetPlaySessionsEdit

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    Me.dtp_to.Value = _today_opening.AddDays(1)
    Me.dtp_to.Checked = True

    Me.dtp_from.Value = _today_opening
    Me.dtp_from.Checked = True

    Me.rb_historic.Checked = True
    Me.rb_finish_date.Checked = True
    Me.rb_open_sessions.Checked = False
    Me.rb_ini_date.Checked = False
    Me.dtp_from.Enabled = True
    Me.dtp_to.Enabled = True
    Me.gb_status.Enabled = True

    Me.cmb_status.Enabled = False
    Me.opt_all_status.Checked = True

    Me.chk_only_gap_sessions.Checked = False
    Me.chk_only_pending_points.Checked = False
    Me.chk_only_manual_points.Checked = False
    Me.chk_only_cdt_sessions.Checked = False
    Me.chk_without_cdt_sessions.Checked = False

    Me.opt_account.Checked = True

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
      Me.chk_view_column_nr.Checked = False
      Me.chk_view_column_re.Checked = False
      Me.chk_grouped.Checked = True
      Me.chk_subtotal.Checked = False
      Me.chk_show_sessions.Checked = True
    Else
      Me.chk_grouped.Checked = False
      Me.chk_subtotal.Checked = True
      Me.chk_show_sessions.Checked = False
    End If

    chk_grouped_CheckedChanged(Nothing, Nothing)

    m_grouped_by = ENUM_GROUPED.NOT_GROUPED
    m_last_grouped_by = ENUM_GROUPED.NOT_GROUPED

    Me.uc_account_sel1.Clear()

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  Private Sub SetGridTitoColumns()

    Call GridColumnReIndex()

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then

      GRID_COLUMN_RE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 8
      GRID_COLUMN_NRE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 9
      GRID_COLUMN_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 10

      GRID_COLUMN_RE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 11
      GRID_COLUMN_NRE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 12
      GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 13
      GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 14

      GRID_COLUMN_RE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 15
      GRID_COLUMN_NRE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 16
      GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 17
      GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 18

      GRID_COLUMN_RE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 19
      GRID_COLUMN_NRE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 20
      GRID_COLUMN_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 21

      GRID_COLUMN_RE_NETWIN = TERMINAL_DATA_COLUMNS + 22
      GRID_COLUMN_NRE_NETWIN = TERMINAL_DATA_COLUMNS + 23
      GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 24

      GRID_COLUMN_HANDPAYS_AMOUNT = TERMINAL_DATA_COLUMNS + 25
      GRID_COLUMN_HANDPAYS_PAID_AMOUNT = TERMINAL_DATA_COLUMNS + 26

      GRID_COLUMN_REDEEMABLE_TICKET_IN = TERMINAL_DATA_COLUMNS + 27
      GRID_COLUMN_PROMO_RE_TICKET_IN = TERMINAL_DATA_COLUMNS + 28
      GRID_COLUMN_BILLS_IN_AMOUNT = TERMINAL_DATA_COLUMNS + 29
      GRID_COLUMN_REDEEMABLE_TICKET_OUT = TERMINAL_DATA_COLUMNS + 30

      GRID_COLUMN_PROMO_NONRE_TICKET_IN = TERMINAL_DATA_COLUMNS + 31
      GRID_COLUMN_PROMO_NONRE_TICKET_OUT = TERMINAL_DATA_COLUMNS + 32

      GRID_COLUMN_TOTAL_TICKET_IN = TERMINAL_DATA_COLUMNS + 33
      GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL = TERMINAL_DATA_COLUMNS + 34
      GRID_COLUMN_TOTAL_TICKET_OUT = TERMINAL_DATA_COLUMNS + 35

      GRID_COLUMN_EGM_CREDIT_INITIAL = TERMINAL_DATA_COLUMNS + 36
      GRID_COLUMN_EGM_CREDIT_FINAL = TERMINAL_DATA_COLUMNS + 37

      GRID_COLUMN_SESSION_ID = TERMINAL_DATA_COLUMNS + 38
      GRID_COLUMN_CARD_ID = TERMINAL_DATA_COLUMNS + 39
      GRID_COLUMN_TERMINAL_TYPE = TERMINAL_DATA_COLUMNS + 40
      GRID_COLUMN_COMPUTED_POINTS = TERMINAL_DATA_COLUMNS + 41
      GRID_COLUMN_AWARDED_POINTS = TERMINAL_DATA_COLUMNS + 42

      GRID_COLUMN_PROMOTIONAL = TERMINAL_DATA_COLUMNS + 43
      GRID_COLUMN_AWARDED_POINTS_ORIGINAL = TERMINAL_DATA_COLUMNS + 44
      GRID_COLUMN_AWARDED_POINTS_STATUS = TERMINAL_DATA_COLUMNS + 45
      GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL = TERMINAL_DATA_COLUMNS + 46
      GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 47
      GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 48
      GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 49
      GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 50
      GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 51
      GRID_COLUMN_PLAYED_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 52
      GRID_COLUMN_WON_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 53
      GRID_COLUMN_RE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 54
      GRID_COLUMN_RE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 55
      GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 56
      GRID_COLUMN_NRE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 57
      GRID_COLUMN_PLAYED_COUNT_OLD = TERMINAL_DATA_COLUMNS + 58
      GRID_COLUMN_WON_COUNT_OLD = TERMINAL_DATA_COLUMNS + 59

    Else

      GRID_COLUMN_RE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 8
      GRID_COLUMN_RE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 9
      GRID_COLUMN_RE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 10
      GRID_COLUMN_RE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 11
      GRID_COLUMN_RE_NETWIN = TERMINAL_DATA_COLUMNS + 12
      GRID_COLUMN_REDEEMABLE_TICKET_IN = TERMINAL_DATA_COLUMNS + 13
      GRID_COLUMN_PROMO_RE_TICKET_IN = TERMINAL_DATA_COLUMNS + 14
      GRID_COLUMN_BILLS_IN_AMOUNT = TERMINAL_DATA_COLUMNS + 15
      GRID_COLUMN_REDEEMABLE_TICKET_OUT = TERMINAL_DATA_COLUMNS + 16
      GRID_COLUMN_NRE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 17
      GRID_COLUMN_NRE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 18
      GRID_COLUMN_NRE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 19
      GRID_COLUMN_NRE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 20
      GRID_COLUMN_NRE_NETWIN = TERMINAL_DATA_COLUMNS + 21
      GRID_COLUMN_PROMO_NONRE_TICKET_IN = TERMINAL_DATA_COLUMNS + 22
      GRID_COLUMN_PROMO_NONRE_TICKET_OUT = TERMINAL_DATA_COLUMNS + 23
      GRID_COLUMN_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 24
      GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 25
      GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 26
      GRID_COLUMN_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 27
      GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 28
      GRID_COLUMN_HANDPAYS_AMOUNT = TERMINAL_DATA_COLUMNS + 29
      GRID_COLUMN_HANDPAYS_PAID_AMOUNT = TERMINAL_DATA_COLUMNS + 30
      GRID_COLUMN_TOTAL_TICKET_IN = TERMINAL_DATA_COLUMNS + 31
      GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL = TERMINAL_DATA_COLUMNS + 32
      GRID_COLUMN_TOTAL_TICKET_OUT = TERMINAL_DATA_COLUMNS + 33

      GRID_COLUMN_EGM_CREDIT_INITIAL = TERMINAL_DATA_COLUMNS + 34
      GRID_COLUMN_EGM_CREDIT_FINAL = TERMINAL_DATA_COLUMNS + 35

      GRID_COLUMN_SESSION_ID = TERMINAL_DATA_COLUMNS + 36
      GRID_COLUMN_CARD_ID = TERMINAL_DATA_COLUMNS + 37
      GRID_COLUMN_TERMINAL_TYPE = TERMINAL_DATA_COLUMNS + 38
      GRID_COLUMN_COMPUTED_POINTS = TERMINAL_DATA_COLUMNS + 39
      GRID_COLUMN_AWARDED_POINTS = TERMINAL_DATA_COLUMNS + 40
      GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 41
      GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 42
      GRID_COLUMN_PROMOTIONAL = TERMINAL_DATA_COLUMNS + 43
      GRID_COLUMN_AWARDED_POINTS_ORIGINAL = TERMINAL_DATA_COLUMNS + 44
      GRID_COLUMN_AWARDED_POINTS_STATUS = TERMINAL_DATA_COLUMNS + 45
      GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL = TERMINAL_DATA_COLUMNS + 46
      GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 47
      GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 48
      GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 49
      GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 50
      GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 51
      GRID_COLUMN_PLAYED_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 52
      GRID_COLUMN_WON_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 53
      GRID_COLUMN_RE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 54
      GRID_COLUMN_RE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 55
      GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 56
      GRID_COLUMN_NRE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 57
      GRID_COLUMN_PLAYED_COUNT_OLD = TERMINAL_DATA_COLUMNS + 58
      GRID_COLUMN_WON_COUNT_OLD = TERMINAL_DATA_COLUMNS + 59

    End If

    GRID_NUM_COLUMNS = TERMINAL_DATA_COLUMNS + 60
  End Sub

  Private Sub SetGridColumns()

    Call GridColumnReIndex()

    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then

      GRID_COLUMN_RE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 8
      GRID_COLUMN_NRE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 9
      GRID_COLUMN_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 10

      GRID_COLUMN_RE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 11
      GRID_COLUMN_NRE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 12
      GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 13
      GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 14

      GRID_COLUMN_RE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 15
      GRID_COLUMN_NRE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 16
      GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 17
      GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 18

      GRID_COLUMN_RE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 19
      GRID_COLUMN_NRE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 20
      GRID_COLUMN_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 21

      GRID_COLUMN_RE_NETWIN = TERMINAL_DATA_COLUMNS + 22
      GRID_COLUMN_NRE_NETWIN = TERMINAL_DATA_COLUMNS + 23
      GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 24

      GRID_COLUMN_HANDPAYS_AMOUNT = TERMINAL_DATA_COLUMNS + 25
      GRID_COLUMN_HANDPAYS_PAID_AMOUNT = TERMINAL_DATA_COLUMNS + 26

      GRID_COLUMN_REDEEMABLE_TICKET_IN = -1
      GRID_COLUMN_PROMO_RE_TICKET_IN = -1
      GRID_COLUMN_BILLS_IN_AMOUNT = -1
      GRID_COLUMN_REDEEMABLE_TICKET_OUT = -1

      GRID_COLUMN_PROMO_NONRE_TICKET_IN = -1
      GRID_COLUMN_PROMO_NONRE_TICKET_OUT = -1

      GRID_COLUMN_TOTAL_TICKET_IN = -1
      GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL = -1
      GRID_COLUMN_TOTAL_TICKET_OUT = -1

      GRID_COLUMN_CASH_IN_PLAY_SESSION = TERMINAL_DATA_COLUMNS + 27
      GRID_COLUMN_SESSION_ID = TERMINAL_DATA_COLUMNS + 28
      GRID_COLUMN_CARD_ID = TERMINAL_DATA_COLUMNS + 29
      GRID_COLUMN_TERMINAL_TYPE = TERMINAL_DATA_COLUMNS + 30
      GRID_COLUMN_COMPUTED_POINTS = TERMINAL_DATA_COLUMNS + 31
      GRID_COLUMN_AWARDED_POINTS = TERMINAL_DATA_COLUMNS + 32

      GRID_COLUMN_PROMOTIONAL = TERMINAL_DATA_COLUMNS + 33
      GRID_COLUMN_AWARDED_POINTS_ORIGINAL = TERMINAL_DATA_COLUMNS + 34
      GRID_COLUMN_AWARDED_POINTS_STATUS = TERMINAL_DATA_COLUMNS + 35
      GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL = TERMINAL_DATA_COLUMNS + 36
      GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 37
      ' DHA 17-MAR-2014
      GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 38
      GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 39
      GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 40
      GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 41
      GRID_COLUMN_PLAYED_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 42
      GRID_COLUMN_WON_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 43
      ' DHA 17-MAR-2014
      GRID_COLUMN_RE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 44
      GRID_COLUMN_RE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 45
      GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 46
      GRID_COLUMN_NRE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 47
      GRID_COLUMN_PLAYED_COUNT_OLD = TERMINAL_DATA_COLUMNS + 48
      GRID_COLUMN_WON_COUNT_OLD = TERMINAL_DATA_COLUMNS + 49

    Else

      GRID_COLUMN_RE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 8
      GRID_COLUMN_RE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 9
      GRID_COLUMN_RE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 10
      GRID_COLUMN_RE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 11
      GRID_COLUMN_RE_NETWIN = TERMINAL_DATA_COLUMNS + 12

      GRID_COLUMN_REDEEMABLE_TICKET_IN = -1
      GRID_COLUMN_PROMO_RE_TICKET_IN = -1
      GRID_COLUMN_BILLS_IN_AMOUNT = -1
      GRID_COLUMN_REDEEMABLE_TICKET_OUT = -1

      GRID_COLUMN_NRE_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 13
      GRID_COLUMN_NRE_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 14
      GRID_COLUMN_NRE_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 15
      GRID_COLUMN_NRE_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 16
      GRID_COLUMN_NRE_NETWIN = TERMINAL_DATA_COLUMNS + 17

      GRID_COLUMN_PROMO_NONRE_TICKET_IN = -1
      GRID_COLUMN_PROMO_NONRE_TICKET_OUT = -1

      GRID_COLUMN_INITIAL_BALANCE = TERMINAL_DATA_COLUMNS + 18
      GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 19
      GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 20
      GRID_COLUMN_FINAL_BALANCE = TERMINAL_DATA_COLUMNS + 21

      GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 22

      GRID_COLUMN_HANDPAYS_AMOUNT = TERMINAL_DATA_COLUMNS + 23
      GRID_COLUMN_HANDPAYS_PAID_AMOUNT = TERMINAL_DATA_COLUMNS + 24

      GRID_COLUMN_TOTAL_TICKET_IN = -1
      GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL = -1
      GRID_COLUMN_TOTAL_TICKET_OUT = -1

      GRID_COLUMN_CASH_IN_PLAY_SESSION = TERMINAL_DATA_COLUMNS + 25
      GRID_COLUMN_SESSION_ID = TERMINAL_DATA_COLUMNS + 26
      GRID_COLUMN_CARD_ID = TERMINAL_DATA_COLUMNS + 27
      GRID_COLUMN_TERMINAL_TYPE = TERMINAL_DATA_COLUMNS + 28
      GRID_COLUMN_COMPUTED_POINTS = TERMINAL_DATA_COLUMNS + 29
      GRID_COLUMN_AWARDED_POINTS = TERMINAL_DATA_COLUMNS + 30
      GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 31
      GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 32
      GRID_COLUMN_PROMOTIONAL = TERMINAL_DATA_COLUMNS + 33
      GRID_COLUMN_AWARDED_POINTS_ORIGINAL = TERMINAL_DATA_COLUMNS + 34
      GRID_COLUMN_AWARDED_POINTS_STATUS = TERMINAL_DATA_COLUMNS + 35
      GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL = TERMINAL_DATA_COLUMNS + 36
      GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 37
      ' DHA 17-MAR-2014
      GRID_COLUMN_RE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 38
      GRID_COLUMN_RE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 39
      GRID_COLUMN_NRE_PLAYED_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 40
      GRID_COLUMN_NRE_WON_AMOUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 41
      GRID_COLUMN_PLAYED_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 42
      GRID_COLUMN_WON_COUNT_ORIGINAL = TERMINAL_DATA_COLUMNS + 43
      ' DHA 17-MAR-2014
      GRID_COLUMN_RE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 44
      GRID_COLUMN_RE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 45
      GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 46
      GRID_COLUMN_NRE_WON_AMOUNT_OLD = TERMINAL_DATA_COLUMNS + 47
      GRID_COLUMN_PLAYED_COUNT_OLD = TERMINAL_DATA_COLUMNS + 48
      GRID_COLUMN_WON_COUNT_OLD = TERMINAL_DATA_COLUMNS + 49

    End If

    GRID_NUM_COLUMNS = TERMINAL_DATA_COLUMNS + 50
  End Sub

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As StringBuilder
    Dim _str_filter_ac As String = ""
    Dim _str_points As String = ""
    Dim _str_index As String = ""

    _str_where = New StringBuilder()
    _str_index = GetSQLIndex()

    _str_where.AppendLine("SELECT   PS_PLAY_SESSION_ID ")
    _str_where.AppendLine("       , PS_STARTED ")
    _str_where.AppendLine("       , PS_FINISHED ")
    _str_where.AppendLine("       , TE_PROVIDER_ID ")
    _str_where.AppendLine("       , TE_NAME ")
    _str_where.AppendLine("       , PS_TERMINAL_ID ")
    _str_where.AppendLine("       , PS_TOTAL_CASH_IN ")
    _str_where.AppendLine("       , PS_PLAYED_AMOUNT ")
    _str_where.AppendLine("       , PS_WON_AMOUNT ")
    _str_where.AppendLine("       , PS_PLAYED_COUNT ")
    _str_where.AppendLine("       , PS_WON_COUNT ")
    _str_where.AppendLine("       , PS_CASH_IN ")
    _str_where.AppendLine("       , PS_TOTAL_CASH_OUT ")
    _str_where.AppendLine("       , PS_STATUS ")
    _str_where.AppendLine("       , AC_ACCOUNT_ID ")
    _str_where.AppendLine("       , AC_TYPE ")
    _str_where.AppendLine("       , AC_TRACK_DATA ")
    _str_where.AppendLine("       , PS_PROMO ")
    _str_where.AppendLine("       , AC_HOLDER_NAME ")
    _str_where.AppendLine("       , TE_TERMINAL_TYPE ")
    _str_where.AppendLine("       , PS_TYPE ")
    _str_where.AppendLine("       , PS_REDEEMABLE_CASH_IN ")
    _str_where.AppendLine("       , PS_REDEEMABLE_PLAYED ")
    _str_where.AppendLine("       , PS_REDEEMABLE_WON ")
    _str_where.AppendLine("       , PS_REDEEMABLE_CASH_OUT ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_CASH_IN ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_PLAYED ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_WON ")
    _str_where.AppendLine("       , PS_NON_REDEEMABLE_CASH_OUT ")
    _str_where.AppendLine("       , PS_COMPUTED_POINTS ")
    _str_where.AppendLine("       , PS_AWARDED_POINTS ")

    If Me.m_is_tito_mode Then
      _str_where.AppendLine("       , PS_RE_TICKET_IN ")
      _str_where.AppendLine("       , PS_PROMO_RE_TICKET_IN ")
      _str_where.AppendLine("       , PS_CASH_IN AS PS_BILLS_IN_AMOUNT ")
      _str_where.AppendLine("       , PS_RE_TICKET_OUT ")
      _str_where.AppendLine("       , PS_PROMO_NR_TICKET_IN ")
      _str_where.AppendLine("       , PS_PROMO_NR_TICKET_OUT ")
      _str_where.AppendLine("       , PS_AUX_FT_RE_CASH_IN ")
      _str_where.AppendLine("       , PS_AUX_FT_NR_CASH_IN  ")
    End If

    _str_where.AppendLine("        , ISNULL(PS_REDEEMABLE_PLAYED_ORIGINAL, PS_REDEEMABLE_PLAYED) As PS_REDEEMABLE_PLAYED_ORIGINAL")
    _str_where.AppendLine("        , ISNULL(PS_REDEEMABLE_WON_ORIGINAL, PS_REDEEMABLE_WON) As PS_REDEEMABLE_WON_ORIGINAL")
    _str_where.AppendLine("        , ISNULL(PS_NON_REDEEMABLE_PLAYED_ORIGINAL, PS_NON_REDEEMABLE_PLAYED) As PS_NON_REDEEMABLE_PLAYED_ORIGINAL")
    _str_where.AppendLine("        , ISNULL(PS_NON_REDEEMABLE_WON_ORIGINAL, PS_NON_REDEEMABLE_WON) As PS_NON_REDEEMABLE_WON_ORIGINAL")
    _str_where.AppendLine("        , ISNULL(PS_PLAYED_COUNT_ORIGINAL, PS_PLAYED_COUNT) As PS_PLAYED_COUNT_ORIGINAL")
    _str_where.AppendLine("        , ISNULL(PS_WON_COUNT_ORIGINAL, PS_WON_COUNT) As PS_WON_COUNT_ORIGINAL")

    _str_where.AppendLine("       , ISNULL (PS_AWARDED_POINTS_STATUS, 0) PS_AWARDED_POINTS_STATUS ")

    ' JMM 20-OCT-2016
    _str_where.AppendLine("       , ISNULL (PS_HANDPAYS_AMOUNT, 0) PS_HANDPAYS_AMOUNT ")
    _str_where.AppendLine("       , ISNULL (PS_HANDPAYS_PAID_AMOUNT, 0) PS_HANDPAYS_PAID_AMOUNT ")

    _str_where.AppendLine("        , ISNULL(PS_RE_FOUND_IN_EGM, 0) + ISNULL(PS_NR_FOUND_IN_EGM, 0) PS_FOUND ")
    _str_where.AppendLine("        , ISNULL(PS_RE_REMAINING_IN_EGM, 0) + ISNULL(PS_NR_REMAINING_IN_EGM, 0) PS_REMAINING ")

    _str_where.AppendLine("  FROM   PLAY_SESSIONS " & _str_index)
    _str_where.AppendLine(" INNER   JOIN ACCOUNTS ON PS_ACCOUNT_ID  = AC_ACCOUNT_ID ")
    _str_where.AppendLine(" INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID ")

    ' Add the account numbers selected in the massive search form.
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.MassiveSearchNumbers) Then
      If Me.uc_account_sel1.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_where.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_ACCOUNT_ID"))
      Else
        _str_where.AppendLine(Me.uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_TRACK_DATA"))
      End If
    End If

    ' Filter Open Sessions / Historic sessions
    If Me.rb_open_sessions.Checked Then
      _str_where.AppendLine(" WHERE PS_STATUS = " & WSI.Common.PlaySessionStatus.Opened)
    Else
      ' Filter Dates
      If Me.rb_ini_date.Checked Then
        If Me.dtp_from.Checked = True Then
          _str_where.AppendLine(" WHERE (PS_STARTED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
        End If

        If Me.dtp_to.Checked = True Then
          _str_where.AppendLine(" AND (PS_STARTED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
        End If
      Else
        If Me.dtp_from.Checked = True Then
          _str_where.AppendLine(" WHERE (PS_FINISHED >= " & GUI_FormatDateDB(dtp_from.Value) & ") ")
        End If

        If Me.dtp_to.Checked = True Then
          _str_where.AppendLine(" AND (PS_FINISHED < " & GUI_FormatDateDB(dtp_to.Value) & ") ")
        End If
      End If
    End If

    If (WSI.Common.Misc.IsCashlessMode And WSI.Common.Misc.IsFeatureTerminalDrawEnabled()) Then
      If Me.chk_only_cdt_sessions.Checked = True Then
        _str_where.AppendLine("   AND   PS_TYPE = " & WSI.Common.PlaySessionType.TERMINAL_DRAW)
      End If

      If Me.chk_without_cdt_sessions.Checked = True Then
        _str_where.AppendLine("   AND   PS_TYPE <> " & WSI.Common.PlaySessionType.TERMINAL_DRAW)
      End If
    End If

    ' Filter Status
    If Me.opt_several_status.Checked Then
      _str_where.AppendLine(" AND PS_STATUS = " & Me.cmb_status.Value)
    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Or Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      _str_where.Append(" AND PS_STATUS IN (" & WSI.Common.PlaySessionStatus.Closed & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.Abandoned & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.ManualClosed & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.PariPlayBet & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.PariPlayPrize & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.PariPlayBetRollback & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.GameGatewayBet & ",")
      _str_where.Append(WSI.Common.PlaySessionStatus.GameGatewayPrize & ",")
      _str_where.AppendLine(WSI.Common.PlaySessionStatus.GameGatewayBetRollback & ") ")
    End If

    ' Filter Providers - Terminals
    _str_where.AppendLine(" AND PS_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    ' RCI 13-DEC-2010: Only Gap Sessions
    If Me.chk_only_gap_sessions.Checked Then
      _str_where.AppendLine(" AND ROUND (PS_PLAYED_AMOUNT -  PS_WON_AMOUNT, 2) <> ROUND (PS_TOTAL_CASH_IN - PS_TOTAL_CASH_OUT, 2)")
    End If

    ' Points
    If Me.chk_only_pending_points.Checked Then
      _str_points = _str_points & AwardPointsStatus.Pending & ","
    End If
    If Me.chk_only_manual_points.Checked Then
      _str_points = _str_points & AwardPointsStatus.Manual & "," & AwardPointsStatus.ManualCalculated & ","
    End If

    If Not Me.chk_only_pending_points.Checked And _
       Not Me.chk_only_manual_points.Checked And _
       Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then

      _str_points = AwardPointsStatus.Pending & "," & _
                    AwardPointsStatus.Manual & "," & _
                    AwardPointsStatus.ManualCalculated & ","
    End If
    If _str_points <> "" Then
      _str_where.AppendLine(" AND PS_AWARDED_POINTS_STATUS IN (" & _str_points.Substring(0, _str_points.Length - 1) & ")")
    End If

    _str_filter_ac = Me.uc_account_sel1.GetFilterSQL()
    If _str_filter_ac <> "" Then
      _str_where.AppendLine(" AND " & _str_filter_ac)
    End If

    Return _str_where.ToString()
  End Function ' GetSqlWhere

  ' PURPOSE: Checks whether the specified row has valid data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains valid data
  '     - FALSE: selected row does not exist or contains no valid data
  '
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column SESSION_ID.
      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_SESSION_ID).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE : Close the selected play session.
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - Boolean: To indicate if the play session has been closed correctly.
  '
  Private Function ClosePlaySession() As Boolean
    Dim idx_row As Int32
    Dim session_id As Integer
    Dim play_session As CLASS_PLAY_SESSION
    Dim ctx As Integer
    Dim rc As Integer
    Dim _frm_play_session_close As frm_play_session_close
    Dim _account_id As Integer
    Dim _account_holder_name As String

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then

        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    End If

    ' Get the complete session ID
    If Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    Else
      session_id = Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value
      _account_id = Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT_ID).Value
      _account_holder_name = Me.Grid.Cell(idx_row, GRID_COLUMN_HOLDER_NAME).Value
    End If

    DbReadObject = New CLASS_PLAY_SESSION
    DbReadObject.DB_Read(session_id, ctx)

    play_session = DbReadObject.Duplicate()

    If play_session.Status <> WSI.Common.PlaySessionStatus.Opened Then
      ' 106 "Session cannot be closed: it is not open."
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(106), ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    If play_session.Inactivity < INACTIVITY_TIMEOUT Then
      ' 107 "Session cannot be closed: it has registered activity during the last %1 minutes."
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(107), ENUM_MB_TYPE.MB_TYPE_ERROR, , , INACTIVITY_TIMEOUT)

      Return False
    End If

    ' Create instance of Play Session Close form
    _frm_play_session_close = New frm_play_session_close()
    _frm_play_session_close.ShowForEdit(_account_id, _account_holder_name)

    If _frm_play_session_close.DialogResult <> DialogResult.OK Then
      Return False

    End If

    rc = play_session.PlaySessionClose(_frm_play_session_close.ClosingReason)
    If rc <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
      ' 105 "Session could not be closed."
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    play_session.AuditorData.Notify(CurrentUser.GuiId, _
                                    CurrentUser.Id, _
                                    CurrentUser.Name, _
                                    CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                    ctx, _
                                    DbReadObject.AuditorData)

    NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(104), ENUM_MB_TYPE.MB_TYPE_INFO) ' "Session closed manualy"

    ' Refresh screen data
    Call GUI_ExecuteQuery()

    Return True

  End Function ' ClosePlaySession

  ' PURPOSE : Data of selected play session.
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - Boolean: Finish correctly.
  '
  Private Function PlaySessionData() As Boolean
    Dim _frm As frm_play_session_data
    Dim _idx_row As Int32
    Dim _session_id As Integer
    Dim _play_session As CLASS_PLAY_SESSION
    Dim _ctx As Integer

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then

        Exit For
      End If
    Next

    If Not IsValidDataRow(_idx_row) Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    End If

    ' Get the complete session ID
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value = "" Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    Else
      _session_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value
    End If

    DbReadObject = New CLASS_PLAY_SESSION
    DbReadObject.DB_Read(_session_id, _ctx)

    _play_session = DbReadObject.Duplicate()

    If _play_session.Status <> WSI.Common.PlaySessionStatus.ManualClosed Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3007), ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_STATISTICS.GetString(404))

      Return False
    End If

    _frm = New frm_play_session_data(_session_id)
    _frm.ShowDialog()

    Return True

  End Function ' PlaySessionData

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)
    Dim _terminal_type As String
    Dim _provider As String
    Dim _provider_terminal As String
    Dim _account As String
    Dim _track_data As String

    UpdateTotalCounters(m_global_total, DbRow)

    If m_grouped_by = ENUM_GROUPED.NOT_GROUPED Then
      Return
    End If

    ' Account Id - Track Data - Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      _account = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      _account = " "
    End If
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_TRACKDATA) Then
      CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_TRACKDATA), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                  CType(DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE), AccountType))
    End If
    _account = DbRow.Value(SQL_COLUMN_ACCOUNT_ID) & vbCr & _track_data & vbCr & _account

    ' Terminal and Provider Name
    _terminal_type = GetTerminalTypeNls(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE))
    _provider = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)
    _provider = _provider.ToUpper()

    ' Add Provider to Terminal
    _provider_terminal = ""
    For _idx As Int32 = 0 To m_current_terminal_data.Count - 1
      If Not m_current_terminal_data(_idx) Is Nothing AndAlso Not m_current_terminal_data(_idx) Is DBNull.Value Then
        _provider_terminal &= m_current_terminal_data(_idx).ToString() & vbCr
      Else
        _provider_terminal &= "" & vbCr
      End If
    Next
    _provider_terminal &= _terminal_type

    If m_grouped_by = ENUM_GROUPED.BY_ACCOUNT Then
      ' Add Account to Provider if grouping by account
      _provider = _provider & vbCr & _account
    End If

    If m_first_time Then
      m_first_time = False
      ResetTotalCounters(m_terminal_total, _provider_terminal)
      ResetTotalCounters(m_provider_total, _provider)
      ResetTotalCounters(m_account_total, _account)
    End If

    Select Case m_grouped_by
      Case ENUM_GROUPED.BY_PROVIDER
        If Not m_provider_total.key.Equals(_provider) Then
          DrawTotalRow(m_terminal_total, ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL)
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
          ' It's a new provider... Reset totals for provider and for terminal.
          ResetTotalCounters(m_terminal_total, _provider_terminal)
          ResetTotalCounters(m_provider_total, _provider)
        ElseIf Not m_terminal_total.key.Equals(_provider_terminal) Then
          DrawTotalRow(m_terminal_total, ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL)
          ' It's a new terminal... Reset totals.
          ResetTotalCounters(m_terminal_total, _provider_terminal)
        End If

        UpdateTotalCounters(m_terminal_total, DbRow)
        UpdateTotalCounters(m_provider_total, DbRow)

      Case ENUM_GROUPED.BY_ACCOUNT
        If Not m_account_total.key.Equals(_account) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
          DrawTotalRow(m_account_total, ENUM_GRID_ROW_TYPE.TOTAL_ACCOUNT)
          ' It's a new account... Reset totals for account and for provider.
          ResetTotalCounters(m_provider_total, _provider)
          ResetTotalCounters(m_account_total, _account)

        ElseIf Not m_provider_total.key.Equals(_provider) Then
          DrawTotalRow(m_provider_total, ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER)
          ' It's a new provider... Reset totals.
          ResetTotalCounters(m_provider_total, _provider)
        End If

        UpdateTotalCounters(m_provider_total, DbRow)
        UpdateTotalCounters(m_account_total, DbRow)

      Case ENUM_GROUPED.NOT_GROUPED
      Case Else
    End Select

  End Sub ' ProcessCounters

  ' PURPOSE : Add and draw a total row.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Type As ENUM_GRID_ROW_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub DrawTotalRow(ByVal Total As TYPE_TOTAL_COUNTERS, ByVal Type As ENUM_GRID_ROW_TYPE)

    'If Me.m_is_tito_mode Then
    '  Call DrawTotalRowTitoMode(Total, Type)
    'Else
    '  Call DrawTotalRowNoneTitoMode(Total, Type)
    'End If
    Dim idx_row As Integer
    Dim str_key As String
    Dim str_key_2 As String
    Dim idx_key As String
    Dim idx_key_2 As String
    Dim str_split As String()
    Dim color As ENUM_GUI_COLOR
    Dim _time As TimeSpan

    If Not Me.chk_subtotal.Checked Then
      Select Case m_grouped_by
        Case ENUM_GROUPED.BY_PROVIDER
          If Type = ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL Then
            Return
          End If

        Case ENUM_GROUPED.BY_ACCOUNT
          If Type = ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER Then
            Return
          End If
      End Select
    End If

    str_key = ""
    idx_key = 0
    str_key_2 = ""
    idx_key_2 = 0

    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1

    Select Case Type
      Case ENUM_GRID_ROW_TYPE.TOTAL_PROVIDER
        idx_key = GRID_COLUMN_PROVIDER_ID

        Select Case m_grouped_by
          Case ENUM_GROUPED.BY_PROVIDER
            str_key = Total.key
            color = ENUM_GUI_COLOR.GUI_COLOR_GREEN_01

          Case ENUM_GROUPED.BY_ACCOUNT
            str_split = Total.key.Split(vbCr)
            str_key = str_split(0)
            If str_split.Length > 3 Then
              Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT_ID).Value = str_split(1)
              Me.Grid.Cell(idx_row, GRID_COLUMN_CARD_ID).Value = str_split(2)
              Me.Grid.Cell(idx_row, GRID_COLUMN_HOLDER_NAME).Value = str_split(3)
            End If
            color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        End Select

      Case ENUM_GRID_ROW_TYPE.TOTAL_TERMINAL
        str_split = Total.key.Split(vbCr)
        If str_split.Length > 1 AndAlso str_split.Length = TERMINAL_DATA_COLUMNS + 1 Then
          For _idx As Int32 = 0 To TERMINAL_DATA_COLUMNS - 1
            Me.Grid.Cell(idx_row, GRID_INIT_TERMINAL_DATA + _idx).Value = str_split(_idx)
          Next
          Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL_TYPE_NAME).Value = str_split(TERMINAL_DATA_COLUMNS)
        Else
          str_key = str_split(0)
          idx_key = GRID_COLUMN_TERMINAL_ID
        End If
        color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01

      Case ENUM_GRID_ROW_TYPE.TOTAL_ACCOUNT
        str_split = Total.key.Split(vbCr)
        If str_split.Length > 2 Then
          str_key = str_split(2)
        Else
          str_key = str_split(0)
        End If
        idx_key = GRID_COLUMN_HOLDER_NAME
        color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        If str_split.Length > 1 Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_ACCOUNT_ID).Value = str_split(0)
          Me.Grid.Cell(idx_row, GRID_COLUMN_CARD_ID).Value = str_split(1)
        End If

      Case ENUM_GRID_ROW_TYPE.TOTAL
        str_key = GLB_NLS_GUI_INVOICING.GetString(205)
        idx_key = GRID_COLUMN_PROVIDER_ID
        color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

        _time = New TimeSpan(0, 0, m_duration_total)
        If _time.TotalSeconds > 0 Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_TIME).Value = TimeSpanToString(_time)
        End If
        If m_count_session > 0 Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_STARTED).Value = GLB_NLS_GUI_STATISTICS.GetString(355) & ": " & GUI_FormatNumber(m_count_session, 0)
        End If

    End Select

    Me.Grid.Row(idx_row).BackColor = GetColor(color)

    If str_key <> "" Then
      Me.Grid.Cell(idx_row, idx_key).Value = str_key
    End If
    If str_key_2 <> "" Then
      Me.Grid.Cell(idx_row, idx_key_2).Value = str_key_2
    End If

    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(Total.played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(Total.won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_INITIAL_BALANCE).Value = GUI_FormatCurrency(Total.initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(Total.fin_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(Total.initial_balance - Total.fin_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_RE_INITIAL_BALANCE).Value = GUI_FormatCurrency(Total.redeemable_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value = GUI_FormatCurrency(Total.redeemable_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value = GUI_FormatCurrency(Total.redeemable_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_RE_FINAL_BALANCE).Value = GUI_FormatCurrency(Total.redeemable_fin_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_RE_NETWIN).Value = GUI_FormatCurrency(Total.redeemable_initial_balance - Total.redeemable_fin_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_NRE_INITIAL_BALANCE).Value = GUI_FormatCurrency(Total.not_redeemable_initial_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value = GUI_FormatCurrency(Total.not_redeemable_played_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value = GUI_FormatCurrency(Total.not_redeemable_won_amount, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_NRE_FINAL_BALANCE).Value = GUI_FormatCurrency(Total.not_redeemable_fin_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_NRE_NETWIN).Value = GUI_FormatCurrency(Total.not_redeemable_initial_balance - Total.not_redeemable_fin_balance, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_HANDPAYS_AMOUNT).Value = GUI_FormatCurrency(Total.handpay, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(idx_row, GRID_COLUMN_COMPUTED_POINTS).Value = GUI_FormatNumber(Total.computed_points)
    Me.Grid.Cell(idx_row, GRID_COLUMN_AWARDED_POINTS).Value = GUI_FormatNumber(Total.awarded_points)
    Me.Grid.Cell(idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value = GUI_FormatNumber(Total.awarded_points)
    Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(Total.played_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(Total.won_count, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    If Me.m_is_tito_mode Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_REDEEMABLE_TICKET_IN).Value = GUI_FormatCurrency(Total.redeemable_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_RE_TICKET_IN).Value = GUI_FormatCurrency(Total.redeemable_ticket_in_promo, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_BILLS_IN_AMOUNT).Value = GUI_FormatCurrency(Total.bill_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_REDEEMABLE_TICKET_OUT).Value = GUI_FormatCurrency(Total.redeemable_ticket_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_NONRE_TICKET_IN).Value = GUI_FormatCurrency(Total.not_redeemable_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROMO_NONRE_TICKET_OUT).Value = GUI_FormatCurrency(Total.not_redeemable_ticket_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_TOTAL_TICKET_IN).Value = GUI_FormatCurrency(Total.total_ticket_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_BILLS_IN_AMOUNT_TOTAL).Value = GUI_FormatCurrency(Total.total_bill_in, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_TOTAL_TICKET_OUT).Value = GUI_FormatCurrency(Total.total_ticket_out, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_EGM_CREDIT_FINAL).Value = GUI_FormatCurrency(Total.total_remaining, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(idx_row, GRID_COLUMN_EGM_CREDIT_INITIAL).Value = GUI_FormatCurrency(Total.total_found, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_CASH_IN_PLAY_SESSION).Value = GUI_FormatCurrency(Total.cash_in_play_session, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

  End Sub ' DrawTotalRow


  ' PURPOSE : Reset Total Counters variable
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Key As String
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal Key As String)
    Total.key = Key
    Total.played_amount = 0
    Total.won_amount = 0
    Total.initial_balance = 0
    Total.cash_in_play_session = 0
    Total.fin_balance = 0
    Total.redeemable_initial_balance = 0
    Total.redeemable_played_amount = 0
    Total.redeemable_won_amount = 0
    Total.redeemable_fin_balance = 0
    Total.not_redeemable_initial_balance = 0
    Total.not_redeemable_played_amount = 0
    Total.not_redeemable_won_amount = 0
    Total.not_redeemable_fin_balance = 0
    Total.computed_points = 0
    Total.awarded_points = 0
    Total.played_count = 0
    Total.won_count = 0
    Total.redeemable_ticket_in = 0
    Total.redeemable_ticket_in_promo = 0
    Total.bill_in = 0
    Total.redeemable_ticket_out = 0
    Total.not_redeemable_ticket_in = 0
    Total.not_redeemable_ticket_out = 0
    Total.total_ticket_in = 0
    Total.total_bill_in = 0
    Total.total_ticket_out = 0
    Total.handpay = 0
    Total.remaining = 0
    Total.found = 0
    Total.total_remaining = 0
    Total.total_found = 0

  End Sub ' ResetTotalCounters

  ' PURPOSE : Update total counters.
  '
  '  PARAMS :
  '     - INPUT :
  '           - Total As TYPE_TOTAL_COUNTERS
  '           - Amount As Decimal
  '           - IsPending As Boolean
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub UpdateTotalCounters(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal DbRow As CLASS_DB_ROW)

    If Me.m_is_tito_mode Then
      Call UpdateTotalCountersTitoMode(Total, DbRow)
    Else
      Call UpdateTotalCountersNoneTitoMode(Total, DbRow)
    End If

  End Sub ' UpdateTotalCounters

  Private Sub UpdateTotalCountersTitoMode(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal DbRow As CLASS_DB_ROW)

    With Total
      .played_amount = .played_amount + DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
      .won_amount = .won_amount + DbRow.Value(SQL_COLUMN_WON_AMOUNT)
      .initial_balance = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayCancelPayment And (DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_CANCELLED_CREDITS Or DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_MANUAL), .initial_balance, .initial_balance + DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN))
      .cash_in_play_session = .cash_in_play_session + DbRow.Value(SQL_COLUMN_CASH_IN_PLAY_SESSION)
      .fin_balance = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment And (DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_CANCELLED_CREDITS Or DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_MANUAL), .fin_balance, .fin_balance + DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT))

      .redeemable_initial_balance = .redeemable_initial_balance + DbRow.Value(SQL_COLUMN_RE_CASH_IN)
      'DHA 04-APR-2017
      .redeemable_initial_balance += IIf(Me.m_is_tito_mode And Not DbRow.IsNull(SQL_COLUMN_AUX_FT_RE_CASH_IN), DbRow.Value(SQL_COLUMN_AUX_FT_RE_CASH_IN), 0)

      .redeemable_played_amount = .redeemable_played_amount + DbRow.Value(SQL_COLUMN_RE_PLAYED_AMOUNT)
      .redeemable_won_amount = .redeemable_won_amount + DbRow.Value(SQL_COLUMN_RE_WON_AMOUNT)
      .redeemable_fin_balance = .redeemable_fin_balance + DbRow.Value(SQL_COLUMN_RE_CASH_OUT)
      .not_redeemable_initial_balance = .not_redeemable_initial_balance + DbRow.Value(SQL_COLUMN_NRE_CASH_IN)
      'DHA 04-APR-2017
      .not_redeemable_initial_balance += IIf(Me.m_is_tito_mode And Not DbRow.IsNull(SQL_COLUMN_AUX_FT_NR_CASH_IN), DbRow.Value(SQL_COLUMN_AUX_FT_NR_CASH_IN), 0)

      .not_redeemable_played_amount = .not_redeemable_played_amount + DbRow.Value(SQL_COLUMN_NRE_PLAYED_AMOUNT)
      .not_redeemable_won_amount = .not_redeemable_won_amount + DbRow.Value(SQL_COLUMN_NRE_WON_AMOUNT)
      .not_redeemable_fin_balance = .not_redeemable_fin_balance + DbRow.Value(SQL_COLUMN_NRE_CASH_OUT)
      .computed_points = GUI_FormatCurrency(.computed_points + IIf(DbRow.IsNull(SQL_COLUMN_COMPUTED_POINTS), 0, DbRow.Value(SQL_COLUMN_COMPUTED_POINTS)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .awarded_points = GUI_FormatCurrency(.awarded_points + IIf(DbRow.IsNull(SQL_COLUMN_AWARDED_POINTS), 0, DbRow.Value(SQL_COLUMN_AWARDED_POINTS)), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .played_count = .played_count + DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
      .won_count = .won_count + DbRow.Value(SQL_COLUMN_WON_COUNT)
      .redeemable_ticket_in += IIf(DbRow.IsNull(SQL_COLUMN_RE_TICKET_IN), 0, DbRow.Value(SQL_COLUMN_RE_TICKET_IN))
      .redeemable_ticket_in_promo += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_IN), 0, DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_IN))
      .bill_in += IIf(DbRow.IsNull(SQL_COLUMN_BILLS_IN_AMOUNT), 0, DbRow.Value(SQL_COLUMN_BILLS_IN_AMOUNT))
      .redeemable_ticket_out += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_RE_TICKET_OUT), 0, DbRow.Value(SQL_COLUMN_PROMO_RE_TICKET_OUT))
      .not_redeemable_ticket_in += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_IN), 0, DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_IN))
      .not_redeemable_ticket_out += IIf(DbRow.IsNull(SQL_COLUMN_PROMO_NONRE_TICKET_OUT), 0, DbRow.Value(SQL_COLUMN_PROMO_NONRE_TICKET_OUT))
      .total_ticket_in = .redeemable_ticket_in + .redeemable_ticket_in_promo + .not_redeemable_ticket_in
      .total_bill_in = .bill_in
      .total_ticket_out = .not_redeemable_ticket_out + .redeemable_ticket_out

      .handpay = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment, .handpay + IIf(DbRow.IsNull(SQL_COLUMN_TOTAL_CASH_OUT), 0, DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)), .handpay)
      .handpay = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayCancelPayment, .handpay + IIf(DbRow.IsNull(SQL_COLUMN_TOTAL_CASH_IN), 0, -DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)), .handpay)

      .remaining += IIf(DbRow.IsNull(SQL_COLUMN_REMAINING_IN_EGM), 0, DbRow.Value(SQL_COLUMN_REMAINING_IN_EGM))
      .found += IIf(DbRow.IsNull(SQL_COLUMN_FOUND_IN_EGM), 0, DbRow.Value(SQL_COLUMN_FOUND_IN_EGM))
      .total_remaining = .remaining
      .total_found = .found
    End With
  End Sub ' UpdateTotalCountersTitoMode

  Private Sub UpdateTotalCountersNoneTitoMode(ByRef Total As TYPE_TOTAL_COUNTERS, ByVal DbRow As CLASS_DB_ROW)
    With Total
      .played_amount = .played_amount + DbRow.Value(SQL_COLUMN_PLAYED_AMOUNT)
      .won_amount = .won_amount + DbRow.Value(SQL_COLUMN_WON_AMOUNT)
      .initial_balance = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayCancelPayment And (DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_CANCELLED_CREDITS Or DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_MANUAL), .initial_balance, .initial_balance + DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN))
      .cash_in_play_session = .cash_in_play_session + DbRow.Value(SQL_COLUMN_CASH_IN_PLAY_SESSION)
      .fin_balance = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment And (DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_CANCELLED_CREDITS Or DbRow.Value(SQL_COLUMN_TYPE) = PlaySessionType.HANDPAY_MANUAL), .fin_balance, .fin_balance + DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT))
      .redeemable_initial_balance = .redeemable_initial_balance + DbRow.Value(SQL_COLUMN_RE_CASH_IN)
      .redeemable_played_amount = .redeemable_played_amount + DbRow.Value(SQL_COLUMN_RE_PLAYED_AMOUNT)
      .redeemable_won_amount = .redeemable_won_amount + DbRow.Value(SQL_COLUMN_RE_WON_AMOUNT)
      .redeemable_fin_balance = .redeemable_fin_balance + DbRow.Value(SQL_COLUMN_RE_CASH_OUT)
      .not_redeemable_initial_balance = .not_redeemable_initial_balance + DbRow.Value(SQL_COLUMN_NRE_CASH_IN)
      .not_redeemable_played_amount = .not_redeemable_played_amount + DbRow.Value(SQL_COLUMN_NRE_PLAYED_AMOUNT)
      .not_redeemable_won_amount = .not_redeemable_won_amount + DbRow.Value(SQL_COLUMN_NRE_WON_AMOUNT)
      .not_redeemable_fin_balance = .not_redeemable_fin_balance + DbRow.Value(SQL_COLUMN_NRE_CASH_OUT)
      .computed_points = .computed_points + IIf(DbRow.IsNull(SQL_COLUMN_COMPUTED_POINTS), 0, DbRow.Value(SQL_COLUMN_COMPUTED_POINTS))
      .awarded_points = .awarded_points + IIf(DbRow.IsNull(SQL_COLUMN_AWARDED_POINTS), 0, DbRow.Value(SQL_COLUMN_AWARDED_POINTS))
      .played_count = .played_count + DbRow.Value(SQL_COLUMN_PLAYED_COUNT)
      .won_count = .won_count + DbRow.Value(SQL_COLUMN_WON_COUNT)
      .handpay = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayPayment, .handpay + IIf(DbRow.IsNull(SQL_COLUMN_TOTAL_CASH_OUT), 0, DbRow.Value(SQL_COLUMN_TOTAL_CASH_OUT)), .handpay)
      .handpay = IIf(DbRow.Value(SQL_COLUMN_STATUS) = WSI.Common.PlaySessionStatus.HandpayCancelPayment, .handpay + IIf(DbRow.IsNull(SQL_COLUMN_TOTAL_CASH_IN), 0, -DbRow.Value(SQL_COLUMN_TOTAL_CASH_IN)), .handpay)
    End With
  End Sub ' UpdateTotalCountersNoneTitoMode

  Private Function GetTerminalTypeNls(ByRef TerminalType As WSI.Common.TerminalTypes) As String

    Select Case TerminalType
      Case WSI.Common.TerminalTypes.WIN
        Return GLB_NLS_GUI_AUDITOR.GetString(379)
      Case WSI.Common.TerminalTypes.T3GS
        Return GLB_NLS_GUI_AUDITOR.GetString(381)
      Case WSI.Common.TerminalTypes.SAS_HOST
        Return GLB_NLS_GUI_AUDITOR.GetString(380)
      Case WSI.Common.TerminalTypes.SITE
        Return GLB_NLS_GUI_AUDITOR.GetString(384)
      Case WSI.Common.TerminalTypes.SITE_JACKPOT
        Return GLB_NLS_GUI_AUDITOR.GetString(382)
      Case WSI.Common.TerminalTypes.MOBILE_BANK
        Return GLB_NLS_GUI_AUDITOR.GetString(383)
      Case WSI.Common.TerminalTypes.MOBILE_BANK_IMB
        Return GLB_NLS_GUI_AUDITOR.GetString(385)
      Case WSI.Common.TerminalTypes.ISTATS
        Return GLB_NLS_GUI_AUDITOR.GetString(386)
      Case WSI.Common.TerminalTypes.PROMOBOX
        Return GLB_NLS_GUI_AUDITOR.GetString(387)
      Case WSI.Common.TerminalTypes.UNKNOWN
        Return "..."
      Case Else
        Return "---"
    End Select

  End Function

  ' PURPOSE : View log file associated with the selected play session.
  '
  '  PARAMS :
  '     - INPUT : 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - Boolean: To indicate if the log file has been showen correctly.
  '
  Private Function ViewLoggerPlaySession() As Boolean
    Dim idx_row As Int32
    Dim _session_id As Integer
    Dim _cmd_id As Integer

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then

        Exit For
      End If
    Next

    If Not IsValidDataRow(idx_row) Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    End If

    ' Get the complete session ID
    If Int64.TryParse(Me.Grid.Cell(idx_row, GRID_COLUMN_SESSION_ID).Value, _session_id) = False Then
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    End If

    If Not GetCommandId(_session_id, _cmd_id) Then
      'No hay play sesion asociada al log
      NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(102), ENUM_MB_TYPE.MB_TYPE_ERROR) ' "No session selected"

      Return False
    End If

    If _cmd_id > 0 Then
      If Not ShowLogger(_cmd_id, Me.MdiParent) Then
        'No hay play sesion asociada al log
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(151), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_PROVIDER_ID).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL_NAME).Value, _
                        Me.Grid.Cell(idx_row, GRID_COLUMN_STARTED).Value)

        Return False
      End If
    Else
      'No hay play sesion asociada al log
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(151), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _
                      Me.Grid.Cell(idx_row, GRID_COLUMN_PROVIDER_ID).Value, _
                      Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL_NAME).Value, _
                      Me.Grid.Cell(idx_row, GRID_COLUMN_STARTED).Value)

      Return False
    End If

    Return True

  End Function

  ' PURPOSE : Get the command id associated with the selected play session.
  '
  '  PARAMS :
  '     - INPUT : 
  '       - session_id 
  '     - OUTPUT :
  '       - cmd_id: Returns the command id
  ' RETURNS : 
  '     - Boolean: To indicate if the log file has been showen correctly.
  '
  Private Function GetCommandId(ByVal SessionId As Integer, ByRef CmdId As Integer) As Boolean
    Dim _obj As Object
    Dim _sb As System.Text.StringBuilder

    Try
      _sb = New System.Text.StringBuilder

      _sb.AppendLine("   SELECT   TOP 1 CMD_ID")
      _sb.AppendLine("     FROM   WCP_COMMANDS")
      _sb.AppendLine("    WHERE   CMD_PS_ID  = @pSessionId ")
      _sb.AppendLine("      AND   CMD_STATUS = @pCmdStatus")
      _sb.AppendLine(" ORDER BY   CMD_ID ASC")

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_command As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId
          _sql_command.Parameters.Add("@pCmdStatus", SqlDbType.Int).Value = ENUM_WCP_COMMANDS_STATUS.STATUS_OK

          _obj = _sql_command.ExecuteScalar()

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            CmdId = _obj
          End If

        End Using
      End Using

      Return True

    Catch ex As Exception

      Log.Exception(ex)
    End Try

    Return False

  End Function 'GetCommandId

  ' PURPOSE: Save DataGrid to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub SavePointsChanges()

    Dim _idx_row As Integer
    Dim _bol_points_update As Boolean = False
    Dim _terminal_id As Int32
    Dim _terminal_name As String
    Dim _session_id As Int64
    Dim _account_id As Int64
    Dim _account_name As String
    Dim _awarded_points As Double
    Dim _awarded_points_original As Double
    Dim _awarded_points_update As Double
    Dim _awarded_points_status As Integer
    Dim _awarded_points_status_original As Integer

    If Not PendingChanges() Then

      Return
    End If

    Try

      m_negative_accounts = New List(Of String)

      Using _db_trx As DB_TRX = New DB_TRX()
        With Me.Grid
          For _idx_row = 0 To .NumRows - 1
            ' Evaluate modified rows
            If .Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value <> .Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value And _
               IsValidDataRow(_idx_row) Then

              'Parse Params
              _terminal_id = Int32.Parse(.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value)
              _terminal_name = .Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value
              _session_id = Int64.Parse(.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value)
              _account_id = Int64.Parse(.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value)
              _account_name = .Cell(_idx_row, GRID_COLUMN_HOLDER_NAME).Value
              _awarded_points = GUI_ParseNumber(.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value)
              _awarded_points_original = GUI_ParseNumber(.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value)
              _awarded_points_update = 0
              _awarded_points_status = Integer.Parse(.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value)
              _awarded_points_status_original = Integer.Parse(.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value)

              'Manual To Manual, create Negative Movement
              If _awarded_points_status_original <> AwardPointsStatus.Pending And _
                 _awarded_points_original > 0 Then
                _awarded_points_update = -_awarded_points_original
              End If

              If _awarded_points > 0 Then
                _awarded_points_update += _awarded_points
              End If

              ' Update Account
              If _awarded_points_update <> 0 Then
                If Not UpdateAccount(_terminal_id, _terminal_name, _session_id, _account_id, _account_name, _awarded_points_update, _db_trx.SqlTransaction) Then
                  NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR)

                  Return
                End If
              End If

              ' Update play session
              If Not UpdatePlaySession(_awarded_points, _awarded_points_status, _session_id, _db_trx.SqlTransaction) Then
                NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR)

                Return
              End If

              _bol_points_update = True
            End If
          Next
        End With
        ' Commit
        _db_trx.Commit()
      End Using

      ' Auditory
      If _bol_points_update Then
        Call AuditChanges()

        If m_negative_accounts.Count > 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2808), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                          , _
                          Environment.NewLine & String.Join(Environment.NewLine, m_negative_accounts.ToArray()))

        End If

        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108) _
              , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO _
              , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub  ' SavePointsChanges


  'End Sub  ' SavePointsChangesNoneTitoMode

  ' PURPOSE: Save DataGrid to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub SaveAmountsChanges()

    Dim _idx_row As Integer
    Dim _bool_plays_update As Boolean = False
    Dim _terminal_id As Int32
    Dim _terminal_name As String
    Dim _session_id As Int64
    Dim _account_id As Int64
    Dim _account_name As String
    Dim _re_played_old As Double
    Dim _re_played_update As Double
    Dim _re_played_dif As Double
    Dim _re_won_old As Double
    Dim _re_won_update As Double
    Dim _re_won_dif As Double
    Dim _nr_played_old As Double
    Dim _nr_played_update As Double
    Dim _nr_played_dif As Double
    Dim _nr_won_old As Double
    Dim _nr_won_update As Double
    Dim _nr_won_dif As Double
    Dim _played_count_old As Integer
    Dim _played_count_update As Integer
    Dim _played_count_dif As Integer
    Dim _won_count_old As Integer
    Dim _won_count_update As Integer
    Dim _won_count_dif As Integer

    If Not PendingChanges() Then

      Return
    End If

    Try

      Using _db_trx As DB_TRX = New DB_TRX()
        With Me.Grid
          For _idx_row = 0 To .NumRows - 1
            ' Evaluate modified rows
            If IsValidDataRow(_idx_row) Then
              _re_played_dif = 0
              _re_won_dif = 0
              _nr_played_dif = 0
              _nr_won_dif = 0
              _played_count_dif = 0
              _won_count_dif = 0

              _re_played_update = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value)
              If .Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value <> .Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value <> "" AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value <> "" Then
                _re_played_old = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value)
                _re_played_dif = _re_played_update - _re_played_old
              End If

              _re_won_update = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value)
              If .Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value <> .Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value <> "" AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value <> "" Then
                _re_won_old = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value)
                _re_won_dif = _re_won_update - _re_won_old
              End If

              _nr_played_update = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value)
              If .Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value <> .Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value <> "" AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value <> "" Then
                _nr_played_old = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value)
                _nr_played_dif = _nr_played_update - _nr_played_old
              End If

              _nr_won_update = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value)
              If .Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value <> .Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value <> "" AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value <> "" Then
                _nr_won_old = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value)
                _nr_won_dif = _nr_won_update - _nr_won_old
              End If

              _played_count_update = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value)
              If .Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value <> .Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value <> "" AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value <> "" Then
                _played_count_old = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value)
                _played_count_dif = _played_count_update - _played_count_old
              End If

              _won_count_update = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value)
              If .Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value <> .Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value <> "" AndAlso _
                  .Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value <> "" Then
                _won_count_old = GUI_ParseCurrency(.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value)
                _won_count_dif = _won_count_update - _won_count_old
              End If

              If _re_played_dif <> 0 Or _re_won_dif <> 0 Or _nr_played_dif <> 0 Or _nr_won_dif <> 0 Or _
              _played_count_dif <> 0 Or _won_count_dif <> 0 Then
                'Parse Params
                _terminal_id = Int32.Parse(.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value)
                _terminal_name = .Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value
                _session_id = Int64.Parse(.Cell(_idx_row, GRID_COLUMN_SESSION_ID).Value)
                _account_id = Int64.Parse(.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value)
                _account_name = .Cell(_idx_row, GRID_COLUMN_HOLDER_NAME).Value

                ' Update play session
                If Not UpdatePlaySession(_re_played_update, _re_won_update, _nr_played_update, _nr_won_update, _played_count_update, _won_count_update, _session_id, _db_trx.SqlTransaction) Then
                  NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR)

                  Return
                End If

                _bool_plays_update = True
              End If
            End If
          Next
        End With
        ' Commit
        _db_trx.Commit()
      End Using

      ' Auditory
      If _bool_plays_update Then
        Call AuditChanges()

        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108) _
              , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO _
              , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub  ' SaveAmountChanges


  ' PURPOSE: Updates Account
  '
  '  PARAMS:
  '     - INPUT :
  '         TerminalId
  '         TerminalName
  '         SessionId
  '         AccountId
  '         AwardedPoints
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Function UpdateAccount(ByVal TerminalId As Int32, ByVal TerminalName As String, ByVal SessionId As Int64, ByVal AccountId As Int64, _
                                 ByVal AccountName As String, ByVal AwardedPoints As Double, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _am_table As AccountMovementsTable
    Dim _fin_points As Decimal
    Dim _dt_buckets As DataTable


    ' Create Table
    _am_table = New AccountMovementsTable(TerminalId, TerminalName, SessionId)
    ' Update account
    'If Not Trx_UpdateAccountPoints(AccountId, AwardedPoints, True, _fin_points, SqlTrans) Then

    Dim _won_bucket_dict As WonBucketDict
    _won_bucket_dict = New WonBucketDict()

    _won_bucket_dict.Add(Buckets.BucketId.RankingLevelPoints, AwardedPoints, SessionId, AccountId, TerminalId, True)

    _won_bucket_dict.Add(Buckets.BucketId.RankingLevelPoints_Generated, AwardedPoints, SessionId, AccountId, TerminalId, True)

    If (Buckets.BucketsHaveSameConfiguration(Buckets.BucketId.RankingLevelPoints, Buckets.BucketId.RedemptionPoints, SqlTrans)) Then
      _won_bucket_dict.Add(Buckets.BucketId.RedemptionPoints, AwardedPoints, SessionId, AccountId, TerminalId, True)
    End If

    _dt_buckets = BucketsUpdate.FromBucketDictToTable(_won_bucket_dict)

    If Not (BucketsUpdate.BatchUpdateBuckets(_dt_buckets, SqlTrans)) Then
      Return False
    End If

    If Not (BucketsUpdate.CreateMovements(OperationCode.MULTIPLE_BUCKETS_MANUAL, MovementType.MULTIPLE_BUCKETS_Manual_Add, CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD, _dt_buckets, SqlTrans)) Then
      Return False
    End If
    ' Account has activity
    If Not Trx_SetActivity(AccountId, SqlTrans) Then

      Return False
    End If

    Dim _account As String
    _account = AccountId.ToString & " - " & AccountName

    If _fin_points < 0 Then
      If Not m_negative_accounts.Contains(_account) Then
        m_negative_accounts.Add(_account)
      End If
    Else
      If m_negative_accounts.Contains(_account) Then
        m_negative_accounts.Remove(_account)
      End If
    End If

    Return True

  End Function ' UpdateAccount

  ' PURPOSE: Save DataGrid to DB
  '
  '  PARAMS:
  '     - INPUT :
  '       AwardedPoints
  '       AwardedPointsStatus
  '       PlaySessionId
  '       SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Function UpdatePlaySession(ByVal AwardedPoints As Double, ByVal AwardedPointsStatus As Integer, _
                                                 ByVal PlaySessionId As Long, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()

      Using _da As New SqlDataAdapter()

        _sb.AppendLine(" UPDATE   PLAY_SESSIONS ")
        _sb.AppendLine("    SET   PS_AWARDED_POINTS = @pAwardedPoints ")
        _sb.AppendLine("        , PS_AWARDED_POINTS_STATUS = @pAwardedPointsStatus ")
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId")

        Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)
          _cmd.Parameters.Add("@pAwardedPoints", SqlDbType.Money).Value = AwardedPoints
          _cmd.Parameters.Add("@pAwardedPointsStatus", SqlDbType.Int).Value = AwardedPointsStatus
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId

          Return (_cmd.ExecuteNonQuery() = 1)
        End Using
      End Using

      Return False
    Catch ex As Exception

      Return False
    End Try

  End Function ' UpdatePlaySession

  ' PURPOSE: Save DataGrid to DB
  '
  '  PARAMS:
  '     - INPUT :
  '       AwardedPoints
  '       AwardedPointsStatus
  '       PlaySessionId
  '       SqlTrans
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Function UpdatePlaySession(ByVal RePlayed As Double, ByVal ReWon As Double, _
                                      ByVal NrPlayed As Double, ByVal NrWon As Double, _
                                      ByVal PlayedCount As Integer, ByVal WonCount As Integer, _
                                      ByVal PlaySessionId As Long, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _sb As StringBuilder

    Try

      _sb = New StringBuilder()

      Using _da As New SqlDataAdapter()

        _sb.AppendLine(" UPDATE   PLAY_SESSIONS ")
        _sb.AppendLine("    SET   PS_REDEEMABLE_PLAYED_ORIGINAL = ISNULL(PS_REDEEMABLE_PLAYED_ORIGINAL, PS_REDEEMABLE_PLAYED) ")
        _sb.AppendLine("        , PS_REDEEMABLE_WON_ORIGINAL = ISNULL(PS_REDEEMABLE_WON_ORIGINAL, PS_REDEEMABLE_WON) ")
        _sb.AppendLine("        , PS_NON_REDEEMABLE_PLAYED_ORIGINAL = ISNULL(PS_NON_REDEEMABLE_PLAYED_ORIGINAL, PS_NON_REDEEMABLE_PLAYED) ")
        _sb.AppendLine("        , PS_NON_REDEEMABLE_WON_ORIGINAL = ISNULL(PS_NON_REDEEMABLE_WON_ORIGINAL, PS_NON_REDEEMABLE_WON) ")
        _sb.AppendLine("        , PS_PLAYED_COUNT_ORIGINAL = ISNULL(PS_PLAYED_COUNT_ORIGINAL, PS_PLAYED_COUNT) ")
        _sb.AppendLine("        , PS_WON_COUNT_ORIGINAL = ISNULL(PS_WON_COUNT_ORIGINAL, PS_WON_COUNT) ")

        _sb.AppendLine("        , PS_REDEEMABLE_PLAYED = @RePlayed ")
        _sb.AppendLine("        , PS_REDEEMABLE_WON = @ReWon ")
        _sb.AppendLine("        , PS_NON_REDEEMABLE_PLAYED = @NrPlayed ")
        _sb.AppendLine("        , PS_NON_REDEEMABLE_WON = @NrWon ")
        _sb.AppendLine("        , PS_PLAYED_COUNT = @PlayedCount ")
        _sb.AppendLine("        , PS_WON_COUNT = @WonCount ")

        _sb.AppendLine("        , PS_PLAYED_AMOUNT = @RePlayed + @NrPlayed ")
        _sb.AppendLine("        , PS_WON_AMOUNT = @ReWon + @NrWon ")
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId")

        Using _cmd As New SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)
          _cmd.Parameters.Add("@RePlayed", SqlDbType.Money).Value = RePlayed
          _cmd.Parameters.Add("@ReWon", SqlDbType.Money).Value = ReWon
          _cmd.Parameters.Add("@NrPlayed", SqlDbType.Money).Value = NrPlayed
          _cmd.Parameters.Add("@NrWon", SqlDbType.Money).Value = NrWon
          _cmd.Parameters.Add("@PlayedCount", SqlDbType.Money).Value = PlayedCount
          _cmd.Parameters.Add("@WonCount", SqlDbType.Money).Value = WonCount
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId

          Return (_cmd.ExecuteNonQuery() = 1)
        End Using
      End Using

      Return False
    Catch ex As Exception

      Return False
    End Try

  End Function ' UpdatePlaySession

  ' PURPOSE: Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AuditChanges()

    Dim _idx_row As Integer
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _orig_awarded_value As String
    Dim _curr_awarded_value As String
    Dim _old_value As String
    Dim _current_value As String
    Dim _total_calculated As Double

    _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)
    _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    ' Set Name and Identifier
    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
      Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), "")
      Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), "")
    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
      _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAY_SESSIONS)
      _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_PLAY_SESSIONS)
      Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), "")
      Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), "")
    End If

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
        ' Column Awarded Points
        _curr_awarded_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value)

        _orig_awarded_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value)

        If (_curr_awarded_value <> _orig_awarded_value) Then
          If IsValidDataRow(_idx_row) Then
            ' Awarded Points
            _curr_awarded_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                  Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                  _curr_awarded_value
            If Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_COMPUTED_POINTS).Value Then
              _curr_awarded_value = _curr_awarded_value & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2803)
            End If
            If Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value = AwardPointsStatus.Pending Then
              _orig_awarded_value = AUDIT_NONE_STRING
            End If
            Call _curr_auditor.SetField(0, _curr_awarded_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2802))
            Call _orig_auditor.SetField(0, _orig_awarded_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2802))
            ' Update Grid
            Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value
            Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value
          End If
        End If
      ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
        ' RE Played
        _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value)

        _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value)

        If (_current_value <> _old_value AndAlso _old_value <> "") AndAlso IsValidDataRow(_idx_row) Then
          _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                _current_value
          Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4740))
          Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4740))
        End If

        ' NR Played
        _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value)

        _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value)

        If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
          _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                _current_value
          Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4742))
          Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4742))
        End If

        ' Total Played
        If Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value <> "" Or Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value <> "" Then
          _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value), _
                AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value)

          _total_calculated = GUI_ParseCurrency(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value) + GUI_ParseCurrency(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value)

          _old_value = IIf(IsDBNull(GUI_FormatCurrency(_total_calculated, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)), _
              AUDIT_NONE_STRING, GUI_FormatCurrency(_total_calculated, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

          If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
            _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                  Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                  _current_value
            Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4755))
            Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4755))
          End If
        End If

        ' RE Won
        _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value)

        _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value)

        If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
          _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                _current_value
          Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4741))
          Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4741))
        End If

        ' NR Won
        _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value)

        _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value)

        If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
          _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                _current_value
          Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4743))
          Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4743))
        End If

        ' Total Won
        If Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value <> "" Or Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value <> "" Then
          _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value), _
                        AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_AMOUNT).Value)

          _total_calculated = GUI_ParseCurrency(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value) + GUI_ParseCurrency(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value)

          _old_value = IIf(IsDBNull(GUI_FormatCurrency(_total_calculated, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)), _
              AUDIT_NONE_STRING, GUI_FormatCurrency(_total_calculated, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

          If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
            _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                  Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                  _current_value
            Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4756))
            Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4756))
          End If
        End If

        ' Count Played
        _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value)

        _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value)

        If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
          _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                _current_value
          Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4744))
          Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4744))
        End If

        ' Count Won
        _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value)

        _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value), _
            AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value)

        If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
          _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
                                _current_value
          Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4745))
          Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4745))
        End If

        ' Update Grid
        Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value = ""
        Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value = ""
        Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value = ""
        Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value = ""
        Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value = ""
        Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value = ""
      End If

    Next ' For _idx_row

    ' Notify
    Call _curr_auditor.Notify(CurrentUser.GuiId, _
                                  CurrentUser.Id, _
                                  CurrentUser.Name, _
                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                  0, _
                                  _orig_auditor)

    _curr_auditor = Nothing
    _orig_auditor = Nothing

  End Sub ' AuditChanges

  'Private Sub AuditChangesTitoMode()

  '  Dim _idx_row As Integer
  '  Dim _curr_auditor As CLASS_AUDITOR_DATA
  '  Dim _orig_auditor As CLASS_AUDITOR_DATA
  '  Dim _orig_awarded_value As String
  '  Dim _curr_awarded_value As String
  '  Dim _old_value As String
  '  Dim _current_value As String

  '  _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)
  '  _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

  '  ' Set Name and Identifier
  '  If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
  '    Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), "")
  '    Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), "")
  '  ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
  '    Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), "")
  '    Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), "")
  '  End If

  '  For _idx_row = 0 To Me.Grid.NumRows - 1
  '    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
  '      ' Column Awarded Points
  '      _curr_awarded_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_TITO_MODE).Value)

  '      _orig_awarded_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL_TITO_MODE).Value)

  '      If (_curr_awarded_value <> _orig_awarded_value) Then
  '        If IsValidDataRow(_idx_row) Then
  '          ' Awarded Points
  '          _curr_awarded_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                                _curr_awarded_value
  '          If Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_TITO_MODE).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_COMPUTED_POINTS_TITO_MODE).Value Then
  '            _curr_awarded_value = _curr_awarded_value & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2803)
  '          End If
  '          If Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL_TITO_MODE).Value = AwardPointsStatus.Pending Then
  '            _orig_awarded_value = AUDIT_NONE_STRING
  '          End If
  '          Call _curr_auditor.SetField(0, _curr_awarded_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2802))
  '          Call _orig_auditor.SetField(0, _orig_awarded_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2802))
  '          ' Update Grid
  '          Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL_TITO_MODE).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_TITO_MODE).Value
  '          Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL_TITO_MODE).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_TITO_MODE).Value
  '        End If
  '      End If
  '    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
  '      ' RE Played
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_TITO_MODE).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD_TITO_MODE).Value)

  '      If (_current_value <> _old_value AndAlso _old_value <> "") AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4740))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4740))
  '      End If

  '      ' RE Won
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_TITO_MODE).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD_TITO_MODE).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4741))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4741))
  '      End If

  '      ' NR Played
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_TITO_MODE).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD_TITO_MODE).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4742))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4742))
  '      End If

  '      ' NR Won
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_TITO_MODE).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD_TITO_MODE).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4743))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4743))
  '      End If

  '      ' Count Played
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_TITO_MODE).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD_TITO_MODE).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4744))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4744))
  '      End If

  '      ' Count Won
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_TITO_MODE).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD_TITO_MODE).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD_TITO_MODE).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED_TITO_MODE).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID_TITO_MODE).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4745))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4745))
  '      End If

  '      ' Update Grid
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD_TITO_MODE).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD_TITO_MODE).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD_TITO_MODE).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD_TITO_MODE).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD_TITO_MODE).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD_TITO_MODE).Value = ""
  '    End If

  '  Next ' For _idx_row

  '  ' Notify
  '  Call _curr_auditor.Notify(CurrentUser.GuiId, _
  '                                CurrentUser.Id, _
  '                                CurrentUser.Name, _
  '                                CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
  '                                0, _
  '                                _orig_auditor)

  '  _curr_auditor = Nothing
  '  _orig_auditor = Nothing

  'End Sub ' AuditChangesTitoMode

  'Private Sub AuditChangesNoneTitoMode()

  '  Dim _idx_row As Integer
  '  Dim _curr_auditor As CLASS_AUDITOR_DATA
  '  Dim _orig_auditor As CLASS_AUDITOR_DATA
  '  Dim _orig_awarded_value As String
  '  Dim _curr_awarded_value As String
  '  Dim _old_value As String
  '  Dim _current_value As String

  '  _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)
  '  _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

  '  ' Set Name and Identifier
  '  If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
  '    Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), "")
  '    Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(2690), "")
  '  ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
  '    Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), "")
  '    Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4739), "")
  '  End If

  '  For _idx_row = 0 To Me.Grid.NumRows - 1
  '    If Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_POINTS_DATA Then
  '      ' Column Awarded Points
  '      _curr_awarded_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value)

  '      _orig_awarded_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value)

  '      If (_curr_awarded_value <> _orig_awarded_value) Then
  '        If IsValidDataRow(_idx_row) Then
  '          ' Awarded Points
  '          _curr_awarded_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                                Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                                _curr_awarded_value
  '          If Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_COMPUTED_POINTS).Value Then
  '            _curr_awarded_value = _curr_awarded_value & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2803)
  '          End If
  '          If Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value = AwardPointsStatus.Pending Then
  '            _orig_awarded_value = AUDIT_NONE_STRING
  '          End If
  '          Call _curr_auditor.SetField(0, _curr_awarded_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2802))
  '          Call _orig_auditor.SetField(0, _orig_awarded_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2802))
  '          ' Update Grid
  '          Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_ORIGINAL).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS).Value
  '          Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS_ORIGINAL).Value = Me.Grid.Cell(_idx_row, GRID_COLUMN_AWARDED_POINTS_STATUS).Value
  '        End If
  '      End If
  '    ElseIf Me.m_screen_mode = ENUM_PLAY_SESSIONS_SCREEN_MODE.MODE_SESSION_EDIT Then
  '      ' RE Played
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value)

  '      If (_current_value <> _old_value AndAlso _old_value <> "") AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4740))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4740))
  '      End If

  '      ' RE Won
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4741))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4741))
  '      End If

  '      ' NR Played
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4742))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4742))
  '      End If

  '      ' NR Won
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4743))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4743))
  '      End If

  '      ' Count Played
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4744))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4744))
  '      End If

  '      ' Count Won
  '      _current_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT).Value)

  '      _old_value = IIf(IsDBNull(Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value), _
  '          AUDIT_NONE_STRING, Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value)

  '      If (_current_value <> _old_value) AndAlso _old_value <> "" AndAlso IsValidDataRow(_idx_row) Then
  '        _current_value = Me.Grid.Cell(_idx_row, GRID_COLUMN_STARTED).Value & " - " & _
  '                              Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_ID).Value & " - " & _
  '                              _current_value
  '        Call _curr_auditor.SetField(0, _current_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4745))
  '        Call _orig_auditor.SetField(0, _old_value, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4745))
  '      End If

  '      ' Update Grid
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_PLAYED_AMOUNT_OLD).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_RE_WON_AMOUNT_OLD).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_PLAYED_AMOUNT_OLD).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_NRE_WON_AMOUNT_OLD).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_PLAYED_COUNT_OLD).Value = ""
  '      Me.Grid.Cell(_idx_row, GRID_COLUMN_WON_COUNT_OLD).Value = ""
  '    End If
  '  Next ' For _idx_row

  '  ' Notify
  '  Call _curr_auditor.Notify(CurrentUser.GuiId, _
  '                                CurrentUser.Id, _
  '                                CurrentUser.Name, _
  '                                CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
  '                                0, _
  '                                _orig_auditor)

  '  _curr_auditor = Nothing
  '  _orig_auditor = Nothing

  'End Sub ' AuditChangesNoneTitoMode

  ' PURPOSE: Set Backcolor cells
  '
  '  PARAMS:
  '     - INPUT:
  '           - IndexRow
  '           - Color
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetRowBackColor(ByVal RowIndex As Integer, ByVal Color As GUI_CommonMisc.ControlColor.ENUM_GUI_COLOR)
    For _index As Integer = 0 To Me.m_number_of_columns - 1
      Me.Grid.Cell(RowIndex, _index).BackColor = GetColor(Color)
    Next
  End Sub ' SetRowBackColor

  ' PURPOSE: Check conditions to set the "better" index for the query
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - The string to use the index for the SQL query
  Private Function GetSQLIndex() As String
    Dim _str_index As String

    '-	Si seleccionada Sesiones abiertas IX_ps_started o Si seleccionada fecha de inicio IX_ps_started
    _str_index = INDEX_DEFAULT

    '-	Si seleccionada fecha de fin IX_ps_finished_status
    If Me.rb_finish_date.Checked Then
      _str_index = INDEX_FINISHED
    End If

    '-	Si esta seleccionado el estado distinto de �Cerrada� se utiliza el IX_ps_status (tanto fecha inicio como fin)
    If Me.cmb_status.Enabled AndAlso _
       Me.cmb_status.Value <> WSI.Common.PlaySessionStatus.Closed Then
      _str_index = INDEX_STATUS
    End If

    '-	Si hay terminales seleccionados y seleccionada fecha de inicio se utiliza el IX_ps_started_terminal_id
    If Not Me.uc_pr_list.AllSelectedChecked AndAlso _
             Me.uc_pr_list.TerminalListHasValues Then
      If Me.rb_finish_date.Checked Then
        _str_index = INDEX_FINISHED
      Else
        _str_index = INDEX_TERMINALS
      End If
    End If

    '-	Si hay cuentas seleccionadas y fecha de inicio se utiliza el IX_ps_account_id_started o si seleccionada fecha de fin se utiliza el IX_ps_account_id_finished
    If Not String.IsNullOrEmpty(Me.uc_account_sel1.GetFilterSQL()) Then
      If Me.rb_finish_date.Checked Then
        _str_index = INDEX_ACCOUNT_FINISHED
      Else
        _str_index = INDEX_ACCOUNT_STARTED
      End If
    End If

    Return _str_index
  End Function

  ' PURPOSE: Set member variable m_is_tito_mode to determine overall form functionallity
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS: 
  '     - None
  Private Sub SetColumnsIndex()

    If Me.m_is_tito_mode Then
      Call SetGridTitoColumns()
    Else
      Call SetGridColumns()
    End If

    Me.m_number_of_columns = GRID_NUM_COLUMNS

  End Sub

  Private Sub GridColumnReIndex()

    m_terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)

    TERMINAL_DATA_COLUMNS = m_terminal_columns.Count

    For _idx As Int32 = 0 To m_terminal_columns.Count - 1
      If m_terminal_columns(_idx).Column = ReportColumn.Provider Then
        m_provider_col = _idx
      End If
      If m_terminal_columns(_idx).Column = ReportColumn.Terminal Then
        m_terminal_col = _idx
      End If
    Next

    GRID_INIT_TERMINAL_DATA = GRID_COLUMN_GROUPED_A1
    GRID_COLUMN_PROVIDER_ID = GRID_INIT_TERMINAL_DATA + m_provider_col
    GRID_COLUMN_TERMINAL_NAME = GRID_INIT_TERMINAL_DATA + m_terminal_col
    GRID_COLUMN_TERMINAL_TYPE_NAME = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS
    GRID_COLUMN_STARTED = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_STATUS = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_TIME = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_FINISHED = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_ACCOUNT_ID = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_HOLDER_NAME = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS + 6

    If Me.chk_grouped.Checked AndAlso Me.chk_grouped.Visible Then
      If Me.opt_account.Checked Then
        GRID_COLUMN_ACCOUNT_ID = GRID_COLUMN_GROUPED_A1
        GRID_COLUMN_HOLDER_NAME = GRID_COLUMN_GROUPED_A2
        GRID_COLUMN_STARTED = GRID_COLUMN_GROUPED_A3
        GRID_COLUMN_STATUS = GRID_COLUMN_GROUPED_A4
        GRID_COLUMN_TIME = GRID_COLUMN_GROUPED_A5
        GRID_COLUMN_FINISHED = GRID_COLUMN_GROUPED_A6
        GRID_INIT_TERMINAL_DATA = GRID_COLUMN_GROUPED_A7
        GRID_COLUMN_PROVIDER_ID = GRID_INIT_TERMINAL_DATA + m_provider_col
        GRID_COLUMN_TERMINAL_NAME = GRID_INIT_TERMINAL_DATA + m_terminal_col
        GRID_COLUMN_TERMINAL_TYPE_NAME = GRID_INIT_TERMINAL_DATA + TERMINAL_DATA_COLUMNS
      End If
    End If
  End Sub

#End Region  ' Private Functions

#Region "Events"

  Private Sub opt_several_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_status.Click
    Me.cmb_status.Enabled = True
  End Sub

  Private Sub opt_all_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_status.Click
    Me.cmb_status.Enabled = False
  End Sub

  Private Sub chk_grouped_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_grouped.CheckedChanged
    Me.opt_provider.Enabled = Me.chk_grouped.Checked
    Me.opt_account.Enabled = Me.chk_grouped.Checked
    Me.chk_subtotal.Enabled = Me.chk_grouped.Checked
    Me.chk_show_sessions.Enabled = Me.chk_grouped.Checked
  End Sub

  Private Sub opt_provider_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_provider.CheckedChanged
    If Me.opt_provider.Checked Then
      Me.chk_subtotal.Text = GLB_NLS_GUI_INVOICING.GetString(371, GLB_NLS_GUI_INVOICING.GetString(424))
    End If
  End Sub

  Private Sub opt_account_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_account.CheckedChanged
    If Me.opt_account.Checked Then
      Me.chk_subtotal.Text = GLB_NLS_GUI_INVOICING.GetString(371, GLB_NLS_GUI_INVOICING.GetString(423))
    End If
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

  ' PURPOSE: Get all events for dates filter. Filter is set depending on the selected option
  '
  '  PARAMS:
  '     - INPUT:
  '           - Sender
  '           - EventArgs
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '           - None
  Private Sub rb_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _rb As RadioButton = sender

    RemoveHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    RemoveHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

    Select Case _rb.Text
      Case GLB_NLS_GUI_STATISTICS.GetString(345) ' 345 - Open Sessions
        Me.rb_open_sessions.Checked = True
        Me.rb_finish_date.Checked = False
        Me.rb_historic.Checked = False
        Me.rb_ini_date.Checked = False
        Me.gb_status.Enabled = False
        Me.opt_all_status.Checked = True
        Me.dtp_from.Enabled = False
        Me.dtp_to.Enabled = False
      Case GLB_NLS_GUI_CONTROLS.GetString(331) ' 331 - History
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.rb_ini_date.Checked = True
        Me.rb_finish_date.Checked = False
        Me.gb_status.Enabled = True
        Me.dtp_to.Enabled = True
        Me.dtp_from.Enabled = True
      Case GLB_NLS_GUI_STATISTICS.GetString(381) ' 381 - Started
        Me.rb_ini_date.Checked = True
        Me.rb_finish_date.Checked = False
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.gb_status.Enabled = True
        Me.dtp_to.Enabled = True
        Me.dtp_from.Enabled = True
      Case GLB_NLS_GUI_PLAYER_TRACKING.GetString(5704) ' 5704 - Finished
        Me.rb_finish_date.Checked = True
        Me.rb_historic.Checked = True
        Me.rb_open_sessions.Checked = False
        Me.rb_ini_date.Checked = False
        Me.gb_status.Enabled = True
        Me.dtp_from.Enabled = True
        Me.dtp_to.Enabled = True
    End Select

    AddHandler rb_open_sessions.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_historic.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_ini_date.Click, AddressOf rb_date_CheckedChanged
    AddHandler rb_finish_date.Click, AddressOf rb_date_CheckedChanged

  End Sub ' rb_date_CheckedChanged

#End Region ' Events

End Class