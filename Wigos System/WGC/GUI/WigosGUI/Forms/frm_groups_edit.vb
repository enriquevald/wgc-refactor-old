'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_groups_edit
'
' DESCRIPTION : Groups editor
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-JUN-2013  RBG    Initial version
' 14-OCT-2013  ANG    Fix bug WIG-64
' 11-DEC-2013  AMF    Fix bug WIG-476
' 27-MAR-2014  MPO    Performance improved for terminal generations
' 27-MAR-2014  DCS    Update exploit with the new development of cloned terminals
' 12-MAY-2015  DLL    WIG-2182: Error when try to create an all group of terminals
' 17-AUG-2015  YNM    Fixed Bug TFS-3580: added Terminal_id to m_elements from uc_terminals_group_filter
'--------------------------------------------------------------------
Imports System.Text

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Controls.uc_terminals_group_filter
Imports WSI.Common.Groups

Public Class frm_groups_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const GROUP_INFO_COLUMN_PARENT = "PARENT"
  Private Const GROUP_INFO_COLUMN_CHILD = "CHILD"
  Private Const GROUP_INFO_COLUMN_TYPE = "TYPE"

  Private Const FORM_DB_MIN_VERSION As Short = 149

#End Region ' Constants

#Region " Members "

  Private m_input_group_name As String            ' Readed Group's name
  Private m_group_modif As DataTable              ' Table where we note the current changes of elements in the group
  Private m_terminals As DataTable                ' Table where we note the current list of terminals
  Private m_delete_operation As Boolean           ' Shows if the we are deleting a group
  Private m_groups_disabled As List(Of Int64)     ' Save id of disabled groups.

  Private m_thread_terminals As System.Threading.Thread
  Private m_thread_elements As System.Threading.Thread

#End Region ' Members

#Region "Properties"

#End Region ' Properties

#Region "Delegate"

  Private Delegate Sub DelegatesNoParams()
  Private Delegate Sub DelegatesParam(ByVal Param As Object)

#End Region ' Delegate

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GROUPS_EDIT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim _dt_root As DataTable

    _dt_root = Nothing
    m_group_modif = CreateElementTable(False)

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_CONTROLS.GetString(462)

    ' Delete button is not available
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' Set Labels
    ef_group_name.Text = GLB_NLS_GUI_CONTROLS.GetString(461)
    ef_group_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_GROUP.MAX_NAME_LEN)

    xElements.Text = GLB_NLS_GUI_CONTROLS.GetString(463)
    xTerminals.Text = GLB_NLS_GUI_CONTROLS.GetString(464)
    xDescription.Text = GLB_NLS_GUI_CONTROLS.GetString(473)

    gb_enabled.Text = GLB_NLS_GUI_CONTROLS.GetString(281)
    opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    lbl_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1220)
    lbl_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
    lbl_inserted.Text = GLB_NLS_GUI_INVOICING.GetString(215)
    gb_status.Text = GLB_NLS_GUI_SYSTEM_MONITOR.GetString(205)

    ' Default form values
    RefreshInfo(Nothing, Nothing)

    ' Set listview propery to display terminals list without column header
    lv_terminals.HeaderStyle = ColumnHeaderStyle.None
    lv_terminals.Items.Clear()

    ' Resolve bug: end of the text on some nodes is cut off
    uc_group_list.Font = New Font(uc_group_list.Font, FontStyle.Bold)

  End Sub ' GUI_InitControls

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = GUI_CommonOperations.CLASS_BASE.ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = GUI_CommonOperations.CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal GroupId As Int64, ByVal GroupName As String)
    Me.m_input_group_name = GroupName

    MyBase.ShowEditItem(GroupId)

  End Sub 'ShowEditItem

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _group As CLASS_GROUP
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Min database version popup alerdy showed. Don't show any message again
      Exit Sub
    End If


    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_GROUP
        _group = DbEditedObject
        _group.ContentType = WSI.Common.GROUP_CONTENT_TYPE.TERMINALS
        _group.Type = GROUP_NODE_ELEMENT.GROUP
        _group.Name = ""
        _group.Updated = WSI.Common.WGDB.Now
        _group.Content = CreateElementTable(False)

        DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          ' "Se ha producido un error al leer el grupo %1."
          _group = Me.DbEditedObject
          _aux_nls = Me.m_input_group_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(162), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' "Ya existe una grupo con el nombre %1."
          _group = Me.DbEditedObject
          _aux_nls = _group.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(163), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_group_name.Focus()

        ElseIf Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          ' "Se ha producido un error al a�adir el grupo %1."
          _group = Me.DbEditedObject
          _aux_nls = _group.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(164), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' "Ya existe una grupo con el nombre %1."
          _group = Me.DbEditedObject
          _aux_nls = _group.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(163), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_group_name.Focus()

        ElseIf Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          ' "Se ha producido un error al modificar el grupo %1."
          _group = Me.DbEditedObject
          _aux_nls = _group.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(165), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        ' "�Est� seguro que quiere borrar el grupo %1?"
        _group = Me.DbEditedObject
        _aux_nls = _group.Name
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(167), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _aux_nls)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' "Se ha producido un error al borrar el grupo %1."
          _group = Me.DbEditedObject
          _aux_nls = _group.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(167), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _group As CLASS_GROUP

    Try

      _group = DbReadObject

      ef_group_name.Value = _group.Name
      rtb_description.Text = _group.Description
      opt_enabled.Checked = _group.Enabled
      opt_disabled.Checked = Not _group.Enabled

      ' Copy de data table to note modifications of the list
      m_group_modif = _group.Content.Copy
      m_group_modif.AcceptChanges()
      Me.uc_group_list.ForbiddenGroups = CalculateForbiddenGroups(_group.GroupId)
      Me.uc_group_list.SetElementList(m_group_modif)

      m_groups_disabled = New List(Of Int64)()

      FillElements()

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim _group As CLASS_GROUP

    _group = DbEditedObject

    If Not _group Is Nothing Then

      _group.Name = ef_group_name.Value
      _group.Content = m_group_modif
      _group.Description = rtb_description.Text
      _group.Enabled = opt_enabled.Checked

    End If

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If ef_group_name.Value = "" Then
      ' "Debe introducir un valor para %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_group_name.Text)
      Call ef_group_name.Focus()

      Return False
    End If

    If lv_elements.Items.Count = 0 Then
      ' "Debe agregar al menos un elemento a la lista."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2597), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , xElements.Text)
      Call lv_elements.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Disable some form elements to prevent forbiden actions 
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DisableFormElements()

    ef_group_name.Enabled = False
    rtb_description.Enabled = False
    btn_add.Enabled = False
    btn_rmv.Enabled = False
    btn_rvm_all.Enabled = False
    lv_elements.Enabled = False
    lv_terminals.Enabled = False

  End Sub ' DisableFormElements

  ' PURPOSE: Enable all form elements
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub EnableFormElements()

    ef_group_name.Enabled = True
    rtb_description.Enabled = True
    btn_add.Enabled = True
    btn_rmv.Enabled = True
    btn_rvm_all.Enabled = True
    lv_elements.Enabled = True
    lv_terminals.Enabled = True

  End Sub ' EnableFormElements

  ' PURPOSE: Calculation of groups that can produce cicles in opened group
  '
  '    - INPUT:
  '         - GroupId Int64: Id from database of the group opened
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function CalculateForbiddenGroups(ByVal GroupId As Int64) As Collection

    Dim _search_tree As TreeNode
    Dim _cl_forbidden_groups As Collection
    Dim _dt As DataTable
    Dim _parent_id As Int64
    Dim _child_id As Int64
    Dim _type As WSI.Common.GROUP_ELEMENT_TYPE
    Dim _opened_group_key As String
    Dim _root_key As String
    Dim _group_id As Int64

    _cl_forbidden_groups = New Collection()

    Try

      _root_key = "-1"
      _opened_group_key = GroupId.ToString()
      _dt = Nothing
      _search_tree = New TreeNode(_root_key)

      ' Add Groups to search tree
      If GetElements(GROUP_NODE_ELEMENT.GROUP, -1, _dt) Then
        For Each _row As DataRow In _dt.Rows
          _group_id = CType(_row(ELEMENTS_COLUMN_ID), Int64)
          _search_tree.Nodes.Add(_group_id.ToString(), _group_id.ToString())
        Next
      End If

      ' Add group childs to search tree
      If GetGroupsInfo(_dt) Then

        ' Create seach group tree
        For Each _row As DataRow In _dt.Rows

          _parent_id = CType(_row(GROUP_INFO_COLUMN_PARENT), Int64)
          _child_id = CType(_row(GROUP_INFO_COLUMN_CHILD), Int64)
          _type = CType(_row(GROUP_INFO_COLUMN_TYPE), WSI.Common.GROUP_ELEMENT_TYPE)

          If _type = WSI.Common.GROUP_ELEMENT_TYPE.GROUP And _search_tree.Nodes.ContainsKey(_parent_id.ToString()) Then
            _search_tree.Nodes(_parent_id.ToString()).Nodes.Add(_child_id.ToString(), _child_id.ToString())
          End If

        Next

        ' Calculate all forbidden groups using search tree

        ' 1. Add opened group
        _cl_forbidden_groups.Add(Convert.ToInt64(_opened_group_key), _opened_group_key)

        ' 2. Add parents of opened group
        AddParentsFromSearchTree(_opened_group_key, _cl_forbidden_groups, _search_tree)

        ' 3. Add groups that have some parent included in opened group
        For Each _group As TreeNode In _search_tree.Nodes

          ' Do nothing if the group has already been added
          If _cl_forbidden_groups.Contains(GroupId) Then
            Continue For
          End If

          If (CheckChildsFromSearchTree(_group.Name, _cl_forbidden_groups, _search_tree)) Then
            _cl_forbidden_groups.Add(Convert.ToInt64(_group.Name), _group.Name)
          End If
        Next

      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

    Return _cl_forbidden_groups

  End Function ' CalculateForbiddenGroups

  ' PURPOSE: Check if some group of a group tree is in a collection
  '
  '    - INPUT:
  '         - GroupId String: Root of the branch
  '         - Groups Collection: Colection to search in tree
  '         - SearchTree TreeNode: All groups organized as a tree
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function CheckChildsFromSearchTree(ByVal GroupId As String, ByRef Groups As Collection, ByVal SearchTree As TreeNode) As Boolean

    If Groups.Contains(GroupId) Then
      Return True
    Else
      For Each _group As TreeNode In SearchTree.Nodes(GroupId).Nodes
        If CheckChildsFromSearchTree(_group.Name, Groups, SearchTree) Then
          Return True
        End If
      Next
    End If

    Return False

  End Function ' CheckChildsFromSearchTree

  ' PURPOSE: Calculate all parents from group
  '
  '    - INPUT:
  '         - GroupId String: the group id
  '         - Parents Collection: Parents of the group
  '         - SearchTree TreeNode: All groups organized as a tree
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function AddParentsFromSearchTree(ByVal GroupId As String, ByRef Parents As Collection, ByVal SearchTree As TreeNode) As Boolean

    Dim _key As String
    Dim _nodes As TreeNode()

    _nodes = SearchTree.Nodes.Find(GroupId, True)

    For Each _node As TreeNode In _nodes

      _key = _node.Parent.Name

      If (_key <> SearchTree.Name And Not Parents.Contains(_key)) Then
        Parents.Add(Convert.ToInt64(_key), _key)
      End If

      AddParentsFromSearchTree(_key, Parents, SearchTree)

    Next

  End Function


  ' PURPOSE: Returns info from GROUP_ELEMENTS Table
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '     - DataTable
  Private Function GetGroupsInfo(ByRef DtGroups As DataTable) As Boolean

    Dim _sb As StringBuilder

    DtGroups = New DataTable()

    DtGroups.Columns.Add(GROUP_INFO_COLUMN_PARENT, Type.GetType("System.Int64"))
    DtGroups.Columns.Add(GROUP_INFO_COLUMN_CHILD, Type.GetType("System.Int64"))
    DtGroups.Columns.Add(GROUP_INFO_COLUMN_TYPE, Type.GetType("System.Int32"))

    _sb = New StringBuilder()

    Try

      _sb.AppendLine(" SELECT   GE_GROUP_ID AS " & GROUP_INFO_COLUMN_PARENT)
      _sb.AppendLine("        , GE_ELEMENT_ID AS " & GROUP_INFO_COLUMN_CHILD)
      _sb.AppendLine("        , GE_ELEMENT_TYPE AS " & GROUP_INFO_COLUMN_TYPE)
      _sb.AppendLine("   FROM   GROUP_ELEMENTS")

      Using _db_trx As New WSI.Common.DB_TRX()

        Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          Using _da As New SqlClient.SqlDataAdapter(_sql_cmd)

            _da.Fill(DtGroups)

          End Using

        End Using

      End Using

      Return True

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

    Return False

  End Function ' GetGroupsInfo


  ' PURPOSE: Thread to add elements and terminals to corresponding lists
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillElements()

    Dim _group As CLASS_GROUP
    Dim _type As GROUP_NODE_ELEMENT
    Dim _params As INPUT_GROUP_PARAMS
    Dim _item As ListViewItem
    Dim _enabled As Boolean

    _group = DbReadObject

    lv_elements.Items.Clear()

    ' Add elements to list
    For Each _row As DataRow In _group.Content.Rows

      _type = CType(_row(ELEMENTS_COLUMN_NODE_TYPE), GROUP_NODE_ELEMENT)
      _enabled = CType(_row(ELEMENTS_COLUMN_ENABLED), GROUP_NODE_ELEMENT)

      Dim _node As New ItemGroup()
      _node.ElementType = _type
      _node.ElementId = CType(_row(ELEMENTS_COLUMN_ID), Int64)
      _node.ElementName = _row(ELEMENTS_COLUMN_NAME).ToString
      _node.NodeFont = New Font(uc_group_list.Tree.Font, FontStyle.Regular)
      _node.ElementEnabled = _enabled

      _item = lv_elements.Items.Add(_node.Text)
      _item.Tag = _node

      If _node.ElementType = GROUP_NODE_ELEMENT.GROUPS AndAlso (_node.ElementEnabled = False) Then
        m_groups_disabled.Add(_node.ElementId)
      End If

      ' Change format if item is disabled 
      ChangeEstateListviewElement(_item)

    Next

    ' Add terminals to list
    _params = New INPUT_GROUP_PARAMS()
    _params.ElementEnabled = True
    _params.ElementId = _group.GroupId
    _params.ElementType = WSI.Common.GROUP_ELEMENT_TYPE.GROUP
    If (GetExploitElements(_params, m_terminals)) Then

      m_thread_terminals = New Threading.Thread(AddressOf InsertTerminals)
      m_thread_terminals.Start(True)

    End If

  End Sub ' FillElements

  ' PURPOSE: Remove all elements in the list
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CleanElements()

    Call uc_group_list.DeselectAllElements()

    ' Remove all terminals
    m_terminals.Rows.Clear()
    lv_terminals.Items.Clear()

    ' Remove all elements
    lv_elements.Items.Clear()
    m_group_modif.RejectChanges()

    m_groups_disabled.Clear()

    For Each _row As DataRow In m_group_modif.Rows

      _row.Delete()

    Next

  End Sub ' CleanElements

  ' PURPOSE: Get if node 'ALL' is added to element's list
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS: Boolean
  '
  Private Function IsItemAllAdded() As Boolean

    Dim _item As ItemGroup

    If lv_elements.Items.Count = 1 Then

      _item = CType(lv_elements.Items(0).Tag, ItemGroup)
      If _item.ElementType = GROUP_NODE_ELEMENT.ALL Then
        Return True
      End If

    End If

    Return False

  End Function ' IsItemAllAdded

  ' PURPOSE: Creates a thread to add a node to element's list
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS: 
  '
  Private Sub AddTreeNodeToList(ByVal SelectedElement As ItemGroup)

    m_thread_elements = New Threading.Thread(AddressOf AddTreeNodeToListThread)
    m_thread_elements.Start(SelectedElement)

  End Sub ' AddTreeNodeToList

  ' PURPOSE: Add a tree node to element's list
  '
  '    - INPUT:
  '         - SelectedElement ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddTreeNodeToListThread(ByVal SelectedElement As Object)

    If Me.InvokeRequired Then
      Dim _inv As DelegatesParam
      _inv = New DelegatesParam(AddressOf AddTreeNodeToListThread)
      Me.Invoke(_inv, New Object() {SelectedElement})
    Else

      Dim _rc As mdl_NLS.ENUM_MB_RESULT
      Dim _selected_element As ItemGroup = CType(SelectedElement, ItemGroup)

      ' If ALL element is added then any other element can't be added
      If IsItemAllAdded() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(171), _
                            ENUM_MB_TYPE.MB_TYPE_INFO, _
                            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Return
      End If

      Select Case _selected_element.ElementType

        Case GROUP_NODE_ELEMENT.GROUP, _
             GROUP_NODE_ELEMENT.PROVIDER, _
             GROUP_NODE_ELEMENT.ZONE

          ' First ask the user
          _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(170), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                         GetTypeTitle(_selected_element.ElementType, False))

          If _rc <> ENUM_MB_RESULT.MB_RESULT_YES Then
            Return
          End If

          ' Calculate subelements of tree node root
          If Not _selected_element.ElementExpanded Then
            Me.uc_group_list.AddChildNodes(_selected_element)
          End If

          ' Add all child elements to list
          For Each _node As ItemGroup In _selected_element.Nodes
            Call AddNewElement(_node)
          Next

        Case GROUP_NODE_ELEMENT.ALL

          _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(169), _
               ENUM_MB_TYPE.MB_TYPE_WARNING, _
               ENUM_MB_BTN.MB_BTN_YES_NO, _
               ENUM_MB_DEF_BTN.MB_DEF_BTN_2)

          If _rc <> ENUM_MB_RESULT.MB_RESULT_YES Then
            Return
          End If

          ' Remove all elements
          Call CleanElements()
          ' Add element to list and focus on it
          Call AddNewElement(_selected_element)

        Case Else

          ' Add element to list and focus on it
          Call AddNewElement(_selected_element)

      End Select

      m_thread_terminals = New Threading.Thread(AddressOf InsertTerminals)
      m_thread_terminals.Start(False)

    End If

  End Sub ' AddTreeNodeToList

  ' PURPOSE: Add an element to element's list
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddNewElement(ByVal Node As ItemGroup)

    Dim _rows() As DataRow
    Dim _add_to_list As Boolean
    Dim _params As INPUT_GROUP_PARAMS
    Dim _dt_aux As DataTable
    Dim _type As WSI.Common.GROUP_ELEMENT_TYPE
    Dim _item As ListViewItem

    _add_to_list = False

    _rows = m_group_modif.Select(ELEMENTS_COLUMN_NODE_TYPE & " = " & CType(Node.ElementType, Int32) & " AND " & ELEMENTS_COLUMN_ID & " = " & Node.ElementId.ToString, _
                                 "", _
                                 DataViewRowState.Unchanged Or DataViewRowState.Deleted Or DataViewRowState.Added)

    ' Note modifications
    If _rows.Length > 0 Then

      ' If the row has just been deleted then mark it as Unchanged
      If _rows(0).RowState = DataRowState.Deleted Then
        _rows(0).RejectChanges()
        _add_to_list = True
      End If

    Else

      Dim _dr As DataRow = m_group_modif.NewRow

      _dr.Item(ELEMENTS_COLUMN_NODE_TYPE) = Node.ElementType
      _dr.Item(ELEMENTS_COLUMN_TYPE) = GroupConvert(Node.ElementType)
      _dr.Item(ELEMENTS_COLUMN_ID) = Node.ElementId
      _dr.Item(ELEMENTS_COLUMN_NAME) = Node.Text
      If Not Node.Parent Is Nothing Then
        _dr.Item(ELEMENTS_COLUMN_PARENT_ID) = CType(Node.Parent, ItemGroup).ElementId
      Else
        _dr.Item(ELEMENTS_COLUMN_PARENT_ID) = CType(Node, ItemGroup).ElementId
      End If
      m_group_modif.Rows.Add(_dr)
      _add_to_list = True

      End If

      If _add_to_list Then

        ' Add element
        _item = lv_elements.Items.Add(Node.Text)
        _item.Tag = Node

        Call Me.uc_group_list.SelectElement(Node.ElementId, Node.ElementType, Node.ElementName)

        ' Change format if item is disabled 
        Call ChangeEstateListviewElement(_item)

        If Node.ElementType = GROUP_NODE_ELEMENT.GROUPS AndAlso (Node.ElementEnabled = False) Then
          m_groups_disabled.Add(Node.ElementId)
        End If

        ' Get new terminals
        _type = GroupConvert(CType(Node.ElementType, GROUP_NODE_ELEMENT))
        _params = New INPUT_GROUP_PARAMS()
        _params.ParentId = Node.ElementId
        _params.ParentType = _type
        _params.ElementEnabled = Node.ElementEnabled
        _params.ElementId = Node.ElementId
        _params.ElementType = _type
        _dt_aux = Nothing
        If (GetExploitElements(_params, _dt_aux)) Then
          m_terminals.Merge(_dt_aux)
        End If
      End If

  End Sub ' AddNewElement

  ' PURPOSE: Changes the visualization of a list box item to show that is a enabled or disbled element
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ChangeEstateListviewElement(ByVal Item As ListViewItem)

    Dim _node As ItemGroup

    _node = CType(Item.Tag, ItemGroup)
    Item.ForeColor = IIf(_node.ElementEnabled, Color.Black, Color.Red)

  End Sub ' SelectElement

  ' PURPOSE: Changes the visualization of a list box item to show that is in the the selected element
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SelectListviewElement(ByVal Item As ListViewItem, ByVal Enabled As Boolean)

    If (Enabled) Then
      Item.ForeColor = Color.Black
    Else
      Item.ForeColor = Color.Red
    End If
    Item.Font = New Font(Item.Font.FontFamily, Item.Font.Size, FontStyle.Bold, Item.Font.Unit)

  End Sub ' SelectElement

  ' PURPOSE: Changes the visualization of a list box item to show that is not in the the selected element
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DeselectListviewElement(ByVal Item As ListViewItem)

    If (m_groups_disabled.Contains(Item.Tag)) Then
      Item.ForeColor = Color.Red
    Else
      Item.ForeColor = Color.Black
    End If

    Item.Font = New Font(Item.Font.FontFamily, Item.Font.Size, FontStyle.Regular, Item.Font.Unit)

  End Sub ' DeselectListviewElement

  ' PURPOSE: Adds all elements to terminal's list with repeated elements
  '
  '    - INPUT:
  '         - SelectFirstElement: Object - Shows what list element select: Firs or Last
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InsertTerminals(ByVal SelectFirstElement As Object)

    Dim _id As Int64
    Dim _parent_id As Int32

    If Me.InvokeRequired Then
      Dim _inv As DelegatesParam
      _inv = New DelegatesParam(AddressOf InsertTerminals)
      Me.Invoke(_inv, New Object() {SelectFirstElement})
    Else

      ' Clear temrinal's list
      lv_terminals.Items.Clear()

      m_terminals.DefaultView.Sort = ELEMENTS_COLUMN_PARENT_ID
      m_terminals = m_terminals.DefaultView.ToTable()

      ' Add terminals to the list
      For Each _row As DataRow In m_terminals.Rows

        ' Get the terminal id as the key in terminal list
        _id = CType(_row(ELEMENTS_COLUMN_ID), Int64)
        _parent_id = CType(_row(ELEMENTS_COLUMN_PARENT_ID), Int32)

        ' Create it even if it exists
        lv_terminals.Items.Add(_id.ToString(), _row(ELEMENTS_COLUMN_NAME).ToString(), "").Tag = _parent_id

      Next

      ' Focus on last or firts element
      If (lv_elements.Items.Count > 0) Then
        If CType(SelectFirstElement, Boolean) Then
          lv_elements.Items(0).Selected = True
        Else
          lv_elements.Items(lv_elements.Items.Count - 1).Selected = True
          lv_elements.EnsureVisible(lv_elements.Items(lv_elements.Items.Count - 1).Index)
        End If
      End If

    End If
  End Sub ' InsertTerminals

  ' PURPOSE: Change information about element selected
  '
  '    - INPUT:
  '         - SelectedItem: ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub RefreshElementInfo(ByVal SelectedItem As ItemGroup)

    Dim _sb As StringBuilder
    Dim _num_terminals As Int32

    _sb = New StringBuilder

    If SelectedItem Is Nothing Then

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(476) & ":")
      _sb.AppendLine("")
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(475) & ":")
      _sb.AppendLine("")
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(261) & ":")
      _sb.AppendLine("")

    Else
      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(476) & ":")
      _sb.AppendLine(SelectedItem.Text)
      _sb.AppendLine("")

      _num_terminals = m_terminals.Select(ELEMENTS_COLUMN_PARENT_TYPE & " = " & CType(GroupConvert(SelectedItem.ElementType), Int32).ToString & " AND " & _
                                          ELEMENTS_COLUMN_PARENT_ID & " = " & SelectedItem.ElementId).Length

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(475) & ":")
      _sb.AppendLine(_num_terminals.ToString)
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(261) & ":")
      _sb.AppendLine(IIf(SelectedItem.ElementEnabled, GLB_NLS_GUI_CONTROLS.GetString(281), GLB_NLS_GUI_CONTROLS.GetString(282)))

    End If

    xDataElements.Text = _sb.ToString()

  End Sub ' RefreshElementInfo

  ' PURPOSE: Change information about terminal selected
  '
  '    - INPUT:
  '         - SelectedItem: ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub RefreshTerminalInfo(ByVal SelectedItem As ListViewItem)

    Dim _sb As StringBuilder
    Dim _master_id As Int32
    Dim _provider As String
    Dim _zone As String
    Dim _area As String
    Dim _bank As String
    Dim _terminal_id As Int32

    _sb = New StringBuilder
    _provider = ""
    _zone = ""
    _area = ""
    _bank = ""

    If SelectedItem Is Nothing Then

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(472) & ":")
      _sb.AppendLine("")
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(467) & ":")
      _sb.AppendLine("")
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(468) & "\" & GLB_NLS_GUI_CONTROLS.GetString(469) & "\" & GLB_NLS_GUI_CONTROLS.GetString(470) & ":")
      _sb.AppendLine("")

    Else

      _master_id = CType(m_terminals.Rows(SelectedItem.Index)(ELEMENTS_COLUMN_ID), Int32)
      GetTerminalInfo(_master_id, _provider, _bank, _area, _zone, _terminal_id)

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(472) & ":")
      _sb.AppendLine(SelectedItem.Text)
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(467) & ":")
      _sb.AppendLine(_provider)
      _sb.AppendLine("")

      _sb.AppendLine(GLB_NLS_GUI_CONTROLS.GetString(468) & "\" & GLB_NLS_GUI_CONTROLS.GetString(469) & "\" & GLB_NLS_GUI_CONTROLS.GetString(470) & ":")
      _sb.AppendLine(_zone & "\" & _area & "\" & _bank)

    End If

    xDataTerminals.Text = _sb.ToString()

  End Sub ' RefreshTerminalInfo

  ' PURPOSE: Change information about element and terminal selected
  '
  '    - INPUT:
  '         - SelectedItem: ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub RefreshInfo(ByVal Node As ItemGroup, ByVal ListElement As ListViewItem)

    RefreshElementInfo(Node)
    RefreshTerminalInfo(ListElement)

  End Sub

  ' PURPOSE: Delete ListViewItem
  '
  '    - INPUT:
  '         - IdGroup String: Id of Element
  '         - Parent Int32: Parent of Element
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SearchAndDeleteFromTerminals(ByVal IdGroup As String, ByVal Parent As Int32)

    For Each _item As ListViewItem In lv_terminals.Items
      If (_item.Name = IdGroup) AndAlso (_item.Tag = Parent) Then
        _item.Remove()
      End If
    Next

  End Sub

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Event handler routine when user selects an element on element's listbox
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub lv_elements_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lv_elements.SelectedIndexChanged

    Dim _selected_item As ItemGroup
    Dim _first_selected As ListViewItem

    Try

      If (lv_elements.SelectedItems.Count = 0) Then

        ' If there are no elements selected, clean information and do nothing
        RefreshElementInfo(Nothing)

        ' If there are no elements selected, there is no terminal selected
        For Each _item As ListViewItem In lv_terminals.Items

          DeselectListviewElement(_item)
        Next

      Else
        _selected_item = CType(lv_elements.SelectedItems(0).Tag, ItemGroup)
        _first_selected = Nothing

        ' Refresh information about element selected
        RefreshElementInfo(_selected_item)

        For Each _item As ListViewItem In lv_terminals.Items

          ' Restore format of old terminal's selection
          If _item.Font.Bold Then
            Call DeselectListviewElement(_item)
          End If

          ' Change format of new terminal's selection
          If Not _selected_item Is Nothing AndAlso _item.Tag.Equals(_selected_item.ElementId) Then
            Call SelectListviewElement(_item, _selected_item.ElementEnabled)
            If _first_selected Is Nothing Then
              _first_selected = _item
            End If
          Else
            If (m_groups_disabled.Contains(_item.Tag)) Then
              _item.ForeColor = Color.Red
            End If
          End If

        Next

        ' Select a terminal of the list
        If _first_selected Is Nothing Then
          If lv_terminals.Items.Count > 0 Then
            lv_terminals.Items(0).Selected = True
            lv_terminals.EnsureVisible(lv_terminals.Items(0).Index)
          End If
        Else
          _first_selected.Selected = True
          lv_terminals.EnsureVisible(_first_selected.Index)
        End If

        ' Change focus
        lv_terminals.Focus()
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' lb_elements_SelectedIndexChanged

  ' PURPOSE: Event handler routine when user clicks on add button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add.Click

    Try

      ' Add the selected tree node to element's list
      If Not uc_group_list.Tree.SelectedNode Is Nothing Then
        AddTreeNodeToList(CType(uc_group_list.Tree.SelectedNode, ItemGroup))
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' btn_add_Click

  ' PURPOSE: Event handler routine when user clicks on remove button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_rmv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_rmv.Click

    Dim _index As Int32
    Dim _rows() As DataRow
    Dim _selected_element As ItemGroup
    Dim _id As Int32

    Try

      If Not lv_elements.SelectedItems Is Nothing AndAlso lv_elements.SelectedItems.Count > 0 Then

        ' Get the selected list node
        _selected_element = CType(lv_elements.SelectedItems(0).Tag, ItemGroup)

        ' Find the nodes that mach in te tree with the element in the list and deselect all
        uc_group_list.DeselectElement(_selected_element.ElementId, _selected_element.ElementType)

        ' Note element's modifications
        _rows = m_group_modif.Select(ELEMENTS_COLUMN_NODE_TYPE & " = " & CType(_selected_element.ElementType, Int32).ToString & " AND " & _
                                     ELEMENTS_COLUMN_ID & " = " & _selected_element.ElementId.ToString)

        If _rows.Length > 0 Then

          ' If the row has just been added then remove it and do nothing in database, else mark as deleted
          If _rows(0).RowState = DataRowState.Added Then
            m_group_modif.Rows.Remove(_rows(0))
          Else
            _rows(0).Delete()
          End If

        End If

        ' Delete element losing the focus
        _index = lv_elements.SelectedIndices.Item(0)
        lv_elements.Items.RemoveAt(_index)
        m_groups_disabled.Remove(_selected_element.ElementId)


        ' Change element's list selection and refresh terminals list.
        If (lv_elements.Items.Count > 0) Then

          ' If there are elements remaining on element's list, focus on the previous.
          lv_elements.Items(Math.Max(_index - 1, 0)).Selected = True

          ' Remove all terminals and relations in element removed
          _rows = m_terminals.Select(ELEMENTS_COLUMN_PARENT_TYPE & " = " & CType(GroupConvert(_selected_element.ElementType), Int32).ToString & " AND " & _
                                     ELEMENTS_COLUMN_PARENT_ID & " = " & _selected_element.ElementId.ToString)

          For Each _row As DataRow In _rows
            ' Get the terminal id as the key in terminal list
            _id = CType(_row(ELEMENTS_COLUMN_ID), Int32)

            ' Delete the element from listview
            SearchAndDeleteFromTerminals(_id.ToString(), _selected_element.ElementId)

            ' Delete the relaion between the element and the terminal
            m_terminals.Rows.Remove(_row)
          Next

        Else
          ' If there are no elements remaining on element's list, delete all terminals in list.
          m_terminals.Rows.Clear()
          lv_terminals.Items.Clear()

          ' Clean element and terminal information if there is no elements in the list.
          RefreshInfo(Nothing, Nothing)

        End If

      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' btn_rmv_Click

  ' PURPOSE: Event handler routine when user clicks on remove all button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_rvm_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_rvm_all.Click

    Try

      If lv_elements.Items.Count > 0 Then

        Dim _rc As mdl_NLS.ENUM_MB_RESULT

        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(168), _
                             ENUM_MB_TYPE.MB_TYPE_WARNING, _
                             ENUM_MB_BTN.MB_BTN_YES_NO, _
                             ENUM_MB_DEF_BTN.MB_DEF_BTN_2)

        If _rc <> ENUM_MB_RESULT.MB_RESULT_YES Then
          Return
        End If

        Call CleanElements()

        RefreshInfo(Nothing, Nothing)

      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' btn_rvm_all_Click

  ' PURPOSE: Event handler routine when user selects an element on terminal's listview
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub lv_terminals_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lv_terminals.SelectedIndexChanged

    ' If there are no elements in the list, clean information and do nothing
    If (lv_terminals.SelectedItems.Count = 0) Then
      RefreshTerminalInfo(Nothing)
    Else
      RefreshTerminalInfo(lv_terminals.SelectedItems(0))
    End If

  End Sub

  ' PURPOSE: Event handler routine when user expands a tree viwer node
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub uc_group_list_TreeNodeDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles uc_group_list.TreeNodeDoubleClick

    Try

      ' Add doubleclicked node to element's list
      If Not e.Node Is Nothing Then
        AddTreeNodeToList(CType(e.Node, ItemGroup))
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' uc_group_list_TreeNodeDoubleClick

#End Region ' Events

  Public Sub New()

    MyBase.New()
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub
End Class
