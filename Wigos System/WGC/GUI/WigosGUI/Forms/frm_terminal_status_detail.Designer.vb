<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_status_detail
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.lbl_warning = New System.Windows.Forms.Label()
    Me.tf_warning = New GUI_Controls.uc_text_field()
    Me.lbl_error = New System.Windows.Forms.Label()
    Me.tf_error = New GUI_Controls.uc_text_field()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.gb_service = New System.Windows.Forms.GroupBox()
    Me.lbl_WC2_ServerName = New System.Windows.Forms.Label()
    Me.lbl_WCP_ServerName = New System.Windows.Forms.Label()
    Me.lbl_WC2_Name = New System.Windows.Forms.Label()
    Me.lbl_WCP_Name = New System.Windows.Forms.Label()
    Me.lbl_SAS_HOST = New System.Windows.Forms.Label()
    Me.lbl_WCP_WC2 = New System.Windows.Forms.Label()
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field()
    Me.ef_terminal_provider = New GUI_Controls.uc_entry_field()
    Me.ef_terminal_session = New GUI_Controls.uc_entry_field()
    Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
    Me.panel_info_terminal = New System.Windows.Forms.Panel()
    Me.gb_plays = New System.Windows.Forms.GroupBox()
    Me.lbl_highroller_anonymous = New System.Windows.Forms.Label()
    Me.btn_38 = New System.Windows.Forms.Button()
    Me.btn_37 = New System.Windows.Forms.Button()
    Me.lbl_highroller_anonymous_value = New System.Windows.Forms.Label()
    Me.lbl_highroller_value = New System.Windows.Forms.Label()
    Me.btn_33 = New System.Windows.Forms.Button()
    Me.lbl_highroller = New System.Windows.Forms.Label()
    Me.lbl_without_plays_value = New System.Windows.Forms.Label()
    Me.lbl_without_plays = New System.Windows.Forms.Label()
    Me.btn_31 = New System.Windows.Forms.Button()
    Me.lbl_played_alarms_message = New System.Windows.Forms.Label()
    Me.btn_6 = New System.Windows.Forms.Button()
    Me.lbl_played = New System.Windows.Forms.Label()
    Me.btn_22 = New System.Windows.Forms.Button()
    Me.lbl_current_payout_value = New System.Windows.Forms.Label()
    Me.lbl_wonamount_alarms_message = New System.Windows.Forms.Label()
    Me.lbl_wonamount_exceeded = New System.Windows.Forms.Label()
    Me.lbl_current_payout = New System.Windows.Forms.Label()
    Me.gb_handpays = New System.Windows.Forms.GroupBox()
    Me.btn_7 = New System.Windows.Forms.Button()
    Me.btn_21 = New System.Windows.Forms.Button()
    Me.lbl_jackpot_alarm_message = New System.Windows.Forms.Label()
    Me.lbl_jackpot = New System.Windows.Forms.Label()
    Me.Button3 = New System.Windows.Forms.Button()
    Me.lbl_c_credits_alarm_message = New System.Windows.Forms.Label()
    Me.lbl_c_credits = New System.Windows.Forms.Label()
    Me.Button4 = New System.Windows.Forms.Button()
    Me.Button5 = New System.Windows.Forms.Button()
    Me.gb_bills = New System.Windows.Forms.GroupBox()
    Me.lbl_counterfeits_alarm_message = New System.Windows.Forms.Label()
    Me.lbl_stacker_counter_value = New System.Windows.Forms.Label()
    Me.lbl_stacker_counter = New System.Windows.Forms.Label()
    Me.lbl_stacker_full = New System.Windows.Forms.Label()
    Me.btn_14 = New System.Windows.Forms.Button()
    Me.lbl_excess_counterfeits = New System.Windows.Forms.Label()
    Me.btn_13 = New System.Windows.Forms.Button()
    Me.lbl_bill_acceptor_hardware_failure = New System.Windows.Forms.Label()
    Me.btn_12 = New System.Windows.Forms.Button()
    Me.lbl_bill_jam = New System.Windows.Forms.Label()
    Me.gb_coins = New System.Windows.Forms.GroupBox()
    Me.btn_36 = New System.Windows.Forms.Button()
    Me.lbl_coin_in_lockout_malfunction = New System.Windows.Forms.Label()
    Me.btn_35 = New System.Windows.Forms.Button()
    Me.lbl_reverse_coin_in = New System.Windows.Forms.Label()
    Me.btn_34 = New System.Windows.Forms.Button()
    Me.lbl_coin_in_tilt = New System.Windows.Forms.Label()
    Me.gb_doors = New System.Windows.Forms.GroupBox()
    Me.btn_5 = New System.Windows.Forms.Button()
    Me.lbl_cashbox_door = New System.Windows.Forms.Label()
    Me.btn_4 = New System.Windows.Forms.Button()
    Me.lbl_belly_door = New System.Windows.Forms.Label()
    Me.btn_3 = New System.Windows.Forms.Button()
    Me.lbl_card_cage = New System.Windows.Forms.Label()
    Me.btn_2 = New System.Windows.Forms.Button()
    Me.lbl_drop_door = New System.Windows.Forms.Label()
    Me.btn_1 = New System.Windows.Forms.Button()
    Me.lbl_slot_door = New System.Windows.Forms.Label()
    Me.gb_printers = New System.Windows.Forms.GroupBox()
    Me.btn_20 = New System.Windows.Forms.Button()
    Me.lbl_printer_power_on = New System.Windows.Forms.Label()
    Me.btn_19 = New System.Windows.Forms.Button()
    Me.lbl_printer_carriage_jammed = New System.Windows.Forms.Label()
    Me.btn_18 = New System.Windows.Forms.Button()
    Me.lbl_printer_paper_low = New System.Windows.Forms.Label()
    Me.btn_17 = New System.Windows.Forms.Button()
    Me.lbl_replace_printer_ribbon = New System.Windows.Forms.Label()
    Me.btn_16 = New System.Windows.Forms.Button()
    Me.lbl_printer_paper_out_error = New System.Windows.Forms.Label()
    Me.btn_15 = New System.Windows.Forms.Button()
    Me.lbl_printer_communication_error = New System.Windows.Forms.Label()
    Me.gb_egm = New System.Windows.Forms.GroupBox()
    Me.btn_27 = New System.Windows.Forms.Button()
    Me.lbl_partitioned_eprom_error_bad_checksum_compare = New System.Windows.Forms.Label()
    Me.btn_28 = New System.Windows.Forms.Button()
    Me.lbl_backup_battery_detected = New System.Windows.Forms.Label()
    Me.btn_25 = New System.Windows.Forms.Button()
    Me.lbl_eprom_error_bad_checksum_compare = New System.Windows.Forms.Label()
    Me.btn_26 = New System.Windows.Forms.Button()
    Me.lbl_partitioned_eprom_error_checksum_version_changed = New System.Windows.Forms.Label()
    Me.btn_24 = New System.Windows.Forms.Button()
    Me.lbl_eprom_error_different_checksum_version_changed = New System.Windows.Forms.Label()
    Me.btn_23 = New System.Windows.Forms.Button()
    Me.lbl_eprom_error_bad_device = New System.Windows.Forms.Label()
    Me.btn_11 = New System.Windows.Forms.Button()
    Me.lbl_eprom_error_data_error = New System.Windows.Forms.Label()
    Me.btn_10 = New System.Windows.Forms.Button()
    Me.lbl_cmos_ram_error_bad_device = New System.Windows.Forms.Label()
    Me.btn_9 = New System.Windows.Forms.Button()
    Me.lbl_cmos_error_no_data_recovered = New System.Windows.Forms.Label()
    Me.btn_8 = New System.Windows.Forms.Button()
    Me.lbl_cmos_error_data_recovered = New System.Windows.Forms.Label()
    Me.gb_call_attendant = New System.Windows.Forms.GroupBox()
    Me.btn_29 = New System.Windows.Forms.Button()
    Me.Button1 = New System.Windows.Forms.Button()
    Me.lbl_call_attendant = New System.Windows.Forms.Label()
    Me.gb_machine_status = New System.Windows.Forms.GroupBox()
    Me.btn_30 = New System.Windows.Forms.Button()
    Me.lbl_locked_by_manually = New System.Windows.Forms.Label()
    Me.btn_39 = New System.Windows.Forms.Button()
    Me.lbl_locked_by_intrusion = New System.Windows.Forms.Label()
    Me.btn_32 = New System.Windows.Forms.Button()
    Me.lbl_locked_by_signature = New System.Windows.Forms.Label()
    Me.gb_software_validation = New System.Windows.Forms.GroupBox()
    Me.lbl_last_checked_detail = New System.Windows.Forms.Label()
    Me.lbl_authentication_status = New System.Windows.Forms.Label()
    Me.lbl_authentication_last_checked = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_service.SuspendLayout()
    Me.panel_info_terminal.SuspendLayout()
    Me.gb_plays.SuspendLayout()
    Me.gb_handpays.SuspendLayout()
    Me.gb_bills.SuspendLayout()
    Me.gb_coins.SuspendLayout()
    Me.gb_doors.SuspendLayout()
    Me.gb_printers.SuspendLayout()
    Me.gb_egm.SuspendLayout()
    Me.gb_call_attendant.SuspendLayout()
    Me.gb_machine_status.SuspendLayout()
    Me.gb_software_validation.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.panel_info_terminal)
    Me.panel_data.Controls.Add(Me.gb_service)
    Me.panel_data.Controls.Add(Me.ef_terminal_session)
    Me.panel_data.Controls.Add(Me.ef_terminal_provider)
    Me.panel_data.Controls.Add(Me.ef_terminal_name)
    Me.panel_data.Controls.Add(Me.gb_status)
    Me.panel_data.Controls.Add(Me.tf_last_update)
    Me.panel_data.Size = New System.Drawing.Size(900, 775)
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_warning)
    Me.gb_status.Controls.Add(Me.tf_warning)
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.tf_error)
    Me.gb_status.Location = New System.Drawing.Point(738, 3)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(148, 70)
    Me.gb_status.TabIndex = 5
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xState"
    '
    'lbl_warning
    '
    Me.lbl_warning.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_warning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_warning.Location = New System.Drawing.Point(12, 24)
    Me.lbl_warning.Name = "lbl_warning"
    Me.lbl_warning.Size = New System.Drawing.Size(16, 16)
    Me.lbl_warning.TabIndex = 0
    '
    'tf_warning
    '
    Me.tf_warning.IsReadOnly = True
    Me.tf_warning.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_warning.LabelForeColor = System.Drawing.Color.Black
    Me.tf_warning.Location = New System.Drawing.Point(6, 20)
    Me.tf_warning.Name = "tf_warning"
    Me.tf_warning.Size = New System.Drawing.Size(135, 24)
    Me.tf_warning.SufixText = "Sufix Text"
    Me.tf_warning.SufixTextVisible = True
    Me.tf_warning.TabIndex = 1
    Me.tf_warning.TabStop = False
    Me.tf_warning.TextVisible = False
    Me.tf_warning.TextWidth = 30
    Me.tf_warning.Value = "xWarning"
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(12, 47)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 3
    '
    'tf_error
    '
    Me.tf_error.IsReadOnly = True
    Me.tf_error.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error.Location = New System.Drawing.Point(6, 43)
    Me.tf_error.Name = "tf_error"
    Me.tf_error.Size = New System.Drawing.Size(135, 24)
    Me.tf_error.SufixText = "Sufix Text"
    Me.tf_error.SufixTextVisible = True
    Me.tf_error.TabIndex = 2
    Me.tf_error.TabStop = False
    Me.tf_error.TextVisible = False
    Me.tf_error.TextWidth = 30
    Me.tf_error.Value = "xError"
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(361, 70)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(250, 24)
    Me.tf_last_update.SufixText = "xSeconds"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 4
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 130
    Me.tf_last_update.Value = "-"
    '
    'gb_service
    '
    Me.gb_service.Controls.Add(Me.lbl_WC2_ServerName)
    Me.gb_service.Controls.Add(Me.lbl_WCP_ServerName)
    Me.gb_service.Controls.Add(Me.lbl_WC2_Name)
    Me.gb_service.Controls.Add(Me.lbl_WCP_Name)
    Me.gb_service.Controls.Add(Me.lbl_SAS_HOST)
    Me.gb_service.Controls.Add(Me.lbl_WCP_WC2)
    Me.gb_service.Location = New System.Drawing.Point(374, 3)
    Me.gb_service.Name = "gb_service"
    Me.gb_service.Size = New System.Drawing.Size(355, 70)
    Me.gb_service.TabIndex = 3
    Me.gb_service.TabStop = False
    Me.gb_service.Text = "xColectordeDatos-Centro"
    '
    'lbl_WC2_ServerName
    '
    Me.lbl_WC2_ServerName.AutoSize = True
    Me.lbl_WC2_ServerName.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_WC2_ServerName.Location = New System.Drawing.Point(211, 50)
    Me.lbl_WC2_ServerName.Name = "lbl_WC2_ServerName"
    Me.lbl_WC2_ServerName.Size = New System.Drawing.Size(85, 13)
    Me.lbl_WC2_ServerName.TabIndex = 5
    Me.lbl_WC2_ServerName.Text = "WC2 ->Name"
    Me.lbl_WC2_ServerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_WCP_ServerName
    '
    Me.lbl_WCP_ServerName.AutoSize = True
    Me.lbl_WCP_ServerName.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_WCP_ServerName.Location = New System.Drawing.Point(211, 33)
    Me.lbl_WCP_ServerName.Name = "lbl_WCP_ServerName"
    Me.lbl_WCP_ServerName.Size = New System.Drawing.Size(85, 13)
    Me.lbl_WCP_ServerName.TabIndex = 3
    Me.lbl_WCP_ServerName.Text = "WCP ->Name"
    Me.lbl_WCP_ServerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_WC2_Name
    '
    Me.lbl_WC2_Name.AutoSize = True
    Me.lbl_WC2_Name.Location = New System.Drawing.Point(174, 50)
    Me.lbl_WC2_Name.Name = "lbl_WC2_Name"
    Me.lbl_WC2_Name.Size = New System.Drawing.Size(71, 13)
    Me.lbl_WC2_Name.TabIndex = 4
    Me.lbl_WC2_Name.Text = "WC2 Name"
    Me.lbl_WC2_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_WCP_Name
    '
    Me.lbl_WCP_Name.AutoSize = True
    Me.lbl_WCP_Name.Location = New System.Drawing.Point(174, 33)
    Me.lbl_WCP_Name.Name = "lbl_WCP_Name"
    Me.lbl_WCP_Name.Size = New System.Drawing.Size(71, 13)
    Me.lbl_WCP_Name.TabIndex = 2
    Me.lbl_WCP_Name.Text = "WCP Name"
    Me.lbl_WCP_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_SAS_HOST
    '
    Me.lbl_SAS_HOST.AutoSize = True
    Me.lbl_SAS_HOST.Location = New System.Drawing.Point(20, 17)
    Me.lbl_SAS_HOST.Name = "lbl_SAS_HOST"
    Me.lbl_SAS_HOST.Size = New System.Drawing.Size(77, 13)
    Me.lbl_SAS_HOST.TabIndex = 0
    Me.lbl_SAS_HOST.Text = "xSAS_HOST"
    Me.lbl_SAS_HOST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_WCP_WC2
    '
    Me.lbl_WCP_WC2.AutoSize = True
    Me.lbl_WCP_WC2.Location = New System.Drawing.Point(174, 16)
    Me.lbl_WCP_WC2.Name = "lbl_WCP_WC2"
    Me.lbl_WCP_WC2.Size = New System.Drawing.Size(41, 13)
    Me.lbl_WCP_WC2.TabIndex = 1
    Me.lbl_WCP_WC2.Text = "xWCP"
    Me.lbl_WCP_WC2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0.0R
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = True
    Me.ef_terminal_name.Location = New System.Drawing.Point(3, 3)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.PlaceHolder = Nothing
    Me.ef_terminal_name.Size = New System.Drawing.Size(352, 24)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 0
    Me.ef_terminal_name.TabStop = False
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.Value = ""
    Me.ef_terminal_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal_provider
    '
    Me.ef_terminal_provider.DoubleValue = 0.0R
    Me.ef_terminal_provider.IntegerValue = 0
    Me.ef_terminal_provider.IsReadOnly = True
    Me.ef_terminal_provider.Location = New System.Drawing.Point(3, 32)
    Me.ef_terminal_provider.Name = "ef_terminal_provider"
    Me.ef_terminal_provider.PlaceHolder = Nothing
    Me.ef_terminal_provider.Size = New System.Drawing.Size(352, 24)
    Me.ef_terminal_provider.SufixText = "Sufix Text"
    Me.ef_terminal_provider.SufixTextVisible = True
    Me.ef_terminal_provider.TabIndex = 1
    Me.ef_terminal_provider.TabStop = False
    Me.ef_terminal_provider.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_provider.TextValue = ""
    Me.ef_terminal_provider.Value = ""
    Me.ef_terminal_provider.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal_session
    '
    Me.ef_terminal_session.DoubleValue = 0.0R
    Me.ef_terminal_session.IntegerValue = 0
    Me.ef_terminal_session.IsReadOnly = True
    Me.ef_terminal_session.Location = New System.Drawing.Point(3, 62)
    Me.ef_terminal_session.Name = "ef_terminal_session"
    Me.ef_terminal_session.PlaceHolder = Nothing
    Me.ef_terminal_session.Size = New System.Drawing.Size(352, 24)
    Me.ef_terminal_session.SufixText = "Sufix Text"
    Me.ef_terminal_session.SufixTextVisible = True
    Me.ef_terminal_session.TabIndex = 2
    Me.ef_terminal_session.TabStop = False
    Me.ef_terminal_session.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_session.TextValue = ""
    Me.ef_terminal_session.Value = ""
    Me.ef_terminal_session.ValueForeColor = System.Drawing.Color.Blue
    '
    'panel_info_terminal
    '
    Me.panel_info_terminal.Controls.Add(Me.gb_plays)
    Me.panel_info_terminal.Controls.Add(Me.gb_handpays)
    Me.panel_info_terminal.Controls.Add(Me.gb_bills)
    Me.panel_info_terminal.Controls.Add(Me.gb_coins)
    Me.panel_info_terminal.Controls.Add(Me.gb_doors)
    Me.panel_info_terminal.Controls.Add(Me.gb_printers)
    Me.panel_info_terminal.Controls.Add(Me.gb_egm)
    Me.panel_info_terminal.Controls.Add(Me.gb_call_attendant)
    Me.panel_info_terminal.Controls.Add(Me.gb_machine_status)
    Me.panel_info_terminal.Controls.Add(Me.gb_software_validation)
    Me.panel_info_terminal.Location = New System.Drawing.Point(10, 95)
    Me.panel_info_terminal.Name = "panel_info_terminal"
    Me.panel_info_terminal.Size = New System.Drawing.Size(880, 675)
    Me.panel_info_terminal.TabIndex = 6
    '
    'gb_plays
    '
    Me.gb_plays.Controls.Add(Me.lbl_highroller_anonymous)
    Me.gb_plays.Controls.Add(Me.btn_38)
    Me.gb_plays.Controls.Add(Me.btn_37)
    Me.gb_plays.Controls.Add(Me.lbl_highroller_anonymous_value)
    Me.gb_plays.Controls.Add(Me.lbl_highroller_value)
    Me.gb_plays.Controls.Add(Me.btn_33)
    Me.gb_plays.Controls.Add(Me.lbl_highroller)
    Me.gb_plays.Controls.Add(Me.lbl_without_plays_value)
    Me.gb_plays.Controls.Add(Me.lbl_without_plays)
    Me.gb_plays.Controls.Add(Me.btn_31)
    Me.gb_plays.Controls.Add(Me.lbl_played_alarms_message)
    Me.gb_plays.Controls.Add(Me.btn_6)
    Me.gb_plays.Controls.Add(Me.lbl_played)
    Me.gb_plays.Controls.Add(Me.btn_22)
    Me.gb_plays.Controls.Add(Me.lbl_current_payout_value)
    Me.gb_plays.Controls.Add(Me.lbl_wonamount_alarms_message)
    Me.gb_plays.Controls.Add(Me.lbl_wonamount_exceeded)
    Me.gb_plays.Controls.Add(Me.lbl_current_payout)
    Me.gb_plays.Location = New System.Drawing.Point(5, 2)
    Me.gb_plays.Name = "gb_plays"
    Me.gb_plays.Size = New System.Drawing.Size(432, 240)
    Me.gb_plays.TabIndex = 10
    Me.gb_plays.TabStop = False
    Me.gb_plays.Text = "xPlayed/won"
    '
    'lbl_highroller_anonymous
    '
    Me.lbl_highroller_anonymous.AutoSize = True
    Me.lbl_highroller_anonymous.Location = New System.Drawing.Point(9, 188)
    Me.lbl_highroller_anonymous.Name = "lbl_highroller_anonymous"
    Me.lbl_highroller_anonymous.Size = New System.Drawing.Size(141, 13)
    Me.lbl_highroller_anonymous.TabIndex = 9
    Me.lbl_highroller_anonymous.Text = "xhighroller_anonymous"
    Me.lbl_highroller_anonymous.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_38
    '
    Me.btn_38.Location = New System.Drawing.Point(350, 190)
    Me.btn_38.Name = "btn_38"
    Me.btn_38.Size = New System.Drawing.Size(75, 20)
    Me.btn_38.TabIndex = 11
    Me.btn_38.Text = "xIgnore"
    Me.btn_38.UseVisualStyleBackColor = True
    '
    'btn_37
    '
    Me.btn_37.Location = New System.Drawing.Point(350, 136)
    Me.btn_37.Name = "btn_37"
    Me.btn_37.Size = New System.Drawing.Size(75, 20)
    Me.btn_37.TabIndex = 11
    Me.btn_37.Text = "xIgnore"
    Me.btn_37.UseVisualStyleBackColor = True
    '
    'lbl_highroller_anonymous_value
    '
    Me.lbl_highroller_anonymous_value.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_highroller_anonymous_value.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_highroller_anonymous_value.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_highroller_anonymous_value.Location = New System.Drawing.Point(106, 186)
    Me.lbl_highroller_anonymous_value.Name = "lbl_highroller_anonymous_value"
    Me.lbl_highroller_anonymous_value.Size = New System.Drawing.Size(239, 45)
    Me.lbl_highroller_anonymous_value.TabIndex = 10
    Me.lbl_highroller_anonymous_value.Text = "xhighroller anonymous" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "_" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "_"
    Me.lbl_highroller_anonymous_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_highroller_value
    '
    Me.lbl_highroller_value.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_highroller_value.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_highroller_value.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_highroller_value.Location = New System.Drawing.Point(106, 130)
    Me.lbl_highroller_value.Name = "lbl_highroller_value"
    Me.lbl_highroller_value.Size = New System.Drawing.Size(239, 52)
    Me.lbl_highroller_value.TabIndex = 10
    Me.lbl_highroller_value.Text = "xhighroller" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "_" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "_" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
    Me.lbl_highroller_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'btn_33
    '
    Me.btn_33.Location = New System.Drawing.Point(350, 103)
    Me.btn_33.Name = "btn_33"
    Me.btn_33.Size = New System.Drawing.Size(75, 20)
    Me.btn_33.TabIndex = 11
    Me.btn_33.Text = "xIgnore"
    Me.btn_33.UseVisualStyleBackColor = True
    '
    'lbl_highroller
    '
    Me.lbl_highroller.AutoSize = True
    Me.lbl_highroller.Location = New System.Drawing.Point(9, 133)
    Me.lbl_highroller.Name = "lbl_highroller"
    Me.lbl_highroller.Size = New System.Drawing.Size(68, 13)
    Me.lbl_highroller.TabIndex = 9
    Me.lbl_highroller.Text = "xhighroller"
    Me.lbl_highroller.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_without_plays_value
    '
    Me.lbl_without_plays_value.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_without_plays_value.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_without_plays_value.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_without_plays_value.Location = New System.Drawing.Point(106, 97)
    Me.lbl_without_plays_value.Name = "lbl_without_plays_value"
    Me.lbl_without_plays_value.Size = New System.Drawing.Size(239, 30)
    Me.lbl_without_plays_value.TabIndex = 10
    Me.lbl_without_plays_value.Text = "xwithout_plays"
    Me.lbl_without_plays_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_without_plays
    '
    Me.lbl_without_plays.AutoSize = True
    Me.lbl_without_plays.Location = New System.Drawing.Point(9, 105)
    Me.lbl_without_plays.Name = "lbl_without_plays"
    Me.lbl_without_plays.Size = New System.Drawing.Size(92, 13)
    Me.lbl_without_plays.TabIndex = 9
    Me.lbl_without_plays.Text = "xwithout_plays"
    Me.lbl_without_plays.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_31
    '
    Me.btn_31.Location = New System.Drawing.Point(350, 73)
    Me.btn_31.Name = "btn_31"
    Me.btn_31.Size = New System.Drawing.Size(75, 20)
    Me.btn_31.TabIndex = 8
    Me.btn_31.Text = "xIgnore"
    Me.btn_31.UseVisualStyleBackColor = True
    '
    'lbl_played_alarms_message
    '
    Me.lbl_played_alarms_message.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_played_alarms_message.BackColor = System.Drawing.Color.Transparent
    Me.lbl_played_alarms_message.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_played_alarms_message.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_played_alarms_message.Location = New System.Drawing.Point(106, 12)
    Me.lbl_played_alarms_message.Name = "lbl_played_alarms_message"
    Me.lbl_played_alarms_message.Size = New System.Drawing.Size(239, 30)
    Me.lbl_played_alarms_message.TabIndex = 1
    Me.lbl_played_alarms_message.Text = "xplayed_alarms_message"
    Me.lbl_played_alarms_message.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'btn_6
    '
    Me.btn_6.Location = New System.Drawing.Point(350, 15)
    Me.btn_6.Name = "btn_6"
    Me.btn_6.Size = New System.Drawing.Size(75, 20)
    Me.btn_6.TabIndex = 2
    Me.btn_6.Text = "xIgnore"
    Me.btn_6.UseVisualStyleBackColor = True
    '
    'lbl_played
    '
    Me.lbl_played.AutoSize = True
    Me.lbl_played.BackColor = System.Drawing.Color.Transparent
    Me.lbl_played.Location = New System.Drawing.Point(9, 17)
    Me.lbl_played.Name = "lbl_played"
    Me.lbl_played.Size = New System.Drawing.Size(52, 13)
    Me.lbl_played.TabIndex = 0
    Me.lbl_played.Text = "xplayed"
    Me.lbl_played.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_22
    '
    Me.btn_22.Location = New System.Drawing.Point(350, 45)
    Me.btn_22.Name = "btn_22"
    Me.btn_22.Size = New System.Drawing.Size(75, 20)
    Me.btn_22.TabIndex = 5
    Me.btn_22.Text = "xIgnore"
    Me.btn_22.UseVisualStyleBackColor = True
    '
    'lbl_current_payout_value
    '
    Me.lbl_current_payout_value.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_current_payout_value.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_current_payout_value.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_current_payout_value.Location = New System.Drawing.Point(106, 67)
    Me.lbl_current_payout_value.Name = "lbl_current_payout_value"
    Me.lbl_current_payout_value.Size = New System.Drawing.Size(239, 33)
    Me.lbl_current_payout_value.TabIndex = 7
    Me.lbl_current_payout_value.Text = "xcurrent_payout"
    Me.lbl_current_payout_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_wonamount_alarms_message
    '
    Me.lbl_wonamount_alarms_message.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_wonamount_alarms_message.BackColor = System.Drawing.Color.Transparent
    Me.lbl_wonamount_alarms_message.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_wonamount_alarms_message.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_wonamount_alarms_message.Location = New System.Drawing.Point(106, 41)
    Me.lbl_wonamount_alarms_message.Name = "lbl_wonamount_alarms_message"
    Me.lbl_wonamount_alarms_message.Size = New System.Drawing.Size(239, 30)
    Me.lbl_wonamount_alarms_message.TabIndex = 4
    Me.lbl_wonamount_alarms_message.Text = "xwonamount_exceeded"
    Me.lbl_wonamount_alarms_message.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_wonamount_exceeded
    '
    Me.lbl_wonamount_exceeded.AutoSize = True
    Me.lbl_wonamount_exceeded.BackColor = System.Drawing.Color.Transparent
    Me.lbl_wonamount_exceeded.Location = New System.Drawing.Point(9, 47)
    Me.lbl_wonamount_exceeded.Name = "lbl_wonamount_exceeded"
    Me.lbl_wonamount_exceeded.Size = New System.Drawing.Size(37, 13)
    Me.lbl_wonamount_exceeded.TabIndex = 3
    Me.lbl_wonamount_exceeded.Text = "xwon"
    Me.lbl_wonamount_exceeded.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_current_payout
    '
    Me.lbl_current_payout.AutoSize = True
    Me.lbl_current_payout.Location = New System.Drawing.Point(9, 75)
    Me.lbl_current_payout.Name = "lbl_current_payout"
    Me.lbl_current_payout.Size = New System.Drawing.Size(101, 13)
    Me.lbl_current_payout.TabIndex = 6
    Me.lbl_current_payout.Text = "xcurrent_payout"
    Me.lbl_current_payout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_handpays
    '
    Me.gb_handpays.Controls.Add(Me.btn_7)
    Me.gb_handpays.Controls.Add(Me.btn_21)
    Me.gb_handpays.Controls.Add(Me.lbl_jackpot_alarm_message)
    Me.gb_handpays.Controls.Add(Me.lbl_jackpot)
    Me.gb_handpays.Controls.Add(Me.Button3)
    Me.gb_handpays.Controls.Add(Me.lbl_c_credits_alarm_message)
    Me.gb_handpays.Controls.Add(Me.lbl_c_credits)
    Me.gb_handpays.Controls.Add(Me.Button4)
    Me.gb_handpays.Controls.Add(Me.Button5)
    Me.gb_handpays.Location = New System.Drawing.Point(5, 248)
    Me.gb_handpays.Name = "gb_handpays"
    Me.gb_handpays.Size = New System.Drawing.Size(432, 65)
    Me.gb_handpays.TabIndex = 11
    Me.gb_handpays.TabStop = False
    Me.gb_handpays.Text = "xHandpays"
    '
    'btn_7
    '
    Me.btn_7.Location = New System.Drawing.Point(350, 39)
    Me.btn_7.Name = "btn_7"
    Me.btn_7.Size = New System.Drawing.Size(75, 20)
    Me.btn_7.TabIndex = 5
    Me.btn_7.Text = "xIgnore"
    Me.btn_7.UseVisualStyleBackColor = True
    '
    'btn_21
    '
    Me.btn_21.Location = New System.Drawing.Point(350, 16)
    Me.btn_21.Name = "btn_21"
    Me.btn_21.Size = New System.Drawing.Size(75, 19)
    Me.btn_21.TabIndex = 2
    Me.btn_21.Text = "xIgnore"
    Me.btn_21.UseVisualStyleBackColor = True
    '
    'lbl_jackpot_alarm_message
    '
    Me.lbl_jackpot_alarm_message.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_jackpot_alarm_message.BackColor = System.Drawing.Color.Transparent
    Me.lbl_jackpot_alarm_message.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_jackpot_alarm_message.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_jackpot_alarm_message.Location = New System.Drawing.Point(105, 11)
    Me.lbl_jackpot_alarm_message.Name = "lbl_jackpot_alarm_message"
    Me.lbl_jackpot_alarm_message.Size = New System.Drawing.Size(243, 28)
    Me.lbl_jackpot_alarm_message.TabIndex = 1
    Me.lbl_jackpot_alarm_message.Text = "xlbl_jackpot_alarm_message"
    Me.lbl_jackpot_alarm_message.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_jackpot
    '
    Me.lbl_jackpot.AutoSize = True
    Me.lbl_jackpot.Location = New System.Drawing.Point(10, 19)
    Me.lbl_jackpot.Name = "lbl_jackpot"
    Me.lbl_jackpot.Size = New System.Drawing.Size(57, 13)
    Me.lbl_jackpot.TabIndex = 0
    Me.lbl_jackpot.Text = "xJackpot"
    Me.lbl_jackpot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Button3
    '
    Me.Button3.Location = New System.Drawing.Point(345, 140)
    Me.Button3.Name = "Button3"
    Me.Button3.Size = New System.Drawing.Size(75, 19)
    Me.Button3.TabIndex = 7
    Me.Button3.TabStop = False
    Me.Button3.Text = "xIgnore"
    Me.Button3.UseVisualStyleBackColor = True
    Me.Button3.Visible = False
    '
    'lbl_c_credits_alarm_message
    '
    Me.lbl_c_credits_alarm_message.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_c_credits_alarm_message.BackColor = System.Drawing.Color.Transparent
    Me.lbl_c_credits_alarm_message.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_c_credits_alarm_message.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_c_credits_alarm_message.Location = New System.Drawing.Point(138, 35)
    Me.lbl_c_credits_alarm_message.Name = "lbl_c_credits_alarm_message"
    Me.lbl_c_credits_alarm_message.Size = New System.Drawing.Size(210, 28)
    Me.lbl_c_credits_alarm_message.TabIndex = 4
    Me.lbl_c_credits_alarm_message.Text = "xc_credits_alarm_message"
    Me.lbl_c_credits_alarm_message.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_c_credits
    '
    Me.lbl_c_credits.AutoSize = True
    Me.lbl_c_credits.BackColor = System.Drawing.Color.Transparent
    Me.lbl_c_credits.Location = New System.Drawing.Point(10, 43)
    Me.lbl_c_credits.Name = "lbl_c_credits"
    Me.lbl_c_credits.Size = New System.Drawing.Size(109, 13)
    Me.lbl_c_credits.TabIndex = 3
    Me.lbl_c_credits.Text = "xcanceled_credits"
    '
    'Button4
    '
    Me.Button4.Location = New System.Drawing.Point(271, 141)
    Me.Button4.Name = "Button4"
    Me.Button4.Size = New System.Drawing.Size(61, 20)
    Me.Button4.TabIndex = 8
    Me.Button4.TabStop = False
    Me.Button4.Text = "xIgnore"
    Me.Button4.UseVisualStyleBackColor = True
    Me.Button4.Visible = False
    '
    'Button5
    '
    Me.Button5.Location = New System.Drawing.Point(271, 141)
    Me.Button5.Name = "Button5"
    Me.Button5.Size = New System.Drawing.Size(61, 20)
    Me.Button5.TabIndex = 9
    Me.Button5.TabStop = False
    Me.Button5.Text = "xIgnore"
    Me.Button5.UseVisualStyleBackColor = True
    Me.Button5.Visible = False
    '
    'gb_bills
    '
    Me.gb_bills.Controls.Add(Me.lbl_counterfeits_alarm_message)
    Me.gb_bills.Controls.Add(Me.lbl_stacker_counter_value)
    Me.gb_bills.Controls.Add(Me.lbl_stacker_counter)
    Me.gb_bills.Controls.Add(Me.lbl_stacker_full)
    Me.gb_bills.Controls.Add(Me.btn_14)
    Me.gb_bills.Controls.Add(Me.lbl_excess_counterfeits)
    Me.gb_bills.Controls.Add(Me.btn_13)
    Me.gb_bills.Controls.Add(Me.lbl_bill_acceptor_hardware_failure)
    Me.gb_bills.Controls.Add(Me.btn_12)
    Me.gb_bills.Controls.Add(Me.lbl_bill_jam)
    Me.gb_bills.Location = New System.Drawing.Point(5, 316)
    Me.gb_bills.Name = "gb_bills"
    Me.gb_bills.Size = New System.Drawing.Size(432, 147)
    Me.gb_bills.TabIndex = 12
    Me.gb_bills.TabStop = False
    Me.gb_bills.Text = "xBills_Acceptor"
    '
    'lbl_counterfeits_alarm_message
    '
    Me.lbl_counterfeits_alarm_message.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_counterfeits_alarm_message.BackColor = System.Drawing.Color.Transparent
    Me.lbl_counterfeits_alarm_message.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_counterfeits_alarm_message.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_counterfeits_alarm_message.Location = New System.Drawing.Point(141, 65)
    Me.lbl_counterfeits_alarm_message.Name = "lbl_counterfeits_alarm_message"
    Me.lbl_counterfeits_alarm_message.Size = New System.Drawing.Size(207, 28)
    Me.lbl_counterfeits_alarm_message.TabIndex = 5
    Me.lbl_counterfeits_alarm_message.Text = "xcounterfeits_alarm_message"
    Me.lbl_counterfeits_alarm_message.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_stacker_counter_value
    '
    Me.lbl_stacker_counter_value.AutoSize = True
    Me.lbl_stacker_counter_value.Location = New System.Drawing.Point(10, 127)
    Me.lbl_stacker_counter_value.Name = "lbl_stacker_counter_value"
    Me.lbl_stacker_counter_value.Size = New System.Drawing.Size(158, 13)
    Me.lbl_stacker_counter_value.TabIndex = 9
    Me.lbl_stacker_counter_value.Text = "xstacker_status_hardware"
    Me.lbl_stacker_counter_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_stacker_counter
    '
    Me.lbl_stacker_counter.Font = New System.Drawing.Font("Courier New", 8.25!)
    Me.lbl_stacker_counter.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_stacker_counter.Location = New System.Drawing.Point(185, 97)
    Me.lbl_stacker_counter.Name = "lbl_stacker_counter"
    Me.lbl_stacker_counter.Size = New System.Drawing.Size(163, 28)
    Me.lbl_stacker_counter.TabIndex = 8
    Me.lbl_stacker_counter.Text = "xstacker_counter"
    Me.lbl_stacker_counter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_stacker_full
    '
    Me.lbl_stacker_full.AutoSize = True
    Me.lbl_stacker_full.Location = New System.Drawing.Point(10, 105)
    Me.lbl_stacker_full.Name = "lbl_stacker_full"
    Me.lbl_stacker_full.Size = New System.Drawing.Size(153, 13)
    Me.lbl_stacker_full.TabIndex = 7
    Me.lbl_stacker_full.Text = "xstacker_status_software"
    Me.lbl_stacker_full.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_14
    '
    Me.btn_14.Location = New System.Drawing.Point(350, 69)
    Me.btn_14.Name = "btn_14"
    Me.btn_14.Size = New System.Drawing.Size(75, 20)
    Me.btn_14.TabIndex = 6
    Me.btn_14.Text = "xIgnore"
    Me.btn_14.UseVisualStyleBackColor = True
    '
    'lbl_excess_counterfeits
    '
    Me.lbl_excess_counterfeits.AutoSize = True
    Me.lbl_excess_counterfeits.Location = New System.Drawing.Point(10, 73)
    Me.lbl_excess_counterfeits.Name = "lbl_excess_counterfeits"
    Me.lbl_excess_counterfeits.Size = New System.Drawing.Size(127, 13)
    Me.lbl_excess_counterfeits.TabIndex = 4
    Me.lbl_excess_counterfeits.Text = "xexcess_counterfeits"
    Me.lbl_excess_counterfeits.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_13
    '
    Me.btn_13.Location = New System.Drawing.Point(350, 37)
    Me.btn_13.Name = "btn_13"
    Me.btn_13.Size = New System.Drawing.Size(75, 20)
    Me.btn_13.TabIndex = 3
    Me.btn_13.Text = "xIgnore"
    Me.btn_13.UseVisualStyleBackColor = True
    '
    'lbl_bill_acceptor_hardware_failure
    '
    Me.lbl_bill_acceptor_hardware_failure.AutoSize = True
    Me.lbl_bill_acceptor_hardware_failure.BackColor = System.Drawing.Color.Transparent
    Me.lbl_bill_acceptor_hardware_failure.Location = New System.Drawing.Point(10, 41)
    Me.lbl_bill_acceptor_hardware_failure.Name = "lbl_bill_acceptor_hardware_failure"
    Me.lbl_bill_acceptor_hardware_failure.Size = New System.Drawing.Size(190, 13)
    Me.lbl_bill_acceptor_hardware_failure.TabIndex = 2
    Me.lbl_bill_acceptor_hardware_failure.Tag = "65577"
    Me.lbl_bill_acceptor_hardware_failure.Text = "xbill_acceptor_hardware_failure"
    Me.lbl_bill_acceptor_hardware_failure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_12
    '
    Me.btn_12.Location = New System.Drawing.Point(350, 15)
    Me.btn_12.Name = "btn_12"
    Me.btn_12.Size = New System.Drawing.Size(75, 20)
    Me.btn_12.TabIndex = 1
    Me.btn_12.Text = "xIgnore"
    Me.btn_12.UseVisualStyleBackColor = True
    '
    'lbl_bill_jam
    '
    Me.lbl_bill_jam.AutoSize = True
    Me.lbl_bill_jam.Location = New System.Drawing.Point(10, 19)
    Me.lbl_bill_jam.Name = "lbl_bill_jam"
    Me.lbl_bill_jam.Size = New System.Drawing.Size(59, 13)
    Me.lbl_bill_jam.TabIndex = 0
    Me.lbl_bill_jam.Tag = "65576"
    Me.lbl_bill_jam.Text = "xbill_jam"
    Me.lbl_bill_jam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_coins
    '
    Me.gb_coins.Controls.Add(Me.btn_36)
    Me.gb_coins.Controls.Add(Me.lbl_coin_in_lockout_malfunction)
    Me.gb_coins.Controls.Add(Me.btn_35)
    Me.gb_coins.Controls.Add(Me.lbl_reverse_coin_in)
    Me.gb_coins.Controls.Add(Me.btn_34)
    Me.gb_coins.Controls.Add(Me.lbl_coin_in_tilt)
    Me.gb_coins.Location = New System.Drawing.Point(5, 466)
    Me.gb_coins.Name = "gb_coins"
    Me.gb_coins.Size = New System.Drawing.Size(432, 84)
    Me.gb_coins.TabIndex = 13
    Me.gb_coins.TabStop = False
    Me.gb_coins.Text = "xCoins_Acceptor"
    '
    'btn_36
    '
    Me.btn_36.Location = New System.Drawing.Point(350, 58)
    Me.btn_36.Name = "btn_36"
    Me.btn_36.Size = New System.Drawing.Size(75, 20)
    Me.btn_36.TabIndex = 5
    Me.btn_36.Text = "xIgnore"
    Me.btn_36.UseVisualStyleBackColor = True
    '
    'lbl_coin_in_lockout_malfunction
    '
    Me.lbl_coin_in_lockout_malfunction.AutoSize = True
    Me.lbl_coin_in_lockout_malfunction.Location = New System.Drawing.Point(10, 62)
    Me.lbl_coin_in_lockout_malfunction.Name = "lbl_coin_in_lockout_malfunction"
    Me.lbl_coin_in_lockout_malfunction.Size = New System.Drawing.Size(175, 13)
    Me.lbl_coin_in_lockout_malfunction.TabIndex = 4
    Me.lbl_coin_in_lockout_malfunction.Tag = "65576"
    Me.lbl_coin_in_lockout_malfunction.Text = "xcoin_in_lockout_malfunction"
    Me.lbl_coin_in_lockout_malfunction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_35
    '
    Me.btn_35.Location = New System.Drawing.Point(350, 37)
    Me.btn_35.Name = "btn_35"
    Me.btn_35.Size = New System.Drawing.Size(75, 20)
    Me.btn_35.TabIndex = 3
    Me.btn_35.Text = "xIgnore"
    Me.btn_35.UseVisualStyleBackColor = True
    '
    'lbl_reverse_coin_in
    '
    Me.lbl_reverse_coin_in.AutoSize = True
    Me.lbl_reverse_coin_in.Location = New System.Drawing.Point(10, 41)
    Me.lbl_reverse_coin_in.Name = "lbl_reverse_coin_in"
    Me.lbl_reverse_coin_in.Size = New System.Drawing.Size(105, 13)
    Me.lbl_reverse_coin_in.TabIndex = 2
    Me.lbl_reverse_coin_in.Tag = "65576"
    Me.lbl_reverse_coin_in.Text = "xreverse_coin_in"
    Me.lbl_reverse_coin_in.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_34
    '
    Me.btn_34.Location = New System.Drawing.Point(350, 15)
    Me.btn_34.Name = "btn_34"
    Me.btn_34.Size = New System.Drawing.Size(75, 20)
    Me.btn_34.TabIndex = 1
    Me.btn_34.Text = "xIgnore"
    Me.btn_34.UseVisualStyleBackColor = True
    '
    'lbl_coin_in_tilt
    '
    Me.lbl_coin_in_tilt.AutoSize = True
    Me.lbl_coin_in_tilt.Location = New System.Drawing.Point(10, 19)
    Me.lbl_coin_in_tilt.Name = "lbl_coin_in_tilt"
    Me.lbl_coin_in_tilt.Size = New System.Drawing.Size(75, 13)
    Me.lbl_coin_in_tilt.TabIndex = 0
    Me.lbl_coin_in_tilt.Tag = "65576"
    Me.lbl_coin_in_tilt.Text = "xcoin_in_tilt"
    Me.lbl_coin_in_tilt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_doors
    '
    Me.gb_doors.Controls.Add(Me.btn_5)
    Me.gb_doors.Controls.Add(Me.lbl_cashbox_door)
    Me.gb_doors.Controls.Add(Me.btn_4)
    Me.gb_doors.Controls.Add(Me.lbl_belly_door)
    Me.gb_doors.Controls.Add(Me.btn_3)
    Me.gb_doors.Controls.Add(Me.lbl_card_cage)
    Me.gb_doors.Controls.Add(Me.btn_2)
    Me.gb_doors.Controls.Add(Me.lbl_drop_door)
    Me.gb_doors.Controls.Add(Me.btn_1)
    Me.gb_doors.Controls.Add(Me.lbl_slot_door)
    Me.gb_doors.Location = New System.Drawing.Point(5, 553)
    Me.gb_doors.Name = "gb_doors"
    Me.gb_doors.Size = New System.Drawing.Size(432, 117)
    Me.gb_doors.TabIndex = 15
    Me.gb_doors.TabStop = False
    Me.gb_doors.Text = "xDoors"
    '
    'btn_5
    '
    Me.btn_5.Location = New System.Drawing.Point(350, 93)
    Me.btn_5.Name = "btn_5"
    Me.btn_5.Size = New System.Drawing.Size(75, 20)
    Me.btn_5.TabIndex = 9
    Me.btn_5.Text = "xIgnore"
    Me.btn_5.UseVisualStyleBackColor = True
    '
    'lbl_cashbox_door
    '
    Me.lbl_cashbox_door.AutoSize = True
    Me.lbl_cashbox_door.BackColor = System.Drawing.Color.Transparent
    Me.lbl_cashbox_door.Location = New System.Drawing.Point(10, 97)
    Me.lbl_cashbox_door.Name = "lbl_cashbox_door"
    Me.lbl_cashbox_door.Size = New System.Drawing.Size(94, 13)
    Me.lbl_cashbox_door.TabIndex = 8
    Me.lbl_cashbox_door.Text = "xcashbox_door"
    Me.lbl_cashbox_door.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_4
    '
    Me.btn_4.Location = New System.Drawing.Point(350, 73)
    Me.btn_4.Name = "btn_4"
    Me.btn_4.Size = New System.Drawing.Size(75, 20)
    Me.btn_4.TabIndex = 7
    Me.btn_4.Text = "xIgnore"
    Me.btn_4.UseVisualStyleBackColor = True
    '
    'lbl_belly_door
    '
    Me.lbl_belly_door.AutoSize = True
    Me.lbl_belly_door.Location = New System.Drawing.Point(10, 77)
    Me.lbl_belly_door.Name = "lbl_belly_door"
    Me.lbl_belly_door.Size = New System.Drawing.Size(78, 13)
    Me.lbl_belly_door.TabIndex = 6
    Me.lbl_belly_door.Text = "xbelly_door "
    Me.lbl_belly_door.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_3
    '
    Me.btn_3.Location = New System.Drawing.Point(350, 53)
    Me.btn_3.Name = "btn_3"
    Me.btn_3.Size = New System.Drawing.Size(75, 20)
    Me.btn_3.TabIndex = 5
    Me.btn_3.Text = "xIgnore"
    Me.btn_3.UseVisualStyleBackColor = True
    '
    'lbl_card_cage
    '
    Me.lbl_card_cage.AutoSize = True
    Me.lbl_card_cage.Location = New System.Drawing.Point(10, 57)
    Me.lbl_card_cage.Name = "lbl_card_cage"
    Me.lbl_card_cage.Size = New System.Drawing.Size(73, 13)
    Me.lbl_card_cage.TabIndex = 4
    Me.lbl_card_cage.Text = "xcard_cage"
    Me.lbl_card_cage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_2
    '
    Me.btn_2.Location = New System.Drawing.Point(350, 33)
    Me.btn_2.Name = "btn_2"
    Me.btn_2.Size = New System.Drawing.Size(75, 20)
    Me.btn_2.TabIndex = 3
    Me.btn_2.Text = "xIgnore"
    Me.btn_2.UseVisualStyleBackColor = True
    '
    'lbl_drop_door
    '
    Me.lbl_drop_door.AutoSize = True
    Me.lbl_drop_door.BackColor = System.Drawing.Color.Transparent
    Me.lbl_drop_door.Location = New System.Drawing.Point(10, 37)
    Me.lbl_drop_door.Name = "lbl_drop_door"
    Me.lbl_drop_door.Size = New System.Drawing.Size(73, 13)
    Me.lbl_drop_door.TabIndex = 2
    Me.lbl_drop_door.Text = "xdrop_door"
    Me.lbl_drop_door.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_1
    '
    Me.btn_1.Location = New System.Drawing.Point(350, 13)
    Me.btn_1.Name = "btn_1"
    Me.btn_1.Size = New System.Drawing.Size(75, 20)
    Me.btn_1.TabIndex = 1
    Me.btn_1.Text = "xIgnore"
    Me.btn_1.UseVisualStyleBackColor = True
    '
    'lbl_slot_door
    '
    Me.lbl_slot_door.AutoSize = True
    Me.lbl_slot_door.Location = New System.Drawing.Point(10, 17)
    Me.lbl_slot_door.Name = "lbl_slot_door"
    Me.lbl_slot_door.Size = New System.Drawing.Size(67, 13)
    Me.lbl_slot_door.TabIndex = 0
    Me.lbl_slot_door.Text = "xslot_door"
    Me.lbl_slot_door.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_printers
    '
    Me.gb_printers.Controls.Add(Me.btn_20)
    Me.gb_printers.Controls.Add(Me.lbl_printer_power_on)
    Me.gb_printers.Controls.Add(Me.btn_19)
    Me.gb_printers.Controls.Add(Me.lbl_printer_carriage_jammed)
    Me.gb_printers.Controls.Add(Me.btn_18)
    Me.gb_printers.Controls.Add(Me.lbl_printer_paper_low)
    Me.gb_printers.Controls.Add(Me.btn_17)
    Me.gb_printers.Controls.Add(Me.lbl_replace_printer_ribbon)
    Me.gb_printers.Controls.Add(Me.btn_16)
    Me.gb_printers.Controls.Add(Me.lbl_printer_paper_out_error)
    Me.gb_printers.Controls.Add(Me.btn_15)
    Me.gb_printers.Controls.Add(Me.lbl_printer_communication_error)
    Me.gb_printers.Location = New System.Drawing.Point(443, 2)
    Me.gb_printers.Name = "gb_printers"
    Me.gb_printers.Size = New System.Drawing.Size(432, 151)
    Me.gb_printers.TabIndex = 14
    Me.gb_printers.TabStop = False
    Me.gb_printers.Text = "xPrinters"
    '
    'btn_20
    '
    Me.btn_20.Location = New System.Drawing.Point(350, 125)
    Me.btn_20.Name = "btn_20"
    Me.btn_20.Size = New System.Drawing.Size(75, 20)
    Me.btn_20.TabIndex = 11
    Me.btn_20.Text = "xIgnore"
    Me.btn_20.UseVisualStyleBackColor = True
    '
    'lbl_printer_power_on
    '
    Me.lbl_printer_power_on.AutoSize = True
    Me.lbl_printer_power_on.Location = New System.Drawing.Point(10, 129)
    Me.lbl_printer_power_on.Name = "lbl_printer_power_on"
    Me.lbl_printer_power_on.Size = New System.Drawing.Size(115, 13)
    Me.lbl_printer_power_on.TabIndex = 10
    Me.lbl_printer_power_on.Tag = "65654"
    Me.lbl_printer_power_on.Text = "xprinter_power_on"
    Me.lbl_printer_power_on.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_19
    '
    Me.btn_19.Location = New System.Drawing.Point(350, 103)
    Me.btn_19.Name = "btn_19"
    Me.btn_19.Size = New System.Drawing.Size(75, 20)
    Me.btn_19.TabIndex = 9
    Me.btn_19.Text = "xIgnore"
    Me.btn_19.UseVisualStyleBackColor = True
    '
    'lbl_printer_carriage_jammed
    '
    Me.lbl_printer_carriage_jammed.AutoSize = True
    Me.lbl_printer_carriage_jammed.BackColor = System.Drawing.Color.Transparent
    Me.lbl_printer_carriage_jammed.Location = New System.Drawing.Point(10, 107)
    Me.lbl_printer_carriage_jammed.Name = "lbl_printer_carriage_jammed"
    Me.lbl_printer_carriage_jammed.Size = New System.Drawing.Size(160, 13)
    Me.lbl_printer_carriage_jammed.TabIndex = 8
    Me.lbl_printer_carriage_jammed.Tag = "65656"
    Me.lbl_printer_carriage_jammed.Text = "xprinter_carriage_jammed"
    Me.lbl_printer_carriage_jammed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_18
    '
    Me.btn_18.Location = New System.Drawing.Point(350, 81)
    Me.btn_18.Name = "btn_18"
    Me.btn_18.Size = New System.Drawing.Size(75, 20)
    Me.btn_18.TabIndex = 7
    Me.btn_18.Text = "xIgnore"
    Me.btn_18.UseVisualStyleBackColor = True
    '
    'lbl_printer_paper_low
    '
    Me.lbl_printer_paper_low.AutoSize = True
    Me.lbl_printer_paper_low.Location = New System.Drawing.Point(10, 85)
    Me.lbl_printer_paper_low.Name = "lbl_printer_paper_low"
    Me.lbl_printer_paper_low.Size = New System.Drawing.Size(118, 13)
    Me.lbl_printer_paper_low.TabIndex = 6
    Me.lbl_printer_paper_low.Tag = "65652"
    Me.lbl_printer_paper_low.Text = "xprinter_paper_low"
    Me.lbl_printer_paper_low.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_17
    '
    Me.btn_17.Location = New System.Drawing.Point(350, 59)
    Me.btn_17.Name = "btn_17"
    Me.btn_17.Size = New System.Drawing.Size(75, 20)
    Me.btn_17.TabIndex = 5
    Me.btn_17.Text = "xIgnore"
    Me.btn_17.UseVisualStyleBackColor = True
    '
    'lbl_replace_printer_ribbon
    '
    Me.lbl_replace_printer_ribbon.AutoSize = True
    Me.lbl_replace_printer_ribbon.Location = New System.Drawing.Point(10, 63)
    Me.lbl_replace_printer_ribbon.Name = "lbl_replace_printer_ribbon"
    Me.lbl_replace_printer_ribbon.Size = New System.Drawing.Size(144, 13)
    Me.lbl_replace_printer_ribbon.TabIndex = 4
    Me.lbl_replace_printer_ribbon.Tag = "65655"
    Me.lbl_replace_printer_ribbon.Text = "xreplace_printer_ribbon"
    Me.lbl_replace_printer_ribbon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_16
    '
    Me.btn_16.Location = New System.Drawing.Point(350, 37)
    Me.btn_16.Name = "btn_16"
    Me.btn_16.Size = New System.Drawing.Size(75, 20)
    Me.btn_16.TabIndex = 3
    Me.btn_16.Text = "xIgnore"
    Me.btn_16.UseVisualStyleBackColor = True
    '
    'lbl_printer_paper_out_error
    '
    Me.lbl_printer_paper_out_error.AutoSize = True
    Me.lbl_printer_paper_out_error.BackColor = System.Drawing.Color.Transparent
    Me.lbl_printer_paper_out_error.Location = New System.Drawing.Point(10, 41)
    Me.lbl_printer_paper_out_error.Name = "lbl_printer_paper_out_error"
    Me.lbl_printer_paper_out_error.Size = New System.Drawing.Size(153, 13)
    Me.lbl_printer_paper_out_error.TabIndex = 2
    Me.lbl_printer_paper_out_error.Tag = "65633"
    Me.lbl_printer_paper_out_error.Text = "xprinter_paper_out_error"
    Me.lbl_printer_paper_out_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_15
    '
    Me.btn_15.Location = New System.Drawing.Point(350, 15)
    Me.btn_15.Name = "btn_15"
    Me.btn_15.Size = New System.Drawing.Size(75, 20)
    Me.btn_15.TabIndex = 1
    Me.btn_15.Text = "xIgnore"
    Me.btn_15.UseVisualStyleBackColor = True
    '
    'lbl_printer_communication_error
    '
    Me.lbl_printer_communication_error.AutoSize = True
    Me.lbl_printer_communication_error.Location = New System.Drawing.Point(10, 21)
    Me.lbl_printer_communication_error.Name = "lbl_printer_communication_error"
    Me.lbl_printer_communication_error.Size = New System.Drawing.Size(181, 13)
    Me.lbl_printer_communication_error.TabIndex = 0
    Me.lbl_printer_communication_error.Tag = "65632"
    Me.lbl_printer_communication_error.Text = "xprinter_communication_error"
    Me.lbl_printer_communication_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_egm
    '
    Me.gb_egm.Controls.Add(Me.btn_27)
    Me.gb_egm.Controls.Add(Me.lbl_partitioned_eprom_error_bad_checksum_compare)
    Me.gb_egm.Controls.Add(Me.btn_28)
    Me.gb_egm.Controls.Add(Me.lbl_backup_battery_detected)
    Me.gb_egm.Controls.Add(Me.btn_25)
    Me.gb_egm.Controls.Add(Me.lbl_eprom_error_bad_checksum_compare)
    Me.gb_egm.Controls.Add(Me.btn_26)
    Me.gb_egm.Controls.Add(Me.lbl_partitioned_eprom_error_checksum_version_changed)
    Me.gb_egm.Controls.Add(Me.btn_24)
    Me.gb_egm.Controls.Add(Me.lbl_eprom_error_different_checksum_version_changed)
    Me.gb_egm.Controls.Add(Me.btn_23)
    Me.gb_egm.Controls.Add(Me.lbl_eprom_error_bad_device)
    Me.gb_egm.Controls.Add(Me.btn_11)
    Me.gb_egm.Controls.Add(Me.lbl_eprom_error_data_error)
    Me.gb_egm.Controls.Add(Me.btn_10)
    Me.gb_egm.Controls.Add(Me.lbl_cmos_ram_error_bad_device)
    Me.gb_egm.Controls.Add(Me.btn_9)
    Me.gb_egm.Controls.Add(Me.lbl_cmos_error_no_data_recovered)
    Me.gb_egm.Controls.Add(Me.btn_8)
    Me.gb_egm.Controls.Add(Me.lbl_cmos_error_data_recovered)
    Me.gb_egm.Location = New System.Drawing.Point(443, 155)
    Me.gb_egm.Name = "gb_egm"
    Me.gb_egm.Size = New System.Drawing.Size(432, 240)
    Me.gb_egm.TabIndex = 16
    Me.gb_egm.TabStop = False
    Me.gb_egm.Text = " "
    '
    'btn_27
    '
    Me.btn_27.Location = New System.Drawing.Point(350, 191)
    Me.btn_27.Name = "btn_27"
    Me.btn_27.Size = New System.Drawing.Size(75, 20)
    Me.btn_27.TabIndex = 17
    Me.btn_27.Text = "xIgnore"
    Me.btn_27.UseVisualStyleBackColor = True
    '
    'lbl_partitioned_eprom_error_bad_checksum_compare
    '
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.AutoSize = True
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.Location = New System.Drawing.Point(10, 195)
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.Name = "lbl_partitioned_eprom_error_bad_checksum_compare"
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.Size = New System.Drawing.Size(317, 13)
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.TabIndex = 16
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.Tag = "65593"
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.Text = "xPartitioned_EPROM_error_(bad_checksum_compare)"
    Me.lbl_partitioned_eprom_error_bad_checksum_compare.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_28
    '
    Me.btn_28.Location = New System.Drawing.Point(350, 213)
    Me.btn_28.Name = "btn_28"
    Me.btn_28.Size = New System.Drawing.Size(75, 20)
    Me.btn_28.TabIndex = 19
    Me.btn_28.Text = "xIgnore"
    Me.btn_28.UseVisualStyleBackColor = True
    '
    'lbl_backup_battery_detected
    '
    Me.lbl_backup_battery_detected.AutoSize = True
    Me.lbl_backup_battery_detected.Location = New System.Drawing.Point(10, 217)
    Me.lbl_backup_battery_detected.Name = "lbl_backup_battery_detected"
    Me.lbl_backup_battery_detected.Size = New System.Drawing.Size(188, 13)
    Me.lbl_backup_battery_detected.TabIndex = 18
    Me.lbl_backup_battery_detected.Tag = "65595"
    Me.lbl_backup_battery_detected.Text = "xLow_backup_battery_detected"
    Me.lbl_backup_battery_detected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_25
    '
    Me.btn_25.Location = New System.Drawing.Point(350, 147)
    Me.btn_25.Name = "btn_25"
    Me.btn_25.Size = New System.Drawing.Size(75, 20)
    Me.btn_25.TabIndex = 13
    Me.btn_25.Text = "xIgnore"
    Me.btn_25.UseVisualStyleBackColor = True
    '
    'lbl_eprom_error_bad_checksum_compare
    '
    Me.lbl_eprom_error_bad_checksum_compare.AutoSize = True
    Me.lbl_eprom_error_bad_checksum_compare.Location = New System.Drawing.Point(10, 151)
    Me.lbl_eprom_error_bad_checksum_compare.Name = "lbl_eprom_error_bad_checksum_compare"
    Me.lbl_eprom_error_bad_checksum_compare.Size = New System.Drawing.Size(249, 13)
    Me.lbl_eprom_error_bad_checksum_compare.TabIndex = 12
    Me.lbl_eprom_error_bad_checksum_compare.Tag = "65591"
    Me.lbl_eprom_error_bad_checksum_compare.Text = "xEPROM_error_(bad_checksum_compare)"
    Me.lbl_eprom_error_bad_checksum_compare.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_26
    '
    Me.btn_26.Location = New System.Drawing.Point(350, 169)
    Me.btn_26.Name = "btn_26"
    Me.btn_26.Size = New System.Drawing.Size(75, 20)
    Me.btn_26.TabIndex = 15
    Me.btn_26.Text = "xIgnore"
    Me.btn_26.UseVisualStyleBackColor = True
    '
    'lbl_partitioned_eprom_error_checksum_version_changed
    '
    Me.lbl_partitioned_eprom_error_checksum_version_changed.AutoSize = True
    Me.lbl_partitioned_eprom_error_checksum_version_changed.Location = New System.Drawing.Point(10, 173)
    Me.lbl_partitioned_eprom_error_checksum_version_changed.Name = "lbl_partitioned_eprom_error_checksum_version_changed"
    Me.lbl_partitioned_eprom_error_checksum_version_changed.Size = New System.Drawing.Size(348, 13)
    Me.lbl_partitioned_eprom_error_checksum_version_changed.TabIndex = 14
    Me.lbl_partitioned_eprom_error_checksum_version_changed.Tag = "65592"
    Me.lbl_partitioned_eprom_error_checksum_version_changed.Text = "xPartitioned_EPROM_error_(checksum_-_versoin_changed)"
    Me.lbl_partitioned_eprom_error_checksum_version_changed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_24
    '
    Me.btn_24.Location = New System.Drawing.Point(350, 125)
    Me.btn_24.Name = "btn_24"
    Me.btn_24.Size = New System.Drawing.Size(75, 20)
    Me.btn_24.TabIndex = 11
    Me.btn_24.Text = "xIgnore"
    Me.btn_24.UseVisualStyleBackColor = True
    '
    'lbl_eprom_error_different_checksum_version_changed
    '
    Me.lbl_eprom_error_different_checksum_version_changed.AutoSize = True
    Me.lbl_eprom_error_different_checksum_version_changed.Location = New System.Drawing.Point(10, 129)
    Me.lbl_eprom_error_different_checksum_version_changed.Name = "lbl_eprom_error_different_checksum_version_changed"
    Me.lbl_eprom_error_different_checksum_version_changed.Size = New System.Drawing.Size(335, 13)
    Me.lbl_eprom_error_different_checksum_version_changed.TabIndex = 10
    Me.lbl_eprom_error_different_checksum_version_changed.Tag = "65590"
    Me.lbl_eprom_error_different_checksum_version_changed.Text = "xEPROM_error_(different_checksum_-_version_changed)"
    Me.lbl_eprom_error_different_checksum_version_changed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_23
    '
    Me.btn_23.Location = New System.Drawing.Point(350, 103)
    Me.btn_23.Name = "btn_23"
    Me.btn_23.Size = New System.Drawing.Size(75, 20)
    Me.btn_23.TabIndex = 9
    Me.btn_23.Text = "xIgnore"
    Me.btn_23.UseVisualStyleBackColor = True
    '
    'lbl_eprom_error_bad_device
    '
    Me.lbl_eprom_error_bad_device.AutoSize = True
    Me.lbl_eprom_error_bad_device.Location = New System.Drawing.Point(10, 107)
    Me.lbl_eprom_error_bad_device.Name = "lbl_eprom_error_bad_device"
    Me.lbl_eprom_error_bad_device.Size = New System.Drawing.Size(179, 13)
    Me.lbl_eprom_error_bad_device.TabIndex = 8
    Me.lbl_eprom_error_bad_device.Tag = "65589"
    Me.lbl_eprom_error_bad_device.Text = "xEEPROM_error_(bad_device)"
    Me.lbl_eprom_error_bad_device.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_11
    '
    Me.btn_11.Location = New System.Drawing.Point(350, 81)
    Me.btn_11.Name = "btn_11"
    Me.btn_11.Size = New System.Drawing.Size(75, 20)
    Me.btn_11.TabIndex = 7
    Me.btn_11.Text = "xIgnore"
    Me.btn_11.UseVisualStyleBackColor = True
    '
    'lbl_eprom_error_data_error
    '
    Me.lbl_eprom_error_data_error.AutoSize = True
    Me.lbl_eprom_error_data_error.Location = New System.Drawing.Point(10, 85)
    Me.lbl_eprom_error_data_error.Name = "lbl_eprom_error_data_error"
    Me.lbl_eprom_error_data_error.Size = New System.Drawing.Size(175, 13)
    Me.lbl_eprom_error_data_error.TabIndex = 6
    Me.lbl_eprom_error_data_error.Tag = "65588"
    Me.lbl_eprom_error_data_error.Text = "xEEPROM_error_(data_error)"
    Me.lbl_eprom_error_data_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_10
    '
    Me.btn_10.Location = New System.Drawing.Point(350, 59)
    Me.btn_10.Name = "btn_10"
    Me.btn_10.Size = New System.Drawing.Size(75, 20)
    Me.btn_10.TabIndex = 5
    Me.btn_10.Text = "xIgnore"
    Me.btn_10.UseVisualStyleBackColor = True
    '
    'lbl_cmos_ram_error_bad_device
    '
    Me.lbl_cmos_ram_error_bad_device.AutoSize = True
    Me.lbl_cmos_ram_error_bad_device.Location = New System.Drawing.Point(10, 63)
    Me.lbl_cmos_ram_error_bad_device.Name = "lbl_cmos_ram_error_bad_device"
    Me.lbl_cmos_ram_error_bad_device.Size = New System.Drawing.Size(199, 13)
    Me.lbl_cmos_ram_error_bad_device.TabIndex = 4
    Me.lbl_cmos_ram_error_bad_device.Tag = "65587"
    Me.lbl_cmos_ram_error_bad_device.Text = "xCMOS_RAM_error_(bad_device)"
    Me.lbl_cmos_ram_error_bad_device.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_9
    '
    Me.btn_9.Location = New System.Drawing.Point(350, 37)
    Me.btn_9.Name = "btn_9"
    Me.btn_9.Size = New System.Drawing.Size(75, 20)
    Me.btn_9.TabIndex = 3
    Me.btn_9.Text = "xIgnore"
    Me.btn_9.UseVisualStyleBackColor = True
    '
    'lbl_cmos_error_no_data_recovered
    '
    Me.lbl_cmos_error_no_data_recovered.AutoSize = True
    Me.lbl_cmos_error_no_data_recovered.BackColor = System.Drawing.Color.Transparent
    Me.lbl_cmos_error_no_data_recovered.Location = New System.Drawing.Point(10, 41)
    Me.lbl_cmos_error_no_data_recovered.Name = "lbl_cmos_error_no_data_recovered"
    Me.lbl_cmos_error_no_data_recovered.Size = New System.Drawing.Size(333, 13)
    Me.lbl_cmos_error_no_data_recovered.TabIndex = 2
    Me.lbl_cmos_error_no_data_recovered.Tag = "65586"
    Me.lbl_cmos_error_no_data_recovered.Text = "xCMOS_RAM_error_(no_data_recovered_from_EEPROM)"
    Me.lbl_cmos_error_no_data_recovered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_8
    '
    Me.btn_8.Location = New System.Drawing.Point(350, 15)
    Me.btn_8.Name = "btn_8"
    Me.btn_8.Size = New System.Drawing.Size(75, 20)
    Me.btn_8.TabIndex = 1
    Me.btn_8.Text = "xIgnore"
    Me.btn_8.UseVisualStyleBackColor = True
    '
    'lbl_cmos_error_data_recovered
    '
    Me.lbl_cmos_error_data_recovered.AutoSize = True
    Me.lbl_cmos_error_data_recovered.Location = New System.Drawing.Point(10, 19)
    Me.lbl_cmos_error_data_recovered.Name = "lbl_cmos_error_data_recovered"
    Me.lbl_cmos_error_data_recovered.Size = New System.Drawing.Size(309, 13)
    Me.lbl_cmos_error_data_recovered.TabIndex = 0
    Me.lbl_cmos_error_data_recovered.Tag = "65585"
    Me.lbl_cmos_error_data_recovered.Text = "xCMOS_RAM_error_(data recovered_from_EEPROM)"
    Me.lbl_cmos_error_data_recovered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_call_attendant
    '
    Me.gb_call_attendant.Controls.Add(Me.btn_29)
    Me.gb_call_attendant.Controls.Add(Me.Button1)
    Me.gb_call_attendant.Controls.Add(Me.lbl_call_attendant)
    Me.gb_call_attendant.Location = New System.Drawing.Point(443, 401)
    Me.gb_call_attendant.Name = "gb_call_attendant"
    Me.gb_call_attendant.Size = New System.Drawing.Size(432, 42)
    Me.gb_call_attendant.TabIndex = 17
    Me.gb_call_attendant.TabStop = False
    Me.gb_call_attendant.Text = "xCallAttendant"
    '
    'btn_29
    '
    Me.btn_29.Location = New System.Drawing.Point(350, 15)
    Me.btn_29.Name = "btn_29"
    Me.btn_29.Size = New System.Drawing.Size(75, 20)
    Me.btn_29.TabIndex = 1
    Me.btn_29.Text = "xIgnore"
    Me.btn_29.UseVisualStyleBackColor = True
    '
    'Button1
    '
    Me.Button1.Location = New System.Drawing.Point(271, 46)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(61, 20)
    Me.Button1.TabIndex = 1
    Me.Button1.TabStop = False
    Me.Button1.Text = "xIgnore"
    Me.Button1.UseVisualStyleBackColor = True
    Me.Button1.Visible = False
    '
    'lbl_call_attendant
    '
    Me.lbl_call_attendant.AutoSize = True
    Me.lbl_call_attendant.Location = New System.Drawing.Point(10, 19)
    Me.lbl_call_attendant.Name = "lbl_call_attendant"
    Me.lbl_call_attendant.Size = New System.Drawing.Size(91, 13)
    Me.lbl_call_attendant.TabIndex = 0
    Me.lbl_call_attendant.Text = "xCallAttendant"
    Me.lbl_call_attendant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_machine_status
    '
    Me.gb_machine_status.Controls.Add(Me.btn_30)
    Me.gb_machine_status.Controls.Add(Me.lbl_locked_by_manually)
    Me.gb_machine_status.Controls.Add(Me.btn_39)
    Me.gb_machine_status.Controls.Add(Me.lbl_locked_by_intrusion)
    Me.gb_machine_status.Controls.Add(Me.btn_32)
    Me.gb_machine_status.Controls.Add(Me.lbl_locked_by_signature)
    Me.gb_machine_status.Location = New System.Drawing.Point(443, 448)
    Me.gb_machine_status.Name = "gb_machine_status"
    Me.gb_machine_status.Size = New System.Drawing.Size(432, 42)
    Me.gb_machine_status.TabIndex = 18
    Me.gb_machine_status.TabStop = False
    Me.gb_machine_status.Text = "xMachineStatus"
    '
    'btn_30
    '
    Me.btn_30.Location = New System.Drawing.Point(350, 59)
    Me.btn_30.Name = "btn_30"
    Me.btn_30.Size = New System.Drawing.Size(75, 20)
    Me.btn_30.TabIndex = 5
    Me.btn_30.Text = "xIgnore"
    Me.btn_30.UseVisualStyleBackColor = True
    Me.btn_30.Visible = False
    '
    'lbl_locked_by_manually
    '
    Me.lbl_locked_by_manually.AutoSize = True
    Me.lbl_locked_by_manually.Location = New System.Drawing.Point(10, 63)
    Me.lbl_locked_by_manually.Name = "lbl_locked_by_manually"
    Me.lbl_locked_by_manually.Size = New System.Drawing.Size(119, 13)
    Me.lbl_locked_by_manually.TabIndex = 4
    Me.lbl_locked_by_manually.Text = "xLockedByManually"
    Me.lbl_locked_by_manually.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lbl_locked_by_manually.Visible = False
    '
    'btn_39
    '
    Me.btn_39.Location = New System.Drawing.Point(350, 15)
    Me.btn_39.Name = "btn_39"
    Me.btn_39.Size = New System.Drawing.Size(75, 20)
    Me.btn_39.TabIndex = 1
    Me.btn_39.Text = "xIgnore"
    Me.btn_39.UseVisualStyleBackColor = True
    '
    'lbl_locked_by_intrusion
    '
    Me.lbl_locked_by_intrusion.AutoSize = True
    Me.lbl_locked_by_intrusion.Location = New System.Drawing.Point(10, 19)
    Me.lbl_locked_by_intrusion.Name = "lbl_locked_by_intrusion"
    Me.lbl_locked_by_intrusion.Size = New System.Drawing.Size(120, 13)
    Me.lbl_locked_by_intrusion.TabIndex = 0
    Me.lbl_locked_by_intrusion.Text = "xLockedByIntrusion"
    Me.lbl_locked_by_intrusion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'btn_32
    '
    Me.btn_32.Location = New System.Drawing.Point(350, 37)
    Me.btn_32.Name = "btn_32"
    Me.btn_32.Size = New System.Drawing.Size(75, 20)
    Me.btn_32.TabIndex = 3
    Me.btn_32.Text = "xIgnore"
    Me.btn_32.UseVisualStyleBackColor = True
    Me.btn_32.Visible = False
    '
    'lbl_locked_by_signature
    '
    Me.lbl_locked_by_signature.AutoSize = True
    Me.lbl_locked_by_signature.Location = New System.Drawing.Point(10, 41)
    Me.lbl_locked_by_signature.Name = "lbl_locked_by_signature"
    Me.lbl_locked_by_signature.Size = New System.Drawing.Size(124, 13)
    Me.lbl_locked_by_signature.TabIndex = 2
    Me.lbl_locked_by_signature.Text = "xLockedBySignature"
    Me.lbl_locked_by_signature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lbl_locked_by_signature.Visible = False
    '
    'gb_software_validation
    '
    Me.gb_software_validation.Controls.Add(Me.lbl_last_checked_detail)
    Me.gb_software_validation.Controls.Add(Me.lbl_authentication_status)
    Me.gb_software_validation.Controls.Add(Me.lbl_authentication_last_checked)
    Me.gb_software_validation.Location = New System.Drawing.Point(443, 495)
    Me.gb_software_validation.Name = "gb_software_validation"
    Me.gb_software_validation.Size = New System.Drawing.Size(432, 67)
    Me.gb_software_validation.TabIndex = 19
    Me.gb_software_validation.TabStop = False
    Me.gb_software_validation.Text = "xSoftwareValidation"
    Me.gb_software_validation.Visible = False
    '
    'lbl_last_checked_detail
    '
    Me.lbl_last_checked_detail.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_last_checked_detail.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_last_checked_detail.Location = New System.Drawing.Point(182, 33)
    Me.lbl_last_checked_detail.Name = "lbl_last_checked_detail"
    Me.lbl_last_checked_detail.Size = New System.Drawing.Size(163, 28)
    Me.lbl_last_checked_detail.TabIndex = 2
    Me.lbl_last_checked_detail.Text = "xLastChecked"
    Me.lbl_last_checked_detail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_authentication_status
    '
    Me.lbl_authentication_status.AutoSize = True
    Me.lbl_authentication_status.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_status.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_authentication_status.Location = New System.Drawing.Point(10, 19)
    Me.lbl_authentication_status.Name = "lbl_authentication_status"
    Me.lbl_authentication_status.Size = New System.Drawing.Size(131, 13)
    Me.lbl_authentication_status.TabIndex = 0
    Me.lbl_authentication_status.Text = "xAuthenticationStatus"
    Me.lbl_authentication_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_authentication_last_checked
    '
    Me.lbl_authentication_last_checked.AutoSize = True
    Me.lbl_authentication_last_checked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_authentication_last_checked.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_authentication_last_checked.Location = New System.Drawing.Point(10, 41)
    Me.lbl_authentication_last_checked.Name = "lbl_authentication_last_checked"
    Me.lbl_authentication_last_checked.Size = New System.Drawing.Size(168, 13)
    Me.lbl_authentication_last_checked.TabIndex = 1
    Me.lbl_authentication_last_checked.Text = "xAuthenticationLastChecked"
    Me.lbl_authentication_last_checked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frm_terminal_status_detail
    '
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ClientSize = New System.Drawing.Size(1000, 782)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_terminal_status_detail"
    Me.Text = "frm_terminal_status_detail"
    Me.panel_data.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_service.ResumeLayout(False)
    Me.gb_service.PerformLayout()
    Me.panel_info_terminal.ResumeLayout(False)
    Me.gb_plays.ResumeLayout(False)
    Me.gb_plays.PerformLayout()
    Me.gb_handpays.ResumeLayout(False)
    Me.gb_handpays.PerformLayout()
    Me.gb_bills.ResumeLayout(False)
    Me.gb_bills.PerformLayout()
    Me.gb_coins.ResumeLayout(False)
    Me.gb_coins.PerformLayout()
    Me.gb_doors.ResumeLayout(False)
    Me.gb_doors.PerformLayout()
    Me.gb_printers.ResumeLayout(False)
    Me.gb_printers.PerformLayout()
    Me.gb_egm.ResumeLayout(False)
    Me.gb_egm.PerformLayout()
    Me.gb_call_attendant.ResumeLayout(False)
    Me.gb_call_attendant.PerformLayout()
    Me.gb_machine_status.ResumeLayout(False)
    Me.gb_machine_status.PerformLayout()
    Me.gb_software_validation.ResumeLayout(False)
    Me.gb_software_validation.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_warning As System.Windows.Forms.Label
  Friend WithEvents tf_warning As GUI_Controls.uc_text_field
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents tf_error As GUI_Controls.uc_text_field
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_session As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_provider As GUI_Controls.uc_entry_field
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents gb_service As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_SAS_HOST As System.Windows.Forms.Label
  Friend WithEvents lbl_WCP_WC2 As System.Windows.Forms.Label
  Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
  Friend WithEvents lbl_WCP_Name As System.Windows.Forms.Label
  Friend WithEvents lbl_WC2_Name As System.Windows.Forms.Label
  Friend WithEvents lbl_WCP_ServerName As System.Windows.Forms.Label
  Friend WithEvents lbl_WC2_ServerName As System.Windows.Forms.Label
  Friend WithEvents panel_info_terminal As System.Windows.Forms.Panel
  Friend WithEvents gb_plays As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_highroller_anonymous As System.Windows.Forms.Label
  Friend WithEvents btn_38 As System.Windows.Forms.Button
  Friend WithEvents btn_37 As System.Windows.Forms.Button
  Friend WithEvents lbl_highroller_anonymous_value As System.Windows.Forms.Label
  Friend WithEvents lbl_highroller_value As System.Windows.Forms.Label
  Friend WithEvents btn_33 As System.Windows.Forms.Button
  Friend WithEvents lbl_highroller As System.Windows.Forms.Label
  Friend WithEvents lbl_without_plays_value As System.Windows.Forms.Label
  Friend WithEvents lbl_without_plays As System.Windows.Forms.Label
  Friend WithEvents btn_31 As System.Windows.Forms.Button
  Friend WithEvents lbl_played_alarms_message As System.Windows.Forms.Label
  Friend WithEvents btn_6 As System.Windows.Forms.Button
  Friend WithEvents lbl_played As System.Windows.Forms.Label
  Friend WithEvents btn_22 As System.Windows.Forms.Button
  Friend WithEvents lbl_current_payout_value As System.Windows.Forms.Label
  Friend WithEvents lbl_wonamount_alarms_message As System.Windows.Forms.Label
  Friend WithEvents lbl_wonamount_exceeded As System.Windows.Forms.Label
  Friend WithEvents lbl_current_payout As System.Windows.Forms.Label
  Friend WithEvents gb_handpays As System.Windows.Forms.GroupBox
  Friend WithEvents btn_7 As System.Windows.Forms.Button
  Friend WithEvents btn_21 As System.Windows.Forms.Button
  Friend WithEvents lbl_jackpot_alarm_message As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot As System.Windows.Forms.Label
  Friend WithEvents Button3 As System.Windows.Forms.Button
  Friend WithEvents lbl_c_credits_alarm_message As System.Windows.Forms.Label
  Friend WithEvents lbl_c_credits As System.Windows.Forms.Label
  Friend WithEvents Button4 As System.Windows.Forms.Button
  Friend WithEvents Button5 As System.Windows.Forms.Button
  Friend WithEvents gb_bills As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_counterfeits_alarm_message As System.Windows.Forms.Label
  Friend WithEvents lbl_stacker_counter_value As System.Windows.Forms.Label
  Friend WithEvents lbl_stacker_counter As System.Windows.Forms.Label
  Friend WithEvents lbl_stacker_full As System.Windows.Forms.Label
  Friend WithEvents btn_14 As System.Windows.Forms.Button
  Friend WithEvents lbl_excess_counterfeits As System.Windows.Forms.Label
  Friend WithEvents btn_13 As System.Windows.Forms.Button
  Friend WithEvents lbl_bill_acceptor_hardware_failure As System.Windows.Forms.Label
  Friend WithEvents btn_12 As System.Windows.Forms.Button
  Friend WithEvents lbl_bill_jam As System.Windows.Forms.Label
  Friend WithEvents gb_coins As System.Windows.Forms.GroupBox
  Friend WithEvents btn_36 As System.Windows.Forms.Button
  Friend WithEvents lbl_coin_in_lockout_malfunction As System.Windows.Forms.Label
  Friend WithEvents btn_35 As System.Windows.Forms.Button
  Friend WithEvents lbl_reverse_coin_in As System.Windows.Forms.Label
  Friend WithEvents btn_34 As System.Windows.Forms.Button
  Friend WithEvents lbl_coin_in_tilt As System.Windows.Forms.Label
  Friend WithEvents gb_doors As System.Windows.Forms.GroupBox
  Friend WithEvents btn_5 As System.Windows.Forms.Button
  Friend WithEvents lbl_cashbox_door As System.Windows.Forms.Label
  Friend WithEvents btn_4 As System.Windows.Forms.Button
  Friend WithEvents lbl_belly_door As System.Windows.Forms.Label
  Friend WithEvents btn_3 As System.Windows.Forms.Button
  Friend WithEvents lbl_card_cage As System.Windows.Forms.Label
  Friend WithEvents btn_2 As System.Windows.Forms.Button
  Friend WithEvents lbl_drop_door As System.Windows.Forms.Label
  Friend WithEvents btn_1 As System.Windows.Forms.Button
  Friend WithEvents lbl_slot_door As System.Windows.Forms.Label
  Friend WithEvents gb_printers As System.Windows.Forms.GroupBox
  Friend WithEvents btn_20 As System.Windows.Forms.Button
  Friend WithEvents lbl_printer_power_on As System.Windows.Forms.Label
  Friend WithEvents btn_19 As System.Windows.Forms.Button
  Friend WithEvents lbl_printer_carriage_jammed As System.Windows.Forms.Label
  Friend WithEvents btn_18 As System.Windows.Forms.Button
  Friend WithEvents lbl_printer_paper_low As System.Windows.Forms.Label
  Friend WithEvents btn_17 As System.Windows.Forms.Button
  Friend WithEvents lbl_replace_printer_ribbon As System.Windows.Forms.Label
  Friend WithEvents btn_16 As System.Windows.Forms.Button
  Friend WithEvents lbl_printer_paper_out_error As System.Windows.Forms.Label
  Friend WithEvents btn_15 As System.Windows.Forms.Button
  Friend WithEvents lbl_printer_communication_error As System.Windows.Forms.Label
  Friend WithEvents gb_egm As System.Windows.Forms.GroupBox
  Friend WithEvents btn_27 As System.Windows.Forms.Button
  Friend WithEvents lbl_partitioned_eprom_error_bad_checksum_compare As System.Windows.Forms.Label
  Friend WithEvents btn_28 As System.Windows.Forms.Button
  Friend WithEvents lbl_backup_battery_detected As System.Windows.Forms.Label
  Friend WithEvents btn_25 As System.Windows.Forms.Button
  Friend WithEvents lbl_eprom_error_bad_checksum_compare As System.Windows.Forms.Label
  Friend WithEvents btn_26 As System.Windows.Forms.Button
  Friend WithEvents lbl_partitioned_eprom_error_checksum_version_changed As System.Windows.Forms.Label
  Friend WithEvents btn_24 As System.Windows.Forms.Button
  Friend WithEvents lbl_eprom_error_different_checksum_version_changed As System.Windows.Forms.Label
  Friend WithEvents btn_23 As System.Windows.Forms.Button
  Friend WithEvents lbl_eprom_error_bad_device As System.Windows.Forms.Label
  Friend WithEvents btn_11 As System.Windows.Forms.Button
  Friend WithEvents lbl_eprom_error_data_error As System.Windows.Forms.Label
  Friend WithEvents btn_10 As System.Windows.Forms.Button
  Friend WithEvents lbl_cmos_ram_error_bad_device As System.Windows.Forms.Label
  Friend WithEvents btn_9 As System.Windows.Forms.Button
  Friend WithEvents lbl_cmos_error_no_data_recovered As System.Windows.Forms.Label
  Friend WithEvents btn_8 As System.Windows.Forms.Button
  Friend WithEvents lbl_cmos_error_data_recovered As System.Windows.Forms.Label
  Friend WithEvents gb_call_attendant As System.Windows.Forms.GroupBox
  Friend WithEvents btn_29 As System.Windows.Forms.Button
  Friend WithEvents Button1 As System.Windows.Forms.Button
  Friend WithEvents lbl_call_attendant As System.Windows.Forms.Label
  Friend WithEvents gb_machine_status As System.Windows.Forms.GroupBox
  Friend WithEvents btn_30 As System.Windows.Forms.Button
  Friend WithEvents lbl_locked_by_manually As System.Windows.Forms.Label
  Friend WithEvents btn_39 As System.Windows.Forms.Button
  Friend WithEvents lbl_locked_by_intrusion As System.Windows.Forms.Label
  Friend WithEvents btn_32 As System.Windows.Forms.Button
  Friend WithEvents lbl_locked_by_signature As System.Windows.Forms.Label
  Friend WithEvents gb_software_validation As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_last_checked_detail As System.Windows.Forms.Label
  Friend WithEvents lbl_authentication_status As System.Windows.Forms.Label
  Friend WithEvents lbl_authentication_last_checked As System.Windows.Forms.Label
End Class
