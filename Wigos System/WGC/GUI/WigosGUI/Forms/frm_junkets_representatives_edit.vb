﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_junkets_representatives_edit.vb
' DESCRIPTION:   Create and edit representative jankets
' AUTHOR:        David Perelló
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  DPC    Initial version
' -------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports GUI_CommonMisc

Public Class frm_junkets_representatives_edit
  Inherits frm_base_edit

#Region " Members "

  Private m_junket_read As CLASS_JUNKETS_REPRESENTATIVE
  Private m_junket_edit As CLASS_JUNKETS_REPRESENTATIVE

#End Region ' Members

#Region " Overrides "

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_JUNKETS_REPRESENTATIVES_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8090)

    Me.ef_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710) '  Code
    Call Me.ef_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 10)

    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399) '  Name
    Call Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.ef_creation_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6506) '  Creation date
    Call Me.ef_creation_date.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_creation_date.IsReadOnly = True

    Me.ef_deposit_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8005) '  Deposit amount
    Call Me.ef_deposit_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)
    Me.ef_deposit_amount.IsReadOnly = True

    Me.chk_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008) '  Status
    Me.chk_internal_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8000) '  Internal employee
    Me.gb_comments.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1716) '  Comments

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      ef_code.IsReadOnly = True
      ef_code.TabStop = False
    End If

    If m_junket_read Is Nothing Then
      m_junket_read = DbReadObject
    End If

    If m_junket_edit Is Nothing Then
      m_junket_edit = DbEditedObject
    End If

  End Sub ' GUI_InitControls

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="SqlCtx"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then

      ef_deposit_amount.Value = GUI_FormatCurrency(0)
      ef_creation_date.Value = GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    Else

      ef_code.Value = m_junket_read.Code
      ef_name.Value = m_junket_read.Name
      chk_internal_employee.Checked = m_junket_read.InternalEmployee
      chk_status.Checked = m_junket_read.Enabled
      ef_deposit_amount.Value = GUI_FormatCurrency(m_junket_read.DepositAmount, 2)
      txt_comments.Text = m_junket_read.Comments
      ef_creation_date.Value = GUI_FormatDate(m_junket_read.Creation, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    End If

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_GetScreenData()

    m_junket_edit.Code = ef_code.Value
    m_junket_edit.Name = ef_name.Value
    m_junket_edit.InternalEmployee = chk_internal_employee.Checked
    m_junket_edit.Enabled = chk_status.Checked
    m_junket_edit.Comments = txt_comments.Text

  End Sub ' GUI_GetScreenData

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="DbOperation"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim ctx As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_JUNKETS_REPRESENTATIVE
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        DbStatus = DbEditedObject.DB_Insert(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        DbStatus = DbEditedObject.DB_Update(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _have_junkets As Boolean

    If String.IsNullOrEmpty(ef_code.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5710))
      ef_code.Focus()

      Return False
    End If

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      If CLASS_JUNKETS_REPRESENTATIVE.ExistRepresentativeJunket(ef_code.Value) = ENUM_STATUS.STATUS_DUPLICATE_KEY Then

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8004), ENUM_MB_TYPE.MB_TYPE_WARNING)
        ef_code.Focus()

        Return False
      End If
    End If

    If String.IsNullOrEmpty(ef_name.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4399))
      ef_name.Focus()

      Return False
    End If

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT AndAlso m_junket_read.Enabled AndAlso Not chk_status.Checked Then
      If CLASS_JUNKETS_REPRESENTATIVE.HaveJunketsAssociated(m_junket_edit.Id, _have_junkets) Then
        If _have_junkets Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8092), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then

            Return False
          End If
        End If
      Else
        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub 'GUI_ButtonClick

#End Region

End Class