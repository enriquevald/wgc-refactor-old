<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bonuses_confirm
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_bonus_info = New System.Windows.Forms.GroupBox
    Me.ef_provider_name = New GUI_Controls.uc_entry_field
    Me.ef_to_transfer = New GUI_Controls.uc_entry_field
    Me.ef_current_status = New GUI_Controls.uc_entry_field
    Me.ef_transferred = New GUI_Controls.uc_entry_field
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field
    Me.ef_datetime = New GUI_Controls.uc_entry_field
    Me.cmb_new_status = New GUI_Controls.uc_combo
    Me.panel_data.SuspendLayout()
    Me.gb_bonus_info.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.cmb_new_status)
    Me.panel_data.Controls.Add(Me.gb_bonus_info)
    Me.panel_data.Size = New System.Drawing.Size(558, 190)
    '
    'gb_bonus_info
    '
    Me.gb_bonus_info.Controls.Add(Me.ef_provider_name)
    Me.gb_bonus_info.Controls.Add(Me.ef_to_transfer)
    Me.gb_bonus_info.Controls.Add(Me.ef_current_status)
    Me.gb_bonus_info.Controls.Add(Me.ef_transferred)
    Me.gb_bonus_info.Controls.Add(Me.ef_terminal_name)
    Me.gb_bonus_info.Controls.Add(Me.ef_datetime)
    Me.gb_bonus_info.Location = New System.Drawing.Point(3, 3)
    Me.gb_bonus_info.Name = "gb_bonus_info"
    Me.gb_bonus_info.Size = New System.Drawing.Size(552, 116)
    Me.gb_bonus_info.TabIndex = 0
    Me.gb_bonus_info.TabStop = False
    Me.gb_bonus_info.Text = "xBonus"
    '
    'ef_provider_name
    '
    Me.ef_provider_name.DoubleValue = 0
    Me.ef_provider_name.IntegerValue = 0
    Me.ef_provider_name.IsReadOnly = True
    Me.ef_provider_name.Location = New System.Drawing.Point(18, 51)
    Me.ef_provider_name.Name = "ef_provider_name"
    Me.ef_provider_name.Size = New System.Drawing.Size(249, 25)
    Me.ef_provider_name.SufixText = "Sufix Text"
    Me.ef_provider_name.SufixTextVisible = True
    Me.ef_provider_name.TabIndex = 5
    Me.ef_provider_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_provider_name.TextValue = ""
    Me.ef_provider_name.Value = ""
    '
    'ef_to_transfer
    '
    Me.ef_to_transfer.DoubleValue = 0
    Me.ef_to_transfer.IntegerValue = 0
    Me.ef_to_transfer.IsReadOnly = True
    Me.ef_to_transfer.Location = New System.Drawing.Point(276, 50)
    Me.ef_to_transfer.Name = "ef_to_transfer"
    Me.ef_to_transfer.Size = New System.Drawing.Size(203, 24)
    Me.ef_to_transfer.SufixText = "Sufix Text"
    Me.ef_to_transfer.SufixTextVisible = True
    Me.ef_to_transfer.TabIndex = 4
    Me.ef_to_transfer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_to_transfer.TextValue = ""
    Me.ef_to_transfer.TextWidth = 100
    Me.ef_to_transfer.Value = ""
    '
    'ef_current_status
    '
    Me.ef_current_status.DoubleValue = 0
    Me.ef_current_status.IntegerValue = 0
    Me.ef_current_status.IsReadOnly = True
    Me.ef_current_status.Location = New System.Drawing.Point(276, 80)
    Me.ef_current_status.Name = "ef_current_status"
    Me.ef_current_status.Size = New System.Drawing.Size(255, 24)
    Me.ef_current_status.SufixText = "Sufix Text"
    Me.ef_current_status.SufixTextVisible = True
    Me.ef_current_status.TabIndex = 3
    Me.ef_current_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_current_status.TextValue = ""
    Me.ef_current_status.TextWidth = 100
    Me.ef_current_status.Value = ""
    '
    'ef_transferred
    '
    Me.ef_transferred.DoubleValue = 0
    Me.ef_transferred.IntegerValue = 0
    Me.ef_transferred.IsReadOnly = True
    Me.ef_transferred.Location = New System.Drawing.Point(276, 20)
    Me.ef_transferred.Name = "ef_transferred"
    Me.ef_transferred.Size = New System.Drawing.Size(203, 24)
    Me.ef_transferred.SufixText = "Sufix Text"
    Me.ef_transferred.SufixTextVisible = True
    Me.ef_transferred.TabIndex = 2
    Me.ef_transferred.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_transferred.TextValue = ""
    Me.ef_transferred.TextWidth = 100
    Me.ef_transferred.Value = ""
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = True
    Me.ef_terminal_name.Location = New System.Drawing.Point(18, 85)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.Size = New System.Drawing.Size(249, 25)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 1
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.Value = ""
    '
    'ef_datetime
    '
    Me.ef_datetime.DoubleValue = 0
    Me.ef_datetime.IntegerValue = 0
    Me.ef_datetime.IsReadOnly = True
    Me.ef_datetime.Location = New System.Drawing.Point(18, 20)
    Me.ef_datetime.Name = "ef_datetime"
    Me.ef_datetime.Size = New System.Drawing.Size(249, 25)
    Me.ef_datetime.SufixText = "Sufix Text"
    Me.ef_datetime.SufixTextVisible = True
    Me.ef_datetime.TabIndex = 0
    Me.ef_datetime.TabStop = False
    Me.ef_datetime.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_datetime.TextValue = ""
    Me.ef_datetime.Value = ""
    '
    'cmb_new_status
    '
    Me.cmb_new_status.AllowUnlistedValues = False
    Me.cmb_new_status.AutoCompleteMode = False
    Me.cmb_new_status.IsReadOnly = False
    Me.cmb_new_status.Location = New System.Drawing.Point(50, 138)
    Me.cmb_new_status.Name = "cmb_new_status"
    Me.cmb_new_status.SelectedIndex = -1
    Me.cmb_new_status.Size = New System.Drawing.Size(288, 24)
    Me.cmb_new_status.SufixText = "Sufix Text"
    Me.cmb_new_status.SufixTextVisible = True
    Me.cmb_new_status.TabIndex = 1
    Me.cmb_new_status.TextWidth = 190
    '
    'frm_bonuses_confirm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(660, 199)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_bonuses_confirm"
    Me.Text = "frm_bonuses_confirm"
    Me.panel_data.ResumeLayout(False)
    Me.gb_bonus_info.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_new_status As GUI_Controls.uc_combo
  Friend WithEvents gb_bonus_info As System.Windows.Forms.GroupBox
  Friend WithEvents ef_to_transfer As GUI_Controls.uc_entry_field
  Friend WithEvents ef_current_status As GUI_Controls.uc_entry_field
  Friend WithEvents ef_transferred As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_datetime As GUI_Controls.uc_entry_field
  Friend WithEvents ef_provider_name As GUI_Controls.uc_entry_field
End Class
