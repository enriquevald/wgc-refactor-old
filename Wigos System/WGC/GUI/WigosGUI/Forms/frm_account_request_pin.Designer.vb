<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_account_request_pin
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_header = New System.Windows.Forms.Label
    Me.grb_cashier = New System.Windows.Forms.GroupBox
    Me.chk_cashier_gift_redeem = New System.Windows.Forms.CheckBox
    Me.chk_cashier_withdrawal = New System.Windows.Forms.CheckBox
    Me.grb_ebox = New System.Windows.Forms.GroupBox
    Me.chk_ebox_play = New System.Windows.Forms.CheckBox
    Me.chk_disable_for_anonymous = New System.Windows.Forms.CheckBox
    Me.panel_data.SuspendLayout()
    Me.grb_cashier.SuspendLayout()
    Me.grb_ebox.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_disable_for_anonymous)
    Me.panel_data.Controls.Add(Me.grb_ebox)
    Me.panel_data.Controls.Add(Me.grb_cashier)
    Me.panel_data.Controls.Add(Me.lbl_header)
    Me.panel_data.Size = New System.Drawing.Size(324, 193)
    '
    'lbl_header
    '
    Me.lbl_header.AutoSize = True
    Me.lbl_header.Location = New System.Drawing.Point(13, 9)
    Me.lbl_header.Name = "lbl_header"
    Me.lbl_header.Size = New System.Drawing.Size(132, 13)
    Me.lbl_header.TabIndex = 0
    Me.lbl_header.Text = "xRequest account PIN"
    '
    'grb_cashier
    '
    Me.grb_cashier.Controls.Add(Me.chk_cashier_gift_redeem)
    Me.grb_cashier.Controls.Add(Me.chk_cashier_withdrawal)
    Me.grb_cashier.Location = New System.Drawing.Point(16, 35)
    Me.grb_cashier.Name = "grb_cashier"
    Me.grb_cashier.Size = New System.Drawing.Size(291, 50)
    Me.grb_cashier.TabIndex = 1
    Me.grb_cashier.TabStop = False
    Me.grb_cashier.Text = "xCajero"
    '
    'chk_cashier_gift_redeem
    '
    Me.chk_cashier_gift_redeem.AutoSize = True
    Me.chk_cashier_gift_redeem.Location = New System.Drawing.Point(15, 20)
    Me.chk_cashier_gift_redeem.Name = "chk_cashier_gift_redeem"
    Me.chk_cashier_gift_redeem.Size = New System.Drawing.Size(104, 17)
    Me.chk_cashier_gift_redeem.TabIndex = 0
    Me.chk_cashier_gift_redeem.Text = "xGift Redeem"
    Me.chk_cashier_gift_redeem.UseVisualStyleBackColor = True
    '
    'chk_cashier_withdrawal
    '
    Me.chk_cashier_withdrawal.AutoSize = True
    Me.chk_cashier_withdrawal.Location = New System.Drawing.Point(170, 20)
    Me.chk_cashier_withdrawal.Name = "chk_cashier_withdrawal"
    Me.chk_cashier_withdrawal.Size = New System.Drawing.Size(96, 17)
    Me.chk_cashier_withdrawal.TabIndex = 1
    Me.chk_cashier_withdrawal.Text = "xWithdrawal"
    Me.chk_cashier_withdrawal.UseVisualStyleBackColor = True
    '
    'grb_ebox
    '
    Me.grb_ebox.Controls.Add(Me.chk_ebox_play)
    Me.grb_ebox.Location = New System.Drawing.Point(16, 95)
    Me.grb_ebox.Name = "grb_ebox"
    Me.grb_ebox.Size = New System.Drawing.Size(291, 50)
    Me.grb_ebox.TabIndex = 3
    Me.grb_ebox.TabStop = False
    Me.grb_ebox.Text = "xEBox"
    '
    'chk_ebox_play
    '
    Me.chk_ebox_play.AutoSize = True
    Me.chk_ebox_play.Location = New System.Drawing.Point(15, 20)
    Me.chk_ebox_play.Name = "chk_ebox_play"
    Me.chk_ebox_play.Size = New System.Drawing.Size(57, 17)
    Me.chk_ebox_play.TabIndex = 0
    Me.chk_ebox_play.Text = "xPlay"
    Me.chk_ebox_play.UseVisualStyleBackColor = True
    '
    'chk_disable_for_anonymous
    '
    Me.chk_disable_for_anonymous.AutoSize = True
    Me.chk_disable_for_anonymous.Location = New System.Drawing.Point(31, 158)
    Me.chk_disable_for_anonymous.Name = "chk_disable_for_anonymous"
    Me.chk_disable_for_anonymous.Size = New System.Drawing.Size(219, 17)
    Me.chk_disable_for_anonymous.TabIndex = 4
    Me.chk_disable_for_anonymous.Text = "xDisable for anonymous accounts"
    Me.chk_disable_for_anonymous.UseVisualStyleBackColor = True
    '
    'frm_account_request_pin
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(427, 204)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_account_request_pin"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_account_request_pin"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.grb_cashier.ResumeLayout(False)
    Me.grb_cashier.PerformLayout()
    Me.grb_ebox.ResumeLayout(False)
    Me.grb_ebox.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_header As System.Windows.Forms.Label
  Friend WithEvents grb_ebox As System.Windows.Forms.GroupBox
  Friend WithEvents chk_ebox_play As System.Windows.Forms.CheckBox
  Friend WithEvents grb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents chk_cashier_gift_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashier_withdrawal As System.Windows.Forms.CheckBox
  Friend WithEvents chk_disable_for_anonymous As System.Windows.Forms.CheckBox
End Class
