<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_lottery_meters_adjustment_working_day
  Inherits GUI_Controls.frm_base_sel_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_lottery_meters_adjustment_working_day))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.rd_last_update = New System.Windows.Forms.RadioButton()
    Me.rd_working_day = New System.Windows.Forms.RadioButton()
    Me.gb_shows = New System.Windows.Forms.GroupBox()
    Me.cb_closed = New System.Windows.Forms.CheckBox()
    Me.cb_opened = New System.Windows.Forms.CheckBox()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.uc_sites_sel1 = New GUI_Controls.uc_sites_sel()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_shows.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.gb_shows)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_sites_sel1)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1100, 173)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_sites_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_shows, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 177)
    Me.panel_data.Size = New System.Drawing.Size(1100, 348)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1094, 10)
    Me.pn_separator_line.TabIndex = 1
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1094, 10)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.uc_dsl)
    Me.gb_date.Controls.Add(Me.rd_last_update)
    Me.gb_date.Controls.Add(Me.rd_working_day)
    Me.gb_date.Location = New System.Drawing.Point(305, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(392, 142)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.FromDateText = "#5339"
    Me.uc_dsl.Location = New System.Drawing.Point(137, 17)
    Me.uc_dsl.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = False
    Me.uc_dsl.Size = New System.Drawing.Size(252, 115)
    Me.uc_dsl.TabIndex = 2
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDateText = "#5340"
    '
    'rd_last_update
    '
    Me.rd_last_update.AutoSize = True
    Me.rd_last_update.Location = New System.Drawing.Point(6, 72)
    Me.rd_last_update.Name = "rd_last_update"
    Me.rd_last_update.Size = New System.Drawing.Size(122, 17)
    Me.rd_last_update.TabIndex = 1
    Me.rd_last_update.TabStop = True
    Me.rd_last_update.Text = "xLastModification"
    Me.rd_last_update.UseVisualStyleBackColor = True
    '
    'rd_working_day
    '
    Me.rd_working_day.AutoSize = True
    Me.rd_working_day.Location = New System.Drawing.Point(6, 39)
    Me.rd_working_day.Name = "rd_working_day"
    Me.rd_working_day.Size = New System.Drawing.Size(101, 17)
    Me.rd_working_day.TabIndex = 0
    Me.rd_working_day.TabStop = True
    Me.rd_working_day.Text = "xWorkingDay"
    Me.rd_working_day.UseVisualStyleBackColor = True
    '
    'gb_shows
    '
    Me.gb_shows.Controls.Add(Me.cb_closed)
    Me.gb_shows.Controls.Add(Me.cb_opened)
    Me.gb_shows.Location = New System.Drawing.Point(703, 101)
    Me.gb_shows.Name = "gb_shows"
    Me.gb_shows.Size = New System.Drawing.Size(167, 47)
    Me.gb_shows.TabIndex = 3
    Me.gb_shows.TabStop = False
    Me.gb_shows.Text = "xShows"
    '
    'cb_closed
    '
    Me.cb_closed.AutoSize = True
    Me.cb_closed.Location = New System.Drawing.Point(89, 20)
    Me.cb_closed.Name = "cb_closed"
    Me.cb_closed.Size = New System.Drawing.Size(72, 17)
    Me.cb_closed.TabIndex = 1
    Me.cb_closed.Text = "xClosed"
    Me.cb_closed.UseVisualStyleBackColor = True
    '
    'cb_opened
    '
    Me.cb_opened.AutoSize = True
    Me.cb_opened.Location = New System.Drawing.Point(6, 20)
    Me.cb_opened.Name = "cb_opened"
    Me.cb_opened.Size = New System.Drawing.Size(77, 17)
    Me.cb_opened.TabIndex = 0
    Me.cb_opened.Text = "xOpened"
    Me.cb_opened.UseVisualStyleBackColor = True
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(703, 6)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(275, 86)
    Me.gb_user.TabIndex = 2
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(6, 20)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(69, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(6, 50)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(69, 24)
    Me.opt_all_users.TabIndex = 1
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 20)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(189, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 2
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'uc_sites_sel1
    '
    Me.uc_sites_sel1.Location = New System.Drawing.Point(6, 6)
    Me.uc_sites_sel1.MultiSelect = True
    Me.uc_sites_sel1.Name = "uc_sites_sel1"
    Me.uc_sites_sel1.ShowIsoCode = False
    Me.uc_sites_sel1.ShowMultisiteRow = True
    Me.uc_sites_sel1.Size = New System.Drawing.Size(293, 167)
    Me.uc_sites_sel1.TabIndex = 3
    '
    'frm_lottery_meters_adjustment_working_day
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1110, 529)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_lottery_meters_adjustment_working_day"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_lottery_meters_adjustment_working_day"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.gb_shows.ResumeLayout(False)
    Me.gb_shows.PerformLayout()
    Me.gb_user.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents gb_shows As System.Windows.Forms.GroupBox
  Friend WithEvents cb_closed As System.Windows.Forms.CheckBox
  Friend WithEvents cb_opened As System.Windows.Forms.CheckBox
  Friend WithEvents rd_last_update As System.Windows.Forms.RadioButton
  Friend WithEvents rd_working_day As System.Windows.Forms.RadioButton
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_sites_sel1 As GUI_Controls.uc_sites_sel
End Class
