<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_area_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_area_name = New GUI_Controls.uc_entry_field
    Me.chk_smoking = New System.Windows.Forms.CheckBox
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_smoking)
    Me.panel_data.Controls.Add(Me.ef_area_name)
    Me.panel_data.Size = New System.Drawing.Size(362, 152)
    '
    'ef_area_name
    '
    Me.ef_area_name.DoubleValue = 0
    Me.ef_area_name.IntegerValue = 0
    Me.ef_area_name.IsReadOnly = False
    Me.ef_area_name.Location = New System.Drawing.Point(3, 40)
    Me.ef_area_name.Name = "ef_area_name"
    Me.ef_area_name.Size = New System.Drawing.Size(316, 25)
    Me.ef_area_name.SufixText = "Sufix Text"
    Me.ef_area_name.SufixTextVisible = True
    Me.ef_area_name.TabIndex = 0
    Me.ef_area_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_area_name.TextValue = ""
    Me.ef_area_name.TextWidth = 90
    Me.ef_area_name.Value = ""
    '
    'chk_smoking
    '
    Me.chk_smoking.Location = New System.Drawing.Point(210, 93)
    Me.chk_smoking.Name = "chk_smoking"
    Me.chk_smoking.Size = New System.Drawing.Size(109, 16)
    Me.chk_smoking.TabIndex = 1
    Me.chk_smoking.Text = "xSmoking"
    '
    'frm_area_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(464, 161)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_area_edit"
    Me.Text = "frm_island_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_area_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_smoking As System.Windows.Forms.CheckBox
End Class
