﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cash_monitor
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.uc_cash_users = New GUI_Controls.uc_grid()
    Me.gb_cash_users = New System.Windows.Forms.GroupBox()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.opt_several_users = New System.Windows.Forms.RadioButton()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.lbl_mb_limit_color = New System.Windows.Forms.Label()
    Me.gb_mb_limit = New System.Windows.Forms.GroupBox()
    Me.chk_mb_recharge_limit = New System.Windows.Forms.CheckBox()
    Me.ef_mb_recharge_limit = New GUI_Controls.uc_entry_field()
    Me.gb_cashier_limit = New System.Windows.Forms.GroupBox()
    Me.lbl_cashier_limit_color = New System.Windows.Forms.Label()
    Me.chk_cashier_recharge_limit = New System.Windows.Forms.CheckBox()
    Me.ef_cashier_recharge_limit = New GUI_Controls.uc_entry_field()
    Me.gb_options = New System.Windows.Forms.GroupBox()
    Me.chk_exclude_no_cash = New System.Windows.Forms.CheckBox()
    Me.chk_exclude_no_limit_mb = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_cash_users.SuspendLayout()
    Me.gb_mb_limit.SuspendLayout()
    Me.gb_cashier_limit.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.gb_cashier_limit)
    Me.panel_filter.Controls.Add(Me.gb_mb_limit)
    Me.panel_filter.Controls.Add(Me.tf_last_update)
    Me.panel_filter.Controls.Add(Me.gb_cash_users)
    Me.panel_filter.Size = New System.Drawing.Size(1278, 174)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cash_users, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_last_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mb_limit, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier_limit, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 178)
    Me.panel_data.Size = New System.Drawing.Size(1278, 449)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1272, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1272, 4)
    '
    'uc_cash_users
    '
    Me.uc_cash_users.CurrentCol = -1
    Me.uc_cash_users.CurrentRow = -1
    Me.uc_cash_users.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_cash_users.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_cash_users.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_cash_users.Location = New System.Drawing.Point(89, 20)
    Me.uc_cash_users.Name = "uc_cash_users"
    Me.uc_cash_users.PanelRightVisible = False
    Me.uc_cash_users.Redraw = True
    Me.uc_cash_users.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_cash_users.Size = New System.Drawing.Size(242, 132)
    Me.uc_cash_users.Sortable = False
    Me.uc_cash_users.SortAscending = True
    Me.uc_cash_users.SortByCol = 0
    Me.uc_cash_users.TabIndex = 2
    Me.uc_cash_users.ToolTipped = True
    Me.uc_cash_users.TopRow = -2
    '
    'gb_cash_users
    '
    Me.gb_cash_users.Controls.Add(Me.opt_all_users)
    Me.gb_cash_users.Controls.Add(Me.opt_several_users)
    Me.gb_cash_users.Controls.Add(Me.uc_cash_users)
    Me.gb_cash_users.Location = New System.Drawing.Point(7, 7)
    Me.gb_cash_users.Name = "gb_cash_users"
    Me.gb_cash_users.Size = New System.Drawing.Size(337, 159)
    Me.gb_cash_users.TabIndex = 0
    Me.gb_cash_users.TabStop = False
    Me.gb_cash_users.Text = "xUsers"
    '
    'opt_all_users
    '
    Me.opt_all_users.AutoSize = True
    Me.opt_all_users.Location = New System.Drawing.Point(7, 44)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(46, 17)
    Me.opt_all_users.TabIndex = 1
    Me.opt_all_users.TabStop = True
    Me.opt_all_users.Text = "xAll"
    Me.opt_all_users.UseVisualStyleBackColor = True
    '
    'opt_several_users
    '
    Me.opt_several_users.AutoSize = True
    Me.opt_several_users.Location = New System.Drawing.Point(7, 21)
    Me.opt_several_users.Name = "opt_several_users"
    Me.opt_several_users.Size = New System.Drawing.Size(76, 17)
    Me.opt_several_users.TabIndex = 0
    Me.opt_several_users.TabStop = True
    Me.opt_several_users.Text = "xSeveral"
    Me.opt_several_users.UseVisualStyleBackColor = True
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(350, 142)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(250, 24)
    Me.tf_last_update.SufixText = "Sufix Text"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 2
    Me.tf_last_update.TextWidth = 130
    '
    'tmr_monitor
    '
    Me.tmr_monitor.Interval = 5000
    '
    'lbl_mb_limit_color
    '
    Me.lbl_mb_limit_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_mb_limit_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_mb_limit_color.Location = New System.Drawing.Point(28, 26)
    Me.lbl_mb_limit_color.Name = "lbl_mb_limit_color"
    Me.lbl_mb_limit_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_mb_limit_color.TabIndex = 4
    '
    'gb_mb_limit
    '
    Me.gb_mb_limit.Controls.Add(Me.lbl_mb_limit_color)
    Me.gb_mb_limit.Controls.Add(Me.chk_mb_recharge_limit)
    Me.gb_mb_limit.Controls.Add(Me.ef_mb_recharge_limit)
    Me.gb_mb_limit.Location = New System.Drawing.Point(350, 76)
    Me.gb_mb_limit.Name = "gb_mb_limit"
    Me.gb_mb_limit.Size = New System.Drawing.Size(316, 62)
    Me.gb_mb_limit.TabIndex = 1
    Me.gb_mb_limit.TabStop = False
    Me.gb_mb_limit.Text = "GroupBox1"
    '
    'chk_mb_recharge_limit
    '
    Me.chk_mb_recharge_limit.AutoSize = True
    Me.chk_mb_recharge_limit.Location = New System.Drawing.Point(7, 26)
    Me.chk_mb_recharge_limit.Name = "chk_mb_recharge_limit"
    Me.chk_mb_recharge_limit.Size = New System.Drawing.Size(15, 14)
    Me.chk_mb_recharge_limit.TabIndex = 2
    Me.chk_mb_recharge_limit.UseVisualStyleBackColor = True
    '
    'ef_mb_recharge_limit
    '
    Me.ef_mb_recharge_limit.DoubleValue = 0.0R
    Me.ef_mb_recharge_limit.IntegerValue = 0
    Me.ef_mb_recharge_limit.IsReadOnly = False
    Me.ef_mb_recharge_limit.Location = New System.Drawing.Point(50, 20)
    Me.ef_mb_recharge_limit.Name = "ef_mb_recharge_limit"
    Me.ef_mb_recharge_limit.PlaceHolder = Nothing
    Me.ef_mb_recharge_limit.Size = New System.Drawing.Size(258, 24)
    Me.ef_mb_recharge_limit.SufixText = "Sufix Text"
    Me.ef_mb_recharge_limit.SufixTextVisible = True
    Me.ef_mb_recharge_limit.TabIndex = 3
    Me.ef_mb_recharge_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_mb_recharge_limit.TextValue = ""
    Me.ef_mb_recharge_limit.TextWidth = 150
    Me.ef_mb_recharge_limit.Value = ""
    Me.ef_mb_recharge_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_cashier_limit
    '
    Me.gb_cashier_limit.Controls.Add(Me.lbl_cashier_limit_color)
    Me.gb_cashier_limit.Controls.Add(Me.chk_cashier_recharge_limit)
    Me.gb_cashier_limit.Controls.Add(Me.ef_cashier_recharge_limit)
    Me.gb_cashier_limit.Location = New System.Drawing.Point(350, 8)
    Me.gb_cashier_limit.Name = "gb_cashier_limit"
    Me.gb_cashier_limit.Size = New System.Drawing.Size(316, 62)
    Me.gb_cashier_limit.TabIndex = 11
    Me.gb_cashier_limit.TabStop = False
    Me.gb_cashier_limit.Text = "GroupBox1"
    '
    'lbl_cashier_limit_color
    '
    Me.lbl_cashier_limit_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_cashier_limit_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_cashier_limit_color.Location = New System.Drawing.Point(28, 26)
    Me.lbl_cashier_limit_color.Name = "lbl_cashier_limit_color"
    Me.lbl_cashier_limit_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_cashier_limit_color.TabIndex = 4
    '
    'chk_cashier_recharge_limit
    '
    Me.chk_cashier_recharge_limit.AutoSize = True
    Me.chk_cashier_recharge_limit.Location = New System.Drawing.Point(7, 26)
    Me.chk_cashier_recharge_limit.Name = "chk_cashier_recharge_limit"
    Me.chk_cashier_recharge_limit.Size = New System.Drawing.Size(15, 14)
    Me.chk_cashier_recharge_limit.TabIndex = 2
    Me.chk_cashier_recharge_limit.UseVisualStyleBackColor = True
    '
    'ef_cashier_recharge_limit
    '
    Me.ef_cashier_recharge_limit.DoubleValue = 0.0R
    Me.ef_cashier_recharge_limit.IntegerValue = 0
    Me.ef_cashier_recharge_limit.IsReadOnly = False
    Me.ef_cashier_recharge_limit.Location = New System.Drawing.Point(50, 20)
    Me.ef_cashier_recharge_limit.Name = "ef_cashier_recharge_limit"
    Me.ef_cashier_recharge_limit.PlaceHolder = Nothing
    Me.ef_cashier_recharge_limit.Size = New System.Drawing.Size(258, 24)
    Me.ef_cashier_recharge_limit.SufixText = "Sufix Text"
    Me.ef_cashier_recharge_limit.SufixTextVisible = True
    Me.ef_cashier_recharge_limit.TabIndex = 3
    Me.ef_cashier_recharge_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashier_recharge_limit.TextValue = ""
    Me.ef_cashier_recharge_limit.TextWidth = 150
    Me.ef_cashier_recharge_limit.Value = ""
    Me.ef_cashier_recharge_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.chk_exclude_no_limit_mb)
    Me.gb_options.Controls.Add(Me.chk_exclude_no_cash)
    Me.gb_options.Location = New System.Drawing.Point(672, 7)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(316, 62)
    Me.gb_options.TabIndex = 12
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "GroupBox1"
    '
    'chk_exclude_no_cash
    '
    Me.chk_exclude_no_cash.AutoSize = True
    Me.chk_exclude_no_cash.Location = New System.Drawing.Point(6, 15)
    Me.chk_exclude_no_cash.Name = "chk_exclude_no_cash"
    Me.chk_exclude_no_cash.Size = New System.Drawing.Size(131, 17)
    Me.chk_exclude_no_cash.TabIndex = 0
    Me.chk_exclude_no_cash.Text = "xRemoveCashless"
    Me.chk_exclude_no_cash.UseVisualStyleBackColor = True
    '
    'chk_exclude_no_limit_mb
    '
    Me.chk_exclude_no_limit_mb.AutoSize = True
    Me.chk_exclude_no_limit_mb.Location = New System.Drawing.Point(6, 36)
    Me.chk_exclude_no_limit_mb.Name = "chk_exclude_no_limit_mb"
    Me.chk_exclude_no_limit_mb.Size = New System.Drawing.Size(138, 17)
    Me.chk_exclude_no_limit_mb.TabIndex = 1
    Me.chk_exclude_no_limit_mb.Text = "xRemoveNoLimitMb"
    Me.chk_exclude_no_limit_mb.UseVisualStyleBackColor = True
    '
    'frm_cash_monitor
    '
    Me.ClientSize = New System.Drawing.Size(1286, 631)
    Me.Name = "frm_cash_monitor"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_cash_users.ResumeLayout(False)
    Me.gb_cash_users.PerformLayout()
    Me.gb_mb_limit.ResumeLayout(False)
    Me.gb_mb_limit.PerformLayout()
    Me.gb_cashier_limit.ResumeLayout(False)
    Me.gb_cashier_limit.PerformLayout()
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_cash_users As System.Windows.Forms.GroupBox
  Friend WithEvents uc_cash_users As GUI_Controls.uc_grid
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_users As System.Windows.Forms.RadioButton
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents lbl_mb_limit_color As System.Windows.Forms.Label
  Friend WithEvents gb_mb_limit As System.Windows.Forms.GroupBox
  Friend WithEvents chk_mb_recharge_limit As System.Windows.Forms.CheckBox
  Friend WithEvents ef_mb_recharge_limit As GUI_Controls.uc_entry_field
  Friend WithEvents gb_cashier_limit As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_cashier_limit_color As System.Windows.Forms.Label
  Friend WithEvents chk_cashier_recharge_limit As System.Windows.Forms.CheckBox
  Friend WithEvents ef_cashier_recharge_limit As GUI_Controls.uc_entry_field
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents chk_exclude_no_cash As System.Windows.Forms.CheckBox
  Friend WithEvents chk_exclude_no_limit_mb As System.Windows.Forms.CheckBox

End Class
