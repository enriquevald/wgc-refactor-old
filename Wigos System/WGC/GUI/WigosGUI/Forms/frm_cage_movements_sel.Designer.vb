<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_movements_sel
  Inherits GUI_Controls.frm_base_sel


  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cage_movements_sel))
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.opt_all_cashiers = New System.Windows.Forms.RadioButton()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.rb_custom = New System.Windows.Forms.RadioButton()
    Me.rb_terminal = New System.Windows.Forms.RadioButton()
    Me.rb_gaming_table = New System.Windows.Forms.RadioButton()
    Me.cmb_cashier = New GUI_Controls.uc_combo()
    Me.rb_cashier = New System.Windows.Forms.RadioButton()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.opt_working_day = New System.Windows.Forms.RadioButton()
    Me.opt_movement_date = New System.Windows.Forms.RadioButton()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_checked_list_status = New GUI_Controls.uc_checked_list()
    Me.gb_sessions = New System.Windows.Forms.GroupBox()
    Me.opt_one_session = New System.Windows.Forms.RadioButton()
    Me.opt_all_sessions = New System.Windows.Forms.RadioButton()
    Me.cmb_sessions = New GUI_Controls.uc_combo()
    Me.gb_order_by = New System.Windows.Forms.GroupBox()
    Me.opt_by_cashier_session = New System.Windows.Forms.RadioButton()
    Me.opt_by_date = New System.Windows.Forms.RadioButton()
    Me.tf_cashier_session = New GUI_Controls.uc_text_field()
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.gb_mode = New System.Windows.Forms.GroupBox()
    Me.opt_history_mode = New System.Windows.Forms.RadioButton()
    Me.opt_monitor_mode = New System.Windows.Forms.RadioButton()
    Me.opt_from_cage = New System.Windows.Forms.RadioButton()
    Me.opt_all = New System.Windows.Forms.RadioButton()
    Me.gb_operation = New System.Windows.Forms.GroupBox()
    Me.opt_from_cashier = New System.Windows.Forms.RadioButton()
    Me.tf_latest_update = New GUI_Controls.uc_text_field()
    Me.lbl_monitor_msg = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_sessions.SuspendLayout()
    Me.gb_order_by.SuspendLayout()
    Me.gb_mode.SuspendLayout()
    Me.gb_operation.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_monitor_msg)
    Me.panel_filter.Controls.Add(Me.gb_mode)
    Me.panel_filter.Controls.Add(Me.gb_order_by)
    Me.panel_filter.Controls.Add(Me.tf_cashier_session)
    Me.panel_filter.Controls.Add(Me.gb_sessions)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_status)
    Me.panel_filter.Controls.Add(Me.gb_operation)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.gb_cashier)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.tf_latest_update)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1164, 253)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_latest_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_operation, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_sessions, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_cashier_session, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_order_by, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mode, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_monitor_msg, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 257)
    Me.panel_data.Size = New System.Drawing.Size(1164, 379)
    Me.panel_data.TabIndex = 0
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1158, 23)
    Me.pn_separator_line.TabIndex = 2
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1158, 4)
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(621, 75)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(381, 69)
    Me.gb_user.TabIndex = 6
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(83, 45)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 41)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(80, 14)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(293, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'opt_all_cashiers
    '
    Me.opt_all_cashiers.Checked = True
    Me.opt_all_cashiers.Location = New System.Drawing.Point(8, 39)
    Me.opt_all_cashiers.Name = "opt_all_cashiers"
    Me.opt_all_cashiers.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_cashiers.TabIndex = 3
    Me.opt_all_cashiers.TabStop = True
    Me.opt_all_cashiers.Text = "xAll"
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.rb_custom)
    Me.gb_cashier.Controls.Add(Me.rb_terminal)
    Me.gb_cashier.Controls.Add(Me.rb_gaming_table)
    Me.gb_cashier.Controls.Add(Me.opt_all_cashiers)
    Me.gb_cashier.Controls.Add(Me.cmb_cashier)
    Me.gb_cashier.Controls.Add(Me.rb_cashier)
    Me.gb_cashier.Location = New System.Drawing.Point(621, 6)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(381, 69)
    Me.gb_cashier.TabIndex = 5
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'rb_custom
    '
    Me.rb_custom.AutoSize = True
    Me.rb_custom.Location = New System.Drawing.Point(279, 18)
    Me.rb_custom.Name = "rb_custom"
    Me.rb_custom.Size = New System.Drawing.Size(76, 17)
    Me.rb_custom.TabIndex = 2
    Me.rb_custom.Text = "xCustom"
    Me.rb_custom.UseVisualStyleBackColor = True
    '
    'rb_terminal
    '
    Me.rb_terminal.AutoSize = True
    Me.rb_terminal.Location = New System.Drawing.Point(201, 18)
    Me.rb_terminal.Name = "rb_terminal"
    Me.rb_terminal.Size = New System.Drawing.Size(81, 17)
    Me.rb_terminal.TabIndex = 5
    Me.rb_terminal.Text = "xTerminal"
    Me.rb_terminal.UseVisualStyleBackColor = True
    '
    'rb_gaming_table
    '
    Me.rb_gaming_table.AutoSize = True
    Me.rb_gaming_table.Location = New System.Drawing.Point(91, 18)
    Me.rb_gaming_table.Name = "rb_gaming_table"
    Me.rb_gaming_table.Size = New System.Drawing.Size(106, 17)
    Me.rb_gaming_table.TabIndex = 1
    Me.rb_gaming_table.Text = "xGamingTable"
    Me.rb_gaming_table.UseVisualStyleBackColor = True
    '
    'cmb_cashier
    '
    Me.cmb_cashier.AllowUnlistedValues = False
    Me.cmb_cashier.AutoCompleteMode = True
    Me.cmb_cashier.IsReadOnly = False
    Me.cmb_cashier.Location = New System.Drawing.Point(80, 40)
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.Size = New System.Drawing.Size(293, 24)
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TabIndex = 4
    Me.cmb_cashier.TextCombo = Nothing
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'rb_cashier
    '
    Me.rb_cashier.AutoSize = True
    Me.rb_cashier.Location = New System.Drawing.Point(8, 18)
    Me.rb_cashier.Name = "rb_cashier"
    Me.rb_cashier.Size = New System.Drawing.Size(76, 17)
    Me.rb_cashier.TabIndex = 0
    Me.rb_cashier.Text = "xCashier"
    Me.rb_cashier.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.opt_working_day)
    Me.gb_date.Controls.Add(Me.opt_movement_date)
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(251, 138)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'opt_working_day
    '
    Me.opt_working_day.Location = New System.Drawing.Point(8, 39)
    Me.opt_working_day.Name = "opt_working_day"
    Me.opt_working_day.Size = New System.Drawing.Size(210, 24)
    Me.opt_working_day.TabIndex = 1
    Me.opt_working_day.Text = "xSearchByWorkingDay"
    '
    'opt_movement_date
    '
    Me.opt_movement_date.Location = New System.Drawing.Point(8, 19)
    Me.opt_movement_date.Name = "opt_movement_date"
    Me.opt_movement_date.Size = New System.Drawing.Size(210, 24)
    Me.opt_movement_date.TabIndex = 0
    Me.opt_movement_date.Text = "xSearchByMovementDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 97)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 3
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 69)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 2
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_checked_list_status
    '
    Me.uc_checked_list_status.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_checked_list_status.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_status.Location = New System.Drawing.Point(263, 6)
    Me.uc_checked_list_status.m_resize_width = 352
    Me.uc_checked_list_status.multiChoice = True
    Me.uc_checked_list_status.Name = "uc_checked_list_status"
    Me.uc_checked_list_status.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_status.SelectedIndexesList = ""
    Me.uc_checked_list_status.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_status.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_status.SelectedValuesList = ""
    Me.uc_checked_list_status.SetLevels = 2
    Me.uc_checked_list_status.Size = New System.Drawing.Size(352, 190)
    Me.uc_checked_list_status.TabIndex = 2
    Me.uc_checked_list_status.ValuesArray = New String(-1) {}
    '
    'gb_sessions
    '
    Me.gb_sessions.Controls.Add(Me.opt_one_session)
    Me.gb_sessions.Controls.Add(Me.opt_all_sessions)
    Me.gb_sessions.Controls.Add(Me.cmb_sessions)
    Me.gb_sessions.Location = New System.Drawing.Point(621, 145)
    Me.gb_sessions.Name = "gb_sessions"
    Me.gb_sessions.Size = New System.Drawing.Size(381, 69)
    Me.gb_sessions.TabIndex = 7
    Me.gb_sessions.TabStop = False
    Me.gb_sessions.Text = "xSession"
    '
    'opt_one_session
    '
    Me.opt_one_session.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_session.Name = "opt_one_session"
    Me.opt_one_session.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_session.TabIndex = 0
    Me.opt_one_session.Text = "xOne"
    '
    'opt_all_sessions
    '
    Me.opt_all_sessions.Location = New System.Drawing.Point(8, 41)
    Me.opt_all_sessions.Name = "opt_all_sessions"
    Me.opt_all_sessions.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_sessions.TabIndex = 2
    Me.opt_all_sessions.Text = "xAll"
    '
    'cmb_sessions
    '
    Me.cmb_sessions.AllowUnlistedValues = False
    Me.cmb_sessions.AutoCompleteMode = False
    Me.cmb_sessions.IsReadOnly = False
    Me.cmb_sessions.Location = New System.Drawing.Point(80, 14)
    Me.cmb_sessions.Name = "cmb_sessions"
    Me.cmb_sessions.SelectedIndex = -1
    Me.cmb_sessions.Size = New System.Drawing.Size(293, 24)
    Me.cmb_sessions.SufixText = "Sufix Text"
    Me.cmb_sessions.SufixTextVisible = True
    Me.cmb_sessions.TabIndex = 1
    Me.cmb_sessions.TextCombo = Nothing
    Me.cmb_sessions.TextVisible = False
    Me.cmb_sessions.TextWidth = 0
    '
    'gb_order_by
    '
    Me.gb_order_by.Controls.Add(Me.opt_by_cashier_session)
    Me.gb_order_by.Controls.Add(Me.opt_by_date)
    Me.gb_order_by.Location = New System.Drawing.Point(263, 196)
    Me.gb_order_by.Name = "gb_order_by"
    Me.gb_order_by.Size = New System.Drawing.Size(238, 46)
    Me.gb_order_by.TabIndex = 3
    Me.gb_order_by.TabStop = False
    Me.gb_order_by.Text = "xOrder by"
    '
    'opt_by_cashier_session
    '
    Me.opt_by_cashier_session.AutoSize = True
    Me.opt_by_cashier_session.Location = New System.Drawing.Point(99, 20)
    Me.opt_by_cashier_session.Name = "opt_by_cashier_session"
    Me.opt_by_cashier_session.Size = New System.Drawing.Size(120, 17)
    Me.opt_by_cashier_session.TabIndex = 1
    Me.opt_by_cashier_session.TabStop = True
    Me.opt_by_cashier_session.Text = "xCashierSession"
    Me.opt_by_cashier_session.UseVisualStyleBackColor = True
    '
    'opt_by_date
    '
    Me.opt_by_date.AutoSize = True
    Me.opt_by_date.Location = New System.Drawing.Point(8, 20)
    Me.opt_by_date.Name = "opt_by_date"
    Me.opt_by_date.Size = New System.Drawing.Size(59, 17)
    Me.opt_by_date.TabIndex = 0
    Me.opt_by_date.TabStop = True
    Me.opt_by_date.Text = "xDate"
    Me.opt_by_date.UseVisualStyleBackColor = True
    '
    'tf_cashier_session
    '
    Me.tf_cashier_session.IsReadOnly = True
    Me.tf_cashier_session.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cashier_session.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cashier_session.Location = New System.Drawing.Point(250, 202)
    Me.tf_cashier_session.Name = "tf_cashier_session"
    Me.tf_cashier_session.Size = New System.Drawing.Size(352, 24)
    Me.tf_cashier_session.SufixText = "Sufix Text"
    Me.tf_cashier_session.SufixTextVisible = True
    Me.tf_cashier_session.TabIndex = 4
    Me.tf_cashier_session.TabStop = False
    Me.tf_cashier_session.TextWidth = 100
    Me.tf_cashier_session.Value = "xCash Desk Session"
    Me.tf_cashier_session.Visible = False
    '
    'tmr_monitor
    '
    '
    'gb_mode
    '
    Me.gb_mode.Controls.Add(Me.opt_history_mode)
    Me.gb_mode.Controls.Add(Me.opt_monitor_mode)
    Me.gb_mode.Location = New System.Drawing.Point(151, 145)
    Me.gb_mode.Name = "gb_mode"
    Me.gb_mode.Size = New System.Drawing.Size(106, 69)
    Me.gb_mode.TabIndex = 11
    Me.gb_mode.TabStop = False
    Me.gb_mode.Text = "xMode"
    '
    'opt_history_mode
    '
    Me.opt_history_mode.AutoSize = True
    Me.opt_history_mode.Location = New System.Drawing.Point(6, 42)
    Me.opt_history_mode.Name = "opt_history_mode"
    Me.opt_history_mode.Size = New System.Drawing.Size(72, 17)
    Me.opt_history_mode.TabIndex = 1
    Me.opt_history_mode.Text = "xHistory"
    Me.opt_history_mode.UseVisualStyleBackColor = True
    '
    'opt_monitor_mode
    '
    Me.opt_monitor_mode.AutoSize = True
    Me.opt_monitor_mode.Location = New System.Drawing.Point(6, 19)
    Me.opt_monitor_mode.Name = "opt_monitor_mode"
    Me.opt_monitor_mode.Size = New System.Drawing.Size(74, 17)
    Me.opt_monitor_mode.TabIndex = 0
    Me.opt_monitor_mode.Text = "xMonitor"
    Me.opt_monitor_mode.UseVisualStyleBackColor = True
    '
    'opt_from_cage
    '
    Me.opt_from_cage.Location = New System.Drawing.Point(8, 22)
    Me.opt_from_cage.Name = "opt_from_cage"
    Me.opt_from_cage.Size = New System.Drawing.Size(125, 17)
    Me.opt_from_cage.TabIndex = 0
    Me.opt_from_cage.TabStop = True
    Me.opt_from_cage.Text = "xCage to cashier"
    Me.opt_from_cage.UseVisualStyleBackColor = True
    '
    'opt_all
    '
    Me.opt_all.Location = New System.Drawing.Point(8, 72)
    Me.opt_all.Name = "opt_all"
    Me.opt_all.Size = New System.Drawing.Size(125, 17)
    Me.opt_all.TabIndex = 2
    Me.opt_all.TabStop = True
    Me.opt_all.Text = "xAll"
    Me.opt_all.UseVisualStyleBackColor = True
    '
    'gb_operation
    '
    Me.gb_operation.Controls.Add(Me.opt_all)
    Me.gb_operation.Controls.Add(Me.opt_from_cashier)
    Me.gb_operation.Controls.Add(Me.opt_from_cage)
    Me.gb_operation.Location = New System.Drawing.Point(6, 145)
    Me.gb_operation.Name = "gb_operation"
    Me.gb_operation.Size = New System.Drawing.Size(139, 97)
    Me.gb_operation.TabIndex = 1
    Me.gb_operation.TabStop = False
    Me.gb_operation.Text = "xOperation"
    '
    'opt_from_cashier
    '
    Me.opt_from_cashier.Location = New System.Drawing.Point(8, 47)
    Me.opt_from_cashier.Name = "opt_from_cashier"
    Me.opt_from_cashier.Size = New System.Drawing.Size(125, 17)
    Me.opt_from_cashier.TabIndex = 1
    Me.opt_from_cashier.TabStop = True
    Me.opt_from_cashier.Text = "xCashier to cage"
    Me.opt_from_cashier.UseVisualStyleBackColor = True
    '
    'tf_latest_update
    '
    Me.tf_latest_update.AutoSize = True
    Me.tf_latest_update.IsReadOnly = True
    Me.tf_latest_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_latest_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_latest_update.Location = New System.Drawing.Point(606, 212)
    Me.tf_latest_update.Name = "tf_latest_update"
    Me.tf_latest_update.Size = New System.Drawing.Size(323, 24)
    Me.tf_latest_update.SufixText = "Sufix Text"
    Me.tf_latest_update.SufixTextVisible = True
    Me.tf_latest_update.TabIndex = 12
    Me.tf_latest_update.TextWidth = 125
    Me.tf_latest_update.Value = "x99999"
    Me.tf_latest_update.Visible = False
    '
    'lbl_monitor_msg
    '
    Me.lbl_monitor_msg.AutoSize = True
    Me.lbl_monitor_msg.ForeColor = System.Drawing.Color.Blue
    Me.lbl_monitor_msg.Location = New System.Drawing.Point(618, 233)
    Me.lbl_monitor_msg.Name = "lbl_monitor_msg"
    Me.lbl_monitor_msg.Size = New System.Drawing.Size(106, 13)
    Me.lbl_monitor_msg.TabIndex = 13
    Me.lbl_monitor_msg.Text = "XMonitorMessage"
    Me.lbl_monitor_msg.Visible = False
    '
    'frm_cage_movements_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1174, 640)
    Me.Name = "frm_cage_movements_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_movements_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_cashier.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_sessions.ResumeLayout(False)
    Me.gb_order_by.ResumeLayout(False)
    Me.gb_order_by.PerformLayout()
    Me.gb_mode.ResumeLayout(False)
    Me.gb_mode.PerformLayout()
    Me.gb_operation.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_checked_list_status As GUI_Controls.uc_checked_list
  Friend WithEvents opt_all_cashiers As System.Windows.Forms.RadioButton
  Friend WithEvents rb_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents rb_custom As System.Windows.Forms.RadioButton
  Friend WithEvents rb_gaming_table As System.Windows.Forms.RadioButton
  Friend WithEvents gb_sessions As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_session As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_sessions As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_sessions As GUI_Controls.uc_combo
  Friend WithEvents gb_order_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_by_cashier_session As System.Windows.Forms.RadioButton
  Friend WithEvents opt_by_date As System.Windows.Forms.RadioButton
  Friend WithEvents tf_cashier_session As GUI_Controls.uc_text_field
  Friend WithEvents opt_working_day As System.Windows.Forms.RadioButton
  Friend WithEvents opt_movement_date As System.Windows.Forms.RadioButton
  Friend WithEvents gb_mode As System.Windows.Forms.GroupBox
  Friend WithEvents opt_history_mode As System.Windows.Forms.RadioButton
  Friend WithEvents opt_monitor_mode As System.Windows.Forms.RadioButton
  Friend WithEvents gb_operation As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_from_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_from_cage As System.Windows.Forms.RadioButton
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents tf_latest_update As GUI_Controls.uc_text_field
  Friend WithEvents lbl_monitor_msg As System.Windows.Forms.Label
  Friend WithEvents rb_terminal As System.Windows.Forms.RadioButton
End Class
