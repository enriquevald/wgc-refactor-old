'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_short_over_report
' DESCRIPTION:   Cash Shortfall and Excess Report Screen
' AUTHOR:        Fernando Jim�nez
' CREATION DATE: 24-OCT-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-OCT-2014  FJC    Initial version
' 15-JUN-2015  AMF    New filter
' 19-JUL-2016  FOS    Product Backlog Item 15068:Mesas (Fase 4): Adapt reports for the multi currency chips
''--------------------------------------------------------------------
' 15-JUL-2016  JBP    Bug 15409:CountR: No se insertan los registros en "Faltantes y sobrantes"
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Data.OleDb
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_short_over_report
  Inherits frm_base_sel

#Region " Enums "

  Private Enum ENUM_TYPE_USER
    USER = 0
    COUNTR = 6
    USER_MOBILE_BANK = 1
  End Enum ' ENUM_TYPE_USER

#End Region ' Enums

#Region " Constants "

  ' Grid configuration
  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 10

  ' Hidden
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_USER_ID As Integer = 1
  Private Const GRID_COLUMN_TYPE_USER As Integer = 2

  ' Showed
  Private Const GRID_COLUMN_USER_LOGIN As Integer = 3
  Private Const GRID_COLUMN_USER_FULLNAME As Integer = 4
  Private Const GRID_COLUMN_USER_CODE_EMPLOYEE As Integer = 5
  Private Const GRID_COLUMN_CURRENY_ISO_CODE As Integer = 6
  Private Const GRID_COLUMN_AMOUNT_OVER As Integer = 7
  Private Const GRID_COLUMN_AMOUNT_SHORT As Integer = 8
  Private Const GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER As Integer = 9

  'SQL Query Columns
  Private Const SQL_COL_USER_ID As Integer = 0
  Private Const SQL_COL_TYPE_USER As Integer = 1
  Private Const SQL_COL_USER_LOGIN As Integer = 2
  Private Const SQL_COL_USER_FULLNAME As Integer = 3
  Private Const SQL_COL_USER_CODE_EMPLOYEE As Integer = 4
  Private Const SQL_COL_CURRENY_ISO_CODE As Integer = 5
  Private Const SQL_COL_AMOUNT_SHORT As Integer = 6
  Private Const SQL_COL_AMOUNT_OVER As Integer = 7
  Private Const SQL_COL_AMOUNT_DIFFERENCE_SHORT_OVER As Integer = 8


  ' Grid Columns Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_USER_LOGIN As Integer = 2000
  Private Const GRID_WIDTH_USER_FULLNAME As Integer = 2000
  Private Const GRID_WIDTH_USER_CODE_EMPLOYEE As Integer = 2000
  Private Const GRID_WIDTH_CURRENY_ISO_CODE As Integer = 900
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 2475
  Private Const GRID_WIDTH_AMOUNT_OVER As Integer = 2475
  Private Const GRID_WIDTH_AMOUNT_DIFFERENCE_SHORT_OVER As Integer = 2475

  Private Const COLOR_CHIPS_ISO_CODE = "X02"
#End Region ' Constants

#Region " Structures"
  Private Structure TYPE_TOTAL_COUNTERS_CURRENCY
    Dim currency As String
    Dim total_over_amount As Decimal
    Dim total_short_amount As Decimal
    Dim total_difference_amount As Decimal
  End Structure

#End Region ' Structures

#Region " Members"
  Private m_currencies As List(Of TYPE_TOTAL_COUNTERS_CURRENCY)
  Private m_currency_find As String ' For searchs

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_filter_date_type As String

#End Region ' Members

#Region " Overrides"
  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SHORT_OVER_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5664) ' 5206 "Informe de Faltantes y Sobrantes"; "Short Over Report"

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)

    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Date 
    Me.opt_movement_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2466)
    Me.opt_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)                                            ' "Fecha"
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)                                           ' "From"
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)                                             ' "Hasta"
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_to.ShowCheckBox = False

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls  

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 

    If Me.dtp_from.Value > Me.dtp_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_from.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _idx_list As Integer
    Dim _type_currency As TYPE_TOTAL_COUNTERS_CURRENCY
    Dim _national_currency_code As String
    Dim _show_currency_symbol As Boolean
    Dim _currency_code As String

    _currency_code = String.Empty
    _show_currency_symbol = False
    _national_currency_code = CurrencyExchange.GetNationalCurrency()

    If Not DbRow.IsNull(SQL_COL_CURRENY_ISO_CODE) Then
      If DbRow.Value(SQL_COL_TYPE_USER) = ENUM_TYPE_USER.USER _
      Or DbRow.Value(SQL_COL_TYPE_USER) = ENUM_TYPE_USER.COUNTR Then
        _currency_code = DbRow.Value(SQL_COL_CURRENY_ISO_CODE)
      ElseIf DbRow.Value(SQL_COL_TYPE_USER) = ENUM_TYPE_USER.USER_MOBILE_BANK Then
        _currency_code = _national_currency_code
      End If

    End If

    If _currency_code <> String.Empty Then

      Me.m_currency_find = _currency_code
      _idx_list = m_currencies.FindIndex(AddressOf FindIdCurrency)

      If _idx_list >= 0 Then
        _type_currency = m_currencies.Item(_idx_list)

      Else
        _type_currency.currency = Me.m_currency_find
        Me.AddCurrency(_type_currency)
        _idx_list = m_currencies.Count - 1
        _type_currency = m_currencies.Item(_idx_list)

      End If

      _type_currency.total_over_amount += DbRow.Value(SQL_COL_AMOUNT_OVER)
      _type_currency.total_short_amount += DbRow.Value(SQL_COL_AMOUNT_SHORT)
      _type_currency.total_difference_amount += DbRow.Value(SQL_COL_AMOUNT_DIFFERENCE_SHORT_OVER)
      m_currencies.Item(_idx_list) = _type_currency
    End If


    ' Row must be added to the grid only if details are required
    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _national_currency_code As String
    Dim _show_currency_symbol As Boolean
    Dim _currency_code As String

    _currency_code = String.Empty
    _show_currency_symbol = False
    _national_currency_code = CurrencyExchange.GetNationalCurrency()

    ' User Id
    If Not DbRow.IsNull(SQL_COL_USER_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_ID).Value = DbRow.Value(SQL_COL_USER_ID)

    End If

    ' User Type
    If Not DbRow.IsNull(SQL_COL_TYPE_USER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_USER).Value = DbRow.Value(SQL_COL_TYPE_USER)

    End If

    ' User Name
    If Not DbRow.IsNull(SQL_COL_USER_LOGIN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_LOGIN).Value = DbRow.Value(SQL_COL_USER_LOGIN)

    End If

    ' User FullName
    If Not DbRow.IsNull(SQL_COL_USER_FULLNAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_FULLNAME).Value = DbRow.Value(SQL_COL_USER_FULLNAME)

    End If

    ' User Employee Code
    If Not DbRow.IsNull(SQL_COL_USER_CODE_EMPLOYEE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_CODE_EMPLOYEE).Value = DbRow.Value(SQL_COL_USER_CODE_EMPLOYEE)

    End If

    ' Currency Iso Code
    If Not DbRow.IsNull(SQL_COL_CURRENY_ISO_CODE) AndAlso _
           Not DbRow.Value(SQL_COL_CURRENY_ISO_CODE) = String.Empty Then

      _currency_code = DbRow.Value(SQL_COL_CURRENY_ISO_CODE)
      If _currency_code = COLOR_CHIPS_ISO_CODE Then
        _currency_code = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291)
      End If

    Else

      If DbRow.Value(SQL_COL_TYPE_USER) = ENUM_TYPE_USER.USER_MOBILE_BANK Then
        _currency_code = _national_currency_code
      Else
        _currency_code = ""
      End If

    End If

    _show_currency_symbol = (String.IsNullOrEmpty(_currency_code) OrElse _currency_code = _national_currency_code)

    If _currency_code <> String.Empty Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CURRENY_ISO_CODE).Value = _currency_code

    End If

    ' Amount Short
    If Not DbRow.IsNull(SQL_COL_AMOUNT_SHORT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_SHORT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_AMOUNT_SHORT), _
                                                                                              ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , _
                                                                                              _show_currency_symbol, _
                                                                                              _currency_code)
    End If

    ' Amount Over
    If Not DbRow.IsNull(SQL_COL_AMOUNT_OVER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_OVER).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_AMOUNT_OVER), _
                                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , _
                                                                                             _show_currency_symbol, _
                                                                                             _currency_code)
    End If

    ' Amount Over - Short
    If Not DbRow.IsNull(SQL_COL_AMOUNT_DIFFERENCE_SHORT_OVER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER).Value = GUI_FormatCurrency(DbRow.Value(SQL_COL_AMOUNT_DIFFERENCE_SHORT_OVER), _
                                                                                                              ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , _
                                                                                                              _show_currency_symbol, _
                                                                                                              _currency_code)
    End If

    ''GUI_FormatCurrency(_total_amount, 2, , _show_currency_symbol)

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As System.Data.SqlClient.SqlCommand
    Dim _sql_cmd As SqlCommand

    _sql_cmd = New SqlCommand("ReportShortfallExcess")
    _sql_cmd.CommandType = CommandType.StoredProcedure
    _sql_cmd.Parameters.Add("@pGamingDayMode", SqlDbType.Bit).Value = Me.opt_working_day.Checked
    If (opt_movement_date.Checked) Then
      _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Me.dtp_from.Value
      _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Me.dtp_to.Value
    Else
      _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Me.uc_working_day_selector.FromDate.AddHours(Me.uc_working_day_selector.ClosingTime)
      _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Me.uc_working_day_selector.ToDate.AddHours(Me.uc_working_day_selector.ClosingTime)
    End If

    Return _sql_cmd
  End Function ' GUI_GetSqlCommand

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Call DrawTotalRowCurrency()

  End Sub

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call ResetTotalCounters()

  End Sub ' GUI_BeforeFirstRow

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(gb_date.Text, m_filter_date_type)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date_from = ""
    m_date_to = ""
    m_filter_date_type = ""

    ' Date type
    If opt_movement_date.Checked Then
      m_filter_date_type = opt_movement_date.Text
    ElseIf opt_working_day.Checked Then
      m_filter_date_type = opt_working_day.Text
    End If

    ' From Date
    m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    ' To Date
    m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                               ENUM_FORMAT_TIME.FORMAT_HHMM)
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions"
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions"
  ' PURPOSE : Add and draw a total row.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Type As ENUM_GRID_ROW_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub DrawTotalRowCurrency()
    Dim _idx_row As Integer
    Dim _str_total_descrip As String

    Dim _national_currency_code As String
    Dim _show_currency_symbol As Boolean
    Dim _currency_code As String

    _idx_row = 0
    _currency_code = String.Empty
    _show_currency_symbol = False
    _national_currency_code = CurrencyExchange.GetNationalCurrency()

    _str_total_descrip = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5557) & ": "

    If m_currencies.Count > 0 Then
      For Each _item_currency As TYPE_TOTAL_COUNTERS_CURRENCY In m_currencies
        Me.Grid.AddRow()
        _idx_row = Me.Grid.NumRows - 1

        _show_currency_symbol = (String.IsNullOrEmpty(_item_currency.currency) OrElse _item_currency.currency = _national_currency_code)

        'Add Row Total (Grouped by Currency)
        AddRowTotalRowCurrency(_idx_row, _str_total_descrip, _
                               _item_currency.currency, _
                               _item_currency.total_over_amount, _
                               _item_currency.total_short_amount, _
                               _item_currency.total_difference_amount, _
                               _show_currency_symbol)

      Next
    Else
      'Show Total with zeros if there's no results
      Me.Grid.AddRow()
      _show_currency_symbol = True
      AddRowTotalRowCurrency(_idx_row, _str_total_descrip, _
                             _national_currency_code, _
                             0, _
                             0, _
                             0, _
                             _show_currency_symbol)
    End If
  End Sub ' DrawTotalRowCurrency

  ' PURPOSE : Add Total Row in the grid
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Type As ENUM_GRID_ROW_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub AddRowTotalRowCurrency(ByVal IndexRowCell As Integer, _
                                     ByVal TotalDescription As String, _
                                     ByVal Currency As String, _
                                     ByVal TotalExcess As Decimal, _
                                     ByVal TotalShortFall As Decimal, _
                                     ByVal TotalDifferenceExcessShortFall As Decimal, _
                                     ByVal ShowCurrencySymbol As Boolean)

    'Total:
    Me.Grid.Cell(IndexRowCell, GRID_COLUMN_USER_CODE_EMPLOYEE).Value = TotalDescription

    'Currency (Divisa)
    If Currency = COLOR_CHIPS_ISO_CODE Then
      Currency = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291)
    End If

    Me.Grid.Cell(IndexRowCell, GRID_COLUMN_CURRENY_ISO_CODE).Value = Currency

    'Total Excess (sobrante)
    Me.Grid.Cell(IndexRowCell, GRID_COLUMN_AMOUNT_OVER).Value = GUI_FormatCurrency(TotalExcess, _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , _
                                                                                   ShowCurrencySymbol, _
                                                                                   Currency)
    'Total Shortfall (faltante)
    Me.Grid.Cell(IndexRowCell, GRID_COLUMN_AMOUNT_SHORT).Value = GUI_FormatCurrency(TotalShortFall, _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , _
                                                                                    ShowCurrencySymbol, _
                                                                                    Currency)
    'Difference (Excess - Shortfall )
    Me.Grid.Cell(IndexRowCell, GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER).Value = GUI_FormatCurrency(TotalDifferenceExcessShortFall, _
                                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, , _
                                                                                                    ShowCurrencySymbol, _
                                                                                                    Currency)
    'Set Color of the row
    Me.Grid.Row(IndexRowCell).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)


  End Sub ' AddRowTotalRowCurrency

  ' PURPOSE : Define All Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Type As ENUM_GRID_ROW_TYPE
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      .Clear()

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)


      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' * User 
      ' User ID
      .Column(GRID_COLUMN_USER_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_USER_ID).Header(1).Text = " ID "
      .Column(GRID_COLUMN_USER_ID).Width = 0

      ' User Type
      .Column(GRID_COLUMN_TYPE_USER).Header(0).Text = ""
      .Column(GRID_COLUMN_TYPE_USER).Header(1).Text = " USER TYPE "
      .Column(GRID_COLUMN_TYPE_USER).Width = 0

      ' User Name 
      .Column(GRID_COLUMN_USER_LOGIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5665)
      .Column(GRID_COLUMN_USER_LOGIN).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(326)
      .Column(GRID_COLUMN_USER_LOGIN).Width = GRID_WIDTH_USER_LOGIN
      .Column(GRID_COLUMN_USER_LOGIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' User FullName
      .Column(GRID_COLUMN_USER_FULLNAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5665)
      .Column(GRID_COLUMN_USER_FULLNAME).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(327)
      .Column(GRID_COLUMN_USER_FULLNAME).Width = GRID_WIDTH_USER_FULLNAME
      .Column(GRID_COLUMN_USER_FULLNAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' User Employee Code
      .Column(GRID_COLUMN_USER_CODE_EMPLOYEE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5665)
      .Column(GRID_COLUMN_USER_CODE_EMPLOYEE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4704)
      .Column(GRID_COLUMN_USER_CODE_EMPLOYEE).Width = GRID_WIDTH_USER_FULLNAME
      .Column(GRID_COLUMN_USER_CODE_EMPLOYEE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Currency Iso Code
      .Column(GRID_COLUMN_CURRENY_ISO_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_CURRENY_ISO_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5668)
      .Column(GRID_COLUMN_CURRENY_ISO_CODE).Width = GRID_WIDTH_CURRENY_ISO_CODE
      .Column(GRID_COLUMN_CURRENY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Short (Faltante)
      .Column(GRID_COLUMN_AMOUNT_SHORT).Header(0).Text = ""
      .Column(GRID_COLUMN_AMOUNT_SHORT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5669)
      .Column(GRID_COLUMN_AMOUNT_SHORT).Width = GRID_WIDTH_AMOUNT_SHORT
      .Column(GRID_COLUMN_AMOUNT_SHORT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Over (Sobrante)
      .Column(GRID_COLUMN_AMOUNT_OVER).Header(0).Text = ""
      .Column(GRID_COLUMN_AMOUNT_OVER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5670)
      .Column(GRID_COLUMN_AMOUNT_OVER).Width = GRID_WIDTH_AMOUNT_OVER
      .Column(GRID_COLUMN_AMOUNT_OVER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Difference (over - short)
      .Column(GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER).Header(0).Text = ""
      .Column(GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5671)
      .Column(GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER).Width = GRID_WIDTH_AMOUNT_DIFFERENCE_SHORT_OVER
      .Column(GRID_COLUMN_AMOUNT_DIFFERENCE_SHORT_OVER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Sortable = False
      ''.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Fill all currency types
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddCurrency(ByVal _currency_type As TYPE_TOTAL_COUNTERS_CURRENCY)

    _currency_type.total_over_amount = 0
    _currency_type.total_short_amount = 0
    _currency_type.total_difference_amount = 0
    m_currencies.Add(_currency_type)

  End Sub ' AddCurrency

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ' Dates
    Me.opt_movement_date.Checked = True
    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening().AddDays(-1)
    Me.dtp_to.Value = WSI.Common.Misc.TodayOpening()
    Me.uc_working_day_selector.FromDate = Me.dtp_from.Value
    Me.uc_working_day_selector.ToDate = Me.dtp_to.Value
    Me.uc_working_day_selector.ClosingTime = GetDefaultClosingTime()
    Me.dtp_to.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE : Find element in the list
  '
  '  PARAMS :
  '      - INPUT :
  '        - DgFilter : TYPE_DG_FILTER_ELEMENT
  '
  '      - OUTPUT :
  '
  ' RETURNS : Boolean (true if exists, false if not)
  '
  Private Function FindIdCurrency(ByVal DgFilter As TYPE_TOTAL_COUNTERS_CURRENCY) As Boolean
    If DgFilter.currency = Me.m_currency_find Then
      Return True
    Else
      Return False
    End If
  End Function ' FindIdStatusHandPay

  ' PURPOSE : Reset counters to 0.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - Total As TYPE_TOTAL_COUNTERS_STATUS
  '           - ActualDay As Date
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ResetTotalCounters()
    m_currencies.Clear()

  End Sub ' ResetTotalCounters

#End Region ' Private Functions

#Region " Constructor "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    m_currencies = New List(Of TYPE_TOTAL_COUNTERS_CURRENCY)

  End Sub

#End Region ' Constructor

#Region " Events "

  Private Sub opt_movement_date_CheckedChanged(sender As Object, e As EventArgs) Handles opt_movement_date.CheckedChanged
    uc_working_day_selector.Visible = opt_working_day.Checked
    If (opt_working_day.Checked) Then
      Me.uc_working_day_selector.FromDate = Me.dtp_from.Value
      Me.uc_working_day_selector.ToDate = Me.dtp_to.Value

    Else
      Dim _time_from As Date
      Dim _time_to As Date

      _time_from = _time_from.AddHours(Me.dtp_from.Value.Hour)
      _time_from = _time_from.AddMinutes(Me.dtp_from.Value.Minute)
      _time_from = _time_from.AddSeconds(Me.dtp_from.Value.Second)

      Me.dtp_from.Value = Me.uc_working_day_selector.FromDate

      Me.dtp_from.Value = Me.dtp_from.Value.AddHours(_time_from.Hour)
      Me.dtp_from.Value = Me.dtp_from.Value.AddMinutes(_time_from.Minute)
      Me.dtp_from.Value = Me.dtp_from.Value.AddSeconds(_time_from.Second)

      _time_to = _time_to.AddHours(Me.dtp_to.Value.Hour)
      _time_to = _time_to.AddMinutes(Me.dtp_to.Value.Minute)
      _time_to = _time_to.AddSeconds(Me.dtp_to.Value.Second)

      Me.dtp_to.Value = Me.uc_working_day_selector.ToDate

      Me.dtp_to.Value = Me.dtp_to.Value.AddHours(_time_to.Hour)
      Me.dtp_to.Value = Me.dtp_to.Value.AddMinutes(_time_to.Minute)
      Me.dtp_to.Value = Me.dtp_to.Value.AddSeconds(_time_to.Second)

    End If

  End Sub

#End Region ' Events

End Class