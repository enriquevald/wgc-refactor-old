﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_terminals_block
'   DESCRIPTION : Block specific terminals
'        AUTHOR : Samuel González
' CREATION DATE : 07-JUL-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-JUL-2015  SGB    Initial version. Backlog Item 2665
' 15-FEB-2017  MS     Bug 24619:Wigos GUI Dev Branch: WGC Machine Lock [frm_terminals_block] Cannot add Machines to Exceptions
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Groups
Imports GUI_Controls.uc_terminals_group_filter

Public Class frm_terminals_block
  Inherits frm_base_edit

#Region " Structures "

  Private Structure PARAMS_TO_ADD

    Dim item_group As ItemGroup
    Dim data_table_to_add As DataTable
    Dim data_table_to_delete As DataTable

  End Structure ' PARAMS_TO_ADD

#End Region ' Structures

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 9
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' GRID TERMINALS CONFLICT
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_PROVIDER As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_FLOOR_ID As Integer = 3
  Private Const GRID_COLUMN_DENOMINATION As Integer = 4
  Private Const GRID_COLUMN_AREA As Integer = 5
  Private Const GRID_COLUMN_BANK_ID As Integer = 6
  Private Const GRID_COLUMN_POSITION As Integer = 7
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 8

  ' GRID TERMINALS CONFLICT WIDTH
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 150
  Private Const GRID_COLUMN_WIDTH_PROVIDER As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_NAME As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_FLOOR_ID As Integer = 1120
  Private Const GRID_COLUMN_WIDTH_DENOMINATION As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_BANK_ID As Integer = 1400
  Private Const GRID_COLUMN_WIDTH_AREA As Integer = 1900
  Private Const GRID_COLUMN_WIDTH_POSITION As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_TERMINAL_ID As Integer = 0

  Private Const DATATABLE_TERMINAL_ID As Integer = 0
  Private Const DATATABLE_PROVIDER_NAME As Integer = 1
  Private Const DATATABLE_MACHINE_NAME As Integer = 2
  Private Const DATATABLE_FLOOR_ID As Integer = 3
  Private Const DATATABLE_DENOMINATION As Integer = 4
  Private Const DATATABLE_AREA As Integer = 5
  Private Const DATATABLE_BANK As Integer = 6
  Private Const DATATABLE_POSITION As Integer = 7

#End Region 'Constants

#Region " Enums"

  Private Enum ENUM_RESULT_BLOCK
    OK = 0
    ERROR_AUDIT = 1
    ERROR_SETFLAG = 2

  End Enum ' ENUM_RESULT_BLOCK

#End Region ' Enums

#Region " Members "

  Private m_terminals_block As DataTable
  Private m_terminals_unblock As DataTable
  Private m_terminals_all As DataTable

#End Region 'Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_BLOCK

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT: None
  '
  '    - OUTPUT: None
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    Dim _terminal_list As TerminalList

    Call MyBase.GUI_InitControls()

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6542)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    'Terminals count
    Me.gb_current_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6635)
    Me.tf_terminals_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6629)
    Me.tf_terminals_block.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6630)
    Me.tf_terminals_block.LabelForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    Me.tf_terminals_unblock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6631)
    Me.tf_terminals_unblock.LabelForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_00)
    Me.tf_terminal_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6634)

    ' Groupbox config terminals block
    Me.gb_block_automatic.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6543)
    Me.chk_block.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6544)
    Me.ef_end_trading.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6545)
    Me.ef_start_trading.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6546)
    Me.lbl_minutes_block.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547)
    Me.lbl_minutes_unblock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6547)
    Me.lbl_last_end_trading.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6550)
    Me.lbl_last_start_trading.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6551)
    Call FillComboAfterBefore()
    Me.ef_end_trading.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_start_trading.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    'Button block machine
    Me.gb_block_manual.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6651)
    Me.btn_block_all_terminals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6637)
    Me.btn_unblock_all_terminals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6638)

    ' Groupbox exception
    Me.gb_block_exceptions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6552)
    Me.tgf_terminals.SetGroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6556)
    Me.gb_terminals_ever_block.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6553, "0")
    Me.gb_terminals_ever_unblock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6554, "0")
    Me.lbl_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6555)
    Me.lbl_info.ForeColor = Color.Blue

    ' Terminals list
    _terminal_list = New TerminalList
    _terminal_list.SetListType(TerminalList.TerminalListType.Listed)
    Me.tgf_terminals.SetTerminalList(_terminal_list)
    Me.tgf_terminals.Size = New System.Drawing.Size(275, 396)

    ' Stylesheet terminals exception
    Call GUI_StyleSheet(dg_terminals_ever_block)
    Call GUI_StyleSheet(dg_terminals_ever_unblock)

    ' Inizialize DataTables
    Call InizializeDataTableTerminalsToGrid(m_terminals_block)
    Call InizializeDataTableTerminalsToGrid(m_terminals_unblock)

  End Sub ' GUI_InitControls

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _terminals_block As CLASS_TERMINALS_BLOCK

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _terminals_block = DbReadObject

    Try

      ' Terminals count
      SetTerminalsCount()

      'Configuration terminals block
      chk_block.Checked = _terminals_block.DisableMachine
      ef_end_trading.TextValue = _terminals_block.EndTrading
      ef_start_trading.TextValue = _terminals_block.StartTrading
      cmb_end_trading.SelectedIndex = _terminals_block.EndTradingAfterBefoure
      cmb_start_trading.SelectedIndex = _terminals_block.StartTradingAfterBefoure

      ' Exception
      Call SetDataTableData(_terminals_block.TerminalsAlwaysBlock, m_terminals_block, Me.dg_terminals_ever_block)
      Call SetDataTableData(_terminals_block.TerminalsAlwaysUnblock, m_terminals_unblock, Me.dg_terminals_ever_unblock)

      ' Call for locking / unlocking control forms
      EnableDisableControls(_terminals_block.DisableMachine)

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub 'GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim _terminals_block As CLASS_TERMINALS_BLOCK

    _terminals_block = DbEditedObject

    _terminals_block.BlockAutomatic = Me.gb_block_automatic.Enabled
    _terminals_block.BlockExceptions = Me.gb_block_exceptions.Enabled

    ' Automatic lock
    If _terminals_block.BlockAutomatic Then
      _terminals_block.DisableMachine = Me.chk_block.Checked
      _terminals_block.EndTrading = GUI_ParseNumber(Me.ef_end_trading.TextValue)
      _terminals_block.StartTrading = GUI_ParseNumber(Me.ef_start_trading.TextValue)
      _terminals_block.EndTradingAfterBefoure = cmb_end_trading.SelectedIndex
      _terminals_block.StartTradingAfterBefoure = cmb_start_trading.SelectedIndex
    End If

    ' Exceptions lock
    If _terminals_block.BlockExceptions Then
      _terminals_block.TerminalsAlwaysBlock = GetDataTableData(m_terminals_block, TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_BLOCKED)
      _terminals_block.TerminalsAlwaysUnblock = GetDataTableData(m_terminals_unblock, TerminalStatusFlags.MACHINE_FLAGS.ALWAYS_UNBLOCKED)
      _terminals_block.TerminalsSelected = m_terminals_all
    End If

    ' Call for locking / unlocking control forms
    EnableDisableControls(_terminals_block.DisableMachine)

  End Sub 'GUI_GetScreenData

  ' PURPOSE: It checks if the entry data are ok calling the function CheckEntryData
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Try
      ' Fill grid by datatable
      If Me.chk_block.Checked Then

        If String.IsNullOrEmpty(ef_end_trading.TextValue) OrElse Integer.Parse(ef_end_trading.TextValue) > 180 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6581), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6545).ToLower, "180")
          Call ef_end_trading.Focus()

          Return False
        End If

        If String.IsNullOrEmpty(ef_start_trading.TextValue) OrElse Integer.Parse(ef_start_trading.TextValue) > 180 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6581), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6546).ToLower, "180")
          Call ef_start_trading.Focus()

          Return False
        End If
      End If

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _tito_params As CLASS_TERMINALS_BLOCK

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TERMINALS_BLOCK()
        _tito_params = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2853), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2854), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '           - AndPerm TYPE_PERMISSIONS
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    MyBase.GUI_Permissions(AndPerm)

    ' GroupBox Permissions
    Me.gb_block_automatic.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_TERMINALS_BLOCK_AUTOMATIC).Write AndAlso AndPerm.Write
    Me.gb_block_manual.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_TERMINALS_BLOCK_MANUAL).Write AndAlso AndPerm.Write
    Me.gb_block_exceptions.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_TERMINALS_BLOCK_EXCEPTIONS).Write AndAlso AndPerm.Write

    ' Form Permissions
    AndPerm.Write = AndPerm.Write And (CurrentUser.Permissions(ENUM_FORM.FORM_TERMINALS_BLOCK_AUTOMATIC).Write OrElse CurrentUser.Permissions(ENUM_FORM.FORM_TERMINALS_BLOCK_EXCEPTIONS).Write)

  End Sub ' GUI_Permissions

#End Region 'Overrides

#Region " Private Functions "

  ' PURPOSE: Fill combos to start trading and end trading.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub FillComboAfterBefore()

    'Before / After
    'block
    Me.cmb_end_trading.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548))
    Me.cmb_end_trading.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549))

    'unblock
    Me.cmb_start_trading.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548))
    Me.cmb_start_trading.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6549))

  End Sub ' FillComboAfterBefore

  ' PURPOSE: Lock or unlock controls depending of DisableMachine parameter
  '
  '  PARAMS:
  '     - INPUT:
  '           - Disable (Boolean)
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableDisableControls(ByVal Disable As Boolean)

    Me.cmb_end_trading.Enabled = Disable
    Me.cmb_start_trading.Enabled = Disable
    Me.ef_end_trading.Enabled = Disable
    Me.ef_start_trading.Enabled = Disable

  End Sub ' EnableDisableControls

  ' PURPOSE: Set values counts state terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetTerminalsCount()

    Dim _dt_terminals_count As DataTable
    Dim _info_tooltip As String

    'Terminals count
    Try
      _dt_terminals_count = CLASS_TERMINALS_BLOCK.ReadCountStatusTerminals()
      tf_terminals_total.Value = _dt_terminals_count.Rows(0).Item("TerminalCount")
      tf_terminals_block.Value = _dt_terminals_count.Rows(0).Item("BlockTotal")
      tf_terminals_unblock.Value = _dt_terminals_count.Rows(0).Item("UnblockTotal")
      tf_terminal_session.Value = _dt_terminals_count.Rows(0).Item("TerminalConnected")

      _info_tooltip = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6632, _
                                                            _dt_terminals_count.Rows(0).Item("BlockTotal"), _
                                                            _dt_terminals_count.Rows(0).Item("BlockAutomatic"), _
                                                            _dt_terminals_count.Rows(0).Item("BlockManual"), _
                                                            _dt_terminals_count.Rows(0).Item("BlockAlways")) _
                     & vbCrLf & _
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(6633, _
                                                            _dt_terminals_count.Rows(0).Item("UnblockTotal"), _
                                                            IIf(Me.chk_block.Checked, _dt_terminals_count.Rows(0).Item("UnblockAutomatic"), "0"), _
                                                            _dt_terminals_count.Rows(0).Item("UnblockManual"), _
                                                            _dt_terminals_count.Rows(0).Item("UnblockAlways"))

      tt_terminal_count.SetToolTip(gb_current_status, _info_tooltip)
      tt_terminal_count.SetToolTip(tf_terminals_total, _info_tooltip)
      tt_terminal_count.SetToolTip(tf_terminals_block, _info_tooltip)
      tt_terminal_count.SetToolTip(tf_terminals_unblock, _info_tooltip)
      tt_terminal_count.SetToolTip(tf_terminal_session, _info_tooltip)

    Catch _ex As Exception

      Log.Exception(_ex)

      tf_terminals_total.Value = 0
      tf_terminals_block.Value = 0
      tf_terminals_unblock.Value = 0
      tf_terminal_session.Value = 0

    End Try

  End Sub ' SetTerminalsCount

  ' PURPOSE : Block / Unblock All Terminales from a site
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Sub BlockUnBlockAllTerminals(sender As Object, e As EventArgs)

    Dim _error_reason As ENUM_RESULT_BLOCK
    Dim _block_unblock As String
    Dim _set_flag As Boolean
    Dim _source_name As String
    Dim _terminals_in_session_lock As Boolean

    _error_reason = ENUM_RESULT_BLOCK.OK
    If sender.name.ToString.ToUpper = "BTN_BLOCK_ALL_TERMINALS" Then
      _block_unblock = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6654)
      _set_flag = True
    Else
      _block_unblock = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6655)
      _set_flag = False
    End If

    _terminals_in_session_lock = CInt(tf_terminal_session.Value) > 0 AndAlso _set_flag = True

    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6568), _
                  IIf(_terminals_in_session_lock, ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_TYPE.MB_TYPE_INFO), _
                  ENUM_MB_BTN.MB_BTN_YES_NO, _
                  ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                  _block_unblock, _
                  IIf(_terminals_in_session_lock, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6652), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6653))) = ENUM_MB_RESULT.MB_RESULT_YES Then

      Using _trx As New DB_TRX
        If (WSI.Common.TerminalStatusFlags.SetAllTerminalsBlockStatus(WSI.Common.TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_MANUALLY, _
                                                                      IIf(sender.name.ToString.ToUpper = "BTN_BLOCK_ALL_TERMINALS", True, False), _
                                                                      _trx.SqlTransaction)) Then

          _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName
          If _set_flag Then
            _block_unblock = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6565).ToLower
          Else
            _block_unblock = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6566).ToLower
          End If

          If Alarm.Register(AlarmSourceCode.TerminalSystem, _
                              GLB_CurrentUser.Id,
                              _source_name, _
                              IIf(_set_flag, AlarmCode.Machine_Manual_Block, AlarmCode.Machine_Manual_Unblock), _
                               GLB_NLS_GUI_PLAYER_TRACKING.GetString(6564, _block_unblock), _
                              AlarmSeverity.Info) AndAlso _
              AuditSetFlagTerminal(_set_flag) Then

            _trx.Commit()

            _error_reason = ENUM_RESULT_BLOCK.OK
          Else

            _error_reason = ENUM_RESULT_BLOCK.ERROR_AUDIT
          End If
        Else

          _error_reason = ENUM_RESULT_BLOCK.ERROR_SETFLAG
        End If
      End Using

      Select Case _error_reason
        Case ENUM_RESULT_BLOCK.OK

          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6564), ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _block_unblock) ' block / unblock succefully

          Call SetTerminalsCount()

        Case ENUM_RESULT_BLOCK.ERROR_SETFLAG,
             ENUM_RESULT_BLOCK.ERROR_AUDIT

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6563), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)

      End Select

    End If

  End Sub ' BlockUnBlockAllTerminals

  ' PURPOSE : Audit Collect Collection 
  '
  '  PARAMS :
  '     - INPUT :
  '         - Block/Unblock machines boolean
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function AuditSetFlagTerminal(ByVal BlockMachines As Boolean)

    Dim _auditor_data As CLASS_AUDITOR_DATA

    Try

      ' Initialize
      _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
      _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6567), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6567))
      _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(6539), _
                             IIf(BlockMachines, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6561), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6562)))

      _auditor_data.Notify(CurrentUser.GuiId, _
                            CurrentUser.Id, _
                            CurrentUser.Name, _
                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                            0)

      Return True

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False

  End Function ' AuditSetFlagTerminal

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet(ByRef Dg As GUI_Controls.uc_grid)

    With Dg
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, Me.Permissions.Write)
      .Sortable = True


      ' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header.Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      ' Provider
      .Column(GRID_COLUMN_PROVIDER).Header.Text = GLB_NLS_GUI_AUDITOR.GetString(341) ' Proveedor
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_COLUMN_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6636) ' Máquina
      .Column(GRID_COLUMN_NAME).Width = GRID_COLUMN_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Floor ID
      .Column(GRID_COLUMN_FLOOR_ID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(432) ' Id de planta
      .Column(GRID_COLUMN_FLOOR_ID).Width = GRID_COLUMN_WIDTH_FLOOR_ID
      .Column(GRID_COLUMN_FLOOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7302) ' Denominación contable
      .Column(GRID_COLUMN_DENOMINATION).Width = GRID_COLUMN_WIDTH_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Location area  
      .Column(GRID_COLUMN_AREA).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435) ' Area
      .Column(GRID_COLUMN_AREA).Width = GRID_COLUMN_WIDTH_AREA
      .Column(GRID_COLUMN_AREA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bank_id (Island)
      .Column(GRID_COLUMN_BANK_ID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431) ' Isla
      .Column(GRID_COLUMN_BANK_ID).Width = GRID_COLUMN_WIDTH_BANK_ID
      .Column(GRID_COLUMN_BANK_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Position
      .Column(GRID_COLUMN_POSITION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5222) ' Posición
      .Column(GRID_COLUMN_POSITION).Width = GRID_COLUMN_WIDTH_POSITION
      .Column(GRID_COLUMN_POSITION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header.Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_COLUMN_WIDTH_TERMINAL_ID

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Add a tree node to element's list
  '
  '    - INPUT:
  '         - ParamsToAdd (PARAMS_TO_ADD)
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddTreeNodeToGrid(ByRef ParamsToAdd As PARAMS_TO_ADD)

    Select Case ParamsToAdd.item_group.ElementType

      Case GROUP_NODE_ELEMENT.GROUP, _
           GROUP_NODE_ELEMENT.GROUPS, _
           GROUP_NODE_ELEMENT.PROVIDER, _
           GROUP_NODE_ELEMENT.PROVIDERS, _
           GROUP_NODE_ELEMENT.ZONE, _
           GROUP_NODE_ELEMENT.ZONES, _
           GROUP_NODE_ELEMENT.AREAS, _
           GROUP_NODE_ELEMENT.BANKS

        ' Calculate subelements of tree node root
        If Not ParamsToAdd.item_group.ElementExpanded Then
          Me.tgf_terminals.AddChildNodes(ParamsToAdd.item_group)
        End If

        ' Add all child elements to list
        For Each _node As ItemGroup In ParamsToAdd.item_group.Nodes
          ParamsToAdd.item_group = _node
          Call AddTreeNodeToGrid(ParamsToAdd)
        Next

      Case GROUP_NODE_ELEMENT.TERMINALS

        ' Add element to list
        Call AddNewElement(ParamsToAdd)

    End Select

  End Sub ' AddTreeNodeToList

  ' PURPOSE: Add new element
  '
  '  PARAMS:
  '     - INPUT:
  '         - ParamsToAdd (PARAMS_TO_ADD)
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub AddNewElement(ByRef ParamsToAdd As PARAMS_TO_ADD)

    ' Delete Item in DataTable
    If ParamsToAdd.data_table_to_delete.Select("TERMINAL_ID = " & ParamsToAdd.item_group.ElementTerminalId).Length > 0 Then
      ParamsToAdd.data_table_to_delete.Rows.Remove(ParamsToAdd.data_table_to_delete.Select("TERMINAL_ID = " & ParamsToAdd.item_group.ElementTerminalId)(0))
    End If

    ' Add Item in DataTable
    If ParamsToAdd.data_table_to_add.Select("TERMINAL_ID = " & ParamsToAdd.item_group.ElementTerminalId).Length = 0 Then
      Call NewRowToDataTable(ParamsToAdd.item_group.ElementTerminalId, ParamsToAdd.item_group.ElementName, ParamsToAdd.data_table_to_add)
    End If

  End Sub ' AddNewElement

  ' PURPOSE: Delete elements on grid.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Grid to delete elements
  '          - DataTable to delete elements
  '          - Delete all boolean
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub DeleteElementsGrid(ByRef GridToDelete As uc_grid, ByRef DataTableTerminals As DataTable, ByVal DeleteAll As Boolean)

    Dim _selected_rows As Array
    Dim _idx_row As Integer
    Dim _idx As Integer

    If DeleteAll Then
      DataTableTerminals.Clear()
      GridToDelete.Clear()
    Else
      _selected_rows = GridToDelete.SelectedRows()
      If _selected_rows Is Nothing Then
        Return
      End If

      For _idx = _selected_rows.Length - 1 To 0 Step -1
        _idx_row = _selected_rows(_idx)

        If (_idx_row <= GridToDelete.NumRows - 1) Then
          If GridToDelete.Row(_idx_row).IsSelected = True OrElse DeleteAll Then
            DataTableTerminals.Rows.Remove(DataTableTerminals.Select("TERMINAL_ID = " & GridToDelete.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value)(0))
            GridToDelete.DeleteRow(_idx_row)
          End If
        End If
      Next

    End If

  End Sub ' DeleteElementsGrid

  ' PURPOSE: Refresh grid.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Grid to add rows
  '          - DataTable origen
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshGrid(ByRef GridToAdd As uc_grid, ByRef DataTableTerminals As DataTable)

    Dim _view As DataView
    Dim _dt As DataTable

    GridToAdd.Redraw = False
    GridToAdd.Clear()

    _view = New DataView(DataTableTerminals)
    _dt = New DataTable()

    _view.Sort = "PROVIDER_NAME, MACHINE_NAME"
    _dt = _view.ToTable()

    For Each _row As DataRow In _dt.Rows
      GridToAdd.AddRow()

      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_PROVIDER).Value = _row.Item(DATATABLE_PROVIDER_NAME).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_NAME).Value = _row.Item(DATATABLE_MACHINE_NAME).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_FLOOR_ID).Value = _row.Item(DATATABLE_FLOOR_ID).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_DENOMINATION).Value = _row.Item(DATATABLE_DENOMINATION).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_AREA).Value = _row.Item(DATATABLE_AREA).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_BANK_ID).Value = _row.Item(DATATABLE_BANK).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_POSITION).Value = _row.Item(DATATABLE_POSITION).ToString()
      GridToAdd.Cell(GridToAdd.NumRows - 1, GRID_COLUMN_TERMINAL_ID).Value = _row.Item(DATATABLE_TERMINAL_ID).ToString()
    Next

    GridToAdd.Redraw = True

  End Sub ' RefreshGrid

  ' PURPOSE: Refresh text groupbox.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Grid to refresh
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshTextGroupBox(ByRef GridToRefresh As uc_grid)
    If GridToRefresh.Name = Me.dg_terminals_ever_block.Name Then
      gb_terminals_ever_block.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6553, GridToRefresh.NumRows.ToString)
    Else
      gb_terminals_ever_unblock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6554, GridToRefresh.NumRows.ToString)
    End If
  End Sub ' RefreshTextGroupBox

  ' PURPOSE: Inizialize DataTables.
  '
  '  PARAMS:
  '     - INPUT:
  '          - DataTable to inizialize
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub InizializeDataTableTerminalsToGrid(ByRef DataTableTerminals As DataTable)

    DataTableTerminals = New DataTable

    DataTableTerminals.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"))
    DataTableTerminals.Columns.Add("PROVIDER_NAME", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("MACHINE_NAME", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("FLOOR_ID", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("DENOMINATION", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("AREA", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("BANK", Type.GetType("System.String"))
    DataTableTerminals.Columns.Add("POSITION", Type.GetType("System.String"))

  End Sub ' InizializeDataTableTerminalsToGrid

  ' PURPOSE: New row to DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '          - Element Id of terminal report
  '          - Element Name of terminal report
  '          - DataTable destination terminal
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub NewRowToDataTable(ByVal ElementId As Integer, ByVal ElementName As String, ByRef DataTableTerminals As DataTable)

    Dim _row As DataRow
    Dim _index As Integer

    _row = DataTableTerminals.NewRow()
    _index = 1

    For Each _column As Object In TerminalReport.GetReportDataList(ElementId, ReportType.Full)
      _row.Item(_index) = _column.ToString()
      _index += 1
    Next

    _row.Item(DATATABLE_TERMINAL_ID) = ElementId
    _row.Item(DATATABLE_MACHINE_NAME) = ElementName

    DataTableTerminals.Rows.Add(_row)

  End Sub ' NewRowToDataTable

  ' PURPOSE: Set DataTable with screen data.
  '
  '  PARAMS:
  '     - INPUT:
  '          - DataTable of origen terminals
  '          - DataTable of destination
  '          - Grid of destionation info
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetDataTableData(ByVal DataTableOrigen As DataTable, ByRef DataTableDestination As DataTable, ByRef GridDestination As uc_grid)

    If DataTableOrigen.Rows.Count > 0 Then

      For Each _row As DataRow In DataTableOrigen.Rows
        Call NewRowToDataTable(_row(DATATABLE_TERMINAL_ID), _row(DATATABLE_MACHINE_NAME), DataTableDestination)
      Next

      Call RefreshGrid(GridDestination, DataTableDestination)
      Call RefreshTextGroupBox(GridDestination)

    End If

  End Sub ' SetDataTableData

  ' PURPOSE: Get DataTable data to screen.
  '
  '  PARAMS:
  '     - INPUT:
  '          - DataTable of Terminals
  '          - Machine_flags
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '          - DataTable
  Private Function GetDataTableData(ByRef DataTableTerminals As DataTable, ByVal MachineFlag As TerminalStatusFlags.MACHINE_FLAGS) As DataTable

    Dim _dt As DataTable
    Dim _dr As DataRow

    _dt = New DataTable()
    _dt.Columns.Add("TS_TERMINAL_ID", Type.GetType("System.Int32"))
    _dt.Columns.Add("TS_MACHINE_FLAGS", Type.GetType("System.Int32"))
    _dt.Columns.Add("TE_NAME", Type.GetType("System.String"))

    Try

      For Each _terminal As DataRow In DataTableTerminals.Rows
        _dr = _dt.NewRow()
        _dr("TS_TERMINAL_ID") = _terminal.Item(DATATABLE_TERMINAL_ID)
        _dr("TS_MACHINE_FLAGS") = MachineFlag
        _dr("TE_NAME") = _terminal.Item(DATATABLE_MACHINE_NAME)
        _dt.Rows.Add(_dr)
      Next

      ' Fill DataTable with all terminals
      If m_terminals_all Is Nothing Then
        m_terminals_all = New DataTable()
      End If
      m_terminals_all.Merge(_dt)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

    Return _dt

  End Function ' GetDataTableData

#End Region 'Private functions

#Region " Events "

  Private Sub chk_block_CheckedChanged(sender As Object, e As EventArgs) Handles chk_block.CheckedChanged
    EnableDisableControls(chk_block.Checked)
  End Sub ' chk_block_CheckedChanged

  Private Sub tmr_terminals_count_Tick(sender As Object, e As EventArgs) Handles tmr_terminals_count.Tick
    SetTerminalsCount()
  End Sub ' tmr_terminals_count_Tick

  Private Sub btn_block_unblock_all_terminals_Click(sender As Object, e As EventArgs) Handles btn_block_all_terminals.Click, btn_unblock_all_terminals.Click
    BlockUnBlockAllTerminals(sender, e)
  End Sub ' btn_block_all_terminals_Click

  Private Sub btn_add_block_Click(sender As Object, e As EventArgs) Handles btn_add_block.Click

    Dim _params As PARAMS_TO_ADD

    Try

      ' Add the selected tree node to element's list
      If Not tgf_terminals.Tree.SelectedNode Is Nothing Then

        _params.item_group = CType(tgf_terminals.Tree.SelectedNode, ItemGroup)
        _params.data_table_to_add = m_terminals_block
        _params.data_table_to_delete = m_terminals_unblock

        Call AddTreeNodeToGrid(_params)
        Call RefreshGrid(Me.dg_terminals_ever_block, _params.data_table_to_add)
        Call RefreshGrid(Me.dg_terminals_ever_unblock, _params.data_table_to_delete)
        Call RefreshTextGroupBox(Me.dg_terminals_ever_block)
        Call RefreshTextGroupBox(Me.dg_terminals_ever_unblock)

      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_add_block_Click

  Private Sub btn_rmv_block_Click(sender As Object, e As EventArgs) Handles btn_rmv_block.Click

    Try

      DeleteElementsGrid(dg_terminals_ever_block, m_terminals_block, False)
      Call RefreshTextGroupBox(Me.dg_terminals_ever_block)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_rmv_block_Click

  Private Sub btn_rmv_block_all_Click(sender As Object, e As EventArgs) Handles btn_rmv_block_all.Click

    Try

      If dg_terminals_ever_block.NumRows = 0 Then
        Return
      End If

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(168), _
              ENUM_MB_TYPE.MB_TYPE_WARNING, _
              ENUM_MB_BTN.MB_BTN_YES_NO, _
              ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then
        Return
      End If

      DeleteElementsGrid(dg_terminals_ever_block, m_terminals_block, True)
      Call RefreshTextGroupBox(Me.dg_terminals_ever_block)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_rmv_block_all_Click

  Private Sub btn_add_unblock_Click(sender As Object, e As EventArgs) Handles btn_add_unblock.Click

    Dim _params As PARAMS_TO_ADD

    Try

      ' Add the selected tree node to element's list
      If Not tgf_terminals.Tree.SelectedNode Is Nothing Then

        _params.item_group = CType(tgf_terminals.Tree.SelectedNode, ItemGroup)
        _params.data_table_to_add = m_terminals_unblock
        _params.data_table_to_delete = m_terminals_block

        Call AddTreeNodeToGrid(_params)
        Call RefreshGrid(Me.dg_terminals_ever_unblock, _params.data_table_to_add)
        Call RefreshGrid(Me.dg_terminals_ever_block, _params.data_table_to_delete)
        Call RefreshTextGroupBox(Me.dg_terminals_ever_unblock)
        Call RefreshTextGroupBox(Me.dg_terminals_ever_block)

      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_add_unblock_Click

  Private Sub btn_rmv_unblock_Click(sender As Object, e As EventArgs) Handles btn_rmv_unblock.Click

    Try

      DeleteElementsGrid(dg_terminals_ever_unblock, m_terminals_unblock, False)
      Call RefreshTextGroupBox(Me.dg_terminals_ever_unblock)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_rmv_unblock_Click

  Private Sub btn_rmv_unblock_all_Click(sender As Object, e As EventArgs) Handles btn_rmv_unblock_all.Click

    Try

      If dg_terminals_ever_unblock.NumRows = 0 Then
        Return
      End If

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(168), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then
        Return
      End If

      DeleteElementsGrid(dg_terminals_ever_unblock, m_terminals_unblock, True)
      Call RefreshTextGroupBox(Me.dg_terminals_ever_unblock)

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' btn_rmv_unblock_all_Click

#End Region ' Events

End Class ' btn_rmv_unblock_Click