﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_generic_report_sel
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_generic_report_sel))
    Me.cmb_report = New GUI_Controls.uc_combo()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dt_date_to = New GUI_Controls.uc_date_picker()
    Me.dt_date_from = New GUI_Controls.uc_date_picker()
    Me.btn_excel = New GUI_Controls.uc_button()
    Me.btn_visualize = New GUI_Controls.uc_button()
    Me.lbl_filename = New GUI_Controls.uc_text_field()
    Me.lnk_filename = New System.Windows.Forms.LinkLabel()
    Me.btn_exit = New GUI_Controls.uc_button()
    Me.btn_print = New GUI_Controls.uc_button()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'cmb_report
    '
    Me.cmb_report.AllowUnlistedValues = False
    Me.cmb_report.AutoCompleteMode = False
    Me.cmb_report.IsReadOnly = False
    Me.cmb_report.Location = New System.Drawing.Point(7, 17)
    Me.cmb_report.Name = "cmb_report"
    Me.cmb_report.SelectedIndex = -1
    Me.cmb_report.Size = New System.Drawing.Size(340, 24)
    Me.cmb_report.SufixText = "Sufix Text"
    Me.cmb_report.SufixTextVisible = True
    Me.cmb_report.TabIndex = 1
    Me.cmb_report.TextCombo = Nothing
    '
    'gb_date
    '
    Me.gb_date.AutoSize = True
    Me.gb_date.Controls.Add(Me.dt_date_to)
    Me.gb_date.Controls.Add(Me.dt_date_from)
    Me.gb_date.Location = New System.Drawing.Point(17, 48)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(330, 99)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "fra_date"
    '
    'dt_date_to
    '
    Me.dt_date_to.Checked = True
    Me.dt_date_to.IsReadOnly = False
    Me.dt_date_to.Location = New System.Drawing.Point(15, 55)
    Me.dt_date_to.Name = "dt_date_to"
    Me.dt_date_to.ShowCheckBox = False
    Me.dt_date_to.ShowUpDown = False
    Me.dt_date_to.Size = New System.Drawing.Size(236, 24)
    Me.dt_date_to.SufixText = "Sufix Text"
    Me.dt_date_to.SufixTextVisible = True
    Me.dt_date_to.TabIndex = 1
    Me.dt_date_to.Value = New Date(2012, 3, 15, 10, 57, 5, 184)
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(15, 20)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = False
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(236, 24)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 0
    Me.dt_date_from.Value = New Date(2012, 3, 15, 10, 57, 5, 200)
    '
    'btn_excel
    '
    Me.btn_excel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_excel.Location = New System.Drawing.Point(151, 155)
    Me.btn_excel.Name = "btn_excel"
    Me.btn_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_excel.TabIndex = 5
    Me.btn_excel.ToolTipped = False
    Me.btn_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_visualize
    '
    Me.btn_visualize.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_visualize.Location = New System.Drawing.Point(256, 155)
    Me.btn_visualize.Name = "btn_visualize"
    Me.btn_visualize.Size = New System.Drawing.Size(90, 30)
    Me.btn_visualize.TabIndex = 6
    Me.btn_visualize.ToolTipped = False
    Me.btn_visualize.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_filename
    '
    Me.lbl_filename.AutoSize = True
    Me.lbl_filename.IsReadOnly = True
    Me.lbl_filename.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_filename.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_filename.Location = New System.Drawing.Point(17, 189)
    Me.lbl_filename.Name = "lbl_filename"
    Me.lbl_filename.Size = New System.Drawing.Size(77, 24)
    Me.lbl_filename.SufixText = "Sufix Text"
    Me.lbl_filename.SufixTextVisible = True
    Me.lbl_filename.TabIndex = 7
    Me.lbl_filename.TabStop = False
    Me.lbl_filename.TextWidth = 0
    Me.lbl_filename.Value = "xFilename:"
    Me.lbl_filename.Visible = False
    '
    'lnk_filename
    '
    Me.lnk_filename.AutoSize = True
    Me.lnk_filename.Location = New System.Drawing.Point(122, 193)
    Me.lnk_filename.Name = "lnk_filename"
    Me.lnk_filename.Size = New System.Drawing.Size(66, 13)
    Me.lnk_filename.TabIndex = 8
    Me.lnk_filename.TabStop = True
    Me.lnk_filename.Text = "xFileName"
    Me.lnk_filename.Visible = False
    '
    'btn_exit
    '
    Me.btn_exit.CausesValidation = False
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(256, 217)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 9
    Me.btn_exit.Tag = ""
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_print
    '
    Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_print.Location = New System.Drawing.Point(46, 155)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_print.TabIndex = 10
    Me.btn_print.ToolTipped = False
    Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'frm_generic_report_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(364, 261)
    Me.Controls.Add(Me.btn_print)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.lbl_filename)
    Me.Controls.Add(Me.lnk_filename)
    Me.Controls.Add(Me.btn_excel)
    Me.Controls.Add(Me.btn_visualize)
    Me.Controls.Add(Me.gb_date)
    Me.Controls.Add(Me.cmb_report)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_generic_report_sel"
    Me.Text = "frm_generic_report_sel"
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents cmb_report As GUI_Controls.uc_combo
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_excel As GUI_Controls.uc_button
  Friend WithEvents btn_visualize As GUI_Controls.uc_button
  Friend WithEvents lbl_filename As GUI_Controls.uc_text_field
  Friend WithEvents lnk_filename As System.Windows.Forms.LinkLabel
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents btn_print As GUI_Controls.uc_button
End Class
