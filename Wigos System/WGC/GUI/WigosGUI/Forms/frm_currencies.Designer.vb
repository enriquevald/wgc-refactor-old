<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_currencies
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.l_remarks_b = New System.Windows.Forms.Label
    Me.gb_bills = New System.Windows.Forms.GroupBox
    Me.dg_bills = New GUI_Controls.uc_grid
    Me.btn_add = New GUI_Controls.uc_button
    Me.btn_del = New GUI_Controls.uc_button
    Me.dg_currencies = New GUI_Controls.uc_grid
    Me.l_remarks_c = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_bills.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.dg_currencies)
    Me.panel_data.Controls.Add(Me.l_remarks_c)
    Me.panel_data.Controls.Add(Me.gb_bills)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Size = New System.Drawing.Size(483, 454)
    '
    'l_remarks_b
    '
    Me.l_remarks_b.BackColor = System.Drawing.SystemColors.Control
    Me.l_remarks_b.Location = New System.Drawing.Point(6, 243)
    Me.l_remarks_b.Name = "l_remarks_b"
    Me.l_remarks_b.Size = New System.Drawing.Size(275, 60)
    Me.l_remarks_b.TabIndex = 3
    Me.l_remarks_b.Text = "xRemarksB"
    '
    'gb_bills
    '
    Me.gb_bills.Controls.Add(Me.dg_bills)
    Me.gb_bills.Controls.Add(Me.btn_add)
    Me.gb_bills.Controls.Add(Me.l_remarks_b)
    Me.gb_bills.Controls.Add(Me.btn_del)
    Me.gb_bills.Location = New System.Drawing.Point(88, 126)
    Me.gb_bills.Name = "gb_bills"
    Me.gb_bills.Size = New System.Drawing.Size(284, 315)
    Me.gb_bills.TabIndex = 2
    Me.gb_bills.TabStop = False
    Me.gb_bills.Text = "xBills"
    '
    'dg_bills
    '
    Me.dg_bills.CurrentCol = -1
    Me.dg_bills.CurrentRow = -1
    Me.dg_bills.Dock = System.Windows.Forms.DockStyle.Top
    Me.dg_bills.Location = New System.Drawing.Point(3, 17)
    Me.dg_bills.Name = "dg_bills"
    Me.dg_bills.PanelRightVisible = False
    Me.dg_bills.Redraw = True
    Me.dg_bills.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_bills.Size = New System.Drawing.Size(278, 181)
    Me.dg_bills.Sortable = False
    Me.dg_bills.SortAscending = True
    Me.dg_bills.SortByCol = 0
    Me.dg_bills.TabIndex = 0
    Me.dg_bills.ToolTipped = True
    Me.dg_bills.TopRow = -2
    '
    'btn_add
    '
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(36, 204)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 1
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_del
    '
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(202, 204)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 2
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'dg_currencies
    '
    Me.dg_currencies.CurrentCol = -1
    Me.dg_currencies.CurrentRow = -1
    Me.dg_currencies.Location = New System.Drawing.Point(3, 3)
    Me.dg_currencies.Name = "dg_currencies"
    Me.dg_currencies.PanelRightVisible = False
    Me.dg_currencies.Redraw = True
    Me.dg_currencies.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_currencies.Size = New System.Drawing.Size(464, 86)
    Me.dg_currencies.Sortable = False
    Me.dg_currencies.SortAscending = True
    Me.dg_currencies.SortByCol = 0
    Me.dg_currencies.TabIndex = 0
    Me.dg_currencies.ToolTipped = True
    Me.dg_currencies.TopRow = -2
    '
    'l_remarks_c
    '
    Me.l_remarks_c.BackColor = System.Drawing.SystemColors.Control
    Me.l_remarks_c.Location = New System.Drawing.Point(5, 94)
    Me.l_remarks_c.Name = "l_remarks_c"
    Me.l_remarks_c.Size = New System.Drawing.Size(451, 27)
    Me.l_remarks_c.TabIndex = 1
    Me.l_remarks_c.Text = "xRemarksC"
    '
    'frm_currencies
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(579, 466)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximumSize = New System.Drawing.Size(585, 490)
    Me.MinimumSize = New System.Drawing.Size(585, 490)
    Me.Name = "frm_currencies"
    Me.Text = "frm_currencies"
    Me.panel_data.ResumeLayout(False)
    Me.gb_bills.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_bills As System.Windows.Forms.GroupBox
  Friend WithEvents dg_bills As GUI_Controls.uc_grid
  Friend WithEvents l_remarks_b As System.Windows.Forms.Label
  Friend WithEvents dg_currencies As GUI_Controls.uc_grid
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents l_remarks_c As System.Windows.Forms.Label
End Class
