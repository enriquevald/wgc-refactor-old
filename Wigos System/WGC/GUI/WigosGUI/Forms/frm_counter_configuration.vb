'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_counter_configuration
' DESCRIPTION:   Configure for counters machine
' AUTHOR:        Samuel Gonz�lez Berdugo
' CREATION DATE: 18-AUG-2014
' 
' REVISION HISTORY:
'
' Date         Author Description 
' -----------  ------ -----------------------------------------------
' 18-AUG-2014  SGB    Initial version
' 30-SEP-2014  DRV    Fixed Bug WIG-1323: Error saving denomination data
' 30-SEP-2014  DRV    Fixed Bug WIG-1328: Multicurrency not supported
' 27-OCT-2014  DRV    Fixed Bug WIG-1574: Unhandled exception defining a limit bigger than 999
' 28-OCT-2014  DRV    Fixed Bug WIG-1575: Denominations should allow modifications. 
' 07-NOV-2014  DRV & SGB    Fixed Bug WIG-1618: Don't control edited denomination value push cancel. 
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports WSI.Common
Imports WSI.Common.NoteScanner

Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl
Imports System.IO.Ports

#End Region                                                 ' Imports

Public Class frm_counter_configuration
  Inherits frm_base_edit

#Region " Constants "

  Private Const SQL_COLUMN_ISO_CODE As Integer = 0
  Private Const SQL_COLUMN_ID As Integer = 1
  Private Const SQL_COLUMN_DENOMINATION As Integer = 2
  Private Const SQL_COLUMN_LIMIT As Integer = 3

  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_DENOMINATION As Integer = 1
  Private Const GRID_COLUMN_DENOMINATION_VALUE As Integer = 2
  Private Const GRID_COLUMN_LIMIT As Integer = 3
  Private Const GRID_COLUMN_INITIAL_ID As Integer = 4
  Private Const GRID_COLUMN_INITIAL_DENOMINATION As Integer = 5
  Private Const GRID_COLUMN_INITIAL_LIMIT As Integer = 6

  Private Const GRID_WIDTH_ID As Integer = 800
  Private Const GRID_WIDTH_DENOMINATION As Integer = 1700
  Private Const GRID_WIDTH_HIDDEN_COLUMN As Integer = 1
  Private Const GRID_WIDTH_LIMIT As Integer = 1500
  Private Const GRID_WIDTH_EDITABLE As Integer = 1

  Private Const GRID_COLUMNS_DENOMINATIONS As Integer = 7
  Private Const GRID_HEADER_ROWS_DENOMINATIONS As Integer = 1

  Private Const GRID_COLUMN_IS_NOT_EDITABLE As Integer = 0
  Private Const GRID_COLUMN_IS_EDITABLE As Integer = 1

#End Region                                             'Constants

#Region " Members "
  Dim m_Denominations As DataTable
  Dim m_ISO_CODE As String
#End Region                                               'Members

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '           - None
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_COUNTER_CONFIGURATION
    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '           - None
  '    - OUTPUT:
  '           - None
  ' RETURNS:
  '           - None
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    '-Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5266)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = Me.Permissions.Write

    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    'Counter machine configuration
    Me.ef_machine.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5268)
    Me.cmb_model.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5269)
    Me.cmb_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5270)
    Me.cmb_communication.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5271)

    'Serial configuration
    Me.cmb_port.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5272)
    Me.cmb_baudrate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5273)
    Me.cmb_stopbits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5274)
    Me.cmb_databits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5275)
    Me.cmb_parity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5276)

    '
    Me.gb_area_conection_serial.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5408)
    Me.gb_table_denomination.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5409)
    Me.gb_counter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5337)

    'Fill combobox values
    Call ModelComboFill()
    Call CurrencyComboFill()
    Call CommunicationTypeComboFill()
    Call PortSerialComboFill()
    Call BaudrateSerialComboFill()
    Call StopbitsSerialComboFill()
    Call DatabitsSerialComboFill()
    Call ParitySerialComboFill()

    Call EnableByModelCounter()

    'Combo default values
    Dim _currency_iso_code As String

    cmb_model.SelectedIndex = NoteCounterModel.None
    cmb_communication.SelectedIndex = NoteCounterCommunication.Serial
    cmb_port.SelectedIndex = NoteCounterPort.COM1
    cmb_baudrate.SelectedIndex = NoteCounterBaudrate.BAUDRATE_9600
    cmb_databits.SelectedIndex = NoteCounterDatabits.DATABITS_8
    cmb_stopbits.SelectedIndex = 0 '"1"
    cmb_parity.SelectedIndex = 2 '"None"

    _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")

    For _idx As Int32 = 0 To cmb_currency.Count - 1
      If cmb_currency.TextValue(_idx) = _currency_iso_code Then
        cmb_currency.SelectedIndex = _idx
        Exit For
      End If
    Next

    Me.m_ISO_CODE = cmb_currency.TextValue
    ' Denomination table
    Call GUI_StyleSheet()

    AddHandler dg_denomination.CellDataChangedEvent, AddressOf CellDataChanged
  End Sub          'GUI_InitControls

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL)
  End Sub       'GUI_SetInitialFocus

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _note_counter_config As CLASS_COUNTER_CONFIGURATION

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_COUNTER_CONFIGURATION()
        _note_counter_config = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2853), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2854), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '           - None
  '    - OUTPUT:
  '           - None
  ' RETURNS:
  '           - None
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _counter_data As CLASS_COUNTER_CONFIGURATION
    Dim _edited_counter_data As CLASS_COUNTER_CONFIGURATION

    Try
      _counter_data = DbReadObject
      _edited_counter_data = DbEditedObject

      ef_machine.Value = Environment.MachineName
      m_Denominations = _edited_counter_data.Denomination

      If _counter_data.Model <> NoteCounterModel.None Then

        cmb_model.SelectedIndex = _counter_data.Model
        cmb_communication.SelectedIndex = _counter_data.CommunicationType
        cmb_port.SelectedIndex = _counter_data.Port
        cmb_baudrate.SelectedIndex = _counter_data.Baudrate
        cmb_stopbits.Value = _counter_data.Stopbits
        cmb_databits.SelectedIndex = _counter_data.Databits
        cmb_parity.Value = _counter_data.Parity

        For _idx As Int32 = 0 To cmb_currency.Count() - 1
          If cmb_currency.TextValue(_idx) = _counter_data.Currency Then
            cmb_currency.SelectedIndex = _idx

            Exit For
          End If
        Next

        'configuration the connection type
        Select Case _counter_data.CommunicationType
          Case WSI.Common.NoteScanner.NoteCounterCommunication.File 'Connection File
            gb_area_conection_serial.Enabled = False
          Case WSI.Common.NoteScanner.NoteCounterCommunication.Serial 'Connection Serial
            gb_area_conection_serial.Enabled = True
          Case Else
            gb_area_conection_serial.Enabled = False
        End Select

        SetTableDenomination()
      End If

    Catch ex As Exception
      Call Log.Exception(ex)

    End Try

  End Sub         'GUI_SetScreenData

  ' PURPOSE: Checks if the entry data is ok
  '         
  ' PARAMS:
  '    - INPUT:
  '           - None
  '    - OUTPUT:
  '           - None
  ' RETURNS:
  '           - None
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Return IsDenominationDataOk()
  End Function   'GUI_IsScreenDataOk

  ' PURPOSE: It gets the values from the entry field and set them to a CLASS_COUNTER_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '           - None
  '    - OUTPUT:
  '           - None
  ' RETURNS:
  '           - None
  Protected Overrides Sub GUI_GetScreenData()

    Dim _counter_data_edit As CLASS_COUNTER_CONFIGURATION

    Try
      Call GetCurrencyDenominations()

      _counter_data_edit = Me.DbEditedObject

      _counter_data_edit.Model = cmb_model.SelectedIndex
      _counter_data_edit.Currency = cmb_currency.TextValue
      _counter_data_edit.CommunicationType = cmb_communication.SelectedIndex
      _counter_data_edit.Port = cmb_port.SelectedIndex
      _counter_data_edit.Baudrate = cmb_baudrate.SelectedIndex
      _counter_data_edit.Stopbits = cmb_stopbits.Value(cmb_stopbits.SelectedIndex)
      _counter_data_edit.Databits = cmb_databits.SelectedIndex
      _counter_data_edit.Parity = cmb_parity.Value(cmb_parity.SelectedIndex)
      _counter_data_edit.Denomination = m_Denominations

    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

  End Sub         'GUI_GetScreenData

  ' PURPOSE: Ignore [ENTER] key when editing details
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  ' RETURNS:
  '           - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_denomination.ContainsFocus Then
      Return Me.dg_denomination.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function 'GUI_KeyPress

#End Region 'Overrides

#Region "Private Functions"

  ' PURPOSE: Define all Grid Points Multiplier Per denomination Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.dg_denomination
      Call .Init(GRID_COLUMNS_DENOMINATIONS, GRID_HEADER_ROWS_DENOMINATIONS, True)

      ' Id
      .Column(GRID_COLUMN_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5277)
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_ID).IsMerged = False
      .Column(GRID_COLUMN_ID).Editable = True
      .Column(GRID_COLUMN_ID).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_ID).EditionControl.EntryField.SetFilter(FORMAT_ALPHA, 1)

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5278)
      .Column(GRID_COLUMN_DENOMINATION).Width = GRID_WIDTH_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DENOMINATION).IsMerged = False
      .Column(GRID_COLUMN_DENOMINATION).Editable = True
      .Column(GRID_COLUMN_DENOMINATION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DENOMINATION).EditionControl.EntryField.SetFilter(FORMAT_MONEY_ALSO_NEGATIVES, 9, 2)

      ' Denomination
      .Column(GRID_COLUMN_DENOMINATION_VALUE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5278)
      .Column(GRID_COLUMN_DENOMINATION_VALUE).Width = GRID_WIDTH_HIDDEN_COLUMN
      .Column(GRID_COLUMN_DENOMINATION_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DENOMINATION_VALUE).IsMerged = False
      .Column(GRID_COLUMN_DENOMINATION_VALUE).Editable = False

      ' Limit
      .Column(GRID_COLUMN_LIMIT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5279)
      .Column(GRID_COLUMN_LIMIT).Width = GRID_WIDTH_LIMIT
      .Column(GRID_COLUMN_LIMIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_LIMIT).IsMerged = False
      .Column(GRID_COLUMN_LIMIT).Editable = True
      .Column(GRID_COLUMN_LIMIT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_LIMIT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7)

      ' Initial Id
      .Column(GRID_COLUMN_INITIAL_ID).Width = GRID_WIDTH_HIDDEN_COLUMN
      .Column(GRID_COLUMN_INITIAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_INITIAL_ID).IsMerged = False
      .Column(GRID_COLUMN_INITIAL_ID).Editable = False

      ' Initial Denomination
      .Column(GRID_COLUMN_INITIAL_DENOMINATION).Width = GRID_WIDTH_HIDDEN_COLUMN
      .Column(GRID_COLUMN_INITIAL_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_INITIAL_DENOMINATION).IsMerged = False
      .Column(GRID_COLUMN_INITIAL_DENOMINATION).Editable = False

      ' Initial Limit
      .Column(GRID_COLUMN_INITIAL_LIMIT).Width = GRID_WIDTH_HIDDEN_COLUMN
      .Column(GRID_COLUMN_INITIAL_LIMIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_INITIAL_LIMIT).IsMerged = False
      .Column(GRID_COLUMN_INITIAL_LIMIT).Editable = False

      '' Edition Enabled
      '.Column(GRID_COLUMN_EDITABLE).Header.Text = ""
      '.Column(GRID_COLUMN_EDITABLE).Width = GRID_WIDTH_EDITABLE
      '.Column(GRID_COLUMN_EDITABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      '.Column(GRID_COLUMN_EDITABLE).IsMerged = False
      '.Column(GRID_COLUMN_EDITABLE).Editable = False

    End With

  End Sub                        'GUI_StyleSheet
  Private Function IsDenominationDataOk() As Boolean
    Dim _idx_compared_row As Integer
    Dim _dr() As DataRow
    _idx_compared_row = 0

    Call Me.GetCurrencyDenominations()

    If m_Denominations Is Nothing Then

      Return True
    End If

    _dr = m_Denominations.Select("", "", DataViewRowState.CurrentRows)

    For _idx As Int32 = 0 To _dr.Length - 1
      'check if, id or denomination, is not repeat
      For _idx_compared_row = _idx + 1 To _dr.Length - 1

        If _dr(_idx)(SQL_COLUMN_ISO_CODE) = _dr(_idx_compared_row)(SQL_COLUMN_ISO_CODE) And _
          (_dr(_idx)(SQL_COLUMN_ID) = _dr(_idx_compared_row)(SQL_COLUMN_ID) _
          OrElse _dr(_idx)(SQL_COLUMN_DENOMINATION) = _dr(_idx_compared_row)(SQL_COLUMN_DENOMINATION)) Then

          Call dg_denomination.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return False
        End If

      Next

      'check is id in range [a-g]
      If (_dr(_idx)(SQL_COLUMN_ID) <> "a" _
        And _dr(_idx)(SQL_COLUMN_ID) <> "b" _
        And _dr(_idx)(SQL_COLUMN_ID) <> "c" _
        And _dr(_idx)(SQL_COLUMN_ID) <> "d" _
        And _dr(_idx)(SQL_COLUMN_ID) <> "e" _
        And _dr(_idx)(SQL_COLUMN_ID) <> "f" _
        And _dr(_idx)(SQL_COLUMN_ID) <> "g") Then

        Call dg_denomination.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1305), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5277), "a", "g")

        Return False
      End If

      'check denomination is > 0
      If _dr(_idx)(SQL_COLUMN_DENOMINATION) <= 0 Then

        Call dg_denomination.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5335), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5278), "0")

        Return False
      End If

      'check if limit is > 0 and <= 100
      If _dr(_idx)(SQL_COLUMN_LIMIT) < 0 Or _dr(_idx)(SQL_COLUMN_LIMIT) > 100 Then

        Call dg_denomination.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(5323), "0", "100")

        Return False
      End If
    Next



    Return True
  End Function



  Private Sub CellDataChanged(ByVal Row As Integer, ByVal Column As Integer)
    Dim _currency_exchange As CurrencyExchangeProperties

    _currency_exchange = CurrencyExchangeProperties.GetProperties(cmb_currency.TextValue)

    RemoveHandler dg_denomination.CellDataChangedEvent, AddressOf CellDataChanged

    If Column = GRID_COLUMN_DENOMINATION Then
      dg_denomination.Cell(Row, GRID_COLUMN_DENOMINATION_VALUE).Value = _dg_denomination.Cell(Row, GRID_COLUMN_DENOMINATION).Value
      dg_denomination.Cell(Row, GRID_COLUMN_DENOMINATION).Value = _currency_exchange.FormatCurrency(dg_denomination.Cell(Row, GRID_COLUMN_DENOMINATION).Value)
    End If

    AddHandler dg_denomination.CellDataChangedEvent, AddressOf CellDataChanged

  End Sub

  ' PURPOSE : Fill combo to posibles machine model
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub ModelComboFill()

    cmb_model.Add(NoteCounterModel.None, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5439)) 'None
    cmb_model.Add(NoteCounterModel.JetScan4091, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5306)) 'JetScan4091

  End Sub                        'ModelComboFill

  ' PURPOSE : Fill combo with possible communications
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub CommunicationTypeComboFill()
    cmb_communication.Add(WSI.Common.NoteScanner.NoteCounterCommunication.Serial, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5308)) 'Serial
#If DEBUG Then
    cmb_communication.Add(WSI.Common.NoteScanner.NoteCounterCommunication.File, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5307)) 'File
#End If

  End Sub            'CommunicationTypeComboFill

  ' PURPOSE : Fill combo to posibles port of configuration serial
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub PortSerialComboFill()
    cmb_port.Add(NoteCounterPort.COM1, NoteCounterPort.COM1.ToString())
    cmb_port.Add(NoteCounterPort.COM2, NoteCounterPort.COM2.ToString())
    cmb_port.Add(NoteCounterPort.COM3, NoteCounterPort.COM3.ToString())
    cmb_port.Add(NoteCounterPort.COM4, NoteCounterPort.COM4.ToString())
    cmb_port.Add(NoteCounterPort.COM5, NoteCounterPort.COM5.ToString())
    cmb_port.Add(NoteCounterPort.COM6, NoteCounterPort.COM6.ToString())
    cmb_port.Add(NoteCounterPort.COM7, NoteCounterPort.COM7.ToString())
    cmb_port.Add(NoteCounterPort.COM8, NoteCounterPort.COM8.ToString())
    cmb_port.Add(NoteCounterPort.COM9, NoteCounterPort.COM9.ToString())
    cmb_port.Add(NoteCounterPort.COM10, NoteCounterPort.COM10.ToString())
    cmb_port.Add(NoteCounterPort.COM11, NoteCounterPort.COM11.ToString())
    cmb_port.Add(NoteCounterPort.COM12, NoteCounterPort.COM12.ToString())
    cmb_port.Add(NoteCounterPort.COM13, NoteCounterPort.COM13.ToString())
    cmb_port.Add(NoteCounterPort.COM14, NoteCounterPort.COM14.ToString())
    cmb_port.Add(NoteCounterPort.COM15, NoteCounterPort.COM15.ToString())
    cmb_port.Add(NoteCounterPort.COM16, NoteCounterPort.COM16.ToString())
    cmb_port.Add(NoteCounterPort.COM17, NoteCounterPort.COM17.ToString())
    cmb_port.Add(NoteCounterPort.COM18, NoteCounterPort.COM18.ToString())
    cmb_port.Add(NoteCounterPort.COM19, NoteCounterPort.COM19.ToString())
    cmb_port.Add(NoteCounterPort.COM20, NoteCounterPort.COM20.ToString())

  End Sub                   'PortSerialComboFill

  ' PURPOSE : Fill combo to posibles baudrate of serial configuration
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub BaudrateSerialComboFill()
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_75, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_75))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_110, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_110))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_134, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_134))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_150, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_150))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_300, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_300))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_600, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_600))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_1200, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_1200))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_1800, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_1800))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_2400, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_2400))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_4800, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_4800))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_7200, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_7200))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_9600, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_9600))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_14400, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_14400))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_19200, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_19200))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_38400, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_38400))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_57600, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_57600))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_115200, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_115200))
    cmb_baudrate.Add(NoteCounterBaudrate.BAUDRATE_128000, CommonScannerFunctions.ReturnStringBaudrate(NoteCounterBaudrate.BAUDRATE_128000))
  End Sub 'BaudrateSerialComboFill


  ' PURPOSE : Fill combo with posible stopbits of serial configuration
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub StopbitsSerialComboFill()

    cmb_stopbits.Add(StopBits.One, "1")
    cmb_stopbits.Add(StopBits.OnePointFive, "1.5")
    cmb_stopbits.Add(StopBits.Two, "2")

  End Sub 'StopbitsSerialComboFill

  ' PURPOSE : Fill combo to posibles databits of configuration serial
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub DatabitsSerialComboFill()

    cmb_databits.Add(NoteCounterDatabits.DATABITS_5, CommonScannerFunctions.ReturnStringDatabits(NoteCounterDatabits.DATABITS_5))
    cmb_databits.Add(NoteCounterDatabits.DATABITS_6, CommonScannerFunctions.ReturnStringDatabits(NoteCounterDatabits.DATABITS_6))
    cmb_databits.Add(NoteCounterDatabits.DATABITS_7, CommonScannerFunctions.ReturnStringDatabits(NoteCounterDatabits.DATABITS_7))
    cmb_databits.Add(NoteCounterDatabits.DATABITS_8, CommonScannerFunctions.ReturnStringDatabits(NoteCounterDatabits.DATABITS_8))

  End Sub 'DatabitsSerialComboFill

  ' PURPOSE : Fill combo with posible parity serial configuration
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub ParitySerialComboFill()
    cmb_parity.Add(Parity.Even, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5329))
    cmb_parity.Add(Parity.Odd, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5330))
    cmb_parity.Add(Parity.None, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5331))
    cmb_parity.Add(Parity.Mark, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5332))
    cmb_parity.Add(Parity.Space, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5333))
  End Sub 'ParitySerialComboFill

  ' PURPOSE : Fill combo with accepted currencies
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub CurrencyComboFill()

    Dim _currencies_accepted As String
    Dim _list_currencies_accepted() As String = Nothing
    Dim _split_delimiters() As Char

    _currencies_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted")

    _split_delimiters = New Char() {";"}
    _list_currencies_accepted = _currencies_accepted.Split(_split_delimiters, StringSplitOptions.RemoveEmptyEntries)

    'Fill comboBox with CurrenciesAccepted (Chips are not allowed)
    For _idx As Integer = 0 To _list_currencies_accepted.Length - 1
      If _list_currencies_accepted(_idx) <> "X01" Then

        cmb_currency.Add(_list_currencies_accepted(_idx))
      End If
    Next

  End Sub 'CurrencyComboFill

  ' PURPOSE : Sets denominations in datagrid from DataBase
  '
  '  PARAMS :
  '	- INPUT :
  '           - DataSet
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub SetTableDenomination()
    Dim _dr() As DataRow
    Dim _currency_exchange As CurrencyExchangeProperties
    Dim _txt As String

    _currency_exchange = CurrencyExchangeProperties.GetProperties(cmb_currency.TextValue)

    dg_denomination.Redraw = False
    dg_denomination.Clear()

    Try
      If m_Denominations Is Nothing Then

        Return
      End If
      If m_Denominations.Rows.Count > 0 Then
        _dr = m_Denominations.Select("ISO_CODE = '" & cmb_currency.TextValue & "'")

        For _idx As Int32 = 0 To _dr.Length - 1

          dg_denomination.AddRow()

          'Id
          If (_dr(_idx).IsNull(SQL_COLUMN_ID)) Then
            dg_denomination.Cell(_idx, GRID_COLUMN_ID).Value = ""
            dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_ID).Value = ""
          Else
            dg_denomination.Cell(_idx, GRID_COLUMN_ID).Value = _dr(_idx)(SQL_COLUMN_ID)
            If _dr(_idx).RowState <> DataRowState.Added Then
              dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_ID).Value = _dr(_idx)(SQL_COLUMN_ID, DataRowVersion.Original)
            Else
              dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_ID).Value = _dr(_idx)(SQL_COLUMN_ID)
            End If

          End If

          'Denomination
          If (_dr(_idx).IsNull(SQL_COLUMN_DENOMINATION)) Then
            dg_denomination.Cell(_idx, GRID_COLUMN_DENOMINATION).Value = ""
            dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_DENOMINATION).Value = ""
          Else

            If Not _currency_exchange Is Nothing Then
              _txt = _currency_exchange.FormatCurrency(_dr(_idx)(SQL_COLUMN_DENOMINATION))
              dg_denomination.Cell(_idx, GRID_COLUMN_DENOMINATION).Value = _txt
              dg_denomination.Cell(_idx, GRID_COLUMN_DENOMINATION_VALUE).Value = _dr(_idx)(SQL_COLUMN_DENOMINATION)
              If _dr(_idx).RowState <> DataRowState.Added Then
                dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_DENOMINATION).Value = _dr(_idx)(SQL_COLUMN_DENOMINATION, DataRowVersion.Original)
              Else
                dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_DENOMINATION).Value = _dr(_idx)(SQL_COLUMN_DENOMINATION)
              End If

            End If
            End If

            'Limit
            If (_dr(_idx).IsNull(SQL_COLUMN_LIMIT)) Then
              dg_denomination.Cell(_idx, GRID_COLUMN_LIMIT).Value = ""
              dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_LIMIT).Value = ""
            Else
            dg_denomination.Cell(_idx, GRID_COLUMN_LIMIT).Value = GUI_FormatNumber(_dr(_idx)(SQL_COLUMN_LIMIT), 0)
            If _dr(_idx).RowState <> DataRowState.Added Then
              dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_LIMIT).Value = GUI_FormatNumber(_dr(_idx)(SQL_COLUMN_LIMIT, DataRowVersion.Original), 0)
            Else
              dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_LIMIT).Value = GUI_FormatNumber(_dr(_idx)(SQL_COLUMN_LIMIT), 0)
            End If

          End If
        Next

      End If
    Finally
      dg_denomination.Redraw = True
    End Try


  End Sub                  'SetTableDenomination

  ' PURPOSE : Insert new denomination from datagrid dg_denomination
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub AddDenominationRow()
    Dim _idx_row As Integer
    Dim _cls_counter As CLASS_COUNTER_CONFIGURATION
    Dim _row As DataRow

    dg_denomination.AddRow()
    _idx_row = dg_denomination.NumRows - 1
    dg_denomination.Cell(_idx_row, GRID_COLUMN_ID).Value = "a"
    dg_denomination.Cell(_idx_row, GRID_COLUMN_DENOMINATION).Value = 0
    dg_denomination.Cell(_idx_row, GRID_COLUMN_DENOMINATION_VALUE).Value = 0
    dg_denomination.Cell(_idx_row, GRID_COLUMN_LIMIT).Value = 0
    dg_denomination.Cell(_idx_row, GRID_COLUMN_INITIAL_ID).Value = "a"
    dg_denomination.Cell(_idx_row, GRID_COLUMN_INITIAL_DENOMINATION).Value = 0
    dg_denomination.Cell(_idx_row, GRID_COLUMN_INITIAL_LIMIT).Value = 0

    _cls_counter = DbEditedObject

    _row = m_Denominations.NewRow()
    _row("ISO_CODE") = cmb_currency.TextValue
    _row("CurrencyId") = "a"
    _row("Denomination") = 0
    _row("Limit") = 0
    m_Denominations.Rows.Add(_row)
  End Sub 'AddDenominationRow

  ' PURPOSE : Delete a selected denomination from datagrid dg_denominations
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub RemoveDenominationRow()
    Dim _idx_row As Integer = 0
    Dim _edited As CLASS_COUNTER_CONFIGURATION
    Dim _rows() As DataRow

    If IsNothing(Me.dg_denomination.SelectedRows) Then
      Exit Sub
    End If

    _edited = DbEditedObject

    _rows = m_Denominations.Select("ISO_CODE = '" & m_ISO_CODE & "'", "", DataViewRowState.Added)
    For Each _row As DataRow In _rows
      _row("CurrencyId") = "a"
      _row("Denomination") = 0
      _row("Limit") = 0
      _row("ISO_CODE") = m_ISO_CODE
    Next

    While _idx_row < Me.dg_denomination.NumRows
      If Me.dg_denomination.Row(_idx_row).IsSelected Then

        _rows = _edited.Denomination.Select("CurrencyId = '" & Me.dg_denomination.Cell(_idx_row, GRID_COLUMN_INITIAL_ID).Value & "' AND " & _
                                           "Denomination = " & GUI_LocalNumberToDBNumber(Me.dg_denomination.Cell(_idx_row, GRID_COLUMN_INITIAL_DENOMINATION).Value) & " AND " & _
                                           "Limit = " & GUI_LocalNumberToDBNumber(Me.dg_denomination.Cell(_idx_row, GRID_COLUMN_INITIAL_LIMIT).Value) & " AND " & _
                                           "ISO_CODE = '" & cmb_currency.TextValue & "'")
        If _rows.Length > 0 Then
          _rows(0).Delete()
        Else
          'Row had been added and not saved to DB yet
          _rows = _edited.Denomination.Select("CurrencyId = 'a' AND Denomination = " & GUI_LocalNumberToDBNumber("0") & " AND Limit = 0")
          If _rows.Length > 0 Then
            _rows(0).Delete()
          End If
        End If
        Me.dg_denomination.Row(_idx_row).IsSelected = False
        Me.dg_denomination.DeleteRowFast(_idx_row)
      Else

        _idx_row += 1
      End If

    End While


  End Sub                    'DeleteDenomination

  ' PURPOSE : Enable selected model from cmb_model
  '
  '  PARAMS :
  '	- INPUT :
  '           - None
  '	- OUTPUT :
  '           - None
  ' RETURNS :
  Private Sub EnableByModelCounter()

    Select Case cmb_model.SelectedIndex
      Case NoteCounterModel.None
        gb_area_conection_serial.Enabled = False
        cmb_port.Enabled = False
        cmb_baudrate.Enabled = False
        cmb_stopbits.Enabled = False
        cmb_databits.Enabled = False
        cmb_parity.Enabled = False
        gb_table_denomination.Enabled = False
        cmb_currency.Enabled = False
        cmb_communication.Enabled = False

      Case NoteCounterModel.JetScan4091
        gb_area_conection_serial.Enabled = True
        cmb_port.Enabled = True
        cmb_baudrate.Enabled = True
        cmb_stopbits.Enabled = True
        cmb_databits.Enabled = True
        cmb_parity.Enabled = True
        gb_table_denomination.Enabled = True
        cmb_currency.Enabled = True
        cmb_communication.Enabled = True

      Case Else
        gb_area_conection_serial.Enabled = True
        cmb_port.Enabled = True
        cmb_baudrate.Enabled = True
        cmb_stopbits.Enabled = True
        cmb_databits.Enabled = True
        cmb_parity.Enabled = True
        gb_table_denomination.Enabled = True
        cmb_currency.Enabled = True
        cmb_communication.Enabled = True

    End Select

  End Sub

  Private Sub GetCurrencyDenominations()
    Dim _currencies() As DataRow
    If m_Denominations Is Nothing Then

      Return
    End If

    _currencies = m_Denominations.Select("ISO_CODE = '" & m_ISO_CODE & "'", "", DataViewRowState.Added)
    For Each _row As DataRow In _currencies
      _row("CurrencyId") = "a"
      _row("Denomination") = 0
      _row("Limit") = 0
      _row("ISO_CODE") = m_ISO_CODE
    Next

    For _idx As Integer = 0 To dg_denomination.NumRows - 1
      _currencies = m_Denominations.Select("CurrencyId = '" & dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_ID).Value & "'" & _
                                           " AND Denomination = " & GUI_LocalNumberToDBNumber(CDec(dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_DENOMINATION).Value)) & _
                                           " AND Limit = " & GUI_LocalNumberToDBNumber(dg_denomination.Cell(_idx, GRID_COLUMN_INITIAL_LIMIT).Value) & _
                                           " AND ISO_CODE = '" & m_ISO_CODE & "'", "", DataViewRowState.OriginalRows)
      If _currencies.Length = 0 Then
        _currencies = m_Denominations.Select("CurrencyId = 'a' AND Denomination = 0 AND Limit = 0 AND ISO_CODE = '" & m_ISO_CODE & "'", "", DataViewRowState.Added)
      End If
      If _currencies.Length > 0 Then
        If _currencies(0)("CurrencyId") <> dg_denomination.Cell(_idx, GRID_COLUMN_ID).Value OrElse _
           _currencies(0)("Denomination") <> CDec(dg_denomination.Cell(_idx, GRID_COLUMN_DENOMINATION_VALUE).Value) OrElse _
           _currencies(0)("Limit") <> CInt(dg_denomination.Cell(_idx, GRID_COLUMN_LIMIT).Value) Then

          _currencies(0)("CurrencyId") = dg_denomination.Cell(_idx, GRID_COLUMN_ID).Value
          _currencies(0)("Denomination") = CDec(dg_denomination.Cell(_idx, GRID_COLUMN_DENOMINATION_VALUE).Value)
          _currencies(0)("Limit") = CInt(dg_denomination.Cell(_idx, GRID_COLUMN_LIMIT).Value)
          _currencies(0)("ISO_CODE") = m_ISO_CODE
        End If
      End If

    Next
  End Sub


#End Region                                       'Private functions

#Region "Public Functions"

#End Region                                        'Public Functions

#Region "Events"
  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
    AddDenominationRow()
  End Sub

  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
    RemoveDenominationRow()
  End Sub

  Private Sub cmb_communication_ValueChangedEvent() Handles cmb_communication.ValueChangedEvent
    'configuration the connection type
    Select Case cmb_communication.SelectedIndex
      Case WSI.Common.NoteScanner.NoteCounterCommunication.File 'Connection File
        gb_area_conection_serial.Enabled = False
      Case WSI.Common.NoteScanner.NoteCounterCommunication.Serial 'Connection Serial
        gb_area_conection_serial.Enabled = True
      Case Else
        gb_area_conection_serial.Enabled = False
    End Select
  End Sub

  Private Sub cmb_currency_ValueChangedEvent() Handles cmb_currency.ValueChangedEvent
    Call Me.GetCurrencyDenominations()
    m_ISO_CODE = cmb_currency.TextValue
    dg_denomination.Clear()
    Call Me.SetTableDenomination()

  End Sub

  Private Sub cmb_model_ValueChangedEvent() Handles cmb_model.ValueChangedEvent
    EnableByModelCounter()
  End Sub

#End Region                                                  'Events


  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub


End Class