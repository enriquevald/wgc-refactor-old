<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_area_bank_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_zone = New System.Windows.Forms.GroupBox
    Me.chk_noSmoking = New System.Windows.Forms.CheckBox
    Me.chk_smoking = New System.Windows.Forms.CheckBox
    Me.ef_area_name = New GUI_Controls.uc_entry_field
    Me.ef_bank_name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_zone.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_bank_name)
    Me.panel_filter.Controls.Add(Me.ef_area_name)
    Me.panel_filter.Controls.Add(Me.gb_zone)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(805, 77)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_zone, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_area_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_bank_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 81)
    Me.panel_data.Size = New System.Drawing.Size(805, 517)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(799, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(799, 4)
    '
    'gb_zone
    '
    Me.gb_zone.Controls.Add(Me.chk_noSmoking)
    Me.gb_zone.Controls.Add(Me.chk_smoking)
    Me.gb_zone.Location = New System.Drawing.Point(292, 10)
    Me.gb_zone.Name = "gb_zone"
    Me.gb_zone.Size = New System.Drawing.Size(170, 57)
    Me.gb_zone.TabIndex = 2
    Me.gb_zone.TabStop = False
    Me.gb_zone.Text = "xZone"
    '
    'chk_noSmoking
    '
    Me.chk_noSmoking.Location = New System.Drawing.Point(11, 34)
    Me.chk_noSmoking.Name = "chk_noSmoking"
    Me.chk_noSmoking.Size = New System.Drawing.Size(123, 17)
    Me.chk_noSmoking.TabIndex = 1
    Me.chk_noSmoking.Text = "xNoSmoking"
    '
    'chk_smoking
    '
    Me.chk_smoking.Location = New System.Drawing.Point(11, 18)
    Me.chk_smoking.Name = "chk_smoking"
    Me.chk_smoking.Size = New System.Drawing.Size(109, 16)
    Me.chk_smoking.TabIndex = 0
    Me.chk_smoking.Text = "xSmoking"
    '
    'ef_area_name
    '
    Me.ef_area_name.DoubleValue = 0
    Me.ef_area_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_area_name.IntegerValue = 0
    Me.ef_area_name.IsReadOnly = False
    Me.ef_area_name.Location = New System.Drawing.Point(3, 6)
    Me.ef_area_name.Name = "ef_area_name"
    Me.ef_area_name.Size = New System.Drawing.Size(250, 25)
    Me.ef_area_name.SufixText = "Sufix Text"
    Me.ef_area_name.SufixTextVisible = True
    Me.ef_area_name.TabIndex = 0
    Me.ef_area_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_area_name.TextValue = ""
    Me.ef_area_name.TextWidth = 70
    Me.ef_area_name.Value = ""
    '
    'ef_bank_name
    '
    Me.ef_bank_name.DoubleValue = 0
    Me.ef_bank_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_bank_name.IntegerValue = 0
    Me.ef_bank_name.IsReadOnly = False
    Me.ef_bank_name.Location = New System.Drawing.Point(3, 37)
    Me.ef_bank_name.Name = "ef_bank_name"
    Me.ef_bank_name.Size = New System.Drawing.Size(250, 25)
    Me.ef_bank_name.SufixText = "Sufix Text"
    Me.ef_bank_name.SufixTextVisible = True
    Me.ef_bank_name.TabIndex = 1
    Me.ef_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_bank_name.TextValue = ""
    Me.ef_bank_name.TextWidth = 70
    Me.ef_bank_name.Value = ""
    '
    'frm_area_bank_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(815, 602)
    Me.Name = "frm_area_bank_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_area_bank_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_zone.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_zone As System.Windows.Forms.GroupBox
  Friend WithEvents chk_noSmoking As System.Windows.Forms.CheckBox
  Friend WithEvents chk_smoking As System.Windows.Forms.CheckBox
  Friend WithEvents ef_area_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_bank_name As GUI_Controls.uc_entry_field
End Class
