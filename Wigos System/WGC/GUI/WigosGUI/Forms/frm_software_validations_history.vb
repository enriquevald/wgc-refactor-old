'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_software_validations_history
' DESCRIPTION:   This form allows you to view the status of Software validations
'                   
' AUTHOR:        David Rigal Vall
' CREATION DATE: 07-ABR-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-ABR-2014  DRV    Initial version
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Text

Public Class frm_software_validations_history
  Inherits frm_base_sel

#Region " Constants "

  Private Const SQL_COLUMN_SV_ID As Integer = 0
  Private Const SQL_COLUMN_STATUS As Integer = 1
  Private Const SQL_COLUMN_INSERTED_DATE As Integer = 2
  Private Const SQL_COLUMN_LAST_REQUEST_DATE As Integer = 3
  Private Const SQL_COLUMN_RECEIVED_DATE As Integer = 4
  Private Const SQL_COLUMN_LKT_STATUS As Integer = 5
  Private Const SQL_COLUMN_EXPECTED_SIGNATURE As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 7
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 8

  ' Indicates if Counters are visible or not.
  Private Const COUNTERS_VISIBLE As Boolean = False

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_status As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SOFTWARE_VALIDATION_HISTORY

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4815)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_dates.Text = GLB_NLS_GUI_CONTROLS.GetString(338)
    Me.dtp_created_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    Me.dtp_created_to.Text = GLB_NLS_GUI_STATISTICS.GetString(310)
    Me.dtp_created_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_created_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)

    ' Status Checks
    Me.chk_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4839)
    Me.chk_validation_ok.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.ConfirmedOK)
    Me.chk_validation_error.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.ConfirmedError)
    Me.chk_canceled_or_timeout.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.CanceledTimeout)
    Me.chk_error.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.Error)
    Me.chk_unknown.Text = SoftwareValidationStatus_StatusText(SoftwareValidationStatus.Unknown)

    ' Color Status Labels
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.Pending, fore_color, back_color)
    Me.lbl_pending.BackColor = back_color
    Me.lbl_pending.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(SoftwareValidationStatus.ConfirmedOK, fore_color, back_color)
    Me.lbl_validation_ok.BackColor = back_color
    Me.lbl_validation_ok.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(SoftwareValidationStatus.ConfirmedError, fore_color, back_color)
    Me.lbl_validation_error.BackColor = back_color
    Me.lbl_validation_error.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.CanceledTimeout, fore_color, back_color)
    Me.lbl_caneled_or_timeout.BackColor = back_color
    Me.lbl_caneled_or_timeout.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.Error, fore_color, back_color)
    Me.lbl_error.BackColor = back_color
    Me.lbl_error.ForeColor = fore_color
    SoftwareValidationStatus_GetUpdateStatusColor(WSI.Common.SoftwareValidationStatus.Unknown, fore_color, back_color)
    Me.lbl_unknown.BackColor = back_color
    Me.lbl_unknown.ForeColor = fore_color

    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    Call Me.uc_pr_list.Init(WSI.Common.Misc.WCPTerminalTypes())

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset



  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_created_from.Checked And Me.dtp_created_to.Checked Then
      If Me.dtp_created_from.Value > Me.dtp_created_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_created_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As stringbuilder

    ' Get Select and from
    _str_sql = New StringBuilder()
    _str_sql.AppendLine("SELECT   SVAL_VALIDATION_ID                ")
    _str_sql.AppendLine("       , SVAL_STATUS                       ")
    _str_sql.AppendLine("       , SVAL_INSERTED                     ")
    _str_sql.AppendLine("       , SVAL_LAST_REQUEST                 ")
    _str_sql.AppendLine("       , SVAL_RECEIVED_DATETIME            ")
    _str_sql.AppendLine("       , SVAL_RECEIVED_STATUS              ")
    _str_sql.AppendLine("       , SVAL_EXPECTED_SIGNATURE           ")
    _str_sql.AppendLine("       , TE_NAME                           ")
    _str_sql.AppendLine("       , TE_PROVIDER_ID                    ")
    _str_sql.AppendLine("  FROM   SOFTWARE_VALIDATIONS, TERMINALS   ")
    _str_sql.AppendLine(" WHERE   TE_TERMINAL_ID = SVAL_TERMINAL_ID ")
    _str_sql.AppendLine(GetSqlWhere())
    _str_sql.AppendLine("ORDER BY SVAL_VALIDATION_ID                ")

    Return _str_sql.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _item As TYPE_SV_ITEM

    _item.id = DbRow.Value(SQL_COLUMN_SV_ID)
    _item.status = DbRow.Value(SQL_COLUMN_STATUS)

    If Not DbRow.IsNull(SQL_COLUMN_INSERTED_DATE) Then
      _item.inserted_datetime = DbRow.Value(SQL_COLUMN_INSERTED_DATE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_LAST_REQUEST_DATE) Then
      _item.last_request = DbRow.Value(SQL_COLUMN_LAST_REQUEST_DATE)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_RECEIVED_DATE) Then
      _item.received = DbRow.Value(SQL_COLUMN_RECEIVED_DATE)
    End If

    If DbRow.IsNull(SQL_COLUMN_PROVIDER_NAME) Then
      _item.provider_name = ""
    Else
      _item.provider_name = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)
    End If

    _item.terminal_name = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)

    If DbRow.IsNull(SQL_COLUMN_LKT_STATUS) Then
      _item.sas_host_error = SoftwareValidationStatusLKT.UNKNOWN
    Else
      _item.sas_host_error = DbRow.Value(SQL_COLUMN_LKT_STATUS)
    End If

    If DbRow.IsNull(SQL_COLUMN_EXPECTED_SIGNATURE) Then
      _item.expected_signature = ""
    Else
      _item.expected_signature = DbRow.Value(SQL_COLUMN_EXPECTED_SIGNATURE)
    End If
    If Not SoftwareValidationStatus_SetupRow(Me.Grid, RowIndex, _item) Then

      Return False
    End If

    Return True
  End Function ' GUI_SetupRow 

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_created_from

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(261), m_status)

    PrintData.FilterValueWidth(2) = 10000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim no_one As Boolean
    Dim all As Boolean

    m_status = ""
    m_terminal = ""
    m_date_from = ""
    m_date_to = ""

    ' Date 
    If Me.dtp_created_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_created_from.Value, _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                    ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_created_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_created_to.Value, _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                  ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Terminals 
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' Status
    no_one = True
    all = True
    If Me.chk_pending.Checked Then
      m_status = m_status & Me.chk_pending.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_validation_ok.Checked Then
      m_status = m_status & Me.chk_validation_ok.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_validation_error.Checked Then
      m_status = m_status & Me.chk_validation_error.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_canceled_or_timeout.Checked Then
      m_status = m_status & Me.chk_canceled_or_timeout.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_error.Checked Then
      m_status = m_status & Me.chk_error.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If Me.chk_unknown.Checked Then
      m_status = m_status & Me.chk_unknown.Text & ", "
      no_one = False
    Else
      all = False
    End If

    If m_status.Length <> 0 Then
      m_status = Strings.Left(m_status, Len(m_status) - 2)
    End If

    If no_one Or all Then
      m_status = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, ByVal ColIndex As Integer, ByRef ToolTipTxt As String)
    ToolTipTxt = SoftwareValidationStatus_GetToolTipText(Me.Grid, RowIndex, ColIndex)
  End Sub

#End Region ' GUI Reports

#End Region  ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    SoftwareValidationStatus_StyleSheet(Me.Grid, COUNTERS_VISIBLE)
    Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_created_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)
    Me.dtp_created_from.Checked = True

    Me.dtp_created_to.Value = Me.dtp_created_from.Value
    Me.dtp_created_to.Checked = False

    Call btn_check_all_Click()

    Call Me.uc_pr_list.SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_list As String

    ' Filter Created Dates
    If Me.dtp_created_from.Checked = True Then
      str_where = str_where & " AND SVAL_INSERTED >= " & GUI_FormatDateDB(dtp_created_from.Value)
    End If

    If Me.dtp_created_to.Checked = True Then
      str_where = str_where & " AND SVAL_INSERTED < " & GUI_FormatDateDB(dtp_created_to.Value)
    End If

    ' Filter Providers - Terminals
    str_where = str_where & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Filter Status
    str_list = GetStatusIdListSelected()
    If str_list <> "" Then
      str_where = str_where & " AND SVAL_STATUS IN (" & str_list & ") "
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Get Status Id list for selected status
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetStatusIdListSelected() As String

    Dim list_type As String = ""

    If Me.chk_pending.Checked Then
      list_type = SoftwareValidationStatus.Pending & ", " & SoftwareValidationStatus.Notified & ", " & SoftwareValidationStatus.ConfirmedNonValidated
    End If

    If Me.chk_validation_ok.Checked Then
      If list_type.Length = 0 Then
        list_type = SoftwareValidationStatus.ConfirmedOK
      Else
        list_type = list_type & ", " & SoftwareValidationStatus.ConfirmedOK
      End If
    End If

    If Me.chk_validation_error.Checked Then
      If list_type.Length = 0 Then
        list_type = SoftwareValidationStatus.ConfirmedError
      Else
        list_type = list_type & ", " & SoftwareValidationStatus.ConfirmedError
      End If
    End If

    If Me.chk_canceled_or_timeout.Checked Then
      If list_type.Length = 0 Then
        list_type = SoftwareValidationStatus.CanceledTimeout
      Else
        list_type = list_type & ", " & SoftwareValidationStatus.CanceledTimeout
      End If
    End If

    If Me.chk_error.Checked Then
      If list_type.Length = 0 Then
        list_type = SoftwareValidationStatus.Error
      Else
        list_type = list_type & ", " & SoftwareValidationStatus.Error
      End If
    End If

    If Me.chk_unknown.Checked Then
      If list_type.Length = 0 Then
        list_type = SoftwareValidationStatus.Unknown
      Else
        list_type = list_type & ", " & SoftwareValidationStatus.Unknown
      End If
    End If

    Return list_type
  End Function 'GetStatusIdListSelected

#End Region  ' Private Functions

#Region " Events"

#Region " Button Events "

  ' PURPOSE: Event handler routine for the CHECK_ALL Button.
  '          Check all status CheckBoxes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_check_all_Click() Handles btn_check_all.ClickEvent
    Me.chk_unknown.Checked = True
    Me.chk_pending.Checked = True
    Me.chk_error.Checked = True
    Me.chk_validation_ok.Checked = True
    Me.chk_validation_error.Checked = True
    Me.chk_canceled_or_timeout.Checked = True
  End Sub ' btn_check_all_Click

  ' PURPOSE: Event handler routine for the UNCHECK_ALL Button.
  '          Uncheck all status CheckBoxes.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_uncheck_all_Click() Handles btn_uncheck_all.ClickEvent
    Me.chk_canceled_or_timeout.Checked = False
    Me.chk_validation_error.Checked = False
    Me.chk_validation_ok.Checked = False
    Me.chk_error.Checked = False
    Me.chk_pending.Checked = False
    Me.chk_unknown.Checked = False
  End Sub ' btn_uncheck_all_Click

#End Region ' Button Events


#End Region 'Events
End Class