'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_class_II_draw_audit_plays
'
' DESCRIPTION : Draw audit data details
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 29-JUL-2008  TJG    Initial version
'--------------------------------------------------------------------

Option Explicit On 

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices

#End Region

Public Class frm_class_II_draw_audit_plays
  Inherits frm_base_edit

#Region " Constants "

  Private Const NUM_PLAYS_DISPLAYED As Integer = 2

  ' Cards grid
  Private Const CARDS_GRID_COLUMN_CARD_ID As Integer = 0
  Private Const CARDS_GRID_COLUMN_CARD_TYPE As Integer = 1
  Private Const CARDS_GRID_COLUMN_NUM_CARD_PRIZES As Integer = 2

  Private Const CARDS_GRID_COLUMNS As Integer = 3
  Private Const CARDS_GRID_HEADER_ROWS As Integer = 2

  ' Card Prizes grid
  Private Const PRIZES_GRID_COLUMN_AMOUNT As Integer = 0

  Private Const PRIZES_GRID_COLUMNS As Integer = 1
  Private Const PRIZES_GRID_HEADER_ROWS As Integer = 1

  ' Draw Number colors
  Private Const FORE_COLOR_DRAW_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const BACK_COLOR_DRAW_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const BACK_COLOR_EMPTY_DRAW_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_GREY_00

  ' Card Number colors
  Private Const FORE_COLOR_CARD_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const BACK_COLOR_CARD_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_FORE_CARD_WINNING_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Friend WithEvents pan_play_a_jackpot As System.Windows.Forms.Panel
  Friend WithEvents ef_play_a_jackpot_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_jackpot_index As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_jackpot_name As GUI_Controls.uc_entry_field
  Friend WithEvents pan_play_b_jackpot As System.Windows.Forms.Panel
  Friend WithEvents ef_play_b_jackpot_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_jackpot_index As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_jackpot_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_play_a_jackpot_title As System.Windows.Forms.Label
  Friend WithEvents lbl_play_b_jackpot_title As System.Windows.Forms.Label
  Private Const BACK_COLOR_CARD_WINNING_NUMBER As ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_RED_00

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents ef_draw_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_num_plays As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_game As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_terminal As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_played_credits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_jackpot_bound_credits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_won_credits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_denomination As GUI_Controls.uc_entry_field
  Friend WithEvents btn_play_a_next_play As System.Windows.Forms.Button
  Friend WithEvents btn_play_b_next_play As System.Windows.Forms.Button
  Friend WithEvents ef_play_b_played_credits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_jackpot_bound_credits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_won_credits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_denomination As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_terminal As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_b_game As GUI_Controls.uc_entry_field
  Friend WithEvents dg_play_a_cards As GUI_Controls.uc_grid
  Friend WithEvents dg_play_b_cards As GUI_Controls.uc_grid
  Friend WithEvents dg_play_a_card_prizes As GUI_Controls.uc_grid
  Friend WithEvents dg_play_b_card_prizes As GUI_Controls.uc_grid
  Friend WithEvents lbl_draw_title As System.Windows.Forms.Label
  Friend WithEvents lbl_play_a_title As System.Windows.Forms.Label
  Friend WithEvents lbl_play_b_title As System.Windows.Forms.Label
  Friend WithEvents lbl_play_a_card_title As System.Windows.Forms.Label
  Friend WithEvents lbl_play_b_card_title As System.Windows.Forms.Label
  Friend WithEvents lbl_draw_numbers_title As System.Windows.Forms.Label
  Friend WithEvents lbl_draw_number_layout As System.Windows.Forms.Label
  Friend WithEvents lbl_card_number_layout As System.Windows.Forms.Label
  Friend WithEvents ef_play_b_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_play_a_date As GUI_Controls.uc_entry_field
  Friend WithEvents pan_play_a As System.Windows.Forms.Panel
  Friend WithEvents pan_play_b As System.Windows.Forms.Panel
  Friend WithEvents pan_draw As System.Windows.Forms.Panel


  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_draw_date = New GUI_Controls.uc_entry_field
    Me.ef_num_plays = New GUI_Controls.uc_entry_field
    Me.ef_play_a_game = New GUI_Controls.uc_entry_field
    Me.ef_play_a_terminal = New GUI_Controls.uc_entry_field
    Me.ef_play_a_denomination = New GUI_Controls.uc_entry_field
    Me.ef_play_a_won_credits = New GUI_Controls.uc_entry_field
    Me.ef_play_a_jackpot_bound_credits = New GUI_Controls.uc_entry_field
    Me.ef_play_a_played_credits = New GUI_Controls.uc_entry_field
    Me.btn_play_a_next_play = New System.Windows.Forms.Button
    Me.btn_play_b_next_play = New System.Windows.Forms.Button
    Me.ef_play_b_played_credits = New GUI_Controls.uc_entry_field
    Me.ef_play_b_jackpot_bound_credits = New GUI_Controls.uc_entry_field
    Me.ef_play_b_won_credits = New GUI_Controls.uc_entry_field
    Me.ef_play_b_denomination = New GUI_Controls.uc_entry_field
    Me.ef_play_b_terminal = New GUI_Controls.uc_entry_field
    Me.ef_play_b_game = New GUI_Controls.uc_entry_field
    Me.dg_play_a_cards = New GUI_Controls.uc_grid
    Me.dg_play_b_cards = New GUI_Controls.uc_grid
    Me.dg_play_a_card_prizes = New GUI_Controls.uc_grid
    Me.dg_play_b_card_prizes = New GUI_Controls.uc_grid
    Me.lbl_play_a_card_title = New System.Windows.Forms.Label
    Me.lbl_play_b_card_title = New System.Windows.Forms.Label
    Me.lbl_draw_numbers_title = New System.Windows.Forms.Label
    Me.lbl_card_number_layout = New System.Windows.Forms.Label
    Me.ef_play_b_date = New GUI_Controls.uc_entry_field
    Me.ef_play_a_date = New GUI_Controls.uc_entry_field
    Me.lbl_play_a_title = New System.Windows.Forms.Label
    Me.lbl_play_b_title = New System.Windows.Forms.Label
    Me.pan_play_a = New System.Windows.Forms.Panel
    Me.pan_play_a_jackpot = New System.Windows.Forms.Panel
    Me.ef_play_a_jackpot_amount = New GUI_Controls.uc_entry_field
    Me.ef_play_a_jackpot_index = New GUI_Controls.uc_entry_field
    Me.ef_play_a_jackpot_name = New GUI_Controls.uc_entry_field
    Me.lbl_play_a_jackpot_title = New System.Windows.Forms.Label
    Me.pan_play_b = New System.Windows.Forms.Panel
    Me.pan_play_b_jackpot = New System.Windows.Forms.Panel
    Me.ef_play_b_jackpot_amount = New GUI_Controls.uc_entry_field
    Me.ef_play_b_jackpot_index = New GUI_Controls.uc_entry_field
    Me.ef_play_b_jackpot_name = New GUI_Controls.uc_entry_field
    Me.lbl_play_b_jackpot_title = New System.Windows.Forms.Label
    Me.lbl_draw_number_layout = New System.Windows.Forms.Label
    Me.pan_draw = New System.Windows.Forms.Panel
    Me.lbl_draw_title = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.pan_play_a.SuspendLayout()
    Me.pan_play_a_jackpot.SuspendLayout()
    Me.pan_play_b.SuspendLayout()
    Me.pan_play_b_jackpot.SuspendLayout()
    Me.pan_draw.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_draw_title)
    Me.panel_data.Controls.Add(Me.ef_play_b_date)
    Me.panel_data.Controls.Add(Me.ef_play_b_played_credits)
    Me.panel_data.Controls.Add(Me.ef_play_b_denomination)
    Me.panel_data.Controls.Add(Me.ef_play_b_terminal)
    Me.panel_data.Controls.Add(Me.ef_play_b_game)
    Me.panel_data.Controls.Add(Me.lbl_play_b_title)
    Me.panel_data.Controls.Add(Me.lbl_play_a_title)
    Me.panel_data.Controls.Add(Me.dg_play_b_card_prizes)
    Me.panel_data.Controls.Add(Me.dg_play_a_card_prizes)
    Me.panel_data.Controls.Add(Me.ef_play_a_date)
    Me.panel_data.Controls.Add(Me.lbl_card_number_layout)
    Me.panel_data.Controls.Add(Me.lbl_play_b_card_title)
    Me.panel_data.Controls.Add(Me.lbl_play_a_card_title)
    Me.panel_data.Controls.Add(Me.dg_play_b_cards)
    Me.panel_data.Controls.Add(Me.dg_play_a_cards)
    Me.panel_data.Controls.Add(Me.ef_play_a_terminal)
    Me.panel_data.Controls.Add(Me.ef_play_a_game)
    Me.panel_data.Controls.Add(Me.ef_play_a_jackpot_bound_credits)
    Me.panel_data.Controls.Add(Me.ef_play_a_won_credits)
    Me.panel_data.Controls.Add(Me.ef_play_a_denomination)
    Me.panel_data.Controls.Add(Me.ef_play_a_played_credits)
    Me.panel_data.Controls.Add(Me.ef_play_b_jackpot_bound_credits)
    Me.panel_data.Controls.Add(Me.ef_play_b_won_credits)
    Me.panel_data.Controls.Add(Me.pan_play_a)
    Me.panel_data.Controls.Add(Me.pan_play_b)
    Me.panel_data.Controls.Add(Me.btn_play_b_next_play)
    Me.panel_data.Controls.Add(Me.btn_play_a_next_play)
    Me.panel_data.Controls.Add(Me.pan_draw)
    Me.panel_data.Location = New System.Drawing.Point(7, 4)
    Me.panel_data.Size = New System.Drawing.Size(883, 811)
    '
    'ef_draw_date
    '
    Me.ef_draw_date.BackColor = System.Drawing.SystemColors.Control
    Me.ef_draw_date.DoubleValue = 0
    Me.ef_draw_date.IntegerValue = 0
    Me.ef_draw_date.IsReadOnly = False
    Me.ef_draw_date.Location = New System.Drawing.Point(3, 36)
    Me.ef_draw_date.Name = "ef_draw_date"
    Me.ef_draw_date.Size = New System.Drawing.Size(201, 24)
    Me.ef_draw_date.SufixText = "Sufix Text"
    Me.ef_draw_date.SufixTextVisible = True
    Me.ef_draw_date.TabIndex = 2
    Me.ef_draw_date.TabStop = False
    Me.ef_draw_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_date.TextValue = ""
    Me.ef_draw_date.TextWidth = 65
    Me.ef_draw_date.Value = ""
    '
    'ef_num_plays
    '
    Me.ef_num_plays.BackColor = System.Drawing.SystemColors.Control
    Me.ef_num_plays.DoubleValue = 0
    Me.ef_num_plays.IntegerValue = 0
    Me.ef_num_plays.IsReadOnly = False
    Me.ef_num_plays.Location = New System.Drawing.Point(3, 64)
    Me.ef_num_plays.Name = "ef_num_plays"
    Me.ef_num_plays.Size = New System.Drawing.Size(118, 24)
    Me.ef_num_plays.SufixText = "Sufix Text"
    Me.ef_num_plays.SufixTextVisible = True
    Me.ef_num_plays.TabIndex = 4
    Me.ef_num_plays.TabStop = False
    Me.ef_num_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_num_plays.TextValue = ""
    Me.ef_num_plays.TextWidth = 65
    Me.ef_num_plays.Value = ""
    '
    'ef_play_a_game
    '
    Me.ef_play_a_game.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_game.DoubleValue = 0
    Me.ef_play_a_game.IntegerValue = 0
    Me.ef_play_a_game.IsReadOnly = False
    Me.ef_play_a_game.Location = New System.Drawing.Point(4, 317)
    Me.ef_play_a_game.Name = "ef_play_a_game"
    Me.ef_play_a_game.Size = New System.Drawing.Size(328, 24)
    Me.ef_play_a_game.SufixText = "Sufix Text"
    Me.ef_play_a_game.SufixTextVisible = True
    Me.ef_play_a_game.TabIndex = 5
    Me.ef_play_a_game.TabStop = False
    Me.ef_play_a_game.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_a_game.TextValue = ""
    Me.ef_play_a_game.TextWidth = 110
    Me.ef_play_a_game.Value = ""
    '
    'ef_play_a_terminal
    '
    Me.ef_play_a_terminal.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_terminal.DoubleValue = 0
    Me.ef_play_a_terminal.IntegerValue = 0
    Me.ef_play_a_terminal.IsReadOnly = False
    Me.ef_play_a_terminal.Location = New System.Drawing.Point(4, 287)
    Me.ef_play_a_terminal.Name = "ef_play_a_terminal"
    Me.ef_play_a_terminal.Size = New System.Drawing.Size(328, 24)
    Me.ef_play_a_terminal.SufixText = "Sufix Text"
    Me.ef_play_a_terminal.SufixTextVisible = True
    Me.ef_play_a_terminal.TabIndex = 9
    Me.ef_play_a_terminal.TabStop = False
    Me.ef_play_a_terminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_a_terminal.TextValue = ""
    Me.ef_play_a_terminal.TextWidth = 110
    Me.ef_play_a_terminal.Value = ""
    '
    'ef_play_a_denomination
    '
    Me.ef_play_a_denomination.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_denomination.DoubleValue = 0
    Me.ef_play_a_denomination.IntegerValue = 0
    Me.ef_play_a_denomination.IsReadOnly = False
    Me.ef_play_a_denomination.Location = New System.Drawing.Point(4, 348)
    Me.ef_play_a_denomination.Name = "ef_play_a_denomination"
    Me.ef_play_a_denomination.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_a_denomination.SufixText = "Sufix Text"
    Me.ef_play_a_denomination.SufixTextVisible = True
    Me.ef_play_a_denomination.TabIndex = 10
    Me.ef_play_a_denomination.TabStop = False
    Me.ef_play_a_denomination.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_a_denomination.TextValue = ""
    Me.ef_play_a_denomination.TextWidth = 110
    Me.ef_play_a_denomination.Value = ""
    '
    'ef_play_a_won_credits
    '
    Me.ef_play_a_won_credits.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_won_credits.DoubleValue = 0
    Me.ef_play_a_won_credits.IntegerValue = 0
    Me.ef_play_a_won_credits.IsReadOnly = False
    Me.ef_play_a_won_credits.Location = New System.Drawing.Point(4, 408)
    Me.ef_play_a_won_credits.Name = "ef_play_a_won_credits"
    Me.ef_play_a_won_credits.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_a_won_credits.SufixText = "Sufix Text"
    Me.ef_play_a_won_credits.SufixTextVisible = True
    Me.ef_play_a_won_credits.TabIndex = 11
    Me.ef_play_a_won_credits.TabStop = False
    Me.ef_play_a_won_credits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_a_won_credits.TextValue = ""
    Me.ef_play_a_won_credits.TextWidth = 110
    Me.ef_play_a_won_credits.Value = ""
    '
    'ef_play_a_jackpot_bound_credits
    '
    Me.ef_play_a_jackpot_bound_credits.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_jackpot_bound_credits.DoubleValue = 0
    Me.ef_play_a_jackpot_bound_credits.IntegerValue = 0
    Me.ef_play_a_jackpot_bound_credits.IsReadOnly = False
    Me.ef_play_a_jackpot_bound_credits.Location = New System.Drawing.Point(4, 438)
    Me.ef_play_a_jackpot_bound_credits.Name = "ef_play_a_jackpot_bound_credits"
    Me.ef_play_a_jackpot_bound_credits.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_a_jackpot_bound_credits.SufixText = "Sufix Text"
    Me.ef_play_a_jackpot_bound_credits.SufixTextVisible = True
    Me.ef_play_a_jackpot_bound_credits.TabIndex = 14
    Me.ef_play_a_jackpot_bound_credits.TabStop = False
    Me.ef_play_a_jackpot_bound_credits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_a_jackpot_bound_credits.TextValue = ""
    Me.ef_play_a_jackpot_bound_credits.TextWidth = 135
    Me.ef_play_a_jackpot_bound_credits.Value = ""
    '
    'ef_play_a_played_credits
    '
    Me.ef_play_a_played_credits.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_played_credits.DoubleValue = 0
    Me.ef_play_a_played_credits.IntegerValue = 0
    Me.ef_play_a_played_credits.IsReadOnly = False
    Me.ef_play_a_played_credits.Location = New System.Drawing.Point(4, 378)
    Me.ef_play_a_played_credits.Name = "ef_play_a_played_credits"
    Me.ef_play_a_played_credits.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_a_played_credits.SufixText = "Sufix Text"
    Me.ef_play_a_played_credits.SufixTextVisible = True
    Me.ef_play_a_played_credits.TabIndex = 15
    Me.ef_play_a_played_credits.TabStop = False
    Me.ef_play_a_played_credits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_a_played_credits.TextValue = ""
    Me.ef_play_a_played_credits.TextWidth = 110
    Me.ef_play_a_played_credits.Value = ""
    '
    'btn_play_a_next_play
    '
    Me.btn_play_a_next_play.Location = New System.Drawing.Point(347, 207)
    Me.btn_play_a_next_play.Name = "btn_play_a_next_play"
    Me.btn_play_a_next_play.Size = New System.Drawing.Size(87, 33)
    Me.btn_play_a_next_play.TabIndex = 16
    Me.btn_play_a_next_play.Text = "xNext Play"
    Me.btn_play_a_next_play.UseVisualStyleBackColor = True
    '
    'btn_play_b_next_play
    '
    Me.btn_play_b_next_play.Location = New System.Drawing.Point(793, 208)
    Me.btn_play_b_next_play.Name = "btn_play_b_next_play"
    Me.btn_play_b_next_play.Size = New System.Drawing.Size(87, 33)
    Me.btn_play_b_next_play.TabIndex = 29
    Me.btn_play_b_next_play.Text = "xNext Play"
    Me.btn_play_b_next_play.UseVisualStyleBackColor = True
    '
    'ef_play_b_played_credits
    '
    Me.ef_play_b_played_credits.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_played_credits.DoubleValue = 0
    Me.ef_play_b_played_credits.IntegerValue = 0
    Me.ef_play_b_played_credits.IsReadOnly = False
    Me.ef_play_b_played_credits.Location = New System.Drawing.Point(456, 378)
    Me.ef_play_b_played_credits.Name = "ef_play_b_played_credits"
    Me.ef_play_b_played_credits.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_b_played_credits.SufixText = "Sufix Text"
    Me.ef_play_b_played_credits.SufixTextVisible = True
    Me.ef_play_b_played_credits.TabIndex = 28
    Me.ef_play_b_played_credits.TabStop = False
    Me.ef_play_b_played_credits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_b_played_credits.TextValue = ""
    Me.ef_play_b_played_credits.TextWidth = 110
    Me.ef_play_b_played_credits.Value = ""
    '
    'ef_play_b_jackpot_bound_credits
    '
    Me.ef_play_b_jackpot_bound_credits.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_jackpot_bound_credits.DoubleValue = 0
    Me.ef_play_b_jackpot_bound_credits.IntegerValue = 0
    Me.ef_play_b_jackpot_bound_credits.IsReadOnly = False
    Me.ef_play_b_jackpot_bound_credits.Location = New System.Drawing.Point(456, 438)
    Me.ef_play_b_jackpot_bound_credits.Name = "ef_play_b_jackpot_bound_credits"
    Me.ef_play_b_jackpot_bound_credits.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_b_jackpot_bound_credits.SufixText = "Sufix Text"
    Me.ef_play_b_jackpot_bound_credits.SufixTextVisible = True
    Me.ef_play_b_jackpot_bound_credits.TabIndex = 27
    Me.ef_play_b_jackpot_bound_credits.TabStop = False
    Me.ef_play_b_jackpot_bound_credits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_b_jackpot_bound_credits.TextValue = ""
    Me.ef_play_b_jackpot_bound_credits.TextWidth = 135
    Me.ef_play_b_jackpot_bound_credits.Value = ""
    '
    'ef_play_b_won_credits
    '
    Me.ef_play_b_won_credits.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_won_credits.DoubleValue = 0
    Me.ef_play_b_won_credits.IntegerValue = 0
    Me.ef_play_b_won_credits.IsReadOnly = False
    Me.ef_play_b_won_credits.Location = New System.Drawing.Point(456, 408)
    Me.ef_play_b_won_credits.Name = "ef_play_b_won_credits"
    Me.ef_play_b_won_credits.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_b_won_credits.SufixText = "Sufix Text"
    Me.ef_play_b_won_credits.SufixTextVisible = True
    Me.ef_play_b_won_credits.TabIndex = 24
    Me.ef_play_b_won_credits.TabStop = False
    Me.ef_play_b_won_credits.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_b_won_credits.TextValue = ""
    Me.ef_play_b_won_credits.TextWidth = 110
    Me.ef_play_b_won_credits.Value = ""
    '
    'ef_play_b_denomination
    '
    Me.ef_play_b_denomination.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_denomination.DoubleValue = 0
    Me.ef_play_b_denomination.IntegerValue = 0
    Me.ef_play_b_denomination.IsReadOnly = False
    Me.ef_play_b_denomination.Location = New System.Drawing.Point(456, 348)
    Me.ef_play_b_denomination.Name = "ef_play_b_denomination"
    Me.ef_play_b_denomination.Size = New System.Drawing.Size(202, 24)
    Me.ef_play_b_denomination.SufixText = "Sufix Text"
    Me.ef_play_b_denomination.SufixTextVisible = True
    Me.ef_play_b_denomination.TabIndex = 23
    Me.ef_play_b_denomination.TabStop = False
    Me.ef_play_b_denomination.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_b_denomination.TextValue = ""
    Me.ef_play_b_denomination.TextWidth = 110
    Me.ef_play_b_denomination.Value = ""
    '
    'ef_play_b_terminal
    '
    Me.ef_play_b_terminal.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_terminal.DoubleValue = 0
    Me.ef_play_b_terminal.IntegerValue = 0
    Me.ef_play_b_terminal.IsReadOnly = False
    Me.ef_play_b_terminal.Location = New System.Drawing.Point(456, 287)
    Me.ef_play_b_terminal.Name = "ef_play_b_terminal"
    Me.ef_play_b_terminal.Size = New System.Drawing.Size(323, 24)
    Me.ef_play_b_terminal.SufixText = "Sufix Text"
    Me.ef_play_b_terminal.SufixTextVisible = True
    Me.ef_play_b_terminal.TabIndex = 22
    Me.ef_play_b_terminal.TabStop = False
    Me.ef_play_b_terminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_b_terminal.TextValue = ""
    Me.ef_play_b_terminal.TextWidth = 110
    Me.ef_play_b_terminal.Value = ""
    '
    'ef_play_b_game
    '
    Me.ef_play_b_game.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_game.DoubleValue = 0
    Me.ef_play_b_game.IntegerValue = 0
    Me.ef_play_b_game.IsReadOnly = False
    Me.ef_play_b_game.Location = New System.Drawing.Point(456, 317)
    Me.ef_play_b_game.Name = "ef_play_b_game"
    Me.ef_play_b_game.Size = New System.Drawing.Size(323, 24)
    Me.ef_play_b_game.SufixText = "Sufix Text"
    Me.ef_play_b_game.SufixTextVisible = True
    Me.ef_play_b_game.TabIndex = 18
    Me.ef_play_b_game.TabStop = False
    Me.ef_play_b_game.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_b_game.TextValue = ""
    Me.ef_play_b_game.TextWidth = 110
    Me.ef_play_b_game.Value = ""
    '
    'dg_play_a_cards
    '
    Me.dg_play_a_cards.BackColor = System.Drawing.SystemColors.Control
    Me.dg_play_a_cards.CurrentCol = -1
    Me.dg_play_a_cards.CurrentRow = -1
    Me.dg_play_a_cards.Location = New System.Drawing.Point(12, 487)
    Me.dg_play_a_cards.Name = "dg_play_a_cards"
    Me.dg_play_a_cards.PanelRightVisible = True
    Me.dg_play_a_cards.Redraw = True
    Me.dg_play_a_cards.Size = New System.Drawing.Size(388, 111)
    Me.dg_play_a_cards.Sortable = False
    Me.dg_play_a_cards.SortAscending = True
    Me.dg_play_a_cards.SortByCol = 0
    Me.dg_play_a_cards.TabIndex = 31
    Me.dg_play_a_cards.ToolTipped = True
    Me.dg_play_a_cards.TopRow = -2
    '
    'dg_play_b_cards
    '
    Me.dg_play_b_cards.BackColor = System.Drawing.SystemColors.Control
    Me.dg_play_b_cards.CurrentCol = -1
    Me.dg_play_b_cards.CurrentRow = -1
    Me.dg_play_b_cards.Location = New System.Drawing.Point(466, 487)
    Me.dg_play_b_cards.Name = "dg_play_b_cards"
    Me.dg_play_b_cards.PanelRightVisible = True
    Me.dg_play_b_cards.Redraw = True
    Me.dg_play_b_cards.Size = New System.Drawing.Size(388, 111)
    Me.dg_play_b_cards.Sortable = False
    Me.dg_play_b_cards.SortAscending = True
    Me.dg_play_b_cards.SortByCol = 0
    Me.dg_play_b_cards.TabIndex = 32
    Me.dg_play_b_cards.ToolTipped = True
    Me.dg_play_b_cards.TopRow = -2
    '
    'dg_play_a_card_prizes
    '
    Me.dg_play_a_card_prizes.BackColor = System.Drawing.SystemColors.Control
    Me.dg_play_a_card_prizes.CurrentCol = -1
    Me.dg_play_a_card_prizes.CurrentRow = -1
    Me.dg_play_a_card_prizes.Location = New System.Drawing.Point(227, 635)
    Me.dg_play_a_card_prizes.Name = "dg_play_a_card_prizes"
    Me.dg_play_a_card_prizes.PanelRightVisible = True
    Me.dg_play_a_card_prizes.Redraw = True
    Me.dg_play_a_card_prizes.Size = New System.Drawing.Size(173, 168)
    Me.dg_play_a_card_prizes.Sortable = False
    Me.dg_play_a_card_prizes.SortAscending = True
    Me.dg_play_a_card_prizes.SortByCol = 0
    Me.dg_play_a_card_prizes.TabIndex = 42
    Me.dg_play_a_card_prizes.ToolTipped = True
    Me.dg_play_a_card_prizes.TopRow = -2
    '
    'dg_play_b_card_prizes
    '
    Me.dg_play_b_card_prizes.BackColor = System.Drawing.SystemColors.Control
    Me.dg_play_b_card_prizes.CurrentCol = -1
    Me.dg_play_b_card_prizes.CurrentRow = -1
    Me.dg_play_b_card_prizes.Location = New System.Drawing.Point(681, 635)
    Me.dg_play_b_card_prizes.Name = "dg_play_b_card_prizes"
    Me.dg_play_b_card_prizes.PanelRightVisible = True
    Me.dg_play_b_card_prizes.Redraw = True
    Me.dg_play_b_card_prizes.Size = New System.Drawing.Size(173, 168)
    Me.dg_play_b_card_prizes.Sortable = False
    Me.dg_play_b_card_prizes.SortAscending = True
    Me.dg_play_b_card_prizes.SortByCol = 0
    Me.dg_play_b_card_prizes.TabIndex = 43
    Me.dg_play_b_card_prizes.ToolTipped = True
    Me.dg_play_b_card_prizes.TopRow = -2
    '
    'lbl_play_a_card_title
    '
    Me.lbl_play_a_card_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_play_a_card_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_play_a_card_title.Location = New System.Drawing.Point(12, 612)
    Me.lbl_play_a_card_title.Name = "lbl_play_a_card_title"
    Me.lbl_play_a_card_title.Size = New System.Drawing.Size(310, 22)
    Me.lbl_play_a_card_title.TabIndex = 33
    Me.lbl_play_a_card_title.Text = "xCard"
    Me.lbl_play_a_card_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_play_b_card_title
    '
    Me.lbl_play_b_card_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_play_b_card_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_play_b_card_title.Location = New System.Drawing.Point(466, 612)
    Me.lbl_play_b_card_title.Name = "lbl_play_b_card_title"
    Me.lbl_play_b_card_title.Size = New System.Drawing.Size(313, 22)
    Me.lbl_play_b_card_title.TabIndex = 35
    Me.lbl_play_b_card_title.Text = "xCard"
    Me.lbl_play_b_card_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_draw_numbers_title
    '
    Me.lbl_draw_numbers_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_draw_numbers_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_draw_numbers_title.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_draw_numbers_title.Location = New System.Drawing.Point(213, 12)
    Me.lbl_draw_numbers_title.Name = "lbl_draw_numbers_title"
    Me.lbl_draw_numbers_title.Size = New System.Drawing.Size(148, 24)
    Me.lbl_draw_numbers_title.TabIndex = 38
    Me.lbl_draw_numbers_title.Text = "xDraw Numbers"
    Me.lbl_draw_numbers_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_card_number_layout
    '
    Me.lbl_card_number_layout.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_card_number_layout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.lbl_card_number_layout.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_card_number_layout.ForeColor = System.Drawing.Color.Blue
    Me.lbl_card_number_layout.Location = New System.Drawing.Point(12, 634)
    Me.lbl_card_number_layout.Name = "lbl_card_number_layout"
    Me.lbl_card_number_layout.Size = New System.Drawing.Size(30, 26)
    Me.lbl_card_number_layout.TabIndex = 39
    Me.lbl_card_number_layout.Text = "xCard Number"
    Me.lbl_card_number_layout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ef_play_b_date
    '
    Me.ef_play_b_date.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_date.DoubleValue = 0
    Me.ef_play_b_date.IntegerValue = 0
    Me.ef_play_b_date.IsReadOnly = False
    Me.ef_play_b_date.Location = New System.Drawing.Point(456, 256)
    Me.ef_play_b_date.Name = "ef_play_b_date"
    Me.ef_play_b_date.Size = New System.Drawing.Size(267, 24)
    Me.ef_play_b_date.SufixText = "Sufix Text"
    Me.ef_play_b_date.SufixTextVisible = True
    Me.ef_play_b_date.TabIndex = 41
    Me.ef_play_b_date.TabStop = False
    Me.ef_play_b_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_b_date.TextValue = ""
    Me.ef_play_b_date.TextWidth = 110
    Me.ef_play_b_date.Value = ""
    '
    'ef_play_a_date
    '
    Me.ef_play_a_date.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_date.DoubleValue = 0
    Me.ef_play_a_date.IntegerValue = 0
    Me.ef_play_a_date.IsReadOnly = False
    Me.ef_play_a_date.Location = New System.Drawing.Point(4, 256)
    Me.ef_play_a_date.Name = "ef_play_a_date"
    Me.ef_play_a_date.Size = New System.Drawing.Size(267, 24)
    Me.ef_play_a_date.SufixText = "Sufix Text"
    Me.ef_play_a_date.SufixTextVisible = True
    Me.ef_play_a_date.TabIndex = 40
    Me.ef_play_a_date.TabStop = False
    Me.ef_play_a_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_a_date.TextValue = ""
    Me.ef_play_a_date.TextWidth = 110
    Me.ef_play_a_date.Value = ""
    '
    'lbl_play_a_title
    '
    Me.lbl_play_a_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_play_a_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_play_a_title.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_play_a_title.Location = New System.Drawing.Point(0, 220)
    Me.lbl_play_a_title.Name = "lbl_play_a_title"
    Me.lbl_play_a_title.Size = New System.Drawing.Size(332, 22)
    Me.lbl_play_a_title.TabIndex = 45
    Me.lbl_play_a_title.Text = "xPlay A"
    Me.lbl_play_a_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_play_b_title
    '
    Me.lbl_play_b_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_play_b_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_play_b_title.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_play_b_title.Location = New System.Drawing.Point(446, 219)
    Me.lbl_play_b_title.Name = "lbl_play_b_title"
    Me.lbl_play_b_title.Size = New System.Drawing.Size(333, 22)
    Me.lbl_play_b_title.TabIndex = 47
    Me.lbl_play_b_title.Text = "xPlay B"
    Me.lbl_play_b_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'pan_play_a
    '
    Me.pan_play_a.BackColor = System.Drawing.SystemColors.Control
    Me.pan_play_a.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pan_play_a.Controls.Add(Me.pan_play_a_jackpot)
    Me.pan_play_a.Controls.Add(Me.lbl_play_a_jackpot_title)
    Me.pan_play_a.Location = New System.Drawing.Point(0, 244)
    Me.pan_play_a.Name = "pan_play_a"
    Me.pan_play_a.Size = New System.Drawing.Size(434, 570)
    Me.pan_play_a.TabIndex = 48
    '
    'pan_play_a_jackpot
    '
    Me.pan_play_a_jackpot.BackColor = System.Drawing.SystemColors.Control
    Me.pan_play_a_jackpot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pan_play_a_jackpot.Controls.Add(Me.ef_play_a_jackpot_amount)
    Me.pan_play_a_jackpot.Controls.Add(Me.ef_play_a_jackpot_index)
    Me.pan_play_a_jackpot.Controls.Add(Me.ef_play_a_jackpot_name)
    Me.pan_play_a_jackpot.Location = New System.Drawing.Point(219, 115)
    Me.pan_play_a_jackpot.Name = "pan_play_a_jackpot"
    Me.pan_play_a_jackpot.Size = New System.Drawing.Size(205, 101)
    Me.pan_play_a_jackpot.TabIndex = 54
    '
    'ef_play_a_jackpot_amount
    '
    Me.ef_play_a_jackpot_amount.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_jackpot_amount.DoubleValue = 0
    Me.ef_play_a_jackpot_amount.IntegerValue = 0
    Me.ef_play_a_jackpot_amount.IsReadOnly = False
    Me.ef_play_a_jackpot_amount.Location = New System.Drawing.Point(11, 65)
    Me.ef_play_a_jackpot_amount.Name = "ef_play_a_jackpot_amount"
    Me.ef_play_a_jackpot_amount.Size = New System.Drawing.Size(179, 24)
    Me.ef_play_a_jackpot_amount.SufixText = "Sufix Text"
    Me.ef_play_a_jackpot_amount.SufixTextVisible = True
    Me.ef_play_a_jackpot_amount.TabIndex = 21
    Me.ef_play_a_jackpot_amount.TabStop = False
    Me.ef_play_a_jackpot_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_a_jackpot_amount.TextValue = ""
    Me.ef_play_a_jackpot_amount.TextWidth = 50
    Me.ef_play_a_jackpot_amount.Value = ""
    '
    'ef_play_a_jackpot_index
    '
    Me.ef_play_a_jackpot_index.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_jackpot_index.DoubleValue = 0
    Me.ef_play_a_jackpot_index.IntegerValue = 0
    Me.ef_play_a_jackpot_index.IsReadOnly = False
    Me.ef_play_a_jackpot_index.Location = New System.Drawing.Point(11, 36)
    Me.ef_play_a_jackpot_index.Name = "ef_play_a_jackpot_index"
    Me.ef_play_a_jackpot_index.Size = New System.Drawing.Size(98, 24)
    Me.ef_play_a_jackpot_index.SufixText = "Sufix Text"
    Me.ef_play_a_jackpot_index.SufixTextVisible = True
    Me.ef_play_a_jackpot_index.TabIndex = 20
    Me.ef_play_a_jackpot_index.TabStop = False
    Me.ef_play_a_jackpot_index.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_a_jackpot_index.TextValue = ""
    Me.ef_play_a_jackpot_index.TextWidth = 50
    Me.ef_play_a_jackpot_index.Value = ""
    '
    'ef_play_a_jackpot_name
    '
    Me.ef_play_a_jackpot_name.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_a_jackpot_name.DoubleValue = 0
    Me.ef_play_a_jackpot_name.IntegerValue = 0
    Me.ef_play_a_jackpot_name.IsReadOnly = False
    Me.ef_play_a_jackpot_name.Location = New System.Drawing.Point(11, 6)
    Me.ef_play_a_jackpot_name.Name = "ef_play_a_jackpot_name"
    Me.ef_play_a_jackpot_name.Size = New System.Drawing.Size(179, 24)
    Me.ef_play_a_jackpot_name.SufixText = "Sufix Text"
    Me.ef_play_a_jackpot_name.SufixTextVisible = True
    Me.ef_play_a_jackpot_name.TabIndex = 19
    Me.ef_play_a_jackpot_name.TabStop = False
    Me.ef_play_a_jackpot_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_a_jackpot_name.TextValue = ""
    Me.ef_play_a_jackpot_name.TextWidth = 50
    Me.ef_play_a_jackpot_name.Value = ""
    '
    'lbl_play_a_jackpot_title
    '
    Me.lbl_play_a_jackpot_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_play_a_jackpot_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_play_a_jackpot_title.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_play_a_jackpot_title.Location = New System.Drawing.Point(219, 90)
    Me.lbl_play_a_jackpot_title.Name = "lbl_play_a_jackpot_title"
    Me.lbl_play_a_jackpot_title.Size = New System.Drawing.Size(208, 22)
    Me.lbl_play_a_jackpot_title.TabIndex = 55
    Me.lbl_play_a_jackpot_title.Text = "xJackpot A"
    Me.lbl_play_a_jackpot_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'pan_play_b
    '
    Me.pan_play_b.BackColor = System.Drawing.SystemColors.Control
    Me.pan_play_b.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pan_play_b.Controls.Add(Me.pan_play_b_jackpot)
    Me.pan_play_b.Controls.Add(Me.lbl_play_b_jackpot_title)
    Me.pan_play_b.Location = New System.Drawing.Point(446, 244)
    Me.pan_play_b.Name = "pan_play_b"
    Me.pan_play_b.Size = New System.Drawing.Size(434, 570)
    Me.pan_play_b.TabIndex = 49
    '
    'pan_play_b_jackpot
    '
    Me.pan_play_b_jackpot.BackColor = System.Drawing.SystemColors.Control
    Me.pan_play_b_jackpot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pan_play_b_jackpot.Controls.Add(Me.ef_play_b_jackpot_amount)
    Me.pan_play_b_jackpot.Controls.Add(Me.ef_play_b_jackpot_index)
    Me.pan_play_b_jackpot.Controls.Add(Me.ef_play_b_jackpot_name)
    Me.pan_play_b_jackpot.Location = New System.Drawing.Point(222, 115)
    Me.pan_play_b_jackpot.Name = "pan_play_b_jackpot"
    Me.pan_play_b_jackpot.Size = New System.Drawing.Size(205, 101)
    Me.pan_play_b_jackpot.TabIndex = 55
    '
    'ef_play_b_jackpot_amount
    '
    Me.ef_play_b_jackpot_amount.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_jackpot_amount.DoubleValue = 0
    Me.ef_play_b_jackpot_amount.IntegerValue = 0
    Me.ef_play_b_jackpot_amount.IsReadOnly = False
    Me.ef_play_b_jackpot_amount.Location = New System.Drawing.Point(11, 66)
    Me.ef_play_b_jackpot_amount.Name = "ef_play_b_jackpot_amount"
    Me.ef_play_b_jackpot_amount.Size = New System.Drawing.Size(179, 24)
    Me.ef_play_b_jackpot_amount.SufixText = "Sufix Text"
    Me.ef_play_b_jackpot_amount.SufixTextVisible = True
    Me.ef_play_b_jackpot_amount.TabIndex = 21
    Me.ef_play_b_jackpot_amount.TabStop = False
    Me.ef_play_b_jackpot_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_b_jackpot_amount.TextValue = ""
    Me.ef_play_b_jackpot_amount.TextWidth = 50
    Me.ef_play_b_jackpot_amount.Value = ""
    '
    'ef_play_b_jackpot_index
    '
    Me.ef_play_b_jackpot_index.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_jackpot_index.DoubleValue = 0
    Me.ef_play_b_jackpot_index.IntegerValue = 0
    Me.ef_play_b_jackpot_index.IsReadOnly = False
    Me.ef_play_b_jackpot_index.Location = New System.Drawing.Point(11, 37)
    Me.ef_play_b_jackpot_index.Name = "ef_play_b_jackpot_index"
    Me.ef_play_b_jackpot_index.Size = New System.Drawing.Size(98, 24)
    Me.ef_play_b_jackpot_index.SufixText = "Sufix Text"
    Me.ef_play_b_jackpot_index.SufixTextVisible = True
    Me.ef_play_b_jackpot_index.TabIndex = 20
    Me.ef_play_b_jackpot_index.TabStop = False
    Me.ef_play_b_jackpot_index.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_play_b_jackpot_index.TextValue = ""
    Me.ef_play_b_jackpot_index.TextWidth = 50
    Me.ef_play_b_jackpot_index.Value = ""
    '
    'ef_play_b_jackpot_name
    '
    Me.ef_play_b_jackpot_name.BackColor = System.Drawing.SystemColors.Control
    Me.ef_play_b_jackpot_name.DoubleValue = 0
    Me.ef_play_b_jackpot_name.IntegerValue = 0
    Me.ef_play_b_jackpot_name.IsReadOnly = False
    Me.ef_play_b_jackpot_name.Location = New System.Drawing.Point(11, 7)
    Me.ef_play_b_jackpot_name.Name = "ef_play_b_jackpot_name"
    Me.ef_play_b_jackpot_name.Size = New System.Drawing.Size(179, 24)
    Me.ef_play_b_jackpot_name.SufixText = "Sufix Text"
    Me.ef_play_b_jackpot_name.SufixTextVisible = True
    Me.ef_play_b_jackpot_name.TabIndex = 19
    Me.ef_play_b_jackpot_name.TabStop = False
    Me.ef_play_b_jackpot_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_play_b_jackpot_name.TextValue = ""
    Me.ef_play_b_jackpot_name.TextWidth = 50
    Me.ef_play_b_jackpot_name.Value = ""
    '
    'lbl_play_b_jackpot_title
    '
    Me.lbl_play_b_jackpot_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_play_b_jackpot_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_play_b_jackpot_title.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_play_b_jackpot_title.Location = New System.Drawing.Point(219, 90)
    Me.lbl_play_b_jackpot_title.Name = "lbl_play_b_jackpot_title"
    Me.lbl_play_b_jackpot_title.Size = New System.Drawing.Size(208, 22)
    Me.lbl_play_b_jackpot_title.TabIndex = 56
    Me.lbl_play_b_jackpot_title.Text = "xJackpot B"
    Me.lbl_play_b_jackpot_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_draw_number_layout
    '
    Me.lbl_draw_number_layout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.lbl_draw_number_layout.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_draw_number_layout.ForeColor = System.Drawing.Color.Blue
    Me.lbl_draw_number_layout.Location = New System.Drawing.Point(216, 36)
    Me.lbl_draw_number_layout.Name = "lbl_draw_number_layout"
    Me.lbl_draw_number_layout.Size = New System.Drawing.Size(30, 26)
    Me.lbl_draw_number_layout.TabIndex = 50
    Me.lbl_draw_number_layout.Text = "xDraw Number"
    Me.lbl_draw_number_layout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'pan_draw
    '
    Me.pan_draw.BackColor = System.Drawing.SystemColors.Control
    Me.pan_draw.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pan_draw.Controls.Add(Me.lbl_draw_number_layout)
    Me.pan_draw.Controls.Add(Me.lbl_draw_numbers_title)
    Me.pan_draw.Controls.Add(Me.ef_num_plays)
    Me.pan_draw.Controls.Add(Me.ef_draw_date)
    Me.pan_draw.Location = New System.Drawing.Point(0, 22)
    Me.pan_draw.Name = "pan_draw"
    Me.pan_draw.Size = New System.Drawing.Size(880, 181)
    Me.pan_draw.TabIndex = 51
    '
    'lbl_draw_title
    '
    Me.lbl_draw_title.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_draw_title.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_draw_title.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_draw_title.Location = New System.Drawing.Point(0, 0)
    Me.lbl_draw_title.Name = "lbl_draw_title"
    Me.lbl_draw_title.Size = New System.Drawing.Size(332, 22)
    Me.lbl_draw_title.TabIndex = 52
    Me.lbl_draw_title.Text = "xDraw "
    Me.lbl_draw_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'frm_class_II_draw_audit_plays
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(988, 822)
    Me.ForeColor = System.Drawing.SystemColors.ControlText
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_class_II_draw_audit_plays"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.pan_play_a.ResumeLayout(False)
    Me.pan_play_a_jackpot.ResumeLayout(False)
    Me.pan_play_b.ResumeLayout(False)
    Me.pan_play_b_jackpot.ResumeLayout(False)
    Me.pan_draw.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_displayed_plays(NUM_PLAYS_DISPLAYED - 1) As Integer

  ' To save the index of the data displayed in each panel
  Private m_idx_data_play(0 To NUM_PLAYS_DISPLAYED - 1) As Integer
  Private m_idx_data_card(0 To NUM_PLAYS_DISPLAYED - 1) As Integer

  Public ef_play_game As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_terminal As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_date As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_denomination As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_played_credits As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_jackpot_bound_credits As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_won_credits As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public pan_play_jackpot As New System.Collections.Generic.List(Of System.Windows.Forms.Panel)
  Public ef_play_jackpot_name As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_jackpot_index As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public ef_play_jackpot_amount As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)
  Public lbl_play_jackpot_title As New System.Collections.Generic.List(Of System.Windows.Forms.Label)
  Public dg_play_cards As New System.Collections.Generic.List(Of GUI_Controls.uc_grid)
  Public dg_play_card_prizes As New System.Collections.Generic.List(Of GUI_Controls.uc_grid)
  Public lbl_play_title As New System.Collections.Generic.List(Of System.Windows.Forms.Label)
  Public lbl_play_card_title As New System.Collections.Generic.List(Of System.Windows.Forms.Label)
  Public lbl_draw_number As New System.Collections.Generic.List(Of System.Windows.Forms.Label)
  Public lbl_card_number As New System.Collections.Generic.List(Of System.Windows.Forms.Label)
  Public btn_next_play As New System.Collections.Generic.List(Of System.Windows.Forms.Button)
  Public pan_play As New System.Collections.Generic.List(Of System.Windows.Forms.Panel)

#End Region

#Region " Override Functions "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CLASS_II_DRAW_AUDIT_PLAYS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Configuration.GUI_Configuration.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub 'GUI_SetFormId

  ' PURPOSE : Initialize the Draw Number controls
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub InitDrawNumbers()

    Dim idx_number As Integer
    Dim idx_coord As Integer
    Dim aux_label As System.Windows.Forms.Label
    Dim coord_x(0 To 30) As Integer
    Dim coord_y(0 To 30) As Integer

    For idx_coord = 0 To coord_x.Length - 1
      coord_x(idx_coord) = idx_coord * (lbl_draw_number_layout.Width + 3)
    Next

    For idx_coord = 0 To coord_y.Length - 1
      coord_y(idx_coord) = idx_coord * (lbl_draw_number_layout.Height + 8)
    Next

    lbl_draw_number_layout.Visible = False

    For idx_number = 0 To CLASS_DRAW_AUDIT.NUMBERS_PER_DRAW - 1
      aux_label = New System.Windows.Forms.Label

      Me.pan_draw.Controls.Add(aux_label)

      With aux_label
        aux_label.Left = lbl_draw_numbers_title.Left _
                       + coord_x(idx_number Mod 20)

        aux_label.Top = lbl_draw_numbers_title.Top _
                      + lbl_draw_numbers_title.Height _
                      + coord_y(idx_number \ 20)

        aux_label.Width = lbl_draw_number_layout.Width
        aux_label.Height = lbl_draw_number_layout.Height

        aux_label.Font = lbl_draw_number_layout.Font
        aux_label.BorderStyle = lbl_draw_number_layout.BorderStyle
        aux_label.TextAlign = lbl_draw_number_layout.TextAlign

        aux_label.ForeColor = GetColor(BACK_COLOR_EMPTY_DRAW_NUMBER)
        aux_label.BackColor = GetColor(BACK_COLOR_EMPTY_DRAW_NUMBER)

        aux_label.Text = ""       ' lbl_draw_number.Count.ToString

        aux_label.Visible = True

        ' Add to the control collection
        lbl_draw_number.Add(aux_label)
      End With
    Next

  End Sub

  ' PURPOSE : Initialize the Card Numbers controls
  '
  '  PARAMS :
  '     - INPUT :
  '           - IdxScreenPlay
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub InitCardNumbers(ByVal IdxScreenPlay As Integer)

    Dim idx_card As Integer
    Dim idx_coord As Integer
    Dim aux_label As System.Windows.Forms.Label
    Dim coord_x(0 To 4) As Integer
    Dim coord_y(0 To 4) As Integer

    For idx_coord = 0 To coord_x.Length - 1
      coord_x(idx_coord) = idx_coord * (lbl_card_number_layout.Width + 3)
    Next

    For idx_coord = 0 To coord_y.Length - 1
      coord_y(idx_coord) = idx_coord * (lbl_card_number_layout.Height + 8)
    Next

    lbl_card_number_layout.Visible = False

    For idx_card = 0 To CLASS_DRAW_AUDIT.NUMBERS_PER_CARD - 1
      aux_label = New System.Windows.Forms.Label

      Me.pan_play.Item(IdxScreenPlay).Controls.Add(aux_label)

      With aux_label
        aux_label.Left = lbl_play_card_title.Item(IdxScreenPlay).Left _
                       - pan_play.Item(IdxScreenPlay).Left _
                       + coord_x(idx_card \ 5)

        aux_label.Top = lbl_play_card_title.Item(IdxScreenPlay).Top _
                      - pan_play.Item(IdxScreenPlay).Top _
                      + lbl_play_card_title.Item(IdxScreenPlay).Height _
                      + coord_y(idx_card Mod 5)

        aux_label.Width = lbl_card_number_layout.Width
        aux_label.Height = lbl_card_number_layout.Height

        aux_label.Font = lbl_card_number_layout.Font
        aux_label.BorderStyle = lbl_card_number_layout.BorderStyle
        aux_label.TextAlign = lbl_card_number_layout.TextAlign

        aux_label.Text = ""       ' lbl_card_number.Count.ToString

        aux_label.Visible = True

        ' Add to the control collection
        lbl_card_number.Add(aux_label)
      End With
    Next

  End Sub

  Private Sub InitCards(ByVal IdxScreenPlay As Integer)

    ' Cards list has just two columns to hold the card id and the card type
    With Me.dg_play_cards.Item(IdxScreenPlay)
      .Init(CARDS_GRID_COLUMNS, CARDS_GRID_HEADER_ROWS)

      .Counter(0).Visible = True
      .Sortable = False

      ' Card Id
      .Column(CARDS_GRID_COLUMN_CARD_ID).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(315)
      .Column(CARDS_GRID_COLUMN_CARD_ID).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(307)
      .Column(CARDS_GRID_COLUMN_CARD_ID).WidthFixed = 2000
      .Column(CARDS_GRID_COLUMN_CARD_ID).Fixed = True
      .Column(CARDS_GRID_COLUMN_CARD_ID).Editable = False
      .Column(CARDS_GRID_COLUMN_CARD_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Card Type
      .Column(CARDS_GRID_COLUMN_CARD_TYPE).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(315)
      .Column(CARDS_GRID_COLUMN_CARD_TYPE).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(308)
      .Column(CARDS_GRID_COLUMN_CARD_TYPE).WidthFixed = 1400
      .Column(CARDS_GRID_COLUMN_CARD_TYPE).Fixed = True
      .Column(CARDS_GRID_COLUMN_CARD_TYPE).Editable = False
      .Column(CARDS_GRID_COLUMN_CARD_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Card's number of prizes
      .Column(CARDS_GRID_COLUMN_NUM_CARD_PRIZES).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(315)
      .Column(CARDS_GRID_COLUMN_NUM_CARD_PRIZES).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(318)
      .Column(CARDS_GRID_COLUMN_NUM_CARD_PRIZES).WidthFixed = 1355
      .Column(CARDS_GRID_COLUMN_NUM_CARD_PRIZES).Fixed = True
      .Column(CARDS_GRID_COLUMN_NUM_CARD_PRIZES).Editable = False
      .Column(CARDS_GRID_COLUMN_NUM_CARD_PRIZES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

  End Sub

  Private Sub InitPrizes(ByVal IdxScreenPlay As Integer)

    ' Cards list has just two columns to hold the card id and the card type
    With Me.dg_play_card_prizes.Item(IdxScreenPlay)
      .Init(PRIZES_GRID_COLUMNS, PRIZES_GRID_HEADER_ROWS)

      .Counter(0).Visible = True
      .Sortable = False

      ' Prize Amount
      .Column(PRIZES_GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(319)
      .Column(PRIZES_GRID_COLUMN_AMOUNT).WidthFixed = 1535
      .Column(PRIZES_GRID_COLUMN_AMOUNT).Fixed = True
      .Column(PRIZES_GRID_COLUMN_AMOUNT).Editable = False
      .Column(PRIZES_GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

  End Sub

  Protected Overrides Sub GUI_InitControls()

    Dim idx_play As Integer

    ' Required by the base class
    MyBase.GUI_InitControls()

    ' Control collections to hold the plays
    ef_play_game.Add(ef_play_a_game)
    ef_play_game.Add(ef_play_b_game)
    ef_play_terminal.Add(ef_play_a_terminal)
    ef_play_terminal.Add(ef_play_b_terminal)
    ef_play_date.Add(ef_play_a_date)
    ef_play_date.Add(ef_play_b_date)
    ef_play_denomination.Add(ef_play_a_denomination)
    ef_play_denomination.Add(ef_play_b_denomination)
    ef_play_played_credits.Add(ef_play_a_played_credits)
    ef_play_played_credits.Add(ef_play_b_played_credits)
    ef_play_jackpot_bound_credits.Add(ef_play_a_jackpot_bound_credits)
    ef_play_jackpot_bound_credits.Add(ef_play_b_jackpot_bound_credits)
    ef_play_won_credits.Add(ef_play_a_won_credits)
    ef_play_won_credits.Add(ef_play_b_won_credits)

    ' Play's jackpot
    pan_play_jackpot.Add(pan_play_a_jackpot)
    pan_play_jackpot.Add(pan_play_b_jackpot)
    lbl_play_jackpot_title.Add(lbl_play_a_jackpot_title)
    lbl_play_jackpot_title.Add(lbl_play_b_jackpot_title)
    ef_play_jackpot_name.Add(ef_play_a_jackpot_name)
    ef_play_jackpot_name.Add(ef_play_b_jackpot_name)
    ef_play_jackpot_index.Add(ef_play_a_jackpot_index)
    ef_play_jackpot_index.Add(ef_play_b_jackpot_index)
    ef_play_jackpot_amount.Add(ef_play_a_jackpot_amount)
    ef_play_jackpot_amount.Add(ef_play_b_jackpot_amount)

    ' Play's card
    dg_play_cards.Add(dg_play_a_cards)
    dg_play_cards.Add(dg_play_b_cards)
    dg_play_card_prizes.Add(dg_play_a_card_prizes)
    dg_play_card_prizes.Add(dg_play_b_card_prizes)

    lbl_play_title.Add(lbl_play_a_title)
    lbl_play_title.Add(lbl_play_b_title)
    lbl_play_card_title.Add(lbl_play_a_card_title)
    lbl_play_card_title.Add(lbl_play_b_card_title)

    btn_next_play.Add(btn_play_a_next_play)
    btn_next_play.Add(btn_play_b_next_play)

    pan_play.Add(pan_play_a)
    pan_play.Add(pan_play_b)

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = False

    Me.Text = GLB_NLS_GUI_CLASS_II.GetString(300)

    ef_draw_date.Text = GLB_NLS_GUI_CLASS_II.GetString(302)
    ef_num_plays.Text = GLB_NLS_GUI_CLASS_II.GetString(304)

    InitDrawNumbers()

    For idx_play = 0 To NUM_PLAYS_DISPLAYED - 1
      ef_play_game.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(305)
      ef_play_terminal.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(306)
      lbl_play_title.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(309, " ")
      ef_play_date.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(314)
      ef_play_denomination.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(310)
      ef_play_played_credits.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(311)
      ef_play_jackpot_bound_credits.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(312)
      ef_play_won_credits.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(313)

      lbl_play_jackpot_title.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(320)
      ef_play_jackpot_name.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(323)
      ef_play_jackpot_index.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(321)
      ef_play_jackpot_amount.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(322)

      btn_next_play.Item(idx_play).Text = GLB_NLS_GUI_CLASS_II.GetString(316)

      InitCards(idx_play)
      InitCardNumbers(idx_play)
      InitPrizes(idx_play)
    Next

    ' Disable username entry field in edition mode.
    ' Note: In edition mode we set onlyUpperCase to false because existing users or users
    ' created or altered out of this form could be in lowercase and then there will be
    ' a change in the username which will be audited but not updated in database.
    ef_draw_date.IsReadOnly = True
    ef_num_plays.IsReadOnly = True

    For idx_play = 0 To NUM_PLAYS_DISPLAYED - 1
      ef_play_game.Item(idx_play).IsReadOnly = True
      ef_play_terminal.Item(idx_play).IsReadOnly = True
      ef_play_date.Item(idx_play).IsReadOnly = True
      ef_play_denomination.Item(idx_play).IsReadOnly = True
      ef_play_played_credits.Item(idx_play).IsReadOnly = True
      ef_play_jackpot_bound_credits.Item(idx_play).IsReadOnly = True
      ef_play_won_credits.Item(idx_play).IsReadOnly = True
    Next

  End Sub 'GUI_InitControls

  Private Function GetSelectedPrize(ByVal IdxScreenPlay As Integer, _
                                    ByRef IdxCardPrize As Integer) As Boolean
    Dim idx_prize As Int32

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      GetSelectedPrize = False

      Exit Function
    End If

    ' Get the selected row from the card list
    For idx_prize = 0 To dg_play_card_prizes.Item(IdxScreenPlay).NumRows - 1
      If dg_play_card_prizes.Item(IdxScreenPlay).Row(idx_prize).IsSelected Then
        IdxCardPrize = idx_prize

        GetSelectedPrize = True

        Exit Function
      End If
    Next

    GetSelectedPrize = False

  End Function

  Private Function GetSelectedCard(ByVal IdxScreenPlay As Integer, _
                                   ByRef IdxCard As Integer) As Boolean
    Dim idx_card As Int32

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      GetSelectedCard = False

      Exit Function
    End If

    ' Get the selected row from the card list
    For idx_card = 0 To dg_play_cards.Item(IdxScreenPlay).NumRows - 1
      If dg_play_cards.Item(IdxScreenPlay).Row(idx_card).IsSelected Then
        IdxCard = idx_card

        GetSelectedCard = True

        Exit Function
      End If
    Next

    GetSelectedCard = False

  End Function

  Private Sub ShowPrizeNumbers(ByVal IdxScreenPlay As Integer, _
                               ByVal IdxDataPlay As Integer, _
                               ByVal IdxDataCard As Integer, _
                               ByVal IdxDataPrize As Integer)

    Dim draw_audit As CLASS_DRAW_AUDIT
    Dim idx_number As Integer
    Dim idx_pos As Integer
    Dim idx_label As Integer
    Dim highlighted_number As Boolean

    ' Check out the parameters
    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    draw_audit = DbReadObject

    If IdxDataPlay < 0 Or IdxDataPlay > draw_audit.NumPlays - 1 Then
      Return
    End If

    If IdxDataCard < 0 Or IdxDataCard > draw_audit.Play(IdxDataPlay).num_cards - 1 Then
      Return
    End If

    If IdxDataPrize < 0 Or IdxDataPrize > draw_audit.Play(IdxDataPlay).cards(IdxDataCard).num_card_prizes - 1 Then
      Return
    End If

    ' Highlight the winning numbers of the selected prize on the card
    With draw_audit.Play(IdxDataPlay).cards(IdxDataCard)    ' Selected card
      ' Browse all the card's numbers 
      For idx_number = 0 To .num_numbers - 1
        ' Check whether the number should be highlighted 
        highlighted_number = False
        For idx_pos = 0 To .prizes(IdxDataPrize).num_positions - 1
          If idx_number = .prizes(IdxDataPrize).position_list(idx_pos) Then
            highlighted_number = True

            Exit For
          End If
        Next

        ' Set the right color
        idx_label = IdxScreenPlay * CLASS_DRAW_AUDIT.NUMBERS_PER_CARD + idx_number

        If highlighted_number Then
          lbl_card_number.Item(idx_label).ForeColor = GetColor(COLOR_FORE_CARD_WINNING_NUMBER)
          lbl_card_number.Item(idx_label).BackColor = GetColor(BACK_COLOR_CARD_WINNING_NUMBER)
        Else
          lbl_card_number.Item(idx_label).ForeColor = GetColor(FORE_COLOR_CARD_NUMBER)
          lbl_card_number.Item(idx_label).BackColor = GetColor(BACK_COLOR_CARD_NUMBER)
        End If
      Next
    End With

  End Sub

  Private Sub ShowPrizes(ByVal IdxScreenPlay As Integer, _
                         ByVal IdxDataPlay As Integer, _
                         ByVal IdxDataCard As Integer)

    Dim draw_audit As CLASS_DRAW_AUDIT
    Dim idx_prize As Integer

    ' Check out the parameters
    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    draw_audit = DbReadObject

    If IdxDataPlay < 0 Or IdxDataPlay > draw_audit.NumPlays - 1 Then
      Return
    End If

    If IdxDataCard < 0 Or IdxDataCard > draw_audit.Play(IdxDataPlay).num_cards - 1 Then
      Return
    End If

    ' Populate play card prizes list
    With dg_play_card_prizes.Item(IdxScreenPlay)
      .Redraw = False
      .Clear()

      For idx_prize = 0 To draw_audit.Play(IdxDataPlay).cards(IdxDataCard).num_card_prizes - 1
        .AddRow()
        .Cell(.NumRows - 1, PRIZES_GRID_COLUMN_AMOUNT).Value = GUI_FormatNumber(draw_audit.Play(IdxDataPlay).cards(IdxDataCard).prizes(idx_prize).prize_credits, 0).ToString
      Next

      .Redraw = True
      .SelectFirstRow(True)
    End With

    ' Display the Prizes of the first card
    ShowPrizeNumbers(IdxScreenPlay, IdxDataPlay, IdxDataCard, 0)

  End Sub

  Private Sub ShowCard(ByVal IdxScreenPlay As Integer, _
                       ByVal IdxDataPlay As Integer, _
                       ByVal IdxDataCard As Integer)

    Dim draw_audit As CLASS_DRAW_AUDIT
    Dim idx_number As Integer
    Dim idx_label As Integer

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    draw_audit = DbReadObject

    If IdxDataPlay < 0 Or IdxDataPlay > draw_audit.NumPlays - 1 Then
      Return
    End If

    ' Save the index of the data displayed in each panel
    m_idx_data_card(IdxScreenPlay) = IdxDataCard

    ' Display the numbers of the selected card
    lbl_play_card_title.Item(IdxScreenPlay).Text = GLB_NLS_GUI_CLASS_II.GetString(317, draw_audit.Play(IdxDataPlay).cards(IdxDataCard).id.Trim)

    For idx_number = 0 To draw_audit.Play(IdxDataPlay).cards(IdxDataCard).num_numbers - 1
      idx_label = IdxScreenPlay * CLASS_DRAW_AUDIT.NUMBERS_PER_CARD + idx_number
      lbl_card_number.Item(idx_label).Text = draw_audit.Play(IdxDataPlay).cards(IdxDataCard).numbers(idx_number)
      lbl_card_number.Item(idx_label).ForeColor = GetColor(FORE_COLOR_CARD_NUMBER)
      lbl_card_number.Item(idx_label).BackColor = GetColor(BACK_COLOR_CARD_NUMBER)
      lbl_card_number.Item(idx_label).Visible = True
    Next

    For idx_number = idx_number + 1 To CLASS_DRAW_AUDIT.NUMBERS_PER_CARD - 1
      idx_label = IdxScreenPlay * CLASS_DRAW_AUDIT.NUMBERS_PER_CARD + idx_number
      lbl_card_number.Item(idx_label).Visible = False
      lbl_card_number.Item(idx_label).Text = ""
      lbl_card_number.Item(idx_label).ForeColor = GetColor(FORE_COLOR_CARD_NUMBER)
      lbl_card_number.Item(idx_label).BackColor = GetColor(BACK_COLOR_CARD_NUMBER)
    Next

    ' Display the Prizes of the first card
    ShowPrizes(IdxScreenPlay, IdxDataPlay, IdxDataCard)

  End Sub

  Private Sub ShowPlay(ByVal IdxScreenPlay As Integer, _
                       ByVal IdxDataPlay As Integer)

    Dim draw_audit As CLASS_DRAW_AUDIT
    Dim idx_card As Integer

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    draw_audit = DbReadObject

    If IdxDataPlay < 0 Or IdxDataPlay > draw_audit.NumPlays - 1 Then
      ' Empty title
      lbl_play_card_title.Item(IdxScreenPlay).Text = GLB_NLS_GUI_CLASS_II.GetString(317, " ")

      Return
    End If

    ' Save the index of the data displayed in each panel
    m_idx_data_play(IdxScreenPlay) = IdxDataPlay

    lbl_play_title.Item(IdxScreenPlay).Text = GLB_NLS_GUI_CLASS_II.GetString(309, (IdxDataPlay + 1).ToString)

    ef_play_game.Item(IdxScreenPlay).Value = draw_audit.Play(IdxDataPlay).game_name
    ef_play_terminal.Item(IdxScreenPlay).Value = draw_audit.Play(IdxDataPlay).terminal_name
    ef_play_date.Item(IdxScreenPlay).Value = GUI_FormatDate(draw_audit.Play(IdxDataPlay).play_date.Value, _
                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                            ENUM_FORMAT_TIME.FORMAT_TIME_LONG)
    ef_play_denomination.Item(IdxScreenPlay).Value = GUI_FormatCurrency(draw_audit.Play(IdxDataPlay).denomination)
    ef_play_played_credits.Item(IdxScreenPlay).Value = GUI_FormatNumber(draw_audit.Play(IdxDataPlay).played_credits, 0)
    ef_play_jackpot_bound_credits.Item(IdxScreenPlay).Value = GUI_FormatNumber(draw_audit.Play(IdxDataPlay).jackpot_bound_credits, 0)
    ef_play_won_credits.Item(IdxScreenPlay).Value = GUI_FormatNumber(draw_audit.Play(IdxDataPlay).won_credits, 0)

    ' Jackpot Information
    If draw_audit.Play(IdxDataPlay).jackpot.awarded Then
      ef_play_jackpot_name.Item(IdxScreenPlay).Value = draw_audit.Play(IdxDataPlay).jackpot.name.Trim
      ef_play_jackpot_index.Item(IdxScreenPlay).Value = draw_audit.Play(IdxDataPlay).jackpot.index.ToString
      ef_play_jackpot_amount.Item(IdxScreenPlay).Value = GUI_FormatCurrency(draw_audit.Play(IdxDataPlay).jackpot.amount)

      pan_play_jackpot.Item(IdxScreenPlay).Visible = True
      lbl_play_jackpot_title.Item(IdxScreenPlay).Visible = True
      ef_play_jackpot_name.Item(IdxScreenPlay).Visible = True
      ef_play_jackpot_index.Item(IdxScreenPlay).Visible = True
      ef_play_jackpot_amount.Item(IdxScreenPlay).Visible = True
    Else
      pan_play_jackpot.Item(IdxScreenPlay).Visible = False
      lbl_play_jackpot_title.Item(IdxScreenPlay).Visible = False
      ef_play_jackpot_name.Item(IdxScreenPlay).Visible = False
      ef_play_jackpot_index.Item(IdxScreenPlay).Visible = False
      ef_play_jackpot_amount.Item(IdxScreenPlay).Visible = False
    End If

    ' Populate play cards list
    With dg_play_cards.Item(IdxScreenPlay)
      .Redraw = False
      .Clear()

      For idx_card = 0 To draw_audit.Play(IdxDataPlay).num_cards - 1
        .AddRow()
        .Cell(.NumRows - 1, CARDS_GRID_COLUMN_CARD_ID).Value = draw_audit.Play(IdxDataPlay).cards(idx_card).id.ToString()
        .Cell(.NumRows - 1, CARDS_GRID_COLUMN_CARD_TYPE).Value = draw_audit.Play(IdxDataPlay).cards(idx_card).type
        .Cell(.NumRows - 1, CARDS_GRID_COLUMN_NUM_CARD_PRIZES).Value = draw_audit.Play(IdxDataPlay).cards(idx_card).num_card_prizes.ToString
      Next

      .Redraw = True
      .SelectFirstRow(True)
    End With

    ShowSelectedCard(IdxScreenPlay)    ' Screen Play Index 

  End Sub

  Private Sub ShowNextPlay(ByVal IdxScreenPlay As Integer)

    Dim draw_audit As CLASS_DRAW_AUDIT
    Dim idx_data_play As Integer
    Dim idx_screen_play As Integer
    Dim already_displayed As Boolean

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If


    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    draw_audit = DbReadObject

    ' Check whether there are more than NUM_PLAYS_DISPLAYED (2) plays for the draw
    If draw_audit.NumPlays <= NUM_PLAYS_DISPLAYED Then
      Return
    End If

    ' Start with current data
    idx_data_play = m_idx_data_play(IdxScreenPlay)

    While True
      idx_data_play = (idx_data_play + 1) Mod draw_audit.NumPlays

      ' Check whether the data is already displayed 
      already_displayed = False
      For idx_screen_play = 0 To NUM_PLAYS_DISPLAYED - 1
        If m_idx_data_play(idx_screen_play) = idx_data_play Then
          already_displayed = True

          Exit For
        End If
      Next

      ' Next data found: not displayed yet
      If Not already_displayed Then
        Exit While
      End If
    End While

    ShowPlay(IdxScreenPlay, idx_data_play)

  End Sub

  Private Sub ShowSelectedPrize(ByVal IdxScreenPlay As Integer)

    Dim idx_prize As Int32

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    If GetSelectedPrize(IdxScreenPlay, idx_prize) Then
      ShowPrizeNumbers(IdxScreenPlay, _
                       m_idx_data_play(IdxScreenPlay), _
                       m_idx_data_card(IdxScreenPlay), _
                       idx_prize)
    End If

  End Sub

  Private Sub ShowSelectedCard(ByVal IdxScreenPlay As Integer)

    Dim idx_card As Int32

    If IdxScreenPlay < 0 Or IdxScreenPlay > NUM_PLAYS_DISPLAYED - 1 Then
      Return
    End If

    If GetSelectedCard(IdxScreenPlay, idx_card) Then
      ShowCard(IdxScreenPlay, _
               m_idx_data_play(IdxScreenPlay), _
               idx_card)
    End If

  End Sub

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim draw_audit As CLASS_DRAW_AUDIT
    Dim idx_play As Integer
    Dim idx_number As Integer

    draw_audit = DbReadObject

    lbl_draw_title.Text = GLB_NLS_GUI_CLASS_II.GetString(301, draw_audit.DrawId.ToString)
    ef_draw_date.Value = GUI_FormatDate(draw_audit.DrawDate, _
                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                        ENUM_FORMAT_TIME.FORMAT_TIME_LONG)

    lbl_draw_numbers_title.Text = GLB_NLS_GUI_CLASS_II.GetString(303, draw_audit.NumWinningNumbers.ToString)

    ' Draw winning numbers
    For idx_number = 0 To draw_audit.NumWinningNumbers - 1
      With lbl_draw_number.Item(idx_number)
        .Text = draw_audit.WinningNumber(idx_number).ToString

        .ForeColor = GetColor(FORE_COLOR_DRAW_NUMBER)
        .BackColor = GetColor(BACK_COLOR_DRAW_NUMBER)

        .Visible = True
      End With
    Next

    ' Number of plays
    ef_num_plays.Value = draw_audit.NumPlays.ToString

    ' Clear selected data indexes
    For idx_play = 0 To NUM_PLAYS_DISPLAYED - 1
      m_idx_data_play(idx_play) = -1
      m_idx_data_card(idx_play) = -1
    Next

    For idx_play = 0 To NUM_PLAYS_DISPLAYED - 1
      ' show the play details
      ShowPlay(idx_play, idx_play)

      ' Hide "Next Play" button if all the plays can be displayed at a time
      If draw_audit.NumPlays <= NUM_PLAYS_DISPLAYED Then
        btn_next_play.Item(idx_play).Visible = False
      End If
    Next

  End Sub ' GUI_SetScreenData

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function 'GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim draw_audit As CLASS_DRAW_AUDIT

    draw_audit = DbEditedObject

  End Sub 'GUI_GetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim draw_audit As CLASS_DRAW_AUDIT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_DRAW_AUDIT

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' Cast on the DB Object to get its ID 
          draw_audit = DbEditedObject

          ' 121 - "Error reading audit data related to Draw Id=%1"
          NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(121), _
                     ENUM_MB_TYPE.MB_TYPE_ERROR, _
                     ENUM_MB_BTN.MB_BTN_OK, _
                     ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                     CStr(draw_audit.DrawId))
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        Me.DbReadObject = Me.DbEditedObject

      Case Else
        MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub 'GUI_DB_Operation

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub 'GUI_Permissions

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.btn_play_a_next_play

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - DrawId
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal DrawId As Int64)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = DrawId

    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If
  End Sub 'ShowEditItem

#End Region


#Region " Events "

#End Region

  Private Sub dg_play_a_cards_DataSelectedEvent() Handles dg_play_a_cards.DataSelectedEvent

    ShowSelectedCard(0)          ' Screen Play Index 

  End Sub

  Private Sub dg_play_b_cards_DataSelectedEvent() Handles dg_play_b_cards.DataSelectedEvent

    ShowSelectedCard(1)          ' Screen Play Index 

  End Sub

  Private Sub dg_play_a_card_prizes_DataSelectedEvent() Handles dg_play_a_card_prizes.DataSelectedEvent

    ShowSelectedPrize(0)    ' Screen Play Index 

  End Sub

  Private Sub dg_play_b_card_prizes_DataSelectedEvent() Handles dg_play_b_card_prizes.DataSelectedEvent

    ShowSelectedPrize(1)    ' Screen Play Index 

  End Sub

  Private Sub btn_play_a_next_play_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_play_a_next_play.Click

    ShowNextPlay(0)         ' Screen Play Index 

  End Sub

  Private Sub btn_play_b_next_play_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_play_b_next_play.Click

    ShowNextPlay(1)         ' Screen Play Index 

  End Sub

End Class
