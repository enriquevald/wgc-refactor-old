'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_area_island_edit
' DESCRIPTION:   Selection/Update/Insert of Areas and Island
' AUTHOR:        Ra�l Cervera
' CREATION DATE: 20-OCT-2011
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 20-OCT-2011 RCI    First Release
' 16-FEB-2012 SSC    Add Providers - Terminals TreeView and modify filters and functionalities
' 20-MAR-2013 LEM    Fixed Bug #652: Database error if terminal name include character ('). 
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_area_island_edit
  Inherits GUI_Controls.frm_base_print

#Region " Constants "

  Private Const COLUMN_AREA_ID As Integer = 0
  Private Const COLUMN_AREA_NAME As Integer = 1
  Private Const COLUMN_AREA_SMOKING As Integer = 2
  Private Const COLUMN_AREA_VERSION_NUMBER As Integer = 3

  Private Const COLUMN_ISLAND_ID As Integer = 0
  Private Const COLUMN_ISLAND_NAME As Integer = 1
  Private Const COLUMN_ISLAND_AREA_ID As Integer = 2
  Private Const COLUMN_ISLAND_AREA_NAME As Integer = 3
  Private Const COLUMN_ISLAND_VERSION_NUMBER As Integer = 4

  Private Const COLUMN_TERMINAL_ID As Integer = 0
  Private Const COLUMN_TERMINAL_FLOOR_ID As Integer = 1
  Private Const COLUMN_TERMINAL_PROVIDER_ID As Integer = 2
  Private Const COLUMN_TERMINAL_NAME As Integer = 3
  Private Const COLUMN_TERMINAL_BANK_ID As Integer = 4
  Private Const COLUMN_TERMINAL_BANK_NAME As Integer = 5
  Private Const COLUMN_TERMINAL_VERSION_NUMBER As Integer = 6

  Private Const COLUMN_PROVIDER_ID As Integer = 1
  Private Const COLUMN_PROVIDER_NAME As Integer = 1

  Private Const COLUMN_PRINT_AREA_NAME As Integer = 0
  Private Const COLUMN_PRINT_AREA_SMOKING As Integer = 1
  Private Const COLUMN_PRINT_ISLAND_NAME As Integer = 2
  Private Const COLUMN_PRINT_TERMINAL_PROVIDER_ID As Integer = 3
  Private Const COLUMN_PRINT_TERMINAL_ID As Integer = 4
  Private Const COLUMN_PRINT_TERMINAL_NAME As Integer = 5

  Private Const TABLE_AREA_NAME As String = "Areas"
  Private Const TABLE_ISLAND_NAME As String = "Islands"
  Private Const TABLE_TERMINAL_NAME As String = "Terminals"

  Private Const TABLE_PROVIDER_NAME As String = "Providers"

  Private Const RELATION_AREA_ISLAND As String = "Areas_Islands"
  Private Const RELATION_ISLAND_TERMINAL As String = "Islands_Terminals"

  Private Const RELATION_PROVIDER_TERMINAL As String = "Providers_Terminals"

  Private Const DEFAULT_AREA_ID As Integer = 1

#End Region ' Constants

#Region " Enums "

  Private Enum ENUM_TABLES
    AREAS = 0
    ISLANDS = 1
    TERMINALS = 2
  End Enum

  Private Enum ENUM_TABLES_NO_BANK
    PROVIDERS = 0
    TERMINALS = 1
  End Enum

  Private Enum TREE_LEVEL
    ROOT = 0
    AREAS = 1
    ISLANDS = 2
    TERMINALS = 3
  End Enum

  Private Enum TREE_LEVEL_NO_BANK
    ROOT = 0
    PROVIDER = 1
    TERMINALS = 2
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_ds_areas As DataSet = Nothing
  Private m_ds_providers As DataSet = Nothing

  Private m_bs_areas As BindingSource = Nothing
  Private m_bs_islands As BindingSource = Nothing
  Private m_bs_terminals As BindingSource = Nothing

  Private m_bs_providers As BindingSource = Nothing
  Private m_bs_terminals_no_bank As BindingSource = Nothing

  Private m_last_drag_dest As TreeNode = Nothing
  Private m_last_drag_dest_time As DateTime
  Private m_last_drag_dest_already As Boolean

  ''''Private m_first_time = True
  Private m_version_number As Integer

  ' For report filters
  Private m_dt_printing As DataTable
  Private m_report_area_name As String
  Private m_report_island_name As String
  Private m_report_terminal_name As String
  Private m_report_smoking As String
  Private m_report_terminals As String

  Private m_tree_view_islands As TreeView

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINALS_SELECTION ' TODO!! Add new ENUM_FORM.

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If
  End Sub ' GUI_Closing

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_CONTROLS.GetString(425)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(28)

    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.chk_smoking_yes.Text = GLB_NLS_GUI_CONTROLS.GetString(283)
    Me.chk_smoking_no.Text = GLB_NLS_GUI_CONTROLS.GetString(284)
    Me.gb_smoking.Text = GLB_NLS_GUI_CONTROLS.GetString(428)
    Me.ef_area_name.Text = GLB_NLS_GUI_CONTROLS.GetString(426)
    Me.ef_island_name.Text = GLB_NLS_GUI_CONTROLS.GetString(427)
    Me.ef_terminal_name.Text = GLB_NLS_GUI_AUDITOR.GetString(333)

    ''''Me.lbl_areas.Text = GLB_NLS_GUI_CONTROLS.GetString(429)
    ''''Me.lbl_islands.Text = GLB_NLS_GUI_CONTROLS.GetString(430)
    ''''Me.lbl_terminals.Text = GLB_NLS_GUI_CONTROLS.GetString(430)

    Me.ef_area_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_island_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_terminal_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    CreateDataTableForPrinting()
    m_version_number = 1

    ' Set filter default values
    Call SetDefaultValues()

    Call AddHandles()

  End Sub ' GUI_InitControls

  ' PURPOSE: First screen activation actions:
  '           - Load data from DB
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_FirstActivation()
    Call GUI_ExecuteQuery()

    ''''m_first_time = False

  End Sub ' GUI_FirstActivation

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

    ' Button Reset, also filter data and show results.
    Call GUI_FilterData()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = ef_area_name

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT :
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_FilterData()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_SaveChanges()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call GUI_AddTerminalsToIsland()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ExecuteQuery()

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Check the filter
      If Not GUI_FilterCheck() Then
        Return
      End If

      ' If pending changes MsgBox
      If PendingChanges() Then
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return
        End If
      End If

      ' Invalidate datagrid
      GUI_ClearGrid()

      ' Execute the query        
      Call ExecuteQuery()

      Call GUI_FilterData()

    Catch exception As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "AreaIslandSel", "FilterApply", _
                            "", "", "", exception.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub 'GUI_ExecuteQuery

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT :
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _dg_data_print As DataGrid

    ' For report filters
    _dg_data_print = PrepareDataToPrint()
    Call GUI_ReportUpdateFilters(_dg_data_print)

    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(429), m_report_area_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(430), m_report_island_name)
    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(428), m_report_smoking)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT :
  '           - MyDataGrid As DataGrid
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters(ByVal MyDataGrid As DataGrid)

    Call MyBase.GUI_ReportUpdateFilters(MyDataGrid)

    m_report_island_name = Me.ef_island_name.Value
    m_report_area_name = Me.ef_area_name.Value
    m_report_terminal_name = Me.ef_terminal_name.Value

    ' Providers - Terminals
    m_report_terminals = Me.uc_pr_list.GetTerminalReportText()

    If Me.chk_smoking_yes.Checked And Me.chk_smoking_no.Checked Then
      m_report_smoking = ""
    ElseIf Me.chk_smoking_yes.Checked Then
      m_report_smoking = GLB_NLS_GUI_AUDITOR.GetString(336)
    ElseIf Me.chk_smoking_no.Checked Then
      m_report_smoking = GLB_NLS_GUI_AUDITOR.GetString(337)
    Else
      m_report_smoking = ""
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public functions "
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Make modal.
    Me.MdiParent = Nothing
    Me.Display(True)

  End Sub ' ShowForEdit

#End Region ' Public functions

#Region " Private Functions "

  ' PURPOSE: Filter data in memory
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_FilterData()

    m_version_number += 1

    m_bs_areas.Filter = GetFilter_Areas()
    m_bs_islands.Filter = GetFilter_Islands()
    m_bs_terminals.Filter = GetFilter_Terminals()
    'm_bs_providers.Filter = GetFilter_Providers()

  End Sub ' GUI_FilterData

  ' PURPOSE: Get the filter for Areas
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetFilter_Areas()
    Dim _filter As String = ""

    If Me.chk_smoking_yes.Checked And Not Me.chk_smoking_no.Checked Then
      _filter &= " AND AR_SMOKING = 1 "
    End If
    If Not Me.chk_smoking_yes.Checked And Me.chk_smoking_no.Checked Then
      _filter &= " AND AR_SMOKING = 0 "
    End If

    If Me.ef_area_name.Value <> "" Then
      _filter &= " AND AR_NAME LIKE '%" & Me.ef_area_name.Value & "%'"
    End If

    ' Final processing
    If _filter <> "" Then
      ' Discard the leading ' AND '
      _filter = Strings.Right(_filter, Len(_filter) - 5)

      ' Add version number filter
      _filter = " ( " & _filter & " ) OR AR_VERSION_NUMBER = " & m_version_number
    End If

    Return _filter
  End Function ' GetFilter_Areas

  ' PURPOSE: Get the filter for Islands
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetFilter_Islands() As String
    Dim _filter As String = ""

    If Me.ef_island_name.Value <> "" Then
      _filter = "IS_ISLAND_NAME LIKE '%" & Me.ef_island_name.Value & "%'"

      ' Add version number filter
      _filter = " ( " & _filter & " ) OR IS_VERSION_NUMBER = " & m_version_number
    End If

    Return _filter
  End Function ' GetFilter_Islands

  ' PURPOSE: Get the filter for Terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetFilter_Terminals() As String
    Dim _filter As String = ""
    Dim _provider_list As String() = Nothing
    Dim _terminal_list As Integer() = Nothing
    Dim _str_terminal_list As String() = Nothing
    Dim _first_term As Boolean
    Dim _filter_providers As String = ""
    Dim _filter_terminals As String = ""

    If Me.ef_terminal_name.Value <> "" Then
      _filter &= " AND TE_NAME LIKE '%" & Me.ef_terminal_name.Value & "%'"
    End If

    ' Filter Providers - Terminals
    If Not Me.uc_pr_list.AllSelectedChecked Then 'Me.uc_pr_list.opt_one_terminal.Checked Or Me.uc_pr_list.opt_several_types.Checked Then

      'TODO: JML 1-APR-2015 This function must be changed before it can be used.
      'Me.uc_pr_list.GetProviderAndTerminalIdListSelected(_provider_list, _terminal_list)

      If _provider_list IsNot Nothing Then
        _filter_providers = "TE_PROVIDER_ID IN ( '"
        _filter_providers &= String.Join("', '", _provider_list)
        _filter_providers &= "' )"
      End If

      If _terminal_list IsNot Nothing Then
        _first_term = True
        _filter_terminals = "TE_TERMINAL_ID IN ( "
        For Each _term As Integer In _terminal_list
          If Not _first_term Then
            _filter_terminals &= ", "
          Else
            _first_term = False
          End If
          _filter_terminals &= _term.ToString()
        Next
        _filter_terminals &= " )"
      End If

      If _filter_providers <> "" And _filter_terminals <> "" Then
        _filter &= " AND ( " & _filter_providers & " OR " & _filter_terminals & " ) "
      ElseIf _filter_providers <> "" And _filter_terminals = "" Then
        _filter &= " AND " & _filter_providers
      ElseIf _filter_providers = "" And _filter_terminals <> "" Then
        _filter &= " AND " & _filter_terminals
      End If
    End If

    ' Final processing
    If _filter <> "" Then
      ' Discard the leading ' AND '
      _filter = Strings.Right(_filter, Len(_filter) - 5)

      ' Add version number filter
      _filter = " ( " & _filter & " ) OR TE_VERSION_NUMBER = " & m_version_number
    End If

    Return _filter
  End Function ' GetFilter_Terminals

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    ef_area_name.Value = ""
    ef_island_name.Value = ""
    ef_terminal_name.Value = ""

    Call Me.uc_pr_list.SetDefaultValues()

    chk_smoking_yes.Checked = False
    chk_smoking_no.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Check if changes have been made in the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Changes have been made. False: Otherwise
  '
  Private Function PendingChanges() As Boolean

    Dim _dt_changes As DataTable

    If m_ds_areas Is Nothing Then
      Return False
    End If

    For Each _table As DataTable In m_ds_areas.Tables
      _dt_changes = _table.GetChanges()
      If _dt_changes IsNot Nothing Then
        If _dt_changes.Rows.Count > 0 Then
          Return True
        End If
      End If
    Next

    Return False
  End Function ' PendingChanges

  ' PURPOSE: Ask user to discard changes or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Discard changes. False: Otherwise
  '
  Private Function DiscardChanges() As Boolean

    ' If pending changes MsgBox
    If PendingChanges() Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    Return True
  End Function ' DiscardChanges

  ' PURPOSE: Save DataGridView data to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub GUI_SaveChanges()
    Dim _dt_areas As DataTable
    Dim _dt_islands As DataTable
    Dim _dt_terminals As DataTable
    Dim _dt_changes_areas As DataTable
    Dim _dt_changes_islands As DataTable
    Dim _dt_changes_terminals As DataTable
    Dim _rows_areas As DataRow()
    Dim _rows_islands As DataRow()
    Dim _rows_terminals As DataRow()
    Dim _sql_trx As SqlTransaction
    Dim _sql_str_insert As String
    Dim _sql_str_update As String
    Dim _sql_cmd As SqlCommand
    Dim _parameter As SqlParameter
    Dim _num_rows_updated As Integer
    Dim _msg_error As String
    Dim _really_changed As Boolean

    _msg_error = ""

    Try
      ' Always must be 3 tables: One for Areas, other for Islands and other for Terminals
      If m_ds_areas.Tables.Count < 3 Then
        Return
      End If

      _dt_areas = m_ds_areas.Tables(ENUM_TABLES.AREAS)
      _dt_islands = m_ds_areas.Tables(ENUM_TABLES.ISLANDS)
      _dt_terminals = m_ds_areas.Tables(ENUM_TABLES.TERMINALS)

      _dt_changes_areas = _dt_areas.GetChanges()
      _dt_changes_islands = _dt_islands.GetChanges()
      _dt_changes_terminals = _dt_terminals.GetChanges()

      ' If nothing has changed, exit sub.
      If _dt_changes_areas Is Nothing And _
         _dt_changes_islands Is Nothing And _
         _dt_changes_terminals Is Nothing Then
        Return
      End If

      ' Trim spaces from String values.
      _rows_areas = _dt_areas.Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
      _rows_islands = _dt_islands.Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
      _rows_terminals = _dt_terminals.Select("", "", DataViewRowState.ModifiedCurrent)
      Call TrimDataValues(_rows_areas)
      Call TrimDataValues(_rows_islands)
      Call TrimDataValues(_rows_terminals)

      If Not GUI_IsDataChangedOk() Then
        Return
      End If

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction

        If _rows_areas.Length > 0 Then
          _sql_str_insert = "INSERT INTO AREAS ( AR_NAME " & _
                            "                  , AR_SMOKING " & _
                            "                  ) " & _
                            "           VALUES ( @pAreaName " & _
                            "                  , @pSmoking " & _
                            "                  ) " & _
                            " SET   @pAreaId = SCOPE_IDENTITY() "

          _sql_str_update = "UPDATE   AREAS " & _
                            "   SET   AR_NAME = @pAreaName " & _
                            "       , AR_SMOKING   = @pSmoking " & _
                            " WHERE   AR_AREA_ID   = @pAreaId "

          Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
            With _sql_adap
              .SelectCommand = Nothing
              .DeleteCommand = Nothing

              _sql_cmd = New SqlCommand(_sql_str_insert, _sql_trx.Connection, _sql_trx)
              _sql_cmd.Parameters.Add("@pAreaName", SqlDbType.NVarChar).SourceColumn = "AR_NAME"
              _sql_cmd.Parameters.Add("@pSmoking", SqlDbType.Bit).SourceColumn = "AR_SMOKING"
              _parameter = _sql_cmd.Parameters.Add("@pAreaId", SqlDbType.Int)
              _parameter.SourceColumn = "AR_AREA_ID"
              _parameter.Direction = ParameterDirection.Output
              _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
              .InsertCommand = _sql_cmd

              _sql_cmd = New SqlCommand(_sql_str_update, _sql_trx.Connection, _sql_trx)
              _sql_cmd.Parameters.Add("@pAreaName", SqlDbType.NVarChar).SourceColumn = "AR_NAME"
              _sql_cmd.Parameters.Add("@pSmoking", SqlDbType.Bit).SourceColumn = "AR_SMOKING"
              _sql_cmd.Parameters.Add("@pAreaId", SqlDbType.Int).SourceColumn = "AR_AREA_ID"
              _sql_cmd.UpdatedRowSource = UpdateRowSource.None
              .UpdateCommand = _sql_cmd

              .UpdateBatchSize = 500

              _num_rows_updated = .Update(_rows_areas)

              If _rows_areas.Length <> _num_rows_updated Then
                _msg_error = GetMsgErrorFromRow(_rows_areas)

                Return
              End If
            End With
          End Using ' _sql_adap for Areas
        End If ' If _rows_areas.Length > 0

        If _rows_islands.Length > 0 Then
          _sql_str_insert = "INSERT INTO ISLANDS ( IS_ISLAND_NAME " & _
                            "                    , IS_AREA_ID " & _
                            "                    ) " & _
                            "             VALUES ( @pIslandName " & _
                            "                    , @pAreaId " & _
                            "                    ) " & _
                            " SET   @pIslandId = SCOPE_IDENTITY() "

          _sql_str_update = "UPDATE   ISLANDS " & _
                            "   SET   IS_ISLAND_NAME = @pIslandName " & _
                            "       , IS_AREA_ID     = @pAreaId " & _
                            " WHERE   IS_ISLAND_ID   = @pIslandId "

          Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
            With _sql_adap
              .SelectCommand = Nothing
              .DeleteCommand = Nothing

              _sql_cmd = New SqlCommand(_sql_str_insert, _sql_trx.Connection, _sql_trx)
              _sql_cmd.Parameters.Add("@pIslandName", SqlDbType.NVarChar).SourceColumn = "IS_ISLAND_NAME"
              _sql_cmd.Parameters.Add("@pAreaId", SqlDbType.Int).SourceColumn = "IS_AREA_ID"
              _parameter = _sql_cmd.Parameters.Add("@pIslandId", SqlDbType.Int)
              _parameter.SourceColumn = "IS_ISLAND_ID"
              _parameter.Direction = ParameterDirection.Output
              _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters
              .InsertCommand = _sql_cmd

              _sql_cmd = New SqlCommand(_sql_str_update, _sql_trx.Connection, _sql_trx)
              _sql_cmd.Parameters.Add("@pIslandName", SqlDbType.NVarChar).SourceColumn = "IS_ISLAND_NAME"
              _sql_cmd.Parameters.Add("@pAreaId", SqlDbType.Int).SourceColumn = "IS_AREA_ID"
              _sql_cmd.Parameters.Add("@pIslandId", SqlDbType.Int).SourceColumn = "IS_ISLAND_ID"
              _sql_cmd.UpdatedRowSource = UpdateRowSource.None
              .UpdateCommand = _sql_cmd

              .UpdateBatchSize = 500

              _num_rows_updated = .Update(_rows_islands)

              If _rows_islands.Length <> _num_rows_updated Then
                _msg_error = GetMsgErrorFromRow(_rows_islands)

                Return
              End If
            End With
          End Using ' _sql_adap for Islands
        End If ' If _rows_islands.Length > 0

        If _rows_terminals.Length > 0 Then
          _sql_str_update = "UPDATE  TERMINALS " & _
                            "   SET   TE_NAME        = @pTerminalName " & _
                            "       , TE_FLOOR_ID    = @pFloorId " & _
                            "       , TE_BANK_ID    = @pIslandId " & _
                            " WHERE   TE_TERMINAL_ID  = @pTerminalId "

          Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
            With _sql_adap
              .SelectCommand = Nothing
              .InsertCommand = Nothing
              .DeleteCommand = Nothing

              _sql_cmd = New SqlCommand(_sql_str_update, _sql_trx.Connection, _sql_trx)
              _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).SourceColumn = "TE_NAME"
              _sql_cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).SourceColumn = "TE_FLOOR_ID"
              _sql_cmd.Parameters.Add("@pIslandId", SqlDbType.Int).SourceColumn = "TE_BANK_ID"
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TE_TERMINAL_ID"
              _sql_cmd.UpdatedRowSource = UpdateRowSource.None
              .UpdateCommand = _sql_cmd

              .UpdateBatchSize = 500

              _num_rows_updated = .Update(_rows_terminals)

              If _rows_terminals.Length <> _num_rows_updated Then
                _msg_error = GetMsgErrorFromRow(_rows_terminals)

                Return
              End If
            End With
          End Using ' _sql_adap for Terminals
        End If ' If _rows_terminals.Length > 0

        _db_trx.Commit()
      End Using ' _db_trx

      ' Auditory: Use table _dt_changes, because it has the original values.
      _really_changed = False

      If _rows_areas.Length > 0 Then
        GUI_WriteAuditoryChangesAreas(_dt_changes_areas.Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added), _really_changed)
      End If
      If _rows_islands.Length > 0 Then
        GUI_WriteAuditoryChangesIslands(_dt_changes_islands.Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added), _really_changed)
      End If
      If _rows_terminals.Length > 0 Then
        GUI_WriteAuditoryChangesTerminals(_dt_changes_terminals.Select("", "", DataViewRowState.ModifiedCurrent), _really_changed)
      End If

      m_ds_areas.AcceptChanges()

      If _really_changed Then
        ' 108 "Los cambios se han guardado correctamente."
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(109), _
                             GLB_NLS_GUI_AUDITOR.GetString(109) & _ex.Message, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

    Finally
      ''''dgv_areas.Refresh()
      ''''dgv_islands.Refresh()
      ''''dgv_terminals.Refresh()
      If _msg_error <> "" Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , _msg_error)
      End If
    End Try

  End Sub ' GUI_SaveChanges

  ' PURPOSE: Check if data in the dataviews is ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok.
  '     - False: Data is NOT ok.

  Private Function GUI_IsDataChangedOk() As Boolean

    Dim _rows_area As DataRow()
    Dim _rows_island As DataRow()
    Dim _rows_terminal As DataRow()
    Dim _row As DataRow
    Dim _nls_string As String
    Dim _empty_or_null As Boolean

    _rows_area = m_ds_areas.Tables(ENUM_TABLES.AREAS).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
    For Each _row In _rows_area
      _row.ClearErrors()
    Next

    _rows_island = m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Select("", "", DataViewRowState.ModifiedCurrent Or DataViewRowState.Added)
    For Each _row In _rows_island
      _row.ClearErrors()
    Next

    _rows_terminal = m_ds_areas.Tables(ENUM_TABLES.TERMINALS).Select("", "", DataViewRowState.ModifiedCurrent)
    For Each _row In _rows_terminal
      _row.ClearErrors()
    Next

    For Each _row In _rows_area
      ' Null values are not allowed
      _empty_or_null = False
      If _row(COLUMN_AREA_NAME) Is DBNull.Value Then
        _empty_or_null = True
      ElseIf _row(COLUMN_AREA_NAME) = "" Then
        _empty_or_null = True
      End If

      If _empty_or_null Then
        ' 114 "Field %1 can't be empty"
        _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONTROLS.GetString(323))
        AssignRowError(_row, _nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If Not CheckDataRow_Areas(_row) Then
        Return False
      End If
    Next

    For Each _row In _rows_island
      ' Null values are not allowed
      _empty_or_null = False
      If _row(COLUMN_ISLAND_NAME) Is DBNull.Value Then
        _empty_or_null = True
      ElseIf _row(COLUMN_ISLAND_NAME) = "" Then
        _empty_or_null = True
      End If

      If _empty_or_null Then
        ' 114 "Field %1 can't be empty"
        _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_CONTROLS.GetString(323))
        AssignRowError(_row, _nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If Not CheckDataRow_Islands(_row) Then
        Return False
      End If
    Next

    For Each _row In _rows_terminal
      ' Null values are not allowed
      _empty_or_null = False
      If _row(COLUMN_TERMINAL_NAME) Is DBNull.Value Then
        _empty_or_null = True
      ElseIf _row(COLUMN_TERMINAL_NAME) = "" Then
        _empty_or_null = True
      End If

      If _empty_or_null Then
        ' 114 "Field %1 can't be empty"
        _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114), GLB_NLS_GUI_AUDITOR.GetString(333))
        AssignRowError(_row, _nls_string)
        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      If Not CheckDataRow_Terminals(_row) Then
        Return False
      End If
    Next

    Return True
  End Function ' GUI_IsDataChangedOk

  ' PURPOSE: Check Areas Data Row for constraints.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow: Row to check.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckDataRow_Areas(ByVal Row As DataRow) As Boolean
    Dim _data_view As DataView
    Dim _row_filter As String
    Dim _nls_string As String
    Dim _area_name As String

    _data_view = New DataView(Me.m_ds_areas.Tables(ENUM_TABLES.AREAS))
    _area_name = Row(COLUMN_AREA_NAME)

    ' Check AREA_NAME is Unique
    _row_filter = "AR_NAME = '" & _area_name.Replace("'", "''") & "'"
    _data_view.RowFilter = _row_filter

    If _data_view.Count > 1 Then

      ' 116 "%1 already exists.\n%2 must be unique."
      _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(116), _
                                  Row(COLUMN_AREA_NAME), GLB_NLS_GUI_CONTROLS.GetString(426))
      AssignRowError(Row, _nls_string)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(116), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    Return True
  End Function ' CheckDataRow_Areas

  ' PURPOSE: Check Islands Data Row for constraints.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow: Row to check.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckDataRow_Islands(ByVal Row As DataRow) As Boolean
    Dim _data_view As DataView
    Dim _row_filter As String
    Dim _nls_string As String
    Dim _island_name As String

    _data_view = New DataView(Me.m_ds_areas.Tables(ENUM_TABLES.ISLANDS))
    _island_name = Row(COLUMN_ISLAND_NAME)

    ' Check couple ISLAND_NAME / AREA_ID
    _row_filter = "IS_ISLAND_NAME = '" & _island_name.Replace("'", "''") & "'" & _
            " AND IS_AREA_ID = " & Row(COLUMN_ISLAND_AREA_ID)
    _data_view.RowFilter = _row_filter

    If _data_view.Count > 1 Then

      ' 115 "The couple %1 / %2 already exists.\nThe couple %3 / %4 must be unique."
      _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(115), _
                                 _island_name, GetIslandAreaName(Row), _
                                 GLB_NLS_GUI_CONTROLS.GetString(427), _
                                 GLB_NLS_GUI_CONTROLS.GetString(426))
      AssignRowError(Row, _nls_string)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(115), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    Return True
  End Function ' CheckDataRow_Islands

  ' PURPOSE: Check Terminals Data Row for constraints.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow: Row to check.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function CheckDataRow_Terminals(ByVal Row As DataRow) As Boolean
    Dim _data_view As DataView
    Dim _row_filter As String
    Dim _nls_string As String
    Dim _terminal_name As String
    Dim _terminal_floor As String


    _data_view = New DataView(Me.m_ds_areas.Tables(ENUM_TABLES.TERMINALS))
    _terminal_name = Row(COLUMN_TERMINAL_NAME)


    ' Check TE_NAME is Unique
    _row_filter = "TE_NAME = '" & _terminal_name.Replace("'", "''") & "'"
    _data_view.RowFilter = _row_filter

    If _data_view.Count > 1 Then

      ' 116 "%1 already exists.\n%2 must be unique."
      _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(116), _
                                  _terminal_name, GLB_NLS_GUI_AUDITOR.GetString(333))
      AssignRowError(Row, _nls_string)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(116), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    ' Check TE_FLOOR_ID is Unique
    ' Allow TE_FLOOR_ID to be Null

    If Row(COLUMN_TERMINAL_FLOOR_ID) Is DBNull.Value Then
      Return True
    End If
    If Row(COLUMN_TERMINAL_FLOOR_ID) = "" Then
      Return True
    End If

    _terminal_floor = Row(COLUMN_TERMINAL_FLOOR_ID)
    _row_filter = "TE_FLOOR_ID = '" & _terminal_floor.Replace("'", "''") & "'"
    _data_view.RowFilter = _row_filter

    If _data_view.Count > 1 Then

      ' 116 "%1 already exists.\n%2 must be unique."
      _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(116), _
                                  _terminal_floor, GLB_NLS_GUI_CONTROLS.GetString(437))
      AssignRowError(Row, _nls_string)
      Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(116), _nls_string, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    Return True
  End Function ' CheckDataRow_Terminals

  ' PURPOSE: Set RowError for the Row. Also restore the RowHeaderWidth for the 3gs DataGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row As DataRow
  '           - MsgError As String
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Private Sub AssignRowError(ByVal Row As DataRow, ByVal MsgError As String)
    Dim _row_parent As DataRow
    Dim _row_grandparent As DataRow
    Dim _area_position As Integer
    Dim _island_position As Integer
    Dim _terminal_position As Integer
    Dim _tree_level As Integer
    Dim _provider_position As Integer

    Row.RowError = MsgError

    _area_position = -1
    _island_position = -1
    _terminal_position = -1

    Select Case Row.Table.TableName
      Case TABLE_AREA_NAME
        _area_position = m_bs_areas.Find("AR_AREA_ID", Row(COLUMN_AREA_ID))
        _tree_level = ENUM_TABLES.AREAS

      Case TABLE_ISLAND_NAME
        _row_parent = Row.GetParentRow(RELATION_AREA_ISLAND)
        _area_position = m_bs_areas.Find("AR_AREA_ID", _row_parent(COLUMN_AREA_ID))
        If _area_position >= 0 Then
          m_bs_areas.Position = _area_position
        End If
        _island_position = m_bs_islands.Find("IS_ISLAND_ID", Row(COLUMN_ISLAND_ID))

      Case TABLE_TERMINAL_NAME
        _row_parent = Row.GetParentRow(RELATION_ISLAND_TERMINAL)
        _row_grandparent = _row_parent.GetParentRow(RELATION_AREA_ISLAND)

        _area_position = m_bs_areas.Find("AR_AREA_ID", _row_grandparent(COLUMN_AREA_ID))
        If _area_position >= 0 Then
          m_bs_areas.Position = _area_position
        End If
        _island_position = m_bs_islands.Find("IS_ISLAND_ID", _row_parent(COLUMN_ISLAND_ID))
        If _island_position >= 0 Then
          m_bs_islands.Position = _island_position
        End If
        ' WARNING!! The next line generates a non-sense exception.
        ' No problem: Do a manual search...
        '_terminal_position = m_bs_terminals.Find("TE_TERMINAL_ID", Row(COLUMN_TERMINAL_ID))
        ''''For _terminal_position = 0 To m_bs_terminals.Count - 1
        ''''  If dgv_terminals.Rows(_terminal_position).Cells(COLUMN_TERMINAL_ID).Value = Row(COLUMN_TERMINAL_ID) Then
        ''''    Exit For
        ''''  End If
        ''''Next
      Case TABLE_PROVIDER_NAME
        _provider_position = m_bs_providers.Find("PV_ID", Row(COLUMN_AREA_ID))
        _tree_level = ENUM_TABLES_NO_BANK.PROVIDERS

        If _provider_position >= 0 Then
          m_bs_providers.Position = _provider_position
        End If

      Case Else
        Return
    End Select


    ' Make visible area row
    ''''If _area_position >= 0 Then
    ''''  m_bs_areas.Position = _area_position
    ''''  If Not dgv_areas.Rows(m_bs_areas.Position).Displayed Then
    ''''    dgv_areas.FirstDisplayedScrollingRowIndex = m_bs_areas.Position
    ''''  End If
    ''''End If

    ''''' Make visible island row
    ''''If _island_position >= 0 Then
    ''''  m_bs_islands.Position = _island_position
    ''''  If Not dgv_islands.Rows(m_bs_islands.Position).Displayed Then
    ''''    dgv_islands.FirstDisplayedScrollingRowIndex = m_bs_islands.Position
    ''''  End If
    ''''End If

    ''''' Make visible terminal row
    ''''If _terminal_position >= 0 Then
    ''''  m_bs_terminals.Position = _terminal_position
    ''''  If Not dgv_terminals.Rows(m_bs_terminals.Position).Displayed Then
    ''''    dgv_terminals.FirstDisplayedScrollingRowIndex = m_bs_terminals.Position
    ''''  End If
    ''''End If

  End Sub ' AssignRowError

  ' PURPOSE: Find the first message error in a array of rows.
  '
  '  PARAMS:
  '     - INPUT: DataRows() Array of rows to search for a message error
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - First msg_error found
  '
  Private Function GetMsgErrorFromRow(ByVal DataRows() As DataRow) As String
    Dim row As DataRow
    Dim msg_error As String = ""

    For Each row In DataRows
      If row.HasErrors Then
        msg_error = row.RowError

        Exit For
      End If
    Next

    Return msg_error

  End Function ' GetMsgErrorFromRow

  ' PURPOSE: Write auditory changes made in Areas.
  '
  '  PARAMS:
  '     - INPUT:
  '           - AreaRows As DataRows()
  '     - OUTPUT:
  '           - ReallyChanged As Boolean
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_WriteAuditoryChangesAreas(ByVal AreaRows As DataRow(), ByRef ReallyChanged As Boolean)
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _orig_value As String
    Dim _curr_value As String
    Dim _row_area As DataRow
    Dim _has_changes As Boolean

    If AreaRows Is Nothing Then
      Return
    End If

    '---------------------------------------------
    ' Insert MODIFIED and ADDED items
    '---------------------------------------------
    For Each _row_area In AreaRows
      _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS) ' TODO: Change auditor code?
      _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

      ' Set Name and Identifier
      Call _curr_auditor.SetName(GLB_NLS_GUI_CONTROLS.Id(426), GetAreaName(_row_area))
      Call _orig_auditor.SetName(GLB_NLS_GUI_CONTROLS.Id(426), GetAreaName(_row_area))

      _has_changes = False

      ' Name
      _curr_value = IIf(IsDBNull(_row_area(COLUMN_AREA_NAME)), AUDIT_NONE_STRING, _row_area(COLUMN_AREA_NAME))
      Call _curr_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(323), _curr_value)

      If _row_area.RowState = DataRowState.Modified Then
        _orig_value = IIf(IsDBNull(_row_area(COLUMN_AREA_NAME, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                          _row_area(COLUMN_AREA_NAME, DataRowVersion.Original))
        Call _orig_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(323), _orig_value)

        If _orig_value <> _curr_value Then
          _has_changes = True
        End If
      End If

      ' Smoking
      _curr_value = IIf(IsDBNull(_row_area(COLUMN_AREA_SMOKING)), AUDIT_NONE_STRING, _row_area(COLUMN_AREA_SMOKING))
      Call _curr_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(428), _curr_value)

      If _row_area.RowState = DataRowState.Modified Then
        _orig_value = IIf(IsDBNull(_row_area(COLUMN_AREA_SMOKING, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                          _row_area(COLUMN_AREA_SMOKING, DataRowVersion.Original))
        Call _orig_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(428), _orig_value)

        If _orig_value <> _curr_value Then
          _has_changes = True
        End If
      End If

      Select Case _row_area.RowState

        Case DataRowState.Modified
          If _has_changes Then
            _curr_auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                 0, _orig_auditor)
            ReallyChanged = True
          End If

        Case DataRowState.Added
          _curr_auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, 0)
          ReallyChanged = True

        Case Else
      End Select

      _curr_auditor = Nothing
      _orig_auditor = Nothing
    Next ' For Each _row_area

  End Sub ' GUI_WriteAuditoryChangesAreas

  ' PURPOSE: Write auditory changes made in Islands.
  '
  '  PARAMS:
  '     - INPUT:
  '           - IslandRows As DataRows()
  '     - OUTPUT:
  '           - ReallyChanged As Boolean
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_WriteAuditoryChangesIslands(ByVal IslandRows As DataRow(), ByRef ReallyChanged As Boolean)
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _island_name As String
    Dim _orig_value As String
    Dim _curr_value As String
    Dim _row_island As DataRow
    Dim _has_changes As Boolean

    If IslandRows Is Nothing Then
      Return
    End If

    '---------------------------------------------
    ' Insert MODIFIED and ADDED items
    '---------------------------------------------
    For Each _row_island In IslandRows
      _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS) ' TODO: Change auditor code?
      _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

      ' Set Name and Identifier
      _island_name = _row_island(COLUMN_ISLAND_NAME) & " " & GLB_NLS_GUI_CONTROLS.GetString(434) & " " & _row_island(COLUMN_ISLAND_AREA_NAME)
      Call _curr_auditor.SetName(GLB_NLS_GUI_CONTROLS.Id(427), _island_name)
      Call _orig_auditor.SetName(GLB_NLS_GUI_CONTROLS.Id(427), _island_name)

      _has_changes = False

      ' Name
      _curr_value = IIf(IsDBNull(_row_island(COLUMN_ISLAND_NAME)), AUDIT_NONE_STRING, _row_island(COLUMN_ISLAND_NAME))
      Call _curr_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(323), _curr_value)

      If _row_island.RowState = DataRowState.Modified Then
        _orig_value = IIf(IsDBNull(_row_island(COLUMN_ISLAND_NAME, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                          _row_island(COLUMN_ISLAND_NAME, DataRowVersion.Original))
        Call _orig_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(323), _orig_value)

        If _orig_value <> _curr_value Then
          _has_changes = True
        End If
      End If

      Select Case _row_island.RowState

        Case DataRowState.Modified
          If _has_changes Then
            _curr_auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                 0, _orig_auditor)
            ReallyChanged = True
          End If

        Case DataRowState.Added
          _curr_auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                               CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, 0)
          ReallyChanged = True

        Case Else
      End Select

      _curr_auditor = Nothing
      _orig_auditor = Nothing
    Next ' For Each _row_island

  End Sub ' GUI_WriteAuditoryChangesIslands

  ' PURPOSE: Write auditory changes made in Terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalRows As DataRows()
  '     - OUTPUT:
  '           - ReallyChanged As Boolean
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_WriteAuditoryChangesTerminals(ByVal TerminalRows As DataRow(), ByRef ReallyChanged As Boolean)
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _terminal_name As String
    Dim _orig_value As String
    Dim _curr_value As String
    Dim _row_terminal As DataRow
    Dim _has_changes As Boolean

    If TerminalRows Is Nothing Then
      Return
    End If

    '---------------------------------------------
    ' Insert MODIFIED items
    '---------------------------------------------
    For Each _row_terminal In TerminalRows
      _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
      _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

      ' Set Name and Identifier
      _terminal_name = _row_terminal(COLUMN_TERMINAL_NAME) & " " & GLB_NLS_GUI_CONTROLS.GetString(438) & " " & _row_terminal(COLUMN_TERMINAL_BANK_NAME)
      Call _curr_auditor.SetName(GLB_NLS_GUI_AUDITOR.Id(333), _terminal_name)
      Call _orig_auditor.SetName(GLB_NLS_GUI_AUDITOR.Id(333), _terminal_name)

      _has_changes = False

      ' Name
      _curr_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_NAME)), AUDIT_NONE_STRING, _row_terminal(COLUMN_TERMINAL_NAME))
      Call _curr_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(323), _curr_value)

      If _row_terminal.RowState = DataRowState.Modified Then
        _orig_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_NAME, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                          _row_terminal(COLUMN_TERMINAL_NAME, DataRowVersion.Original))
        Call _orig_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(323), _orig_value)

        If _orig_value <> _curr_value Then
          _has_changes = True
        End If
      End If

      ' Floor ID
      _curr_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_FLOOR_ID)), AUDIT_NONE_STRING, _row_terminal(COLUMN_TERMINAL_FLOOR_ID))
      Call _curr_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(437), _curr_value)

      If _row_terminal.RowState = DataRowState.Modified Then
        _orig_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_FLOOR_ID, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                          _row_terminal(COLUMN_TERMINAL_FLOOR_ID, DataRowVersion.Original))
        Call _orig_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(437), _orig_value)

        If _orig_value <> _curr_value Then
          _has_changes = True
        End If
      End If

      ' Island ID
      _curr_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_BANK_ID)), AUDIT_NONE_STRING, _row_terminal(COLUMN_TERMINAL_BANK_ID))

      If _row_terminal.RowState = DataRowState.Modified Then
        _orig_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_BANK_ID, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                          _row_terminal(COLUMN_TERMINAL_BANK_ID, DataRowVersion.Original))

        If _orig_value <> _curr_value Then
          _curr_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_BANK_NAME)), AUDIT_NONE_STRING, _row_terminal(COLUMN_TERMINAL_BANK_NAME))
          _orig_value = IIf(IsDBNull(_row_terminal(COLUMN_TERMINAL_BANK_NAME, DataRowVersion.Original)), AUDIT_NONE_STRING, _
                            _row_terminal(COLUMN_TERMINAL_BANK_NAME, DataRowVersion.Original))

          Call _curr_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(427), _curr_value)
          Call _orig_auditor.SetField(GLB_NLS_GUI_CONTROLS.Id(427), _orig_value)

          _has_changes = True
        End If
      End If

      Select Case _row_terminal.RowState

        Case DataRowState.Modified
          If _has_changes Then
            _curr_auditor.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                 0, _orig_auditor)
            ReallyChanged = True
          End If

        Case Else
      End Select

      _curr_auditor = Nothing
      _orig_auditor = Nothing
    Next ' For Each _row_terminal

  End Sub ' GUI_WriteAuditoryChangesTerminals

  ' PURPOSE: Initialize data in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_ClearGrid()

    ''''dgv_areas.DataSource = Nothing
    ''''dgv_islands.DataSource = Nothing
    ''''dgv_terminals.DataSource = Nothing

  End Sub 'GUI_ClearGrid

  ' PURPOSE: Build an SQL query from conditions set in the filters for Areas
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database to obtain Areas

  Private Function GUI_FilterGetSqlQuery_Areas() As String
    Dim _str_sql As String

    _str_sql = "SELECT   AR_AREA_ID " & _
               "       , AR_NAME " & _
               "       , AR_SMOKING " & _
               "       , CAST(0 AS INT) AS AR_VERSION_NUMBER " & _
               "  FROM   AREAS " & _
               "ORDER BY AR_NAME "

    Return _str_sql
  End Function ' GUI_FilterGetSqlQuery_Areas

  ' PURPOSE: Build an SQL query from conditions set in the filters for Islands
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database to obtain Islands

  Private Function GUI_FilterGetSqlQuery_Islands() As String
    Dim _str_sql As String

    ' The field IS_AREA_NAME here is only for Audit purposes (COLUMN_ISLAND_AREA_NAME).
    _str_sql = "SELECT   IS_ISLAND_ID " & _
               "       , IS_ISLAND_NAME " & _
               "       , IS_AREA_ID " & _
               "       , '' AS IS_AREA_NAME " & _
               "       , CAST(0 AS INT) AS IS_VERSION_NUMBER " & _
               "  FROM   ISLANDS " & _
               "ORDER BY IS_AREA_ID, IS_ISLAND_NAME "

    Return _str_sql
  End Function ' GUI_FilterGetSqlQuery_Islands

  ' PURPOSE: Build an SQL query from conditions set in the filters for Terminals
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database to obtain Islands

  Private Function GUI_FilterGetSqlQuery_Terminals() As String
    Dim _str_sql As String

    ' The field TE_ISLAND_NAME here is only for Audit purposes (COLUMN_TERMINAL_BANK_NAME).
    ' TODO: TE_BANK_ID is hardcoded now.
    _str_sql = "SELECT   TE_TERMINAL_ID " & _
               "       , TE_FLOOR_ID " & _
               "       , TE_PROVIDER_ID " & _
               "       , TE_NAME " & _
               "       , 1 AS TE_BANK_ID " & _
               "       , '' AS TE_BANK_NAME" & _
               "       , CAST(0 AS INT) AS TE_VERSION_NUMBER " & _
               "  FROM   TERMINALS " & _
               "  WHERE  (TE_BANK_ID IS NOT NULL) " & _
               "ORDER BY TE_BANK_ID, TE_PROVIDER_ID, TE_NAME "

    Return _str_sql
  End Function ' GUI_FilterGetSqlQuery_Terminals

  ' PURPOSE: Build an SQL query from conditions set in the filters for Terminals without bank
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database to obtain Islands

  Private Function GUI_FilterGetSqlQuery_Terminals_No_Bank() As String
    Dim _str_sql As String

    _str_sql = "SELECT   TE_TERMINAL_ID " & _
               "       , TE_FLOOR_ID " & _
               "       , TE_PROVIDER_ID " & _
               "       , TE_NAME " & _
               "  FROM   TERMINALS " & _
               "  WHERE  TE_BANK_ID IS NULL " & _
               "ORDER BY TE_PROVIDER_ID, TE_NAME "

    Return _str_sql
  End Function ' GUI_FilterGetSqlQuery_Terminals


  ' PURPOSE: Build an SQL query from conditions set in the filters for Providers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database to obtain Providers

  Private Function GUI_FilterGetSqlQuery_Providers() As String
    Dim _str_sql As String

    ' The field TE_ISLAND_NAME here is only for Audit purposes (COLUMN_TERMINAL_BANK_NAME).
    ' TODO: TE_BANK_ID is hardcoded now.
    _str_sql = "SELECT   PV_ID " & _
               "       , PV_NAME " & _
               "  FROM   PROVIDERS " & _
               "ORDER BY PV_NAME "

    Return _str_sql
  End Function ' GUI_FilterGetSqlQuery_Providers

  ' PURPOSE: Create DataGridViews with SQL queries, and show generated info in screen.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub ExecuteQuery()
    Dim _column_parent As DataColumn
    Dim _column_child As DataColumn
    Dim _sql_trx As SqlTransaction
    Dim _data_relation As DataRelation

    Try
      If m_bs_areas IsNot Nothing Then
        m_bs_areas.Dispose()
      End If
      m_bs_areas = New BindingSource()

      If m_bs_islands IsNot Nothing Then
        m_bs_islands.Dispose()
      End If
      m_bs_islands = New BindingSource()

      If m_bs_terminals IsNot Nothing Then
        m_bs_terminals.Dispose()
      End If
      m_bs_terminals = New BindingSource()

      'SSC 16-FEB-2012: Providers and Terminals without bank
      If m_bs_providers IsNot Nothing Then
        m_bs_providers.Dispose()
      End If
      m_bs_providers = New BindingSource()

      If m_bs_terminals_no_bank IsNot Nothing Then
        m_bs_terminals_no_bank.Dispose()
      End If
      m_bs_terminals_no_bank = New BindingSource()

      Using _db_trx As DB_TRX = New DB_TRX()
        _sql_trx = _db_trx.SqlTransaction
        If m_ds_areas IsNot Nothing Then
          m_ds_areas.Dispose()
        End If
        m_ds_areas = New DataSet()

        If m_ds_providers IsNot Nothing Then
          m_ds_providers.Dispose()
        End If
        m_ds_providers = New DataSet()

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
          _sql_adap.SelectCommand = New SqlCommand(GUI_FilterGetSqlQuery_Areas(), _sql_trx.Connection, _sql_trx)
          _sql_adap.Fill(m_ds_areas, TABLE_AREA_NAME)
        End Using

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
          _sql_adap.SelectCommand = New SqlCommand(GUI_FilterGetSqlQuery_Islands(), _sql_trx.Connection, _sql_trx)
          _sql_adap.Fill(m_ds_areas, TABLE_ISLAND_NAME)
        End Using

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
          _sql_adap.SelectCommand = New SqlCommand(GUI_FilterGetSqlQuery_Terminals(), _sql_trx.Connection, _sql_trx)
          _sql_adap.Fill(m_ds_areas, TABLE_TERMINAL_NAME)
        End Using

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
          _sql_adap.SelectCommand = New SqlCommand(GUI_FilterGetSqlQuery_Providers(), _sql_trx.Connection, _sql_trx)
          _sql_adap.Fill(m_ds_providers, TABLE_PROVIDER_NAME)
        End Using

        Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
          _sql_adap.SelectCommand = New SqlCommand(GUI_FilterGetSqlQuery_Terminals_No_Bank(), _sql_trx.Connection, _sql_trx)
          _sql_adap.Fill(m_ds_providers, TABLE_TERMINAL_NAME)
        End Using

      End Using ' Using _db_trx

      ' Default Values for Table Areas
      m_ds_areas.Tables(ENUM_TABLES.AREAS).Columns(COLUMN_AREA_SMOKING).DefaultValue = 0
      m_ds_areas.Tables(ENUM_TABLES.AREAS).Columns(COLUMN_AREA_VERSION_NUMBER).DefaultValue = 0

      m_ds_areas.Tables(ENUM_TABLES.AREAS).Columns(COLUMN_AREA_ID).AutoIncrement = True
      m_ds_areas.Tables(ENUM_TABLES.AREAS).Columns(COLUMN_AREA_ID).AutoIncrementStep = -1
      m_ds_areas.Tables(ENUM_TABLES.AREAS).Columns(COLUMN_AREA_ID).AutoIncrementSeed = -1

      ' Default Values for Table Islands
      m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_AREA_NAME).DefaultValue = ""
      m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_VERSION_NUMBER).DefaultValue = 0

      m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_ID).AutoIncrement = True
      m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_ID).AutoIncrementStep = -1
      m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_ID).AutoIncrementSeed = -1

      ' Default Values for Table Terminals
      m_ds_areas.Tables(ENUM_TABLES.TERMINALS).Columns(COLUMN_TERMINAL_BANK_NAME).DefaultValue = ""
      m_ds_areas.Tables(ENUM_TABLES.TERMINALS).Columns(COLUMN_TERMINAL_VERSION_NUMBER).DefaultValue = 0

      ' Relations between Areas - Islands and Islands - Terminals.
      _column_parent = m_ds_areas.Tables(ENUM_TABLES.AREAS).Columns(COLUMN_AREA_ID)
      _column_child = m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_AREA_ID)
      _data_relation = New DataRelation(RELATION_AREA_ISLAND, _column_parent, _column_child, True)
      m_ds_areas.Relations.Add(_data_relation)

      _column_parent = m_ds_areas.Tables(ENUM_TABLES.ISLANDS).Columns(COLUMN_ISLAND_ID)
      _column_child = m_ds_areas.Tables(ENUM_TABLES.TERMINALS).Columns(COLUMN_TERMINAL_BANK_ID)
      _data_relation = New DataRelation(RELATION_ISLAND_TERMINAL, _column_parent, _column_child, True)
      m_ds_areas.Relations.Add(_data_relation)

      FillTerminalIslandAreaNames()

      ' Bind the master data connector (Areas) to the Areas table.
      m_bs_areas.DataSource = m_ds_areas
      m_bs_areas.DataMember = TABLE_AREA_NAME

      ' Bind the details data connector (Islands) to the master data connector (Areas),
      ' using the DataRelation name to filter the information in the
      ' details table based on the current row in the master table.
      m_bs_islands.DataSource = m_bs_areas
      m_bs_islands.DataMember = RELATION_AREA_ISLAND

      m_bs_terminals.DataSource = m_bs_islands
      m_bs_terminals.DataMember = RELATION_ISLAND_TERMINAL

      FillTreeViewArea()

      'SSC 16-FEB-2012: Providers and Terminals without bank

      ' Default Values for Table Providers
      m_ds_providers.Tables(ENUM_TABLES_NO_BANK.PROVIDERS).Columns(COLUMN_PROVIDER_NAME).DefaultValue = ""

      ' Relations between Providers - Terminals.
      _column_parent = m_ds_providers.Tables(ENUM_TABLES_NO_BANK.PROVIDERS).Columns(COLUMN_PROVIDER_ID)
      _column_child = m_ds_providers.Tables(ENUM_TABLES_NO_BANK.TERMINALS).Columns(COLUMN_TERMINAL_PROVIDER_ID)
      _data_relation = New DataRelation(RELATION_PROVIDER_TERMINAL, _column_parent, _column_child, True)
      m_ds_providers.Relations.Add(_data_relation)

      FillTerminalProviderNames()

      ' Bind the master data connector (Provider) to the Providers table.
      m_bs_providers.DataSource = m_ds_providers
      m_bs_providers.DataMember = TABLE_PROVIDER_NAME

      m_bs_terminals.DataSource = m_bs_providers
      m_bs_terminals.DataMember = RELATION_PROVIDER_TERMINAL

      FillTreeViewProvider()

    Catch _ex As Exception
      ' Do nothing
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' ExecuteQuery


  ' PURPOSE: Fill the missing data in DataTables.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTerminalIslandAreaNames()
    Dim _area_row As DataRow
    Dim _island_row As DataRow
    Dim _terminal_row As DataRow

    For Each _area_row In m_ds_areas.Tables(ENUM_TABLES.AREAS).Rows
      For Each _island_row In _area_row.GetChildRows(RELATION_AREA_ISLAND)
        If String.IsNullOrEmpty(_island_row(COLUMN_ISLAND_AREA_NAME)) Then
          _island_row(COLUMN_ISLAND_AREA_NAME) = GetAreaName(_area_row)
        End If
        For Each _terminal_row In _island_row.GetChildRows(RELATION_ISLAND_TERMINAL)
          If String.IsNullOrEmpty(_terminal_row(COLUMN_TERMINAL_BANK_NAME)) Then
            ''''_terminal_row(COLUMN_TERMINAL_BANK_NAME) = _island_row(COLUMN_ISLAND_AREA_NAME) & " / " & GetIslandName(_island_row)
            _terminal_row(COLUMN_TERMINAL_BANK_NAME) = GetIslandName(_island_row)
          End If
        Next
      Next
    Next

    m_ds_areas.AcceptChanges()
  End Sub ' FillTerminalIslandAreaNames

  Private Sub FillTerminalProviderNames()
    Dim _provider_row As DataRow
    Dim _terminal_no_bank_row As DataRow

    For Each _provider_row In m_ds_providers.Tables(ENUM_TABLES_NO_BANK.PROVIDERS).Rows
      For Each _terminal_no_bank_row In _provider_row.GetChildRows(RELATION_PROVIDER_TERMINAL)
        If String.IsNullOrEmpty(_terminal_no_bank_row(COLUMN_TERMINAL_PROVIDER_ID)) Then
          _terminal_no_bank_row(COLUMN_TERMINAL_PROVIDER_ID) = GetProviderName(_provider_row)
        End If
      Next
    Next

    m_ds_areas.AcceptChanges()
  End Sub ' FillTerminalIslandAreaNames


  ' PURPOSE: Fill the TreeView --> Terminals grouped by island and area
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTreeViewArea()
    Dim _area_idx As Integer
    Dim _island_idx As Integer
    Dim _terminal_idx As Integer
    Dim _root_node As TreeNode
    Dim _area_node As TreeNode
    Dim _island_node As TreeNode
    Dim _terminal_node As TreeNode
    Dim _area_row As DataRow
    Dim _island_row As DataRow
    Dim _terminal_row As DataRow

    ' Display a wait cursor while the TreeNodes are being created.
    Cursor.Current = Cursors.WaitCursor

    ' Suppress repainting the TreeView until all the objects have been created.
    tv_areas.BeginUpdate()

    Try
      ' Clear the TreeView each time the method is called.
      tv_areas.Nodes.Clear()

      _root_node = tv_areas.Nodes.Add("Sala")

      For _area_idx = 0 To m_bs_areas.Count - 1
        m_bs_areas.Position = _area_idx

        _area_row = CType(m_bs_areas.Current, DataRowView).Row
        _area_node = New TreeNode(GetAreaName(_area_row))
        _area_node.Tag = _area_row
        _root_node.Nodes.Add(_area_node)

        For _island_idx = 0 To m_bs_islands.Count - 1
          m_bs_islands.Position = _island_idx

          _island_row = CType(m_bs_islands.Current, DataRowView).Row
          _island_node = New TreeNode(GetIslandName(_island_row))
          _island_node.Tag = _island_row
          _area_node.Nodes.Add(_island_node)

          For _terminal_idx = 0 To m_bs_terminals.Count - 1
            m_bs_terminals.Position = _terminal_idx

            _terminal_row = CType(m_bs_terminals.Current, DataRowView).Row
            _terminal_node = New TreeNode(GetTerminalName(_terminal_row))
            _terminal_node.Tag = _terminal_row
            _island_node.Nodes.Add(_terminal_node)

          Next _terminal_idx
        Next _island_idx
      Next _area_idx

      _root_node.Expand()

    Catch ex As Exception

    Finally
      ' Reset the cursor to the default for all controls.
      Cursor.Current = System.Windows.Forms.Cursors.Default

      ' Begin repainting the TreeView.
      tv_areas.EndUpdate()
    End Try

  End Sub ' FillTreeViewArea

  ' PURPOSE: Fill the TreeView --> Terminals without island grouped by provider
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillTreeViewProvider()
    Dim _provider_idx As Integer
    Dim _terminal_idx As Integer
    Dim _root_node As TreeNode
    Dim _provider_node As TreeNode
    Dim _terminal_node As TreeNode
    Dim _provider_row As DataRow
    Dim _terminal_row As DataRow

    ' Display a wait cursor while the TreeNodes are being created.
    Cursor.Current = Cursors.WaitCursor

    ' Suppress repainting the TreeView until all the objects have been created.
    tv_providers.BeginUpdate()

    Try
      ' Clear the TreeView each time the method is called.
      tv_providers.Nodes.Clear()

      _root_node = tv_providers.Nodes.Add("Proveedor")

      For _provider_idx = 0 To m_bs_providers.Count - 1
        m_bs_providers.Position = _provider_idx

        _provider_row = CType(m_bs_providers.Current, DataRowView).Row
        _provider_node = New TreeNode(GetProviderName(_provider_row))
        _provider_node.Tag = _provider_row
        _root_node.Nodes.Add(_provider_node)

        For _terminal_idx = 0 To m_bs_terminals.Count - 1
          m_bs_terminals.Position = _terminal_idx

          _terminal_row = CType(m_bs_terminals.Current, DataRowView).Row
          _terminal_node = New TreeNode(GetTerminalName(_terminal_row))
          _terminal_node.Tag = _terminal_row
          _provider_node.Nodes.Add(_terminal_node)

        Next _terminal_idx
      Next _provider_idx

      _root_node.Expand()

    Catch ex As Exception

    Finally
      ' Reset the cursor to the default for all controls.
      Cursor.Current = System.Windows.Forms.Cursors.Default

      ' Begin repainting the TreeView.
      tv_providers.EndUpdate()
    End Try

  End Sub ' FillTreeViewProvider


  ' PURPOSE: Init the Columns of the DataGridView for Areas.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitGridAreas()

    ''''dgv_areas.Columns(COLUMN_AREA_ID).Width = 0
    ''''dgv_areas.Columns(COLUMN_AREA_ID).Visible = False

    ''''dgv_areas.Columns(COLUMN_AREA_NAME).Width = 163
    ''''dgv_areas.Columns(COLUMN_AREA_NAME).HeaderText = GLB_NLS_GUI_CONTROLS.GetString(323)

    ''''dgv_areas.Columns(COLUMN_AREA_SMOKING).Width = 75
    ''''dgv_areas.Columns(COLUMN_AREA_SMOKING).HeaderText = GLB_NLS_GUI_CONTROLS.GetString(428)

    ''''dgv_areas.Columns(COLUMN_AREA_VERSION_NUMBER).Width = 0
    ''''dgv_areas.Columns(COLUMN_AREA_VERSION_NUMBER).Visible = False

  End Sub ' InitGridAreas

  ' PURPOSE: Init the Columns of the DataGridView for Islands.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitGridIslands()

    ''''dgv_islands.Columns(COLUMN_ISLAND_ID).Width = 0
    ''''dgv_islands.Columns(COLUMN_ISLAND_ID).Visible = False

    ''''dgv_islands.Columns(COLUMN_ISLAND_NAME).Width = 179
    ''''dgv_islands.Columns(COLUMN_ISLAND_NAME).HeaderText = GLB_NLS_GUI_CONTROLS.GetString(323)

    ''''dgv_islands.Columns(COLUMN_ISLAND_AREA_ID).Width = 0
    ''''dgv_islands.Columns(COLUMN_ISLAND_AREA_ID).Visible = False

    ''''dgv_islands.Columns(COLUMN_ISLAND_AREA_NAME).Width = 0
    ''''dgv_islands.Columns(COLUMN_ISLAND_AREA_NAME).Visible = False

    ''''dgv_islands.Columns(COLUMN_ISLAND_VERSION_NUMBER).Width = 0
    ''''dgv_islands.Columns(COLUMN_ISLAND_VERSION_NUMBER).Visible = False

  End Sub ' InitGridIslands

  ' PURPOSE: Init the Columns of the DataGridView for Terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitGridTerminals()

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_ID).Width = 0
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_ID).Visible = False

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_FLOOR_ID).Width = 78
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_FLOOR_ID).HeaderText = GLB_NLS_GUI_CONTROLS.GetString(437)

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_PROVIDER_ID).Width = 170
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_PROVIDER_ID).HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_PROVIDER_ID).ReadOnly = True

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_NAME).Width = 170
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_NAME).HeaderText = GLB_NLS_GUI_AUDITOR.GetString(333)

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_BANK_ID).Width = 0
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_BANK_ID).Visible = False

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_BANK_NAME).Width = 0
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_BANK_NAME).Visible = False

    ''''dgv_terminals.Columns(COLUMN_TERMINAL_VERSION_NUMBER).Width = 0
    ''''dgv_terminals.Columns(COLUMN_TERMINAL_VERSION_NUMBER).Visible = False

  End Sub ' InitGridIslands

  ' PURPOSE: Get the smoking flag in the format "(NF)" or "(F)"
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetSmokingDesc(ByVal Smoking As Boolean) As String
    Dim _desc As String

    _desc = ""
    If Smoking Then
      _desc = GLB_NLS_GUI_CONTROLS.GetString(432)
    Else
      _desc = GLB_NLS_GUI_CONTROLS.GetString(433)
    End If

    Return _desc
  End Function ' GetSmokingDesc

  ' PURPOSE: Get the area name with the smoking flag in the format "Name (NF)"
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetIslandAreaName(ByVal IslandRow As DataRow) As String

    Dim _row_area As DataRow
    Dim _area_name As String

    _area_name = IslandRow(COLUMN_ISLAND_AREA_ID)
    _row_area = IslandRow.GetParentRow(RELATION_AREA_ISLAND)
    If _row_area IsNot Nothing Then
      _area_name = _row_area(COLUMN_AREA_NAME) & " " & GetSmokingDesc(_row_area(COLUMN_AREA_SMOKING))
    End If

    Return _area_name
  End Function ' GetIslandAreaName

  ' PURPOSE: Get the area name with the smoking flag in the format "Name (NF)"
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetAreaName(ByVal Row As DataGridViewRow) As String

    If Row.Cells(COLUMN_AREA_NAME).Value Is DBNull.Value Then
      Return ""
    End If

    Return Row.Cells(COLUMN_AREA_NAME).Value & " " & GetSmokingDesc(Row.Cells(COLUMN_AREA_SMOKING).Value)
  End Function ' GetAreaName

  ' PURPOSE: Get the area name with the smoking flag in the format "Name (NF)"
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetAreaName(ByVal Row As DataRow) As String

    If Row(COLUMN_AREA_NAME) Is DBNull.Value Then
      Return ""
    End If

    Return Row(COLUMN_AREA_NAME) & " " & GetSmokingDesc(Row(COLUMN_AREA_SMOKING))
  End Function ' GetAreaName

  ' PURPOSE: Get the provider name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetProviderName(ByVal Row As DataRow) As String

    If Row(COLUMN_PROVIDER_NAME) Is DBNull.Value Then
      Return ""
    End If

    Return Row(COLUMN_PROVIDER_NAME)
  End Function ' GetProviderName

  ' PURPOSE: Get the island name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetIslandName(ByVal Row As DataGridViewRow) As String

    If Row.Cells(COLUMN_ISLAND_NAME).Value Is DBNull.Value Then
      Return ""
    End If

    Return Row.Cells(COLUMN_ISLAND_NAME).Value
  End Function ' GetIslandName

  ' PURPOSE: Get the island name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetIslandName(ByVal Row As DataRow) As String

    If Row(COLUMN_ISLAND_NAME) Is DBNull.Value Then
      Return ""
    End If

    Return Row(COLUMN_ISLAND_NAME)
  End Function ' GetIslandName

  ' PURPOSE: Get the terminal name
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetTerminalName(ByVal Row As DataRow) As String

    If Row(COLUMN_TERMINAL_NAME) Is DBNull.Value Then
      Return ""
    End If

    Return Row(COLUMN_TERMINAL_PROVIDER_ID) & "/" & Row(COLUMN_TERMINAL_NAME)
  End Function ' GetTerminalName


  ' PURPOSE: Check if can delete a DataGridView row
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowCancelParam
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UserDeletingRow(ByVal RowCancelParam As DataGridViewRowCancelEventArgs)
    Dim _row_view As DataRowView
    Dim _row As DataRow
    Dim _island_rows As DataRow()
    Dim _island_row As DataRow
    Dim _terms_rows As DataRow()
    Dim _term_row As DataRow

    _row_view = RowCancelParam.Row.DataBoundItem

    If _row_view Is Nothing Then
      Return
    End If
    _row = _row_view.Row

    ' If it's not a recently added row (only added in memory), cancel the deleting...
    ' It means, allow to delete only recently added rows in memory.
    If _row.RowState <> DataRowState.Added Then
      RowCancelParam.Cancel = True

      Return
    End If

    Select Case _row.Table.TableName
      Case TABLE_AREA_NAME
        _island_rows = _row.GetChildRows(RELATION_AREA_ISLAND)
        For Each _island_row In _island_rows
          _terms_rows = _island_row.GetChildRows(RELATION_ISLAND_TERMINAL)
          For Each _term_row In _terms_rows
            _term_row.RejectChanges()
          Next
        Next

      Case TABLE_ISLAND_NAME
        _terms_rows = _row.GetChildRows(RELATION_ISLAND_TERMINAL)
        For Each _term_row In _terms_rows
          _term_row.RejectChanges()
        Next

      Case TABLE_TERMINAL_NAME
        ' Can't be. Can't delete terminal rows.
    End Select

  End Sub ' UserDeletingRow

  ' PURPOSE: Add terminals to the selected island
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_AddTerminalsToIsland()
    ''''Dim _frm_sel As frm_terminals_sel
    ''''Dim _sel_params As TYPE_TERMINAL_SEL_PARAMS
    ''''Dim _selected_terminals_id As List(Of Integer)

    ''''If dgv_islands.SelectedRows.Count = 0 Then
    ''''  Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(147), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

    ''''  Return
    ''''End If
    ''''If dgv_islands.SelectedRows(0).Cells(COLUMN_ISLAND_ID).Value Is Nothing Then
    ''''  Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(147), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

    ''''  Return
    ''''End If

    ''''_frm_sel = New frm_terminals_sel()

    ''''_sel_params = New TYPE_TERMINAL_SEL_PARAMS
    ''''_sel_params.FilterTypeDefault = False
    ''''_sel_params.FilterTypeEnable(0) = False    ' Disable all 
    ''''' Check the Terminal Games and don't allow unselection
    ''''_sel_params.FilterTypeValue(WSI.Common.TerminalTypes.WIN) = True
    ''''_sel_params.FilterTypeEnable(WSI.Common.TerminalTypes.WIN) = True
    ''''_sel_params.FilterTypeLocked(WSI.Common.TerminalTypes.WIN) = True
    ''''_sel_params.FilterTypeValue(WSI.Common.TerminalTypes.SAS_HOST) = True
    ''''_sel_params.FilterTypeEnable(WSI.Common.TerminalTypes.SAS_HOST) = True
    ''''_sel_params.FilterTypeLocked(WSI.Common.TerminalTypes.SAS_HOST) = True
    ''''_sel_params.FilterTypeValue(WSI.Common.TerminalTypes.T3GS) = True
    ''''_sel_params.FilterTypeEnable(WSI.Common.TerminalTypes.T3GS) = True
    ''''_sel_params.FilterTypeLocked(WSI.Common.TerminalTypes.T3GS) = True

    ''''_selected_terminals_id = _frm_sel.ShowForMultiSelect(_sel_params)

    ''''If _selected_terminals_id.Count = 0 Then
    ''''  Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

    ''''  Return
    ''''End If

    ''''AddTerminalsToIsland(dgv_islands.CurrentRow.Cells(COLUMN_ISLAND_ID).Value, _
    ''''                     dgv_islands.CurrentRow.Cells(COLUMN_ISLAND_NAME).Value, _
    ''''                     _selected_terminals_id)

  End Sub ' GUI_AddTerminalsToIsland

  ' PURPOSE: Add the list of terminals to the indicated island
  '
  '  PARAMS:
  '     - INPUT:
  '           - IslandId
  '           - IslandName
  '           - Terminals
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Private Sub AddTerminalsToIsland(ByVal IslandId As Int32, _
                                        ByVal IslandName As String, _
                                        ByVal Terminals As List(Of Integer))
    Dim _dt_terminals As DataTable
    Dim _rows As DataRow()

    _dt_terminals = m_ds_areas.Tables(ENUM_TABLES.TERMINALS)
    For Each _terminal As Integer In Terminals
      _rows = _dt_terminals.Select("TE_TERMINAL_ID = " & _terminal)
      If _rows.Length = 0 Then
        Continue For
      End If

      _rows(0).BeginEdit()
      _rows(0)(COLUMN_TERMINAL_BANK_ID) = IslandId
      _rows(0)(COLUMN_TERMINAL_BANK_NAME) = IslandName
      _rows(0).EndEdit()
    Next

  End Sub ' AddTerminalsToIsland

  ' PURPOSE: Create the DataTable that will be used to print the data in screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CreateDataTableForPrinting()

    m_dt_printing = New DataTable("Printing")
    With m_dt_printing
      .Columns.Add("AREA_NAME", Type.GetType("System.String"))
      .Columns.Add("SMOKING", Type.GetType("System.Boolean"))
      .Columns.Add("ISLAND_NAME", Type.GetType("System.String")).AllowDBNull = True
      .Columns.Add("PROVIDER_ID", Type.GetType("System.String")).AllowDBNull = True
      .Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = True
      .Columns.Add("TERMINAL_NAME", Type.GetType("System.String")).AllowDBNull = True
    End With

  End Sub ' CreateDataTableForPrinting

  Private Function NewTableStylePrinting(ByVal TableName As String) As DataGridTableStyle
    Dim _table_style As DataGridTableStyle
    Dim _dgb_area As DataGridTextBoxColumn
    Dim _dgb_smoking As DataGridBoolColumn
    Dim _dgb_island As DataGridTextBoxColumn
    Dim _dgb_provider As DataGridTextBoxColumn
    Dim _dgb_terminal_id As DataGridTextBoxColumn
    Dim _dgb_terminal_name As DataGridTextBoxColumn

    _table_style = New DataGridTableStyle()
    _table_style.MappingName = TableName

    _dgb_area = New DataGridTextBoxColumn()
    _dgb_area.MappingName = "AREA_NAME"
    _dgb_area.HeaderText = GLB_NLS_GUI_CONTROLS.GetString(426)
    _dgb_area.Width = 180

    _dgb_smoking = New DataGridBoolColumn()
    _dgb_smoking.MappingName = "SMOKING"
    _dgb_smoking.HeaderText = GLB_NLS_GUI_CONTROLS.GetString(428)
    _dgb_smoking.Width = 80
    ' To disable null values in DataGridBoolColumn
    _dgb_smoking.AllowNull = False

    _dgb_island = New DataGridTextBoxColumn()
    _dgb_island.MappingName = "ISLAND_NAME"
    _dgb_island.HeaderText = GLB_NLS_GUI_CONTROLS.GetString(427)
    _dgb_island.Width = 180

    _dgb_provider = New DataGridTextBoxColumn()
    _dgb_provider.MappingName = "PROVIDER_ID"
    _dgb_provider.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(341)
    _dgb_provider.Width = 150

    _dgb_terminal_id = New DataGridTextBoxColumn()
    _dgb_terminal_id.MappingName = "TERMINAL_ID"
    _dgb_terminal_id.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(333)
    _dgb_terminal_id.Width = 70
    _dgb_provider.Alignment = HorizontalAlignment.Right

    _dgb_terminal_name = New DataGridTextBoxColumn()
    _dgb_terminal_name.MappingName = "TERMINAL_NAME"
    _dgb_terminal_name.HeaderText = GLB_NLS_GUI_AUDITOR.GetString(330)
    _dgb_terminal_name.Width = 200

    _table_style.GridColumnStyles.Add(_dgb_area)
    _table_style.GridColumnStyles.Add(_dgb_smoking)
    _table_style.GridColumnStyles.Add(_dgb_island)
    _table_style.GridColumnStyles.Add(_dgb_provider)
    _table_style.GridColumnStyles.Add(_dgb_terminal_id)
    _table_style.GridColumnStyles.Add(_dgb_terminal_name)

    Return _table_style
  End Function ' NewTableStylePrinting

  Private Function PrepareDataToPrint() As DataGrid
    Dim _dg_printing As DataGrid
    ''''Dim _table_style As DataGridTableStyle
    ''''Dim _new_row As DataRow
    ''''Dim _area_current_position As Integer
    ''''Dim _island_current_position As Integer
    ''''Dim _terminal_current_position As Integer
    ''''Dim _area_idx As Integer
    ''''Dim _island_idx As Integer
    ''''Dim _terminal_idx As Integer

    _dg_printing = New DataGrid()
    _dg_printing.DataSource = New DataView(m_dt_printing)
    m_dt_printing.Clear()

    ''''If IsNothing(_dg_printing.TableStyles.Item(m_dt_printing.TableName)) Then

    ''''  _table_style = NewTableStylePrinting(m_dt_printing.TableName)
    ''''  _dg_printing.TableStyles.Add(_table_style)
    ''''End If

    ''''_area_current_position = m_bs_areas.Position
    ''''_island_current_position = m_bs_islands.Position
    ''''_terminal_current_position = m_bs_terminals.Position

    ''''m_dt_printing.BeginLoadData()

    ''''For _area_idx = 0 To m_bs_areas.Count - 1
    ''''  m_bs_areas.Position = _area_idx

    ''''  For _island_idx = 0 To m_bs_islands.Count - 1
    ''''    m_bs_islands.Position = _island_idx

    ''''    For _terminal_idx = 0 To m_bs_terminals.Count - 1

    ''''      ' We have all needed data here. Add row to m_dt_printing.
    ''''      _new_row = m_dt_printing.NewRow()
    ''''      _new_row(COLUMN_PRINT_AREA_NAME) = dgv_areas.Rows(_area_idx).Cells(COLUMN_AREA_NAME).Value
    ''''      _new_row(COLUMN_PRINT_AREA_SMOKING) = dgv_areas.Rows(_area_idx).Cells(COLUMN_AREA_SMOKING).Value
    ''''      _new_row(COLUMN_PRINT_ISLAND_NAME) = dgv_islands.Rows(_island_idx).Cells(COLUMN_AREA_NAME).Value
    ''''      _new_row(COLUMN_PRINT_TERMINAL_PROVIDER_ID) = dgv_terminals.Rows(_terminal_idx).Cells(COLUMN_TERMINAL_PROVIDER_ID).Value
    ''''      _new_row(COLUMN_PRINT_TERMINAL_ID) = dgv_terminals.Rows(_terminal_idx).Cells(COLUMN_TERMINAL_ID).Value
    ''''      _new_row(COLUMN_PRINT_TERMINAL_NAME) = dgv_terminals.Rows(_terminal_idx).Cells(COLUMN_TERMINAL_NAME).Value
    ''''      m_dt_printing.Rows.Add(_new_row)
    ''''    Next
    ''''    ' Add one line to add a totalizer row.
    ''''    _new_row = m_dt_printing.NewRow()
    ''''    _new_row(COLUMN_PRINT_AREA_NAME) = dgv_areas.Rows(_area_idx).Cells(COLUMN_AREA_NAME).Value
    ''''    _new_row(COLUMN_PRINT_AREA_SMOKING) = dgv_areas.Rows(_area_idx).Cells(COLUMN_AREA_SMOKING).Value
    ''''    _new_row(COLUMN_PRINT_ISLAND_NAME) = dgv_islands.Rows(_island_idx).Cells(COLUMN_AREA_NAME).Value
    ''''    _new_row(COLUMN_PRINT_TERMINAL_PROVIDER_ID) = "TOTAL TERMS: " & m_bs_terminals.Count.ToString()
    ''''    _new_row(COLUMN_PRINT_TERMINAL_ID) = DBNull.Value
    ''''    _new_row(COLUMN_PRINT_TERMINAL_NAME) = DBNull.Value
    ''''    m_dt_printing.Rows.Add(_new_row)
    ''''  Next
    ''''  ' Add one line to add a totalizer row.
    ''''  _new_row = m_dt_printing.NewRow()
    ''''  _new_row(COLUMN_PRINT_AREA_NAME) = dgv_areas.Rows(_area_idx).Cells(COLUMN_AREA_NAME).Value
    ''''  _new_row(COLUMN_PRINT_AREA_SMOKING) = dgv_areas.Rows(_area_idx).Cells(COLUMN_AREA_SMOKING).Value
    ''''  _new_row(COLUMN_PRINT_ISLAND_NAME) = DBNull.Value
    ''''  _new_row(COLUMN_PRINT_TERMINAL_PROVIDER_ID) = "TOTAL ISLAS: " & m_bs_islands.Count.ToString()
    ''''  _new_row(COLUMN_PRINT_TERMINAL_ID) = DBNull.Value
    ''''  _new_row(COLUMN_PRINT_TERMINAL_NAME) = DBNull.Value
    ''''  m_dt_printing.Rows.Add(_new_row)
    ''''Next
    ''''' Add one line to add a totalizer row.
    ''''_new_row = m_dt_printing.NewRow()
    ''''_new_row(COLUMN_PRINT_AREA_NAME) = DBNull.Value
    ''''_new_row(COLUMN_PRINT_AREA_SMOKING) = DBNull.Value
    ''''_new_row(COLUMN_PRINT_ISLAND_NAME) = DBNull.Value
    ''''_new_row(COLUMN_PRINT_TERMINAL_PROVIDER_ID) = "TOTAL AREAS: " & m_bs_areas.Count.ToString()
    ''''_new_row(COLUMN_PRINT_TERMINAL_ID) = DBNull.Value
    ''''_new_row(COLUMN_PRINT_TERMINAL_NAME) = DBNull.Value
    ''''m_dt_printing.Rows.Add(_new_row)

    ''''m_dt_printing.EndLoadData()

    ''''m_bs_areas.Position = _area_current_position
    ''''m_bs_islands.Position = _island_current_position
    ''''m_bs_terminals.Position = _terminal_current_position

    Return _dg_printing
  End Function ' PrepareDataToPrint

  Private Sub AddHandles()

    'TreeView areas
    AddHandler tv_areas.ItemDrag, AddressOf tv_areas_ItemDrag
    AddHandler tv_areas.DragEnter, AddressOf tv_areas_DragEnter
    'AddHandler tv_areas.DragOver, AddressOf tv_areas_DragOver
    AddHandler tv_areas.DragOver, AddressOf tv_providers_DragOver
    AddHandler tv_areas.DragDrop, AddressOf tv_areas_DragDrop
    AddHandler tv_areas.Click, AddressOf tv_areas_Click
    AddHandler tv_areas.KeyUp, AddressOf tv_areas_KeyUp
    AddHandler tv_areas.AfterLabelEdit, AddressOf tv_areas_AfterLabelEdit

    'TreeView providers
    AddHandler tv_providers.ItemDrag, AddressOf tv_providers_ItemDrag
    AddHandler tv_providers.DragEnter, AddressOf tv_providers_DragEnter
    AddHandler tv_providers.DragOver, AddressOf tv_providers_DragOver
    AddHandler tv_providers.DragDrop, AddressOf tv_providers_DragDrop
    AddHandler tv_providers.Click, AddressOf tv_providers_Click

    AddHandler ef_area_name.EntryFieldValueChanged, AddressOf ef_area_name_EntryFieldValueChanged
    AddHandler chk_smoking_yes.CheckedChanged, AddressOf chk_smoking_yes_CheckedChanged
    AddHandler chk_smoking_no.CheckedChanged, AddressOf chk_smoking_no_CheckedChanged
    AddHandler ef_island_name.EntryFieldValueChanged, AddressOf ef_island_name_EntryFieldValueChanged
    AddHandler ef_terminal_name.EntryFieldValueChanged, AddressOf ef_terminal_name_EntryFieldValueChanged
    AddHandler uc_pr_list.ValueChangedEvent, AddressOf uc_pr_list_ValueChangedEvent

  End Sub ' AddHandles

  ' PURPOSE: Obtain the level of a given node.
  '          ROOT is level 0, AREA is 1, ISLAND is 2, TERMINAL is 3.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer
  Private Function GetNodeLevel(ByVal Node As TreeNode) As Integer
    Dim _level As Integer
    Dim _node As TreeNode

    _level = 0
    _node = Node.Parent

    While _node IsNot Nothing
      _level += 1
      _node = _node.Parent
    End While

    Return _level
  End Function ' GetNodeLevel

#End Region ' Private Functions

#Region " Events "

  ''''Private Sub dgv_areas_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
  ''''  Dim _area_name As String

  ''''  If dgv_areas.CurrentRow Is Nothing Then
  ''''    Return
  ''''  End If

  ''''  If dgv_areas.CurrentRow.Index <> -1 Then
  ''''    '''dgv_areas.CurrentRow.Selected = True
  ''''    _area_name = GetAreaName(dgv_areas.CurrentRow)
  ''''    If String.IsNullOrEmpty(_area_name) Then
  ''''      _area_name = "---"
  ''''    End If
  ''''    lbl_islands.Text = GLB_NLS_GUI_CONTROLS.GetString(431, _area_name)

  ''''    If dgv_areas.CurrentRow.Cells(COLUMN_AREA_ID).Value = DEFAULT_AREA_ID Then
  ''''      dgv_islands.AllowUserToAddRows = False
  ''''    Else
  ''''      dgv_islands.AllowUserToAddRows = Me.Permissions.Write
  ''''    End If
  ''''  End If
  ''''End Sub ' dgv_areas_CurrentCellChanged

  ''''Private Sub dgv_islands_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
  ''''  Dim _empty_island_name As String
  ''''  Dim _island_name As String

  ''''  _empty_island_name = "---"

  ''''  If dgv_islands.CurrentRow Is Nothing Then
  ''''    lbl_terminals.Text = GLB_NLS_GUI_CONTROLS.GetString(436, _empty_island_name)

  ''''    Return
  ''''  End If

  ''''  If dgv_islands.CurrentRow.Index <> -1 Then
  ''''    '''dgv_islands.CurrentRow.Selected = True
  ''''    _island_name = GetIslandName(dgv_islands.CurrentRow)
  ''''    If String.IsNullOrEmpty(_island_name) Then
  ''''      _island_name = _empty_island_name
  ''''    End If
  ''''    lbl_terminals.Text = GLB_NLS_GUI_CONTROLS.GetString(436, _island_name)
  ''''  End If
  ''''End Sub ' dgv_islands_CurrentCellChanged

  '''''''''' Excepcion debugando!!!
  '''''''''Private Sub dgv_areas_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs)
  '''''''''  Dim _dgv As DataGridView
  '''''''''  Dim _dgv_row As DataGridViewRow
  '''''''''  Dim _dgv_cell As DataGridViewCell

  '''''''''  If Not m_first_time Then
  '''''''''    Return
  '''''''''  End If
  '''''''''  m_first_time = False

  '''''''''  _dgv = sender

  '''''''''  For Each _dgv_row In _dgv.Rows
  '''''''''    For Each _dgv_cell In _dgv_row.Cells
  '''''''''      _dgv_cell.ToolTipText = _dgv_cell.Value
  '''''''''    Next
  '''''''''  Next
  '''''''''End Sub ' dgv_areas_DataBindingComplete

  ''''Private Sub dgv_areas_CellValueChanged(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs)
  ''''  Dim _dgv As DataGridView
  ''''  Dim _dgv_row As DataGridViewRow
  ''''  '''''Dim _dgv_cell As DataGridViewCell
  ''''  Dim _row_view As DataRowView
  ''''  Dim _area_row As DataRow
  ''''  Dim _island_row As DataRow
  ''''  Dim _area_name As String

  ''''  If e.RowIndex < 0 Or e.ColumnIndex < 0 Then
  ''''    Return
  ''''  End If

  ''''  _dgv = sender
  ''''  _dgv_row = _dgv.Rows(e.RowIndex)

  ''''  _dgv_row.Cells(COLUMN_AREA_VERSION_NUMBER).Value = m_version_number

  ''''  '''''_dgv_cell = _dgv_row.Cells(e.ColumnIndex)
  ''''  '''''_dgv_cell.ToolTipText = _dgv_cell.Value

  ''''  _row_view = _dgv_row.DataBoundItem
  ''''  If _row_view Is Nothing Then
  ''''    Return
  ''''  End If
  ''''  _area_row = _row_view.Row

  ''''  ' If Area name is changed, must change in all island childs.
  ''''  For Each _island_row In _area_row.GetChildRows(RELATION_AREA_ISLAND)
  ''''    _area_name = GetAreaName(_dgv_row)
  ''''    If _island_row(COLUMN_ISLAND_AREA_NAME) <> _area_name Then
  ''''      _island_row(COLUMN_ISLAND_AREA_NAME) = _area_name
  ''''    End If
  ''''  Next

  ''''  dgv_areas_CurrentCellChanged(Nothing, Nothing)

  ''''End Sub ' dgv_areas_CellValueChanged

  ''''Private Sub dgv_islands_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
  ''''  Dim _dgv As DataGridView
  ''''  Dim _dgv_row As DataGridViewRow
  ''''  '''''Dim _dgv_cell As DataGridViewCell
  ''''  Dim _row_view As DataRowView
  ''''  Dim _island_row As DataRow
  ''''  Dim _area_name As String

  ''''  If e.RowIndex < 0 Or e.ColumnIndex < 0 Then
  ''''    Return
  ''''  End If

  ''''  _dgv = sender
  ''''  _dgv_row = _dgv.Rows(e.RowIndex)

  ''''  _dgv_row.Cells(COLUMN_ISLAND_VERSION_NUMBER).Value = m_version_number

  ''''  '''''_dgv_cell = _dgv_row.Cells(e.ColumnIndex)
  ''''  '''''_dgv_cell.ToolTipText = _dgv_cell.Value

  ''''  _row_view = _dgv_row.DataBoundItem
  ''''  If _row_view Is Nothing Then
  ''''    Return
  ''''  End If
  ''''  _island_row = _row_view.Row

  ''''  _area_name = GetIslandAreaName(_island_row)
  ''''  If _island_row(COLUMN_ISLAND_AREA_NAME) <> _area_name Then
  ''''    _island_row(COLUMN_ISLAND_AREA_NAME) = _area_name
  ''''  End If

  ''''  dgv_islands_CurrentCellChanged(Nothing, Nothing)

  ''''End Sub ' dgv_islands_CellValueChanged

  ''''Private Sub dgv_terminals_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
  ''''  Dim _dgv As DataGridView
  ''''  Dim _dgv_row As DataGridViewRow

  ''''  If e.RowIndex < 0 Or e.ColumnIndex < 0 Then
  ''''    Return
  ''''  End If

  ''''  _dgv = sender
  ''''  _dgv_row = _dgv.Rows(e.RowIndex)

  ''''  _dgv_row.Cells(COLUMN_TERMINAL_VERSION_NUMBER).Value = m_version_number

  ''''  '''''_dgv_row.Cells(e.ColumnIndex).ToolTipText = _dgv_cell.Value

  ''''End Sub ' dgv_terminals_CellValueChanged

  ''''Private Sub dgv_areas_RowPostPaint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs)
  ''''  If Not String.IsNullOrEmpty(dgv_areas.Rows(e.RowIndex).ErrorText) Then
  ''''    dgv_areas.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
  ''''    dgv_areas.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.White
  ''''  Else
  ''''    dgv_areas.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
  ''''    dgv_areas.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Black
  ''''  End If
  ''''End Sub ' dgv_areas_RowPostPaint

  ''''Private Sub dgv_islands_RowPostPaint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs)
  ''''  If Not String.IsNullOrEmpty(dgv_islands.Rows(e.RowIndex).ErrorText) Then
  ''''    dgv_islands.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
  ''''    dgv_islands.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.White
  ''''  Else
  ''''    dgv_islands.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
  ''''    dgv_islands.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Black
  ''''  End If
  ''''End Sub ' dgv_islands_RowPostPaint

  ''''Private Sub dgv_terminals_RowPostPaint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs)
  ''''  If Not String.IsNullOrEmpty(dgv_terminals.Rows(e.RowIndex).ErrorText) Then
  ''''    dgv_terminals.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
  ''''    dgv_terminals.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.White
  ''''  Else
  ''''    dgv_terminals.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
  ''''    dgv_terminals.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Black
  ''''  End If
  ''''End Sub ' dgv_terminals_RowPostPaint

  ''''Private Sub dgv_areas_UserDeletingRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs)
  ''''  UserDeletingRow(e)
  ''''End Sub ' dgv_areas_UserDeletingRow

  ''''Private Sub dgv_islands_UserDeletingRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs)
  ''''  UserDeletingRow(e)
  ''''End Sub ' dgv_islands_UserDeletingRow

  Public Sub tv_areas_ItemDrag(ByVal sender As Object, ByVal e As ItemDragEventArgs)
    Dim _node As TreeNode
    Dim _effect As DragDropEffects

    _node = e.Item

    Select Case GetNodeLevel(_node)
      Case TREE_LEVEL.AREAS, TREE_LEVEL.ISLANDS
        _effect = DragDropEffects.None

      Case TREE_LEVEL.TERMINALS
        _effect = DragDropEffects.Move

      Case Else
        Return
    End Select

    DoDragDrop(e.Item, _effect Or DragDropEffects.Scroll)
  End Sub ' tv_areas_ItemDrag

  Public Sub tv_areas_DragEnter(ByVal sender As Object, ByVal e As DragEventArgs) Handles tv_providers.DragOver, tv_areas.DragOver
    e.Effect = DragDropEffects.Move
  End Sub ' tv_areas_DragEnter

  Private Sub tv_areas_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)
    Dim _tree_view As TreeView
    Dim _dest_node As TreeNode
    Dim _drag_node As TreeNode
    Dim _point As Point
    Dim _effect As DragDropEffects
    Dim _delta As Integer
    Dim _hover_time As TimeSpan

    _tree_view = sender

    If Not e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
      Return
    End If

    ' Get the node from the mouse position
    _point = _tree_view.PointToClient(New Point(e.X, e.Y))
    _dest_node = _tree_view.GetNodeAt(_point)
    _tree_view.SelectedNode = _dest_node

    ' Auto-Scroll
    _delta = _tree_view.Height - _point.Y
    If _delta < _tree_view.Height / 2 And _delta > 0 Then
      If _dest_node.NextVisibleNode IsNot Nothing Then
        _dest_node.NextVisibleNode.EnsureVisible()
      End If
    End If
    If _delta > _tree_view.Height / 2 And _delta < _tree_view.Height Then
      If _dest_node.PrevVisibleNode IsNot Nothing Then
        _dest_node.PrevVisibleNode.EnsureVisible()
      End If
    End If

    If e.AllowedEffect = DragDropEffects.None Then
      Return
    End If

    _effect = DragDropEffects.Move

    Select Case GetNodeLevel(_dest_node)
      Case TREE_LEVEL.AREAS, TREE_LEVEL.TERMINALS
        _effect = DragDropEffects.None
      Case TREE_LEVEL.ISLANDS
        ' This node is sure to be a Terminal
        _drag_node = CType(e.Data.GetData("System.Windows.Forms.TreeNode"), TreeNode)
        If _drag_node.Parent Is _dest_node Then
          _effect = DragDropEffects.None
        End If
      Case Else
        _effect = DragDropEffects.None
    End Select

    e.Effect = _effect

    'if we are on a new object, reset our timer
    'otherwise check to see if enough time has passed and expand the destination node
    If _dest_node IsNot m_last_drag_dest Then
      m_last_drag_dest = _dest_node
      m_last_drag_dest_time = WSI.Common.WGDB.Now
      m_last_drag_dest_already = False
    Else
      _hover_time = WSI.Common.WGDB.Now.Subtract(m_last_drag_dest_time)
      If _hover_time.TotalMilliseconds > 1000 And Not m_last_drag_dest_already Then
        _dest_node.Toggle()
        m_last_drag_dest_time = WSI.Common.WGDB.Now
        m_last_drag_dest_already = True
      End If
    End If

  End Sub ' tv_areas_DragOver

  Public Sub tv_areas_DragDrop(ByVal sender As Object, ByVal e As DragEventArgs)
    Dim _tree_view As TreeView
    Dim _drag_node As TreeNode
    Dim _dest_node As TreeNode
    Dim _new_node As TreeNode
    Dim _point As Point
    Dim _island_row As DataRow
    Dim _terminal_row As DataRow

    _tree_view = sender

    If Not e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
      Return
    End If

    _point = _tree_view.PointToClient(New Point(e.X, e.Y))
    _dest_node = _tree_view.GetNodeAt(_point)

    If _dest_node Is Nothing Then
      Return
    End If

    _drag_node = CType(e.Data.GetData("System.Windows.Forms.TreeNode"), TreeNode)

    _new_node = _drag_node.Clone()
    _new_node.Tag = _drag_node.Tag

    _island_row = _dest_node.Tag
    _terminal_row = _new_node.Tag

    _terminal_row.BeginEdit()
    _terminal_row(COLUMN_TERMINAL_BANK_ID) = _island_row(COLUMN_ISLAND_ID)
    _terminal_row(COLUMN_TERMINAL_BANK_NAME) = GetIslandName(_island_row)
    _terminal_row.EndEdit()

    _dest_node.Nodes.Add(_new_node)
    _dest_node.Expand()
    'Remove original node
    _drag_node.Remove()
  End Sub ' tv_areas_DragDrop

  Private Sub tv_areas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _tree_view As TreeView

    _tree_view = sender
    m_tree_view_islands = _tree_view

    _tree_view.SelectedNode = _tree_view.GetNodeAt(_tree_view.PointToClient(Cursor.Position))
    m_tree_view_islands.SelectedNode = _tree_view.GetNodeAt(_tree_view.PointToClient(Cursor.Position))
  End Sub ' tv_areas_Click

  Private Sub tv_areas_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Dim _tree_view As TreeView

    _tree_view = sender

    If e.KeyCode = Keys.F2 Then
      _tree_view.LabelEdit = True
      _tree_view.SelectedNode.BeginEdit()
    End If

  End Sub ' tv_areas_KeyUp

  Private Sub tv_areas_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs)
    Dim _row As DataRow

    _row = e.Node.Tag

    If e.Label Is Nothing OrElse e.Label.Trim().Length = 0 Then
      ' Can not be empty text
      Select Case _row.Table.TableName
        Case TABLE_AREA_NAME
          e.Node.Text = GetAreaName(_row)
      End Select

      e.CancelEdit = True

      Return
    End If

    Select Case _row.Table.TableName
      Case TABLE_AREA_NAME
        _row(COLUMN_AREA_NAME) = e.Label
        e.CancelEdit = True
        e.Node.Text = GetAreaName(_row)

      Case TABLE_ISLAND_NAME
        _row(COLUMN_ISLAND_NAME) = e.Label

      Case TABLE_TERMINAL_NAME
        _row(COLUMN_TERMINAL_NAME) = e.Label

      Case Else
        Return
    End Select

  End Sub ' tv_areas_AfterLabelEdit

  ' PURPOSE: Events of tv_providers TreeView
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Public Sub tv_providers_ItemDrag(ByVal sender As Object, ByVal e As ItemDragEventArgs)
    Dim _node As TreeNode
    Dim _effect As DragDropEffects

    _node = e.Item

    Select Case GetNodeLevel(_node)
      Case TREE_LEVEL_NO_BANK.PROVIDER
        _effect = DragDropEffects.None

      Case TREE_LEVEL_NO_BANK.TERMINALS
        _effect = DragDropEffects.Move

      Case Else
        Return
    End Select

    DoDragDrop(e.Item, _effect Or DragDropEffects.Scroll)
  End Sub ' tv_providers_ItemDrag

  Public Sub tv_providers_DragEnter(ByVal sender As Object, ByVal e As DragEventArgs) Handles tv_providers.DragOver, tv_areas.DragOver
    e.Effect = DragDropEffects.Move
  End Sub ' tv_tv_providers_DragEnter

  Private Sub tv_providers_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tv_providers.DragOver, tv_areas.DragOver
    Dim _tree_view As TreeView
    Dim _dest_node As TreeNode
    Dim _drag_node As TreeNode
    Dim _point As Point
    Dim _effect As DragDropEffects
    Dim _delta As Integer
    Dim _hover_time As TimeSpan

    _tree_view = CType(sender, TreeView)

    e.Effect = DragDropEffects.None

    If e.Data.GetData(GetType(TreeNode)) Is Nothing Then
      Return
    End If

    _point = _tree_view.PointToClient(New Point(e.X, e.Y))

    _dest_node = _tree_view.GetNodeAt(_point)

    If Not _dest_node Is Nothing Then
      e.Effect = DragDropEffects.Copy
      _tree_view.SelectedNode = _dest_node
    End If

    '''''''''''''''

    ' Auto-Scroll
    _delta = _tree_view.Height - _point.Y
    If _delta < _tree_view.Height / 2 And _delta > 0 Then
      If _dest_node.NextVisibleNode IsNot Nothing Then
        _dest_node.NextVisibleNode.EnsureVisible()
      End If
    End If
    If _delta > _tree_view.Height / 2 And _delta < _tree_view.Height Then
      If _dest_node.PrevVisibleNode IsNot Nothing Then
        _dest_node.PrevVisibleNode.EnsureVisible()
      End If
    End If

    If e.AllowedEffect = DragDropEffects.None Then
      Return
    End If

    _effect = DragDropEffects.Move

    Select Case GetNodeLevel(_dest_node)
      Case TREE_LEVEL.AREAS, TREE_LEVEL.TERMINALS
        _effect = DragDropEffects.None
      Case TREE_LEVEL.ISLANDS
        ' This node is sure to be a Terminal
        _drag_node = CType(e.Data.GetData("System.Windows.Forms.TreeNode"), TreeNode)
        If _drag_node.Parent Is _dest_node Then
          _effect = DragDropEffects.None
        End If
      Case Else
        _effect = DragDropEffects.None
    End Select

    e.Effect = _effect

    'if we are on a new object, reset our timer
    'otherwise check to see if enough time has passed and expand the destination node
    If _dest_node IsNot m_last_drag_dest Then
      m_last_drag_dest = _dest_node
      m_last_drag_dest_time = WSI.Common.WGDB.Now
      m_last_drag_dest_already = False
    Else
      _hover_time = WSI.Common.WGDB.Now.Subtract(m_last_drag_dest_time)
      If _hover_time.TotalMilliseconds > 1000 And Not m_last_drag_dest_already Then
        _dest_node.Toggle()
        m_last_drag_dest_time = WSI.Common.WGDB.Now
        m_last_drag_dest_already = True
      End If
    End If

  End Sub ' tv_providers_DragOver

  Public Sub tv_providers_DragDrop(ByVal sender As Object, ByVal e As DragEventArgs) Handles tv_providers.DragDrop, tv_areas.DragDrop
    Dim NewNode As TreeNode
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
      Dim pt As Point
      Dim DestinationNode As TreeNode
      pt = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
      DestinationNode = CType(sender, TreeView).GetNodeAt(pt)
      NewNode = CType(e.Data.GetData("System.Windows.Forms.TreeNode"),  _
                                      TreeNode)
      If Not DestinationNode.TreeView Is NewNode.TreeView Then
        DestinationNode.Nodes.Add(NewNode.Clone)
        DestinationNode.Expand()
        'Remove original node
        NewNode.Remove()
      End If
    End If
  End Sub ' tv_providers_DragDrop

  Private Sub tv_providers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim _tree_view_no_bank As TreeView

    _tree_view_no_bank = sender

    _tree_view_no_bank.SelectedNode = _tree_view_no_bank.GetNodeAt(_tree_view_no_bank.PointToClient(Cursor.Position))
  End Sub 'tv_providers_Click



  Private Sub ef_area_name_EntryFieldValueChanged()
    m_bs_areas.Filter = GetFilter_Areas()
  End Sub ' ef_area_name_EntryFieldValueChanged

  Private Sub chk_smoking_yes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    m_bs_areas.Filter = GetFilter_Areas()
  End Sub ' chk_smoking_yes_CheckedChanged

  Private Sub chk_smoking_no_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    m_bs_areas.Filter = GetFilter_Areas()
  End Sub ' chk_smoking_no_CheckedChanged

  Private Sub ef_island_name_EntryFieldValueChanged()
    m_bs_islands.Filter = GetFilter_Islands()
  End Sub ' ef_island_name_EntryFieldValueChanged

  Private Sub ef_terminal_name_EntryFieldValueChanged()
    m_bs_terminals.Filter = GetFilter_Terminals()
  End Sub ' ef_terminal_name_EntryFieldValueChanged

  Private Sub uc_pr_list_ValueChangedEvent()
    m_bs_terminals.Filter = GetFilter_Terminals()
  End Sub ' uc_pr_list_ValueChangedEvent

#End Region ' Events

End Class ' frm_area_island_sel
