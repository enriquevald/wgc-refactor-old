﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_time_disconnected
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_init_date = New System.Windows.Forms.GroupBox()
    Me.lbl_to_hour = New System.Windows.Forms.Label()
    Me.lbl_from_hour = New System.Windows.Forms.Label()
    Me.dtp_init_to = New GUI_Controls.uc_date_picker()
    Me.dtp_init_from = New GUI_Controls.uc_date_picker()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.uc_site_select = New GUI_Controls.uc_sites_sel()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.chk_totals = New System.Windows.Forms.CheckBox()
    Me.chk_show_only_disconnected = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_init_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_show_only_disconnected)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.chk_totals)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_init_date)
    Me.panel_filter.Controls.Add(Me.uc_site_select)
    Me.panel_filter.Size = New System.Drawing.Size(992, 246)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_site_select, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_init_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_totals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_only_disconnected, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 250)
    Me.panel_data.Size = New System.Drawing.Size(992, 321)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(986, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(986, 4)
    '
    'gb_init_date
    '
    Me.gb_init_date.Controls.Add(Me.lbl_to_hour)
    Me.gb_init_date.Controls.Add(Me.lbl_from_hour)
    Me.gb_init_date.Controls.Add(Me.dtp_init_to)
    Me.gb_init_date.Controls.Add(Me.dtp_init_from)
    Me.gb_init_date.Location = New System.Drawing.Point(19, 15)
    Me.gb_init_date.Name = "gb_init_date"
    Me.gb_init_date.Size = New System.Drawing.Size(240, 91)
    Me.gb_init_date.TabIndex = 1
    Me.gb_init_date.TabStop = False
    Me.gb_init_date.Text = "xDate"
    '
    'lbl_to_hour
    '
    Me.lbl_to_hour.AutoSize = True
    Me.lbl_to_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_to_hour.Location = New System.Drawing.Point(180, 54)
    Me.lbl_to_hour.Name = "lbl_to_hour"
    Me.lbl_to_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_to_hour.TabIndex = 15
    Me.lbl_to_hour.Text = "00:00"
    '
    'lbl_from_hour
    '
    Me.lbl_from_hour.AutoSize = True
    Me.lbl_from_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_from_hour.Location = New System.Drawing.Point(180, 26)
    Me.lbl_from_hour.Name = "lbl_from_hour"
    Me.lbl_from_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_from_hour.TabIndex = 14
    Me.lbl_from_hour.Text = "00:00"
    '
    'dtp_init_to
    '
    Me.dtp_init_to.Checked = True
    Me.dtp_init_to.IsReadOnly = False
    Me.dtp_init_to.Location = New System.Drawing.Point(6, 51)
    Me.dtp_init_to.Name = "dtp_init_to"
    Me.dtp_init_to.ShowCheckBox = True
    Me.dtp_init_to.ShowUpDown = False
    Me.dtp_init_to.Size = New System.Drawing.Size(162, 24)
    Me.dtp_init_to.SufixText = "Sufix Text"
    Me.dtp_init_to.SufixTextVisible = True
    Me.dtp_init_to.TabIndex = 1
    Me.dtp_init_to.TextWidth = 50
    Me.dtp_init_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_init_from
    '
    Me.dtp_init_from.Checked = True
    Me.dtp_init_from.IsReadOnly = False
    Me.dtp_init_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_init_from.Name = "dtp_init_from"
    Me.dtp_init_from.ShowCheckBox = True
    Me.dtp_init_from.ShowUpDown = False
    Me.dtp_init_from.Size = New System.Drawing.Size(162, 24)
    Me.dtp_init_from.SufixText = "Sufix Text"
    Me.dtp_init_from.SufixTextVisible = True
    Me.dtp_init_from.TabIndex = 0
    Me.dtp_init_from.TextWidth = 50
    Me.dtp_init_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(273, 11)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(393, 200)
    Me.uc_pr_list.TabIndex = 5
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'uc_site_select
    '
    Me.uc_site_select.Location = New System.Drawing.Point(17, 12)
    Me.uc_site_select.MultiSelect = True
    Me.uc_site_select.Name = "uc_site_select"
    Me.uc_site_select.ShowIsoCode = False
    Me.uc_site_select.ShowMultisiteRow = True
    Me.uc_site_select.Size = New System.Drawing.Size(287, 171)
    Me.uc_site_select.TabIndex = 0
    Me.uc_site_select.Visible = False
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(25, 153)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(209, 16)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'chk_totals
    '
    Me.chk_totals.AutoSize = True
    Me.chk_totals.Location = New System.Drawing.Point(25, 129)
    Me.chk_totals.Name = "chk_totals"
    Me.chk_totals.Size = New System.Drawing.Size(67, 17)
    Me.chk_totals.TabIndex = 2
    Me.chk_totals.Text = "xTotals"
    Me.chk_totals.UseVisualStyleBackColor = True
    '
    'chk_show_only_disconnected
    '
    Me.chk_show_only_disconnected.Location = New System.Drawing.Point(25, 175)
    Me.chk_show_only_disconnected.Name = "chk_show_only_disconnected"
    Me.chk_show_only_disconnected.Size = New System.Drawing.Size(209, 16)
    Me.chk_show_only_disconnected.TabIndex = 4
    Me.chk_show_only_disconnected.Text = "xShow only disconnected"
    '
    'frm_terminals_time_disconnected
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1000, 575)
    Me.Name = "frm_terminals_time_disconnected"
    Me.Text = "frm_terminals_disconnected_data"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_init_date.ResumeLayout(False)
    Me.gb_init_date.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_init_date As System.Windows.Forms.GroupBox
  Protected WithEvents lbl_to_hour As System.Windows.Forms.Label
  Protected WithEvents lbl_from_hour As System.Windows.Forms.Label
  Friend WithEvents dtp_init_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_init_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_site_select As GUI_Controls.uc_sites_sel
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_totals As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_only_disconnected As System.Windows.Forms.CheckBox
End Class
