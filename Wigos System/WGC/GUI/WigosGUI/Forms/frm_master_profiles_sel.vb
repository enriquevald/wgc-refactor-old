'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_master_profiles_sel.vb
' DESCRIPTION:   Master Profiles selection form
' AUTHOR:        Alberto Marcos
' CREATION DATE: 21-JUN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-JUN-2013  AMF    Initial version.
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On 

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

#End Region ' Imports

Public Class frm_master_profiles_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents ef_profile As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_profile = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_profile)
    Me.panel_filter.Size = New System.Drawing.Size(541, 81)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_profile, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 85)
    Me.panel_data.Size = New System.Drawing.Size(541, 301)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(535, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(535, 4)
    '
    'ef_profile
    '
    Me.ef_profile.DoubleValue = 0
    Me.ef_profile.IntegerValue = 0
    Me.ef_profile.IsReadOnly = False
    Me.ef_profile.Location = New System.Drawing.Point(6, 27)
    Me.ef_profile.Name = "ef_profile"
    Me.ef_profile.OnlyUpperCase = True
    Me.ef_profile.Size = New System.Drawing.Size(428, 25)
    Me.ef_profile.SufixText = "Sufix Text"
    Me.ef_profile.SufixTextVisible = True
    Me.ef_profile.TabIndex = 1
    Me.ef_profile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_profile.TextValue = ""
    Me.ef_profile.TextWidth = 60
    Me.ef_profile.Value = ""
    Me.ef_profile.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_master_profiles_sel
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(549, 390)
    Me.Name = "frm_master_profiles_sel"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region ' Windows Form Designer generated code

#Region " Constants "

  Private Const LEN_PROFILE_NAME As Integer = 40

  Private Const SQL_COLUMN_PROFILE_ID As Integer = 0
  Private Const SQL_COLUMN_PROFILE_NAME As Integer = 1

  Private Const GRID_COLUMN_PROFILE_ID As Integer = 0
  Private Const GRID_COLUMN_PROFILE_NAME As Integer = 1

  Private Const GRID_COLUMNS As Integer = 2
  Private Const GRID_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Members "

  ' For return items selected in selection mode
  Private UsersSelected() As Integer

  ' For report filters 
  Private m_profile_name As String

#End Region ' Members

#Region " Properties "

#End Region ' Properties

#Region " Overridable GUI function "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MASTER_PROFILE_SEL

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Buttons control. Replace New button by 2 custom buttons
    If ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION Then
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
      GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    End If

    ' Assign NLS id's
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2098)
    ef_profile.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)

    ' New Profile
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(2)

    ' Filters
    Call ef_profile.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROFILE_NAME)

    Call GUI_FilterReset()

    Call GUI_StyleView()

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_GetSelectedItems()

    Dim result_column As Integer
    Dim user_id As Integer

    Erase UsersSelected

    UsersSelected = Me.Grid.SelectedRows
    If IsNothing(UsersSelected) Then
      Exit Sub
    End If

    result_column = GRID_COLUMN_PROFILE_ID

    user_id = Me.Grid.Cell(UsersSelected(0), result_column).Value
    UsersSelected(0) = user_id

  End Sub 'GUI_GetSelectedItems

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Short
    Dim item_id As Integer
    Dim item_name As String
    Dim frm_edit As frm_profile_edit

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    item_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_ID).Value
    item_name = Me.Grid.Cell(idx_row, GRID_COLUMN_PROFILE_NAME).Value
    frm_edit = New frm_profile_edit(ENUM_FORM.FORM_MASTER_PROFILE_EDIT)

    Call frm_edit.ShowEditMaster(item_id, item_name)
    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Any data is ok in filter fields
    Return True

  End Function 'GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Return GetSqlProfiles() & GetFilterProfiles()

  End Function 'GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_FilterReset()

    ef_profile.Value = ""

  End Sub 'GUI_FilterReset

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call EditNewProfile()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_profile

  End Sub 'GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(319), m_profile_name)

  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2098)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_profile_name = ""

    ' Profile name
    If Me.ef_profile.Value.Length > 0 Then
      m_profile_name = Me.ef_profile.Value
    End If

  End Sub

#End Region ' GUI Reports

#End Region ' Overridable GUI function

#Region " Private Functions "

  ' PURPOSE: Open profile edit form to create New Profile
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub EditNewProfile()

    Dim frm_edit As frm_profile_edit

    frm_edit = New frm_profile_edit(ENUM_FORM.FORM_MASTER_PROFILE_EDIT)

    Call frm_edit.ShowNewMaster()
    frm_edit = Nothing
    Call Grid.Focus()

  End Sub 'EditNewProfile

  ' PURPOSE: Create the filter to query for profiles
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with conditions based on dialog items state
  '
  Private Function GetFilterProfiles() As String

    Dim filter_string As String

    filter_string = ""
    filter_string = filter_string & " WHERE GUP_PROFILE_ID > 0"

    ' Restrict to profiles with names that match the pattern
    If ef_profile.Value <> "" Then
      filter_string = filter_string & " AND" & GUI_FilterField("GUP_NAME", ef_profile.Value, False, False, True)
    End If

    filter_string = filter_string & " AND GUP_MASTER_ID IS NOT NULL ORDER BY GUP_PROFILE_ID "

    Return filter_string

  End Function 'GetFilterProfiles

  ' PURPOSE: Create query statment without filter to get profiles
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - a string with query statment
  '
  Private Function GetSqlProfiles() As String

    Dim str_sql As String

    str_sql = "SELECT  GUP_PROFILE_ID    " & _
              "      , GUP_NAME          " & _
              " FROM   GUI_USER_PROFILES "

    Return str_sql

  End Function 'GetSqlProfiles

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)
    Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    ' GUP_PROFILE_ID
    Me.Grid.Column(GRID_COLUMN_PROFILE_ID).Width = 0
    Me.Grid.Column(GRID_COLUMN_PROFILE_ID).Mapping = SQL_COLUMN_PROFILE_ID

    ' GUP_NAME
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Width = 3850
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Mapping = SQL_COLUMN_PROFILE_NAME
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_PROFILE_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

  End Sub 'GUI_StyleView

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for select mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - SelResType: Fix filter to this type of results
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - List of ProfileId or UserId selected
  Public Function ShowForSelect(ByVal SelResType As Integer) As Integer()

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION

    Me.Display(True)

    Return UsersSelected

  End Function ' ShowForSelect

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

#End Region ' Events

End Class
