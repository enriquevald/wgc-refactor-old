'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_users_assignation
' DESCRIPTION:   Edit users/runners to assign to a bank
' AUTHOR:        Xavi Guirado
' CREATION DATE: 04-JAN-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-NOV-2017  XGJ    Initial version
' 30-MAY-2018  DMT    PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12452 The two lists in 'Runners assignation' doesn't show the correct ones.
' 31-MAY-2018  DMT    PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12491 When we press Exit button on 'Runners assignation' having made no changes, message 'All unsaved changes will be lost' is shown.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Data
Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading
Imports WSI.Common

Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT

Public Class frm_users_assignation
  Inherits frm_base_edit

#Region " Constants "
  Private m_user_assignation_previos As CLASS_USERS_ASSIGNATION
  Private m_user_assignation_final As CLASS_USERS_ASSIGNATION
#End Region ' Constants

#Region " Members "

  Private m_default_button As IButtonControl
  Public m_bank As Integer
  Private m_area As Integer
  Private m_zona As Integer
  Private m_assigned_runners_has_change As Boolean
  Private m_initial_assigned_runners As List(Of LbItem)
  Private m_final_assigned_runners As List(Of LbItem)
  Private m_initial_runners As String
  Private m_final_runners As String

  Class LbItem
    Public _id As Integer
    Public _name As String

    Public Sub New(ByVal Id As Integer, ByVal Name As String)
      _id = Id
      _name = Name
    End Sub

    Public Overrides Function ToString() As String
      Return _name
    End Function

    Public Function GetName() As String
      Return _name
    End Function

    Public Function GetId() As Integer
      Return _id
    End Function

  End Class


#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MOBIBANK_USER_ASSIGNATION

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8768)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4576)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Area
    Me.ef_area_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)

    ' Bank
    Me.ef_bank_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)

    ' Zona
    Me.ef_zone_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)
  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  'RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim users_assignation As CLASS_USERS_ASSIGNATION

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_USERS_ASSIGNATION
        users_assignation = DbEditedObject
        users_assignation.Name = InitialRunners()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()


  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    Dim _runners As String

    _runners = ""

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        m_user_assignation_final = New CLASS_USERS_ASSIGNATION()

        If (m_assigned_runners_has_change) Then
          RemoveMobiliBanksAssignedToBank()

          For Each _item As LbItem In lb_assigned_runners.Items
            AddBankMobileToMobiBAnkAreas(_item.GetId, _item.GetName)
            _runners = _runners + " " + _item.GetName
          Next

          m_user_assignation_final.UserId = m_bank
          m_user_assignation_final.Name = _runners
          DbEditedObject = Me.m_user_assignation_final
        End If

        If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
          If Me.CloseOnOkClick Then
            Call Me.Close()
          End If

          Exit Sub
        End If

        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
        If DbStatus = ENUM_STATUS.STATUS_OK Then
          Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
        End If

        If DbStatus = ENUM_STATUS.STATUS_OK Then
          Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
          If Me.CloseOnOkClick Then
            Call Me.Close()
          End If
        End If

        Me.Close()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL
        m_user_assignation_final = New CLASS_USERS_ASSIGNATION()

        For Each _item As LbItem In lb_assigned_runners.Items
          If _runners = String.Empty Then
            _runners = _item.GetName
          Else
            _runners = _runners + ", " + _item.GetName
          End If
        Next

        If m_assigned_runners_has_change Then
          m_user_assignation_final.Name = _runners
          DbEditedObject = m_user_assignation_final
          Call Me.Close()
        Else
          Call Me.Close()
        End If
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select


  End Sub ' GUI_ButtonClick

#End Region ' Overrides

#Region " Private "

#End Region ' Private

#Region "Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.m_user_assignation_previos = New CLASS_USERS_ASSIGNATION()

    If (GetBankInfo()) Then
      GetAreaInfo()
    End If

    Me.ef_bank_name.IsReadOnly = True
    Me.ef_area_name.IsReadOnly = True
    Me.ef_zone_name.IsReadOnly = True

    ' Add all runners at the list
    GetAllRuners()

    ' Add all the assigned runners in the bank to the list
    GetAllAssignedRunners()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

    m_assigned_runners_has_change = False

  End Sub ' ShowForEdit

  Public Sub Show(Bank As Integer)

    ' Sets filter values
    m_bank = Bank

    ShowForEdit()
  End Sub

#End Region ' Public Functions

#Region " Events "

  Private Function GetBankInfo() As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine(" SELECT   BK_NAME                 ")
    _sb.AppendLine("        , BK_AREA_ID              ")
    _sb.AppendLine("   FROM   BANKS                   ")
    _sb.AppendLine("  WHERE   BK_BANK_ID  = @pBankId  ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = m_bank

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If _reader.Read() Then

              Me.ef_bank_name.Value = _reader(0)
              m_area = _reader(1)

            End If

          End Using

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try

    Return True

  End Function

  Private Function GetAreaInfo() As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine(" SELECT   AR_NAME                 ")
    _sb.AppendLine("        , AR_SMOKING              ")
    _sb.AppendLine("   FROM   AREAS                   ")
    _sb.AppendLine("  WHERE   AR_AREA_ID  = @pAreaId  ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pAreaId", SqlDbType.BigInt).Value = m_area

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If _reader.Read() Then
              Me.ef_area_name.Value = _reader(0)

              If (_reader(1) = 0) Then
                Me.ef_zone_name.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)
              Else
                Me.ef_zone_name.Value = GLB_NLS_GUI_CONFIGURATION.GetString(436)
              End If

            End If

          End Using

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try

    Return True


  End Function

  Private Function GetAllRuners() As Boolean
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    '_sb.AppendLine("     SELECT       DISTINCT GU_USER_ID,                                      ")
    '_sb.AppendLine("                  COALESCE(NULLIF(GU_FULL_NAME,''), '--------')             ")
    '_sb.AppendLine("      FROM        GUI_PROFILE_FORMS                                         ")
    '_sb.AppendLine("INNER JOIN        GUI_USER_PROFILES ON GPF_PROFILE_ID = GUP_PROFILE_ID      ")
    '_sb.AppendLine("INNER JOIN        GUI_USERS ON GU_PROFILE_ID = GUP_PROFILE_ID               ")
    '_sb.AppendLine("     WHERE        GPF_GUI_ID = @pMobibankProfile                            ")
    '_sb.AppendLine("       AND        GPF_READ_PERM = 1                                         ")
    '_sb.AppendLine("       AND        GU_USER_ID NOT IN (                                       ")
    '_sb.AppendLine("                                    SELECT       MBA_ACCOUNT_ID            ")
    '_sb.AppendLine("                                      FROM       MOBIBANK_AREAS             ")
    '_sb.AppendLine("                                     WHERE       MBA_BANK_ID = @pBankId)   ")

    'Comment for 'DMT 30-MAY-2018 PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12452 The two lists in 'Runners assignation' doesn't show the correct ones.
    '_sb.AppendLine("     SELECT       DISTINCT GU_USER_ID,                                      ")
    '_sb.AppendLine("                  COALESCE(NULLIF(GU_FULL_NAME,''), '--------')             ")
    '_sb.AppendLine("      FROM        GUI_USERS                                                 ")
    '_sb.AppendLine("INNER JOIN        MOBILE_BANKS ON GU_USER_ID = MB_USER_ID                   ")
    '_sb.AppendLine("     WHERE        GU_USER_ID NOT IN (                                       ")
    '_sb.AppendLine("                                    SELECT       MBA_ACCOUNT_ID             ")
    '_sb.AppendLine("                                      FROM       MOBIBANK_AREAS             ")
    '_sb.AppendLine("                                     WHERE       MBA_BANK_ID = @pBankId)    ")

    'DMT 30-MAY-2018 PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK - WIGOS-12452 The two lists in 'Runners assignation' doesn't show the correct ones.
    _sb.AppendLine("     SELECT DISTINCT GU_USER_ID,                                      ")
    _sb.AppendLine("                     COALESCE(NULLIF(GU_FULL_NAME,''), '--------')    ")
    _sb.AppendLine("     FROM GUI_USERS                                                   ")
    _sb.AppendLine("     LEFT OUTER JOIN   MOBILE_BANKS  ON  MB_USER_ID  =  GU_USER_ID    ")
    _sb.AppendLine("     WHERE GU_BLOCK_REASON = 0 And MB_BLOCKED = 0                     ")
    _sb.AppendLine("     AND MB_USER_ID IS NOT NULL                                       ")
    _sb.AppendLine("     AND   EXISTS (                                                   ")
    _sb.AppendLine("                   SELECT   *                                         ")
    _sb.AppendLine("                   FROM GUI_PROFILE_FORMS                             ")
    _sb.AppendLine("                   WHERE GPF_PROFILE_ID = GU_PROFILE_ID               ")
    _sb.AppendLine("                   AND   GPF_GUI_ID      = @pGUIId                    ")
    _sb.AppendLine("                   AND   GPF_FORM_ID     = @pMobibankAccess           ")
    _sb.AppendLine("                   AND   GPF_READ_PERM   = 1                          ")
    _sb.AppendLine("                  )                                                   ")
    _sb.AppendLine("     AND GU_USER_ID NOT IN (                                          ")
    _sb.AppendLine("                            SELECT MBA_ACCOUNT_ID                     ")
    _sb.AppendLine("                            FROM MOBIBANK_AREAS                       ")
    _sb.AppendLine("                            WHERE       MBA_BANK_ID = @pBankId        ")
    _sb.AppendLine("                           )                                          ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = m_bank
          _cmd.Parameters.Add("@pGUIId", SqlDbType.Int).Value = Convert.ToInt16(ENUM_GUI.WIGOS_GUI)
          _cmd.Parameters.Add("@pMobibankAccess", SqlDbType.Int).Value = Convert.ToInt16(ENUM_FORM.FORM_MOBIBANK_USER_CONFIG)

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If _reader.HasRows Then
              Do While _reader.Read()
                Me.lb_all_runners.Items.Add(New LbItem(_reader(0), _reader(1).ToString.ToUpper))
              Loop
            End If

          End Using

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try

    Return True

  End Function

  Private Function GetAllAssignedRunners() As Boolean
    Dim _sb As StringBuilder
    Dim _runner As LbItem
    Dim _assigned_users As String

    m_initial_assigned_runners = New List(Of LbItem)
    m_final_assigned_runners = New List(Of LbItem)
    _assigned_users = ""

    _sb = New StringBuilder()

    _sb.AppendLine(" SELECT   GU_USER_ID                                          ")
    _sb.AppendLine("        , GU_FULL_NAME                                        ")
    _sb.AppendLine(" FROM    MOBIBANK_AREAS                                       ")
    _sb.AppendLine(" INNER    JOIN GUI_USERS ON GU_USER_ID = MBA_ACCOUNT_ID       ")
    _sb.AppendLine(" INNER    JOIN MOBILE_BANKS ON MB_USER_ID = GU_USER_ID        ")
    _sb.AppendLine(" WHERE   MBA_BANK_ID = @pBankId                               ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = m_bank

          Using _reader As SqlDataReader = _cmd.ExecuteReader()

            If _reader.HasRows Then
              Do While _reader.Read()
                Me.lb_assigned_runners.Items.Add(New LbItem(_reader(0), _reader(1).ToString.ToUpper))
                _runner = New LbItem(_reader(0), _reader(1).ToString.ToUpper)
                m_initial_assigned_runners.Add(_runner)
                ' Audioria
                _assigned_users = _assigned_users + " " + _reader(1).ToString.ToUpper

              Loop
            End If

            m_user_assignation_previos.UserId = m_bank
            m_user_assignation_previos.Name = _assigned_users

          End Using

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try

    Return True
  End Function

  Private Function RemoveMobiliBanksAssignedToBank() As Boolean
    Dim _sb As StringBuilder
    Dim _row_affected As Integer

    _sb = New StringBuilder()

    _sb.AppendLine(" DELETE FROM  MOBIBANK_AREAS    ")
    _sb.AppendLine("  WHERE MBA_BANK_ID = @pBankId   ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = m_bank

          _row_affected = _cmd.ExecuteNonQuery()
          If _row_affected = 0 Then
            Return False
          End If

        End Using

        ' --- Ok
        _db_trx.Commit()
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try

    Return True
  End Function

  Private Function AddBankMobileToMobiBAnkAreas(ByRef MobileBankUserId As Integer, MobileBankName As String) As Boolean
    Dim _sb As StringBuilder
    Dim _row_affected As Integer
    Dim _runner As LbItem

    _sb = New StringBuilder()

    _sb.AppendLine(" INSERT INTO MOBIBANK_AREAS (MBA_ACCOUNT_ID, MBA_BANK_ID)   ")
    _sb.AppendLine(" VALUES (@pModiBankIserId, @pBankId)                          ")

    Try

      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pModiBankIserId", SqlDbType.BigInt).Value = MobileBankUserId
          _cmd.Parameters.Add("@pBankId", SqlDbType.BigInt).Value = m_bank

          _row_affected = _cmd.ExecuteNonQuery()

          If _row_affected = 0 Then
            Return False
          End If

          _runner = New LbItem(MobileBankUserId, MobileBankName)
          m_final_assigned_runners.Add(_runner)

        End Using

        ' --- Ok
        _db_trx.Commit()
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
      Return False
    End Try

    Return True
  End Function

#End Region ' Events

  Private Sub btn_add_selected_Click(sender As Object, e As EventArgs) Handles btn_add_selected.Click
    Dim _idx As Integer
    Dim _item As LbItem
    Dim _list As List(Of Integer)

    _list = New List(Of Integer)

    For _idx = 0 To Me.lb_all_runners.Items.Count - 1

      ' Determine if the item is selected.
      If Me.lb_all_runners.GetSelected(_idx) = True Then
        _item = Me.lb_all_runners.Items.Item(_idx)
        Me.lb_assigned_runners.Items.Add(New LbItem(_item.GetId, _item.GetName))
        _list.Add(_idx)
      End If
    Next

    For _idx = 0 To _list.Count - 1
      Me.lb_all_runners.Items.RemoveAt(_list((_list.Count - 1) - _idx))
    Next

    Me.lb_all_runners.Refresh()
    Me.lb_assigned_runners.Refresh()

    m_assigned_runners_has_change = True
  End Sub

  Private Sub btn_add_all_Click(sender As Object, e As EventArgs) Handles btn_add_all.Click
    Dim _idx As Integer
    Dim _item As LbItem
    Dim _list As List(Of Integer)

    _list = New List(Of Integer)

    For _idx = 0 To Me.lb_all_runners.Items.Count - 1
      _item = Me.lb_all_runners.Items.Item(_idx)
      Me.lb_assigned_runners.Items.Add(New LbItem(_item.GetId, _item.GetName))
      _list.Add(_idx)
    Next

    For _idx = 0 To _list.Count - 1
      Me.lb_all_runners.Items.RemoveAt(_list((_list.Count - 1) - _idx))
    Next

    Me.lb_all_runners.Refresh()
    Me.lb_assigned_runners.Refresh()

    m_assigned_runners_has_change = True
  End Sub

  Private Sub btn_delete_selected_Click(sender As Object, e As EventArgs) Handles btn_delete_selected.Click
    Dim _idx As Integer
    Dim _item As LbItem
    Dim _list As List(Of Integer)

    _list = New List(Of Integer)

    For _idx = 0 To Me.lb_assigned_runners.Items.Count - 1
      ' Determine if the item is selected.
      If Me.lb_assigned_runners.GetSelected(_idx) = True Then
        _item = Me.lb_assigned_runners.Items.Item(_idx)
        Me.lb_all_runners.Items.Add(New LbItem(_item.GetId, _item.GetName))
        _list.Add(_idx)
      End If
    Next

    For _idx = 0 To _list.Count - 1
      Me.lb_assigned_runners.Items.RemoveAt(_list((_list.Count - 1) - _idx))
    Next

    Me.lb_all_runners.Refresh()
    Me.lb_assigned_runners.Refresh()

    m_assigned_runners_has_change = True
  End Sub

  Private Sub btn_delete_all_Click(sender As Object, e As EventArgs) Handles btn_delete_all.Click
    Dim _idx As Integer
    Dim _item As LbItem
    Dim _list As List(Of Integer)

    _list = New List(Of Integer)

    For _idx = 0 To Me.lb_assigned_runners.Items.Count - 1
      _item = Me.lb_assigned_runners.Items.Item(_idx)
      Me.lb_all_runners.Items.Add(New LbItem(_item.GetId, _item.GetName))
      _list.Add(_idx)
    Next

    For _idx = 0 To _list.Count - 1
      Me.lb_assigned_runners.Items.RemoveAt(_list((_list.Count - 1) - _idx))
    Next

    Me.lb_all_runners.Refresh()
    Me.lb_assigned_runners.Refresh()

    m_assigned_runners_has_change = True
  End Sub


  Private Function InitialRunners() As String
    Dim _first As Boolean

    _first = True

    For Each _item As LbItem In m_initial_assigned_runners
      If _first Then
        m_initial_runners = _item.GetName.ToUpper
      Else
        m_initial_runners = m_initial_runners + ", " + _item.GetName.ToUpper
      End If

      _first = False
    Next

    Return m_initial_runners
  End Function

  Private Function FinalRunners() As String
    Dim _first As Boolean

    _first = True

    For Each _item As LbItem In m_final_assigned_runners
      If _first Then
        m_final_runners = _item.GetName.ToUpper
      Else
        m_final_runners = m_final_runners + ", " + _item.GetName.ToUpper
      End If

      _first = False
    Next

    Return m_final_runners
  End Function
End Class ' frm_mailing_programming_edit
