<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jackpot_monitor
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.Grid = New GUI_Controls.uc_grid
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.lbl_status = New GUI_Controls.uc_text_field
    Me.btn_exit = New GUI_Controls.uc_button
    Me.lbl_jackpot_contribution = New GUI_Controls.uc_text_field
    Me.lbl_to_compensate = New GUI_Controls.uc_text_field
    Me.lbl_accumulated_total = New GUI_Controls.uc_text_field
    Me.panel_buttons = New System.Windows.Forms.Panel
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'Grid
    '
    Me.Grid.CurrentCol = -1
    Me.Grid.CurrentRow = -1
    Me.Grid.Location = New System.Drawing.Point(8, 59)
    Me.Grid.Name = "Grid"
    Me.Grid.PanelRightVisible = False
    Me.Grid.Redraw = True
    Me.Grid.Size = New System.Drawing.Size(313, 71)
    Me.Grid.Sortable = False
    Me.Grid.SortAscending = True
    Me.Grid.SortByCol = 0
    Me.Grid.TabIndex = 2
    Me.Grid.ToolTipped = True
    Me.Grid.TopRow = -2
    '
    'Timer1
    '
    '
    'lbl_status
    '
    Me.lbl_status.IsReadOnly = True
    Me.lbl_status.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_status.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_status.Location = New System.Drawing.Point(8, 7)
    Me.lbl_status.Name = "lbl_status"
    Me.lbl_status.Size = New System.Drawing.Size(270, 25)
    Me.lbl_status.SufixText = "Sufix Text"
    Me.lbl_status.SufixTextVisible = True
    Me.lbl_status.TabIndex = 0
    Me.lbl_status.TextWidth = 170
    Me.lbl_status.Value = "xEnabled"
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(1, 3)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 0
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_jackpot_contribution
    '
    Me.lbl_jackpot_contribution.IsReadOnly = True
    Me.lbl_jackpot_contribution.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_jackpot_contribution.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_jackpot_contribution.Location = New System.Drawing.Point(8, 29)
    Me.lbl_jackpot_contribution.Name = "lbl_jackpot_contribution"
    Me.lbl_jackpot_contribution.Size = New System.Drawing.Size(246, 24)
    Me.lbl_jackpot_contribution.SufixText = "Sufix Text"
    Me.lbl_jackpot_contribution.SufixTextVisible = True
    Me.lbl_jackpot_contribution.TabIndex = 1
    Me.lbl_jackpot_contribution.TextWidth = 170
    Me.lbl_jackpot_contribution.Value = "x100.00%"
    '
    'lbl_to_compensate
    '
    Me.lbl_to_compensate.IsReadOnly = True
    Me.lbl_to_compensate.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_to_compensate.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_to_compensate.Location = New System.Drawing.Point(36, 147)
    Me.lbl_to_compensate.Name = "lbl_to_compensate"
    Me.lbl_to_compensate.Size = New System.Drawing.Size(282, 24)
    Me.lbl_to_compensate.SufixText = "Sufix Text"
    Me.lbl_to_compensate.SufixTextVisible = True
    Me.lbl_to_compensate.TabIndex = 4
    Me.lbl_to_compensate.TextWidth = 160
    Me.lbl_to_compensate.Value = "x134.00"
    '
    'lbl_accumulated_total
    '
    Me.lbl_accumulated_total.IsReadOnly = True
    Me.lbl_accumulated_total.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_accumulated_total.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_accumulated_total.Location = New System.Drawing.Point(36, 128)
    Me.lbl_accumulated_total.Name = "lbl_accumulated_total"
    Me.lbl_accumulated_total.Size = New System.Drawing.Size(282, 24)
    Me.lbl_accumulated_total.SufixText = "Sufix Text"
    Me.lbl_accumulated_total.SufixTextVisible = True
    Me.lbl_accumulated_total.TabIndex = 3
    Me.lbl_accumulated_total.TextWidth = 160
    Me.lbl_accumulated_total.Value = "x35.000.00"
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_exit)
    Me.panel_buttons.Location = New System.Drawing.Point(227, 177)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(94, 35)
    Me.panel_buttons.TabIndex = 5
    '
    'frm_jackpot_monitor
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(329, 221)
    Me.Controls.Add(Me.lbl_to_compensate)
    Me.Controls.Add(Me.lbl_accumulated_total)
    Me.Controls.Add(Me.panel_buttons)
    Me.Controls.Add(Me.lbl_jackpot_contribution)
    Me.Controls.Add(Me.lbl_status)
    Me.Controls.Add(Me.Grid)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_jackpot_monitor"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
    Me.Text = "frm_jackpot_monitor"
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Grid As GUI_Controls.uc_grid
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents lbl_status As GUI_Controls.uc_text_field
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents lbl_jackpot_contribution As GUI_Controls.uc_text_field
  Friend WithEvents lbl_to_compensate As GUI_Controls.uc_text_field
  Friend WithEvents lbl_accumulated_total As GUI_Controls.uc_text_field
  Friend WithEvents panel_buttons As System.Windows.Forms.Panel
End Class
