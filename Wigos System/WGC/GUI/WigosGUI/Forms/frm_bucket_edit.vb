﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_bucket_edit
'   DESCRIPTION : Edit buckets
'        AUTHOR : Samuel González
' CREATION DATE : 11-DIC-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-DIC-2015  SGB    Initial version. Backlog Item 7157
' 02-FEB-2016  ETP    Avoid crashing when ALL is bucket level is checked.
' 17-MAR-2016  JRC    Bug 10734. If Not enabled then avoid validations
' 02-MAY-2017  AMF    WIGOS-1714: Buckets - Necesario añadir 3 dígitos en el campo de expiracion de días
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Promotion
Imports WSI.Common.Buckets
Imports WSI.Common
Imports GUI_CommonMisc

Public Class frm_bucket_edit
  Inherits frm_base_edit

#Region " Constants "
  Public Const GRID_COLUMN_LEVEL = 0
  Public Const GRID_COLUMN_SE_OBTIENEN = 1
  Public Const GRID_COLUMN_A = 2
  Public Const GRID_COLUMN_UNIDADES = 3
  Public Const GRID_COLUMN_B = 4
  Public Const GRID_COLUMN_DE_JUGADO_NETWIN = 5

  Public Const WITH_COLUMN_LEVEL = 2050
  Public Const WITH_COLUMN_SE_OBTIENEN = 2200
  Public Const WITH_COLUMN_A = 1300
  Public Const WITH_COLUMN_UNIDADES = 1600
  Public Const WITH_COLUMN_B = 1300
  Public Const WITH_COLUMN_DE_JUGADO_NETWIN = 1740



#End Region ' Constants

#Region " Enum "

#End Region ' Enum

#Region " Members "

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BUCKET_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6997)

    chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6999)
    ef_bucket_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 100)
    ef_bucket_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)

    'Expiration
    gb_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7003)
    ef_expiration_days.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7006)
    ef_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    chk_expiration_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7009)
    chk_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7005)

    'Visible
    gb_visible.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7010)
    chk_lcd.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7011)
    chk_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7012)
    chk_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7013)

    rbLevelBased.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7015)
    rbGlobal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7016)

    lblType.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254) & ": "


    cb_CalculoCoefK.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7023)

    cb_CalculoCoefK.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7017)) '"Por jugado"

    cb_CalculoCoefK.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7020)) '"por netwin teórico" 
    cb_CalculoCoefK.SelectedIndex = 0

    GUI_DG_Coef_StyleSheet()

    'Task 10636:4. Multiple Buckets: Sincronización MultiSite: Condicionar sincronización según PlayerTracking_Mode
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False

    Dim IsCenter As Boolean = False
    Dim IsSite As Boolean = False
    Dim IsNoMultisite As Boolean = False

    If (WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False)) Then
      IsCenter = True
    End If

    If (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False)) Then
      IsSite = True
    End If

    If Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And Not WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
      IsNoMultisite = True
    End If

    If (IsCenter And CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Multisite) Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
    End If

    If (IsSite And CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site) Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
    End If

    If IsNoMultisite Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
    End If


    ' because of Bug 10734, If Not enabled then avoid validations, these 2 are became invisible and the group box redimensioned
    chk_lcd.Visible = False
    chk_promobox.Visible = False


  End Sub ' GUI_InitControls

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _bucket As CLASS_BUCKET
    Dim _bucket_read As CLASS_BUCKET
    Dim _bucket_edit As CLASS_BUCKET

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_BUCKET()
        _bucket = DbEditedObject
        _bucket.BucketId = 0
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2853), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE _
         , ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        _bucket_read = DbReadObject
        _bucket_edit = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE _
         , ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _bucket_edit = DbEditedObject
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2605), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, "") ' _
          'ef_stacker_id.Text, " '" & _stacker_edit.StackerId & "'")
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _bucket As CLASS_BUCKET
    Dim _bucket_can_disable As Boolean

    _bucket = DbReadObject


    ' chk_enabled
    chk_enabled.Checked = _bucket.Enabled
    _bucket_can_disable = Buckets.GetProperties(_bucket.BucketId).can_disable

    chk_enabled.Enabled = True

    If chk_enabled.Checked Then
      If Not _bucket_can_disable Then
        chk_enabled.Enabled = False
      End If
    End If


    chk_lcd.Enabled = Buckets.GetProperties(_bucket.BucketId).visible_lcd_enabled
    chk_promobox.Enabled = Buckets.GetProperties(_bucket.BucketId).visible_promobox_enabled

    chk_expiration_days.Checked = _bucket.ExpirationDays > 0
    chk_expiration_days_CheckedChanged(chk_expiration_days, New System.EventArgs)

    Select Case _bucket.BucketType
      Case BucketType.RedemptionPoints
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7028)

      Case BucketType.RankingLevelPoints
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7029)

      Case BucketType.Credit_NR
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7030)

      Case BucketType.Credit_RE
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7031)

      Case BucketType.Comp1_RedemptionPoints
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7032)

      Case BucketType.Comp2_Credit_NR
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7033)

      Case BucketType.Comp3_Credit_RE
        lblType.Text = lblType.Text & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7034)
    End Select


    ef_bucket_name.Value = _bucket.BucketName

    ef_expiration_days.Value = _bucket.ExpirationDays

    chk_expiration_date.Checked = _bucket.ExpirationDate <> Nothing
    chk_expiration_date_CheckedChanged(chk_expiration_date, New System.EventArgs)

    If _bucket.ExpirationDate = Nothing Then
      dtp_expiration_date.Value = Now
    Else
      dtp_expiration_date.Value = WSI.Common.Format.DBDatetimeToLocalDateTime(_bucket.ExpirationDate)
    End If

    chk_lcd.Checked = _bucket.LCD
    chk_cashier.Checked = _bucket.Cashier
    chk_promobox.Checked = _bucket.PromoBox

    ' GroupBox Expiration
    If (chk_expiration_days.Checked Or chk_expiration_date.Checked) Then
      gb_expiration.Enabled = True
    Else
      gb_expiration.Enabled = Buckets.GetProperties(_bucket.BucketId).gui_expiration_can_enabled
    End If


    RemoveHandler rbGlobal.CheckedChanged, AddressOf rbGlobal_CheckedChanged
    RemoveHandler rbLevelBased.CheckedChanged, AddressOf rbLevelBased_CheckedChanged

    rbLevelBased.Checked = _bucket.Level_flags
    dg_Coef_Level.Enabled = _bucket.Level_flags
    dg_Coef_Level.Visible = _bucket.Level_flags
    rbGlobal.Checked = Not _bucket.Level_flags
    dg_Coef_Global.Enabled = Not _bucket.Level_flags
    dg_Coef_Global.Visible = Not _bucket.Level_flags

    AddHandler rbGlobal.CheckedChanged, AddressOf rbGlobal_CheckedChanged
    AddHandler rbLevelBased.CheckedChanged, AddressOf rbLevelBased_CheckedChanged

    'clean grids
    dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value = ""
    dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value = ""
    For _idx As Integer = 1 To dg_Coef_Level.NumRows()
      dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value = ""
      dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value = ""
    Next

    If _bucket.Level_flags Then
      For _idx As Integer = 1 To CLASS_BUCKET.MAX_LEVELS
        If _bucket.Params_By_Level.ContainsKey(_idx) Then
          If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
            dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value = GUI_FormatCurrency(_bucket.Params_By_Level(_idx).Param_A)
            'dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value = GUI_FormatCurrency(_bucket.Params_By_Level(_idx).Param_B)
            dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_UNIDADES).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7018)
          Else
            dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value = GUI_FormatNumber(_bucket.Params_By_Level(_idx).Param_A, 2)
            'dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value = GUI_FormatNumber(_bucket.Params_By_Level(_idx).Param_B, 2)
            dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_UNIDADES).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502) & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7018)
          End If
          dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value = GUI_FormatCurrency(_bucket.Params_By_Level(_idx).Param_B)
        End If
      Next
    Else
      If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
        dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value = GUI_FormatCurrency(_bucket.Params_By_Level(1).Param_A)
        'dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value = GUI_FormatCurrency(_bucket.Params_By_Level(1).Param_B)
      Else
        dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value = GUI_FormatNumber(_bucket.Params_By_Level(1).Param_A, 2)
        'dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value = GUI_FormatNumber(_bucket.Params_By_Level(1).Param_B, 2)
      End If
      dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value = GUI_FormatCurrency(_bucket.Params_By_Level(1).Param_B)
    End If

    If _bucket.Param_K Then
      cb_CalculoCoefK.SelectedIndex = 1 '"por netwin teórico"
    Else
      cb_CalculoCoefK.SelectedIndex = 0 '"por jugado"
    End If
    cb_CalculoCoefK_ValueChangedEvent()
    ' 17-MAR-2016  
    chk_enabled_CheckedChanged()


  End Sub ' GUI_SetScreenData

  ' PURPOSE: It gets the values from the entry field and set them to a TITO_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _bucket As CLASS_BUCKET
    Dim m_closing_hour As Integer = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 8)
    Dim _level_param As CLASS_BUCKET.TYPE_BUCKET_LEVEL_PARAMS


    _bucket = DbEditedObject

    _bucket.BucketName = ef_bucket_name.Value
    _bucket.Enabled = chk_enabled.Checked
    _bucket.SystemType = BucketSystemType.System

    If chk_expiration_days.Checked Then
      _bucket.ExpirationDays = IIf(String.IsNullOrEmpty(ef_expiration_days.Value), 0, ef_expiration_days.Value)
    Else
      _bucket.ExpirationDays = 0
    End If


    If chk_expiration_date.Checked Then
      _bucket.ExpirationDate = dtp_expiration_date.Value.ToString("dd/MM")
    Else
      _bucket.ExpirationDate = Nothing
    End If

    _bucket.LCD = chk_lcd.Checked
    _bucket.Cashier = chk_cashier.Checked
    _bucket.PromoBox = chk_promobox.Checked

    _bucket.Params_By_Level.Clear()


    If rbLevelBased.Checked Then
      _bucket.Level_flags = True
      For _idx As Integer = 1 To CLASS_BUCKET.MAX_LEVELS
        _level_param = New CLASS_BUCKET.TYPE_BUCKET_LEVEL_PARAMS
        _level_param.Param_A = IIf(String.IsNullOrEmpty(dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value), 0, dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value)
        _level_param.Param_B = IIf(String.IsNullOrEmpty(dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value), 0, dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value)
        _bucket.Params_By_Level.Add(_idx, _level_param)
      Next
    Else
      _bucket.Level_flags = False
      _level_param = New CLASS_BUCKET.TYPE_BUCKET_LEVEL_PARAMS
      _level_param.Param_A = IIf(String.IsNullOrEmpty(dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value), 0, dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value)
      _level_param.Param_B = IIf(String.IsNullOrEmpty(dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value), 0, dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value)
      _bucket.Params_By_Level.Add(1, _level_param)

    End If
    _bucket.Param_K = (cb_CalculoCoefK.SelectedIndex = 1)


  End Sub ' GUI_GetScreenData

  ' PURPOSE: It checks if the entry data are ok calling the function CheckEntryData
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _bucket As CLASS_BUCKET
    Dim _result As Boolean = True
    Dim _a_value As Integer
    Dim _b_value As Integer

    _bucket = DbEditedObject

    If String.IsNullOrEmpty(ef_bucket_name.Value.Trim()) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_bucket_name.Text)

      Return False
    End If

    ' 17-MAR-2016  
    If Not chk_enabled.Checked Then
      'Not enabled, no validations
      Return True
    End If



    If rbLevelBased.Checked Then

      For _idx As Integer = 1 To CLASS_BUCKET.MAX_LEVELS
        _a_value = 0
        _b_value = 0

        If Not String.IsNullOrEmpty(dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value) Then
          _a_value = dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_A).Value
        End If

        If Not String.IsNullOrEmpty(dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value) Then
          _b_value = dg_Coef_Level.Cell(_idx - 1, GRID_COLUMN_B).Value
        End If

        If _a_value < 1 Then
          _result = False
        End If

        If _b_value < 1 Then
          _result = False
        End If
      Next

    Else
      _a_value = 0
      _b_value = 0

      If Not String.IsNullOrEmpty(dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value) Then
        _a_value = dg_Coef_Global.Cell(0, GRID_COLUMN_A).Value
      End If

      If Not String.IsNullOrEmpty(dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value) Then
        _b_value = dg_Coef_Global.Cell(0, GRID_COLUMN_B).Value
      End If

      If _a_value < 1 Then
        _result = False
      End If
      If _b_value < 1 Then
        _result = False
      End If
    End If

    If Not _result Then
      '4678 "coeficients A and B must be greater or equal than 1."
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7019), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Can disabled
    If Not chk_enabled.Checked Then

      If Not Buckets.GetProperties(_bucket.BucketId).can_disable Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7080), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_bucket_name.Value)
      End If

    End If

    Return True
  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Private Functions "

  Private Sub GUI_DG_Coef_StyleSheet()

    Dim _bucket As CLASS_BUCKET
    _bucket = DbEditedObject


    ' Grilla por level
    Call dg_Coef_Level.Init(6, 1, True)
    dg_Coef_Level.Sortable = False
    Dim _idx_row As Integer
    dg_Coef_Level.Clear()
    dg_Coef_Level.Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008)
    dg_Coef_Level.Column(1).Header(0).Text = ""
    dg_Coef_Level.Column(2).Header(0).Text = ""
    dg_Coef_Level.Column(3).Header(0).Text = ""
    dg_Coef_Level.Column(4).Header(0).Text = ""
    dg_Coef_Level.Column(5).Header(0).Text = " "
    dg_Coef_Level.Column(GRID_COLUMN_LEVEL).Editable = False
    dg_Coef_Level.Column(GRID_COLUMN_SE_OBTIENEN).Editable = False
    dg_Coef_Level.Column(GRID_COLUMN_A).Width = WITH_COLUMN_A
    dg_Coef_Level.Column(GRID_COLUMN_A).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER
    dg_Coef_Level.Column(GRID_COLUMN_A).IsMerged = False
    dg_Coef_Level.Column(GRID_COLUMN_A).Editable = True
    dg_Coef_Level.Column(GRID_COLUMN_A).EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
    dg_Coef_Level.Column(GRID_COLUMN_A).EditionControl.EntryField.IsReadOnly = False

    If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
      dg_Coef_Level.Column(GRID_COLUMN_A).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Else
      dg_Coef_Level.Column(GRID_COLUMN_A).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 2)
    End If

    dg_Coef_Level.Column(GRID_COLUMN_UNIDADES).Editable = False
    dg_Coef_Level.Column(GRID_COLUMN_LEVEL).Width = WITH_COLUMN_LEVEL
    dg_Coef_Level.Column(GRID_COLUMN_SE_OBTIENEN).Width = WITH_COLUMN_SE_OBTIENEN
    dg_Coef_Level.Column(GRID_COLUMN_UNIDADES).Width = WITH_COLUMN_UNIDADES
    dg_Coef_Level.Column(GRID_COLUMN_DE_JUGADO_NETWIN).Width = WITH_COLUMN_DE_JUGADO_NETWIN
    dg_Coef_Level.Column(GRID_COLUMN_DE_JUGADO_NETWIN).Editable = False
    dg_Coef_Level.Column(GRID_COLUMN_B).Editable = True
    dg_Coef_Level.Column(GRID_COLUMN_B).Width = WITH_COLUMN_B
    dg_Coef_Level.Column(GRID_COLUMN_B).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER
    dg_Coef_Level.Column(GRID_COLUMN_B).EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
    dg_Coef_Level.Column(GRID_COLUMN_B).EditionControl.EntryField.IsReadOnly = False

    'If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
    dg_Coef_Level.Column(GRID_COLUMN_B).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    'Else
    'dg_Coef_Level.Column(GRID_COLUMN_B).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    'End If

    'dg_Coef_Level.Column(GRID_COLUMN_LEVEL).Width = 1000

    For _idx As Integer = 1 To CLASS_BUCKET.MAX_LEVELS
      _idx_row = dg_Coef_Level.AddRow()
      dg_Coef_Level.Cell(_idx_row, GRID_COLUMN_LEVEL).Value = WSI.Common.GeneralParam.GetString("PlayerTracking", "Level0" & _idx & ".Name")
      dg_Coef_Level.Cell(_idx_row, GRID_COLUMN_SE_OBTIENEN).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7007)
      If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
        dg_Coef_Level.Cell(_idx_row, GRID_COLUMN_UNIDADES).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7018)
      Else
        dg_Coef_Level.Cell(_idx_row, GRID_COLUMN_UNIDADES).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502) & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7018)
      End If
    Next

    ' grilla global
    Call dg_Coef_Global.Init(6, 1, True)
    dg_Coef_Global.Sortable = False
    dg_Coef_Global.Clear()
    dg_Coef_Global.Column(GRID_COLUMN_LEVEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7008)
    dg_Coef_Global.Column(1).Header(0).Text = ""
    dg_Coef_Global.Column(2).Header(0).Text = ""
    dg_Coef_Global.Column(3).Header(0).Text = ""
    dg_Coef_Global.Column(4).Header(0).Text = ""
    dg_Coef_Global.Column(5).Header(0).Text = " "
    dg_Coef_Global.Column(GRID_COLUMN_LEVEL).Editable = False
    dg_Coef_Global.Column(GRID_COLUMN_SE_OBTIENEN).Editable = False
    dg_Coef_Global.Column(GRID_COLUMN_A).Width = WITH_COLUMN_A
    dg_Coef_Global.Column(GRID_COLUMN_A).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER
    dg_Coef_Global.Column(GRID_COLUMN_A).IsMerged = False
    dg_Coef_Global.Column(GRID_COLUMN_A).Editable = True
    dg_Coef_Global.Column(GRID_COLUMN_A).EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
    dg_Coef_Global.Column(GRID_COLUMN_A).EditionControl.EntryField.IsReadOnly = False

    If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
      dg_Coef_Global.Column(GRID_COLUMN_A).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Else
      dg_Coef_Global.Column(GRID_COLUMN_A).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 9, 2)
    End If

    dg_Coef_Global.Column(GRID_COLUMN_UNIDADES).Editable = False
    dg_Coef_Global.Column(GRID_COLUMN_LEVEL).Width = WITH_COLUMN_LEVEL
    dg_Coef_Global.Column(GRID_COLUMN_SE_OBTIENEN).Width = WITH_COLUMN_SE_OBTIENEN
    dg_Coef_Global.Column(GRID_COLUMN_UNIDADES).Width = WITH_COLUMN_UNIDADES
    dg_Coef_Global.Column(GRID_COLUMN_DE_JUGADO_NETWIN).Width = WITH_COLUMN_DE_JUGADO_NETWIN

    dg_Coef_Global.Column(GRID_COLUMN_B).Editable = True
    dg_Coef_Global.Column(GRID_COLUMN_DE_JUGADO_NETWIN).Editable = False
    dg_Coef_Global.Column(GRID_COLUMN_B).Width = WITH_COLUMN_B
    dg_Coef_Global.Column(GRID_COLUMN_B).EditionControl.Type = GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
    dg_Coef_Global.Column(GRID_COLUMN_B).EditionControl.EntryField.IsReadOnly = False

    'If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
    dg_Coef_Global.Column(GRID_COLUMN_B).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    'Else
    'dg_Coef_Global.Column(GRID_COLUMN_B).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    'End If

    'dg_Coef_Global.Column(GRID_COLUMN_LEVEL).Width = 1000
    _idx_row = dg_Coef_Global.AddRow()

    dg_Coef_Global.Cell(_idx_row, GRID_COLUMN_LEVEL).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    dg_Coef_Global.Cell(_idx_row, GRID_COLUMN_SE_OBTIENEN).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7007)

    If Buckets.GetBucketUnits(_bucket.BucketId) = BucketUnits.Money Then
      dg_Coef_Global.Cell(_idx_row, GRID_COLUMN_UNIDADES).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7018)
    Else
      dg_Coef_Global.Cell(_idx_row, GRID_COLUMN_UNIDADES).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(502) & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7018)
    End If




    ' dg_Coef_Global.Cell(_idx_row, GRID_COLUMN_DE_JUGADO_NETWIN).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7020)

    dg_Coef_Global.Visible = False
    dg_Coef_Level.Visible = False


  End Sub




  Private Sub EnabledExpiration(ByVal check As Boolean)

    'If check Then
    '  chk_enabled_expiration.Checked = True
    '  opt_expiration_days.Enabled = True
    '  opt_expiration_days.Checked = True
    '  ef_expiration_days.Enabled = True
    'Else
    '  chk_enabled_expiration.Checked = False
    '  opt_expiration_days.Checked = False
    '  opt_expiration_days.Enabled = False
    '  ef_expiration_days.Enabled = False
    'End If

  End Sub ' EnabledExpiration


#End Region ' Private Functions

#Region " Public Functions "

#End Region  ' Public Functions

#Region " Events "

  Private Sub chk_expiration_date_CheckedChanged(sender As Object, e As EventArgs) Handles chk_expiration_date.CheckedChanged
    dtp_expiration_date.Enabled = chk_expiration_date.Checked
  End Sub

  Private Sub chk_expiration_days_CheckedChanged(sender As Object, e As EventArgs) Handles chk_expiration_days.CheckedChanged
    ef_expiration_days.Enabled = chk_expiration_days.Checked
  End Sub

  Private Sub rbGlobal_CheckedChanged(sender As Object, e As EventArgs) Handles rbGlobal.CheckedChanged
    dg_Coef_Global.Enabled = rbGlobal.Checked
    dg_Coef_Global.Visible = dg_Coef_Global.Enabled
    'rbGlobal.Checked = Not rbLevelBased.Checked

  End Sub

  Private Sub rbLevelBased_CheckedChanged(sender As Object, e As EventArgs) Handles rbLevelBased.CheckedChanged
    dg_Coef_Level.Enabled = rbLevelBased.Checked
    dg_Coef_Level.Visible = dg_Coef_Level.Enabled
    'rbLevelBased.Checked = Not rbGlobal.Checked

  End Sub


  Private Sub cb_CalculoCoefK_ValueChangedEvent() Handles cb_CalculoCoefK.ValueChangedEvent

    If dg_Coef_Level.NumRows > 0 Then
      For _idx As Integer = 0 To CLASS_BUCKET.MAX_LEVELS - 1
        dg_Coef_Level.Cell(_idx, GRID_COLUMN_DE_JUGADO_NETWIN).Value = cb_CalculoCoefK.TextValue.ToLower
      Next

    End If
    If dg_Coef_Global.NumRows > 0 Then
      dg_Coef_Global.Cell(0, GRID_COLUMN_DE_JUGADO_NETWIN).Value = cb_CalculoCoefK.TextValue.ToLower
    End If


  End Sub

#End Region ' Events

  Private Sub chk_enabled_CheckedChanged() Handles chk_enabled.CheckedChanged
    ' 17-MAR-2016   new method

    cb_CalculoCoefK.Enabled = chk_enabled.Checked
    rbGlobal.Enabled = chk_enabled.Checked
    rbLevelBased.Enabled = chk_enabled.Checked
    chk_expiration_days.Enabled = chk_enabled.Checked
    chk_expiration_date.Enabled = chk_enabled.Checked
    dg_Coef_Level.Enabled = chk_enabled.Checked
    dg_Coef_Global.Enabled = chk_enabled.Checked
    chk_cashier.Enabled = chk_enabled.Checked
    chk_lcd.Enabled = chk_enabled.Checked
    chk_promobox.Enabled = chk_enabled.Checked

  End Sub
End Class ' frm_bucket_edit