<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_prize_taxes_report
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_prize_taxes_report))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_prize = New System.Windows.Forms.GroupBox()
    Me.cmb_prize_filter = New GUI_Controls.uc_combo()
    Me.ef_greater_than = New GUI_Controls.uc_entry_field()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel()
    Me.cmb_report_type = New GUI_Controls.uc_combo()
    Me.lbl_info = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_prize.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_info)
    Me.panel_filter.Controls.Add(Me.cmb_report_type)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Controls.Add(Me.gb_prize)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1373, 141)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_prize, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_report_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_info, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 145)
    Me.panel_data.Size = New System.Drawing.Size(1373, 523)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1367, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1367, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(315, 5)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(242, 72)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(230, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(230, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_prize
    '
    Me.gb_prize.Controls.Add(Me.cmb_prize_filter)
    Me.gb_prize.Controls.Add(Me.ef_greater_than)
    Me.gb_prize.Location = New System.Drawing.Point(561, 5)
    Me.gb_prize.Name = "gb_prize"
    Me.gb_prize.Size = New System.Drawing.Size(260, 72)
    Me.gb_prize.TabIndex = 2
    Me.gb_prize.TabStop = False
    Me.gb_prize.Text = "xPrize"
    '
    'cmb_prize_filter
    '
    Me.cmb_prize_filter.AllowUnlistedValues = False
    Me.cmb_prize_filter.AutoCompleteMode = False
    Me.cmb_prize_filter.IsReadOnly = False
    Me.cmb_prize_filter.Location = New System.Drawing.Point(6, 14)
    Me.cmb_prize_filter.Name = "cmb_prize_filter"
    Me.cmb_prize_filter.SelectedIndex = -1
    Me.cmb_prize_filter.Size = New System.Drawing.Size(248, 24)
    Me.cmb_prize_filter.SufixText = "Sufix Text"
    Me.cmb_prize_filter.SufixTextVisible = True
    Me.cmb_prize_filter.TabIndex = 0
    Me.cmb_prize_filter.TextCombo = Nothing
    Me.cmb_prize_filter.TextWidth = 0
    '
    'ef_greater_than
    '
    Me.ef_greater_than.DoubleValue = 0.0R
    Me.ef_greater_than.IntegerValue = 0
    Me.ef_greater_than.IsReadOnly = False
    Me.ef_greater_than.Location = New System.Drawing.Point(76, 41)
    Me.ef_greater_than.Name = "ef_greater_than"
    Me.ef_greater_than.PlaceHolder = Nothing
    Me.ef_greater_than.Size = New System.Drawing.Size(108, 24)
    Me.ef_greater_than.SufixText = "Sufix Text"
    Me.ef_greater_than.SufixTextVisible = True
    Me.ef_greater_than.TabIndex = 1
    Me.ef_greater_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_greater_than.TextValue = ""
    Me.ef_greater_than.TextWidth = 0
    Me.ef_greater_than.Value = ""
    Me.ef_greater_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(4, 2)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 0
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'cmb_report_type
    '
    Me.cmb_report_type.AllowUnlistedValues = False
    Me.cmb_report_type.AutoCompleteMode = False
    Me.cmb_report_type.IsReadOnly = False
    Me.cmb_report_type.Location = New System.Drawing.Point(826, 6)
    Me.cmb_report_type.Name = "cmb_report_type"
    Me.cmb_report_type.SelectedIndex = -1
    Me.cmb_report_type.Size = New System.Drawing.Size(255, 24)
    Me.cmb_report_type.SufixText = "Sufix Text"
    Me.cmb_report_type.SufixTextVisible = True
    Me.cmb_report_type.TabIndex = 3
    Me.cmb_report_type.TextCombo = Nothing
    Me.cmb_report_type.TextWidth = 90
    '
    'lbl_info
    '
    Me.lbl_info.AutoSize = True
    Me.lbl_info.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_info.Location = New System.Drawing.Point(312, 84)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.Size = New System.Drawing.Size(163, 13)
    Me.lbl_info.TabIndex = 4
    Me.lbl_info.Text = "xCMeansPrizeWithEvidence"
    '
    'frm_prize_taxes_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1383, 672)
    Me.Name = "frm_prize_taxes_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_prize_taxes_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_prize.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_prize As System.Windows.Forms.GroupBox
  Friend WithEvents ef_greater_than As GUI_Controls.uc_entry_field
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents cmb_report_type As GUI_Controls.uc_combo
  Friend WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents cmb_prize_filter As GUI_Controls.uc_combo
End Class
