<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_session_detail
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_cg_cage_session_status = New GUI_Controls.uc_entry_field()
    Me.ef_cg_cage_closing_date = New GUI_Controls.uc_entry_field()
    Me.ef_cg_cage_opening_date = New GUI_Controls.uc_entry_field()
    Me.ef_cg_cage_closing_user = New GUI_Controls.uc_entry_field()
    Me.ef_cg_cage_opening_user = New GUI_Controls.uc_entry_field()
    Me.ef_cg_session_name = New GUI_Controls.uc_entry_field()
    Me.dtp_working_day = New GUI_Controls.uc_date_picker()
    Me.uc_checked_list = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_checked_list)
    Me.panel_filter.Controls.Add(Me.dtp_working_day)
    Me.panel_filter.Controls.Add(Me.ef_cg_session_name)
    Me.panel_filter.Controls.Add(Me.ef_cg_cage_session_status)
    Me.panel_filter.Controls.Add(Me.ef_cg_cage_closing_date)
    Me.panel_filter.Controls.Add(Me.ef_cg_cage_opening_date)
    Me.panel_filter.Controls.Add(Me.ef_cg_cage_closing_user)
    Me.panel_filter.Controls.Add(Me.ef_cg_cage_opening_user)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1213, 150)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_cg_cage_opening_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_cg_cage_closing_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_cg_cage_opening_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_cg_cage_closing_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_cg_cage_session_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_cg_session_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.dtp_working_day, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 154)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(0)
    Me.panel_data.Size = New System.Drawing.Size(1213, 460)
    Me.panel_data.TabIndex = 0
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1207, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1207, 4)
    '
    'ef_cg_cage_session_status
    '
    Me.ef_cg_cage_session_status.DoubleValue = 0.0R
    Me.ef_cg_cage_session_status.IntegerValue = 0
    Me.ef_cg_cage_session_status.IsReadOnly = True
    Me.ef_cg_cage_session_status.Location = New System.Drawing.Point(78, 105)
    Me.ef_cg_cage_session_status.Name = "ef_cg_cage_session_status"
    Me.ef_cg_cage_session_status.PlaceHolder = Nothing
    Me.ef_cg_cage_session_status.Size = New System.Drawing.Size(291, 24)
    Me.ef_cg_cage_session_status.SufixText = "Sufix Text"
    Me.ef_cg_cage_session_status.SufixTextVisible = True
    Me.ef_cg_cage_session_status.TabIndex = 34
    Me.ef_cg_cage_session_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cg_cage_session_status.TextValue = ""
    Me.ef_cg_cage_session_status.TextWidth = 130
    Me.ef_cg_cage_session_status.Value = ""
    Me.ef_cg_cage_session_status.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cg_cage_closing_date
    '
    Me.ef_cg_cage_closing_date.DoubleValue = 0.0R
    Me.ef_cg_cage_closing_date.IntegerValue = 0
    Me.ef_cg_cage_closing_date.IsReadOnly = True
    Me.ef_cg_cage_closing_date.Location = New System.Drawing.Point(385, 75)
    Me.ef_cg_cage_closing_date.Name = "ef_cg_cage_closing_date"
    Me.ef_cg_cage_closing_date.PlaceHolder = Nothing
    Me.ef_cg_cage_closing_date.Size = New System.Drawing.Size(291, 24)
    Me.ef_cg_cage_closing_date.SufixText = "Sufix Text"
    Me.ef_cg_cage_closing_date.SufixTextVisible = True
    Me.ef_cg_cage_closing_date.TabIndex = 33
    Me.ef_cg_cage_closing_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cg_cage_closing_date.TextValue = ""
    Me.ef_cg_cage_closing_date.TextWidth = 130
    Me.ef_cg_cage_closing_date.Value = ""
    Me.ef_cg_cage_closing_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cg_cage_opening_date
    '
    Me.ef_cg_cage_opening_date.DoubleValue = 0.0R
    Me.ef_cg_cage_opening_date.IntegerValue = 0
    Me.ef_cg_cage_opening_date.IsReadOnly = True
    Me.ef_cg_cage_opening_date.Location = New System.Drawing.Point(385, 44)
    Me.ef_cg_cage_opening_date.Name = "ef_cg_cage_opening_date"
    Me.ef_cg_cage_opening_date.PlaceHolder = Nothing
    Me.ef_cg_cage_opening_date.Size = New System.Drawing.Size(291, 24)
    Me.ef_cg_cage_opening_date.SufixText = "Sufix Text"
    Me.ef_cg_cage_opening_date.SufixTextVisible = True
    Me.ef_cg_cage_opening_date.TabIndex = 32
    Me.ef_cg_cage_opening_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cg_cage_opening_date.TextValue = ""
    Me.ef_cg_cage_opening_date.TextWidth = 130
    Me.ef_cg_cage_opening_date.Value = ""
    Me.ef_cg_cage_opening_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cg_cage_closing_user
    '
    Me.ef_cg_cage_closing_user.DoubleValue = 0.0R
    Me.ef_cg_cage_closing_user.IntegerValue = 0
    Me.ef_cg_cage_closing_user.IsReadOnly = True
    Me.ef_cg_cage_closing_user.Location = New System.Drawing.Point(8, 75)
    Me.ef_cg_cage_closing_user.Name = "ef_cg_cage_closing_user"
    Me.ef_cg_cage_closing_user.PlaceHolder = Nothing
    Me.ef_cg_cage_closing_user.Size = New System.Drawing.Size(361, 24)
    Me.ef_cg_cage_closing_user.SufixText = "Sufix Text"
    Me.ef_cg_cage_closing_user.SufixTextVisible = True
    Me.ef_cg_cage_closing_user.TabIndex = 31
    Me.ef_cg_cage_closing_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cg_cage_closing_user.TextValue = ""
    Me.ef_cg_cage_closing_user.TextWidth = 130
    Me.ef_cg_cage_closing_user.Value = ""
    Me.ef_cg_cage_closing_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cg_cage_opening_user
    '
    Me.ef_cg_cage_opening_user.DoubleValue = 0.0R
    Me.ef_cg_cage_opening_user.IntegerValue = 0
    Me.ef_cg_cage_opening_user.IsReadOnly = True
    Me.ef_cg_cage_opening_user.Location = New System.Drawing.Point(8, 45)
    Me.ef_cg_cage_opening_user.Name = "ef_cg_cage_opening_user"
    Me.ef_cg_cage_opening_user.PlaceHolder = Nothing
    Me.ef_cg_cage_opening_user.Size = New System.Drawing.Size(361, 24)
    Me.ef_cg_cage_opening_user.SufixText = "Sufix Text"
    Me.ef_cg_cage_opening_user.SufixTextVisible = True
    Me.ef_cg_cage_opening_user.TabIndex = 30
    Me.ef_cg_cage_opening_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cg_cage_opening_user.TextValue = ""
    Me.ef_cg_cage_opening_user.TextWidth = 130
    Me.ef_cg_cage_opening_user.Value = ""
    Me.ef_cg_cage_opening_user.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cg_session_name
    '
    Me.ef_cg_session_name.DoubleValue = 0.0R
    Me.ef_cg_session_name.IntegerValue = 0
    Me.ef_cg_session_name.IsReadOnly = False
    Me.ef_cg_session_name.Location = New System.Drawing.Point(8, 15)
    Me.ef_cg_session_name.Name = "ef_cg_session_name"
    Me.ef_cg_session_name.PlaceHolder = Nothing
    Me.ef_cg_session_name.Size = New System.Drawing.Size(361, 24)
    Me.ef_cg_session_name.SufixText = "Sufix Text"
    Me.ef_cg_session_name.SufixTextVisible = True
    Me.ef_cg_session_name.TabIndex = 35
    Me.ef_cg_session_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cg_session_name.TextValue = ""
    Me.ef_cg_session_name.TextWidth = 130
    Me.ef_cg_session_name.Value = ""
    Me.ef_cg_session_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_working_day
    '
    Me.dtp_working_day.Checked = True
    Me.dtp_working_day.IsReadOnly = False
    Me.dtp_working_day.Location = New System.Drawing.Point(405, 15)
    Me.dtp_working_day.Name = "dtp_working_day"
    Me.dtp_working_day.ShowCheckBox = False
    Me.dtp_working_day.ShowUpDown = False
    Me.dtp_working_day.Size = New System.Drawing.Size(210, 24)
    Me.dtp_working_day.SufixText = "Sufix Text"
    Me.dtp_working_day.SufixTextVisible = True
    Me.dtp_working_day.TabIndex = 37
    Me.dtp_working_day.TextWidth = 110
    Me.dtp_working_day.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_checked_list
    '
    Me.uc_checked_list.GroupBoxText = "xCheckedList"
    Me.uc_checked_list.Location = New System.Drawing.Point(713, 1)
    Me.uc_checked_list.m_resize_width = 303
    Me.uc_checked_list.multiChoice = True
    Me.uc_checked_list.Name = "uc_checked_list"
    Me.uc_checked_list.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list.SelectedIndexesList = ""
    Me.uc_checked_list.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list.SelectedValuesList = ""
    Me.uc_checked_list.SetLevels = 2
    Me.uc_checked_list.Size = New System.Drawing.Size(303, 148)
    Me.uc_checked_list.TabIndex = 38
    Me.uc_checked_list.ValuesArray = New String(-1) {}
    '
    'frm_cage_session_detail
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
    Me.AutoScroll = True
    Me.ClientSize = New System.Drawing.Size(1223, 618)
    Me.Name = "frm_cage_session_detail"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_session_detail"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_cg_cage_session_status As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cg_cage_closing_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cg_cage_opening_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cg_cage_closing_user As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cg_cage_opening_user As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cg_session_name As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_working_day As GUI_Controls.uc_date_picker
  Friend WithEvents uc_checked_list As GUI_Controls.uc_checked_list
End Class
