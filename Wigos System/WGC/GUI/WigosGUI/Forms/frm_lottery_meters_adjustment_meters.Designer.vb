<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_lottery_meters_adjustment_meters
  Inherits GUI_Controls.frm_base_sel_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_working_day = New GUI_Controls.uc_date_picker()
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field()
    Me.panel_totals = New System.Windows.Forms.Panel()
    Me.lblTotalWinValue = New System.Windows.Forms.Label()
    Me.lblTotalWinText = New System.Windows.Forms.Label()
    Me.gb_site = New System.Windows.Forms.GroupBox()
    Me.txt_site_name = New GUI_Controls.uc_entry_field()
    Me.gb_terminal = New System.Windows.Forms.GroupBox()
    Me.ef_provider_name = New GUI_Controls.uc_entry_field()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.lbl_status = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.panel_totals.SuspendLayout()
    Me.gb_site.SuspendLayout()
    Me.gb_terminal.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_terminal)
    Me.panel_filter.Controls.Add(Me.gb_site)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1410, 78)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_site, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 131)
    Me.panel_data.Size = New System.Drawing.Size(1410, 547)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1404, 22)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1404, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_working_day)
    Me.gb_date.Location = New System.Drawing.Point(298, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(213, 63)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_working_day
    '
    Me.dtp_working_day.Checked = True
    Me.dtp_working_day.IsReadOnly = True
    Me.dtp_working_day.Location = New System.Drawing.Point(6, 23)
    Me.dtp_working_day.Name = "dtp_working_day"
    Me.dtp_working_day.ShowCheckBox = False
    Me.dtp_working_day.ShowUpDown = False
    Me.dtp_working_day.Size = New System.Drawing.Size(200, 24)
    Me.dtp_working_day.SufixText = "Sufix Text"
    Me.dtp_working_day.SufixTextVisible = True
    Me.dtp_working_day.TabIndex = 4
    Me.dtp_working_day.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0.0R
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(6, 12)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.PlaceHolder = Nothing
    Me.ef_terminal_name.Size = New System.Drawing.Size(280, 24)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 0
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 110
    Me.ef_terminal_name.Value = ""
    Me.ef_terminal_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'panel_totals
    '
    Me.panel_totals.Controls.Add(Me.lblTotalWinValue)
    Me.panel_totals.Controls.Add(Me.lblTotalWinText)
    Me.panel_totals.Dock = System.Windows.Forms.DockStyle.Top
    Me.panel_totals.Location = New System.Drawing.Point(5, 82)
    Me.panel_totals.Name = "panel_totals"
    Me.panel_totals.Size = New System.Drawing.Size(1410, 49)
    Me.panel_totals.TabIndex = 1
    '
    'lblTotalWinValue
    '
    Me.lblTotalWinValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblTotalWinValue.Location = New System.Drawing.Point(222, 23)
    Me.lblTotalWinValue.Name = "lblTotalWinValue"
    Me.lblTotalWinValue.Size = New System.Drawing.Size(140, 19)
    Me.lblTotalWinValue.TabIndex = 1
    Me.lblTotalWinValue.Text = "lblTotalWinValue"
    Me.lblTotalWinValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblTotalWinText
    '
    Me.lblTotalWinText.AutoSize = True
    Me.lblTotalWinText.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblTotalWinText.Location = New System.Drawing.Point(219, 3)
    Me.lblTotalWinText.Name = "lblTotalWinText"
    Me.lblTotalWinText.Padding = New System.Windows.Forms.Padding(0, 0, 0, 7)
    Me.lblTotalWinText.Size = New System.Drawing.Size(109, 20)
    Me.lblTotalWinText.TabIndex = 0
    Me.lblTotalWinText.Text = "lblTotalWinText"
    Me.lblTotalWinText.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'gb_site
    '
    Me.gb_site.Controls.Add(Me.txt_site_name)
    Me.gb_site.Location = New System.Drawing.Point(6, 8)
    Me.gb_site.Name = "gb_site"
    Me.gb_site.Size = New System.Drawing.Size(285, 63)
    Me.gb_site.TabIndex = 0
    Me.gb_site.TabStop = False
    Me.gb_site.Text = "xSite"
    '
    'txt_site_name
    '
    Me.txt_site_name.DoubleValue = 0.0R
    Me.txt_site_name.Enabled = False
    Me.txt_site_name.IntegerValue = 0
    Me.txt_site_name.IsReadOnly = False
    Me.txt_site_name.Location = New System.Drawing.Point(4, 23)
    Me.txt_site_name.Name = "txt_site_name"
    Me.txt_site_name.PlaceHolder = Nothing
    Me.txt_site_name.Size = New System.Drawing.Size(273, 24)
    Me.txt_site_name.SufixText = "Sufix Text"
    Me.txt_site_name.SufixTextVisible = True
    Me.txt_site_name.TabIndex = 1
    Me.txt_site_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_site_name.TextValue = ""
    Me.txt_site_name.TextWidth = 50
    Me.txt_site_name.Value = ""
    Me.txt_site_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.ef_provider_name)
    Me.gb_terminal.Controls.Add(Me.ef_terminal_name)
    Me.gb_terminal.Location = New System.Drawing.Point(517, 8)
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.Size = New System.Drawing.Size(293, 63)
    Me.gb_terminal.TabIndex = 2
    Me.gb_terminal.TabStop = False
    Me.gb_terminal.Text = "xTerminal"
    '
    'ef_provider_name
    '
    Me.ef_provider_name.DoubleValue = 0.0R
    Me.ef_provider_name.IntegerValue = 0
    Me.ef_provider_name.IsReadOnly = False
    Me.ef_provider_name.Location = New System.Drawing.Point(6, 33)
    Me.ef_provider_name.Name = "ef_provider_name"
    Me.ef_provider_name.PlaceHolder = Nothing
    Me.ef_provider_name.Size = New System.Drawing.Size(280, 24)
    Me.ef_provider_name.SufixText = "Sufix Text"
    Me.ef_provider_name.SufixTextVisible = True
    Me.ef_provider_name.TabIndex = 1
    Me.ef_provider_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_provider_name.TextValue = ""
    Me.ef_provider_name.TextWidth = 110
    Me.ef_provider_name.Value = ""
    Me.ef_provider_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_status)
    Me.gb_status.Location = New System.Drawing.Point(818, 8)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(185, 63)
    Me.gb_status.TabIndex = 3
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_status
    '
    Me.lbl_status.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_status.Location = New System.Drawing.Point(11, 28)
    Me.lbl_status.Name = "lbl_status"
    Me.lbl_status.Size = New System.Drawing.Size(162, 13)
    Me.lbl_status.TabIndex = 17
    Me.lbl_status.Text = "xWorkingDayStatus"
    Me.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frm_lottery_meters_adjustment_meters
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1420, 682)
    Me.Controls.Add(Me.panel_totals)
    Me.Name = "frm_lottery_meters_adjustment_meters"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_lottery_meters_adjustment_meters"
    Me.Controls.SetChildIndex(Me.panel_filter, 0)
    Me.Controls.SetChildIndex(Me.panel_totals, 0)
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.panel_totals.ResumeLayout(False)
    Me.panel_totals.PerformLayout()
    Me.gb_site.ResumeLayout(False)
    Me.gb_terminal.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_working_day As GUI_Controls.uc_date_picker
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents panel_totals As System.Windows.Forms.Panel
  Friend WithEvents lblTotalWinValue As System.Windows.Forms.Label
  Friend WithEvents lblTotalWinText As System.Windows.Forms.Label
  Friend WithEvents gb_site As System.Windows.Forms.GroupBox
  Friend WithEvents txt_site_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents ef_provider_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_status As System.Windows.Forms.Label
End Class
