'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_island_edit
' DESCRIPTION:   New or Edit island item
' AUTHOR:        Susana Sams�
' CREATION DATE: 20-FEB-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-FEB-2012  SSC    Initial version.
' 17-SEP-2012  JAB    Fixed bug, when island�s name has a "'" fails.
' 06-MAY-2013  RBG    Protect from Null Exception when an Area is created whith no name.
' 19-JUN-2013  RCI    Fixed bug #884: When edit/create island, show the correct choice of smoking
' 28-OCT-2016  OVS    Allow save in the island alphanumeric field and increase its capacity on 5
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class frm_island_edit
  Inherits frm_base_edit

#Region " Constants "
#End Region ' Constants

#Region " Members "

  Private m_input_island_name As String
  Private m_delete_operation As Boolean
  Private m_input_area_id As Integer
  Private m_island_mask As String

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ISLAND_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(572)                    ' 352 "Edici�n de Isla"

    ' Delete button is only available whe editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False  ' (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT)

    cmb_area.Text = GLB_NLS_GUI_CONFIGURATION.GetString(435)                ' 435 "Area"
    ef_smoking.Text = GLB_NLS_GUI_CONFIGURATION.GetString(430)              ' 430 "Zona"
    ef_bank.Text = GLB_NLS_GUI_CONFIGURATION.GetString(431)                 ' 431 "Isla"

    ef_smoking.IsReadOnly = True
    ef_smoking.TabStop = False

    m_island_mask = GetIslandMask()

    'Accepts alphanumeric filter and extends lenght 3 to 12
    ef_bank.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 12, 0)

    Call AreaComboFill()

    AddHandler cmb_area.ValueChangedEvent, AddressOf cmb_area_ValueChangedEvent

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _reg_ex

    ' The following values must be entered
    '   - Bank or island : Mandator

    If ef_bank.Value.Length = 0 Then
      ' 123 "Debe introducir un valor."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_bank.Text)
      Call ef_bank.Focus()

      Return False
    End If

    _reg_ex = New Regex(m_island_mask)

    If Not _reg_ex.IsMatch(ef_bank.Value) Then
      Return ErrorValidacion()
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _island As CLASS_ISLAND
    Dim _table As DataTable

    _island = DbReadObject

    ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)
    cmb_area.TextValue = ""
    cmb_area.SelectedIndex = 0

    If _island.AreaName <> "" Then
      cmb_area.TextValue = _island.AreaName

    ElseIf m_input_area_id <> 0 Then
      _table = GUI_GetTableUsingSQL("SELECT AR_NAME FROM AREAS WHERE AR_AREA_ID = " & m_input_area_id, Integer.MaxValue)

      If Not IsNothing(_table) Then
        If _table.Rows.Count() > 0 Then
          cmb_area.TextValue = _table.Rows.Item(0).Item("AR_NAME")
        End If
      End If

    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _island.BankId = 0 Then
      ef_bank.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 12, 0)
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
      GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
      ef_bank.Enabled = False

      AreaComboFillDefaultValue()

    End If

    ef_bank.Value = _island.Name

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()

    Dim _island As CLASS_ISLAND
    Dim _table As DataTable

    _island = DbEditedObject

    If _ef_bank.Enabled Then

      _island.Name = ef_bank.Value

      'Area
      If Not cmb_area.TextValue Is Nothing Then
        _table = GUI_GetTableUsingSQL("SELECT AR_AREA_ID FROM AREAS WHERE AR_NAME = '" & cmb_area.TextValue.Replace("'", "''") & "'", Integer.MaxValue)
      Else
        _table = Nothing
      End If

      If Not IsNothing(_table) Then
        If _table.Rows.Count() > 0 Then
          _island.AreaId = _table.Rows.Item(0).Item("AR_AREA_ID")
          _island.AreaName = cmb_area.TextValue
        Else
          _island.AreaName = ""
          _island.AreaId = -1I
        End If
      End If
    End If

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _island As CLASS_ISLAND
    Dim _aux_nls As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_ISLAND
        _island = DbEditedObject
        _island.BankId = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 137 "Se ha producido un error al leer la isla %1."
          _island = Me.DbEditedObject
          _aux_nls = m_input_island_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(137), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 138 "Ya existe una isla con el nombre %1."
          _island = Me.DbEditedObject
          _aux_nls = _island.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(138), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_bank.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 139 "Se ha producido un error al a�adir la isla %1."
          _island = Me.DbEditedObject
          _aux_nls = _island.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(139), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          ' 138 "Ya existe una isla con el nombre %1."
          _island = Me.DbEditedObject
          _aux_nls = _island.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(138), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
          Call ef_bank.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 140 "Se ha producido un error al modificar la isla %1."
          _island = Me.DbEditedObject
          _aux_nls = _island.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(140), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        ' 142 "�Est� seguro que quiere borrar la isla %1?"
        _island = Me.DbEditedObject
        _aux_nls = _island.Name
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(142), _
                         ENUM_MB_TYPE.MB_TYPE_WARNING, _
                         ENUM_MB_BTN.MB_BTN_YES_NO, _
                         ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _aux_nls)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          ' 141 "Se ha producido un error al borrar la isla %1."
          _island = Me.DbEditedObject
          _aux_nls = _island.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(141), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_bank

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '

  Public Overloads Sub ShowNewItem(ByVal AreaId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Me.m_input_area_id = AreaId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal BankId As Integer)

    Dim _table As DataTable
    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = BankId

    _table = GUI_GetTableUsingSQL("SELECT BK_NAME FROM BANKS WHERE BK_BANK_ID = " & BankId, Integer.MaxValue)

    If Not IsNothing(_table) Then
      If _table.Rows.Count() > 0 Then
        Me.m_input_island_name = _table.Rows.Item(0).Item("BK_NAME")
      End If
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region ' Overrides

#Region " Private functions "

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub AreaComboFill()
    Dim idx As Integer
    Dim table As DataTable

    table = GUI_GetTableUsingSQL("SELECT DISTINCT AR_NAME FROM AREAS WHERE AR_AREA_ID <> 0 ORDER BY AR_NAME", Integer.MaxValue)

    For idx = 0 To table.Rows.Count - 1
      Me.cmb_area.Add(table.Rows.Item(idx).Item("AR_NAME"))
    Next

  End Sub     ' AreaComboFill

  ' PURPOSE: Init areas combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub AreaComboFillDefaultValue()
    Dim table As DataTable

    table = GUI_GetTableUsingSQL("SELECT AR_NAME FROM AREAS WHERE AR_AREA_ID = 0", Integer.MaxValue)
    If (table.Rows.Count > 0) Then
      Me.cmb_area.Clear()
      Me.cmb_area.Add(table.Rows.Item(0).Item("AR_NAME"))
    End If

  End Sub     ' AreaComboFill

  Private Function GetIslandMask() As String

    Dim general_params As DataTable
    Dim sql_text As String

    ' Build query
    sql_text = "select GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'Terminal'" _
              & "  AND  GP_SUBJECT_KEY = 'Island.Mask'"

    ' Check query results
    general_params = GUI_GetTableUsingSQL(sql_text, 1)

    ' Obtain data
    Return general_params.Rows.Item(0).Item("GP_KEY_VALUE").ToString.Trim()

  End Function 'GetIslandMask

  Private Function ErrorValidacion() As Boolean
    '(1654) "El formato del campo %1 no es correcto."
    'Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
    Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_bank.Text)
    Call ef_bank.Focus()
    Return False

  End Function 'ErrorValidacion

#End Region 'Private functions

#Region " Events "

  ' PURPOSE: Area selection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub cmb_area_ValueChangedEvent()

    Dim table As DataTable

    'Zone (smoking - no smoking)
    ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(437)

    If Not cmb_area.TextValue = Nothing Then
      ' JAB 17-SEP-2012 Fixed bug, when island�s name has a "'" fails.
      table = GUI_GetTableUsingSQL("SELECT AR_SMOKING FROM AREAS WHERE AR_NAME = '" & cmb_area.TextValue.Replace("'", "''") & "'", Integer.MaxValue)

      If Not IsNothing(table) Then
        If table.Rows.Count() > 0 Then
          If table.Rows.Item(0).Item("AR_SMOKING") Then
            ef_smoking.Value = GLB_NLS_GUI_CONFIGURATION.GetString(436)
          End If
        End If
      End If
    End If
  End Sub 'cmb_area_ValueChangedEvent

#End Region

End Class