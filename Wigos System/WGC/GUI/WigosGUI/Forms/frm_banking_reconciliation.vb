﻿'--------------------------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_banking_reconciliation.vb
'   DESCRIPTION :  Banking reconciliation
'        AUTHOR :  Luis Tenorio
' CREATION DATE :  05-AGO-2016
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 05-AGO-2016  LTC    Initial version - Product Backlog Item 15199:TPV Televisa: GUI, banking cards reconciliations
' 23-AGO-2016  LTC    Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
' 05-OCT-2016  FAV    Invalid value showed in for the Amount column.
' 14-OCT-2016  ETP    Bug 18619:Televisa: Columns has to be sortable.
' 24-OCT-2016  ATB    Bug 19232:WigosGUI: Al ordenar los datos mostrados en "Conciliación de tarjetas bancarias" no es posible desmarcar o marcar la casilla
' 26-OCT-2016  ETP    Bug 18619: Sortable has errors that can not be solved in this version.
' 07-FEB-2018  DHA    Bug 31436:WIGOS-8001 [Ticket #12150] Todos: Error Reporte de Sesión de Caja Tarjeta Bancaria/PinPad
' 13-FEB-2018  EOR    Bug 31480: WIGOS-8155 Bank card reconciliation operation is not audited 
'----------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports System.IO
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common.TITO
Imports WSI.Common
Imports WSI.Common.PinPad
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_Reports

Public Class frm_banking_reconciliation
  Inherits frm_base_edit

#Region " Constants"

#Region " Central Grid"

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 7

  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_OPERATION_NUMBER As Integer = 2
  Private Const GRID_COLUMN_AUTHORIZATION_CODE As Integer = 3
  Private Const GRID_COLUMN_TOTAL_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_CARD_NUMBER As Integer = 5
  Private Const GRID_COLUMN_RECONCILIATED As Integer = 6

  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_OPERATION_NUMBER As Integer = 1500
  Private Const GRID_WIDTH_AUTHORIZATION_CODE As Integer = 1500
  Private Const GRID_WIDTH_TOTAL_AMOUNT As Integer = 1500
  Private Const GRID_WIDTH_CARD_NUMBER As Integer = 1800
  Private Const GRID_WIDTH_RECONCILIATED As Integer = 3000

  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_OPERATION_NUMBER As Integer = 1
  Private Const SQL_COLUMN_AUTHORIZATION_CODE As Integer = 2
  Private Const SQL_COLUMN_TOTAL_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_CARD_NUMBER As Integer = 4
  Private Const SQL_COLUMN_RECONCILIATED As Integer = 5

#End Region

#End Region

#Region " Members"

  Private m_query_as_table As DataTable
  Private m_user_selected As Int32
  Private m_user_name As String
  Private m_withdrawal As String
  Private m_cashier_session_id As Int64
  Private ItemsSelected As Integer
  Private m_print_datetime As Date
  Private m_report_name As String
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA
  Private m_print_data As CLASS_PRINT_DATA

#End Region

#Region " Constructor"

  Public Sub New()

    InitializeComponent()

    Me.m_user_selected = 0
    Me.m_withdrawal = ""

    Me.ef_amount_given_amount.Text = 0
    Me.ef_amount_given_count.Text = 0
    Me.ef_amount_nogiven_amount.Text = 0
    Me.ef_amount_nogiven_count.Text = 0

    m_print_data = New CLASS_PRINT_DATA()

  End Sub

#End Region

#Region " Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BANKING_RECONCILIATION
    Call MyBase.GUI_SetFormId()

  End Sub

  Protected Sub GUI_EditSelectedItem()

    If IsNothing(Me.Grid.SelectedRows()) Then
      Exit Sub
    End If

    ItemsSelected = Me.Grid.Cell(Me.Grid.SelectedRows(0), 2).Value

    If (Me.Grid.Cell(Me.Grid.SelectedRows(0), 6).Value = True) Then
      Me.Grid.Cell(Me.Grid.SelectedRows(0), 6).Value = False
    Else
      Me.Grid.Cell(Me.Grid.SelectedRows(0), 6).Value = True
    End If

    Call CalculateTotals()

  End Sub

  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7490)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4576)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(5)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(27)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)


    Call Me.GUI_StyleSheet()
    Call Me.SetDefaultValues()
    Call GUI_ExecuteQueryCustom()

    If Grid.NumRows > 0 Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True
    End If

    m_report_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7490)

  End Sub

  Public Function GUI_SetupRow(RowIndex As Integer, DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean

    With Grid

      If Not DbRow.IsNull(SQL_COLUMN_DATE) Then
        .Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_OPERATION_NUMBER) Then
        .Cell(RowIndex, GRID_COLUMN_OPERATION_NUMBER).Value = DbRow.Value(SQL_COLUMN_OPERATION_NUMBER)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_AUTHORIZATION_CODE) Then
        .Cell(RowIndex, GRID_COLUMN_AUTHORIZATION_CODE).Value = DbRow.Value(SQL_COLUMN_AUTHORIZATION_CODE)
      End If

      If Not DbRow.IsNull(SQL_COLUMN_TOTAL_AMOUNT) Then
        .Cell(RowIndex, GRID_COLUMN_TOTAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_AMOUNT))
      End If

      If Not DbRow.IsNull(SQL_COLUMN_CARD_NUMBER) Then
        .Cell(RowIndex, GRID_COLUMN_CARD_NUMBER).Value = DbRow.Value(SQL_COLUMN_CARD_NUMBER)
      End If

      .Cell(RowIndex, GRID_COLUMN_RECONCILIATED).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED

    End With

    Return True
  End Function

  Protected Sub GUI_ExecuteQueryCustom()
    Dim _sql_command As SqlCommand = New SqlCommand()
    Dim db_row As frm_base_sel.CLASS_DB_ROW
    Dim _query_as_datatable As DataTable
    Dim _row As DataRow
    Dim idx_row As Integer

    Call GUI_StyleSheet()

    _sql_command.CommandText = "GetBankTransacctionsToReconciliate"
    _sql_command.CommandType = CommandType.StoredProcedure
    _sql_command.Parameters.Add("@pUser", SqlDbType.Int).Value = m_user_selected
    _sql_command.Parameters.Add("@pWithdrawal", SqlDbType.VarChar).Value = m_withdrawal 'LTC 23-AGO-2016

    _query_as_datatable = GUI_GetTableUsingCommand(_sql_command, Integer.MaxValue)

    If _query_as_datatable.Rows.Count > 0 Then

      m_query_as_table = _query_as_datatable

      For Each _row In _query_as_datatable.Rows
        Try
          db_row = New frm_base_sel.CLASS_DB_ROW(_row)

          Me.Grid.AddRow()
          idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(idx_row, db_row) Then
            Me.Grid.DeleteRowFast(idx_row)
          End If

        Catch ex As OutOfMemoryException
          Throw
        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "GUI_ExecuteQueryCustom", _
                                "GetBankTransacctionsToReconciliate", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

      Call GUI_AfterLastRow()
    End If
  End Sub

  Protected Sub GUI_AfterLastRow()
    Call SetRowTotalGrid()
    Call CalculateTotals()
  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK

        If CheckSelected() > 0 Then
          'LTC 23-AGO-2016
          If MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7519), MsgBoxStyle.YesNo, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6504)) = MsgBoxResult.No Then
            Exit Sub
          End If
        Else
          If MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7520), MsgBoxStyle.YesNo, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6504)) = MsgBoxResult.No Then
            Exit Sub
          End If
        End If

        Call AceptChanges()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_EXCEL)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select
  End Sub

  Protected Overrides Sub GUI_PrintResultList()
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      If Not IsNothing(m_excel_data) Then
        m_excel_data = Nothing
      End If


      For _idx_row As Integer = 0 To Me.Grid.NumRows - 1
        If (Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = uc_grid.GRID_CHK_CHECKED) Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = "X"
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = " "
        End If
      Next

      m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA(True, True)

      If m_excel_data.IsNull() Then
        Return
      End If

      If Not m_excel_data.Cancelled Then
        Call GUI_ReportParams(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportFilter(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportData(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_PrintReport(m_excel_data)
      End If

      For _idx_row As Integer = 0 To Me.Grid.NumRows - 1
        If (Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = "X") Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = uc_grid.GRID_CHK_UNCHECKED
        End If
        If _idx_row = Me.Grid.NumRows - 1 Or _idx_row = Me.Grid.NumRows - 2 Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Picture = CLASS_GUI_RESOURCES.GetImage("window.png")
        End If
      Next


    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "frm_banking_reconciliation.GUI_PrintResultList()", _
                            "", _
                            "", _
                            "", _
                           "Error in Print.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    Finally
      If m_excel_data Is Nothing Then
      Else
        m_excel_data.Dispose()
        m_excel_data = Nothing
      End If
    End Try

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Protected Overrides Sub GUI_ExcelResultList()
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      If Not IsNothing(m_excel_data) Then
        m_excel_data = Nothing

      End If

      For _idx_row As Integer = 0 To Me.Grid.NumRows - 1
        If (Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = uc_grid.GRID_CHK_CHECKED) Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = "X"
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = " "
        End If
      Next

      m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA()

      If m_excel_data.IsNull() Then
        Return
      End If

      If Not m_excel_data.Cancelled Then
        Call GUI_ReportParams(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportFilter(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportData(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportFooter(m_excel_data)
      End If

      If Not m_excel_data.Cancelled Then
        Call GUI_PrintReport(m_excel_data)
      End If

      For _idx_row As Integer = 0 To Me.Grid.NumRows - 1
        If (Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = "X") Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Value = uc_grid.GRID_CHK_UNCHECKED
        End If
        If _idx_row = Me.Grid.NumRows - 1 Or _idx_row = Me.Grid.NumRows - 2 Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Picture = CLASS_GUI_RESOURCES.GetImage("window.png")
        End If
      Next

    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "frm_banking_reconciliation.GUI_ExcelResultList()", _
                            "", _
                            "", _
                            "", _
                           "Error in Excel data export.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    Finally
      If m_excel_data Is Nothing Then
      Else
        m_excel_data.Dispose()
        m_excel_data = Nothing
      End If
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub

#End Region

#Region " Private Functions"

  Private Sub GUI_StyleSheet()

    With Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
      .EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE

      .Column(GRID_COLUMN_SELECT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794)
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7491)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_OPERATION_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794)
      .Column(GRID_COLUMN_OPERATION_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6166)
      .Column(GRID_COLUMN_OPERATION_NUMBER).Width = GRID_WIDTH_OPERATION_NUMBER
      .Column(GRID_COLUMN_OPERATION_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794)
      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1619)
      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Width = GRID_WIDTH_AUTHORIZATION_CODE
      .Column(GRID_COLUMN_AUTHORIZATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794)
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Width = GRID_WIDTH_TOTAL_AMOUNT
      .Column(GRID_COLUMN_TOTAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_CARD_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6794)
      .Column(GRID_COLUMN_CARD_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5469)
      .Column(GRID_COLUMN_CARD_NUMBER).Width = GRID_WIDTH_CARD_NUMBER
      .Column(GRID_COLUMN_CARD_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_RECONCILIATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7494)
      .Column(GRID_COLUMN_RECONCILIATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7425)
      .Column(GRID_COLUMN_RECONCILIATED).Width = GRID_WIDTH_RECONCILIATED
      .Column(GRID_COLUMN_RECONCILIATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_RECONCILIATED).Editable = True
      .Column(GRID_COLUMN_RECONCILIATED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX


    End With

  End Sub

  Private Sub SetDefaultValues()

    Me.gb_delivered.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7492)
    Me.gb_no_delivered.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7493)

    Me.ef_amount_given_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
    Me.ef_amount_given_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7495)
    Me.ef_amount_nogiven_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
    Me.ef_amount_nogiven_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7495)

    Me.ef_amount_given_amount.Value = 0
    Me.ef_amount_given_count.Value = 0
    Me.ef_amount_nogiven_amount.Value = 0
    Me.ef_amount_nogiven_count.Value = 0

    Me.ef_amount_given_amount.Enabled = False
    Me.ef_amount_given_count.Enabled = False
    Me.ef_amount_nogiven_amount.Enabled = False
    Me.ef_amount_nogiven_count.Enabled = False

  End Sub

  Private Sub SetRowTotalGrid()
    Dim _idx_row As Integer
    Dim _total_amount As Decimal

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    'Label - TOTAL
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    'Total Amount
    _total_amount = m_query_as_table.Compute("SUM(pt_total_amount)", "")
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_AMOUNT).Value = GUI_FormatCurrency(_total_amount)
    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Picture = CLASS_GUI_RESOURCES.GetImage("")

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    'Label - USER
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2608)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TOTAL_AMOUNT).Value = Me.m_user_name

    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_RECONCILIATED).Picture = CLASS_GUI_RESOURCES.GetImage("")

  End Sub

  Private Sub CalculateTotals()

    Dim _DeliveredCount As Integer
    Dim _DeliveredAmount As Currency
    Dim _NoDeliveredCount As Integer
    Dim _NoDeliveredAmount As Currency

    For _row As Integer = 0 To Me.Grid.NumRows - 1
      ' Check delivered count
      'LTC 23-AGO-2016
      If (Me.Grid.Cell(_row, GRID_COLUMN_RECONCILIATED).Value = False And Me.Grid.Cell(_row, GRID_COLUMN_OPERATION_NUMBER).Value <> "") Then
        _DeliveredCount = _DeliveredCount + 1
        _DeliveredAmount = _DeliveredAmount + Convert.ToDecimal(Me.Grid.Cell(_row, GRID_COLUMN_TOTAL_AMOUNT).Value.Replace("$", ""))
      ElseIf (Me.Grid.Cell(_row, GRID_COLUMN_RECONCILIATED).Value = True And Me.Grid.Cell(_row, GRID_COLUMN_OPERATION_NUMBER).Value <> "") Then
        _NoDeliveredCount = _NoDeliveredCount + 1
        _NoDeliveredAmount = _NoDeliveredAmount + Convert.ToDecimal(Me.Grid.Cell(_row, GRID_COLUMN_TOTAL_AMOUNT).Value.Replace("$", ""))
      End If
    Next

    Me.ef_amount_given_count.Value = _DeliveredCount
    Me.ef_amount_given_amount.Value = _DeliveredAmount
    Me.ef_amount_nogiven_count.Value = _NoDeliveredCount
    Me.ef_amount_nogiven_amount.Value = _NoDeliveredAmount

  End Sub

  Private Sub AceptChanges()

    Dim _sb As StringBuilder
    Dim _ids As List(Of String)
    Dim _total_amount As Decimal
    Dim _row_affected As Integer

    _total_amount = 0
    _ids = New List(Of String)
    _sb = New StringBuilder()

    Try


      For _row As Integer = 0 To Me.Grid.NumRows - 1
        ' Check delivered count
        If (Me.Grid.Cell(_row, GRID_COLUMN_OPERATION_NUMBER).Value <> "") And Me.Grid.Cell(_row, GRID_COLUMN_RECONCILIATED).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED Then

          _ids.Add(Me.Grid.Cell(_row, GRID_COLUMN_OPERATION_NUMBER).Value)
          _total_amount += Me.Grid.Cell(_row, GRID_COLUMN_TOTAL_AMOUNT).Value

        End If
      Next

      If _ids.Count > 0 Then

        Using _db_trx As New WSI.Common.DB_TRX()

          _sb.AppendLine(" UPDATE   PINPAD_TRANSACTIONS_RECONCILIATION")
          _sb.AppendLine("    SET   PTC_RECONCILIATE  = 1 ")
          _sb.AppendLine("        , PTC_DRAWAL_STATUS = 1 ")     'LTC 22-AGO-2016
          _sb.AppendLine("  WHERE   PTC_ID IN (" & String.Join(",", _ids.ToArray()) & ")")

          Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _row_affected = _sql_cmd.ExecuteNonQuery()

            If _row_affected <> _ids.Count Then

              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6185), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , , , , , )

              Return
            End If

          End Using

          ' Card session balance must be updated without the total concilliate tickets from amount card
          If _total_amount > 0 Then
            Cashier.UpdateSessionBalance(_db_trx.SqlTransaction, m_cashier_session_id, -_total_amount, CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CARD)
          End If

          _db_trx.Commit()

        End Using

      End If

      Call GUI_Audit()  'EOR 09-FEB-2018

      Me.Close()

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub
  'LTC 23-AGO-2016
  Private Function CheckSelected() As Integer
    Dim _Count As Integer
    _Count = 0

    For _row As Integer = 0 To Me.Grid.NumRows - 1
      If (Me.Grid.Cell(_row, GRID_COLUMN_OPERATION_NUMBER).Value <> "") Then
        _Count += IIf(Me.Grid.Cell(_row, GRID_COLUMN_RECONCILIATED).Value = True, 0, 1)
      End If
    Next

    Return _Count
  End Function

  Private Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call frm_base_sel.GUI_ReportParamsX(Me.Grid, ExcelData, m_report_name, WGDB.Now)

    ExcelData.SetColumnFormat(GRID_COLUMN_OPERATION_NUMBER - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)
    ExcelData.SetColumnFormat(GRID_COLUMN_AUTHORIZATION_CODE - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TRACKDATA)

  End Sub

  Private Sub GUI_ReportFilter(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    ' Insert filter data
    ExcelData.UpdateReportFilters(m_print_data.Filter.ItemArray)
  End Sub

  Private Sub GUI_ReportData(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)

    Call frm_base_sel.GUI_ReportDataX(Me.Grid, ExcelData)

  End Sub

  Private Sub GUI_PrintReport(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call ExcelData.Preview()
  End Sub

  Private Sub GUI_ReportFooter(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)

  End Sub

  ' 24-10-2016  ATB
  ''' <summary>
  ''' Won't allow the user clicks on the yellow cells (info cells)
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub Grid_CellDataChangedEvent_1(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles Grid.CellDataChangedEvent
    If (Me.Grid.Cell(Row, Column).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)) Then
      Me.Grid.Cell(Row, Column).Value = String.Empty
    End If

  End Sub

  'EOR 09-FEB-2018
  Private Sub GUI_Audit()
    Dim _auditor_data As CLASS_AUDITOR_DATA

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    _auditor_data.SetName(0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(9017) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7490))

    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372) & ": " & ef_amount_given_amount.Value)
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7492), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7495) & ": " & ef_amount_given_count.Value)

    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7493), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372) & ": " & ef_amount_nogiven_amount.Value)
    _auditor_data.SetField(GLB_NLS_GUI_PLAYER_TRACKING.Id(7493), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7495) & ": " & ef_amount_nogiven_count.Value)

    _auditor_data.Notify(GLB_CurrentUser.GuiId, GLB_CurrentUser.Id, GLB_CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)
  End Sub

#End Region

#Region " Public Functions"

  Public Sub ShowForEdit(ByVal UserId As System.Int32, ByVal UserName As System.String, ByVal Withdrawal As System.String, ByVal CashierSessionId As System.Int64, ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    If Me.DbStatus = ENUM_STATUS.STATUS_OK Then

      m_user_selected = UserId
      m_user_name = UserName
      m_withdrawal = Withdrawal
      m_cashier_session_id = CashierSessionId

      Call Me.Display(True)

    End If

  End Sub

#End Region

#Region "Events"

  Private Sub Grid_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles Grid.CellDataChangedEvent
    Call CalculateTotals()
  End Sub

#End Region

  Private Sub Grid_DataSelectedEvent() Handles Grid.DataSelectedEvent

  End Sub
End Class