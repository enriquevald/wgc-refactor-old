'------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'------------------------------------------------------------------------
'
' MODULE NAME:   frm_machine_alias
' DESCRIPTION:   Alias of Machines
' AUTHOR:        Sergi Mart�nez
' CREATION DATE: 25-MAR-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------------
' 25-MAR-2013 SMN    Initial version
' 02-MAY-2013 DHA    Fixed Bug #760: Application error when Alias DB field  is NULL
' 19-NOV-2013 LJM    Changes for auditing forms
'-------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.IO
Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.frm_base_print
Imports GUI_Controls.frm_base_sel
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_machine_alias
  Inherits GUI_Controls.frm_base_print

#Region " Constants "

  ' GRID MACHINE DEFINITIONS
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_APP_TERMINAL As Integer = 1
  Private Const GRID_COLUMN_APP_ALIAS As Integer = 2
  Private Const GRID_COLUMN_APP_IP_ADDRESS As Integer = 3
  Private Const GRID_COLUMN_APP_LOGIN_NAME As Integer = 4
  Private Const GRID_COLUMN_APP_ID As Integer = 5
  Private Const GRID_COLUMN_APP_ALIAS_ORIGINAL As Integer = 6

  Private Const GRID_COLUMNS As Integer = 7

  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_WIDTH_APP_TERMINAL As Integer = 2700
  Private Const GRID_WIDTH_APP_ALIAS As Integer = 2700
  Private Const GRID_WIDTH_APP_IP_ADDRESS As Integer = 2100
  Private Const GRID_WIDTH_APP_LOGIN_NAME As Integer = 2100

  Private Const GRID_LENGTH_APP_ALIAS As Integer = 15

#End Region ' Constants

#Region " Enums "

  Private Enum ENUM_APPLICATIONS
    CASHIER = 0
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_print_datetime As Date

#End Region ' Members

#Region " Properties "

  Private ReadOnly Property ApplicationNameInDB(ByVal Application As ENUM_APPLICATIONS) As String
    Get
      Select Case Application
        Case ENUM_APPLICATIONS.CASHIER
          Return "CASHIER"

        Case Else
          Return Nothing

      End Select
    End Get
  End Property

#End Region ' Properties

#Region " Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MACHINE_ALIAS
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    'Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1816) ' Alias de terminales Cajero y GUI

    ' Buttons Text
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)  ' Buscar
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)     ' Excel
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)     ' Guardar
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)       ' Salir

    ' Buttons Visible
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = True  ' Buscar
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True      ' Excel
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True      ' Guardar
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True        ' Salir
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Buttons Enable
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False      ' Excel
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False      ' Guardar

    ' Style Sheet
    Call GUI_StyleSheet(Me.dg_application)

    ' Initialize Grid
    Call GUI_ExecuteQuery()

  End Sub 'GUI_InitControls

  ' PURPOSE : Sets initial focus
  '
  '  PARAMS :
  '
  ' RETURNS :
  '
  '  NOTES :
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Call GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Focus()

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT :
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0 ' Save
        Call GUI_SaveChanges()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ExecuteQuery()
    Dim _dt_applications As DataTable

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Save DateTime for Excel Print
      m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

      ' If pending changes MsgBox
      If PendingChanges() Then
        If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
          Return
        End If
      End If

      ' Invalidate datagrid
      Call GUI_ClearGrid()

      ' Execute the query        
      _dt_applications = ExecuteQuery()

      ' Fill data
      If Not IsNothing(_dt_applications) And _dt_applications.Rows.Count > 0 Then
        Call FillDataGrid(_dt_applications)
      Else
        Call GUI_NoDataFound()
      End If

    Catch _ex As Exception
      ' Do nothing
      Debug.WriteLine(_ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

      ' Buttons Enable
      GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = (dg_application.NumRows > 0) ' Excel
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = (dg_application.NumRows > 0) And _
                                                        CurrentUser.Permissions(ENUM_FORM.FORM_MACHINE_ALIAS).Write ' Save
    End Try

  End Sub 'GUI_ExecuteQuery

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If

  End Sub ' GUI_Closing

  ' PURPOSE: Key Press Control 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: True / False
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If dg_application.ContainsFocus() Then
          dg_application.KeyPressed(sender, e)

          Return True
        End If

        Return False

      Case Chr(Keys.Escape)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select

  End Function ' GUI_KeyPress

  ' PURPOSE: Filters and sets its value to display in the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _filter_header As String

    _filter_header = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1812) ' Application

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition

    _prev_cursor = Me.Cursor
    Cursor = Cursors.WaitCursor

    Try
      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)
      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime
        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1816)

        _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
        _uc_grid_def.UcGrid = dg_application
        _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        .ListOfSheets.Add(_sheet_layout)
      End With

      _report_layout.GUI_GenerateExcel()

    Finally
      Cursor = _prev_cursor
    End Try

  End Sub ' GUI_ExcelResultList

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet(ByRef Dg As GUI_Controls.uc_grid)

    With Dg
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, Me.Permissions.Write)
      .Sortable = True

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Application Terminal
      .Column(GRID_COLUMN_APP_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1814)
      .Column(GRID_COLUMN_APP_TERMINAL).Width = GRID_WIDTH_APP_TERMINAL
      .Column(GRID_COLUMN_APP_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Application IP Address
      .Column(GRID_COLUMN_APP_IP_ADDRESS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1897)
      .Column(GRID_COLUMN_APP_IP_ADDRESS).Width = GRID_WIDTH_APP_IP_ADDRESS
      .Column(GRID_COLUMN_APP_IP_ADDRESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Application Login Name
      .Column(GRID_COLUMN_APP_LOGIN_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1898)
      .Column(GRID_COLUMN_APP_LOGIN_NAME).Width = GRID_WIDTH_APP_LOGIN_NAME
      .Column(GRID_COLUMN_APP_LOGIN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Application alias
      .Column(GRID_COLUMN_APP_ALIAS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1815)
      .Column(GRID_COLUMN_APP_ALIAS).Width = GRID_WIDTH_APP_ALIAS
      .Column(GRID_COLUMN_APP_ALIAS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_APP_ALIAS).Editable = True
      .Column(GRID_COLUMN_APP_ALIAS).IsMerged = False
      .Column(GRID_COLUMN_APP_ALIAS).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_APP_ALIAS).EditionControl.EntryField.SetFilter(FORMAT_TEXT, GRID_LENGTH_APP_ALIAS)

      ' Application Id
      .Column(GRID_COLUMN_APP_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_APP_ID).Width = 0
      .Column(GRID_COLUMN_APP_ID).IsColumnPrintable = False

      ' Application alias orignial
      .Column(GRID_COLUMN_APP_ALIAS_ORIGINAL).Header(0).Text = " "
      .Column(GRID_COLUMN_APP_ALIAS_ORIGINAL).Width = 0
      .Column(GRID_COLUMN_APP_ALIAS_ORIGINAL).IsColumnPrintable = False

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Create DataGridViews with SQL queries, and show generated info in screen.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function ExecuteQuery() As DataTable

    Dim _sb As New StringBuilder()
    Dim _app_name As String

    ' Build Query
    _sb.AppendLine("SELECT   APP_ID       ")
    _sb.AppendLine("       , APP_NAME     ")
    _sb.AppendLine("       , APP_MACHINE  ")
    _sb.AppendLine("       , APP_IP_ADDRESS  ")
    _sb.AppendLine("       , APP_LOGIN_NAME  ")
    _sb.AppendLine("       , ISNULL(APP_ALIAS, '') AS APP_ALIAS")
    _sb.AppendLine("  FROM   APPLICATIONS ")
    _sb.AppendLine(" WHERE   APP_NAME = @pAppName ")
    _sb.AppendLine("ORDER BY APP_MACHINE ")

    ' Filter
    _app_name = ApplicationNameInDB(ENUM_APPLICATIONS.CASHIER)

    Using _cmd As New SqlCommand(_sb.ToString())
      _cmd.Parameters.Add("@pAppName", SqlDbType.NVarChar).Value = _app_name

      Return GUI_GetTableUsingCommand(_cmd, 5000)
    End Using

  End Function ' ExecuteQuery

  ' PURPOSE: Fill Data Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillDataGrid(ByVal Applications As DataTable)

    Dim _idx_row As Integer
    Dim _app_row As DataRow

    For _idx_row = 0 To Applications.Rows.Count - 1
      _app_row = Applications.Rows(_idx_row)

      With dg_application
        .AddRow()
        .Cell(_idx_row, GRID_COLUMN_APP_TERMINAL).Value = _app_row.Item("APP_MACHINE")
        .Cell(_idx_row, GRID_COLUMN_APP_IP_ADDRESS).Value = _app_row.Item("APP_IP_ADDRESS")
        .Cell(_idx_row, GRID_COLUMN_APP_LOGIN_NAME).Value = Replace(CStr(_app_row.Item("APP_LOGIN_NAME")), "<None>", "")
        .Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value = _app_row.Item("APP_ALIAS")
        .Cell(_idx_row, GRID_COLUMN_APP_ID).Value = _app_row.Item("APP_ID")
        .Cell(_idx_row, GRID_COLUMN_APP_ALIAS_ORIGINAL).Value = _app_row.Item("APP_ALIAS")
      End With
    Next

  End Sub ' FillDataGrid

  ' PURPOSE: Save DataGrid to DB checking data constraints
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Sub GUI_SaveChanges()

    Dim _num_app_updated As Integer
    Dim _sb As New StringBuilder()
    Dim _app_update As DataTable
    Dim _ex_type As Integer = 0
    Dim _nls_string As String
    Dim _db_error_message As String
    Dim _db_error_found As Boolean
    Dim _app_row_error As DataRow
    Dim _app_alias_error As String

    _num_app_updated = 0
    _db_error_found = False
    _db_error_message = ""

    If Not GUI_IsScreenDataOk() Then
      Return
    End If

    ' Get modified Applications
    _app_update = Nothing
    Call ApplicationsModified(_app_update)

    If _app_update.Rows.Count = 0 Then
      Return
    End If

    Try
      _sb = New StringBuilder()
      _sb.AppendLine("UPDATE   APPLICATIONS            ")
      _sb.AppendLine("   SET   APP_ALIAS = @pApp_Alias ")
      _sb.AppendLine("WHERE    APP_ID    = @pApp_Id    ")

      Using _sql_adap As SqlDataAdapter = New SqlDataAdapter()
        Using _db_trx As DB_TRX = New DB_TRX()
          Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            _sql_cmd.Parameters.Add("@pApp_Alias", SqlDbType.NVarChar).SourceColumn = "APP_ALIAS"
            _sql_cmd.Parameters.Add("@pApp_Id", SqlDbType.Int).SourceColumn = "APP_ID"
            _sql_cmd.UpdatedRowSource = UpdateRowSource.None

            ' Insert command executes "Update"
            _sql_adap.InsertCommand = _sql_cmd
            _sql_adap.ContinueUpdateOnError = False
            _sql_adap.UpdateBatchSize = 500

            _num_app_updated = _sql_adap.Update(_app_update)
          End Using

          ' Commit
          _db_trx.Commit()

        End Using

        ' Auditory
        If (_num_app_updated > 0) Then
          Call AuditChanges()
        End If

        _app_update.AcceptChanges()

      End Using ' _sql_adap

      If _num_app_updated > 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108) _
                      , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO _
                      , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If


    Catch _exsql As SqlException
      _db_error_found = True
      _num_app_updated = 0
      _ex_type = _exsql.Number

      ' Find Row with error
      _app_alias_error = ""
      _app_row_error = ApplicationWithError(_app_update)
      If Not IsNothing(_app_row_error) Then
        _app_alias_error = _app_row_error("APP_ALIAS")

        ' unselect selected rows
        Call UnselectSelectedRows()

        ' select the row with error
        Call SelectRowWithError(_app_row_error)

      End If

      ' Message error
      If (_ex_type = 2601 Or _ex_type = 2627) Then ' Unique Key Violation
        ' 1890 "A %1 terminal with alias %2 already exists"
        _nls_string = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(1890) _
                    , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1818) _
                    , _app_alias_error)

        Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1890) _
                  , _nls_string _
                  , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Else ' Others SqlException
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853) _
                      , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR _
                      , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      End If

      _db_error_message = _exsql.Message

    Catch _ex As Exception
      _db_error_found = True
      _num_app_updated = 0

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853) _
            , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR _
            , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      _db_error_message = _ex.Message

    Finally
      If _db_error_found Then
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR _
                            , Me.Name _
                            , "GUI_SaveChanges" _
                            , _db_error_message)
      End If

    End Try

  End Sub  ' GUI_SaveChanges

  ' PURPOSE: Initialize data in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - Application: DataTable that contains a error row
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataRow with error
  Private Function ApplicationWithError(ByVal Application As DataTable) As DataRow

    For Each _application_row As DataRow In Application.Rows
      If _application_row.HasErrors() Then
        Return _application_row
      End If
    Next

    Return Nothing

  End Function ' ApplicationWithError

  ' PURPOSE: Initialize data in the grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ClearGrid()
    dg_application.Clear()
  End Sub 'GUI_ClearGrid

  ' PURPOSE: Check if changes have been made in the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Changes have been made. False: Otherwise
  '
  Private Function PendingChanges() As Boolean

    Dim _idx_row As Integer

    For _idx_row = 0 To dg_application.NumRows() - 1
      If dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value <> _
          dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS_ORIGINAL).Value Then
        Return True
      End If
    Next

    Return False

  End Function ' PendingChanges

  ' PURPOSE: Ask user to discard changes or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: Discard changes. False: Otherwise
  '
  Private Function DiscardChanges() As Boolean

    ' If pending changes MsgBox
    If PendingChanges() Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101) _
                  , ENUM_MB_TYPE.MB_TYPE_WARNING _
                  , ENUM_MB_BTN.MB_BTN_YES_NO _
                  , ENUM_MB_DEF_BTN.MB_DEF_BTN_2) _
                  = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    Return True

  End Function ' DiscardChanges

  ' PURPOSE: Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AuditChanges()

    Dim _idx_row As Integer
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _orig_auditor As CLASS_AUDITOR_DATA
    Dim _orig_value As String
    Dim _curr_value As String
    Dim _machine_name As String
    Dim _NlsId_auditor_name As Integer

    _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)
    _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_NAME_CASHIER_CONFIGURATION)

    ' Auditor Name 
    _NlsId_auditor_name = GLB_NLS_GUI_PLAYER_TRACKING.Id(1816) ' "Cash Desk Aliases"

    ' Set Name and Identifier
    Call _curr_auditor.SetName(_NlsId_auditor_name, "")
    Call _orig_auditor.SetName(_NlsId_auditor_name, "")

    For _idx_row = 0 To dg_application.NumRows() - 1

      ' Column Alias
      _curr_value = IIf(IsDBNull(dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value), _
          AUDIT_NONE_STRING, dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value)

      _orig_value = IIf(IsDBNull(dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS_ORIGINAL).Value), _
          AUDIT_NONE_STRING, dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS_ORIGINAL).Value)

      ' Column Terminal
      _machine_name = IIf(IsDBNull(dg_application.Cell(_idx_row, GRID_COLUMN_APP_TERMINAL).Value), _
          AUDIT_NONE_STRING, dg_application.Cell(_idx_row, GRID_COLUMN_APP_TERMINAL).Value)

      If (_curr_value <> _orig_value) Then
        ' Machine [#NameMachine].Alias = #Value (before #Value_original)
        Call _curr_auditor.SetField(0 _
                                  , _curr_value _
                                  , GLB_NLS_GUI_PLAYER_TRACKING.GetString(492) & " [" & _machine_name & "]." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1815))
        Call _orig_auditor.SetField(0 _
                                  , _orig_value _
                                  , GLB_NLS_GUI_PLAYER_TRACKING.GetString(492) & " [" & _machine_name & "]." & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1815))

        ' Update Grid
        dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS_ORIGINAL).Value = _
            dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value

      End If
    Next ' For _idx_row

    ' Notify
    Call _curr_auditor.Notify(CurrentUser.GuiId, _
                                  CurrentUser.Id, _
                                  CurrentUser.Name, _
                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                  0, _
                                  _orig_auditor)

    _curr_auditor = Nothing
    _orig_auditor = Nothing

  End Sub ' AuditChanges

  ' PURPOSE: Check if data in the datagrid is ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           -  Dt: Data Table with modified rows
  '
  ' RETURNS:
  '     - True:  Data is ok.
  '     - False: Data is not ok.
  Private Function GUI_IsScreenDataOk() As Boolean

    Dim _idx_row As Integer
    Dim _alias As String
    Dim _nls_string As String

    ' Evaluate each Application row
    For _idx_row = 0 To dg_application.NumRows() - 1

      _alias = dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value

      ' Evaluate empty Alias
      If _alias.Trim() = "" Then

        ' unselect selected rows
        Call UnselectSelectedRows()

        ' select the row with error
        dg_application.Row(_idx_row).IsSelected = True
        dg_application.RepaintRow(_idx_row)

        ' 114 "Field %1 can't be empty"
        _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(114) _
                                  , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1815))

        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(114) _
                            , _nls_string _
                            , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      'Evaluate maximum length of Alias field
      If _alias.Trim().Length > GRID_LENGTH_APP_ALIAS Then

        ' unselect selected rows
        Call UnselectSelectedRows()

        ' select the row with error
        dg_application.Row(_idx_row).IsSelected = True
        dg_application.RepaintRow(_idx_row)

        ' 423 "%1 field name must not exceed %2 characters."
        _nls_string = NLS_GetString(GLB_NLS_GUI_CONFIGURATION.Id(423) _
                                  , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1815) _
                                  , CStr(GRID_LENGTH_APP_ALIAS))

        Call NLS_MountedMsgBox(GLB_NLS_GUI_CONFIGURATION.Id(423) _
                            , _nls_string _
                            , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

      'Evaluate repeated Alias
      If ExistsAlias(_alias, _idx_row) Then

        ' unselect selected rows
        Call UnselectSelectedRows()

        ' select the row with error
        dg_application.Row(_idx_row).IsSelected = True
        dg_application.RepaintRow(_idx_row)

        ' 116 "%1 already exists.\n%2 must be unique."
        _nls_string = NLS_GetString(GLB_NLS_GUI_AUDITOR.Id(116), _
                         _alias, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1815))

        Call NLS_MountedMsgBox(GLB_NLS_GUI_AUDITOR.Id(116) _
                            , _nls_string _
                            , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If

    Next ' _idx_row

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Create a table to save updated data and send it to Update Command
  '
  '  PARAMS:
  '     - INPUT:
  '           - TableName: Name of new table
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DataTable created
  '
  Private Function CreateTableForUpdate(ByVal TableName As String) As DataTable
    Dim _table As DataTable

    _table = New DataTable(TableName)

    _table.Columns.Add("APP_ID", Type.GetType("System.Int64"))
    _table.Columns.Add("APP_ALIAS", Type.GetType("System.String"))

    _table.PrimaryKey = New DataColumn() {_table.Columns(0)}

    Return _table
  End Function ' CreateTable

  ' PURPOSE: Find Alias in ucGrid
  '
  '  PARAMS:
  '     - INPUT:
  '           - AliasName: alias name to find 
  '           - IdxRowIgnored: index of row that it ignores
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True: Alias exists
  '     - False: Alias 
  Private Function ExistsAlias(ByVal AliasName As String, Optional ByVal RowIgnoredIdx As Integer = -1) As Boolean

    Dim _idx_row As Integer

    For _idx_row = 0 To dg_application.NumRows() - 1
      If RowIgnoredIdx <> _idx_row Then
        If AliasName.Equals(dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value, StringComparison.InvariantCultureIgnoreCase) Then
          Return True
        End If
      End If
    Next

    Return False
  End Function ' ExistsAlias

  ' PURPOSE: Unselect Selected Rows of ucGrid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Number of unselected rows.
  Private Function UnselectSelectedRows() As Integer

    Dim _selected_rows() As Integer
    Dim _idx_selected_rows As Integer

    _selected_rows = dg_application.SelectedRows()
    If IsNothing(_selected_rows) Then

      Return 0
    End If

    For _idx_selected_rows = 0 To _selected_rows.Length - 1
      If dg_application.Row(_selected_rows(_idx_selected_rows)).IsSelected Then
        dg_application.Row(_selected_rows(_idx_selected_rows)).IsSelected = False
        dg_application.RepaintRow(_selected_rows(_idx_selected_rows))
      End If
    Next

    If IsNothing(_selected_rows) Then

      Return 0
    Else
      Return _selected_rows.Length
    End If

  End Function ' UnselectSelectedRows

  ' PURPOSE: Select row of uc_grid applications that has an error
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None.
  Private Sub SelectRowWithError(ByVal RowError As DataRow)

    Dim _idx_row As Integer

    For _idx_row = 0 To dg_application.NumRows() - 1
      If dg_application.Cell(_idx_row, GRID_COLUMN_APP_ID).Value = RowError("APP_ID") Then

        dg_application.Row(_idx_row).IsSelected = True
        dg_application.RepaintRow(_idx_row)

        Exit For
      End If
    Next

  End Sub ' SelectRowWithError

  ' PURPOSE: Get modified applications
  '
  '  PARAMS:
  '     - INPUT:
  '           - ApplicationsUpdate
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None.
  Private Sub ApplicationsModified(ByRef ApplicationsUpdate As DataTable)

    Dim _idx_row As Integer

    ApplicationsUpdate = CreateTableForUpdate("Applications")

    For _idx_row = 0 To dg_application.NumRows() - 1

      ' Evaluate modified rows
      If dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value <> _
         dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS_ORIGINAL).Value Then

        ' Add updated row
        ApplicationsUpdate.Rows.Add(dg_application.Cell(_idx_row, GRID_COLUMN_APP_ID).Value _
                                  , dg_application.Cell(_idx_row, GRID_COLUMN_APP_ALIAS).Value)
      End If

    Next

  End Sub ' ApplicationsModified

  ' PURPOSE: Message if no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - None.
  Private Sub GUI_NoDataFound()

    Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119) _
                  , mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO _
                  , mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
  End Sub ' GUI_NoDataFound

#End Region ' Private Functions

End Class