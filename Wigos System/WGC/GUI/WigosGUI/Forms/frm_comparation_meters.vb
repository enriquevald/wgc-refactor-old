'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_comparation_meters
' DESCRIPTION:   Comparison of different meters
' AUTHOR:        Marcos Piedra
' CREATION DATE: 31-NOV-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-NOV-2011  MPO    Initial version.
' 01-DEC-2011  JMM    Some bug solving
' 19-JAN-2012  MPO    New column orphan credit 
' 31-JAN-2012  MPO    Fixed bug: The color of row not match with the results. Change the function IsMisMatch.
' 02-FEB-2012  MPO    Fixed bug: It need SUM(ORPHAN_CREDIT) in HAVING (GUI_FilterGetSqlQuery)
' 29-MAY-2012  MPO    Defect #305: New logic to calculate final balance.
' 28-JUN-2012  MPO    Not use "DATEDIFF(SECOND,PS_STARTED,PS_FINISHED)" because it can have milliseconds not seconds
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 22-SEP-2015  AVZ    Fixed Bug TFS-4618: Cashless Mode: Use Stored Procedure - TITO Mode: Use Query
' 16-FEB-2016  MPO    Fixed bug: TFS-9449: Query optimized for TITO
' 06-APR-2016  FJC    Fixed Bug 11400:LCD Touch: Al transferir cr�ditos promocionales no redimibles, no se actualiza el Input de "cuadratura de entradas y salidas de m�quina"
' 28-JUL-2016  JRC    Fixed Bug 14489: GUI: Cuadratura de entradas y salidas de m�quina, descuadre del contador de monedas  Netwin (A-B).
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_comparation_meters
  Inherits frm_base_sel

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private GRID_COLUMN_SENTINEL As Integer = 0
  Private GRID_INIT_TERMINAL_DATA As Integer = 1
  Private GRID_COLUMN_PROVIDER As Integer = 1
  Private GRID_COLUMN_PS_TO_GM As Integer = 3
  Private GRID_COLUMN_PS_FROM_GM As Integer = 4
  Private GRID_COLUMN_ORPHAN As Integer = 5
  Private GRID_COLUMN_PS_NETWIN As Integer = 6
  Private GRID_COLUMN_MM_TO_GM As Integer = 7
  Private GRID_COLUMN_MM_FROM_GM As Integer = 8
  Private GRID_COLUMN_MM_NETWIN As Integer = 9

  'AVZ 22-SEP-2015: Cashless Mode - Grid Columns
  Private GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS As Integer = 10
  Private GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS As Integer = 11
  Private GRID_COLUMN_DIFF_NETWIN_CASHLESS As Integer = 12
  Private GRID_COLUMNS_CASHLESS As Integer = 13

  'AVZ 22-SEP-2015: TITO Mode - Grid Columns
  Private GRID_COLUMN_DIFF_NETWIN_TITO As Integer = 10
  Private GRID_COLUMNS_TITO As Integer = 11

  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_WIDTH_PROVIDER As Integer = 1800
  Private Const GRID_WIDTH_TERMINAL As Integer = 2300
  Private Const GRID_WIDTH_CURRENCY As Integer = 1750
  Private Const GRID_WIDTH_NETWIN As Integer = 1740

  ' Sql Columns
  'AVZ 22-SEP-2015: Cashless Mode - Sql Columns
  Private Const SQL_COLUMN_PROVIDER_NAME_CASHLESS As Integer = 0
  Private Const SQL_COLUMN_PROVIDER_ID_CASHLESS As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_NAME_CASHLESS As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_ID_CASHLESS As Integer = 3

  Private Const SQL_COLUMN_PS_TO_GM_CASHLESS As Integer = 4
  Private Const SQL_COLUMN_PS_FROM_GM_CASHLESS As Integer = 5
  Private Const SQL_COLUMN_PS_NETWIN_CASHLESS As Integer = 6

  Private Const SQL_COLUMN_MM_TO_GM_CASHLESS As Integer = 7
  Private Const SQL_COLUMN_MM_FROM_GM_CASHLESS As Integer = 8
  Private Const SQL_COLUMN_MM_NETWIN_CASHLESS As Integer = 9

  Private Const SQL_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS As Integer = 10
  Private Const SQL_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS As Integer = 11


  Private Const SQL_COLUMN_TOTAL_IMBALANCE_CASHLESS As Integer = 12
  Private Const SQL_COLUMN_ORPHAN_CASHLESS As Integer = 16

  'AVZ 22-SEP-2015: TITO Mode - Sql Columns
  Private Const SQL_COLUMN_TERMINAL_TITO As Integer = 0
  Private Const SQL_COLUMN_PROVIDER_TITO As Integer = 1
  Private Const SQL_COLUMN_PS_TO_GM_TITO As Integer = 2
  Private Const SQL_COLUMN_PS_FROM_GM_TITO As Integer = 3
  Private Const SQL_COLUMN_MM_TO_GM_TITO As Integer = 4
  Private Const SQL_COLUMN_MM_FROM_GM_TITO As Integer = 5
  Private Const SQL_COLUMN_ORPHAN_TITO As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_ID_TITO As Integer = 7

  ' Counter
  Private Const COUNTER_OK As Integer = 1
  Private Const COUNTER_DIFF As Integer = 2

#End Region ' Constants

#Region " Member "

  'REPORT FILTER
  Dim m_date_from As String
  Dim m_date_to As String
  Dim m_terminal As String
  Dim m_type As String

  Dim m_change_col As Boolean

  'TOTAL
  Dim m_ps_netwin As Decimal
  Dim m_mm_netwin As Decimal
  Dim m_diff_netwin As Decimal
  Dim m_diff_total_from_egm As Decimal
  Dim m_diff_total_to_egm As Decimal

  Dim m_ps_to_gm As Decimal
  Dim m_ps_from_gm As Decimal
  Dim m_mm_to_gm As Decimal
  Dim m_mm_from_gm As Decimal
  Dim m_credit_orphan As Decimal

  Private m_terminal_report_type As ReportType = ReportType.Provider

  Private _is_tito_mode As Boolean

#End Region ' Member

#Region " ENUMS "

  'AVZ 22-SEP-2015
  Public Enum ENUM_TYPE_OF_METER_CODES
    BILLS_IN_CENTS
    TICKETS_IN_CENTS
    TICKETS_OUT_CENTS
    STACKER_IN_CENTS
    STACKERS_OUT_CENTS
    FT_NR_IN
    FT_OUT
    FT_IN
  End Enum

#End Region ' ENUMS

#Region " Overrides "

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TF_GAP_REPORT

    'AVZ 22-SEP-2015: Set is tito mode
    _is_tito_mode = TITO.Utils.IsTitoMode

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize all form controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Protected Overrides Sub GUI_InitControls()


    Call MyBase.GUI_InitControls()

    ' Form Title
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(266)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Options
    Me.gb_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5682)
    Me.chk_show_terminals.Text = GLB_NLS_GUI_STATISTICS.GetString(472)
    Me.chk_show_imbalance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5043)
    Me.chk_show_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)
    Me.lbl_detail_filter.ForeColor = Color.Blue

    ' Providers - Terminals
    Dim _terminal_types() As Integer = Nothing

    ReDim Preserve _terminal_types(0 To 0)
    _terminal_types(0) = WSI.Common.TerminalTypes.SAS_HOST
    Call Me.uc_pr_list.Init(_terminal_types)

    ' Events
    AddHandler Me.chk_show_terminals.CheckedChanged, AddressOf chk_CheckedChanged
    AddHandler Me.chk_show_imbalance.CheckedChanged, AddressOf chk_CheckedChanged
    AddHandler Me.chk_show_terminal_location.CheckedChanged, AddressOf chk_CheckedChanged

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True, False)

    Me.m_change_col = False

    Call SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset


  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As System.Data.SqlClient.SqlCommand

    'AVZ 22-SEP-2015
    If (_is_tito_mode) Then
      Return GetSqlCommandForTITO()
    Else
      Return GetSqlCommandForCashless()
    End If

  End Function ' GUI_GetSqlCommand

  'AVZ 22-SEP-2015
  Private Function GetSqlCommandForTITO() As SqlCommand

    Dim _sql_cmd As SqlCommand
    Dim _str_sql As System.Text.StringBuilder
    Dim _having As Boolean = False
    Dim _order_terminal As Boolean = False
    Dim _types_in() As ENUM_TYPE_OF_METER_CODES = {ENUM_TYPE_OF_METER_CODES.BILLS_IN_CENTS, ENUM_TYPE_OF_METER_CODES.TICKETS_IN_CENTS, ENUM_TYPE_OF_METER_CODES.FT_NR_IN, ENUM_TYPE_OF_METER_CODES.FT_IN}
    Dim _types_out() As ENUM_TYPE_OF_METER_CODES = {ENUM_TYPE_OF_METER_CODES.TICKETS_OUT_CENTS}
    Dim _mico_types_out() As ENUM_TYPE_OF_METER_CODES = {ENUM_TYPE_OF_METER_CODES.FT_OUT, ENUM_TYPE_OF_METER_CODES.TICKETS_OUT_CENTS}
    Dim _sql_tito As String = ""

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" DECLARE @STARTED AS DATETIME ")
    _str_sql.AppendLine(" DECLARE @FINISHED AS DATETIME ")
    _str_sql.AppendLine(" DECLARE @TYPE_HP AS INTEGER")

    _str_sql.AppendLine(" SET @STARTED = " & GUI_FormatDateDB(uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)))
    _str_sql.AppendLine(" SET @FINISHED = " & GUI_FormatDateDB(ToDate()))
    _str_sql.AppendLine(" SET @TYPE_HP = 2")

    If Not Me.chk_show_terminals.Checked Then
      _str_sql.AppendLine(" SELECT	'',")
    Else
      _str_sql.AppendLine(" SELECT	TE_NAME,")
    End If

    _str_sql.AppendLine(" 		TE_PROVIDER_ID, ")
    _str_sql.AppendLine(" 		SUM(INI_BALANCE)                    AS PS_TO_GM, ")
    _str_sql.AppendLine(" 		SUM(FIN_BALANCE)					          AS PS_FROM_GM, ")
    _str_sql.AppendLine(" 		SUM(ISNULL(TO_AMOUNT,0))            AS MM_TO_GM, ")
    _str_sql.AppendLine(" 		SUM(ISNULL(FROM_AMOUNT,0))          AS MM_FROM_GM, ")
    _str_sql.AppendLine("  		SUM(ORPHAN_CREDIT)                  AS ORPHAN_CREDIT ")
    If Not Me.chk_show_terminals.Checked Then
      _str_sql.AppendLine("  ,	'' ")
    Else
      _str_sql.AppendLine("  ,	TE_TERMINAL_ID ")
    End If

    _str_sql.AppendLine("   FROM ")
    _str_sql.AppendLine("   (	")
    _str_sql.AppendLine("  	SELECT	TERMINAL_ID")
    _str_sql.AppendLine("  			,SUM(INI_BALANCE)	AS INI_BALANCE")
    _str_sql.AppendLine("  			,SUM(FIN_BALANCE)	AS FIN_BALANCE ")
    _str_sql.AppendLine("  			,SUM(ORPHAN_CREDIT) AS ORPHAN_CREDIT")

    _str_sql.AppendLine("  		FROM ")
    _str_sql.AppendLine("            (  ")

    _sql_tito = "+ ISNULL(PS_RE_TICKET_IN,0) + ISNULL(PS_PROMO_RE_TICKET_IN,0) + ISNULL(PS_PROMO_NR_TICKET_IN,0) + ISNULL(PS_AUX_FT_NR_CASH_IN,0)"

    _str_sql.AppendLine("  				SELECT	PS_TERMINAL_ID			AS TERMINAL_ID ")
    _str_sql.AppendLine("  						,SUM( ")
    _str_sql.AppendLine("  							CASE WHEN PS_STARTED<>PS_FINISHED THEN (PS_INITIAL_BALANCE+PS_CASH_IN " & _sql_tito & ")  ")
    _str_sql.AppendLine("  								ELSE 0 END ")
    _str_sql.AppendLine("  							)					AS INI_BALANCE")
    _str_sql.AppendLine("  						,0					AS FIN_BALANCE")
    _str_sql.AppendLine("  						,0          AS ORPHAN_CREDIT")
    _str_sql.AppendLine("  				FROM PLAY_SESSIONS WITH (INDEX(IX_ps_started))	 ")
    _str_sql.AppendLine("  				WHERE	PS_STARTED < @FINISHED ")
    _str_sql.AppendLine("  					AND	PS_STARTED >= @STARTED  						")
    _str_sql.AppendLine("  				GROUP BY PS_TERMINAL_ID	")

    _str_sql.AppendLine("  			UNION ALL ")

    _sql_tito = "+ ISNULL(PS_RE_TICKET_OUT,0) + ISNULL(PS_PROMO_NR_TICKET_OUT,0) "

    _str_sql.AppendLine("  				SELECT	PS_TERMINAL_ID ")
    _str_sql.AppendLine("  						,0")
    _str_sql.AppendLine("  						,SUM( ")
    _str_sql.AppendLine("  							  CASE WHEN PS_STARTED<>PS_FINISHED THEN ")
    _str_sql.AppendLine("  							     CASE WHEN PS_BALANCE_MISMATCH = 1 THEN ")
    _str_sql.AppendLine("  							          PS_REPORTED_BALANCE_MISMATCH + PS_CASH_OUT " & _sql_tito & " ")
    _str_sql.AppendLine("  							     ELSE ")
    _str_sql.AppendLine("  							  	      PS_FINAL_BALANCE + PS_CASH_OUT " & _sql_tito & " ")
    _str_sql.AppendLine("  							     END ")
    _str_sql.AppendLine("  							  ELSE 0 END	")
    _str_sql.AppendLine("  							  ) ")
    _str_sql.AppendLine("  						,0")
    _str_sql.AppendLine("  				FROM PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status))	")
    _str_sql.AppendLine("  				WHERE	PS_FINISHED < @FINISHED ")
    _str_sql.AppendLine("  					AND	PS_FINISHED >= @STARTED  						")
    _str_sql.AppendLine("  				GROUP BY PS_TERMINAL_ID ")

    _str_sql.AppendLine("  			UNION ALL")

    _str_sql.AppendLine(" 				SELECT	HP_TERMINAL_ID")
    _str_sql.AppendLine(" 						,0")
    _str_sql.AppendLine(" 						,0")
    _str_sql.AppendLine(" 						,SUM(HP_AMOUNT) ")
    _str_sql.AppendLine(" 				FROM HANDPAYS")
    _str_sql.AppendLine(" 				WHERE	HP_DATETIME < @FINISHED ")
    _str_sql.AppendLine(" 					AND HP_DATETIME >= @STARTED ")
    _str_sql.AppendLine(" 					AND HP_TYPE = @TYPE_HP")
    _str_sql.AppendLine(" 				GROUP BY HP_TERMINAL_ID	")

    _str_sql.AppendLine("  			) AS PS ")
    _str_sql.AppendLine(" 		GROUP BY TERMINAL_ID ")

    _str_sql.AppendLine("  )  ")

    _str_sql.AppendLine("  AS PS	RIGHT JOIN TERMINALS ON TERMINAL_ID = TERMINALS.TE_TERMINAL_ID ")
    _str_sql.AppendLine(" 		LEFT JOIN (	 ")

    'JML 30-NOV-2013: Calculate for TITOs

    _str_sql.AppendLine("SELECT TERMINAL ")
    _str_sql.AppendLine("      , SUM(FROM_AMOUNT) AS FROM_AMOUNT ")
    _str_sql.AppendLine("      , SUM(TO_AMOUNT) AS TO_AMOUNT ")
    _str_sql.AppendLine(" FROM ( ")
    _str_sql.AppendLine(" 	SELECT   TSMH_TERMINAL_ID AS TERMINAL ")

    _str_sql.AppendLine("          , CASE WHEN TSMH_METER_CODE IN (" & GetMeterCodes(_types_in) & ") THEN ")
    _str_sql.AppendLine("              CAST(TSMH_METER_INCREMENT AS MONEY)/100 ")
    _str_sql.AppendLine("            END AS TO_AMOUNT ")

    If TITO.Utils.IsMico2 Then

      _str_sql.AppendLine("          , CASE WHEN TSMH_METER_CODE IN (" & GetMeterCodes(_mico_types_out) & ") THEN ")
      _str_sql.AppendLine("              CAST(TSMH_METER_INCREMENT AS MONEY)/100 ")
      _str_sql.AppendLine("            END AS FROM_AMOUNT ")

    Else

      _str_sql.AppendLine("          , CASE WHEN TSMH_METER_CODE IN (" & GetMeterCodes(_types_out) & ") THEN ")
      _str_sql.AppendLine("              CAST(TSMH_METER_INCREMENT AS MONEY)/100 ")
      _str_sql.AppendLine("            END AS FROM_AMOUNT ")

    End If

    _str_sql.AppendLine(" 	  FROM   TERMINAL_SAS_METERS_HISTORY WITH(INDEX(IX_tsmh_datetime_type)) ")
    _str_sql.AppendLine("    WHERE   TSMH_DATETIME >= @STARTED  ")
    _str_sql.AppendLine("      AND   TSMH_DATETIME <  @FINISHED  ")
    _str_sql.AppendLine(" 	   AND   TSMH_TYPE = 1 AND TSMH_METER_INCREMENT > 0 ")

    If TITO.Utils.IsMico2 Then
      _str_sql.AppendLine("      AND   TSMH_METER_CODE IN (" & GetMeterCodes(_types_in) & "," & GetMeterCodes(_mico_types_out) & ") ")
    Else
      _str_sql.AppendLine("      AND   TSMH_METER_CODE IN (" & GetMeterCodes(_types_in) & "," & GetMeterCodes(_types_out) & ") ")
    End If

    _str_sql.AppendLine("        ) AS TSMH  GROUP BY TSMH.TERMINAL ")
    _str_sql.AppendLine(" 		) ")
    _str_sql.AppendLine(" 		AS MSH ON MSH.TERMINAL = TERMINALS.TE_TERMINAL_ID ")
    _str_sql.AppendLine("  WHERE TE_TERMINAL_TYPE IN ( " & Me.uc_pr_list.GetTerminalTypes() & " ) ")
    _str_sql.AppendLine("  AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected())

    If Not Me.chk_show_terminals.Checked Then
      _str_sql.AppendLine("  GROUP BY TE_PROVIDER_ID ")
      _order_terminal = False
    Else
      _str_sql.AppendLine("  GROUP BY TE_TERMINAL_ID,TE_PROVIDER_ID,TE_NAME ")
      _order_terminal = True
    End If

    If Me.chk_show_imbalance.Checked Then
      'JMM 1-DEC-2011: Mismatch if cashin - cashout differs
      _str_sql.AppendLine("  HAVING ")
      _str_sql.AppendLine("      SUM(ISNULL(INI_BALANCE,0)) - SUM(ISNULL(FIN_BALANCE,0)) - SUM(ORPHAN_CREDIT) <> SUM(ISNULL(TO_AMOUNT,0)) - SUM(ISNULL(FROM_AMOUNT,0) )	")
    End If

    If _order_terminal Then
      _str_sql.AppendLine("  ORDER BY TE_NAME ")
    Else
      _str_sql.AppendLine("  ORDER BY TE_PROVIDER_ID ")
    End If

    _sql_cmd = New SqlCommand(_str_sql.ToString())
    _sql_cmd.CommandType = CommandType.Text

    Return _sql_cmd

  End Function

  Private Function GetStringBillValue()

    Dim _sb As System.Text.StringBuilder = New System.Text.StringBuilder()

    _sb.AppendLine("CASE TSMH_METER_CODE ")
    _sb.AppendLine("WHEN 64 THEN 1 ")
    _sb.AppendLine("WHEN 65 THEN 2 ")
    _sb.AppendLine("WHEN 66 THEN 5 ")
    _sb.AppendLine("WHEN 67 THEN 10 ")
    _sb.AppendLine("WHEN 68 THEN 20 ")
    _sb.AppendLine("WHEN 69 THEN 25 ")
    _sb.AppendLine("WHEN 70 THEN 50 ")
    _sb.AppendLine("WHEN 71 THEN 100 ")
    _sb.AppendLine("WHEN 72 THEN 200 ")
    _sb.AppendLine("WHEN 73 THEN 250 ")
    _sb.AppendLine("WHEN 74 THEN 500 ")
    _sb.AppendLine("WHEN 75 THEN 1000 ")
    _sb.AppendLine("WHEN 76 THEN 2000 ")
    _sb.AppendLine("WHEN 77 THEN 2500 ")
    _sb.AppendLine("WHEN 78 THEN 5000 ")
    _sb.AppendLine("WHEN 79 THEN 10000 ")
    _sb.AppendLine("WHEN 80 THEN 20000 ")
    _sb.AppendLine("WHEN 81 THEN 25000 ")
    _sb.AppendLine("WHEN 82 THEN 50000 ")
    _sb.AppendLine("WHEN 83 THEN 100000 ")
    _sb.AppendLine("WHEN 84 THEN 200000 ")
    _sb.AppendLine("WHEN 85 THEN 250000 ")
    _sb.AppendLine("WHEN 86 THEN 500000 ")
    _sb.AppendLine("WHEN 87 THEN 1000000 ")
    _sb.AppendLine("ELSE 0 END ")

    Return _sb.ToString()

  End Function

  'AVZ 22-SEP-2015
  Private Function GetSqlCommandForCashless() As SqlCommand

    Dim _sql_cmd As New SqlCommand("ReportMachineInputOutputBalance")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = ToDate()

    _sql_cmd.Parameters.Add("@pShowTerminal", SqlDbType.Bit).Value = Me.chk_show_terminals.Checked
    _sql_cmd.Parameters.Add("@pShowImbalance", SqlDbType.Bit).Value = Me.chk_show_imbalance.Checked
    _sql_cmd.Parameters.Add("@pTerminalWhere", SqlDbType.NVarChar).Value = " TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    Return _sql_cmd

  End Function

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'AVZ 22-SEP-2015
    If (_is_tito_mode) Then
      Return SetupRowForTITO(RowIndex, DbRow)
    Else
      Return SetupRowForCashless(RowIndex, DbRow)
    End If

  End Function  ' GUI_SetupRow

  'AVZ 22-SEP-2015
  Private Function SetupRowForTITO(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _ps_netwin As Decimal
    Dim _mm_netwin As Decimal
    Dim _diff_netwin As Decimal
    Dim _terminal_data As List(Of Object)
    Dim _ps_to_gm As Decimal
    Dim _ps_from_gm As Decimal
    Dim _orphan As Decimal

    _ps_to_gm = 0
    _ps_from_gm = 0
    _orphan = 0

    If Not chk_show_terminals.Checked Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER_TITO)
    Else
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID_TITO), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next
    End If

    If Not DbRow.IsNull(SQL_COLUMN_PS_TO_GM_TITO) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_PS_TO_GM_TITO)) Then
      _ps_to_gm = DbRow.Value(SQL_COLUMN_PS_TO_GM_TITO)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PS_TO_GM).Value = GUI_FormatCurrency(_ps_to_gm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If Not DbRow.IsNull(SQL_COLUMN_PS_FROM_GM_TITO) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_PS_FROM_GM_TITO)) Then
      _ps_from_gm = DbRow.Value(SQL_COLUMN_PS_FROM_GM_TITO)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PS_FROM_GM).Value = GUI_FormatCurrency(_ps_from_gm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If Not DbRow.IsNull(SQL_COLUMN_ORPHAN_TITO) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_ORPHAN_TITO)) Then
      _orphan = DbRow.Value(SQL_COLUMN_ORPHAN_TITO)
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORPHAN).Value = GUI_FormatCurrency(_orphan, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


    _ps_netwin = _ps_to_gm - _ps_from_gm - _orphan

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PS_NETWIN).Value = GUI_FormatCurrency(_ps_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_MM_TO_GM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MM_TO_GM_TITO), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MM_FROM_GM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MM_FROM_GM_TITO), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    _mm_netwin = DbRow.Value(SQL_COLUMN_MM_TO_GM_TITO) - DbRow.Value(SQL_COLUMN_MM_FROM_GM_TITO)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MM_NETWIN).Value = GUI_FormatCurrency(_mm_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    _diff_netwin = _ps_netwin - _mm_netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFF_NETWIN_TITO).Value = GUI_FormatCurrency(_diff_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If IsMisMatch(DbRow) Then
      If Not Me.chk_show_imbalance.Checked Then
        Me.Grid.Row(RowIndex).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Row(RowIndex).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      End If
      Me.Grid.Counter(COUNTER_DIFF).Value += 1
    Else
      Me.Grid.Counter(COUNTER_OK).Value += 1
    End If

    m_ps_netwin += _ps_netwin
    m_mm_netwin += _mm_netwin
    m_diff_netwin += _diff_netwin

    m_ps_to_gm += _ps_to_gm
    m_ps_from_gm += _ps_from_gm

    m_mm_to_gm += DbRow.Value(SQL_COLUMN_MM_TO_GM_TITO)
    m_mm_from_gm += DbRow.Value(SQL_COLUMN_MM_FROM_GM_TITO)
    m_credit_orphan += _orphan

    Return True
  End Function

  'AVZ 22-SEP-2015
  Private Function SetupRowForCashless(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER_NAME_CASHLESS)
    If Me.chk_show_terminals.Checked Then
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID_CASHLESS), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next
    End If

    ' Gaming Sessions - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PS_TO_GM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PS_TO_GM_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Gaming Sessions - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PS_FROM_GM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PS_FROM_GM_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Gaming Sessions - Abandonded Credits
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORPHAN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_ORPHAN_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    '_ps_netwin = _ps_to_gm - _ps_from_gm - _orphan

    ' Gaming Sessions - NETWIN
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PS_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PS_NETWIN_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Machine Meters - Inputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MM_TO_GM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MM_TO_GM_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Machine Meters - Outputs
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MM_FROM_GM).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MM_FROM_GM_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Machine Meters - NETWIN
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MM_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MM_NETWIN_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' diff.total.in
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' diff.total.OUT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)


    'Unbalance (_diff_netwin = _ps_netwin - _mm_netwin)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DIFF_NETWIN_CASHLESS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_IMBALANCE_CASHLESS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If (DbRow.Value(SQL_COLUMN_TOTAL_IMBALANCE_CASHLESS) <> 0) Then
      If Not Me.chk_show_imbalance.Checked Then
        Me.Grid.Row(RowIndex).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Row(RowIndex).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      End If
      Me.Grid.Counter(COUNTER_DIFF).Value += 1
    Else
      Me.Grid.Counter(COUNTER_OK).Value += 1
    End If


    m_ps_to_gm += DbRow.Value(SQL_COLUMN_PS_TO_GM_CASHLESS)
    m_ps_from_gm += DbRow.Value(SQL_COLUMN_PS_FROM_GM_CASHLESS)
    m_credit_orphan += DbRow.Value(SQL_COLUMN_ORPHAN_CASHLESS)
    m_ps_netwin += DbRow.Value(SQL_COLUMN_PS_NETWIN_CASHLESS)

    m_mm_to_gm += DbRow.Value(SQL_COLUMN_MM_TO_GM_CASHLESS)
    m_mm_from_gm += DbRow.Value(SQL_COLUMN_MM_FROM_GM_CASHLESS)
    m_mm_netwin += DbRow.Value(SQL_COLUMN_MM_NETWIN_CASHLESS)


    m_diff_total_from_egm += DbRow.Value(SQL_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS)
    m_diff_total_to_egm += DbRow.Value(SQL_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS)
    m_diff_netwin += DbRow.Value(SQL_COLUMN_TOTAL_IMBALANCE_CASHLESS)

    Return True
  End Function

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        If m_change_col Then
          m_change_col = False
          Call GUI_StyleSheet()
        End If
        Call MyBase.GUI_ButtonClick(ButtonId)
        Me.Grid.Counter(0).Value = Me.Grid.Counter(COUNTER_DIFF).Value + Me.Grid.Counter(COUNTER_OK).Value
      Case Else

        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_ps_netwin = 0
    m_mm_netwin = 0
    m_diff_netwin = 0
    m_diff_total_from_egm = 0
    m_diff_total_to_egm = 0
    m_ps_to_gm = 0
    m_ps_from_gm = 0
    m_mm_to_gm = 0
    m_mm_from_gm = 0
    m_credit_orphan = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_index As Integer

    Me.Grid.AddRow()
    _row_index = Me.Grid.NumRows - 1

    Me.Grid.Cell(_row_index, GRID_COLUMN_PROVIDER).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    Me.Grid.Cell(_row_index, GRID_COLUMN_PS_TO_GM).Value = GUI_FormatCurrency(m_ps_to_gm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PS_FROM_GM).Value = GUI_FormatCurrency(m_ps_from_gm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_PS_NETWIN).Value = GUI_FormatCurrency(m_ps_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_ORPHAN).Value = GUI_FormatCurrency(m_credit_orphan, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_MM_TO_GM).Value = GUI_FormatCurrency(m_mm_to_gm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_MM_FROM_GM).Value = GUI_FormatCurrency(m_mm_from_gm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.Grid.Cell(_row_index, GRID_COLUMN_MM_NETWIN).Value = GUI_FormatCurrency(m_mm_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    If _is_tito_mode Then
      Me.Grid.Cell(_row_index, GRID_COLUMN_DIFF_NETWIN_TITO).Value = GUI_FormatCurrency(m_diff_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(_row_index, GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS).Value = GUI_FormatCurrency(m_diff_total_from_egm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_row_index, GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS).Value = GUI_FormatCurrency(m_diff_total_to_egm, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_row_index, GRID_COLUMN_DIFF_NETWIN_CASHLESS).Value = GUI_FormatCurrency(m_diff_netwin, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If


    For _col_index As Integer = 0 To Me.Grid.NumColumns - 1
      Me.Grid.Cell(_row_index, _col_index).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Next

    If Math.Abs(m_diff_netwin) > 0 Then
      If _is_tito_mode Then
        Me.Grid.Cell(_row_index, GRID_COLUMN_DIFF_NETWIN_TITO).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(_row_index, GRID_COLUMN_DIFF_NETWIN_TITO).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      Else
        Me.Grid.Cell(_row_index, GRID_COLUMN_DIFF_NETWIN_CASHLESS).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(_row_index, GRID_COLUMN_DIFF_NETWIN_CASHLESS).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      End If
    End If

  End Sub   ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_dsl

  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub  ' GUI_ReportParams 

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(471), m_type)
    PrintData.SetFilter("", "", True)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    ' Date
    _date = uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _date = ToDate()
    m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    m_type = Me.lbl_detail_filter.Text
    'If opt_all_providers.Checked = True Then
    '  m_type = GLB_NLS_GUI_STATISTICS.GetString(338)
    'ElseIf opt_by_provider.Checked = True Then
    '  m_type = GLB_NLS_GUI_STATISTICS.GetString(339)
    'ElseIf opt_by_terminal.Checked = True Then
    '  m_type = GLB_NLS_GUI_STATISTICS.GetString(317)
    'Else

    'End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    Call Me.uc_pr_list.SetDefaultValues()

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_show_terminals.Checked = False
    Me.chk_show_terminal_location.Checked = False
    Me.chk_show_terminal_location.Enabled = False
    Me.chk_show_imbalance.Checked = False

    Call RefreshInfoOptions()

  End Sub ' SetDefaultValues

  ' PURPOSE: Format appearance of the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid

      'AVZ 22-SEP-2015
      If (_is_tito_mode) Then
        Call .Init(GRID_COLUMNS_TITO, GRID_HEADER_ROWS, False)
      Else
        Call .Init(GRID_COLUMNS_CASHLESS, GRID_HEADER_ROWS, False)
      End If

      .Sortable = True

      .Counter(COUNTER_OK).Visible = True
      .Counter(COUNTER_OK).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_OK).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_DIFF).Visible = True
      .Counter(COUNTER_DIFF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      .Counter(COUNTER_DIFF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To Math.Min(_terminal_columns.Count - 1, TERMINAL_DATA_COLUMNS - 1)
        If Me.chk_show_terminals.Checked Then
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)
        Else
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = IIf(_idx > 0, 0, _terminal_columns(_idx).Width)
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = ""
        End If
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' Play sessions - To Game Meters
      .Column(GRID_COLUMN_PS_TO_GM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)
      .Column(GRID_COLUMN_PS_TO_GM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(259)
      .Column(GRID_COLUMN_PS_TO_GM).Width = GRID_WIDTH_CURRENCY
      .Column(GRID_COLUMN_PS_TO_GM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Play sessions - From Game Meter
      .Column(GRID_COLUMN_PS_FROM_GM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)
      .Column(GRID_COLUMN_PS_FROM_GM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(260)
      .Column(GRID_COLUMN_PS_FROM_GM).Width = GRID_WIDTH_CURRENCY
      .Column(GRID_COLUMN_PS_FROM_GM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Play sessions - NetWin (A)
      .Column(GRID_COLUMN_PS_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)
      .Column(GRID_COLUMN_PS_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(261)
      .Column(GRID_COLUMN_PS_NETWIN).Width = GRID_WIDTH_NETWIN
      .Column(GRID_COLUMN_PS_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Play sessions - Orphan Credits
      .Column(GRID_COLUMN_ORPHAN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(258)
      .Column(GRID_COLUMN_ORPHAN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(268)
      .Column(GRID_COLUMN_ORPHAN).Width = GRID_WIDTH_CURRENCY
      .Column(GRID_COLUMN_ORPHAN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TF Meters - To Game Meter
      .Column(GRID_COLUMN_MM_TO_GM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(262)
      .Column(GRID_COLUMN_MM_TO_GM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(259)
      .Column(GRID_COLUMN_MM_TO_GM).Width = GRID_WIDTH_CURRENCY
      .Column(GRID_COLUMN_MM_TO_GM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TF Meters - From Game Meter
      .Column(GRID_COLUMN_MM_FROM_GM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(262)
      .Column(GRID_COLUMN_MM_FROM_GM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(260)
      .Column(GRID_COLUMN_MM_FROM_GM).Width = GRID_WIDTH_CURRENCY
      .Column(GRID_COLUMN_MM_FROM_GM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TF Meters - NetWin (B)
      .Column(GRID_COLUMN_MM_NETWIN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(262)
      .Column(GRID_COLUMN_MM_NETWIN).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(263)
      .Column(GRID_COLUMN_MM_NETWIN).Width = GRID_WIDTH_NETWIN
      .Column(GRID_COLUMN_MM_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'AVZ 22-SEP-2015
      If (_is_tito_mode) Then
        ' TF Meters - NetWin (A-B)
        .Column(GRID_COLUMN_DIFF_NETWIN_TITO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(264)
        .Column(GRID_COLUMN_DIFF_NETWIN_TITO).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(265)
        .Column(GRID_COLUMN_DIFF_NETWIN_TITO).Width = GRID_WIDTH_NETWIN
        .Column(GRID_COLUMN_DIFF_NETWIN_TITO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        ' TF Meters - Diff Total From Egm
        .Column(GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(264)
        .Column(GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS).Header(1).Text = "Diff.Total.IN"
        .Column(GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS).Width = GRID_WIDTH_NETWIN
        .Column(GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' TF Meters - Diff Total To Egm
        .Column(GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(264)
        .Column(GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS).Header(1).Text = "Diff.Total.OUT"
        .Column(GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS).Width = GRID_WIDTH_NETWIN
        .Column(GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' TF Meters - NetWin (A-B)
        .Column(GRID_COLUMN_DIFF_NETWIN_CASHLESS).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(264)
        .Column(GRID_COLUMN_DIFF_NETWIN_CASHLESS).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(265)
        .Column(GRID_COLUMN_DIFF_NETWIN_CASHLESS).Width = GRID_WIDTH_NETWIN
        .Column(GRID_COLUMN_DIFF_NETWIN_CASHLESS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Function ToDate() As Date

    If uc_dsl.ToDateSelected Then
      Return New Date(uc_dsl.ToDate.Year, uc_dsl.ToDate.Month, uc_dsl.ToDate.Day, uc_dsl.ClosingTime, 0, 0)
    Else
      Return New Date(Today.Year, Today.Month, Today.Day, uc_dsl.ClosingTime, 0, 0).AddDays(1)
    End If

  End Function  ' ToDate

  Private Function IsMisMatch(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _net_win As Decimal
    Dim _ps_to_gm As Decimal
    Dim _ps_from_gm As Decimal
    Dim _orphan As Decimal

    If DbRow.Value(SQL_COLUMN_PS_TO_GM_TITO) Is DBNull.Value Then
      _ps_to_gm += 0
    Else
      _ps_to_gm += DbRow.Value(SQL_COLUMN_PS_TO_GM_TITO)
    End If
    If DbRow.Value(SQL_COLUMN_PS_FROM_GM_TITO) Is DBNull.Value Then
      _ps_from_gm += 0
    Else
      _ps_from_gm += DbRow.Value(SQL_COLUMN_PS_FROM_GM_TITO)
    End If
    If DbRow.Value(SQL_COLUMN_ORPHAN_TITO) Is DBNull.Value Then
      _orphan += 0
    Else
      _orphan += DbRow.Value(SQL_COLUMN_ORPHAN_TITO)
    End If

    _net_win = _ps_to_gm - _ps_from_gm - _orphan

    'JMM 1-DEC-2011: Mismatch if cashin - cashout differs
    'MPO 31-JAN-2011
    If _net_win <> DbRow.Value(SQL_COLUMN_MM_TO_GM_TITO) - DbRow.Value(SQL_COLUMN_MM_FROM_GM_TITO) Then
      Return True
    End If

    Return False

  End Function  ' IsMisMatch



  ' PURPOSE: Return meter codes for Cash in
  '
  '  PARAMS:
  '     - INPUT:
  '           - Array of meter types 
  '     - OUTPUT:
  '           - String with code meters
  '
  ' RETURNS:
  '     

  ''AVZ 22-SEP-2015
  Private Function GetMeterCodes(ByVal Types() As ENUM_TYPE_OF_METER_CODES) As String

    Dim _codes_list As String = ""

    For _idx As Int16 = 0 To Types.Length - 1

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.BILLS_IN_CENTS Then
        '_codes_list &= "11, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, "
        ' 11--> total bills.  The rest is the detail of bills
        ' 28-JUL-2016  JRC bug 14489 no suma monedas, codigo 8
        _codes_list &= "8, 11, "
      End If

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.TICKETS_IN_CENTS Then
        _codes_list &= "128, 130, 132, "
      End If

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.TICKETS_OUT_CENTS Then
        _codes_list &= "134, 136, 138, "
      End If

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.STACKER_IN_CENTS Then
        _codes_list &= ""
      End If

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.STACKERS_OUT_CENTS Then
        _codes_list &= ""
      End If

      ' 06-APR-2016 FJC Fixed Bug 11400:LCD Touch: Al transferir cr�ditos promocionales no redimibles, no se actualiza el Input de "cuadratura de entradas y salidas de m�quina"
      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.FT_NR_IN Then
        _codes_list &= "162, "
      End If

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.FT_IN Then
        _codes_list &= "160, 164, "
      End If

      If Types(_idx) = ENUM_TYPE_OF_METER_CODES.FT_OUT Then
        _codes_list &= "184, 186, 188, "
      End If

    Next

    _codes_list = _codes_list.Substring(0, _codes_list.Length - 2)

    Return _codes_list

  End Function  ' GetMeterCodesCashIn

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub GridColumnReIndex()
    If Me.chk_show_terminals.Checked Then
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)
    Else
      TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(ReportType.Provider)
    End If

    GRID_COLUMN_SENTINEL = 0
    GRID_INIT_TERMINAL_DATA = 1
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_PS_TO_GM = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_PS_FROM_GM = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_ORPHAN = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_PS_NETWIN = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_MM_TO_GM = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_MM_FROM_GM = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_MM_NETWIN = TERMINAL_DATA_COLUMNS + 7

    'AVZ 22-SEP-2015
    If (_is_tito_mode) Then
      GRID_COLUMN_DIFF_NETWIN_TITO = TERMINAL_DATA_COLUMNS + 8
      GRID_COLUMNS_TITO = TERMINAL_DATA_COLUMNS + 9
    Else
      GRID_COLUMN_TOTAL_DIFF_FROM_EGM_CASHLESS = TERMINAL_DATA_COLUMNS + 8
      GRID_COLUMN_TOTAL_DIFF_TO_EGM_CASHLESS = TERMINAL_DATA_COLUMNS + 9
      GRID_COLUMN_DIFF_NETWIN_CASHLESS = TERMINAL_DATA_COLUMNS + 10

      GRID_COLUMNS_CASHLESS = TERMINAL_DATA_COLUMNS + 11
    End If

  End Sub

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub RefreshInfoOptions()
    Dim _info_text As String

    _info_text = String.Empty

    If Not Me.chk_show_terminals.Checked AndAlso Not Me.chk_show_imbalance.Checked Then

      _info_text = GLB_NLS_GUI_STATISTICS.GetString(338) ' All Providers 
    ElseIf (Not Me.chk_show_terminals.Checked AndAlso Me.chk_show_imbalance.Checked) Then

      _info_text = GLB_NLS_GUI_STATISTICS.GetString(339) ' Only unbalance providers
    ElseIf (Me.chk_show_terminals.Checked AndAlso Not Me.chk_show_imbalance.Checked) Then

      _info_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6640) ' All Terminals 
    ElseIf (Me.chk_show_terminals.Checked AndAlso Me.chk_show_imbalance.Checked) Then

      _info_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6641) ' Only unbalance terminals
    End If

    Me.lbl_detail_filter.Text = _info_text

  End Sub

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Me.m_change_col = True

    chk_show_terminal_location.Enabled = Me.chk_show_terminals.Checked

    If chk_show_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    Call RefreshInfoOptions()
  End Sub  ' opt_CheckedChanged

#End Region ' Events


End Class