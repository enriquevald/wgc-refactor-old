'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_retreats_report
' DESCRIPTION:   Retreats report
' AUTHOR:        Jos� Mart�nez
' CREATION DATE: 20-SEP-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-SEP-2012  JML    Initial version.
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 21-AUG-2013  DHA    If ServiceCharge value is 0, row is hidden
'                     Added visibility for columns Prize/Refund    
' 22-MAY-2014  AMF    Added filter uc_account_sel
' 02-JUN-2014  AMF    Added filter winnings amount 
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 26-APR-2016  EOR    Product Backlog Item 11298: Codere - Tax Custody: Reports GUI 
' 22-SEP-2016  EOR    PBI 17963: Televisa - Service Charge: Modifications Voucher / Report / Settings
' 15-MAR-2017  ATB    PBI 25737: Third TAX - Report columns
' 16-MAR-2017  ATB    PBI 25737: Third TAX - Report columns
' 03-APR-2017  ATB    PBI 25737: Third TAX - Report columns
' 20-JUN-2017  ETP    Fixed Bug 28872:[WIGOS-3872] Service charge report shows extra column with Third Tax 
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_retreats_report
  Inherits frm_base_sel

#Region " Constants "

  Private Const GRID_COL_SESSION_ID As Integer = 0
  Private Const GRID_COL_ACCOUNT_ID As Integer = 1
  Private Const GRID_COL_ACCOUNT_NAME As Integer = 2
  Private Const GRID_COL_DATE As Integer = 3
  Private Const GRID_COL_REFUND As Integer = 4
  Private Const GRID_COL_PRIZE_COUPON As Integer = 5
  Private Const GRID_COL_PRIZE_MACHINE As Integer = 6
  Private Const GRID_COL_PRIZE As Integer = 7
  Private Const GRID_COL_TAX1 As Integer = 8
  Private Const GRID_COL_TAX2 As Integer = 9
  Private Const GRID_COL_TAX3 As Integer = 10
  Private Const GRID_COL_CUSTODY As Integer = 11    'EOR 26-APR-2016
  Private Const GRID_COL_ROUNDING_FOR_RETENTIONS As Integer = 12
  Private Const GRID_COL_SERVICE_CHARGE As Integer = 13
  Private Const GRID_COL_TO_PAY As Integer = 14
  Private Const GRID_COL_ROUNDING_FOR_DECIMALS As Integer = 15
  Private Const GRID_COL_TOTAL_PAID As Integer = 16

  Private Const GRID_COLUMNS As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Counter
  Private Const COUNTER_DETAILS As Integer = 1
  Private Const COUNTER_SESSION_TOTALS As Integer = 2

#End Region  ' Constants

#Region " Structures " ' Retreat

  Private Class RETREAT_REPORT_GRID_ROW
    Public m_session_id As Object     'typed as object to allow null value on total row
    Public m_account_id As Object     'typed as object to allow null value on total row
    Public m_account_name As Object    'typed as object to allow null value on total row
    Public m_movement_date As Object  'typed as object to allow null value on total row
    Public m_refund As Decimal
    Public m_prize_coupon As Decimal
    Public m_prize_machine As Decimal
    Public m_prize As Decimal
    Public m_tax1 As Decimal
    Public m_tax2 As Decimal
    Public m_tax3 As Decimal
    Public m_rounding_for_retentions As Decimal
    Public m_service_charge As Decimal
    Public m_to_pay As Decimal
    Public m_rounding_for_decimals As Decimal
    Public m_total_paid As Decimal
    Public m_tax_custody As Decimal   'EOR  26-APR-2016

    'EOR 26-APR-2016
    Public Sub New(ByVal SessionID As Object, _
                   ByVal AccountID As Object, _
                   ByVal AccountName As Object, _
                   ByVal MovementDate As Object, _
                   ByVal Refund As Decimal, _
                   ByVal PrizeCoupon As Decimal, _
                   ByVal PrizeMachine As Decimal, _
                   ByVal Prize As Decimal, _
                   ByVal Tax1 As Decimal, _
                   ByVal Tax2 As Decimal, _
                   ByVal Tax3 As Decimal, _
                   ByVal RoundingForRetentions As Object, _
                   ByVal ServiceCharge As Decimal, _
                   ByVal ToPay As Decimal, _
                   ByVal RoundingForDecimals As Decimal, _
                   ByVal TotalPaid As Decimal, _
                   ByVal TaxCustody As Decimal)
      m_session_id = SessionID
      m_account_id = AccountID
      m_account_name = AccountName
      m_rounding_for_retentions = RoundingForRetentions
      m_movement_date = MovementDate
      m_refund = Refund
      m_prize_coupon = PrizeCoupon
      m_prize_machine = PrizeMachine
      m_prize = Prize
      m_tax1 = Tax1
      m_tax2 = Tax2
      m_tax3 = Tax3
      m_service_charge = ServiceCharge
      m_to_pay = ToPay
      m_rounding_for_decimals = RoundingForDecimals
      m_total_paid = TotalPaid
      m_tax_custody = TaxCustody
    End Sub
  End Class
#End Region ' Structures

#Region " Enums "

#End Region      ' Enums

#Region " Members "

  ' Report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_users As String
  Private m_show_details As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_winnings_amount As String

  Private m_apply_winnings_amount_filter As Boolean
  Private m_is_council_tax_active As Boolean

#End Region    ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    m_apply_winnings_amount_filter = True

    Select Case Me.FormId
      Case ENUM_FORM.FORM_RETREATS_REPORT
        ' Retreats report
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1360)
        Me.Width = 1171

      Case ENUM_FORM.FORM_SERVICE_CHARGE_REPORT
        ' Service Charge report
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2214)
        Me.Width = 1051
        m_apply_winnings_amount_filter = False

      Case Else
        ' Retreats report
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1360)
        Me.Width = 1171

    End Select

    'Call set form features resolutions when size's form exceeds desktop area
    Call GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    'Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_INVOICING.GetString(437)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' Filters
    Me.chk_show_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1361)
    Me.chk_only_report_of_decimals_rounding.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1362)

    ' Winnings amount
    Me.ef_winnings_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5016)
    Me.ef_winnings_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)
    Me.ef_winnings_amount.Visible = m_apply_winnings_amount_filter

    ' Set filter default values
    Call SetDefaultValues()

    Call GUI_StyleSheet()

  End Sub       ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub        ' GUI_FilterReset  

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.dtp_from

  End Sub    ' GUI_SetInitialFocus

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel.ValidateFormat() Then
      Return False
    End If

    Return True
  End Function   ' GUI_FilterCheck  

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function  ' GUI_GetQueryType

  ' PURPOSE : Define the ExecuteQuery customized
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _from_date As Date
    Dim _to_date As Date

    Dim _ds As DataSet

    Dim _column_parent As DataColumn
    Dim _column_child As DataColumn

    Dim _table_mov As String
    Dim _table_ses As String
    Dim _relation_name As String
    Dim _data_relation As DataRelation

    Dim _table_account As String
    Dim _relation_name_acc As String
    Dim _data_relation_acc As DataRelation
    Dim _internal_card_id As String
    Dim _account_filter As String
    Dim _card_type As Integer

    Dim _dr_session() As DataRow

    Dim _where As String
    Dim _sort As String

    Dim _grid_row As RETREAT_REPORT_GRID_ROW

    Dim _account_name As String

    Dim _refund As Decimal
    Dim _refund_temp As Decimal
    Dim _prize_coupon As Decimal
    Dim _prize_machine As Decimal
    Dim _prize As Decimal
    Dim _tax1 As Decimal
    Dim _tax2 As Decimal
    Dim _tax3 As Decimal
    Dim _rounding_for_retentions As Decimal
    Dim _service_charge As Decimal
    Dim _to_pay As Decimal
    Dim _rounding_for_decimals As Decimal
    Dim _total_paid As Decimal
    Dim _tax_custody As Decimal   'EOR 26-APR-2016

    Dim _refund_session As Decimal
    Dim _prize_coupon_session As Decimal
    Dim _prize_machine_session As Decimal
    Dim _prize_session As Decimal
    Dim _tax1_session As Decimal
    Dim _tax2_session As Decimal
    Dim _tax3_session As Decimal
    Dim _rounding_for_retentions_session As Decimal
    Dim _service_charge_session As Decimal
    Dim _to_pay_session As Decimal
    Dim _rounding_for_decimals_session As Decimal
    Dim _total_paid_session As Decimal
    Dim _tax_custody_session As Decimal     'EOR 26-APR-2016

    Dim _refund_total As Decimal
    Dim _prize_coupon_total As Decimal
    Dim _prize_machine_total As Decimal
    Dim _prize_total As Decimal
    Dim _tax1_total As Decimal
    Dim _tax2_total As Decimal
    Dim _tax3_total As Decimal
    Dim _rounding_for_retentions_total As Decimal
    Dim _service_charge_total As Decimal
    Dim _to_pay_total As Decimal
    Dim _rounding_for_decimals_total As Decimal
    Dim _total_paid_total As Decimal
    Dim _tax_custody_total As Decimal   'EOR 26-APR-2016

    Dim _idx_row As Integer

    Dim _rows_session As Integer

    Dim _split_data As New TYPE_SPLITS

    Split.ReadSplitParameters(_split_data)

    _ds = Nothing
    _dr_session = Nothing

    _refund_total = 0
    _prize_coupon_total = 0
    _prize_machine_total = 0
    _prize_total = 0
    _tax1_total = 0
    _tax2_total = 0
    _tax3_total = 0
    _rounding_for_retentions_total = 0
    _service_charge_total = 0
    _to_pay_total = 0
    _rounding_for_decimals_total = 0
    _total_paid_total = 0
    _tax_custody_total = 0  'EOR 26-APR-2016

    _sort = "SesionCaja, NoCuenta, IdOperacion, FechaHora" 'Default sort

    _table_mov = "Movimiento"
    _table_ses = "SesionesCaja"
    _relation_name = "Movimiento-Sesion"

    _table_account = "Cuenta"
    _relation_name_acc = "Cuenta-Movimiento"

    Try
      Dim _t0 As TimeSpan
      Dim _t1 As TimeSpan
      _t0 = WSI.Common.Performance.GetTickCount()

      Call GUI_StyleSheet()

      'Get date filters
      If Me.dtp_from.Checked Then
        _from_date = Me.dtp_from.Value
      Else
        _from_date = New Date(2007, 1, 1) 'There wouldn't be any prize before 1-JAN-2007
      End If

      If Me.dtp_to.Checked Then
        _to_date = Me.dtp_to.Value
      Else
        _to_date = WSI.Common.Misc.TodayOpening().AddDays(1)
      End If

      _ds = WSI.Common.XMLReport.AccountMovements(_from_date, _to_date, WSI.Common.XMLReport.EnumAccountMovementsReport.RetreatsReport)

      If Not WSI.Common.XMLReport.AddCashierSessionsTable(_ds) Then
        Exit Sub
      End If

      ' Relations between Movements and Session
      _column_parent = _ds.Tables(_table_ses).Columns("cs_session_id")
      _column_child = _ds.Tables(_table_mov).Columns("SesionCaja")
      _data_relation = New DataRelation(_relation_name, _column_parent, _column_child, True)
      _ds.Relations.Add(_data_relation)

      ' Relations between Account and Movement
      _column_parent = _ds.Tables(_table_mov).Columns("NoCuenta")
      _column_child = _ds.Tables(_table_account).Columns("NoCuenta")
      _data_relation_acc = New DataRelation(_relation_name_acc, _column_parent, _column_child, False)
      _ds.Relations.Add(_data_relation_acc)


      Me.Grid.Redraw = False

      If _ds.Tables.Count < 4 Then
        Throw New Exception("Minimum tables has not been retrieved. Tables.Count = " & _ds.Tables.Count)
      End If

      _where = "" ' "ISNULL(Deposito,0)<=0 "

      ' Filter User
      If Me.opt_one_user.Checked = True Then
        _where = _where & " CS_USER_ID = " & Me.cmb_user.Value
      End If

      _dr_session = _ds.Tables(_table_ses).Select(_where, "cs_session_id")

      ' JAB 24-AUG-2012: Control to show huge amounts of records.
      If Not ShowHugeNumberOfRows(_dr_session.Length) Then
        MyBase.m_user_canceled_data_shows = True
        Exit Sub
      End If

      For Each _row_session As DataRow In _dr_session
        _refund_session = 0
        _prize_coupon_session = 0
        _prize_machine_session = 0
        _prize_session = 0
        _tax1_session = 0
        _tax2_session = 0
        _tax3_session = 0
        _rounding_for_retentions_session = 0
        _service_charge_session = 0
        _to_pay_session = 0
        _rounding_for_decimals_session = 0
        _total_paid_session = 0
        _rows_session = 0
        _tax_custody_session = 0    'EOR 26-APR-2016

        For Each _row_mov As DataRow In _row_session.GetChildRows(_data_relation)

          If IsDBNull(_row_mov.Item("Deposito")) OrElse _row_mov.Item("Deposito") <= 0 Then
            ' JAB 24-AUG-2012: DoEvents run each second.
            If Not GUI_DoEvents(Me.Grid) Then
              Exit Sub
            End If

            If Not String.IsNullOrEmpty(uc_account_sel.Account) AndAlso _row_mov.Item("NoCuenta") <> uc_account_sel.Account Then
              Continue For
            End If

            If Not String.IsNullOrEmpty(uc_account_sel.TrackData) Then
              If uc_account_sel.TrackData.Length = TRACK_DATA_LENGTH_WIN Then
                _internal_card_id = ""
                If WSI.Common.CardNumber.TrackDataToInternal(uc_account_sel.TrackData, _internal_card_id, _card_type) Then
                  _account_filter = _internal_card_id
                Else
                  _account_filter = uc_account_sel.TrackData
                End If
              Else
                _account_filter = uc_account_sel.TrackData
              End If

              If _row_mov.GetChildRows(_data_relation_acc) Is Nothing OrElse Not _row_mov.GetChildRows(_data_relation_acc)(0).Item("TrackData") = _account_filter Then
                Continue For
              End If
            End If

            If Not IsDBNull(_row_mov.Item("Nombre")) Then
              _account_name = _row_mov.Item("Nombre")
            Else
              _account_name = ""
            End If
            If Not String.IsNullOrEmpty(uc_account_sel.Holder) AndAlso Not UCase(_account_name).ToString().Contains(UCase(uc_account_sel.Holder)) Then
              Continue For
            End If

            If IsDBNull(_row_mov.Item("Devolucion")) Then
              _refund = 0
            Else
              _refund = _row_mov.Item("Devolucion")
            End If

            If IsDBNull(_row_mov.Item("Premio")) Then
              _prize = 0
            Else
              _prize = _row_mov.Item("Premio")
            End If
            If m_apply_winnings_amount_filter AndAlso Me.ef_winnings_amount.Value <> String.Empty AndAlso _prize <= Me.ef_winnings_amount.Value Then
              Continue For
            End If

            If IsDBNull(_row_mov.Item("ISR1")) Then
              _tax1 = 0
            Else
              _tax1 = _row_mov.Item("ISR1")
            End If

            If IsDBNull(_row_mov.Item("ISR2")) Then
              _tax2 = 0
            Else
              _tax2 = _row_mov.Item("ISR2")
            End If

            If IsDBNull(_row_mov.Item("ISR3")) Then
              _tax3 = 0
            Else
              _tax3 = _row_mov.Item("ISR3")
            End If

            'EOR 26-APR-2016
            If IsDBNull(_row_mov.Item("ImpuestoCustodia")) Then
              _tax_custody = 0
            Else
              _tax_custody = _row_mov.Item("ImpuestoCustodia")
            End If

            If IsDBNull(_row_mov.Item("RedondeoPorRetencion")) Then
              _rounding_for_retentions = 0
            Else
              _rounding_for_retentions = _row_mov.Item("RedondeoPorRetencion")
            End If

            If IsDBNull(_row_mov.Item("CargoPorServicio")) Then
              _service_charge = 0
            Else
              _service_charge = _row_mov.Item("CargoPorServicio")
            End If

            _to_pay = _refund + _prize - _tax1 - _tax2 - _tax3 + _rounding_for_retentions - _service_charge + _tax_custody  'EOR  26-APR-2016

            If IsDBNull(_row_mov.Item("RedondeoPorDecimales")) Then
              _rounding_for_decimals = 0
            Else
              _rounding_for_decimals = _row_mov.Item("RedondeoPorDecimales")
            End If

            _total_paid = _to_pay + _rounding_for_decimals

            _refund_temp = Math.Round(WSI.Common.DevolutionPrizeParts.InflateDueToCashInTax(_refund), 2, MidpointRounding.AwayFromZero)
            _prize_coupon = Math.Round((_refund_temp * _split_data.company_b.cashin_pct) / (_split_data.company_a.cashin_pct), 2, MidpointRounding.AwayFromZero)
            If _prize_coupon > _prize Then
              _prize_coupon = _prize
            End If
            _prize_machine = _prize - _prize_coupon

            _grid_row = New RETREAT_REPORT_GRID_ROW(_row_session.Item("cs_name"), _
                                                    _row_mov.Item("NoCuenta"), _
                                                    _account_name, _
                                                    _row_mov.Item("FechaHora"), _
                                                    _refund, _
                                                    _prize_coupon, _
                                                    _prize_machine, _
                                                    _prize, _
                                                    _tax1, _
                                                    _tax2, _
                                                    _tax3, _
                                                    _rounding_for_retentions, _
                                                    _service_charge, _
                                                    _to_pay, _
                                                    _rounding_for_decimals, _
                                                    _total_paid, _
                                                    _tax_custody)


            If Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT And _grid_row.m_service_charge = 0 Then
              ' DHA 21-AUG-2013: If ServiceCharge is 0, the row is not added and totals are not updated
            Else
              If chk_show_details.Checked Then

                Me.Grid.AddRow()

                _idx_row = Me.Grid.NumRows - 1

                If Not SetupRow(_idx_row, _grid_row) Then
                  ' The row can not be added
                  Me.Grid.DeleteRowFast(_idx_row)
                Else
                  Me.Grid.Counter(COUNTER_DETAILS).Value += 1
                End If

              End If

              _refund_session += _refund
              _prize_coupon_session += _prize_coupon
              _prize_machine_session += _prize_machine
              _prize_session += _prize
              _tax1_session += _tax1
              _tax2_session += _tax2
              _tax3_session += _tax3
              _rounding_for_retentions_session += _rounding_for_retentions
              _service_charge_session += _service_charge
              _to_pay_session += _to_pay
              _rounding_for_decimals_session += _rounding_for_decimals
              _total_paid_session += _total_paid
              _tax_custody_session += _tax_custody

              _refund_total += _refund
              _prize_coupon_total += _prize_coupon
              _prize_machine_total += _prize_machine
              _prize_total += _prize
              _tax1_total += _tax1
              _tax2_total += _tax2
              _tax3_total += _tax3
              _rounding_for_retentions_total += _rounding_for_retentions
              _service_charge_total += _service_charge
              _to_pay_total += _to_pay
              _rounding_for_decimals_total += _rounding_for_decimals
              _total_paid_total += _total_paid
              _tax_custody_total += _tax_custody

              _rows_session += 1

            End If
          End If

        Next

        'put on the grid the session's totals 
        If _rows_session > 0 Then
          If Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT And _service_charge_total = 0 Then
            ' DHA 21-AUG-2013: If ServiceCharge is 0, the row is not added
          Else
            Call ShowSesionTotals(_row_session.Item("cs_name"), _
                                  _refund_session, _
                                  _prize_coupon_session, _
                                  _prize_machine_session, _
                                  _prize_session, _
                                  _tax1_session, _
                                  _tax2_session, _
                                  _tax3_session, _
                                  _rounding_for_retentions_session, _
                                  _service_charge_session, _
                                  _to_pay_session, _
                                  _rounding_for_decimals_session, _
                                  _total_paid_session, _
                                  _tax_custody_session) 'EOR 26-APR-2016
          End If
        End If
      Next


      'put on the grid the totals 
      Call ShowTotals(_refund_total, _
                      _prize_coupon_total, _
                      _prize_machine_total, _
                      _prize_total, _
                      _tax1_total, _
                      _tax2_total, _
                      _tax3_total, _
                      _rounding_for_retentions_total, _
                      _service_charge_total, _
                      _to_pay_total, _
                      _rounding_for_decimals_total, _
                      _total_paid_total, _
                      _tax_custody_total)

      _t1 = WSI.Common.Performance.GetElapsedTicks(_t0)

      Debug.WriteLine(_t1.ToString())


    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Capacity", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)

    Finally
      If Not Me.IsDisposed Then
        Me.Grid.Redraw = True
      End If

      Call Application.DoEvents()

      If _ds IsNot Nothing Then
        Call _ds.Clear()
        Call _ds.Dispose()
        _ds = Nothing
      End If

    End Try

  End Sub ' GUI_ExecuteQueryCustom

#Region " GUI Reports "

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'First col
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(220), m_users)
    PrintData.SetFilter(Me.chk_show_details.Text, m_show_details)

    If m_apply_winnings_amount_filter Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5016), m_winnings_amount)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)

    PrintData.FilterHeaderWidth(1) = 1500
    PrintData.FilterValueWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 1500
    PrintData.FilterValueWidth(2) = 5000
    PrintData.FilterHeaderWidth(3) = 1000
    PrintData.FilterValueWidth(3) = 1000
    PrintData.FilterHeaderWidth(4) = 1000
    PrintData.FilterValueWidth(4) = 1000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  ' 
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_users = ""
    m_date_from = ""
    m_date_to = ""
    m_winnings_amount = ""

    ' Card Account number
    m_account = Me.uc_account_sel.Account()
    m_track_data = Me.uc_account_sel.TrackData()
    m_holder = Me.uc_account_sel.Holder()

    ' Date
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      m_date_from = ""
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      m_date_to = ""
    End If

    ' Users
    If Me.opt_all_users.Checked Then
      m_users = Me.opt_all_users.Text
    Else
      m_users = Me.cmb_user.TextValue
    End If

    ' Show Details
    If Me.chk_show_details.Checked = True Then
      m_show_details = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_show_details = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If m_apply_winnings_amount_filter AndAlso Me.ef_winnings_amount.Value <> String.Empty Then
      m_winnings_amount = GUI_FormatCurrency(Me.ef_winnings_amount.Value)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region        ' GUI Reports

#End Region  ' OVERRIDES

#Region " Private Functions"

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim initial_time As Date

    initial_time = WSI.Common.Misc.TodayOpening()

    ' Set at current month's first day 
    Me.dtp_from.Value = initial_time.AddDays(-initial_time.Day + 1)
    Me.dtp_from.Checked = True
    Me.dtp_to.Value = initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    ' Set combo with Users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " _
                  & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")

    Me.opt_all_users.Checked = True
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Checked = False
    Me.chk_show_all.Enabled = False

    Me.chk_show_details.Checked = True
    Me.chk_only_report_of_decimals_rounding.Checked = False

    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.ShowVipClients = False

    Me.ef_winnings_amount.Value = String.Empty

    Me.m_is_council_tax_active = WSI.Common.GeneralParam.GetDecimal("Cashier", "Promotions.RE.Tax.3.Pct") > 0

  End Sub     ' SetDefaultValues  

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()


    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False

      .Counter(COUNTER_DETAILS).Visible = True
      .Counter(COUNTER_DETAILS).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_DETAILS).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      .Counter(COUNTER_SESSION_TOTALS).Visible = True
      .Counter(COUNTER_SESSION_TOTALS).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_SESSION_TOTALS).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Session id
      .Column(GRID_COL_SESSION_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(208)
      .Column(GRID_COL_SESSION_ID).Width = 3000
      .Column(GRID_COL_SESSION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
      .Column(GRID_COL_SESSION_ID).IsMerged = True

      If Me.chk_show_details.Checked Then
        ' AccountID
        .Column(GRID_COL_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
        .Column(GRID_COL_ACCOUNT_ID).Width = 1150
        .Column(GRID_COL_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER
        .Column(GRID_COL_ACCOUNT_ID).IsMerged = True

        ' Name
        .Column(GRID_COL_ACCOUNT_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(210)
        .Column(GRID_COL_ACCOUNT_NAME).Width = 3000
        .Column(GRID_COL_ACCOUNT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER
        .Column(GRID_COL_ACCOUNT_NAME).IsMerged = True

        ' Date
        .Column(GRID_COL_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
        .Column(GRID_COL_DATE).Width = 2000
        .Column(GRID_COL_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      Else
        ' AccountID
        .Column(GRID_COL_ACCOUNT_ID).Header(0).Text = ""
        .Column(GRID_COL_ACCOUNT_ID).Width = 0

        ' Name
        .Column(GRID_COL_ACCOUNT_NAME).Header(0).Text = ""
        .Column(GRID_COL_ACCOUNT_NAME).Width = 0

        ' Date
        .Column(GRID_COL_DATE).Header(0).Text = ""
        .Column(GRID_COL_DATE).Width = 0

      End If

      If Not Me.chk_only_report_of_decimals_rounding.Checked Then
        ' Refund
        .Column(GRID_COL_REFUND).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(31)
        ' DHA: 21-AUG-2013: Visible/hidden by general param
        If Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT And Not WSI.Common.GeneralParam.GetBoolean("Cashier.ServiceCharge", "Report.ShowRefund", False) Then
          .Column(GRID_COL_REFUND).Width = 0
        Else
          .Column(GRID_COL_REFUND).Width = 1800
        End If
        .Column(GRID_COL_REFUND).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


        ' Prize, B
        If Me.FormId = ENUM_FORM.FORM_RETREATS_REPORT And Not WSI.Common.GeneralParam.GetBoolean("Cashier.Withdrawal", "Report.PrizeCoupon.Hide", False) Then
          .Column(GRID_COL_PRIZE_COUPON).Width = 1800
          .Column(GRID_COL_PRIZE_COUPON).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier.Withdrawal", "Report.PrizeCoupon.Title")
        Else
          .Column(GRID_COL_PRIZE_COUPON).Width = 0
        End If
        .Column(GRID_COL_PRIZE_COUPON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Prize, Machine
        If Me.FormId = ENUM_FORM.FORM_RETREATS_REPORT And Not WSI.Common.GeneralParam.GetBoolean("Cashier.Withdrawal", "Report.MachinePrize.Hide", False) Then
          .Column(GRID_COL_PRIZE_MACHINE).Width = 1800
          .Column(GRID_COL_PRIZE_MACHINE).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier.Withdrawal", "Report.MachinePrize.Title")
        Else
          .Column(GRID_COL_PRIZE_MACHINE).Width = 0
        End If
        .Column(GRID_COL_PRIZE_MACHINE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT





        ' Prize
        .Column(GRID_COL_PRIZE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(33)
        ' DHA: 21-AUG-2013: Visible/hidden by general param
        If Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT And Not WSI.Common.GeneralParam.GetBoolean("Cashier.ServiceCharge", "Report.ShowPrize", False) Then
          .Column(GRID_COL_PRIZE).Width = 0
        Else
          .Column(GRID_COL_PRIZE).Width = 1800
        End If
        .Column(GRID_COL_PRIZE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Tax1
        .Column(GRID_COL_TAX1).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")
        .Column(GRID_COL_TAX1).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1750)
        .Column(GRID_COL_TAX1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Tax2
        .Column(GRID_COL_TAX2).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")
        .Column(GRID_COL_TAX2).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1750)
        .Column(GRID_COL_TAX2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Tax3
        .Column(GRID_COL_TAX3).Header(0).Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name")
        If m_is_council_tax_active Then
          .Column(GRID_COL_TAX3).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1750)
        Else
          .Column(GRID_COL_TAX3).Width = 0
        End If
        .Column(GRID_COL_TAX3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Tax Custody EOR 26-APR-2016
        .Column(GRID_COL_CUSTODY).Header(0).Text = WSI.Common.Misc.TaxCustodyRefundName
        .Column(GRID_COL_CUSTODY).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1750)
        .Column(GRID_COL_CUSTODY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Rounding for retentions
        .Column(GRID_COL_ROUNDING_FOR_RETENTIONS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1346)
        .Column(GRID_COL_ROUNDING_FOR_RETENTIONS).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1750)
        .Column(GRID_COL_ROUNDING_FOR_RETENTIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        ' Service charge
        .Column(GRID_COL_SERVICE_CHARGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127)
        .Column(GRID_COL_SERVICE_CHARGE).Width = 1750
        .Column(GRID_COL_SERVICE_CHARGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        ' Refund
        .Column(GRID_COL_REFUND).Header(0).Text = ""
        .Column(GRID_COL_REFUND).Width = 0

        ' Prize, B
        .Column(GRID_COL_PRIZE_COUPON).Header(0).Text = ""
        .Column(GRID_COL_PRIZE_COUPON).Width = 0

        ' Prize, Machine
        .Column(GRID_COL_PRIZE_MACHINE).Header(0).Text = ""
        .Column(GRID_COL_PRIZE_MACHINE).Width = 0

        ' Prize
        .Column(GRID_COL_PRIZE).Header(0).Text = ""
        .Column(GRID_COL_PRIZE).Width = 0

        ' Tax1
        .Column(GRID_COL_TAX1).Header(0).Text = ""
        .Column(GRID_COL_TAX1).Width = 0

        ' Tax2
        .Column(GRID_COL_TAX2).Header(0).Text = ""
        .Column(GRID_COL_TAX2).Width = 0

        ' Tax3
        .Column(GRID_COL_TAX3).Header(0).Text = ""
        .Column(GRID_COL_TAX3).Width = 0

        ' Rounding for retentions
        .Column(GRID_COL_ROUNDING_FOR_RETENTIONS).Header(0).Text = ""
        .Column(GRID_COL_ROUNDING_FOR_RETENTIONS).Width = 0

        ' Service charge
        .Column(GRID_COL_SERVICE_CHARGE).Header(0).Text = ""
        .Column(GRID_COL_SERVICE_CHARGE).Width = 0
      End If

      ' To pay 
      .Column(GRID_COL_TO_PAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1347)
      .Column(GRID_COL_TO_PAY).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1800)
      .Column(GRID_COL_TO_PAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Rounding for decimals
      .Column(GRID_COL_ROUNDING_FOR_DECIMALS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1348)
      .Column(GRID_COL_ROUNDING_FOR_DECIMALS).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1800)
      .Column(GRID_COL_ROUNDING_FOR_DECIMALS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Paid
      .Column(GRID_COL_TOTAL_PAID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1349)
      .Column(GRID_COL_TOTAL_PAID).Width = IIf(Me.FormId = ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, 0, 1800)
      .Column(GRID_COL_TOTAL_PAID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub       ' GUI_StyleSheet

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  ' 
  ' NOTES :
  '     -  It differs from overridiable one on frm_base_sel
  ' 
  Private Function SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As RETREAT_REPORT_GRID_ROW) As Boolean

    If Not IsDBNull(DbRow.m_session_id) Then
      Me.Grid.Cell(RowIndex, GRID_COL_SESSION_ID).Value = DbRow.m_session_id
    End If

    If Not IsDBNull(DbRow.m_account_id) Then
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_ID).Value = DbRow.m_account_id
    End If

    If Not IsDBNull(DbRow.m_account_name) Then
      Me.Grid.Cell(RowIndex, GRID_COL_ACCOUNT_NAME).Value = DbRow.m_account_name
    End If

    If Not IsDBNull(DbRow.m_movement_date) Then
      Me.Grid.Cell(RowIndex, GRID_COL_DATE).Value = GUI_FormatDate(DbRow.m_movement_date, _
                                                                       ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                       ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If



    Me.Grid.Cell(RowIndex, GRID_COL_REFUND).Value = GUI_FormatCurrency(DbRow.m_refund)
    Me.Grid.Cell(RowIndex, GRID_COL_PRIZE_COUPON).Value = GUI_FormatCurrency(DbRow.m_prize_coupon)
    Me.Grid.Cell(RowIndex, GRID_COL_PRIZE_MACHINE).Value = GUI_FormatCurrency(DbRow.m_prize_machine)
    Me.Grid.Cell(RowIndex, GRID_COL_PRIZE).Value = GUI_FormatCurrency(DbRow.m_prize)
    Me.Grid.Cell(RowIndex, GRID_COL_TAX1).Value = GUI_FormatCurrency(DbRow.m_tax1)
    Me.Grid.Cell(RowIndex, GRID_COL_TAX2).Value = GUI_FormatCurrency(DbRow.m_tax2)
    If (DbRow.m_tax3 > 0 AndAlso Me.FormId = ENUM_FORM.FORM_RETREATS_REPORT) Then
      Me.Grid.Column(GRID_COL_TAX3).Fixed = False
      Me.Grid.Column(GRID_COL_TAX3).Width = 1750
    End If
    Me.Grid.Cell(RowIndex, GRID_COL_TAX3).Value = GUI_FormatCurrency(DbRow.m_tax3)
    Me.Grid.Cell(RowIndex, GRID_COL_CUSTODY).Value = GUI_FormatCurrency(DbRow.m_tax_custody)   'EOR 26-APR-2016
    Me.Grid.Cell(RowIndex, GRID_COL_ROUNDING_FOR_RETENTIONS).Value = GUI_FormatCurrency(DbRow.m_rounding_for_retentions)
    Me.Grid.Cell(RowIndex, GRID_COL_SERVICE_CHARGE).Value = GUI_FormatCurrency(DbRow.m_service_charge)
    Me.Grid.Cell(RowIndex, GRID_COL_TO_PAY).Value = GUI_FormatCurrency(DbRow.m_to_pay)
    Me.Grid.Cell(RowIndex, GRID_COL_ROUNDING_FOR_DECIMALS).Value = GUI_FormatCurrency(DbRow.m_rounding_for_decimals)
    Me.Grid.Cell(RowIndex, GRID_COL_TOTAL_PAID).Value = GUI_FormatCurrency(DbRow.m_total_paid)


    Return True
  End Function        ' GUI_SetupRow

  ' PURPOSE : Loads the totals into the Grid
  '
  '  PARAMS :
  '      - INPUT :
  '           - PrizeTotal : Decimal
  '           - Tax1Total : Decimal
  '           - Tax2Total : Decimal
  '           - Tax3Total : Decimal
  '           - PaymentTotal : Decimal
  '
  '      - OUTPUT :
  '
  Private Sub ShowTotals(ByVal Refund As Decimal, _
                         ByVal PrizeCoupon As Decimal, _
                         ByVal PrizeMachine As Decimal, _
                         ByVal Prize As Decimal, _
                         ByVal Tax1 As Decimal, _
                         ByVal Tax2 As Decimal, _
                         ByVal Tax3 As Decimal, _
                         ByVal RoundingForRetentions As Decimal, _
                         ByVal ServiceCharge As Decimal, _
                         ByVal ToPaid As Decimal, _
                         ByVal RoundingForDecimals As Decimal, _
                         ByVal Total_Paid As Decimal, _
                         ByVal TaxCustody As Decimal)

    Dim _idx_row As Integer
    Dim _grid_row As RETREAT_REPORT_GRID_ROW

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    _grid_row = New RETREAT_REPORT_GRID_ROW(GLB_NLS_GUI_INVOICING.GetString(205) & " ", _
                                            DBNull.Value, _
                                            DBNull.Value, _
                                            DBNull.Value, _
                                            Refund, _
                                            PrizeCoupon, _
                                            PrizeMachine, _
                                            Prize, _
                                            Tax1, _
                                            Tax2, _
                                            Tax3, _
                                            RoundingForRetentions, _
                                            ServiceCharge, _
                                            ToPaid, _
                                            RoundingForDecimals, _
                                            Total_Paid, _
                                            TaxCustody)

    SetupRow(_idx_row, _grid_row)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub           ' ShowTotals 

  ' PURPOSE : Loads the accounts totals into the Grid
  '
  '  PARAMS :
  '      - INPUT :
  '           - AccountDR : Account DataRow
  '
  '      - OUTPUT :
  '
  ' NOTES :
  '      - Columns names match the XML fields exported to the XML requiered by SAT
  Private Sub ShowSesionTotals(ByVal SessionId As String, _
                               ByVal Refund As Decimal, _
                               ByVal PrizeCoupon As Decimal, _
                               ByVal PrizeMachine As Decimal, _
                               ByVal Prize As Decimal, _
                               ByVal Tax1 As Decimal, _
                               ByVal Tax2 As Decimal, _
                               ByVal Tax3 As Decimal, _
                               ByVal RoundingForRetentions As Decimal, _
                               ByVal ServiceCharge As Decimal, _
                               ByVal ToPaid As Decimal, _
                               ByVal RoundingForDecimals As Decimal, _
                               ByVal Total_Paid As Decimal, _
                               ByVal TaxCustody As Decimal) 'EOR 26-APR-2016

    Dim _idx_row As Integer
    Dim _grid_row As RETREAT_REPORT_GRID_ROW
    Dim _total_text As String

    Me.Grid.AddRow()

    _idx_row = Me.Grid.NumRows - 1

    If (_idx_row Mod 2 = 0) Then
      _total_text = GLB_NLS_GUI_INVOICING.GetString(205)
    Else
      _total_text = GLB_NLS_GUI_INVOICING.GetString(205) & " "
    End If

    _grid_row = New RETREAT_REPORT_GRID_ROW(SessionId.ToString() & " ", _
                                            DBNull.Value, _
                                            _total_text, _
                                            DBNull.Value, _
                                            Refund, _
                                            PrizeCoupon, _
                                            PrizeMachine, _
                                            Prize, _
                                            Tax1, _
                                            Tax2, _
                                            Tax3, _
                                            RoundingForRetentions, _
                                            ServiceCharge, _
                                            ToPaid, _
                                            RoundingForDecimals, _
                                            Total_Paid, _
                                            TaxCustody) 'EOR 26-APR-2016


    SetupRow(_idx_row, _grid_row)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Me.Grid.Counter(COUNTER_SESSION_TOTALS).Value += 1

  End Sub     ' ShowAccountTotals

#End Region ' Private Functions

#Region " Public Functions "

  Public Sub New()
    MyBase.New()

    Me.FormId = ENUM_FORM.FORM_RETREATS_REPORT

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub ' New

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub ' New

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub opt_several_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub      ' opt_several_users_Click

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub          ' opt_all_users_Click

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub  ' chk_show_all

  Private Sub ef_winnings_amount_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_winnings_amount.Validated

    'if the entry field is not empty and < 0, let's set at 0
    If Me.ef_winnings_amount.Value <> String.Empty AndAlso Me.ef_winnings_amount.Value < 0 Then
      Me.ef_winnings_amount.Value = 0
    End If

  End Sub ' ef_winnings_amount_Validated

#End Region       ' Events

End Class