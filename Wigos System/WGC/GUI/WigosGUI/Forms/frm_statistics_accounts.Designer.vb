<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_statistics_accounts
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel
    Me.chk_only_anonymous = New System.Windows.Forms.CheckBox
    Me.gb_level = New System.Windows.Forms.GroupBox
    Me.chk_level_04 = New System.Windows.Forms.CheckBox
    Me.chk_level_03 = New System.Windows.Forms.CheckBox
    Me.chk_level_02 = New System.Windows.Forms.CheckBox
    Me.chk_level_01 = New System.Windows.Forms.CheckBox
    Me.gb_gender = New System.Windows.Forms.GroupBox
    Me.chk_gender_female = New System.Windows.Forms.CheckBox
    Me.chk_gender_male = New System.Windows.Forms.CheckBox
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.chk_account = New System.Windows.Forms.RadioButton
    Me.xAccountProvider = New System.Windows.Forms.GroupBox
    Me.chk_account_provider = New System.Windows.Forms.RadioButton
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_gender.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.xAccountProvider.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.chk_only_anonymous)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.xAccountProvider)
    Me.panel_filter.Controls.Add(Me.gb_gender)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(889, 197)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gender, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.xAccountProvider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_anonymous, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 201)
    Me.panel_data.Size = New System.Drawing.Size(889, 293)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(883, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(883, 4)
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(3, 3)
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.Size = New System.Drawing.Size(309, 108)
    Me.uc_account_sel.TabIndex = 11
    Me.uc_account_sel.TrackData = ""
    '
    'chk_only_anonymous
    '
    Me.chk_only_anonymous.AutoSize = True
    Me.chk_only_anonymous.Location = New System.Drawing.Point(329, 171)
    Me.chk_only_anonymous.Name = "chk_only_anonymous"
    Me.chk_only_anonymous.Size = New System.Drawing.Size(130, 17)
    Me.chk_only_anonymous.TabIndex = 19
    Me.chk_only_anonymous.Text = "xOnly Anonymous"
    Me.chk_only_anonymous.UseVisualStyleBackColor = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(318, 5)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(160, 101)
    Me.gb_level.TabIndex = 12
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(11, 77)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 16
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(11, 57)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 15
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(11, 37)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 14
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(11, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 13
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'gb_gender
    '
    Me.gb_gender.Controls.Add(Me.chk_gender_female)
    Me.gb_gender.Controls.Add(Me.chk_gender_male)
    Me.gb_gender.Location = New System.Drawing.Point(318, 108)
    Me.gb_gender.Name = "gb_gender"
    Me.gb_gender.Size = New System.Drawing.Size(160, 58)
    Me.gb_gender.TabIndex = 13
    Me.gb_gender.TabStop = False
    Me.gb_gender.Text = "xGender"
    '
    'chk_gender_female
    '
    Me.chk_gender_female.Location = New System.Drawing.Point(11, 36)
    Me.chk_gender_female.Name = "chk_gender_female"
    Me.chk_gender_female.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_female.TabIndex = 18
    Me.chk_gender_female.Text = "xWomen"
    '
    'chk_gender_male
    '
    Me.chk_gender_male.Location = New System.Drawing.Point(11, 17)
    Me.chk_gender_male.Name = "chk_gender_male"
    Me.chk_gender_male.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_male.TabIndex = 17
    Me.chk_gender_male.Text = "xMen"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 111)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(306, 77)
    Me.gb_date.TabIndex = 20
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(49, 46)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(197, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(49, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(197, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2003, 5, 20, 0, 0, 0, 0)
    '
    'chk_account
    '
    Me.chk_account.AutoSize = True
    Me.chk_account.Location = New System.Drawing.Point(18, 20)
    Me.chk_account.Name = "chk_account"
    Me.chk_account.Size = New System.Drawing.Size(101, 17)
    Me.chk_account.TabIndex = 21
    Me.chk_account.TabStop = True
    Me.chk_account.Text = "RadioButton1"
    Me.chk_account.UseVisualStyleBackColor = True
    '
    'xAccountProvider
    '
    Me.xAccountProvider.Controls.Add(Me.chk_account_provider)
    Me.xAccountProvider.Controls.Add(Me.chk_account)
    Me.xAccountProvider.Location = New System.Drawing.Point(486, 4)
    Me.xAccountProvider.Name = "xAccountProvider"
    Me.xAccountProvider.Size = New System.Drawing.Size(145, 73)
    Me.xAccountProvider.TabIndex = 13
    Me.xAccountProvider.TabStop = False
    Me.xAccountProvider.Text = "xGender"
    '
    'chk_account_provider
    '
    Me.chk_account_provider.AutoSize = True
    Me.chk_account_provider.Location = New System.Drawing.Point(18, 43)
    Me.chk_account_provider.Name = "chk_account_provider"
    Me.chk_account_provider.Size = New System.Drawing.Size(101, 17)
    Me.chk_account_provider.TabIndex = 21
    Me.chk_account_provider.TabStop = True
    Me.chk_account_provider.Text = "RadioButton1"
    Me.chk_account_provider.UseVisualStyleBackColor = True
    '
    'frm_statistics_accounts
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(899, 498)
    Me.Name = "frm_statistics_accounts"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_statistics_accounts"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_gender.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.xAccountProvider.ResumeLayout(False)
    Me.xAccountProvider.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents chk_only_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents gb_gender As System.Windows.Forms.GroupBox
  Friend WithEvents chk_gender_female As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_male As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents xAccountProvider As System.Windows.Forms.GroupBox
  Friend WithEvents chk_account_provider As System.Windows.Forms.RadioButton
  Friend WithEvents chk_account As System.Windows.Forms.RadioButton
End Class
