'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_card_writer
'
' DESCRIPTION : Allows to record magnetic cards.
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-SEP-2008  RRT    Initial version.
' 11-MAY-2012  JCM    Fixed Bug #6: A read only GUI user can record magnetic cards.
' 28-AUG-2013  JCA    Implemented necessaries changes to generate excel file with the new library (SpreadSheetGear2012).
' 26-FEB-2013  JAB    Changes to save in different formats according program version installed.
' 01-DEC-2016  ETP    PBI 21172 Export two track data to player cards.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports SpreadsheetGear
Imports WSI.Common

#End Region

Public Class frm_card_writer
  Inherits frm_base_edit

#Region " External Prototypes "

  Private Declare Function Common_MCW_DeviceInit Lib "CommonMisc" (ByVal Model As Short) As Short

#End Region

#Region " Constants "

  Private Const MODULE_NAME As String = "frm_card_writer"
  Private Const DEFAULT_EXTENSION_EXCEL_2010 As String = "xlsx"
  Private Const DEFAULT_EXTENSION_EXCEL_2007 As String = "xls"

#End Region

#Region " Structures "

#End Region

#Region " Members "

  Dim m_frm_message As frm_message
  Dim m_export_msg_thread As System.Threading.Thread
  Dim WithEvents mcw_api As CLASS_MCW_API
  Dim models_data As New ArrayList
  Dim models_combo_filled As Boolean
  Dim models_read As Boolean
  Dim num_devices_read As Integer
  Dim excel_is_available As Boolean
  Dim card_device_is_available As Boolean

  ' Timer to check available data to fill writer model combo box.
  Dim timer_delegate As TimerCallback = AddressOf Me.FillCardWriterModels
  Dim fill_models_timer As System.Threading.Timer

#End Region

#Region " Delegates "

  Private Delegate Sub FillComboCallback(ByVal StateInfo As Object)
  Private Delegate Sub DelegatesNoParams()

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CARD_RECORD

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Text = GLB_NLS_GUI_CLASS_II.GetString(350)

    ' - Buttons
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    '   - Start
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CLASS_II.GetString(1)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = False
    '   - Exit
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CLASS_II.GetString(3)

    ' Site input data
    ef_site_id.Text = GLB_NLS_GUI_CLASS_II.GetString(351)
    ef_site_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 4, 0)
    ef_site_id.Value = ""

    ' Card writer model
    gb_card_writer.Text = " " & GLB_NLS_GUI_CLASS_II.GetString(352) & " "

    cmb_card_writer_model.Text = GLB_NLS_GUI_CLASS_II.GetString(353)
    cmb_card_writer_model.Clear()

    ' Card coercivity type
    gp_card_coercivity.Text = " " & GLB_NLS_GUI_CLASS_II.GetString(362) & " "
    gp_card_coercivity.Enabled = False

    rb_coercivity_low.Text = GLB_NLS_GUI_CLASS_II.GetString(363)
    rb_coercivity_high.Text = GLB_NLS_GUI_CLASS_II.GetString(364)
    rb_coercivity_high.Checked = True

    chk_internal_track.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7794)

    ef_export_num_cards.Text = GLB_NLS_GUI_CLASS_II.GetString(366)
    ef_export_num_cards.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 0)
    ef_export_num_cards.Value = ""
    ef_export_num_cards.Enabled = False

  End Sub 'GUI_InitControls


  ' PURPOSE: Define the control which focus is set to.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    ef_site_id.Focus()

  End Sub ' GUI_SetInitialFocus


  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ

      Case ENUM_DB_OPERATION.DB_OPERATION_READ
        ' Read Operations:

        ' Initialize variables
        models_read = False
        excel_is_available = False
        card_device_is_available = False

        ' Create mcw api class instance
        mcw_api = New CLASS_MCW_API()

        ' Fill cards writers models combo box - Start timer callback to fill mcw card models.
        fill_models_timer = New System.Threading.Timer(timer_delegate, Nothing, 400, 600)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Button operations
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      ' Start Card writing
      Case ENUM_BUTTON.BUTTON_OK
        If Me.Permissions.Write Then
          Call StartCardWriting()
        End If

      Case ENUM_BUTTON.BUTTON_CANCEL
        Call StopCardWriting()
        Me.Close()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    Call StopCardWriting()

  End Sub ' GUI_Closing

#End Region

#Region " Public Functions "

  ' PURPOSE: Constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    models_combo_filled = False

  End Sub

  ' PURPOSE: Form entry point from menu selection.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem

#End Region

#Region " Private Functions "

  ' PURPOSE : Gets site data. (Identifier, Name) from table GENERAL_PARAMS
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '         - SiteId: Side id.
  '         - SiteName: Site name.
  '
  ' RETURNS:
  '     - TRUE: Success
  '     - FALSE: Error

  Private Function Site_GetIdentifier(ByRef SiteId As Integer, ByRef SiteName As String) As Boolean

    Dim general_params As DataTable
    Dim site_data As CLASS_MCW_API.TYPE_SITE_DATA
    Dim sql_text As String
    Dim key_name As String
    Dim key_value As String
    Dim idx_row As Integer

    ' Initialization
    site_data.id = 0
    site_data.name = ""

    ' Build query
    sql_text = "select GP_SUBJECT_KEY, GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'Site'"

    ' Check query results
    general_params = GUI_GetTableUsingSQL(sql_text, 2)
    If IsNothing(general_params) Then
      Return False
    End If

    ' Check for at least two rows related to the site must exist:
    ' Site - Identifier
    ' Site - Name
    If general_params.Rows.Count() < 2 Then
      Return False
    End If

    For idx_row = 0 To general_params.Rows.Count - 1
      key_name = general_params.Rows.Item(idx_row).Item("GP_SUBJECT_KEY").ToString.ToUpper
      key_value = general_params.Rows.Item(idx_row).Item("GP_KEY_VALUE").ToString.Trim.ToUpper

      ' Check for empty values
      If key_value = "" Then
        Continue For
      End If

      If key_name = "IDENTIFIER" Then
        site_data.id = Convert.ToInt32(key_value)
      ElseIf key_name = "NAME" Then
        site_data.name = key_value
      End If
    Next

    ' Return values
    SiteId = site_data.id
    SiteName = site_data.name

    Return True

  End Function ' Site_GetIdentifier


  ' PURPOSE : Copy data writer models.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: Combo box filled with card writers models.
  '     - FALSE: Not data available
  ' NOTES:
  ' Procedure has a StateInfo to be defined as Timer Callback.
  Private Sub FillCardWriterModels(ByVal StateInfo As Object)

    Try
      If mcw_api.IsInitialized And Not models_combo_filled Then
        models_data = mcw_api.ModelsData

        models_combo_filled = True
        fill_models_timer.Dispose()

        If cmb_card_writer_model.InvokeRequired Then
          Call Me.Invoke(New DelegatesNoParams(AddressOf FillCardModelsCombo))
        End If
      End If

    Catch ex As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "CardWriter", _
                            "FillCardWriterModels", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    End Try

  End Sub ' FillCardWriterModels


  ' PURPOSE : Fills the combo box with card writer models description.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub FillCardModelsCombo()

    Dim mcw_model As CLASS_MCW_API.TYPE_MCW_MODEL_DATA
    Dim idx_model As Integer
    Dim excel_app As Object

    ' Variables initialization
    excel_app = Nothing

    cmb_card_writer_model.Clear()

    ' Card writer models: Insert found models.
    For idx_model = 0 To models_data.Count - 1
      mcw_model = models_data.Item(idx_model)
      cmb_card_writer_model.Add(mcw_model.id, mcw_model.name)
    Next
    num_devices_read = models_data.Count

    If num_devices_read > 0 Then
      gp_card_coercivity.Enabled = True
      card_device_is_available = True
    End If

    ' Export to Excel
    Try
      ' Check if Excel exists.
      excel_app = CreateObject("Excel.Application")
      If excel_app IsNot Nothing Then

        cmb_card_writer_model.Add(0, GLB_NLS_GUI_CLASS_II.GetString(365))
        excel_is_available = True

        ' Enable entry field is there no other device than excel.
        If num_devices_read = 0 Then
          ef_export_num_cards.Enabled = True
        End If

        num_devices_read = num_devices_read + 1
      End If

    Catch ex As Exception
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            MODULE_NAME, _
                            "FillCardModelsCombo", _
                            ex.Message)
    Finally

      If excel_app IsNot Nothing Then
        Marshal.ReleaseComObject(excel_app)
        excel_app = Nothing
      End If

      If num_devices_read > 0 Then
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
      End If

      ef_site_id.Focus()
      models_read = True
    End Try

  End Sub ' FillCardModelsCombo

  ' PURPOSE: Start card writing button actions:
  '           - General:
  '             - Check at least one driver has been selected.
  '             - Read current sequence for the site.
  '             - Check the type of driver selected: Card Record or Export to a file.
  '
  '             - Card Record: 
  '               - Initialize selected card writer model.
  '               - Get card writer model status.
  '               - Write card data.
  '
  '             - Export to a file:
  '               - Check number of cards to export.
  '               - Build file name.
  '               - Open file save dialog.
  '               - Create excel file with exported data.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub StartCardWriting()
    Dim _bool_rc As Boolean
    Dim _file_name_initial_seq As Int64
    Dim _num_cards As UInt32
    Dim _version As ExcelConversion.EXCEL_VERSION
    Dim _default_extension As String
    Dim _default_filter As String

    _num_cards = 0
    _default_extension = ""
    _default_filter = ""
    _version = ExcelConversion.EXCEL_VERSION.AFTER_2007

    ' Check there is a model selected.
    If cmb_card_writer_model.Count = 0 Then
      ' There are no models available
      Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(189), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Call cmb_card_writer_model.Focus()

      Exit Sub
    End If

    ' Check there is a model selected.
    If cmb_card_writer_model.SelectedIndex = -1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Call cmb_card_writer_model.Focus()

      Exit Sub
    End If

    ' Obtain site id. used to record cards.
    If ef_site_id.Value = "" Then

      ' "Please input the number of cards to be exported."
      Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(184), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Call ef_site_id.Focus()

      Exit Sub
    End If
    mcw_api.SiteId = Convert.ToUInt32(ef_site_id.Value)


    ' Read current sequence
    _bool_rc = mcw_api.Site_GetLastSequence(mcw_api.SiteId, mcw_api.SiteLastSequence)
    If _bool_rc = False Then
      Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(183), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    If cmb_card_writer_model.SelectedIndex = num_devices_read - 1 Then
      If excel_is_available Then

        ' Export to a file selected.
        '   - Check number of cards to export.
        '   - Build file name.
        '   - Open file save dialog.
        '   - Create excel file with exported data.
        Dim fd_cards_export As New System.Windows.Forms.SaveFileDialog

        ' Check is the number of cards are set
        If ef_export_num_cards.Value = "" Then

          ' "Please input the number of cards to be exported."
          Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(185), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Call ef_export_num_cards.Focus()

          Exit Sub
        End If

        If Convert.ToUInt32(ef_export_num_cards.Value) = 0 Then
          ' "The number of cards to be exported cannot be zero."
          Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(186), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Call ef_export_num_cards.Focus()

          Exit Sub
        End If

        _bool_rc = mcw_api.Site_GetLastSequence(mcw_api.SiteId, _file_name_initial_seq)
        If _bool_rc = False Then
          Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(183), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Exit Sub
        End If

        _num_cards = Convert.ToUInt32(ef_export_num_cards.Value)

        ' - Build file name.
        Try

          fd_cards_export = New System.Windows.Forms.SaveFileDialog()

          _version = ExcelConversion.ExcelVersion

          Select Case _version
            Case ExcelConversion.EXCEL_VERSION.AFTER_2007
              If _num_cards <= ExcelConversion.EXCEL_MAX_ROWS_BEFORE_2007 Then
                _default_extension = "." & DEFAULT_EXTENSION_EXCEL_2010
                _default_filter = "Excel 2010 Files|*.xlsx|Excel 2007 Files|*.xls"
              Else
                _default_extension = "." & DEFAULT_EXTENSION_EXCEL_2010
                _default_filter = "Excel 2010 Files|*.xlsx"
              End If
            Case ExcelConversion.EXCEL_VERSION.BEFORE_2007
              If _num_cards <= ExcelConversion.EXCEL_MAX_ROWS_BEFORE_2007 Then
                _default_extension = "." & DEFAULT_EXTENSION_EXCEL_2007
                _default_filter = "Excel 2007 Files|*.xls"
              Else
                _default_extension = "." & DEFAULT_EXTENSION_EXCEL_2010
                _default_filter = "Excel 2010 Files|*.xlsx"
              End If
          End Select

          With fd_cards_export
            .Title = GLB_NLS_GUI_CLASS_II.GetString(367)
            .FileName = BuildCardsExportFileName(_file_name_initial_seq, _num_cards)
            .DefaultExt = _default_extension
            .Filter = _default_filter
            .InitialDirectory = "C:\"
          End With

          If fd_cards_export.ShowDialog() = System.Windows.Forms.DialogResult.Cancel Then
            ' Operation canceled
            Exit Sub
          Else
            ' Feed the dialog
            Call Application.DoEvents()
            ' Start exporting process
            BuildCardsExportFile(fd_cards_export.FileName, _num_cards)

            ef_export_num_cards.Value = ""
          End If

        Catch ex As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)
          Call Trace.WriteLine(ex.ToString())
          Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                MODULE_NAME, _
                                "StartCardWriting", _
                                ex.Message)
        Finally

        End Try

      End If
    Else
      If card_device_is_available Then

        ' Model driver selected:
        '   - Initialize selected card writer model.
        '   - Get card writer model status.
        '   - Write card data.

        Dim mcw_model As CLASS_MCW_API.TYPE_MCW_MODEL_DATA

        ' Do a device init for the selected device
        mcw_model = models_data.Item(cmb_card_writer_model.SelectedIndex)
        Dim model_id As Short
        model_id = mcw_model.id
        If Common_MCW_DeviceInit(model_id) = False Then

          Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(191), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Call cmb_card_writer_model.Focus()

          Exit Sub
        End If

        If rb_coercivity_low.Checked = True Then
          mcw_api.CardCoercivity = CLASS_MCW_API.MCW_CARD_LO_CO
        Else
          mcw_api.CardCoercivity = CLASS_MCW_API.MCW_CARD_HI_CO
        End If

        mcw_api.WriteInternalTrackDataToTrack2 = chk_internal_track.Checked

        mcw_api.DeviceInitialized = True
        mcw_api.Thread_EnableWrite()

        ' Show swipe a card dialog
        Dim frm_card_record As New frm_card_writer_record(mcw_api)

        frm_card_record.Display(True)
      End If
    End If

  End Sub ' StartCardWriting

  ' PURPOSE: Stop card writing.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub StopCardWriting()

    mcw_api.Thread_StopRequests()
    mcw_api.Thread_Exit()

  End Sub ' StopCardWriting

#End Region

  ' PURPOSE: Called when a combo value is changed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub cmb_card_writer_model_ValueChangedEvent() Handles cmb_card_writer_model.ValueChangedEvent

    If models_read Then
      ' Check there is a model selected.
      If cmb_card_writer_model.SelectedIndex = -1 Then
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Call cmb_card_writer_model.Focus()

        Exit Sub
      Else
        If cmb_card_writer_model.SelectedIndex = num_devices_read - 1 Then
          If excel_is_available Then
            ' Export to a file selected.
            gp_card_coercivity.Enabled = False
            ef_export_num_cards.Enabled = True
          End If
        Else
          ' Model driver selected.
          gp_card_coercivity.Enabled = True
          ef_export_num_cards.Enabled = False
        End If
      End If
    End If

  End Sub ' cmb_card_writer_model_ValueChangedEvent

  ' PURPOSE: Create an excel file with the especified number of cards to be exported.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub StartExportWaitMessageThread()

    ' Create a thread to display the card exporting message
    m_export_msg_thread = New Threading.Thread(AddressOf Me.RunExportMessage)
    m_export_msg_thread.Name = "Card Export Message Thread"
    m_export_msg_thread.Start()

  End Sub ' StartExportMessageThread

  ' PURPOSE: Thread to diplay exporting cards message
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RunExportMessage()

    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

    m_frm_message = New frm_message(GLB_NLS_GUI_CLASS_II.GetString(371), GLB_NLS_GUI_CLASS_II.GetString(372), True)
    ' RCI & JCM 11-MAY-2012: Don't use Display(), use ShowDialog().
    '                        frm_base.Display() uses permissions and in this thread the permissions are not set.
    'm_frm_message.Display(True)
    m_frm_message.ShowDialog()

  End Sub ' RunExportMessage


  ' PURPOSE: Close the card exporting message
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub StopExportWaitMessageThread()

    If m_frm_message.InvokeRequired Then
      Call Me.Invoke(New DelegatesNoParams(AddressOf CloseCardExportWaitMessage))
    End If

  End Sub ' StopExportWaitMessageThread

  ' PURPOSE: Close the card exporting message
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CloseCardExportWaitMessage()

    m_frm_message.Close()

    System.Windows.Forms.Cursor.Current = Cursors.Default
  End Sub ' CloseCardExportWaitMessage


  ' PURPOSE: Create an excel file with the especified number of cards to be exported.
  '
  '  PARAMS:
  '     - INPUT:
  '       - FileToExport: Name of excel used to export cards.
  '       - NumCards: Number of cards to export.
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub BuildCardsExportFile(ByVal FileToExport As String, ByVal NumCards As UInt32)

    Dim _auditor_data As CLASS_AUDITOR_DATA

    Dim _workbook As IWorkbook
    Dim _sheet As IWorksheet
    Dim _range As IRange

    Dim _last_sequence As Int64
    Dim _card_idx As Integer
    Dim _bool_rc As Boolean
    Dim _card_string As String
    Dim _internal_card_id As String
    Dim _export_result As Boolean
    Dim _rc As Integer
    Dim _current_culture As System.Globalization.CultureInfo

    ' Variables initialization
    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_CARD_WRITER)
    _workbook = Nothing
    _sheet = Nothing
    _range = Nothing
    _export_result = True
    _current_culture = Nothing

    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = False
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = False

    ' Display a message for exporting
    StartExportWaitMessageThread()

    ' Feed the dialog
    Call Application.DoEvents()

    Try

      _current_culture = System.Threading.Thread.CurrentThread.CurrentCulture
      System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")

      _workbook = Factory.GetWorkbook()
      If _workbook Is Nothing Then
        _export_result = False
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Exit Sub
      End If

      _sheet = _workbook.Worksheets("Sheet1")
      If _sheet Is Nothing Then
        _export_result = False
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Exit Sub
      End If

      ' Set sheet format
      _sheet.Cells.ColumnWidth = 21
      _sheet.Cells.NumberFormat = "@"

      _bool_rc = mcw_api.Site_GetLastSequence(mcw_api.SiteId, _last_sequence)
      If _bool_rc = False Then
        _export_result = False
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(183), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Exit Sub
      End If

      For _card_idx = 1 To NumCards
        _card_string = ""
        _last_sequence = _last_sequence + 1
        _internal_card_id = WSI.Common.CardNumber.MakeInternalTrackData(mcw_api.SiteId, _last_sequence)

        _bool_rc = WSI.Common.CardNumber.TrackDataToExternal(_card_string, _internal_card_id, MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER)
        If _bool_rc = False Then
          _export_result = False
          Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(198), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Exit Sub
        End If

        _sheet.Range("A" + _card_idx.ToString()).Value = _card_string
        If chk_internal_track.Checked Then
          _sheet.Range("B" + _card_idx.ToString()).Value = _internal_card_id
        End If
      Next

      ' Save and close file
      Try
        If FileToExport.Contains(DEFAULT_EXTENSION_EXCEL_2010) Then
          _workbook.SaveAs(FileToExport, FileFormat.OpenXMLWorkbook)
        Else
          _workbook.SaveAs(FileToExport, FileFormat.Excel8)
        End If

      Finally
        _workbook.Close()
      End Try

      ' Update sequence
      _rc = mcw_api.Site_SetLastSequence(mcw_api.SiteId, _last_sequence)
      If _rc = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB Then
        _export_result = False
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(188), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

      ' Audit data
      '   - Session of cards exported
      _auditor_data.SetName(GLB_NLS_GUI_CLASS_II.Id(368), GLB_NLS_GUI_CLASS_II.GetString(368))
      '     - Site
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(369), GUI_FormatNumber(mcw_api.SiteId, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE, ), 0)
      '     - Number of cards exported
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(370), GUI_FormatNumber(NumCards, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE, ), 0)

      If Not _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                                 GLB_CurrentUser.Id, _
                                 GLB_CurrentUser.Name, _
                                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                 0) Then
      End If


    Catch ex As Exception

      _export_result = False
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            MODULE_NAME, _
                            "BuildCardsExportFile", _
                            ex.Message)

    Finally

      If _sheet IsNot Nothing Then
        _sheet = Nothing
      End If

      If _workbook IsNot Nothing Then
        _workbook = Nothing
      End If

      ' Set the garbage collector to clean RCW COM objects in memory
      GC.Collect()

      System.Threading.Thread.CurrentThread.CurrentCulture = _current_culture

      ' connection.Close()

      StopExportWaitMessageThread()

      If _export_result = True Then
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(196), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_CLASS_II.Id(197), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

      ef_export_num_cards.Value = ""

      GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
      GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    End Try

  End Sub ' BuildCardsExportFile

  ' PURPOSE: Build the file name of the card export file
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Function BuildCardsExportFileName(ByVal InitialSequence As Int64, ByVal NumCards As UInt32) As String
    Dim file_initial_sequence As Int64
    Dim file_last_sequence As Int64
    Dim str_date_time As String
    Const FORMAT_DATE_TIME As String = "yyyy/MM/dd"

    str_date_time = Microsoft.VisualBasic.Strings.Format(GUI_GetDateTime, FORMAT_DATE_TIME) & _
                    "_" & GUI_FormatTime(GUI_GetDateTime, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Increment by one over the current sequence.
    file_initial_sequence = InitialSequence + 1
    file_last_sequence = InitialSequence + NumCards

    str_date_time = Replace(str_date_time, "/", "")
    str_date_time = Replace(str_date_time, "-", "")
    str_date_time = Replace(str_date_time, ":", "")

    Return "EC_Site_" + Microsoft.VisualBasic.Strings.Format(mcw_api.SiteId, "0000") + "_" + str_date_time + _
           "_NC_" + Microsoft.VisualBasic.Strings.Format(NumCards, "00000") + "_" + file_initial_sequence.ToString() + "_" + file_last_sequence.ToString()

  End Function ' BuildCardsExportFileName

End Class