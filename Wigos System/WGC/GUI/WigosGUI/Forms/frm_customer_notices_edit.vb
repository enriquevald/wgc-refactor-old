﻿'-------------------------------------------------------------------
' Copyright © 2007-2017 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_customer_notices_edit
' DESCRIPTION:   Customer Notices Edition.
' AUTHOR:        Fernando Jiménez Cepeda
' CREATION DATE: 04-SEP-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-SEP-2017  FJC    Initial version.
' 20-SEP-2017  XGJ    WIGOS-5162: Customer notices - GUI - Update "Customer notices edit" form
' 28-MAR-2018  DPC    Bug 32096:[WIGOS-9547]: Customer notices - NEW button has incorrect behaviour with disabled notices
' 28-MAR-2018  DPC    Bug 32097:[WIGOS-9564]: Customer notices - Disabled notices has the field title enabled
' 28-MAR-2018  FOS    Bug 32107:[WIGOS-9551]: Customer notices - Button "Historic" shouldn't appear when is new notice
' 29-MAR-2018  JGC    Bug 32108:[WIGOS-9595]: Customer notices - customer selection shows anonymous customer
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports WSI.Common.CustomerNotices
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_CommonOperations.CLASS_BASE
Imports WigosGUI.CLASS_CUSTOMER_NOTICES

Public Class frm_customer_notices_edit
  Inherits frm_base_edit

#Region " Const "
  Private Const FORM_DB_MIN_VERSION As Short = 224 ' TODO: Revisar que la base de datos sea la correcta.
#End Region ' Const

#Region " Members "
  Private m_current_customer_notices As CLASS_CUSTOMER_NOTICES
  Private m_priorities As Dictionary(Of Int64, TYPE_CUSTOMER_NOTICES_PRIORITY)
  Private m_account_id As Int64
  Private m_has_history As Boolean
#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Sets form Id 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CUSTOMER_NOTICES_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8634)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)                   ' Aceptar
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(4)              ' Cancelar

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8641)   ' Desactivar
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    If m_current_customer_notices.CustomerNoticeScreenMode = ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_NEW Then

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    End If

    'Player Data
    Me.gb_player_info.Text = String.Empty
    Me.ef_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1910)    '1910 "Account"
    Me.ef_document.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1565)   '1565 "Document"
    Me.ef_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723)     '1723 "Gender"
    Me.ef_age.Text = GLB_NLS_GUI_INVOICING.GetString(415)               '415 "Age"
    Me.ef_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1725) '1725 "Date of Birth"
    Me.ef_account.TextValue = String.Empty
    Me.ef_document.TextValue = String.Empty
    Me.ef_gender.TextValue = String.Empty
    Me.ef_age.TextValue = String.Empty
    Me.ef_birth_date.TextValue = String.Empty
    Me.lbl_blocked.Text = String.Empty
    ' Player Data -button selection player-
    If m_current_customer_notices.CustomerNoticeScreenMode = ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_EDIT OrElse
       m_current_customer_notices.CustomerNoticeAccountId > 0 Then

      Me.bt_open_account_summary.Enabled = False
    End If

    ' GB Creacion
    Me.gb_creation_notice.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8635)
    Me.ef_date_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6854)        ' Fecha 
    Me.ef_user_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786)        ' User

    ' GB Status
    Me.gb_status_notice.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8636)
    Me.uc_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8636)               ' Status 
    Me.ef_date_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8642)          ' Fecha 
    Me.ef_user_updated_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8643)  ' User
    Me.ef_user_updated_status.Value = String.Empty
    Me.ef_date_status.Value = String.Empty

    ' GB Priority

    Me.gb_priority.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8630)
    Me.gb_priority_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8637)
    Me.uc_priority.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8630)

    Dim _options As CustomerNotices.CustomerNoticesOptionsDict
    _options = New CustomerNotices.CustomerNoticesOptionsDict()
    _options.SetOption(CustomerNoticesPriorityOptions.ShowInCashier, False)
    _options.SetOption(CustomerNoticesPriorityOptions.ShowInReception, False)
    _options.SetOption(CustomerNoticesPriorityOptions.ShowInGamingTables, False)
    mdl_custom_notices.FillGridOptions(dg_options, _options, False)

    'GB Text 
    Me.gb_notice_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8631)

    'GB Title name

    Me.gb_title_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8725)
    Me.Uc_title_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8725)
    Me.Uc_title_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 25)

    ' It looks like redundant code, but it's necessary.
    Me.ef_account.IsReadOnly = True
    Me.ef_document.IsReadOnly = True
    Me.ef_gender.IsReadOnly = True
    Me.ef_age.IsReadOnly = True
    Me.ef_birth_date.IsReadOnly = True
    Me.ef_user_creation.IsReadOnly = True
    Me.ef_user_updated_status.IsReadOnly = True
    Me.ef_date_creation.IsReadOnly = True
    Me.ef_date_status.IsReadOnly = True
    Me.uc_status.IsReadOnly = True

    'Load Prorities 
    LoadComboPriorities()

    'Load Status-es 
    LoadComboStatus()

    'Refresh Color Status
    RefreshStatusColor()

    'Refresh Priorities visibility
    RefreshPriorityVisibility()

    ' XGJ 20-SEP-2017
    m_has_history = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(466)   ' See notice history
  End Sub ' GUI_InitControls

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.uc_priority.Focus()
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _customer_notices As CLASS_CUSTOMER_NOTICES
    Dim ctx As Integer

    If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_CUSTOMER_NOTICES
        _customer_notices = DbEditedObject
        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
          m_current_customer_notices.CustomerNoticeCreationUserId = GLB_CurrentUser.Id
          m_current_customer_notices.CustomerNoticeLastUpdateUserId = Nothing
          m_current_customer_notices.CustomerNoticeCreationDate = WGDB.Now
          m_current_customer_notices.CustomerNoticeLastUpdateDate = Nothing
          m_current_customer_notices.CustomerNoticeStatus = CLASS_CUSTOMER_NOTICES.ENUM_CUSTOMER_NOTICES_STATUS.ACTIVE
          ''
          _customer_notices.CustomerNoticeId = m_current_customer_notices.CustomerNoticeId
          _customer_notices.CustomerNoticeAccountId = m_current_customer_notices.CustomerNoticeAccountId
          _customer_notices.CustomerNoticeCreationUserId = m_current_customer_notices.CustomerNoticeCreationUserId
          _customer_notices.CustomerNoticeCreationDate = m_current_customer_notices.CustomerNoticeCreationDate
          _customer_notices.CustomerNoticeStatus = m_current_customer_notices.CustomerNoticeStatus
          _customer_notices.CustomerNoticeLastUpdateUserId = m_current_customer_notices.CustomerNoticeLastUpdateUserId
          _customer_notices.CustomerNoticeLastUpdateDate = m_current_customer_notices.CustomerNoticeLastUpdateDate
          _customer_notices.CustomerNoticePriorityId = m_current_customer_notices.CustomerNoticePriorityId
        End If

        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            _customer_notices.CustomerNoticeScreenMode = CLASS_CUSTOMER_NOTICES.ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            _customer_notices.CustomerNoticeScreenMode = CLASS_CUSTOMER_NOTICES.ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_EDIT
        End Select

        DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ

        If Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND Then
          Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
          Me.DbStatus = ENUM_STATUS.STATUS_OK
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        DbStatus = DbEditedObject.DB_Insert(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        DbStatus = DbEditedObject.DB_Update(ctx)

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select


  End Sub ' GUI_DB_Operation

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _customer_notices As CLASS_CUSTOMER_NOTICES
    Dim _gui_user_name As String

    _gui_user_name = String.Empty

    _customer_notices = DbReadObject

    m_current_customer_notices = _customer_notices
    If _customer_notices.CustomerNoticeScreenMode = ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_NEW Then
      Me.ef_date_creation.Value = _customer_notices.CustomerNoticeCreationDate.ToShortDateString()
    Else
      Me.ef_date_creation.Value = _customer_notices.CustomerNoticeCreationDate
    End If

    WSI.Common.Users.GetActivityUser(_customer_notices.CustomerNoticeCreationUserId, False, _gui_user_name, New Date(), String.Empty, New Date, New TimeSpan, String.Empty, 0)
    Me.ef_user_creation.Value = _gui_user_name

    ' Status
    Me.uc_status.Value = _customer_notices.CustomerNoticeStatus
    RefreshStatusColor()

    ' User Updated Status
    If _customer_notices.CustomerNoticeLastUpdateUserId.HasValue Then
      WSI.Common.Users.GetActivityUser(_customer_notices.CustomerNoticeLastUpdateUserId, False, _gui_user_name, New Date(), String.Empty, New Date, New TimeSpan, String.Empty, 0)
      Me.ef_user_updated_status.Value = _gui_user_name
    End If

    ' Date Updated Status
    If (_customer_notices.CustomerNoticeLastUpdateDate.HasValue) Then
      Me.ef_date_status.Value = _customer_notices.CustomerNoticeLastUpdateDate
    End If

    ' Priority
    Me.uc_priority.Value = _customer_notices.CustomerNoticePriorityId
    _customer_notices.CustomerNoticePriorityName = Me.uc_priority.TextValue

    ' Notice Text
    Me.ef_notice_text.Text = _customer_notices.CustomerNoticeText

    ' Notice title name
    Me.Uc_title_name.Value = _customer_notices.CustomerNoticeTitleName

    ' Other 
    If _customer_notices.CustomerNoticeStatus = ENUM_CUSTOMER_NOTICES_STATUS.ACTIVE AndAlso
       _customer_notices.CustomerNoticeScreenMode = ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_EDIT AndAlso
       Me.Permissions.Write Then

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
    End If

    If _customer_notices.CustomerNoticeStatus = ENUM_CUSTOMER_NOTICES_STATUS.DESACTIVE Then
      Me.uc_priority.Enabled = False
      Me.ef_notice_text.Enabled = False
      Me.Uc_title_name.Enabled = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False
    End If

    'Get Data from Account
    If _customer_notices.CustomerNoticeAccountId > 0 Then
      Me.GetAccountData(_customer_notices.CustomerNoticeAccountId)
    End If

    If _customer_notices.CustomerNoticeId <> -1 AndAlso m_current_customer_notices.CustomerNoticeScreenMode = ENUM_CUSTOMER_NOTICES_SCREEN_MODE.MODE_EDIT Then
      HasHistory(_customer_notices.CustomerNoticeId)
    End If

    If m_has_history Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True

    Else
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
      'Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8697)  ' See notice history
    End If

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Get data from screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()
    Dim m_customer_notices_edit As CLASS_CUSTOMER_NOTICES
    Dim m_customer_notices_read As CLASS_CUSTOMER_NOTICES
    Dim _result_user_id As Int32

    m_customer_notices_edit = DbEditedObject
    m_customer_notices_read = DbReadObject

    m_customer_notices_edit.CustomerNoticeAccountId = m_current_customer_notices.CustomerNoticeAccountId
    m_customer_notices_edit.CustomerNoticeCreationUserId = m_current_customer_notices.CustomerNoticeCreationUserId
    m_customer_notices_edit.CustomerNoticeStatus = uc_status.Value
    If Int32.TryParse(Me.ef_user_updated_status.Tag, _result_user_id) Then
      m_customer_notices_edit.CustomerNoticeLastUpdateUserId = _result_user_id
    End If
    If Not String.IsNullOrEmpty(Me.ef_date_status.Value) Then
      m_customer_notices_edit.CustomerNoticeLastUpdateDate = Me.ef_date_status.Value
    End If
    m_customer_notices_edit.CustomerNoticePriorityId = Me.uc_priority.Value
    m_customer_notices_edit.CustomerNoticePriorityName = Me.uc_priority.TextValue
    m_customer_notices_edit.CustomerNoticeText = Me.ef_notice_text.Text
    m_customer_notices_edit.CustomerNoticeTitleName = Me.Uc_title_name.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)
    Dim frm_history As frm_customer_notices_history

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK        ' Aceptar cambios
        MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_CUSTOM_0  ' Activate / Deactivate
        If DeActivateCustomerNotice() Then
          Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        frm_history = New frm_customer_notices_history
        frm_history.m_selected_id = Me.m_current_customer_notices.CustomerNoticeId
        frm_history.ShowDialog()
        Windows.Forms.Cursor.Current = Cursors.Default

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: To control the key pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    ' The keypress event is done in uc_grid control
    ''Return MyBase.GUI_KeyPress(sender, e)
    If Me.ef_notice_text.ContainsFocus Then

      Return True
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  ' PURPOSE: To determine if screen data are ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    If m_current_customer_notices.CustomerNoticeAccountId <= 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202))

      Return False
    End If

    If Me.uc_priority.Value <= 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8665), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202))

      Return False
    End If

    If (String.IsNullOrEmpty(Me.ef_notice_text.Text.Trim())) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8670), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202))

      Return False
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Privates "

  ''' <summary>
  ''' Fill Acount Player Data
  ''' </summary>
  ''' <param name="AccountId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetAccountData(ByVal AccountId As Long) As Boolean
    Dim _card_data As WSI.Common.CardData

    _card_data = New WSI.Common.CardData

    Try
      Using _db_trx As New DB_TRX
        If WSI.Common.CardData.DB_CardGetPersonalData(AccountId, _card_data, _db_trx.SqlTransaction) Then
          m_current_customer_notices.CustomerNoticeAccountId = AccountId
          Me.ef_account.Value = AccountId & " - " & _card_data.PlayerTracking.HolderName
          Me.ef_document.Value = _card_data.PlayerTracking.HolderId
          Me.ef_birth_date.Value = _card_data.PlayerTracking.HolderBirthDateText
          Me.ef_age.Value = WSI.Common.Misc.CalculateAge(_card_data.PlayerTracking.HolderBirthDate)
          Me.ef_gender.Value = WSI.Common.CardData.GenderString(_card_data.PlayerTracking.HolderGender)

          If _card_data.Blocked Then
            Me.lbl_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(238) ' "Locked"
          Else
            Me.lbl_blocked.Text = ""
          End If

          Return True
        End If
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function

  ''' <summary>
  ''' Load Combo of Status
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadComboPriorities()
    m_priorities = m_current_customer_notices.DB_GetPrioritiesDictionary()

    Me.uc_priority.Clear()

    For Each _key_par_values As KeyValuePair(Of Int64, CLASS_CUSTOMER_NOTICES.TYPE_CUSTOMER_NOTICES_PRIORITY) In m_priorities
      Me.uc_priority.Add(Convert.ToInt32(_key_par_values.Key), Convert.ToInt32(_key_par_values.Value.customer_notice_priority_order) & " - " & _key_par_values.Value.customer_notice_priority_name)
    Next
  End Sub

  ''' <summary>
  ''' Load combo with Status
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadComboStatus()
    Me.uc_status.Clear()

    Me.uc_status.Add(CLASS_CUSTOMER_NOTICES.ENUM_CUSTOMER_NOTICES_STATUS.ACTIVE,
                     m_current_customer_notices.DB_GetCustomerNoticesStatusDescription(ENUM_CUSTOMER_NOTICES_STATUS.ACTIVE))

    Me.uc_status.Add(CLASS_CUSTOMER_NOTICES.ENUM_CUSTOMER_NOTICES_STATUS.DESACTIVE,
                     m_current_customer_notices.DB_GetCustomerNoticesStatusDescription(ENUM_CUSTOMER_NOTICES_STATUS.DESACTIVE))

    Me.uc_status.SelectedIndex = 1
  End Sub

  ''' <summary>
  ''' Disbled a NOtice. Pass to Deactivate
  ''' </summary>
  ''' <remarks></remarks>
  Private Function DeActivateCustomerNotice()
    Dim _gui_user_name As String
    Dim _customer_notices As CLASS_CUSTOMER_NOTICES
    Dim _comments As frm_comments

    _gui_user_name = String.Empty
    _customer_notices = Me.DbEditedObject

    _comments = New frm_comments(NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(495)), _
                                 NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(496)), _
                                 NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(1)), _
                                 NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(2)), _
                                 False) ' Comments

    Try
      If _comments.ShowGPDialog() = Windows.Forms.DialogResult.OK Then

        _customer_notices.CustomerNoticeComments = _comments.Comments
        'm_current_customer_notices.CustomerNoticeComments = _comments.Comments
        If String.IsNullOrEmpty(_customer_notices.CustomerNoticeComments) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          Return False
        End If
      Else
        _customer_notices.CustomerNoticeComments = String.Empty

        Return False
      End If

      Me.uc_status.Value = CLASS_CUSTOMER_NOTICES.ENUM_CUSTOMER_NOTICES_STATUS.DESACTIVE
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      If Me.ef_notice_text.Text.Length > 0 Then
        Me.ef_notice_text.Enabled = False
      End If
      Me.ef_date_status.Value = WGDB.Now.ToShortDateString()
      Me.ef_user_updated_status.Tag = GLB_CurrentUser.Id
      WSI.Common.Users.GetActivityUser(GLB_CurrentUser.Id, False, _gui_user_name, New Date(), String.Empty, New Date, New TimeSpan, String.Empty, 0)
      Me.ef_user_updated_status.Value = _gui_user_name
      Me.uc_priority.Enabled = False
      RefreshStatusColor()
    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    Finally
      _comments.Dispose()
    End Try

    Return True
  End Function

  ''' <summary>
  ''' Refresh the color of Status
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RefreshStatusColor()
    If Me.uc_status.Value = ENUM_CUSTOMER_NOTICES_STATUS.ACTIVE Then
      Me.pb_status.BackColor = Color.Green
    Else
      Me.pb_status.BackColor = Color.Red
    End If
  End Sub
#End Region ' Private Functions

#Region " Public Functions "

  ''' <summary>
  ''' Point of entry for Customer Notice (By CustomerNOticeId)
  ''' </summary>
  ''' <param name="CustomerNoticesId"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowNewEditItemNotice(ByVal CustomerNoticesId As Long, ByVal IsNewCustomerNotice As Boolean)
    ShowNewEditItem(CustomerNoticesId, -1, IsNewCustomerNotice)
  End Sub ' ShowNewEditItemNotice

  ''' <summary>
  ''' Point of entry for Customer Notice (By AccountId)
  ''' </summary>
  ''' <param name="AccountId"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowNewEditItemAccount(Optional ByVal AccountId As Long = -1)
    ShowNewEditItem(-1, AccountId, False)
  End Sub ' ShowNewEditItemAccount

#End Region ' Public Functions

#Region " Private Methods"

  ''' <summary>
  ''' Point of Entry
  ''' </summary>
  ''' <param name="CustomerNoticesId"></param>
  ''' <param name="AccountId"></param>
  ''' <remarks></remarks>
  Private Overloads Sub ShowNewEditItem(ByVal CustomerNoticesId As Long, ByVal AccountId As Long, ByVal IsNewCustomerNotice As Boolean)
    DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

    Me.m_current_customer_notices = New CLASS_CUSTOMER_NOTICES
    Me.m_current_customer_notices.CustomerNoticeId = CustomerNoticesId
    Me.m_current_customer_notices.CustomerNoticeAccountId = AccountId

    If (Me.m_current_customer_notices.CustomerNoticeId) <> -1 AndAlso IsNewCustomerNotice Then

      ' Sets the screen mode
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

      Me.m_current_customer_notices.CustomerNoticeId = CustomerNoticesId

      DbObjectId = Nothing
      DbStatus = ENUM_STATUS.STATUS_OK

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
      If CustomerNoticesId > 0 Then
        Me.m_current_customer_notices = DbEditedObject

        DbObjectId = CustomerNoticesId
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
        DbObjectId = Nothing

      End If
      Me.m_current_customer_notices.CustomerNoticeCreationDate = WGDB.Now
      Me.m_current_customer_notices.CustomerNoticeCreationUserId = GLB_CurrentUser.Id
      Me.m_current_customer_notices.CustomerNoticeCopiedFromAnother = True
      Me.m_current_customer_notices.CustomerNoticeStatus = ENUM_CUSTOMER_NOTICES_STATUS.ACTIVE
    Else
      If (m_current_customer_notices.CustomerNoticeId) <> -1 Then
        DbObjectId = m_current_customer_notices.CustomerNoticeId
        Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
      Else
        DbObjectId = -1
        Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
      End If

      Me.m_current_customer_notices.CustomerNoticeScreenMode = Me.ScreenMode
      Me.MdiParent = MdiParent

      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    End If

    If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    Else
      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowNewEditItem

  ''' <summary>
  ''' Refresh Icon with priority visibility
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RefreshPriorityVisibility()
    Dim _priority As TYPE_CUSTOMER_NOTICES_PRIORITY
    Dim _options As CustomerNotices.CustomerNoticesOptionsDict

    Try
      _priority = New TYPE_CUSTOMER_NOTICES_PRIORITY
      dg_options.Clear()

      If m_priorities.TryGetValue(uc_priority.Value, _priority) Then

        _options = New CustomerNotices.CustomerNoticesOptionsDict()
        _options.SetOption(CustomerNoticesPriorityOptions.ShowInCashier, _priority.customer_notice_show_in_cashier)
        _options.SetOption(CustomerNoticesPriorityOptions.ShowInReception, _priority.customer_notice_show_in_reception)
        _options.SetOption(CustomerNoticesPriorityOptions.ShowInGamingTables, _priority.customer_notice_show_in_gaming_tables)
        mdl_custom_notices.FillGridOptions(dg_options, _options, False)
      End If
    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

  End Sub


  Private Sub HasHistory(ByVal NoticeId As Integer)
    Dim _str_sql As System.Text.StringBuilder
    Dim _data As Object

    Try
      Using _db_trx As New WSI.Common.DB_TRX()
        _str_sql = New StringBuilder()

        _str_sql.AppendLine("SELECT COUNT(*)                  ")
        _str_sql.AppendLine("FROM CUSTOMER_NOTICES_HISTORY    ")
        _str_sql.AppendLine("WHERE CNH_NOTICE_ID = @NoticeId  ")

        Using _sql_cmd As New SqlClient.SqlCommand(_str_sql.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@NoticeId", SqlDbType.Int).Value = NoticeId
          _data = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_data) AndAlso Convert.ToInt16(_data) <> 0 Then
            m_has_history = True
          Else
            m_has_history = False
          End If

        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub
#End Region

#Region " Events "
  ''' <summary>
  ''' Select an Account
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub bt_open_account_summary_ClickEvent() Handles bt_open_account_summary.ClickEvent
    Dim _frm As frm_account_summary
    Dim _account As cls_account

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_summary()

    _frm.is_selection_mode = True
    _frm.hide_anonymous = True
    _frm.ShowDialog(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

    _account = New cls_account

    _account.id_account = _frm.account.id_account

    If Not _account.id_account Is Nothing Then
      GetAccountData(_frm.account.id_account)
    End If
  End Sub

  ''' <summary>
  ''' Refresh PriorityVisibility
  ''' </summary>
  ''' <remarks></remarks>
  ''' 
  Private Sub uc_priority_ValueChangedEvent() Handles uc_priority.ValueChangedEvent
    RefreshPriorityVisibility()
  End Sub
#End Region ' Events  

  Private Sub uc_priority_Click(sender As Object, e As EventArgs) Handles uc_priority.Click

  End Sub
End Class