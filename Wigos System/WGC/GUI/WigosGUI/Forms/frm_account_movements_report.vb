'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_movements_report
' DESCRIPTION:   Information various from the movements of account and session playing 
' AUTHOR:        Javier Molina
' CREATION DATE: 17-JAN-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-JAN-2012  JMM    Initial version.
' 24-JAN-2012  RCI    Export to Excel: Put all tables in DataSet in a unique file.
'                     Put each DataTable in a different Excel Sheet.
' 31-JAN-2012  JMM    Protect against possible read-only & file exclusive sharing errors
' 15-FEB-2012  JMM    Remove DocumentID column from Movimiento Datatable
' 07-MAR-2013  RXM     Updated calls to ExcelConversion functions. 
'                     Site info, generated and generated by values are sent as param.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.IO

Public Class frm_account_movements_report
  Inherits frm_base

#Region "Constants"
  Private Const OUTPUT_FOLDER As String = "Reports"
#End Region

#Region " Enums "

  Public Enum ENUM_REPORT_TYPE

    TYPE_XML = 0
    TYPE_EXCEL = 1

  End Enum

#End Region ' Enums

#Region " Members "
#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_ACCOUNT_MOVEMENTS_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(475)

    ' - Buttons
    Me.btn_generate_xml.Text = GLB_NLS_GUI_STATISTICS.GetString(474)
    Me.btn_generate_excel.Text = GLB_NLS_GUI_STATISTICS.GetString(478)
    Me.btn_exit.Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' - Dates
    Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(204)
    Me.dt_date_from.Text = GLB_NLS_GUI_STATISTICS.GetString(309)
    Me.dt_date_to.Text = GLB_NLS_GUI_STATISTICS.GetString(310)
    Me.dt_date_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dt_date_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' - Get the last ended day
    Me.dt_date_from.Value = WSI.Common.Misc.TodayOpening()
    Me.dt_date_from.Value = Me.dt_date_from.Value.AddDays(-1)

    ' - Hours and minutes will be discarted to look for a 12.00am
    'Me.dt_date_from.Value = Me.dt_date_from.Value.AddMinutes(-Me.dt_date_from.Value.Minute)
    'Me.dt_date_from.Value = Me.dt_date_from.Value.AddHours(-Me.dt_date_from.Value.Hour)

    ' - DateTo will be the last day of the last ended month
    Call CalculateDateTo(dt_date_from.Value)

    Me.lbl_filename.Value = ""
    Me.lbl_num_records.Value = ""

  End Sub ' GUI_InitControls

#End Region

#Region " Events "
  ' PURPOSE: Close form.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent

  Private Sub btn_generate_xml_ClickEvent() Handles btn_generate_xml.ClickEvent

    If CheckDates() Then
      Call GenerateReportFile(ENUM_REPORT_TYPE.TYPE_XML)
    End If

  End Sub

  Private Sub btn_generate_excel_ClickEvent() Handles btn_generate_excel.ClickEvent

    If CheckDates() Then
      Call GenerateReportFile(ENUM_REPORT_TYPE.TYPE_EXCEL)
    End If

  End Sub

  Private Sub dt_date_from_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dt_date_from.Validated
    Call CalculateDateTo(Me.dt_date_from.Value)

    Me.lbl_num_records.Value = ""
  End Sub

  Private Sub dt_date_to_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dt_date_to.Validated
    Me.lbl_num_records.Value = ""
  End Sub

#End Region ' Events 

#Region "Public Functions"

  ' PURPOSE: Constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  Private Function CheckDates() As Boolean

    Dim _today As Date

    ' Dates 
    If Me.dt_date_from.Value >= Me.dt_date_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dt_date_to.Focus()

      Return False
    End If

    _today = WSI.Common.Misc.TodayOpening()
    '_today = _today.AddHours(-_today.Hour).AddMinutes(-_today.Minute)

    If Me.dt_date_from.Value >= _today _
      Or Me.dt_date_to.Value > _today Then
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING)

      Return False
    End If

    Return True
  End Function ' CheckDates

  ' PURPOSE: Generate the report file
  '
  '    - INPUT:
  '         - ReportType: Specifies report format (XML or Excel)
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub GenerateReportFile(ByVal ReportType As ENUM_REPORT_TYPE)

    Dim _dataset As DataSet
    Dim _stream As IO.Stream
    Dim _output_path As String
    Dim _filepath As String
    Dim _total_records As Int32
    Dim _prize As Decimal
    Dim _file_stream As FileStream
    Dim _params As Dictionary(Of String, String)

    _params = New Dictionary(Of String, String)
    _params.Add("SITE", GetSiteId() & " - " & GetSiteName())
    _params.Add(GLB_NLS_GUI_CONTROLS.GetString(377), GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    _params.Add(GLB_NLS_GUI_CONTROLS.GetString(378), CurrentUser.Name & "@" & Environment.MachineName)

    Try

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Me.lbl_filename.Value = ""
      Me.lbl_num_records.Value = GLB_NLS_GUI_STATISTICS.GetString(120)

      _dataset = WSI.Common.XMLReport.AccountMovements(dt_date_from.Value, dt_date_to.Value)

      _dataset.Tables("Cuenta").Columns.Remove("TrackData")

      If _dataset.Tables("Movimiento").Rows.Count = 0 _
         And _dataset.Tables("Movimiento").Rows.Count = 0 Then
        ' No data found for the selected conditions

        ' 117 "No hay datos para el rango de fechas indicado.  \n\n�Desea generar el fichero vac�o?"
        If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(117), _
                   ENUM_MB_TYPE.MB_TYPE_INFO, _
                   ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Me.lbl_filename.Value = ""
          Me.lbl_num_records.Value = ""

          Return
        End If

      End If

      ' Create the output folder if it does not exist.
      _output_path = System.Environment.CurrentDirectory & Path.DirectorySeparatorChar & ".." & Path.DirectorySeparatorChar & OUTPUT_FOLDER

      If Not Directory.Exists(_output_path) Then
        Directory.CreateDirectory(_output_path)
      End If

      _filepath = _output_path & Path.DirectorySeparatorChar & GenerateFileName()

      Select Case ReportType
        Case ENUM_REPORT_TYPE.TYPE_XML
          _filepath = _filepath & ".xml"

        Case ENUM_REPORT_TYPE.TYPE_EXCEL
          _filepath = ExcelConversion.GetFullFileName(_filepath)

      End Select

      'Show to the user the filename to be exported
      Me.lbl_filename.Value = OUTPUT_FOLDER & Path.DirectorySeparatorChar & Path.GetFileName(_filepath)

      'Check filename doesn't exists before open it.  If does, ask user
      If File.Exists(_filepath) Then

        ' 122 "The file %1 is in read-only mode.\n\nIt won't be possible to generate the file."
        If (File.GetAttributes(_filepath) And FileAttributes.ReadOnly) <> 0 Then
          NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(122), _
                      ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      ENUM_MB_BTN.MB_BTN_OK, _
                      , _
                      Path.GetFileName(_filepath))

          Me.lbl_filename.Value = ""
          Me.lbl_num_records.Value = ""

          Return
        End If

        ' 116 "File %1 already exists.  \n\nDo you want to overwrite it?"
        If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(116), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                      , _
                      Path.GetFileName(_filepath)) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Me.lbl_filename.Value = ""
          Me.lbl_num_records.Value = ""

          Return
        End If

        'To know if the file is already open (it could be happen on Excel's files),
        'let's try to open it on exclusive mode.  If its not possible, ask user to close the file
        Try
          _file_stream = File.Open(_filepath, FileMode.Open, FileAccess.Write, FileShare.None)

          Call _file_stream.Close()
        Catch ex As Exception

          Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(121), _
                       ENUM_MB_TYPE.MB_TYPE_ERROR, _
                       ENUM_MB_BTN.MB_BTN_OK, _
                       , _
                       Path.GetFileName(_filepath))

          Call Trace.WriteLine(ex.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "Accounts Movements GenerateReportFile ", _
                                "btn_generate_xxx_ClickEvent", _
                                ex.Message)

          Me.lbl_filename.Value = ""
          Me.lbl_num_records.Value = ""

          Return
        End Try

      End If

      _total_records = _dataset.Tables("TotalMovimientos").Rows(0).Item("TotalMovimientos")
      _dataset.Tables.Remove("TotalMovimientos")

      For Each _mov_row As DataRow In _dataset.Tables("Movimiento").Rows

        If Not _mov_row("Constancia") Then
          'JMM 30-JAN-2012:  Temporarly holder data from prizes lower than 1000 cleaned. 
          'When all the evicences stuff would be implemented,
          'all holder data from the movements w/o evicence will be cleaned
          If Not IsDBNull(_mov_row("Premio")) Then
            _prize = _mov_row("Premio")
          Else
            _prize = 0
          End If

          If _prize <= 10000 Then
            _mov_row("Constancia.Nombre") = Nothing
            _mov_row("Constancia.RFC") = Nothing
            _mov_row("Constancia.CURP") = Nothing
          End If

        End If
      Next

      'Removing columns not asked to export
      _dataset.Tables("Movimiento").Columns.Remove("Domicilio")
      _dataset.Tables("Movimiento").Columns.Remove("Localidad")
      _dataset.Tables("Movimiento").Columns.Remove("CodigoPostal")
      _dataset.Tables("Movimiento").Columns.Remove("Nombre")
      _dataset.Tables("Movimiento").Columns.Remove("RFC")
      _dataset.Tables("Movimiento").Columns.Remove("CURP")
      _dataset.Tables("Movimiento").Columns.Remove("Constancia.Domicilio")
      _dataset.Tables("Movimiento").Columns.Remove("Constancia.Localidad")
      _dataset.Tables("Movimiento").Columns.Remove("Constancia.CodigoPostal")
      _dataset.Tables("Movimiento").Columns.Remove("Constancia")
      _dataset.Tables("Movimiento").Columns.Remove("DocumentID")

      _dataset.Tables("Movimiento").Columns("Constancia.Nombre").ColumnName = "Nombre"
      _dataset.Tables("Movimiento").Columns("Constancia.RFC").ColumnName = "RFC"
      _dataset.Tables("Movimiento").Columns("Constancia.CURP").ColumnName = "CURP"

      ' 479 "Movements(s)"
      Me.lbl_num_records.Value = _total_records.ToString() & " " & GLB_NLS_GUI_STATISTICS.GetString(479)

      Select Case ReportType
        Case ENUM_REPORT_TYPE.TYPE_XML

          Try
            'Now write the DataSet to an XML file
            _stream = New IO.FileStream(_filepath, IO.FileMode.Create)
            _dataset.WriteXml(_stream)

            'Clean up
            Call _stream.Close()

          Catch ex As Exception
            NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(124), _
                       ENUM_MB_TYPE.MB_TYPE_ERROR, _
                       ENUM_MB_BTN.MB_BTN_OK, _
                       , _
                       Path.GetFileName(_filepath))

            Call Trace.WriteLine(ex.ToString())
            Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  "Accounts Movements GenerateReportFile ", _
                                  "btn_generate_xxx_ClickEvent", _
                                  ex.Message)

            Me.lbl_filename.Value = ""
            Me.lbl_num_records.Value = ""

            Return
          End Try

        Case ENUM_REPORT_TYPE.TYPE_EXCEL
          Try
            ' RCI 24-JAN-2012
            If Not ExcelConversion.DataSetToExcel(_dataset, _filepath, _params) Then
              NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(123), _
                         ENUM_MB_TYPE.MB_TYPE_ERROR, _
                         ENUM_MB_BTN.MB_BTN_OK, _
                         , _
                         Path.GetFileName(_filepath))

              Me.lbl_filename.Value = ""
              Me.lbl_num_records.Value = ""

              Return
            End If

          Catch ex As Exception

            NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(123), _
                         ENUM_MB_TYPE.MB_TYPE_ERROR, _
                         ENUM_MB_BTN.MB_BTN_OK, _
                         , _
                         Path.GetFileName(_filepath))

            Call Trace.WriteLine(ex.ToString())
            Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  "Accounts Movements GenerateReportFile ", _
                                  "btn_generate_xxx_ClickEvent", _
                                  ex.Message)

            Me.lbl_filename.Value = ""
            Me.lbl_num_records.Value = ""

            Return

          End Try
      End Select

      ' 115 "Account movements have been exported to %1 correctly.  \n\nDo you want to export more movements?"   
      If NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(115), _
                    ENUM_MB_TYPE.MB_TYPE_INFO, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    , _
                    Path.GetFileName(_filepath)) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Call Me.Close()
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(155), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Accounts Movements GenerateReportFile ", _
                            "btn_generate_xxx_ClickEvent", _
                            ex.Message)

      Me.lbl_filename.Value = ""
      Me.lbl_num_records.Value = ""

      _filepath = ""

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' GenerateReportFile

  ' PURPOSE: Calculates DateTo
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub CalculateDateTo(ByVal dateFrom As DateTime)

    If dateFrom > Me.dt_date_to.Value Then
      Me.dt_date_to.Value = dateFrom.AddDays(1)
    End If

  End Sub 'CalculateDateTo

  ' PURPOSE: Generates the file name according to the site name and the selected range of dates
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Function GenerateFileName() As String

    Dim _site_id As Integer
    Dim _dates_filename_part As String

    If Not Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("Site", "Identifier"), _site_id) Then
      _site_id = 0
    End If

    _dates_filename_part = dt_date_from.Value.ToString("yyyyMMdd") & "-" & dt_date_to.Value.ToString("yyyyMMdd")

    Return GLB_NLS_GUI_STATISTICS.GetString(477) & "-" & _site_id.ToString("000") & "-" & _dates_filename_part

  End Function

#End Region ' Private Functions 

  Private Sub lbl_filename_Load(sender As Object, e As EventArgs) Handles lbl_filename.Load

  End Sub
End Class