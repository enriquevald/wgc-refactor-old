'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_cashier_configuration.vb
'
' DESCRIPTION : Cashier Configuration form
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-MAR-2009  DRG    Initial Draft.
' 17-SEP-2010  TJG    Player Tracking parameters
' 30-DIC-2011  MPO    New fields of configuration TextPromotion, TextPrizeCoupon, TextInfoChashIn 
' 04-JAN-2012  JMM    Optionally, Hide Totals in A and B tickets. Remove field for the ticket Cash in.
' 20-JAN-2012  RCI    In PlayerTracking tab, added a table of points multiplier per provider.
' 20-MAR-2012  JAB    Add "Promo and CouponTogether" field
' 27-MAR-2012  JAB    Amendment to show and hide the "TabPage" company b
' 02-MAY-2012  MPO    New tab --> Evidence configuration
' 09-MAY-2012  MPO    Change the time to edit evidence to 15000 and added TIME_LIMIT_EDITABLE_EVIDENCE
' 10-MAY-2012  JCM    Change the time to edit evidence TIME_LIMIT_EDITABLE_EVIDENCE = 45000 (+31 days)
' 11-MAY-2012  JCM    On save, verify evidence fields if evidence is enabled
' 03-JUL-2012  JCM    Cupon Prize text enabled/disabled dependends of Split B company value
' 20-SEP-2012  JML    Get back Cupon Prize CheckBox
' 19-NOV-2012  RRB    In PlayerTracking tab, added expiration day month, points to keep and max days no activity.
' 28-FEB-2013  ANG    Add support for MultisiteMember
' 04-APR-2013  RRB    Fixed Bug #215: error when saving with empty fields
' 11-APR-2013  RXM    Fixed Bug #704: error on loading expiration date when regional setting date format is not "dd/MM"
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 13-MAY-2013  AMF    uc_entry_gift_voucher_expiration the same behavior as uc_entry_points_expiration.
' 18-JUN-2013  JAB    The sum Cash In percentage should be equal to 100%.
' 27-JUN-2013  PGJ    Added Account customization voucher footer
' 28-OCT-2013  QMP    Always show "Account Customization Footer" tab
' 25-FEB-2013  JFC    Fixed bug WIG-420: MaxAllowedAccountBalance no longer used.
' 30-JUN-2014  AMF    Personal Card Replacement
' 11-FEB-2015  DRV    Fixed Bug WIG-2034: Some entry fields must allow decimals despite currency doesn't allow it
' 24-FEB-2015  YNM    Fixed Bug WIG-2052: Incorrect decimal format on AuditorData. 
' 29-MAY-2015  TPF    Fixed Bug WIG-2395: Correct crash program when percent is "" in taxes (Company A and B): System->Configuration->General Operations->Taxes
' 01-JUL-2015  FOS    Created new parameters to amount input company b
' 07-AUG-2015  DCS    Refactor and use common functions
' 30-SEP-2015  FOS    Generated documents external retention
' 30-SEP-2015  AMF    Fixed Bug TFS-4823: Block points expiration in site of multisite
' 15-JAN-2016  RAB    Product Backlog Item 7914: Multiple buckets: Cambio pantalla actual de Programa de puntos (delete columns redeemable played, redeembla spend, total played)
' 13-JAN-2016  JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
' 11-FEB-2016  FGB    Product Backlog Item 9234: Multiple buckets: Modificaci�n pantalla "Operaciones Generales".
' 21-MAR-2016 JRC     PBI: 1792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
' 16-NOV-2016 ATB     Bug 18413: General Operations screen: Reward program version V03.002.0020
' 17-OCT-2016  FAV    PBI: 17896: CFDI configuration tab.
' 25-OCT-2016  JBP    PBI 19598:Constancias - Mejoras REVIEW Sprint 31
' 22-NOV-2016  JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
' 08-MAR-2017  JMM    PBI 25267:Third TAX - Refund TAX Configuration
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports WigosGUI.CLASS_CASHIER_CONFIGURATION
Imports WSI.Common.ExtentionMethods
Imports System.IO

Public Class frm_cashier_configuration
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PROVIDER_ID As Integer = 1
  Private Const GRID_COLUMN_PROVIDER_NAME As Integer = 2
  Private Const GRID_COLUMN_POINTS_MULTIPLIER As Integer = 3

  Private Const GRID_COLUMNS As Integer = 4
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_PROVIDER_NAME As Integer = 2400
  Private Const GRID_WIDTH_POINTS As Integer = 1200

  Private Const TIME_LIMIT_EDITABLE_EVIDENCE As Integer = 45000

  Private Const TIME_PERIOD_LIMIT As Integer = 2500

  Private Const GRID_COLUMN_CFDI_STATUS As Integer = 0
  Private Const GRID_COLUMN_CFDI_LAST_MONTH As Integer = 1
  Private Const GRID_COLUMN_CFDI_CURRENT_MONTH As Integer = 2

  Private Const GRID_WIDTH_CFDI_STATUS As Integer = 1630
  Private Const GRID_WIDTH_CFDI_TOTAL As Integer = 2005

  Private Const DEFAULT_VALUE_PROCESS As String = "60"
  Private Const DEFAULT_VALUE_LOAD As String = "60"

  Private Const TAB_CONTROL_POINTS_INDEX = 0
  Private Const TAB_CONTROL_POINTS_BLOCK = 1
  Private Const TAB_CONTROL_POINTS_PLAYER_LEVEL_ICON = 2

  ' PlayerLevelIcon config
  Private Const DEFAULT_NUM_LEVELS As Integer = 4
  Private Const PLAYER_LEVEL_ICON_COMBO_PREFIX As String = "cmb_player_level_icon_0{0}"
  Private Const PLAYER_LEVEL_ICON_PICTURE_PREFIX As String = "pb_level_0{0}"

#End Region 'Constants

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASHIER_CONFIGURATION

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then
      ' - Form MultiSite
      Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)      ' 292 "Programa de Puntos"
    Else
      ' - Form
      Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(201)      ' 201 "Cashier Configuration"
    End If

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    'btn_custom_0 is used as the 'Apply/Save' button
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    Me.lbl_cashier_type.Text = GLB_NLS_GUI_CONFIGURATION.GetString(208)
    Me.lbl_cashier_type.Focus()
    Me.lbl_cashier_type.Visible = GLB_GuiMode = ENUM_GUI.WIGOS_GUI

    ' Harcoded values of the combo box.
    Me.uc_cmb_cashier_type.Add(CashlessMode.WIN, GLB_NLS_GUI_CONFIGURATION.GetString(220))             ' 220 "WIN"
    Me.uc_cmb_cashier_type.Add(CashlessMode.ALESIS, GLB_NLS_GUI_CONFIGURATION.GetString(221))          ' 221 "3GS"
    Me.uc_cmb_cashier_type.Visible = GLB_GuiMode = ENUM_GUI.WIGOS_GUI

    Me.tab_control.Controls.Clear()

    Me.TabWin.Text = GLB_NLS_GUI_CONFIGURATION.GetString(220)          ' 220 "WIN"
    Me.TabAlesis.Text = GLB_NLS_GUI_CONFIGURATION.GetString(221)       ' 221 "3GS"
    Me.TabPoints.Text = GLB_NLS_GUI_CONFIGURATION.GetString(292)       ' 292 "Programa de Puntos" 
    Me.TabTaxes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(371)      ' 371 "Impuestos"
    Me.TabTaxesOnPrize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7139)      ' 7139 "Sobre Premios"
    Me.TabTaxesOnPromotions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7140)      ' 7140 "Sobre promociones en efectivo"
    Me.TabEvidence.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(665)   ' 665 "Evidencia"
    Me.id_evidence_configuration.Init()

    ' FBA 06-SEP-2013
    Me.TabLock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2576)
    Me.TabPointsDef.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205)
    Me.uc_cmb_Level1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2571)
    Me.uc_cmb_Level1.IsReadOnly = False
    'automatic lock control on level 1 is not necessary, it can be controlled from other levels - combo disabled for now
    Me.uc_cmb_Level1.Enabled = False
    Call FillLevelCombos(uc_cmb_Level1)
    Me.uc_cmb_Level2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2571)
    Me.uc_cmb_Level2.IsReadOnly = False
    Call FillLevelCombos(uc_cmb_Level2)
    Me.uc_cmb_Level3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2571)
    Me.uc_cmb_Level3.IsReadOnly = False
    Call FillLevelCombos(uc_cmb_Level3)
    Me.uc_cmb_Level4.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2571)
    Me.uc_cmb_Level4.IsReadOnly = False
    Call FillLevelCombos(uc_cmb_Level4)

    Me.TabPlayerLevelIcons.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8557)

    ' TJG 17-SEP-2010 Player Tracking
    Call InitControlsPlayerTracking()

    ' MBF 28-SEP-2010 Taxes
    Call InitControlsTaxes()

    ' CFDI
    Call InitControlsCFDI()

    Call BindComboControls()

    ' FGB 11-FEB-2016 Mostramos mensaje "La caducidad de puntos se debe realizar en los Buckets"
    Me.lbl_points_expiration_msg.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7114)



    If Not (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then 'Buckets SPACE	
      Me.lbl_points_expiration_msg.Visible = False
    End If

    Me.Timer1.Interval = 1000
    Me.Timer1.Start()

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_Exit()
    ' Perform any clean-up
    ' RCI 22-JUL-2010: Release used memory.
    Call Me.uc_alesis_cashier_config.Dispose()
    Call MyBase.GUI_Exit()
  End Sub ' GUI_Exit

  Protected Overrides Sub GUI_GetScreenData()

    Dim cashier_config_edit As CLASS_CASHIER_CONFIGURATION
    Dim cashier_config_read As CLASS_CASHIER_CONFIGURATION
    Dim idx As Integer

    Try
      cashier_config_edit = Me.DbEditedObject
      cashier_config_read = Me.DbReadObject

      cashier_config_edit.CashierType() = uc_cmb_cashier_type.Value

      ' FGB 11-FEB-2016
      'cashier_config_edit.PlayerTrackingPointsExpiration = GUI_LocalNumberToDBNumber(uc_entry_points_expiration.Value)
      'If Me.chk_expiration_day_month_points.Checked Then
      '  cashier_config_edit.PlayerTrackingPointsExpirationDayMonth = dtp_expiration_day_month_points.Value.ToString("dd/MM")
      'Else
      '  cashier_config_edit.PlayerTrackingPointsExpirationDayMonth = ""
      'End If
      cashier_config_edit.PlayerTrackingPointsExpiration = 0
      cashier_config_edit.PlayerTrackingPointsExpirationDayMonth = ""
      If Me.chk_expiration_day_month.Checked Then
        cashier_config_edit.PlayerTrackingLevelsExpirationDayMonth = dtp_expiration_day_month.Value.ToString("dd/MM")
      Else
        cashier_config_edit.PlayerTrackingLevelsExpirationDayMonth = ""
      End If
      cashier_config_edit.PlayerTrackingGiftExpiration = GUI_LocalNumberToDBNumber(uc_entry_gift_voucher_expiration.Value)
      cashier_config_edit.PlayerTrackingLevelsDaysCountingPoints = GUI_LocalNumberToDBNumber(uc_entry_levels_days_counting_points.Value)
      cashier_config_edit.PlayerTrackingLevelName(0) = uc_entry_01_name.Value
      cashier_config_edit.PlayerTrackingLevelName(1) = uc_entry_02_name.Value
      cashier_config_edit.PlayerTrackingLevelName(2) = uc_entry_03_name.Value
      cashier_config_edit.PlayerTrackingLevelName(3) = uc_entry_04_name.Value

      ' RAB 15-JAN-2016
      'cashier_config_edit.PlayerTrackingRedeemPlayedTo1Point(0) = GUI_LocalNumberToDBNumber(uc_entry_01_redeem_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemPlayedTo1Point(1) = GUI_LocalNumberToDBNumber(uc_entry_02_redeem_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemPlayedTo1Point(2) = GUI_LocalNumberToDBNumber(uc_entry_03_redeem_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemPlayedTo1Point(3) = GUI_LocalNumberToDBNumber(uc_entry_04_redeem_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingTotalPlayedTo1Point(0) = GUI_LocalNumberToDBNumber(uc_entry_01_total_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingTotalPlayedTo1Point(1) = GUI_LocalNumberToDBNumber(uc_entry_02_total_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingTotalPlayedTo1Point(2) = GUI_LocalNumberToDBNumber(uc_entry_03_total_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingTotalPlayedTo1Point(3) = GUI_LocalNumberToDBNumber(uc_entry_04_total_played_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemableSpentTo1Point(0) = GUI_LocalNumberToDBNumber(uc_entry_01_redeem_spent_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemableSpentTo1Point(1) = GUI_LocalNumberToDBNumber(uc_entry_02_redeem_spent_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemableSpentTo1Point(2) = GUI_LocalNumberToDBNumber(uc_entry_03_redeem_spent_to_1_point.Value)
      'cashier_config_edit.PlayerTrackingRedeemableSpentTo1Point(3) = GUI_LocalNumberToDBNumber(uc_entry_04_redeem_spent_to_1_point.Value)

      cashier_config_edit.PlayerTrackingPointsToEnter(1) = GUI_LocalNumberToDBNumber(uc_entry_02_points_to_enter.Value)
      cashier_config_edit.PlayerTrackingPointsToEnter(2) = GUI_LocalNumberToDBNumber(uc_entry_03_points_to_enter.Value)
      cashier_config_edit.PlayerTrackingPointsToEnter(3) = GUI_LocalNumberToDBNumber(uc_entry_04_points_to_enter.Value)
      cashier_config_edit.PlayerTrackingDaysOnLevel(1) = GUI_LocalNumberToDBNumber(uc_entry_02_days_on_level.Value)
      cashier_config_edit.PlayerTrackingDaysOnLevel(2) = GUI_LocalNumberToDBNumber(uc_entry_03_days_on_level.Value)
      cashier_config_edit.PlayerTrackingDaysOnLevel(3) = GUI_LocalNumberToDBNumber(uc_entry_04_days_on_level.Value)
      cashier_config_edit.PlayerTrackingPointsToKeep(1) = GUI_LocalNumberToDBNumber(uc_entry_02_points_to_keep.Value)
      cashier_config_edit.PlayerTrackingPointsToKeep(2) = GUI_LocalNumberToDBNumber(uc_entry_03_points_to_keep.Value)
      cashier_config_edit.PlayerTrackingPointsToKeep(3) = GUI_LocalNumberToDBNumber(uc_entry_04_points_to_keep.Value)

      cashier_config_edit.PlayerTrackingUpgradeDowngradeAction(0) = GUI_LocalNumberToDBNumber(uc_cmb_Level1.Value)
      cashier_config_edit.PlayerTrackingUpgradeDowngradeAction(1) = GUI_LocalNumberToDBNumber(uc_cmb_Level2.Value)
      cashier_config_edit.PlayerTrackingUpgradeDowngradeAction(2) = GUI_LocalNumberToDBNumber(uc_cmb_Level3.Value)
      cashier_config_edit.PlayerTrackingUpgradeDowngradeAction(3) = GUI_LocalNumberToDBNumber(uc_cmb_Level4.Value)


      If Me.chk_max_days_no_activity.Checked Then
        If uc_entry_02_max_days_no_activity.Value.Length > 0 Then
          cashier_config_edit.PlayerTrackingMaxDaysNoActivity(1) = GUI_LocalNumberToDBNumber(uc_entry_02_max_days_no_activity.Value)
        Else
          cashier_config_edit.PlayerTrackingMaxDaysNoActivity(1) = ""
        End If
        If uc_entry_03_max_days_no_activity.Value.Length > 0 Then
          cashier_config_edit.PlayerTrackingMaxDaysNoActivity(2) = GUI_LocalNumberToDBNumber(uc_entry_03_max_days_no_activity.Value)
        Else
          cashier_config_edit.PlayerTrackingMaxDaysNoActivity(2) = ""
        End If
        If uc_entry_04_max_days_no_activity.Value.Length > 0 Then
          cashier_config_edit.PlayerTrackingMaxDaysNoActivity(3) = GUI_LocalNumberToDBNumber(uc_entry_04_max_days_no_activity.Value)
        Else
          cashier_config_edit.PlayerTrackingMaxDaysNoActivity(3) = ""
        End If
      Else
        cashier_config_edit.PlayerTrackingMaxDaysNoActivity(1) = ""
        cashier_config_edit.PlayerTrackingMaxDaysNoActivity(2) = ""
        cashier_config_edit.PlayerTrackingMaxDaysNoActivity(3) = ""
      End If

      If chk_max_points.Checked Then
        cashier_config_edit.PlayerTrackingMaxPoints = GUI_LocalNumberToDBNumber(uc_max_points.Value)
      Else
        cashier_config_edit.PlayerTrackingMaxPoints = "0"
      End If
      If chk_max_points_per_minute.Checked Then
        cashier_config_edit.PlayerTrackingMaxPointsPerMinute = GUI_LocalNumberToDBNumber(uc_max_points_per_minute.Value)
      Else
        cashier_config_edit.PlayerTrackingMaxPointsPerMinute = "0"
      End If
      cashier_config_edit.PlayerTrackingHasZeroPlays = Me.chk_has_zero_plays.Checked
      cashier_config_edit.PlayerTrackingHasMismatch = Me.chk_has_mismatch.Checked

      '
      ' GET SCREEN DATA IN MULTISITE MODE
      ' EXIT WHEN END IN MULTISITE MODE
      '

      If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then

        Exit Sub
      End If

      cashier_config_edit.CardPrice = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.CardPrice)
      cashier_config_edit.PersonalCardPrice = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.PersonalCardPrice)
      cashier_config_edit.PersonalCardReplacementPrice = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.PersonalCardReplacementPrice)
      'cashier_config_edit.PersonalCardReplacementPriceInPoints = GUI_LocalNumberToDBNumber(IIf(String.IsNullOrEmpty(Uc_wcp_cashier_config1.PersonalCardReplacementPriceInPoints), 0, Uc_wcp_cashier_config1.PersonalCardReplacementPriceInPoints))
      cashier_config_edit.PersonalCardReplacementPriceInPoints = "0"
      cashier_config_edit.PersonalCardReplacementChargeAfterTimes = GUI_LocalNumberToDBNumber(IIf(String.IsNullOrEmpty(uc_wcp_cashier_config.PersonalCardReplacementChargeAfterTimes), 0, uc_wcp_cashier_config.PersonalCardReplacementChargeAfterTimes))
      cashier_config_edit.CardRefundable = uc_wcp_cashier_config.CardRefundable
      cashier_config_edit.CashierMode = uc_wcp_cashier_config.CashierMode
      cashier_config_edit.Language = uc_wcp_cashier_config.Language
      cashier_config_edit.MinTimeToCloseSession = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.MinTimeToCloseSession)
      cashier_config_edit.VoucherHeader = uc_wcp_cashier_config.VoucherHeader
      cashier_config_edit.VoucherFooter = uc_wcp_cashier_config.VoucherFooter
      cashier_config_edit.CardPrintTopX = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.CardPrintTopX)
      cashier_config_edit.CardPrintTopY = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.CardPrintTopY)
      cashier_config_edit.CardPrintBottomX = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.CardPrintBottomX)
      cashier_config_edit.CardPrintBottomY = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.CardPrintBottomY)
      cashier_config_edit.CardPrintFontSize = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.CardPrintFontSize)
      cashier_config_edit.CardPrinterName = uc_wcp_cashier_config.CardPrinterName
      cashier_config_edit.CardPrintBold = uc_wcp_cashier_config.CardPrintBold
      cashier_config_edit.AllowAnon = uc_wcp_cashier_config.AllowAnon
      cashier_config_edit.HandPayTimePeriod = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.HandPayTimePeriod)
      cashier_config_edit.HandPayTimeToCancel = GUI_LocalNumberToDBNumber(uc_wcp_cashier_config.HandPayCancelTime)

      ' PGJ 26-JUN-2013: Account customization voucher
      cashier_config_edit.VoucherFooterAccountsCustomize = uc_wcp_cashier_config.VoucherFooterAccountsCustomization

      ' RCI 20-JAN-2012
      GetScreenProviders(cashier_config_edit.PlayerTrackingPointsMultiplierProviders)

      ' MBF 28-SEP-2010 Taxes
      cashier_config_edit.TaxesBusinessAName = uc_business_a.BusinessName
      cashier_config_edit.TaxesBusinessAConcept = uc_business_a.ConceptName
      cashier_config_edit.TaxesBusinessACashInPct = GUI_LocalNumberToDBNumber(uc_business_a.CashInPct)
      cashier_config_edit.TaxesBusinessATaxName = uc_business_a.TaxName
      cashier_config_edit.TaxesBusinessATaxPct = GUI_LocalNumberToDBNumber(uc_business_a.TaxPct)
      cashier_config_edit.TaxesBusinessAVoucherHeader = uc_business_a.GetVoucherText("Header")
      cashier_config_edit.TaxesBusinessAVoucherFooter = uc_business_a.GetVoucherText("Footer")
      cashier_config_edit.TaxesBusinessAVoucherFooterCashIn = uc_business_a.GetVoucherText("Footer.CashIn")
      cashier_config_edit.TaxesBusinessAVoucherFooterDev = uc_business_a.GetVoucherText("Footer.Dev")
      cashier_config_edit.TaxesBusinessAVoucherFooterCancel = uc_business_a.GetVoucherText("Footer.Cancel")
      cashier_config_edit.TaxesBusinessAPrizeCoupon = IIf(uc_business_a.PrizeCoupon, "1", "0")
      cashier_config_edit.TaxesBusinessAHidePromo = IIf(uc_business_a.HidePromo, "1", "0")
      cashier_config_edit.TaxesBusinessAPromotionText = uc_business_a.TextPromotion
      cashier_config_edit.TaxesBusinessAPrizeCouponText = uc_business_a.TextPrizeCoupon
      cashier_config_edit.TaxesBusinessAHideTotalCashIn = IIf(uc_business_a.HideTotalCashIn, "1", "0")
      ' JAB 21-MAR-2012 Promo and CouponTogether
      cashier_config_edit.TaxesBusinessAPromoAndCouponTogether = IIf(uc_business_a.PromoAndCouponTogether, "1", "0")
      cashier_config_edit.TaxesBusinessAPromoAndCouponTogetherText = uc_business_a.PromoAndCouponTogetherText
      cashier_config_edit.TaxesBusinessAPayTitle = uc_business_a.PayTitle
      cashier_config_edit.TaxesBusinessADevTitle = uc_business_a.DevTitle

      cashier_config_edit.TaxesBusinessBName = uc_business_b.BusinessName
      cashier_config_edit.TaxesBusinessBConcept = uc_business_b.ConceptName
      cashier_config_edit.TaxesBusinessBCashInPct = GUI_LocalNumberToDBNumber(uc_business_b.CashInPct)
      cashier_config_edit.TaxesBusinessBTaxName = uc_business_b.TaxName
      cashier_config_edit.TaxesBusinessBTaxPct = GUI_LocalNumberToDBNumber(uc_business_b.TaxPct)
      cashier_config_edit.TaxesBusinessBVoucherHeader = uc_business_b.GetVoucherText("Header")
      cashier_config_edit.TaxesBusinessBVoucherFooter = uc_business_b.GetVoucherText("Footer")
      cashier_config_edit.TaxesBusinessBVoucherFooterCashIn = uc_business_b.GetVoucherText("Footer.CashIn")
      cashier_config_edit.TaxesBusinessBVoucherFooterDev = uc_business_b.GetVoucherText("Footer.Dev")
      cashier_config_edit.TaxesBusinessBVoucherFooterCancel = uc_business_b.GetVoucherText("Footer.Cancel")
      cashier_config_edit.TaxesBusinessBHideTotalCashIn = IIf(uc_business_b.HideTotalCashIn, "1", "0")
      cashier_config_edit.TaxesBusinessBPayTitle = uc_business_b.PayTitle
      cashier_config_edit.TaxesBusinessBDevTitle = uc_business_b.DevTitle

      cashier_config_edit.TaxesBusinessBEnable = IIf(chk_split_enabled.Checked, "1", "0")

      cashier_config_edit.Taxes1Pct = GUI_LocalNumberToDBNumber(Uc_entry_tax_1_on_won_pct.Value)
      cashier_config_edit.Taxes2Pct = GUI_LocalNumberToDBNumber(Uc_entry_tax_2_on_won_pct.Value)
      cashier_config_edit.Taxes3Pct = GUI_LocalNumberToDBNumber(Uc_entry_tax_3_on_won_pct.Value)
      cashier_config_edit.TaxesPromo1Pct = GUI_LocalNumberToDBNumber(Uc_entry_promo_tax_1_on_won_pct.Value)
      cashier_config_edit.TaxesPromo2Pct = GUI_LocalNumberToDBNumber(Uc_entry_promo_tax_2_on_won_pct.Value)
      cashier_config_edit.TaxesPromo3Pct = GUI_LocalNumberToDBNumber(Uc_entry_promo_tax_3_on_won_pct.Value)
      cashier_config_edit.Taxes1Name = Uc_entry_tax_1_on_won_name.Value
      cashier_config_edit.Taxes2Name = Uc_entry_tax_2_on_won_name.Value
      cashier_config_edit.Taxes3Name = Uc_entry_tax_3_on_won_name.Value
      cashier_config_edit.Taxes1ApplyIf = GUI_LocalNumberToDBNumber(Uc_entry_tax_1_apply_if.Value)
      cashier_config_edit.Taxes2ApplyIf = GUI_LocalNumberToDBNumber(Uc_entry_tax_2_apply_if.Value)
      cashier_config_edit.Taxes3ApplyIf = GUI_LocalNumberToDBNumber(Uc_entry_tax_3_apply_if.Value)
      cashier_config_edit.TaxesPromo1ApplyIf = GUI_LocalNumberToDBNumber(Uc_entry_promo_tax_1_apply_if.Value)
      cashier_config_edit.TaxesPromo2ApplyIf = GUI_LocalNumberToDBNumber(Uc_entry_promo_tax_2_apply_if.Value)
      cashier_config_edit.TaxesPromo3ApplyIf = GUI_LocalNumberToDBNumber(Uc_entry_promo_tax_3_apply_if.Value)
      If Me.chk_enable_taxes_redeem_promotions.Checked Then
        cashier_config_edit.EnableRedeemPromotionTaxes = 1
      Else
        cashier_config_edit.EnableRedeemPromotionTaxes = 0
      End If

      cashier_config_edit.DataBaseIp() = uc_alesis_cashier_config.DataBaseIp()
      cashier_config_edit.DataBaseName() = uc_alesis_cashier_config.DataBaseName()
      cashier_config_edit.VendorId() = uc_alesis_cashier_config.VendorId()
      cashier_config_edit.Username() = uc_alesis_cashier_config.Username()
      cashier_config_edit.Password() = uc_alesis_cashier_config.Password()

      cashier_config_edit.ConfTerms.Clear()
      ' cashier_config_edit.NotConfTerms.Clear()

      For idx = 0 To uc_alesis_cashier_config.TotalRows - 1
        Dim temp_element As New CLASS_CASHIER_CONFIGURATION.TYPE_TERMINAL
        temp_element.Clear()
        'Get from screen
        uc_alesis_cashier_config.GetConfiguredRow(idx, temp_element.id, temp_element.name, temp_element.external_id, temp_element.machine_id, temp_element.status)

        'Compare with the current data
        temp_element.Updated = False
        temp_element.Added = False
        temp_element.Deleted = False
        If cashier_config_read.ConfTerms(idx).machine_id <> temp_element.machine_id Then
          If cashier_config_read.ConfTerms(idx).machine_id = 0 And temp_element.machine_id <> 0 Then
            temp_element.Added = True
          End If
          If cashier_config_read.ConfTerms(idx).machine_id <> 0 And temp_element.machine_id = 0 Then
            temp_element.Deleted = True
          End If
          If cashier_config_read.ConfTerms(idx).machine_id <> 0 And temp_element.machine_id <> 0 Then
            temp_element.Updated = True
          End If
        End If
        cashier_config_edit.ConfTerms.Add(temp_element)
      Next

      cashier_config_edit.EvidenceCompanyId1 = Me.id_evidence_configuration.CompanyID1
      cashier_config_edit.EvidenceCompanyId2 = Me.id_evidence_configuration.CompanyID2
      cashier_config_edit.EvidenceCompanyName = Me.id_evidence_configuration.CompanyName
      cashier_config_edit.EvidenceRepresentativeId1 = Me.id_evidence_configuration.RepresentativeID1
      cashier_config_edit.EvidenceRepresentativeId2 = Me.id_evidence_configuration.RepresentativeID2
      cashier_config_edit.EvidenceRepresentativeName = Me.id_evidence_configuration.RepresentativeName
      cashier_config_edit.EvidenceRepresentativeSign = Me.id_evidence_configuration.RepresentativeSignature
      cashier_config_edit.EvidenceEditableMinutes = Me.id_evidence_configuration.EditableMinutes
      cashier_config_edit.EvidenceEnabled = Me.id_evidence_configuration.EvidenceEnabled
      cashier_config_edit.EvidencePrizeGreaterThan = Me.id_evidence_configuration.PrizeGratherThan
      cashier_config_edit.CardPlayerConcept = Me.uc_wcp_cashier_config.CardPlayerConcept
      cashier_config_edit.CardPlayerAmountCompanyB = Me.uc_wcp_cashier_config.CardPlayerAmountToCompanyB
      cashier_config_edit.GeneratedWitholdingDocument = Me.id_evidence_configuration.GeneratedWitholdingDocument

      cashier_config_edit.CFDI_Active = IIf(uc_entry_cfdi_active.Checked, "1", "0")
      cashier_config_edit.CFDI_Password = uc_entry_cfdi_password.Value
      cashier_config_edit.CFDI_Retries = uc_entry_cfdi_retries.Value
      cashier_config_edit.CFDI_SchedulingLoad = uc_entry_cfdi_scheduling_load.Value
      cashier_config_edit.CFDI_SchedulingProcess = uc_entry_cfdi_scheduling_process.Value
      cashier_config_edit.CFDI_Site = uc_entry_cfdi_site.Value
      cashier_config_edit.CFDI_Url = uc_entry_cfdi_url.Value
      cashier_config_edit.CFDI_User = uc_entry_cfdi_user.Value
      cashier_config_edit.CFDI_RFC_Anonymous = uc_entry_cfdi_rfc_anonymous.Value
      cashier_config_edit.CFDI_EF_Anonymous = uc_entry_cfdi_ef_anonymous.Value.PadLeft(2, "0")
      cashier_config_edit.CFDI_EF_Foreign = uc_entry_cfdi_ef_foreign.Value.PadLeft(2, "0")
      cashier_config_edit.CFDI_RFC_Foreign = uc_entry_cfdi_rfc_foreign.Value

      Me.GetPlayerLevelIcons(cashier_config_edit.PlayerLevelIcons)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_GetScreenData", _
                            ex.Message)
    Finally

    End Try

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim cashier_configuration As CLASS_CASHIER_CONFIGURATION
    Dim idx As Integer
    Dim temp_term As CLASS_CASHIER_CONFIGURATION.TYPE_TERMINAL

    Try
      cashier_configuration = Me.DbReadObject


      uc_entry_levels_days_counting_points.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingLevelsDaysCountingPoints)
      uc_entry_01_name.Value = cashier_configuration.PlayerTrackingLevelName(0)
      uc_entry_02_name.Value = cashier_configuration.PlayerTrackingLevelName(1)
      uc_entry_03_name.Value = cashier_configuration.PlayerTrackingLevelName(2)
      uc_entry_04_name.Value = cashier_configuration.PlayerTrackingLevelName(3)

      ' RAB 15-JAN-2016
      'uc_entry_01_redeem_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemPlayedTo1Point(0))
      'uc_entry_02_redeem_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemPlayedTo1Point(1))
      'uc_entry_03_redeem_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemPlayedTo1Point(2))
      'uc_entry_04_redeem_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemPlayedTo1Point(3))
      'uc_entry_01_total_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingTotalPlayedTo1Point(0))
      'uc_entry_02_total_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingTotalPlayedTo1Point(1))
      'uc_entry_03_total_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingTotalPlayedTo1Point(2))
      'uc_entry_04_total_played_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingTotalPlayedTo1Point(3))
      'uc_entry_01_redeem_spent_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemableSpentTo1Point(0))
      'uc_entry_02_redeem_spent_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemableSpentTo1Point(1))
      'uc_entry_03_redeem_spent_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemableSpentTo1Point(2))
      'uc_entry_04_redeem_spent_to_1_point.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingRedeemableSpentTo1Point(3))

      uc_entry_02_points_to_enter.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsToEnter(1))
      uc_entry_03_points_to_enter.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsToEnter(2))
      uc_entry_04_points_to_enter.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsToEnter(3))
      uc_entry_02_days_on_level.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingDaysOnLevel(1))
      uc_entry_03_days_on_level.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingDaysOnLevel(2))
      uc_entry_04_days_on_level.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingDaysOnLevel(3))
      uc_entry_02_points_to_keep.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsToKeep(1))
      uc_entry_03_points_to_keep.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsToKeep(2))
      uc_entry_04_points_to_keep.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsToKeep(3))
      ' FGB 11-FEB-2016
      'uc_entry_points_expiration.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingPointsExpiration)
      'chk_expiration_day_month_points.Checked = (Not String.IsNullOrEmpty(cashier_configuration.PlayerTrackingPointsExpirationDayMonth))
      'chk_expiration_day_month_points_CheckedChanged(Nothing, Nothing)
      'If String.IsNullOrEmpty(cashier_configuration.PlayerTrackingPointsExpirationDayMonth) Then
      '  dtp_expiration_day_month_points.Value = New Date(2001, 12, 31)
      'Else
      '  dtp_expiration_day_month_points.Value = WSI.Common.Format.DBDatetimeToLocalDateTime(cashier_configuration.PlayerTrackingPointsExpirationDayMonth)
      'End If
      uc_entry_gift_voucher_expiration.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingGiftExpiration)
      ' FBA 06-SEP-2013
      If cashier_configuration.PlayerTrackingUpgradeDowngradeAction(0) = "" Then
        uc_cmb_Level1.Value = WSI.Common.UpgradeDowngradeAction.None
      Else
        uc_cmb_Level1.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingUpgradeDowngradeAction(0))
      End If
      If cashier_configuration.PlayerTrackingUpgradeDowngradeAction(1) = "" Then
        uc_cmb_Level2.Value = WSI.Common.UpgradeDowngradeAction.None
      Else
        uc_cmb_Level2.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingUpgradeDowngradeAction(1))
      End If
      If cashier_configuration.PlayerTrackingUpgradeDowngradeAction(2) = "" Then
        uc_cmb_Level3.Value = WSI.Common.UpgradeDowngradeAction.None
      Else
        uc_cmb_Level3.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingUpgradeDowngradeAction(2))
      End If
      If cashier_configuration.PlayerTrackingUpgradeDowngradeAction(3) = "" Then
        uc_cmb_Level4.Value = WSI.Common.UpgradeDowngradeAction.None
      Else
        uc_cmb_Level4.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingUpgradeDowngradeAction(3))
      End If

      chk_expiration_day_month.Checked = (Not String.IsNullOrEmpty(cashier_configuration.PlayerTrackingLevelsExpirationDayMonth))
      chk_expiration_day_month_CheckedChanged(Nothing, Nothing)

      If String.IsNullOrEmpty(cashier_configuration.PlayerTrackingLevelsExpirationDayMonth) Then
        dtp_expiration_day_month.Value = New Date(2001, 12, 31)
      Else
        dtp_expiration_day_month.Value = WSI.Common.Format.DBDatetimeToLocalDateTime(cashier_configuration.PlayerTrackingLevelsExpirationDayMonth)
      End If

      chk_max_days_no_activity.Checked = (Not String.IsNullOrEmpty(cashier_configuration.PlayerTrackingMaxDaysNoActivity(1) & _
                                                                   cashier_configuration.PlayerTrackingMaxDaysNoActivity(2) & _
                                                                   cashier_configuration.PlayerTrackingMaxDaysNoActivity(3)))
      chk_max_no_activity_days_CheckedChanged(Nothing, Nothing)
      If chk_max_days_no_activity.Checked Then
        If cashier_configuration.PlayerTrackingMaxDaysNoActivity(1).Length > 0 Then
          uc_entry_02_max_days_no_activity.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingMaxDaysNoActivity(1))
        Else
          uc_entry_02_max_days_no_activity.Value = ""
        End If
        If cashier_configuration.PlayerTrackingMaxDaysNoActivity(2).Length > 0 Then
          uc_entry_03_max_days_no_activity.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingMaxDaysNoActivity(2))
        Else
          uc_entry_03_max_days_no_activity.Value = ""
        End If
        If cashier_configuration.PlayerTrackingMaxDaysNoActivity(3).Length > 0 Then
          uc_entry_04_max_days_no_activity.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingMaxDaysNoActivity(3))
        Else
          uc_entry_04_max_days_no_activity.Value = ""
        End If
      Else
        uc_entry_02_max_days_no_activity.Value = ""
        uc_entry_03_max_days_no_activity.Value = ""
        uc_entry_04_max_days_no_activity.Value = ""
      End If

      uc_max_points.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingMaxPoints)
      If uc_max_points.Value <> "0" Then
        chk_max_points.Checked = True
        uc_max_points.Enabled = True
      Else
        chk_max_points.Checked = False
        uc_max_points.Enabled = False
      End If
      uc_max_points_per_minute.Value = GUI_DBNumberToLocalNumber(cashier_configuration.PlayerTrackingMaxPointsPerMinute)
      If uc_max_points_per_minute.Value <> "0" Then
        chk_max_points_per_minute.Checked = True
        uc_max_points_per_minute.Enabled = True
      Else
        chk_max_points_per_minute.Checked = False
        uc_max_points_per_minute.Enabled = False
      End If
      chk_has_zero_plays.Checked = cashier_configuration.PlayerTrackingHasZeroPlays
      chk_has_mismatch.Checked = cashier_configuration.PlayerTrackingHasMismatch

      '
      ' SET DATA RELATED TO TAB POINTS
      ' EXIT IN MULTISITE MODE
      '

      ' RAB 15-JAN-2016
      'Call RefreshExample()

      If (GLB_GuiMode = ENUM_GUI.MULTISITE_GUI And CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site) _
        Or (Not GLB_GuiMode = ENUM_GUI.MULTISITE_GUI And Not CommonMultiSite.GetPlayerTrackingMode() = PlayerTracking_Mode.Site) Then

        Me.uc_entry_01_name.Enabled = False

        ' RAB 15-JAN-2016
        'Me.uc_entry_01_redeem_spent_to_1_point.Enabled = False
        'Me.uc_entry_01_redeem_played_to_1_point.Enabled = False
        'Me.uc_entry_01_total_played_to_1_point.Enabled = False

        Me.uc_entry_02_name.Enabled = False

        ' RAB 15-JAN-2016
        'Me.uc_entry_02_redeem_spent_to_1_point.Enabled = False
        'Me.uc_entry_02_redeem_played_to_1_point.Enabled = False
        'Me.uc_entry_02_total_played_to_1_point.Enabled = False

        Me.uc_entry_02_points_to_enter.Enabled = False
        Me.uc_entry_02_days_on_level.Enabled = False
        Me.uc_entry_02_points_to_keep.Enabled = False
        Me.uc_entry_02_max_days_no_activity.Enabled = False

        Me.uc_entry_03_name.Enabled = False

        ' RAB 15-JAN-2016
        'Me.uc_entry_03_redeem_spent_to_1_point.Enabled = False
        'Me.uc_entry_03_redeem_played_to_1_point.Enabled = False
        'Me.uc_entry_03_total_played_to_1_point.Enabled = False

        Me.uc_entry_03_points_to_enter.Enabled = False
        Me.uc_entry_03_days_on_level.Enabled = False
        Me.uc_entry_03_points_to_keep.Enabled = False
        Me.uc_entry_03_max_days_no_activity.Enabled = False

        Me.uc_entry_04_name.Enabled = False

        ' RAB 15-JAN-2016
        'Me.uc_entry_04_redeem_spent_to_1_point.Enabled = False
        'Me.uc_entry_04_redeem_played_to_1_point.Enabled = False
        'Me.uc_entry_04_total_played_to_1_point.Enabled = False
        Me.uc_entry_04_points_to_enter.Enabled = False
        Me.uc_entry_04_days_on_level.Enabled = False
        Me.uc_entry_04_points_to_keep.Enabled = False
        Me.uc_entry_04_max_days_no_activity.Enabled = False

        Me.uc_cmb_Level1.Enabled = False
        Me.uc_cmb_Level2.Enabled = False
        Me.uc_cmb_Level3.Enabled = False
        Me.uc_cmb_Level4.Enabled = False

        Me.uc_entry_levels_days_counting_points.Enabled = False

        Me.chk_expiration_day_month.Enabled = False
        Me.chk_max_days_no_activity.Enabled = False
        Me.dtp_expiration_day_month.Enabled = False

        'Me.uc_entry_points_expiration.Enabled = False
        'Me.chk_expiration_day_month_points.Enabled = False
        'Me.dtp_expiration_day_month_points.Enabled = False

        Me.uc_entry_gift_voucher_expiration.Enabled = False

        Me.chk_max_points.Enabled = False
        Me.uc_max_points.Enabled = False
        Me.chk_max_points_per_minute.Enabled = False
        Me.uc_max_points_per_minute.Enabled = False
        Me.chk_has_zero_plays.Enabled = False
        Me.chk_has_mismatch.Enabled = False
      End If

      If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then

        Exit Sub
      End If

      uc_cmb_cashier_type.Value = cashier_configuration.CashierType()

      uc_wcp_cashier_config.CardPrice = GUI_DBNumberToLocalNumber(cashier_configuration.CardPrice)
      uc_wcp_cashier_config.PersonalCardPrice = GUI_DBNumberToLocalNumber(cashier_configuration.PersonalCardPrice)
      uc_wcp_cashier_config.PersonalCardReplacementPrice = GUI_DBNumberToLocalNumber(cashier_configuration.PersonalCardReplacementPrice)
      'Uc_wcp_cashier_config1.PersonalCardReplacementPriceInPoints = GUI_DBNumberToLocalNumber(cashier_configuration.PersonalCardReplacementPriceInPoints)
      uc_wcp_cashier_config.PersonalCardReplacementPriceInPoints = "0"
      uc_wcp_cashier_config.PersonalCardReplacementChargeAfterTimes = GUI_DBNumberToLocalNumber(cashier_configuration.PersonalCardReplacementChargeAfterTimes)
      uc_wcp_cashier_config.CardRefundable = cashier_configuration.CardRefundable
      uc_wcp_cashier_config.CashierMode = cashier_configuration.CashierMode
      uc_wcp_cashier_config.Language = cashier_configuration.Language
      uc_wcp_cashier_config.MinTimeToCloseSession = GUI_DBNumberToLocalNumber(cashier_configuration.MinTimeToCloseSession)
      uc_wcp_cashier_config.VoucherHeader = cashier_configuration.VoucherHeader
      uc_wcp_cashier_config.VoucherFooter = cashier_configuration.VoucherFooter
      uc_wcp_cashier_config.CardPrintTopX = GUI_DBNumberToLocalNumber(cashier_configuration.CardPrintTopX)
      uc_wcp_cashier_config.CardPrintTopY = GUI_DBNumberToLocalNumber(cashier_configuration.CardPrintTopY)
      uc_wcp_cashier_config.CardPrintBottomX = GUI_DBNumberToLocalNumber(cashier_configuration.CardPrintBottomX)
      uc_wcp_cashier_config.CardPrintBottomY = GUI_DBNumberToLocalNumber(cashier_configuration.CardPrintBottomY)
      uc_wcp_cashier_config.CardPrintFontSize = GUI_DBNumberToLocalNumber(cashier_configuration.CardPrintFontSize)
      uc_wcp_cashier_config.CardPrinterName = cashier_configuration.CardPrinterName
      uc_wcp_cashier_config.CardPrintBold = cashier_configuration.CardPrintBold
      uc_wcp_cashier_config.AllowAnon = cashier_configuration.AllowAnon
      uc_wcp_cashier_config.HandPayTimePeriod = GUI_DBNumberToLocalNumber(cashier_configuration.HandPayTimePeriod)
      uc_wcp_cashier_config.HandPayCancelTime = GUI_DBNumberToLocalNumber(cashier_configuration.HandPayTimeToCancel)
      'Uc_wcp_cashier_config1.CashInSplitPct = GUI_DBNumberToLocalNumber(cashier_configuration.CashInSplitPct)
      'Uc_wcp_cashier_config1.CashInSplitName = cashier_configuration.CashInSplitName
      'Uc_wcp_cashier_config1.CashInSplitPrize = cashier_configuration.CashInSplitPrize

      ' PGJ 26-JUN-2013: Account customization voucher
      uc_wcp_cashier_config.VoucherFooterAccountsCustomization = cashier_configuration.VoucherFooterAccountsCustomize

      ' RCI 20-JAN-2012
      SetScreenProviders(cashier_configuration.PlayerTrackingPointsMultiplierProviders)

      ' MBF 28-SEP-2010 Taxes
      uc_business_a.BusinessName = cashier_configuration.TaxesBusinessAName
      uc_business_a.ConceptName = cashier_configuration.TaxesBusinessAConcept
      uc_business_a.CashInPct = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesBusinessACashInPct)
      uc_business_a.TaxName = cashier_configuration.TaxesBusinessATaxName
      uc_business_a.TaxPct = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesBusinessATaxPct)
      uc_business_a.SetVoucherText("Header", cashier_configuration.TaxesBusinessAVoucherHeader)
      uc_business_a.SetVoucherText("Footer", cashier_configuration.TaxesBusinessAVoucherFooter)
      uc_business_a.SetVoucherText("Footer.CashIn", cashier_configuration.TaxesBusinessAVoucherFooterCashIn)
      uc_business_a.SetVoucherText("Footer.Dev", cashier_configuration.TaxesBusinessAVoucherFooterDev)
      uc_business_a.SetVoucherText("Footer.Cancel", cashier_configuration.TaxesBusinessAVoucherFooterCancel)
      uc_business_a.SplitB = (cashier_configuration.TaxesBusinessBEnable <> "0")
      uc_business_a.PrizeCoupon = (cashier_configuration.TaxesBusinessAPrizeCoupon <> "0")
      uc_business_a.HidePromo = (cashier_configuration.TaxesBusinessAHidePromo <> "0")
      uc_business_a.TextPromotion = cashier_configuration.TaxesBusinessAPromotionText
      uc_business_a.TextPrizeCoupon = cashier_configuration.TaxesBusinessAPrizeCouponText
      uc_business_a.HideTotalCashIn = (cashier_configuration.TaxesBusinessAHideTotalCashIn <> "0")
      ' JAB 21-MAR-2012 Promo and CouponTogether
      uc_business_a.PromoAndCouponTogether = (cashier_configuration.TaxesBusinessAPromoAndCouponTogether <> "0")
      uc_business_a.PromoAndCouponTogetherText = cashier_configuration.TaxesBusinessAPromoAndCouponTogetherText
      uc_business_a.PayTitle = cashier_configuration.TaxesBusinessAPayTitle
      uc_business_a.DevTitle = cashier_configuration.TaxesBusinessADevTitle

      uc_business_b.BusinessName = cashier_configuration.TaxesBusinessBName
      uc_business_b.ConceptName = cashier_configuration.TaxesBusinessBConcept
      uc_business_b.CashInPct = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesBusinessBCashInPct)
      uc_business_b.TaxName = cashier_configuration.TaxesBusinessBTaxName
      uc_business_b.TaxPct = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesBusinessBTaxPct)
      uc_business_b.SetVoucherText("Header", cashier_configuration.TaxesBusinessBVoucherHeader)
      uc_business_b.SetVoucherText("Footer", cashier_configuration.TaxesBusinessBVoucherFooter)
      uc_business_b.SetVoucherText("Footer.CashIn", cashier_configuration.TaxesBusinessBVoucherFooterCashIn)
      uc_business_b.SetVoucherText("Footer.Dev", cashier_configuration.TaxesBusinessBVoucherFooterDev)
      uc_business_b.SetVoucherText("Footer.Cancel", cashier_configuration.TaxesBusinessBVoucherFooterCancel)
      uc_business_b.HideTotalCashIn = (cashier_configuration.TaxesBusinessBHideTotalCashIn <> "0")
      uc_business_b.PayTitle = cashier_configuration.TaxesBusinessBPayTitle
      uc_business_b.DevTitle = cashier_configuration.TaxesBusinessBDevTitle

      chk_split_enabled.Checked = (cashier_configuration.TaxesBusinessBEnable <> "0")

      chk_split_enabled_CheckedChanged(Nothing, Nothing)

      Uc_entry_tax_1_on_won_pct.Value = GUI_DBNumberToLocalNumber(cashier_configuration.Taxes1Pct)
      Uc_entry_tax_2_on_won_pct.Value = GUI_DBNumberToLocalNumber(cashier_configuration.Taxes2Pct)
      Uc_entry_tax_3_on_won_pct.Value = GUI_DBNumberToLocalNumber(cashier_configuration.Taxes3Pct)
      Uc_entry_promo_tax_1_on_won_pct.Value = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesPromo1Pct)
      Uc_entry_promo_tax_2_on_won_pct.Value = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesPromo2Pct)
      Uc_entry_promo_tax_3_on_won_pct.Value = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesPromo3Pct)
      If cashier_configuration.EnableRedeemPromotionTaxes = 1 Then
        chk_enable_taxes_redeem_promotions.Checked = True
      Else
        chk_enable_taxes_redeem_promotions.Checked = False
      End If

      Uc_entry_tax_1_on_won_name.Value = cashier_configuration.Taxes1Name
      Uc_entry_tax_2_on_won_name.Value = cashier_configuration.Taxes2Name
      Uc_entry_tax_3_on_won_name.Value = cashier_configuration.Taxes3Name
      Uc_entry_tax_1_apply_if.Value = GUI_DBNumberToLocalNumber(cashier_configuration.Taxes1ApplyIf)
      Uc_entry_tax_2_apply_if.Value = GUI_DBNumberToLocalNumber(cashier_configuration.Taxes2ApplyIf)
      Uc_entry_tax_3_apply_if.Value = GUI_DBNumberToLocalNumber(cashier_configuration.Taxes3ApplyIf)
      Uc_entry_promo_tax_1_apply_if.Value = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesPromo1ApplyIf)
      Uc_entry_promo_tax_2_apply_if.Value = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesPromo2ApplyIf)
      Uc_entry_promo_tax_3_apply_if.Value = GUI_DBNumberToLocalNumber(cashier_configuration.TaxesPromo3ApplyIf)

      uc_alesis_cashier_config.DataBaseIp() = cashier_configuration.DataBaseIp()
      uc_alesis_cashier_config.DataBaseName() = cashier_configuration.DataBaseName()
      uc_alesis_cashier_config.VendorId() = cashier_configuration.VendorId()
      uc_alesis_cashier_config.Username() = cashier_configuration.Username()
      uc_alesis_cashier_config.Password() = cashier_configuration.Password()

      ' RCI 01-JUN-2010: Redraw = False. Speed up adding rows.
      uc_alesis_cashier_config.RedrawConfigured(False)
      For idx = 0 To cashier_configuration.ConfTerms.Count - 1
        temp_term = cashier_configuration.ConfTerms(idx)
        uc_alesis_cashier_config.AddConfiguredRow(temp_term.id, temp_term.name, temp_term.external_id, temp_term.machine_id, temp_term.status, idx)
      Next
      ' RCI 01-JUN-2010: Redraw = True.
      uc_alesis_cashier_config.RedrawConfigured(True)

      Me.id_evidence_configuration.CompanyID1 = cashier_configuration.EvidenceCompanyId1
      Me.id_evidence_configuration.CompanyID2 = cashier_configuration.EvidenceCompanyId2
      Me.id_evidence_configuration.CompanyName = cashier_configuration.EvidenceCompanyName
      Me.id_evidence_configuration.RepresentativeID1 = cashier_configuration.EvidenceRepresentativeId1
      Me.id_evidence_configuration.RepresentativeID2 = cashier_configuration.EvidenceRepresentativeId2
      Me.id_evidence_configuration.RepresentativeName = cashier_configuration.EvidenceRepresentativeName
      Me.id_evidence_configuration.RepresentativeSignature = cashier_configuration.EvidenceRepresentativeSign
      Me.id_evidence_configuration.EditableMinutes = cashier_configuration.EvidenceEditableMinutes
      Me.id_evidence_configuration.EvidenceEnabled = cashier_configuration.EvidenceEnabled
      Me.id_evidence_configuration.PrizeGratherThan = cashier_configuration.EvidencePrizeGreaterThan
      Me.id_evidence_configuration.GeneratedWitholdingDocument = cashier_configuration.GeneratedWitholdingDocument

      Me.uc_wcp_cashier_config.CardPlayerConcept = cashier_configuration.CardPlayerConcept
      Me.uc_wcp_cashier_config.CardPlayerAmountToCompanyB = cashier_configuration.CardPlayerAmountCompanyB

      ' CFDI
      SetCFDIScreenData(cashier_configuration)


      If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", True) Then
        ' PlayerLevelIcon
        SetPlayerLevelIcon(cashier_configuration)
      Else
        Dim _tab As New TabPage
        _tab = tab_control_points.TabPages(TAB_CONTROL_POINTS_PLAYER_LEVEL_ICON)
        tab_control_points.TabPages.Remove(_tab)
      End If


    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub ' GUI_SetScreenData

  ''' <summary>
  ''' Method to load player level icons
  ''' </summary>
  ''' <param name="CashierConfiguration"></param>
  ''' <remarks></remarks>
  Public Sub SetPlayerLevelIcon(ByVal CashierConfiguration As CLASS_CASHIER_CONFIGURATION)
    Dim _idx As Integer

    ' Set all PlayerLevelIcon controls by level
    For Each _level As PlayerLevelIcon.TypePlayerLevel In [Enum].GetValues(GetType(PlayerLevelIcon.TypePlayerLevel))

      If _level <> PlayerLevelIcon.TypePlayerLevel.NONE Then
        Me.SetPlayerLevelIconConfig(CashierConfiguration.PlayerLevelIcons.GetIconByLevel(_level), _idx)
      End If

      _idx = _idx + 1
    Next

  End Sub ' SetPlayerLevelIcon

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub  'GUI_Permissions

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim cashier_config As CLASS_CASHIER_CONFIGURATION
    ' add here new cashier class objects, once per control

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_CASHIER_CONFIGURATION
        cashier_config = DbEditedObject
        ' add here new cashier configurations, once per control

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Dim _comments As frm_comments

        cashier_config = Me.DbEditedObject

        _comments = New frm_comments(NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(495)), _
                                     NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(496)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(1)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(2)), _
                                     False) ' Comments

        Try
          If _comments.ShowGPDialog() = Windows.Forms.DialogResult.OK Then
            cashier_config.Comments = _comments.Comments
            Me.DbStatus = ENUM_STATUS.STATUS_OK
          Else
            cashier_config.Comments = ""
            Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED
          End If
        Finally
          _comments.Dispose()
        End Try

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_SUPPORTED Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          ' Call Me.DB_CheckAllCounters() DO NOTHING
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          cashier_config = Me.DbEditedObject
          cashier_config.Comments = ""
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim nls_id As Integer

    '     - CFDI
    If Not IsScreenDataOkCFDI() Then

      Return False
    End If

    If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then

      '     - PlayerTracking
      If Not IsScreenDataOkPlayerTracking() Then

        Return False
      End If

      Return True

    End If

    Select Case uc_cmb_cashier_type.Value
      Case CashlessMode.WIN
        ' Checking
        '     - WCP params
        '     - PlayerTracking
        '     - Taxes
        '     - Evidence

        '     - WCP params
        nls_id = uc_wcp_cashier_config.DataOk()
        If nls_id <> 0 Then
          tab_control.SelectedTab = TabWin
          Call uc_wcp_cashier_config.SetFocus(nls_id)
          Select Case nls_id
            Case 1421
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1421), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(263), TIME_PERIOD_LIMIT)

            Case Else
              ' 219 "Field '%1' is mandatory"
              Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(nls_id))
          End Select

          Return False
        End If

        '     - PlayerTracking
        If Not IsScreenDataOkPlayerTracking() Then

          Return False
        End If

        '     - Taxes
        If Not IsScreenDataOkTaxes() Then

          Return False
        End If

        '    - Evidence
        If Not IsScreenDataOkEvidence() Then

          Return False
        End If

      Case CashlessMode.ALESIS
        ' Checking
        '     - Alesis params

        '     - Alesis params
        nls_id = uc_alesis_cashier_config.DataOk()
        If nls_id <> 0 Then
          tab_control.SelectedTab = TabAlesis
          Call uc_alesis_cashier_config.SetFocus(nls_id)
          ' 219 "Field '%1' is mandatory"
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(nls_id))

          Return False
        End If

        If Not IsScreenDataOkEvidence() Then

          Return False
        End If

    End Select

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        Me.uc_alesis_cashier_config.KeyPressed(sender, e)

        If Me.dg_providers.ContainsFocus Then
          Me.dg_providers.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)
        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    If ButtonId = ENUM_BUTTON.BUTTON_CUSTOM_0 Then
      ApplyButton()
    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If

  End Sub ' GUI_ButtonClick

#End Region ' Overrides

#Region "Private Functions"

  Private Sub ApplyButton()

    ' 193 "Are you sure you want to save changes?"  
    If NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(193), _
                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
                  ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
      Exit Sub
    End If

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to cahnge the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
      Exit Sub
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

  End Sub ' ApplyButton

  ' PURPOSE : Initializes the CFDI tab's controls 
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub InitControlsCFDI()

    'CFDI labels
    Me.TabCFDI.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7649)
    Me.uc_entry_cfdi_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7767)
    Me.lbl_cfdi_url.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7650)
    Me.lbl_cfdi_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2696)
    Me.lbl_cfdi_password.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2697)
    Me.lbl_cfdi_scheduling_load.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7651)
    Me.lbl_cfdi_scheduling_process.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7655)
    Me.lbl_cfdi_minutes_load.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1583)
    Me.lbl_cfdi_minutes_process.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1583)
    Me.lbl_cfdi_retries.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7652)
    Me.lbl_cfdi_site.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7653)
    Me.gb_cfdi_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1063)
    Me.gb_cfdi_resume.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1403)
    Me.gb_special_account_data.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7775)
    Me.lbl_special_account_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7773)
    Me.lbl_special_account_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7772)
    Me.lbl_special_account_ef.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7774)

    'CFDI entrys
    Me.uc_entry_cfdi_url.SetFilter(FORMAT_TEXT, 200)
    Me.uc_entry_cfdi_user.SetFilter(FORMAT_TEXT, 20)
    Me.uc_entry_cfdi_password.SetFilter(FORMAT_TEXT, 20)
    Me.uc_entry_cfdi_scheduling_load.SetFilter(FORMAT_NUMBER, 4, 0)
    Me.uc_entry_cfdi_scheduling_process.SetFilter(FORMAT_NUMBER, 4, 0)
    Me.uc_entry_cfdi_retries.SetFilter(FORMAT_NUMBER, 2, 0)
    Me.uc_entry_cfdi_site.SetFilter(FORMAT_TEXT, 20)
    Me.uc_entry_cfdi_ef_anonymous.SetFilter(FORMAT_NUMBER, 2, 0)
    Me.uc_entry_cfdi_ef_foreign.SetFilter(FORMAT_NUMBER, 2, 0)
    Me.uc_entry_cfdi_rfc_anonymous.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    Me.uc_entry_cfdi_rfc_foreign.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)

    ' Scheduling
    Me.gb_scheduling.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(291)

    'CFDI Grid
    GUI_Style_CFDI_Resume()

    ' CFDI Scheduling info
    Call UpdateInfoText()

  End Sub ' InitControlsCFDI

  ''' <summary>
  ''' To set the Combo datasource
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindComboControls()

    Dim cashier_config_read As CLASS_CASHIER_CONFIGURATION
    cashier_config_read = Me.DbReadObject


    For Each _icon As PlayerLevelIcon In cashier_config_read.PlayerLevelIcons

      Me.cmb_player_level_icon_01.Add(_icon.Id, GLB_NLS_GUI_PLAYER_TRACKING.Id(_icon.NLSid))
      Me.cmb_player_level_icon_02.Add(_icon.Id, GLB_NLS_GUI_PLAYER_TRACKING.Id(_icon.NLSid))
      Me.cmb_player_level_icon_03.Add(_icon.Id, GLB_NLS_GUI_PLAYER_TRACKING.Id(_icon.NLSid))
      Me.cmb_player_level_icon_04.Add(_icon.Id, GLB_NLS_GUI_PLAYER_TRACKING.Id(_icon.NLSid))

    Next

  End Sub ' BindListBoxControls

  ' PURPOSE : Initializes the Player Tracking tab's controls 
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub InitControlsPlayerTracking()

    ' NLS
    Me.gb_level.Text = GLB_NLS_GUI_CONFIGURATION.GetString(256)                                     ' 256  "Niveles"

    ' FGB 11-FEB-2016
    'Me.gb_points_expiration.Text = GLB_NLS_GUI_CONFIGURATION.GetString(287)                         ' 287  "Caducidad de puntos"
    'Me.uc_entry_points_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6689)                ' 6689 "A los"
    'Me.uc_entry_points_expiration.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(846)            ' 846  "d�as sin actividad"
    'Me.chk_expiration_day_month_points.Checked = True
    'Me.chk_expiration_day_month_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6688)           ' 6689 "A fecha"
    'Me.dtp_expiration_day_month_points.Format = DateTimePickerFormat.Custom
    'Me.dtp_expiration_day_month_points.CustomFormat = "dd MMMM"
    'Me.uc_entry_points_expiration.TextVisible = True

    Me.gb_gift_expiration.Text = GLB_NLS_GUI_CONFIGURATION.GetString(288)                           ' 288  "Caducidad de solicitud de regalos"
    Me.uc_entry_gift_voucher_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6689)          ' 6689 "A los"
    Me.uc_entry_gift_voucher_expiration.SufixText = GLB_NLS_GUI_CONFIGURATION.GetString(286)        ' 286  "d�as"
    Me.uc_entry_levels_days_counting_points.Text = GLB_NLS_GUI_CONFIGURATION.GetString(335)         ' 335  "D�as a Contar Puntos"
    Me.uc_entry_levels_days_counting_points.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1441) ' 1441 "d�as (Per�odo de estudio)"

    Me.gb_playsession.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2855)
    Me.uc_max_points.TextVisible = True
    Me.uc_max_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2856)
    Me.uc_max_points.SufixTextVisible = True
    Me.uc_max_points.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2857)
    Me.uc_max_points_per_minute.TextVisible = True
    Me.uc_max_points_per_minute.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2856)
    Me.uc_max_points_per_minute.SufixTextVisible = True
    Me.uc_max_points_per_minute.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2858)
    Me.chk_has_zero_plays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2859)
    Me.chk_has_mismatch.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2860)

    Me.uc_entry_gift_voucher_expiration.TextVisible = True
    Me.uc_entry_levels_days_counting_points.TextVisible = True

    Me.lbl_01_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)                            ' 289 "B�sico"
    Me.lbl_02_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)                            ' 290 "Medio"
    Me.lbl_03_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)                            ' 291 "Alto"
    Me.lbl_04_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)                            ' 332 "Premium"

    Me.lbl_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(251)                               ' 251 "Nombre"

    ' RAB 15-JAN-2016
    'Me.lbl_redeem_played_to_1_point.Text = GLB_NLS_GUI_CONFIGURATION.GetString(252)           ' 252 "Redimible Jugado"
    'Me.lbl_total_played_to_1_point.Text = GLB_NLS_GUI_CONFIGURATION.GetString(253)            ' 253 "Total Jugado"
    'Me.lbl_redeem_spent_to_1_point.Text = GLB_NLS_GUI_CONFIGURATION.GetString(254)            ' 254 "Redimible Agotado"

    Me.lbl_points_to_enter.Text = GLB_NLS_GUI_CONFIGURATION.GetString(333)                    ' 333 "Puntos Entrada"

    Me.lbl_suffix_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(294)                          ' 294 "Se obtiene 1 punto por cada una de las cantidades anteriores" 

    ' RAB 15-JAN_2016
    'Me.lbl_example_title.Text = GLB_NLS_GUI_CONFIGURATION.GetString(297)
    Me.lbl_points_program.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7059)                  ' 7059 "La definici�n del programa de puntos se debe realizar en los Buckets. Notar que en los Buckets se han separado los puntos de nivel y de canje."
    Me.lbl_points_program.MaximumSize = New Size(200, 0)
    Me.lbl_points_program.TextAlign = ContentAlignment.BottomCenter
    Me.lbl_points_program.AutoSize = True


    ' RAB 15-JAN-2016
    'Me.lbl_example_more.Text = GLB_NLS_GUI_CONFIGURATION.GetString(296 _
    '                                                             , GUI_FormatCurrency(100, 2).PadLeft(10, " ") _
    '                                                             , GUI_FormatCurrency(500, 2).PadLeft(10, " ") _
    '                                                             , GUI_FormatCurrency(1000, 2).PadLeft(10, " "))


    ' RCI 20-JAN-2012
    If GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI Then
      Me.gb_providers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(567)
      GUI_StyleSheetProviders()

    ElseIf GLB_GuiMode = public_globals.ENUM_GUI.MULTISITE_GUI Then
      Me.gb_providers.Visible = False
    End If

    ' Formats
    uc_entry_gift_voucher_expiration.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_levels_days_counting_points.SetFilter(FORMAT_NUMBER, 3, 0)
    ' FGB 11-FEB-2016
    'uc_entry_points_expiration.SetFilter(FORMAT_NUMBER, 3, 0)

    uc_max_points.SetFilter(FORMAT_NUMBER, 7, 0)
    uc_max_points_per_minute.SetFilter(FORMAT_NUMBER, 7, 0)

    uc_entry_01_name.SetFilter(FORMAT_TEXT, 50)
    uc_entry_02_name.SetFilter(FORMAT_TEXT, 50)
    uc_entry_03_name.SetFilter(FORMAT_TEXT, 50)
    uc_entry_04_name.SetFilter(FORMAT_TEXT, 50)

    ' RAB 15-JAN-2016
    'uc_entry_01_redeem_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_02_redeem_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_03_redeem_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_04_redeem_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_01_total_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_02_total_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_03_total_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_04_total_played_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_01_redeem_spent_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_02_redeem_spent_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_03_redeem_spent_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)
    'uc_entry_04_redeem_spent_to_1_point.SetFilter(FORMAT_MONEY, 8, 2)

    uc_entry_02_points_to_enter.SetFilter(FORMAT_NUMBER, 8, 0)
    uc_entry_03_points_to_enter.SetFilter(FORMAT_NUMBER, 8, 0)
    uc_entry_04_points_to_enter.SetFilter(FORMAT_NUMBER, 8, 0)

    uc_entry_02_days_on_level.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_03_days_on_level.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_04_days_on_level.SetFilter(FORMAT_NUMBER, 3, 0)

    ' RRB 14-NOV-2012 Points to keep
    Me.gb_permanency.Text = GLB_NLS_GUI_CONFIGURATION.GetString(442)
    Me.lbl_points_to_keep_information.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1442)
    Me.gb_inactivity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1443)
    ' Points to keep
    Me.lbl_points_to_keep.Text = GLB_NLS_GUI_CONFIGURATION.GetString(440) ' "Puntos Permanecer"
    Me.uc_entry_02_points_to_keep.SetFilter(FORMAT_NUMBER, 8, 0)
    Me.uc_entry_03_points_to_keep.SetFilter(FORMAT_NUMBER, 8, 0)
    Me.uc_entry_04_points_to_keep.SetFilter(FORMAT_NUMBER, 8, 0)

    ' Expiration day month
    Me.chk_expiration_day_month.Checked = True
    Me.chk_expiration_day_month.Text = GLB_NLS_GUI_INVOICING.GetString(471) ' "Fecha Caducidad"
    Me.lbl_expiration_day_month.Enabled = True
    Me.lbl_expiration_day_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1440)
    Me.dtp_expiration_day_month.Format = DateTimePickerFormat.Custom
    Me.dtp_expiration_day_month.CustomFormat = "dd MMMM"

    ' Max No Activity Days
    Me.chk_max_days_no_activity.Checked = True
    Me.chk_max_days_no_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1438)
    Me.lbl_max_days_no_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1439)

    Me.uc_entry_02_max_days_no_activity.SetFilter(FORMAT_NUMBER, 3, 0)
    Me.uc_entry_03_max_days_no_activity.SetFilter(FORMAT_NUMBER, 3, 0)
    Me.uc_entry_04_max_days_no_activity.SetFilter(FORMAT_NUMBER, 3, 0)

  End Sub ' InitControlsPlayerTracking

  ' PURPOSE : Initializes the Taxes tab's controls 
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub InitControlsTaxes()

    Me.uc_business_a.SetBusiness("A")
    Me.uc_business_b.SetBusiness("B")
    Me.tab_business.TabPages(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " " & "A"
    Me.tab_business.TabPages(0).Name = "tbc_business" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " " & "A"
    Me.tab_business.TabPages(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " " & "B"
    Me.tab_business.TabPages(1).Name = "tbc_business" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " " & "B"

    'Me.gb_taxes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7142) '"Nombres"
    'Me.gb_prize_taxes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7139) '"Sobre premios"
    'Me.gb_promotion_taxes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7140) '"Sobre promociones en efectivo"
    Me.lbl_taxes_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7142) '"Nombre"
    Me.Uc_entry_tax_1_on_won_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367) ' "Nombre"
    Me.Uc_entry_tax_2_on_won_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367) ' "Nombre"
    Me.Uc_entry_tax_3_on_won_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367) ' "Nombre"
    Me.Uc_entry_tax_1_on_won_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    Me.Uc_entry_tax_2_on_won_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    Me.Uc_entry_tax_3_on_won_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    Me.Uc_entry_promo_tax_1_on_won_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    Me.Uc_entry_promo_tax_2_on_won_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    Me.Uc_entry_promo_tax_3_on_won_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    Me.Uc_entry_tax_1_apply_if.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(380)
    Me.Uc_entry_tax_2_apply_if.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(380)
    Me.Uc_entry_tax_3_apply_if.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(380)
    Me.Uc_entry_promo_tax_1_apply_if.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(380)
    Me.Uc_entry_promo_tax_2_apply_if.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(380)
    Me.Uc_entry_promo_tax_3_apply_if.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(380)
    Me.Uc_entry_tax_1_apply_if.SetFilter(FORMAT_MONEY, 8, 2)
    Me.Uc_entry_tax_2_apply_if.SetFilter(FORMAT_MONEY, 8, 2)
    Me.Uc_entry_tax_3_apply_if.SetFilter(FORMAT_MONEY, 8, 2)
    Me.Uc_entry_promo_tax_1_apply_if.SetFilter(FORMAT_MONEY, 8, 2)
    Me.Uc_entry_promo_tax_2_apply_if.SetFilter(FORMAT_MONEY, 8, 2)
    Me.Uc_entry_promo_tax_3_apply_if.SetFilter(FORMAT_MONEY, 8, 2)
    Me.chk_enable_taxes_redeem_promotions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7141)

    Me.Uc_entry_tax_1_on_won_name.SetFilter(FORMAT_TEXT, 50)
    Me.Uc_entry_tax_2_on_won_name.SetFilter(FORMAT_TEXT, 50)
    Me.Uc_entry_tax_3_on_won_name.SetFilter(FORMAT_TEXT, 50)

    Me.Uc_entry_tax_1_on_won_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    Me.Uc_entry_tax_2_on_won_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    Me.Uc_entry_tax_3_on_won_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    Me.Uc_entry_promo_tax_1_on_won_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    Me.Uc_entry_promo_tax_2_on_won_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    Me.Uc_entry_promo_tax_3_on_won_pct.SetFilter(FORMAT_NUMBER, 5, 2)

    Me.chk_split_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(370) ' "Habilitar segunda empresa"
    Me.chk_split_enabled.Checked = True

    Me.Uc_entry_tax_1_apply_if.TextVisible = True
    Me.Uc_entry_tax_2_apply_if.TextVisible = True
    Me.Uc_entry_tax_3_apply_if.TextVisible = True
    Me.Uc_entry_tax_1_on_won_name.TextVisible = True
    Me.Uc_entry_tax_2_on_won_name.TextVisible = True
    Me.Uc_entry_tax_3_on_won_name.TextVisible = True
    Me.Uc_entry_tax_1_on_won_pct.TextVisible = True
    Me.Uc_entry_tax_2_on_won_pct.TextVisible = True
    Me.Uc_entry_tax_3_on_won_pct.TextVisible = True

    Me.Uc_entry_promo_tax_1_apply_if.TextVisible = True
    Me.Uc_entry_promo_tax_2_apply_if.TextVisible = True
    Me.Uc_entry_promo_tax_3_apply_if.TextVisible = True
    Me.Uc_entry_promo_tax_1_on_won_pct.TextVisible = True
    Me.Uc_entry_promo_tax_2_on_won_pct.TextVisible = True
    Me.Uc_entry_promo_tax_3_on_won_pct.TextVisible = True

  End Sub ' InitControlsTaxes

  ' RAB 15-JAN-2016
  'Private Sub RefreshExample()

  '  Me.lbl_example_1.Text = Me.uc_entry_01_name.Value & ": " & _
  '                          GUI_FormatNumber(CalculateExample(GUI_ParseNumber(uc_entry_01_total_played_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_01_redeem_spent_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_01_redeem_played_to_1_point.Value)), 4)

  '  Me.lbl_example_2.Text = Me.uc_entry_02_name.Value & ": " & _
  '                          GUI_FormatNumber(CalculateExample(GUI_ParseNumber(uc_entry_02_total_played_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_02_redeem_spent_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_02_redeem_played_to_1_point.Value)), 4)

  '  Me.lbl_example_3.Text = Me.uc_entry_03_name.Value & ": " & _
  '                          GUI_FormatNumber(CalculateExample(GUI_ParseNumber(uc_entry_03_total_played_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_03_redeem_spent_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_03_redeem_played_to_1_point.Value)), 4)

  '  Me.lbl_example_4.Text = Me.uc_entry_04_name.Value & ": " & _
  '                          GUI_FormatNumber(CalculateExample(GUI_ParseNumber(uc_entry_04_total_played_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_04_redeem_spent_to_1_point.Value) _
  '                                                          , GUI_ParseNumber(uc_entry_04_redeem_played_to_1_point.Value)), 4)

  'End Sub ' RefreshExample

  ' PURPOSE: Define all Grid Points Multiplier Per Provider Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetProviders()
    With Me.dg_providers
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Provider Id
      .Column(GRID_COLUMN_PROVIDER_ID).Header.Text = ""
      .Column(GRID_COLUMN_PROVIDER_ID).Width = 0

      ' Provider Name
      .Column(GRID_COLUMN_PROVIDER_NAME).Header.Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER_NAME).Width = GRID_WIDTH_PROVIDER_NAME
      .Column(GRID_COLUMN_PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Multiplier
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(568)
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).Editable = True
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_POINTS_MULTIPLIER).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 10, 2)

    End With

  End Sub 'GUI_StyleSheetProviders

  ' PURPOSE: Define CFDI Grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_Style_CFDI_Resume()
    Dim _grid_columns As Integer = 3
    Dim _last_month_date As DateTime
    Dim _current_month_date As DateTime

    _last_month_date = WGDB.Now.AddMonths(-1)
    _current_month_date = WGDB.Now

    With Me.dg_cfdi_resume
      Call .Init(_grid_columns, GRID_HEADER_ROWS, True)

      ' Status
      .Column(GRID_COLUMN_CFDI_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6681)
      .Column(GRID_COLUMN_CFDI_STATUS).Width = GRID_WIDTH_CFDI_STATUS
      .Column(GRID_COLUMN_CFDI_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Last month
      .Column(GRID_COLUMN_CFDI_LAST_MONTH).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7654, _last_month_date.ToString("MMMM"), _last_month_date.ToString("yyyy"))
      .Column(GRID_COLUMN_CFDI_LAST_MONTH).Width = GRID_WIDTH_CFDI_TOTAL
      .Column(GRID_COLUMN_CFDI_LAST_MONTH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Current month
      .Column(GRID_COLUMN_CFDI_CURRENT_MONTH).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7654, _current_month_date.ToString("MMMM"), _current_month_date.ToString("yyyy"))
      .Column(GRID_COLUMN_CFDI_CURRENT_MONTH).Width = GRID_WIDTH_CFDI_TOTAL
      .Column(GRID_COLUMN_CFDI_CURRENT_MONTH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub 'GUI_StyleSheetProviders

  ' PURPOSE: Update the Providers DataTable with the multiplier value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Providers As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenProviders(ByVal Providers As DataTable)
    Dim _providers() As DataRow

    For _idx_row As Integer = 0 To Me.dg_providers.NumRows - 1
      _providers = Providers.Select("PV_ID = " & Me.dg_providers.Cell(_idx_row, GRID_COLUMN_PROVIDER_ID).Value)
      If _providers.Length = 1 Then
        If Me.dg_providers.Cell(_idx_row, GRID_COLUMN_POINTS_MULTIPLIER).Value <> "" Then
          If _providers(0)(CLASS_CASHIER_CONFIGURATION.SQL_COLUMN_POINTS_MULTIPLIER) <> Me.dg_providers.Cell(_idx_row, GRID_COLUMN_POINTS_MULTIPLIER).Value Then
            _providers(0)(CLASS_CASHIER_CONFIGURATION.SQL_COLUMN_POINTS_MULTIPLIER) = Me.dg_providers.Cell(_idx_row, GRID_COLUMN_POINTS_MULTIPLIER).Value
          End If
        Else
          ' RCI & AJQ 24-JAN-2012: Assign -1 only to detect changes, NOT to save value -1.
          _providers(0)(CLASS_CASHIER_CONFIGURATION.SQL_COLUMN_POINTS_MULTIPLIER) = -1
        End If
      End If
    Next
  End Sub ' GetScreenProviders

  ' PURPOSE: Set the uc_grid from the screen data with the values from the Providers DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Providers As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenProviders(ByVal Providers As DataTable)
    Dim _idx_row As Integer

    dg_providers.Redraw = False
    dg_providers.Clear()
    dg_providers.Redraw = True

    dg_providers.Redraw = False

    Try
      For Each _provider As DataRow In Providers.Rows
        dg_providers.AddRow()
        _idx_row = dg_providers.NumRows - 1

        dg_providers.Cell(_idx_row, GRID_COLUMN_PROVIDER_ID).Value = _provider(CLASS_CASHIER_CONFIGURATION.SQL_COLUMN_PROVIDER_ID)
        dg_providers.Cell(_idx_row, GRID_COLUMN_PROVIDER_NAME).Value = _provider(CLASS_CASHIER_CONFIGURATION.SQL_COLUMN_PROVIDER_NAME)
        dg_providers.Cell(_idx_row, GRID_COLUMN_POINTS_MULTIPLIER).Value = GUI_FormatNumber(_provider(CLASS_CASHIER_CONFIGURATION.SQL_COLUMN_POINTS_MULTIPLIER), 2)
      Next

    Finally
      dg_providers.Redraw = True
    End Try

  End Sub ' SetScreenProviders

  'RAB 15-JAN-2016
  'Private Function CalculateExample(ByVal TotalPlayed As Double, ByVal Spend As Double, ByVal Played As Double) As Double

  '  Dim _points As Double

  '  _points = 0

  '  If (TotalPlayed > 0) Then
  '    _points += 1000 / TotalPlayed
  '  End If

  '  If (Spend > 0) Then
  '    _points += 100 / Spend
  '  End If


  '  If (Played > 0) Then
  '    _points += 500 / Played
  '  End If

  '  Return _points
  'End Function ' CalculateExample

  Private Function IsScreenDataOkTaxes() As Boolean
    Dim _nls_id As Integer
    Dim _cash_in_pct As Decimal
    Dim _tax_pct As Decimal
    Dim _promo_tax_pct As Decimal

    ' To control that total percentage is equal to 100%
    If String.IsNullOrEmpty(uc_business_a.CashInPct) Then
      tab_control.SelectedTab = TabTaxes
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(233))
      Return False
    End If
    _cash_in_pct = GUI_LocalNumberToDBNumber(uc_business_a.CashInPct)
    If chk_split_enabled.Checked Then
      If String.IsNullOrEmpty(uc_business_b.CashInPct) Then
        tab_control.SelectedTab = TabTaxes
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(233))
        Return False
      End If
      _cash_in_pct += GUI_LocalNumberToDBNumber(uc_business_b.CashInPct)
    End If

    If _cash_in_pct <> 100.0 Then
      tab_control.SelectedTab = TabTaxes
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2069), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    _nls_id = uc_business_a.DataOk()
    If _nls_id <> 0 Then
      tab_control.SelectedTab = TabTaxes
      Call uc_business_a.SetFocus(_nls_id)
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id))

      Return False
    End If

    If chk_split_enabled.Checked Then
      _nls_id = uc_business_b.DataOk()
      If _nls_id <> 0 Then
        tab_control.SelectedTab = TabTaxes
        Call uc_business_b.SetFocus(_nls_id)
        ' 219 "Field '%1' is mandatory"
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(_nls_id))

        Return False
      End If
    End If

    If Uc_entry_tax_1_on_won_name.Value = "" Then
      tab_control.SelectedTab = TabTaxes
      Call Uc_entry_tax_1_on_won_name.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Uc_entry_tax_1_on_won_name.Text)

      Return False
    End If

    If Uc_entry_tax_2_on_won_name.Value = "" Then
      tab_control.SelectedTab = TabTaxes
      Call Uc_entry_tax_2_on_won_name.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Uc_entry_tax_2_on_won_name.Text)

      Return False
    End If

    _tax_pct = Uc_entry_tax_3_on_won_pct.Value
    _promo_tax_pct = Uc_entry_promo_tax_3_on_won_pct.Value

    If Uc_entry_tax_3_on_won_name.Value = "" And (_tax_pct <> 0 Or _promo_tax_pct <> 0) Then
      If (Uc_entry_tax_3_on_won_pct.Value > 0 Or Uc_entry_promo_tax_3_on_won_pct.Value > 0) Then
        tab_control.SelectedTab = TabTaxes
        Call Uc_entry_tax_3_on_won_name.Focus()

        ' 219 "Field '%1' is mandatory"
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Uc_entry_tax_3_on_won_name.Text)

        Return False
      End If
    End If

    ' Tax.OnPrize.1.Pct
    If String.IsNullOrEmpty(Me.Uc_entry_tax_1_on_won_pct.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPrize
      Call Uc_entry_tax_1_on_won_pct.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If

    ' Tax.OnPrize.2.Pct
    If String.IsNullOrEmpty(Me.Uc_entry_tax_2_on_won_pct.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPrize
      Call Uc_entry_tax_2_on_won_pct.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If

    ' Tax.OnPrize.3.Pct
    If String.IsNullOrEmpty(Me.Uc_entry_tax_3_on_won_pct.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPrize
      Call Uc_entry_tax_3_on_won_pct.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If

    ' Check OnPrize taxes don't exceed 100%
    _tax_pct = 0

    _tax_pct += Me.Uc_entry_tax_1_on_won_pct.Value
    _tax_pct += Me.Uc_entry_tax_2_on_won_pct.Value
    _tax_pct += Me.Uc_entry_tax_3_on_won_pct.Value

    If _tax_pct > 100.0 Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPrize
      Call Uc_entry_tax_1_on_won_pct.Focus()

      '7938  "Taxes % total can't exceed 100"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7938), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If

    ' Tax.OnPrize.1.Threshold
    If String.IsNullOrEmpty(Me.Uc_entry_tax_1_apply_if.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPrize
      Call Uc_entry_tax_1_apply_if.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(380))

      Return False
    End If

    ' Tax.OnPrize.2.Threshold
    If String.IsNullOrEmpty(Me.Uc_entry_tax_2_apply_if.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPrize
      Call Uc_entry_tax_2_apply_if.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(380))

      Return False
    End If

    ' Promotion.RE.Tax.1.Pct
    If String.IsNullOrEmpty(Me.Uc_entry_promo_tax_1_on_won_pct.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPromotions
      Call Uc_entry_promo_tax_1_on_won_pct.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If

    ' Promotion.RE.Tax.2.Pct
    If String.IsNullOrEmpty(Me.Uc_entry_promo_tax_2_on_won_pct.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPromotions
      Call Uc_entry_promo_tax_2_on_won_pct.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If

    ' Check Promotion taxes don't exceed 100%
    _tax_pct = 0

    _tax_pct += Me.Uc_entry_promo_tax_1_on_won_pct.Value
    _tax_pct += Me.Uc_entry_promo_tax_2_on_won_pct.Value
    _tax_pct += Me.Uc_entry_promo_tax_3_on_won_pct.Value

    If _tax_pct > 100.0 Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPromotions
      Call Uc_entry_promo_tax_1_on_won_pct.Focus()

      '7938  "Taxes % total can't exceed 100"
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7938), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(368))

      Return False
    End If


    ' Promotion.RE.Tax.1.Threshold()
    If String.IsNullOrEmpty(Me.Uc_entry_promo_tax_1_apply_if.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPromotions
      Call Uc_entry_promo_tax_1_apply_if.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(380))

      Return False
    End If

    ' Promotion.RE.Tax.2.Threshold()
    If String.IsNullOrEmpty(Me.Uc_entry_promo_tax_2_apply_if.Value) Then
      tab_control.SelectedTab = TabTaxes
      tab_taxes.SelectedTab = TabTaxesOnPromotions
      Call Uc_entry_promo_tax_2_apply_if.Focus()
      ' 219 "Field '%1' is mandatory"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(380))

      Return False
    End If

    Return True

  End Function ' IsScreenDataOkTaxes

  ' PURPOSE: Check all the multiplier values have a valid value. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean. True: all ok. False: otherwise.
  Private Function IsScreenDataOkPlayerTracking() As Boolean
    ' RRB 02-APR-2013
    ' RAB 15-JAN-2016
    '' Redeem spent to point
    'If String.IsNullOrEmpty(Me.uc_entry_01_redeem_spent_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(254))
    '  Call Me.uc_entry_01_redeem_spent_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_02_redeem_spent_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(254))
    '  Call Me.uc_entry_02_redeem_spent_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_03_redeem_spent_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(254))
    '  Call Me.uc_entry_03_redeem_spent_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_04_redeem_spent_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(254))
    '  Call Me.uc_entry_04_redeem_spent_to_1_point.Focus()

    '  Return False
    'End If

    '' Redeem played
    'If String.IsNullOrEmpty(Me.uc_entry_01_redeem_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(252))
    '  Call Me.uc_entry_01_redeem_played_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_02_redeem_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(252))
    '  Call Me.uc_entry_02_redeem_played_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_03_redeem_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(252))
    '  Call Me.uc_entry_03_redeem_played_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_04_redeem_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(252))
    '  Call Me.uc_entry_04_redeem_played_to_1_point.Focus()

    '  Return False
    'End If

    '' Total played
    'If String.IsNullOrEmpty(Me.uc_entry_01_total_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(253))
    '  Call Me.uc_entry_01_total_played_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_02_total_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(253))
    '  Call Me.uc_entry_02_total_played_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_03_total_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(253))
    '  Call Me.uc_entry_03_total_played_to_1_point.Focus()

    '  Return False
    'End If

    'If String.IsNullOrEmpty(Me.uc_entry_04_total_played_to_1_point.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(253))
    '  Call Me.uc_entry_04_total_played_to_1_point.Focus()

    '  Return False
    'End If

    ' Points To Enter
    If String.IsNullOrEmpty(Me.uc_entry_02_points_to_enter.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(333))
      Call Me.uc_entry_02_points_to_enter.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_entry_03_points_to_enter.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(333))
      Call Me.uc_entry_03_points_to_enter.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_entry_04_points_to_enter.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(333))
      Call Me.uc_entry_04_points_to_enter.Focus()

      Return False
    End If

    ' Points To Keep
    If String.IsNullOrEmpty(Me.uc_entry_02_points_to_keep.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(440))
      Call Me.uc_entry_02_points_to_keep.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_entry_03_points_to_keep.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(440))
      Call Me.uc_entry_03_points_to_keep.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_entry_04_points_to_keep.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(440))
      Call Me.uc_entry_04_points_to_keep.Focus()

      Return False
    End If

    ' Days on level
    If String.IsNullOrEmpty(Me.uc_entry_02_days_on_level.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_days_on_level.Text)
      Call Me.uc_entry_02_days_on_level.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_entry_03_days_on_level.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_days_on_level.Text)
      Call Me.uc_entry_03_days_on_level.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_entry_04_days_on_level.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_days_on_level.Text)
      Call Me.uc_entry_04_days_on_level.Focus()

      Return False
    End If

    ' Levels days counting points
    If String.IsNullOrEmpty(Me.uc_entry_levels_days_counting_points.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(448))
      Call Me.uc_entry_levels_days_counting_points.Focus()

      Return False
    End If

    ' Points expiration
    ' FGB 11-FEB-2016
    'If String.IsNullOrEmpty(Me.uc_entry_points_expiration.Value) Then
    '  tab_control.SelectedTab = TabPoints
    '  Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(287))
    '  Call Me.uc_entry_points_expiration.Focus()
    '  Return False
    'End If

    ' Gift voucher expiration
    If String.IsNullOrEmpty(Me.uc_entry_gift_voucher_expiration.Value) Then
      tab_control.SelectedTab = TabPoints
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_CONFIGURATION.GetString(288))
      Call Me.uc_entry_gift_voucher_expiration.Focus()

      Return False
    End If

    ' Max points
    If Me.chk_max_points.Checked Then
      If String.IsNullOrEmpty(Me.uc_max_points.Value) Then
        tab_control.SelectedTab = TabPoints
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2857))
        Call Me.uc_max_points.Focus()

        Return False
      ElseIf Not Me.uc_max_points.Value > 0 Then
        tab_control.SelectedTab = TabPoints
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2857))
        Call Me.uc_max_points.Focus()

        Return False
      End If
    End If

    ' Max points per minute
    If Me.chk_max_points_per_minute.Checked Then
      If String.IsNullOrEmpty(Me.uc_max_points_per_minute.Value) Then
        tab_control.SelectedTab = TabPoints
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2858))
        Call Me.uc_max_points_per_minute.Focus()

        Return False
      ElseIf Not Me.uc_max_points_per_minute.Value > 0 Then
        tab_control.SelectedTab = TabPoints
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2858))
        Call Me.uc_max_points_per_minute.Focus()

        Return False
      End If
    End If

    ' Points to Enter - Points to Keep
    If GUI_ParseNumber(Me.uc_entry_02_points_to_enter.Value) >= GUI_ParseNumber(Me.uc_entry_03_points_to_enter.Value) Then
      tab_control.SelectedTab = TabPoints
      Me.uc_entry_03_points_to_enter.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1437), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_CONFIGURATION.GetString(333), Me.uc_entry_03_name.Value, Me.uc_entry_02_name.Value)

      Return False
    End If

    If GUI_ParseNumber(Me.uc_entry_03_points_to_enter.Value) >= GUI_ParseNumber(Me.uc_entry_04_points_to_enter.Value) Then
      tab_control.SelectedTab = TabPoints
      Me.uc_entry_04_points_to_enter.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1437), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_CONFIGURATION.GetString(333), Me.uc_entry_04_name.Value, Me.uc_entry_03_name.Value)

      Return False
    End If

    If GUI_ParseNumber(Me.uc_entry_02_points_to_enter.Value) < GUI_ParseNumber(Me.uc_entry_02_points_to_keep.Value) Then
      tab_control.SelectedTab = TabPoints
      Me.uc_entry_02_points_to_keep.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1436), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_CONFIGURATION.GetString(440), GLB_NLS_GUI_CONFIGURATION.GetString(333))

      Return False
    End If

    If GUI_ParseNumber(Me.uc_entry_03_points_to_enter.Value) < GUI_ParseNumber(Me.uc_entry_03_points_to_keep.Value) Then
      tab_control.SelectedTab = TabPoints
      Me.uc_entry_03_points_to_keep.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1436), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_CONFIGURATION.GetString(440), GLB_NLS_GUI_CONFIGURATION.GetString(333))

      Return False
    End If

    If GUI_ParseNumber(Me.uc_entry_04_points_to_enter.Value) < GUI_ParseNumber(Me.uc_entry_04_points_to_keep.Value) Then
      tab_control.SelectedTab = TabPoints
      Me.uc_entry_04_points_to_keep.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1436), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_CONFIGURATION.GetString(440), GLB_NLS_GUI_CONFIGURATION.GetString(333))

      Return False
    End If

    ' Max days no activity
    If Me.chk_max_days_no_activity.Checked Then
      If Me.uc_entry_02_max_days_no_activity.Value <> "" Then
        If GUI_ParseNumber(Me.uc_entry_02_max_days_no_activity.Value) < 1 Then
          tab_control.SelectedTab = TabPoints
          Me.uc_entry_02_max_days_no_activity.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1439))

          Return False
        End If
      End If

      If Me.uc_entry_03_max_days_no_activity.Value <> "" Then
        If GUI_ParseNumber(Me.uc_entry_03_max_days_no_activity.Value) < 1 Then
          tab_control.SelectedTab = TabPoints
          Me.uc_entry_03_max_days_no_activity.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1439))

          Return False
        End If
      End If

      If Me.uc_entry_04_max_days_no_activity.Value <> "" Then
        If GUI_ParseNumber(Me.uc_entry_04_max_days_no_activity.Value) < 1 Then
          tab_control.SelectedTab = TabPoints
          Me.uc_entry_04_max_days_no_activity.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1439))

          Return False
        End If
      End If

      If String.IsNullOrEmpty(Me.uc_entry_02_max_days_no_activity.Value & _
                              Me.uc_entry_03_max_days_no_activity.Value & _
                              Me.uc_entry_04_max_days_no_activity.Value) Then
        tab_control.SelectedTab = TabPoints
        Me.uc_entry_02_max_days_no_activity.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1444), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1439))

        Return False
      End If
    End If

    If GLB_GuiMode <> ENUM_GUI.MULTISITE_GUI Then

      For _idx_row As Integer = 0 To Me.dg_providers.NumRows - 1
        If Me.dg_providers.Cell(_idx_row, GRID_COLUMN_POINTS_MULTIPLIER).Value = "" Then
          tab_control.SelectedTab = TabPoints
          Call dg_providers.Focus()
          dg_providers.ClearSelection()
          dg_providers.CurrentRow = _idx_row
          dg_providers.CurrentCol = GRID_COLUMN_POINTS_MULTIPLIER
          dg_providers.IsSelected(_idx_row) = True
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(136), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)

          Return False
        End If
      Next

    End If

    Return True
  End Function ' IsScreenDataOkPlayerTracking


  ' PURPOSE: Check CFDI field values. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean. True: all ok. False: otherwise.
  Private Function IsScreenDataOkCFDI() As Boolean

    ' Check if CFDI Is enabled
    If Not GeneralParam.GetBoolean("CFDI", "Enabled", False) OrElse Me.uc_entry_cfdi_active.Checked Then
      Return True
    End If

    ' Sheduling Load
    If GUI_ParseNumber(Me.uc_entry_cfdi_scheduling_load.Value) <= 0 Then
      tab_control.SelectedTab = TabCFDI
      Me.uc_entry_cfdi_scheduling_load.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7651))

      Return False
    End If


    ' Sheduling Process
    If GUI_ParseNumber(Me.uc_entry_cfdi_scheduling_process.Value) <= 0 Then
      tab_control.SelectedTab = TabCFDI
      Me.uc_entry_cfdi_scheduling_process.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7655))

      Return False
    End If


    ' Retries
    If GUI_ParseNumber(Me.uc_entry_cfdi_retries.Value) <= 0 Then
      tab_control.SelectedTab = TabCFDI
      Me.uc_entry_cfdi_retries.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(7652))

      Return False
    End If

    ' CFDI.Anonymous.State
    If String.IsNullOrEmpty(Me.uc_entry_cfdi_ef_anonymous.Value) Then
      tab_control.SelectedTab = TabCFDI
      Me.uc_entry_cfdi_ef_anonymous.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.uc_entry_cfdi_ef_anonymous.Text)

      Return False
    End If

    ' CFDI.Foreign.State
    If String.IsNullOrEmpty(Me.uc_entry_cfdi_ef_foreign.Value) Then
      tab_control.SelectedTab = TabCFDI
      Me.uc_entry_cfdi_ef_foreign.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.uc_entry_cfdi_ef_foreign.Text)

      Return False
    End If

    Return True

  End Function ' IsScreenDataOkCFDI

  Private Function IsScreenDataOkEvidence() As Boolean
    Dim nls_param1 As String
    Dim nls_param2 As String

    'If evidence disabled, Save values without verify the field values 
    If Not Me.id_evidence_configuration.EvidenceEnabled Then

      Return True
    End If

    'Verify fields are not empty, except the entity Id2 (Company - Curp) that can be empty
    If Me.id_evidence_configuration.EditableMinutesIsNullOrEmpty Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.id_evidence_configuration.EditableMinutesText)
      Call Me.id_evidence_configuration.SetFocusEditableMinutes()

      Return False
    End If

    If Me.id_evidence_configuration.PrizeGratherThanIsNullOrEmpty Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.id_evidence_configuration.PrizeGratherThanText)
      Call Me.id_evidence_configuration.SetFocusOnPrizeGratherThan()

      Return False
    End If

    If String.IsNullOrEmpty(Me.id_evidence_configuration.CompanyID1) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.id_evidence_configuration.CompanyID1Text)
      Call Me.id_evidence_configuration.SetFocusCompanyId1()

      Return False
    End If

    If String.IsNullOrEmpty(Me.id_evidence_configuration.CompanyName) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.id_evidence_configuration.CompanyNameText)
      Call Me.id_evidence_configuration.SetFocusCompanyName()

      Return False
    End If

    If String.IsNullOrEmpty(Me.id_evidence_configuration.RepresentativeID1) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.id_evidence_configuration.RepresentativeID1Text)
      Call Me.id_evidence_configuration.SetFocusRepresentativeId1()

      Return False
    End If

    If String.IsNullOrEmpty(Me.id_evidence_configuration.RepresentativeName) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.id_evidence_configuration.RepresentativeNameText)
      Call Me.id_evidence_configuration.SetFocusRepresentativeName()

      Return False
    End If

    'Verify Editable Minutes is less than MAX_EDIT_TIME
    If Me.id_evidence_configuration.EditableMinutes > TIME_LIMIT_EDITABLE_EVIDENCE Then
      nls_param1 = Me.id_evidence_configuration.EditableMinutesText
      nls_param2 = GUI_FormatNumber(TIME_LIMIT_EDITABLE_EVIDENCE, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, 0)
      tab_control.SelectedTab = TabEvidence

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(338), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1, nls_param2)

      Me.id_evidence_configuration.SetFocusEditableMinutes()

      Return False
    End If

    Return Me.id_evidence_configuration.ValidateFormat()
  End Function

  Private Sub UpdateControlTabs()
    If GLB_GuiMode = ENUM_GUI.MULTISITE_GUI Then
      Me.tab_control.Controls.Clear()
      Me.tab_control.Controls.Add(TabPoints)

      Exit Sub
    End If

    Select Case uc_cmb_cashier_type.Value
      Case CashlessMode.WIN
        Me.tab_control.Controls.Clear()
        Me.tab_control.Controls.Add(TabWin)
        Me.tab_control.Controls.Add(TabTaxes)
        Me.tab_control.Controls.Add(TabEvidence)
        Me.tab_control.Controls.Add(TabPoints)

        If (GeneralParam.GetBoolean("CFDI", "Enabled", False)) Then
          Me.tab_control.Controls.Add(TabCFDI)
        End If
      Case CashlessMode.ALESIS
        Me.tab_control.Controls.Clear()
        Me.tab_control.Controls.Add(TabAlesis)

    End Select

  End Sub

  ' PURPOSE: Fill LevelUpgrade/Downgrade combos with its values 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub FillLevelCombos(ByVal combo_level As uc_combo)
    combo_level.Add(UpgradeDowngradeAction.None, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2572)) '0 - None
    combo_level.Add(UpgradeDowngradeAction.BlockOnEnter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2573)) '1 - enter
    combo_level.Add(UpgradeDowngradeAction.BlockOnLeave, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2574)) '2 - leave
    combo_level.Add(UpgradeDowngradeAction.Both, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2575)) '3 - both
  End Sub ' FillLevelCombos

  ' PURPOSE: Fill resume of CFDI operations (last month, current month) 
  '
  '  PARAMS:
  '     - INPUT:
  '           - cashier_configuration
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Private Sub SetScreenCFDIResume(CashierConfiguration As CLASS_CASHIER_CONFIGURATION)
    Dim _total_current_month As CLASS_CASHIER_CONFIGURATION.TYPE_CFDI_REGISTERS_STATUS
    Dim _total_last_month As CLASS_CASHIER_CONFIGURATION.TYPE_CFDI_REGISTERS_STATUS

    If (GeneralParam.GetBoolean("CFDI", "Enabled", False)) Then
      _total_current_month = CashierConfiguration.CFDI_Resume_Current_Month()
      _total_last_month = CashierConfiguration.CFDI_Resume_Last_Month()

      dg_cfdi_resume.AddRow()
      dg_cfdi_resume.AddRow()
      dg_cfdi_resume.AddRow()

      dg_cfdi_resume.Cell(0, GRID_COLUMN_CFDI_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7656) 'Pending
      dg_cfdi_resume.Cell(1, GRID_COLUMN_CFDI_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7657) 'Errors
      dg_cfdi_resume.Cell(2, GRID_COLUMN_CFDI_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7658) 'Sent


      dg_cfdi_resume.Cell(0, GRID_COLUMN_CFDI_LAST_MONTH).Value = (_total_last_month.TotalPending + _total_last_month.TotalRetries) 'Pending
      dg_cfdi_resume.Cell(1, GRID_COLUMN_CFDI_LAST_MONTH).Value = _total_last_month.TotalError 'Errors
      dg_cfdi_resume.Cell(2, GRID_COLUMN_CFDI_LAST_MONTH).Value = _total_last_month.TotalSend 'Sent

      dg_cfdi_resume.Cell(0, GRID_COLUMN_CFDI_CURRENT_MONTH).Value = (_total_current_month.TotalPending + _total_current_month.TotalRetries) 'Pending
      dg_cfdi_resume.Cell(1, GRID_COLUMN_CFDI_CURRENT_MONTH).Value = _total_current_month.TotalError 'Errors
      dg_cfdi_resume.Cell(2, GRID_COLUMN_CFDI_CURRENT_MONTH).Value = _total_current_month.TotalSend 'Sent
    End If
  End Sub

  ''' <summary>
  ''' Update text info for scheduling parameters 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub UpdateInfoText()
    Dim _load_info As String
    Dim _process_info As String
    Dim _process_retries As String

    _load_info = IIf(IsDefaultThreadValue(uc_entry_cfdi_scheduling_load.Value), DEFAULT_VALUE_LOAD, uc_entry_cfdi_scheduling_load.Value)
    _process_info = IIf(IsDefaultThreadValue(uc_entry_cfdi_scheduling_process.Value), DEFAULT_VALUE_PROCESS, uc_entry_cfdi_scheduling_process.Value)
    _process_retries = IIf(IsDefaultThreadValue(uc_entry_cfdi_retries.Value), "0", uc_entry_cfdi_retries.Value)

    Me.lbl_info_load.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7687, _load_info)
    Me.lbl_info_process.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7688, _process_info, _process_retries)

  End Sub ' UpdateInfoText

  ''' <summary>
  ''' Check CFDI Thread default value
  ''' </summary>
  ''' <param name="Value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsDefaultThreadValue(ByVal Value As String) As Boolean
    Return (String.IsNullOrEmpty(Value) OrElse Value = "0")
  End Function

  ''' <summary>
  ''' Set CFD Screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetCFDIScreenData(ByVal CashierConfiguration As CLASS_CASHIER_CONFIGURATION)
    uc_entry_cfdi_active.Checked = IIf(CashierConfiguration.CFDI_Active = "1", True, False)
    uc_entry_cfdi_password.Value = CashierConfiguration.CFDI_Password
    uc_entry_cfdi_retries.Value = CashierConfiguration.CFDI_Retries
    uc_entry_cfdi_scheduling_load.Value = CashierConfiguration.CFDI_SchedulingLoad
    uc_entry_cfdi_scheduling_process.Value = CashierConfiguration.CFDI_SchedulingProcess
    uc_entry_cfdi_site.Value = CashierConfiguration.CFDI_Site
    uc_entry_cfdi_url.Value = CashierConfiguration.CFDI_Url
    uc_entry_cfdi_user.Value = CashierConfiguration.CFDI_User
    uc_entry_cfdi_rfc_anonymous.Value = CashierConfiguration.CFDI_RFC_Anonymous
    uc_entry_cfdi_ef_anonymous.Value = CashierConfiguration.CFDI_EF_Anonymous
    uc_entry_cfdi_ef_foreign.Value = CashierConfiguration.CFDI_EF_Foreign
    uc_entry_cfdi_rfc_foreign.Value = CashierConfiguration.CFDI_RFC_Foreign

    gb_cfdi_configuration.Enabled = Me.uc_entry_cfdi_active.Checked

    SetScreenCFDIResume(CashierConfiguration)
  End Sub

  ''' <summary>
  ''' Set player level icon configs
  ''' </summary>
  ''' <param name="level"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Sub SetPlayerLevelIconConfig(ByVal PlayerLevelIcon As PlayerLevelIcon, ByVal LevelId As Integer)

    Dim _combo As uc_combo
    Dim _picture As PictureBox
    Dim _combo_name As String
    Dim _picture_name As String

    _combo_name = String.Format(PLAYER_LEVEL_ICON_COMBO_PREFIX, LevelId)
    _picture_name = String.Format(PLAYER_LEVEL_ICON_PICTURE_PREFIX, LevelId)

    ' Set combo selected value
    _combo = Me.tab_control_points.TabPages(TAB_CONTROL_POINTS_PLAYER_LEVEL_ICON).Controls.Item(_combo_name)
    _combo.SelectedIndex = PlayerLevelIcon.Index

    ' Set combo selected value
    _picture = Me.tab_control_points.TabPages(TAB_CONTROL_POINTS_PLAYER_LEVEL_ICON).Controls.Item(_picture_name)
    _picture.Image = PlayerLevelIcon.Icon

  End Sub ' SetPlayerLevelIconConfig

  ''' <summary>
  ''' Get selected player level icons values
  ''' </summary>
  ''' <param name="PlayerLevelIcons"></param>
  ''' <remarks></remarks>
  Private Sub GetPlayerLevelIcons(ByRef PlayerLevelIcons As List(Of PlayerLevelIcon))

    Me.ResetPlayerLevelsIcon(PlayerLevelIcons)

    For Each _icon As PlayerLevelIcon In PlayerLevelIcons

      ' Level 1
      If _icon.Index = cmb_player_level_icon_01.SelectedIndex Then
        _icon.AddLevel(PlayerLevelIcon.TypePlayerLevel.LEVEL_01)
      End If

      ' Level 2
      If _icon.Index = cmb_player_level_icon_02.SelectedIndex Then
        _icon.AddLevel(PlayerLevelIcon.TypePlayerLevel.LEVEL_02)
      End If

      ' Level 3
      If _icon.Index = cmb_player_level_icon_03.SelectedIndex Then
        _icon.AddLevel(PlayerLevelIcon.TypePlayerLevel.LEVEL_03)
      End If

      ' Level 4
      If _icon.Index = cmb_player_level_icon_04.SelectedIndex Then
        _icon.AddLevel(PlayerLevelIcon.TypePlayerLevel.LEVEL_04)
      End If

    Next

  End Sub ' GetPlayerLevelIcons

  ''' <summary>
  ''' Reset all player icon levels
  ''' </summary>
  ''' <param name="PlayerLevelIcons"></param>
  ''' <remarks></remarks>
  Private Sub ResetPlayerLevelsIcon(ByRef PlayerLevelIcons As List(Of PlayerLevelIcon))

    For Each _iconn As PlayerLevelIcon In PlayerLevelIcons
      _iconn.Level = 0
    Next
  End Sub ' ResetPlayerLevelsIcon

  ''' <summary>
  ''' Set PlayerLevelIcon icon by selected index
  ''' </summary>
  ''' <param name="SelectedIndex"></param>
  ''' <param name="PlayerLevelIconPictureBox"></param>
  ''' <remarks></remarks>
  Private Sub SetPlayerLevelIcon(ByVal SelectedIndex As Integer, ByRef PlayerLevelIconPictureBox As PictureBox)
    Dim cashier_configuration As CLASS_CASHIER_CONFIGURATION

    cashier_configuration = Me.DbReadObject
    PlayerLevelIconPictureBox.Image = cashier_configuration.PlayerLevelIcons.GetIconByLevel(SelectedIndex).Icon()
  End Sub ' SetPlayerLevelIcon

#End Region ' Private Functions

#Region "Events"

  Private Sub frm_cashier_configuration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Call UpdateControlTabs()
  End Sub ' frm_cashier_configuration_Load

  Private Sub uc_cmb_cashier_type_ValueChangedEvent() Handles uc_cmb_cashier_type.ValueChangedEvent
    Call UpdateControlTabs()
  End Sub ' uc_cmb_cashier_type_ValueChangedEvent

  Private Sub chk_split_enabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_split_enabled.CheckedChanged

    If Not chk_split_enabled.Checked Then
      If tab_business.SelectedIndex = 1 Then
        tab_business.SelectedIndex = 0
      End If
      tab_business.TabPages.Remove(Me.TabPage2)
    Else
      If Not tab_business.Contains(Me.TabPage2) Then
        tab_business.TabPages.Add(Me.TabPage2)
      End If
    End If
    uc_business_a.SplitB = chk_split_enabled.Checked
  End Sub ' chk_split_enabled_CheckedChanged

  'RAB 15-JAN-2016
  'Private Sub uc_entry_01_redeem_spent_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_01_redeem_spent_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_02_redeem_spent_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_02_redeem_spent_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_03_redeem_spent_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_03_redeem_spent_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_04_redeem_spent_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_04_redeem_spent_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_01_redeem_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_01_redeem_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_02_redeem_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_02_redeem_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_03_redeem_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_03_redeem_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_04_redeem_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_04_redeem_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_01_total_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_01_total_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_02_total_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_02_total_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_03_total_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_03_total_played_to_1_point_EntryFieldValueChanged
  'Private Sub uc_entry_04_total_played_to_1_point_EntryFieldValueChanged()
  '  Call RefreshExample()
  'End Sub ' uc_entry_04_total_played_to_1_point_EntryFieldValueChanged

  Private Sub tab_business_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_business.SelectedIndexChanged
    If Not chk_split_enabled.Checked Then
      tab_business.SelectedIndex = 0
    End If
  End Sub ' tab_business_SelectedIndexChanged

  ' FGB 11-FEB-2016
  'Private Sub chk_expiration_day_month_points_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
  '  Me.dtp_expiration_day_month_points.Enabled = Me.chk_expiration_day_month_points.Checked
  'End Sub ' chk_expiration_day_month_points_CheckedChanged

  Private Sub chk_expiration_day_month_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_expiration_day_month.CheckedChanged
    If chk_expiration_day_month.Checked Then
      Me.dtp_expiration_day_month.Enabled = True
      Me.lbl_expiration_day_month.Enabled = True

      Me.lbl_days_on_level.Text = GLB_NLS_GUI_CONFIGURATION.GetString(443)                      ' 443 "M�nimo D�as de Permanencia"
    Else
      Me.dtp_expiration_day_month.Enabled = False
      Me.lbl_expiration_day_month.Enabled = False

      Me.lbl_days_on_level.Text = GLB_NLS_GUI_CONFIGURATION.GetString(441)                      ' 441 "D�as de Permanencia"
    End If

  End Sub ' chk_expiration_day_month_CheckedChanged

  Private Sub chk_max_no_activity_days_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_max_days_no_activity.CheckedChanged
    If chk_max_days_no_activity.Checked Then
      Me.uc_entry_02_max_days_no_activity.Enabled = True
      Me.uc_entry_03_max_days_no_activity.Enabled = True
      Me.uc_entry_04_max_days_no_activity.Enabled = True
    Else
      Me.uc_entry_02_max_days_no_activity.Enabled = False
      Me.uc_entry_03_max_days_no_activity.Enabled = False
      Me.uc_entry_04_max_days_no_activity.Enabled = False
    End If

  End Sub ' chk_max_no_activity_days_CheckedChanged
  'ATB 16-NOV-2016
  Private Sub chk_max_points_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_max_points.CheckedChanged
    If chk_max_points.Checked Then
      Me.uc_max_points.Enabled = True
    Else
      Me.uc_max_points.Enabled = False
    End If

  End Sub ' chk_max_points_CheckedChanged

  'ATB 16-NOV-2016
  Private Sub chk_max_points_per_minute_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_max_points_per_minute.CheckedChanged
    If chk_max_points_per_minute.Checked Then
      Me.uc_max_points_per_minute.Enabled = True
    Else
      Me.uc_max_points_per_minute.Enabled = False
    End If

  End Sub ' chk_max_points_per_minute_CheckedChanged

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Call UpdateInfoText()
  End Sub


  Private Sub uc_entry_cfdi_active_CheckedChanged(sender As Object, e As EventArgs) Handles uc_entry_cfdi_active.CheckedChanged
    gb_cfdi_configuration.Enabled = Me.uc_entry_cfdi_active.Checked
  End Sub

  Private Sub cmb_player_level_icon_01_ValueChangedEvent() Handles cmb_player_level_icon_01.ValueChangedEvent
    Me.SetPlayerLevelIcon(Me.cmb_player_level_icon_01.SelectedIndex, Me.pb_level_01)
  End Sub

  Private Sub cmb_player_level_icon_02_ValueChangedEvent() Handles cmb_player_level_icon_02.ValueChangedEvent
    Me.SetPlayerLevelIcon(Me.cmb_player_level_icon_02.SelectedIndex, Me.pb_level_02)
  End Sub

  Private Sub cmb_player_level_icon_03_ValueChangedEvent() Handles cmb_player_level_icon_03.ValueChangedEvent
    Me.SetPlayerLevelIcon(Me.cmb_player_level_icon_03.SelectedIndex, Me.pb_level_03)
  End Sub

  Private Sub cmb_player_level_icon_04_ValueChangedEvent() Handles cmb_player_level_icon_04.ValueChangedEvent
    Me.SetPlayerLevelIcon(Me.cmb_player_level_icon_04.SelectedIndex, Me.pb_level_04)
  End Sub
#End Region ' Events



End Class ' frm_cashier_configuration