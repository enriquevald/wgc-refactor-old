<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_pending
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_terminals_pending = New System.Windows.Forms.DataGrid
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.gb_terminal_pending = New System.Windows.Forms.GroupBox
    Me.cmb_source = New GUI_Controls.uc_combo
    Me.chk_hide_ignored = New System.Windows.Forms.CheckBox
    Me.ef_terminal_pending_serial_number = New GUI_Controls.uc_entry_field
    Me.ef_terminal_pending_vendor = New GUI_Controls.uc_entry_field
    Me.dg_terminals_added = New WigosGUI.DataGridWithDelete
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    CType(Me.dg_terminals_pending, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.gb_terminal_pending.SuspendLayout()
    CType(Me.dg_terminals_added, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.SplitContainer1)
    Me.panel_grids.Location = New System.Drawing.Point(4, 146)
    Me.panel_grids.Size = New System.Drawing.Size(1009, 533)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.SplitContainer1, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(921, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 533)
    Me.panel_buttons.TabIndex = 1
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_terminal_pending)
    Me.panel_filter.Size = New System.Drawing.Size(1009, 119)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal_pending, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 123)
    Me.pn_separator_line.Size = New System.Drawing.Size(1009, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1009, 4)
    '
    'dg_terminals_pending
    '
    Me.dg_terminals_pending.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_terminals_pending.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_pending.CaptionText = "xPendingTerminals"
    Me.dg_terminals_pending.DataMember = ""
    Me.dg_terminals_pending.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_pending.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_terminals_pending.Location = New System.Drawing.Point(0, 0)
    Me.dg_terminals_pending.Name = "dg_terminals_pending"
    Me.dg_terminals_pending.Size = New System.Drawing.Size(918, 310)
    Me.dg_terminals_pending.TabIndex = 0
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.dg_terminals_pending)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.dg_terminals_added)
    Me.SplitContainer1.Size = New System.Drawing.Size(921, 533)
    Me.SplitContainer1.SplitterDistance = 313
    Me.SplitContainer1.TabIndex = 0
    '
    'gb_terminal_pending
    '
    Me.gb_terminal_pending.Controls.Add(Me.cmb_source)
    Me.gb_terminal_pending.Controls.Add(Me.chk_hide_ignored)
    Me.gb_terminal_pending.Controls.Add(Me.ef_terminal_pending_serial_number)
    Me.gb_terminal_pending.Controls.Add(Me.ef_terminal_pending_vendor)
    Me.gb_terminal_pending.Location = New System.Drawing.Point(6, 6)
    Me.gb_terminal_pending.Name = "gb_terminal_pending"
    Me.gb_terminal_pending.Size = New System.Drawing.Size(279, 109)
    Me.gb_terminal_pending.TabIndex = 0
    Me.gb_terminal_pending.TabStop = False
    Me.gb_terminal_pending.Text = "xTerminalPending"
    '
    'cmb_source
    '
    Me.cmb_source.AllowUnlistedValues = False
    Me.cmb_source.AutoCompleteMode = False
    Me.cmb_source.IsReadOnly = False
    Me.cmb_source.Location = New System.Drawing.Point(133, 78)
    Me.cmb_source.Name = "cmb_source"
    Me.cmb_source.SelectedIndex = -1
    Me.cmb_source.Size = New System.Drawing.Size(200, 24)
    Me.cmb_source.SufixText = "Sufix Text"
    Me.cmb_source.SufixTextVisible = True
    Me.cmb_source.TabIndex = 3
    Me.cmb_source.Visible = False
    '
    'chk_hide_ignored
    '
    Me.chk_hide_ignored.Checked = True
    Me.chk_hide_ignored.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_hide_ignored.Location = New System.Drawing.Point(16, 79)
    Me.chk_hide_ignored.Name = "chk_hide_ignored"
    Me.chk_hide_ignored.Size = New System.Drawing.Size(221, 23)
    Me.chk_hide_ignored.TabIndex = 2
    Me.chk_hide_ignored.Text = "xHideIgnored"
    '
    'ef_terminal_pending_serial_number
    '
    Me.ef_terminal_pending_serial_number.DoubleValue = 0
    Me.ef_terminal_pending_serial_number.IntegerValue = 0
    Me.ef_terminal_pending_serial_number.IsReadOnly = False
    Me.ef_terminal_pending_serial_number.Location = New System.Drawing.Point(6, 48)
    Me.ef_terminal_pending_serial_number.Name = "ef_terminal_pending_serial_number"
    Me.ef_terminal_pending_serial_number.Size = New System.Drawing.Size(264, 24)
    Me.ef_terminal_pending_serial_number.SufixText = "Sufix Text"
    Me.ef_terminal_pending_serial_number.SufixTextVisible = True
    Me.ef_terminal_pending_serial_number.TabIndex = 1
    Me.ef_terminal_pending_serial_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_pending_serial_number.TextValue = ""
    Me.ef_terminal_pending_serial_number.TextWidth = 100
    Me.ef_terminal_pending_serial_number.Value = ""
    Me.ef_terminal_pending_serial_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal_pending_vendor
    '
    Me.ef_terminal_pending_vendor.DoubleValue = 0
    Me.ef_terminal_pending_vendor.IntegerValue = 0
    Me.ef_terminal_pending_vendor.IsReadOnly = False
    Me.ef_terminal_pending_vendor.Location = New System.Drawing.Point(6, 22)
    Me.ef_terminal_pending_vendor.Name = "ef_terminal_pending_vendor"
    Me.ef_terminal_pending_vendor.Size = New System.Drawing.Size(264, 24)
    Me.ef_terminal_pending_vendor.SufixText = "Sufix Text"
    Me.ef_terminal_pending_vendor.SufixTextVisible = True
    Me.ef_terminal_pending_vendor.TabIndex = 0
    Me.ef_terminal_pending_vendor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_pending_vendor.TextValue = ""
    Me.ef_terminal_pending_vendor.TextWidth = 100
    Me.ef_terminal_pending_vendor.Value = ""
    Me.ef_terminal_pending_vendor.ValueForeColor = System.Drawing.Color.Blue
    '
    'dg_terminals_added
    '
    Me.dg_terminals_added.CaptionFont = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_added.CaptionText = "xAddedTerminals"
    Me.dg_terminals_added.DataMember = ""
    Me.dg_terminals_added.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_terminals_added.HeaderFont = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_terminals_added.HeaderForeColor = System.Drawing.SystemColors.ControlText
    Me.dg_terminals_added.Location = New System.Drawing.Point(0, 0)
    Me.dg_terminals_added.Name = "dg_terminals_added"
    Me.dg_terminals_added.Size = New System.Drawing.Size(921, 216)
    Me.dg_terminals_added.TabIndex = 0
    '
    'frm_terminals_pending
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1017, 683)
    Me.Name = "frm_terminals_pending"
    Me.Text = "frm_terminals_pending"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    CType(Me.dg_terminals_pending, System.ComponentModel.ISupportInitialize).EndInit()
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.gb_terminal_pending.ResumeLayout(False)
    CType(Me.dg_terminals_added, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_terminals_pending As System.Windows.Forms.DataGrid
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents gb_terminal_pending As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_source As GUI_Controls.uc_combo
  Friend WithEvents chk_hide_ignored As System.Windows.Forms.CheckBox
  Friend WithEvents ef_terminal_pending_serial_number As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal_pending_vendor As GUI_Controls.uc_entry_field
  Friend WithEvents dg_terminals_added As WigosGUI.DataGridWithDelete
End Class
