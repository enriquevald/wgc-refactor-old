﻿'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_winup_notifications_sel
' DESCRIPTION:   
' AUTHOR:        Pablo Molina
' CREATION DATE: 18-JAN-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-JAN-2017  PDM    Initial version
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_winup_notifications_sel
  Inherits frm_base_sel

#Region "Members"

#End Region

#Region "Constants"

  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS_COUNT As Integer = 8

  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_DATE_FROM As Integer = 1
  Private Const GRID_COLUMN_DATE_TO As Integer = 2
  Private Const GRID_COLUMN_TITLE As Integer = 3
  Private Const GRID_COLUMN_MESSAGE As Integer = 4
  Private Const GRID_COLUMN_TARGET As Integer = 5
  Private Const GRID_COLUMN_STATUS As Integer = 6
  Private Const GRID_COLUMN_SCHEDULE As Integer = 7

  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_DATE_FROM As Integer = 1
  Private Const SQL_COLUMN_DATE_TO As Integer = 2
  Private Const SQL_COLUMN_TITLE As Integer = 3
  Private Const SQL_COLUMN_MESSAGE As Integer = 4
  Private Const SQL_COLUMN_TARGET As Integer = 5
  Private Const SQL_COLUMN_STATUS As Integer = 6
  Private Const SQL_COLUMN_SCHEDULE As Integer = 7

  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_DATE_FROM As Integer = 1500
  Private Const GRID_WIDTH_DATE_TO As Integer = 1500
  Private Const GRID_WIDTH_TITLE As Integer = 2000
  Private Const GRID_WIDTH_MESSAGE As Integer = 2500
  Private Const GRID_WIDTH_TARGET As Integer = 2000
  Private Const GRID_WIDTH_STATUS As Integer = 1200
  Private Const GRID_WIDTH_SCHEDULE As Integer = 1700

#End Region

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_WINUP_NOTIFICATION

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("SELECT   NO_ID_NOTIFICATION               ")
    _sb.AppendLine("        ,NS_DATE_FROM   AS NO_DATE_FROM   ")
    _sb.AppendLine("        ,NS_DATE_TO  AS NO_DATE_FROM      ")
    _sb.AppendLine("        ,NO_TITLE                         ")
    _sb.AppendLine("        ,NO_MESSAGE                       ")
    _sb.AppendLine("        ,NO_TARGET                        ")
    _sb.AppendLine("        ,NO_STATUS                        ")
    _sb.AppendLine("        ,NO_SCHEDULE                      ")
    _sb.AppendLine("  FROM   MAPP_NOTIFICATION                ")
    _sb.AppendLine("  LEFT JOIN  MAPP_NOTIFICATION_SCHEDULE   ")
    _sb.AppendLine(" NS ON  MAPP_NOTIFICATION.NO_ID_NOTIFICATION = NS.NS_ID_NOTIFICATION ")

    _sb.Append(GetSqlWhere())

    Return _sb.ToString()
  End Function


  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    ef_title.Value = ""
    chk_enable.Checked = False
    chk_disable.Checked = False

    If cmb_section.Count > 0 Then
      cmb_section.SelectedIndex = 0
    End If

    If cmb_section_type.Count > 0 Then
      cmb_section_type.SelectedIndex = 0
    End If


  End Sub ' GUI_FilterReset

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7866)

    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ef_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
    chk_enable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    chk_disable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    gb_enable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
    lbl_section.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7867)
    lbl_type_section.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7868)

    Me.ef_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, 1000)

    Call Me.LoadSections()

    Call Me.GUI_StyleSheet()
    Call Me.SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE : Fills the rows
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                        ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)

    If DbRow.Value(SQL_COLUMN_SCHEDULE) = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = "-"
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = "-"
    Else

      If IsDBNull(DbRow.Value(SQL_COLUMN_DATE_FROM)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = "-"
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_FROM), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      End If

      If IsDBNull(DbRow.Value(SQL_COLUMN_DATE_TO)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = "-"
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_TO), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
      End If

      'Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DATE_FROM)), "-", GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_FROM), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))
      'Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DATE_TO)), "-", GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_TO), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT))
      'Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DATE_FROM)), "-", DbRow.Value(SQL_COLUMN_DATE_FROM))
      'Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = IIf(IsDBNull(DbRow.Value(SQL_COLUMN_DATE_TO)), "-", DbRow.Value(SQL_COLUMN_DATE_TO))
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITLE).Value = DbRow.Value(SQL_COLUMN_TITLE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MESSAGE).Value = DbRow.Value(SQL_COLUMN_MESSAGE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TARGET).Value = DbRow.Value(SQL_COLUMN_TARGET)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = IIf(DbRow.Value(SQL_COLUMN_STATUS), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SCHEDULE).Value = IIf(DbRow.Value(SQL_COLUMN_SCHEDULE) = 0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7870), GLB_NLS_GUI_PLAYER_TRACKING.GetString(7871))

    Return True

  End Function

  ' PURPOSE: Process button actions 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        GUI_EditNewItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        'Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()
    'If Me.Permissions.Write Then

    Dim _idx_row As Short
    Dim _id As Integer
    Dim _frm_edit As frm_winup_notifications_edit

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    '' Get the  ID 
    _id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _frm_edit = New frm_winup_notifications_edit()

    Call _frm_edit.ShowEditItem(_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

    ' End If
  End Sub

  Protected Overrides Sub GUI_EditNewItem()

    Dim _frm_edit As frm_winup_notifications_edit
    _frm_edit = New frm_winup_notifications_edit()

    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub

#End Region

#Region "Private Functions"

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT)

      '' ID
      .Column(GRID_COLUMN_ID).Header.Text = " "
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID

      '' DATE FROM
      .Column(GRID_COLUMN_DATE_FROM).Header.Text = GLB_NLS_GUI_INVOICING.GetString(202)
      .Column(GRID_COLUMN_DATE_FROM).Width = GRID_WIDTH_DATE_FROM
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '' DATE TO
      .Column(GRID_COLUMN_DATE_TO).Header.Text = GLB_NLS_GUI_INVOICING.GetString(203)
      .Column(GRID_COLUMN_DATE_TO).Width = GRID_WIDTH_DATE_FROM
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '' TITLE
      .Column(GRID_COLUMN_TITLE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7522)
      .Column(GRID_COLUMN_TITLE).Width = GRID_WIDTH_TITLE
      .Column(GRID_COLUMN_TITLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '' MESSAGE
      .Column(GRID_COLUMN_MESSAGE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428)
      .Column(GRID_COLUMN_MESSAGE).Width = GRID_WIDTH_MESSAGE
      .Column(GRID_COLUMN_MESSAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '' MESSAGE
      .Column(GRID_COLUMN_TARGET).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7867)
      .Column(GRID_COLUMN_TARGET).Width = GRID_WIDTH_TARGET
      .Column(GRID_COLUMN_TARGET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '' MESSAGE
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '' MESSAGE
      .Column(GRID_COLUMN_SCHEDULE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7869)
      .Column(GRID_COLUMN_SCHEDULE).Width = GRID_WIDTH_SCHEDULE
      .Column(GRID_COLUMN_SCHEDULE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


    End With

  End Sub

  Private Sub LoadSections()

    Dim _class_base As CLASS_WINUP_NOTIFICATIONS

    _class_base = New CLASS_WINUP_NOTIFICATIONS

    cmb_section.Clear()

    cmb_section.Add(_class_base.GetSections())

    If cmb_section.Count > 0 Then
      cmb_section.SelectedIndex = 0
    End If

  End Sub


  Private Sub LoadTypeSections(ByVal IdSection As Integer)

    Dim _class_base As CLASS_WINUP_NOTIFICATIONS

    _class_base = New CLASS_WINUP_NOTIFICATIONS

    cmb_section_type.Clear()

    cmb_section_type.Add(_class_base.GetTypeSections(IdSection))

    If cmb_section_type.Count > 0 Then
      cmb_section_type.SelectedIndex = 0
    End If

  End Sub


  Private Sub SetDefaultValues()

    Me.dtp_from.Value = WSI.Common.Misc.TodayOpening()
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    ef_title.Value = ""
    chk_enable.Checked = False
    chk_disable.Checked = False

  End Sub

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _sb_where As StringBuilder
    Dim _date_from As String
    Dim _date_to As String

    _sb_where = New StringBuilder()
    _date_from = ""
    _date_to = ""

    ' Filter Dates
    _sb_where.Append(" WHERE 1=1 ")

    If Me.dtp_from.Checked Then

      _sb_where.Append(" AND NS_DATE_FROM >= " & GUI_FormatDayDB(dtp_from.Value))

    End If

    If Me.dtp_to.Checked Then
      _sb_where.Append(" AND NS_DATE_TO <= " & GUI_FormatDayDB(dtp_to.Value))
    End If

    If (chk_enable.Checked Xor chk_disable.Checked) Then
      If chk_enable.Checked Then
        _sb_where.Append(" AND   NO_STATUS = 1 ")
      ElseIf chk_disable.Checked Then
        _sb_where.Append(" AND   NO_STATUS = 0 ")
      End If
    End If

    If ef_title.Value <> String.Empty Then
      _sb_where.Append(" AND   NO_TITLE = '" & ef_title.Value & "'")
    End If

    If cmb_section.Value > 0 Then
      _sb_where.Append(" AND   NO_TARGET = '" & cmb_section.TextValue.ToString().Trim() & "'")
    End If

    If cmb_section_type.Value > 0 Then
      _sb_where.Append(" AND   NO_TARGET_IDENTIFIER = '" & cmb_section_type.Value & "'")
    End If

    Return _sb_where.ToString()

  End Function

#End Region

#Region "Events"

  Private Sub cmb_section_ValueChangedEvent() Handles cmb_section.ValueChangedEvent
    LoadTypeSections(cmb_section.Value)
  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Me.Display(False)

  End Sub ' ShowForEdit

#End Region


End Class