'-------------------------------------------------------------------
' Copyright © 2007 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_sales_terminal
' DESCRIPTION:   This screen allows to view the statistics between:
'                           - two dates 
'                           - grouped by daily, weekly or monthly and 
'                           - for all terminals or one terminal
'                           - detailed by terminal
' AUTHOR:        Agustí Poch
' CREATION DATE: 19-SEP-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-SEP-2007  APB    Initial version
' 04-APR-2012  JCM    NLS String "Varios" changed to NLS String "Multijuego"
' 08-JUN-2012  JAB    DoEvents run each second.
' 12-JUN-2012  JAB    DoEvents run each second (use common routine).
' 24-AUG-2012  JAB    Control to show huge amounts of records & DoEvents run each second.
' 18-OCT-2012  RRB    Show netwin % in excel.
' 03-DEC-2012  RRB    Add Average Bet column.
' 28-MAR-2013  RCI & HBB   Added column SPH_THEORETICAL_WON_AMOUNT in table SALES_PER_HOUR. Needed to calculate the theoretical payout %. Also added a column to the grid with the theoretical payout %
' 25-MAY-2013  JCA    Change to use new table providers_games
' 17-JUN-2013  NMR    Fixed Bug #863: Don't show message when user stops operation
' 19-JUN-2013  NMR    Fixed Bug #860: Control the correct number of rows to show/draw
' 18-AUG-2014  AMF    Progressive Jackpot
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 01-OCT-2014  LEM    Fixed Bug WIG-1361: Provider name is hidden when "Terminals" option is deactivated.
' 07-OCT-2014  ACC    Fixed bug WIG-1431: Estadísticas: Error subtotal proveedor y fecha en el netwin
' 17-OCT-2014  JMV    Fixed WIG-1431
' 11-NOV-2015  FOS    Product Backlog Item 5839:Garcia River change NLS
' 30-AUG-2016  LTC    Bug 16897: The field: "game" on report "Statics grouped by date/prov/term" is shown wrongly
' 03-OCT-2017  DHA    Bug 30041:WIGOS-5490 [Ticket #9175] Reporte en Estadísticas Agrupadas por Fecha, Proveedor y Terminal – Denominación Contable V03.06.0023
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_sales_date_terminal
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents rb_daily As System.Windows.Forms.RadioButton
  Friend WithEvents rb_weekly As System.Windows.Forms.RadioButton
  Friend WithEvents rb_monthly As System.Windows.Forms.RadioButton
  Friend WithEvents gb_game As System.Windows.Forms.GroupBox
  Friend WithEvents opt_several_game As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_game As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_game As GUI_Controls.uc_combo
  Friend WithEvents cb_rows As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents cb_grouped_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents cb_subtotal_date As System.Windows.Forms.CheckBox
  Friend WithEvents cb_grouped As System.Windows.Forms.CheckBox
  Friend WithEvents cb_total_provider As System.Windows.Forms.CheckBox
  Friend WithEvents cb_provider_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents cb_totalizer As System.Windows.Forms.CheckBox
  Friend WithEvents cb_subtotal_date_provider As System.Windows.Forms.CheckBox
  Friend WithEvents rb_24_hours As System.Windows.Forms.RadioButton
  Friend WithEvents rb_hourly As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_netwin As System.Windows.Forms.Label
  Friend WithEvents lbl_payout As System.Windows.Forms.Label
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_options = New System.Windows.Forms.GroupBox
    Me.rb_24_hours = New System.Windows.Forms.RadioButton
    Me.rb_hourly = New System.Windows.Forms.RadioButton
    Me.cb_subtotal_date_provider = New System.Windows.Forms.CheckBox
    Me.cb_total_provider = New System.Windows.Forms.CheckBox
    Me.cb_provider_terminals = New System.Windows.Forms.CheckBox
    Me.cb_totalizer = New System.Windows.Forms.CheckBox
    Me.cb_grouped = New System.Windows.Forms.CheckBox
    Me.cb_subtotal_date = New System.Windows.Forms.CheckBox
    Me.cb_grouped_terminals = New System.Windows.Forms.CheckBox
    Me.cb_rows = New System.Windows.Forms.CheckBox
    Me.rb_monthly = New System.Windows.Forms.RadioButton
    Me.rb_weekly = New System.Windows.Forms.RadioButton
    Me.rb_daily = New System.Windows.Forms.RadioButton
    Me.gb_game = New System.Windows.Forms.GroupBox
    Me.opt_several_game = New System.Windows.Forms.RadioButton
    Me.opt_all_game = New System.Windows.Forms.RadioButton
    Me.cmb_game = New GUI_Controls.uc_combo
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.lbl_netwin = New System.Windows.Forms.Label
    Me.lbl_payout = New System.Windows.Forms.Label
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.gb_game.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.lbl_payout)
    Me.panel_filter.Controls.Add(Me.lbl_netwin)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_game)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 228)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_game, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_netwin, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_payout, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 232)
    Me.panel_data.Size = New System.Drawing.Size(1226, 486)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.rb_24_hours)
    Me.gb_options.Controls.Add(Me.rb_hourly)
    Me.gb_options.Controls.Add(Me.cb_subtotal_date_provider)
    Me.gb_options.Controls.Add(Me.cb_total_provider)
    Me.gb_options.Controls.Add(Me.cb_provider_terminals)
    Me.gb_options.Controls.Add(Me.cb_totalizer)
    Me.gb_options.Controls.Add(Me.cb_grouped)
    Me.gb_options.Controls.Add(Me.cb_subtotal_date)
    Me.gb_options.Controls.Add(Me.cb_grouped_terminals)
    Me.gb_options.Controls.Add(Me.cb_rows)
    Me.gb_options.Controls.Add(Me.rb_monthly)
    Me.gb_options.Controls.Add(Me.rb_weekly)
    Me.gb_options.Controls.Add(Me.rb_daily)
    Me.gb_options.Location = New System.Drawing.Point(607, 8)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(441, 174)
    Me.gb_options.TabIndex = 3
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'rb_24_hours
    '
    Me.rb_24_hours.AutoSize = True
    Me.rb_24_hours.Location = New System.Drawing.Point(134, 65)
    Me.rb_24_hours.Name = "rb_24_hours"
    Me.rb_24_hours.Size = New System.Drawing.Size(76, 17)
    Me.rb_24_hours.TabIndex = 5
    Me.rb_24_hours.Text = "24 Hours"
    '
    'rb_hourly
    '
    Me.rb_hourly.AutoSize = True
    Me.rb_hourly.Location = New System.Drawing.Point(49, 65)
    Me.rb_hourly.Name = "rb_hourly"
    Me.rb_hourly.Size = New System.Drawing.Size(62, 17)
    Me.rb_hourly.TabIndex = 4
    Me.rb_hourly.Text = "Hourly"
    '
    'cb_subtotal_date_provider
    '
    Me.cb_subtotal_date_provider.AutoSize = True
    Me.cb_subtotal_date_provider.Location = New System.Drawing.Point(22, 109)
    Me.cb_subtotal_date_provider.Name = "cb_subtotal_date_provider"
    Me.cb_subtotal_date_provider.Size = New System.Drawing.Size(132, 17)
    Me.cb_subtotal_date_provider.TabIndex = 7
    Me.cb_subtotal_date_provider.Text = "xSubtotal provider"
    '
    'cb_total_provider
    '
    Me.cb_total_provider.AutoSize = True
    Me.cb_total_provider.Location = New System.Drawing.Point(297, 66)
    Me.cb_total_provider.Name = "cb_total_provider"
    Me.cb_total_provider.Size = New System.Drawing.Size(136, 17)
    Me.cb_total_provider.TabIndex = 12
    Me.cb_total_provider.Text = "xTotal per provider"
    '
    'cb_provider_terminals
    '
    Me.cb_provider_terminals.AutoSize = True
    Me.cb_provider_terminals.Location = New System.Drawing.Point(297, 43)
    Me.cb_provider_terminals.Name = "cb_provider_terminals"
    Me.cb_provider_terminals.Size = New System.Drawing.Size(89, 17)
    Me.cb_provider_terminals.TabIndex = 11
    Me.cb_provider_terminals.Text = "xTerminals"
    '
    'cb_totalizer
    '
    Me.cb_totalizer.AutoSize = True
    Me.cb_totalizer.Location = New System.Drawing.Point(283, 20)
    Me.cb_totalizer.Name = "cb_totalizer"
    Me.cb_totalizer.Size = New System.Drawing.Size(82, 17)
    Me.cb_totalizer.TabIndex = 10
    Me.cb_totalizer.Text = "xTotalizer"
    '
    'cb_grouped
    '
    Me.cb_grouped.AutoSize = True
    Me.cb_grouped.Location = New System.Drawing.Point(8, 20)
    Me.cb_grouped.Name = "cb_grouped"
    Me.cb_grouped.Size = New System.Drawing.Size(82, 17)
    Me.cb_grouped.TabIndex = 0
    Me.cb_grouped.Text = "xGrouped"
    '
    'cb_subtotal_date
    '
    Me.cb_subtotal_date.AutoSize = True
    Me.cb_subtotal_date.Location = New System.Drawing.Point(22, 131)
    Me.cb_subtotal_date.Name = "cb_subtotal_date"
    Me.cb_subtotal_date.Size = New System.Drawing.Size(109, 17)
    Me.cb_subtotal_date.TabIndex = 8
    Me.cb_subtotal_date.Text = "xSubtotal date"
    '
    'cb_grouped_terminals
    '
    Me.cb_grouped_terminals.AutoSize = True
    Me.cb_grouped_terminals.Location = New System.Drawing.Point(22, 87)
    Me.cb_grouped_terminals.Name = "cb_grouped_terminals"
    Me.cb_grouped_terminals.Size = New System.Drawing.Size(89, 17)
    Me.cb_grouped_terminals.TabIndex = 6
    Me.cb_grouped_terminals.Text = "xTerminals"
    '
    'cb_rows
    '
    Me.cb_rows.AutoSize = True
    Me.cb_rows.Location = New System.Drawing.Point(8, 153)
    Me.cb_rows.Name = "cb_rows"
    Me.cb_rows.Size = New System.Drawing.Size(63, 17)
    Me.cb_rows.TabIndex = 9
    Me.cb_rows.Text = "xRows"
    Me.cb_rows.UseVisualStyleBackColor = True
    '
    'rb_monthly
    '
    Me.rb_monthly.AutoSize = True
    Me.rb_monthly.Location = New System.Drawing.Point(168, 42)
    Me.rb_monthly.Name = "rb_monthly"
    Me.rb_monthly.Size = New System.Drawing.Size(69, 17)
    Me.rb_monthly.TabIndex = 3
    Me.rb_monthly.Text = "Monthly"
    '
    'rb_weekly
    '
    Me.rb_weekly.AutoSize = True
    Me.rb_weekly.Location = New System.Drawing.Point(89, 42)
    Me.rb_weekly.Name = "rb_weekly"
    Me.rb_weekly.Size = New System.Drawing.Size(67, 17)
    Me.rb_weekly.TabIndex = 2
    Me.rb_weekly.Text = "Weekly"
    '
    'rb_daily
    '
    Me.rb_daily.AutoSize = True
    Me.rb_daily.Location = New System.Drawing.Point(22, 42)
    Me.rb_daily.Name = "rb_daily"
    Me.rb_daily.Size = New System.Drawing.Size(54, 17)
    Me.rb_daily.TabIndex = 1
    Me.rb_daily.Text = "Daily"
    '
    'gb_game
    '
    Me.gb_game.Controls.Add(Me.opt_several_game)
    Me.gb_game.Controls.Add(Me.opt_all_game)
    Me.gb_game.Controls.Add(Me.cmb_game)
    Me.gb_game.Location = New System.Drawing.Point(7, 110)
    Me.gb_game.Name = "gb_game"
    Me.gb_game.Size = New System.Drawing.Size(257, 72)
    Me.gb_game.TabIndex = 1
    Me.gb_game.TabStop = False
    Me.gb_game.Text = "xGame"
    '
    'opt_several_game
    '
    Me.opt_several_game.Location = New System.Drawing.Point(8, 14)
    Me.opt_several_game.Name = "opt_several_game"
    Me.opt_several_game.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_game.TabIndex = 0
    Me.opt_several_game.Text = "xOne"
    '
    'opt_all_game
    '
    Me.opt_all_game.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_game.Name = "opt_all_game"
    Me.opt_all_game.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_game.TabIndex = 1
    Me.opt_all_game.Text = "xAll"
    '
    'cmb_game
    '
    Me.cmb_game.AllowUnlistedValues = False
    Me.cmb_game.AutoCompleteMode = False
    Me.cmb_game.IsReadOnly = False
    Me.cmb_game.Location = New System.Drawing.Point(80, 14)
    Me.cmb_game.Name = "cmb_game"
    Me.cmb_game.SelectedIndex = -1
    Me.cmb_game.Size = New System.Drawing.Size(171, 24)
    Me.cmb_game.SufixText = "Sufix Text"
    Me.cmb_game.SufixTextVisible = True
    Me.cmb_game.TabIndex = 2
    Me.cmb_game.TextVisible = False
    Me.cmb_game.TextWidth = 0
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(257, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(270, 4)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(338, 188)
    Me.uc_pr_list.TabIndex = 2
    '
    'lbl_netwin
    '
    Me.lbl_netwin.AutoSize = True
    Me.lbl_netwin.ForeColor = System.Drawing.Color.Navy
    Me.lbl_netwin.Location = New System.Drawing.Point(4, 209)
    Me.lbl_netwin.Name = "lbl_netwin"
    Me.lbl_netwin.Size = New System.Drawing.Size(52, 13)
    Me.lbl_netwin.TabIndex = 18
    Me.lbl_netwin.Text = "xNetwin"
    '
    'lbl_payout
    '
    Me.lbl_payout.AutoSize = True
    Me.lbl_payout.ForeColor = System.Drawing.Color.Navy
    Me.lbl_payout.Location = New System.Drawing.Point(4, 191)
    Me.lbl_payout.Name = "lbl_payout"
    Me.lbl_payout.Size = New System.Drawing.Size(53, 13)
    Me.lbl_payout.TabIndex = 19
    Me.lbl_payout.Text = "xPayout"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(615, 191)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_sales_date_terminal
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1234, 722)
    Me.Name = "frm_sales_date_terminal"
    Me.Text = "frm_sales_date_terminal"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.gb_game.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_game As String
  Private m_options_value As String

  Dim m_data_set As DataSet
  Dim m_query_from_date As DateTime
  Dim m_query_to_date As DateTime
  Dim m_query_sales_provider_filter As WSI.Common.QuerySalesProviderFilter
  Dim m_query_date_filter As WSI.Common.GroupDateFilter

  Dim m_grouped_date As String

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " Constants "

  Private TERMINAL_DATA_COLUMNS As Int32

  Private Const SQL_COLUMN_SINCE As Integer = 0
  Private Const SQL_COLUMN_UNTIL As Integer = 1
  Private Const SQL_COLUMN_TERMINAL As Integer = 2
  Private Const SQL_COLUMN_PLAYED_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_WON_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_AMOUNT_PER_CENT As Integer = 5
  Private Const SQL_COLUMN_NETWIN As Integer = 6
  Private Const SQL_COLUMN_NETWIN_PER_CENT As Integer = 7
  Private Const SQL_COLUMN_PLAYED_COUNT As Integer = 8
  Private Const SQL_COLUMN_WON_COUNT As Integer = 9
  Private Const SQL_COLUMN_COUNT_PER_CENT As Integer = 10
  Private Const SQL_COLUMN_PROVIDER As Integer = 11
  Private Const SQL_COLUMN_MIN_GAME_ID As Integer = 12
  Private Const SQL_COLUMN_MAX_GAME_ID As Integer = 13
  Private Const SQL_COLUMN_MIN_GAME_NAME As Integer = 14
  Private Const SQL_COLUMN_THEORETICAL_AMOUNT_PER_CENT As Integer = 15
  Private Const SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer = 18
  Private Const SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 19
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer = 20
  Private Const SQL_COLUMN_PROGRESSIVE_JACKPOT_0_AMOUNT As Integer = 21
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 22

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_SINCE As Integer
  Private GRID_COLUMN_UNTIL As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_GAME_NAME As Integer
  Private GRID_COLUMN_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_WON_AMOUNT As Integer
  Private GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT As Integer
  Private GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT As Integer
  Private GRID_COLUMN_AMOUNT_PER_CENT As Integer
  Private GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT As Integer
  Private GRID_COLUMN_NETWIN As Integer
  Private GRID_COLUMN_NETWIN_PER_CENT As Integer
  Private GRID_COLUMN_PLAYED_COUNT As Integer
  Private GRID_COLUMN_WON_COUNT As Integer
  Private GRID_COLUMN_COUNT_PER_CENT As Integer
  Private GRID_COLUMN_AVERAGE_BET As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

#End Region ' Constants

#Region " OVERRIDES "

  '' PURPOSE: Set a different value for the maximum number of rows that can be showed
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''
  ''     - OUTPUT:
  ''
  '' RETURNS:
  ''

  ''Protected Overrides Function GUI_MaxRows() As Integer
  ''  Return 50000
  ''End Function ' GUI_MaxRows

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DATE_TERMINAL

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub 'GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(249)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Games
    Me.gb_game.Text = GLB_NLS_GUI_INVOICING.GetString(462)
    Me.opt_several_game.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_game.Text = GLB_NLS_GUI_INVOICING.GetString(219)

    ' Progressives
    Me.lbl_payout.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5400)
    Me.lbl_netwin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5349)

    Call GUI_StyleSheet()

    ' Set combo with Games
    'Call SetCombo(Me.cmb_game, SELECT_GAMES)
    'Call SetCombo(Me.cmb_game, SELECT_GAMES_V2)
    Call SetComboGamesWithUnknown(Me.cmb_game, SELECT_GAMES_V2)

    Me.cmb_game.Enabled = False

    ' Grouped options
    Me.gb_options.Text = GLB_NLS_GUI_INVOICING.GetString(369)

    Me.cb_grouped.Text = GLB_NLS_GUI_INVOICING.GetString(458)

    Me.rb_hourly.Text = GLB_NLS_GUI_INVOICING.GetString(379)
    Me.rb_24_hours.Text = GLB_NLS_GUI_INVOICING.GetString(380)
    Me.rb_daily.Text = GLB_NLS_GUI_INVOICING.GetString(459)
    Me.rb_weekly.Text = GLB_NLS_GUI_INVOICING.GetString(460)
    Me.rb_monthly.Text = GLB_NLS_GUI_INVOICING.GetString(461)

    Me.cb_grouped_terminals.Text = GLB_NLS_GUI_INVOICING.GetString(370)
    Me.cb_subtotal_date_provider.Text = GLB_NLS_GUI_INVOICING.GetString(376)
    Me.cb_subtotal_date.Text = GLB_NLS_GUI_INVOICING.GetString(371, Me.rb_daily.Text)
    Me.cb_rows.Text = GLB_NLS_GUI_INVOICING.GetString(464)

    Me.cb_totalizer.Text = GLB_NLS_GUI_INVOICING.GetString(372)
    Me.cb_provider_terminals.Text = GLB_NLS_GUI_INVOICING.GetString(370)
    Me.cb_total_provider.Text = GLB_NLS_GUI_INVOICING.GetString(373)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Sub GUI_AfterLastRow()

    Dim total_played_amount As Double
    Dim total_won_amount As Double
    Dim total_played_count As Double
    Dim total_won_count As Double
    Dim total_theoretical_won_amount As Double
    Dim total_progressive_provision_amount As Double
    Dim total_non_progressive_jackpot_amount As Double
    Dim total_progressive_jackpot_amount As Double
    Dim total_progressive_jackpot_amount0 As Double
    Dim netwin As Double
    Dim dt_data As DataTable
    Dim rows_providers As DataRow()
    Dim rows_terminals_provider As DataRow()
    Dim total_row As DataRow
    Dim idx_row As Integer
    Dim data_type As String
    Dim amount_per_cent As Double

    total_played_amount = 0
    total_won_amount = 0
    total_played_count = 0
    total_won_count = 0
    total_theoretical_won_amount = 0
    total_progressive_provision_amount = 0
    total_non_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount = 0
    total_progressive_jackpot_amount0 = 0
    amount_per_cent = 0

    dt_data = m_data_set.Tables("DATA_SP")
    If dt_data Is Nothing Then
      dt_data = m_data_set.Tables("DATA_SD")
      If dt_data Is Nothing Then
        Return
      End If
    End If

    If cb_grouped.Checked And cb_totalizer.Checked Then
      If Me.Grid.NumRows > 0 Then
        Me.Grid.AddRow()
        idx_row = Me.Grid.NumRows - 1
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If
    End If

    data_type = ""
    If cb_totalizer.Checked Then
      If cb_total_provider.Checked Then
        If cb_provider_terminals.Checked Then
          data_type = "Provider"
        Else
          data_type = "Provider_ALONE"
        End If
      End If
    End If

    rows_providers = dt_data.Select()
    For Each row_provider As DataRow In rows_providers
      If row_provider("PlayedCount") IsNot DBNull.Value Then

        If cb_totalizer.Checked Then

          If cb_provider_terminals.Checked Then
            rows_terminals_provider = row_provider.GetChildRows("Provider_PT")
            For Each row_terminal As DataRow In rows_terminals_provider
              GUI_AddDataRow(row_terminal, "Provider_Terminal")
            Next
          End If

          If cb_total_provider.Checked Then
            GUI_AddDataRow(row_provider, data_type)
          End If
        End If

        total_played_amount = total_played_amount + row_provider("PlayedAmount")
        total_won_amount = total_won_amount + row_provider("WonAmount")
        total_played_count = total_played_count + row_provider("PlayedCount")
        total_won_count = total_won_count + row_provider("WonCount")
        total_theoretical_won_amount = total_theoretical_won_amount + row_provider("TheoreticalWonAmount")
        total_progressive_provision_amount = total_progressive_provision_amount + row_provider("ProgressiveProvisionAmount")
        total_non_progressive_jackpot_amount = total_non_progressive_jackpot_amount + row_provider("NonProgressiveJackpotAmount")
        total_progressive_jackpot_amount = total_progressive_jackpot_amount + row_provider("ProgressiveJackpotAmount")
        total_progressive_jackpot_amount0 = total_progressive_jackpot_amount0 + row_provider("ProgressiveJackpotAmount0")

      End If
    Next

    netwin = total_played_amount - (total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount0)

    If total_played_amount = 0 Then
      amount_per_cent = 0
    Else
      amount_per_cent = ((total_won_amount + total_progressive_provision_amount - total_progressive_jackpot_amount0) / total_played_amount) * 100
    End If


    total_row = dt_data.NewRow()
    total_row(SQL_COLUMN_PLAYED_AMOUNT) = total_played_amount
    total_row(SQL_COLUMN_WON_AMOUNT) = total_won_amount
    'total_row(SQL_COLUMN_AMOUNT_PER_CENT) = IIf(total_played_amount = 0, DBNull.Value, total_won_amount * 100 / total_played_amount)
    total_row(SQL_COLUMN_AMOUNT_PER_CENT) = amount_per_cent
    total_row(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT) = total_progressive_provision_amount
    total_row(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT) = total_non_progressive_jackpot_amount
    total_row(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT) = total_progressive_jackpot_amount
    total_row(SQL_COLUMN_PROGRESSIVE_JACKPOT_0_AMOUNT) = total_progressive_jackpot_amount0
    total_row(SQL_COLUMN_NETWIN) = netwin
    total_row(SQL_COLUMN_NETWIN_PER_CENT) = IIf(total_played_amount = 0, DBNull.Value, netwin * 100 / total_played_amount)
    total_row(SQL_COLUMN_PLAYED_COUNT) = total_played_count
    total_row(SQL_COLUMN_WON_COUNT) = total_won_count
    total_row(SQL_COLUMN_COUNT_PER_CENT) = IIf(total_played_amount = 0, DBNull.Value, total_won_count * 100 / total_played_count)
    total_row(SQL_COLUMN_THEORETICAL_AMOUNT_PER_CENT) = IIf(total_played_amount = 0, DBNull.Value, total_theoretical_won_amount * 100 / total_played_amount)

    GUI_AddDataRow(total_row, "GRAND_TOTAL")

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim grouped_date As String

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    If Not Me.cb_grouped.Checked And Not Me.cb_totalizer.Checked Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(103), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.cb_grouped.Focus()

      Return False
    End If

    If Me.cb_grouped.Checked And _
       Not Me.cb_grouped_terminals.Checked And _
       Not Me.cb_subtotal_date_provider.Checked And _
       Not Me.cb_subtotal_date.Checked Then

      grouped_date = ""
      If Me.rb_hourly.Checked Then
        grouped_date = rb_hourly.Text
      ElseIf Me.rb_daily.Checked Then
        grouped_date = rb_daily.Text
      ElseIf Me.rb_weekly.Checked Then
        grouped_date = rb_weekly.Text
      ElseIf Me.rb_monthly.Checked Then
        grouped_date = rb_monthly.Text
      End If

      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(104), ENUM_MB_TYPE.MB_TYPE_WARNING, , , grouped_date)
      Call Me.cb_grouped.Focus()

      Return False
    End If

    If Me.cb_totalizer.Checked And _
       Not Me.cb_provider_terminals.Checked And _
       Not Me.cb_total_provider.Checked Then
      Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(105), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.cb_grouped.Focus()

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    m_query_date_filter = WSI.Common.GroupDateFilter.NO_FILTER
    If cb_grouped.Checked Then
      If Me.rb_hourly.Checked Then
        m_query_date_filter = WSI.Common.GroupDateFilter.HOURLY
        m_grouped_date = rb_hourly.Text
      ElseIf Me.rb_24_hours.Checked Then
        m_query_date_filter = WSI.Common.GroupDateFilter.G24_HOUR
        m_grouped_date = rb_24_hours.Text
      ElseIf Me.rb_daily.Checked Then
        m_query_date_filter = WSI.Common.GroupDateFilter.DAILY
        m_grouped_date = rb_daily.Text
      ElseIf Me.rb_weekly.Checked Then
        m_query_date_filter = WSI.Common.GroupDateFilter.WEEKLY
        m_grouped_date = rb_weekly.Text
      ElseIf Me.rb_monthly.Checked Then
        m_query_date_filter = WSI.Common.GroupDateFilter.MONTHLY
        m_grouped_date = rb_monthly.Text
      End If
    End If

    m_query_from_date = DateTime.MinValue
    If uc_dsl.FromDateSelected Then
      m_query_from_date = uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime)
    End If

    m_query_to_date = DateTime.MaxValue
    If uc_dsl.ToDateSelected Then
      m_query_to_date = uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime)
    End If

    m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.NO_FILTER

    If cb_grouped.Checked Then
      If cb_grouped_terminals.Checked Then
        m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.DATE_PROVIDER_TERMINAL
      ElseIf cb_provider_terminals.Checked Then
        m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.DATE_PROVIDER_TERMINAL
      ElseIf cb_subtotal_date_provider.Checked Then
        m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.DATE_PROVIDER
      ElseIf cb_subtotal_date.Checked Then
        If cb_totalizer.Checked Then
          m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.DATE_PROVIDER
        Else
          m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.DATE
        End If
      End If
    Else
      If cb_totalizer.Checked Then
        If cb_provider_terminals.Checked Then
          m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.PROVIDER_TERMINAL
        Else
          m_query_sales_provider_filter = WSI.Common.QuerySalesProviderFilter.PROVIDER
        End If
      End If
    End If

    str_sql = WSI.Common.SqlStatistics.GetQuerySalesByDateAndProvider(m_query_from_date, m_query_to_date, _
                     m_query_date_filter, m_query_sales_provider_filter, True, uc_pr_list.OneTerminalChecked, _
                     uc_pr_list.GetProviderIdListSelected(), opt_several_game.Checked, cmb_game.Value, False, "", 0)

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim sql_trans As SqlTransaction
    Dim str_sql As String
    Dim count As Integer
    Dim bln_redraw As Boolean
    Dim function_name As String
    Dim more_than_max As Boolean
    Dim row_step As Integer
    Dim max_rows As Integer
    Dim selected_rows As Integer
    Dim p_parent_row As DataRow
    Dim p_last_parent_row As DataRow
    Dim d_parent_row As DataRow
    Dim d_last_parent_row As DataRow
    Dim data_type As String

    'XVV 16/04/2007
    'Assign Nothing to SQL Trans because compiler generate warning for not have value
    Dim table As DataTable = Nothing
    Dim row As DataRow

    function_name = "ExecuteQueryCustom"
    max_rows = GUI_MaxRows()

    Try
      ' In the function GUI_FilterGetSqlQuery(), the following vars take value:
      '                 m_query_from_date, m_query_to_date, m_query_date_filter, m_query_sales_provider_filter.
      str_sql = GUI_FilterGetSqlQuery()

      If str_sql = "" Then
        Return
      End If

      sql_trans = WSI.Common.WGDB.Connection().BeginTransaction()

      m_data_set = WSI.Common.SqlStatistics.GetSalesByDateAndProvider(str_sql, m_query_from_date, m_query_to_date, _
                            m_query_date_filter, m_query_sales_provider_filter, sql_trans)

      Select Case m_query_sales_provider_filter
        Case WSI.Common.QuerySalesProviderFilter.DATE_PROVIDER_TERMINAL
          If cb_grouped_terminals.Checked Then
            table = m_data_set.Tables("DATA_SDPT")
          ElseIf cb_subtotal_date_provider.Checked Then
            table = m_data_set.Tables("DATA_SDP")
          Else
            table = m_data_set.Tables("DATA_SD")
          End If

        Case WSI.Common.QuerySalesProviderFilter.DATE_PROVIDER
          If cb_subtotal_date_provider.Checked Then
            table = m_data_set.Tables("DATA_SDP")
          Else
            table = m_data_set.Tables("DATA_SD")
          End If

        Case WSI.Common.QuerySalesProviderFilter.DATE
          table = m_data_set.Tables("DATA_SD")

        Case WSI.Common.QuerySalesProviderFilter.PROVIDER_TERMINAL
          table = m_data_set.Tables("DATA_SPT")

        Case WSI.Common.QuerySalesProviderFilter.PROVIDER
          table = m_data_set.Tables("DATA_SP")
      End Select

      bln_redraw = Me.Grid.Redraw

      ' Invalidate datagrid
      Call Me.Grid.Clear()
      Me.Grid.Redraw = False

      more_than_max = False

      Call GUI_BeforeFirstRow()

      row_step = max_rows
      row_step = row_step / 20
      If row_step < MAX_RECORDS_FIRST_LOAD Then
        row_step = MAX_RECORDS_FIRST_LOAD
      End If

      p_last_parent_row = Nothing
      d_last_parent_row = Nothing

      count = 0

      If cb_grouped.Checked Then

        ' Exclude rows without activity
        If Me.cb_rows.Checked Then
          selected_rows = table.Select("AmountPc IS NOT NULL ").Length
        Else
          selected_rows = table.Rows.Count
        End If

        ' JAB 24-AUG-2012: Control to show huge amounts of records.
        If Not ShowHugeNumberOfRows(selected_rows) Then
          MyBase.m_user_canceled_data_shows = True
          Exit Sub
        End If

        For Each row In table.Rows

          Try

            ' JAB 12-JUN-2012: DoEvents run each second.
            If Not GUI_DoEvents(Me.Grid) Then
              Exit Sub
            End If

            p_parent_row = Nothing
            data_type = ""

            If cb_grouped_terminals.Checked Then
              p_parent_row = row.GetParentRow("Date_Provider")
              data_type = "Date_Provider_Terminal"

              If p_last_parent_row Is Nothing Then
                p_last_parent_row = p_parent_row
              End If

              If Not p_last_parent_row.Equals(p_parent_row) Then
                If cb_subtotal_date_provider.Checked Then
                  If GUI_AddDataRow(p_last_parent_row, "Date_Provider") Then
                    count = count + 1
                  End If
                End If

                p_last_parent_row = p_parent_row
              End If
            ElseIf cb_subtotal_date_provider.Checked Then
              p_parent_row = row
              data_type = "Date_Provider_ALONE"
            ElseIf cb_subtotal_date.Checked Then
              p_parent_row = Nothing
              data_type = "Date_ALONE"
            End If

            If cb_grouped_terminals.Checked Or cb_subtotal_date_provider.Checked Then
              If cb_subtotal_date.Checked Then
                d_parent_row = p_parent_row.GetParentRow("Date")

                If d_last_parent_row Is Nothing Then
                  d_last_parent_row = d_parent_row
                End If

                If Not d_last_parent_row.Equals(d_parent_row) Then
                  If GUI_AddDataRow(d_last_parent_row, "Date") Then
                    count = count + 1
                  End If

                  d_last_parent_row = d_parent_row
                End If
              End If
            End If

            If GUI_AddDataRow(row, data_type) Then
              count = count + 1
            End If

          Catch exception As Exception
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
            Call Trace.WriteLine(exception.ToString())
            Exit For
          End Try

          ' RCI 22-DEC-2010: Need count > 0, otherwise it's always doing Redraw for 0 rows!!
          If count > 0 And count Mod row_step = 0 Then
            Me.Grid.Redraw = True
            Call Application.DoEvents()
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            Me.Grid.Redraw = False
          End If

          If count >= max_rows Then
            more_than_max = True

            Exit For
          End If
        Next

        If cb_subtotal_date_provider.Checked And p_last_parent_row IsNot Nothing Then
          GUI_AddDataRow(p_last_parent_row, "Date_Provider")
        End If

        If cb_grouped_terminals.Checked Or cb_subtotal_date_provider.Checked Then
          If cb_subtotal_date.Checked And d_last_parent_row IsNot Nothing Then
            GUI_AddDataRow(d_last_parent_row, "Date")
          End If
        End If

      End If ' cb_grouped.Checked

      Call GUI_AfterLastRow()

      Me.Grid.Redraw = True

      If more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call Trace.WriteLine(ex.ToString())
      Me.Grid.Redraw = True

    Finally
      If Not IsNothing(m_data_set) Then
        Call m_data_set.Clear()
      End If
      m_data_set = Nothing
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(251), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(462), m_game)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options_value)

    PrintData.FilterValueWidth(1) = 2000
    PrintData.FilterValueWidth(2) = 3000
    PrintData.FilterValueWidth(3) = 6500

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_terminals = ""
    m_game = ""
    m_options_value = ""

    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.Date.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    ' Game
    If Me.opt_all_game.Checked Then
      m_game = Me.opt_all_game.Text
    Else
      m_game = Me.cmb_game.TextValue
    End If

    ' Options
    ' Grouped
    m_options_value = cb_grouped.Text & ": "
    If cb_grouped.Checked Then
      ' By hour, 24 hours, daily, weekly or monthly
      If Me.rb_hourly.Checked Then
        m_options_value = m_options_value & Me.rb_hourly.Text
      ElseIf Me.rb_24_hours.Checked Then
        m_options_value = m_options_value & Me.rb_24_hours.Text
      ElseIf Me.rb_daily.Checked Then
        m_options_value = m_options_value & Me.rb_daily.Text
      ElseIf Me.rb_weekly.Checked Then
        m_options_value = m_options_value & Me.rb_weekly.Text
      ElseIf Me.rb_monthly.Checked Then
        m_options_value = m_options_value & Me.rb_monthly.Text
      End If
      ' Show terminals
      m_options_value = m_options_value & ", " & cb_grouped_terminals.Text & ": "
      If cb_grouped_terminals.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
      ' Subtotal by provider
      m_options_value = m_options_value & ", " & cb_subtotal_date_provider.Text & ": "
      If cb_subtotal_date_provider.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
      ' Subtotal by period
      m_options_value = m_options_value & ", " & cb_subtotal_date.Text & ": "
      If cb_subtotal_date.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
      ' Exclude rows without activity
      m_options_value = m_options_value & ", " & cb_rows.Text & ": "
      If cb_rows.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_options_value = m_options_value & ". " & cb_totalizer.Text & ": "

    ' Totalizer
    If cb_totalizer.Checked Then
      ' Show terminals
      m_options_value = m_options_value & cb_provider_terminals.Text & ": "
      If cb_provider_terminals.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
      ' Total by provider
      m_options_value = m_options_value & ", " & cb_total_provider.Text & ": "
      If cb_total_provider.Checked Then
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    Else
      m_options_value = m_options_value & GLB_NLS_GUI_INVOICING.GetString(480)
    End If
    m_options_value = m_options_value & "."

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Since date
      .Column(GRID_COLUMN_SINCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_SINCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(202)
      .Column(GRID_COLUMN_SINCE).Width = 1700
      .Column(GRID_COLUMN_SINCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Until date
      .Column(GRID_COLUMN_UNTIL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_UNTIL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(203)
      .Column(GRID_COLUMN_UNTIL).Width = 1700
      .Column(GRID_COLUMN_UNTIL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      '  Game Name
      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(215)
      .Column(GRID_COLUMN_GAME_NAME).Width = 1950
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Played Amount
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(0).Text = ""
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6275) 'Coin In
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PLAYED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMN_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(257)
      .Column(GRID_COLUMN_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5391)
      .Column(GRID_COLUMN_WON_AMOUNT).Width = 1950
      .Column(GRID_COLUMN_WON_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non progressive Jackpot amount
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5293)
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1600
      .Column(GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive Jackpot amount
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5294)
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Progressive provision amount
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(307)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5292)
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Width = 1250
      .Column(GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Amount Per Cent
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(258)
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Width = 1250
      .Column(GRID_COLUMN_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Theorical Amount Per Cent
      .Column(GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1924)
      .Column(GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1809)
      .Column(GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT).Width = GRID_COLUMN_THEORICAL_PAYOUT_WIDTH
      .Column(GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '  Netwin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(259)
      .Column(GRID_COLUMN_NETWIN).Width = 1250
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Netwin Pay out
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(260)
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Width = 1250
      .Column(GRID_COLUMN_NETWIN_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Count
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_PLAYED_COUNT).Width = 950
      .Column(GRID_COLUMN_PLAYED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Count
      .Column(GRID_COLUMN_WON_COUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_WON_COUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(262)
      .Column(GRID_COLUMN_WON_COUNT).Width = 950
      .Column(GRID_COLUMN_WON_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count Per Cent
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(261)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(316)
      .Column(GRID_COLUMN_COUNT_PER_CENT).Width = 820
      .Column(GRID_COLUMN_COUNT_PER_CENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Average bet
      .Column(GRID_COLUMN_AVERAGE_BET).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(308)
      .Column(GRID_COLUMN_AVERAGE_BET).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(347)
      .Column(GRID_COLUMN_AVERAGE_BET).Width = 1400
      .Column(GRID_COLUMN_AVERAGE_BET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.opt_all_game.Checked = True

    Me.cmb_game.Enabled = False

    Me.cb_grouped.Checked = True
    Me.rb_daily.Checked = True
    cb_grouped_CheckedChanged(Nothing, Nothing)
    Me.cb_grouped_terminals.Checked = True
    Me.cb_subtotal_date_provider.Checked = True
    Me.cb_subtotal_date.Checked = True
    Me.cb_rows.Checked = False

    Me.cb_totalizer.Checked = True
    Me.cb_provider_terminals.Checked = False
    Me.cb_total_provider.Checked = True

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE : Add a row to the Grid and set its values
  '
  '  PARAMS :
  '     - INPUT :
  '           - Row
  '           - RelationName
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Private Function GUI_AddDataRow(ByVal Row As DataRow, _
                                  ByVal DataType As String) As Boolean

    Dim idx_row As Integer
    Dim db_row As CLASS_DB_ROW
    Dim min_game_id As Integer
    Dim max_game_id As Integer
    Dim min_game_name As String
    Dim _since As DateTime
    Dim _until As DateTime
    Dim _average_bet As Double
    Dim _terminal_data As List(Of Object)

    db_row = New CLASS_DB_ROW(Row)

    idx_row = Me.Grid.NumRows

    ' Exclude rows without activity
    If Me.cb_rows.Checked And DataType <> "GRAND_TOTAL" Then
      If db_row.IsNull(SQL_COLUMN_AMOUNT_PER_CENT) Then
        Return False
      End If
    End If

    Me.Grid.AddRow()
    idx_row = Me.Grid.NumRows - 1

    Select Case DataType
      Case "Date_Provider_Terminal", "Date_Provider_ALONE", "Date_ALONE"
        If m_query_date_filter = WSI.Common.GroupDateFilter.G24_HOUR Then
          ' Since DateTime
          Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GUI_FormatTime(db_row.Value(SQL_COLUMN_SINCE), ENUM_FORMAT_TIME.FORMAT_HHMM)
          ' Until DateTime
          If Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = "23:00" Then
            Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = "24:00"
          Else
            Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GUI_FormatTime(db_row.Value(SQL_COLUMN_UNTIL), ENUM_FORMAT_TIME.FORMAT_HHMM)
          End If
        Else
          _since = db_row.Value(SQL_COLUMN_SINCE)
          _until = db_row.Value(SQL_COLUMN_UNTIL)
          If m_query_date_filter <> WSI.Common.GroupDateFilter.HOURLY Then
            _since = _since.Date.AddHours(Me.uc_dsl.ClosingTime)
            _until = _until.Date.AddHours(Me.uc_dsl.ClosingTime)
          End If

          ' Since DateTime
          ' Added 'm_date_from' comparison when Me.uc_dsl.FromDateSelected = True
          If Me.uc_dsl.FromDateSelected Then
            If _since < m_date_from Then
              Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GUI_FormatDate(m_date_from, , ENUM_FORMAT_TIME.FORMAT_HHMM)
            Else
              Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GUI_FormatDate(_since, , ENUM_FORMAT_TIME.FORMAT_HHMM)
            End If
          Else
            Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GUI_FormatDate(_since, , ENUM_FORMAT_TIME.FORMAT_HHMM)
          End If

          ' Until DateTime
          ' Added 'm_date_to' comparison when Me.uc_dsl.ToDateSelected = True
          If Me.uc_dsl.ToDateSelected Then
            If _until > m_date_to Then
              Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GUI_FormatDate(m_date_to, , ENUM_FORMAT_TIME.FORMAT_HHMM)
            Else
              Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GUI_FormatDate(_until, , ENUM_FORMAT_TIME.FORMAT_HHMM)
            End If
          Else
            Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GUI_FormatDate(_until, , ENUM_FORMAT_TIME.FORMAT_HHMM)
          End If
        End If

      Case "Provider_Terminal", "Provider_ALONE"
        ' Since DateTime
        If m_date_from <> "" Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GUI_FormatDate(m_date_from, , ENUM_FORMAT_TIME.FORMAT_HHMM)
        Else
          Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = ""
        End If

        ' Until DateTime
        If m_date_to <> "" Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GUI_FormatDate(m_date_to, , ENUM_FORMAT_TIME.FORMAT_HHMM)
        Else
          Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = ""
        End If

      Case "Date"
        ' Since DateTime
        Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GLB_NLS_GUI_INVOICING.GetString(374, m_grouped_date)
        ' Until DateTime
        Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = ""

      Case "Date_Provider", "Provider"
        Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = ""
        Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = GLB_NLS_GUI_INVOICING.GetString(375)

      Case "GRAND_TOTAL"
        Me.Grid.Cell(idx_row, GRID_COLUMN_SINCE).Value = GLB_NLS_GUI_INVOICING.GetString(205)  '"TOTAL: "
        Me.Grid.Cell(idx_row, GRID_COLUMN_UNTIL).Value = ""
        Me.Grid.Cell(idx_row, GRID_COLUMN_GAME_NAME).Value = ""
    End Select

    If DataType <> "GRAND_TOTAL" Then
      ' Game
      min_game_id = db_row.Value(SQL_COLUMN_MIN_GAME_ID)
      max_game_id = db_row.Value(SQL_COLUMN_MAX_GAME_ID)
      min_game_name = db_row.Value(SQL_COLUMN_MIN_GAME_NAME)

      If min_game_id = max_game_id Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_GAME_NAME).Value = min_game_name
      Else
        Me.Grid.Cell(idx_row, GRID_COLUMN_GAME_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634) ' Multijuego 
      End If
    End If

    'Terminal Report
    If DataType.Contains("Terminal") AndAlso Not db_row.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      _terminal_data = TerminalReport.GetReportDataList(db_row.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1 'LTC 30-AUG-2016
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(idx_row, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        Else
          Me.Grid.Cell(idx_row, GRID_INIT_TERMINAL_DATA + _idx).Value = ""
        End If
      Next
    Else
      If Not db_row.IsNull(SQL_COLUMN_PROVIDER) Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_PROVIDER).Value = db_row.Value(SQL_COLUMN_PROVIDER)
      Else
        Me.Grid.Cell(idx_row, GRID_COLUMN_PROVIDER).Value = ""
      End If
    End If
    

    ' PlayedAmount
    If Not db_row.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = GUI_FormatCurrency(db_row.Value(SQL_COLUMN_PLAYED_AMOUNT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_AMOUNT).Value = "0"
    End If

    ' WonAmount
    If Not db_row.IsNull(SQL_COLUMN_WON_AMOUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = GUI_FormatCurrency(db_row.Value(SQL_COLUMN_WON_AMOUNT))
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_WON_AMOUNT).Value = "0"
    End If

    ' AmountPerCent
    If Not db_row.IsNull(SQL_COLUMN_AMOUNT_PER_CENT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = GUI_FormatNumber(db_row.Value(SQL_COLUMN_AMOUNT_PER_CENT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT_PER_CENT).Value = ""
    End If

    ' TheoreticalAmountPerCent
    If Not db_row.IsNull(SQL_COLUMN_THEORETICAL_AMOUNT_PER_CENT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT).Value = GUI_FormatNumber(db_row.Value(SQL_COLUMN_THEORETICAL_AMOUNT_PER_CENT), 2) & "%"
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT).Value = ""
    End If

    ' Non progressive Jackpot amount
    If Not db_row.IsNull(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(db_row.Value(SQL_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT))
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT).Value = ""
    End If

    ' Progressive Jackpot amount
    If Not db_row.IsNull(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = GUI_FormatCurrency(db_row.Value(SQL_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT))
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT).Value = ""
    End If

    ' Progressive provision amount
    If Not db_row.IsNull(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = GUI_FormatCurrency(db_row.Value(SQL_COLUMN_PROGRESSIVE_PROVISION_AMOUNT))
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT).Value = ""
    End If

    ' Netwin
    If Not db_row.IsNull(SQL_COLUMN_NETWIN) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(db_row.Value(SQL_COLUMN_NETWIN))
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN).Value = "0"
    End If

    ' Netwin PerCent
    If Not db_row.IsNull(SQL_COLUMN_NETWIN_PER_CENT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = GUI_FormatNumber(db_row.Value(SQL_COLUMN_NETWIN_PER_CENT), _
                                                                                   ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_NETWIN_PER_CENT).Value = ""
    End If

    ' PlayedCount
    If Not db_row.IsNull(SQL_COLUMN_PLAYED_COUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = GUI_FormatNumber(db_row.Value(SQL_COLUMN_PLAYED_COUNT), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_PLAYED_COUNT).Value = "0"
    End If

    ' WonCount
    If Not db_row.IsNull(SQL_COLUMN_WON_COUNT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = GUI_FormatNumber(db_row.Value(SQL_COLUMN_WON_COUNT), _
                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_WON_COUNT).Value = "0"
    End If

    ' CountPerCent
    If Not db_row.IsNull(SQL_COLUMN_COUNT_PER_CENT) Then
      Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = GUI_FormatNumber(db_row.Value(SQL_COLUMN_COUNT_PER_CENT), _
                                                                                  ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & "%"
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_COUNT_PER_CENT).Value = ""
    End If

    ' Average bet
    If Not db_row.IsNull(SQL_COLUMN_PLAYED_COUNT) And Not db_row.IsNull(SQL_COLUMN_PLAYED_AMOUNT) Then
      If db_row.Value(SQL_COLUMN_PLAYED_COUNT) = 0 Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
      Else
        _average_bet = Math.Round(db_row.Value(SQL_COLUMN_PLAYED_AMOUNT) / db_row.Value(SQL_COLUMN_PLAYED_COUNT), 2, MidpointRounding.AwayFromZero)

        If _average_bet = 0 Then
          Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
        Else
          Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = GUI_FormatCurrency(_average_bet, _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        End If
      End If
    Else
      Me.Grid.Cell(idx_row, GRID_COLUMN_AVERAGE_BET).Value = ""
    End If

    Select Case DataType
      Case "Date_Provider", "Date_Provider_ALONE", "Provider", "Provider_ALONE"
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      Case "Date", "Date_ALONE"
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      Case "GRAND_TOTAL"
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      Case Else
        Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    End Select

    Return True
  End Function ' GUI_AddDataRow

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_SINCE = 1
    GRID_COLUMN_UNTIL = 2
    GRID_INIT_TERMINAL_DATA = 3

    GRID_COLUMN_GAME_NAME = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_NON_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_PROGRESSIVE_JACKPOT_AMOUNT = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_PROGRESSIVE_PROVISION_AMOUNT = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_AMOUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_THEORETICAL_AMOUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_NETWIN_PER_CENT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_WON_COUNT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_COUNT_PER_CENT = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_AVERAGE_BET = TERMINAL_DATA_COLUMNS + 16

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 17

  End Sub

#End Region ' Private Functions

#Region " Events"

  Private Sub opt_all_game_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_game.Click
    Me.cmb_game.Enabled = False
  End Sub

  Private Sub opt_several_game_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_game.Click
    Me.cmb_game.Enabled = True
  End Sub

  Private Sub cb_grouped_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_grouped.CheckedChanged
    If Me.cb_grouped.Checked Then
      Me.rb_hourly.Enabled = True
      Me.rb_24_hours.Enabled = True
      Me.rb_daily.Enabled = True
      Me.rb_weekly.Enabled = True
      Me.rb_monthly.Enabled = True
      Me.cb_grouped_terminals.Enabled = True
      Me.cb_subtotal_date_provider.Enabled = True
      Me.cb_subtotal_date.Enabled = True
      Me.cb_rows.Enabled = True

      If Me.rb_24_hours.Checked Then
        rb_24_hours_CheckedChanged(Nothing, Nothing)
      End If
    Else
      Me.rb_hourly.Enabled = False
      Me.rb_24_hours.Enabled = False
      Me.rb_daily.Enabled = False
      Me.rb_weekly.Enabled = False
      Me.rb_monthly.Enabled = False
      Me.cb_grouped_terminals.Enabled = False
      Me.cb_subtotal_date_provider.Enabled = False
      Me.cb_subtotal_date.Enabled = False
      Me.cb_rows.Enabled = False
    End If
  End Sub

  Private Sub cb_totalizer_provider_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_totalizer.CheckedChanged
    If Me.cb_totalizer.Checked Then
      Me.cb_provider_terminals.Enabled = True
      Me.cb_total_provider.Enabled = True
    Else
      Me.cb_provider_terminals.Enabled = False
      Me.cb_total_provider.Enabled = False
    End If
  End Sub

  Private Sub rb_daily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_daily.CheckedChanged
    If Me.rb_daily.Checked Then
      Me.cb_subtotal_date.Text = GLB_NLS_GUI_INVOICING.GetString(371, Me.rb_daily.Text)
    End If
  End Sub

  Private Sub rb_weekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_weekly.CheckedChanged
    If Me.rb_weekly.Checked Then
      Me.cb_subtotal_date.Text = GLB_NLS_GUI_INVOICING.GetString(371, Me.rb_weekly.Text)
    End If
  End Sub

  Private Sub rb_monthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_monthly.CheckedChanged
    If Me.rb_monthly.Checked Then
      Me.cb_subtotal_date.Text = GLB_NLS_GUI_INVOICING.GetString(371, Me.rb_monthly.Text)
    End If
  End Sub

  Private Sub rb_hourly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_hourly.CheckedChanged
    If Me.rb_hourly.Checked Then
      Me.cb_subtotal_date.Text = GLB_NLS_GUI_INVOICING.GetString(371, Me.rb_hourly.Text)
    End If
  End Sub

  Private Sub rb_24_hours_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_24_hours.CheckedChanged
    If Me.rb_24_hours.Checked Then
      Me.cb_subtotal_date.Text = GLB_NLS_GUI_INVOICING.GetString(371, Me.rb_24_hours.Text)

      Me.cb_grouped_terminals.Checked = False
      Me.cb_grouped_terminals.Enabled = False
      Me.cb_subtotal_date_provider.Checked = False
      Me.cb_subtotal_date_provider.Enabled = False
      Me.cb_subtotal_date.Checked = True
      Me.cb_subtotal_date.Enabled = False
      Me.cb_rows.Checked = False
      Me.cb_rows.Enabled = False
    Else
      Me.cb_grouped_terminals.Enabled = True
      Me.cb_subtotal_date_provider.Enabled = True
      Me.cb_subtotal_date.Checked = False
      Me.cb_subtotal_date.Enabled = True
      Me.cb_rows.Enabled = True
    End If
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

End Class