'-------------------------------------------------------------------
' Copyright � 2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_gaming_tables_session_import.vb
'
' DESCRIPTION : Allows to import gaming table play sessions from excel.
'
' AUTHOR:        David Hern�ndez
' CREATION DATE: 02-FEB-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -------------------------------------------------------------------------------
' 02-FEB-2015  DHA    Initial version
' 20-OCT-2015  ECP    Backlog Item 1080 GUI - Improvements to Gaming Table Plays Sessions Import
' 18-NOV-2015  ECP    Backlog Item 6703 Sprint Review 15 Winions - Mex. Sessions Import Revision
' 15-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
' 31-OCT-2017  RAB    Bug 30497:WIGOS-6175 Gambling tables: Table gaming session import error
'----------------------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices
Imports WSI.Common

#End Region

Public Class frm_gaming_tables_session_import
  Inherits frm_base_edit

#Region "Constants"

#End Region

#Region "Structures"

#End Region

#Region "Members"
  Private m_permission As CLASS_GUI_USER.TYPE_PERMISSIONS
  Private m_thread_import As Thread 'Main Thread.
  Private m_gaming_table_plays_sessions As GamingTablePlaysSessionsImport
  Private m_imported_data_table As DataTable
  Private m_state As WSI.Common.AccountsImport.IMPORT_STATES 'Import State.
  Private m_log_file As String

#End Region

#Region "Delegates"
  Private Delegate Sub SetTextCallback(ByVal e As WSI.Common.GamingTablePlaysSessionsImportEventArgs)
  Private Delegate Function ShowErrorLog(ByVal GamingTablePlaysSession As DataTable, ByVal ErrorsDataTable As DataTable, ByVal Message As String, ByVal ButtonOKText As String, ByVal ButtonCancelText As String) As Boolean
  Private Delegate Sub SetStopButtonDelegate(ByVal Enabled As Boolean)
#End Region

#Region "Overrides"
  'PURPOSE: Form controls initialization
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim _ctrl_file_path As uc_entry_field
    Dim _sell_chips_text As String
    Dim _sell_chips_fieldname As String

    Call MyBase.GUI_InitControls()

    'Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5882)

    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6935) ' Label description

    'Buttons

    'OK
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = False

    'STOP
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CLASS_II.GetString(427)

    'EXIT
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CLASS_II.GetString(3)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False

    'Formating Screen
    Me.gb_output.Text = GLB_NLS_GUI_CLASS_II.GetString(425)

    Me.tb_output.Enabled = True
    Me.tb_output.ReadOnly = True

    'Load Permisions
    m_permission = CurrentUser.Permissions(ENUM_FORM.FORM_GAMING_TABLE_SESSIONS_IMPORT)

    'Hide Text File:
    _ctrl_file_path = Me.dti_excel_import.Controls.Find("ef_file_path", False)(0)
    _ctrl_file_path.Text = String.Empty

    dti_excel_import.ExcelUseCurrentRegion = False

    'Get SellChips text
    _sell_chips_text = GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", String.Empty)

    'Get Buy-In fieldname
    If String.IsNullOrEmpty(_sell_chips_text) Then
      _sell_chips_fieldname = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5289)
    Else
      _sell_chips_fieldname = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5073, _sell_chips_text)
    End If

    'Load fields to import
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979), "System.String") 'Table Name
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2749), "System.String") 'Account Id

    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285), "System.DateTime") 'Arrival
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286), "System.DateTime") 'Departure
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4984), "System.DateTime") 'Walk Time

    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486), "System.String") 'Current Bet
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372), "System.String") 'Played Amount
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4983), "System.String") 'No of plays
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(420), "System.String") 'Min Bet
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(845), "System.String") 'Max Bet

    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287), "System.String") 'Chips In
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288), "System.String") 'Chips Out

    dti_excel_import.AddColumnsMapping(_sell_chips_fieldname, "System.String")                       'Buy-In

    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684), "System.String") 'Awared points
    dti_excel_import.AddColumnsMapping(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5554), "System.String") 'Iso code
  End Sub 'GUI_InitControls

  'PURPOSE: Form controls initialization
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE
        StopGamingTablePlaysSessionsImport()
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLE_SESSIONS_IMPORT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

#End Region

#Region "Public Functions"
  ' PURPOSE: Form entry point from menu selection.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowEditItem
#End Region

#Region "Private Functions"
  'PURPOSE: Start the importing gaming table plays sessions threads.
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Private Sub StartGamingTablePlaysSessionsImport(ByRef ImportedDataTable As DataTable)

    m_imported_data_table = ImportedDataTable

    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = False
    Me.dti_excel_import.Enabled = False
    Me.gb_output.Text = String.Empty
    Me.tb_output.Text = String.Empty
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Focus()

    'Create thread
    m_thread_import = New Thread(AddressOf GamingTablePlaysSessionsImportThread)
    m_thread_import.Start() ' Launch import thread

  End Sub
  'PURPOSE: Start the importing gaming table sessions threads.
  '
  'PARAMS
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  Private Sub GamingTablePlaysSessionsImportThread()

    Dim _ds_gaming_table_plays_sessions As DataSet
    Dim _ask_for_continue_callback As New ShowErrorLog(AddressOf ShowBrowserMessage)
    Dim _stop_callback As New SetStopButtonDelegate(AddressOf SetStopButton)
    Dim _message As String
    Dim _txt_ok As String
    Dim _txt_cancel As String
    Dim _str_long_line As String

    m_log_file = dti_excel_import.LogFile + WGDB.Now.ToString("yyyy.MM.dd") + ".txt"
    
    _ds_gaming_table_plays_sessions = New DataSet()
    m_gaming_table_plays_sessions = New GamingTablePlaysSessionsImport(m_log_file)

    AddHandler m_gaming_table_plays_sessions.OnRowUpdated, AddressOf UpdateProcess 'Every time that launch the event, launch UpdateProcess

    _str_long_line = Resource.String("STR_AC_IMPORT_SEPARATOR") + Resource.String("STR_AC_IMPORT_START2") + WGDB.Now.ToString()
    m_gaming_table_plays_sessions.WriteLog(_str_long_line)

    'Read Imported DataTable and fill Dataset
    If (m_gaming_table_plays_sessions.GetGamingTablePlaysSessions(m_imported_data_table, _ds_gaming_table_plays_sessions)) Then

      'Formating text
      _txt_ok = GLB_NLS_GUI_CONTROLS.GetString(3)
      _txt_cancel = GLB_NLS_GUI_CONTROLS.GetString(4)
      _message = String.Empty

      'If there's no errors and no rows to process, show other text.
      If Not _ds_gaming_table_plays_sessions.Tables("GamingTablePlaysSessions") Is Nothing And _ds_gaming_table_plays_sessions.Tables("GamingTablePlaysSessions").Rows.Count = 0 Then
        _message = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5883)
        _txt_ok = String.Empty
        _txt_cancel = GLB_NLS_GUI_CONTROLS.GetString(1)
      End If

      'Launch form
      If (Me.Invoke(_ask_for_continue_callback, _ds_gaming_table_plays_sessions.Tables("GamingTablePlaysSessions"), _ds_gaming_table_plays_sessions.Tables("Errors"), _message, _txt_ok, _txt_cancel) = True) Then
        m_gaming_table_plays_sessions.AddImportedGamingTablePlaysSessions(_ds_gaming_table_plays_sessions.Tables("GamingTablePlaysSessions"), GLB_CurrentUser.Id, GLB_CurrentUser.Name)
        Call RegisterAudit()
      Else
        m_gaming_table_plays_sessions.setLastEvent()
      End If
    Else
      m_gaming_table_plays_sessions.setErrorEvent()
      m_gaming_table_plays_sessions.setLastEvent()
    End If

    Me.Invoke(_stop_callback, False)
  End Sub

  ' PURPOSE: handle m_accounts_import.OnRowUpdated event
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub UpdateProcess(ByVal Sender As Object, ByVal UpdateEventArgs As WSI.Common.GamingTablePlaysSessionsImportEventArgs)
    GamingTablePlaysSessionsImportStep(UpdateEventArgs)
  End Sub

  ' PURPOSE: called from m_thread_import throw a delegate, set BUTTON_DELETE enabled/disabled
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub SetStopButton(ByVal Enabled As Boolean)
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = Enabled
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
    Me.dti_excel_import.Enabled = True
  End Sub

  ' PURPOSE: called from m_thread_import throw a delegate, set output text and progressbar progress
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub GamingTablePlaysSessionsImportStep(ByVal GamingTablePlaysSessionsImportEventArgs As WSI.Common.GamingTablePlaysSessionsImportEventArgs)

    Dim _callback As SetTextCallback
    Dim _progress As Double

    'If control is not accessible, launch another thread with the event info.
    If (Me.tb_output.InvokeRequired) Then
      _callback = New SetTextCallback(AddressOf GamingTablePlaysSessionsImportStep)
      Me.Invoke(_callback, New Object() {GamingTablePlaysSessionsImportEventArgs})
    Else
      'Fill the interface info
      _progress = 0
      tb_output.AppendText(GamingTablePlaysSessionsImportEventArgs.Message)
      tb_output.SelectionStart = tb_output.TextLength
      tb_output.ScrollToCaret()

      Try
        'Fill ProgessBar
        If GamingTablePlaysSessionsImportEventArgs.Row_count > 0 Then
          _progress = ((CDbl(GamingTablePlaysSessionsImportEventArgs.Row_count) - CDbl(GamingTablePlaysSessionsImportEventArgs.Pending_rows)) / CDbl(GamingTablePlaysSessionsImportEventArgs.Row_count) * 100)
        Else
          _progress = 0
        End If

        Me.pgb_import_progress.Value = CInt(_progress)
      Catch ex As Exception
        _progress = 0
        Me.pgb_import_progress.Value = CInt(_progress)
      End Try

      'Update process state
      Me.m_state = GamingTablePlaysSessionsImportEventArgs.State

      'If we finished reading the excel document, enable Stop button.
      If (m_state = AccountsImport.IMPORT_STATES.DocumentReaded) Then
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True
      End If

      If (m_state = AccountsImport.IMPORT_STATES.DBUpdated) Then
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
      End If

    End If
  End Sub 'GamingTableSessionsImportStep

  ' PURPOSE: called from UpdateProcess. Update the info of the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Function ShowBrowserMessage(ByVal GamingTablePlaysSession As DataTable, ByVal ErrorsDataTable As DataTable, ByVal Message As String, ByVal ButtonOKText As String, ByVal ButtonCancelText As String) As Boolean
    Dim _message_Text As String
    Dim _frm_dialog As frm_YesNo_web_browser
    Dim _str_errors As String
    Dim _str_document_text As String
    Dim _str_valid_rows As String
    Dim _str_not_valid_rows As String
    Dim _str_total_rows As String
    Dim _datatable_unique_accounts As DataTable
    Dim _str_distinct_players As String
    Dim _tmp_total_awarded_points As Object
    Dim _str_total_awarded_points As String

    _frm_dialog = New frm_YesNo_web_browser()
    _str_errors = String.Empty

    If Message.Trim() = String.Empty Then
      _message_Text = GLB_NLS_GUI_CLASS_II.GetString(430) 'Validation errors where found. Do you want to continue importing?
    Else
      _message_Text = Message
    End If

    For Each _row As DataRow In ErrorsDataTable.Rows
      _str_errors += GLB_NLS_GUI_CLASS_II.GetString(432) + _row(0).ToString() + ": " + _row(1).ToString().Replace("<", """").Replace(">", """") + "</br>" 'Fila
    Next

    _datatable_unique_accounts = GamingTablePlaysSession.DefaultView.ToTable(True, "AccountId")

    _str_total_rows = (GamingTablePlaysSession.Rows.Count() + ErrorsDataTable.Rows.Count()).ToString()
    _str_valid_rows = GamingTablePlaysSession.Rows.Count().ToString()
    _str_not_valid_rows = ErrorsDataTable.Rows.Count().ToString()
    _str_distinct_players = _datatable_unique_accounts.Rows.Count().ToString()

    _tmp_total_awarded_points = GamingTablePlaysSession.Compute("SUM(AwaredPoints)", "")
    If _tmp_total_awarded_points Is Nothing OrElse IsDBNull(_tmp_total_awarded_points) Then
      _str_total_awarded_points = "0"
    Else
      _str_total_awarded_points = GamingTablePlaysSession.Compute("SUM(AwaredPoints)", "").ToString()
    End If

    _str_document_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6842) + ": " + _str_total_rows + "</br>"
    _str_document_text = _str_document_text + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6843) + ": " + _str_valid_rows + "</br>"
    _str_document_text = _str_document_text + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6844) + ": " + _str_not_valid_rows + "</br>"
    _str_document_text = _str_document_text + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6845) + ": " + _str_distinct_players + "</br>"
    _str_document_text = _str_document_text + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6846) + ": " + _str_total_awarded_points + "</br>"

    If _str_errors.Trim() = String.Empty Then
      _str_errors = "</br>" + GLB_NLS_GUI_CLASS_II.GetString(416) '"No se han encontrado errores."
      _message_Text = GLB_NLS_GUI_CLASS_II.GetString(434)
      ButtonOKText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1238)
      ButtonCancelText = GLB_NLS_GUI_CONTROLS.GetString(2)
      _frm_dialog.Init(_message_Text, ButtonOKText, ButtonCancelText)
    Else
      _str_document_text = _str_document_text + "</br>" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(6847) + ": " + "</br>"
      _frm_dialog.Init(_message_Text, ButtonOKText, ButtonCancelText) '"�Continuar procesando las sesiones correctas?"
    End If

    _str_document_text = _str_document_text + _str_errors

    Call m_gaming_table_plays_sessions.WriteLog(_str_errors.Replace("</br>", vbCrLf))

    _frm_dialog.wb_browser.DocumentText = _str_document_text
    Call _frm_dialog.ShowDialog()

    If (_frm_dialog.DialogResult = Windows.Forms.DialogResult.Yes) Then
      Call m_gaming_table_plays_sessions.WriteLog(_message_Text + ": " + ButtonOKText)
      Return True
    Else
      Call m_gaming_table_plays_sessions.WriteLog(_message_Text + ": " + ButtonCancelText)
      Return False
    End If

  End Function

  ' PURPOSE: stop Import Thread
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub StopGamingTablePlaysSessionsImport()

    If Not m_thread_import Is Nothing Then
      If (m_thread_import.ThreadState <> ThreadState.Stopped) Then
        m_thread_import.Abort()
        pgb_import_progress.Value = 0
        tb_output.Text += vbCrLf + GLB_NLS_GUI_CLASS_II.GetString(431)
        Call m_gaming_table_plays_sessions.WriteLog(vbCrLf + GLB_NLS_GUI_CLASS_II.GetString(431))
        tb_output.SelectionStart = tb_output.TextLength
        tb_output.ScrollToCaret()
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
        Me.dti_excel_import.Enabled = True
      End If
    End If

  End Sub 'StopGamingTableSessionsImport

  ' PURPOSE: Audit import action
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RegisterAudit()
    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _file_name As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_CODE_GAMING_TABLES)

    _auditor_data.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5882), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5882))

    _file_name = System.IO.Path.GetFileName(dti_excel_import.Value)
    _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(550), _file_name)

    _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                         GLB_CurrentUser.Id, _
                         GLB_CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                         0)

  End Sub ' RegisterAudit

#End Region

#Region "Events"
  Private Sub dti_excel_import_FileImported(ByRef ImportedDataTable As System.Data.DataTable) Handles dti_excel_import.FileImported
    Call StartGamingTablePlaysSessionsImport(ImportedDataTable)
  End Sub
#End Region

End Class
