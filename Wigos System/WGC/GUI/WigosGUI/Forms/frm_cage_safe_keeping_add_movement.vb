'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_cage_safe_add_movement
'
' DESCRIPTION : Add safe keeping movements
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-APR-2015  DRV    Initial version. Backlog Item 686
' 23-NOV-2015  FOS    Bug 6771:Payment tickets & handpays
' 07-OCT-2016  FAV    Fixed Bug 18772: Exception making deposits in "custodia" 
'--------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports WSI.Common.CageMeters
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE

Public Class frm_cage_safe_keeping_add_movement
  Inherits frm_base_edit

#Region "CONSTANTS"
  Private Const SQL_COLUMN_DATETIME As Integer = 0
  Private Const SQL_COLUMN_AMOUNT As Integer = 1
  Private Const SQL_COLUMN_BALANCE As Integer = 2
  Private Const SQL_COLUMN_TYPE As Integer = 3

  Private Const GRID_COLUMN_DATETIME As Integer = 0
  Private Const GRID_COLUMN_AMOUNT As Integer = 1
  Private Const GRID_COLUMN_BALANCE As Integer = 2

  Private Const GRID_WIDTH_COLUMN_DATETIME As Integer = 2100
  Private Const GRID_WIDTH_COLUMN_AMOUNT As Integer = 1700
  Private Const GRID_WIDTH_COLUMN_BALANCE As Integer = 1700

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1
#End Region

#Region "OVERRIDES"
  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    If CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_DEPOSIT_MOVEMENT).Read Then
      Me.FormId = ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_DEPOSIT_MOVEMENT
    Else
      Me.FormId = ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_WITHDRAW_MOVEMENT
    End If

    MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Call GUI_StyleSheet()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6192)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    Me.btn_add_amount.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_DEPOSIT_MOVEMENT).Write
    Me.btn_sub_amount.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SAFE_KEEPING_ADD_WITHDRAW_MOVEMENT).Write

    Me.gb_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6190)
    Me.ef_holder_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6191)
    Me.ef_balance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6189)
    Me.gb_operations.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6220)
    Me.ef_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6188)
    Me.btn_add_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6193)
    Me.btn_sub_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6194)
    Me.lbl_blocked.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(558).ToUpper()
    Me.cmb_cage_sessions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6213)
    Me.gb_last_movements.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6218)

    Call SetComboCageSessions(cmb_cage_sessions, False)

    ef_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 10)
  End Sub

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_SAFE_KEEPING_ADD_MOVEMENT
        DbStatus = ENUM_STATUS.STATUS_OK

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _data As CLASS_SAFE_KEEPING_ADD_MOVEMENT

    _data = DbReadObject

    Call FillGrid(_data.Movements)

    Me.ef_holder_name.TextValue = _data.HolderName
    Me.ef_balance.TextValue = GUI_FormatCurrency(_data.Balance)
    Me.ef_amount.TextValue = ""

    If _data.Blocked Then
      ef_amount.Location = New Point(ef_amount.Location.X, ef_amount.Location.Y - 10)
      ef_amount.Enabled = False
      btn_add_amount.Enabled = False
      btn_sub_amount.Enabled = False
      cmb_cage_sessions.Enabled = False
      lbl_blocked.Visible = True
    End If

    MyBase.GUI_SetScreenData(SqlCtx)
  End Sub

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overloads Sub ShowEditItem(ByVal SafeKeepingAccountId As Integer)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = SafeKeepingAccountId

    Call LoadData()

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

#End Region

#Region "PRIVATE FUNCTIONS"

  ' PURPOSE : Initializes movements sheet.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Column(GRID_COLUMN_DATETIME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6187)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_COLUMN_DATETIME
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_AMOUNT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6188)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_COLUMN_AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_BALANCE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6219)
      .Column(GRID_COLUMN_BALANCE).Width = GRID_WIDTH_COLUMN_BALANCE
      .Column(GRID_COLUMN_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With
  End Sub

  ' PURPOSE : Fill the movements sheet.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub FillGrid(ByVal Data As DataTable)
    Dim _idx As Int32
    Dim _color As Color
    Dim _amount As Currency

    Me.Grid.Clear()

    For Each _row As DataRow In Data.Rows
      _idx = Me.Grid.AddRow()
      _color = Color.Blue
      _amount = CType(_row.Item(SQL_COLUMN_AMOUNT), Decimal)
      If _row.Item(SQL_COLUMN_TYPE) = OperationCode.SAFE_KEEPING_WITHDRAW Then
        _color = Color.Red
        _amount = CType(_row.Item(SQL_COLUMN_AMOUNT), Decimal) * -1
      End If

      Me.Grid.Cell(_idx, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(_row.Item(SQL_COLUMN_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.Grid.Cell(_idx, GRID_COLUMN_AMOUNT).Value = IIf(_amount.SqlMoney >= 0, "+", "") & GUI_FormatCurrency(_amount)
      Me.Grid.Cell(_idx, GRID_COLUMN_AMOUNT).ForeColor = _color
      Me.Grid.Cell(_idx, GRID_COLUMN_BALANCE).Value = GUI_FormatCurrency(_row.Item(SQL_COLUMN_BALANCE))
    Next
  End Sub

  ' PURPOSE : Perform deposit or withdraw operation.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub DepositOrWithdraw(ByVal IsDeposit As Boolean)
    Dim _query As String
    Dim _cage_session As Int64
    Dim _operation As OperationCode
    Dim _operation_id As Long
    Dim _data As CLASS_SAFE_KEEPING_ADD_MOVEMENT
    Dim _amount As Currency
    Dim _str_operation As String
    Dim _input_params As VoucherSafeKeepingFillInFillOut.VoucherSafeKeepingFillInFillOutInputParams
    Dim Voucher_Keeping As VoucherSafeKeepingFillInFillOut

    Voucher_Keeping = Nothing

    If Not CheckDepositOrWithdrawAvailability(IsDeposit) Then

      Return
    End If


    _amount = CType(ef_amount.Value, Currency)
    _data = DbReadObject

    _str_operation = IIf(IsDeposit, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6196), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6197))
    If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6186), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , _str_operation, _amount.ToString()) <> ENUM_MB_RESULT.MB_RESULT_YES Then

      Return
    End If

    _cage_session = Me.cmb_cage_sessions.Value

    _query = GetDepositOrWithdrawQuery(IsDeposit)

    _operation = IIf(IsDeposit, OperationCode.SAFE_KEEPING_DEPOSIT, OperationCode.SAFE_KEEPING_WITHDRAW)

    Try
      Using _db_trx As New DB_TRX

        'Creates de Operation and the safe keeping movement
        Operations.DB_InsertOperation(_operation, 0, 0, 0, 0, _amount, 0, 0, 0, String.Empty, _operation_id, _db_trx.SqlTransaction)

        Using _sql_cmd As New SqlCommand(_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("pAmount", SqlDbType.Money).Value = _amount.SqlMoney
          _sql_cmd.Parameters.Add("pOperationId", SqlDbType.BigInt).Value = _operation_id
          _sql_cmd.Parameters.Add("pOperationType", SqlDbType.BigInt).Value = _operation
          _sql_cmd.Parameters.Add("pUserId", SqlDbType.BigInt).Value = CurrentUser.Id
          _sql_cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = _cage_session
          _sql_cmd.Parameters.Add("@pSafeKeepingId", SqlDbType.BigInt).Value = DbObjectId

          _sql_cmd.ExecuteNonQuery()
        End Using

        'Updates the Cage Meter
        Using _sql_cmd As New SqlCommand("CageUpdateMeter", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.CommandType = CommandType.StoredProcedure

          If IsDeposit Then
            _sql_cmd.Parameters.Add("@pValueIn", SqlDbType.Money).Value = _amount.SqlMoney
            _sql_cmd.Parameters.Add("@pValueOut", SqlDbType.Money).Value = 0
          Else
            _sql_cmd.Parameters.Add("@pValueIn", SqlDbType.Money).Value = 0
            _sql_cmd.Parameters.Add("@pValueOut", SqlDbType.Money).Value = _amount.SqlMoney
          End If
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _cage_session
          _sql_cmd.Parameters.Add("@pConceptId", SqlDbType.BigInt).Value = CageConceptId.SafeKeeping
          _sql_cmd.Parameters.Add("@pSourceTagetId", SqlDbType.BigInt).Value = CageSystemSourceTarget.System
          _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency()
          _sql_cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).Value = 0 'Is cash value
          _sql_cmd.Parameters.Add("@pOperation", SqlDbType.Int).Value = UpdateCageMetersOperationType.Increment
          _sql_cmd.Parameters.Add("@pSessionGetMode", SqlDbType.Int).Value = UpdateCageMetersgetGetSessionMode.FromParameter

          _sql_cmd.ExecuteNonQuery()

        End Using

        Call AuditDepositOrWithdraw(IsDeposit)

        'Generate Voucher
        _input_params = New VoucherSafeKeepingFillInFillOut.VoucherSafeKeepingFillInFillOutInputParams
        _input_params.CageSession = WSI.Common.Cage.GetCageSessionName(_cage_session)
        _input_params.GuiUser = CurrentUser.Name
        _input_params.HolderId = _data.HolderId
        _input_params.HolderName = _data.HolderName
        _input_params.AmountFillInOut = _amount.SqlMoney
        Select Case _operation
          Case OperationCode.SAFE_KEEPING_DEPOSIT
            _input_params.CurrentBalance = _data.Balance.SqlMoney + _amount.SqlMoney
          Case OperationCode.SAFE_KEEPING_WITHDRAW
            _input_params.CurrentBalance = _data.Balance.SqlMoney - _amount.SqlMoney
        End Select
        _input_params.OperationId = _operation_id
        _input_params.TypeFillInOutOperation = _operation

        If CreateVoucherDepositOrWithdraw(_input_params, _db_trx, Voucher_Keeping) Then
          _db_trx.Commit()

          'Now print Voucher
          HtmlPrinter.AddHtml(Voucher_Keeping.VoucherHTML)
        Else

          _db_trx.Rollback()
          Throw New Exception("Error creating voucher on DepositOrWithdraw")
        End If

      End Using

      Call LoadData()

    Catch _ex As Exception
      Log.Exception(_ex)

      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6185), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
    End Try

  End Sub

  ' PURPOSE : Returns the query to update/insert into safe keeping accounts and movements database tables.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetDepositOrWithdrawQuery(ByVal IsDeposit As Boolean) As String
    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()


    _str_sql.AppendLine(" DECLARE @FinalBalanceUpdate AS TABLE (BALANCE MONEY) ")
    _str_sql.AppendLine(" DECLARE @FinalBalance AS MONEY ")
    _str_sql.AppendLine(" UPDATE   SAFE_KEEPING_ACCOUNTS ")
    If IsDeposit Then
      _str_sql.AppendLine("    SET   SKA_BALANCE = SKA_BALANCE + @pAmount ")
    Else
      _str_sql.AppendLine("    SET   SKA_BALANCE = SKA_BALANCE - @pAmount ")
    End If
    _str_sql.AppendLine(" , SKA_LAST_OPERATION = GETDATE() ")
    _str_sql.AppendLine(" OUTPUT INSERTED.SKA_BALANCE INTO @FinalBalanceUpdate ")
    _str_sql.AppendLine(" WHERE SKA_SAFE_KEEPING_ID = @pSafeKeepingId")

    _str_sql.AppendLine(" SELECT TOP 1 @FinalBalance = BALANCE  FROM @FinalBalanceUpdate")

    _str_sql.AppendLine(" INSERT INTO   SAFE_KEEPING_OPERATIONS ")
    _str_sql.AppendLine("             ( SKO_OPERATION_ID ")
    _str_sql.AppendLine("             , SKO_DATETIME ")
    _str_sql.AppendLine("             , SKO_TYPE ")
    _str_sql.AppendLine("             , SKO_USER_ID ")
    _str_sql.AppendLine("             , SKO_CAGE_SESION ")
    _str_sql.AppendLine("             , SKO_SAFE_KEEPING_ID ")
    _str_sql.AppendLine("             , SKO_AMOUNT ")
    _str_sql.AppendLine("             , SKO_FINAL_BALANCE ")
    _str_sql.AppendLine("             ) VALUES ")
    _str_sql.AppendLine("             ( @pOperationId ")
    _str_sql.AppendLine("             , GETDATE() ")
    _str_sql.AppendLine("             , @pOperationType ")
    _str_sql.AppendLine("             , @pUserId ")
    _str_sql.AppendLine("             , @pCageSessionId ")
    _str_sql.AppendLine("             , @pSafeKeepingId ")
    _str_sql.AppendLine("             , @pAmount ")
    _str_sql.AppendLine("             , @FinalBalance ")
    _str_sql.AppendLine("             ) ")

    Return _str_sql.ToString()
  End Function

  ' PURPOSE : Loads database data and set it to screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub LoadData()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    GUI_SetScreenData(0)
  End Sub

  ' PURPOSE : Checks screen data and account configuration before performing the deposit or withdraw.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function CheckDepositOrWithdrawAvailability(ByVal IsDeposit As Boolean) As Boolean

    Dim _data As CLASS_SAFE_KEEPING_ADD_MOVEMENT
    Dim _amount As Currency

    _data = DbReadObject

    If Me.cmb_cage_sessions.Value < 1 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6184), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      cmb_cage_sessions.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(ef_amount.Value) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6181), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      ef_amount.Focus()

      Return False
    End If

    _amount = CType(ef_amount.Value, Currency)

    If _amount.SqlMoney = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6181), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      ef_amount.Focus()

      Return False
    End If

    If IsDeposit Then
      If _data.MaxBalance.SqlMoney <> -1 And (_data.Balance.SqlMoney + _amount.SqlMoney) > _data.MaxBalance.SqlMoney Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6182), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , GUI_FormatCurrency(_data.MaxBalance))
        ef_amount.Focus()

        Return False
      End If
    Else
      If _data.Balance.SqlMoney - _amount.SqlMoney < 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6183), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        ef_amount.Focus()

        Return False
      End If
    End If

    Return True
  End Function

  ' PURPOSE : Creates audit register with the deposit or withdraw operation.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function AuditDepositOrWithdraw(ByVal IsDeposit As Boolean) As Boolean
    Dim _auditor As CLASS_AUDITOR_DATA
    Dim _str_descritpion As String
    _auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    _auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(6192), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6192))

    _str_descritpion = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6190) & ": " & ef_holder_name.TextValue & " " & _
                       GLB_NLS_GUI_PLAYER_TRACKING.GetString(6188) & ": " & ef_amount.TextValue & " " & _
                       GLB_NLS_GUI_PLAYER_TRACKING.GetString(6198) & ": " & IIf(IsDeposit, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6196), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6197))

    _auditor.SetField(0, Nothing, _str_descritpion, CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)
    _auditor.Notify(CurrentUser.GuiId, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)
  End Function

  ' PURPOSE : Generate Voucher
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function CreateVoucherDepositOrWithdraw(ByVal InputParams As VoucherSafeKeepingFillInFillOut.VoucherSafeKeepingFillInFillOutInputParams, _
                                                  ByVal DbTrx As DB_TRX, _
                                                  ByRef Voucher_Keeping As VoucherSafeKeepingFillInFillOut) As Boolean
    Try

      Voucher_Keeping = New VoucherSafeKeepingFillInFillOut(PrintMode.Print, InputParams, DbTrx.SqlTransaction)

      Voucher_Keeping.PrintMode = PrintMode.Print
      Voucher_Keeping.PrintVoucher = True

      Voucher.PageSetup()
      Voucher_Keeping.VoucherHTML = VoucherManager.FontSetup(Voucher_Keeping.VoucherHTML)

      If (Voucher_Keeping.Save(InputParams.OperationId, DbTrx.SqlTransaction)) Then

        Return True
      Else

        Return False
      End If

    Catch ex As Exception

      Return False
    End Try

  End Function

  ' PURPOSE: To set the cage sessions combo box
  '
  ' PARAMS:
  '       - Combo
  '       - ShowEmptyElement
  '
  ' RETURNS:
  '       - None
  '
  ' NOTES:
  Public Sub SetComboCageSessions(ByRef Combo As uc_combo, Optional ByVal ShowEmptyElement As Boolean = False)
    Dim _sb As StringBuilder
    Dim _table As DataTable

    _sb = New StringBuilder

    _sb.AppendLine("    SELECT   CGS_CAGE_SESSION_ID  ")
    _sb.AppendLine("           , CGS_SESSION_NAME ")
    _sb.AppendLine("      FROM   CAGE_SESSIONS ")
    _sb.AppendLine("INNER JOIN   GUI_USERS ")
    _sb.AppendLine("        ON   CGS_OPEN_USER_ID = GU_USER_ID ")
    _sb.AppendLine("     WHERE   CGS_CLOSE_DATETIME IS NULL ")
    _sb.AppendLine("  ORDER BY   CGS_OPEN_DATETIME ASC ")

    Try

      _table = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Catch ex As Exception
      _table = New DataTable
    End Try

    Call Combo.Clear()

    If ShowEmptyElement Then
      Combo.Add(-1, String.Empty)
    End If

    Call Combo.Add(_table)
    Combo.TextValue = String.Empty
    Combo.SelectedIndex = -1
  End Sub ' SetComboCageSessions

#End Region

#Region "EVENTS"

  Private Sub btn_add_amount_ClickEvent() Handles btn_add_amount.ClickEvent
    Call DepositOrWithdraw(True)
  End Sub

  Private Sub btn_sub_amount_ClickEvent() Handles btn_sub_amount.ClickEvent
    Call DepositOrWithdraw(False)
  End Sub

#End Region

End Class
