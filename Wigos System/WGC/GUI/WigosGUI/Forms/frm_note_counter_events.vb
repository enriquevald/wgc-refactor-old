'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_note_counter_events
' DESCRIPTION:   
' AUTHOR:        Humberto Braojos
' CREATION DATE: 27-AUG-2014
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 26-AUG-2014 HBB 
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Data.SqlClient
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Text
Imports WSI.Common.Misc
Imports WSI.Common.NoteScanner

Public Delegate Sub InvokeSetEventToGrid(ByVal EventReceived As String)
Public Class frm_note_counter_events
  Inherits frm_base

#Region "Constants"
  Private Const GRID_COLUMN_EVENT_TEXT = 0
  Private Const GRID_COLUMNS = 1
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_WIDTH_EVENT_TEXT = 7945
#End Region

  'Dim WithEvents m_frm_cage_control As frm_cage_control
  Dim m_list_of_event As List(Of String)

  Public Sub New(ByVal ListOfEvent As List(Of String))

    MyBase.New()
    m_list_of_event = ListOfEvent
    ' m_frm_cage_control = FrmCageControl
    InitializeComponent()
    Call Initialize()
  End Sub

#Region "Override functions"

  Protected Overrides Sub GUI_InitControls()
    If Me.dg_note_counter_event.NumRows = -1 Then
      Call GUI_StyleSheet()
    End If
    btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5347)

  End Sub

  'Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
  '  ' MyBase.GUI_Closing(CloseCanceled)
  '  Me.Hide()
  'End Sub
#End Region
  Protected Overrides Sub gui_exit()
    Me.Hide()
  End Sub

  Private Sub GUI_StyleSheet()

    With Me.dg_note_counter_event
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Column(GRID_COLUMN_EVENT_TEXT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5345)
      .Column(GRID_COLUMN_EVENT_TEXT).Width = GRID_WIDTH_EVENT_TEXT
      .Column(GRID_COLUMN_EVENT_TEXT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Me.ShowDialog()

  End Sub

  Public Sub Clear()
    dg_note_counter_event.Clear()
  End Sub

  Public Sub SetEventToGrid(ByVal EventText As String)

    dg_note_counter_event.AddRow()
    dg_note_counter_event.Cell(dg_note_counter_event.NumRows - 1, GRID_COLUMN_EVENT_TEXT).Value = EventText

  End Sub

  Private Sub Initialize()
    Call GUI_InitControls()
    If Me.m_list_of_event.Count > 0 Then
      For Each _str As String In Me.m_list_of_event
        Call SetEventToGrid(_str)
      Next
    End If
  End Sub


  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent
    Me.Hide()
  End Sub

  ' PURPOSE: To close the form if ESC button is pressed
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  ' 
  Private Sub frm_note_counter_events_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
    If (e.KeyCode = Keys.Escape) Then
      Me.Hide()
    End If
  End Sub 'frm_note_counter_events_KeyDown
End Class