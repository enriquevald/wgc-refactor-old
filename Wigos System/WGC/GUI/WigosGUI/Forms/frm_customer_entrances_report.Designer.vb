﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customer_entrances_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.chk_holder_data = New System.Windows.Forms.CheckBox()
    Me.gb_last_activity = New System.Windows.Forms.GroupBox()
    Me.rb_order_by_account_date = New System.Windows.Forms.RadioButton()
    Me.rb_order_by_date_account = New System.Windows.Forms.RadioButton()
    Me.chk_show_totals = New System.Windows.Forms.CheckBox()
    Me.lbl_total_entradas = New System.Windows.Forms.Label()
    Me.lbl_numero_total_entradas = New System.Windows.Forms.Label()
    Me.chk_inside_customers = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_last_activity.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.AutoSize = True
    Me.panel_filter.Controls.Add(Me.chk_inside_customers)
    Me.panel_filter.Controls.Add(Me.lbl_numero_total_entradas)
    Me.panel_filter.Controls.Add(Me.lbl_total_entradas)
    Me.panel_filter.Controls.Add(Me.chk_show_totals)
    Me.panel_filter.Controls.Add(Me.gb_last_activity)
    Me.panel_filter.Controls.Add(Me.chk_holder_data)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1171, 140)
    Me.panel_filter.TabIndex = 4
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_holder_data, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_last_activity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_totals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_total_entradas, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_numero_total_entradas, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_inside_customers, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 144)
    Me.panel_data.Size = New System.Drawing.Size(1171, 463)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1165, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1165, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(269, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 2
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(6, 6)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 84)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'chk_holder_data
    '
    Me.chk_holder_data.AutoSize = True
    Me.chk_holder_data.Location = New System.Drawing.Point(588, 13)
    Me.chk_holder_data.Name = "chk_holder_data"
    Me.chk_holder_data.Size = New System.Drawing.Size(128, 17)
    Me.chk_holder_data.TabIndex = 3
    Me.chk_holder_data.Text = "xShowHolderData"
    Me.chk_holder_data.UseVisualStyleBackColor = True
    '
    'gb_last_activity
    '
    Me.gb_last_activity.Controls.Add(Me.rb_order_by_account_date)
    Me.gb_last_activity.Controls.Add(Me.rb_order_by_date_account)
    Me.gb_last_activity.Location = New System.Drawing.Point(6, 92)
    Me.gb_last_activity.Name = "gb_last_activity"
    Me.gb_last_activity.Size = New System.Drawing.Size(257, 45)
    Me.gb_last_activity.TabIndex = 1
    Me.gb_last_activity.TabStop = False
    Me.gb_last_activity.Text = "xOrderBy"
    '
    'rb_order_by_account_date
    '
    Me.rb_order_by_account_date.AutoSize = True
    Me.rb_order_by_account_date.Location = New System.Drawing.Point(123, 19)
    Me.rb_order_by_account_date.Name = "rb_order_by_account_date"
    Me.rb_order_by_account_date.Size = New System.Drawing.Size(77, 17)
    Me.rb_order_by_account_date.TabIndex = 1
    Me.rb_order_by_account_date.Text = "xAccount"
    Me.rb_order_by_account_date.UseVisualStyleBackColor = True
    '
    'rb_order_by_date_account
    '
    Me.rb_order_by_date_account.AutoSize = True
    Me.rb_order_by_date_account.Checked = True
    Me.rb_order_by_date_account.Location = New System.Drawing.Point(14, 19)
    Me.rb_order_by_date_account.Name = "rb_order_by_date_account"
    Me.rb_order_by_date_account.Size = New System.Drawing.Size(59, 17)
    Me.rb_order_by_date_account.TabIndex = 0
    Me.rb_order_by_date_account.TabStop = True
    Me.rb_order_by_date_account.Text = "xDate"
    Me.rb_order_by_date_account.UseVisualStyleBackColor = True
    '
    'chk_show_totals
    '
    Me.chk_show_totals.AutoSize = True
    Me.chk_show_totals.Location = New System.Drawing.Point(588, 36)
    Me.chk_show_totals.Name = "chk_show_totals"
    Me.chk_show_totals.Size = New System.Drawing.Size(98, 17)
    Me.chk_show_totals.TabIndex = 4
    Me.chk_show_totals.Text = "xShowTotals"
    Me.chk_show_totals.UseVisualStyleBackColor = True
    '
    'lbl_total_entradas
    '
    Me.lbl_total_entradas.AutoSize = True
    Me.lbl_total_entradas.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_entradas.Location = New System.Drawing.Point(837, 108)
    Me.lbl_total_entradas.Name = "lbl_total_entradas"
    Me.lbl_total_entradas.Size = New System.Drawing.Size(119, 18)
    Me.lbl_total_entradas.TabIndex = 15
    Me.lbl_total_entradas.Text = "xTotalEntradas"
    '
    'lbl_numero_total_entradas
    '
    Me.lbl_numero_total_entradas.AutoSize = True
    Me.lbl_numero_total_entradas.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_numero_total_entradas.Location = New System.Drawing.Point(960, 108)
    Me.lbl_numero_total_entradas.Name = "lbl_numero_total_entradas"
    Me.lbl_numero_total_entradas.Size = New System.Drawing.Size(19, 18)
    Me.lbl_numero_total_entradas.TabIndex = 16
    Me.lbl_numero_total_entradas.Text = "0"
    '
    'chk_inside_customers
    '
    Me.chk_inside_customers.AutoSize = True
    Me.chk_inside_customers.Location = New System.Drawing.Point(588, 59)
    Me.chk_inside_customers.Name = "chk_inside_customers"
    Me.chk_inside_customers.Size = New System.Drawing.Size(130, 17)
    Me.chk_inside_customers.TabIndex = 5
    Me.chk_inside_customers.Text = "xInsideCustomers"
    Me.chk_inside_customers.UseVisualStyleBackColor = True
    '
    'frm_customer_entrances_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1179, 611)
    Me.Name = "frm_customer_entrances_report"
    Me.Text = "frm_customer_entrances_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_last_activity.ResumeLayout(False)
    Me.gb_last_activity.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_holder_data As System.Windows.Forms.CheckBox
  Friend WithEvents gb_last_activity As System.Windows.Forms.GroupBox
  Friend WithEvents rb_order_by_account_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_order_by_date_account As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_totals As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_numero_total_entradas As System.Windows.Forms.Label
  Friend WithEvents lbl_total_entradas As System.Windows.Forms.Label
  Friend WithEvents chk_inside_customers As System.Windows.Forms.CheckBox
End Class
