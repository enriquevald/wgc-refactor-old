'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_flags_edit.vb
' DESCRIPTION:   Flags edition form
' AUTHOR:        Xavier Cots
' CREATION DATE: 27-AUG-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-AUG-2012  XCD    Initial version.
' 21-APR-2017  ETP    WIGOS-706: Junkets - Technical story - Refactor FlagType Enum to common
'--------------------------------------------------------------------

Option Explicit On 

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports WSI.Common

#End Region

Public Class frm_flags_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_EXP_TIME As Integer = 4 ' In DB Int
  Private Const EXP_TYPE_INIT_INDEX As Integer = -1 ' Value of selected index cmb_expiration_type

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_expiration As System.Windows.Forms.GroupBox
  Friend WithEvents gb_state As System.Windows.Forms.GroupBox
  Friend WithEvents rb_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents rb_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents tb_description As System.Windows.Forms.TextBox
  Friend WithEvents dp_expiration_date As GUI_Controls.uc_date_picker
  Friend WithEvents cd_button_color As System.Windows.Forms.ColorDialog
  Friend WithEvents btn_change_color As System.Windows.Forms.Button
  Friend WithEvents uc_expiration_type As GUI_Controls.uc_combo
  Friend WithEvents ef_expiration_number As GUI_Controls.uc_entry_field
  Friend WithEvents rb_expiration_never As System.Windows.Forms.RadioButton
  Friend WithEvents rb_expiration_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_expiration_interval As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_color_rectangle As System.Windows.Forms.Label
  Friend WithEvents lbl_color_text As System.Windows.Forms.Label

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.gb_expiration = New System.Windows.Forms.GroupBox
    Me.rb_expiration_never = New System.Windows.Forms.RadioButton
    Me.rb_expiration_date = New System.Windows.Forms.RadioButton
    Me.rb_expiration_interval = New System.Windows.Forms.RadioButton
    Me.dp_expiration_date = New GUI_Controls.uc_date_picker
    Me.ef_expiration_number = New GUI_Controls.uc_entry_field
    Me.uc_expiration_type = New GUI_Controls.uc_combo
    Me.gb_state = New System.Windows.Forms.GroupBox
    Me.rb_disabled = New System.Windows.Forms.RadioButton
    Me.rb_enabled = New System.Windows.Forms.RadioButton
    Me.lbl_description = New System.Windows.Forms.Label
    Me.tb_description = New System.Windows.Forms.TextBox
    Me.btn_change_color = New System.Windows.Forms.Button
    Me.cd_button_color = New System.Windows.Forms.ColorDialog
    Me.lbl_color_text = New System.Windows.Forms.Label
    Me.lbl_color_rectangle = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_expiration.SuspendLayout()
    Me.gb_state.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.btn_change_color)
    Me.panel_data.Controls.Add(Me.lbl_color_rectangle)
    Me.panel_data.Controls.Add(Me.lbl_color_text)
    Me.panel_data.Controls.Add(Me.lbl_description)
    Me.panel_data.Controls.Add(Me.tb_description)
    Me.panel_data.Controls.Add(Me.gb_state)
    Me.panel_data.Controls.Add(Me.gb_expiration)
    Me.panel_data.Controls.Add(Me.ef_name)
    Me.panel_data.Size = New System.Drawing.Size(440, 256)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(22, 23)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.Size = New System.Drawing.Size(271, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 0
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    '
    'gb_expiration
    '
    Me.gb_expiration.Controls.Add(Me.rb_expiration_never)
    Me.gb_expiration.Controls.Add(Me.rb_expiration_date)
    Me.gb_expiration.Controls.Add(Me.rb_expiration_interval)
    Me.gb_expiration.Controls.Add(Me.dp_expiration_date)
    Me.gb_expiration.Controls.Add(Me.ef_expiration_number)
    Me.gb_expiration.Controls.Add(Me.uc_expiration_type)
    Me.gb_expiration.Location = New System.Drawing.Point(261, 140)
    Me.gb_expiration.Name = "gb_expiration"
    Me.gb_expiration.Size = New System.Drawing.Size(175, 110)
    Me.gb_expiration.TabIndex = 4
    Me.gb_expiration.TabStop = False
    Me.gb_expiration.Text = "xExpiraci�n"
    '
    'rb_expiration_never
    '
    Me.rb_expiration_never.AutoSize = True
    Me.rb_expiration_never.Location = New System.Drawing.Point(7, 82)
    Me.rb_expiration_never.Name = "rb_expiration_never"
    Me.rb_expiration_never.Size = New System.Drawing.Size(67, 17)
    Me.rb_expiration_never.TabIndex = 3
    Me.rb_expiration_never.TabStop = True
    Me.rb_expiration_never.Text = "xNunca"
    Me.rb_expiration_never.UseVisualStyleBackColor = True
    '
    'rb_expiration_date
    '
    Me.rb_expiration_date.AutoSize = True
    Me.rb_expiration_date.Location = New System.Drawing.Point(7, 55)
    Me.rb_expiration_date.Name = "rb_expiration_date"
    Me.rb_expiration_date.Size = New System.Drawing.Size(14, 13)
    Me.rb_expiration_date.TabIndex = 2
    Me.rb_expiration_date.TabStop = True
    Me.rb_expiration_date.UseVisualStyleBackColor = True
    '
    'rb_expiration_interval
    '
    Me.rb_expiration_interval.AutoSize = True
    Me.rb_expiration_interval.Location = New System.Drawing.Point(7, 25)
    Me.rb_expiration_interval.Name = "rb_expiration_interval"
    Me.rb_expiration_interval.Size = New System.Drawing.Size(14, 13)
    Me.rb_expiration_interval.TabIndex = 1
    Me.rb_expiration_interval.TabStop = True
    Me.rb_expiration_interval.UseVisualStyleBackColor = True
    '
    'dp_expiration_date
    '
    Me.dp_expiration_date.Checked = True
    Me.dp_expiration_date.IsReadOnly = False
    Me.dp_expiration_date.Location = New System.Drawing.Point(23, 49)
    Me.dp_expiration_date.Name = "dp_expiration_date"
    Me.dp_expiration_date.ShowCheckBox = False
    Me.dp_expiration_date.ShowUpDown = False
    Me.dp_expiration_date.Size = New System.Drawing.Size(148, 24)
    Me.dp_expiration_date.SufixText = "Sufix Text"
    Me.dp_expiration_date.SufixTextVisible = True
    Me.dp_expiration_date.TabIndex = 6
    Me.dp_expiration_date.TextWidth = 0
    Me.dp_expiration_date.Value = New Date(2012, 8, 23, 12, 20, 28, 300)
    '
    'ef_expiration_number
    '
    Me.ef_expiration_number.DoubleValue = 0
    Me.ef_expiration_number.IntegerValue = 0
    Me.ef_expiration_number.IsReadOnly = False
    Me.ef_expiration_number.Location = New System.Drawing.Point(23, 19)
    Me.ef_expiration_number.Name = "ef_expiration_number"
    Me.ef_expiration_number.Size = New System.Drawing.Size(56, 24)
    Me.ef_expiration_number.SufixText = "Sufix Text"
    Me.ef_expiration_number.SufixTextVisible = True
    Me.ef_expiration_number.TabIndex = 4
    Me.ef_expiration_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_number.TextValue = ""
    Me.ef_expiration_number.TextWidth = 0
    Me.ef_expiration_number.Value = ""
    '
    'uc_expiration_type
    '
    Me.uc_expiration_type.IsReadOnly = False
    Me.uc_expiration_type.Location = New System.Drawing.Point(85, 19)
    Me.uc_expiration_type.Name = "uc_expiration_type"
    Me.uc_expiration_type.SelectedIndex = -1
    Me.uc_expiration_type.Size = New System.Drawing.Size(86, 24)
    Me.uc_expiration_type.SufixText = "Sufix Text"
    Me.uc_expiration_type.SufixTextVisible = True
    Me.uc_expiration_type.TabIndex = 5
    Me.uc_expiration_type.TextWidth = 0
    '
    'gb_state
    '
    Me.gb_state.Controls.Add(Me.rb_disabled)
    Me.gb_state.Controls.Add(Me.rb_enabled)
    Me.gb_state.Location = New System.Drawing.Point(104, 140)
    Me.gb_state.Name = "gb_state"
    Me.gb_state.Size = New System.Drawing.Size(151, 75)
    Me.gb_state.TabIndex = 2
    Me.gb_state.TabStop = False
    Me.gb_state.Text = "xEstado"
    '
    'rb_disabled
    '
    Me.rb_disabled.AutoSize = True
    Me.rb_disabled.Location = New System.Drawing.Point(7, 44)
    Me.rb_disabled.Name = "rb_disabled"
    Me.rb_disabled.Size = New System.Drawing.Size(109, 17)
    Me.rb_disabled.TabIndex = 2
    Me.rb_disabled.TabStop = True
    Me.rb_disabled.Text = "xDeshabilitada"
    Me.rb_disabled.UseVisualStyleBackColor = True
    '
    'rb_enabled
    '
    Me.rb_enabled.AutoSize = True
    Me.rb_enabled.Location = New System.Drawing.Point(7, 21)
    Me.rb_enabled.Name = "rb_enabled"
    Me.rb_enabled.Size = New System.Drawing.Size(88, 17)
    Me.rb_enabled.TabIndex = 1
    Me.rb_enabled.TabStop = True
    Me.rb_enabled.Text = "xHabilitada"
    Me.rb_enabled.UseVisualStyleBackColor = True
    '
    'lbl_description
    '
    Me.lbl_description.Location = New System.Drawing.Point(3, 56)
    Me.lbl_description.Margin = New System.Windows.Forms.Padding(0)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(99, 78)
    Me.lbl_description.TabIndex = 9
    Me.lbl_description.Text = "xDescripci�n"
    Me.lbl_description.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'tb_description
    '
    Me.tb_description.Location = New System.Drawing.Point(104, 53)
    Me.tb_description.MaxLength = 200
    Me.tb_description.Multiline = True
    Me.tb_description.Name = "tb_description"
    Me.tb_description.Size = New System.Drawing.Size(332, 81)
    Me.tb_description.TabIndex = 1
    '
    'btn_change_color
    '
    Me.btn_change_color.Location = New System.Drawing.Point(145, 221)
    Me.btn_change_color.Name = "btn_change_color"
    Me.btn_change_color.Size = New System.Drawing.Size(75, 24)
    Me.btn_change_color.TabIndex = 3
    Me.btn_change_color.UseVisualStyleBackColor = True
    '
    'cd_button_color
    '
    Me.cd_button_color.SolidColorOnly = True
    '
    'lbl_color_text
    '
    Me.lbl_color_text.Location = New System.Drawing.Point(2, 221)
    Me.lbl_color_text.Margin = New System.Windows.Forms.Padding(0)
    Me.lbl_color_text.Name = "lbl_color_text"
    Me.lbl_color_text.Size = New System.Drawing.Size(99, 24)
    Me.lbl_color_text.TabIndex = 10
    Me.lbl_color_text.Text = "xColor"
    Me.lbl_color_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_color_rectangle
    '
    Me.lbl_color_rectangle.BackColor = System.Drawing.Color.Red
    Me.lbl_color_rectangle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_rectangle.Location = New System.Drawing.Point(111, 225)
    Me.lbl_color_rectangle.Name = "lbl_color_rectangle"
    Me.lbl_color_rectangle.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_rectangle.TabIndex = 11
    '
    'frm_flags_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(542, 264)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_flags_edit"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_expiration.ResumeLayout(False)
    Me.gb_expiration.PerformLayout()
    Me.gb_state.ResumeLayout(False)
    Me.gb_state.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_input_flag_name As String
  Private DeleteOperation As Boolean

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_FLAGS_EDIT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1260) ' Edici�n de Bandera

    ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) ' Bandera

    ' GB Expiration
    gb_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1288) ' Expiraci�n
    ' Expiration Number
    ef_expiration_number.Text = ""
    ef_expiration_number.Enabled = False
    ' Expiration Type Combo
    uc_expiration_type.Add(FLAG_EXPIRATION_TYPE.MINUTES, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1351)) 'Minutos
    uc_expiration_type.Add(FLAG_EXPIRATION_TYPE.HOURS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(259)) 'Horas
    uc_expiration_type.Add(FLAG_EXPIRATION_TYPE.DAYS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)) ' D�as
    uc_expiration_type.Add(FLAG_EXPIRATION_TYPE.MONTH, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1289)) ' Meses
    uc_expiration_type.SelectedIndex = EXP_TYPE_INIT_INDEX
    uc_expiration_type.Enabled = True
    rb_expiration_interval.Checked = True
    ' Expiration Date
    dp_expiration_date.Text = ""
    dp_expiration_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    dp_expiration_date.Value = (GUI_GetDate().AddHours(GetDefaultClosingTime())).AddMonths(1)
    dp_expiration_date.Enabled = False
    ' Expiration Never
    rb_expiration_never.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1287) ' Nunca

    ' State
    gb_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) ' Estado
    rb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260) ' Habilitada
    rb_enabled.Checked = True
    rb_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(248) ' Deshabilitada
    rb_disabled.Checked = False

    ' Color
    lbl_color_text.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1291) ' Color
    lbl_color_rectangle.BackColor = Color.White
    btn_change_color.Enabled = True
    btn_change_color.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1334)
    ' Color dialog
    cd_button_color.FullOpen = True

    ' Description
    lbl_description.Text = GLB_NLS_GUI_CONTROLS.GetString(312) ' Descripci�n
    tb_description.Text = ""

    ' Filters
    Call ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, CLASS_FLAG.MAX_NAME_LEN)
    Call ef_expiration_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_EXP_TIME)

  End Sub 'GUI_InitControls

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _flag_class As CLASS_FLAG
    Dim _nls_param1 As String
    Dim _nls_param2 As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_FLAG
        _flag_class = DbEditedObject
        _flag_class.FlagId = -1
        _flag_class.FlagCreated = GUI_GetDateTime()
        _flag_class.FlagColor = Color.White.ToArgb()
        _flag_class.ExpirationType = FLAG_EXPIRATION_TYPE.UNKNOWN

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
          _nls_param2 = m_input_flag_name
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
          _nls_param2 = _flag_class.FlagId
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(114), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
          Call ef_name.Focus()
        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
          Call ef_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
          _nls_param2 = _flag_class.FlagName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_ALREADY_EXISTS Then
          Call ef_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
          _nls_param2 = _flag_class.FlagName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(107), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
          _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1237) 'Cuentas
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(22), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1.ToLower, _
                          _nls_param2.ToLower)
        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED And DeleteOperation Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
          _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(949) ' Promociones
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(22), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1.ToLower, _
                          _nls_param2.ToLower)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          _flag_class = Me.DbEditedObject
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
          _nls_param2 = _flag_class.FlagName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(116), _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1, _
                          _nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        _flag_class = Me.DbEditedObject
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
        _nls_param2 = _flag_class.FlagName
        _rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(112), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _nls_param1, _
                        _nls_param2)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub

  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  Public Overloads Sub ShowEditItem(ByVal FlagId As Integer, ByVal FlagName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = FlagId
    Me.m_input_flag_name = FlagName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _flag_item As CLASS_FLAG

    _flag_item = DbReadObject

    ' flag name
    ef_name.Value = _flag_item.FlagName

    ' flag color
    lbl_color_rectangle.BackColor = System.Drawing.Color.FromArgb(_flag_item.FlagColor)
    cd_button_color.Color = lbl_color_rectangle.BackColor

    Select Case Me.ScreenMode
      Case ENUM_SCREEN_MODE.MODE_NEW
        ' Force user to select expiration interval
        rb_expiration_interval.Checked = True
        uc_expiration_type.SelectedIndex = EXP_TYPE_INIT_INDEX
      Case ENUM_SCREEN_MODE.MODE_EDIT
        ' Expiration
        Select Case _flag_item.ExpirationType
          Case FLAG_EXPIRATION_TYPE.DATETIME
            rb_expiration_date.Checked = True
            dp_expiration_date.Value = _flag_item.ExpirationDate
          Case FLAG_EXPIRATION_TYPE.NEVER
            rb_expiration_never.Checked = True
          Case Else
            rb_expiration_interval.Checked = True
            ef_expiration_number.Value = _flag_item.ExpirationInterval
            uc_expiration_type.Value = _flag_item.ExpirationType
        End Select
    End Select

    'status
    If _flag_item.FlagStatus.Equals(CLASS_FLAG.STATUS.ENABLED) Then
      rb_enabled.Checked = True
      rb_disabled.Checked = False
    Else
      rb_enabled.Checked = False
      rb_disabled.Checked = True
    End If

    ' description
    tb_description.Text = _flag_item.FlagDescription


  End Sub

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _flag_item As CLASS_FLAG

    _flag_item = DbEditedObject
    _flag_item.FlagName = ef_name.Value
    _flag_item.FlagStatus = IIf(rb_enabled.Checked, CLASS_FLAG.STATUS.ENABLED, CLASS_FLAG.STATUS.DISABLED)
    _flag_item.FlagColor = lbl_color_rectangle.BackColor.ToArgb()
    _flag_item.FlagDescription = tb_description.Text
    _flag_item.FlagType = AccountFlag.FlagTypes.Manual

    If rb_expiration_date.Checked Then
      _flag_item.ExpirationType = FLAG_EXPIRATION_TYPE.DATETIME
      _flag_item.ExpirationDate = dp_expiration_date.Value()
      _flag_item.ExpirationInterval = 0
    End If

    If rb_expiration_interval.Checked Then
      ' Interval value
      If String.IsNullOrEmpty(ef_expiration_number.Value()) Then
        _flag_item.ExpirationInterval = 0
      Else
        _flag_item.ExpirationInterval = GUI_ParseNumber(ef_expiration_number.Value())
      End If

      ' Interval type
      If uc_expiration_type.SelectedIndex = EXP_TYPE_INIT_INDEX Then
        _flag_item.ExpirationType = FLAG_EXPIRATION_TYPE.UNKNOWN
      Else
        _flag_item.ExpirationType = uc_expiration_type.Value()
      End If
      _flag_item.ExpirationDate = CLASS_FLAG.DATE_NULL
    End If

    If rb_expiration_never.Checked Then
      _flag_item.ExpirationType = FLAG_EXPIRATION_TYPE.NEVER
      _flag_item.ExpirationInterval = 0
      _flag_item.ExpirationDate = CLASS_FLAG.DATE_NULL
    End If

  End Sub

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_name

  End Sub ' GUI_SetInitialFocus

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _nls_param1 As String

    ' name blank field
    If ef_name.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_name.Focus()

      Return False
    End If

    ' Expiration number and type
    If rb_expiration_interval.Checked Then
      ' Expiration Number
      If String.IsNullOrEmpty(ef_expiration_number.Value()) Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1288)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call ef_expiration_number.Focus()
        Return False
      Else
        If GUI_ParseNumber(ef_expiration_number.Value()) < 1 Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1288)
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          Call ef_expiration_number.Focus()
          Return False
        End If
      End If
      ' expiration type
      If uc_expiration_type.SelectedIndex = EXP_TYPE_INIT_INDEX Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1288)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call uc_expiration_type.Focus()
        Return False
      End If
    End If

    ' Check only as new flag
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      ' expiration date > today()
      If rb_expiration_date.Checked And dp_expiration_date.Value < GUI_GetDateTime() Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(135), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call dp_expiration_date.Focus()
        Return False
      End If
    End If

    Return True

  End Function

#End Region

#Region " Private Functions "

#End Region

#Region " Events "

  Private Sub bt_color_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_change_color.Click
    If cd_button_color.ShowDialog() = Windows.Forms.DialogResult.OK Then
      lbl_color_rectangle.BackColor = cd_button_color.Color()
    End If
  End Sub

  Private Sub rb_expiration_interval_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_expiration_interval.CheckedChanged
    If rb_expiration_interval.Checked Then
      ef_expiration_number.Enabled = True
      uc_expiration_type.Enabled = True
    Else
      ef_expiration_number.Enabled = False
      uc_expiration_type.Enabled = False
    End If

  End Sub

  Private Sub rb_expiration_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_expiration_date.CheckedChanged
    If rb_expiration_date.Checked Then
      dp_expiration_date.Enabled = True
    Else
      dp_expiration_date.Enabled = False
    End If
  End Sub

#End Region

End Class
