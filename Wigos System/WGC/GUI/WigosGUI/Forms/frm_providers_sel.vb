'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_providers_sel.vb
'
' DESCRIPTION:   Providers selection form
'
' AUTHOR:        Daniel Moreno
'
' CREATION DATE: 30-APR-2013
'
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-APR-2013  DMR    Initial version.
' 02-MAY-2013  JBP    Adjust grid and columns sizes.
'                     Format columns results.
'                     Added SetDefaultValues ().
'                     Added GUI_SetFormId().
' 31-MAY-2013  DMR    Added GUI_ReportUpdateFilters().
' 03-JUN-2013  DMR    Selection Mode Single
' 10-JUN-2013  DMR    Added Payouts column
' 19-JUN-2013  ANM    Stetic modifications in form objects
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region ' Imports

Public Class frm_providers_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  Private m_color_row As System.Drawing.Color
  Private m_color_row2 As System.Drawing.Color
  Private m_color_row_last As System.Drawing.Color
  Private m_provider_ant As String
  Private m_provider_count As String
#End Region ' Members

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_JACKPOT As Integer = 2
  Private Const SQL_COLUMN_MULTIPLIER As Integer = 3
  Private Const SQL_COLUMN_REDEEMABLE_ONLY As Integer = 4
  Private Const SQL_COLUMN_3GS As Integer = 5
  Private Const SQL_COLUMN_IP As Integer = 6
  Private Const SQL_COLUMN_3GS_ID As Integer = 7
  Private Const SQL_COLUMN_GAME_NAME As Integer = 8
  Private Const SQL_COLUMN_GAME_PAYOUT_1 As Integer = 9
  Private Const SQL_COLUMN_GAME_PAYOUT_2 As Integer = 10
  Private Const SQL_COLUMN_GAME_PAYOUT_3 As Integer = 11
  Private Const SQL_COLUMN_GAME_PAYOUT_4 As Integer = 12
  Private Const SQL_COLUMN_GAME_PAYOUT_5 As Integer = 13
  Private Const SQL_COLUMN_GAME_PAYOUT_6 As Integer = 14
  Private Const SQL_COLUMN_GAME_PAYOUT_7 As Integer = 15
  Private Const SQL_COLUMN_GAME_PAYOUT_8 As Integer = 16
  Private Const SQL_COLUMN_GAME_PAYOUT_9 As Integer = 17
  Private Const SQL_COLUMN_GAME_PAYOUT_10 As Integer = 18

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_3GS As Integer = 3
  Private Const GRID_COLUMN_IP As Integer = 4
  Private Const GRID_COLUMN_3GS_ID As Integer = 5
  Private Const GRID_COLUMN_JACKPOT As Integer = 6
  Private Const GRID_COLUMN_MULTIPLIER As Integer = 7
  Private Const GRID_COLUMN_REDEEMABLE_ONLY As Integer = 8
  Private Const GRID_COLUMN_GAME_NAME As Integer = 9
  Private Const GRID_COLUMN_GAME_PAYOUTS As Integer = 10

  Private Const GRID_COLUMNS As Integer = 11
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_PAYOUTS_WIDTH As Integer = 7800 '10350
  Private Const GRID_GAME_NAME_WIDTH = 6255

  ' For report filters 
  Private m_providers_name As String
  Private m_3gs_checked As String
  Private m_games_checked As String

#End Region ' Constants

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_PROVIDERS_GAMES_SEL
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    m_provider_ant = ""
    m_provider_count = 0

    Dim _is_multisite_member As Boolean
    Dim _tables_master_mode As ENUM_TABLES_MASTER_MODE

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1916)

    Me.lbl_edition.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2010)

    m_color_row = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    m_color_row2 = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)
    _tables_master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0)

    If _is_multisite_member And _tables_master_mode = ENUM_TABLES_MASTER_MODE.MODE_MULTISITE_CENTER Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
      Me.lbl_edition.Visible = True
    ElseIf Me.Permissions.Write Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    End If

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(6)
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.ef_providers_games_name.Text = GLB_NLS_GUI_AUDITOR.GetString(341)
    Me.ef_providers_games_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 64)
    Me.chk_3gs.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920)
    Me.chk_games.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2012)

    Call Me.Grid.Clear()
    'Me.Grid.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function ' GUI_GetQueryType

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_providers_games_name
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    m_provider_ant = ""
    m_provider_count = 0

    Dim _sql_cmd As SqlCommand

    Dim _str_sql As StringBuilder
    _str_sql = New StringBuilder()

    _str_sql.AppendLine("SELECT   RD.PV_ID                                ")
    _str_sql.AppendLine("       , RD.PV_NAME                              ")
    _str_sql.AppendLine("       , RD.PV_SITE_JACKPOT                      ")
    _str_sql.AppendLine("       , RD.PV_POINTS_MULTIPLIER                 ")
    _str_sql.AppendLine("       , RD.PV_ONLY_REDEEMABLE                   ")
    _str_sql.AppendLine("       , RD.PV_3GS 					                    ")
    _str_sql.AppendLine("       , RD.PV_3GS_VENDOR_IP                     ")
    _str_sql.AppendLine("       , RD.PV_3GS_VENDOR_ID                     ")

    If Me.chk_games.Checked Then
      _str_sql.AppendLine("     , ISNULL (PG_GAME_NAME,'') AS PG_GAME_NAME")

      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_1,NULL) AS PG_PAYOUT_1  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_2,NULL) AS PG_PAYOUT_2  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_3,NULL) AS PG_PAYOUT_3  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_4,NULL) AS PG_PAYOUT_4  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_5,NULL) AS PG_PAYOUT_5  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_6,NULL) AS PG_PAYOUT_6  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_7,NULL) AS PG_PAYOUT_7  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_8,NULL) AS PG_PAYOUT_8  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_9,NULL) AS PG_PAYOUT_9  ")
      _str_sql.AppendLine("     , ISNULL (PG_PAYOUT_10,NULL) AS PG_PAYOUT_10")

    Else
      _str_sql.AppendLine("       , NULL AS PG_GAME_NAME                  ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_1                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_2                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_3                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_4                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_5                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_6                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_7                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_8                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_9                   ")
      _str_sql.AppendLine("       , NULL AS PG_PAYOUT_10                  ")
    End If

    _str_sql.AppendLine("  FROM   PROVIDERS RD                            ")

    If Me.chk_games.Checked Then
      _str_sql.AppendLine("  LEFT JOIN PROVIDERS_GAMES ON PV_ID = PG_PV_ID")
    End If

    _str_sql.AppendLine("    WHERE   " & GUI_FilterField("RD.PV_NAME", Me.ef_providers_games_name.Value, False, False, True))

    If Me.chk_3gs.Checked Then
      _str_sql.AppendLine(" AND  RD.PV_3GS = 1                             ")
    End If
    _str_sql.AppendLine(" AND  RD.PV_NAME <> 'UNKNOWN'                     ")
    _str_sql.AppendLine(" ORDER BY RD.PV_NAME                              ")
    If Me.chk_games.Checked Then
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_GAME_NAME            ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_1             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_2             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_3             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_4             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_5             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_6             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_7             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_8             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_9             ")
      _str_sql.AppendLine("       ,PROVIDERS_GAMES.PG_PAYOUT_10            ")
    End If

    _sql_cmd = New SqlCommand(_str_sql.ToString())
    _sql_cmd.Parameters.Add("@pPvName", SqlDbType.NVarChar).Value = Me.ef_providers_games_name.Value

    Return _sql_cmd

  End Function ' GUI_GetSqlCommand

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call EditNewProvider()
      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Me.Grid.Update()
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(341), m_providers_name.ToString())
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920), m_3gs_checked.ToString())
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2012), m_games_checked.ToString())

  End Sub ' GUI_ReportFilter


  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_providers_name = ""
    m_3gs_checked = ""
    m_games_checked = ""

    ' Operator name
    If Not String.IsNullOrEmpty(Me.ef_providers_games_name.Value) Then
      m_providers_name = Me.ef_providers_games_name.Value
    Else
      m_providers_name = ""
    End If

    If Me.chk_3gs.Checked Then
      m_3gs_checked = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_3gs_checked = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

    If Me.chk_games.Checked Then
      m_games_checked = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_games_checked = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _prov_id As Integer
    Dim _prov_name As String
    Dim _prov_jackpot As String
    Dim _prov_multiplier As String
    Dim _prov_redeemable_only As String
    Dim _prov_3gs As String
    Dim _prov_3gs_vendor_ip As String
    Dim _prov_3gs_vendor_id As String
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Provider Values, and launch the editor
    _prov_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _prov_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_NAME).Value
    _prov_jackpot = Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT).Value
    _prov_multiplier = Me.Grid.Cell(_idx_row, GRID_COLUMN_MULTIPLIER).Value
    _prov_redeemable_only = Me.Grid.Cell(_idx_row, GRID_COLUMN_REDEEMABLE_ONLY).Value
    _prov_3gs = Me.Grid.Cell(_idx_row, GRID_COLUMN_3GS).Value
    _prov_3gs_vendor_ip = Me.Grid.Cell(_idx_row, GRID_COLUMN_IP).Value
    _prov_3gs_vendor_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_3GS_ID).Value

    _frm_edit = New frm_providers_edit

    Call _frm_edit.ShowEditItem(_prov_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _str_payouts As StringBuilder
    _str_payouts = New StringBuilder()


    If m_provider_ant <> DbRow.Value(SQL_COLUMN_NAME) Then
      If m_provider_count Mod 2 Then
        m_color_row_last = m_color_row
      Else
        m_color_row_last = m_color_row2
      End If
      m_provider_count = m_provider_count + 1
      m_provider_ant = DbRow.Value(SQL_COLUMN_NAME)
    End If

    Me.Grid.Row(RowIndex).BackColor = m_color_row_last

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    'id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)
    'Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    'Game Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = DbRow.Value(SQL_COLUMN_GAME_NAME).ToString()

    'Game Payouts
    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_1) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_1), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_2) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_2), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_3) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_3), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_4) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_4), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_5) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_5), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_6) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_6), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_7) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_7), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_8) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_8), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_9) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_9), 2).ToString() & "%, ")
    End If

    If Not DbRow.Value(SQL_COLUMN_GAME_PAYOUT_10) Is DBNull.Value Then
      _str_payouts.Append(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_GAME_PAYOUT_10), 2).ToString() & "%")
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_GAME_PAYOUTS).Value = _str_payouts.ToString().TrimEnd().TrimEnd(",")

    'Multiplier
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MULTIPLIER).Value = IIf(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_MULTIPLIER), 2) = 0, "", GUI_FormatNumber(DbRow.Value(SQL_COLUMN_MULTIPLIER), 2))

    'Jackpot
    If DbRow.Value(SQL_COLUMN_JACKPOT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT).Value = Me.Grid.Column(GRID_COLUMN_JACKPOT).Header.Text
    End If

    'Redeemable only
    If DbRow.Value(SQL_COLUMN_REDEEMABLE_ONLY) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE_ONLY).Value = Me.Grid.Column(GRID_COLUMN_REDEEMABLE_ONLY).Header.Text
    End If

    '3GS
    If DbRow.Value(SQL_COLUMN_3GS) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_3GS).Value = Me.Grid.Column(GRID_COLUMN_3GS).Header.Text
    End If

    'IP
    If Not DbRow.Value(SQL_COLUMN_IP) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_IP).Value = DbRow.Value(SQL_COLUMN_IP)
    End If

    'ID
    If Not DbRow.Value(SQL_COLUMN_3GS_ID) Is DBNull.Value Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_3GS_ID).Value = DbRow.Value(SQL_COLUMN_3GS_ID)
    End If

    Return True

  End Function ' GUI_SetupRow

#End Region ' Overrides

#Region " Private Functions "

  Private Sub EditNewProvider()

    Dim _idx_row As Short
    'XVV Variable not use Dim row_type As Integer
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    _frm_edit = New frm_providers_edit
    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' EditNewUser

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX).Editable = False

      ' ID
      .Column(GRID_COLUMN_ID).Width = 0

      ' NAME
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1922) ' "Provider"
      .Column(GRID_COLUMN_NAME).Width = 4580
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).IsMerged = True

      ' GAME_NAME
      .Column(GRID_COLUMN_GAME_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008) ' "Game Name"
      .Column(GRID_COLUMN_GAME_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GAME PAYOUTS
      .Column(GRID_COLUMN_GAME_PAYOUTS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2048) ' Payouts
      .Column(GRID_COLUMN_GAME_PAYOUTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      If Me.chk_games.Checked Then
        .Column(GRID_COLUMN_GAME_NAME).Width = GRID_GAME_NAME_WIDTH
        .Column(GRID_COLUMN_GAME_PAYOUTS).Width = GRID_PAYOUTS_WIDTH
      Else
        .Column(GRID_COLUMN_GAME_NAME).Width = 0
        .Column(GRID_COLUMN_GAME_PAYOUTS).Width = 0
      End If

      ' JACKPOT
      .Column(GRID_COLUMN_JACKPOT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1918) ' "Jackpot"
      .Column(GRID_COLUMN_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_JACKPOT).Width = 0

      ' MULTIPLIER
      .Column(GRID_COLUMN_MULTIPLIER).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1921) ' "Multiplier"
      .Column(GRID_COLUMN_MULTIPLIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' REDEEMABLE ONLY
      .Column(GRID_COLUMN_REDEEMABLE_ONLY).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1919) ' "Redeemable only"
      .Column(GRID_COLUMN_REDEEMABLE_ONLY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' 3GS
      .Column(GRID_COLUMN_3GS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1920) ' "3GS"
      .Column(GRID_COLUMN_3GS).Width = 650
      .Column(GRID_COLUMN_3GS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If Me.chk_games.Checked And Not Me.chk_3gs.Checked Then
        .Column(GRID_COLUMN_3GS).Width = 0
      Else
        .Column(GRID_COLUMN_3GS).Width = 650
      End If

      ' IP
      .Column(GRID_COLUMN_IP).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1925) ' "IP address"
      .Column(GRID_COLUMN_IP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' 3GS ID
      .Column(GRID_COLUMN_3GS_ID).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2011) ' "Provider ID"
      .Column(GRID_COLUMN_3GS_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If Me.chk_games.Checked Or Me.chk_3gs.Checked Then
        .Column(GRID_COLUMN_JACKPOT).Width = 0
        .Column(GRID_COLUMN_MULTIPLIER).Width = 0
        .Column(GRID_COLUMN_REDEEMABLE_ONLY).Width = 0
      Else
        .Column(GRID_COLUMN_JACKPOT).Width = 1700
        .Column(GRID_COLUMN_MULTIPLIER).Width = 2300
        .Column(GRID_COLUMN_REDEEMABLE_ONLY).Width = 1620
      End If
      If Me.chk_3gs.Checked Then
        .Column(GRID_COLUMN_IP).Width = 1600
        .Column(GRID_COLUMN_3GS_ID).Width = 1600
      Else
        .Column(GRID_COLUMN_IP).Width = 0
        .Column(GRID_COLUMN_3GS_ID).Width = 0
      End If


    End With

  End Sub ' GUI_StyleSheet

  Private Sub SetDefaultValues()

    ' Reset values 
    Me.ef_providers_games_name.Value = ""
    Me.chk_3gs.Checked = False
    Me.chk_games.Checked = False
    Call ef_providers_games_name.Focus()

  End Sub ' SetDefaultValues

#End Region  'Private Functions

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

End Class