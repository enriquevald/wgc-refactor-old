﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_blacklist_import
'
' DESCRIPTION : Allows to import data from excel document to database.
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-FEB-2016   YNM   Initial version
' 01-MAR-2016   YNM   Fixed Bug 10151 Reception: can't access having permissions
' 01-MAR-2016   YNM   Fixed Bug 10152 Reception: Error while importing an incorrect file
' 06-MAY-2016   SMN   Product Backlog Item 12929:Visitas / Recepción: BlackList - Ampliar funcionalidad
' 20-MAY-2016   JBP   Product Backlog Item 13262:Recepción - versión Junio
' 06-JUN-2016   PDM   Recepción: GUI - Error al importar la lista de prohibidos. No aparece el detalle de los registros con error.
' 24-JUN-2016   PDM   PBI 14776: Visitas / Recepción: Blacklist - mejorar importación Excel
'--------------------------------------------------------------------

#Region " Imports "

Imports System.Data.SqlClient
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices
Imports WSI.Common
Imports WSI.Common.Entrances
Imports WSI.Common.Entrances.Enums
Imports System.Text

#End Region

Public Class frm_blacklist_import
  Inherits frm_base_edit

#Region " Constants "

#End Region

#Region " Delegates "

  Private Delegate Sub SetTextCallback(ByVal e As WSI.Common.ImportEventArgs)
  Private Delegate Sub SetTextBoxCallback(ByVal text As String)
  Private Delegate Sub SetProgressBarCallback(ByVal progress As Integer)
  Private Delegate Function GetComboBoxValueDelegate(Combo As uc_combo) As Integer

#End Region

#Region " Members "

  Private m_blacklist_import As BlacklistImport
  Private m_thread_import As Thread
  Private m_log_file As String
  Private m_no_timeout_timer As System.Timers.Timer

#End Region

#Region " Overrides "

  ''' <summary>
  ''' Set form id
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FROM_BLACKLIST_IMPORT

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ''' <summary>
  ''' Initialize controls
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7125)

    ' - Buttons

    '   - Stop
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_CLASS_II.GetString(427) ' stop
    '   - Start
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CLASS_II.GetString(426) ' start
    '   - Exit
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CLASS_II.GetString(3) ' start
    GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False


    ' Site input data
    ef_file_path.Text = GLB_NLS_GUI_CLASS_II.GetString(428)
    ef_file_path.Value = ""
    gb_output.Text = GLB_NLS_GUI_CLASS_II.GetString(425)

    tb_output.Enabled = True
    tb_output.ReadOnly = True

    cmb_blacklist_type.Text = GLB_NLS_GUI_CONTROLS.GetString(486) ' Type

    LoadComboBlacklistTypes()
    If cmb_blacklist_type.Count > 0 Then
      cmb_blacklist_type.SelectedIndex = 0
    End If

  End Sub  'GUI_InitControls

  ''' <summary>
  ''' Manage actions for bottons 
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        Call StartBlacklistImport()

      Case ENUM_BUTTON.BUTTON_CANCEL
        If m_thread_import Is Nothing Then
          Me.Close()
        ElseIf (m_thread_import.ThreadState = ThreadState.Stopped) Then
          Me.Close()
        End If

      Case ENUM_BUTTON.BUTTON_DELETE
        Call StopBlacklistImport()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#End Region

#Region " Public Functions "

  ''' <summary>
  ''' Overloads for showforedit method
  ''' </summary>
  ''' <param name="MdiParent"></param>
  ''' <remarks></remarks>
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call Me.Display(False)

  End Sub

#End Region

#Region " Private Functions "

  ''' <summary>
  ''' Get value of a ComboBox
  ''' </summary>
  ''' <param name="Combo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetComboBoxValueFunction(Combo As uc_combo) As Integer
    Return Combo.Value
  End Function

  ''' <summary>
  ''' Load combo with types of blacklist import
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadComboBlacklistTypes()
    Dim _cls_blacklist_type As cls_blacklist_import_type

    _cls_blacklist_type = New cls_blacklist_import_type()

    cmb_blacklist_type.Clear()
    For Each _dr As DataRow In _cls_blacklist_type.GetTypes().Rows
      cmb_blacklist_type.Add(_dr("BKLT_ID_TYPE"), _dr("BKLT_NAME"))
    Next

  End Sub ' LoadComboBlacklistTypes

  ''' <summary>
  ''' Start thread for BlacklistImportWork
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub StartBlacklistImport()

    Me.pgb_import_progress.Value = 0

    If (String.IsNullOrEmpty(ef_file_path.Value) Or Not File.Exists(ef_file_path.Value)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(160), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    If Not CheckFileExtension(Path.GetExtension(ef_file_path.Value)) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(160), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    If (cmb_blacklist_type.SelectedIndex = -1) Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(102), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_CONTROLS.GetString(486)) ' You must select a Type
      Return
    End If

    If m_thread_import Is Nothing Then
      m_thread_import = New Thread(AddressOf BlacklistImportThread)
    End If

    If (m_thread_import.ThreadState = ThreadState.Stopped Or m_thread_import.ThreadState = ThreadState.Unstarted) Then
      If m_thread_import.ThreadState = ThreadState.Stopped Then
        m_thread_import = New Thread(AddressOf BlacklistImportThread)
      End If
      m_no_timeout_timer = New System.Timers.Timer(New TimeSpan(0, 1, 0).TotalMilliseconds)
      AddHandler m_no_timeout_timer.Elapsed, AddressOf m_no_timeout_timer_tick
      Me.tb_output.Text = ""
      m_no_timeout_timer.Start()
      m_thread_import.Start()
      GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True

    End If

  End Sub 'StartBlacklistImport

  ''' <summary>
  ''' Stop thread for BlacklistImportWork
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub StopBlacklistImport()

    If Not m_thread_import Is Nothing Then
      If (m_thread_import.ThreadState <> ThreadState.Stopped) Then
        m_thread_import.Abort()
        pgb_import_progress.Value = 0
        UpdateOutput(GLB_NLS_GUI_CLASS_II.GetString(431))
        m_blacklist_import.WriteLog(vbCrLf + GLB_NLS_GUI_CLASS_II.GetString(431)) 'TODO:Validate this function on class
        GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        m_no_timeout_timer.Stop()
      End If
    End If

  End Sub 'StopBlacklistImport

  ''' <summary>
  ''' Check file extension
  ''' </summary>
  ''' <param name="Extension"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckFileExtension(ByVal Extension) As Boolean

    If Extension.ToLower() = ".xlsx" Or Extension = ".xls" Then
      Return True
    End If

    Return False
  End Function 'StopBlacklistImport

  ''' <summary>
  ''' Audit blacklist Import
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RegisterAudit()
    RegisterAudit(False)
  End Sub

  Private Sub RegisterAudit(ByRef IsEmptyList)

    Dim _auditor_data As CLASS_AUDITOR_DATA
    Dim _file_name As String

    _auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_ACCOUNT)

    'TODO: Validate Strings 
    _auditor_data.SetName(GLB_NLS_GUI_STATISTICS.Id(207), GLB_NLS_GUI_CLASS_II.GetString(556))

    _file_name = System.IO.Path.GetFileName(ef_file_path.Value)
    _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(550), _file_name)

    If IsEmptyList Then
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(557), GLB_NLS_GUI_CLASS_II.GetString(555))
    Else
      _auditor_data.SetField(GLB_NLS_GUI_CLASS_II.Id(557), GLB_NLS_GUI_CLASS_II.GetString(558))
    End If

    _auditor_data.Notify(GLB_CurrentUser.GuiId, _
                 GLB_CurrentUser.Id, _
                 GLB_CurrentUser.Name, _
                 CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                 0)
  End Sub 'RegisterAudit

  ''' <summary>
  ''' Threat to import  blacklist information
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BlacklistImportThread()
    Dim _type As Integer
    Dim _delegate_combo As GetComboBoxValueDelegate
    Dim _empty_list As Boolean
    Dim _parArray() As Object = {cmb_blacklist_type}

    _delegate_combo = AddressOf GetComboBoxValueFunction

    _type = cmb_blacklist_type.Invoke(_delegate_combo, _parArray)

    _delegate_combo = AddressOf GetComboBoxValueFunction

    _type = cmb_blacklist_type.Invoke(_delegate_combo, _parArray)

    Try
      m_log_file = ef_file_path.Value + WGDB.Now.ToString("yyyy.MM.dd") + ".txt"
      m_blacklist_import = New BlacklistImport(m_log_file)

      AddHandler m_blacklist_import.OnRowUpdated, AddressOf UpdateProcess
      m_blacklist_import.Import(ef_file_path.Value, _type)
      RemoveHandler m_blacklist_import.OnRowUpdated, AddressOf UpdateProcess

      Customers.RefreshBlackListCache(ENUM_BLACKLIST_SOURCE.EXTERNAL_FILE)

      _empty_list = CheckEmptyList(_type)

      Call RegisterAudit(_empty_list)

      If _empty_list Then
        UpdateProgressBar(100)
        UpdateOutput(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7383), True)
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub 'BlacklistImportThread

  ''' <summary>
  ''' Update progressbar progress
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub UpdateProgressBar(ByVal Progress As Integer)

    If Me.pgb_import_progress.InvokeRequired Then
      tb_output.Invoke(New SetProgressBarCallback(AddressOf UpdateProgressBar), New Object() {Progress})
    Else
      Me.pgb_import_progress.Value = Progress
      GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    End If

  End Sub

  ''' <summary>
  ''' Check empty list
  ''' </summary>
  ''' <param name="BlackListId"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckEmptyList(ByVal BlackListId As Integer) As Boolean
    Dim _result As Boolean
    Dim _sb As StringBuilder
    Dim _result_data As Object

    _result = False

    _sb = New StringBuilder()
    _sb.AppendLine(" SELECT  COUNT(BLKF_ID_TYPE)           ")
    _sb.AppendLine("   FROM  BLACKLIST_FILE_IMPORTED       ")
    _sb.AppendLine("  WHERE  BLKF_ID_TYPE = @pBlackListId ")

    Try

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pBlackListId", SqlDbType.Int).Value = BlackListId
          _result_data = _sql_cmd.ExecuteScalar()

          If Not IsNothing(_result_data) Then
            If CInt(_result_data) > 0 Then
              Return False
            End If
          End If

        End Using
      End Using

    Catch _ex As Exception

    End Try

    Return True

  End Function

  ''' <summary>
  ''' Handle m_blacklist_import.OnRowUpdated event
  ''' </summary>
  ''' <param name="Sender"></param>
  ''' <param name="UpdateEventArgs"></param>
  ''' <remarks></remarks>
  Private Sub UpdateProcess(ByVal Sender As Object, ByVal UpdateEventArgs As WSI.Common.ImportEventArgs)
    BlacklistImportStep(UpdateEventArgs)
  End Sub 'UpdateProcess

  ''' <summary>
  ''' Avoid automatic session close 
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub m_no_timeout_timer_tick(sender As Object, e As Timers.ElapsedEventArgs)
    WSI.Common.Users.SetLastAction("InProgress", Nothing)
  End Sub 'm_no_timeout_timer_tick

  ''' <summary>
  ''' Called from m_thread_import throw a delegate, set output text and progressbar progress
  ''' </summary>
  ''' <param name="ImportEventArgs"></param>
  ''' <remarks></remarks>
  Private Sub BlacklistImportStep(ImportEventArgs As ImportEventArgs)

    Dim _callback As SetTextCallback
    Dim _progress As Double

    If (Me.tb_output.InvokeRequired) Then
      _callback = New SetTextCallback(AddressOf BlacklistImportStep)
      Me.Invoke(_callback, New Object() {ImportEventArgs})
    Else

      _progress = 0

      Try
        _progress = ((CDbl(ImportEventArgs.RowCount) - CDbl(ImportEventArgs.PendingRows)) / CDbl(ImportEventArgs.RowCount) * 100)
        UpdateOutput(ImportEventArgs.ToString(), True)

        If (_progress = 100) Then

          If ImportEventArgs.ErrorsDetails.Count > 0 Then
            For Each _error As ErrorDetail In ImportEventArgs.ErrorsDetails
              UpdateOutput(String.Format(vbTab & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7353), _error.RowNumber.ToString(), _error.DescriptionError))
            Next
          End If

          If ImportEventArgs.WarningList.Count > 0 Then
            For Each _warning As Warning In ImportEventArgs.WarningList
              UpdateOutput(String.Format(vbTab & GLB_NLS_GUI_PLAYER_TRACKING.GetString(7379), _warning.RowNumber.ToString(), _warning.Message))
            Next
          End If

          If ImportEventArgs.ProcessedErrors = 0 Then
            'If ImportEventArgs.WarningList.Count = 0 Then
            '  UpdateOutput(NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(6494)))
            'Else
            UpdateOutput(NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(6494)))
            ''End If
            UpdateOutput(String.Format(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7381), ImportEventArgs.ProcessedOk))
          Else
            UpdateOutput(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7380))
            UpdateOutput(String.Format(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7381), "0"))
          End If

          GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        End If

        Me.pgb_import_progress.Value = CInt(_progress)
      Catch ex As Exception

      End Try

      'Me.m_state = ImportEventArgs.

      'If (m_state = AccountsImport.IMPORT_STATES.DocumentReaded) Then
      '  GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = True

      'End If
    End If

  End Sub

  Private Sub UpdateOutput(ByVal Message As String, Optional ByVal Reset As Boolean = False)

    If Me.tb_output.InvokeRequired Then
      tb_output.Invoke(New SetTextBoxCallback(AddressOf UpdateOutput), New Object() {Message})
    Else
      If (Reset) Then
        tb_output.Text = Message & vbCrLf
      Else
        tb_output.Text += Message & vbCrLf
      End If

      tb_output.SelectionStart = tb_output.TextLength
      tb_output.ScrollToCaret()
    End If

  End Sub
  'BlacklistImportStep

#End Region

#Region " Events "

  ''' <summary>
  ''' Opens a file dialog to chose the document to import
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub btn_open_file_dialog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_open_file_dialog.Click

    Dim _file_dialog As OpenFileDialog
    _file_dialog = New OpenFileDialog()
    _file_dialog.Multiselect = False
    _file_dialog.Filter = "Excel Documents(*.XLS;*.XLSX)|*.XLS;*.XLSX"
    _file_dialog.ShowDialog()
    ef_file_path.Value = _file_dialog.FileName.ToString()

  End Sub

  ''' <summary>
  ''' Opens blacklist type edit form
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub btn_blacklist_type_Click(sender As Object, e As EventArgs) Handles btn_blacklist_type.Click
    Dim _frm As frm_blacklist_import_type

    _frm = New frm_blacklist_import_type
    _frm.ShowEditItem(Me.MdiParent)

    LoadComboBlacklistTypes()

  End Sub ' btn_blacklist_type_Click

#End Region

End Class