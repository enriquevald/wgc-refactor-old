'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   TODO
' DESCRIPTION:   TODO
' AUTHOR:        Marcos Piedra
' CREATION DATE: 27-OCT-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-OCT-2011  MPO    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient

'/****************/
'/***** TODO *****/
'/****************/

Public Class frm_statistics_accounts
  Inherits frm_base_sel

#Region " Constants "

  ' Num grid columns / headers
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 8

  ' Grid Columns
  Private Const GRID_COLUMN_CARD_ACCOUNT As Integer = 0
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 1
  Private Const GRID_COLUMN_PROVIDER As Integer = 2
  Private Const GRID_COLUMN_PLAYER As Integer = 3
  Private Const GRID_COLUMN_WON As Integer = 4
  Private Const GRID_COLUMN_CASHIN As Integer = 5
  Private Const GRID_COLUMN_CASHOUT As Integer = 6
  Private Const GRID_COLUMN_NETWIN As Integer = 7

  ' SQL Columns
  Private Const SQL_COLUMN_CARD_ACCOUNT As Integer = 0
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 1
  Private Const SQL_COLUMN_PROVIDER As Integer = 2
  Private Const SQL_COLUMN_PLAYER As Integer = 3
  Private Const SQL_COLUMN_WON As Integer = 4
  Private Const SQL_COLUMN_CASHIN As Integer = 5
  Private Const SQL_COLUMN_CASHOUT As Integer = 6
  Private Const SQL_COLUMN_NETWIN As Integer = 7

  ' Width
  Private Const GRID_WIDTH_CARD_ACCOUNT As Integer = 800
  Private Const GRID_WIDTH_HOLDER_NAME As Integer = 2500
  Private Const GRID_WIDTH_PROVIDER As Integer = 2400
  Private Const GRID_WIDTH_PLAYER As Integer = 1100
  Private Const GRID_WIDTH_WON As Integer = 1100
  Private Const GRID_WIDTH_CASHIN As Integer = 1100
  Private Const GRID_WIDTH_CASHOUT As Integer = 1100
  Private Const GRID_WIDTH_NETWIN As Integer = 1100

  ' Type of card for where clause
  Private Const CARD_TYPE As Integer = 2

#End Region ' Constants

#Region " Members "

  ' Variables for show the filter in the report
  Dim m_account As String
  Dim m_track_data As String
  Dim m_holder As String
  Dim m_date_from As String
  Dim m_date_to As String
  Dim m_level As String
  Dim m_gender As String
  Dim m_only_anonymous As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Public Overrides Sub GUI_SetFormId()

    'TODO
    'Me.FormId = ENUM_FORM.FORM_ACCOUNT_STATISTICS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = "TODO: Statistics accounts"

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_STATISTICS.GetString(204)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Gender
    Me.gb_gender.Text = GLB_NLS_GUI_INVOICING.GetString(403)
    Me.chk_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)
    Me.chk_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)

    ' Level
    Me.gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    Me.chk_level_04.Text = GetCashierPlayerTrackingData("Level01.Name")
    If Me.chk_level_04.Text = "" Then
      Me.chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    Me.chk_level_03.Text = GetCashierPlayerTrackingData("Level02.Name")
    If Me.chk_level_03.Text = "" Then
      Me.chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    Me.chk_level_02.Text = GetCashierPlayerTrackingData("Level03.Name")
    If Me.chk_level_02.Text = "" Then
      Me.chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    Me.chk_level_01.Text = GetCashierPlayerTrackingData("Level04.Name")
    If Me.chk_level_01.Text = "" Then
      Me.chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    ' Only anonymous
    Me.chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean


    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sql As String = ""

    ' Get Select and from

    _sql &= " DECLARE @pFromStarted AS DATETIME "
    _sql &= " DECLARE @pToStarted AS DATETIME "
    _sql &= " SET @pFromStarted = '2011-07-05 09:00:00' "
    _sql &= " SET @pToStarted = '2011-09-06 09:00:00' "
    _sql &= " SELECT   AC_ACCOUNT_ID                                        Cuenta "
    _sql &= "        , AC_HOLDER_NAME                                       Titular "
    If chk_account_provider.Checked Then
      _sql &= "      , TE_PROVIDER_ID                                       Proveedor "
    End If
    _sql &= "        , PS_PLAYED_AMOUNT                                     Jugado "
    _sql &= "        , PS_WON_AMOUNT                                        Ganado "
    _sql &= "        , PS_INITIAL_BALANCE + PS_CASH_IN                      CashIn "
    _sql &= "        , PS_FINAL_BALANCE                                     CashOut "
    _sql &= "        , PS_INITIAL_BALANCE + PS_CASH_IN - PS_FINAL_BALANCE   Netwin "
    _sql &= "   FROM ( SELECT   PS_ACCOUNT_ID "
    If chk_account_provider.Checked Then
      _sql &= "               , TE_PROVIDER_ID "
    End If
    _sql &= "                 , SUM(PS_PLAYED_AMOUNT) PS_PLAYED_AMOUNT "
    _sql &= "                 , SUM(PS_WON_AMOUNT) PS_WON_AMOUNT "
    _sql &= "                 , SUM(PS_INITIAL_BALANCE) PS_INITIAL_BALANCE "
    _sql &= "                 , SUM(PS_CASH_IN) PS_CASH_IN "
    _sql &= "                 , SUM(PS_FINAL_BALANCE) PS_FINAL_BALANCE "
    _sql &= "            FROM   PLAY_SESSIONS     "
    If chk_account_provider.Checked Then
      _sql &= "                 , TERMINALS "
    End If
    _sql &= "           WHERE   "
    If chk_account_provider.Checked Then
      _sql &= "                 TE_TERMINAL_ID = PS_TERMINAL_ID AND "
    End If
    _sql &= "                   PS_STARTED >= @pFromStarted "
    _sql &= "             AND   PS_STARTED  < @pToStarted "
    _sql &= "             AND   PS_TERMINAL_ID IN "
    _sql &= "                   (SELECT  TE_TERMINAL_ID FROM TERMINALS "
    _sql &= "                     WHERE  TE_TERMINAL_TYPE IN ( 1, 3, 5)  "
    _sql &= "                   ) "
    _sql &= "          GROUP BY PS_ACCOUNT_ID"
    If chk_account_provider.Checked Then
      _sql &= "               , TE_PROVIDER_ID "
    End If
    _sql &= "        ) ACCOUNT_ACUMS "
    _sql &= "   INNER JOIN ACCOUNTS ON PS_ACCOUNT_ID = AC_ACCOUNT_ID "
    _sql &= " ORDER BY AC_ACCOUNT_ID, TE_PROVIDER_ID "

    _sql &= GetSqlWhere()

    Return _sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_ACCOUNT).Value = DbRow.Value(SQL_COLUMN_CARD_ACCOUNT)

    ' Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    ' Provider (if exists)
    If Not DbRow.IsNull(GRID_COLUMN_PROVIDER) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = DbRow.Value(SQL_COLUMN_PROVIDER)
    End If

    ' Total player
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PLAYER).Value = DbRow.Value(SQL_COLUMN_PLAYER)

    ' Total won
    Me.Grid.Cell(RowIndex, GRID_COLUMN_WON).Value = DbRow.Value(SQL_COLUMN_WON)

    ' Total cashin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIN).Value = DbRow.Value(SQL_COLUMN_CASHIN)

    ' Total cashout
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHOUT).Value = DbRow.Value(SQL_COLUMN_CASHOUT)

    ' Total netwin
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = DbRow.Value(SQL_COLUMN_NETWIN)

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.uc_account_sel

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -

  Protected Overrides Sub GUI_ExecuteQueryCustom()

    Dim _function_name As String
    Dim _db_trx As WSI.Common.DB_TRX
    Dim _sql_da As SqlDataAdapter
    Dim _ds_query As DataSet
    Dim _dt_query As DataTable

    _function_name = "GUI_ExecuteQueryCustom"

    Try

      Call Me.Grid.Clear()
      Me.Grid.Redraw = False

      _db_trx = New WSI.Common.DB_TRX()
      _sql_da = New SqlDataAdapter(New SqlCommand())

      _sql_da.SelectCommand.CommandText = GUI_FilterGetSqlQuery()

      '_sql_da.SelectCommand.Parameters.Add("@fecha", SqlDbType.DateTime).Value = WSI.Common.WGDB.Now

      _ds_query = New DataSet()
      _db_trx.Fill(_sql_da, _ds_query)
      _dt_query = _ds_query.Tables(0)

      Me.Grid.Redraw = True

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Text, _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True


    Finally

    End Try


  End Sub

#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    With Me.Grid
    End With

    Call MyBase.GUI_ReportParams(ExcelData)
  End Sub

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

    End With

    Call MyBase.GUI_ReportParams(PrintData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(231), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    If Me.chk_only_anonymous.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), m_only_anonymous)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), m_level)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(403), m_gender)
    End If
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterHeaderWidth(2) = 1800
    PrintData.FilterHeaderWidth(3) = 1800
    PrintData.FilterValueWidth(3) = 3200

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""

    m_level = ""
    m_gender = ""
    m_only_anonymous = ""

    ' Card Account number
    m_account = Me.uc_account_sel.Account()
    m_track_data = Me.uc_account_sel.TrackData()
    m_holder = Me.uc_account_sel.Holder()

    ' Date created
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Level
    m_level = GetLevelsSelected()

    ' Gender
    m_gender = GetGendersSelected()

    ' Only anonymous
    m_only_anonymous = IIf(Me.chk_only_anonymous.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Account - Number
      .Column(GRID_COLUMN_CARD_ACCOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_CARD_ACCOUNT).Width = GRID_WIDTH_CARD_ACCOUNT
      .Column(GRID_COLUMN_CARD_ACCOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account - Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Provider Name 
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Player
      .Column(GRID_COLUMN_PLAYER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(381)
      .Column(GRID_COLUMN_PLAYER).Width = GRID_WIDTH_PLAYER
      .Column(GRID_COLUMN_PLAYER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Won
      .Column(GRID_COLUMN_WON).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_WON).Width = GRID_WIDTH_WON
      .Column(GRID_COLUMN_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cashin
      .Column(GRID_COLUMN_CASHIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_CASHIN).Width = GRID_WIDTH_CASHIN
      .Column(GRID_COLUMN_CASHIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CashOut
      .Column(GRID_COLUMN_CASHOUT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_CASHOUT).Width = GRID_WIDTH_CASHOUT
      .Column(GRID_COLUMN_CASHOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' NetWin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(403)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_WIDTH_NETWIN
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()
    Dim initial_time As Date

    initial_time = WSI.Common.Misc.TodayOpening()

    Me.dtp_from.Value = initial_time
    Me.dtp_from.Checked = False

    Me.chk_level_01.Checked = False
    Me.chk_level_02.Checked = False
    Me.chk_level_03.Checked = False
    Me.chk_level_04.Checked = False

    Me.chk_gender_male.Checked = False
    Me.chk_gender_female.Checked = False

    Me.chk_only_anonymous.Checked = False

  End Sub

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_where_account As String = ""

    str_where = " WHERE AC_TYPE = " & CARD_TYPE

    str_where_account = Me.uc_account_sel.GetFilterSQL()

    If str_where_account <> "" Then
      str_where &= " AND " & str_where_account
    End If

    ' Level
    If Not Me.chk_only_anonymous.Checked Then
      If Me.chk_level_01.Checked Or _
         Me.chk_level_02.Checked Or _
         Me.chk_level_03.Checked Or _
         Me.chk_level_04.Checked Then

        str_where &= " AND AC_HOLDER_LEVEL IN (" & GetLevelsSelected() & ") "
      End If

      ' Gender
      If Me.chk_gender_male.Checked Or _
         Me.chk_gender_female.Checked Then

        str_where &= " AND AC_HOLDER_GENDER IN (" & GetGendersSelected() & ") "
      End If
    Else
      str_where &= " AND ( AC_USER_TYPE IS NULL OR AC_USER_TYPE = 0 ) "
    End If

    Return str_where

  End Function

  ' PURPOSE: Get Level list for selected levels
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetLevelsSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""
    If Me.chk_level_01.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_level_01.Text)
    End If
    If Me.chk_level_02.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_level_02.Text)
    End If
    If Me.chk_level_03.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "3", Me.chk_level_03.Text)
    End If
    If Me.chk_level_04.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "4", Me.chk_level_04.Text)
    End If

    Return list_type
  End Function ' GetLevelsSelected

  ' PURPOSE: Get Gender list for selected genders
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetGendersSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim list_type As String

    list_type = ""
    If chk_gender_male.Checked Then
      list_type = IIf(GetIds, "1", Me.chk_gender_male.Text)
    End If
    If chk_gender_female.Checked Then
      If list_type.Length > 0 Then
        list_type = list_type & ", "
      End If
      list_type = list_type & IIf(GetIds, "2", Me.chk_gender_female.Text)
    End If

    Return list_type
  End Function ' GetGendersSelected

  ' PURPOSE: Depends of check box appropriate it shows the provider column
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Sub ShowColumnsProvider()

    If chk_account.Checked Then
      Me.Grid.Column(GRID_COLUMN_PROVIDER).Width = 0
      Me.Grid.Column(GRID_COLUMN_PROVIDER).WidthFixed = 0
      Me.Grid.Column(GRID_COLUMN_PROVIDER).IsColumnPrintable = False
    ElseIf chk_account_provider.Checked Then
      Me.Grid.Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_PROVIDER
      Me.Grid.Column(GRID_COLUMN_PROVIDER).IsColumnPrintable = True
    End If

  End Sub

#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub chk_only_anonymous_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_only_anonymous.CheckedChanged
    gb_level.Enabled = Not chk_only_anonymous.Checked
    gb_gender.Enabled = Not chk_only_anonymous.Checked
    If chk_only_anonymous.Checked Then
      chk_level_01.Checked = False
      chk_level_02.Checked = False
      chk_level_03.Checked = False
      chk_level_04.Checked = False

      chk_gender_male.Checked = False
      chk_gender_female.Checked = False
    End If
  End Sub

#End Region ' Events

End Class
