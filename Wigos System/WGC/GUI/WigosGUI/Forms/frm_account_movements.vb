'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_movements
'
' DESCRIPTION:   This screen allows to view account movements: 
'                           - between two dates 
'                           - for one or all cashiers
'                           - for one or all users
'                           - for one or all movements types
'                           - for one or all cashier sessions
' AUTHOR:        Agust� Poch
' CREATION DATE: 06-SEP-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-SEP-2007  APB    Initial version
' 21-OCT-2011  JCA    Added new movement (Card Adjustment)
' 22-NOV-2011  JMM    Added new movements (PIN Change & PIN Generation)
' 17-FEB-2012  RCI    Added new movements (CashIn CoverCoupon & Not Redeemable 2 Expiration)
' 24-APR-2012  JCM    Added new movement (Recycle Card)
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 05-JUL-2012  DDM    Added new movement (MOV_TYPE_DB_REDEEMABLE). Modified some names of movements
' 31-JUL-2012  DDM    Added new movement (Promo. Per Points & Promo. Per Points Cancelled). 
' 27-AUG-2012  RRB    Added new information for account adjustment (Reasons and Details).
' 06-FEB-2013  RRB    Added new movement (PointsStatusChanged).
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 18-APR-2013  ICS    Changed the cashier name by its alias.
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty 
' 12-AUG-2013  FBA    Added new movement (DepositCancelation) and sets default value for movements.
' 27-AUG-2013  NMR    Added control for TITO's movement points
' 09-SEP-2013  DRV    Added Totalizator to Account movements
' 12-SEP-2013  DRV    Added Movement type Filter
' 16-SEP-2013  DRV    Updated Movement type Filter - items grouped by type
' 18-SEP-2013  NMR    Added new movement TITO type
' 18-SEP-2013  JML    No view balance for Cash draw participation
' 19-SEP-2013  NMR    Upgrading NLS
' 25-SEP-2013  NMR    Adding new TITO movement types
' 02-OCT-2013  DRV    Fix Bug WIG-247
' 03-OCT-2013  DRV    Fix Bug WIG-226
' 04-OCT-2013  DRV    Added Header check status "Checked Disabled" when some items are selected
' 16-OCT-2013  ICS    Added Undo Operation suport
' 22-OCT-2013  LEM    Show TrackData movement info instead TrackData account info
' 23-OCT-2013  RCI & RMS    Missing WHERE clausule
' 31-OCT-2013  LEM    Fixed Bug WIG-363: Show (AC_TRACK_DATA*) for movements without AM_TRACK_DATA
' 16-DEC-2013  RMS    In TITO Mode virtual accounts should be hidden
' 18-DEC-2013  LJM    New chips functionality added
' 02-JAN-2014  DLL    Added new movement CASH_IN_TAX and grouped movement description
' 20-JAN-2014  DRV    Fixed Bug: WIGOSTITO-996: Falta a�adir la NLS del movimiento 112
' 29-JAN-2014  LJM    Remove hidden movements
' 30-JAN-2014  JPJ    Added promotion movements and Participation in the Draw
' 31-JAN-2014  JPJ    Added new movements Handpay and jackpot
' 05-FEB-2014  DHA    Added new movements Handpay and jackpot redeem
' 25-MAR-2014  JFC    Fixed bug WIG-371: must show manually assigned points in frm_plays_sessions.
' 25-APR-2014  DHA & JML Rename TITO Cashier/Account Movements
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 26-JUN-2014  LEM    Fixed Bug WIG-1053: Shows asterisc (*) when trackdata movement is not null
' 07-JUL-2014  AMF    Personal Card Replacement
' 18-SEP-2014  DLL    Added new movement Cash Advance
' 02-JUL-2015  JCA & FJC Fixed Bug 2525
' 17-JUL-2015  YNM    Fixed Bug WIG-2596: Empty User/Machine field for LCD TOUCH 
' 23-OCT-2015  AMF    Backlog Item 4703: GameGateway
' 07-DIC-2015  FJC    Fixed BUG: 7360 (Added Cashier Movement when we reserve credit)
' 12-JAN-2016  AMF    PBI 8260: Changes NLS
' 20-JAN-2016  AMF    PBI 8267: Rollback
' 03-FEB-2016 ETP    FIXED BUG 8941 NLS And bucket movement are incorrect
' 02-MAR-2016  DHA    Product Backlog Item 10085: Winpot - added Tax Provisions
' 21-MAR-2016 JRC     PBI: 1792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
' 02-MAR-2016 DHA     Product Backlog Item 10085: Winpot - added Tax Provisions
' 23-MAR-2016 JML     Product Backlog Item 9755:Gaming Tables (Fase 1): Cage
' 19-APR-2016  RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
' 27-APR-2016  RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
' 08-JUN-2016  DLL    Bug 14302:A�adir/Fijar Puntos: no funciona para cuentas con ID > 2^31
' 27-JUL-2016  PDM    Product Backlog Item 15448:Visitas / Recepci�n: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
' 28-JUL-2016  FAV    Fixed Bug 15837: Terminals with a number in the name are exported like '00:00'
' 19-SEP-2016  XGJ    Prepare accounts for PariPlay
' 29-SEP-2016  RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
' 04-OCT-2016  JBP    PBI 18464:PariPlay - Ajustar pantallas de BonoPlay
' 05-OCT-2016  RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
' 18-OCT-2016  ATB    Bug 18774: Falta NLS en el gui para asignaci�n de tarjeta
' 20-OCT-2016  ATB    Bug 19365: Televia filters not working
' 20-OCT-2016  FAV    Bug 19400: Movements cashier Televisa
' 13-DIC-2016  XGJ    Bug 5589:Movimientos de cuenta: Error en los filtros usuario/m�quina
' 12-JAN-2017  FAV    Bug 22549: It doesn�t show the filter for Reception
' 01-FEB-2017  DPC    Bug 13206:Exportar: no se incluyen las NLS de los movimientos de Custodia
' 13-MAR-2017  FGB    PBI 25735: Third TAX - Movement reports
' 17-MAR-2017  AMF    WIGOS-109: CreditLine - Get a Marker
' 19-JUN-2017  RAB    PBI 28000: WIGOS-2732 - Rounding - Movements
' 06-JUL-2017  RAB    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
' 21-FEB-2018  RGR    Bug 31635: WIGOS-3785 Incorrect warning message when Several is selected in selection criterion
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_account_movements
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    ' RRB 27-AUG-2012 Set FormId
    Me.FormId = ENUM_FORM.FORM_ACCOUNT_MOVEMENTS
    Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId

    If FormId = ENUM_FORM.FORM_ACCOUNT_MOVEMENTS Then
      Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL
    Else
      Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS
    End If

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_terminal As GUI_Controls.uc_combo
  Friend WithEvents opt_one_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_terminals As System.Windows.Forms.RadioButton
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_level_change As System.Windows.Forms.RadioButton
  Friend WithEvents opt_points_accreditations As System.Windows.Forms.RadioButton
  Friend WithEvents opt_points_status As System.Windows.Forms.RadioButton
  Friend WithEvents gb_move_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Friend WithEvents lbl_trackdata_note As GUI_Controls.uc_text_field
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_account_movements))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_terminal = New System.Windows.Forms.GroupBox()
    Me.opt_one_terminal = New System.Windows.Forms.RadioButton()
    Me.cmb_terminal = New GUI_Controls.uc_combo()
    Me.opt_one_cashier = New System.Windows.Forms.RadioButton()
    Me.opt_all_terminals = New System.Windows.Forms.RadioButton()
    Me.cmb_cashier = New GUI_Controls.uc_combo()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.opt_points_status = New System.Windows.Forms.RadioButton()
    Me.opt_level_change = New System.Windows.Forms.RadioButton()
    Me.opt_points_accreditations = New System.Windows.Forms.RadioButton()
    Me.opt_several_types = New System.Windows.Forms.RadioButton()
    Me.gb_move_type = New System.Windows.Forms.GroupBox()
    Me.opt_all_types = New System.Windows.Forms.RadioButton()
    Me.dg_filter = New GUI_Controls.uc_grid()
    Me.lbl_trackdata_note = New GUI_Controls.uc_text_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_terminal.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.gb_move_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_trackdata_note)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_terminal)
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.gb_move_type)
    resources.ApplyResources(Me.panel_filter, "panel_filter")
    Me.panel_filter.Controls.SetChildIndex(Me.gb_move_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_trackdata_note, 0)
    '
    'panel_data
    '
    resources.ApplyResources(Me.panel_data, "panel_data")
    '
    'pn_separator_line
    '
    resources.ApplyResources(Me.pn_separator_line, "pn_separator_line")
    '
    'pn_line
    '
    resources.ApplyResources(Me.pn_line, "pn_line")
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    resources.ApplyResources(Me.gb_date, "gb_date")
    Me.gb_date.Name = "gb_date"
    Me.gb_date.TabStop = False
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    resources.ApplyResources(Me.dtp_to, "dtp_to")
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    resources.ApplyResources(Me.dtp_from, "dtp_from")
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.opt_one_terminal)
    Me.gb_terminal.Controls.Add(Me.cmb_terminal)
    Me.gb_terminal.Controls.Add(Me.opt_one_cashier)
    Me.gb_terminal.Controls.Add(Me.opt_all_terminals)
    Me.gb_terminal.Controls.Add(Me.cmb_cashier)
    resources.ApplyResources(Me.gb_terminal, "gb_terminal")
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.TabStop = False
    '
    'opt_one_terminal
    '
    resources.ApplyResources(Me.opt_one_terminal, "opt_one_terminal")
    Me.opt_one_terminal.Name = "opt_one_terminal"
    '
    'cmb_terminal
    '
    Me.cmb_terminal.AllowUnlistedValues = False
    Me.cmb_terminal.AutoCompleteMode = False
    Me.cmb_terminal.IsReadOnly = False
    resources.ApplyResources(Me.cmb_terminal, "cmb_terminal")
    Me.cmb_terminal.Name = "cmb_terminal"
    Me.cmb_terminal.SelectedIndex = -1
    Me.cmb_terminal.SufixText = "Sufix Text"
    Me.cmb_terminal.SufixTextVisible = True
    Me.cmb_terminal.TextCombo = Nothing
    Me.cmb_terminal.TextVisible = False
    Me.cmb_terminal.TextWidth = 0
    '
    'opt_one_cashier
    '
    resources.ApplyResources(Me.opt_one_cashier, "opt_one_cashier")
    Me.opt_one_cashier.Name = "opt_one_cashier"
    '
    'opt_all_terminals
    '
    resources.ApplyResources(Me.opt_all_terminals, "opt_all_terminals")
    Me.opt_all_terminals.Name = "opt_all_terminals"
    '
    'cmb_cashier
    '
    Me.cmb_cashier.AllowUnlistedValues = False
    Me.cmb_cashier.AutoCompleteMode = False
    Me.cmb_cashier.IsReadOnly = False
    resources.ApplyResources(Me.cmb_cashier, "cmb_cashier")
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TextCombo = Nothing
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    resources.ApplyResources(Me.uc_account_sel1, "uc_account_sel1")
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.opt_points_status)
    Me.gb_type.Controls.Add(Me.opt_level_change)
    Me.gb_type.Controls.Add(Me.opt_points_accreditations)
    resources.ApplyResources(Me.gb_type, "gb_type")
    Me.gb_type.Name = "gb_type"
    Me.gb_type.TabStop = False
    '
    'opt_points_status
    '
    resources.ApplyResources(Me.opt_points_status, "opt_points_status")
    Me.opt_points_status.Name = "opt_points_status"
    Me.opt_points_status.UseVisualStyleBackColor = True
    '
    'opt_level_change
    '
    resources.ApplyResources(Me.opt_level_change, "opt_level_change")
    Me.opt_level_change.Name = "opt_level_change"
    Me.opt_level_change.UseVisualStyleBackColor = True
    '
    'opt_points_accreditations
    '
    resources.ApplyResources(Me.opt_points_accreditations, "opt_points_accreditations")
    Me.opt_points_accreditations.Checked = True
    Me.opt_points_accreditations.Name = "opt_points_accreditations"
    Me.opt_points_accreditations.TabStop = True
    Me.opt_points_accreditations.UseVisualStyleBackColor = True
    '
    'opt_several_types
    '
    resources.ApplyResources(Me.opt_several_types, "opt_several_types")
    Me.opt_several_types.Name = "opt_several_types"
    '
    'gb_move_type
    '
    Me.gb_move_type.Controls.Add(Me.opt_all_types)
    Me.gb_move_type.Controls.Add(Me.opt_several_types)
    Me.gb_move_type.Controls.Add(Me.dg_filter)
    resources.ApplyResources(Me.gb_move_type, "gb_move_type")
    Me.gb_move_type.Name = "gb_move_type"
    Me.gb_move_type.TabStop = False
    '
    'opt_all_types
    '
    resources.ApplyResources(Me.opt_all_types, "opt_all_types")
    Me.opt_all_types.Name = "opt_all_types"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    resources.ApplyResources(Me.dg_filter, "dg_filter")
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    Me.dg_filter.WordWrap = False
    '
    'lbl_trackdata_note
    '
    Me.lbl_trackdata_note.IsReadOnly = True
    Me.lbl_trackdata_note.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_trackdata_note.LabelForeColor = System.Drawing.Color.Blue
    resources.ApplyResources(Me.lbl_trackdata_note, "lbl_trackdata_note")
    Me.lbl_trackdata_note.Name = "lbl_trackdata_note"
    Me.lbl_trackdata_note.SufixText = "Sufix Text"
    Me.lbl_trackdata_note.SufixTextVisible = True
    Me.lbl_trackdata_note.TextVisible = False
    Me.lbl_trackdata_note.TextWidth = 0
    Me.lbl_trackdata_note.Value = "* TrackData Note"
    '
    'frm_account_movements
    '
    resources.ApplyResources(Me, "$this")
    Me.Name = "frm_account_movements"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_terminal.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.gb_type.PerformLayout()
    Me.gb_move_type.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_ACCT_NUMBER As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 1
  Private Const SQL_COLUMN_CARD_TRACK As Integer = 2
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 3
  Private Const SQL_COLUMN_DATE As Integer = 4
  Private Const SQL_COLUMN_MACHINE_ID As Integer = 5
  Private Const SQL_COLUMN_MACHINE_NAME As Integer = 6
  Private Const SQL_COLUMN_TYPE_ID As Integer = 7
  Private Const SQL_COLUMN_INIT_BALANCE As Integer = 8
  Private Const SQL_COLUMN_SUB_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_ADD_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_FINAL_BALANCE As Integer = 11
  Private Const SQL_COLUMN_UNDO_STATUS As Integer = 12
  Private Const SQL_COLUMN_AM_TRACKDATA As Integer = 13
  Private Const SQL_COLUMN_DETAILS As Integer = 14
  Private Const SQL_COLUMN_REASONS As Integer = 15


  ' Grid Columns
  Private Const GRID_COLUMN_ACCT_NUMBER As Integer = 0
  Private Const GRID_COLUMN_ACCOUNT_TYPE As Integer = 1
  Private Const GRID_COLUMN_CARD_TRACK As Integer = 2
  Private Const GRID_COLUMN_HOLDER_NAME As Integer = 3
  Private Const GRID_COLUMN_DATE As Integer = 4
  Private Const GRID_COLUMN_TYPE_ID As Integer = 5
  Private Const GRID_COLUMN_TYPE_NAME As Integer = 6
  Private GRID_COLUMN_TERMINAL_ID As Integer = 7
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 8
  Private Const GRID_COLUMN_INIT_BALANCE As Integer = 9
  Private Const GRID_COLUMN_SUB_AMOUNT As Integer = 10
  Private GRID_COLUMN_ADD_AMOUNT As Integer = 11
  Private Const GRID_COLUMN_FINAL_BALANCE As Integer = 12
  Private Const GRID_COLUMN_REASONS As Integer = 13
  Private Const GRID_COLUMN_DETAILS As Integer = 14

  Private Const GRID_COLUMNS_NORMAL As Integer = 13
  Private Const GRID_COLUMNS_DETAILS As Integer = 15
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_TIME As Integer = 1300
  Private Const GRID_WIDTH_TERMINAL As Integer = 1700
  Private Const GRID_WIDTH_AMOUNT_SHORT As Integer = 1100
  Private Const GRID_WIDTH_AMOUNT_LONG As Integer = 1400
  Private Const GRID_WIDTH_MOVEMENT_TYPE_NAME As Integer = 2030
  Private Const GRID_WIDTH_CARD_HOLDER_NAME As Integer = 2150
  Private Const GRID_WIDTH_CARD_TRACK_DATA As Integer = 2300
  Private Const GRID_WIDTH_REASONS As Integer = 4000
  Private Const GRID_WIDTH_DETAILS As Integer = 10000

  ' Database codes
  Private Const MOV_TYPE_DB_CARD_REDEEMABLE_SPENT_USED As Integer = 45

  ' Mov. types data grid
  Private Const GRID_2_COLUMN_CODE As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_ROW_TYPE As Integer = 3
  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1

  Private Const GRID_2_COLUMNS As Integer = 4

  ' Left padding for data rows in the mov. types data grid
  Private Const GRID_2_TAB As String = "     "

  Private Const FORM_DB_MIN_VERSION As Short = 156


#End Region ' Constants

#Region " Enums "

  ' RRB 27-AUG-2012 
  Public Enum ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE

    TYPE_NORMAL = 0
    TYPE_DETAILS = 1

  End Enum

  Public Structure TYPE_DG_FILTER_ELEMENT
    Dim code As Integer
    Dim description As String
    Dim elem_type As Integer
  End Structure

#End Region ' Enums

#Region " Members "

  ' RRB 27-AUG-2012 Type of account movements shown
  Private m_account_movements_type_mode As ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_terminal As String
  Private m_type As String
  Private m_movement_type As String

  ' For calls from other screens
  Private m_initial_account As Int64

  Private m_prize_taxes_1_name As String
  Private m_prize_taxes_2_name As String
  Private m_prize_taxes_3_name As String

  Private m_level_name() As String
  Private m_split_a_name_str As String

  ' For grid totalizators
  Dim m_total_points As Double
  Dim m_totals_data_table As DataTable

  Public m_select_terminals As String = "SELECT TLC_MASTER_ID, TLC_BASE_NAME FROM TERMINALS_LAST_CHANGED INNER JOIN TERMINALS ON TE_TERMINAL_ID = TLC_TERMINAL_ID " & _
                                        " WHERE TE_TYPE = '1' AND TE_TERMINAL_TYPE IN (" & WSI.Common.Misc.GamingTerminalTypeListToString() & ", 102, 109) ORDER BY TLC_BASE_NAME"

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    ' Leave this sub empty, as the FormId is set now in the Constructor of the class.
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Dim _sql As String

    ' XGJ 13-DIC-2016
    _sql = "SELECT CT_CASHIER_ID, CT_NAME FROM CASHIER_TERMINALS WHERE CT_TERMINAL_ID IS NULL AND CT_GAMING_TABLE_ID IS NULL  ORDER BY CT_NAME"
    Call MyBase.GUI_InitControls()

    ' TITO Mode
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      uc_account_sel1.InitExtendedQuery(False)
    End If

    ' RRB 27-AUG-2012 Form name
    Select Case Me.m_account_movements_type_mode
      Case ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL
        Me.Text = GLB_NLS_GUI_INVOICING.GetString(228) ' Movimientos de cuentas

      Case ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1257) ' Informe de Asignaciones Manuales

      Case Else
        Me.Text = GLB_NLS_GUI_INVOICING.GetString(228) ' Movimientos de cuentas

    End Select

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.lbl_trackdata_note.Value = "* " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2862)
    Me.lbl_trackdata_note.Visible = False

    ' Terminals
    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      Me.gb_terminal.Visible = False
    End If
    Me.gb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1270)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(234)
    Me.opt_one_terminal.Text = GLB_NLS_GUI_INVOICING.GetString(233)
    Me.opt_all_terminals.Text = GLB_NLS_GUI_INVOICING.GetString(224)

    ' Type Account Movement (ManualHolderLevelChanged & CardAdjustment)
    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
      Me.panel_filter.Size = New Size(1377, 210)
      Me.gb_date.Location = New Point(9, 138)
      Me.gb_terminal.Location = New Point(319, 6)
      Me.gb_type.Location = New Point(613, 6)
      Me.gb_move_type.Location = New Point(613, 6)
      Me.gb_type.Visible = False
      Me.gb_move_type.Visible = True
      Me.lbl_trackdata_note.Location = New Point(lbl_trackdata_note.Location.X, gb_terminal.Location.Y + gb_terminal.Height + 10)
    Else
      Me.panel_filter.Size = New Size(1377, 140)
      Me.gb_move_type.Visible = False
      Me.gb_type.Location = New System.Drawing.Point(Me.gb_terminal.Location.X, Me.gb_terminal.Location.Y)
    End If
    Me.gb_type.Text = GLB_NLS_GUI_INVOICING.GetString(211)
    Me.opt_points_accreditations.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1276)
    Me.opt_level_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)
    Me.opt_points_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1645)

    ' Grid
    Call GUI_StyleSheet()

    ' Mov. Type Grid Filter
    Me.gb_move_type.Text = GLB_NLS_GUI_INVOICING.GetString(211)
    Me.opt_several_types.Text = GLB_NLS_GUI_INVOICING.GetString(223)
    Me.opt_all_types.Text = GLB_NLS_GUI_INVOICING.GetString(224)
    Call GUI_StyleSheetMovementTypes()

    m_prize_taxes_1_name = GetCashierData("Tax.OnPrize.1.Name")
    m_prize_taxes_2_name = GetCashierData("Tax.OnPrize.2.Name")
    m_prize_taxes_3_name = GetCashierData("Tax.OnPrize.3.Name")

    ' Set combo with Cashier Terminals Alias
    ' XGJ 13-DIC-2016
    Call SetComboCashierAlias(Me.cmb_cashier, False, False, _sql)
    Me.cmb_cashier.Enabled = False

    ' Set combo with Gaming Terminals
    Call SetCombo(Me.cmb_terminal, m_select_terminals)
    Me.cmb_terminal.Enabled = False

    ' Init level name
    ReDim m_level_name(5)
    m_level_name(0) = "---"
    m_level_name(1) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level01.Name")
    m_level_name(2) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level02.Name")
    m_level_name(3) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level03.Name")
    m_level_name(4) = WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "Level04.Name")

    m_split_a_name_str = WSI.Common.Misc.ReadGeneralParams("Cashier", "Split.A.Name")

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If
    'Check Types
    Call Me.CheckMovTypeFilter()

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Check consistency of the data grid filter values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: filter values are consistent
  '     - False: filter values are not consistent
  Private Sub CheckMovTypeFilter()

    If Me.opt_several_types.Checked Then
      If GetTypeIdListSelected() = "" Then
        Me.opt_several_types.Checked = False
        Me.opt_all_types.Checked = True

      End If
    End If

  End Sub ' CheckMovTypeFilter

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String
    Dim _with_index As String

    _with_index = ""

    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS OrElse Me.opt_several_types.Checked Then
      _with_index = " WITH (INDEX(IX_type_date_account)) "
    Else
      If Me.uc_account_sel1.GetFilterSQL() <> "" Then
        _with_index = " WITH (INDEX(IX_movements_account_date)) "
      Else
        _with_index = " WITH (INDEX(IX_am_datetime)) "
      End If
    End If

    ' Get Select and from
    str_sql = "SELECT " & _
                  " AM_ACCOUNT_ID, " & _
                  " AC_TYPE, " & _
                  " AC_TRACK_DATA, " & _
                  " AC_HOLDER_NAME, " & _
                  " AM_DATETIME, " & _
                  " CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_ID " & _
                       " WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_ID " & _
                       " ELSE NULL " & _
                  " END, " & _
                  " CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_NAME" & _
                       " WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_NAME" & _
                       " WHEN AM_TERMINAL_ID IS NULL AND AM_CASHIER_ID IS NULL THEN ISNULL(AM_CASHIER_NAME, AM_TERMINAL_NAME)" & _
                       " ELSE NULL " & _
                  " END, " & _
                  " AM_TYPE, " & _
                  " AM_INITIAL_BALANCE, " & _
                  " AM_SUB_AMOUNT, " & _
                  " AM_ADD_AMOUNT, " & _
                  " AM_FINAL_BALANCE, " & _
                  " AM_UNDO_STATUS, " & _
                  " AM_TRACK_DATA, "

    ' RRB 27-AUG-2012
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      str_sql = str_sql & " AM_DETAILS, " & _
                          " AM_REASONS "
    Else
      str_sql = str_sql & " AM_DETAILS "
    End If

    str_sql = str_sql & " FROM ACCOUNT_MOVEMENTS " & _with_index & _
                        " INNER JOIN ACCOUNTS ON AC_ACCOUNT_ID = AM_ACCOUNT_ID "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC "

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Me.m_total_points = 0

    Call GUI_StyleSheet()

    Call InitTotalsDataTable()

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row As DataRow
    Dim _all_rows() As DataRow
    Dim _first_time As Boolean
    Dim _idx_row As Integer
    Dim _blank_row As Boolean
    Dim _blank_balance As Boolean
    Dim _mov_type_name As String
    Dim _currency_format As Integer
    Dim _unrecognized_movement As Boolean

    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS _
       And Me.opt_points_accreditations.Checked Then

      _idx_row = Me.Grid.NumRows

      Me.Grid.AddRow()

      ' Label - TOTAL:
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCT_NUMBER).Value = GLB_NLS_GUI_INVOICING.GetString(205)

      ' Total - Points
      Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatNumber(Me.m_total_points)

      ' Color Row
      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    Else
      _all_rows = m_totals_data_table.Select("", "MOV_TYPE_NAME")
      _first_time = True

      For Each _row In _all_rows

        _mov_type_name = TranslateMovementType(_row.Item("MOV_TYPE"), _currency_format, _blank_row, _blank_balance, _unrecognized_movement)
        Me.Grid.AddRow()
        _idx_row = Me.Grid.NumRows - 1

        'Add 'TOTAL:' label
        If _first_time = True Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCT_NUMBER).Value = GLB_NLS_GUI_INVOICING.GetString(205)
          _first_time = False
        End If

        Me.Grid.Cell(_idx_row, GRID_COLUMN_TYPE_NAME).Value = _row.Item("MOV_TYPE_NAME")
        Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = _row.Item("COUNT")
        Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

        'If it's an unrecognized movement, it shows intital and final balance
        If _unrecognized_movement = True Then
          Me.Grid.Cell(_idx_row, GRID_COLUMN_INIT_BALANCE).Value = GUI_FormatNumber(_row.Item("TOTAL_INITIAL_BALANCE"))
          Me.Grid.Cell(_idx_row, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatNumber(_row.Item("TOTAL_FINAL_BALANCE"))
        End If
        'If blank_row it's true, add and sub ammounts are not shown
        If _blank_row = False Then
          If _row.Item("CURRENCY_FORMAT") = True Then
            Me.Grid.Cell(_idx_row, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatCurrency(_row.Item("TOTAL_SUB_AMOUNT"))
            Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatCurrency(_row.Item("TOTAL_ADD_AMOUNT"))
          Else
            Me.Grid.Cell(_idx_row, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatNumber(_row.Item("TOTAL_SUB_AMOUNT"))
            Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatNumber(_row.Item("TOTAL_ADD_AMOUNT"))
          End If
        Else
          Me.Grid.Cell(_idx_row, GRID_COLUMN_SUB_AMOUNT).Value = ""
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ADD_AMOUNT).Value = ""
        End If
      Next
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _mov_type As Integer
    Dim _str_type As String = ""
    Dim _track_data As String
    Dim _currency_format As Boolean
    Dim _blank_row As Boolean
    Dim _blank_balance As Boolean
    Dim _terminal As String
    Dim _split As String()
    Dim _undo_status As UndoStatus
    Dim _account_type As AccountType
    Dim _sub As Decimal

    _sub = 0
    ' Account type
    _account_type = DbRow.Value(SQL_COLUMN_ACCOUNT_TYPE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_TYPE).Value = _account_type

    ' Card Track Data
    _track_data = ""
    If Not DbRow.IsNull(SQL_COLUMN_AM_TRACKDATA) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_AM_TRACKDATA)) Then
      _track_data = DbRow.Value(SQL_COLUMN_AM_TRACKDATA)
    Else
      If Not DbRow.IsNull(SQL_COLUMN_CARD_TRACK) Then
        If CardNumber.VisibleTrackData(_track_data, DbRow.Value(SQL_COLUMN_CARD_TRACK), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _account_type) _
           AndAlso _account_type = AccountType.ACCOUNT_WIN Then
          _track_data &= "*"
          Me.lbl_trackdata_note.Visible = True
        End If
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CARD_TRACK).Value = _track_data

    ' Card Account Number
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCT_NUMBER).Value = DbRow.Value(SQL_COLUMN_ACCT_NUMBER)

    ' Card Holder Name
    If Not DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = ""
    End If

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), _
                                                                                ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Movement type
    _mov_type = DbRow.Value(SQL_COLUMN_TYPE_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_ID).Value = DbRow.Value(SQL_COLUMN_TYPE_ID)

    _currency_format = True
    _blank_row = False
    _blank_balance = False

    _str_type = TranslateMovementType(_mov_type, _currency_format, _blank_row, _blank_balance)

    ' Movement type names that adds additional data to title
    Select Case _mov_type

      Case WSI.Common.MovementType.TITO_TicketReissue
        If Not DbRow.IsNull(SQL_COLUMN_DETAILS) Then
          _str_type &= " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
        End If

      Case WSI.Common.MovementType.PromotionNotRedeemable
        If Not DbRow.IsNull(SQL_COLUMN_DETAILS) Then
          _str_type &= " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
        End If


      Case WSI.Common.MovementType.HolderLevelChanged, _
           WSI.Common.MovementType.ManualHolderLevelChanged
        Dim _new_level As String
        Dim _old_level As String

        _old_level = m_level_name(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))
        _new_level = m_level_name(DbRow.Value(SQL_COLUMN_ADD_AMOUNT))

        _str_type = IIf(_mov_type = WSI.Common.MovementType.HolderLevelChanged, _
                       GLB_NLS_GUI_INVOICING.GetString(85, _new_level, _old_level), _
                       GLB_NLS_GUI_PLAYER_TRACKING.GetString(1123, _new_level, _old_level))
        _blank_row = True
        _blank_balance = True

        ' JCA 21-OCT-2011 
      Case WSI.Common.MovementType.ManuallyAddedPointsOnlyForRedeem
        Dim _old_points As String
        Dim _new_points As String
        Dim _add_points As String

        If (m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS) Then
          _old_points = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE))
          _add_points = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT))
          _new_points = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_FINAL_BALANCE))

          If DbRow.Value(SQL_COLUMN_SUB_AMOUNT) = 0 Then
            _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1125, _add_points, _new_points, _old_points)
          Else
            _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1124, _new_points, _old_points)
          End If
        End If
        _currency_format = False

        ' DDM 05-JUL-2012
      Case WSI.Common.MovementType.PromotionRedeemable
        If Not WSI.Common.Misc.IsVoucherModeTV Then
          If Not DbRow.IsNull(SQL_COLUMN_DETAILS) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_DETAILS)) Then
            _str_type &= " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
          End If
        End If
      Case WSI.Common.MovementType.PromotionPoint
        If Not DbRow.IsNull(SQL_COLUMN_DETAILS) AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_DETAILS)) Then
          _str_type &= " (" + DbRow.Value(SQL_COLUMN_DETAILS) + ")"
        End If

      Case WSI.Common.MovementType.PointsStatusChanged
        Dim _new_points_status As WSI.Common.ACCOUNT_POINTS_STATUS = DbRow.Value(SQL_COLUMN_ADD_AMOUNT)
        Dim _old_points_status As WSI.Common.ACCOUNT_POINTS_STATUS = DbRow.Value(SQL_COLUMN_SUB_AMOUNT)
        Dim _new_pt_status As String = ""
        Dim _old_pt_status As String = ""

        Select Case _new_points_status
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED
            _new_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643)
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED
            _new_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644)
        End Select

        Select Case _old_points_status
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED
            _old_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1643)
          Case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED
            _old_pt_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1644)
        End Select

        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1646, _new_pt_status, _old_pt_status)

      Case MovementType.MULTIPLE_BUCKETS_EndSession To MovementType.MULTIPLE_BUCKETS_Manual_Set

        _currency_format = GetBucketFormatedAmount(_mov_type) = Buckets.BucketUnits.Money

      Case WSI.Common.MovementType.CashdeskDrawParticipation

        If Not DbRow.IsNull(SQL_COLUMN_SUB_AMOUNT) Then
          _sub = DbRow.Value(SQL_COLUMN_SUB_AMOUNT)
        End If


        If _sub = 0 Then
          _blank_balance = True
          _blank_row = True

        Else
          _blank_balance = False
          _blank_row = False

        End If

      Case MovementType.CashInTax, MovementType.TaxCustody, MovementType.TaxReturnCustody
        _blank_balance = False

      Case MovementType.TaxProvisions
        _blank_balance = True

    End Select

    ' Check if movement operation is undone
    _undo_status = UndoStatus.None

    If Not DbRow.IsNull(SQL_COLUMN_UNDO_STATUS) Then
      _undo_status = DbRow.Value(SQL_COLUMN_UNDO_STATUS)
    End If

    If _undo_status = UndoStatus.UndoOperation Then
      _str_type = "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2786) & ") " & _str_type
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_NAME).Value = _str_type

    ' Allowed combinations:
    '
    ' Machine Id      NOT NULL            NULL          NULL
    ' Machine Name    NOT NULL            NOT NULL      NULL
    ' ------------    ----------------    ---------     ----------
    ' What is it?     Terminal/Cashier    GUI           No Machine

    ' Machine Id
    If Not DbRow.IsNull(SQL_COLUMN_MACHINE_NAME) Then
      If Not DbRow.IsNull(SQL_COLUMN_MACHINE_ID) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_MACHINE_ID)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = ""
      End If
    Else
    End If

    ' Terminal Name
    If Not DbRow.IsNull(SQL_COLUMN_MACHINE_NAME) Then
      _terminal = DbRow.Value(SQL_COLUMN_MACHINE_NAME)
      _split = _terminal.Split("@")

      ' Check if it's a Cashier terminal (user@terminal)
      If _split.Length = 2 Then
        _terminal = _split(0) & "@" & Computer.Alias(_split(1))
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = _terminal
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = ""
    End If

    ' Initial balance
    If Not _blank_balance Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_INIT_BALANCE))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_INIT_BALANCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_INIT_BALANCE))
      End If
    End If

    ' Subtract amount
    If Not DbRow.IsNull(SQL_COLUMN_SUB_AMOUNT) And Not _blank_row Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = ""
    End If

    ' Add amount
    If Not DbRow.IsNull(SQL_COLUMN_ADD_AMOUNT) And Not _blank_row Then
      'FBA 12-AUG-2013 sets ADD_AMOUNT column to blank if movement type is Deposit Cancelation

      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_ADD_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = IIf(Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, _
                                                                   GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT)), _
                                                                   GUI_FormatNumber(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT)) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_AMOUNT))))
        Me.m_total_points += GUI_FormatNumber(GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ADD_AMOUNT)) - GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SUB_AMOUNT)))
      End If

      'DHA 02-MAR-2016
      If _mov_type = MovementType.CashInTax Or _mov_type = MovementType.TaxProvisions Or _mov_type = MovementType.TaxCustody Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = GUI_FormatNumber(0)
      End If

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ADD_AMOUNT).Value = ""
    End If

    ' Final balance
    If Not _blank_balance Then
      If _currency_format Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_FINAL_BALANCE))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_FINAL_BALANCE).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_FINAL_BALANCE))
      End If
    End If

    ' RRB 27-AUG-2012
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      ' Reasons
      If Not DbRow.IsNull(SQL_COLUMN_REASONS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REASONS).Value = DbRow.Value(SQL_COLUMN_REASONS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REASONS).Value = ""
      End If
      ' Details
      If Not DbRow.IsNull(SQL_COLUMN_DETAILS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = DbRow.Value(SQL_COLUMN_DETAILS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DETAILS).Value = ""
      End If
    End If

    If _mov_type = MovementType.TaxReturnCustody Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUB_AMOUNT).Value = GUI_FormatNumber(0)
    End If

    Call SetTotalsDataTable(DbRow, _currency_format)

    Return True

  End Function ' GUI_SetupRow

  Public Function GetBucketFormatedAmount(AccountMovement As MovementType) As Buckets.BucketUnits
    Dim _bucket_form As BucketsForm
    Dim _Type As Buckets.BucketType

    _bucket_form = New BucketsForm()

    _Type = Buckets.BucketId.RankingLevelPoints

    _bucket_form.GetBucketType(AccountMovement, _Type)

    Return Buckets.GetBucketUnits(_Type)

  End Function

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_account_sel1
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
      If Me.opt_one_terminal.Checked Then
        PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(233), m_terminal)
      End If
      If Me.opt_one_cashier.Checked Then
        PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(234), m_terminal)
      End If
      If Me.opt_all_terminals.Checked Then
        PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(251), m_terminal)
      End If
      If m_movement_type <> "" Then
        PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_movement_type)
      End If
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_type)
    End If

    If lbl_trackdata_note.Visible Then
      PrintData.SetFilter("*", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2862))
    Else
      PrintData.SetFilter("", "", True)
    End If

    ' Allow enough space for terminal type labels in the report
    PrintData.FilterHeaderWidth(3) = 2000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      PrintData.Settings.Columns(GRID_COLUMN_DATE).IsWidthFixed = True
      PrintData.Settings.Columns(GRID_COLUMN_CARD_TRACK).IsWidthFixed = True

      With Me.Grid
        .Column(GRID_COLUMN_TYPE_NAME).PrintWidth = 3000
        .Column(GRID_COLUMN_REASONS).PrintWidth = 2000
        .Column(GRID_COLUMN_DETAILS).PrintWidth = 2000
      End With

    Else
      PrintData.Settings.Columns(GRID_COLUMN_DATE).IsWidthFixed = True
      PrintData.Settings.Columns(GRID_COLUMN_CARD_TRACK).IsWidthFixed = True
    End If

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(228), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1257))

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(ExcelData)
    ExcelData.SetColumnFormat(5, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) 'terminal
  End Sub

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_account = ""
    m_track_data = ""
    m_holder = ""
    m_holder_is_vip = ""
    m_date_from = ""
    m_date_to = ""
    m_terminal = ""
    m_movement_type = ""

    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

    Me.lbl_trackdata_note.Visible = False

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Terminals
    If Me.opt_all_terminals.Checked Then
      m_terminal = Me.opt_all_terminals.Text
    End If
    If Me.opt_one_cashier.Checked Then
      m_terminal = Me.cmb_cashier.TextValue
    End If
    If Me.opt_one_terminal.Checked Then
      m_terminal = Me.cmb_terminal.TextValue
    End If

    ' Type Account Movement
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      If Me.opt_points_accreditations.Checked Then
        m_type = Me.opt_points_accreditations.Text
      ElseIf Me.opt_level_change.Checked Then
        m_type = Me.opt_level_change.Text
      Else
        m_type = Me.opt_points_status.Text
      End If
    End If

    'Movement Type Grid
    If Me.opt_all_types.Checked Then
      m_movement_type = Me.opt_all_types.Text
    Else
      Call GetSelectedMovementTypes(m_movement_type)
    End If
  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    m_initial_account = 0
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal account_id As Int64)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_initial_account = account_id
    uc_account_sel1.Account = m_initial_account

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    ' RRB 28-AUG-2012 Not search if don't have permissions - coming from card summary (Avoid popup: Not found)
    If Me.Permissions.Read Then
      Call Application.DoEvents()
      System.Threading.Thread.Sleep(100)
      Call Application.DoEvents()

      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

#Region " Movement Types"

  ' PURPOSE: Gets the movement name and options of each kind of movement type
  '
  '  PARAMS:
  '     - INPUT:
  '           - mov_type: the Code of the movement
  '     - OUTPUT:
  '           - currency_format: True if the balance has to be formatted as currency
  '           - blank_row: True if the row doesn't have to show the add and sub cells
  '           - blank_balance: True if the row doesn't have to show the initial and final balance cells
  '           - unrecognized_movement: true if the movement doesn't have a personalized title
  '
  ' RETURNS:
  '     - A string with the name of the movement type
  Private Function TranslateMovementType(ByVal mov_type As Integer, Optional ByRef currency_format As Boolean = False, Optional ByRef blank_row As Boolean = False, Optional ByRef blank_balance As Boolean = False, Optional ByRef unrecognized_movement As Boolean = False) As String
    Dim _str_type As String
    Dim _buckets_forms As BucketsForm
    Dim _name As String

    _str_type = ""
    currency_format = True
    blank_row = False
    blank_balance = False
    unrecognized_movement = False

    _buckets_forms = New BucketsForm()
    _name = ""

    Select Case mov_type

      Case WSI.Common.MovementType.Play
        _str_type = GLB_NLS_GUI_INVOICING.GetString(41)

      Case WSI.Common.MovementType.CashIn
        _str_type = m_split_a_name_str

      Case WSI.Common.MovementType.CashOut
        If WSI.Common.Misc.IsVoucherModeTV Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7681)
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(43)
        End If

      Case WSI.Common.MovementType.Devolution
        _str_type = GLB_NLS_GUI_INVOICING.GetString(44)

      Case WSI.Common.MovementType.TaxOnPrize1
        _str_type = m_prize_taxes_1_name

      Case WSI.Common.MovementType.TaxOnPrize2
        _str_type = m_prize_taxes_2_name

      Case WSI.Common.MovementType.TaxOnPrize3
        _str_type = m_prize_taxes_3_name

      Case WSI.Common.MovementType.PromotionRedeemableFederalTax
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), m_prize_taxes_1_name)

      Case WSI.Common.MovementType.PromotionRedeemableStateTax
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), m_prize_taxes_2_name)

      Case WSI.Common.MovementType.PromotionRedeemableCouncilTax
        _str_type = String.Format("{0} - {1}", GLB_NLS_GUI_INVOICING.GetString(358), m_prize_taxes_3_name)

      Case WSI.Common.MovementType.StartCardSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(46)

      Case WSI.Common.MovementType.CancelStartSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(96)

      Case WSI.Common.MovementType.EndCardSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(47)

      Case WSI.Common.MovementType.Activate
        _str_type = GLB_NLS_GUI_INVOICING.GetString(48)

      Case WSI.Common.MovementType.Deactivate
        _str_type = GLB_NLS_GUI_INVOICING.GetString(49)

      Case WSI.Common.MovementType.PromotionNotRedeemable
        _str_type = GLB_NLS_GUI_INVOICING.GetString(50)

      Case WSI.Common.MovementType.CashInCoverCoupon
        _str_type = GLB_NLS_GUI_INVOICING.GetString(38)

      Case WSI.Common.MovementType.CancelNotRedeemable
        _str_type = GLB_NLS_GUI_INVOICING.GetString(53)

      Case WSI.Common.MovementType.DepositIn
        _str_type = GLB_NLS_GUI_INVOICING.GetString(51)

      Case WSI.Common.MovementType.DepositOut
        _str_type = GLB_NLS_GUI_INVOICING.GetString(52)

      Case WSI.Common.MovementType.CardReplacement
        _str_type = GLB_NLS_GUI_INVOICING.GetString(70)

      Case WSI.Common.MovementType.PromoCredits
        _str_type = GLB_NLS_GUI_INVOICING.GetString(64)

      Case WSI.Common.MovementType.PromoToRedeemable
        _str_type = GLB_NLS_GUI_INVOICING.GetString(65)

      Case WSI.Common.MovementType.PromoExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(66)

      Case WSI.Common.MovementType.PromoCancel
        _str_type = GLB_NLS_GUI_INVOICING.GetString(67)

      Case WSI.Common.MovementType.PromoStartSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(68)

      Case WSI.Common.MovementType.PromoEndSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(69)

      Case WSI.Common.MovementType.CreditsExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(477)

      Case WSI.Common.MovementType.PrizeExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(89)

      Case WSI.Common.MovementType.ReservedExpired
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8674)

      Case WSI.Common.MovementType.CreditsNotRedeemableExpired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(478)

      Case WSI.Common.MovementType.CreditsNotRedeemable2Expired
        _str_type = GLB_NLS_GUI_INVOICING.GetString(95)

      Case WSI.Common.MovementType.DrawTicketPrint
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(71)

      Case WSI.Common.MovementType.PointsToDrawTicketPrint
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(83)

      Case WSI.Common.MovementType.Handpay
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(364)

      Case WSI.Common.MovementType.HandpayCancellation
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(365)

      Case WSI.Common.MovementType.ManualHandpay
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(366)

      Case WSI.Common.MovementType.ManualHandpayCancellation
        _str_type = GLB_NLS_GUI_CONFIGURATION.GetString(367)

      Case WSI.Common.MovementType.ManualEndSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(74)

      Case WSI.Common.MovementType.PromoManualEndSession
        _str_type = GLB_NLS_GUI_INVOICING.GetString(75)

      Case WSI.Common.MovementType.PointsAwarded
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(76)

      Case WSI.Common.MovementType.PointsToGiftRequest
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(77)

      Case WSI.Common.MovementType.PointsToNotRedeemable
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(78)

      Case WSI.Common.MovementType.PointsToRedeemable
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(88)

      Case WSI.Common.MovementType.PointsGiftDelivery
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(79)

      Case WSI.Common.MovementType.PointsGiftServices
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(84)

      Case WSI.Common.MovementType.PointsExpired
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(81)

      Case WSI.Common.MovementType.HolderLevelChanged
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(545)

      Case WSI.Common.MovementType.ManualHolderLevelChanged
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1866)

      Case WSI.Common.MovementType.PrizeCoupon
        _str_type = GLB_NLS_GUI_INVOICING.GetString(86)

      Case WSI.Common.MovementType.CardCreated
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(90)

      Case WSI.Common.MovementType.AccountPersonalization
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(91)

      Case WSI.Common.MovementType.DEPRECATED_RedeemableSpentUsedInPromotions
        _str_type = GLB_NLS_GUI_INVOICING.GetString(87)

        ' JCA 21-OCT-2011 
      Case WSI.Common.MovementType.ManuallyAddedPointsOnlyForRedeem
        currency_format = False
        _str_type = GLB_NLS_GUI_INVOICING.GetString(92)
        ' JMM 22-NOV-2011
      Case WSI.Common.MovementType.AccountPINChange
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(93)

      Case WSI.Common.MovementType.AccountPINRandom
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_INVOICING.GetString(94)

        ' JCM 24-APR-2012 Card Recycle Movement
      Case WSI.Common.MovementType.CardRecycled
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(654)

        ' DDM 05-JUL-2012
      Case WSI.Common.MovementType.PromotionRedeemable
        If WSI.Common.Misc.IsVoucherModeTV Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7682)
        Else
          _str_type = GLB_NLS_GUI_INVOICING.GetString(100)
        End If

      Case WSI.Common.MovementType.CancelPromotionRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1120)

      Case WSI.Common.MovementType.PromotionPoint
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1121)

      Case WSI.Common.MovementType.CancelPromotionPoint
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1122)

      Case WSI.Common.MovementType.TaxReturningOnPrizeCoupon
        _str_type = GLB_NLS_GUI_INVOICING.GetString(483)

        ' RRB 13-SEP-2012
      Case WSI.Common.MovementType.DecimalRounding
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1126)

      Case WSI.Common.MovementType.ServiceCharge
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1127)

      Case WSI.Common.MovementType.CancelGiftInstance
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1129)

      Case WSI.Common.MovementType.PointsStatusChanged
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1645)

      Case WSI.Common.MovementType.ManuallyAddedPointsForLevel  '= 68, Points added manually  
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1860)

      Case WSI.Common.MovementType.ImportedAccount              '= 70, Imported(account) 
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1150)

      Case WSI.Common.MovementType.ImportedPointsOnlyForRedeem  '= 71, Imported points (not accounted for the level) 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1861)

      Case WSI.Common.MovementType.ImportedPointsForLevel       '= 72, Imported(points) 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1862)

      Case WSI.Common.MovementType.ImportedPointsHistory        '= 73, History points imported 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1863)

      Case WSI.Common.MovementType.AccountBlocked               '= 75, Account blocked
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1133)

      Case WSI.Common.MovementType.AccountUnblocked             '= 76, Account unblocked
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1134)

      Case WSI.Common.MovementType.AccountInBlacklist           '= 240, Account added in blacklist
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7452)

      Case WSI.Common.MovementType.AccountNotInBlacklist        '= 241, Account removed from blacklist
        currency_format = False
        blank_row = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7453)

        'FBA 12-AUG-2013 new movement ID 77. Deposit Cancelation
      Case WSI.Common.MovementType.Cancellation                 '= 77, Deposit Cancelation
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2503)

        ' DHA 25-APR-2014 modified/added TITO tickets movements
      Case WSI.Common.MovementType.TITO_TicketCashierPrintedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2545)

      Case WSI.Common.MovementType.TITO_TicketCashierPrintedPromoRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2655)

      Case WSI.Common.MovementType.TITO_TicketCashierPrintedPromoNotRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4571)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4880)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedPromoNotRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4881)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2634)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidPromoRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4882)

      Case WSI.Common.MovementType.TITO_TicketMachinePlayedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2841)

      Case WSI.Common.MovementType.TITO_TicketMachinePlayedPromoRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4883)

      Case WSI.Common.MovementType.TITO_ticketMachinePlayedPromoNotRedeemable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4884)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidJackpot
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4599)

      Case WSI.Common.MovementType.TITO_TicketCashierPaidHandpay
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4598)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedHandpay
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4583)

      Case WSI.Common.MovementType.TITO_TicketMachinePrintedJackpot
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4584)

      Case WSI.Common.MovementType.TITO_TicketReissue
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2997)

      Case WSI.Common.MovementType.TITO_AccountToTerminalCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3398)

      Case WSI.Common.MovementType.TITO_TerminalToAccountCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3399)

      Case WSI.Common.MovementType.MultiSiteCurrentLocalPoints  '= 101, Multisite - Current points in site, similar ManuallyAddedPointsOnlyForRedeem 
        currency_format = False
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1864)

      Case WSI.Common.MovementType.CashdeskDrawParticipation
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2570)
        blank_row = True

      Case WSI.Common.MovementType.CashdeskDrawPlaySession
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2596)

        ' LJM 18-DEC-2013 For Chip movements
      Case WSI.Common.MovementType.ChipsPurchaseTotal
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1870)

      Case WSI.Common.MovementType.ChipsSaleTotal
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1869)

      Case WSI.Common.MovementType.ChipsSaleRemainingAmount
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8417)

      Case WSI.Common.MovementType.ChipsSaleConsumedRemainingAmount
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8475)

      Case WSI.Common.MovementType.ChipsSaleDevolutionForTito
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1871)
        ' LJM

        ' DLL 31-DEC-2013 Tax deposit
      Case WSI.Common.MovementType.CashInTax
        _str_type = GeneralParam.GetString("Cashier", "CashInTax.Name", GLB_NLS_GUI_PLAYER_TRACKING.GetString(4562))

      Case WSI.Common.MovementType.TaxCustody
        _str_type = WSI.Common.Misc.TaxCustodyName()

      Case WSI.Common.MovementType.TaxReturnCustody
        _str_type = WSI.Common.Misc.TaxCustodyRefundName()

        ' DHA 02-MAR-2016 Tax provisions
      Case WSI.Common.MovementType.TaxProvisions
        _str_type = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", GLB_NLS_GUI_PLAYER_TRACKING.GetString(7151))

      Case WSI.Common.MovementType.TITO_TicketOffline
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4470)

      Case WSI.Common.MovementType.CashdeskDrawParticipation
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4572)

        ' RMS 21-JAN-2014
      Case WSI.Common.MovementType.TransferCreditIn
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4479)

      Case WSI.Common.MovementType.TransferCreditOut
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4480)

        'HBB 29-MAY-2014
        'Case WSI.Common.MovementType.ChipsSaleWithCashIn
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5007)
        'Case WSI.Common.MovementType.ChipsPurchaseWithCashOut
        '  _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5008)

      Case WSI.Common.MovementType.CardReplacementForFree
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5079)

      Case WSI.Common.MovementType.CardReplacementInPoints
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5080)

      Case WSI.Common.MovementType.CashAdvance
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5461)

        ' OPC 04-NOV-2014
      Case WSI.Common.MovementType.ExternalSystemPointsSubstract
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5694)
        currency_format = False

      Case WSI.Common.MovementType.WinLossStatementRequest
        currency_format = False
        blank_row = True
        blank_balance = True
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6253)

        ' DHA 09-JUL-2015
      Case WSI.Common.MovementType.CardDepositInCheck
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6513)

      Case WSI.Common.MovementType.CardDepositInCurrencyExchange
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6514)

      Case WSI.Common.MovementType.CardDepositInCardCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6515)

      Case WSI.Common.MovementType.CardDepositInCardDebit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6516)

      Case WSI.Common.MovementType.CardDepositInCardGeneric
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6517)

      Case WSI.Common.MovementType.CardReplacementCheck
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6508)

      Case WSI.Common.MovementType.CardReplacementCardCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6510)

      Case WSI.Common.MovementType.CardReplacementCardDebit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6511)

      Case WSI.Common.MovementType.CardReplacementCardGeneric
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6512)

        'BonoPlay
      Case WSI.Common.MovementType.GameGatewayBet
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))

      Case WSI.Common.MovementType.GameGatewayPrize
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))

      Case WSI.Common.MovementType.GameGatewayBetRollback
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, GeneralParam.GetString("GameGateway", "Name", "BonoPlay"))

      Case WSI.Common.MovementType.GameGatewayReserveCredit
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6966)

      Case WSI.Common.MovementType.CurrencyExchangeCashierIn
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7323)

      Case WSI.Common.MovementType.CurrencyExchangeCashierOut
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7324)

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Expired To WSI.Common.MovementType.MULTIPLE_BUCKETS_ExpiredLast ' [1100-1199]
        If (_buckets_forms.GetBucketName(CType(mov_type, MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7074, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_EndSession To WSI.Common.MovementType.MULTIPLE_BUCKETS_EndSessionLast ' [1200-1299]
        If (_buckets_forms.GetBucketName(CType(mov_type, MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7075, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Add To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Add_Last ' [1300-1399]
        If (_buckets_forms.GetBucketName(CType(mov_type, MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7065, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Sub To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Sub_Last ' [1400-1499]
        If (_buckets_forms.GetBucketName(CType(mov_type, MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7066, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If

      Case WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Set To WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Set_Last ' [1300-1399]
        If (_buckets_forms.GetBucketName(CType(mov_type, MovementType), _name)) Then
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7076, _name)
        Else
          _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString) ' Default value if error.
          unrecognized_movement = True
        End If

        ' XGJ 07-set-2016
      Case WSI.Common.MovementType.Admission
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7583)

        'ATB 18-oct-2016
      Case WSI.Common.MovementType.CardAssociate
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7403)

        'FBA 12-AUG-2013 default movement ID

        ' XGJ 19-SEPT-2016 
        ' Pariplay
      Case WSI.Common.MovementType.PariPlayBet
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7052, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))

      Case WSI.Common.MovementType.PariPlayPrize
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7053, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))

      Case WSI.Common.MovementType.PariPlayBetRollback
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7064, GeneralParam.GetString("PariPlay", "Name", "PariPlay"))

      Case WSI.Common.MovementType.TITO_TicketCountRPrintedCashable
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7289)

      Case MovementType.CreditLineCreate
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7964)

      Case MovementType.CreditLineUpdate
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7965)

      Case MovementType.CreditLineApproved
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7966)

      Case MovementType.CreditLineNotApproved
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7967)

      Case MovementType.CreditLineSuspended
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7968)

      Case MovementType.CreditLineExpired
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7969)

      Case MovementType.CreditLineGetMarker
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7970)

      Case MovementType.CreditLinePayback
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7971)

      Case MovementType.CreditLineGetMarkerCardReplacement
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7989)

      Case Else  'Movement not denifed -> Movement (XX)
        _str_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2504, mov_type.ToString)
        unrecognized_movement = True

    End Select
    Return _str_type
  End Function

  ' PURPOSE: Set the counter and the sub and add amount cells of each total row
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row: the row to process
  '           - currency_format: true if the values of add an sub amount are currency
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetTotalsDataTable(ByRef Row As GUI_Controls.frm_base_sel.CLASS_DB_ROW, ByVal currency_format As Boolean)
    Dim _total_row As DataRow
    Dim _mov_type As Integer
    Dim _mov_type_name As String
    Dim _new_level As String

    _mov_type = Row.Value(SQL_COLUMN_TYPE_ID)
    Select Case (_mov_type)

      Case WSI.Common.MovementType.HolderLevelChanged, _
           WSI.Common.MovementType.ManualHolderLevelChanged

        _new_level = m_level_name(Row.Value(SQL_COLUMN_ADD_AMOUNT))
        _mov_type_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1865, _new_level)
        _total_row = GetRowFromTotalsByType(_mov_type, currency_format, _mov_type_name)
        _total_row.Item("MOV_TYPE_NAME") = _mov_type_name

      Case Else
        _total_row = GetRowFromTotalsByType(_mov_type, currency_format)

    End Select
    _total_row.Item("COUNT") = _total_row.Item("COUNT") + 1
    _total_row.Item("TOTAL_SUB_AMOUNT") = _total_row.Item("TOTAL_SUB_AMOUNT") + Row.Value(SQL_COLUMN_SUB_AMOUNT)
    _total_row.Item("TOTAL_ADD_AMOUNT") = _total_row.Item("TOTAL_ADD_AMOUNT") + Row.Value(SQL_COLUMN_ADD_AMOUNT)
    _total_row.Item("TOTAL_INITIAL_BALANCE") = _total_row.Item("TOTAL_INITIAL_BALANCE") + Row.Value(SQL_COLUMN_INIT_BALANCE)
    _total_row.Item("TOTAL_FINAL_BALANCE") = _total_row.Item("TOTAL_FINAL_BALANCE") + Row.Value(SQL_COLUMN_FINAL_BALANCE)
    _total_row.Item("CURRENCY_FORMAT") = currency_format

  End Sub

  ' PURPOSE : Create the data table for the totals by movement type.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub InitTotalsDataTable()

    Dim _column As DataColumn

    If m_totals_data_table IsNot Nothing Then
      m_totals_data_table.Dispose()
      m_totals_data_table = Nothing
    End If

    m_totals_data_table = New DataTable("TOTALS_BY_MOV_TYPE")

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "MOV_TYPE"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.String")
    _column.ColumnName = "MOV_TYPE_NAME"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ColumnName = "COUNT"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "TOTAL_INITIAL_BALANCE"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "TOTAL_SUB_AMOUNT"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "TOTAL_ADD_AMOUNT"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Decimal")
    _column.ColumnName = "TOTAL_FINAL_BALANCE"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.Boolean")
    _column.ColumnName = "CURRENCY_FORMAT"
    m_totals_data_table.Columns.Add(_column)

    _column = New DataColumn()
    _column.DataType = System.Type.GetType("System.String")
    _column.ColumnName = "ISO_CODE"
    _column.ReadOnly = False
    m_totals_data_table.Columns.Add(_column)



  End Sub ' InitTotalsDataTable

  ' PURPOSE : Obtain the row from DataTable m_totals_data_table according to MovType.
  '           If it doesn't exist, add a new row for the MovType.
  '
  '  PARAMS :
  '     - INPUT :
  '           - MovType
  '           - CurrencyCode: Indicates if the sub and add amount of the account movement are currency
  '           - MovTypeName: Used only with the movements: HolderLevelChanged and ManualholderLevelChanged to differenciate between destination levels 
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '           - A DataRow for the MovType totals.
  '
  Private Function GetRowFromTotalsByType(ByVal MovType As Integer, ByVal CurrencyCode As String, Optional ByVal MovTypeName As String = "") As DataRow

    Dim _selected_rows() As DataRow
    Dim _filter As String
    Dim _row_new_type As DataRow
    Dim _currency_format As Boolean
    Dim _blank_line As Boolean
    Dim _blank_balance As Boolean

    If (String.IsNullOrEmpty(MovTypeName)) Then
      _filter = "MOV_TYPE = " & MovType & " AND ISO_CODE = '" & CurrencyCode & "'"
    Else
      _filter = "ISO_CODE = '" & CurrencyCode & "'" & " AND MOV_TYPE_NAME = '" & MovTypeName & "'"
    End If

    ' As column MOV_TYPE is Unique, if MovType exists, only one row is selected.
    _selected_rows = m_totals_data_table.Select(_filter)

    ' MovType doesn't exist. Add it to the data table.
    If _selected_rows.Length = 0 Then
      _row_new_type = m_totals_data_table.NewRow()
      _row_new_type.Item("MOV_TYPE") = MovType
      _row_new_type.Item("COUNT") = 0
      _row_new_type.Item("TOTAL_SUB_AMOUNT") = 0
      _row_new_type.Item("TOTAL_ADD_AMOUNT") = 0
      _row_new_type.Item("TOTAL_INITIAL_BALANCE") = 0
      _row_new_type.Item("TOTAL_FINAL_BALANCE") = 0
      _row_new_type.Item("MOV_TYPE_NAME") = TranslateMovementType(MovType, _currency_format, _blank_line, _blank_balance)
      _row_new_type.Item("ISO_CODE") = CurrencyCode
      _row_new_type.Item("CURRENCY_FORMAT") = _currency_format
      m_totals_data_table.Rows.Add(_row_new_type)
    Else
      _row_new_type = _selected_rows(0)
    End If

    Return _row_new_type

  End Function ' GetRowFromTotalsByType

#End Region 'Movement Types

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
        GRID_COLUMN_TERMINAL_ID = 11
        GRID_COLUMN_ADD_AMOUNT = 7
        Call .Init(GRID_COLUMNS_DETAILS, GRID_HEADER_ROWS)
      Else
        GRID_COLUMN_TERMINAL_ID = 7
        GRID_COLUMN_ADD_AMOUNT = 11
        Call .Init(GRID_COLUMNS_NORMAL, GRID_HEADER_ROWS)
      End If

      ' Account number
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCT_NUMBER).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCT_NUMBER).Width = 1150
      .Column(GRID_COLUMN_ACCT_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Account type
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(231)
      .Column(GRID_COLUMN_ACCOUNT_TYPE).Width = 0

      ' Card Track
      .Column(GRID_COLUMN_CARD_TRACK).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_CARD_TRACK).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(212)
      .Column(GRID_COLUMN_CARD_TRACK).Width = GRID_WIDTH_CARD_TRACK_DATA
      .Column(GRID_COLUMN_CARD_TRACK).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(235)
      .Column(GRID_COLUMN_HOLDER_NAME).Width = GRID_WIDTH_CARD_HOLDER_NAME
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Type Id
      .Column(GRID_COLUMN_TYPE_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TYPE_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TYPE_ID).Width = 0
      .Column(GRID_COLUMN_TYPE_ID).IsColumnPrintable = False

      ' Operation Type Name
      .Column(GRID_COLUMN_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TYPE_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(211)
      .Column(GRID_COLUMN_TYPE_NAME).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, 2200, 4800)
      .Column(GRID_COLUMN_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_TERMINAL_ID).IsColumnPrintable = False

      ' Terminal Name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1270)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = 2600
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Initial balance
      .Column(GRID_COLUMN_INIT_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_INIT_BALANCE).Header(1).Text = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(213), "")
      .Column(GRID_COLUMN_INIT_BALANCE).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_INIT_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Sub amount
      .Column(GRID_COLUMN_SUB_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_SUB_AMOUNT).Header(1).Text = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(214), "")
      .Column(GRID_COLUMN_SUB_AMOUNT).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GRID_WIDTH_AMOUNT_SHORT, 0)
      .Column(GRID_COLUMN_SUB_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Add amount
      ' RRB
      .Column(GRID_COLUMN_ADD_AMOUNT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
        .Column(GRID_COLUMN_ADD_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(215)
        .Column(GRID_COLUMN_ADD_AMOUNT).Width = GRID_WIDTH_AMOUNT_SHORT
        .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        If Me.opt_points_accreditations.Checked Then
          .Column(GRID_COLUMN_ADD_AMOUNT).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(305)
          .Column(GRID_COLUMN_ADD_AMOUNT).Width = GRID_WIDTH_AMOUNT_SHORT
          .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        Else
          .Column(GRID_COLUMN_ADD_AMOUNT).Header(1).Text = ""
          .Column(GRID_COLUMN_ADD_AMOUNT).Width = 0
          .Column(GRID_COLUMN_ADD_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        End If
      End If

      ' Final balance
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
      .Column(GRID_COLUMN_FINAL_BALANCE).Header(1).Text = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GLB_NLS_GUI_INVOICING.GetString(216), "")
      .Column(GRID_COLUMN_FINAL_BALANCE).Width = IIf(m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL, GRID_WIDTH_AMOUNT_LONG, 0)
      .Column(GRID_COLUMN_FINAL_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' RRB 27-AUG-2012
      If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
        ' Reasons
        .Column(GRID_COLUMN_REASONS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
        .Column(GRID_COLUMN_REASONS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1262)
        .Column(GRID_COLUMN_REASONS).Width = GRID_WIDTH_REASONS
        .Column(GRID_COLUMN_REASONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

        ' Details
        .Column(GRID_COLUMN_DETAILS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(236)
        .Column(GRID_COLUMN_DETAILS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1263)
        .Column(GRID_COLUMN_DETAILS).Width = GRID_WIDTH_DETAILS
        .Column(GRID_COLUMN_DETAILS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End If

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    Me.uc_account_sel1.Clear()

    If m_initial_account <> 0 Then
      Me.uc_account_sel1.Account = m_initial_account
    End If

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    If Me.m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_NORMAL Then
      Me.opt_all_terminals.Checked = True
      Me.cmb_cashier.Enabled = False
      Me.cmb_terminal.Enabled = False
    Else
      Me.opt_points_accreditations.Checked = True
      Me.opt_level_change.Checked = False
    End If

    'Movement Types Filter Data Grid
    Call InitMovementTypesFilter()

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""
    Dim str_where_account As String = ""

    If str_where_account <> "" Then
      str_where = str_where_account
    End If

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      str_where = str_where & " AND (AM_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & ") "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND (AM_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & ") "
    End If

    ' In TITO Mode, if the user introduces an account id sysem will ignore ac_type to show information.
    If WSI.Common.TITO.Utils.IsTitoMode() AndAlso Me.uc_account_sel1.Account <> String.Empty Then
      str_where_account = Me.uc_account_sel1.GetFilterSQL()
    Else
      ' In other cases, filer by ac_type = CARD
      str_where_account = Me.uc_account_sel1.GetFilterSQL()
    End If

    If str_where_account <> "" Then
      str_where = str_where & " AND " & str_where_account
    End If

    ' Filter Cashier
    If Me.opt_one_cashier.Checked = True Then
      str_where = str_where & " AND (AM_CASHIER_ID = " & Me.cmb_cashier.Value & ") "
    End If

    ' Filter Terminal
    If Me.opt_one_terminal.Checked = True Then
      str_where = str_where & " AND (AM_TERMINAL_ID = " & Me.cmb_terminal.Value & ") "
    End If

    ' RRB 27-AUG-2012 Filter Movement type (Change level or Add/fix points)
    If m_account_movements_type_mode = ENUM_ACCOUNT_MOVEMENTS_TYPE_MODE.TYPE_DETAILS Then
      If Me.opt_points_accreditations.Checked Then
        str_where = str_where & " AND (AM_TYPE BETWEEN " & WSI.Common.MovementType.MULTIPLE_BUCKETS_Manual_Add & " AND " & MovementType.MULTIPLE_BUCKETS_Manual_Set_Last & " ) "
      ElseIf Me.opt_level_change.Checked Then
        str_where = str_where & " AND (AM_TYPE IN ( " & WSI.Common.MovementType.ManualHolderLevelChanged & " )) "
      Else
        str_where = str_where & " AND (AM_TYPE IN ( " & WSI.Common.MovementType.PointsStatusChanged & " )) "
      End If
    End If

    ' Filter Movement type
    str_where = str_where & GetSqlWhereMovType("AM_TYPE")

    ' LJM 29-JAN-2014: Remove hidden movements
    str_where = str_where & "AND AM_TYPE NOT IN ( " & MovementType.HIDDEN_RechargeNotDoneWithCash & ")"

    If Not String.IsNullOrEmpty(str_where) Then
      str_where = " WHERE " & str_where.Remove(str_where.IndexOf("AND"), 3)
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE: Build search component to add to the 'where' clause of the 'select' statement
  '          according to the values selected in the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - DBField: database field name to use in the search condition
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Search condition to add
  Private Function GetSqlWhereMovType(ByVal DBField As String) As String
    Dim str_where As String
    Dim types_list As String

    str_where = ""

    ' Operation Code Type Filter
    If Me.opt_several_types.Checked = True Then
      types_list = GetTypeIdListSelected()
      If types_list <> "" Then
        str_where = " AND (" & DBField & " IN (" & types_list & "))"
      End If
    End If

    Return str_where
  End Function ' GetSqlWhereMovType

#Region " Movement Type Region "

  ' PURPOSE: Define the layout of the movement types grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetMovementTypes()
    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, Editable:=True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_2_COLUMN_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 4105
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_ROW_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_ROW_TYPE).WidthFixed = 0
    End With

  End Sub 'GUI_StyleSheetMovementTypes

  ' PURPOSE: Initialize the movement types filter grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitMovementTypesFilter()

    Me.opt_all_types.Checked = True
    Me.dg_filter.Enabled = True

    ' Reset dg selections
    Call FillMovTypesFilterGrid(GetMovTypesFilterGridData())

  End Sub ' InitMovementTypesFilter

  ' PURPOSE: Populate the movement types data grid filter.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillMovTypesFilterGrid(ByVal DgList As List(Of TYPE_DG_FILTER_ELEMENT))

    Me.dg_filter.Clear()
    Me.dg_filter.Redraw = False

    ' Get the filters array and insert it into the grid.

    For Each _element As TYPE_DG_FILTER_ELEMENT In DgList
      If (_element.elem_type = GRID_2_ROW_TYPE_DATA) Then
        Call AddOneRowData(_element.code, _element.description)
      ElseIf (_element.elem_type = GRID_2_ROW_TYPE_HEADER) Then
        Call AddOneRowHeader(_element.description)
      End If
    Next

    Me.dg_filter.Redraw = True

  End Sub ' FillMovTypesFilterGrid
  ' PURPOSE: Add Points movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddPoints(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2612)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsAwarded
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsToGiftRequest
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsToNotRedeemable
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsGiftDelivery
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsExpired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsToDrawTicketPrint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsGiftServices
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsToRedeemable
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ManuallyAddedPointsOnlyForRedeem
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromotionPoint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CancelPromotionPoint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CancelGiftInstance
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsStatusChanged
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ManuallyAddedPointsForLevel
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.MultiSiteCurrentLocalPoints
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ImportedPointsForLevel
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ImportedPointsOnlyForRedeem
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    ' OPC 04-NOV-2014: Added new type filter.
    _row.code = MovementType.ExternalSystemPointsSubstract
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

  End Sub

  ' PURPOSE: Add Handpay movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddHandpay(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2613)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.Handpay
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.HandpayCancellation
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ManualHandpay
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ManualHandpayCancellation
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)
  End Sub

  ' PURPOSE: Add Cash Operations movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddCashOperations(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _is_tito_mode As Boolean

    _is_tito_mode = TITO.Utils.IsTitoMode

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2614)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.Play
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CashIn
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    If GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") Then
      _row.code = MovementType.CashInTax
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)
    End If

    If WSI.Common.Misc.IsTaxCustodyEnabled() Then
      _row.code = MovementType.TaxCustody
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)
    End If

    If WSI.Common.Misc.IsTaxCustodyEnabled() Then
      _row.code = MovementType.TaxReturnCustody
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)
    End If

    ' DHA 02-MAR-2016 TAx Provisions
    If WSI.Common.Misc.IsTaxProvisionsEnabled() Then
      _row.code = MovementType.TaxProvisions
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)
    End If

    _row.code = MovementType.Devolution
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.DepositIn
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.DepositOut
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.Cancellation
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PrizeCoupon
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CashInCoverCoupon
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.TaxReturningOnPrizeCoupon
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.DecimalRounding
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ServiceCharge
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    ' LJM 13-DEC-2013 Chips movements
    _row.code = MovementType.ChipsPurchaseTotal
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ChipsSaleTotal
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ChipsSaleRemainingAmount
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ChipsSaleConsumedRemainingAmount
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ChipsSaleDevolutionForTito
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    ' DLL 16-SEP-2014 Cash Advance
    _row.code = MovementType.CashAdvance
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    If _is_tito_mode Then
      ' DHA 25-APR-2014 modified/added TITO tickets movements
      _row.code = MovementType.TITO_TicketCashierPrintedCashable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketCashierPrintedPromoRedeemable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketCashierPrintedPromoNotRedeemable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketMachinePrintedCashable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketMachinePrintedPromoNotRedeemable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketCashierPaidCashable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketCashierPaidPromoRedeemable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketMachinePlayedCashable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketMachinePlayedPromoRedeemable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_ticketMachinePlayedPromoNotRedeemable
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketCashierPaidHandpay
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketCashierPaidJackpot
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketMachinePrintedJackpot
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketMachinePrintedHandpay
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketReissue
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_AccountToTerminalCredit
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TerminalToAccountCredit
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.TITO_TicketOffline
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CashdeskDrawParticipation
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

    End If

    ' RMS 21-JAN-2014
    _row.code = MovementType.TransferCreditIn
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4479)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.TransferCreditOut
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4480)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    ' DHA 09-JUL-2015
    _row.code = MovementType.CardDepositInCheck
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6513)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardDepositInCurrencyExchange
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6514)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardDepositInCardCredit
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6515)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardDepositInCardDebit
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6516)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardDepositInCardGeneric
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6517)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardReplacementCheck
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6508)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardReplacementCardCredit
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6510)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardReplacementCardDebit
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6511)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardReplacementCardGeneric
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6512)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    If GeneralParam.GetBoolean("Reception", "Enabled") Then
      _row.code = MovementType.Admission
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)
    End If

    ' RCI & DRV 08-JUL-2014: For now, don't use special movements for Integrated chip operations (Palacio Shortcut).
    '                        USe them if the AC_INITIAL_CASH_IN_FOR_CHIPS is enabled in the future.
    '
    '_row.code = MovementType.ChipsSaleWithCashIn
    '_row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5007)
    '_row.elem_type = GRID_2_ROW_TYPE_DATA
    'DgFilterRows.Add(_row)

    '_row.code = MovementType.ChipsPurchaseWithCashOut
    '_row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5008)
    '_row.elem_type = GRID_2_ROW_TYPE_DATA
    'DgFilterRows.Add(_row)

  End Sub

  ' PURPOSE: Add Prize movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddPrize(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2618)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.CashOut
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.TaxOnPrize1
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.TaxOnPrize2
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PrizeExpired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromotionRedeemableFederalTax
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromotionRedeemableStateTax
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)


  End Sub

  ' PURPOSE: Add Account movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddAccount(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2615)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.Activate
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.Deactivate
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardReplacement
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.HolderLevelChanged
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardCreated
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountPINChange
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountPINRandom
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountPersonalization
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardRecycled
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ManualHolderLevelChanged
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountBlocked
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountUnblocked
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountInBlacklist
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.AccountNotInBlacklist
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CardReplacementForFree
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.WinLossStatementRequest
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)
  End Sub

  ' PURPOSE: Add Promotions movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddPromotions(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT
    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2616)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromotionNotRedeemable
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CancelNotRedeemable
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoCredits
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoToRedeemable
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoExpired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoCancel
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoStartSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoEndSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CreditsExpired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CreditsNotRedeemableExpired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)


    _row.code = MovementType.ReservedExpired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromoManualEndSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CreditsNotRedeemable2Expired
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromotionRedeemable
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CancelPromotionRedeemable
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    _row.description = TranslateMovementType(_row.code)
    DgFilterRows.Add(_row)

    _row.code = MovementType.PromotionPoint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CancelPromotionPoint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

  End Sub

  ' PURPOSE: Add Import movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddImport(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2617)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.ImportedAccount
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ImportedPointsOnlyForRedeem
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ImportedPointsForLevel
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ImportedPointsHistory
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)
  End Sub

  ' PURPOSE: Add Draw movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddDraw(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2619)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)

    _row.code = MovementType.DrawTicketPrint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.PointsToDrawTicketPrint
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CashdeskDrawParticipation
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CashdeskDrawPlaySession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)
  End Sub

  ' PURPOSE: Add Session movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddSession(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    _row.code = -1
    _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2620)
    _row.elem_type = GRID_2_ROW_TYPE_HEADER
    DgFilterRows.Add(_row)


    _row.code = MovementType.StartCardSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.EndCardSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.ManualEndSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CancelStartSession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    _row.code = MovementType.CashdeskDrawPlaySession
    _row.description = TranslateMovementType(_row.code)
    _row.elem_type = GRID_2_ROW_TYPE_DATA
    DgFilterRows.Add(_row)

    'This movement is reserved for a future use
    '_row.code = MovementType.CashInNotRedeemable2
    '_row.description = TranslateMovementType(_row.code)
    'DgFilterRows.Add(_row)
    '

  End Sub

  ' PURPOSE: Add GameGateway movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddGameGateway(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT
    Dim _provider_gp_id As String

    If GeneralParam.GetBoolean("GameGateway", "Enabled", False) Then

      _provider_gp_id = GeneralParam.GetString("GameGateway", "Provider", "000")

      If GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.Enabled", _provider_gp_id), False) Then

        _row.code = -1
        _row.description = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Name", _provider_gp_id), "GameGateway")
        _row.elem_type = GRID_2_ROW_TYPE_HEADER
        DgFilterRows.Add(_row)

        'Bets
        _row.code = MovementType.GameGatewayBet
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        'Prizes
        _row.code = MovementType.GameGatewayPrize
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        'Rollback
        _row.code = MovementType.GameGatewayBetRollback
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        'Reserve Credit
        _row.code = MovementType.GameGatewayReserveCredit
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

      End If

    End If


  End Sub

  ' PURPOSE: Add Buckets movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddBuckets(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row_header As TYPE_DG_FILTER_ELEMENT
    Dim _row As TYPE_DG_FILTER_ELEMENT

    Dim _id As Integer
    Dim _buckets_forms As BucketsForm

    _buckets_forms = New BucketsForm()
    ' DOING

    Dim _buckets_id As List(Of Long)
    _buckets_id = New List(Of Long)

    _buckets_forms.GetBucketsID(_buckets_id)

    If (_buckets_id.Count > 0) Then

      _row_header.code = -1
      _row_header.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6968)
      _row_header.elem_type = GRID_2_ROW_TYPE_HEADER
      DgFilterRows.Add(_row_header)

      ' buckets
      For Each _id In _buckets_id
        _row.code = MovementType.MULTIPLE_BUCKETS_EndSession + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = MovementType.MULTIPLE_BUCKETS_Expired + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = MovementType.MULTIPLE_BUCKETS_Manual_Add + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = MovementType.MULTIPLE_BUCKETS_Manual_Sub + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

        _row.code = MovementType.MULTIPLE_BUCKETS_Manual_Set + _id
        _row.description = TranslateMovementType(_row.code)
        _row.elem_type = GRID_2_ROW_TYPE_DATA
        DgFilterRows.Add(_row)

      Next
    End If

  End Sub

  ' PURPOSE: Add PariPlay movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT: DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT)
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - 
  Private Sub MovTypesFilterGridAddPariPlay(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    If GeneralParam.GetBoolean("PariPlay", "Enabled", False) Then

      _row.code = -1
      _row.description = GeneralParam.GetString("PariPlay", "Name", "PariPlay")
      _row.elem_type = GRID_2_ROW_TYPE_HEADER
      DgFilterRows.Add(_row)

      'Bets
      _row.code = MovementType.PariPlayBet
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      'Prizes
      _row.code = MovementType.PariPlayPrize
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      'Rollback
      _row.code = MovementType.PariPlayBetRollback
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

    End If


  End Sub

  ''' <summary>
  ''' Add credit line movements to filter
  ''' </summary>
  ''' <param name="DgFilterRows"></param>
  ''' <remarks></remarks>
  Private Sub MovTypesFilterGridAddCreditLine(ByRef DgFilterRows As List(Of TYPE_DG_FILTER_ELEMENT))
    Dim _row As TYPE_DG_FILTER_ELEMENT

    If GeneralParam.GetBoolean("CreditLine", "Enabled", False) Then

      _row.code = -1
      _row.description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7947)
      _row.elem_type = GRID_2_ROW_TYPE_HEADER
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineCreate
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineUpdate
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineApproved
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineNotApproved
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineSuspended
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineExpired
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineGetMarker
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLinePayback
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

      _row.code = MovementType.CreditLineGetMarkerCardReplacement
      _row.description = TranslateMovementType(_row.code)
      _row.elem_type = GRID_2_ROW_TYPE_DATA
      DgFilterRows.Add(_row)

    End If

  End Sub ' MovTypesFilterGridAddCreditLine

  ' PURPOSE: Obtain the list of elements that will populate the 
  '         movement types filter grid.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - List of elements to be added to the grid
  Private Function GetMovTypesFilterGridData() As List(Of TYPE_DG_FILTER_ELEMENT)

    ' Dimensioned this way to avoid getting an extra element: VB uses the constant to set the upper value, 
    ' not the number of elements
    Dim _dg_filter_rows_list As List(Of TYPE_DG_FILTER_ELEMENT)

    _dg_filter_rows_list = New List(Of TYPE_DG_FILTER_ELEMENT)

    'ADD Points section
    MovTypesFilterGridAddPoints(_dg_filter_rows_list)

    'ADD Hanpays section
    MovTypesFilterGridAddHandpay(_dg_filter_rows_list)

    'ADD Cash Operations
    MovTypesFilterGridAddCashOperations(_dg_filter_rows_list)

    'ADD Prize section
    MovTypesFilterGridAddPrize(_dg_filter_rows_list)

    'ADD Account section
    MovTypesFilterGridAddAccount(_dg_filter_rows_list)

    'ADD Promotions section
    MovTypesFilterGridAddPromotions(_dg_filter_rows_list)

    'ADD Import section
    MovTypesFilterGridAddImport(_dg_filter_rows_list)

    'ADD Draw section
    MovTypesFilterGridAddDraw(_dg_filter_rows_list)

    'ADD Session section
    MovTypesFilterGridAddSession(_dg_filter_rows_list)

    'ADD GameGateway
    MovTypesFilterGridAddGameGateway(_dg_filter_rows_list)

    'BUCKETS 
    If (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) = 0) Then 'Buckets SPACE	
      MovTypesFilterGridAddBuckets(_dg_filter_rows_list)
    End If

    'ADD PariPlay
    MovTypesFilterGridAddPariPlay(_dg_filter_rows_list)

    'ADD Credit Lines
    MovTypesFilterGridAddCreditLine(_dg_filter_rows_list)

    Return _dg_filter_rows_list

  End Function ' GetMovTypesFilterGridData

  ' PURPOSE: Add a new header row the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - HeaderMsg: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowHeader(ByVal HeaderMsg As String)

    dg_filter.AddRow()

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = HeaderMsg
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER
    Call dg_filter.Row(dg_filter.NumRows - 1).SetSignalAllowExpand(GRID_2_COLUMN_DESC)
  End Sub ' AddOneRowHeader

  ' PURPOSE: Add a new data row at the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - TypeCode: numeric identifier
  '           - TypeDesc: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowData(ByVal TypeCode As Integer, ByVal TypeDesc As String)
    Dim prev_redraw As Boolean

    prev_redraw = Me.dg_filter.Redraw
    Me.dg_filter.Redraw = False

    dg_filter.AddRow()

    Me.dg_filter.Redraw = False

    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_CODE).Value = TypeCode
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & TypeDesc
    dg_filter.Cell(dg_filter.NumRows - 1, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_DATA

    Me.dg_filter.Row(dg_filter.NumRows - 1).Height = 0

    Me.dg_filter.Redraw = prev_redraw
  End Sub ' AddOneRowData

  ' PURPOSE: Get the list of the items selected in the filter data grid
  '          Format list to build query: X, X, X
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String contining id's of the selected rows
  Private Function GetTypeIdListSelected() As String
    Dim idx_row As Integer
    Dim list_mov_types As String

    list_mov_types = ""

    For idx_row = 0 To Me.dg_filter.NumRows - 1
      If dg_filter.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And dg_filter.Cell(idx_row, GRID_2_COLUMN_CODE).Value <> "" Then
        If list_mov_types.Length = 0 Then
          list_mov_types = dg_filter.Cell(idx_row, GRID_2_COLUMN_CODE).Value
        Else
          list_mov_types = list_mov_types & ", " & dg_filter.Cell(idx_row, GRID_2_COLUMN_CODE).Value
        End If
      End If
    Next

    Return list_mov_types
  End Function 'GetTypeIdListSelected

  Private Function RowIsHeader(ByVal RowIdx As Integer) As Boolean

    If Me.dg_filter.Cell(RowIdx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

      Return True
    Else

      Return False
    End If

  End Function ' RowIsHeader

  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_ROW_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

        Exit Sub
      End If

      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been unchecked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _checked As Boolean

    _idx_bottom_header = -1
    _idx_header = -1
    ' Go look for the header from current row upwards
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If

      If Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _idx_header = -1 Then
        ' One or more upper rows are already unchecked
        _checked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_filter.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _checked = True
      End If
    Next

    'update de check status of the header row when some or all the dependant rows has been checked
    If Not _checked Then
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Else
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If

  End Sub ' TryToUncheckHeader
  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToCheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _is_unchecked As Boolean

    _is_unchecked = False
    _idx_header = -1
    _idx_bottom_header = -1
    ' Go look for the header from current row upwards usually controls if any item is unchecked
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If
      If _idx_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If

    Next
    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.dg_filter.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Updates the header check status if all rows are checked
    If _is_unchecked = False Then
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.dg_filter.Cell(_idx_header, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If
  End Sub ' TryToCheckHeader

  ' PURPOSE: Returns a string with the description of all the selected movement types
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - SelectedMovements
  '
  ' RETURNS:
  '     - None
  Private Sub GetSelectedMovementTypes(ByRef SelectedMovements As String)
    Dim idx As Integer

    SelectedMovements = ""

    For idx = 0 To dg_filter.NumRows - 1
      If Not RowIsHeader(idx) Then
        If Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          If SelectedMovements = "" Then
            SelectedMovements = Me.dg_filter.Cell(idx, GRID_2_COLUMN_DESC).Value.Trim()
          Else
            SelectedMovements &= ", " & Me.dg_filter.Cell(idx, GRID_2_COLUMN_DESC).Value.Trim()
          End If
        End If
      End If
    Next
  End Sub

#End Region

#End Region ' Private Functions

#Region " Events "

  Private Sub opt_one_cashier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
    Me.cmb_terminal.Enabled = False
  End Sub ' opt_one_cashier_Click

  Private Sub opt_one_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_terminal.Click
    Me.cmb_cashier.Enabled = False
    Me.cmb_terminal.Enabled = True
  End Sub ' opt_one_terminal_Click

  Private Sub opt_all_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_terminals.Click
    Me.cmb_terminal.Enabled = False
    Me.cmb_cashier.Enabled = False
  End Sub ' opt_all_terminals_Click

  Private Sub opt_all_types_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_types.Click
    Dim idx As Integer

    For idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub ' opt_all_types_Click

  Private Sub dg_types_DataSelectedEvent() Handles dg_filter.DataSelectedEvent
    Dim _idx As Integer
    Dim _last_row As Integer
    Dim _row_is_header As Boolean

    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

    With Me.dg_filter
      _row_is_header = RowIsHeader(.CurrentRow)

      If _row_is_header And .CurrentCol <> GRID_2_COLUMN_CHECKED Then
        _last_row = .CurrentRow
        For _idx = .CurrentRow + 1 To .NumRows - 1
          If RowIsHeader(_idx) Then

            Exit For
          End If
          _last_row = _idx
        Next

        Me.dg_filter.CollapseExpand(.CurrentRow, _last_row, GRID_2_COLUMN_DESC)

      End If
    End With
  End Sub ' dg_types_DataSelectedEvent

  Private Sub dg_types_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    Dim current_row As Integer

    ' Prevent recursive calls
    If m_refreshing_grid = True Then

      Exit Sub
    End If

    ' Update radio buttons accordingly
    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

    With Me.dg_filter

      current_row = .CurrentRow
      m_refreshing_grid = True

      If RowIsHeader(.CurrentRow) Then
        Call ChangeCheckofRows(.CurrentRow, .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
      Else
        If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          Call TryToUncheckHeader(.CurrentRow)
        Else
          Call TryToCheckHeader(.CurrentRow)
        End If
      End If

      m_refreshing_grid = False
      .CurrentRow = current_row

    End With
  End Sub ' dg_types_CellDataChangedEvent

#End Region ' Events

End Class
