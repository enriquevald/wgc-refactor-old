﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd. 
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_drop_conciliation_edit.vb
' DESCRIPTION:   Conciliation Drop
' AUTHOR:        José Martínez
' CREATION DATE: 24-JUN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-JUN-2014  JML    Initial version.
' 06-APR-2017  RAB    PBI 26540: MES10 Ticket validation - Prevent the use of TITO tickets
' 12-APR-2017  DHA    Bug 26772:The name of buttons Conciliation and delete conciliation are cut on gambling table session form
' 20-APR-2017  DHA    PBI 26808: MES10 Ticket validation - Ticket conciliation
' 26-APR-2017  JML    PBI 26808: MES10 Ticket validation - Ticket conciliation
' 26-APR-2017  JML    PBI 26810: MES10 Ticket validation - Ticket conciliation action (without gaming table validation)
' 02-MAY-2017  JML    Fixed Bug 27159:Cancel conciliation when the tickets have been scanned, the Chip conciliation form is closed without the pop-up showed "are you sure cancel..."
' 03-MAY-2017  JML    PBI 27170: MES10 Ticket validation - Retrocompatibility
' 05-MAY-2017  JML    WIGOS-1679: Drop gaming table information - conciliation parameter
' 08-MAY-2017  JML    PBI 27174: Drop gaming table information - Conciliation parameter
' 08-MAY-2017  RAB    PBI 27176: Ticket validation - Barcode format
' 05-JUN-2018  AGS		Bug 32889:WIGOS-12142, WIGOS-12143, WIGOS-12146, WIGOS-12148 - Drop mesas
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.frm_base_sel
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

#End Region

Public Class frm_drop_conciliation_edit
  Inherits frm_base_edit

#Region " Constants "
  Private Const FORM_DB_MIN_VERSION As Short = 270

  'Grid distriburion Column
  Private Const GRID_INDEX As Integer = 0
  Private Const GRID_COLUMN_TICKET_TABLE_SESSION As Integer = 1
  Private Const GRID_COLUMN_TICKET_NUMBER As Integer = 2
  Private Const GRID_COLUMN_TICKET_ISSUED_VALUE As Integer = 3
  Private Const GRID_COLUMN_TICKET_ISSUED_ISO_CODE As Integer = 4
  Private Const GRID_COLUMN_TICKET_VALUE As Integer = 5
  Private Const GRID_COLUMN_TICKET_DATE As Integer = 6
  Private Const GRID_COLUMN_TICKET_STATUS As Integer = 7
  Private Const GRID_COLUMN_TICKET_STATUS_DESCRIPTION As Integer = 8

  'Grid distriburion Column Width 
  Private Const GRID_INDEX_WIDTH As Int32 = 200
  Private Const GRID_COLUMN_TICKET_TABLE_SESSION_WIDTH As Int32 = 0
  Private Const GRID_COLUMN_TICKET_NUMBER_WIDTH As Int32 = 2800
  Private Const GRID_COLUMN_TICKET_ISSUED_VALUE_WIDTH As Int32 = 1600
  Private Const GRID_COLUMN_TICKET_ISSUED_ISO_CODE_WIDTH As Int32 = 600
  Private Const GRID_COLUMN_TICKET_VALUE_WIDTH As Int32 = 1700
  Private Const GRID_COLUMN_TICKET_DATE_WIDTH As Int32 = 2100
  Private Const GRID_COLUMN_TICKET_STATUS_WIDTH As Int32 = 0
  Private Const GRID_COLUMN_TICKET_STATUS_DESCRIPTION_WIDTH As Int32 = 3400 '5000

  Private Const GRID_TICKET_COLUMN_COUNT As Integer = 9
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const COUNTER_VOUCHER_NO_OK As Integer = 1

  Private Const COLOR_VOUCHER_NO_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_VOUCHER_NO_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_VOUCHER_PENDING = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

#End Region  'Constants

#Region " Members "
  Private m_drop_conciliation As CLASS_DROP_CONCILIATION
  Private m_drop_amount As Decimal

  Private m_barcode_handler As KbdMsgFilterHandler

  Private m_counter_voucher_no_ok As Int32

  Private m_is_tito_mode As Boolean

  Private m_is_enable_suggest_tickets As Boolean

#End Region    'Members

#Region "Properties"

#End Region   'Properties

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DROP_CONCILIATION_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Drop Conciliation
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6498)    ' Drop Conciliation

    Me.gb_gaming_table_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6500)
    Me.lbl_table_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6501)
    Me.lbl_table_session_value.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_00)
    Me.lbl_open_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6502)
    Me.lbl_open_date_value.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_00)

    Me.ef_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6535)
    Me.ef_ticket.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 25)
    Me.ef_ticket.TextAlign = HorizontalAlignment.Left

    KeyboardMessageFilter.Init()
    m_barcode_handler = AddressOf Me.BarcodeEvent
    KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.FilterAny, m_barcode_handler)

    Me.btn_entry.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6526)

    Me.ef_total_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6577)
    Me.ef_total_tickets.Value = GUI_FormatCurrency(0)
    Me.ef_total_tickets.IsReadOnly = True
    Me.ef_total_tickets.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)

    Me.ef_collected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6576)
    Me.ef_collected.Value = GUI_FormatCurrency(0)
    Me.ef_collected.IsReadOnly = True
    Me.ef_collected.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)

    Me.ef_drop.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6527)
    Me.ef_drop.IsReadOnly = True
    Me.ef_drop.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 12, 2)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6528)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = False

    m_is_tito_mode = TITO.Utils.IsTitoMode()

    Call GUI_StyleView()

    m_counter_voucher_no_ok = 0

  End Sub ' GUI_InitControls

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    KeyboardMessageFilter.UnregisterHandler(Me, m_barcode_handler)
    MyBase.GUI_Closing(CloseCanceled)
  End Sub

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If GUI_ParseNumber(ef_total_tickets.Value) < 0 Then
      ' NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6500), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Call Me.ef_ticket.Focus()

      Return False
    End If

    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Me.DbEditedObject = m_drop_conciliation

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _has_conciliate As Boolean
    Dim _row As Integer

    _has_conciliate = m_drop_conciliation.m_type_voucher.HasConciliate
    DbReadObject = m_drop_conciliation.Duplicate
    _row = 0

    dg_entry_tickets.Clear()

    For Each _voucher As CLASS_DROP_CONCILIATION.TYPE_RECONCILED_VOUCHER In m_drop_conciliation.m_type_voucher.Vouchers
      Call AddTicketToGrid(_row, _voucher)
      Call AddTotalsOfGrid(_row, _voucher)
    Next

    ef_drop.Value = m_drop_amount

    If (m_drop_conciliation.m_type_voucher.Vouchers.Count = 0 And m_drop_conciliation.m_type_voucher.ValidatedTickets.Count > 0) Then
      LoadValidatedTicketsToGrid(m_drop_conciliation.m_type_voucher.ValidatedTickets)
    End If

    Me.ef_collected.Value = GUI_FormatCurrency(CDec(Me.ef_drop.Value) - CDec(Me.ef_total_tickets.Value))

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_DROP_CONCILIATION_EDIT).Write And Not _has_conciliate
    Me.ef_ticket.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_DROP_CONCILIATION_EDIT).Write And Not _has_conciliate
    Me.btn_entry.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_DROP_CONCILIATION_EDIT).Write And Not _has_conciliate

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_DROP_CONCILIATION_EDIT).Delete And _has_conciliate

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT :
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ButtonId As ENUM_BUTTON)
    Dim _inquiry As GUI_CommonMisc.ENUM_MB_RESULT

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE
        _inquiry = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6537), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6536))
        If _inquiry = ENUM_MB_RESULT.MB_RESULT_YES Then
          MyBase.GUI_ButtonClick(ButtonId)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub  'GUI_ButtonClick

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_DROP_CONCILIATION

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Else
          DbEditedObject = m_drop_conciliation
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_SUPPORTED Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        Call MyBase.GUI_DB_Operation(DbOperation)
        'Call Me.Parent.

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub  'GUI_DB_Operation

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_ticket

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Keypress 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.dg_entry_tickets.ContainsFocus Then
          Me.dg_entry_tickets.KeyPressed(sender, e)

          Return True
        End If

        If Me.ActiveControl.Name = ef_ticket.Name Then
          Call btn_entry_ClickEvent()

          Return True
        End If

        Return False

      Case Chr(Keys.Escape)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select
  End Function ' GUI_KeyPress

#End Region    'Overrides

#Region "Public"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub ' New

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowEditItem(ByVal GammingTableSessionId As Int64 _
                                  , ByVal GammingTableName As String _
                                  , ByVal GammingTableSessionOpenDate As String _
                                  , ByVal Drop As Decimal)

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.m_drop_conciliation = New CLASS_DROP_CONCILIATION()

    Me.m_drop_conciliation.m_type_voucher.TableSessionId = GammingTableSessionId
    Me.m_drop_conciliation.m_type_voucher.TableName = GammingTableName
    Me.m_drop_conciliation.m_type_voucher.OpenSessionDate = GammingTableSessionOpenDate

    Me.lbl_table_session_value.Text = GammingTableName
    Me.lbl_open_date_value.Text = GammingTableSessionOpenDate
    Me.ef_drop.Value = GUI_FormatCurrency(Drop)
    Me.ef_collected.Value = GUI_FormatCurrency(Drop)
    Me.DbObjectId = m_drop_conciliation

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowEditItem

#End Region       'Public

#Region "Privates"

  Private Sub InitializeAuditorData()
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' InitializeAuditorData

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()

    With dg_entry_tickets

      Call .Init(GRID_TICKET_COLUMN_COUNT, GRID_HEADER_ROWS, True)

      .Counter(COUNTER_VOUCHER_NO_OK).Visible = True
      ' Completed Counter
      .Counter(COUNTER_VOUCHER_NO_OK).Visible = True
      .Counter(COUNTER_VOUCHER_NO_OK).BackColor = GetColor(COLOR_VOUCHER_NO_OK_BACK)
      .Counter(COUNTER_VOUCHER_NO_OK).ForeColor = GetColor(COLOR_VOUCHER_NO_OK_FORE)

      ' Grid Columns

      ' Index
      .Column(GRID_INDEX).Header(0).Text = " "
      .Column(GRID_INDEX).Header(1).Text = " "
      .Column(GRID_INDEX).Width = GRID_INDEX_WIDTH
      .Column(GRID_INDEX).HighLightWhenSelected = False

      'Ticket table session
      .Column(GRID_COLUMN_TICKET_TABLE_SESSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6503)
      .Column(GRID_COLUMN_TICKET_TABLE_SESSION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6500)
      .Column(GRID_COLUMN_TICKET_TABLE_SESSION).Width = GRID_COLUMN_TICKET_TABLE_SESSION_WIDTH
      .Column(GRID_COLUMN_TICKET_TABLE_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Ticket number
      .Column(GRID_COLUMN_TICKET_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6503)
      .Column(GRID_COLUMN_TICKET_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6535)
      .Column(GRID_COLUMN_TICKET_NUMBER).Width = GRID_COLUMN_TICKET_NUMBER_WIDTH
      .Column(GRID_COLUMN_TICKET_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Ticket issued value
      .Column(GRID_COLUMN_TICKET_ISSUED_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6503)
      .Column(GRID_COLUMN_TICKET_ISSUED_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7104)
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_TICKET_ISSUED_VALUE).Width = GRID_COLUMN_TICKET_ISSUED_VALUE_WIDTH
      Else
        .Column(GRID_COLUMN_TICKET_ISSUED_VALUE).Width = 0
      End If
      .Column(GRID_COLUMN_TICKET_ISSUED_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Ticket issued iso code
      .Column(GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6503)
      .Column(GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7104)
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Width = GRID_COLUMN_TICKET_ISSUED_ISO_CODE_WIDTH
      Else
        .Column(GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Width = 0
      End If
      .Column(GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Ticket value
      .Column(GRID_COLUMN_TICKET_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6503)
      .Column(GRID_COLUMN_TICKET_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6505)
      .Column(GRID_COLUMN_TICKET_VALUE).Width = GRID_COLUMN_TICKET_VALUE_WIDTH
      .Column(GRID_COLUMN_TICKET_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Ticket date
      .Column(GRID_COLUMN_TICKET_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6503)
      .Column(GRID_COLUMN_TICKET_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6506)
      .Column(GRID_COLUMN_TICKET_DATE).Width = GRID_COLUMN_TICKET_DATE_WIDTH
      .Column(GRID_COLUMN_TICKET_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Ticket status
      .Column(GRID_COLUMN_TICKET_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6504)
      .Column(GRID_COLUMN_TICKET_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6507)
      .Column(GRID_COLUMN_TICKET_STATUS).Width = GRID_COLUMN_TICKET_STATUS_WIDTH
      .Column(GRID_COLUMN_TICKET_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      'Ticket status description
      .Column(GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6504)
      .Column(GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6507)
      If m_is_tito_mode Then
        .Column(GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Width = GRID_COLUMN_TICKET_STATUS_DESCRIPTION_WIDTH
      Else
        .Column(GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Width = GRID_COLUMN_TICKET_STATUS_DESCRIPTION_WIDTH + GRID_COLUMN_TICKET_ISSUED_VALUE_WIDTH + GRID_COLUMN_TICKET_ISSUED_ISO_CODE_WIDTH
      End If
      .Column(GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleView

  Private Sub AddTicketToGrid(ByRef Row As Integer, Voucher As CLASS_DROP_CONCILIATION.TYPE_RECONCILED_VOUCHER)
    ' Adds new row to the tickets grid.
    dg_entry_tickets.AddRow()
    Row = dg_entry_tickets.NumRows - 1

    If (Voucher.OperationBarcode.Length <= 18) Then
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_NUMBER).Value = WSI.Common.TITO.ValidationNumberManager.FormatValidationNumber(Voucher.OperationBarcode, TITO_VALIDATION_TYPE.SYSTEM, False)
    Else
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_NUMBER).Value = Voucher.OperationBarcode
    End If
    If Voucher.VoucherStatus <> CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.NOT_EXISTS Then
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_ISSUED_VALUE).Value = GUI_FormatCurrency(Voucher.IssuedValueAmount, , , False)
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Value = Voucher.IssuedIsoCode
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_VALUE).Value = GUI_FormatCurrency(Voucher.ValueAmount)
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_DATE).Value = GUI_FormatDate(Voucher.CreationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_ISSUED_VALUE).Value = ""
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Value = ""
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_VALUE).Value = ""
      Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_DATE).Value = ""
    End If
    Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_STATUS).Value = Voucher.VoucherStatus
    Me.dg_entry_tickets.Cell(Row, GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Value = Voucher.StatusDescription

  End Sub ' AddTicketToGrid

  Private Sub AddTotalsOfGrid(Row As Integer, Voucher As CLASS_DROP_CONCILIATION.TYPE_RECONCILED_VOUCHER)
    Dim _find As Boolean
    Dim _item As CLASS_DROP_CONCILIATION.TYPE_TOTAL_CURRENCIES

    _find = False

    If Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK Or Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK_OVER_AMOUNT Then
      m_drop_amount += Voucher.ValueAmount
      Me.ef_total_tickets.Value = GUI_FormatCurrency(m_drop_amount)

      For Each _curr As CLASS_DROP_CONCILIATION.TYPE_TOTAL_CURRENCIES In m_drop_conciliation.m_type_voucher.SalesAmounts
        If _curr.TotalCurrencyIsoCode = Voucher.IssuedIsoCode Then
          _curr.TotalIssuedAmount += Voucher.IssuedValueAmount
          _curr.TotalAmount += Voucher.ValueAmount
          _find = True
        End If
      Next
      If Not _find Then
        _item = New CLASS_DROP_CONCILIATION.TYPE_TOTAL_CURRENCIES()
        _item.TotalIssuedAmount = Voucher.IssuedValueAmount
        _item.TotalCurrencyIsoCode = Voucher.IssuedIsoCode
        _item.TotalAmount = Voucher.ValueAmount
        m_drop_conciliation.m_type_voucher.SalesAmounts.Add(_item)
      End If

    Else
      Me.dg_entry_tickets.Cell(Row, GRID_INDEX).BackColor = GetColor(COLOR_VOUCHER_NO_OK_BACK)
      m_counter_voucher_no_ok += 1
    End If

    dg_entry_tickets.Counter(COUNTER_VOUCHER_NO_OK).Value = m_counter_voucher_no_ok

    Me.ef_ticket.Value = String.Empty

  End Sub ' AddTotalsOfGrid


  Private Sub LoadValidatedTicketsToGrid(ValidatedTickets As List(Of CLASS_DROP_CONCILIATION.TYPE_VALIDATED_TICKETS))
    Dim _row As Integer

    m_is_enable_suggest_tickets = GeneralParam.GetBoolean("GamingTables", "Conciliation.SuggestTickets", True)

    If m_is_enable_suggest_tickets Then
      For Each _validated_ticket As CLASS_DROP_CONCILIATION.TYPE_VALIDATED_TICKETS In ValidatedTickets

        ' Adds new row to the tickets grid.
        dg_entry_tickets.AddRow()
        _row = dg_entry_tickets.NumRows - 1

        If (_validated_ticket.ValidationNumber.Length <= 18) Then
          Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_NUMBER).Value = WSI.Common.TITO.ValidationNumberManager.FormatValidationNumber(_validated_ticket.ValidationNumber, TITO_VALIDATION_TYPE.SYSTEM, False)
        Else
          Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_NUMBER).Value = _validated_ticket.ValidationNumber
        End If
        Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_ISSUED_VALUE).Value = GUI_FormatCurrency(_validated_ticket.TicketAmountIssued, , , False)
        Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_ISSUED_ISO_CODE).Value = _validated_ticket.TicketIsocodeIssued
        Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_VALUE).Value = GUI_FormatCurrency(_validated_ticket.TicketAmount)
        Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_DATE).Value = GUI_FormatDate(_validated_ticket.TicketCreationDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_STATUS).Value = IIf(_validated_ticket.TicketCollected, CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.REPEATED_IN_OTHER_RECONCILIATION, CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.PENDING)
        Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Value = m_drop_conciliation.GetStatusDescription(CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.PENDING)
        Me.dg_entry_tickets.Cell(_row, GRID_INDEX).BackColor = GetColor(COLOR_VOUCHER_PENDING)

      Next
    End If

  End Sub ' AddTicketToGrid

  Private Sub VoucherData(VoucherNumber As Int64, Voucher As CLASS_DROP_CONCILIATION.TYPE_RECONCILED_VOUCHER, ByRef Row As Integer, ByRef IsNeededAddRow As Boolean)
    Dim _sb As StringBuilder
    Dim _table_result As DataTable
    Dim _dr As DataRow
    Dim _other_conciliation As String
    Dim _is_validated_ticket As Boolean
    Dim _is_validated_collected As Boolean

    _table_result = New DataTable()
    _other_conciliation = ""

    IsNeededAddRow = True
    _is_validated_ticket = False

    _sb = New StringBuilder()

    _sb.AppendLine(" SELECT   CM_MOVEMENT_ID             ")
    _sb.AppendLine("        , CM_TYPE                    ")
    _sb.AppendLine("        , CM_OPERATION_ID            ")
    _sb.AppendLine("        , CM_DATE                    ")
    _sb.AppendLine("        , CM_GAMING_TABLE_SESSION_ID ")
    _sb.AppendLine("        , CASE WHEN CM_TYPE = @pMovementType THEN CM_SUB_AMOUNT ELSE CM_INITIAL_BALANCE END AS ISSUED_AMOUNT ")
    _sb.AppendLine("        , CM_CURRENCY_ISO_CODE       ")
    _sb.AppendLine("        , CM_SUB_AMOUNT              ")
    _sb.AppendLine("        , AO_UNDO_STATUS             ")
    _sb.AppendLine("        , AO_UNDO_OPERATION_ID       ")
    _sb.AppendLine("        , CS_OPENING_DATE            ")
    _sb.AppendLine("   FROM   CASHIER_MOVEMENTS       WITH(INDEX( IX_cm_operation_id ))    ")
    _sb.AppendLine("  INNER   JOIN ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID ")
    _sb.AppendLine("   LEFT   JOIN CASHIER_SESSIONS   ON CS_SESSION_ID = CM_SESSION_ID     ")
    _sb.AppendLine("  WHERE   CM_OPERATION_ID = @pOperationId  ")
    _sb.AppendLine("    AND   CM_TYPE       IN (@pMovementType ")
    _sb.AppendLine("                          , @pMovementTypeExchange) ")

    Try
      Using _db_trx As New DB_TRX()

        If Not (Ticket.LoadTicketsByCondition(" TI_COLLECTED IS NULL AND TI_VALIDATION_NUMBER = " + ef_ticket.Value, _table_result, _db_trx.SqlTransaction)) Then
          Return
        End If
        _is_validated_collected = _table_result.Rows(0).Item("TI_COLLECTED")
        If (_table_result.Rows.Count = 1 AndAlso _table_result.Rows(0).Item("TI_TYPE_ID") <> TITO_TICKET_TYPE.CHIPS_DEALER_COPY) Then
          Voucher.OperationBarcode = ef_ticket.Value
          Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.NOT_EXISTS
          Voucher.StatusDescription = m_drop_conciliation.GetStatusDescription(Voucher.VoucherStatus, _other_conciliation)
          Return
        End If

        If (_table_result.Rows.Count = 1 AndAlso _table_result.Rows(0).Item("TI_TYPE_ID") = TITO_TICKET_TYPE.CHIPS_DEALER_COPY) Then
          Voucher.OperationBarcode = ef_ticket.Value
          Voucher.StatusDescription = m_drop_conciliation.GetStatusDescription(Voucher.VoucherStatus, _other_conciliation)
          VoucherNumber = _table_result.Rows(0).Item("TI_TRANSACTION_ID")
          Voucher.TickedId = _table_result.Rows(0).Item("TI_TICKET_ID")
          Voucher.TickedStatus = DirectCast(_table_result.Rows(0).Item("TI_STATUS"), TITO_TICKET_STATUS)
          If m_is_enable_suggest_tickets Then

            For _row As Integer = 0 To dg_entry_tickets.NumRows - 1
              If ef_ticket.Value = dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_NUMBER).Value.Replace("-", "") Then
                If Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK And dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_STATUS).Value = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.PENDING Then
                  Voucher.IssuedValueAmount = _table_result.Rows(0).Item("TI_AMT0")
                  Voucher.IssuedIsoCode = _table_result.Rows(0).Item("TI_CUR0")
                  Voucher.ValueAmount = _table_result.Rows(0).Item("TI_AMOUNT")
                  Voucher.CreationDate = _table_result.Rows(0).Item("TI_CREATED_DATETIME")
                  dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_STATUS).Value = Voucher.VoucherStatus
                  Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_STATUS_DESCRIPTION).Value = m_drop_conciliation.GetStatusDescription(Voucher.VoucherStatus)
                  Row = _row
                  IsNeededAddRow = False
                  _is_validated_ticket = True
                  Me.dg_entry_tickets.Cell(_row, GRID_INDEX).BackColor = Me.dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_NUMBER).BackColor
                  'Return
                End If
              End If
            Next

          Else

            For Each _ticket As CLASS_DROP_CONCILIATION.TYPE_VALIDATED_TICKETS In m_drop_conciliation.m_type_voucher.ValidatedTickets
              If ef_ticket.Value = _ticket.ValidationNumber Then
                If Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK Then
                  Voucher.IssuedValueAmount = _table_result.Rows(0).Item("TI_AMT0")
                  Voucher.IssuedIsoCode = _table_result.Rows(0).Item("TI_CUR0")
                  Voucher.ValueAmount = _table_result.Rows(0).Item("TI_AMOUNT")
                  Voucher.CreationDate = _table_result.Rows(0).Item("TI_CREATED_DATETIME")
                  _is_validated_ticket = True
                End If
              End If
            Next

          End If
        End If

        'm_drop_conciliation.m_type_voucher.ValidatedTickets


        If Row = -1 Then
          Row = dg_entry_tickets.NumRows
        End If

        _table_result = New DataTable()

        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = VoucherNumber
          _sql_cmd.Parameters.Add("@pMovementType", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL
          _sql_cmd.Parameters.Add("@pMovementTypeExchange", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, _table_result)

          End Using
          If IsNothing(_table_result) Then
            Return
          End If

          Voucher.OperationBarcode = ef_ticket.Value
          Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.NOT_EXISTS
          Voucher.StatusDescription = m_drop_conciliation.GetStatusDescription(Voucher.VoucherStatus, _other_conciliation)

          If _table_result.Rows.Count() < 1 Then
            Voucher.IsOverAmount = True
            Return
          End If

          _dr = _table_result.Rows(0)


          If Not IsDBNull(_dr.Item("CM_GAMING_TABLE_SESSION_ID")) Then
            Voucher.ReconciledInSessionId = _dr.Item("CM_GAMING_TABLE_SESSION_ID")
          End If

          If Not IsDBNull(_dr.Item("CM_OPERATION_ID")) Then
            Voucher.OperationId = _dr.Item("CM_OPERATION_ID")
          End If

          If Not IsDBNull(_dr.Item("CM_MOVEMENT_ID")) Then
            Voucher.CashierMovementId = _dr.Item("CM_MOVEMENT_ID")
          End If

          If Not IsDBNull(_dr.Item("ISSUED_AMOUNT")) Then
            Voucher.IssuedValueAmount = _dr.Item("ISSUED_AMOUNT")
          End If

          If Not IsDBNull(_dr.Item("CM_CURRENCY_ISO_CODE")) Then
            Voucher.IssuedIsoCode = _dr.Item("CM_CURRENCY_ISO_CODE")
          End If

          If Not IsDBNull(_dr.Item("CM_SUB_AMOUNT")) Then
            Voucher.ValueAmount = _dr.Item("CM_SUB_AMOUNT")
          End If

          If Not IsDBNull(_dr.Item("CM_DATE")) Then
            Voucher.CreationDate = _dr.Item("CM_DATE")
          End If

          Voucher.IsOverAmount = False

          '-- GamingTables.PlayerTracking: BuyIn.Mode 
          If GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation And Not _is_validated_ticket Then
            Voucher.IsOverAmount = True
            If m_is_enable_suggest_tickets Then
              Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK_OVER_AMOUNT
            Else
              Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK
            End If
          Else
            Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK
          End If

          If Not IsDBNull(_dr.Item("AO_UNDO_STATUS")) AndAlso _dr.Item("AO_UNDO_STATUS") > 0 Then
            Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.TICKET_CANCELLED
            Voucher.TickedStatus = TITO_TICKET_STATUS.DISCARDED
          ElseIf Not IsDBNull(_dr.Item("CM_GAMING_TABLE_SESSION_ID")) AndAlso _dr.Item("CM_GAMING_TABLE_SESSION_ID") > 0 _
              AndAlso _dr.Item("CM_GAMING_TABLE_SESSION_ID") <> Me.m_drop_conciliation.m_type_voucher.TableSessionId _
              AndAlso _is_validated_collected Then
            Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.REPEATED_IN_OTHER_RECONCILIATION
            '_other_conciliation = "(" & GUI_FormatDate(_dr.Item("CS_OPENING_DATE"), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM) & ")"
          End If

          If (Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK Or Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.OK_OVER_AMOUNT) _
            And IsNeededAddRow Then
            For _row As Integer = 0 To dg_entry_tickets.NumRows - 1
              If ef_ticket.Value.PadLeft(30, "0") = dg_entry_tickets.Cell(_row, GRID_COLUMN_TICKET_NUMBER).Value.Replace("-", "").PadLeft(30, "0") Then
                Voucher.VoucherStatus = CLASS_DROP_CONCILIATION.CONCILIATION_STATUS.REPEATED_IN_THIS_RECONCILIATION
              End If
            Next
          End If

          Voucher.StatusDescription = m_drop_conciliation.GetStatusDescription(Voucher.VoucherStatus, _other_conciliation)

        End Using
      End Using
    Catch ex As Exception
      Return
    End Try

  End Sub ' VoucherData

  ' PURPOSE: Manages barcode received events
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub BarcodeEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)
    Dim _data_read As WSI.Common.Barcode

    If Not Me.ef_ticket.Enabled Then
      Return
    End If

    Try
      If Not Data Is Nothing Then
        If e = KbdMsgEvent.EventNotValid Then
          Me.ef_ticket.TextValue = Data
        Else
          _data_read = DirectCast(Data, WSI.Common.Barcode)
          Me.ef_ticket.TextValue = _data_read.ExternalCode
        End If

        Call btn_entry_ClickEvent()

      End If
    Catch ex As Exception
    End Try

  End Sub ' BarcodeEvent

#End Region     'Privates

#Region "Events"

  Private Sub btn_entry_ClickEvent() Handles btn_entry.ClickEvent
    Dim _voucher_number As Int64
    Dim _voucher As CLASS_DROP_CONCILIATION.TYPE_RECONCILED_VOUCHER
    Dim _is_needed_add_row As Boolean
    Dim _row As Integer

    If ef_ticket.Value.Length() > 3 Then
      ef_ticket.Value = ef_ticket.Value.ToString().PadLeft(18, "0")
    End If

    _row = -1
    _voucher_number = WSI.Common.Barcode.GetOperationFromBarcode(ef_ticket.Value.ToString())

    If ef_ticket.Value.Length > 0 Then
      _voucher = New CLASS_DROP_CONCILIATION.TYPE_RECONCILED_VOUCHER()

      Call VoucherData(_voucher_number, _voucher, _row, _is_needed_add_row)

      If _voucher.OperationBarcode.Length > 0 Then
        m_drop_conciliation.m_type_voucher.Vouchers.Add(_voucher)
        If _is_needed_add_row Then
          Call AddTicketToGrid(_row, _voucher)
        End If
        Call AddTotalsOfGrid(_row, _voucher)
        Me.ef_drop.Value = GUI_FormatCurrency(CDec(Me.ef_collected.Value) + CDec(Me.ef_total_tickets.Value))
      End If

    End If

  End Sub ' btn_entry_ClickEvent

  Private Sub ef_ticket_EntryFieldValueChanged() Handles ef_ticket.EntryFieldValueChanged
    Dim _idx_char As Int32
    Dim _tmp_string As StringBuilder
    Dim _chr As String

    _tmp_string = New StringBuilder(ef_ticket.Value.Length)

    For _idx_char = 0 To ef_ticket.Value.Length - 1
      _chr = ef_ticket.Value(_idx_char)
      Select Case _chr
        Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
          _tmp_string.Append(_chr)
        Case Else
          Exit For
      End Select
    Next

    ef_ticket.Value = _tmp_string.ToString()

  End Sub ' ef_ticket_EntryFieldValueChanged

#End Region

End Class
