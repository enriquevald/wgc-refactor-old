﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_blacklist_import_type.vb
' DESCRIPTION:   Form to manage blacklist types 
' AUTHOR:        Sergi Martínez
' CREATION DATE: 06-MAY-2016
'
' REVISION HISTORY:
'
' Date         Author     Description
' -----------  ------     -----------------------------------------------
' 20-JAN-2016  FJC        Initial version. Product Backlog Item 12929:Visitas / Recepción: BlackList - Ampliar funcionalidad
' 20-MAY-2016  FJC        Fixed Bug 13386:Recepción - versión Junio. -Importación de listas negras. No se actualiza la caché-
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common
Imports WSI.Common.Entrances
Imports WSI.Common.Entrances.Enums



Public Class frm_blacklist_import_type
    Inherits frm_base_edit

#Region " Enums"
    Private Enum ROWACTION
        Editable = 2
        Added = 5
        Updated = 6
    End Enum
#End Region

#Region " Constants"

    ' Grid Columns
    Private Const GRID_COLUMNS As Integer = 5
    Private Const GRID_HEADER_ROWS As Integer = 1

    Private Const GRID_COLUMN_INDEX As Integer = 0
    Private Const GRID_COLUMN_NAME As Integer = 1
    Private Const GRID_COLUMN_MESSAGE As Integer = 2
    Private Const GRID_COLUMN_CHECK_ENTRANCE_ALLOWED As Integer = 3
    Private Const GRID_COLUMN_EDITABLE As Integer = 4 '0-No editable, 1-Editable, 8-Deleted

    ' Grid Columns WIDTH
    Private Const GRID_COLUMN_INDEX_WIDTH As Integer = 0
    Private Const GRID_COLUMN_NAME_WIDTH As Integer = 1500
    Private Const GRID_COLUMN_MESSAGE_WIDTH As Integer = 3200
    Private Const GRID_COLUMN_ENTRANCE_ALLOWED_WIDTH As Integer = 1700

#End Region

#Region " Memebers"
    Private m_dt_types As DataTable
    Private m_dict_types As Dictionary(Of Integer, String)

    Private m_index As Integer = 0
#End Region

#Region " Overrides "

    ''' <summary>
    ''' To control the key pressed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

        Select Case e.KeyChar
            Case Chr(Keys.Enter)

                If Me.dg_types.ContainsFocus Then

                    Me.dg_types.KeyPressed(sender, e)

                End If

                Return True
            Case Chr(Keys.Escape)

                Return MyBase.GUI_KeyPress(sender, e)
            Case Else
        End Select

        ' The keypress event is done in uc_grid control

    End Function ' GUI_KeyPress

    ''' <summary>
    ''' Establish Form Id, according to the enumeration in the application
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub GUI_SetFormId()
        Me.FormId = ENUM_FORM.FROM_BLACKLIST_IMPORT

        Call MyBase.GUI_SetFormId()

    End Sub 'GUI_SetFormId

    ''' <summary>
    ''' Set focus to a control when first entering the form
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_SetInitialFocus()
        Me.ActiveControl = Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL)
    End Sub      ' GUI_SetInitialFocus

    ''' <summary>
    ''' Initializes form controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_InitControls()
        ' Initialize parent control, required
        Call MyBase.GUI_InitControls()

        m_dict_types = New Dictionary(Of Integer, String)

        ' Enable / Disable controls
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True
        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)

        Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
        Me.btn_add.Enabled = Me.Permissions.Write
        Me.btn_del.Enabled = Me.Permissions.Write

        Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
        Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7125) + " - " + GLB_NLS_GUI_PLAYER_TRACKING.GetString(7304)

        Me.gb_types.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7304) ' Types

        Me.dg_types.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

        Call GUI_StyleSheet()

    End Sub ' GUI_InitControls

    ''' <summary>
    ''' Get data from the screen
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_GetScreenData()
        Dim _blacklist_import_type_edited As cls_blacklist_import_type
        Dim _row_select() As DataRow

        Try
            _blacklist_import_type_edited = Me.DbEditedObject
            For _idx_row As Integer = 0 To Me.dg_types.NumRows - 1
                _row_select = _blacklist_import_type_edited.BlackListImportType.Select(cls_blacklist_import_type.SQL_COLUMN_DESC_TYPE_ID & _
                                                                                              " = " & Me.dg_types.Cell(_idx_row, GRID_COLUMN_INDEX).Value)
                If _row_select.Length >= 1 Then
                    _row_select(_row_select.Length - 1)(cls_blacklist_import_type.SQL_COLUMN_NAME) = Me.dg_types.Cell(_idx_row, GRID_COLUMN_NAME).Value
                    _row_select(_row_select.Length - 1)(cls_blacklist_import_type.SQL_COLUMN_MESSAGE) = Me.dg_types.Cell(_idx_row, GRID_COLUMN_MESSAGE).Value
                    _row_select(_row_select.Length - 1)(cls_blacklist_import_type.SQL_COLUMN_ENTRANCE_ALLOWED) = (Me.dg_types.Cell(_idx_row, GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).Value = uc_grid.GRID_CHK_CHECKED)
                End If
            Next

        Catch ex As Exception
            Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)

        End Try

    End Sub ' GUI_GetScreenData

    ''' <summary>
    ''' Set initial data on the screen.
    ''' </summary>
    ''' <param name="SqlCtx"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
        Dim _blacklist_import_type As cls_blacklist_import_type

        Try
            _blacklist_import_type = Me.DbEditedObject
            SetDataGrid(_blacklist_import_type.BlackListImportType)

        Catch ex As Exception
            Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  Me.Name, _
                                  "GUI_SetScreenData", _
                                  ex.Message)

            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

        Finally

        End Try

    End Sub        ' GUI_SetScreenData

    ''' <summary>
    ''' Validate the data presented on the screen.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function GUI_IsScreenDataOk() As Boolean

        For _idx_row As Int32 = 0 To Me.dg_types.NumRows - 1

            'Check Name
            If String.IsNullOrEmpty(dg_types.Cell(_idx_row, GRID_COLUMN_NAME).Value.Trim) Then
                Call dg_types.Focus()

                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_CONTROLS.GetString(323))

                Return False
            End If

            'Check Message
            If String.IsNullOrEmpty(dg_types.Cell(_idx_row, GRID_COLUMN_MESSAGE).Value.Trim) Then
                Call dg_types.Focus()

                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428))

                Return False
            End If

        Next

        Return True
    End Function ' GUI_IsScreenDataOk

    ''' <summary>
    ''' Database overridable operations. Define specific DB operation for this form.
    ''' </summary>
    ''' <param name="DbOperation"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New cls_blacklist_import_type()

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = GUI_CommonOperations.CLASS_BASE.ENUM_STATUS.STATUS_DEPENDENCIES Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(7311), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Customers.RefreshBlackListCache(ENUM_BLACKLIST_SOURCE.EXTERNAL_FILE) ' REFRESH BlackList Cache
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Customers.RefreshBlackListCache(ENUM_BLACKLIST_SOURCE.EXTERNAL_FILE) ' REFRESH BlackList Cache
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

    End Sub         ' GUI_DB_Operation

    ''' <summary>
    ''' Process clicks on data grid (double-clicks) and select button
    ''' </summary>
    ''' <param name="ButtonId"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

        MyBase.GUI_ButtonClick(ButtonId)

    End Sub          ' GUI_ButtonClick

#End Region

#Region " Public Methods "

    ' PURPOSE: Opens dialog with selection mode settings
    '
    '  PARAMS:
    '     - INPUT:
    '           - CageSessionId
    '     - OUTPUT:
    '           - None
    '
    ' RETURNS:
    '     - None
    '
    Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
        Me.MdiParent = MdiParent
        Me.Display(False)
    End Sub

#End Region

#Region " Private Methods"

    ''' <summary>
    ''' Loads Data of Types to the Grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetDataGrid(ByVal BlackListImportType As DataTable)
        Dim _idx_row As Integer

        Try
            For Each _dt_row As DataRow In BlackListImportType.Rows
                Me.dg_types.AddRow()

                Me.dg_types.Cell(_idx_row, GRID_COLUMN_INDEX).Value = _dt_row(cls_blacklist_import_type.SQL_COLUMN_TYPE_ID)
                Me.dg_types.Cell(_idx_row, GRID_COLUMN_NAME).Value = _dt_row(cls_blacklist_import_type.SQL_COLUMN_NAME)
                Me.dg_types.Cell(_idx_row, GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).Value = IIf(_dt_row(cls_blacklist_import_type.SQL_COLUMN_ENTRANCE_ALLOWED), uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
                Me.dg_types.Cell(_idx_row, GRID_COLUMN_MESSAGE).Value = _dt_row(cls_blacklist_import_type.SQL_COLUMN_MESSAGE)

                _idx_row += 1
            Next
            If dg_types.NumRows > 0 Then
                Me.dg_types.IsSelected(0) = True
            End If

        Catch _ex As Exception

            WSI.Common.Log.Message("Error in BlackListImportType() ")
            WSI.Common.Log.Exception(_ex)
        End Try

    End Sub ' SetDataGridEntrancePrices

    ''' <summary>
    ''' Define layout of all Main Grid Columns 
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub GUI_StyleSheet()
        With Me.dg_types

            Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

            .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

            ' Index
            .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
            .Column(GRID_COLUMN_INDEX).Width = GRID_COLUMN_INDEX_WIDTH
            .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
            .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
            .Column(GRID_COLUMN_INDEX).Editable = False

            ' Name
            .Column(GRID_COLUMN_NAME).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(323)
            .Column(GRID_COLUMN_NAME).Width = GRID_COLUMN_NAME_WIDTH
            .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
            .Column(GRID_COLUMN_NAME).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_NAME).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
            .Column(GRID_COLUMN_NAME).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

            ' Message
            .Column(GRID_COLUMN_MESSAGE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1428)
            .Column(GRID_COLUMN_MESSAGE).Width = GRID_COLUMN_MESSAGE_WIDTH
            .Column(GRID_COLUMN_MESSAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
            .Column(GRID_COLUMN_MESSAGE).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_MESSAGE).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
            .Column(GRID_COLUMN_MESSAGE).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

            ' Entrance Allowed
            .Column(GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7303)
            .Column(GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).Width = GRID_COLUMN_ENTRANCE_ALLOWED_WIDTH
            .Column(GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
            .Column(GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).Editable = Me.Permissions.Write
            .Column(GRID_COLUMN_CHECK_ENTRANCE_ALLOWED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

            ' Editable
            .Column(GRID_COLUMN_EDITABLE).Header(0).Text = "  "
            .Column(GRID_COLUMN_EDITABLE).Header(1).Text = "  "
            .Column(GRID_COLUMN_EDITABLE).Width = 0

        End With

    End Sub ' GUI_StyleSheet

    ''' <summary>
    ''' Add to Grid new row (Type)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertType()
        Dim _row As Int32
        Dim _edit As cls_blacklist_import_type
        Dim _dt_row As DataRow

        If Not Permissions.Write Then
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

            Exit Sub
        End If

        m_index += -1

        With dg_types
            _row = .AddRow()

            .ClearSelection()
            .IsSelected(_row) = True
            .Redraw = True
            .TopRow = _row

            .Cell(_row, GRID_COLUMN_INDEX).Value = m_index
            .Cell(_row, GRID_COLUMN_MESSAGE).Value = String.Empty
            .Cell(_row, GRID_COLUMN_NAME).Value = String.Empty

            .Cell(_row, GRID_COLUMN_EDITABLE).Value = ROWACTION.Added
            .CurrentRow = _row
            .Focus()
        End With
        _edit = Me.DbEditedObject

        _dt_row = _edit.BlackListImportType.NewRow()
        _dt_row(cls_blacklist_import_type.SQL_COLUMN_TYPE_ID) = m_index
        _dt_row(cls_blacklist_import_type.SQL_COLUMN_NAME) = String.Empty
        _dt_row(cls_blacklist_import_type.SQL_COLUMN_MESSAGE) = String.Empty

        _edit.BlackListImportType.Rows.Add(_dt_row)

    End Sub            ' InsertType

    ''' <summary>
    ''' Check if can delete a row
    ''' </summary>
    ''' <param name="Type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EvaluateDelete(Optional ByVal Type As CageCurrencyType = -1) As Boolean
        Try
            If Not Permissions.Delete Then
                Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

                Return False

            End If

            If Not Me.dg_types.SelectedRows Is Nothing AndAlso _
               Me.dg_types.SelectedRows.Length > 0 Then

                DeleteSelectedRows(Me.dg_types)

                If Me.dg_types.NumRows > 0 Then

                    Me.dg_types.IsSelected(0) = True
                End If

            End If

            Return True

        Catch ex As Exception
            WSI.Common.Log.Message("Error in EvaluateDelete() ")
            WSI.Common.Log.Exception(ex)

        End Try

        Return False

    End Function 'EvaluateDelete

    ''' <summary>
    ''' Performs a delete action in the grid
    ''' </summary>
    ''' <param name="Grid"></param>
    ''' <remarks></remarks>
    Private Sub DeleteSelectedRows(ByVal Grid As GUI_Controls.uc_grid)
        Dim _blacklist_import_type As cls_blacklist_import_type
        Dim _blacklist_import_type_selected() As DataRow
        Dim _selected_rows() As Integer
        Dim _sql As String

        _selected_rows = Grid.SelectedRows

        If IsNothing(_selected_rows) Then
            Exit Sub
        End If

        _blacklist_import_type = Me.DbEditedObject

        For Each _selected_row As Integer In Grid.SelectedRows

            _sql = cls_blacklist_import_type.SQL_COLUMN_DESC_TYPE_ID & " = " & Grid.Cell(_selected_row, cls_blacklist_import_type.SQL_COLUMN_TYPE_ID).Value
            _blacklist_import_type_selected = _blacklist_import_type.BlackListImportType.Select(_sql)

            If _blacklist_import_type_selected.Length > 0 Then
                _blacklist_import_type_selected(0).Delete()
            End If

            Grid.IsSelected(_selected_row) = False
            Grid.DeleteRowFast(_selected_row)

            Call GUI_GetScreenData()

            Exit For

        Next

    End Sub ' DeleteSelectedRows


#End Region

#Region " Events"

    ''' <summary>
    ''' Button click Add Event
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
        InsertType()
    End Sub

    ''' <summary>
    ''' Button click Delete Event
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
        EvaluateDelete()
    End Sub

#End Region

End Class