<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_misc_sw_version
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.dg_list = New GUI_Controls.uc_grid
    Me.btn_exit = New GUI_Controls.uc_button
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_standby = New System.Windows.Forms.Label
    Me.tf_outdated = New GUI_Controls.uc_text_field
    Me.lbl_running = New System.Windows.Forms.Label
    Me.tf_to_date = New GUI_Controls.uc_text_field
    Me.tf_latest_cashier_build = New GUI_Controls.uc_text_field
    Me.gb_app_type = New System.Windows.Forms.GroupBox
    Me.flp_versions = New System.Windows.Forms.FlowLayoutPanel
    Me.tf_latest_gui_build = New GUI_Controls.uc_text_field
    Me.flp_app = New System.Windows.Forms.FlowLayoutPanel
    Me.opt_services = New System.Windows.Forms.RadioButton
    Me.opt_wigos_gui = New System.Windows.Forms.RadioButton
    Me.opt_win_cashier = New System.Windows.Forms.RadioButton
    Me.opt_all_types = New System.Windows.Forms.RadioButton
    Me.btn_clean = New GUI_Controls.uc_button
    Me.btn_excel = New GUI_Controls.uc_button
    Me.btn_print = New GUI_Controls.uc_button
    Me.gb_status.SuspendLayout()
    Me.gb_app_type.SuspendLayout()
    Me.flp_versions.SuspendLayout()
    Me.flp_app.SuspendLayout()
    Me.SuspendLayout()
    '
    'dg_list
    '
    Me.dg_list.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_list.CurrentCol = -1
    Me.dg_list.CurrentRow = -1
    Me.dg_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_list.Location = New System.Drawing.Point(9, 146)
    Me.dg_list.Name = "dg_list"
    Me.dg_list.PanelRightVisible = True
    Me.dg_list.Redraw = True
    Me.dg_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_list.Size = New System.Drawing.Size(940, 551)
    Me.dg_list.Sortable = False
    Me.dg_list.SortAscending = True
    Me.dg_list.SortByCol = 0
    Me.dg_list.TabIndex = 1
    Me.dg_list.ToolTipped = True
    Me.dg_list.TopRow = -2
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(947, 667)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 4
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Timer1
    '
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_standby)
    Me.gb_status.Controls.Add(Me.tf_outdated)
    Me.gb_status.Controls.Add(Me.lbl_running)
    Me.gb_status.Controls.Add(Me.tf_to_date)
    Me.gb_status.Location = New System.Drawing.Point(356, 12)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(123, 71)
    Me.gb_status.TabIndex = 109
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xUpdate Status"
    '
    'lbl_standby
    '
    Me.lbl_standby.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_standby.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_standby.Location = New System.Drawing.Point(10, 41)
    Me.lbl_standby.Name = "lbl_standby"
    Me.lbl_standby.Size = New System.Drawing.Size(16, 16)
    Me.lbl_standby.TabIndex = 107
    '
    'tf_outdated
    '
    Me.tf_outdated.IsReadOnly = True
    Me.tf_outdated.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_outdated.LabelForeColor = System.Drawing.Color.Black
    Me.tf_outdated.Location = New System.Drawing.Point(4, 37)
    Me.tf_outdated.Name = "tf_outdated"
    Me.tf_outdated.Size = New System.Drawing.Size(116, 24)
    Me.tf_outdated.SufixText = "Sufix Text"
    Me.tf_outdated.SufixTextVisible = True
    Me.tf_outdated.TabIndex = 108
    Me.tf_outdated.TabStop = False
    Me.tf_outdated.TextVisible = False
    Me.tf_outdated.TextWidth = 30
    Me.tf_outdated.Value = "xOutdated"
    '
    'lbl_running
    '
    Me.lbl_running.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_running.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_running.Location = New System.Drawing.Point(10, 19)
    Me.lbl_running.Name = "lbl_running"
    Me.lbl_running.Size = New System.Drawing.Size(16, 16)
    Me.lbl_running.TabIndex = 106
    '
    'tf_to_date
    '
    Me.tf_to_date.IsReadOnly = True
    Me.tf_to_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_to_date.LabelForeColor = System.Drawing.Color.Black
    Me.tf_to_date.Location = New System.Drawing.Point(4, 15)
    Me.tf_to_date.Name = "tf_to_date"
    Me.tf_to_date.Size = New System.Drawing.Size(114, 24)
    Me.tf_to_date.SufixText = "Sufix Text"
    Me.tf_to_date.SufixTextVisible = True
    Me.tf_to_date.TabIndex = 104
    Me.tf_to_date.TabStop = False
    Me.tf_to_date.TextVisible = False
    Me.tf_to_date.TextWidth = 30
    Me.tf_to_date.Value = "xTo Date"
    '
    'tf_latest_cashier_build
    '
    Me.tf_latest_cashier_build.IsReadOnly = True
    Me.tf_latest_cashier_build.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_latest_cashier_build.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_latest_cashier_build.Location = New System.Drawing.Point(0, 24)
    Me.tf_latest_cashier_build.Margin = New System.Windows.Forms.Padding(0)
    Me.tf_latest_cashier_build.Name = "tf_latest_cashier_build"
    Me.tf_latest_cashier_build.Size = New System.Drawing.Size(184, 24)
    Me.tf_latest_cashier_build.SufixText = "Sufix Text"
    Me.tf_latest_cashier_build.SufixTextVisible = True
    Me.tf_latest_cashier_build.TabIndex = 110
    Me.tf_latest_cashier_build.TabStop = False
    Me.tf_latest_cashier_build.TextWidth = 95
    Me.tf_latest_cashier_build.Value = "x99.999"
    '
    'gb_app_type
    '
    Me.gb_app_type.Controls.Add(Me.flp_versions)
    Me.gb_app_type.Controls.Add(Me.flp_app)
    Me.gb_app_type.Location = New System.Drawing.Point(9, 13)
    Me.gb_app_type.Name = "gb_app_type"
    Me.gb_app_type.Size = New System.Drawing.Size(343, 116)
    Me.gb_app_type.TabIndex = 111
    Me.gb_app_type.TabStop = False
    Me.gb_app_type.Text = "xApplication Type"
    '
    'flp_versions
    '
    Me.flp_versions.Controls.Add(Me.tf_latest_gui_build)
    Me.flp_versions.Controls.Add(Me.tf_latest_cashier_build)
    Me.flp_versions.Location = New System.Drawing.Point(150, 39)
    Me.flp_versions.Name = "flp_versions"
    Me.flp_versions.Size = New System.Drawing.Size(188, 52)
    Me.flp_versions.TabIndex = 113
    '
    'tf_latest_gui_build
    '
    Me.tf_latest_gui_build.IsReadOnly = True
    Me.tf_latest_gui_build.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_latest_gui_build.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_latest_gui_build.Location = New System.Drawing.Point(0, 0)
    Me.tf_latest_gui_build.Margin = New System.Windows.Forms.Padding(0)
    Me.tf_latest_gui_build.Name = "tf_latest_gui_build"
    Me.tf_latest_gui_build.Size = New System.Drawing.Size(184, 24)
    Me.tf_latest_gui_build.SufixText = "Sufix Text"
    Me.tf_latest_gui_build.SufixTextVisible = True
    Me.tf_latest_gui_build.TabIndex = 112
    Me.tf_latest_gui_build.TabStop = False
    Me.tf_latest_gui_build.TextWidth = 95
    Me.tf_latest_gui_build.Value = "x99.999"
    '
    'flp_app
    '
    Me.flp_app.Controls.Add(Me.opt_services)
    Me.flp_app.Controls.Add(Me.opt_wigos_gui)
    Me.flp_app.Controls.Add(Me.opt_win_cashier)
    Me.flp_app.Controls.Add(Me.opt_all_types)
    Me.flp_app.Location = New System.Drawing.Point(8, 16)
    Me.flp_app.Margin = New System.Windows.Forms.Padding(0)
    Me.flp_app.Name = "flp_app"
    Me.flp_app.Size = New System.Drawing.Size(139, 93)
    Me.flp_app.TabIndex = 113
    '
    'opt_services
    '
    Me.opt_services.Location = New System.Drawing.Point(0, 0)
    Me.opt_services.Margin = New System.Windows.Forms.Padding(0)
    Me.opt_services.Name = "opt_services"
    Me.opt_services.Size = New System.Drawing.Size(131, 24)
    Me.opt_services.TabIndex = 1
    Me.opt_services.TabStop = True
    Me.opt_services.Text = "xServices"
    '
    'opt_wigos_gui
    '
    Me.opt_wigos_gui.Location = New System.Drawing.Point(0, 24)
    Me.opt_wigos_gui.Margin = New System.Windows.Forms.Padding(0)
    Me.opt_wigos_gui.Name = "opt_wigos_gui"
    Me.opt_wigos_gui.Size = New System.Drawing.Size(138, 24)
    Me.opt_wigos_gui.TabIndex = 2
    Me.opt_wigos_gui.Text = "xWigos GUI"
    '
    'opt_win_cashier
    '
    Me.opt_win_cashier.Location = New System.Drawing.Point(0, 48)
    Me.opt_win_cashier.Margin = New System.Windows.Forms.Padding(0)
    Me.opt_win_cashier.Name = "opt_win_cashier"
    Me.opt_win_cashier.Size = New System.Drawing.Size(131, 24)
    Me.opt_win_cashier.TabIndex = 4
    Me.opt_win_cashier.Text = "xWIN Cashier"
    '
    'opt_all_types
    '
    Me.opt_all_types.Location = New System.Drawing.Point(0, 72)
    Me.opt_all_types.Margin = New System.Windows.Forms.Padding(0)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(131, 24)
    Me.opt_all_types.TabIndex = 3
    Me.opt_all_types.Text = "xAll Types"
    '
    'btn_clean
    '
    Me.btn_clean.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_clean.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_clean.Location = New System.Drawing.Point(947, 632)
    Me.btn_clean.Name = "btn_clean"
    Me.btn_clean.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_clean.Size = New System.Drawing.Size(90, 30)
    Me.btn_clean.TabIndex = 112
    Me.btn_clean.ToolTipped = False
    Me.btn_clean.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_excel
    '
    Me.btn_excel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_excel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_excel.Location = New System.Drawing.Point(947, 562)
    Me.btn_excel.Name = "btn_excel"
    Me.btn_excel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_excel.TabIndex = 113
    Me.btn_excel.ToolTipped = False
    Me.btn_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_print
    '
    Me.btn_print.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_print.Location = New System.Drawing.Point(947, 597)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_print.TabIndex = 114
    Me.btn_print.ToolTipped = False
    Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_misc_sw_version
    '
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(1044, 704)
    Me.Controls.Add(Me.btn_print)
    Me.Controls.Add(Me.btn_excel)
    Me.Controls.Add(Me.btn_clean)
    Me.Controls.Add(Me.gb_app_type)
    Me.Controls.Add(Me.gb_status)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.dg_list)
    Me.Name = "frm_misc_sw_version"
    Me.Text = "xfrm_misc_sw_version"
    Me.gb_status.ResumeLayout(False)
    Me.gb_app_type.ResumeLayout(False)
    Me.flp_versions.ResumeLayout(False)
    Me.flp_app.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_list As GUI_Controls.uc_grid

  Private Sub Grid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_list.Load

  End Sub
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_standby As System.Windows.Forms.Label
  Friend WithEvents tf_outdated As GUI_Controls.uc_text_field
  Friend WithEvents lbl_running As System.Windows.Forms.Label
  Friend WithEvents tf_to_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_latest_cashier_build As GUI_Controls.uc_text_field
  Friend WithEvents gb_app_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_win_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_services As System.Windows.Forms.RadioButton
  Friend WithEvents opt_wigos_gui As System.Windows.Forms.RadioButton
  Friend WithEvents tf_latest_gui_build As GUI_Controls.uc_text_field
  Friend WithEvents flp_app As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents flp_versions As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents btn_clean As GUI_Controls.uc_button
  Friend WithEvents btn_excel As GUI_Controls.uc_button
  Friend WithEvents btn_print As GUI_Controls.uc_button
End Class
