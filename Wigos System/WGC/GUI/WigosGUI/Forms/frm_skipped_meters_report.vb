'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tickets_report
' DESCRIPTION:   Tickets report
' AUTHOR:        David Lasdiez
' CREATION DATE: 14-NOV-2013
'
' REVISION HISTORY:
'
' Date         Author   Description
' -----------  ------   -----------------------------------------------
' 14-NOV-2013  DLL      Initial version
' 20-NOV-2013  DLL      SAS meters not included for this version
' 07-MAY-2014  DRV    Fixed Bug WIG-901: changed the row order
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 17-JUN-2015  FOS    Fixed Bug WIG-2465 : control unaligned (designer)
' 18-JUN-2015  FOS    Fixed Bug WIG-2467 : resize dates control (designer)
'----------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text.RegularExpressions
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL

Public Class frm_skipped_meters_report
  Inherits frm_base_sel

#Region " Constants "
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS As Int32

  ' SQL Columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_POVIDER_NAME As Integer = 2
  Private Const SQL_COLUMN_CODE As Integer = 3
  Private Const SQL_COLUMN_NAME As Integer = 4
  Private Const SQL_COLUMN_DESCRIPTION As Integer = 5
  Private Const SQL_COLUMN_DATETIME As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 7

  ' Grid Columns
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private GRID_COLUMNS_SEPARATE As Integer
  Private GRID_COLUMNS_GROUPED As Integer

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_DATETIME As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_GAME_NAME As Integer
  Private GRID_COLUMN_DESCRIPTION As Integer
  Private GRID_COLUMN_METER_CODE As Integer

  ' Separate mode
  Private GRID_COLUMN_SAS_METERS_CODE As Integer
  Private GRID_COLUMN_SAS_OLD_METERS As Integer
  Private GRID_COLUMN_SAS_NEW_METER As Integer 

  Private GRID_COLUMN_HPC_OLD_METER As Integer
  Private GRID_COLUMN_HPC_NEW_METER As Integer

  Private GRID_COLUMN_PWJ_OLD_PLAYED_COUNT As Integer 
  Private GRID_COLUMN_PWJ_OLD_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_PWJ_NEW_PLAYED_COUNT As Integer
  Private GRID_COLUMN_PWJ_NEW_PLAYED_AMOUNT As Integer
  Private GRID_COLUMN_PWJ_OLD_WON_COUNT As Integer 
  Private GRID_COLUMN_PWJ_OLD_WON_AMOUNT As Integer
  Private GRID_COLUMN_PWJ_NEW_WON_COUNT As Integer
  Private GRID_COLUMN_PWJ_NEW_WON_AMOUNT As Integer
  Private GRID_COLUMN_PWJ_OLD_JACKPOT As Integer
  Private GRID_COLUMN_PWJ_NEW_JACKPOT As Integer

  Private GRID_COLUMN_AFT_EFT_OLD_TOGM_QUANTITY As Integer
  Private GRID_COLUMN_AFT_EFT_OLD_TOGM_AMOUNT As Integer
  Private GRID_COLUMN_AFT_EFT_NEW_TOGM_QUANTITY As Integer
  Private GRID_COLUMN_AFT_EFT_NEW_TOGM_AMOUNT As Integer
  Private GRID_COLUMN_AFT_EFT_OLD_FROMGM_QUANTITY As Integer
  Private GRID_COLUMN_AFT_EFT_OLD_FROMGM_AMOUNT As Integer
  Private GRID_COLUMN_AFT_EFT_NEW_FROMGM_QUANTITY As Integer
  Private GRID_COLUMN_AFT_EFT_NEW_FROMGM_AMOUNT As Integer

  ' Grouped mode
  Private GRID_COLUMN_OLD_NEW As Integer
  Private GRID_COLUMN_GROUPED As Integer

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

  Private m_terminals As String

  'Private m_meters_head As String
  Private m_meters_list As String

  Private m_font_courier As Font

  Private m_change_color As Boolean

  Private m_refresh_grid As Boolean = False

  Public Class MeterValues
    Public m_description As String
    'MachineMeterBigIncrement(PlayedWon)
    'MachineMeterReset(PlayedWon)
    'GameMeterBigIncrement
    'GameMeterReset
    Public m_is_played_won As Boolean = False
    Public m_pwj_game_name As String
    Public m_pwj_old_played_count As String
    Public m_pwj_old_played_amount As String
    Public m_pwj_old_won_count As String
    Public m_pwj_old_won_amount As String
    Public m_pwj_old_jackpot As String
    Public m_pwj_new_played_count As String
    Public m_pwj_new_played_amount As String
    Public m_pwj_new_won_count As String
    Public m_pwj_new_won_amount As String
    Public m_pwj_new_jackpot As String

    Public m_sas_old_meter As String
    Public m_sas_new_meter As String
    Public m_sas_meter_code As String

    Public m_hpc_old_amount As String
    Public m_hpc_new_amount As String

    'MachineMeterBigIncrement(FundTransfer)
    'MachineMeterReset(FundTransfer)
    Public m_aft_old_togm_quantity As String
    Public m_aft_old_togm_amount As String
    Public m_aft_old_fromgm_quantity As String
    Public m_aft_old_fromgm_amount As String
    Public m_aft_new_togm_quantity As String
    Public m_aft_new_togm_amount As String
    Public m_aft_new_fromgm_quantity As String
    Public m_aft_new_fromgm_amount As String

    Public m_grouped_old As String
    Public m_grouped_new As String

  End Class

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SKIPPED_METERS_REPORT

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_font_courier = New Font("Courier New", 8)

    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2937)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_ALARMS.GetString(443)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Mode
    Me.gb_mode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(474)
    Me.opt_grouped.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2984)
    Me.opt_separate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2982)
    Me.opt_grouped.Checked = True

    ' Game Providers - Terminals
    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 0)
    _terminal_types(0) = TerminalTypes.SAS_HOST

    Call Me.uc_pr_list.Init(_terminal_types)

    ' Meters
    Me.uc_checked_list_meters.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Fill operators
    Call Me.uc_checked_list_meters.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, 4250)
    Call FillMetersFilterGrid()

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_sql As String

    _str_sql = ""

    _str_sql = "     SELECT   AL_ALARM_ID " & _
               "            , TE_NAME " & _
               "            , TE_PROVIDER_ID " & _
               "            , AL_ALARM_CODE " & _
               "            , AL_ALARM_NAME " & _
               "            , AL_ALARM_DESCRIPTION " & _
               "            , AL_DATETIME " & _
               "            , AL_SOURCE_ID " & _
               "       FROM   ALARMS " & _
               " INNER JOIN   TERMINALS ON AL_SOURCE_ID = TE_TERMINAL_ID"

    _str_sql &= GetSqlWhere()

    _str_sql &= " ORDER BY TE_PROVIDER_ID ASC, TE_NAME ASC, AL_DATETIME DESC"

    Return _str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'Dim _meters_values As 

    Dim _meter_values As New MeterValues()
    Dim _next_row As Integer
    Dim _terminal_data As List(Of Object)

    GetValuesString(DbRow.Value(SQL_COLUMN_DESCRIPTION), DbRow.Value(SQL_COLUMN_CODE), _meter_values)

    If Me.opt_grouped.Checked Then
      If String.IsNullOrEmpty(_meter_values.m_grouped_old) AndAlso String.IsNullOrEmpty(_meter_values.m_grouped_new) Then

        Return False
      End If
    End If

    _next_row = RowIndex + 1
    With Me.Grid

      'Add new row
      .AddRow()

      ' set row color
      If m_change_color Then
        .Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        .Row(_next_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      End If
      m_change_color = Not m_change_color

      ' Index Id
      .Cell(RowIndex, GRID_COLUMN_INDEX).Value = ""
      .Cell(_next_row, GRID_COLUMN_INDEX).Value = ""

      ' Generated datetime
      .Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      .Cell(_next_row, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)

      ' Terminal Report
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
          Me.Grid.Cell(_next_row, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next

      ' Game Name
      .Cell(RowIndex, GRID_COLUMN_GAME_NAME).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_game_name), _meter_values.m_pwj_game_name, "")
      .Cell(_next_row, GRID_COLUMN_GAME_NAME).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_game_name), _meter_values.m_pwj_game_name, "")

      ' Description
      .Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_description), _meter_values.m_description, "")
      .Cell(_next_row, GRID_COLUMN_DESCRIPTION).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_description), _meter_values.m_description, "")

      ' Meter Code
      .Cell(RowIndex, GRID_COLUMN_METER_CODE).Value = DbRow.Value(SQL_COLUMN_CODE)
      .Cell(_next_row, GRID_COLUMN_METER_CODE).Value = DbRow.Value(SQL_COLUMN_CODE)

      If Me.opt_grouped.Checked Then

        '.Row(RowIndex).Height *= 2

        .Cell(RowIndex, GRID_COLUMN_OLD_NEW).Value = "OLD"
        .Cell(RowIndex, GRID_COLUMN_GROUPED).Value = _meter_values.m_grouped_old
        .SetFont(RowIndex, GRID_COLUMN_GROUPED, m_font_courier)

        .Cell(_next_row, GRID_COLUMN_OLD_NEW).Value = "NEW"
        .Cell(_next_row, GRID_COLUMN_GROUPED).Value = _meter_values.m_grouped_new
        .SetFont(_next_row, GRID_COLUMN_GROUPED, m_font_courier)

        Return True
      End If

      ' SAS meters code
      '.Cell(RowIndex, GRID_COLUMN_SAS_METERS_CODE).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_sas_meter_code), _meter_values.m_sas_meter_code, "")

      ' SAS old meters
      '.Cell(RowIndex, GRID_COLUMN_SAS_OLD_METERS).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_sas_old_meter), _meter_values.m_sas_old_meter, "")

      ' SAS new meters
      '.Cell(RowIndex, GRID_COLUMN_SAS_NEW_METER).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_sas_new_meter), _meter_values.m_sas_new_meter, "")

      ' HPC old meter
      .Cell(RowIndex, GRID_COLUMN_HPC_OLD_METER).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_hpc_old_amount), _meter_values.m_hpc_old_amount, "")

      ' HPC new meter
      .Cell(RowIndex, GRID_COLUMN_HPC_NEW_METER).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_hpc_new_amount), _meter_values.m_hpc_new_amount, "")

      ' PWJ old played count
      .Cell(RowIndex, GRID_COLUMN_PWJ_OLD_PLAYED_COUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_old_played_count), _meter_values.m_pwj_old_played_count, "")

      ' PWJ old played amount
      .Cell(RowIndex, GRID_COLUMN_PWJ_OLD_PLAYED_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_old_played_amount), _meter_values.m_pwj_old_played_amount, "")

      ' PWJ old won count
      .Cell(RowIndex, GRID_COLUMN_PWJ_OLD_WON_COUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_old_won_count), _meter_values.m_pwj_old_won_count, "")

      ' PWJ old won amount
      .Cell(RowIndex, GRID_COLUMN_PWJ_OLD_WON_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_old_won_amount), _meter_values.m_pwj_old_won_amount, "")

      ' PW old jackpot
      .Cell(RowIndex, GRID_COLUMN_PWJ_OLD_JACKPOT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_old_jackpot), _meter_values.m_pwj_old_jackpot, "")

      ' PW new played count
      .Cell(RowIndex, GRID_COLUMN_PWJ_NEW_PLAYED_COUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_new_played_count), _meter_values.m_pwj_new_played_count, "")

      ' PW new played amount
      .Cell(RowIndex, GRID_COLUMN_PWJ_NEW_PLAYED_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_new_played_amount), _meter_values.m_pwj_new_played_amount, "")

      ' PW new won count
      .Cell(RowIndex, GRID_COLUMN_PWJ_NEW_WON_COUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_new_won_count), _meter_values.m_pwj_new_won_count, "")

      ' PW new won amount
      .Cell(RowIndex, GRID_COLUMN_PWJ_NEW_WON_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_new_won_amount), _meter_values.m_pwj_new_won_amount, "")

      ' PW new jackpot
      .Cell(RowIndex, GRID_COLUMN_PWJ_NEW_JACKPOT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_pwj_new_jackpot), _meter_values.m_pwj_new_jackpot, "")

      ' AFT EFT old togm quantity
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_OLD_TOGM_QUANTITY).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_old_togm_quantity), _meter_values.m_aft_old_togm_quantity, "")

      ' AFT EFT old togm amount
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_OLD_TOGM_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_old_togm_amount), _meter_values.m_aft_old_togm_amount, "")

      ' AFT EFT old fromgm quantity
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_OLD_FROMGM_QUANTITY).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_old_fromgm_quantity), _meter_values.m_aft_old_fromgm_quantity, "")

      ' AFT EFT old fromgm amount
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_OLD_FROMGM_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_old_fromgm_amount), _meter_values.m_aft_old_fromgm_amount, "")

      ' AFT EFT new togm quantity
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_NEW_TOGM_QUANTITY).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_new_togm_quantity), _meter_values.m_aft_new_togm_quantity, "")

      ' AFT EFT new togm amount
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_NEW_TOGM_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_new_togm_amount), _meter_values.m_aft_new_togm_amount, "")

      ' AFT EFT new fromgm quantity
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_NEW_FROMGM_QUANTITY).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_new_fromgm_quantity), _meter_values.m_aft_new_fromgm_quantity, "")

      '  AFT EFT new fromgm amount
      .Cell(RowIndex, GRID_COLUMN_AFT_EFT_NEW_FROMGM_AMOUNT).Value = IIf(Not String.IsNullOrEmpty(_meter_values.m_aft_new_fromgm_amount), _meter_values.m_aft_new_fromgm_amount, "")

    End With

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_change_color = False

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub 'GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642), m_meters_list)
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim all_checked As Boolean
    m_date_from = ""
    m_date_to = ""
    m_terminals = ""

    'Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_pr_list.GetTerminalReportText()

    all_checked = True

    If uc_checked_list_meters.SelectedIndexes.Length <> uc_checked_list_meters.Count() Then
      all_checked = False
    End If

    If all_checked = True Then
      m_meters_list = GLB_NLS_GUI_AUDITOR.GetString(263)
    Else
      m_meters_list = uc_checked_list_meters.SelectedValuesList()
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      If Me.opt_separate.Checked Then
        Call .Init(GRID_COLUMNS_SEPARATE, GRID_HEADER_ROWS)
        .Sortable = True
      Else
        Call .Init(GRID_COLUMNS_GROUPED, GRID_HEADER_ROWS)
        .Sortable = False
      End If

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Datetime
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = ""
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(443)
      .Column(GRID_COLUMN_DATETIME).Width = 2100
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(442)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      .Column(GRID_COLUMN_GAME_NAME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(442)
      .Column(GRID_COLUMN_GAME_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008)
      .Column(GRID_COLUMN_GAME_NAME).Width = 2000

      .Column(GRID_COLUMN_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(269)
      .Column(GRID_COLUMN_DESCRIPTION).Width = 3500

      .Column(GRID_COLUMN_METER_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_METER_CODE).Header(1).Text = "METER CODE"
      .Column(GRID_COLUMN_METER_CODE).Width = 0

      '.Clear()
      If Me.opt_grouped.Checked Then
        .Column(GRID_COLUMN_OLD_NEW).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
        .Column(GRID_COLUMN_OLD_NEW).Header(1).Text = ""
        .Column(GRID_COLUMN_OLD_NEW).Width = 550

        .Column(GRID_COLUMN_GROUPED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
        .Column(GRID_COLUMN_GROUPED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2981)
        .Column(GRID_COLUMN_GROUPED).Width = 8500

      Else
        .Column(GRID_COLUMN_SAS_METERS_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096)
        .Column(GRID_COLUMN_SAS_METERS_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2956)
        .Column(GRID_COLUMN_SAS_METERS_CODE).Width = 0
        .Column(GRID_COLUMN_SAS_METERS_CODE).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SAS_METERS_CODE).IsColumnPrintable = False

        .Column(GRID_COLUMN_SAS_OLD_METERS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096)
        .Column(GRID_COLUMN_SAS_OLD_METERS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2957)
        .Column(GRID_COLUMN_SAS_OLD_METERS).Width = 0
        .Column(GRID_COLUMN_SAS_OLD_METERS).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SAS_OLD_METERS).IsColumnPrintable = False

        .Column(GRID_COLUMN_SAS_NEW_METER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2096)
        .Column(GRID_COLUMN_SAS_NEW_METER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2958)
        .Column(GRID_COLUMN_SAS_NEW_METER).Width = 0
        .Column(GRID_COLUMN_SAS_NEW_METER).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SAS_NEW_METER).IsColumnPrintable = False

        .Column(GRID_COLUMN_HPC_OLD_METER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2959)
        .Column(GRID_COLUMN_HPC_OLD_METER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2960)
        .Column(GRID_COLUMN_HPC_OLD_METER).Width = 1500

        .Column(GRID_COLUMN_HPC_NEW_METER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2959)
        .Column(GRID_COLUMN_HPC_NEW_METER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2961)
        .Column(GRID_COLUMN_HPC_NEW_METER).Width = 1500

        .Column(GRID_COLUMN_PWJ_OLD_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2962)
        .Column(GRID_COLUMN_PWJ_OLD_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2963)
        .Column(GRID_COLUMN_PWJ_OLD_PLAYED_COUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_OLD_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2962)
        .Column(GRID_COLUMN_PWJ_OLD_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2964)
        .Column(GRID_COLUMN_PWJ_OLD_PLAYED_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_NEW_PLAYED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2962)
        .Column(GRID_COLUMN_PWJ_NEW_PLAYED_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2965)
        .Column(GRID_COLUMN_PWJ_NEW_PLAYED_COUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_NEW_PLAYED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2962)
        .Column(GRID_COLUMN_PWJ_NEW_PLAYED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2966)
        .Column(GRID_COLUMN_PWJ_NEW_PLAYED_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_OLD_WON_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2967)
        .Column(GRID_COLUMN_PWJ_OLD_WON_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2963)
        .Column(GRID_COLUMN_PWJ_OLD_WON_COUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_OLD_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2967)
        .Column(GRID_COLUMN_PWJ_OLD_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2964)
        .Column(GRID_COLUMN_PWJ_OLD_WON_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_NEW_WON_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2967)
        .Column(GRID_COLUMN_PWJ_NEW_WON_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2965)
        .Column(GRID_COLUMN_PWJ_NEW_WON_COUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_NEW_WON_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2967)
        .Column(GRID_COLUMN_PWJ_NEW_WON_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2966)
        .Column(GRID_COLUMN_PWJ_NEW_WON_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_PWJ_OLD_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2968)
        .Column(GRID_COLUMN_PWJ_OLD_JACKPOT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2964)
        .Column(GRID_COLUMN_PWJ_OLD_JACKPOT).Width = 1500

        .Column(GRID_COLUMN_PWJ_NEW_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2968)
        .Column(GRID_COLUMN_PWJ_NEW_JACKPOT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2966)
        .Column(GRID_COLUMN_PWJ_NEW_JACKPOT).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_OLD_TOGM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2969)
        .Column(GRID_COLUMN_AFT_EFT_OLD_TOGM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2970)
        .Column(GRID_COLUMN_AFT_EFT_OLD_TOGM_QUANTITY).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_OLD_TOGM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2969)
        .Column(GRID_COLUMN_AFT_EFT_OLD_TOGM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2964)
        .Column(GRID_COLUMN_AFT_EFT_OLD_TOGM_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_NEW_TOGM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2969)
        .Column(GRID_COLUMN_AFT_EFT_NEW_TOGM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2971)
        .Column(GRID_COLUMN_AFT_EFT_NEW_TOGM_QUANTITY).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_NEW_TOGM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2969)
        .Column(GRID_COLUMN_AFT_EFT_NEW_TOGM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2966)
        .Column(GRID_COLUMN_AFT_EFT_NEW_TOGM_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_OLD_FROMGM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2972)
        .Column(GRID_COLUMN_AFT_EFT_OLD_FROMGM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2970)
        .Column(GRID_COLUMN_AFT_EFT_OLD_FROMGM_QUANTITY).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_OLD_FROMGM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2972)
        .Column(GRID_COLUMN_AFT_EFT_OLD_FROMGM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2964)
        .Column(GRID_COLUMN_AFT_EFT_OLD_FROMGM_AMOUNT).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_NEW_FROMGM_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2972)
        .Column(GRID_COLUMN_AFT_EFT_NEW_FROMGM_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2971)
        .Column(GRID_COLUMN_AFT_EFT_NEW_FROMGM_QUANTITY).Width = 1500

        .Column(GRID_COLUMN_AFT_EFT_NEW_FROMGM_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2972)
        .Column(GRID_COLUMN_AFT_EFT_NEW_FROMGM_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2966)
        .Column(GRID_COLUMN_AFT_EFT_NEW_FROMGM_AMOUNT).Width = 1500

      End If
    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.dtp_from.Value = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0)

    Me.dtp_from.Checked = True

    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.uc_pr_list.Width = 500

    Me.opt_grouped.Checked = True

    Me.chk_terminal_location.Checked = False

    Call Me.uc_pr_list.SetDefaultValues()

    Call Me.uc_checked_list_meters.SetDefaultValue(True)

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _source_code As String = ""
    Dim _alarm_code_list As String = ""

    ' Filter Dates
    If Me.dtp_from.Checked = True Then
      _str_where &= " AND AL_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & " "
    End If

    If Me.dtp_to.Checked = True Then
      _str_where &= " AND AL_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & " "
    End If

    _str_where &= " AND AL_ALARM_CODE IN (" & Me.uc_checked_list_meters.SelectedIndexesList & " )"

    If Not Me.uc_pr_list.AllSelectedChecked Then
      _str_where &= " AND  AL_SOURCE_ID IN (" & Me.uc_pr_list.GetProviderIdListSelected() & " ) "
    End If

    If _str_where <> "" Then
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where

  End Function ' GetSqlWhere

  Private Sub GetValuesString(ByVal Meters As String, ByVal TypeMeter As Integer, ByRef MeterValue As MeterValues)

    Dim _expresion As String
    Dim _match As Match
    Dim _ex_old_meter As String
    Dim _ex_new_meter As String
    Dim _ex_hpc_old_meter As String
    Dim _ex_hpc_new_meter As String
    Dim _ex_game As String
    Dim _ex_played_won As String
    Dim _ex_jackpot As String
    Dim _ex_aft_eft As String

    _ex_old_meter = "Old meter values:\s+"
    _ex_new_meter = "New meter values:\s+"
    _ex_game = "Game: ([^\n]+)\s+[^\n]+\s+"
    _ex_played_won = "Played Count/Cents: (\d+) / (\d+)\s+Won Count/Cents: (\d+) / (\d+)\s+"
    _ex_jackpot = "Jackpot Cents: (\d+)\s+"
    _ex_aft_eft = "ToGm Quantity/Cents: (\d+) / (\d+)\s+FromGm Quantity/Cents: (\d+) / (\d+)\s+"
    _expresion = ""

    Select Case TypeMeter
      Case AlarmCode.TerminalSystem_MachineMeterBigIncrement
        _expresion &= _ex_old_meter
        If Meters.Contains("PlayedWon") Then
          _expresion &= _ex_played_won & _ex_new_meter & _ex_played_won
        Else
          _expresion &= _ex_aft_eft & _ex_new_meter & _ex_aft_eft
        End If

      Case AlarmCode.TerminalSystem_GameMeterBigIncrement, _
           AlarmCode.TerminalSystem_GameMeterReset
        _expresion &= _ex_old_meter & _ex_game & _ex_played_won & _ex_jackpot & _
                      _ex_new_meter & _ex_game & _ex_played_won & _ex_jackpot

      Case AlarmCode.TerminalSystem_HPCMeterBigIncrement
        _ex_hpc_old_meter = "Old hpc meter value - Handpay Cents: (\d+)"
        _ex_hpc_new_meter = "New hpc meter value - Handpay Cents: (\d+)"
        _expresion &= _ex_hpc_old_meter & _ex_hpc_new_meter

      Case AlarmCode.TerminalSystem_SasMeterBigIncrement
        _expresion &= "Meter: ([^\n]+)\s+Old meter value: (\d+)\s+New meter value: (\d+)\s+"

      Case AlarmCode.TerminalSystem_MachineMeterReset
        _expresion &= _ex_old_meter
        If Meters.Contains("PlayedWon") Then
          _expresion &= _ex_played_won & _ex_jackpot
        Else
          _expresion &= _ex_aft_eft
        End If

      Case AlarmCode.TerminalSystem_SasMeterReset
        _expresion &= "Meter: ([^\n]+)\s+Old meter value: (\d+)\s+"

      Case Else
        _expresion &= "*"
    End Select

    _match = Regex.Match(Meters, _expresion, RegexOptions.IgnoreCase)

    If _match.Success Then
      ' _match.Groups(0).Value is the regular expression
      ' When there is a reset, all new values goes with 0
      Select Case TypeMeter
        Case AlarmCode.TerminalSystem_MachineMeterBigIncrement
          If Meters.Contains("PlayedWon") Then
            MeterValue.m_is_played_won = True
            MeterValue.m_pwj_old_played_count = GUI_FormatNumber(_match.Groups(1).Value, 0)
            MeterValue.m_pwj_old_played_amount = GUI_FormatCurrency(_match.Groups(2).Value / 100, 2)
            MeterValue.m_pwj_old_won_count = GUI_FormatNumber(_match.Groups(3).Value, 0)
            MeterValue.m_pwj_old_won_amount = GUI_FormatCurrency(_match.Groups(4).Value / 100, 2)
            MeterValue.m_pwj_new_played_count = GUI_FormatNumber(_match.Groups(5).Value, 0)
            MeterValue.m_pwj_new_played_amount = GUI_FormatCurrency(_match.Groups(6).Value / 100, 2)
            MeterValue.m_pwj_new_won_count = GUI_FormatNumber(_match.Groups(7).Value, 0)
            MeterValue.m_pwj_new_won_amount = GUI_FormatCurrency(_match.Groups(8).Value / 100, 2)
          Else
            MeterValue.m_aft_old_togm_quantity = GUI_FormatNumber(_match.Groups(1).Value, 0)
            MeterValue.m_aft_old_togm_amount = GUI_FormatCurrency(_match.Groups(2).Value / 100, 2)
            MeterValue.m_aft_old_fromgm_quantity = GUI_FormatNumber(_match.Groups(3).Value, 0)
            MeterValue.m_aft_old_fromgm_amount = GUI_FormatCurrency(_match.Groups(4).Value / 100, 2)
            MeterValue.m_aft_new_togm_quantity = GUI_FormatNumber(_match.Groups(5).Value, 0)
            MeterValue.m_aft_new_togm_amount = GUI_FormatCurrency(_match.Groups(6).Value / 100, 2)
            MeterValue.m_aft_new_fromgm_quantity = GUI_FormatNumber(_match.Groups(7).Value, 0)
            MeterValue.m_aft_new_fromgm_amount = GUI_FormatCurrency(_match.Groups(8).Value / 100, 2)
          End If

        Case AlarmCode.TerminalSystem_GameMeterReset, _
             AlarmCode.TerminalSystem_GameMeterBigIncrement
          MeterValue.m_is_played_won = True
          MeterValue.m_pwj_game_name = _match.Groups(1).Value
          MeterValue.m_pwj_old_played_count = GUI_FormatNumber(_match.Groups(2).Value, 0)
          MeterValue.m_pwj_old_played_amount = GUI_FormatCurrency(_match.Groups(3).Value / 100, 2)
          MeterValue.m_pwj_old_won_count = GUI_FormatNumber(_match.Groups(4).Value, 0)
          MeterValue.m_pwj_old_won_amount = GUI_FormatCurrency(_match.Groups(5).Value / 100, 2)
          MeterValue.m_pwj_old_jackpot = GUI_FormatCurrency(_match.Groups(6).Value, 2)
          ' _match.Groups(7).Value is game name
          MeterValue.m_pwj_new_played_count = GUI_FormatNumber(_match.Groups(8).Value, 0)
          MeterValue.m_pwj_new_played_amount = GUI_FormatCurrency(_match.Groups(9).Value / 100, 2)
          MeterValue.m_pwj_new_won_count = GUI_FormatNumber(_match.Groups(10).Value, 0)
          MeterValue.m_pwj_new_won_amount = GUI_FormatCurrency(_match.Groups(11).Value / 100, 2)
          MeterValue.m_pwj_new_jackpot = GUI_FormatCurrency(_match.Groups(12).Value, 2)

        Case AlarmCode.TerminalSystem_HPCMeterBigIncrement
          MeterValue.m_hpc_old_amount = GUI_FormatCurrency(_match.Groups(1).Value / 100, 2)
          MeterValue.m_hpc_new_amount = GUI_FormatCurrency(_match.Groups(2).Value / 100, 2)

        Case AlarmCode.TerminalSystem_SasMeterBigIncrement
          MeterValue.m_sas_meter_code = _match.Groups(1).Value
          MeterValue.m_sas_old_meter = GUI_FormatNumber(_match.Groups(2).Value, 0)
          MeterValue.m_sas_new_meter = GUI_FormatNumber(_match.Groups(3).Value, 0)

        Case AlarmCode.TerminalSystem_MachineMeterReset
          If Meters.Contains("PlayedWon") Then
            MeterValue.m_is_played_won = True
            MeterValue.m_pwj_old_played_count = GUI_FormatNumber(_match.Groups(1).Value, 0)
            MeterValue.m_pwj_old_played_amount = GUI_FormatCurrency(_match.Groups(2).Value / 100, 2)
            MeterValue.m_pwj_old_won_count = GUI_FormatNumber(_match.Groups(3).Value, 0)
            MeterValue.m_pwj_old_won_amount = GUI_FormatCurrency(_match.Groups(4).Value / 100, 2)
            MeterValue.m_pwj_old_jackpot = GUI_FormatCurrency(_match.Groups(5).Value, 2)

            MeterValue.m_pwj_new_played_count = GUI_FormatNumber(0, 0)
            MeterValue.m_pwj_new_played_amount = GUI_FormatCurrency(0, 2)
            MeterValue.m_pwj_new_won_count = GUI_FormatNumber(0, 0)
            MeterValue.m_pwj_new_won_amount = GUI_FormatCurrency(0, 2)

          Else
            MeterValue.m_aft_old_togm_quantity = GUI_FormatNumber(_match.Groups(1).Value, 0)
            MeterValue.m_aft_old_togm_amount = GUI_FormatCurrency(_match.Groups(2).Value / 100, 2)
            MeterValue.m_aft_old_fromgm_quantity = GUI_FormatNumber(_match.Groups(3).Value, 0)
            MeterValue.m_aft_old_fromgm_amount = GUI_FormatCurrency(_match.Groups(4).Value / 100, 2)

            MeterValue.m_aft_new_togm_quantity = GUI_FormatNumber(0, 0)
            MeterValue.m_aft_new_togm_amount = GUI_FormatCurrency(0, 2)
            MeterValue.m_aft_new_fromgm_quantity = GUI_FormatNumber(0, 0)
            MeterValue.m_aft_new_fromgm_amount = GUI_FormatCurrency(0, 2)
          End If

        Case AlarmCode.TerminalSystem_SasMeterReset
          MeterValue.m_sas_meter_code = _match.Groups(1).Value
          MeterValue.m_sas_old_meter = GUI_FormatNumber(_match.Groups(2).Value, 0)
          MeterValue.m_sas_new_meter = GUI_FormatNumber(0, 0)

      End Select

      Call SetGroupedValues(TypeMeter, MeterValue)
    End If

    Select Case TypeMeter
      Case AlarmCode.TerminalSystem_MachineMeterBigIncrement
        MeterValue.m_description = "Machine Big Increment"
        If Meters.Contains("PlayedWon") Then
          MeterValue.m_description &= " (PlayedWon)"
        Else
          MeterValue.m_description &= " (AFT/EFT)"
        End If

      Case AlarmCode.TerminalSystem_GameMeterReset
        MeterValue.m_description = "Game Reset"

      Case AlarmCode.TerminalSystem_GameMeterBigIncrement
        MeterValue.m_description = "Game Big Increment"

      Case AlarmCode.TerminalSystem_HPCMeterBigIncrement
        MeterValue.m_description = "HPC Big Increment"

      Case AlarmCode.TerminalSystem_SasMeterBigIncrement
        MeterValue.m_description = "SAS Big Increment"

      Case AlarmCode.TerminalSystem_MachineMeterReset
        MeterValue.m_description = "Machine Reset"
        If Meters.Contains("PlayedWon") Then
          MeterValue.m_description &= " (PlayedWon)"
        Else
          MeterValue.m_description &= " (AFT/EFT)"
        End If

      Case AlarmCode.TerminalSystem_SasMeterReset
        MeterValue.m_description = "SAS Big Increment"

      Case Else
        MeterValue.m_description = ""

    End Select
  End Sub ' GetValuesString

  Private Sub SetGroupedValues(ByVal TypeMeter As Integer, ByRef MeterValue As MeterValues)

    Dim _count_value_zero As String = GUI_FormatNumber(0, 0)
    Dim _amount_value_zero As String = GUI_FormatCurrency(0, 2)
    Dim _played_won_jackpot_all_zero As Boolean
    Dim _from_to_egm_all_zero As Boolean

    MeterValue.m_grouped_old = ""
    MeterValue.m_grouped_new = ""

    Select Case TypeMeter
      Case AlarmCode.TerminalSystem_HPCMeterBigIncrement
        If Not String.IsNullOrEmpty(MeterValue.m_hpc_old_amount) AndAlso Not String.IsNullOrEmpty(MeterValue.m_hpc_new_amount) Then
          MeterValue.m_grouped_old += "   HPC: " + MeterValue.m_hpc_old_amount.PadLeft(10) + " "
          MeterValue.m_grouped_new += "   HPC: " + MeterValue.m_hpc_new_amount.PadLeft(10) + " "
        End If

      Case AlarmCode.TerminalSystem_GameMeterReset, _
           AlarmCode.TerminalSystem_GameMeterBigIncrement, _
           AlarmCode.TerminalSystem_MachineMeterBigIncrement, _
           AlarmCode.TerminalSystem_MachineMeterReset

        _played_won_jackpot_all_zero = True

        If MeterValue.m_is_played_won Then
          If Not String.IsNullOrEmpty(MeterValue.m_pwj_old_played_count) AndAlso Not String.IsNullOrEmpty(MeterValue.m_pwj_old_played_amount) AndAlso _
             Not String.IsNullOrEmpty(MeterValue.m_pwj_new_played_count) AndAlso Not String.IsNullOrEmpty(MeterValue.m_pwj_new_played_amount) Then
            MeterValue.m_grouped_old += "Played: " + MeterValue.m_pwj_old_played_count.PadLeft(8) + "/" + MeterValue.m_pwj_old_played_amount.PadLeft(10) + " "
            MeterValue.m_grouped_new += "Played: " + MeterValue.m_pwj_new_played_count.PadLeft(8) + "/" + MeterValue.m_pwj_new_played_amount.PadLeft(10) + " "

            _played_won_jackpot_all_zero = False
          Else
            MeterValue.m_grouped_old += "Played: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
            MeterValue.m_grouped_new += "Played: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
          End If

          If Not String.IsNullOrEmpty(MeterValue.m_pwj_old_won_count) AndAlso Not String.IsNullOrEmpty(MeterValue.m_pwj_old_won_amount) AndAlso _
             Not String.IsNullOrEmpty(MeterValue.m_pwj_new_won_count) AndAlso Not String.IsNullOrEmpty(MeterValue.m_pwj_new_won_amount) Then
            MeterValue.m_grouped_old += "     Won: " + MeterValue.m_pwj_old_won_count.PadLeft(8) + "/" + MeterValue.m_pwj_old_won_amount.PadLeft(10) + " "
            MeterValue.m_grouped_new += "     Won: " + MeterValue.m_pwj_new_won_count.PadLeft(8) + "/" + MeterValue.m_pwj_new_won_amount.PadLeft(10) + " "

            _played_won_jackpot_all_zero = False
          Else
            MeterValue.m_grouped_old += "     Won: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
            MeterValue.m_grouped_new += "     Won: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
          End If

          If Not String.IsNullOrEmpty(MeterValue.m_pwj_old_jackpot) AndAlso Not String.IsNullOrEmpty(MeterValue.m_pwj_new_jackpot) Then
            MeterValue.m_grouped_old += " Jackpot: " + MeterValue.m_pwj_old_jackpot.PadLeft(10) + " "
            MeterValue.m_grouped_new += " Jackpot: " + MeterValue.m_pwj_new_jackpot.PadLeft(10) + " "

            _played_won_jackpot_all_zero = False
          Else
            MeterValue.m_grouped_old += " Jackpot: " + _amount_value_zero.PadLeft(10) + " "
            MeterValue.m_grouped_new += " Jackpot: " + _amount_value_zero.PadLeft(10) + " "
          End If

          If _played_won_jackpot_all_zero Then
            MeterValue.m_grouped_old = ""
            MeterValue.m_grouped_new = ""
          End If
        Else
          _from_to_egm_all_zero = True

          If Not String.IsNullOrEmpty(MeterValue.m_aft_old_togm_quantity) AndAlso Not String.IsNullOrEmpty(MeterValue.m_aft_old_togm_amount) AndAlso _
             Not String.IsNullOrEmpty(MeterValue.m_aft_new_togm_quantity) AndAlso Not String.IsNullOrEmpty(MeterValue.m_aft_new_togm_amount) Then

            MeterValue.m_grouped_old += "To EGM: " + MeterValue.m_aft_old_togm_quantity.PadLeft(8) + "/" + MeterValue.m_aft_old_togm_amount.PadLeft(10) + " "
            MeterValue.m_grouped_new += "To EGM: " + MeterValue.m_aft_new_togm_quantity.PadLeft(8) + "/" + MeterValue.m_aft_new_togm_amount.PadLeft(10) + " "

            _from_to_egm_all_zero = False
          Else
            MeterValue.m_grouped_old += "To EGM: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
            MeterValue.m_grouped_new += "To EGM: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
          End If

          If Not String.IsNullOrEmpty(MeterValue.m_aft_old_fromgm_quantity) AndAlso Not String.IsNullOrEmpty(MeterValue.m_aft_old_fromgm_amount) AndAlso _
             Not String.IsNullOrEmpty(MeterValue.m_aft_new_fromgm_quantity) AndAlso Not String.IsNullOrEmpty(MeterValue.m_aft_new_fromgm_amount) Then
            MeterValue.m_grouped_old += "From EGM: " + MeterValue.m_aft_old_fromgm_quantity.PadLeft(8) + "/" + MeterValue.m_aft_old_fromgm_amount.PadLeft(10) + " "
            MeterValue.m_grouped_new += "From EGM: " + MeterValue.m_aft_new_fromgm_quantity.PadLeft(8) + "/" + MeterValue.m_aft_new_fromgm_amount.PadLeft(10) + " "

            _from_to_egm_all_zero = False
          Else
            MeterValue.m_grouped_old += "From EGM: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
            MeterValue.m_grouped_new += "From EGM: " + _count_value_zero.PadLeft(8) + "/" + _amount_value_zero.PadLeft(10) + " "
          End If

          If _from_to_egm_all_zero Then
            MeterValue.m_grouped_old = ""
            MeterValue.m_grouped_new = ""
          End If

        End If

      Case AlarmCode.TerminalSystem_SasMeterReset
      Case AlarmCode.TerminalSystem_SasMeterBigIncrement

    End Select

  End Sub 'SetGroupedValues

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillMetersFilterGrid()

    Call Me.uc_checked_list_meters.Clear()
    Call Me.uc_checked_list_meters.ReDraw(False)

    'uc_checked_list_meters.Add(AlarmCode.TerminalSystem_SasMeterBigIncrement, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2974))
    'uc_checked_list_meters.Add(AlarmCode.TerminalSystem_SasMeterReset, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2975))
    uc_checked_list_meters.Add(AlarmCode.TerminalSystem_HPCMeterBigIncrement, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2976))
    uc_checked_list_meters.Add(AlarmCode.TerminalSystem_MachineMeterBigIncrement, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2977))
    uc_checked_list_meters.Add(AlarmCode.TerminalSystem_MachineMeterReset, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2978))
    uc_checked_list_meters.Add(AlarmCode.TerminalSystem_GameMeterBigIncrement, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2979))
    uc_checked_list_meters.Add(AlarmCode.TerminalSystem_GameMeterReset, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2980))

    If Me.uc_checked_list_meters.Count() > 0 Then
      Call Me.uc_checked_list_meters.CurrentRow(0)
    End If

    Call Me.uc_checked_list_meters.ReDraw(True)

  End Sub 'FillMetesFilterGrid

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    ' Grid Columns
    GRID_COLUMNS_SEPARATE = TERMINAL_DATA_COLUMNS + 28
    GRID_COLUMNS_GROUPED = TERMINAL_DATA_COLUMNS + 7

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_DATETIME = 1
    GRID_INIT_TERMINAL_DATA = 2
    GRID_COLUMN_GAME_NAME = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_DESCRIPTION = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_METER_CODE = TERMINAL_DATA_COLUMNS + 4

    ' Separate mode
    GRID_COLUMN_SAS_METERS_CODE = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_SAS_OLD_METERS = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_SAS_NEW_METER = TERMINAL_DATA_COLUMNS + 7

    GRID_COLUMN_HPC_OLD_METER = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_HPC_NEW_METER = TERMINAL_DATA_COLUMNS + 9

    GRID_COLUMN_PWJ_OLD_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_PWJ_OLD_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_PWJ_NEW_PLAYED_COUNT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_PWJ_NEW_PLAYED_AMOUNT = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_PWJ_OLD_WON_COUNT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_PWJ_OLD_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_PWJ_NEW_WON_COUNT = TERMINAL_DATA_COLUMNS + 16
    GRID_COLUMN_PWJ_NEW_WON_AMOUNT = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_PWJ_OLD_JACKPOT = TERMINAL_DATA_COLUMNS + 18
    GRID_COLUMN_PWJ_NEW_JACKPOT = TERMINAL_DATA_COLUMNS + 19

    GRID_COLUMN_AFT_EFT_OLD_TOGM_QUANTITY = TERMINAL_DATA_COLUMNS + 20
    GRID_COLUMN_AFT_EFT_OLD_TOGM_AMOUNT = TERMINAL_DATA_COLUMNS + 21
    GRID_COLUMN_AFT_EFT_NEW_TOGM_QUANTITY = TERMINAL_DATA_COLUMNS + 22
    GRID_COLUMN_AFT_EFT_NEW_TOGM_AMOUNT = TERMINAL_DATA_COLUMNS + 23
    GRID_COLUMN_AFT_EFT_OLD_FROMGM_QUANTITY = TERMINAL_DATA_COLUMNS + 24
    GRID_COLUMN_AFT_EFT_OLD_FROMGM_AMOUNT = TERMINAL_DATA_COLUMNS + 25
    GRID_COLUMN_AFT_EFT_NEW_FROMGM_QUANTITY = TERMINAL_DATA_COLUMNS + 26
    GRID_COLUMN_AFT_EFT_NEW_FROMGM_AMOUNT = TERMINAL_DATA_COLUMNS + 27

    ' Grouped mode
    GRID_COLUMN_OLD_NEW = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_GROUPED = TERMINAL_DATA_COLUMNS + 6
  End Sub

#End Region ' Private Functions

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region

End Class
