<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_table_player_movements
  Inherits GUI_Controls.frm_base_sel_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_table_player_movements))
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.opt_one_cashier = New System.Windows.Forms.RadioButton()
    Me.opt_all_terminals = New System.Windows.Forms.RadioButton()
    Me.cmb_cashier = New GUI_Controls.uc_combo()
    Me.gb_move_type = New System.Windows.Forms.GroupBox()
    Me.opt_all_types = New System.Windows.Forms.RadioButton()
    Me.opt_several_types = New System.Windows.Forms.RadioButton()
    Me.dg_filter = New GUI_Controls.uc_grid()
    Me.uc_checked_list_tables = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.gb_move_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_move_type)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_tables)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_cashier)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1174, 233)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_tables, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_move_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 237)
    Me.panel_data.Size = New System.Drawing.Size(1174, 441)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1168, 22)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1168, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(0, 156)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(307, 75)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(47, 44)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(47, 16)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(0, 0)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 0
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.opt_one_cashier)
    Me.gb_cashier.Controls.Add(Me.opt_all_terminals)
    Me.gb_cashier.Controls.Add(Me.cmb_cashier)
    Me.gb_cashier.Location = New System.Drawing.Point(313, 156)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(347, 75)
    Me.gb_cashier.TabIndex = 3
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'opt_one_cashier
    '
    Me.opt_one_cashier.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_one_cashier.Location = New System.Drawing.Point(37, 16)
    Me.opt_one_cashier.Name = "opt_one_cashier"
    Me.opt_one_cashier.Size = New System.Drawing.Size(85, 24)
    Me.opt_one_cashier.TabIndex = 0
    Me.opt_one_cashier.Text = "xCashier"
    '
    'opt_all_terminals
    '
    Me.opt_all_terminals.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_all_terminals.Location = New System.Drawing.Point(37, 44)
    Me.opt_all_terminals.Name = "opt_all_terminals"
    Me.opt_all_terminals.Size = New System.Drawing.Size(85, 24)
    Me.opt_all_terminals.TabIndex = 4
    Me.opt_all_terminals.Text = "xAll"
    '
    'cmb_cashier
    '
    Me.cmb_cashier.AllowUnlistedValues = False
    Me.cmb_cashier.AutoCompleteMode = False
    Me.cmb_cashier.IsReadOnly = False
    Me.cmb_cashier.Location = New System.Drawing.Point(128, 16)
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.Size = New System.Drawing.Size(184, 24)
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TabIndex = 1
    Me.cmb_cashier.TextCombo = Nothing
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'gb_move_type
    '
    Me.gb_move_type.Controls.Add(Me.opt_all_types)
    Me.gb_move_type.Controls.Add(Me.opt_several_types)
    Me.gb_move_type.Controls.Add(Me.dg_filter)
    Me.gb_move_type.Location = New System.Drawing.Point(666, 3)
    Me.gb_move_type.Name = "gb_move_type"
    Me.gb_move_type.Size = New System.Drawing.Size(415, 153)
    Me.gb_move_type.TabIndex = 4
    Me.gb_move_type.TabStop = False
    Me.gb_move_type.Text = "xMov. Type"
    '
    'opt_all_types
    '
    Me.opt_all_types.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_all_types.Location = New System.Drawing.Point(8, 48)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(80, 24)
    Me.opt_all_types.TabIndex = 1
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.opt_several_types.Location = New System.Drawing.Point(8, 21)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(80, 24)
    Me.opt_several_types.TabIndex = 0
    Me.opt_several_types.Text = "xSeveral"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter.Location = New System.Drawing.Point(94, 14)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(315, 132)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 2
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    Me.dg_filter.WordWrap = False
    '
    'uc_checked_list_tables
    '
    Me.uc_checked_list_tables.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_tables.Location = New System.Drawing.Point(313, 3)
    Me.uc_checked_list_tables.m_resize_width = 347
    Me.uc_checked_list_tables.multiChoice = True
    Me.uc_checked_list_tables.Name = "uc_checked_list_tables"
    Me.uc_checked_list_tables.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_tables.SelectedIndexesList = ""
    Me.uc_checked_list_tables.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_tables.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_tables.SelectedValuesList = ""
    Me.uc_checked_list_tables.SetLevels = 2
    Me.uc_checked_list_tables.Size = New System.Drawing.Size(347, 153)
    Me.uc_checked_list_tables.TabIndex = 2
    Me.uc_checked_list_tables.ValuesArray = New String(-1) {}
    '
    'frm_table_player_movements
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1184, 682)
    Me.Name = "frm_table_player_movements"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_table_play_sessions"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_move_type.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_terminals As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  Friend WithEvents gb_move_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Friend WithEvents uc_checked_list_tables As GUI_Controls.uc_checked_list
End Class
