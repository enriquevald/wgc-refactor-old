<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_draws_cashdesk_detail_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_draws_cashdesk_detail_sel))
    Me.uc_asl = New GUI_Controls.uc_account_sel()
    Me.gb_cashier = New System.Windows.Forms.GroupBox()
    Me.opt_one_cashier = New System.Windows.Forms.RadioButton()
    Me.opt_all_cashiers = New System.Windows.Forms.RadioButton()
    Me.cmb_cashier = New GUI_Controls.uc_combo()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.ef_draw_id = New GUI_Controls.uc_entry_field()
    Me.ef_site_id = New GUI_Controls.uc_entry_field()
    Me.gp_draws_type = New System.Windows.Forms.GroupBox()
    Me.chk_GamingTable_draw = New System.Windows.Forms.CheckBox()
    Me.chk_CashDesk_draw = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_cashier.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gp_draws_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gp_draws_type)
    Me.panel_filter.Controls.Add(Me.ef_site_id)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.gb_cashier)
    Me.panel_filter.Controls.Add(Me.uc_asl)
    Me.panel_filter.Controls.Add(Me.ef_draw_id)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1188, 166)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_draw_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_asl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashier, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_site_id, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gp_draws_type, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 170)
    Me.panel_data.Size = New System.Drawing.Size(1188, 477)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1182, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1182, 4)
    '
    'uc_asl
    '
    Me.uc_asl.Account = ""
    Me.uc_asl.AccountText = ""
    Me.uc_asl.Anon = False
    Me.uc_asl.AutoSize = True
    Me.uc_asl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_asl.BirthDate = New Date(CType(0, Long))
    Me.uc_asl.DisabledHolder = False
    Me.uc_asl.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_asl.Holder = ""
    Me.uc_asl.Location = New System.Drawing.Point(544, 5)
    Me.uc_asl.MassiveSearchNumbers = ""
    Me.uc_asl.MassiveSearchNumbersToEdit = ""
    Me.uc_asl.MassiveSearchType = 0
    Me.uc_asl.Name = "uc_asl"
    Me.uc_asl.SearchTrackDataAsInternal = True
    Me.uc_asl.ShowMassiveSearch = False
    Me.uc_asl.ShowVipClients = True
    Me.uc_asl.Size = New System.Drawing.Size(307, 134)
    Me.uc_asl.TabIndex = 2
    Me.uc_asl.Telephone = ""
    Me.uc_asl.TrackData = ""
    Me.uc_asl.Vip = False
    Me.uc_asl.WeddingDate = New Date(CType(0, Long))
    '
    'gb_cashier
    '
    Me.gb_cashier.Controls.Add(Me.opt_one_cashier)
    Me.gb_cashier.Controls.Add(Me.opt_all_cashiers)
    Me.gb_cashier.Controls.Add(Me.cmb_cashier)
    Me.gb_cashier.Location = New System.Drawing.Point(269, 8)
    Me.gb_cashier.Name = "gb_cashier"
    Me.gb_cashier.Size = New System.Drawing.Size(272, 72)
    Me.gb_cashier.TabIndex = 1
    Me.gb_cashier.TabStop = False
    Me.gb_cashier.Text = "xCashier"
    '
    'opt_one_cashier
    '
    Me.opt_one_cashier.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_cashier.Name = "opt_one_cashier"
    Me.opt_one_cashier.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_cashier.TabIndex = 0
    Me.opt_one_cashier.Text = "xOne"
    '
    'opt_all_cashiers
    '
    Me.opt_all_cashiers.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_cashiers.Name = "opt_all_cashiers"
    Me.opt_all_cashiers.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_cashiers.TabIndex = 2
    Me.opt_all_cashiers.Text = "xAll"
    '
    'cmb_cashier
    '
    Me.cmb_cashier.AllowUnlistedValues = False
    Me.cmb_cashier.AutoCompleteMode = False
    Me.cmb_cashier.IsReadOnly = False
    Me.cmb_cashier.Location = New System.Drawing.Point(80, 14)
    Me.cmb_cashier.Name = "cmb_cashier"
    Me.cmb_cashier.SelectedIndex = -1
    Me.cmb_cashier.Size = New System.Drawing.Size(184, 24)
    Me.cmb_cashier.SufixText = "Sufix Text"
    Me.cmb_cashier.SufixTextVisible = True
    Me.cmb_cashier.TabIndex = 1
    Me.cmb_cashier.TextCombo = Nothing
    Me.cmb_cashier.TextVisible = False
    Me.cmb_cashier.TextWidth = 0
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(269, 86)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(272, 72)
    Me.gb_user.TabIndex = 4
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(81, 45)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(8, 14)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(8, 40)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 2
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(78, 14)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 1
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(8, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(255, 72)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_draw_id
    '
    Me.ef_draw_id.DoubleValue = 0.0R
    Me.ef_draw_id.IntegerValue = 0
    Me.ef_draw_id.IsReadOnly = False
    Me.ef_draw_id.Location = New System.Drawing.Point(60, 96)
    Me.ef_draw_id.Name = "ef_draw_id"
    Me.ef_draw_id.PlaceHolder = Nothing
    Me.ef_draw_id.Size = New System.Drawing.Size(176, 24)
    Me.ef_draw_id.SufixText = "Sufix Text"
    Me.ef_draw_id.SufixTextVisible = True
    Me.ef_draw_id.TabIndex = 3
    Me.ef_draw_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_id.TextValue = ""
    Me.ef_draw_id.Value = ""
    Me.ef_draw_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_site_id
    '
    Me.ef_site_id.DoubleValue = 0.0R
    Me.ef_site_id.IntegerValue = 0
    Me.ef_site_id.IsReadOnly = False
    Me.ef_site_id.Location = New System.Drawing.Point(60, 126)
    Me.ef_site_id.Name = "ef_site_id"
    Me.ef_site_id.PlaceHolder = Nothing
    Me.ef_site_id.Size = New System.Drawing.Size(176, 24)
    Me.ef_site_id.SufixText = "Sufix Text"
    Me.ef_site_id.SufixTextVisible = True
    Me.ef_site_id.TabIndex = 11
    Me.ef_site_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_site_id.TextValue = ""
    Me.ef_site_id.Value = ""
    Me.ef_site_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'gp_draws_type
    '
    Me.gp_draws_type.Controls.Add(Me.chk_GamingTable_draw)
    Me.gp_draws_type.Controls.Add(Me.chk_CashDesk_draw)
    Me.gp_draws_type.Location = New System.Drawing.Point(857, 8)
    Me.gp_draws_type.Name = "gp_draws_type"
    Me.gp_draws_type.Size = New System.Drawing.Size(188, 72)
    Me.gp_draws_type.TabIndex = 12
    Me.gp_draws_type.TabStop = False
    Me.gp_draws_type.Text = "xDrawsType"
    '
    'chk_GamingTable_draw
    '
    Me.chk_GamingTable_draw.AutoSize = True
    Me.chk_GamingTable_draw.Checked = True
    Me.chk_GamingTable_draw.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_GamingTable_draw.Location = New System.Drawing.Point(9, 45)
    Me.chk_GamingTable_draw.Name = "chk_GamingTable_draw"
    Me.chk_GamingTable_draw.Size = New System.Drawing.Size(145, 17)
    Me.chk_GamingTable_draw.TabIndex = 5
    Me.chk_GamingTable_draw.Text = "xGamingTable_Draw"
    Me.chk_GamingTable_draw.UseVisualStyleBackColor = True
    '
    'chk_CashDesk_draw
    '
    Me.chk_CashDesk_draw.AutoSize = True
    Me.chk_CashDesk_draw.Checked = True
    Me.chk_CashDesk_draw.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_CashDesk_draw.Location = New System.Drawing.Point(9, 19)
    Me.chk_CashDesk_draw.Name = "chk_CashDesk_draw"
    Me.chk_CashDesk_draw.Size = New System.Drawing.Size(126, 17)
    Me.chk_CashDesk_draw.TabIndex = 4
    Me.chk_CashDesk_draw.Text = "xCashdesk_Draw"
    Me.chk_CashDesk_draw.UseVisualStyleBackColor = True
    '
    'frm_draws_cashdesk_detail_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1198, 651)
    Me.Name = "frm_draws_cashdesk_detail_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_draws_cashdesk_detail_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_cashier.ResumeLayout(False)
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gp_draws_type.ResumeLayout(False)
    Me.gp_draws_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_asl As GUI_Controls.uc_account_sel
  Friend WithEvents gb_cashier As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_cashier As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_cashiers As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_cashier As GUI_Controls.uc_combo
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents ef_draw_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_site_id As GUI_Controls.uc_entry_field
  Friend WithEvents gp_draws_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_GamingTable_draw As System.Windows.Forms.CheckBox
  Friend WithEvents chk_CashDesk_draw As System.Windows.Forms.CheckBox
End Class
