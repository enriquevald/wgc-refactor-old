'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_sw_download_package_details
'
' DESCRIPTION : SW Download Package details
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-SEP-2008  TJG    Initial version
' 11-MAR-2016  JCA    Added "SmartFloor" in package name.
'--------------------------------------------------------------------

Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports WSI.Common

Public Class frm_sw_package_details
  Inherits frm_base_edit

#Region " Constants "

  Private Const NUM_PLAYS_DISPLAYED As Integer = 2

  ' Cards grid
  Private Const MODULES_GRID_COLUMN_FILE_NAME As Integer = 0
  Private Const MODULES_GRID_COLUMN_VERSION As Integer = 1
  Private Const MODULES_GRID_COLUMN_FILE_SIZE As Integer = 2

  Private Const MODULES_GRID_COLUMNS As Integer = 3
  Private Const MODULES_GRID_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents ef_package_name As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_version As System.Windows.Forms.Label
  Friend WithEvents pan_version As System.Windows.Forms.Panel
  Friend WithEvents ef_target As GUI_Controls.uc_entry_field
  Friend WithEvents ef_upgrade As GUI_Controls.uc_entry_field
  Friend WithEvents ef_date_insertion As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_locations As System.Windows.Forms.Label
  Friend WithEvents ef_location_b As GUI_Controls.uc_entry_field
  Friend WithEvents ef_location_a As GUI_Controls.uc_entry_field
  Friend WithEvents dg_modules As GUI_Controls.uc_grid

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ef_package_name = New GUI_Controls.uc_entry_field
    Me.pan_version = New System.Windows.Forms.Panel
    Me.lbl_locations = New System.Windows.Forms.Label
    Me.ef_location_b = New GUI_Controls.uc_entry_field
    Me.ef_location_a = New GUI_Controls.uc_entry_field
    Me.ef_date_insertion = New GUI_Controls.uc_entry_field
    Me.ef_target = New GUI_Controls.uc_entry_field
    Me.ef_upgrade = New GUI_Controls.uc_entry_field
    Me.lbl_version = New System.Windows.Forms.Label
    Me.dg_modules = New GUI_Controls.uc_grid
    Me.panel_data.SuspendLayout()
    Me.pan_version.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.dg_modules)
    Me.panel_data.Controls.Add(Me.lbl_version)
    Me.panel_data.Controls.Add(Me.pan_version)
    Me.panel_data.Location = New System.Drawing.Point(7, 4)
    Me.panel_data.Size = New System.Drawing.Size(620, 662)
    '
    'ef_package_name
    '
    Me.ef_package_name.BackColor = System.Drawing.SystemColors.Control
    Me.ef_package_name.DoubleValue = 0
    Me.ef_package_name.IntegerValue = 0
    Me.ef_package_name.IsReadOnly = False
    Me.ef_package_name.Location = New System.Drawing.Point(276, 44)
    Me.ef_package_name.Name = "ef_package_name"
    Me.ef_package_name.Size = New System.Drawing.Size(335, 24)
    Me.ef_package_name.SufixText = "Sufix Text"
    Me.ef_package_name.SufixTextVisible = True
    Me.ef_package_name.TabIndex = 4
    Me.ef_package_name.TabStop = False
    Me.ef_package_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_package_name.TextValue = "xxx"
    Me.ef_package_name.Value = "xxx"
    '
    'pan_version
    '
    Me.pan_version.BackColor = System.Drawing.SystemColors.Control
    Me.pan_version.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.pan_version.Controls.Add(Me.lbl_locations)
    Me.pan_version.Controls.Add(Me.ef_location_b)
    Me.pan_version.Controls.Add(Me.ef_location_a)
    Me.pan_version.Controls.Add(Me.ef_date_insertion)
    Me.pan_version.Controls.Add(Me.ef_target)
    Me.pan_version.Controls.Add(Me.ef_upgrade)
    Me.pan_version.Controls.Add(Me.ef_package_name)
    Me.pan_version.Location = New System.Drawing.Point(0, 22)
    Me.pan_version.Name = "pan_version"
    Me.pan_version.Size = New System.Drawing.Size(618, 179)
    Me.pan_version.TabIndex = 51
    '
    'lbl_locations
    '
    Me.lbl_locations.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_locations.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_locations.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_locations.Location = New System.Drawing.Point(3, 81)
    Me.lbl_locations.Name = "lbl_locations"
    Me.lbl_locations.Size = New System.Drawing.Size(355, 22)
    Me.lbl_locations.TabIndex = 56
    Me.lbl_locations.Text = "xLocations"
    Me.lbl_locations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'ef_location_b
    '
    Me.ef_location_b.BackColor = System.Drawing.SystemColors.Control
    Me.ef_location_b.DoubleValue = 0
    Me.ef_location_b.IntegerValue = 0
    Me.ef_location_b.IsReadOnly = False
    Me.ef_location_b.Location = New System.Drawing.Point(4, 137)
    Me.ef_location_b.Name = "ef_location_b"
    Me.ef_location_b.Size = New System.Drawing.Size(607, 24)
    Me.ef_location_b.SufixText = "Sufix Text"
    Me.ef_location_b.SufixTextVisible = True
    Me.ef_location_b.TabIndex = 55
    Me.ef_location_b.TabStop = False
    Me.ef_location_b.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_location_b.TextValue = ""
    Me.ef_location_b.TextVisible = False
    Me.ef_location_b.TextWidth = 0
    Me.ef_location_b.Value = ""
    '
    'ef_location_a
    '
    Me.ef_location_a.BackColor = System.Drawing.SystemColors.Control
    Me.ef_location_a.DoubleValue = 0
    Me.ef_location_a.IntegerValue = 0
    Me.ef_location_a.IsReadOnly = False
    Me.ef_location_a.Location = New System.Drawing.Point(4, 106)
    Me.ef_location_a.Name = "ef_location_a"
    Me.ef_location_a.Size = New System.Drawing.Size(607, 25)
    Me.ef_location_a.SufixText = "Sufix Text"
    Me.ef_location_a.SufixTextVisible = True
    Me.ef_location_a.TabIndex = 54
    Me.ef_location_a.TabStop = False
    Me.ef_location_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_location_a.TextValue = ""
    Me.ef_location_a.TextVisible = False
    Me.ef_location_a.TextWidth = 0
    Me.ef_location_a.Value = ""
    '
    'ef_date_insertion
    '
    Me.ef_date_insertion.BackColor = System.Drawing.SystemColors.Control
    Me.ef_date_insertion.DoubleValue = 0
    Me.ef_date_insertion.IntegerValue = 0
    Me.ef_date_insertion.IsReadOnly = False
    Me.ef_date_insertion.Location = New System.Drawing.Point(276, 11)
    Me.ef_date_insertion.Name = "ef_date_insertion"
    Me.ef_date_insertion.Size = New System.Drawing.Size(335, 24)
    Me.ef_date_insertion.SufixText = "Sufix Text"
    Me.ef_date_insertion.SufixTextVisible = True
    Me.ef_date_insertion.TabIndex = 53
    Me.ef_date_insertion.TabStop = False
    Me.ef_date_insertion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_date_insertion.TextValue = "xxx"
    Me.ef_date_insertion.Value = "xxx"
    '
    'ef_target
    '
    Me.ef_target.BackColor = System.Drawing.SystemColors.Control
    Me.ef_target.DoubleValue = 0
    Me.ef_target.IntegerValue = 0
    Me.ef_target.IsReadOnly = False
    Me.ef_target.Location = New System.Drawing.Point(3, 11)
    Me.ef_target.Name = "ef_target"
    Me.ef_target.Size = New System.Drawing.Size(250, 24)
    Me.ef_target.SufixText = "Sufix Text"
    Me.ef_target.SufixTextVisible = True
    Me.ef_target.TabIndex = 52
    Me.ef_target.TabStop = False
    Me.ef_target.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_target.TextValue = "xxx"
    Me.ef_target.Value = "xxx"
    '
    'ef_upgrade
    '
    Me.ef_upgrade.BackColor = System.Drawing.SystemColors.Control
    Me.ef_upgrade.DoubleValue = 0
    Me.ef_upgrade.IntegerValue = 0
    Me.ef_upgrade.IsReadOnly = False
    Me.ef_upgrade.Location = New System.Drawing.Point(3, 44)
    Me.ef_upgrade.Name = "ef_upgrade"
    Me.ef_upgrade.Size = New System.Drawing.Size(250, 24)
    Me.ef_upgrade.SufixText = "Sufix Text"
    Me.ef_upgrade.SufixTextVisible = True
    Me.ef_upgrade.TabIndex = 51
    Me.ef_upgrade.TabStop = False
    Me.ef_upgrade.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_upgrade.TextValue = "xxx"
    Me.ef_upgrade.Value = "xxx"
    '
    'lbl_version
    '
    Me.lbl_version.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_version.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_version.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_version.Location = New System.Drawing.Point(0, 0)
    Me.lbl_version.Name = "lbl_version"
    Me.lbl_version.Size = New System.Drawing.Size(617, 22)
    Me.lbl_version.TabIndex = 52
    Me.lbl_version.Text = "xVersion"
    Me.lbl_version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'dg_modules
    '
    Me.dg_modules.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.dg_modules.BackColor = System.Drawing.SystemColors.Control
    Me.dg_modules.CurrentCol = -1
    Me.dg_modules.CurrentRow = -1
    Me.dg_modules.Location = New System.Drawing.Point(0, 217)
    Me.dg_modules.Name = "dg_modules"
    Me.dg_modules.PanelRightVisible = True
    Me.dg_modules.Redraw = True
    Me.dg_modules.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_modules.Size = New System.Drawing.Size(618, 440)
    Me.dg_modules.Sortable = False
    Me.dg_modules.SortAscending = True
    Me.dg_modules.SortByCol = 0
    Me.dg_modules.TabIndex = 53
    Me.dg_modules.ToolTipped = True
    Me.dg_modules.TopRow = -2
    '
    'frm_sw_package_details
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(725, 673)
    Me.ForeColor = System.Drawing.SystemColors.ControlText
    Me.Name = "frm_sw_package_details"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.pan_version.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_form_package As New CLASS_SW_PACKAGE
  Private ContinueOperation As Boolean
  Private m_num_copies As Integer

  Public ef_locations As New System.Collections.Generic.List(Of GUI_Controls.uc_entry_field)

#End Region ' Members

#Region " Override Functions "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SW_DOWNLOAD_DETAILS

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Configuration.GUI_Configuration.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    'Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Dim idx_location As Integer

    ' Required by the base class
    MyBase.GUI_InitControls()

    ' Control collection to hold the locations
    ef_locations.Add(ef_location_a)
    ef_locations.Add(ef_location_b)

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True

    Me.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(300)

    lbl_version.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(301, " ")
    lbl_locations.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(302, " ")

    ef_target.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(305)
    ef_upgrade.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(304)
    ef_date_insertion.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(303)
    ef_package_name.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(309)

    ef_target.IsReadOnly = True
    ef_upgrade.IsReadOnly = True
    ef_date_insertion.IsReadOnly = True
    ef_package_name.IsReadOnly = True

    For idx_location = 0 To CLASS_SW_PACKAGE.MAX_LOCATIONS - 1
      ef_locations.Item(idx_location).IsReadOnly = True
    Next

    With Me.dg_modules
      Call .Init(MODULES_GRID_COLUMNS, MODULES_GRID_HEADER_ROWS)

      '  File Name
      .Column(MODULES_GRID_COLUMN_FILE_NAME).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(310)
      .Column(MODULES_GRID_COLUMN_FILE_NAME).Width = 5200
      .Column(MODULES_GRID_COLUMN_FILE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Version: column is hidden because this field is not currently populated
      .Column(MODULES_GRID_COLUMN_VERSION).Header(0).Text = ""  'GLB_NLS_GUI_SW_DOWNLOAD.GetString(311)
      .Column(MODULES_GRID_COLUMN_VERSION).Width = 0
      .Column(MODULES_GRID_COLUMN_VERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '  File Size
      .Column(MODULES_GRID_COLUMN_FILE_SIZE).Header(0).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(312)
      .Column(MODULES_GRID_COLUMN_FILE_SIZE).Width = 2775
      .Column(MODULES_GRID_COLUMN_FILE_SIZE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim sw_package As CLASS_SW_PACKAGE
    Dim idx_location As Integer
    Dim idx_module As Integer
    Dim rc As Boolean

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      sw_package = m_form_package
    ElseIf Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
      ' Cast the DB object
      sw_package = DbReadObject
    Else
      Return
    End If

    lbl_version.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(301, CLASS_SW_PACKAGE.FormatBuild(sw_package.ClientId, sw_package.BuildId).ToString)
    lbl_locations.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(302, sw_package.NumLocations.ToString)

    ef_package_name.Value = sw_package.PackageName
    ef_target.Value = CLASS_SW_PACKAGE.GetTerminalTypeName(sw_package.TerminalType)

    Select Case sw_package.BuildType
      Case CLASS_SW_PACKAGE.BUILD_TYPE_FULL
        ef_upgrade.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(306)

      Case CLASS_SW_PACKAGE.BUILD_TYPE_UPGRADE
        ef_upgrade.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(307)

      Case Else

    End Select

    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      ' Make button invisible for new versions
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

      ' No insertion date
      ef_date_insertion.Value = ""

      ' No status yet
      'ef_status.Value = ""

      ' Modules info
      For idx_module = 0 To sw_package.NumModules - 1
        With dg_modules
          .AddRow()

          .Cell(idx_module, MODULES_GRID_COLUMN_FILE_NAME).Value = sw_package.PackageModule(idx_module).name
          .Cell(idx_module, MODULES_GRID_COLUMN_VERSION).Value = sw_package.PackageModule(idx_module).version
          .Cell(idx_module, MODULES_GRID_COLUMN_FILE_SIZE).Value = GUI_FormatNumber(sw_package.PackageModule(idx_module).size, 0)
        End With
      Next

    ElseIf Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
      ' Insertion date
      ef_date_insertion.Value = GUI_FormatDate(sw_package.InsertionDate, _
                                               ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                               ENUM_FORMAT_TIME.FORMAT_TIME_LONG)

      ' Show components details read from available Locations
      rc = CLASS_SW_PACKAGE.RetrievePackageInfoFromFtpLocation(sw_package)
      If (Not rc) Then
        ' 115 - "Could not retrieve modules information for package file %1."
        NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(115), _
                   ENUM_MB_TYPE.MB_TYPE_ERROR, _
                   ENUM_MB_BTN.MB_BTN_OK, _
                   ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                   sw_package.PackageName)

      End If

      For idx_module = 0 To sw_package.NumModules - 1
        With dg_modules
          .AddRow()

          .Cell(idx_module, MODULES_GRID_COLUMN_FILE_NAME).Value = sw_package.PackageModule(idx_module).name
          .Cell(idx_module, MODULES_GRID_COLUMN_VERSION).Value = sw_package.PackageModule(idx_module).version
          .Cell(idx_module, MODULES_GRID_COLUMN_FILE_SIZE).Value = GUI_FormatNumber(sw_package.PackageModule(idx_module).size, 0)
        End With
      Next

    End If

    For idx_location = 0 To CLASS_SW_PACKAGE.MAX_LOCATIONS - 1
      ef_locations.Item(idx_location).Value = sw_package.Location(idx_location)
    Next

  End Sub ' GUI_SetScreenData

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim sw_package As CLASS_SW_PACKAGE
    Dim idx_module As Integer
    Dim idx_location As Integer

    sw_package = DbEditedObject

    ' Populate relevant fields 
    sw_package.FileName = m_form_package.FileName
    sw_package.FullFileName = m_form_package.FullFileName
    sw_package.ClientId = m_form_package.ClientId
    sw_package.BuildId = m_form_package.BuildId
    sw_package.TerminalType = m_form_package.TerminalType

    sw_package.NumModules = m_form_package.NumModules
    For idx_module = 0 To m_form_package.NumModules - 1
      sw_package.PackageModule(idx_module) = m_form_package.PackageModule(idx_module)
    Next

    sw_package.NumLocations = m_form_package.NumLocations
    For idx_location = 0 To sw_package.NumLocations - 1
      sw_package.Location(idx_location) = m_form_package.Location(idx_location)
    Next

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim sw_package As CLASS_SW_PACKAGE
    Dim sw_package_id As CLASS_SW_PACKAGE
    Dim rc As Integer
    Dim context As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_SW_PACKAGE

        If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_EDIT Then
          sw_package = DbEditedObject
          sw_package_id = Me.DbObjectId

          sw_package.BuildId = sw_package_id.BuildId
          sw_package.ClientId = sw_package_id.ClientId
          sw_package.TerminalType = sw_package_id.TerminalType
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' Cast on the DB Object and its ID to copy the ID
          sw_package = DbEditedObject
          sw_package_id = Me.DbObjectId

          sw_package.BuildId = sw_package_id.BuildId
          sw_package.ClientId = sw_package_id.ClientId
          sw_package.TerminalType = sw_package_id.TerminalType

          ' 121 - "Error reading data related to Software Package=%1"
          NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(121), _
                     ENUM_MB_TYPE.MB_TYPE_ERROR, _
                     ENUM_MB_BTN.MB_BTN_OK, _
                     ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                     sw_package.Version)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        ContinueOperation = True
        sw_package = Me.DbEditedObject

        ' Check locations
        If sw_package.NumLocations = 0 Then
          ' No locations are available for the package to be copied
          rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(109), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

          ContinueOperation = False
        Else
          If (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WIN_KIOSK) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHIER) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WCP_SERVICE) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WC2_SERVICE) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WXP_SERVICE) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WWP_SITE_SERVICE) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WWP_CENTER_SERVICE) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_SAS_HOST) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_SITE_JACKPOT) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_PROMOBOX) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_ISTATS) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_PSA_CLIENT) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_AFIP_CLIENT) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_SMARTFLOOR_DATA_SERVICE) And _
             (sw_package.TerminalType <> ENUM_TSV_PACKET.TSV_PACKET_PROTOCOLS_SERVICE) Then

            rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(113), _
                            mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                            , _
                            sw_package.FileName)

            ContinueOperation = False
          Else
            If (sw_package.BuildType <> CLASS_SW_PACKAGE.BUILD_TYPE_FULL) Then
              rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(114), _
                              mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

              ContinueOperation = False
            Else
              ' Check that this package is not already in the database
              If CLASS_SW_PACKAGE.CheckSwPackageExists(sw_package, context) Then
                rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(103), _
                                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                sw_package.ClientId, _
                                sw_package.BuildId, _
                                CLASS_SW_PACKAGE.GetTerminalTypeName(sw_package.TerminalType))

                ContinueOperation = False
              Else
                ' Launch process to copy files to final locations
                m_num_copies = CLASS_SW_PACKAGE.CopyToLocations(sw_package)

                ' Process can be considered as successful if at least one copy is done
                If m_num_copies = 0 Then
                  ' Package could not be copied to any location
                  rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(108), _
                                  mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                  mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                                  , _
                                  sw_package.FileName, _
                                  sw_package.NumLocations)

                  ContinueOperation = False
                Else
                  ' Copy operation was successful
                  Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)
                End If
              End If
            End If
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_INSERT
        If ContinueOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          ContinueOperation = False
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If ContinueOperation Then
          sw_package = Me.DbEditedObject

          If DbStatus = ENUM_STATUS.STATUS_OK Then
            Select Case m_num_copies
              Case 0
                ' Already considered

              Case sw_package.NumLocations
                ' Package was copied to all the locations
                rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(107), _
                                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                                mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                sw_package.FileName, _
                                sw_package.NumLocations)

              Case Else
                ' Package was only copied to some of the locations
                rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(106), _
                                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                sw_package.FileName, _
                                m_num_copies, _
                                sw_package.NumLocations)

            End Select
          End If
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        ContinueOperation = False
        sw_package = Me.DbEditedObject
        rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(104), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        sw_package.PackageName)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          ContinueOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If ContinueOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If ContinueOperation Then
          sw_package = Me.DbEditedObject
          m_num_copies = CLASS_SW_PACKAGE.CleanFtpLocation(sw_package)
          If (m_num_copies = 0) Then
            rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(118), _
                            mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                            mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                            , _
                            sw_package.PackageName, _
                            sw_package.NumLocations)
          Else
            If (m_num_copies < sw_package.NumLocations) Then
              rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(105), _
                              mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                              , _
                              sw_package.PackageName, _
                              m_num_copies)
            Else
              rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(110), _
                              mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                              , _
                              sw_package.PackageName)
            End If
          End If
        End If

      Case Else
          MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_package_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - DrawId
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal SwPackage As CLASS_SW_PACKAGE)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = SwPackage

    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If
  End Sub ' ShowEditItem

  Public Overloads Sub ShowNewItem()

    Dim fd_browse_package As New System.Windows.Forms.OpenFileDialog
    Dim sw_package As New CLASS_SW_PACKAGE
    Dim rc As Integer
    Dim rc_bool As Boolean

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    ' Steps
    '   - Get the package file 
    '   - Get the software package locations (2) and build the destination location full path
    '   - Copy the package file to the destination locations
    '   - Add the package file entry to the DB

    '   - Get the package file 
    '       - Open the file browser 
    fd_browse_package.Filter = "Packages (*.TSV)" & "|*.TSV"
    fd_browse_package.FilterIndex = 1
    fd_browse_package.RestoreDirectory = True

    If fd_browse_package.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
      ' Cancel operation
      Exit Sub
    End If

    '       - Feed the dialog
    Call Application.DoEvents()

    ' Change cursor appearance
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    '       - Get the package file name 
    rc_bool = CLASS_SW_PACKAGE.GetPackageFileDetails(fd_browse_package.FileName, m_form_package)

    ' Change cursor appearance
    Windows.Forms.Cursor.Current = Cursors.Default

    If Not rc_bool Then
      ' Wrong sw package 
      ' Show error message
      rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(111), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                      , _
                      fd_browse_package.FileName)
      Exit Sub
    End If

    '   - Get the software package locations (2) and build the destination location full path
    If Not CLASS_SW_PACKAGE.GetPackageLocations(m_form_package) Then
      ' Wrong package locations
      ' Show error message
      rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(112), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

#End Region ' Override Functions

End Class
