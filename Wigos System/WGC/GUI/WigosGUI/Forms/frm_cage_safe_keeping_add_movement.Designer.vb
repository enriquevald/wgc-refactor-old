<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_safe_keeping_add_movement
    Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.Grid = New GUI_Controls.uc_grid
    Me.ef_holder_name = New GUI_Controls.uc_entry_field
    Me.ef_balance = New GUI_Controls.uc_entry_field
    Me.gb_account = New System.Windows.Forms.GroupBox
    Me.btn_add_amount = New GUI_Controls.uc_button
    Me.btn_sub_amount = New GUI_Controls.uc_button
    Me.gb_operations = New System.Windows.Forms.GroupBox
    Me.cmb_cage_sessions = New GUI_Controls.uc_combo
    Me.lbl_blocked = New System.Windows.Forms.Label
    Me.ef_amount = New GUI_Controls.uc_entry_field
    Me.gb_last_movements = New System.Windows.Forms.GroupBox
    Me.panel_data.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.gb_operations.SuspendLayout()
    Me.gb_last_movements.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_last_movements)
    Me.panel_data.Controls.Add(Me.gb_operations)
    Me.panel_data.Controls.Add(Me.gb_account)
    Me.panel_data.Size = New System.Drawing.Size(807, 229)
    '
    'Grid
    '
    Me.Grid.CurrentCol = -1
    Me.Grid.CurrentRow = -1
    Me.Grid.EditableCellBackColor = System.Drawing.Color.Empty
    Me.Grid.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.Grid.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.Grid.Enabled = False
    Me.Grid.Location = New System.Drawing.Point(6, 17)
    Me.Grid.Name = "Grid"
    Me.Grid.PanelRightVisible = False
    Me.Grid.Redraw = True
    Me.Grid.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.Grid.Size = New System.Drawing.Size(370, 196)
    Me.Grid.Sortable = False
    Me.Grid.SortAscending = True
    Me.Grid.SortByCol = 0
    Me.Grid.TabIndex = 0
    Me.Grid.ToolTipped = True
    Me.Grid.TopRow = -2
    '
    'ef_holder_name
    '
    Me.ef_holder_name.DoubleValue = 0
    Me.ef_holder_name.IntegerValue = 0
    Me.ef_holder_name.IsReadOnly = True
    Me.ef_holder_name.Location = New System.Drawing.Point(9, 20)
    Me.ef_holder_name.Name = "ef_holder_name"
    Me.ef_holder_name.PlaceHolder = Nothing
    Me.ef_holder_name.Size = New System.Drawing.Size(392, 24)
    Me.ef_holder_name.SufixText = "Sufix Text"
    Me.ef_holder_name.SufixTextVisible = True
    Me.ef_holder_name.TabIndex = 0
    Me.ef_holder_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_holder_name.TextValue = ""
    Me.ef_holder_name.Value = ""
    Me.ef_holder_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_balance
    '
    Me.ef_balance.DoubleValue = 0
    Me.ef_balance.IntegerValue = 0
    Me.ef_balance.IsReadOnly = True
    Me.ef_balance.Location = New System.Drawing.Point(9, 51)
    Me.ef_balance.Name = "ef_balance"
    Me.ef_balance.PlaceHolder = Nothing
    Me.ef_balance.Size = New System.Drawing.Size(392, 25)
    Me.ef_balance.SufixText = "Sufix Text"
    Me.ef_balance.SufixTextVisible = True
    Me.ef_balance.TabIndex = 1
    Me.ef_balance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_balance.TextValue = ""
    Me.ef_balance.TextWidth = 258
    Me.ef_balance.Value = ""
    Me.ef_balance.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.ef_holder_name)
    Me.gb_account.Controls.Add(Me.ef_balance)
    Me.gb_account.Location = New System.Drawing.Point(394, 3)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(407, 87)
    Me.gb_account.TabIndex = 4
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "GroupBox1"
    '
    'btn_add_amount
    '
    Me.btn_add_amount.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add_amount.Location = New System.Drawing.Point(215, 90)
    Me.btn_add_amount.Name = "btn_add_amount"
    Me.btn_add_amount.Size = New System.Drawing.Size(90, 30)
    Me.btn_add_amount.TabIndex = 2
    Me.btn_add_amount.ToolTipped = False
    Me.btn_add_amount.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_sub_amount
    '
    Me.btn_sub_amount.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_sub_amount.Location = New System.Drawing.Point(311, 90)
    Me.btn_sub_amount.Name = "btn_sub_amount"
    Me.btn_sub_amount.Size = New System.Drawing.Size(90, 30)
    Me.btn_sub_amount.TabIndex = 3
    Me.btn_sub_amount.ToolTipped = False
    Me.btn_sub_amount.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_operations
    '
    Me.gb_operations.Controls.Add(Me.cmb_cage_sessions)
    Me.gb_operations.Controls.Add(Me.lbl_blocked)
    Me.gb_operations.Controls.Add(Me.ef_amount)
    Me.gb_operations.Controls.Add(Me.btn_sub_amount)
    Me.gb_operations.Controls.Add(Me.btn_add_amount)
    Me.gb_operations.Location = New System.Drawing.Point(394, 96)
    Me.gb_operations.Name = "gb_operations"
    Me.gb_operations.Size = New System.Drawing.Size(407, 130)
    Me.gb_operations.TabIndex = 7
    Me.gb_operations.TabStop = False
    Me.gb_operations.Text = "GroupBox2"
    '
    'cmb_cage_sessions
    '
    Me.cmb_cage_sessions.AllowUnlistedValues = False
    Me.cmb_cage_sessions.AutoCompleteMode = False
    Me.cmb_cage_sessions.IsReadOnly = False
    Me.cmb_cage_sessions.Location = New System.Drawing.Point(15, 17)
    Me.cmb_cage_sessions.Name = "cmb_cage_sessions"
    Me.cmb_cage_sessions.SelectedIndex = -1
    Me.cmb_cage_sessions.Size = New System.Drawing.Size(386, 24)
    Me.cmb_cage_sessions.SufixText = "Sufix Text"
    Me.cmb_cage_sessions.SufixTextVisible = True
    Me.cmb_cage_sessions.TabIndex = 0
    Me.cmb_cage_sessions.TextWidth = 110
    '
    'lbl_blocked
    '
    Me.lbl_blocked.BackColor = System.Drawing.Color.Red
    Me.lbl_blocked.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_blocked.ForeColor = System.Drawing.Color.White
    Me.lbl_blocked.Location = New System.Drawing.Point(31, 65)
    Me.lbl_blocked.Name = "lbl_blocked"
    Me.lbl_blocked.Size = New System.Drawing.Size(149, 23)
    Me.lbl_blocked.TabIndex = 14
    Me.lbl_blocked.Text = "BLOQUEADO"
    Me.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lbl_blocked.Visible = False
    '
    'ef_amount
    '
    Me.ef_amount.DoubleValue = 0
    Me.ef_amount.IntegerValue = 0
    Me.ef_amount.IsReadOnly = False
    Me.ef_amount.Location = New System.Drawing.Point(186, 53)
    Me.ef_amount.Name = "ef_amount"
    Me.ef_amount.PlaceHolder = Nothing
    Me.ef_amount.Size = New System.Drawing.Size(215, 24)
    Me.ef_amount.SufixText = "Sufix Text"
    Me.ef_amount.SufixTextVisible = True
    Me.ef_amount.TabIndex = 1
    Me.ef_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount.TextValue = ""
    Me.ef_amount.Value = ""
    Me.ef_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_last_movements
    '
    Me.gb_last_movements.Controls.Add(Me.Grid)
    Me.gb_last_movements.Location = New System.Drawing.Point(3, 3)
    Me.gb_last_movements.Name = "gb_last_movements"
    Me.gb_last_movements.Size = New System.Drawing.Size(385, 223)
    Me.gb_last_movements.TabIndex = 8
    Me.gb_last_movements.TabStop = False
    Me.gb_last_movements.Text = "GroupBox1"
    '
    'frm_cage_safe_keeping_add_movement
    '
    Me.ClientSize = New System.Drawing.Size(909, 238)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_cage_safe_keeping_add_movement"
    Me.panel_data.ResumeLayout(False)
    Me.gb_account.ResumeLayout(False)
    Me.gb_operations.ResumeLayout(False)
    Me.gb_last_movements.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents Grid As GUI_Controls.uc_grid
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents ef_holder_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_balance As GUI_Controls.uc_entry_field
  Friend WithEvents btn_sub_amount As GUI_Controls.uc_button
  Friend WithEvents btn_add_amount As GUI_Controls.uc_button
  Friend WithEvents gb_operations As System.Windows.Forms.GroupBox
  Friend WithEvents ef_amount As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_blocked As System.Windows.Forms.Label
  Friend WithEvents cmb_cage_sessions As GUI_Controls.uc_combo
  Friend WithEvents gb_last_movements As System.Windows.Forms.GroupBox

End Class
