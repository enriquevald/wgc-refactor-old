﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_winup_content_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.txt_message = New System.Windows.Forms.TextBox()
    Me.lbl_title = New System.Windows.Forms.Label()
    Me.ef_title = New GUI_Controls.uc_entry_field()
    Me.lbl_description_detail = New System.Windows.Forms.Label()
    Me.tab_control_HTML = New System.Windows.Forms.TabControl()
    Me.tab_html = New System.Windows.Forms.TabPage()
    Me.chk_add_style = New System.Windows.Forms.CheckBox()
    Me.txt_abstract = New System.Windows.Forms.RichTextBox()
    Me.btn_insert_tag = New System.Windows.Forms.Button()
    Me.cmbTag = New System.Windows.Forms.ComboBox()
    Me.tab_preview = New System.Windows.Forms.TabPage()
    Me.web_browser_html = New System.Windows.Forms.WebBrowser()
    Me.tab_app_images = New System.Windows.Forms.TabControl()
    Me.TabBankCard = New System.Windows.Forms.TabPage()
    Me.Uc_imageList = New GUI_Controls.uc_image()
    Me.TabCheck = New System.Windows.Forms.TabPage()
    Me.Uc_imageDetail = New GUI_Controls.uc_image()
    Me.ef_order = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.tab_control_HTML.SuspendLayout()
    Me.tab_html.SuspendLayout()
    Me.tab_preview.SuspendLayout()
    Me.tab_app_images.SuspendLayout()
    Me.TabBankCard.SuspendLayout()
    Me.TabCheck.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.ef_order)
    Me.panel_data.Controls.Add(Me.tab_app_images)
    Me.panel_data.Controls.Add(Me.tab_control_HTML)
    Me.panel_data.Controls.Add(Me.lbl_description_detail)
    Me.panel_data.Controls.Add(Me.chk_enabled)
    Me.panel_data.Controls.Add(Me.lbl_description)
    Me.panel_data.Controls.Add(Me.txt_message)
    Me.panel_data.Controls.Add(Me.lbl_title)
    Me.panel_data.Controls.Add(Me.ef_title)
    Me.panel_data.Size = New System.Drawing.Size(895, 598)
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(129, 33)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(89, 17)
    Me.chk_enabled.TabIndex = 3
    Me.chk_enabled.Text = "xHabilitado"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'lbl_description
    '
    Me.lbl_description.Location = New System.Drawing.Point(7, 59)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(115, 13)
    Me.lbl_description.TabIndex = 30
    Me.lbl_description.Text = "Descripcion listado"
    Me.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'txt_message
    '
    Me.txt_message.Location = New System.Drawing.Point(128, 56)
    Me.txt_message.MaxLength = 1000
    Me.txt_message.Multiline = True
    Me.txt_message.Name = "txt_message"
    Me.txt_message.Size = New System.Drawing.Size(478, 71)
    Me.txt_message.TabIndex = 2
    '
    'lbl_title
    '
    Me.lbl_title.Location = New System.Drawing.Point(7, 4)
    Me.lbl_title.Name = "lbl_title"
    Me.lbl_title.Size = New System.Drawing.Size(85, 24)
    Me.lbl_title.TabIndex = 29
    Me.lbl_title.Text = "xTitle"
    Me.lbl_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'ef_title
    '
    Me.ef_title.AllowDrop = True
    Me.ef_title.DoubleValue = 0.0R
    Me.ef_title.IntegerValue = 0
    Me.ef_title.IsReadOnly = False
    Me.ef_title.Location = New System.Drawing.Point(127, 3)
    Me.ef_title.Name = "ef_title"
    Me.ef_title.PlaceHolder = Nothing
    Me.ef_title.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.ef_title.Size = New System.Drawing.Size(299, 24)
    Me.ef_title.SufixText = "Sufix Text"
    Me.ef_title.SufixTextVisible = True
    Me.ef_title.TabIndex = 0
    Me.ef_title.TabStop = False
    Me.ef_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_title.TextValue = ""
    Me.ef_title.TextWidth = 0
    Me.ef_title.Value = ""
    Me.ef_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_description_detail
    '
    Me.lbl_description_detail.Location = New System.Drawing.Point(7, 132)
    Me.lbl_description_detail.Name = "lbl_description_detail"
    Me.lbl_description_detail.Size = New System.Drawing.Size(217, 13)
    Me.lbl_description_detail.TabIndex = 33
    Me.lbl_description_detail.Text = "xMessage"
    Me.lbl_description_detail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_control_HTML
    '
    Me.tab_control_HTML.Controls.Add(Me.tab_html)
    Me.tab_control_HTML.Controls.Add(Me.tab_preview)
    Me.tab_control_HTML.Location = New System.Drawing.Point(7, 163)
    Me.tab_control_HTML.Name = "tab_control_HTML"
    Me.tab_control_HTML.SelectedIndex = 0
    Me.tab_control_HTML.Size = New System.Drawing.Size(456, 434)
    Me.tab_control_HTML.TabIndex = 34
    '
    'tab_html
    '
    Me.tab_html.Controls.Add(Me.chk_add_style)
    Me.tab_html.Controls.Add(Me.txt_abstract)
    Me.tab_html.Controls.Add(Me.btn_insert_tag)
    Me.tab_html.Controls.Add(Me.cmbTag)
    Me.tab_html.Location = New System.Drawing.Point(4, 22)
    Me.tab_html.Name = "tab_html"
    Me.tab_html.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_html.Size = New System.Drawing.Size(448, 408)
    Me.tab_html.TabIndex = 0
    Me.tab_html.Text = "HTML"
    Me.tab_html.UseVisualStyleBackColor = True
    '
    'chk_add_style
    '
    Me.chk_add_style.AutoSize = True
    Me.chk_add_style.Location = New System.Drawing.Point(129, 10)
    Me.chk_add_style.Name = "chk_add_style"
    Me.chk_add_style.Size = New System.Drawing.Size(81, 17)
    Me.chk_add_style.TabIndex = 24
    Me.chk_add_style.Text = "Add Style"
    Me.chk_add_style.UseVisualStyleBackColor = True
    '
    'txt_abstract
    '
    Me.txt_abstract.Location = New System.Drawing.Point(3, 47)
    Me.txt_abstract.Name = "txt_abstract"
    Me.txt_abstract.Size = New System.Drawing.Size(439, 355)
    Me.txt_abstract.TabIndex = 19
    Me.txt_abstract.Text = ""
    '
    'btn_insert_tag
    '
    Me.btn_insert_tag.Location = New System.Drawing.Point(227, 6)
    Me.btn_insert_tag.Name = "btn_insert_tag"
    Me.btn_insert_tag.Size = New System.Drawing.Size(65, 23)
    Me.btn_insert_tag.TabIndex = 23
    Me.btn_insert_tag.Text = "insert"
    Me.btn_insert_tag.UseVisualStyleBackColor = True
    '
    'cmbTag
    '
    Me.cmbTag.FormattingEnabled = True
    Me.cmbTag.Location = New System.Drawing.Point(6, 6)
    Me.cmbTag.Name = "cmbTag"
    Me.cmbTag.Size = New System.Drawing.Size(105, 21)
    Me.cmbTag.TabIndex = 21
    '
    'tab_preview
    '
    Me.tab_preview.Controls.Add(Me.web_browser_html)
    Me.tab_preview.Location = New System.Drawing.Point(4, 22)
    Me.tab_preview.Name = "tab_preview"
    Me.tab_preview.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_preview.Size = New System.Drawing.Size(448, 408)
    Me.tab_preview.TabIndex = 2
    Me.tab_preview.Text = "Preview"
    Me.tab_preview.UseVisualStyleBackColor = True
    '
    'web_browser_html
    '
    Me.web_browser_html.Location = New System.Drawing.Point(11, 10)
    Me.web_browser_html.MinimumSize = New System.Drawing.Size(20, 20)
    Me.web_browser_html.Name = "web_browser_html"
    Me.web_browser_html.Size = New System.Drawing.Size(431, 392)
    Me.web_browser_html.TabIndex = 22
    '
    'tab_app_images
    '
    Me.tab_app_images.Controls.Add(Me.TabBankCard)
    Me.tab_app_images.Controls.Add(Me.TabCheck)
    Me.tab_app_images.Location = New System.Drawing.Point(473, 163)
    Me.tab_app_images.Name = "tab_app_images"
    Me.tab_app_images.SelectedIndex = 0
    Me.tab_app_images.Size = New System.Drawing.Size(421, 434)
    Me.tab_app_images.TabIndex = 35
    '
    'TabBankCard
    '
    Me.TabBankCard.Controls.Add(Me.Uc_imageList)
    Me.TabBankCard.Location = New System.Drawing.Point(4, 22)
    Me.TabBankCard.Name = "TabBankCard"
    Me.TabBankCard.Padding = New System.Windows.Forms.Padding(3)
    Me.TabBankCard.Size = New System.Drawing.Size(413, 408)
    Me.TabBankCard.TabIndex = 0
    Me.TabBankCard.Text = "Image List"
    Me.TabBankCard.UseVisualStyleBackColor = True
    '
    'Uc_imageList
    '
    Me.Uc_imageList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Uc_imageList.AutoSize = True
    Me.Uc_imageList.ButtonDeleteEnabled = True
    Me.Uc_imageList.FreeResize = False
    Me.Uc_imageList.Image = Nothing
    Me.Uc_imageList.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList.ImageName = Nothing
    Me.Uc_imageList.Location = New System.Drawing.Point(6, 34)
    Me.Uc_imageList.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList.Name = "Uc_imageList"
    Me.Uc_imageList.Size = New System.Drawing.Size(403, 364)
    Me.Uc_imageList.TabIndex = 11
    '
    'TabCheck
    '
    Me.TabCheck.Controls.Add(Me.Uc_imageDetail)
    Me.TabCheck.Location = New System.Drawing.Point(4, 22)
    Me.TabCheck.Name = "TabCheck"
    Me.TabCheck.Padding = New System.Windows.Forms.Padding(3)
    Me.TabCheck.Size = New System.Drawing.Size(413, 408)
    Me.TabCheck.TabIndex = 1
    Me.TabCheck.Text = "Image Detail"
    Me.TabCheck.UseVisualStyleBackColor = True
    '
    'Uc_imageDetail
    '
    Me.Uc_imageDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Uc_imageDetail.AutoSize = True
    Me.Uc_imageDetail.ButtonDeleteEnabled = True
    Me.Uc_imageDetail.FreeResize = False
    Me.Uc_imageDetail.Image = Nothing
    Me.Uc_imageDetail.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageDetail.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageDetail.ImageName = Nothing
    Me.Uc_imageDetail.Location = New System.Drawing.Point(6, 34)
    Me.Uc_imageDetail.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageDetail.Name = "Uc_imageDetail"
    Me.Uc_imageDetail.Size = New System.Drawing.Size(403, 363)
    Me.Uc_imageDetail.TabIndex = 13
    '
    'ef_order
    '
    Me.ef_order.DoubleValue = 0.0R
    Me.ef_order.IntegerValue = 0
    Me.ef_order.IsReadOnly = False
    Me.ef_order.Location = New System.Drawing.Point(492, 5)
    Me.ef_order.Name = "ef_order"
    Me.ef_order.PlaceHolder = Nothing
    Me.ef_order.Size = New System.Drawing.Size(114, 24)
    Me.ef_order.SufixText = "Sufix Text"
    Me.ef_order.SufixTextVisible = True
    Me.ef_order.TabIndex = 1
    Me.ef_order.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order.TextValue = ""
    Me.ef_order.TextVisible = False
    Me.ef_order.Value = ""
    Me.ef_order.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_winup_content_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(997, 607)
    Me.KeyPreview = False
    Me.Name = "frm_winup_content_edit"
    Me.Text = "frm_winup_content_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.tab_control_HTML.ResumeLayout(False)
    Me.tab_html.ResumeLayout(False)
    Me.tab_html.PerformLayout()
    Me.tab_preview.ResumeLayout(False)
    Me.tab_app_images.ResumeLayout(False)
    Me.TabBankCard.ResumeLayout(False)
    Me.TabBankCard.PerformLayout()
    Me.TabCheck.ResumeLayout(False)
    Me.TabCheck.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_message As System.Windows.Forms.TextBox
  Friend WithEvents lbl_title As System.Windows.Forms.Label
  Friend WithEvents ef_title As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_description_detail As System.Windows.Forms.Label
  Friend WithEvents tab_control_HTML As System.Windows.Forms.TabControl
  Friend WithEvents tab_html As System.Windows.Forms.TabPage
  Friend WithEvents chk_add_style As System.Windows.Forms.CheckBox
  Friend WithEvents txt_abstract As System.Windows.Forms.RichTextBox
  Friend WithEvents btn_insert_tag As System.Windows.Forms.Button
  Friend WithEvents cmbTag As System.Windows.Forms.ComboBox
  Friend WithEvents tab_preview As System.Windows.Forms.TabPage
  Friend WithEvents web_browser_html As System.Windows.Forms.WebBrowser
  Friend WithEvents tab_app_images As System.Windows.Forms.TabControl
  Friend WithEvents TabBankCard As System.Windows.Forms.TabPage
  Friend WithEvents Uc_imageList As GUI_Controls.uc_image
  Friend WithEvents TabCheck As System.Windows.Forms.TabPage
  Friend WithEvents Uc_imageDetail As GUI_Controls.uc_image
  Friend WithEvents ef_order As GUI_Controls.uc_entry_field
End Class
