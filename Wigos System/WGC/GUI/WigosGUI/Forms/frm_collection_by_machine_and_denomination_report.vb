'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_collection_by_machine_and_denomination_report.vb
' DESCRIPTION:   Collection by machine and denomination report
' AUTHOR:        Jos� Mart�nez L�pez
' CREATION DATE: 01-SEP-2014
'
' REVISION HISTORY: 
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-SEP-2014  JML    Initial version
' 09-MAR-2015  FJC    Fixed Bug: WIG-2144
' 26-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 10-FEB-2016  DHA    Bug 24283: Error in accounting denomination columns mapping
' 28-SEP-2017  ETP    Fixed Bug 29953:[WIGOS-4910] Denominations filter values do not match with terminal denomination values 
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports System.Globalization

#End Region ' Imports

Public Class frm_collection_by_machine_and_denomination_report
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' DB Columns
  Private Const SQL_COLUMN_ORDER_DATE As Integer = 0
  Private Const SQL_COLUMN_TE_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TE_PROVIDER_ID As Integer = 2
  Private Const SQL_COLUMN_DENOMINATION As Integer = 3
  Private Const SQL_COLUMN_COLLECTION As Integer = 4
  Private Const SQL_COLUMN_BILL As Integer = 5
  Private Const SQL_COLUMN_TICKET As Integer = 6
  Private Const SQL_COLUMN_MANUAL As Integer = 7
  Private Const SQL_COLUMN_CREDIT_CANCEL As Integer = 8
  Private Const SQL_COLUMN_JACKPOT_DE_SALA As Integer = 9
  Private Const SQL_COLUMN_PROGRESIVES As Integer = 10
  Private Const SQL_COLUMN_NO_PROGRESIVES As Integer = 11
  Private Const SQL_COLUMN_PROGRESIVE_PROVISIONS As Integer = 12

  'Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_ORDER_DATE As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private TERMINAL_DATA_COLUMNS As Integer

  Private GRID_COLUMN_TE_NAME As Integer
  Private GRID_COLUMN_DENOMINATION As Integer
  Private GRID_COLUMN_COLLECTION As Integer
  Private GRID_COLUMN_BILL As Integer
  Private GRID_COLUMN_TICKET As Integer
  Private GRID_COLUMN_MANUAL As Integer
  Private GRID_COLUMN_CREDIT_CANCEL As Integer
  Private GRID_COLUMN_JACKPOT_DE_SALA As Integer
  Private GRID_COLUMN_PROGRESIVES As Integer
  Private GRID_COLUMN_NO_PROGRESIVES As Integer
  Private GRID_COLUMN_PROGRESIVE_PROVISIONS As Integer
  Private GRID_COLUMN_NETWIN_TITO As Integer
  Private GRID_COLUMN_NETWIN_BILL As Integer
  Private GRID_COLUMN_NETWIN As Integer

  Private GRID_COLUMNS As Integer

  ' counters
  Private Const MAX_COUNTERS As Integer = 3
  Private Const COUNTER_DETAILS As Integer = 1
  Private Const COUNTER_SUBTOTALS As Integer = 2

#End Region

#Region " Member "

  'REPORT FILTER
  Dim m_date_from As String
  Dim m_date_to As String
  Dim m_terminal As String
  Dim m_denomination As String
  Dim m_list_type As String

  Dim m_subtotal_break As String

  Private m_query_as_datatable As DataTable

  Private m_counter_list(MAX_COUNTERS) As Integer

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COLLECTION_BY_MACHINE_AND_DENOMINATION_REPORT

    'Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5350)

    ' - Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True, False)

    ' Show 
    Me.gb_show.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5361)
    Me.cb_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5362)
    Me.cb_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5363)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5517)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes())

    ' Denominations
    Me.uc_denominations.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3477)

    ' Style main Grid
    Call Me.GUI_StyleSheet()

    'Default values
    Call Me.SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call Me.SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE

    'Return ENUM_QUERY_TYPE.QUERY_COMMAND
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM

  End Function ' GUI_GetQueryType

  '' PURPOSE: Build an SQL query from conditions set in the filters
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''
  ''     - OUTPUT:
  ''
  '' RETURNS:
  ''     - SQL query text ready to send to the database
  'Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

  '  Dim _sql_cmd As SqlCommand
  '  Dim _str_sql As StringBuilder

  '  _str_sql = New StringBuilder()
  '  _str_sql.AppendLine(" DECLARE @pFromDt AS DATETIME             ")
  '  _str_sql.AppendLine(" DECLARE @pToDt AS DATETIME               ")

  '  _str_sql.AppendLine(" SET @pFromDt = " & GUI_FormatDateDB(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)) & " ")
  '  If Me.uc_dsl.ToDateSelected Then
  '    _str_sql.AppendLine(" SET @pToDt = " & GUI_FormatDateDB(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime)) & " ")
  '  Else
  '    _str_sql.AppendLine(" SET @pToDt = " & GUI_FormatDateDB(WGDB.Now().AddDays(1)) & " ")
  '  End If

  '  _str_sql.AppendLine(" DECLARE @Nrows int                              ")
  '  _str_sql.AppendLine(" DECLARE @index int                              ")
  '  _str_sql.AppendLine(" DECLARE @q table  (i int)                       ")
  '  _str_sql.AppendLine(" SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 ")
  '  _str_sql.AppendLine(" SET @index = 0                                  ")
  '  _str_sql.AppendLine(" WHILE @index < @Nrows                           ")
  '  _str_sql.AppendLine(" BEGIN	                                          ")
  '  _str_sql.AppendLine("   INSERT INTO @q VALUES(@index)	                ")
  '  _str_sql.AppendLine("   SET @index = @index + 1                       ")
  '  _str_sql.AppendLine(" END                                             ")

  '  _str_sql.AppendLine(" SELECT   ORDER_DATE                                                ") '  0
  '  _str_sql.AppendLine("        , TE_TERMINAL_ID                                            ") '  1
  '  _str_sql.AppendLine("        , TE_NAME                                                   ") '  2
  '  _str_sql.AppendLine("        , TE_PROVIDER_ID                                            ") '  3
  '  _str_sql.AppendLine("        , ISNULL(TE_MULTI_DENOMINATION, '--') AS DENOMINATION       ") '  4
  '  _str_sql.AppendLine("        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION                 ") '  5
  '  _str_sql.AppendLine("        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL                     ") '  6 
  '  _str_sql.AppendLine("        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET               ") '  7
  '  _str_sql.AppendLine("        , ISNULL(MANUAL, 0) AS MANUAL                               ") '  8
  '  _str_sql.AppendLine("        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL                 ") '  9
  '  _str_sql.AppendLine("        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA             ") ' 10
  '  _str_sql.AppendLine("        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES                     ") ' 11
  '  _str_sql.AppendLine("        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES               ") ' 12
  '  _str_sql.AppendLine("        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS ") ' 13
  '  _str_sql.AppendLine("   FROM   TERMINALS                                                                                     ")
  '  _str_sql.AppendLine(" LEFT JOIN (SELECT TOP (@Nrows) DATEADD(DAY, I, CAST(@pFromDt AS DATE)) AS ORDER_DATE FROM  @q) DIA ON ORDER_DATE <= GETDATE() ")
  '  _str_sql.AppendLine(" LEFT JOIN (SELECT   PS_TERMINAL_ID                                                                                                     ")
  '  _str_sql.AppendLine("                   , CAST(PS_FINISHED AS DATE) AS PS_FINISHED                                                                           ")
  '  _str_sql.AppendLine("                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN    ")
  '  _str_sql.AppendLine("                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT   ")
  '  _str_sql.AppendLine("                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN  ")
  '  _str_sql.AppendLine("                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT ")
  '  _str_sql.AppendLine("                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN     ")
  '  _str_sql.AppendLine("                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT    ")
  '  _str_sql.AppendLine("              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status))                                                                ")
  '  _str_sql.AppendLine("             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0                                                                                    ")
  '  _str_sql.AppendLine("               AND   PS_FINISHED >= @pFromDt                                                                                            ")
  '  _str_sql.AppendLine("               AND   PS_FINISHED  < @pToDt                                                                                              ")
  '  _str_sql.AppendLine("          GROUP BY   PS_TERMINAL_ID                                                                                                     ")
  '  _str_sql.AppendLine("                   , CAST(PS_FINISHED as date)) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE                   ")
  '  _str_sql.AppendLine(" LEFT JOIN (SELECT   HP_TERMINAL_ID                                                                                                                     ")
  '  _str_sql.AppendLine("                   , CAST(HP_DATETIME AS DATE) AS HP_DATETIME                                                                                           ")
  '  _str_sql.AppendLine("                   , SUM(CASE HP_TYPE WHEN 10 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL                                                           ") '--'MANUAL'
  '  _str_sql.AppendLine("                   , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL                                                    ") '--'CREDITOS CANCELADOS'
  '  _str_sql.AppendLine("                   , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA                                                  ") '--'JACKPOT DE SALA(MISTERY???)' 
  '  _str_sql.AppendLine("                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES    ") '--'PROGRESIVES'
  '  _str_sql.AppendLine("                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES ") '--'NO PROGRESIVES (MISTERY???)' 
  '  _str_sql.AppendLine("              FROM   HANDPAYS                                                                                                                           ")
  '  _str_sql.AppendLine("             WHERE   HP_DATETIME >= @pFromDt                                                                                                            ")
  '  _str_sql.AppendLine("               AND   HP_DATETIME < @pToDt                                                                                                               ")
  '  _str_sql.AppendLine("             GROUP   BY HP_TERMINAL_ID                                                                                                                  ")
  '  _str_sql.AppendLine("                   , CAST(HP_DATETIME AS DATE)) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE                                   ")
  '  _str_sql.AppendLine(" LEFT JOIN (SELECT   PPT_TERMINAL_ID                                                                                   ")
  '  _str_sql.AppendLine("                   , CAST(PGP_HOUR_TO AS DATE) AS PGP_HOUR_TO                                                          ")
  '  _str_sql.AppendLine("                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS                                               ")
  '  _str_sql.AppendLine("             FROM    PROGRESSIVES_PROVISIONS_TERMINALS                                                                 ")
  '  _str_sql.AppendLine("        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID                                    ")
  '  _str_sql.AppendLine("                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID                                ")
  '  _str_sql.AppendLine("                                            AND PGP_HOUR_TO >= @pFromDt                                                ")
  '  _str_sql.AppendLine("                                            AND PGP_HOUR_TO < @pToDt                                                   ")
  '  _str_sql.AppendLine("             GROUP   BY PPT_TERMINAL_ID                                                                                ")
  '  _str_sql.AppendLine("                   , CAST(PGP_HOUR_TO AS DATE)) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE ")
  '  _str_sql.AppendLine("  WHERE ( BILL_IN IS NOT NULL                   ")
  '  _str_sql.AppendLine("     OR   BILL_OUT IS NOT NULL                  ")
  '  _str_sql.AppendLine("     OR   TICKET_IN IS NOT NULL                 ")
  '  _str_sql.AppendLine("     OR   TICKET_OUT IS NOT NULL                ")
  '  _str_sql.AppendLine("     OR   TTL_IN IS NOT NULL                    ")
  '  _str_sql.AppendLine("     OR   TTL_OUT IS NOT NULL                   ")
  '  _str_sql.AppendLine("     OR   MANUAL IS NOT NULL                    ")
  '  _str_sql.AppendLine("     OR   CREDIT_CANCEL IS NOT NULL             ")
  '  _str_sql.AppendLine("     OR   JACKPOT_DE_SALA IS NOT NULL           ")
  '  _str_sql.AppendLine("     OR   PROGRESIVES IS NOT NULL               ")
  '  _str_sql.AppendLine("     OR   NO_PROGRESIVES IS NOT NULL            ")
  '  _str_sql.AppendLine("     OR   PROGRESIVE_PROVISIONS IS NOT NULL   ) ")

  '  _str_sql.AppendLine(GetSqlWhere())

  '  _str_sql.AppendLine("  ORDER   BY TE_PROVIDER_ID, ORDER_DATE, TE_NAME   ")

  '  ' Set data table
  '  m_query_as_datatable = GUI_GetTableUsingSQL(_str_sql.ToString(), Integer.MaxValue)

  '  _sql_cmd = New SqlCommand(_str_sql.ToString())

  '  Return _sql_cmd

  'End Function ' GUI_GetSqlCommand

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _sql_where As String
    Dim _row As DataRow
    Dim _final_time As DateTime

    _final_time = New DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day, Me.uc_dsl.ClosingTime, 0, 0)

    _sql_cmd = New SqlCommand("PR_collection_by_machine_and_denomination")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pFromDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.FromDateSelected, WGDB.Now(), Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime))
    _sql_cmd.Parameters.Add("@pToDt", SqlDbType.DateTime).Value = IIf(Not Me.uc_dsl.ToDateSelected, _final_time.AddDays(1), Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime))
    _sql_cmd.Parameters.Add("@pClosingTime", SqlDbType.Int).Value = Me.uc_dsl.ClosingTime

    _sql_where = GetSqlWhere()
    _sql_cmd.Parameters.Add("@pTerminalWhere", SqlDbType.NVarChar).Value = IIf(String.IsNullOrEmpty(_sql_where), " ", _sql_where)

    m_query_as_datatable = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    If m_query_as_datatable.Rows.Count > 0 Then
      For Each _row In m_query_as_datatable.Rows

        Try
          db_row = New CLASS_DB_ROW(_row)

          If GUI_CheckOutRowBeforeAdd(db_row) Then
            ' Add the db row to the grid
            Me.Grid.AddRow()
            count = count + 1
            idx_row = Me.Grid.NumRows - 1

            If Not GUI_SetupRow(idx_row, db_row) Then
              ' The row can not be added
              Me.Grid.DeleteRowFast(idx_row)
              count = count - 1
            End If
          End If

        Catch ex As OutOfMemoryException
          Throw ex

        Catch exception As Exception
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          Call Trace.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "frm_gaming_tables_income_report", _
                                "GUI_SetupRow", _
                                exception.Message)
          Exit For
        End Try

        db_row = Nothing

        If Not GUI_DoEvents(Me.Grid) Then
          Exit Sub
        End If

      Next

    End If
    Call GUI_AfterLastRow()

  End Sub

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' ORDER_DATE
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ORDER_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_ORDER_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' TE_TERMINAL_ID
    'Me.Grid.Cell(RowIndex, GRID_COLUMN_TE_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID)

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' DENOMINATION
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = DbRow.Value(SQL_COLUMN_DENOMINATION)

    ' COLLECTION
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COLLECTION), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' BILL
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BILL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TICKET
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' MANUAL
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MANUAL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MANUAL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CREDIT_CANCEL
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CREDIT_CANCEL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' JACKPOT_DE_SALA
    Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT_DE_SALA).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOT_DE_SALA), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVES
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESIVES).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESIVES), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NO_PROGRESIVES
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NO_PROGRESIVES).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NO_PROGRESIVES), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVE_PROVISIONS
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PROGRESIVE_PROVISIONS), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TITO
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_TITO).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_BILL
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN_BILL).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET) + DbRow.Value(SQL_COLUMN_BILL), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    m_counter_list(COUNTER_DETAILS) += 1

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If
    m_refresh_grid = False

    m_subtotal_break = ""
    m_counter_list(COUNTER_DETAILS) = 0
    m_counter_list(COUNTER_SUBTOTALS) = 0
  End Sub

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _text_subtotal As String
    If Not Me.cb_total.Checked Then
      Return True
    End If

    If m_subtotal_break <> "" And m_subtotal_break <> DbRow.Value(SQL_COLUMN_DENOMINATION) Then
      _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)

      ProcesSubTotal(False, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal & " " & m_subtotal_break)
    End If

    If Not DbRow.IsNull(SQL_COLUMN_DENOMINATION) Then
      m_subtotal_break = DbRow.Value(SQL_COLUMN_DENOMINATION)
    End If

    If Not Me.cb_detail.Checked Then
      Return False
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _text_subtotal As String
    Dim _text_total As String

    _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)
    _text_total = GLB_NLS_GUI_INVOICING.GetString(205)

    If Me.cb_total.Checked Then
      ProcesSubTotal(False, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal & " " & m_subtotal_break)
    End If
    ProcesSubTotal(True, GRID_COLUMN_TE_NAME, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, _text_total)

    If Grid.Counter(COUNTER_DETAILS).Value <> m_counter_list(COUNTER_DETAILS) Then
      Grid.Counter(COUNTER_DETAILS).Value = m_counter_list(COUNTER_DETAILS)
    End If
    If Grid.Counter(COUNTER_SUBTOTALS).Value <> m_counter_list(COUNTER_SUBTOTALS) Then
      Grid.Counter(COUNTER_SUBTOTALS).Value = m_counter_list(COUNTER_SUBTOTALS)
    End If

  End Sub ' GUI_AfterLastRow

#End Region  ' Overrides

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3477), m_denomination)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5361), m_list_type)

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _date As Date

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""

    ' Date
    _date = uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    m_date_from = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    _date = ToDate()
    m_date_to = GUI_FormatDate(_date, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    If m_terminal Is Nothing Then m_terminal = ""

    m_denomination = Me.uc_denominations.SelectedValuesList

    m_list_type = ""
    If Me.cb_detail.Checked Then
      m_list_type &= Me.cb_detail.Text & ", "
    End If

    If Me.cb_total.Checked Then
      m_list_type &= Me.cb_total.Text & ", "
    End If

    If Me.chk_terminal_location.Checked Then
      m_list_type &= Me.chk_terminal_location.Text & ", "
    End If

    If m_list_type.Length > 2 Then
      m_list_type = m_list_type.Substring(0, m_list_type.Length - 2)
    End If

  End Sub 'GUI_ReportUpdateFilters

#End Region ' GUI Reports

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '           - MdiParent
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region 'Public Functions

#Region " Private Functions "

  ' PURPOSE: Put values to default
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.cb_detail.Checked = True
    Me.cb_total.Checked = False
    Me.chk_terminal_location.Checked = False

    ' Machines
    Call Me.uc_pr_list.SetDefaultValues()

    ' Denominations
    Call FillDenominationsFilterGrid()
    Call Me.uc_denominations.ExpandAll()
    Call Me.uc_denominations.SetDefaultValue(True)

  End Sub 'SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)
      .Sortable = True
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_DETAILS).Visible = True
      .Counter(COUNTER_DETAILS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_DETAILS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      .Counter(COUNTER_SUBTOTALS).Visible = True
      .Counter(COUNTER_SUBTOTALS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_SUBTOTALS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' ORDER_DATE
      .Column(GRID_COLUMN_ORDER_DATE).Header(0).Text = ""
      .Column(GRID_COLUMN_ORDER_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5367)
      .Column(GRID_COLUMN_ORDER_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_ORDER_DATE).Width = 1400

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TE_NAME = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5371)
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_DENOMINATION).Width = 2000

      ' COLLECTION
      .Column(GRID_COLUMN_COLLECTION).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(5365)
      .Column(GRID_COLUMN_COLLECTION).Header(1).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(5372)
      .Column(GRID_COLUMN_COLLECTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_COLLECTION).Width = 0 '1800

      ' BILL
      .Column(GRID_COLUMN_BILL).Header(0).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(5365)
      .Column(GRID_COLUMN_BILL).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(5373)
      .Column(GRID_COLUMN_BILL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_BILL).Width = 0

      ' TICKET
      .Column(GRID_COLUMN_TICKET).Header(0).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(5365)
      .Column(GRID_COLUMN_TICKET).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(5374)
      .Column(GRID_COLUMN_TICKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_TICKET).Width = 0

      ' MANUAL
      .Column(GRID_COLUMN_MANUAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_MANUAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5375)
      .Column(GRID_COLUMN_MANUAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MANUAL).Width = 1800

      ' CREDIT_CANCEL
      .Column(GRID_COLUMN_CREDIT_CANCEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_CREDIT_CANCEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5376)
      .Column(GRID_COLUMN_CREDIT_CANCEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CREDIT_CANCEL).Width = 1800

      ' JACKPOT_DE_SALA
      .Column(GRID_COLUMN_JACKPOT_DE_SALA).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_JACKPOT_DE_SALA).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5377)
      .Column(GRID_COLUMN_JACKPOT_DE_SALA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_JACKPOT_DE_SALA).Width = 1800

      ' PROGRESIVES
      .Column(GRID_COLUMN_PROGRESIVES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_PROGRESIVES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5378)
      .Column(GRID_COLUMN_PROGRESIVES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_PROGRESIVES).Width = 2200

      ' NO_PROGRESIVES 
      .Column(GRID_COLUMN_NO_PROGRESIVES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5366)
      .Column(GRID_COLUMN_NO_PROGRESIVES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5379)
      .Column(GRID_COLUMN_NO_PROGRESIVES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NO_PROGRESIVES).Width = 2400

      ' PROGRESIVE_PROVISIONS
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Header(0).Text = ""
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5380)
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_PROGRESIVE_PROVISIONS).Width = 2500

      ' NETWIN_TITO
      .Column(GRID_COLUMN_NETWIN_TITO).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN_TITO).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5381)
      .Column(GRID_COLUMN_NETWIN_TITO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NETWIN_TITO).Width = 1800

      ' NETWIN_BILL
      .Column(GRID_COLUMN_NETWIN_BILL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN_BILL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5382)
      .Column(GRID_COLUMN_NETWIN_BILL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NETWIN_BILL).Width = 1800

      ' NETWIN
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5372)
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_NETWIN).Width = 1800

    End With

  End Sub ' GUI_StyleSheet

  Private Function ToDate() As Date

    If uc_dsl.ToDateSelected Then
      Return New Date(uc_dsl.ToDate.Year, uc_dsl.ToDate.Month, uc_dsl.ToDate.Day, uc_dsl.ClosingTime, 0, 0)
    Else
      Return New Date(Today.Year, Today.Month, Today.Day, uc_dsl.ClosingTime, 0, 0).AddDays(1)
    End If

  End Function  ' ToDate

  ' PURPOSE: Fill the filter Grid with data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillDenominationsFilterGrid()
    Dim _denominations As String
    Dim _denominations_list As List(Of String)

    Dim _local_separator As String
    Dim _db_separator As String

    Call Me.uc_denominations.Clear()
    Call Me.uc_denominations.ReDraw(False)

    _denominations_list = New List(Of String)

    _local_separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
    _db_separator = CultureInfo.GetCultureInfo("en-US").NumberFormat.NumberDecimalSeparator

    _denominations = GeneralParam.GetString("EGM", "SingleDenominations").Replace(_db_separator, _local_separator)
    _denominations_list.AddRange(CLASS_TERMINAL_DENOMINATIONS.DenominationsToList(_denominations))

    _denominations = GeneralParam.GetString("EGM", "MultiDenominations").Replace(_db_separator, _local_separator)
    _denominations_list.AddRange(CLASS_TERMINAL_DENOMINATIONS.DenominationsToList(_denominations))

    _denominations_list.Add("--")

    Me.uc_denominations.Add(_denominations_list)

    Call Me.uc_denominations.CurrentRow(0)

    Call Me.uc_denominations.ReDraw(True)

  End Sub 'FillTableTypeFilterGrid

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As StringBuilder
    Dim _str_terminal_id As String
    Dim _selected_values As String()
    Dim _selected_values_string As String

    _str_where = New StringBuilder()
    _selected_values_string = ""

    ' Filter Providers - Terminals
    _str_terminal_id = " TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()
    _str_where.AppendLine(" AND " & _str_terminal_id)

    ' Filter Denominations
    If Me.uc_denominations.SelectedIndexes.Length <> Me.uc_denominations.Count() Then
      _selected_values = Me.uc_denominations.SelectedValuesArray
      For Each _item As String In _selected_values
        If _selected_values_string.Length > 0 Then
          _selected_values_string &= ","
        End If
        _selected_values_string &= "'" & _item & "'"
      Next
      _str_where.AppendLine(" AND TE_MULTI_DENOMINATION IN (" & _selected_values_string & ")  ")
    End If

    Return _str_where.ToString()

  End Function ' GetSqlWhere

  ' PURPOSE: Process subtotal value 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ProcesSubTotal(ByVal IsTotal As Boolean, ByVal IdxTotalColumn As Integer, ByVal ENUM_GUI As ControlColor.ENUM_GUI_COLOR, ByVal TextTotal As String)

    Dim _idx_row As Integer = Me.Grid.NumRows
    Dim _condition As String
    Dim _temp_money As Decimal
    Dim _temp_money_2 As Decimal

    If Not (Me.Grid.NumRows > 0 Or Me.cb_total.Checked) Then
      If IsTotal Then
        Call AddSubtotalWithoutValues(IdxTotalColumn)
      End If
      Return
    End If

    Me.Grid.AddRow()

    If IsTotal Then
      _condition = ""
    Else
      _condition = "DENOMINATION = '" & m_subtotal_break & "'"
      m_counter_list(COUNTER_SUBTOTALS) += 1
    End If

    ' Label - TOTAL
    Me.Grid.Cell(_idx_row, IdxTotalColumn).Value = TextTotal

    ' COLLECTION
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(COLLECTION)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(COLLECTION)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COLLECTION).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' BILL
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(BILL)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(BILL)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILL).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TICKET
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(TICKET)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(TICKET)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKET).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' MANUAL
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(MANUAL)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(MANUAL)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_MANUAL).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CREDIT_CANCEL
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(CREDIT_CANCEL)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(CREDIT_CANCEL)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' JACKPOT_DE_SALA
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(JACKPOT_DE_SALA)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(JACKPOT_DE_SALA)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_JACKPOT_DE_SALA).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVES
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(PROGRESIVES)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(PROGRESIVES)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVES).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NO_PROGRESIVES
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(NO_PROGRESIVES)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(NO_PROGRESIVES)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NO_PROGRESIVES).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVE_PROVISIONS
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(PROGRESIVE_PROVISIONS)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(PROGRESIVE_PROVISIONS)", _condition))
    Me.Grid.Cell(_idx_row, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TITO (TICKET)
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(TICKET)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(TICKET)", _condition))
    _temp_money_2 = _temp_money
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_TITO).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_BILL (BILL)
    _temp_money = IIf(m_query_as_datatable.Compute("SUM(BILL)", _condition) Is DBNull.Value, 0, m_query_as_datatable.Compute("SUM(BILL)", _condition))
    _temp_money_2 += _temp_money
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN_BILL).Value = GUI_FormatCurrency(_temp_money, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_temp_money_2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI)

  End Sub ' ProcesSubTotal

  Private Sub AddSubtotalWithoutValues(ByVal IdxTotalColumn As Integer)

    Me.Grid.AddRow()

    ' Label - TOTAL
    Me.Grid.Cell(0, IdxTotalColumn).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' COLLECTION
    Me.Grid.Cell(0, GRID_COLUMN_COLLECTION).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' BILL
    Me.Grid.Cell(0, GRID_COLUMN_BILL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' TICKET
    Me.Grid.Cell(0, GRID_COLUMN_TICKET).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' MANUAL
    Me.Grid.Cell(0, GRID_COLUMN_MANUAL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' CREDIT_CANCEL
    Me.Grid.Cell(0, GRID_COLUMN_CREDIT_CANCEL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' JACKPOT_DE_SALA
    Me.Grid.Cell(0, GRID_COLUMN_JACKPOT_DE_SALA).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVES
    Me.Grid.Cell(0, GRID_COLUMN_PROGRESIVES).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NO_PROGRESIVES
    Me.Grid.Cell(0, GRID_COLUMN_NO_PROGRESIVES).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' PROGRESIVE_PROVISIONS
    Me.Grid.Cell(0, GRID_COLUMN_PROGRESIVE_PROVISIONS).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_TITO (TICKET)
    Me.Grid.Cell(0, GRID_COLUMN_NETWIN_TITO).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN_BILL (BILL)
    Me.Grid.Cell(0, GRID_COLUMN_NETWIN_BILL).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' NETWIN
    Me.Grid.Cell(0, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Color Row
    Me.Grid.Row(0).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
  End Sub

  ' PURPOSE: Set column order 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_ORDER_DATE = 1
    GRID_INIT_TERMINAL_DATA = 2

    GRID_COLUMN_DENOMINATION = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_COLLECTION = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_BILL = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_TICKET = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_MANUAL = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_CREDIT_CANCEL = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_JACKPOT_DE_SALA = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_PROGRESIVES = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_NO_PROGRESIVES = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_PROGRESIVE_PROVISIONS = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_NETWIN_TITO = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_NETWIN_BILL = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_NETWIN = TERMINAL_DATA_COLUMNS + 14

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 15

  End Sub

#End Region   ' Private Functions

#Region " Events"

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region 'Events


End Class