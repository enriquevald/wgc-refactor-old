﻿'-------------------------------------------------------------------
' Copyright © 2007-2015 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_report_buckets_by_client
'
' DESCRIPTION:   This screen allows to view buckets by a client
' AUTHOR:        Jorge Concheyro
' CREATION DATE: 09-DIC-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' 09-DIC-2015  JRC    Initial version

'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Data
Imports System.Text
Imports WSI.Common

Public Class frm_report_buckets_by_client
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 1


  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ACCOUNT_ID As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT_HOLDER_NAME As Integer = 2


  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Width
  Private Const GRID_WIDTH_ACCOUNT_ID As Integer = 1250
  Private Const GRID_WIDTH_ACCOUNT_HOLDER_NAME As Integer = 3000

  ' Database codes
  Private Const FORM_DB_MIN_VERSION As Short = 156


  ' Colors for total and subtotal rows
  Private Const COLOR_ROW_SUBTOTAL As GUI_CommonMisc.ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
  Private Const COLOR_ROW_TOTAL As GUI_CommonMisc.ENUM_GUI_COLOR = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

  'Identifies this particular report in order to know which buckets shoul be visible
  Private Const BucketReportID As Buckets.BucketVisibility = Buckets.BucketVisibility.Report_BucketsByClient '256





#End Region ' Constants

#Region " Enums "

#End Region ' Enums

#Region " Members "

  ' For report filters 
  Private m_selectedaccounts As String
  Private m_account As String                      ' Account Id
  Private m_track_data As String                   ' Account track data (Card Number)
  Private m_holder_name As String                  ' Account Holder Name

  Dim Buckets_list As New List(Of Integer)()

  ' totalizers
  Dim m_total_bucket As New List(Of Double)()

  Dim m_pivot_columns As String '[1],[2],[3],[4],[5],[6],[7]




#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_REPORT_BUCKET_BY_CLIENT
    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    ' TITO Mode

    ' Form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6990) ' Reporte de Buckets por Cliente

    Me.Uc_account_sel1.InitExtendedQuery(False)
    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    Call PivotColumns()

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()


  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Account selection
    If Not Me.Uc_account_sel1.ValidateFormat() Then
      Return False
    End If

    ' Birthday and Wedding validate date
    If Not Me.Uc_account_sel1.IsBirthdayDateValid() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5881), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.Uc_account_sel1.SetFocus(uc_account_sel.ENUM_FOCUS.FOCUS_BIRTHDAY)

      Return False
    End If

    If Not Me.Uc_account_sel1.IsWeddingDateValid() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5881), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.Uc_account_sel1.SetFocus(uc_account_sel.ENUM_FOCUS.FOCUS_BIRTHDAY)

      Return False
    End If



    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder
    _sb = New System.Text.StringBuilder()

    ' CCG 16-SEP-2013: Massive Search
    If Not String.IsNullOrEmpty(Me.Uc_account_sel1.MassiveSearchNumbers) Then
      _sb.AppendLine(Me.Uc_account_sel1.CreateAndInsertAccountData())
    End If

    ' Get Select and from
    _sb.AppendLine(" SELECT    AC_ACCOUNT_ID                                             ")
    _sb.AppendLine("          ,AC_HOLDER_NAME,                                           ")
    _sb.AppendLine(m_pivot_columns)
    _sb.AppendLine("  FROM                                                               ")
    _sb.AppendLine("         ACCOUNTS A                                                  ")
    _sb.AppendLine("  LEFT JOIN                                                          ")
    _sb.AppendLine("         (                                                           ")
    _sb.AppendLine("           SELECT CB.CBU_CUSTOMER_ID, CB.CBU_BUCKET_ID, CBU_VALUE    ")
    _sb.AppendLine("           FROM CUSTOMER_BUCKET CB                                   ")
    _sb.AppendLine("         ) SRC                                                       ")
    _sb.AppendLine("         PIVOT                                                       ")
    _sb.AppendLine("         (                                                           ")
    _sb.AppendLine("           SUM(CBU_VALUE)                                            ")
    _sb.AppendLine("           FOR CBU_BUCKET_ID IN                                      ")
    _sb.AppendLine("           (                                                         ")
    _sb.AppendLine(m_pivot_columns)
    _sb.AppendLine("           )                                                         ")
    _sb.AppendLine("         ) PIV                                                       ")
    _sb.AppendLine("         ON PIV.CBU_CUSTOMER_ID = A.AC_ACCOUNT_ID                    ")

    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine(" ORDER BY A.AC_ACCOUNT_ID                                            ")


    'CCG 16-SET-2013: Add the account numbers selected in the massive search form.
    If Not String.IsNullOrEmpty(Me.Uc_account_sel1.MassiveSearchNumbers) Then
      If Me.Uc_account_sel1.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _sb.AppendLine(Me.Uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_ACCOUNT_ID"))
      Else
        _sb.AppendLine(Me.Uc_account_sel1.GetInnerJoinAccountMassiveSearch("AC_TRACK_DATA"))
      End If
    End If

    ' CCG 16-SEP-2013: Delete the temporary table
    If Not String.IsNullOrEmpty(Me.Uc_account_sel1.MassiveSearchNumbers) Then
      _sb.AppendLine(Me.Uc_account_sel1.DropTableAccountMassiveSearch())
    End If

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    InitTotals()
    Call GUI_StyleSheet()
  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer
    Dim _column_number As Integer = 3 'first column for buckets

    _idx_row = Me.Grid.AddRow()

    ' Label - TOTAL:
    Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_HOLDER_NAME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    For _idx As Integer = 0 To Buckets_list.Count() - 1

      If (m_total_bucket(_idx) <> 0) Then
        If (Buckets.GetBucketUnits(Buckets_list(_idx)) = Buckets.BucketUnits.Money) Then
          ' format money
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Value = GUI_FormatCurrency(m_total_bucket(_idx))
        End If

        If (Buckets.GetBucketUnits(Buckets_list(_idx)) = Buckets.BucketUnits.Points) Then
          ' format points
          Me.Grid.Cell(_idx_row, GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Value = GUI_FormatNumber(m_total_bucket(_idx), 0)
        End If
      End If


    Next

    ' Color Row
    Me.Grid.Row(_idx_row).BackColor = GetColor(COLOR_ROW_TOTAL)


    InitTotals()

  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' User Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
    ' Group Alarm
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_HOLDER_NAME)

    For _idx As Integer = 0 To Buckets_list.Count() - 1


      If Not (DbRow.Value(SQL_COLUMN_ACCOUNT_HOLDER_NAME + _idx + 1) Is DBNull.Value) Then
        If (Buckets.GetBucketUnits(Buckets_list(_idx)) = Buckets.BucketUnits.Money) Then
          ' format money
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_HOLDER_NAME + _idx + 1).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_ACCOUNT_HOLDER_NAME + _idx + 1))
        End If

        If (Buckets.GetBucketUnits(Buckets_list(_idx)) = Buckets.BucketUnits.Points) Then
          ' format points
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_HOLDER_NAME + _idx + 1).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_ACCOUNT_HOLDER_NAME + _idx + 1), 0)
        End If
      End If


    Next


    UpdateTotalizers(DbRow)

    Return True

  End Function ' GUI_SetupRow


  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.Uc_account_sel1
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Account selector filter
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(207), m_account)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(209), m_track_data)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(210), m_holder_name)



    '/TODO 
    'PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6904) & "  " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    'PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6904) & "  " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    'If Me.Uc_account_sel1.BirthDateIsVisible Then
    '  PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651), m_birth_date)
    'End If
    'If Me.Uc_account_sel1.WeddingDateIsVisible Then
    '  PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1652), m_wedding_date)
    'End If
    'If Me.Uc_account_sel1.TelephoneIsVisible Then
    '  PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(360), m_telephone)
    'End If



  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_account = String.Empty
    m_track_data = String.Empty
    m_holder_name = String.Empty

    ' Account filter
    m_account = Me.Uc_account_sel1.Account
    m_track_data = Me.Uc_account_sel1.TrackData
    m_holder_name = Me.Uc_account_sel1.Holder



  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Uc_account_sel1.Clear()
  End Sub ' Sub SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer
    _total_columns = Buckets_list.Count() + 3


    Dim _title As String = ""

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)
      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' AccountId  - 0
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = " "
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = "Cuenta" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6905)  '"Runner" TODO
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = GRID_WIDTH_ACCOUNT_ID

      ' Holder Name - 1
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Header(0).Text = " "
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Header(1).Text = "Holder" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(6906) '"Tipo"
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Width = GRID_WIDTH_ACCOUNT_HOLDER_NAME
      .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT_CENTER



      For _idx As Integer = 0 To Buckets_list.Count() - 1

        Select Case Buckets_list(_idx)
          Case Buckets.BucketType.RedemptionPoints
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7028)  ' "RedemptionPoints"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 1700
          Case Buckets.BucketType.RankingLevelPoints
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7029)  ' "RankingLevelPoints"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 1700
          Case Buckets.BucketType.Credit_NR
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7030)  ' "NR"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 1500
          Case Buckets.BucketType.Credit_RE
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7031)  ' "RE"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 1500
          Case Buckets.BucketType.Comp1_RedemptionPoints
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7032)  ' "Comp1_canje"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 2200
          Case Buckets.BucketType.Comp2_Credit_NR
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7033)  ' "Comp2_nr"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 2000
          Case Buckets.BucketType.Comp3_Credit_RE
            _title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7034)  ' "Comp2_re"
            .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Width = 2000
        End Select

        .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Header(0).Text = " "
        .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Header(1).Text = _title

        .Column(GRID_COLUMN_ACCOUNT_HOLDER_NAME + 1 + _idx).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      Next

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Updates subtotals and totals variables
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateTotalizers(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    For _idx As Integer = 0 To Buckets_list.Count() - 1
      If Not (DbRow.Value(_idx + 2) Is DBNull.Value) Then
        m_total_bucket(_idx) = m_total_bucket(_idx) + DbRow.Value(_idx + 2)
      End If
    Next

  End Sub ' UpdateTotalizers


  ' PURPOSE: Initializes totals variables 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitTotals()
    For _idx As Integer = 0 To Buckets_list.Count() - 1
      m_total_bucket(_idx) = 0
    Next
  End Sub ' InitTotals


  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""

    ' Filter Account
    _str_where = Me.Uc_account_sel1.GetFilterSQL()
    If _str_where <> "" Then
      _str_where = " WHERE " & " A.AC_ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE " & _str_where & ") "
    End If
    Return _str_where

  End Function ' GetSqlWhere

  Private Sub PivotColumns()
    Dim _sb As StringBuilder
    Dim _report_param As SqlParameter

    _report_param = New SqlParameter("@pReport", SqlDbType.Int)

    _sb = New StringBuilder()
    _sb.AppendLine(" SELECT   BU_BUCKET_ID     ")
    _sb.AppendLine("          FROM   BUCKETS   ")
    _sb.AppendLine("  WHERE   BU_VISIBLE_FLAGS & @pReport = @pReport")
    _sb.AppendLine("  ORDER BY BU_ORDER_ON_REPORTS ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString())
          _cmd.Parameters.Add("@pReport", SqlDbType.Int).Value = BucketReportID

          Using _reader As SqlDataReader = _db_trx.ExecuteReader(_cmd)
            While (_reader.Read())
              If Not _reader.IsDBNull(0) Then
                Buckets_list.Add(Convert.ToString(_reader.GetInt64(0)))
                m_total_bucket.Add(0)
              End If
            End While
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    m_pivot_columns = ""
    For _i As Integer = 0 To Buckets_list.Count - 1
      m_pivot_columns = m_pivot_columns + "[" + Buckets_list(_i).ToString + "]"
      If (Buckets_list.Count > 1) And (_i < Buckets_list.Count - 1) Then
        m_pivot_columns = m_pivot_columns + ","
      End If
    Next


  End Sub


#End Region

End Class