'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_profile_edit.vb
' DESCRIPTION:   Profile edition form
' AUTHOR:        Jaume Sala
' CREATION DATE: 03-JUL-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-JUL-2002  JSV    Initial version.
' 15-MAR-2012  RCI    In RefreshGrid, add a prefix word to the Report Forms.
' 06-MAR-2013  ICS    Added a new feature to limit the number of users per profile.
' 28-AUG-2013  JCA    Show CASHDESK_DRAWS_GUI forms permission
' 11-FEB-2014  DLL    Istats permission allowed in multisite
' 22-AUG-2014  AMF    Fixed Bug WIG-1197: Feature of form
' 08-JAN-2015  OPC    Added new option export to excel and print for profiles.
' 24-JUN-2016  ESE    Product Backlog Item 13581:Generic reports: Add Report Tool to GUI
' 24-NOV-2016  LTC    Bug 20996:WigosGUI: Is not posible to configure permissions on generic reports to mater profiles on SITE
' 08-DIC-2016  ESE    Bug 21294:WigosGUI: Hide generic reports to master profile
'--------------------------------------------------------------------

Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

#End Region

Public Class frm_profile_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_PROFILE_NAME As Integer = 40

  ' State
  Private Const STATE_ENABLED As Integer = 1
  Private Const STATE_DISABLED As Integer = 0

  ' Row type
  Private Const ROW_TYPE_GUI_HEADER As Integer = 1
  Private Const ROW_TYPE_FORM As Integer = 0


  Private Const GRID_COLUMN_ROW_TYPE As Integer = 0
  Private Const GRID_COLUMN_GROUP_INFO As Integer = 1
  Private Const GRID_COLUMN_GUI_ID As Integer = 2
  Private Const GRID_COLUMN_FORM_ID As Integer = 3
  Private Const GRID_COLUMN_GUI_NAME As Integer = 4
  Private Const GRID_COLUMN_FORM_NAME As Integer = 5
  Private Const GRID_COLUMN_READ_PERM As Integer = 6
  Private Const GRID_COLUMN_WRITE_PERM As Integer = 7
  Private Const GRID_COLUMN_DELETE_PERM As Integer = 8
  Private Const GRID_COLUMN_EXECUTE_PERM As Integer = 9
  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const ISTATS_GUI_ID As Integer = 104

  Friend WithEvents lbl_master_info As System.Windows.Forms.Label

#End Region 'Constants

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()
    Me.m_master = False

    Me.FormId = ENUM_FORM.FORM_PROFILE_EDIT

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    Me.FormId = FormId
    Me.m_master = True

    Call MyBase.GUI_SetFormId()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents ef_profile_name As GUI_Controls.uc_entry_field
  Friend WithEvents dg_privileges As GUI_Controls.uc_grid
  Friend WithEvents chk_users_limit As System.Windows.Forms.CheckBox
  Friend WithEvents ef_users_limit As GUI_Controls.uc_entry_field

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.dg_privileges = New GUI_Controls.uc_grid
    Me.ef_profile_name = New GUI_Controls.uc_entry_field
    Me.chk_users_limit = New System.Windows.Forms.CheckBox
    Me.ef_users_limit = New GUI_Controls.uc_entry_field
    Me.lbl_master_info = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_master_info)
    Me.panel_data.Controls.Add(Me.ef_users_limit)
    Me.panel_data.Controls.Add(Me.chk_users_limit)
    Me.panel_data.Controls.Add(Me.ef_profile_name)
    Me.panel_data.Controls.Add(Me.dg_privileges)
    Me.panel_data.Size = New System.Drawing.Size(899, 599)
    '
    'dg_privileges
    '
    Me.dg_privileges.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_privileges.CurrentCol = -1
    Me.dg_privileges.CurrentRow = -1
    Me.dg_privileges.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_privileges.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_privileges.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_privileges.Location = New System.Drawing.Point(0, 60)
    Me.dg_privileges.Name = "dg_privileges"
    Me.dg_privileges.PanelRightVisible = True
    Me.dg_privileges.Redraw = True
    Me.dg_privileges.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_privileges.Size = New System.Drawing.Size(896, 534)
    Me.dg_privileges.Sortable = False
    Me.dg_privileges.SortAscending = True
    Me.dg_privileges.SortByCol = 0
    Me.dg_privileges.TabIndex = 3
    Me.dg_privileges.ToolTipped = True
    Me.dg_privileges.TopRow = -2
    '
    'ef_profile_name
    '
    Me.ef_profile_name.DoubleValue = 0
    Me.ef_profile_name.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_profile_name.IntegerValue = 0
    Me.ef_profile_name.IsReadOnly = False
    Me.ef_profile_name.Location = New System.Drawing.Point(16, 9)
    Me.ef_profile_name.Name = "ef_profile_name"
    Me.ef_profile_name.OnlyUpperCase = True
    Me.ef_profile_name.Size = New System.Drawing.Size(448, 25)
    Me.ef_profile_name.SufixText = "Sufix Text"
    Me.ef_profile_name.SufixTextVisible = True
    Me.ef_profile_name.TabIndex = 0
    Me.ef_profile_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_profile_name.TextValue = ""
    Me.ef_profile_name.Value = ""
    Me.ef_profile_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_users_limit
    '
    Me.chk_users_limit.AutoSize = True
    Me.chk_users_limit.Location = New System.Drawing.Point(481, 12)
    Me.chk_users_limit.Name = "chk_users_limit"
    Me.chk_users_limit.Size = New System.Drawing.Size(219, 17)
    Me.chk_users_limit.TabIndex = 1
    Me.chk_users_limit.Text = "xLimitar el n�mero de usuarios a "
    Me.chk_users_limit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_users_limit.UseVisualStyleBackColor = True
    '
    'ef_users_limit
    '
    Me.ef_users_limit.DoubleValue = 0
    Me.ef_users_limit.Enabled = False
    Me.ef_users_limit.IntegerValue = 0
    Me.ef_users_limit.IsReadOnly = False
    Me.ef_users_limit.Location = New System.Drawing.Point(689, 9)
    Me.ef_users_limit.Name = "ef_users_limit"
    Me.ef_users_limit.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.ef_users_limit.Size = New System.Drawing.Size(38, 24)
    Me.ef_users_limit.SufixText = "Sufix Text"
    Me.ef_users_limit.SufixTextVisible = True
    Me.ef_users_limit.TabIndex = 2
    Me.ef_users_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_users_limit.TextValue = ""
    Me.ef_users_limit.TextVisible = False
    Me.ef_users_limit.TextWidth = 0
    Me.ef_users_limit.Value = ""
    Me.ef_users_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_master_info
    '
    Me.lbl_master_info.AutoSize = True
    Me.lbl_master_info.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_master_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_master_info.Location = New System.Drawing.Point(3, 42)
    Me.lbl_master_info.Name = "lbl_master_info"
    Me.lbl_master_info.Size = New System.Drawing.Size(94, 13)
    Me.lbl_master_info.TabIndex = 5
    Me.lbl_master_info.Text = "xMasterProfiles"
    Me.lbl_master_info.Visible = False
    '
    'frm_profile_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1001, 607)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_profile_edit"
    Me.Text = "#5272 "
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_input_profile_name As String
  Private m_delete_operation As Boolean
  Private m_continue_operation As Boolean
  Private m_cashier_bool As Boolean
  Private m_current_user_profile As CLASS_PROFILE
  Private m_master As Boolean

#End Region 'Members

#Region " Properties "

#End Region ' Properties

#Region " Public Functions "

  ' PURPOSE: Init form in new master mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowNewMaster()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_master = True

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewMaster

  ' PURPOSE: Init form in edit master mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '           - ProfileName
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowEditMaster(ByVal ProfileId As Integer, ByVal ProfileName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = ProfileId
    Me.m_input_profile_name = ProfileName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub 'ShowEditMaster

#End Region ' Public Functions

#Region " Override Functions "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    'Me.FormId = ENUM_FORM.FORM_PROFILE_EDIT

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Configuration.GUI_Configuration.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    'Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    'Add any initialization after the MyBase.GUI_InitControls() call

    If Me.m_master Then
      Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2103)
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
      If WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
        'ESE 08-DIC-2016
        GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = True
        GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = True
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
        GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
        ef_profile_name.Enabled = False
        chk_users_limit.Enabled = False
        ef_users_limit.Enabled = False
        lbl_master_info.Visible = True
        lbl_master_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2193)
      Else
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      End If
    Else
      Me.Text = GLB_NLS_GUI_CONFIGURATION.GetString(318)
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    End If

    ef_profile_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(319)       ' 319 "Profile"
    Call ef_profile_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROFILE_NAME)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(1)

    'add expand and collapse buttons to view the grid lines
    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_ADD).Enabled = True
    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_DELETE).Enabled = True

    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_ADD).Visible = True
    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_DELETE).Visible = True

    ' Init user limit controls
    Me.chk_users_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1761)
    Me.chk_users_limit.Checked = False
    Me.ef_users_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_users_limit.Enabled = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(5)  ' Excel
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(27) ' Print
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True

    ' Set Grid style
    Call GUI_StyleView()

  End Sub 'GUI_InitControls

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim profile_item As CLASS_PROFILE

    profile_item = DbReadObject
    ef_profile_name.Value = profile_item.ProfileName

    If profile_item.MaxUsers > 0 Then
      ef_users_limit.Value = profile_item.MaxUsers
      chk_users_limit.Checked = True
    End If

    Call RefreshGrid(profile_item)

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String
    Dim _profile_item As CLASS_PROFILE
    Dim _limit As Integer

    ' blank field
    If ef_profile_name.Value = "" Then
      ' 319 "Profile"
      ' 101 "You must input a value for %1."
      _nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      _nls_param1)
      Call ef_profile_name.Focus()

      Return False
    End If

    If Not CurrentUser.IsSuperUser _
   And Not Me.m_master _
   And Not m_current_user_profile Is Nothing Then
      Try
        _profile_item = DbEditedObject
        Call GetGridData(_profile_item)

        ' Check whether the permissions of the profile fit into the profile of the current user
        If Not _profile_item.CheckProfileFitsInto(m_current_user_profile) Then
          ' 135 "El perfil de usuario no puede ser guardado ya que concede permisos de acceso superiores a los permisos de acceso asignados al usuario %1."
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(135), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          , _
                          , _
                          CurrentUser.Name)

          Call dg_privileges.Focus()

          Return False
        End If

      Finally
      End Try
    End If

    ' Check that checkbox is checked and is not empty
    If Me.chk_users_limit.Checked And String.IsNullOrEmpty(ef_users_limit.Value) Then
      ' 1763 "Users limit"
      ' 101 "You must input a value for %1."
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1763)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), _
                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      , _
                      , _
                      _nls_param1)
      Call ef_users_limit.Focus()

      Return False
    End If

    ' Limit user is checked and is greater than 0
    If Me.chk_users_limit.Checked Then
      _limit = GUI_ParseNumber(ef_users_limit.Value)
      If _limit <= 0 Then
        ' 1763 Users limit
        ' 125 "You must enter a %1 value greater than 0."
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1763)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(125), _
                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        , _
                        , _
                        _nls_param1)
        Call ef_users_limit.Focus()

        Return False
      End If
    End If


    Return True

  End Function 'GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _profile_item As CLASS_PROFILE

    _profile_item = DbEditedObject
    _profile_item.ProfileName = ef_profile_name.Value

    _profile_item.MaxUsers = 0
    If chk_users_limit.Checked Then
      _profile_item.MaxUsers = GUI_ParseNumber(Me.ef_users_limit.Value)
    End If

    Call GetGridData(_profile_item)

  End Sub 'GUI_GetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim profile_class As CLASS_PROFILE
    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT
    Dim ctx As Integer

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROFILE
        profile_class = DbEditedObject
        profile_class.ProfileId = 0
        profile_class.GupMaster = Me.m_master
        DbStatus = ENUM_STATUS.STATUS_OK

        If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
          Try
            'XVV 17/04/2007
            'Comment this function, because are obsolete (Oracle version)
            'GUI_SqlCtxAlloc(ctx)
            Me.DbStatus = profile_class.DB_ReadProfileForms(0, ctx)
          Finally
            'XVV 17/04/2007
            'Comment this function, because are obsolete (Oracle version)
            'GUI_SqlCtxRelease(ctx)
          End Try

          If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
            profile_class = Me.DbEditedObject

            ' 129 "Error found retrieving the information of the existing forms."
            Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(129), _
                            ENUM_MB_TYPE.MB_TYPE_ERROR, _
                            ENUM_MB_BTN.MB_BTN_OK, _
                            ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
          End If
        End If

        ' Get the profile of the current user
        If Not CurrentUser.IsSuperUser Then
          Try
            m_current_user_profile = New CLASS_PROFILE
            m_current_user_profile.ProfileId = CurrentUser.ProfileId
            m_current_user_profile.DB_Read(m_current_user_profile.ProfileId, ctx)
          Finally
          End Try
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        profile_class = Me.DbEditedObject

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 319 "Profile"
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
          nls_param2 = m_input_profile_name
          ' 102 "Se ha producido un error al leer el %1 %2."
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        m_continue_operation = True
        profile_class = Me.DbEditedObject
        If Not UsefulProfile(profile_class) Then
          ' 134 "Creating a user profile with no access permissions is equivalent to disabling the user. Do you wish to continue?"
          rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(134), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO)

          If rc = mdl_NLS.ENUM_MB_RESULT.MB_RESULT_NO Then
            m_continue_operation = False
          End If
        Else
          Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_INSERT
        If m_continue_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          profile_class = Me.DbEditedObject
          ' 319 "Profile"
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
          nls_param2 = profile_class.ProfileName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(110), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_profile_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_continue_operation Then
          profile_class = Me.DbEditedObject
          ' 319 "Profile"
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
          nls_param2 = profile_class.ProfileName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)

        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        m_continue_operation = True
        profile_class = Me.DbEditedObject

        If Not UsefulProfile(profile_class) Then
          ' "Creating a user profile with no access permissions is equivalent to disabling the user. Do you wish to continue?"
          rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(134), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO)
          If rc = mdl_NLS.ENUM_MB_RESULT.MB_RESULT_NO Then
            m_continue_operation = False
          End If

        Else
          Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If m_continue_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          profile_class = Me.DbEditedObject
          ' 319 "Profile"
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
          nls_param2 = profile_class.ProfileName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(110), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)
          Call ef_profile_name.Focus()

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_continue_operation Then
          profile_class = Me.DbEditedObject
          ' 319 "Profile"
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
          nls_param2 = profile_class.ProfileName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)

        ElseIf Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          profile_class = Me.DbEditedObject
          If UsefulProfile(profile_class) Then
            Call MyBase.GUI_DB_Operation(frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
          End If

        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          profile_class = Me.DbEditedObject
          nls_param1 = profile_class.ProfileName
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(128), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        profile_class = Me.DbEditedObject
        ' 319 "Profile"
        nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
        nls_param2 = profile_class.ProfileName
        rc = NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(111), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1, _
                        nls_param2)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub 'GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

    Dim _profile_item As CLASS_PROFILE
    Dim profile_item As CLASS_PROFILE

    ' Only Super users can edit profiles
    '
    'If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
    '  AndPerm.Write = False
    '  AndPerm.Delete = False
    'End If
    'If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
    '  AndPerm.Read = False
    'End If

    If Not CurrentUser.IsSuperUser _
   And Not Me.m_master _
   And Not m_current_user_profile Is Nothing Then
      If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
        _profile_item = DbReadObject

        ' Check whether the permissions of the profile fit into the profile of the current user
        If Not _profile_item.CheckProfileFitsInto(m_current_user_profile) Then
          AndPerm.Write = False
          AndPerm.Delete = False

          ' 136 "El perfil de usuario %2 no podr� ser modificado ya que contiene permisos de acceso superiores a los permisos de acceso asignados al usuario %1."
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(136), _
                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          , _
                          , _
                          CurrentUser.Name, _
                          _profile_item.ProfileName)
        End If
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = AndPerm.Write
      End If
    End If

    If WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) Then
      profile_item = DbReadObject
      If profile_item.GupMaster Then
        Me.m_master = True
      Else
        Me.m_master = False
      End If
      AndPerm.Write = AndPerm.Write
      AndPerm.Delete = AndPerm.Delete
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = AndPerm.Write And Not profile_item.GupMaster
    End If

  End Sub 'GUI_Permissions

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call CustomButtonClickEvent()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_EXCEL)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub 'GUI_ButtonClick

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_profile_name
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_master = False

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProfileId
  '           - ProfileName
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowEditItem(ByVal ProfileId As Integer, ByVal ProfileName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = ProfileId
    Me.m_input_profile_name = ProfileName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub 'ShowEditItem

  Protected Overrides Sub GUI_ExcelResultList()
    Call ExportToExcel(False)
  End Sub ' GUI_ExcelResultList

  Protected Overrides Sub GUI_PrintResultList()
    Call ExportToExcel(True)
  End Sub ' GUI_PrintResultList

#End Region 'Override Functions

#Region " Private Functions "

  ' PURPOSE: Configure Grid columns and headers
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()


    Call dg_privileges.Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

    ' ROW_TYPE: Hidden colimn to diferentiate a gui-header row and a form row
    dg_privileges.Column(GRID_COLUMN_ROW_TYPE).Width = 0

    ' GROUP_INFO: Hidden. On 'gui-header row' contains idx of last row for this gui.
    ' On 'form row' contains idx of 'gui-header row'
    dg_privileges.Column(GRID_COLUMN_GROUP_INFO).Width = 0

    ' GUI_ID: Hidden
    dg_privileges.Column(GRID_COLUMN_GUI_ID).Width = 0

    ' FORM_ID: Hidden
    dg_privileges.Column(GRID_COLUMN_FORM_ID).Width = 0

    ' GUI_NAME
    dg_privileges.Column(GRID_COLUMN_GUI_NAME).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(331)
    dg_privileges.Column(GRID_COLUMN_GUI_NAME).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(331)
    dg_privileges.Column(GRID_COLUMN_GUI_NAME).Width = 2250
    dg_privileges.Column(GRID_COLUMN_GUI_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    dg_privileges.Column(GRID_COLUMN_GUI_NAME).IsMerged = True
    dg_privileges.Column(GRID_COLUMN_GUI_NAME).Editable = False

    ' FORM_NAME
    dg_privileges.Column(GRID_COLUMN_FORM_NAME).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(320)
    dg_privileges.Column(GRID_COLUMN_FORM_NAME).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(320)
    dg_privileges.Column(GRID_COLUMN_FORM_NAME).Width = 6100
    dg_privileges.Column(GRID_COLUMN_FORM_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    dg_privileges.Column(GRID_COLUMN_FORM_NAME).IsMerged = True
    dg_privileges.Column(GRID_COLUMN_FORM_NAME).Editable = False

    ' READ_PERM
    dg_privileges.Column(GRID_COLUMN_READ_PERM).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(312)
    dg_privileges.Column(GRID_COLUMN_READ_PERM).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(321)
    dg_privileges.Column(GRID_COLUMN_READ_PERM).WidthFixed = 900
    dg_privileges.Column(GRID_COLUMN_READ_PERM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    dg_privileges.Column(GRID_COLUMN_READ_PERM).IsMerged = False
    dg_privileges.Column(GRID_COLUMN_READ_PERM).Editable = True
    dg_privileges.Column(GRID_COLUMN_READ_PERM).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    ' WRITE_PERM
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(312)
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(322)
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).WidthFixed = 900
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).IsMerged = False
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).Editable = True
    dg_privileges.Column(GRID_COLUMN_WRITE_PERM).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    ' DELETE_PERM
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(312)
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(323)
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).WidthFixed = 900
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).IsMerged = False
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).Editable = True
    dg_privileges.Column(GRID_COLUMN_DELETE_PERM).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    ' EXECUTE_PERM
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).Header(0).Text = GLB_NLS_GUI_CONFIGURATION.GetString(312)
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(324)
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).WidthFixed = 900
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).IsMerged = False
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).Editable = True
    dg_privileges.Column(GRID_COLUMN_EXECUTE_PERM).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    'Rename the buttons of the grid to expand and collapse
    dg_privileges.RenameButtons(GLB_NLS_GUI_CONFIGURATION.GetString(7), GLB_NLS_GUI_CONFIGURATION.GetString(8))

  End Sub

  ' PURPOSE: Create a row per form and show its permisions
  '
  '  PARAMS:
  '     - INPUT:
  '           - Profile object which contains array of form permissions
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub RefreshGrid(ByVal profile_item As CLASS_PROFILE)

    Dim last_gui As Integer ' to know when insert caption row betwen guis
    Dim last_gui_row As Integer ' to save caption row for last gui to update permisions when all forms readed
    Dim idx_form As Integer
    Dim idx_row As Integer
    Dim gui_name As String
    Dim form_name As String
    Dim chk_read As String
    Dim chk_write As String
    Dim chk_delete As String
    Dim chk_execute As String

    Call Me.dg_privileges.Clear()
    Me.dg_privileges.Redraw = False

    ' Put a form per row on the grid
    last_gui = -1
    last_gui_row = -1
    For idx_form = 0 To profile_item.NumForms - 1

      ' Get GUI and FORM name from auditor data
      gui_name = profile_item.GuiName(profile_item.ProfileForms(idx_form).gui_id)
      form_name = profile_item.ProfileForms(idx_form).FormName

      'JBC 11-NOV-2013: Allow to replace Anti-Money Laundering Name.
      If form_name.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2909)) Then
        form_name = form_name.Replace(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2909), WSI.Common.Accounts.getAMLName())
      End If

      'JBC 27-NOV-2014: Allow to replace %1 to Tax name.
      If form_name.Contains(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5765)) Then
        form_name = form_name.Replace(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5766), WSI.Common.GeneralParam.GetString("Cashier", "Split.B.Tax.Name", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5766)))
      End If

      ' RCI 15-MAR-2012: If FormId is one of the Report Forms, add a word prefix to identify it.
      If profile_item.ProfileForms(idx_form).form_id >= ENUM_FORM.FORM_REPORTS_REPORT_000 And _
        profile_item.ProfileForms(idx_form).form_id <= ENUM_FORM.FORM_REPORTS_REPORT_999 Then
        form_name = GLB_NLS_REPORT.GetString(0) & " - " & form_name
      End If

      If Not IsVisibleProfileForm(profile_item.ProfileForms(idx_form).feature, profile_item.ProfileForms(idx_form).gui_id) Then

        Continue For
      End If

      If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And _
         Not Me.m_master And _
         profile_item.ProfileForms(idx_form).gui_id <> ENUM_GUI.MULTISITE_GUI Then

        'JCA CashDeskDraws
        If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
        Else
          Continue For
        End If
      End If

      If Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And _
         Not WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And _
         profile_item.ProfileForms(idx_form).gui_id = ENUM_GUI.MULTISITE_GUI Then

        Continue For
      End If

      ' First row for each gui is a caption row for all forms on this gui
      If profile_item.ProfileForms(idx_form).gui_id <> last_gui Then

        ' If it is not the first group, set header group info for previous group
        If last_gui_row <> -1 Then

          ' group_info column in a gui_header type row indicates row index of last form for this gui
          Me.dg_privileges.Cell(last_gui_row, GRID_COLUMN_GROUP_INFO).Value = idx_row

          ' computes check box status for header-row
          Call calcGroupPerm(last_gui_row, GRID_COLUMN_READ_PERM)
          Call calcGroupPerm(last_gui_row, GRID_COLUMN_WRITE_PERM)
          Call calcGroupPerm(last_gui_row, GRID_COLUMN_DELETE_PERM)
          Call calcGroupPerm(last_gui_row, GRID_COLUMN_EXECUTE_PERM)

        End If

        ' add the gui-header row
        idx_row = Me.dg_privileges.AddRow()

        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_READ_PERM).BackColor() = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_WRITE_PERM).BackColor() = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_DELETE_PERM).BackColor() = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_EXECUTE_PERM).BackColor() = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_02)

        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_ROW_TYPE).Value = ROW_TYPE_GUI_HEADER
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GROUP_INFO).Value = idx_row
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GUI_ID).Value = profile_item.ProfileForms(idx_form).gui_id
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_ID).Value = 0
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GUI_NAME).Value = gui_name
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).Value = ""
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_READ_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_WRITE_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_DELETE_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
        Me.dg_privileges.Cell(idx_row, GRID_COLUMN_EXECUTE_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED

        last_gui = profile_item.ProfileForms(idx_form).gui_id
        last_gui_row = idx_row

        'XVV 16/04/2007
        'Change Me.XXX for Frm_Profile_edit.XXX, compiler generate a warning
        Call Me.dg_privileges.Row(idx_row).SetSignalAllowExpand(GRID_COLUMN_FORM_NAME)

      End If

      ' Computes check box status for each permission
      If profile_item.ProfileForms(idx_form).read_perm = CLASS_PROFILE.ENUM_PERMISION.ALLOWED Then
        chk_read = GUI_Controls.uc_grid.GRID_CHK_CHECKED
      Else
        chk_read = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
      End If

      If profile_item.ProfileForms(idx_form).write_perm = CLASS_PROFILE.ENUM_PERMISION.ALLOWED Then
        chk_write = GUI_Controls.uc_grid.GRID_CHK_CHECKED
      Else
        chk_write = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
      End If

      If profile_item.ProfileForms(idx_form).delete_perm = CLASS_PROFILE.ENUM_PERMISION.ALLOWED Then
        chk_delete = GUI_Controls.uc_grid.GRID_CHK_CHECKED
      Else
        chk_delete = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
      End If

      If profile_item.ProfileForms(idx_form).execute_perm = CLASS_PROFILE.ENUM_PERMISION.ALLOWED Then
        chk_execute = GUI_Controls.uc_grid.GRID_CHK_CHECKED
      Else
        chk_execute = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED
      End If

      ' add a row for this form
      idx_row = Me.dg_privileges.AddRow()
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_ROW_TYPE).Value = ROW_TYPE_FORM
      ' group_info column in a form_type row indicates row index of gui-header for this gui
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GROUP_INFO).Value = last_gui_row
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GUI_ID).Value = profile_item.ProfileForms(idx_form).gui_id
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_ID).Value = profile_item.ProfileForms(idx_form).form_id
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GUI_NAME).Value = gui_name
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).Value = form_name
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_READ_PERM).Value = chk_read
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_WRITE_PERM).Value = chk_write
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_DELETE_PERM).Value = chk_delete
      Me.dg_privileges.Cell(idx_row, GRID_COLUMN_EXECUTE_PERM).Value = chk_execute

      Me.dg_privileges.Row(idx_row).Height = 0

      'FJC 17-02-2015
      If profile_item.ProfileForms(idx_form).gui_id = GUI_CommonMisc.ENUM_GUI.SMARTFLOOR Then
        Select Case profile_item.ProfileForms(idx_form).form_id
          Case GUI_Controls.ENUM_LAYOUT_FORM.FORM_PCA_DASHBOARD To _
               GUI_Controls.ENUM_LAYOUT_FORM.FORM_PCA_CHAT 'PCA

            Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_03)

          Case GUI_Controls.ENUM_LAYOUT_FORM.FORM_OPS_DASHBOARD To _
               GUI_Controls.ENUM_LAYOUT_FORM.FORM_OPS_CHAT 'OPS

            Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)

          Case GUI_Controls.ENUM_LAYOUT_FORM.FORM_SLO_DASHBOARD To _
               GUI_Controls.ENUM_LAYOUT_FORM.FORM_SLO_CHAT  'SLO

            Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_01)

          Case GUI_Controls.ENUM_LAYOUT_FORM.FORM_TCH_DASHBOARD To _
               GUI_Controls.ENUM_LAYOUT_FORM.FORM_TCH_CHAT 'TCH

            Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00)

          Case GUI_Controls.ENUM_LAYOUT_FORM.FORM_ADM_CONFIGURATION To _
               GUI_Controls.ENUM_LAYOUT_FORM.FORM_ADM_CAPTION_EDIT          'ADM

            Me.dg_privileges.Cell(idx_row, GRID_COLUMN_FORM_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
        End Select
      End If

    Next

    ' set gui-header info for last gui group
    If last_gui_row <> -1 Then

      ' group_info column in a gui_header type row indicates row index of last form for this gui
      Me.dg_privileges.Cell(last_gui_row, GRID_COLUMN_GROUP_INFO).Value = idx_row

      ' computes check box status for header-row
      Call calcGroupPerm(last_gui_row, GRID_COLUMN_READ_PERM)
      Call calcGroupPerm(last_gui_row, GRID_COLUMN_WRITE_PERM)
      Call calcGroupPerm(last_gui_row, GRID_COLUMN_DELETE_PERM)
      Call calcGroupPerm(last_gui_row, GRID_COLUMN_EXECUTE_PERM)

    End If

    Me.dg_privileges.Redraw = True

  End Sub

  ' PURPOSE: updates array of form permission with values of grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Profile object which contains array of form permissions
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub GetGridData(ByVal profile_item As CLASS_PROFILE)

    Dim last_gui As Integer
    Dim idx_form As Integer
    Dim idx_row As Integer

    idx_row = 0
    last_gui = 0
    For idx_form = 0 To profile_item.NumForms - 1

      If Not IsVisibleProfileForm(profile_item.ProfileForms(idx_form).feature, profile_item.ProfileForms(idx_form).gui_id) Then

        Continue For
      End If

      If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And _
         Not Me.m_master And _
         profile_item.ProfileForms(idx_form).gui_id <> ENUM_GUI.MULTISITE_GUI Then

        'JCA CashDeskDraws
        If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
        Else
          Continue For
        End If
      End If

      If WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And _
         Me.m_master And _
         profile_item.ProfileForms(idx_form).gui_id = ISTATS_GUI_ID Then

        Continue For
      End If

      If Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) And _
         Not WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And _
         profile_item.ProfileForms(idx_form).gui_id = ENUM_GUI.MULTISITE_GUI Then

        Continue For
      End If

      ' Fist row for each gui is a caption row for all forms on this gui. Jump it.
      If Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GUI_ID).Value <> last_gui Then
        last_gui = Me.dg_privileges.Cell(idx_row, GRID_COLUMN_GUI_ID).Value
        idx_row = idx_row + 1
      End If

      ' Update permissions values depending on checked status of check box.
      ' Note: Number and order of rows is fixed and can't change. Each row is related to each form into array.
      profile_item.ProfileForms(idx_form).read_perm = _
        IIf(Me.dg_privileges.Cell(idx_row, GRID_COLUMN_READ_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED, _
          CLASS_PROFILE.ENUM_PERMISION.ALLOWED, _
          CLASS_PROFILE.ENUM_PERMISION.NOT_ALLOWED)

      profile_item.ProfileForms(idx_form).write_perm = _
        IIf(Me.dg_privileges.Cell(idx_row, GRID_COLUMN_WRITE_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED, _
          CLASS_PROFILE.ENUM_PERMISION.ALLOWED, _
          CLASS_PROFILE.ENUM_PERMISION.NOT_ALLOWED)

      profile_item.ProfileForms(idx_form).delete_perm = _
        IIf(Me.dg_privileges.Cell(idx_row, GRID_COLUMN_DELETE_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED, _
          CLASS_PROFILE.ENUM_PERMISION.ALLOWED, _
          CLASS_PROFILE.ENUM_PERMISION.NOT_ALLOWED)

      profile_item.ProfileForms(idx_form).execute_perm = _
        IIf(Me.dg_privileges.Cell(idx_row, GRID_COLUMN_EXECUTE_PERM).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED, _
          CLASS_PROFILE.ENUM_PERMISION.ALLOWED, _
          CLASS_PROFILE.ENUM_PERMISION.NOT_ALLOWED)

      idx_row = idx_row + 1

    Next

  End Sub

  ' PURPOSE: Computes if all forms of gui are cheched, all unchecked or some checked and some unchecked
  '
  '  PARAMS:
  '     - INPUT:
  '           - gui_header_row: row index for header row for which compute status
  '           - column: column index of permission to recalculate
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub calcGroupPerm(ByVal gui_header_row As Integer, ByVal column As Integer)

    Dim idx_row As Integer
    Dim first_form_row As Integer
    Dim last_form_row As Integer
    Dim cell_status As String
    Dim cashier_column As Integer

    If m_cashier_bool Then
      For cashier_column = GRID_COLUMN_READ_PERM To GRID_COLUMN_EXECUTE_PERM
        first_form_row = gui_header_row + 1
        last_form_row = dg_privileges.Cell(gui_header_row, GRID_COLUMN_GROUP_INFO).Value
        cell_status = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED

        idx_row = first_form_row
        While idx_row <= last_form_row

          If idx_row = first_form_row Then
            ' initial value set to firs form value  
            cell_status = dg_privileges.Cell(idx_row, column).Value
          Else
            ' if any value is different of first one, then group checkbox status is set to 'some checked-some unckecked'
            If cell_status <> dg_privileges.Cell(idx_row, column).Value Then
              cell_status = GUI_Controls.uc_grid.GRID_CHK_CHECKED_DISABLED
              Exit While
            End If
          End If
          idx_row = idx_row + 1

        End While

        dg_privileges.Cell(gui_header_row, cashier_column).Value = cell_status
      Next
    Else
      first_form_row = gui_header_row + 1
      last_form_row = dg_privileges.Cell(gui_header_row, GRID_COLUMN_GROUP_INFO).Value
      cell_status = GUI_Controls.uc_grid.GRID_CHK_UNCHECKED

      idx_row = first_form_row
      While idx_row <= last_form_row

        If idx_row = first_form_row Then
          ' initial value set to firs form value  
          cell_status = dg_privileges.Cell(idx_row, column).Value
        Else
          ' if any value is different of first one, then group checkbox status is set to 'some checked-some unckecked'
          If cell_status <> dg_privileges.Cell(idx_row, column).Value Then
            cell_status = GUI_Controls.uc_grid.GRID_CHK_CHECKED_DISABLED
            Exit While
          End If
        End If
        idx_row = idx_row + 1

      End While

      dg_privileges.Cell(gui_header_row, column).Value = cell_status
    End If

  End Sub

  'PURPOSE: Check if the profile edited has at least one checkebox checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - Profile object
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean

  Private Function UsefulProfile(ByVal ProfileItem As CLASS_PROFILE) As Boolean

    Dim idx_form As Integer

    idx_form = 0

    UsefulProfile = False

    While (idx_form <= ProfileItem.NumForms - 1) And Not UsefulProfile
      UsefulProfile = ProfileItem.ProfileForms(idx_form).read_perm _
                   Or ProfileItem.ProfileForms(idx_form).write_perm _
                   Or ProfileItem.ProfileForms(idx_form).delete_perm _
                   Or ProfileItem.ProfileForms(idx_form).execute_perm

      idx_form = idx_form + 1
    End While

    Return UsefulProfile

  End Function

  ' PURPOSE: Select a profile and update edited profile with permisions of selected profile
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub CustomButtonClickEvent()

    Dim profile_edited As CLASS_PROFILE
    Dim profile_source As CLASS_PROFILE
    Dim profiles_selected() As Integer
    Dim frm_p As frm_user_profiles_sel
    Dim rc As ENUM_CONFIGURATION_RC
    Dim nls_param1 As String
    Dim nls_param2 As String

    ' Show Profile selection form to select a profile from which get permisions
    frm_p = New frm_user_profiles_sel
    profiles_selected = frm_p.ShowForSelect(frm_user_profiles_sel.ENUM_RESULT_TYPE.RT_PROFILE, Me.m_master)

    If Not IsNothing(profiles_selected) Then
      If profiles_selected.Length > 0 Then
        Dim ctx As Integer
        ' read from database forms permisions for that profile
        profile_source = New CLASS_PROFILE

        Try
          'XVV 17/04/2007
          'Comment this function, because are obsolete (Oracle version)
          'GUI_SqlCtxAlloc(ctx)
          rc = profile_source.DB_Read(profiles_selected(0), ctx)
        Finally
          'XVV 17/04/2007
          'Comment this function, because are obsolete (Oracle version)
          'GUI_SqlCtxRelease(ctx)
        End Try

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 319 "Profile"
          nls_param1 = GLB_NLS_GUI_CONFIGURATION.GetString(319)
          nls_param2 = profiles_selected(0)
          ' 102 "Se ha producido un error al leer el %1 %2."
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1, _
                          nls_param2)

          Return
        End If

        ' Copy array of forms permisions into edited object
        profile_edited = Me.DbEditedObject

        If profile_source.NumForms > 0 Then
          ReDim profile_edited.ProfileForms(profile_source.ProfileForms.Length - 1)
          profile_source.ProfileForms.CopyTo(profile_edited.ProfileForms, 0)
        Else
          profile_edited.ProfileForms = Nothing
        End If

        ' to prevent 'double click' send a 'click event' to grid
        Call ef_profile_name.Focus()
        Call Application.DoEvents()

        ' Show changes on screen
        Call RefreshGrid(profile_edited)

      End If
    End If

  End Sub

  Private Function RowIsHeader(ByVal RowIndex As Integer) As Boolean
    If Me.dg_privileges.Cell(RowIndex, GRID_COLUMN_ROW_TYPE).Value <> ROW_TYPE_GUI_HEADER Then

      Return False
    End If

    Return True
  End Function

#End Region 'Private Functions

#Region " Events "

  ' PURPOSE: set checked status of gui-header row or set checked status for all forms of gui
  '
  '  PARAMS:
  '     - INPUT:
  '           - row and column changed
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub dg_privileges_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_privileges.CellDataChangedEvent

    Dim gui_header_row As Integer
    Dim first_form_row As Integer
    Dim last_form_row As Integer
    Dim group_info As Integer
    Dim idx_row As Integer
    Dim row_type As Integer
    Dim cell_status As String
    Dim cashier_column As Integer

    Me.dg_privileges.Redraw = False
    m_cashier_bool = False

    row_type = dg_privileges.Cell(Row, GRID_COLUMN_ROW_TYPE).Value
    group_info = dg_privileges.Cell(Row, GRID_COLUMN_GROUP_INFO).Value

    ' LTC 24-NOV-2016
    ' ESE 08-DIC-2016
    If (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And Me.m_master) _
      And Not (Me.dg_privileges.Cell(Row, GRID_COLUMN_FORM_ID).Value >= 11000 And Me.dg_privileges.Cell(Row, GRID_COLUMN_FORM_ID).Value <= 12000) _
      And Not (Me.dg_privileges.Cell(Row, GRID_COLUMN_FORM_ID).Value >= 566 And Me.dg_privileges.Cell(Row, GRID_COLUMN_FORM_ID).Value <= 581) Then

      If dg_privileges.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED Then
        dg_privileges.Cell(Row, Column).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_privileges.Cell(Row, Column).Value = uc_grid.GRID_CHK_CHECKED
      End If

      Me.dg_privileges.Redraw = True
      Return
    End If

    If Me.dg_privileges.Cell(Row, GRID_COLUMN_GUI_ID).Value = ENUM_GUI.CASHIER Then
      m_cashier_bool = True
      For cashier_column = GRID_COLUMN_READ_PERM To GRID_COLUMN_EXECUTE_PERM
        If row_type = ROW_TYPE_GUI_HEADER Then

          ' Set value of all forms to the same value of header
          first_form_row = Row + 1
          last_form_row = group_info
          cell_status = Me.dg_privileges.Cell(Row, Column).Value

          For idx_row = first_form_row To last_form_row
            dg_privileges.Cell(idx_row, cashier_column).Value = cell_status
          Next

        Else

          ' Computes common status for all forms in the group
          gui_header_row = group_info
          calcGroupPerm(gui_header_row, Column)

        End If

        ' Leave actual cell Selected
        dg_privileges.Cell(Row, cashier_column).Value = dg_privileges.Cell(Row, Column).Value
      Next
    Else
      If row_type = ROW_TYPE_GUI_HEADER Then

        ' Set value of all forms to the same value of header
        first_form_row = Row + 1
        last_form_row = group_info
        cell_status = Me.dg_privileges.Cell(Row, Column).Value

        For idx_row = first_form_row To last_form_row
          dg_privileges.Cell(idx_row, Column).Value = cell_status
        Next

      Else

        If Me.dg_privileges.Cell(Row, GRID_COLUMN_FORM_ID).Value >= 11000 And Me.dg_privileges.Cell(Row, GRID_COLUMN_FORM_ID).Value <= 11100 Then
          For cashier_column = GRID_COLUMN_READ_PERM To GRID_COLUMN_EXECUTE_PERM
            ' Computes common status for all forms in the group
            gui_header_row = group_info
            calcGroupPerm(gui_header_row, Column)

            ' Leave actual cell Selected
            dg_privileges.Cell(Row, cashier_column).Value = dg_privileges.Cell(Row, Column).Value
          Next
        Else
          ' Computes common status for all forms in the group
          gui_header_row = group_info
          calcGroupPerm(gui_header_row, Column)

          ' Leave actual cell Selected
          dg_privileges.Cell(Row, Column).Value = dg_privileges.Cell(Row, Column).Value
        End If
      End If

    End If

    Me.dg_privileges.Redraw = True

  End Sub

  ' PURPOSE: Activated when one of the buttons in the data grid (expand / collapse) has been pressed
  '
  '  PARAMS:
  '     - INPUT:
  '           - Pressed Button
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub dg_privileges_ButtonClickEvent(ByVal ButtonId As GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE) Handles dg_privileges.ButtonClickEvent

    Dim idx As Integer

    'XVV 16/04/2007
    'Unnused variable, disabled declaration because compiler generate a warning
    'Dim row_type As Integer

    Dim last_form_row As Integer
    Dim redraw_status As Boolean
    Dim ActionCollapse As GUI_Controls.uc_grid.ENUM_GRID_EXPAND_COLLAPSE

    redraw_status = Me.dg_privileges.Redraw
    Me.dg_privileges.Redraw = False

    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_ADD).Enabled = False
    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_DELETE).Enabled = False

    'Expand using Add Button, Collapse using Delete button.
    Select Case ButtonId
      Case uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_ADD
        ActionCollapse = uc_grid.ENUM_GRID_EXPAND_COLLAPSE.GRID_EXPAND_ALL
      Case uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_DELETE
        ActionCollapse = uc_grid.ENUM_GRID_EXPAND_COLLAPSE.GRID_COLLAPSE_ALL
      Case Else
        ActionCollapse = uc_grid.ENUM_GRID_EXPAND_COLLAPSE.GRID_EXPAND_COLLAPSE_NOTHING
    End Select

    For idx = 0 To Me.dg_privileges.NumRows - 1
      If RowIsHeader(idx) Then
        last_form_row = dg_privileges.Cell(idx, GRID_COLUMN_GROUP_INFO).Value
        dg_privileges.CollapseExpand(idx, last_form_row, GRID_COLUMN_FORM_NAME, ActionCollapse)
      End If
    Next

    Me.dg_privileges.Redraw = redraw_status

    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_ADD).Enabled = True
    Me.dg_privileges.Button(uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_DELETE).Enabled = True

  End Sub 'dg_privileges_ButtonClickEvent

  ' PURPOSE: Activated when a row of the grid title is double clicked
  '          in order to expand / collapse the forms rows of a specific Gui.
  '  PARAMS:
  '     - INPUT:
  '           - System.EventArgs
  '           - sender
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub dg_privileges_DataSelectedEvent() Handles dg_privileges.DataSelectedEvent

    Dim row_type As Integer
    Dim gui_header_row As Integer
    Dim last_form_row As Integer

    'set params
    row_type = dg_privileges.Cell(dg_privileges.CurrentRow, GRID_COLUMN_ROW_TYPE).Value
    last_form_row = dg_privileges.Cell(dg_privileges.CurrentRow, GRID_COLUMN_GROUP_INFO).Value
    gui_header_row = dg_privileges.CurrentRow

    If row_type = ROW_TYPE_GUI_HEADER Then
      dg_privileges.CollapseExpand(gui_header_row, last_form_row, GRID_COLUMN_FORM_NAME)
    End If

  End Sub

  ' PURPOSE: Activated when checkbox status changes and sets to enabled ef_users_limit
  '  PARAMS:
  '     - INPUT:
  '           - System.EventArgs
  '           - sender
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub chk_users_limit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_users_limit.CheckedChanged
    ef_users_limit.Enabled = chk_users_limit.Checked And chk_users_limit.Enabled
  End Sub

  ' PURPOSE: Export the main grid to Excel file.
  '  PARAMS:
  '     - INPUT:
  '           - WithPreview
  '     - OUTPUT:
  '           - none
  ' RETURNS:
  '     - none
  Private Sub ExportToExcel(ByVal WithPreview As Boolean)
    Dim _excel_data As GUI_Reports.CLASS_EXCEL_DATA
    Dim _ds_profile As DataSet
    Dim _header_title As String
    Dim _filter_title As String
    Dim _profile_title As String

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _excel_data = Nothing
    _ds_profile = Nothing

    Try
      _excel_data = New GUI_Reports.CLASS_EXCEL_DATA(WithPreview, WithPreview)
      _ds_profile = New DataSet()

      If _excel_data.IsNull() Then
        Return
      End If

      Call LoadGridInToDataSet(_ds_profile)

      _header_title = Environment.MachineName _
                     & "  " & GLB_NLS_GUI_CONTROLS.GetString(377) _
                     & ": " & GUI_FormatDate(WSI.Common.WGDB.Now(), _
                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                              ENUM_FORMAT_TIME.FORMAT_HHMMSS) _
                     & "  " & GLB_NLS_GUI_CONTROLS.GetString(378) _
                     & ": " & CurrentUser.Name

      If (chk_users_limit.Checked) Then
        _filter_title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5825) & ": " & GLB_NLS_GUI_CONFIGURATION.GetString(264) & " (" & ef_users_limit.Value & ")"
      Else
        _filter_title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5825) & ": " & GLB_NLS_GUI_CONFIGURATION.GetString(265)
      End If

      _profile_title = GLB_NLS_GUI_CONFIGURATION.GetString(319) & ": " & ef_profile_name.Value

      _excel_data.PrintDateTime = GUI_FormatDate(WSI.Common.WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      _excel_data.PrintUser = CurrentUser.Name & "@" & Environment.MachineName
      _excel_data.Title = Me.Text
      _excel_data.Site = GetSiteOrMultiSiteName()
      _excel_data.PrintPrivileges(_ds_profile.Tables(0), _
                                   _header_title, _
                                   _profile_title, _
                                   _filter_title, _
                                   WithPreview)
    Catch _ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "frm_profile_edit.ExportToExcel()", _
                            "", _
                            "", _
                            "", _
                           "Error in Excel data export.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            _ex.Message)

    Finally
      Call SafeDispose(_ds_profile)
      Call SafeDispose(_excel_data)

      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub

  Private Sub LoadGridInToDataSet(ByRef DataSetProfile As DataSet)
    Dim _profile_table As DataTable
    Dim _profile_row As DataRow

    _profile_table = New DataTable()

    _profile_table.Columns.Add("gui_name")
    _profile_table.Columns.Add("form_name")
    _profile_table.Columns.Add("read_perm")
    _profile_table.Columns.Add("write_perm")
    _profile_table.Columns.Add("delete_perm")
    _profile_table.Columns.Add("execute_perm")

    _profile_row = _profile_table.Rows.Add()
    _profile_row.Item("gui_name") = GLB_NLS_GUI_CONFIGURATION.GetString(331)
    _profile_row.Item("form_name") = GLB_NLS_GUI_CONFIGURATION.GetString(320)
    _profile_row.Item("read_perm") = GLB_NLS_GUI_CONFIGURATION.GetString(312)

    _profile_row = _profile_table.Rows.Add()
    _profile_row.Item("read_perm") = GLB_NLS_GUI_CONFIGURATION.GetString(321)
    _profile_row.Item("write_perm") = GLB_NLS_GUI_CONFIGURATION.GetString(322)
    _profile_row.Item("delete_perm") = GLB_NLS_GUI_CONFIGURATION.GetString(323)
    _profile_row.Item("execute_perm") = GLB_NLS_GUI_CONFIGURATION.GetString(324)

    For _idx_row As Integer = 0 To Me.dg_privileges.NumRows - 1
      If (Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_ROW_TYPE).Value.ToString() <> "1") Then
        _profile_row = _profile_table.Rows.Add()

        _profile_row.Item("gui_name") = Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_GUI_NAME).Value
        _profile_row.Item("form_name") = Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_FORM_NAME).Value

        If (Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_READ_PERM).Value = uc_grid.GRID_CHK_CHECKED) Then
          _profile_row.Item("read_perm") = "X"
        Else
          _profile_row.Item("read_perm") = String.Empty
        End If

        If (Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_WRITE_PERM).Value = uc_grid.GRID_CHK_CHECKED) Then
          _profile_row.Item("write_perm") = "X"
        Else
          _profile_row.Item("write_perm") = String.Empty
        End If

        If (Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_DELETE_PERM).Value = uc_grid.GRID_CHK_CHECKED) Then
          _profile_row.Item("delete_perm") = "X"
        Else
          _profile_row.Item("delete_perm") = String.Empty
        End If

        If (Me.dg_privileges.Cell(_idx_row, GRID_COLUMN_EXECUTE_PERM).Value = uc_grid.GRID_CHK_CHECKED) Then
          _profile_row.Item("execute_perm") = "X"
        Else
          _profile_row.Item("execute_perm") = String.Empty
        End If
      End If
    Next

    DataSetProfile.Tables.Add(_profile_table)
  End Sub ' LoadGridInToDataSet

  Private Sub SafeDispose(ByRef Obj As Object, Optional ByVal TryDispose As Boolean = False)
    If TryDispose AndAlso Obj IsNot Nothing AndAlso TypeOf Obj Is IDisposable Then
      CType(Obj, IDisposable).Dispose()
    End If
    Obj = Nothing
  End Sub

#End Region   'Events

End Class