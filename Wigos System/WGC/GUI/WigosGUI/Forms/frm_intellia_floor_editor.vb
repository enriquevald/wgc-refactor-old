﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_intellia_floor_editor
' DESCRIPTION:   Edit Intellia Floors
'                   
' AUTHOR:        Gustavo Daniel Alí
' CREATION DATE: 27-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-MAR-2017  GDA    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_intellia_floor_editor
  Inherits frm_base

#Region " Members "

#End Region

#Region " Properties "

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_INTELLIA_FLOOR_EDITOR
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize frm_mobile_backend form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Dim appName As String
    Dim _url As String
    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8002)  ' 1 "Editor de Planos"
    appName = Process.GetCurrentProcess().ProcessName + ".exe"
    SetIE8KeyforWebBrowserControl(appName)
    _url = WSI.Common.GeneralParam.GetString("Intellia", "Editor.Url") + "?q=NabAnjjeZiTepilAHxxBeNN"
    wb_floor_editor.Navigate(_url)

  End Sub ' GUI_InitControls

#End Region

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

#End Region

#Region " Events "

#End Region

  Public Function Isx64Architecture() As Boolean
    Dim architecture As String = String.Empty

    Dim mgmt As New System.Management.ManagementClass("Win32_Processor")
    Dim objCol As System.Management.ManagementObjectCollection = mgmt.GetInstances()

    For Each obj As System.Management.ManagementObject In objCol
      If architecture = String.Empty Then
        architecture = obj.Properties("Architecture").Value.ToString()
      End If
    Next

    If Convert.ToInt32(architecture) = 9 Then
      Return True
    Else
      Return False
    End If

  End Function

  Private Sub wb_floor_editor_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles wb_floor_editor.DocumentCompleted

    'With wb_floor_editor
    '  .Document.All("loginBox_UserName").SetAttribute("Value", CurrentUser.Name)
    '  .Document.All("loginBox_Password").SetAttribute("Value", CurrentUser.Password)
    '  .Document.All("loginBox_Button1").InvokeMember("click")
    'End With
  End Sub

  Private Sub SetIE8KeyforWebBrowserControl(appName As String)
    Dim Regkey As Microsoft.Win32.RegistryKey = Nothing

    Try

      Dim ieVersion As Integer = 10001 'Renders as IE10 Standards mode, prevents entering quirk mode
      Regkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", True)

      ' If the path is not correct or if the user haven't priviledges to access the registry
      If Regkey Is Nothing Then

        Try
            Regkey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION")
            Regkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", True)
        Catch ex As Exception
            Throw New Exception("No access to Windows Registry. Contact support.")
        End Try

      End If

      Dim keyValue As Object = Regkey.GetValue(appName)
      Dim keyValueString As String = ""

      If Not keyValue Is Nothing Then
          keyValueString = keyValue.ToString()
      End If

      Dim IE10KeyValue = ieVersion.ToString()

      ' Check if key value is already present
      If keyValueString = IE10KeyValue Then
        Regkey.Close()
        Return
      End If

      ' If a key is not present add the key, Key value 8000 (decimal)
      If String.IsNullOrEmpty(keyValueString) Or keyValueString <> IE10KeyValue Then
        Regkey.SetValue(appName, ieVersion, Microsoft.Win32.RegistryValueKind.DWord)

        ' Check for the key after adding
        keyValue = (Regkey.GetValue(appName)).ToString()

        If keyValue <> IE10KeyValue Then
            Throw New Exception("No access to Windows Registry. Contact support.")
        End If
      End If
    Catch ex As Exception

      MessageBox.Show("Application Settings Failed: " + vbCrLf + ex.Message)

    Finally

      ' Close the Registry
      If Not Regkey Is Nothing Then
        Regkey.Close()
      End If

    End Try
  End Sub

End Class