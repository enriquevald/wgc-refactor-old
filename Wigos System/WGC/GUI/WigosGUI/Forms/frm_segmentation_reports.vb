'------------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'------------------------------------------------------------------------
'
' MODULE NAME:   frm_segmentation_reports
' DESCRIPTION:   Reports of Segmentation
' AUTHOR:        Daniel Dom�nguez
' CREATION DATE: 16-APR-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------------
' 16-APR-2012 DDM    Initial version.
' 18-APR-2012 DDM    Fixed Bug #271: Gender values are wrong.
' 18-APR-2012 DDM    Fixed Bug #273: Round and calculate Totals.
' 30-APR-2012 DDM    Modifications to spend part of the code to the Kernel.
' 09-MAY-2012 JCM    Fixed Bug #295: Generate DataTable only if there are not open sessions and is necessary .
' 23-JAN-2013 DDM    Fixed Bug #470.
' 20-FEB-2013 RXM    Changed datetime filter control to uc_date_picker.
' 15-APR-2013 LEM    Changed the Excel sheets title.
' 02-JUL-2013 JAB    The average calculation does not include the columns of months to 0.
' 19-NOV-2013 LJM    Changes for auditing forms
' 04-DEC-2013 LJM	   Changes to use the base function to fulfill tabs with grids.
' 29-MAY-2014 JAB    Added permissions: To buttons "Excel" and "Print".
'-------------------------------------------------------------------------

Imports System.IO
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.frm_base_sel
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_segmentation_reports
  Inherits GUI_Controls.frm_base_print

#Region " Constants "

  Private Const TABLE_HOLDER_LEVEL_ROWS_COUNT As Integer = 5
  Private Const TABLE_GAME_TYPE_ROWS_COUNT As Integer = 5
  Private Const TABLE_HOLDER_GENDER_ROWS_COUNT As Integer = 3
  Private Const TABLE_YEARS_OLD_ROWS_COUNT As Integer = 10
  Private Const TABLE_ADT_GROUP_ROWS_COUNT As Integer = 7

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_AGROUP_NAME As Integer = 1
  Private Const GRID_COLUMN_MONTH_1 As Integer = 2
  Private Const GRID_COLUMN_MONTH_2 As Integer = 3
  Private Const GRID_COLUMN_MONTH_3 As Integer = 4
  Private Const GRID_COLUMN_MONTH_4 As Integer = 5
  Private Const GRID_COLUMN_MONTH_5 As Integer = 6
  Private Const GRID_COLUMN_MONTH_6 As Integer = 7
  Private Const GRID_COLUMN_MONTH_7 As Integer = 8
  Private Const GRID_COLUMN_MONTH_8 As Integer = 9
  Private Const GRID_COLUMN_MONTH_9 As Integer = 10
  Private Const GRID_COLUMN_MONTH_10 As Integer = 11
  Private Const GRID_COLUMN_MONTH_11 As Integer = 12
  Private Const GRID_COLUMN_MONTH_12 As Integer = 13
  Private Const GRID_COLUMN_MONTH_13 As Integer = 14
  Private Const GRID_COLUMN_PROMEDIO As Integer = 15
  Private Const GRID_COLUMN_VAR As Integer = 16
  Private Const GRID_COLUMN_PC_VAR As Integer = 17

  Private Const GRID_COLUMNS As Integer = 16
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const MONTHS_NUMBER As Integer = 13

  'index grid in tabpage
  Private Const IDX_GRID_TABPAGE As Integer = 0
  Private Const IDX_LABEL_TABPAGE As Integer = 1

#End Region ' Constants

#Region " Members "

  Private m_ds As New DataSet()
  Private m_loaded_tables As Boolean()

  Private m_date_from As DateTime
  Private m_date_to As DateTime
  Private m_ht_names_subtables As Hashtable

  Private m_st_agroup_names As Reports.AgroupNamesFromSegmentation
  Private m_print_datetime As Date
  Private m_msg_open_sessions As String = ""

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SEGMENTATION_REPORTS

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form title
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(244)

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(8)

    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)

    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Call SetDefaultMembers()

    Call CreateDataGridInTabControl(New GUI_Controls.uc_grid(), GLB_NLS_GUI_STATISTICS.GetString(219), _
                                    0, False, GLB_NLS_GUI_STATISTICS.GetString(219), Me.tab_segmentation)
    Call CreateDataGridInTabControl(New GUI_Controls.uc_grid(), GLB_NLS_GUI_STATISTICS.GetString(220), _
                                    0, False, GLB_NLS_GUI_STATISTICS.GetString(221), Me.tab_segmentation)
    Call CreateDataGridInTabControl(New GUI_Controls.uc_grid(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(542), _
                                    0, False, GLB_NLS_GUI_STATISTICS.GetString(222), Me.tab_segmentation)
    Call CreateDataGridInTabControl(New GUI_Controls.uc_grid(), GLB_NLS_GUI_STATISTICS.GetString(223), _
                                    0, False, GLB_NLS_GUI_STATISTICS.GetString(224), Me.tab_segmentation)

    Call SetDefaultValues()

    GUI_StyleSheet(0, Me.tab_segmentation.SelectedTab.Controls(IDX_GRID_TABPAGE))

    m_st_agroup_names = Me.GetLiteralsFromSegmentationAgroups()
  End Sub ' GUI_InitControls

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ExecuteQuery()

    ' Reset load tables
    m_loaded_tables = New Boolean() {False, False, False, False}

    ' Check the filter 
    If Not GUI_FilterCheck() Then

      Return
    End If

    ' Filter
    GUI_ReportUpdateFilters2()

    Call RetrieveDataFromMonths()

    Try
      Me.ShowTable(Me.tab_segmentation.SelectedIndex)

      If m_msg_open_sessions.Length > 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(295), ENUM_MB_TYPE.MB_TYPE_WARNING, , , m_msg_open_sessions)
      End If

      GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

    Catch ex As Exception
      GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    End Try

  End Sub ' GUI_ExecuteQuery

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Filters and sets its value to display in the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(m_date_from, ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(m_date_to, ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
    PrintData.SetFilter(String.Empty, String.Empty, True)

    If m_msg_open_sessions.Length > 0 Then
      PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(345) & ":", m_msg_open_sessions, False)
    Else
      PrintData.SetFilter(String.Empty, String.Empty, True)
    End If


    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition

    _prev_cursor = Me.Cursor
    Cursor = Cursors.WaitCursor

    Try

      Call LoadGrids()

      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime
        For Each _tab_page As TabPage In Me.tab_segmentation.TabPages
          _sheet_layout = New ExcelReportLayout.SheetLayout()
          _sheet_layout.Title = Me.FormTitle
          _sheet_layout.Name = _tab_page.Text

          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.UcGrid = _tab_page.Controls(IDX_GRID_TABPAGE)
          _uc_grid_def.Title = _tab_page.Text
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
          .ListOfSheets.Add(_sheet_layout)
        Next
      End With
      _report_layout.GUI_GenerateExcel()

    Finally
      Cursor = _prev_cursor
    End Try
  End Sub ' GUI_ExcelResultList

#End Region ' Overrides

#Region " Public Functions  "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions 

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _final_time As Date

    _final_time = WSI.Common.Misc.TodayOpening().AddMonths(-1)

    Me.dtp_to.Value = New DateTime(_final_time.Year, _final_time.Month, 1, _final_time.Hour, 0, 0)
    Me.dtp_from.Value = Me.dtp_to.Value.AddMonths(-(MONTHS_NUMBER - 1))

    Me.dtp_to.SetRange(New DateTime(2008, 1, 1, _final_time.Hour, 0, 0), _
                        Me.dtp_to.Value)

  End Sub ' SetDefaultValues

  ' PURPOSE: Set default values to Members
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultMembers()
    Dim _title As String

    ' Reset load tables
    m_loaded_tables = New Boolean() {False, False, False, False}

    ' Hashtable by Subtable names
    If IsNothing(m_ht_names_subtables) Then
      _title = "_TITLE"
      m_ht_names_subtables = New Hashtable()
      m_ht_names_subtables.Add(Reports.SR_SUBTABLE_NAME_HOLDER_LEVEL & _title, GLB_NLS_GUI_STATISTICS.GetString(225))
      m_ht_names_subtables.Add(Reports.SR_SUBTABLE_NAME_GAME_TYPE & _title, GLB_NLS_GUI_CONFIGURATION.GetString(427))
      m_ht_names_subtables.Add(Reports.SR_SUBTABLE_NAME_HOLDER_GENDER & _title, GLB_NLS_GUI_INVOICING.GetString(403))
      m_ht_names_subtables.Add(Reports.SR_SUBTABLE_NAME_YEARS_OLD & _title, GLB_NLS_GUI_INVOICING.GetString(415))
      m_ht_names_subtables.Add(Reports.SR_SUBTABLE_NAME_ADT_GROUP & _title, GLB_NLS_GUI_STATISTICS.GetString(227))
    End If

  End Sub ' SetDefaultMembers

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet(ByVal IndexTable As Integer, _
                             ByRef Dg As GUI_Controls.uc_grid)

    Dim _index_month As Integer
    Dim _date_month As DateTime

    _date_month = Me.dtp_from.Value

    With Dg
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Agroup name( aforo, theoretical, visitis or ADT
      .Column(GRID_COLUMN_AGROUP_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_AGROUP_NAME).Header(1).Text = ""
      .Column(GRID_COLUMN_AGROUP_NAME).Width = 1500
      .Column(GRID_COLUMN_AGROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      _index_month = GRID_COLUMN_MONTH_1
      For _i As Integer = 0 To MONTHS_NUMBER - 1
        .Column(_index_month).Header(0).Text = Strings.Format(_date_month, "MMM-yyyy")
        .Column(_index_month).Width = 1310
        .Column(_index_month).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        _date_month = _date_month.AddMonths(1)
        _index_month += 1
      Next

      ' STATISTICS
      ' Media o promedio
      .Column(GRID_COLUMN_PROMEDIO).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(287)
      .Column(GRID_COLUMN_PROMEDIO).Width = 1310
      .Column(GRID_COLUMN_PROMEDIO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Var
      '.Column(GRID_COLUMN_VAR).Header(0).Text =GLB_NLS_GUI_STATISTICS.GetString(245)
      '.Column(GRID_COLUMN_VAR).Width = 1160
      '.Column(GRID_COLUMN_VAR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      '' PC Var
      '.Column(GRID_COLUMN_PC_VAR).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(246)
      '.Column(GRID_COLUMN_PC_VAR).Width = 1160
      '.Column(GRID_COLUMN_PC_VAR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set values to Date
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ReportUpdateFilters2()

    ' the dates range
    m_date_from = New DateTime(dtp_from.Value.Year, dtp_from.Value.Month, 1)
    m_date_to = New DateTime(dtp_to.Value.Year, dtp_to.Value.Month, 1)

    m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

  End Sub ' GUI_ReportUpdateFilters2

  ' PURPOSE: Retrieve data from Months. 
  '          If not exist the query, save it as XML in BD
  '          If exist, we retrieve data 
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RetrieveDataFromMonths()
    Dim _dt_months As DataTable
    Dim _current_month As DateTime
    Dim _sql_text As String
    Dim _drow As DataRow
    Dim _dt As DataTable
    Dim _ds As DataSet
    Dim _rpt_saved As Boolean
    Dim _open_play_sessions As Int64

    _rpt_saved = False
    _open_play_sessions = 0
    m_msg_open_sessions = ""

    Try
      Using _db_trx As New DB_TRX()

        _dt_months = New DataTable()
        m_ds = New DataSet()

        ' Months
        _sql_text = " SELECT   * " & _
                    "   FROM   REPORTS" & _
                    "  WHERE   REP_TYPE  = " & Reports.SEGMENTATION_REPORT_NAME & _
                    "    AND ( REP_DATE >= '" & DB_FormatDate(m_date_from.Date) & "'" & _
                    "    AND   REP_DATE  < '" & DB_FormatDate(m_date_to.AddMonths(1).Date) & "')"

        _dt_months = GUI_GetTableUsingSQL(_sql_text, 1000, , _db_trx.SqlTransaction)
      End Using

      _current_month = m_date_from

      For i As Integer = 0 To MONTHS_NUMBER - 1
        _drow = SearchMonths(_dt_months, _current_month)
        _ds = New DataSet()
        If Not IsNothing(_drow) Then
          ' Exist, add to ds
          Try
            WSI.Common.Misc.DataSetFromXml(_ds, _drow("REP_DATA"), XmlReadMode.Auto)

            _dt = _ds.Tables(0)
            _dt.TableName = "T" & i

            _ds.Tables.Remove(_dt)
            m_ds.Tables.Add(_dt)
          Catch
          End Try

        Else
          ' Not exists
          _dt = New DataTable()
          Reports.SetReportSegmentationFromNewMonth(_current_month, m_st_agroup_names, False, True, _dt, _rpt_saved, _open_play_sessions)
          _dt.TableName = "T" & i
          m_ds.Tables.Add(_dt)

          If m_msg_open_sessions.Length > 0 Then
            m_msg_open_sessions = m_msg_open_sessions & IIf(_rpt_saved, "", ", " & GUI_FormatDate(_current_month, ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
          Else
            m_msg_open_sessions = m_msg_open_sessions & IIf(_rpt_saved, "", GUI_FormatDate(_current_month, ENUM_FORMAT_DATE.FORMAT_DATE_YEAR_MONTH, ENUM_FORMAT_TIME.FORMAT_TIME_NONE))
          End If

        End If

        _current_month = _current_month.AddMonths(1)
        _open_play_sessions = 0
      Next

    Catch ex As Exception
    End Try
  End Sub ' RetrieveDataFromMonths

  ' PURPOSE: Search month in a DataTable
  '
  '  PARAMS:
  '     - INPUT:
  '           - Dt_months Datatable the Months
  '           - DateMonth Date to search
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Datarow
  Private Function SearchMonths(ByVal DtMonths As DataTable, _
                                ByVal DateMonth As DateTime) As DataRow
    Dim _date_time As DateTime

    For Each drow As DataRow In DtMonths.Rows
      _date_time = drow("REP_DATE")
      If _date_time.Month = DateMonth.Month AndAlso _date_time.Year = DateMonth.Year Then

        Return drow
      End If
    Next

    Return Nothing
  End Function ' SearchMonths

  ' PURPOSE: Show the grid according with tabpage selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - IndexTable Index the tabpage selected
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ShowTable(ByVal IndexTable As Integer)
    Dim _table As DataTable
    Dim _column As Integer
    If IsNothing(m_loaded_tables) OrElse Me.m_loaded_tables(IndexTable) Then

      Return
    Else
      _table = Nothing
      ' Load style
      Call GUI_StyleSheet(IndexTable, Me.tab_segmentation.TabPages(IndexTable).Controls(IDX_GRID_TABPAGE))

      If m_ds.Tables.Count > 0 Then
        _column = IndexTable + 2
        TableToGrid(_column, Me.tab_segmentation.TabPages(IndexTable).Controls(IDX_GRID_TABPAGE))
        ' Update the array (tables shown)
        m_loaded_tables(IndexTable) = True
      End If

    End If
  End Sub ' ShowTable

  ' PURPOSE: Set values of grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Index: Index from tabpage selected
  '           - DgSegmentation: DataGrid from Tabpage
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TableToGrid(ByVal Index As Integer, _
                          ByRef DgSegmentation As GUI_Controls.uc_grid)

    Dim _idx_row As Integer
    Dim _idx_column As Integer
    Dim _idx_column_table As String
    Dim _redraw As Boolean
    Dim _total As Decimal
    Dim _al_media As ArrayList
    Dim _total_adt As Decimal 'total netwin te�rico / visita.
    Dim _found_level_rows As Boolean
    Dim _count_month As Integer

    _al_media = New ArrayList()
    _total = 0
    _idx_column_table = ""
    _total_adt = 0

    Select Case Index
      Case 2
        _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO
      Case 3
        _idx_column_table = Reports.SR_SQL_COLUMN_TD_THEORETICAL_HOLD
      Case 4
        _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS
      Case 5
        _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT
    End Select

    _redraw = DgSegmentation.Redraw
    DgSegmentation.Redraw = False
    _found_level_rows = False

    Try
      _idx_column = GRID_COLUMN_MONTH_1
      For Each _dt_month_values As DataTable In m_ds.Tables
        _idx_row = 0
        For Each _dr_agroup_month_values As DataRow In _dt_month_values.Rows

          ' Add new row
          If _idx_column = GRID_COLUMN_MONTH_1 Then
            DgSegmentation.AddRow()
            _idx_row = DgSegmentation.NumRows - 1
          End If

          ' Agroup (level, game type, gender,etc..)
          If _dr_agroup_month_values.Table.Columns.Contains(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) Then
            If IsDBNull(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME)) Then
              If IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = "---"
              End If
            Else
              '4 posibility row: Total, empty line, Title and normal line
              If _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "TOTAL" Then
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(423)  'SUBTOTAL:  
                DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
              ElseIf _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "EMPTY" Then
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = ""
                DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
              ElseIf Me.m_ht_names_subtables.ContainsKey(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME)) Then
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = Me.m_ht_names_subtables(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME))
                DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
              Else
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_AGROUP_NAME).Value = _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME)
                DgSegmentation.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
              End If
            End If
          End If

          ' Months (13)
          If _dr_agroup_month_values.Table.Columns.Contains(_idx_column_table) Then
            If IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
              'TOTAL values
              If _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "TOTAL" Then

                If _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO OrElse _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS Then
                  DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatNumber(_total, 0)
                Else
                  If _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT Then
                    _total_adt = CalculateTotalADT(_dt_month_values, _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_TYPE))
                    DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatCurrency(_total_adt, 2)
                  Else
                    DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatCurrency(_total, 2)
                  End If

                End If

              Else
                DgSegmentation.Cell(_idx_row, _idx_column).Value = ""
              End If

            Else
              'Months values
              If _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO OrElse _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS Then
                DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatNumber(_dr_agroup_month_values(_idx_column_table), 0)
              Else
                DgSegmentation.Cell(_idx_row, _idx_column).Value = GUI_FormatCurrency(_dr_agroup_month_values(_idx_column_table), 2)
              End If
              _total += _dr_agroup_month_values(_idx_column_table)
            End If
          End If

          ' Calculate media
          If _idx_column = GRID_COLUMN_MONTH_1 Then
            If Not IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
              _al_media.Add(CType(_dr_agroup_month_values(_idx_column_table), Decimal))
            ElseIf _dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_NAME) = "TOTAL" Then
              If _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT Then
                _al_media.Add(_total_adt)
              Else
                _al_media.Add(_total)
              End If
              _total = 0
            Else
              _al_media.Add(0)
            End If

          Else
            If Not IsDBNull(_dr_agroup_month_values(_idx_column_table)) Then
              _al_media(_idx_row) += _dr_agroup_month_values(_idx_column_table)
            Else
              If _idx_column_table = Reports.SR_SQL_COLUMN_TD_ADT Then
                _al_media(_idx_row) += _total_adt
              Else
                _al_media(_idx_row) += _total
              End If

              _total = 0
            End If

          End If

          ' Set media
          If _idx_column = GRID_COLUMN_MONTH_13 Then
            '  rows: Total and normal line.
            If Not IsDBNull(_dr_agroup_month_values(Reports.SR_SQL_COLUMN_TD_AGROUP_TYPE)) Then

              _count_month = 0

              For _count_current_mounth As Integer = GRID_COLUMN_MONTH_1 To GRID_COLUMN_MONTH_13
                If DgSegmentation.Cell(_idx_row, _count_current_mounth).Value > 0 Then
                  _count_month += 1
                End If
              Next

              If _count_month = 0 Then
                _count_month = MONTHS_NUMBER
              End If

              If _idx_column_table = Reports.SR_SQL_COLUMN_TD_AFORO OrElse _idx_column_table = Reports.SR_SQL_COLUMN_TD_VISITS Then
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_PROMEDIO).Value = GUI_FormatNumber(_al_media(_idx_row) / _count_month, 0)
              Else
                DgSegmentation.Cell(_idx_row, GRID_COLUMN_PROMEDIO).Value = GUI_FormatCurrency(_al_media(_idx_row) / _count_month, 2)
              End If

            Else
              DgSegmentation.Cell(_idx_row, GRID_COLUMN_PROMEDIO).Value = ""
            End If
          End If

          If _idx_column <> GRID_COLUMN_MONTH_1 Then
            _idx_row += 1
          End If
        Next
        _idx_column += 1

      Next

    Catch ex As Exception

    Finally
      DgSegmentation.Redraw = _redraw
    End Try

  End Sub ' TableToGrid

  ' PURPOSE: Calculate total for adt
  '
  '  PARAMS:
  '     - INPUT:
  '           - MonthValues: Month values
  '           - AgroupType: Agroup type (game type, client type, gent,etc..)
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Total Value ADT
  Private Function CalculateTotalADT(ByVal MonthValues As DataTable, _
                                     ByVal AgroupType As String) As Decimal

    Dim _drows As DataRow()
    Dim _total_theoretical As Decimal
    Dim _total_visits As Decimal

    _total_theoretical = 0
    _total_visits = 0
    _drows = MonthValues.Select(Reports.SR_SQL_COLUMN_TD_AGROUP_TYPE & " = '" & AgroupType & "'")

    For Each _drow As DataRow In _drows
      If Not IsDBNull(_drow(Reports.SR_SQL_COLUMN_TD_THEORETICAL_HOLD)) Then
        _total_theoretical += _drow(Reports.SR_SQL_COLUMN_TD_THEORETICAL_HOLD)
        _total_visits += _drow(Reports.SR_SQL_COLUMN_TD_VISITS)
      End If

    Next

    If _total_visits = 0 Then

      Return 0
    End If

    Return _total_theoretical / _total_visits
  End Function ' CalculateTotalADT

  ' PURPOSE: Load grids of tabs
  '
  '  PARAMS:
  '     - INPUT:
  '           - None 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub LoadGrids()

    For _i As Integer = 0 To Me.tab_segmentation.TabPages.Count - 1
      Call ShowTable(_i)
    Next

  End Sub ' LoadGrids

  ' PURPOSE: Return of the struct with the literals from agroups the segmentation
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetLiteralsFromSegmentationAgroups() As Reports.AgroupNamesFromSegmentation
    Dim _dt_level_names As DataTable
    Dim _dt_games_names As DataTable
    Dim _agroup_names As Reports.AgroupNamesFromSegmentation

    Using _db_trx As New DB_TRX()
      _dt_level_names = Reports.GetLevelNames(_db_trx.SqlTransaction)
      _dt_games_names = Reports.GetGameNames(_db_trx.SqlTransaction)
    End Using

    _agroup_names = New Reports.AgroupNamesFromSegmentation()

    ' HOLDER LEVEL
    _agroup_names.holder_level.Add(0, GLB_NLS_GUI_STATISTICS.GetString(226)) ' an�nimo
    For Each _drow_level As DataRow In _dt_level_names.Rows
      If _drow_level(0) <> 0 Then
        _agroup_names.holder_level.Add(_drow_level(0), _drow_level(1))
      End If
    Next

    ' GAME TYPE
    For Each _drow_game As DataRow In _dt_games_names.Rows
      _agroup_names.game_type.Add(_drow_game(0), _drow_game(1))
    Next

    ' HOLDER GENDER  
    _agroup_names.holder_gender.Add(0, GLB_NLS_GUI_STATISTICS.GetString(226))
    _agroup_names.holder_gender.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)) ' men
    _agroup_names.holder_gender.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)) ' woman

    ' YEARS OLD 
    _agroup_names.years_old.Add(0, GLB_NLS_GUI_STATISTICS.GetString(226)) ' an�nimo 
    _agroup_names.years_old.Add(1, GLB_NLS_GUI_STATISTICS.GetString(228)) ' menor 18
    _agroup_names.years_old.Add(2, GLB_NLS_GUI_STATISTICS.GetString(229)) ' 18-24
    _agroup_names.years_old.Add(3, GLB_NLS_GUI_STATISTICS.GetString(230)) ' 25-34
    _agroup_names.years_old.Add(4, GLB_NLS_GUI_STATISTICS.GetString(231)) ' 35-44
    _agroup_names.years_old.Add(5, GLB_NLS_GUI_STATISTICS.GetString(232)) ' 45-54
    _agroup_names.years_old.Add(6, GLB_NLS_GUI_STATISTICS.GetString(233)) ' 55-64
    _agroup_names.years_old.Add(7, GLB_NLS_GUI_STATISTICS.GetString(234)) ' 65-74
    _agroup_names.years_old.Add(8, GLB_NLS_GUI_STATISTICS.GetString(235)) ' 75-84
    _agroup_names.years_old.Add(9, GLB_NLS_GUI_STATISTICS.GetString(236)) ' 85 o mas.      

    ' ADT GROUP
    _agroup_names.adt_group.Add(0, GLB_NLS_GUI_STATISTICS.GetString(237)) ' $0-49
    _agroup_names.adt_group.Add(1, GLB_NLS_GUI_STATISTICS.GetString(238)) ' $50-99
    _agroup_names.adt_group.Add(2, GLB_NLS_GUI_STATISTICS.GetString(239)) ' $100-149
    _agroup_names.adt_group.Add(3, GLB_NLS_GUI_STATISTICS.GetString(240)) ' $150-249
    _agroup_names.adt_group.Add(4, GLB_NLS_GUI_STATISTICS.GetString(241)) ' $250-399
    _agroup_names.adt_group.Add(5, GLB_NLS_GUI_STATISTICS.GetString(242)) ' $400-999
    _agroup_names.adt_group.Add(6, GLB_NLS_GUI_STATISTICS.GetString(243)) ' $1,000 o more

    Return _agroup_names

  End Function ' GetLiteralsFromSegmentationAgroups

#End Region ' Private Functions

#Region " Events "

  Private Sub tab_productivity_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tab_segmentation.Selected
    Me.ShowTable(Me.tab_segmentation.SelectedIndex)
  End Sub

  Private Sub dtp_to_DatePickerValueChanged() Handles dtp_to.DatePickerValueChanged
    dtp_from.Value = dtp_to.Value.AddMonths(-(MONTHS_NUMBER - 1))
  End Sub

#End Region ' Events

End Class
