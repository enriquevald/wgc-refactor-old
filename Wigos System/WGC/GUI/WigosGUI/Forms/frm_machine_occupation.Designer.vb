<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_machine_occupation
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.chk_providers = New System.Windows.Forms.CheckBox
    Me.gpr_options = New System.Windows.Forms.GroupBox
    Me.chk_terminales = New System.Windows.Forms.CheckBox
    Me.gb_caption = New System.Windows.Forms.GroupBox
    Me.lbl_caption_total_providers = New System.Windows.Forms.Label
    Me.lbl_caption_total_terminals = New System.Windows.Forms.Label
    Me.lbl_color_total_terminals = New System.Windows.Forms.Label
    Me.lbl_color_total_providers = New System.Windows.Forms.Label
    Me.gb_ratio = New System.Windows.Forms.GroupBox
    Me.lbl_4 = New System.Windows.Forms.Label
    Me.lbl_25 = New System.Windows.Forms.Label
    Me.lbl_1 = New System.Windows.Forms.Label
    Me.lbl_05 = New System.Windows.Forms.Label
    Me.lbl_025 = New System.Windows.Forms.Label
    Me.lbl_gradient = New System.Windows.Forms.Label
    Me.lbl_025_mark = New System.Windows.Forms.Label
    Me.lbl_1_mark = New System.Windows.Forms.Label
    Me.lbl_05_mark = New System.Windows.Forms.Label
    Me.lbl_25_mark = New System.Windows.Forms.Label
    Me.lbl_4_mark = New System.Windows.Forms.Label
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gpr_options.SuspendLayout()
    Me.gb_caption.SuspendLayout()
    Me.gb_ratio.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_ratio)
    Me.panel_filter.Controls.Add(Me.gb_caption)
    Me.panel_filter.Controls.Add(Me.gpr_options)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1237, 149)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gpr_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_caption, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_ratio, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 153)
    Me.panel_data.Size = New System.Drawing.Size(1237, 501)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1231, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1231, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(3, 3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(305, 81)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'chk_providers
    '
    Me.chk_providers.Checked = True
    Me.chk_providers.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_providers.Location = New System.Drawing.Point(22, 22)
    Me.chk_providers.Name = "chk_providers"
    Me.chk_providers.Size = New System.Drawing.Size(154, 17)
    Me.chk_providers.TabIndex = 0
    Me.chk_providers.Text = "xProveedores"
    Me.chk_providers.UseVisualStyleBackColor = True
    '
    'gpr_options
    '
    Me.gpr_options.Controls.Add(Me.chk_terminales)
    Me.gpr_options.Controls.Add(Me.chk_providers)
    Me.gpr_options.Location = New System.Drawing.Point(314, 3)
    Me.gpr_options.Name = "gpr_options"
    Me.gpr_options.Size = New System.Drawing.Size(182, 73)
    Me.gpr_options.TabIndex = 1
    Me.gpr_options.TabStop = False
    Me.gpr_options.Text = "xShow"
    '
    'chk_terminales
    '
    Me.chk_terminales.Checked = True
    Me.chk_terminales.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_terminales.Location = New System.Drawing.Point(22, 45)
    Me.chk_terminales.Name = "chk_terminales"
    Me.chk_terminales.Size = New System.Drawing.Size(154, 17)
    Me.chk_terminales.TabIndex = 1
    Me.chk_terminales.Text = "xTerminales"
    Me.chk_terminales.UseVisualStyleBackColor = True
    '
    'gb_caption
    '
    Me.gb_caption.Controls.Add(Me.lbl_caption_total_providers)
    Me.gb_caption.Controls.Add(Me.lbl_caption_total_terminals)
    Me.gb_caption.Controls.Add(Me.lbl_color_total_terminals)
    Me.gb_caption.Controls.Add(Me.lbl_color_total_providers)
    Me.gb_caption.Location = New System.Drawing.Point(314, 79)
    Me.gb_caption.Name = "gb_caption"
    Me.gb_caption.Size = New System.Drawing.Size(182, 61)
    Me.gb_caption.TabIndex = 2
    Me.gb_caption.TabStop = False
    Me.gb_caption.Text = "xCaption"
    '
    'lbl_caption_total_providers
    '
    Me.lbl_caption_total_providers.AutoSize = True
    Me.lbl_caption_total_providers.Location = New System.Drawing.Point(44, 41)
    Me.lbl_caption_total_providers.Name = "lbl_caption_total_providers"
    Me.lbl_caption_total_providers.Size = New System.Drawing.Size(96, 13)
    Me.lbl_caption_total_providers.TabIndex = 3
    Me.lbl_caption_total_providers.Text = "xTotalProviders"
    '
    'lbl_caption_total_terminals
    '
    Me.lbl_caption_total_terminals.AutoSize = True
    Me.lbl_caption_total_terminals.Location = New System.Drawing.Point(44, 19)
    Me.lbl_caption_total_terminals.Name = "lbl_caption_total_terminals"
    Me.lbl_caption_total_terminals.Size = New System.Drawing.Size(72, 13)
    Me.lbl_caption_total_terminals.TabIndex = 1
    Me.lbl_caption_total_terminals.Text = "xTotalRows"
    '
    'lbl_color_total_terminals
    '
    Me.lbl_color_total_terminals.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_total_terminals.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_total_terminals.Location = New System.Drawing.Point(22, 18)
    Me.lbl_color_total_terminals.Name = "lbl_color_total_terminals"
    Me.lbl_color_total_terminals.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_total_terminals.TabIndex = 0
    '
    'lbl_color_total_providers
    '
    Me.lbl_color_total_providers.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_total_providers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_total_providers.Location = New System.Drawing.Point(22, 40)
    Me.lbl_color_total_providers.Name = "lbl_color_total_providers"
    Me.lbl_color_total_providers.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_total_providers.TabIndex = 2
    '
    'gb_ratio
    '
    Me.gb_ratio.Controls.Add(Me.lbl_4)
    Me.gb_ratio.Controls.Add(Me.lbl_25)
    Me.gb_ratio.Controls.Add(Me.lbl_1)
    Me.gb_ratio.Controls.Add(Me.lbl_05)
    Me.gb_ratio.Controls.Add(Me.lbl_025)
    Me.gb_ratio.Controls.Add(Me.lbl_gradient)
    Me.gb_ratio.Controls.Add(Me.lbl_025_mark)
    Me.gb_ratio.Controls.Add(Me.lbl_1_mark)
    Me.gb_ratio.Controls.Add(Me.lbl_05_mark)
    Me.gb_ratio.Controls.Add(Me.lbl_25_mark)
    Me.gb_ratio.Controls.Add(Me.lbl_4_mark)
    Me.gb_ratio.Location = New System.Drawing.Point(502, 3)
    Me.gb_ratio.Name = "gb_ratio"
    Me.gb_ratio.Size = New System.Drawing.Size(360, 73)
    Me.gb_ratio.TabIndex = 4
    Me.gb_ratio.TabStop = False
    Me.gb_ratio.Text = "xLeyenda Ratio"
    '
    'lbl_4
    '
    Me.lbl_4.Location = New System.Drawing.Point(314, 25)
    Me.lbl_4.Name = "lbl_4"
    Me.lbl_4.Size = New System.Drawing.Size(40, 13)
    Me.lbl_4.TabIndex = 32
    Me.lbl_4.Text = "4.00"
    '
    'lbl_25
    '
    Me.lbl_25.Location = New System.Drawing.Point(239, 25)
    Me.lbl_25.Name = "lbl_25"
    Me.lbl_25.Size = New System.Drawing.Size(47, 13)
    Me.lbl_25.TabIndex = 31
    Me.lbl_25.Text = "2.50"
    '
    'lbl_1
    '
    Me.lbl_1.Location = New System.Drawing.Point(164, 25)
    Me.lbl_1.Name = "lbl_1"
    Me.lbl_1.Size = New System.Drawing.Size(47, 13)
    Me.lbl_1.TabIndex = 30
    Me.lbl_1.Text = "1.00"
    '
    'lbl_05
    '
    Me.lbl_05.Location = New System.Drawing.Point(89, 25)
    Me.lbl_05.Name = "lbl_05"
    Me.lbl_05.Size = New System.Drawing.Size(47, 13)
    Me.lbl_05.TabIndex = 29
    Me.lbl_05.Text = "0.50"
    '
    'lbl_025
    '
    Me.lbl_025.Location = New System.Drawing.Point(16, 25)
    Me.lbl_025.Name = "lbl_025"
    Me.lbl_025.Size = New System.Drawing.Size(47, 13)
    Me.lbl_025.TabIndex = 24
    Me.lbl_025.Text = "0.25"
    '
    'lbl_gradient
    '
    Me.lbl_gradient.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_gradient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_gradient.Location = New System.Drawing.Point(29, 44)
    Me.lbl_gradient.Name = "lbl_gradient"
    Me.lbl_gradient.Size = New System.Drawing.Size(300, 16)
    Me.lbl_gradient.TabIndex = 22
    '
    'lbl_025_mark
    '
    Me.lbl_025_mark.Location = New System.Drawing.Point(25, 38)
    Me.lbl_025_mark.Name = "lbl_025_mark"
    Me.lbl_025_mark.Size = New System.Drawing.Size(31, 13)
    Me.lbl_025_mark.TabIndex = 23
    Me.lbl_025_mark.Text = "|"
    '
    'lbl_1_mark
    '
    Me.lbl_1_mark.Location = New System.Drawing.Point(173, 38)
    Me.lbl_1_mark.Name = "lbl_1_mark"
    Me.lbl_1_mark.Size = New System.Drawing.Size(31, 13)
    Me.lbl_1_mark.TabIndex = 26
    Me.lbl_1_mark.Text = "|"
    '
    'lbl_05_mark
    '
    Me.lbl_05_mark.Location = New System.Drawing.Point(98, 38)
    Me.lbl_05_mark.Name = "lbl_05_mark"
    Me.lbl_05_mark.Size = New System.Drawing.Size(31, 13)
    Me.lbl_05_mark.TabIndex = 25
    Me.lbl_05_mark.Text = "|"
    '
    'lbl_25_mark
    '
    Me.lbl_25_mark.Location = New System.Drawing.Point(248, 38)
    Me.lbl_25_mark.Name = "lbl_25_mark"
    Me.lbl_25_mark.Size = New System.Drawing.Size(31, 13)
    Me.lbl_25_mark.TabIndex = 27
    Me.lbl_25_mark.Text = "|"
    '
    'lbl_4_mark
    '
    Me.lbl_4_mark.Location = New System.Drawing.Point(324, 38)
    Me.lbl_4_mark.Name = "lbl_4_mark"
    Me.lbl_4_mark.Size = New System.Drawing.Size(31, 13)
    Me.lbl_4_mark.TabIndex = 28
    Me.lbl_4_mark.Text = "|"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(521, 89)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_machine_occupation
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1247, 658)
    Me.Name = "frm_machine_occupation"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_machine_occupation"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gpr_options.ResumeLayout(False)
    Me.gb_caption.ResumeLayout(False)
    Me.gb_caption.PerformLayout()
    Me.gb_ratio.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_providers As System.Windows.Forms.CheckBox
  Friend WithEvents gpr_options As System.Windows.Forms.GroupBox
  Friend WithEvents gb_caption As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_caption_total_terminals As System.Windows.Forms.Label
  Friend WithEvents lbl_color_total_terminals As System.Windows.Forms.Label
  Friend WithEvents lbl_color_total_providers As System.Windows.Forms.Label
  Friend WithEvents lbl_caption_total_providers As System.Windows.Forms.Label
  Friend WithEvents gb_ratio As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_4 As System.Windows.Forms.Label
  Friend WithEvents lbl_25 As System.Windows.Forms.Label
  Friend WithEvents lbl_1 As System.Windows.Forms.Label
  Friend WithEvents lbl_05 As System.Windows.Forms.Label
  Friend WithEvents lbl_025 As System.Windows.Forms.Label
  Friend WithEvents lbl_gradient As System.Windows.Forms.Label
  Friend WithEvents lbl_025_mark As System.Windows.Forms.Label
  Friend WithEvents lbl_1_mark As System.Windows.Forms.Label
  Friend WithEvents lbl_05_mark As System.Windows.Forms.Label
  Friend WithEvents lbl_25_mark As System.Windows.Forms.Label
  Friend WithEvents lbl_4_mark As System.Windows.Forms.Label
  Friend WithEvents chk_terminales As System.Windows.Forms.CheckBox
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
