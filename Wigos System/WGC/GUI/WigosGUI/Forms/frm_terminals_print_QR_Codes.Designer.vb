﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_print_QR_Codes
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_terminals_print_QR_Codes))
    Me.btn_rmv_all = New System.Windows.Forms.Button()
    Me.tgf_terminals = New GUI_Controls.uc_terminals_group_filter()
    Me.btn_rmv = New System.Windows.Forms.Button()
    Me.btn_add_block = New System.Windows.Forms.Button()
    Me.dg_terminals = New GUI_Controls.uc_grid()
    Me.gb_print_progress = New System.Windows.Forms.GroupBox()
    Me.btn_print_cancel = New GUI_Controls.uc_button()
    Me.btn_pause_continue = New GUI_Controls.uc_button()
    Me.lbl_print_status = New System.Windows.Forms.Label()
    Me.lblCountSelected = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_print_progress.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.lblCountSelected)
    Me.panel_data.Controls.Add(Me.btn_rmv_all)
    Me.panel_data.Controls.Add(Me.dg_terminals)
    Me.panel_data.Controls.Add(Me.btn_add_block)
    Me.panel_data.Controls.Add(Me.tgf_terminals)
    Me.panel_data.Controls.Add(Me.btn_rmv)
    Me.panel_data.Size = New System.Drawing.Size(1035, 377)
    '
    'btn_rmv_all
    '
    Me.btn_rmv_all.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv_all.Location = New System.Drawing.Point(295, 217)
    Me.btn_rmv_all.Name = "btn_rmv_all"
    Me.btn_rmv_all.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv_all.TabIndex = 3
    Me.btn_rmv_all.Text = "<<"
    Me.btn_rmv_all.UseVisualStyleBackColor = True
    '
    'tgf_terminals
    '
    Me.tgf_terminals.ForbiddenGroups = CType(resources.GetObject("tgf_terminals.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.tgf_terminals.HeightOnExpanded = 422
    Me.tgf_terminals.Location = New System.Drawing.Point(14, 17)
    Me.tgf_terminals.MinimumSize = New System.Drawing.Size(275, 339)
    Me.tgf_terminals.Name = "tgf_terminals"
    Me.tgf_terminals.SelectedIndex = 0
    Me.tgf_terminals.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutButtons
    Me.tgf_terminals.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.tgf_terminals.ShowGroupBoxLine = True
    Me.tgf_terminals.ShowGroups = Nothing
    Me.tgf_terminals.ShowNodeAll = False
    Me.tgf_terminals.Size = New System.Drawing.Size(275, 339)
    Me.tgf_terminals.TabIndex = 0
    Me.tgf_terminals.ThreeStateCheckMode = False
    '
    'btn_rmv
    '
    Me.btn_rmv.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_rmv.Location = New System.Drawing.Point(295, 170)
    Me.btn_rmv.Name = "btn_rmv"
    Me.btn_rmv.Size = New System.Drawing.Size(45, 45)
    Me.btn_rmv.TabIndex = 2
    Me.btn_rmv.Text = "<"
    Me.btn_rmv.UseVisualStyleBackColor = True
    '
    'btn_add_block
    '
    Me.btn_add_block.Font = New System.Drawing.Font("Agency FB", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_add_block.Location = New System.Drawing.Point(295, 123)
    Me.btn_add_block.Name = "btn_add_block"
    Me.btn_add_block.Size = New System.Drawing.Size(45, 45)
    Me.btn_add_block.TabIndex = 1
    Me.btn_add_block.Text = ">"
    Me.btn_add_block.UseVisualStyleBackColor = True
    '
    'dg_terminals
    '
    Me.dg_terminals.CurrentCol = -1
    Me.dg_terminals.CurrentRow = -1
    Me.dg_terminals.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_terminals.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_terminals.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_terminals.Location = New System.Drawing.Point(351, 23)
    Me.dg_terminals.Name = "dg_terminals"
    Me.dg_terminals.PanelRightVisible = False
    Me.dg_terminals.Redraw = True
    Me.dg_terminals.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_terminals.Size = New System.Drawing.Size(674, 331)
    Me.dg_terminals.Sortable = False
    Me.dg_terminals.SortAscending = True
    Me.dg_terminals.SortByCol = 0
    Me.dg_terminals.TabIndex = 0
    Me.dg_terminals.ToolTipped = True
    Me.dg_terminals.TopRow = -2
    '
    'gb_print_progress
    '
    Me.gb_print_progress.Controls.Add(Me.btn_print_cancel)
    Me.gb_print_progress.Controls.Add(Me.btn_pause_continue)
    Me.gb_print_progress.Controls.Add(Me.lbl_print_status)
    Me.gb_print_progress.Location = New System.Drawing.Point(752, 369)
    Me.gb_print_progress.Name = "gb_print_progress"
    Me.gb_print_progress.Size = New System.Drawing.Size(272, 75)
    Me.gb_print_progress.TabIndex = 6
    Me.gb_print_progress.TabStop = False
    Me.gb_print_progress.Text = "xPrintProgress"
    '
    'btn_print_cancel
    '
    Me.btn_print_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print_cancel.Enabled = False
    Me.btn_print_cancel.Location = New System.Drawing.Point(160, 38)
    Me.btn_print_cancel.Name = "btn_print_cancel"
    Me.btn_print_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_print_cancel.TabIndex = 2
    Me.btn_print_cancel.ToolTipped = False
    Me.btn_print_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_pause_continue
    '
    Me.btn_pause_continue.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_pause_continue.Enabled = False
    Me.btn_pause_continue.Location = New System.Drawing.Point(29, 38)
    Me.btn_pause_continue.Name = "btn_pause_continue"
    Me.btn_pause_continue.Size = New System.Drawing.Size(90, 30)
    Me.btn_pause_continue.TabIndex = 1
    Me.btn_pause_continue.ToolTipped = False
    Me.btn_pause_continue.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_print_status
    '
    Me.lbl_print_status.AutoSize = True
    Me.lbl_print_status.Location = New System.Drawing.Point(6, 20)
    Me.lbl_print_status.Name = "lbl_print_status"
    Me.lbl_print_status.Size = New System.Drawing.Size(76, 13)
    Me.lbl_print_status.TabIndex = 0
    Me.lbl_print_status.Text = "xPrintStatus"
    '
    'lblCountSelected
    '
    Me.lblCountSelected.AutoSize = True
    Me.lblCountSelected.Location = New System.Drawing.Point(358, 359)
    Me.lblCountSelected.Name = "lblCountSelected"
    Me.lblCountSelected.Size = New System.Drawing.Size(97, 13)
    Me.lblCountSelected.TabIndex = 3
    Me.lblCountSelected.Text = "xCountSelected"
    '
    'frm_terminals_print_QR_Codes
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1134, 488)
    Me.Controls.Add(Me.gb_print_progress)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_terminals_print_QR_Codes"
    Me.Text = "frm_terminals_print_QR_Codes"
    Me.Controls.SetChildIndex(Me.panel_data, 0)
    Me.Controls.SetChildIndex(Me.gb_print_progress, 0)
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_print_progress.ResumeLayout(False)
    Me.gb_print_progress.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_terminals As GUI_Controls.uc_grid
  Friend WithEvents btn_rmv As System.Windows.Forms.Button
  Friend WithEvents btn_add_block As System.Windows.Forms.Button
  Friend WithEvents tgf_terminals As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents btn_rmv_all As System.Windows.Forms.Button
  Friend WithEvents gb_print_progress As System.Windows.Forms.GroupBox
  Friend WithEvents btn_print_cancel As GUI_Controls.uc_button
  Friend WithEvents btn_pause_continue As GUI_Controls.uc_button
  Friend WithEvents lbl_print_status As System.Windows.Forms.Label
  Friend WithEvents lblCountSelected As System.Windows.Forms.Label
End Class
