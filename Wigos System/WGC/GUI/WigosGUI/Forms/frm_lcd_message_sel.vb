'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_lcd_message
' DESCRIPTION:   LCD Message
' AUTHOR:        David Hernández
' CREATION DATE: 28-NOV-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-NOV-2014  DHA    Initial version
' 16-DEC-2014  DHA    Fixed Bug #WIG-1836: Excel wrong format dates 
' 17-DEC-2014  DCS    Fixed Bug #WIG-1850: Message visible if at least one site is selected
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common

Public Class frm_lcd_message_sel
  Inherits GUI_Controls.frm_base_sel_edit

#Region "Members"
  Private m_filt_date_from As String
  Private m_filt_date_to As String
  Private m_filt_enabled As Boolean
  Private m_filt_disabled As Boolean
  Private m_filt_current_only As Boolean
  Private m_filt_multisite As Boolean
  Private m_filt_site As Boolean
  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)
  Private m_is_multisite_member As Boolean = GeneralParam.GetBoolean("Site", "MultiSiteMember", False)

#End Region

#Region "CONSTANT"
  ' DB Columns
  Private Const SQL_COLUMN_MSG_ID As Integer = 0
  Private Const SQL_COLUMN_MSG_TYPE As Integer = 1
  Private Const SQL_COLUMN_MSG_ENABLED As Integer = 2
  Private Const SQL_COLUMN_MSG_ORDER As Integer = 3
  Private Const SQL_COLUMN_MSG_SCHEDULE_START As Integer = 4
  Private Const SQL_COLUMN_MSG_SCHEDULE_END As Integer = 5
  Private Const SQL_COLUMN_MSG_MESSAGE As Integer = 6
  Private Const SQL_COLUMN_MSG_MASTER_SEQUENCE As Integer = 7

  ' Grid Columns
  Private Const GRID_COLUMN_MSG_INDEX As Integer = 0
  Private Const GRID_COLUMN_MSG_ID As Integer = 1
  Private Const GRID_COLUMN_MSG_VIEW_ORDER As Integer = 2
  Private Const GRID_COLUMN_MSG_SCHEDULE_START As Integer = 3
  Private Const GRID_COLUMN_MSG_SCHEDULE_END As Integer = 4
  Private Const GRID_COLUMN_MSG_ENABLED As Integer = 5
  Private Const GRID_COLUMN_MSG_SOURCE As Integer = 6
  Private Const GRID_COLUMN_MSG_TYPE As Integer = 7
  Private Const GRID_COLUMN_MSG_ORDER As Integer = 8
  Private Const GRID_COLUMN_MSG_MESSAGE_L1 As Integer = 9
  Private Const GRID_COLUMN_MSG_MESSAGE_L2 As Integer = 10
  Private Const GRID_COLUMN_MSG_MASTER_SEQUENCE As Integer = 11

  Private Const GRID_COLUMNS As Integer = 12
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_VIEW_ORDER As Integer = 600
  Private Const GRID_WIDTH_MSG_TYPE As Integer = 2400
  Private Const GRID_WIDTH_MSG_ENABLED As Integer = 1250
  Private Const GRID_WIDTH_MSG_ORDER As Integer = 0
  Private Const GRID_WIDTH_SOURCE As Integer = 800
  Private Const GRID_WIDTH_MSG_SCHEDULE_START As Integer = 2000
  Private Const GRID_WIDTH_MSG_SCHEDULE_END As Integer = 2000
  Private Const GRID_WIDTH_MSG_MESSAGE_L1 As Integer = 2400
  Private Const GRID_WIDTH_MSG_MESSAGE_L2 As Integer = 2400
  Private Const GRID_WIDTH_MSG_MASTER_SEQUENCE As Integer = 0

  Private Const SOURCE_MULTISITE As String = "M"
  Private Const SOURCE_SITE As String = "S"

  Private Const COLOR_MULTISITE = ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00

#End Region

#Region "OVERRRIDES"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_LCD_MESSAGE
    MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId 

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5770)

    ' Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5777)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5776)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    ' INITIALIZE LABEL CAPTIONS

    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(708)
    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(695) ' From
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(696) ' To

    Me.chk_current.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(255) ' Active only
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4941) ' Enabled
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4942) ' Disabled

    'Results
    chk_multisite.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1907) & " (" & SOURCE_MULTISITE & ")" ' Multisite
    lbl_color_multisite.BackColor = GetColor(COLOR_MULTISITE)
    chk_site.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906) & " (" & SOURCE_SITE & ")"  ' Site

    If Not m_is_multisite_member Or m_is_center Then
      Me.gb_results.Visible = False
    End If

    SetDefaultValues()

    GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database
  '
  '   NOTES :
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder

    _str_sql = New StringBuilder()

    _str_sql.AppendLine(" SELECT   MSG_UNIQUE_ID ")
    _str_sql.AppendLine("        , MSG_TYPE ")
    _str_sql.AppendLine("        , MSG_ENABLED ")
    _str_sql.AppendLine("        , MSG_COMPUTED_ORDER ")
    _str_sql.AppendLine("        , MSG_SCHEDULE_START ")
    _str_sql.AppendLine("        , MSG_SCHEDULE_END ")
    _str_sql.AppendLine("        , MSG_MESSAGE ")
    _str_sql.AppendLine("        , MSG_MASTER_SEQUENCE_ID ")
    _str_sql.AppendLine("   FROM   LCD_MESSAGES ")
    If Not m_is_center Then
      _str_sql.AppendLine("  OUTER   APPLY MSG_SITE_LIST.nodes('/Sites/Site') AS X(p) ")
    End If

    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine("  ORDER   BY CASE WHEN MSG_MASTER_SEQUENCE_ID = 0 THEN 0 ELSE 1 END ")
    _str_sql.AppendLine("        , MSG_COMPUTED_ORDER DESC ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean ' GUI_SetupRow

    Dim _lcd_message As CLASS_LCD_MESSAGE
    Dim _msg_id As Int64
    Dim _type As CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE
    Dim _enabled As Boolean
    Dim _order As Integer
    Dim _schedule_start As DateTime
    Dim _schedule_end As DateTime
    Dim _message_line1 As String
    Dim _message_line2 As String
    Dim _master_sequence As Int64

    _lcd_message = New CLASS_LCD_MESSAGE
    _message_line1 = String.Empty
    _message_line2 = String.Empty

    ' ID
    _msg_id = DbRow.Value(SQL_COLUMN_MSG_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_ID).Value = _msg_id

    ' View Order
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_VIEW_ORDER).Value = RowIndex + 1

    ' Schedule Start
    _schedule_start = DbRow.Value(SQL_COLUMN_MSG_SCHEDULE_START)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_SCHEDULE_START).Value = GUI_FormatDate(_schedule_start, _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Schedule End
    If (Not DbRow.IsNull(SQL_COLUMN_MSG_SCHEDULE_END)) Then
      _schedule_end = DbRow.Value(SQL_COLUMN_MSG_SCHEDULE_END)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_SCHEDULE_END).Value = GUI_FormatDate(_schedule_end, _
                                                                                ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                                ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Enabled
    If (Not DbRow.IsNull(SQL_COLUMN_MSG_ENABLED)) Then
      _enabled = DbRow.Value(SQL_COLUMN_MSG_ENABLED)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_ENABLED).Value = IIf(_enabled, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219))
    End If

    If Not m_is_center Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_SOURCE).Value = IIf((_msg_id Mod 1000) = 0, SOURCE_MULTISITE, SOURCE_SITE)
    End If

    ' Type
    _type = DbRow.Value(SQL_COLUMN_MSG_TYPE)
    If _type = CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE.WITHOUT_CARD Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5773)
    ElseIf _type = CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE.ANONYMOUS_ACCOUNT Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5774)
    ElseIf _type = CLASS_LCD_MESSAGE.ENUM_TYPE_MESSAGE.CANCELED_ACCOUNT Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5788)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_TYPE).Value = ""
    End If

    ' Order
    _order = DbRow.Value(SQL_COLUMN_MSG_ORDER)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_ORDER).Value = _order

    ' Message
    WSI.Common.LCDMessages.SplitLcdMessage(DbRow.Value(SQL_COLUMN_MSG_MESSAGE), _message_line1, _message_line2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_MESSAGE_L1).Value = _message_line1
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_MESSAGE_L2).Value = _message_line2

    ' Master sequence
    _master_sequence = DbRow.Value(SQL_COLUMN_MSG_MASTER_SEQUENCE)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MSG_MASTER_SEQUENCE).Value = _master_sequence

    ' Set color if the message is from multisite
    If _master_sequence <> 0 Then
      Me.Grid.Row(RowIndex).BackColor = GetColor(COLOR_MULTISITE)
    End If
    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _lcd_message_id As Integer
    Dim _frm_edit As Object

    'Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    _lcd_message_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_MSG_ID).Value

    _frm_edit = New frm_lcd_message_edit

    Call _frm_edit.ShowEditItem(_lcd_message_id)

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Select first row only if grid has more than one row
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Return Me.Grid.NumRows > 1
  End Function ' GUI_SelectFirstRow


  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter
    Dim _enabled As String = ""

    ' Current only
    If m_filt_current_only Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(255))
    Else
      ' Dates
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(708) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_filt_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(708) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_filt_date_to)

      ' Enabled or disabled
      If m_filt_enabled And m_filt_disabled Or Not m_filt_enabled And Not m_filt_disabled Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218) & " , " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219))
      End If

      If m_filt_enabled And Not m_filt_disabled Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218))
      End If

      If Not m_filt_enabled And m_filt_disabled Then
        PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456), GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219))
      End If

    End If

    ' Multisite and site
    If m_filt_multisite And m_filt_site Or Not m_filt_multisite And Not m_filt_site Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1907) & " , " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906))
    End If

    If m_filt_multisite And Not m_filt_site Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1907))
    End If

    If Not m_filt_multisite And m_filt_site Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906))
    End If
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _string_empty As String
    _string_empty = "---"

    m_filt_current_only = Me.chk_current.Checked
    m_filt_multisite = Me.chk_multisite.Checked
    m_filt_site = Me.chk_site.Checked

    If Me.gb_date.Enabled Then

      ' Date
      If Me.dtp_from.Checked Then
        m_filt_date_from = GUI_FormatDate(Me.dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        m_filt_date_from = _string_empty
      End If

      If Me.dtp_to.Checked Then
        m_filt_date_to = GUI_FormatDate(Me.dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        m_filt_date_to = _string_empty
      End If

    End If

    ' Enable / Disabled
    m_filt_disabled = chk_disabled.Checked
    m_filt_enabled = chk_enabled.Checked

  End Sub ' GUI_ReportUpdateFilters 


  ' PURPOSE: Process clicks on data grid (doible-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON) ' GUI_ButtonClick
    Dim _indx_sel As Integer

    _indx_sel = -1

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW ' New button    
        If Not IsNothing(Me.Grid.SelectedRows) AndAlso Me.Grid.SelectedRows.Length > 0 Then
          _indx_sel = Me.Grid.SelectedRows(0)
        End If

        If _indx_sel <> -1 Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5789), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            Call GUI_NewItemFromSelected()
          Else
            Call GUI_ShowNewLCDMessageForm()
          End If
        Else
          Call GUI_ShowNewLCDMessageForm()
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        If Not MoveMessageDown() Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5782), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        If Not MoveMessageUp() Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5782), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    If SelectedRow >= 0 And Grid.NumRows > 0 Then
      If SelectedRow > 0 Then
        If m_is_center Or Me.Grid.Cell(SelectedRow, GRID_COLUMN_MSG_MASTER_SEQUENCE).Value = 0 And Me.Grid.Cell(SelectedRow - 1, GRID_COLUMN_MSG_MASTER_SEQUENCE).Value = 0 Then
          GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True And CurrentUser.Permissions(ENUM_FORM.FORM_LCD_MESSAGE_EDIT).Write
        End If
      End If

      If SelectedRow <> Grid.NumRows - 1 Then
        If m_is_center Or Me.Grid.Cell(SelectedRow, GRID_COLUMN_MSG_MASTER_SEQUENCE).Value = 0 And Me.Grid.Cell(SelectedRow + 1, GRID_COLUMN_MSG_MASTER_SEQUENCE).Value = 0 Then
          GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True And CurrentUser.Permissions(ENUM_FORM.FORM_LCD_MESSAGE_EDIT).Write
        End If
      End If

    End If
  End Sub ' GUI_RowSelectedEvent

#End Region

#Region "Private Functions"

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_MSG_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_MSG_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_MSG_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_MSG_INDEX).IsColumnPrintable = False

      ' ID
      .Column(GRID_COLUMN_MSG_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_MSG_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_MSG_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_MSG_ID).IsColumnPrintable = False

      ' View Order
      .Column(GRID_COLUMN_MSG_VIEW_ORDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5778)
      .Column(GRID_COLUMN_MSG_VIEW_ORDER).Width = GRID_WIDTH_VIEW_ORDER

      ' Schedule Start
      .Column(GRID_COLUMN_MSG_SCHEDULE_START).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(708)
      .Column(GRID_COLUMN_MSG_SCHEDULE_START).Width = GRID_WIDTH_MSG_SCHEDULE_START
      .Column(GRID_COLUMN_MSG_SCHEDULE_START).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Schedule End
      .Column(GRID_COLUMN_MSG_SCHEDULE_END).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(709)
      .Column(GRID_COLUMN_MSG_SCHEDULE_END).Width = GRID_WIDTH_MSG_SCHEDULE_END
      .Column(GRID_COLUMN_MSG_SCHEDULE_END).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Enabled
      .Column(GRID_COLUMN_MSG_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3456)
      .Column(GRID_COLUMN_MSG_ENABLED).Width = GRID_WIDTH_MSG_ENABLED

      ' Source
      .Column(GRID_COLUMN_MSG_SOURCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3427)
      If Not m_is_multisite_member Or m_is_center Then
        .Column(GRID_COLUMN_MSG_SOURCE).Width = 0
      Else
        .Column(GRID_COLUMN_MSG_SOURCE).Width = GRID_WIDTH_SOURCE
      End If

      ' Type
      .Column(GRID_COLUMN_MSG_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5775)
      .Column(GRID_COLUMN_MSG_TYPE).Width = GRID_WIDTH_MSG_TYPE

      ' Order
      .Column(GRID_COLUMN_MSG_ORDER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5778)
      .Column(GRID_COLUMN_MSG_ORDER).Width = GRID_WIDTH_MSG_ORDER

      ' Message
      .Column(GRID_COLUMN_MSG_MESSAGE_L1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5772) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5703, "1")
      .Column(GRID_COLUMN_MSG_MESSAGE_L1).Width = GRID_WIDTH_MSG_MESSAGE_L1

      ' Message
      .Column(GRID_COLUMN_MSG_MESSAGE_L2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5772) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5703, "2")
      .Column(GRID_COLUMN_MSG_MESSAGE_L2).Width = GRID_WIDTH_MSG_MESSAGE_L2

      ' Master sequence
      .Column(GRID_COLUMN_MSG_MASTER_SEQUENCE).Header(0).Text = ""
      .Column(GRID_COLUMN_MSG_MASTER_SEQUENCE).Width = GRID_WIDTH_MSG_MASTER_SEQUENCE

    End With

  End Sub

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()


    Dim _current_date As Date
    Dim _initial_date As Date
    Dim _final_date As Date

    _current_date = WSI.Common.WGDB.Now

    _initial_date = New Date(_current_date.Year, _current_date.Month, _current_date.Day)
    _final_date = _initial_date.AddDays(1)

    Me.dtp_from.Value = _initial_date
    Me.dtp_to.Value = _final_date

    Me.chk_current.Checked = True
    Me.gb_date.Enabled = False

    Me.dtp_from.Checked = True
    Me.dtp_to.Checked = False

    Me.chk_enabled.Checked = False
    Me.chk_disabled.Checked = False

    Me.chk_multisite.Checked = False
    Me.chk_site.Checked = False

  End Sub

  Private Function GetSqlWhere() As String
    Dim _sb_where As String
    Dim _date_from As String
    Dim _date_to As String

    _sb_where = ""
    _date_from = ""
    _date_to = ""

    If Not m_is_center Then
      _sb_where += "        ((MSG_MASTER_SEQUENCE_ID > 0 AND p.value('./@Id', 'NVARCHAR(4)') = " & GeneralParam.GetInt32("Site", "Identifier") & ") "
      _sb_where += "        	OR "
      _sb_where += "        (MSG_MASTER_SEQUENCE_ID = 0 AND MSG_SITE_LIST IS NULL)) "
    End If

    If Me.chk_current.Checked Then

      _date_from = GUI_FormatDateDB(Date.Now)

      _sb_where += " AND   MSG_SCHEDULE_START <= " & _date_from
      _sb_where += " AND   MSG_ENABLED = 1"
      _sb_where += " AND   (MSG_SCHEDULE_END >= " & _date_from & " OR MSG_SCHEDULE_END IS NULL)"

    Else
      ' Filter Dates
      If Me.dtp_from.Checked Then
        If Me.dtp_from.Checked Then
          _date_from = GUI_FormatDayDB(dtp_from.Value)
        Else
          _date_from = GUI_FormatDateDB(dtp_from.Value)
        End If
      End If
      If Me.dtp_to.Checked Then
        If Me.dtp_to.Checked Then
          _date_to = GUI_FormatDayDB(dtp_to.Value)
        Else
          _date_to = GUI_FormatDateDB(dtp_to.Value)
        End If
      End If

      _sb_where += WSI.Common.Misc.DateSQLFilter("MSG_SCHEDULE_START", _date_from, _date_to, False)

      If (chk_enabled.Checked Xor chk_disabled.Checked) Then
        If chk_enabled.Checked Then
          _sb_where += " AND   MSG_ENABLED = 1"
        ElseIf chk_disabled.Checked Then
          _sb_where += " AND   MSG_ENABLED = 0"
        End If

      End If
    End If

    If m_is_multisite_member And Not m_is_center Then
      If (chk_multisite.Checked Xor chk_site.Checked) Then
        If chk_multisite.Checked Then
          _sb_where += " AND   MSG_MASTER_SEQUENCE_ID <> 0"
        End If
        If chk_site.Checked Then
          _sb_where += " AND   MSG_MASTER_SEQUENCE_ID = 0"
        End If
      End If
    End If

    If String.IsNullOrEmpty(_sb_where) Then
      Return ""
    Else
      Return "WHERE " + _sb_where.Substring(" AND".Length)
    End If

  End Function

  Private Function MoveMessageUp() As Boolean
    Dim _msg_id_selected As Integer
    Dim _msg_id_up As Integer
    Dim _msg_order_selected As Integer
    Dim _msg_order_up As Integer
    Dim _msg_order_view_selected As Integer
    Dim _msg_order_view_up As Integer
    Dim _msg_message_selected_L1 As String
    Dim _msg_message_selected_L2 As String
    Dim _msg_message_up_L1 As String
    Dim _msg_message_up_L2 As String
    Dim _selected_rows As Integer()

    Try

      _selected_rows = Me.Grid.SelectedRows()

      If _selected_rows.Length > 0 And _selected_rows(0) >= 0 And Grid.NumRows > 0 Then
        If _selected_rows(0) > 0 Then

          'Get id
          _msg_id_selected = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_ID).Value
          _msg_id_up = Me.Grid.Cell(_selected_rows(0) - 1, GRID_COLUMN_MSG_ID).Value

          ' Get order values
          _msg_order_selected = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_ORDER).Value
          _msg_order_up = Me.Grid.Cell(_selected_rows(0) - 1, GRID_COLUMN_MSG_ORDER).Value

          ' Get order values
          _msg_order_view_selected = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_VIEW_ORDER).Value
          _msg_order_view_up = Me.Grid.Cell(_selected_rows(0) - 1, GRID_COLUMN_MSG_VIEW_ORDER).Value

          ' Get message
          _msg_message_selected_L1 = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_MESSAGE_L1).Value
          _msg_message_up_L1 = Me.Grid.Cell(_selected_rows(0) - 1, GRID_COLUMN_MSG_MESSAGE_L1).Value

          ' Get message
          _msg_message_selected_L2 = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_MESSAGE_L2).Value
          _msg_message_up_L2 = Me.Grid.Cell(_selected_rows(0) - 1, GRID_COLUMN_MSG_MESSAGE_L2).Value

          ' Save data
          Using _db_trx As WSI.Common.DB_TRX = New WSI.Common.DB_TRX()

            If Not UpdateLCDMessageOrder(_msg_id_selected, _msg_order_up, _db_trx.SqlTransaction) Then
              Return False
            End If

            If Not UpdateLCDMessageOrder(_msg_id_up, _msg_order_selected, _db_trx.SqlTransaction) Then
              Return False
            End If

            Call AuditChanges(_msg_message_selected_L1, _msg_message_selected_L2, _msg_order_view_up, _msg_message_up_L1, _msg_message_up_L2, _msg_order_view_selected)

            _db_trx.Commit()

          End Using
        End If
      End If

      Me.GUI_ExecuteQuery()

      SetLastSelectedRow(_msg_id_selected)

      Return True
    Catch ex As Exception

    End Try

    Return False
  End Function

  Private Function MoveMessageDown() As Boolean
    Dim _msg_id_selected As Integer
    Dim _msg_id_down As Integer
    Dim _msg_order_selected As Integer
    Dim _msg_order_down As Integer
    Dim _msg_view_order_selected As Integer
    Dim _msg_view_order_down As Integer
    Dim _msg_message_selected_L1 As String
    Dim _msg_message_selected_L2 As String
    Dim _msg_message_down_L1 As String
    Dim _msg_message_down_L2 As String
    Dim _selected_rows As Integer()

    Try

      _selected_rows = Me.Grid.SelectedRows()

      If _selected_rows.Length > 0 And _selected_rows(0) >= 0 And Grid.NumRows > 0 Then
        If _selected_rows(0) < Grid.NumRows Then
          'Get id
          _msg_id_selected = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_ID).Value
          _msg_id_down = Me.Grid.Cell(_selected_rows(0) + 1, GRID_COLUMN_MSG_ID).Value

          ' Get order values
          _msg_order_selected = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_ORDER).Value
          _msg_order_down = Me.Grid.Cell(_selected_rows(0) + 1, GRID_COLUMN_MSG_ORDER).Value

          ' Get View order
          _msg_view_order_selected = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_VIEW_ORDER).Value
          _msg_view_order_down = Me.Grid.Cell(_selected_rows(0) + 1, GRID_COLUMN_MSG_VIEW_ORDER).Value

          ' Get message
          _msg_message_selected_L1 = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_MESSAGE_L1).Value
          _msg_message_down_L1 = Me.Grid.Cell(_selected_rows(0) + 1, GRID_COLUMN_MSG_MESSAGE_L1).Value

          _msg_message_selected_L2 = Me.Grid.Cell(_selected_rows(0), GRID_COLUMN_MSG_MESSAGE_L2).Value
          _msg_message_down_L2 = Me.Grid.Cell(_selected_rows(0) + 1, GRID_COLUMN_MSG_MESSAGE_L2).Value

          ' Save data
          Using _db_trx As WSI.Common.DB_TRX = New WSI.Common.DB_TRX()

            If Not UpdateLCDMessageOrder(_msg_id_selected, _msg_order_down, _db_trx.SqlTransaction) Then
              Return False
            End If

            If Not UpdateLCDMessageOrder(_msg_id_down, _msg_order_selected, _db_trx.SqlTransaction) Then
              Return False
            End If

            Call AuditChanges(_msg_message_selected_L1, _msg_message_selected_L2, _msg_view_order_down, _msg_message_down_L1, _msg_message_down_L2, _msg_view_order_selected)

            _db_trx.Commit()

          End Using
        End If
      End If

      Me.GUI_ExecuteQuery()

      SetLastSelectedRow(_msg_id_selected)

      Return True

    Catch ex As Exception
    End Try

    Return False
  End Function

  ' PURPOSE: Update message order to DB
  '
  '  PARAMS:
  '     - INPUT :
  '       MsgId
  '       Order
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Private Function UpdateLCDMessageOrder(ByVal MsgId As Integer, ByVal Order As Integer, ByVal SqlTrans As SqlClient.SqlTransaction) As Boolean
    Dim _sb As StringBuilder
    Dim _rows_updated As Integer

    Try

      _sb = New StringBuilder()

      _sb.AppendLine(" UPDATE   LCD_MESSAGES ")
      _sb.AppendLine("    SET   MSG_ORDER = @pOrder ")
      _sb.AppendLine("  WHERE   MSG_UNIQUE_ID = @pMsgId")

      Using _cmd As New SqlClient.SqlCommand(_sb.ToString, SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pOrder", SqlDbType.Int).Value = Order
        _cmd.Parameters.Add("@pMsgId", SqlDbType.BigInt).Value = MsgId

        _rows_updated = _cmd.ExecuteNonQuery()

        If _rows_updated = 1 Then
          Return True
        End If
      End Using
      Return False
    Catch ex As Exception
    End Try

    Return False

  End Function ' UpdateLCDMessageOrder

  Private Sub SetLastSelectedRow(ByVal MsgId As Integer)
    Dim _idx_row As Integer

    ' Unselect all rows
    For _idx_row = 0 To Me.Grid.NumRows - 1
      Me.Grid.IsSelected(_idx_row) = False
    Next

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Cell(_idx_row, GRID_COLUMN_MSG_ID).Value = MsgId Then
        Me.Grid.IsSelected(_idx_row) = True
        Exit For
      End If
    Next

    GUI_RowSelectedEvent(_idx_row)

  End Sub

  ' PURPOSE : Adds a new lcd message table to the system (Copy message)
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_NewItemFromSelected()

    Dim _idx_row As Int32
    Dim _lcd_message_id As Integer

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the GamingTable ID, and launch the editor
    _lcd_message_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_MSG_ID).Value

    Call GUI_ShowNewLCDMessageForm(_lcd_message_id)

  End Sub ' NewPromotion

  ' PURPOSE: Auditor Data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AuditChanges(ByVal Message1_L1 As String, ByVal Message1_L2 As String, ByVal Order1 As Integer, _
                            ByVal Message2_L1 As String, ByVal Message2_L2 As String, ByVal Order2 As Integer)
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _orig_auditor As CLASS_AUDITOR_DATA

    _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _orig_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    Call _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5770), "")
    Call _orig_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(5770), "")

    Call _curr_auditor.SetField(0, Order1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5783, "'" & Message1_L1 & "' - '" & Message1_L2 & "'"))
    Call _orig_auditor.SetField(0, Order2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5783, "'" & Message1_L1 & "' - '" & Message1_L2 & "'"))

    Call _curr_auditor.SetField(0, Order2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5783, "'" & Message2_L1 & "' - '" & Message2_L2 & "'"))
    Call _orig_auditor.SetField(0, Order1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5783, "'" & Message2_L1 & "' - '" & Message2_L2 & "'"))

    ' Notify
    Call _curr_auditor.Notify(CurrentUser.GuiId, _
                                  CurrentUser.Id, _
                                  CurrentUser.Name, _
                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                  0, _
                                  _orig_auditor)

    _curr_auditor = Nothing
    _orig_auditor = Nothing

  End Sub


#End Region

#Region "Public Functions"
  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, Optional ByVal _procedence_promotions As Boolean = False)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE : Adds a new lcd message to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_ShowNewLCDMessageForm(Optional ByVal LCDMessageCopiedId As Long = 0)

    Dim frm_edit As frm_lcd_message_edit
    frm_edit = New frm_lcd_message_edit()
    frm_edit.ShowNewItem(LCDMessageCopiedId)
    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' NewPromotion
#End Region

#Region "Events"

  Private Sub chk_current_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_current.CheckedChanged
    Me.gb_date.Enabled = Not Me.chk_current.Checked
    Me.chk_disabled.Enabled = Not Me.chk_current.Checked
    Me.chk_enabled.Enabled = Not Me.chk_current.Checked
  End Sub

#End Region


End Class