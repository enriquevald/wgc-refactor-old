<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customer_base
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_customer_base))
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.chk_only_anonymous = New System.Windows.Forms.CheckBox()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.gb_gender = New System.Windows.Forms.GroupBox()
    Me.chk_gender_female = New System.Windows.Forms.CheckBox()
    Me.chk_gender_male = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.chk_holder_data = New System.Windows.Forms.CheckBox()
    Me.chk_current = New System.Windows.Forms.CheckBox()
    Me.ef_days_activity = New GUI_Controls.uc_entry_field()
    Me.gb_activity = New System.Windows.Forms.GroupBox()
    Me.pgb_maximum = New System.Windows.Forms.Panel()
    Me.rb_without_maximum = New System.Windows.Forms.RadioButton()
    Me.rb_maximum = New System.Windows.Forms.RadioButton()
    Me.rb_activity = New System.Windows.Forms.RadioButton()
    Me.rb_all = New System.Windows.Forms.RadioButton()
    Me.gb_flag = New System.Windows.Forms.GroupBox()
    Me.btn_select_none_flags = New System.Windows.Forms.Button()
    Me.btn_select_all_flags = New System.Windows.Forms.Button()
    Me.dg_select_flag = New GUI_Controls.uc_grid()
    Me.chk_show_bucket = New System.Windows.Forms.CheckBox()
    Me.chk_only_without_welcome_draw = New System.Windows.Forms.CheckBox()
    Me.chk_only_with_welcome_draw = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_gender.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_activity.SuspendLayout()
    Me.pgb_maximum.SuspendLayout()
    Me.gb_flag.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_only_without_welcome_draw)
    Me.panel_filter.Controls.Add(Me.chk_only_with_welcome_draw)
    Me.panel_filter.Controls.Add(Me.chk_show_bucket)
    Me.panel_filter.Controls.Add(Me.gb_flag)
    Me.panel_filter.Controls.Add(Me.chk_current)
    Me.panel_filter.Controls.Add(Me.gb_activity)
    Me.panel_filter.Controls.Add(Me.chk_holder_data)
    Me.panel_filter.Controls.Add(Me.chk_only_anonymous)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_gender)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1174, 233)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_gender, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_anonymous, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_holder_data, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_activity, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_current, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_flag, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_bucket, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_with_welcome_draw, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_without_welcome_draw, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 237)
    Me.panel_data.Size = New System.Drawing.Size(1174, 471)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1168, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1168, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = False
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.FromDateText = "#5339"
    Me.uc_dsl.Location = New System.Drawing.Point(6, 3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(300, 80)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDateText = "#5340"
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(768, 0)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 8
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'chk_only_anonymous
    '
    Me.chk_only_anonymous.AutoSize = True
    Me.chk_only_anonymous.Location = New System.Drawing.Point(327, 163)
    Me.chk_only_anonymous.Name = "chk_only_anonymous"
    Me.chk_only_anonymous.Size = New System.Drawing.Size(130, 17)
    Me.chk_only_anonymous.TabIndex = 4
    Me.chk_only_anonymous.Text = "xOnly Anonymous"
    Me.chk_only_anonymous.UseVisualStyleBackColor = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(316, 3)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(170, 93)
    Me.gb_level.TabIndex = 2
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(11, 16)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 0
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(11, 34)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 1
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(11, 53)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 2
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(11, 72)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 3
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'gb_gender
    '
    Me.gb_gender.Controls.Add(Me.chk_gender_female)
    Me.gb_gender.Controls.Add(Me.chk_gender_male)
    Me.gb_gender.Location = New System.Drawing.Point(316, 98)
    Me.gb_gender.Name = "gb_gender"
    Me.gb_gender.Size = New System.Drawing.Size(170, 59)
    Me.gb_gender.TabIndex = 3
    Me.gb_gender.TabStop = False
    Me.gb_gender.Text = "xGender"
    '
    'chk_gender_female
    '
    Me.chk_gender_female.Location = New System.Drawing.Point(11, 35)
    Me.chk_gender_female.Name = "chk_gender_female"
    Me.chk_gender_female.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_female.TabIndex = 1
    Me.chk_gender_female.Text = "xWomen"
    '
    'chk_gender_male
    '
    Me.chk_gender_male.Location = New System.Drawing.Point(11, 18)
    Me.chk_gender_male.Name = "chk_gender_male"
    Me.chk_gender_male.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_male.TabIndex = 0
    Me.chk_gender_male.Text = "xMen"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(771, 140)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(192, 87)
    Me.gb_date.TabIndex = 9
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xCreationDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 48)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(177, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(177, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'chk_holder_data
    '
    Me.chk_holder_data.AutoSize = True
    Me.chk_holder_data.Location = New System.Drawing.Point(327, 179)
    Me.chk_holder_data.Name = "chk_holder_data"
    Me.chk_holder_data.Size = New System.Drawing.Size(128, 17)
    Me.chk_holder_data.TabIndex = 5
    Me.chk_holder_data.Text = "xShowHolderData"
    Me.chk_holder_data.UseVisualStyleBackColor = True
    '
    'chk_current
    '
    Me.chk_current.AutoSize = True
    Me.chk_current.Location = New System.Drawing.Point(327, 196)
    Me.chk_current.Name = "chk_current"
    Me.chk_current.Size = New System.Drawing.Size(129, 17)
    Me.chk_current.TabIndex = 6
    Me.chk_current.Text = "xCurrent situation"
    Me.chk_current.UseVisualStyleBackColor = True
    '
    'ef_days_activity
    '
    Me.ef_days_activity.DoubleValue = 0.0R
    Me.ef_days_activity.IntegerValue = 0
    Me.ef_days_activity.IsReadOnly = False
    Me.ef_days_activity.Location = New System.Drawing.Point(88, 22)
    Me.ef_days_activity.Name = "ef_days_activity"
    Me.ef_days_activity.PlaceHolder = Nothing
    Me.ef_days_activity.Size = New System.Drawing.Size(185, 24)
    Me.ef_days_activity.SufixText = "x days"
    Me.ef_days_activity.SufixTextVisible = True
    Me.ef_days_activity.SufixTextWidth = 140
    Me.ef_days_activity.TabIndex = 2
    Me.ef_days_activity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_days_activity.TextValue = ""
    Me.ef_days_activity.TextVisible = False
    Me.ef_days_activity.TextWidth = 0
    Me.ef_days_activity.Value = ""
    Me.ef_days_activity.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_activity
    '
    Me.gb_activity.Controls.Add(Me.pgb_maximum)
    Me.gb_activity.Controls.Add(Me.rb_activity)
    Me.gb_activity.Controls.Add(Me.rb_all)
    Me.gb_activity.Location = New System.Drawing.Point(6, 86)
    Me.gb_activity.Name = "gb_activity"
    Me.gb_activity.Size = New System.Drawing.Size(300, 115)
    Me.gb_activity.TabIndex = 1
    Me.gb_activity.TabStop = False
    Me.gb_activity.Text = "xActivity"
    '
    'pgb_maximum
    '
    Me.pgb_maximum.Controls.Add(Me.rb_without_maximum)
    Me.pgb_maximum.Controls.Add(Me.rb_maximum)
    Me.pgb_maximum.Controls.Add(Me.ef_days_activity)
    Me.pgb_maximum.Location = New System.Drawing.Point(19, 61)
    Me.pgb_maximum.Name = "pgb_maximum"
    Me.pgb_maximum.Size = New System.Drawing.Size(275, 49)
    Me.pgb_maximum.TabIndex = 2
    '
    'rb_without_maximum
    '
    Me.rb_without_maximum.AutoSize = True
    Me.rb_without_maximum.Location = New System.Drawing.Point(3, 3)
    Me.rb_without_maximum.Name = "rb_without_maximum"
    Me.rb_without_maximum.Size = New System.Drawing.Size(140, 17)
    Me.rb_without_maximum.TabIndex = 0
    Me.rb_without_maximum.TabStop = True
    Me.rb_without_maximum.Text = "x Without maximum"
    Me.rb_without_maximum.UseVisualStyleBackColor = True
    '
    'rb_maximum
    '
    Me.rb_maximum.AutoSize = True
    Me.rb_maximum.Location = New System.Drawing.Point(3, 26)
    Me.rb_maximum.Name = "rb_maximum"
    Me.rb_maximum.Size = New System.Drawing.Size(91, 17)
    Me.rb_maximum.TabIndex = 1
    Me.rb_maximum.TabStop = True
    Me.rb_maximum.Text = "x Maximum"
    Me.rb_maximum.UseVisualStyleBackColor = True
    '
    'rb_activity
    '
    Me.rb_activity.AutoSize = True
    Me.rb_activity.Location = New System.Drawing.Point(6, 39)
    Me.rb_activity.Name = "rb_activity"
    Me.rb_activity.Size = New System.Drawing.Size(182, 17)
    Me.rb_activity.TabIndex = 1
    Me.rb_activity.TabStop = True
    Me.rb_activity.Text = "x With activity in the period"
    Me.rb_activity.UseVisualStyleBackColor = True
    '
    'rb_all
    '
    Me.rb_all.AutoSize = True
    Me.rb_all.Location = New System.Drawing.Point(6, 18)
    Me.rb_all.Name = "rb_all"
    Me.rb_all.Size = New System.Drawing.Size(50, 17)
    Me.rb_all.TabIndex = 0
    Me.rb_all.TabStop = True
    Me.rb_all.Text = "x All"
    Me.rb_all.UseVisualStyleBackColor = True
    '
    'gb_flag
    '
    Me.gb_flag.Controls.Add(Me.btn_select_none_flags)
    Me.gb_flag.Controls.Add(Me.btn_select_all_flags)
    Me.gb_flag.Controls.Add(Me.dg_select_flag)
    Me.gb_flag.Location = New System.Drawing.Point(495, 3)
    Me.gb_flag.Name = "gb_flag"
    Me.gb_flag.Size = New System.Drawing.Size(267, 154)
    Me.gb_flag.TabIndex = 7
    Me.gb_flag.TabStop = False
    Me.gb_flag.Text = "#17286"
    '
    'btn_select_none_flags
    '
    Me.btn_select_none_flags.Location = New System.Drawing.Point(135, 117)
    Me.btn_select_none_flags.Name = "btn_select_none_flags"
    Me.btn_select_none_flags.Size = New System.Drawing.Size(125, 30)
    Me.btn_select_none_flags.TabIndex = 2
    Me.btn_select_none_flags.Text = "btn_select_none"
    Me.btn_select_none_flags.UseVisualStyleBackColor = True
    '
    'btn_select_all_flags
    '
    Me.btn_select_all_flags.Location = New System.Drawing.Point(6, 117)
    Me.btn_select_all_flags.Name = "btn_select_all_flags"
    Me.btn_select_all_flags.Size = New System.Drawing.Size(125, 30)
    Me.btn_select_all_flags.TabIndex = 1
    Me.btn_select_all_flags.Text = "xSelectAll"
    Me.btn_select_all_flags.UseVisualStyleBackColor = True
    '
    'dg_select_flag
    '
    Me.dg_select_flag.CurrentCol = -1
    Me.dg_select_flag.CurrentRow = -1
    Me.dg_select_flag.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_select_flag.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_select_flag.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_select_flag.Location = New System.Drawing.Point(6, 15)
    Me.dg_select_flag.Name = "dg_select_flag"
    Me.dg_select_flag.PanelRightVisible = False
    Me.dg_select_flag.Redraw = True
    Me.dg_select_flag.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_select_flag.Size = New System.Drawing.Size(255, 100)
    Me.dg_select_flag.Sortable = False
    Me.dg_select_flag.SortAscending = True
    Me.dg_select_flag.SortByCol = 0
    Me.dg_select_flag.TabIndex = 0
    Me.dg_select_flag.ToolTipped = True
    Me.dg_select_flag.TopRow = -2
    Me.dg_select_flag.WordWrap = False
    '
    'chk_show_bucket
    '
    Me.chk_show_bucket.AutoSize = True
    Me.chk_show_bucket.Location = New System.Drawing.Point(327, 213)
    Me.chk_show_bucket.Name = "chk_show_bucket"
    Me.chk_show_bucket.Size = New System.Drawing.Size(109, 17)
    Me.chk_show_bucket.TabIndex = 13
    Me.chk_show_bucket.Text = "xShowBuckets"
    Me.chk_show_bucket.UseVisualStyleBackColor = True
    '
    'chk_only_without_welcome_draw
    '
    Me.chk_only_without_welcome_draw.AutoSize = True
    Me.chk_only_without_welcome_draw.Location = New System.Drawing.Point(536, 176)
    Me.chk_only_without_welcome_draw.Name = "chk_only_without_welcome_draw"
    Me.chk_only_without_welcome_draw.Size = New System.Drawing.Size(181, 17)
    Me.chk_only_without_welcome_draw.TabIndex = 15
    Me.chk_only_without_welcome_draw.Text = "xOnlyWithoutTerminalDraw"
    Me.chk_only_without_welcome_draw.UseVisualStyleBackColor = True
    '
    'chk_only_with_welcome_draw
    '
    Me.chk_only_with_welcome_draw.AutoSize = True
    Me.chk_only_with_welcome_draw.Location = New System.Drawing.Point(536, 160)
    Me.chk_only_with_welcome_draw.Name = "chk_only_with_welcome_draw"
    Me.chk_only_with_welcome_draw.Size = New System.Drawing.Size(163, 17)
    Me.chk_only_with_welcome_draw.TabIndex = 14
    Me.chk_only_with_welcome_draw.Text = "xOnlyWithTerminalDraw"
    Me.chk_only_with_welcome_draw.UseVisualStyleBackColor = True
    '
    'frm_customer_base
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1184, 712)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_customer_base"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_customer_base"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_gender.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_activity.ResumeLayout(False)
    Me.gb_activity.PerformLayout()
    Me.pgb_maximum.ResumeLayout(False)
    Me.pgb_maximum.PerformLayout()
    Me.gb_flag.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents chk_only_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents gb_gender As System.Windows.Forms.GroupBox
  Friend WithEvents chk_gender_female As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_male As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_holder_data As System.Windows.Forms.CheckBox
  Friend WithEvents chk_current As System.Windows.Forms.CheckBox
  Friend WithEvents ef_days_activity As GUI_Controls.uc_entry_field
  Friend WithEvents gb_activity As System.Windows.Forms.GroupBox
  Friend WithEvents rb_activity As System.Windows.Forms.RadioButton
  Friend WithEvents rb_all As System.Windows.Forms.RadioButton
  Friend WithEvents rb_without_maximum As System.Windows.Forms.RadioButton
  Friend WithEvents rb_maximum As System.Windows.Forms.RadioButton
  Friend WithEvents pgb_maximum As System.Windows.Forms.Panel
  Friend WithEvents gb_flag As System.Windows.Forms.GroupBox
  Friend WithEvents btn_select_none_flags As System.Windows.Forms.Button
  Friend WithEvents btn_select_all_flags As System.Windows.Forms.Button
  Friend WithEvents dg_select_flag As GUI_Controls.uc_grid
  Friend WithEvents chk_show_bucket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_without_welcome_draw As System.Windows.Forms.CheckBox
  Friend WithEvents chk_only_with_welcome_draw As System.Windows.Forms.CheckBox
End Class
