'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_sw_version.vb
' DESCRIPTION:   Monitor for WCP and WC2 services
' AUTHOR:        Agust� Poch
' CREATION DATE: 02-DEC-2008
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-DEC-2008  APB    Initial Draft.
' 06-SEP-2012  DDM    Added PromoBOX in filter.
' 29-APR-2013  DHA    Fixed bug #734: ignores filters on forms automatic update (timer).
' 28-JUN-2013  DMR    Added Excel Functionality.
' 23-AUG-2013  CCG    Remove Duplicated methods.
' 12-DEC-2013  LJM    Fixed Bug #WIG-480 Wrong message on Audit forms.
' 08-JAN-2014  DHA    Fixed Bug #WIGOSTITO-951 A message is shown when the forms is not initialized yet.
' 12-MAR-2014  DLL    Fixed Bug #WIG-717 Report Filter is not correct. Without search update filters.
' 29-MAY-2014  JAB    Added permissions: To buttons "Excel" and "Print".
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 23-FEB-2015  OPC    Fixed Bug #WIG-2094: wrong version when export to excel.
' 24-FEB-2015  ANM    Enhanced Terminal version control
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 14-APR-2015  FOS    TASK 1074: Grid BackGround Color white for  WCP and WC2 name server
' 18-JUL-2017  MS     PBI 28847: WIGOS-3645 Errors in selection criteria in Terminal control versions screen
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_CommonMisc
Imports WSI.Common

Public Class frm_terminal_sw_version
  Inherits frm_base

#Region "Constants"

  Private m_print_data As GUI_Reports.CLASS_PRINT_DATA
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA
  Private m_print_datetime As Date

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private GRID_COLUMN_SENTINEL As Int32
  Private GRID_INIT_TERMINAL_DATA As Int32
  Private GRID_COLUMN_TERMINAL As Int32
  Private GRID_COLUMN_TERMINAL_TYPE As Int32
  Private GRID_COLUMN_BUILD_STR As Int32
  Private GRID_COLUMN_STATUS_SESSION As Int32
  Private GRID_COLUMN_STATUS_WCP As Int32
  Private GRID_COLUMN_SERVER_WCP As Int32
  Private GRID_COLUMN_STATUS_WC2 As Int32
  Private GRID_COLUMN_SERVER_WC2 As Int32
  Private GRID_COLUMN_UPDATE_STATUS As Int32
  Private GRID_COLUMN_STATUS_SESSION_CODE As Int32
  Private GRID_COLUMN_STATUS_WCP_CODE As Int32
  Private GRID_COLUMN_STATUS_WC2_CODE As Int32
  Private GRID_COLUMN_TERMINAL_ID As Int32

  Private GRID_COLUMNS As Int32

  Private Const SQL_COLUMN_TERMINAL = 0
  Private Const SQL_COLUMN_PROVIDER = 1
  Private Const SQL_COLUMN_IN_USE = 2
  Private Const SQL_COLUMN_CLIENT_ID = 3
  Private Const SQL_COLUMN_BUILD_ID = 4
  Private Const SQL_COLUMN_UPDATE_STATUS = 5
  Private Const SQL_COLUMN_TERMINAL_ID = 6

  Private Const EXCEL_BUILD_STR = 4

  ' Grid counters
  Private Const COUNTER_TERMINAL_UP_TO_DATE As Integer = 1
  Private Const COUNTER_TERMINAL_OUTDATED As Integer = 2

  ' Session Status
  ' Private Const TERM_STATUS_STOPPED = 0
  Private Const TERM_STATUS_FREE = 1
  Private Const TERM_STATUS_BUSY = 2

  ' WCP & WC2 status
  Private Const TERM_STATUS_DISCONNECTED = 0
  Private Const TERM_STATUS_CONNECTED = 1

  Private Const PLAY_SESSION_STATUS_OPEN = 0

  ' Time since last message to consider a terminal as stopped
  Private Const ACTIVITY_TIMEOUT = 180

  ' Timer interval to refresh services status (in milliseconds)
  Private Const REFRESH_TIMEOUT = 5000

#End Region

#Region "Enums"

  Private Enum ENUM_UPDATE_STATUS
    STATUS_OUTDATED = 0
    STATUS_TO_DATE = 1
  End Enum

#End Region ' Enums

#Region "Members"

  Private m_filter_term_id_list As String
  Private m_filter_term_selected_one As Boolean
  Private m_filter_term_selected_several As Boolean

  Private m_filter_session_free As Boolean
  Private m_filter_session_oc As Boolean

  Private m_filter_wcp_conn As Boolean
  Private m_filter_wcp_disconn As Boolean

  Private m_filter_wc2_conn As Boolean
  Private m_filter_wc2_disconn As Boolean

  Private m_filter_active_only As Boolean

  Private m_filter_term_type_id_list As String

  Private m_refresh_click As String

  Private m_session As String
  Private m_wcp As String
  Private m_wc2 As String
  Private m_terminal As String
  Private m_term_type As String
  Private m_active_only As String
  Private m_location_check As String

#End Region ' Members

#Region "Properties"

  Public ReadOnly Property Grid() As uc_grid
    Get
      Return Me.dg_list
    End Get
  End Property

#End Region

#Region "Overrides"

  ' PURPOSE: Set the internal Form Id
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_BY_BUILD
    Call MyBase.GUI_SetFormId()
  End Sub 'GUI_SetFormId

  ' PURPOSE: Initialize all form controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    ' Form Title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4833)

    Me.btn_excel.Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.btn_excel.Enabled = False

    Me.btn_exit.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(2)
    Me.gb_status.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(217)

    Me.tf_to_date.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(222)
    GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_TO_DATE, _
                          fore_color, _
                          back_color)
    lbl_running.BackColor = back_color
    lbl_running.ForeColor = fore_color

    Me.tf_outdated.Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(221)
    GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_OUTDATED, _
                          fore_color, _
                          back_color)
    lbl_standby.BackColor = back_color
    lbl_standby.ForeColor = fore_color

    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)

    Me.btn_refresh.Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    Me.btn_filter_reset.Text = GLB_NLS_GUI_CONTROLS.GetString(7)

    Me.gb_session.Text = GLB_NLS_GUI_CONTROLS.GetString(354)
    Me.cb_session_free.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(218)
    Me.cb_session_oc.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)

    GetUpdateStatusColor(TERM_STATUS_CONNECTED, _
                          fore_color, _
                          back_color)
    lbl_color_wcp_connected.BackColor = back_color
    lbl_color_wcp_connected.ForeColor = fore_color
    lbl_color_wc2_connected.BackColor = back_color
    lbl_color_wc2_connected.ForeColor = fore_color
    GetUpdateStatusColor(TERM_STATUS_DISCONNECTED, _
                          fore_color, _
                          back_color)
    lbl_color_wcp_disconnected.BackColor = back_color
    lbl_color_wcp_disconnected.ForeColor = fore_color
    lbl_color_wc2_disconnected.BackColor = back_color
    lbl_color_wc2_disconnected.ForeColor = fore_color

    Me.gb_wc2.Text = "WC2"
    Me.cb_wc2_connected.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
    Me.cb_wc2_disconnected.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)

    Me.gb_wcp.Text = "WCP"
    Me.cb_wcp_connected.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
    Me.cb_wcp_disconnected.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)

    Me.cb_active_only.Text = GLB_NLS_GUI_CONFIGURATION.GetString(496)

    Me.Timer1.Interval = REFRESH_TIMEOUT

    m_filter_session_free = Me.cb_session_free.Checked()
    m_filter_session_oc = Me.cb_session_oc.Checked()
    m_filter_wcp_conn = Me.cb_wcp_connected.Checked()
    m_filter_wcp_disconn = Me.cb_wcp_disconnected.Checked()
    m_filter_wc2_conn = Me.cb_wc2_connected.Checked()
    m_filter_wc2_disconn = Me.cb_wc2_disconnected.Checked()
    m_filter_active_only = Me.cb_active_only.Checked()

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    GUI_StyleSheet()

    ' Providers - Terminals
    Dim _terminal_types() As Integer = Nothing

    _terminal_types = WSI.Common.Misc.AllTerminalTypesExcept3GS()
    Call Me.uc_pr_list.Init(_terminal_types)

    Call Me.uc_pr_list.SetDefaultValues()

    Call Me.uc_type_list.Init(_terminal_types)

    Me.Timer1.Start()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FirstActivation()
    Call GUI_ReportUpdateFilters()
    Call Refresh_Grid()
  End Sub

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Refresh data in the grid with fresh info from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub RefreshGrid()

    Dim data_table As DataTable
    Dim row_index As Integer
    Dim data_row As DataRow
    Dim num_updated As Integer
    Dim num_outdated As Integer
    Dim terminal_name As String
    Dim found_rows() As Data.DataRow

    Try

      uc_type_list.RefreshVersions()

      If m_refresh_grid Then
        Call GUI_StyleSheet()
      End If

      m_refresh_grid = False

      ' Data Grid
      data_table = GUI_GetTableUsingSQL(GetSqlQuery(), 5000)

      If IsNothing(data_table) Then
        Exit Sub
      End If

      num_updated = 0
      num_outdated = 0

      row_index = 0

      ' update existing rows
      While row_index < Me.dg_list.NumRows

        terminal_name = Me.dg_list.Cell(row_index, GRID_COLUMN_TERMINAL).Value

        ' Search for terminal
        found_rows = data_table.Select("te_name Like '" + DataTable_FilterValue(terminal_name) + "'")

        If found_rows.Length > 0 Then

          ' Terminal found
          ' Only change if changed version, session, wcp or wc2
          If Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value <> found_rows(0).Item("update_status") _
          Or Me.dg_list.Cell(row_index, GRID_COLUMN_STATUS_SESSION_CODE).Value <> found_rows(0).Item("session_state") _
          Or Me.dg_list.Cell(row_index, GRID_COLUMN_STATUS_WCP_CODE).Value <> found_rows(0).Item("wcp_state") _
          Or Me.dg_list.Cell(row_index, GRID_COLUMN_STATUS_WC2_CODE).Value <> found_rows(0).Item("wc2_state") Then
            ' Update fields
            Me.dg_list.Redraw = False
            UpdateRow(found_rows(0), row_index)
            Me.dg_list.Redraw = True
          End If

          ' mark row from datatable to be deleted
          found_rows(0).Delete()

          ' update counter
          If Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE Then
            num_updated += 1
          Else
            num_outdated += 1
          End If

          row_index = row_index + 1

        Else
          ' Terminal not found, delete grid row!
          Me.dg_list.Redraw = False
          Me.dg_list.DeleteRow(row_index)
          Me.dg_list.Redraw = True
        End If

      End While

      data_table.AcceptChanges()

      ' New rows to add
      For Each data_row In data_table.Rows
        Me.dg_list.Redraw = False
        Me.dg_list.AddRow()

        UpdateRow(data_row, row_index)

        ' Update counters
        If Me.dg_list.Cell(row_index, GRID_COLUMN_UPDATE_STATUS).Value = ENUM_UPDATE_STATUS.STATUS_TO_DATE Then
          num_updated += 1
        Else
          num_outdated += 1
        End If

        row_index += 1
        Me.dg_list.Redraw = True
      Next

      Me.dg_list.Counter(COUNTER_TERMINAL_UP_TO_DATE).Value = num_updated
      Me.dg_list.Counter(COUNTER_TERMINAL_OUTDATED).Value = num_outdated

      Me.btn_excel.Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read

      If Me.dg_list.NumRows <= 0 Then
        If m_refresh_click = 1 Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          m_refresh_click = 0
        End If

      End If

    Catch ex As Exception

    End Try
  End Sub ' RefreshGrid

  ' PURPOSE: Update or insert data in grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - DataRow
  '           - RowIndex
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '   
  Private Function UpdateRow(ByVal Row As DataRow, ByVal RowIndex As Integer) As Integer

    Dim fore_color As Color
    Dim back_color As Color
    Dim _terminal_data As List(Of Object)

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = Row.Item("te_terminal_id")

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(Row.Item("te_terminal_id"), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(CInt(Row.Item("te_terminal_type")))

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WCP_CODE).Value = Row.Item("wcp_state")

    GetUpdateStatusColor(Row.Item("wcp_state"), fore_color, back_color)

    Select Case Row.Item("wcp_state")
      Case TERM_STATUS_CONNECTED
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WCP).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_SERVER_WCP).Value = Row.Item("ws_server_name")

      Case TERM_STATUS_DISCONNECTED
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WCP).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WCP).ForeColor = fore_color
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WCP).BackColor = back_color
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_SERVER_WCP).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1048)

      Case Else
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WCP).Value = ""
    End Select

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WC2_CODE).Value = Row.Item("wc2_state")
    Select Case Row.Item("wc2_state")
      Case TERM_STATUS_CONNECTED
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WC2).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(325)
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_SERVER_WC2).Value = Row.Item("w2s_server_name")

      Case TERM_STATUS_DISCONNECTED
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WC2).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(326)
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WC2).ForeColor = fore_color
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WC2).BackColor = back_color
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_SERVER_WC2).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1048)

      Case Else
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_WC2).Value = ""
    End Select

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_SESSION_CODE).Value = Row.Item("session_state")
    Select Case Row.Item("session_state")
      Case TERM_STATUS_FREE
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_SESSION).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(218)

      Case TERM_STATUS_BUSY
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_SESSION).Value = GLB_NLS_GUI_SW_DOWNLOAD.GetString(216)

      Case Else
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_STATUS_SESSION).Value = ""
    End Select

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_UPDATE_STATUS).Value = Row.Item("update_status")
    GetUpdateStatusColor(Me.dg_list.Cell(RowIndex, GRID_COLUMN_UPDATE_STATUS).Value, fore_color, back_color)

    If (Not IsDBNull(Row.Item("te_client_id")) And _
        Not IsDBNull(Row.Item("te_build_id"))) Then
      Me.dg_list.Cell(RowIndex, GRID_COLUMN_BUILD_STR).Value = uc_terminal_type.FormatBuild(Row.Item("te_client_id"), Row.Item("te_build_id"))
      'only fill in red if value
      If Me.dg_list.Cell(RowIndex, GRID_COLUMN_BUILD_STR).Value <> "" Then
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_BUILD_STR).ForeColor = fore_color
        Me.dg_list.Cell(RowIndex, GRID_COLUMN_BUILD_STR).BackColor = back_color
      End If
    Else
      Me.dg_list.Cell(RowIndex, GRID_COLUMN_BUILD_STR).Value = ""
    End If

    Me.dg_list.Cell(RowIndex, GRID_COLUMN_UPDATE_STATUS).Value = Row.Item("update_status")

    Return 1
  End Function ' UpdateRow

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Function GetSqlQuery() As String
    Dim sql_str As String
    Dim sql_filter_type As String
    Dim current_build_win As Integer
    Dim current_build_sas As Integer
    Dim current_build_site_jackpot As Integer
    Dim current_build_imb As Integer
    Dim current_build_istats As Integer
    Dim current_build_promo_box As Integer

    current_build_win = uc_type_list.GetCurrentBuild(WSI.Common.TerminalTypes.WIN)
    current_build_sas = uc_type_list.GetCurrentBuild(WSI.Common.TerminalTypes.SAS_HOST)
    current_build_site_jackpot = uc_type_list.GetCurrentBuild(WSI.Common.TerminalTypes.SITE_JACKPOT)
    current_build_imb = uc_type_list.GetCurrentBuild(WSI.Common.TerminalTypes.MOBILE_BANK_IMB)
    current_build_istats = uc_type_list.GetCurrentBuild(WSI.Common.TerminalTypes.ISTATS)
    current_build_promo_box = uc_type_list.GetCurrentBuild(WSI.Common.TerminalTypes.PROMOBOX)

    sql_filter_type = m_filter_term_type_id_list

    If m_filter_session_free And m_filter_session_oc Then
      sql_filter_type = sql_filter_type & ""
    ElseIf m_filter_session_free Then
      sql_filter_type = sql_filter_type & " AND ps_play_session_id is NULL "
    ElseIf m_filter_session_oc Then
      sql_filter_type = sql_filter_type & " AND ps_play_session_id is NOT NULL "
    End If

    ' Filter Providers - Terminals
    sql_filter_type = sql_filter_type & " AND TE_TERMINAL_ID IN " & m_filter_term_id_list

    If m_filter_wcp_conn And m_filter_wcp_disconn Then
      sql_filter_type = sql_filter_type & ""
    ElseIf m_filter_wcp_conn Then
      sql_filter_type = sql_filter_type & " AND (datediff(second,ws_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") "
    ElseIf m_filter_wcp_disconn Then
      sql_filter_type = sql_filter_type & " AND ( ws_last_rcvd_msg IS NULL OR (datediff(second,ws_last_rcvd_msg,getdate()) > " & ACTIVITY_TIMEOUT & ")) "
    End If

    If m_filter_wc2_conn And m_filter_wc2_disconn Then
      sql_filter_type = sql_filter_type & ""
    ElseIf m_filter_wc2_conn Then
      sql_filter_type = sql_filter_type & " AND (datediff(second,w2s_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") "
    ElseIf m_filter_wc2_disconn Then
      sql_filter_type = sql_filter_type & " AND ( w2s_last_rcvd_msg IS NULL OR (datediff(second,w2s_last_rcvd_msg,getdate()) > " & ACTIVITY_TIMEOUT & ")) "
    End If

    If m_filter_active_only Then
      sql_filter_type = sql_filter_type & " AND TE_STATUS = " & WSI.Common.TerminalStatus.ACTIVE & " "
    End If

    sql_str = " Select te_name " & _
                   ", te_terminal_type " & _
                   ", te_provider_id " & _
                   ", session_state = CASE " & _
                                    " WHEN (ps_play_session_id is NOT NULL) THEN " & TERM_STATUS_BUSY & " " & _
                                    " WHEN (ps_play_session_id is NULL) THEN " & TERM_STATUS_FREE & " " & _
                                    " ELSE 0 " & _
                                    " END " & _
                   ", wcp_state = CASE " & _
                                " WHEN (datediff(second,ws_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") THEN " & TERM_STATUS_CONNECTED & " " & _
                                " ELSE " & TERM_STATUS_DISCONNECTED & " " & _
                                 " END " & _
                   ", wc2_state = CASE " & _
                                " WHEN (datediff(second,w2s_last_rcvd_msg,getdate()) <= " & ACTIVITY_TIMEOUT & ") THEN " & TERM_STATUS_CONNECTED & " " & _
                                " ELSE " & TERM_STATUS_DISCONNECTED & " " & _
                                 " END " & _
                   ", te_client_id " & _
                   ", te_build_id " & _
                   ", update_status = CASE WHEN ((te_build_id = " & current_build_win & " AND te_terminal_type = " & WSI.Common.TerminalTypes.WIN & ")" & _
                                            " OR (te_build_id = " & current_build_sas & " AND te_terminal_type = " & WSI.Common.TerminalTypes.SAS_HOST & ")" & _
                                            " OR (te_build_id = " & current_build_sas & " AND te_terminal_type = " & WSI.Common.TerminalTypes.MOBILE_BANK & ")" & _
                                            " OR (te_build_id = " & current_build_imb & " AND te_terminal_type = " & WSI.Common.TerminalTypes.MOBILE_BANK_IMB & ")" & _
                                            " OR (te_build_id = " & current_build_istats & " AND te_terminal_type = " & WSI.Common.TerminalTypes.ISTATS & ")" & _
                                            " OR (te_build_id = " & current_build_site_jackpot & " AND te_terminal_type = " & WSI.Common.TerminalTypes.SITE_JACKPOT & ")" & _
                                            " OR (te_build_id = " & current_build_promo_box & " AND te_terminal_type = " & WSI.Common.TerminalTypes.PROMOBOX & ")" & _
                                            ") THEN 1 ELSE 0 END " & _
                   ", te_terminal_id " & _
                   ", ws_server_name " & _
                   ", w2s_server_name " & _
             " FROM terminals LEFT OUTER JOIN (SELECT PS_TERMINAL_ID, MAX(PS_PLAY_SESSION_ID) AS PS_PLAY_SESSION_ID " & _
                                              "  FROM PLAY_SESSIONS " & _
                                              " WHERE PS_STATUS = " & PLAY_SESSION_STATUS_OPEN & " GROUP BY PS_TERMINAL_ID) LAST_PLAY_SESSIONS ON TE_TERMINAL_ID = PS_TERMINAL_ID " & _
                                              " " & _
             "                LEFT OUTER JOIN (SELECT WS_TERMINAL_ID, WS_SERVER_NAME, MAX(WS_LAST_RCVD_MSG) AS WS_LAST_RCVD_MSG " & _
                                              "  FROM WCP_SESSIONS " & _
                                              " WHERE WS_STATUS = " & PLAY_SESSION_STATUS_OPEN & " GROUP BY WS_TERMINAL_ID, WS_SERVER_NAME) LAST_WCP_SESSIONS ON TE_TERMINAL_ID = WS_TERMINAL_ID " & _
                                              " " & _
             "                LEFT OUTER JOIN (SELECT W2S_TERMINAL_ID, W2S_SERVER_NAME, MAX(W2S_LAST_RCVD_MSG) AS W2S_LAST_RCVD_MSG " & _
                                              "  FROM WC2_SESSIONS " & _
                                              " WHERE W2S_STATUS = " & PLAY_SESSION_STATUS_OPEN & " GROUP BY W2S_TERMINAL_ID, W2S_SERVER_NAME) LAST_WC2_SESSIONS ON TE_TERMINAL_ID = W2S_TERMINAL_ID " & _
                                              " " & _
             " WHERE te_client_id IS NOT NULL " & _
               " AND te_build_id IS NOT NULL " & _
               sql_filter_type & _
           " ORDER BY update_status, te_name, wcp_state,wc2_state, session_state "

    Return sql_str
  End Function ' GetSqlQuery

  ' PURPOSE: Format appearance of the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()

    Dim fore_color As Color
    Dim back_color As Color
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.dg_list
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .Sortable = True

      GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_TO_DATE, fore_color, back_color)
      .Counter(COUNTER_TERMINAL_UP_TO_DATE).Visible = True
      .Counter(COUNTER_TERMINAL_UP_TO_DATE).BackColor = back_color
      .Counter(COUNTER_TERMINAL_UP_TO_DATE).ForeColor = fore_color

      GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_OUTDATED, fore_color, back_color)
      .Counter(COUNTER_TERMINAL_OUTDATED).Visible = True
      .Counter(COUNTER_TERMINAL_OUTDATED).BackColor = back_color
      .Counter(COUNTER_TERMINAL_OUTDATED).ForeColor = fore_color

      ' Sentinel
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)    'terminal
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200
      .Column(GRID_COLUMN_SENTINEL).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SENTINEL).IsColumnPrintable = False


      ' Terminal ID
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0


      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
        'Terminal report's header or "name" to avoid terminal/terminal (2111 -> name)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _
            IIf(_terminal_columns(_idx).Header0 <> GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106), _
            _terminal_columns(_idx).Header0, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2111))
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' Build String
      .Column(GRID_COLUMN_BUILD_STR).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_BUILD_STR).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(GRID_COLUMN_BUILD_STR).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(217)
      .Column(GRID_COLUMN_BUILD_STR).Width = 900     '1200 (decrease to fit the grid)
      .Column(GRID_COLUMN_BUILD_STR).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal Type
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(GRID_COLUMN_TERMINAL_TYPE).Header(1).Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(322)
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = 1700
      .Column(GRID_COLUMN_TERMINAL_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status Session
      .Column(GRID_COLUMN_STATUS_SESSION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS_SESSION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(GRID_COLUMN_STATUS_SESSION).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(354)
      .Column(GRID_COLUMN_STATUS_SESSION).Width = 1200
      .Column(GRID_COLUMN_STATUS_SESSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status WCP
      .Column(GRID_COLUMN_STATUS_WCP).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS_WCP).Header(0).Text = "WCP"
      .Column(GRID_COLUMN_STATUS_WCP).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(562)
      .Column(GRID_COLUMN_STATUS_WCP).Width = 1500
      .Column(GRID_COLUMN_STATUS_WCP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Server WCP
      .Column(GRID_COLUMN_SERVER_WCP).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SERVER_WCP).Header(0).Text = "WCP"
      .Column(GRID_COLUMN_SERVER_WCP).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(413)
      .Column(GRID_COLUMN_SERVER_WCP).Width = 1500
      .Column(GRID_COLUMN_SERVER_WCP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      ' Status WC2
      .Column(GRID_COLUMN_STATUS_WC2).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_STATUS_WC2).Header(0).Text = "WC2"
      .Column(GRID_COLUMN_STATUS_WC2).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(562)
      .Column(GRID_COLUMN_STATUS_WC2).Width = 1500
      .Column(GRID_COLUMN_STATUS_WC2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Server WC2
      .Column(GRID_COLUMN_SERVER_WC2).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SERVER_WC2).Header(0).Text = "WC2"
      .Column(GRID_COLUMN_SERVER_WC2).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(413)
      .Column(GRID_COLUMN_SERVER_WC2).Width = 1500
      .Column(GRID_COLUMN_SERVER_WC2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status Code
      .Column(GRID_COLUMN_UPDATE_STATUS).Header.Text = ""
      .Column(GRID_COLUMN_UPDATE_STATUS).Width = 0

      ' Session Code
      .Column(GRID_COLUMN_STATUS_SESSION_CODE).Header.Text = ""
      .Column(GRID_COLUMN_STATUS_SESSION_CODE).Width = 0

      ' Status Code
      .Column(GRID_COLUMN_STATUS_WCP_CODE).Header.Text = ""
      .Column(GRID_COLUMN_STATUS_WCP_CODE).Width = 0

      ' Status Code
      .Column(GRID_COLUMN_STATUS_WC2_CODE).Header.Text = ""
      .Column(GRID_COLUMN_STATUS_WC2_CODE).Width = 0

    End With
  End Sub 'GUI_StyleSheet

  ' PURPOSE: Get color by SERVICE status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_SERVICE_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '
  Private Sub GetUpdateStatusColor(ByVal Status As ENUM_UPDATE_STATUS, _
                                    ByRef ForeColor As Color, _
                                    ByRef BackColor As Color)
    Select Case Status

      Case ENUM_UPDATE_STATUS.STATUS_TO_DATE
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      Case ENUM_UPDATE_STATUS.STATUS_OUTDATED
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_02)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_00)

    End Select
  End Sub 'GetUpdateStatusColor

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_ExcelResultList()

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      If Not IsNothing(m_excel_data) Then
        m_excel_data = Nothing

      End If

      m_excel_data = New GUI_Reports.CLASS_EXCEL_DATA()

      If m_excel_data.IsNull() Then
        Return
      End If

      If Not m_excel_data.Cancelled Then
        Call GUI_ReportParams(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportFilter(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportData(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_ReportFooter(m_excel_data)
      End If
      If Not m_excel_data.Cancelled Then
        Call GUI_PrintReport(m_excel_data)
      End If

    Catch ex As Exception

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(146), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "GUI_ExcelResultList()", _
                            "", _
                            "", _
                            "", _
                           "Error in Excel data export.")
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "Exception", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    Finally
      If m_excel_data Is Nothing Then
      Else
        m_excel_data.Dispose()
        m_excel_data = Nothing
      End If
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub

  Private Sub ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_EXCEL
        Call GUI_ExcelResultList()
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_refresh_click = 1
        Call GUI_ReportUpdateFilters()
        Call Refresh_Grid()
      Case Else
        '
    End Select

    If GUI_HasToBeAudited(ButtonId) Then
      _print_data = New GUI_Reports.CLASS_PRINT_DATA
      Call GUI_ReportFilter(_print_data)
      Call MyBase.AuditFormClick(ButtonId, _print_data)
    End If
  End Sub

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SENTINEL = 0
    GRID_INIT_TERMINAL_DATA = 1
    GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_TERMINAL_TYPE = TERMINAL_DATA_COLUMNS + 1
    GRID_COLUMN_BUILD_STR = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_STATUS_SESSION = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_STATUS_WCP = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_SERVER_WCP = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_STATUS_WC2 = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_SERVER_WC2 = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_UPDATE_STATUS = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_STATUS_SESSION_CODE = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_STATUS_WCP_CODE = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_STATUS_WC2_CODE = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_TERMINAL_ID = TERMINAL_DATA_COLUMNS + 12

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 13
  End Sub

  Private Sub ResetFilters()
    ' Providers - Terminals
    Dim _terminal_types() As Integer = Nothing

    _terminal_types = WSI.Common.Misc.AllTerminalTypesExcept3GS()
    Call Me.uc_pr_list.Init(_terminal_types)
    Call Me.uc_pr_list.SetDefaultValues()

    Call Me.uc_type_list.Init(_terminal_types)

    Me.cb_wcp_connected.Checked = True
    Me.cb_wcp_disconnected.Checked = True

    Me.cb_wc2_connected.Checked = True
    Me.cb_wc2_disconnected.Checked = True

    Me.cb_session_oc.Checked = True
    Me.cb_session_free.Checked = True

    Me.cb_active_only.Checked = True
    Me.chk_terminal_location.Checked = False

  End Sub

#End Region ' Private Functions

#Region "Protected Functions"

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    m_print_datetime = GUI_GetPrintDateTime()
    Call frm_base_sel.GUI_ReportParamsX(Me.Grid, ExcelData, FormTitle, m_print_datetime)
    ExcelData.SetColumnFormat(EXCEL_BUILD_STR, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
  End Sub

  ' PURPOSE: Get reports from print information and update report filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Sub GUI_ReportFilter(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)

    ' Get ReportFilter from PrintData
    If Not IsNothing(m_print_data) Then
      m_print_data = Nothing
    End If

    m_print_data = New GUI_Reports.CLASS_PRINT_DATA

    Call GUI_ReportFilter(m_print_data)

    'Insert filter data
    ExcelData.UpdateReportFilters(m_print_data.Filter.ItemArray)

  End Sub

  ' PURPOSE: Overridable routine to initialize report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(354), m_session)

    PrintData.SetFilter("WCP", m_wcp)

    PrintData.SetFilter("WC2", m_wc2)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)

    PrintData.SetFilter(GLB_NLS_GUI_SW_DOWNLOAD.GetString(233), m_term_type)

    PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(496), m_active_only)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), m_location_check)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Get report data from grid and inserts it intro Excel document
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Sub GUI_ReportData(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)

    Call frm_base_sel.GUI_ReportDataX(Me.Grid, ExcelData)

  End Sub ' GUI_ReportData

  Protected Sub GUI_ReportFooter(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
  End Sub

  ' PURPOSE: Show Excel document
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Sub GUI_PrintReport(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA)
    Call ExcelData.Preview()
  End Sub ' GUI_PrintReport

  ' PURPOSE: Update report filters.
  '
  '  PARAMS:
  '     - INPUT:
  '           - MyDataGrid As DataGrid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Sub GUI_ReportUpdateFilters()

    m_session = String.Empty
    m_wcp = String.Empty
    m_wc2 = String.Empty
    m_terminal = String.Empty
    m_term_type = String.Empty
    m_active_only = String.Empty
    m_location_check = String.Empty

    m_session = Me.cb_session_oc.Text & ": "
    If Me.cb_session_oc.Checked Then
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_session = m_session & ", " & Me.cb_session_free.Text & ": "

    If Me.cb_session_free.Checked Then
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_session = m_session & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_wcp = Me.cb_wcp_connected.Text & ": "
    If Me.cb_wcp_connected.Checked Then
      m_wcp = m_wcp & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_wcp = m_wcp & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_wcp = m_wcp & ", " & Me.cb_wcp_disconnected.Text & ": "

    If Me.cb_wcp_disconnected.Checked Then
      m_wcp = m_wcp & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_wcp = m_wcp & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_wc2 = Me.cb_wc2_connected.Text & ": "
    If Me.cb_wc2_connected.Checked Then
      m_wc2 = m_wc2 & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_wc2 = m_wc2 & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_wc2 = m_wc2 & ", " & Me.cb_wc2_disconnected.Text & ": "

    If Me.cb_wc2_disconnected.Checked Then
      m_wc2 = m_wc2 & GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_wc2 = m_wc2 & GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If Me.cb_active_only.Checked Then
      m_active_only = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_active_only = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    If Me.uc_type_list.opt_all_types.Checked Then
      m_term_type = Me.uc_type_list.opt_all_types.Text
    Else
      m_term_type = Me.uc_type_list.opt_several_types.Text
    End If

    If Me.chk_terminal_location.Checked Then
      m_location_check = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_location_check = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

  End Sub

#End Region ' Protected Functions

#Region "Events"

  ' PURPOSE: Execute proper actions when user presses the 'Excel' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub btn_excel_Click() Handles btn_excel.ClickEvent
    Call ButtonClick(ENUM_BUTTON.BUTTON_EXCEL)
  End Sub

  ' PURPOSE: Execute proper actions when user presses the 'Exit' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub btn_cancel_Click() Handles btn_exit.ClickEvent
    Me.Close()
  End Sub

  ' PURPOSE: Execute proper actions when programmed timer elapses
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Me.RefreshGrid()
    Me.tf_last_update.Value = WSI.Common.Format.CustomFormatTime(WSI.Common.WGDB.Now, True)
  End Sub

  Private Sub btn_refresh_Click() Handles btn_refresh.ClickEvent
    Call ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
  End Sub

  Private Sub Refresh_Grid()
    Me.Timer1.Stop()
    m_filter_term_selected_one = Me.uc_pr_list.OneTerminalChecked
    m_filter_term_selected_several = Me.uc_pr_list.SeveralTerminalsChecked
    m_filter_term_id_list = Me.uc_pr_list.GetProviderIdListSelected()

    m_filter_session_free = Me.cb_session_free.Checked
    m_filter_session_oc = Me.cb_session_oc.Checked

    m_filter_wcp_conn = Me.cb_wcp_connected.Checked
    m_filter_wcp_disconn = Me.cb_wcp_disconnected.Checked

    m_filter_wc2_conn = Me.cb_wc2_connected.Checked
    m_filter_wc2_disconn = Me.cb_wc2_disconnected.Checked

    m_filter_active_only = Me.cb_active_only.Checked

    m_filter_term_type_id_list = Me.uc_type_list.GetTerminalTypesSelected()

    Me.dg_list.Clear()
    Me.RefreshGrid()
    Me.tf_last_update.Value = WSI.Common.Format.CustomFormatTime(WSI.Common.WGDB.Now, True)
    Me.Timer1.Start()
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Events

#Region "Public Functions"

  ' PURPOSE: Entry point for the screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Call Me.Display(False)
  End Sub ' ShowForEdit

  ' PURPOSE: Get the actual datetime to use in the print operations
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Shared Function GUI_GetPrintDateTime() As Date

    Return GUI_GetDateTime()
  End Function ' GUI_GetPrintDateTime

#End Region ' Public Functions

  Private Sub btn_filter_reset_ClickEvent() Handles btn_filter_reset.ClickEvent
    Call Me.ResetFilters()
  End Sub
End Class ' frm_terminal_sw_version
