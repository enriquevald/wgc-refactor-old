﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_code_junket = New GUI_Controls.uc_entry_field()
    Me.ef_name_junket = New GUI_Controls.uc_entry_field()
    Me.gb_junket = New System.Windows.Forms.GroupBox()
    Me.gb_junket_representative = New System.Windows.Forms.GroupBox()
    Me.ef_code_representative = New GUI_Controls.uc_entry_field()
    Me.ef_name_representative = New GUI_Controls.uc_entry_field()
    Me.gb_init_date = New System.Windows.Forms.GroupBox()
    Me.rb_date_ini = New System.Windows.Forms.RadioButton()
    Me.rb_active = New System.Windows.Forms.RadioButton()
    Me.dtp_init_to = New GUI_Controls.uc_date_picker()
    Me.dtp_init_from = New GUI_Controls.uc_date_picker()
    Me.lbl_to_hour = New System.Windows.Forms.Label()
    Me.lbl_from_hour = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_junket.SuspendLayout()
    Me.gb_junket_representative.SuspendLayout()
    Me.gb_init_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_init_date)
    Me.panel_filter.Controls.Add(Me.gb_junket)
    Me.panel_filter.Controls.Add(Me.gb_junket_representative)
    Me.panel_filter.Size = New System.Drawing.Size(1097, 148)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_junket_representative, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_junket, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_init_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 152)
    Me.panel_data.Size = New System.Drawing.Size(1097, 434)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1091, 31)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1091, 4)
    '
    'ef_code_junket
    '
    Me.ef_code_junket.DoubleValue = 0.0R
    Me.ef_code_junket.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_code_junket.IntegerValue = 0
    Me.ef_code_junket.IsReadOnly = False
    Me.ef_code_junket.Location = New System.Drawing.Point(7, 20)
    Me.ef_code_junket.Name = "ef_code_junket"
    Me.ef_code_junket.PlaceHolder = Nothing
    Me.ef_code_junket.Size = New System.Drawing.Size(281, 25)
    Me.ef_code_junket.SufixText = "Sufix Text"
    Me.ef_code_junket.SufixTextVisible = True
    Me.ef_code_junket.TabIndex = 0
    Me.ef_code_junket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_code_junket.TextValue = ""
    Me.ef_code_junket.TextWidth = 60
    Me.ef_code_junket.Value = ""
    Me.ef_code_junket.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name_junket
    '
    Me.ef_name_junket.DoubleValue = 0.0R
    Me.ef_name_junket.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_name_junket.IntegerValue = 0
    Me.ef_name_junket.IsReadOnly = False
    Me.ef_name_junket.Location = New System.Drawing.Point(7, 51)
    Me.ef_name_junket.Name = "ef_name_junket"
    Me.ef_name_junket.PlaceHolder = Nothing
    Me.ef_name_junket.Size = New System.Drawing.Size(281, 25)
    Me.ef_name_junket.SufixText = "Sufix Text"
    Me.ef_name_junket.SufixTextVisible = True
    Me.ef_name_junket.TabIndex = 1
    Me.ef_name_junket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name_junket.TextValue = ""
    Me.ef_name_junket.TextWidth = 60
    Me.ef_name_junket.Value = ""
    Me.ef_name_junket.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_junket
    '
    Me.gb_junket.Controls.Add(Me.ef_code_junket)
    Me.gb_junket.Controls.Add(Me.ef_name_junket)
    Me.gb_junket.Location = New System.Drawing.Point(252, 7)
    Me.gb_junket.Name = "gb_junket"
    Me.gb_junket.Size = New System.Drawing.Size(294, 89)
    Me.gb_junket.TabIndex = 1
    Me.gb_junket.TabStop = False
    Me.gb_junket.Text = "xJunket"
    '
    'gb_junket_representative
    '
    Me.gb_junket_representative.Controls.Add(Me.ef_code_representative)
    Me.gb_junket_representative.Controls.Add(Me.ef_name_representative)
    Me.gb_junket_representative.Location = New System.Drawing.Point(552, 7)
    Me.gb_junket_representative.Name = "gb_junket_representative"
    Me.gb_junket_representative.Size = New System.Drawing.Size(294, 89)
    Me.gb_junket_representative.TabIndex = 2
    Me.gb_junket_representative.TabStop = False
    Me.gb_junket_representative.Text = "xRepresentative"
    '
    'ef_code_representative
    '
    Me.ef_code_representative.DoubleValue = 0.0R
    Me.ef_code_representative.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_code_representative.IntegerValue = 0
    Me.ef_code_representative.IsReadOnly = False
    Me.ef_code_representative.Location = New System.Drawing.Point(7, 20)
    Me.ef_code_representative.Name = "ef_code_representative"
    Me.ef_code_representative.PlaceHolder = Nothing
    Me.ef_code_representative.Size = New System.Drawing.Size(281, 25)
    Me.ef_code_representative.SufixText = "Sufix Text"
    Me.ef_code_representative.SufixTextVisible = True
    Me.ef_code_representative.TabIndex = 2
    Me.ef_code_representative.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_code_representative.TextValue = ""
    Me.ef_code_representative.TextWidth = 60
    Me.ef_code_representative.Value = ""
    Me.ef_code_representative.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name_representative
    '
    Me.ef_name_representative.DoubleValue = 0.0R
    Me.ef_name_representative.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_name_representative.IntegerValue = 0
    Me.ef_name_representative.IsReadOnly = False
    Me.ef_name_representative.Location = New System.Drawing.Point(7, 51)
    Me.ef_name_representative.Name = "ef_name_representative"
    Me.ef_name_representative.PlaceHolder = Nothing
    Me.ef_name_representative.Size = New System.Drawing.Size(281, 25)
    Me.ef_name_representative.SufixText = "Sufix Text"
    Me.ef_name_representative.SufixTextVisible = True
    Me.ef_name_representative.TabIndex = 3
    Me.ef_name_representative.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name_representative.TextValue = ""
    Me.ef_name_representative.TextWidth = 60
    Me.ef_name_representative.Value = ""
    Me.ef_name_representative.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_init_date
    '
    Me.gb_init_date.Controls.Add(Me.lbl_to_hour)
    Me.gb_init_date.Controls.Add(Me.lbl_from_hour)
    Me.gb_init_date.Controls.Add(Me.rb_date_ini)
    Me.gb_init_date.Controls.Add(Me.rb_active)
    Me.gb_init_date.Controls.Add(Me.dtp_init_to)
    Me.gb_init_date.Controls.Add(Me.dtp_init_from)
    Me.gb_init_date.Location = New System.Drawing.Point(6, 7)
    Me.gb_init_date.Name = "gb_init_date"
    Me.gb_init_date.Size = New System.Drawing.Size(240, 131)
    Me.gb_init_date.TabIndex = 0
    Me.gb_init_date.TabStop = False
    Me.gb_init_date.Text = "xDate"
    '
    'rb_date_ini
    '
    Me.rb_date_ini.AutoSize = True
    Me.rb_date_ini.Location = New System.Drawing.Point(6, 43)
    Me.rb_date_ini.Name = "rb_date_ini"
    Me.rb_date_ini.Size = New System.Drawing.Size(74, 17)
    Me.rb_date_ini.TabIndex = 1
    Me.rb_date_ini.Text = "xDateIni"
    Me.rb_date_ini.UseVisualStyleBackColor = True
    '
    'rb_active
    '
    Me.rb_active.AutoSize = True
    Me.rb_active.Checked = True
    Me.rb_active.Location = New System.Drawing.Point(6, 20)
    Me.rb_active.Name = "rb_active"
    Me.rb_active.Size = New System.Drawing.Size(67, 17)
    Me.rb_active.TabIndex = 0
    Me.rb_active.TabStop = True
    Me.rb_active.Text = "xActive"
    Me.rb_active.UseVisualStyleBackColor = True
    '
    'dtp_init_to
    '
    Me.dtp_init_to.Checked = True
    Me.dtp_init_to.IsReadOnly = False
    Me.dtp_init_to.Location = New System.Drawing.Point(6, 96)
    Me.dtp_init_to.Name = "dtp_init_to"
    Me.dtp_init_to.ShowCheckBox = True
    Me.dtp_init_to.ShowUpDown = False
    Me.dtp_init_to.Size = New System.Drawing.Size(162, 25)
    Me.dtp_init_to.SufixText = "Sufix Text"
    Me.dtp_init_to.SufixTextVisible = True
    Me.dtp_init_to.TabIndex = 3
    Me.dtp_init_to.TextWidth = 50
    Me.dtp_init_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_init_from
    '
    Me.dtp_init_from.Checked = True
    Me.dtp_init_from.IsReadOnly = False
    Me.dtp_init_from.Location = New System.Drawing.Point(6, 65)
    Me.dtp_init_from.Name = "dtp_init_from"
    Me.dtp_init_from.ShowCheckBox = False
    Me.dtp_init_from.ShowUpDown = False
    Me.dtp_init_from.Size = New System.Drawing.Size(162, 25)
    Me.dtp_init_from.SufixText = "Sufix Text"
    Me.dtp_init_from.SufixTextVisible = True
    Me.dtp_init_from.TabIndex = 2
    Me.dtp_init_from.TextWidth = 50
    Me.dtp_init_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'lbl_to_hour
    '
    Me.lbl_to_hour.AutoSize = True
    Me.lbl_to_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_to_hour.Location = New System.Drawing.Point(180, 99)
    Me.lbl_to_hour.Name = "lbl_to_hour"
    Me.lbl_to_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_to_hour.TabIndex = 15
    Me.lbl_to_hour.Text = "00:00"
    '
    'lbl_from_hour
    '
    Me.lbl_from_hour.AutoSize = True
    Me.lbl_from_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_from_hour.Location = New System.Drawing.Point(180, 71)
    Me.lbl_from_hour.Name = "lbl_from_hour"
    Me.lbl_from_hour.Size = New System.Drawing.Size(40, 13)
    Me.lbl_from_hour.TabIndex = 14
    Me.lbl_from_hour.Text = "00:00"
    '
    'frm_junkets_sel
    '
    Me.ClientSize = New System.Drawing.Size(1105, 590)
    Me.Name = "frm_junkets_sel"
    Me.Text = "frm_junkets_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_junket.ResumeLayout(False)
    Me.gb_junket_representative.ResumeLayout(False)
    Me.gb_init_date.ResumeLayout(False)
    Me.gb_init_date.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_name_junket As GUI_Controls.uc_entry_field
  Friend WithEvents ef_code_junket As GUI_Controls.uc_entry_field
  Friend WithEvents gb_junket_representative As System.Windows.Forms.GroupBox
  Friend WithEvents gb_junket As System.Windows.Forms.GroupBox
  Friend WithEvents ef_code_representative As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name_representative As GUI_Controls.uc_entry_field
  Friend WithEvents gb_init_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_init_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_init_from As GUI_Controls.uc_date_picker
  Friend WithEvents rb_date_ini As System.Windows.Forms.RadioButton
  Friend WithEvents rb_active As System.Windows.Forms.RadioButton
  Protected WithEvents lbl_to_hour As System.Windows.Forms.Label
  Protected WithEvents lbl_from_hour As System.Windows.Forms.Label

End Class
