<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_promotions_delivered_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_promotions_delivered_report))
    Me.gb_promo_credit_type = New System.Windows.Forms.GroupBox()
    Me.chk_credit_type_points = New System.Windows.Forms.CheckBox()
    Me.chk_credit_type_redeem = New System.Windows.Forms.CheckBox()
    Me.chk_credit_type_non_redeem = New System.Windows.Forms.CheckBox()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel()
    Me.ef_promo_name = New GUI_Controls.uc_entry_field()
    Me.gb_order = New System.Windows.Forms.GroupBox()
    Me.opt_order_account_day = New System.Windows.Forms.RadioButton()
    Me.opt_order_prom_session_user = New System.Windows.Forms.RadioButton()
    Me.opt_order_session_user_prom = New System.Windows.Forms.RadioButton()
    Me.chk_view_data_account = New System.Windows.Forms.CheckBox()
    Me.gb_promo_type = New System.Windows.Forms.GroupBox()
    Me.opt_promo_preassigned = New System.Windows.Forms.RadioButton()
    Me.opt_promo_all = New System.Windows.Forms.RadioButton()
    Me.opt_promo_system_period = New System.Windows.Forms.RadioButton()
    Me.opt_promo_per_points = New System.Windows.Forms.RadioButton()
    Me.opt_promo_system_auto = New System.Windows.Forms.RadioButton()
    Me.opt_promo_manual = New System.Windows.Forms.RadioButton()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.chk_show_all = New System.Windows.Forms.CheckBox()
    Me.opt_one_user = New System.Windows.Forms.RadioButton()
    Me.gb_user = New System.Windows.Forms.GroupBox()
    Me.opt_all_users = New System.Windows.Forms.RadioButton()
    Me.cmb_user = New GUI_Controls.uc_combo()
    Me.chk_details = New System.Windows.Forms.CheckBox()
    Me.gb_options = New System.Windows.Forms.GroupBox()
    Me.gb_caption = New System.Windows.Forms.GroupBox()
    Me.lbl_caption_subtotal2 = New System.Windows.Forms.Label()
    Me.lbl_caption_subtotal1 = New System.Windows.Forms.Label()
    Me.lbl_color_caption_subtotal1 = New System.Windows.Forms.Label()
    Me.lbl_color_caption_subtotal2 = New System.Windows.Forms.Label()
    Me.cmb_categories = New GUI_Controls.uc_combo()
    Me.lbl_msg_no_includes_auto = New System.Windows.Forms.Label()
    Me.gb_status = New System.Windows.Forms.GroupBox()
    Me.opt_status_pending_draw = New System.Windows.Forms.RadioButton()
    Me.opt_status_bought = New System.Windows.Forms.RadioButton()
    Me.opt_status_all = New System.Windows.Forms.RadioButton()
    Me.opt_status_pending_player = New System.Windows.Forms.RadioButton()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_promo_credit_type.SuspendLayout()
    Me.gb_order.SuspendLayout()
    Me.gb_promo_type.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.gb_caption.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.lbl_msg_no_includes_auto)
    Me.panel_filter.Controls.Add(Me.gb_promo_credit_type)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Controls.Add(Me.cmb_categories)
    Me.panel_filter.Controls.Add(Me.gb_caption)
    Me.panel_filter.Controls.Add(Me.ef_promo_name)
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.gb_promo_type)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.gb_order)
    Me.panel_filter.Size = New System.Drawing.Size(1439, 217)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_order, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_promo_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_promo_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_caption, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_categories, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_promo_credit_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_msg_no_includes_auto, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 221)
    Me.panel_data.Size = New System.Drawing.Size(1439, 406)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1433, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1433, 4)
    '
    'gb_promo_credit_type
    '
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_points)
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_redeem)
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_non_redeem)
    Me.gb_promo_credit_type.Location = New System.Drawing.Point(754, 3)
    Me.gb_promo_credit_type.Name = "gb_promo_credit_type"
    Me.gb_promo_credit_type.Size = New System.Drawing.Size(143, 102)
    Me.gb_promo_credit_type.TabIndex = 7
    Me.gb_promo_credit_type.TabStop = False
    Me.gb_promo_credit_type.Text = "xPromoCreditType"
    '
    'chk_credit_type_points
    '
    Me.chk_credit_type_points.AutoSize = True
    Me.chk_credit_type_points.Location = New System.Drawing.Point(19, 72)
    Me.chk_credit_type_points.Name = "chk_credit_type_points"
    Me.chk_credit_type_points.Size = New System.Drawing.Size(67, 17)
    Me.chk_credit_type_points.TabIndex = 2
    Me.chk_credit_type_points.Text = "xPoints"
    Me.chk_credit_type_points.UseVisualStyleBackColor = True
    '
    'chk_credit_type_redeem
    '
    Me.chk_credit_type_redeem.AutoSize = True
    Me.chk_credit_type_redeem.Location = New System.Drawing.Point(19, 47)
    Me.chk_credit_type_redeem.Name = "chk_credit_type_redeem"
    Me.chk_credit_type_redeem.Size = New System.Drawing.Size(111, 17)
    Me.chk_credit_type_redeem.TabIndex = 1
    Me.chk_credit_type_redeem.Text = "xRedeemeable"
    Me.chk_credit_type_redeem.UseVisualStyleBackColor = True
    '
    'chk_credit_type_non_redeem
    '
    Me.chk_credit_type_non_redeem.AutoSize = True
    Me.chk_credit_type_non_redeem.Location = New System.Drawing.Point(19, 21)
    Me.chk_credit_type_non_redeem.Name = "chk_credit_type_non_redeem"
    Me.chk_credit_type_non_redeem.Size = New System.Drawing.Size(106, 17)
    Me.chk_credit_type_non_redeem.TabIndex = 0
    Me.chk_credit_type_non_redeem.Text = "xNon Redeem"
    Me.chk_credit_type_non_redeem.UseVisualStyleBackColor = True
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(437, 0)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 5
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0.0R
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(9, 189)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.PlaceHolder = Nothing
    Me.ef_promo_name.Size = New System.Drawing.Size(254, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 1
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 67
    Me.ef_promo_name.Value = ""
    Me.ef_promo_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_order
    '
    Me.gb_order.Controls.Add(Me.opt_order_account_day)
    Me.gb_order.Controls.Add(Me.opt_order_prom_session_user)
    Me.gb_order.Controls.Add(Me.opt_order_session_user_prom)
    Me.gb_order.Location = New System.Drawing.Point(905, 3)
    Me.gb_order.Name = "gb_order"
    Me.gb_order.Size = New System.Drawing.Size(195, 102)
    Me.gb_order.TabIndex = 8
    Me.gb_order.TabStop = False
    Me.gb_order.Text = "xOrder"
    '
    'opt_order_account_day
    '
    Me.opt_order_account_day.AutoSize = True
    Me.opt_order_account_day.Location = New System.Drawing.Point(17, 68)
    Me.opt_order_account_day.Name = "opt_order_account_day"
    Me.opt_order_account_day.Size = New System.Drawing.Size(100, 17)
    Me.opt_order_account_day.TabIndex = 2
    Me.opt_order_account_day.TabStop = True
    Me.opt_order_account_day.Text = "xAccountDay"
    Me.opt_order_account_day.UseVisualStyleBackColor = True
    '
    'opt_order_prom_session_user
    '
    Me.opt_order_prom_session_user.AutoSize = True
    Me.opt_order_prom_session_user.Location = New System.Drawing.Point(17, 22)
    Me.opt_order_prom_session_user.Name = "opt_order_prom_session_user"
    Me.opt_order_prom_session_user.Size = New System.Drawing.Size(120, 17)
    Me.opt_order_prom_session_user.TabIndex = 0
    Me.opt_order_prom_session_user.TabStop = True
    Me.opt_order_prom_session_user.Text = "xPromotionLevel"
    Me.opt_order_prom_session_user.UseVisualStyleBackColor = True
    '
    'opt_order_session_user_prom
    '
    Me.opt_order_session_user_prom.AutoSize = True
    Me.opt_order_session_user_prom.Location = New System.Drawing.Point(17, 45)
    Me.opt_order_session_user_prom.Name = "opt_order_session_user_prom"
    Me.opt_order_session_user_prom.Size = New System.Drawing.Size(107, 17)
    Me.opt_order_session_user_prom.TabIndex = 1
    Me.opt_order_session_user_prom.TabStop = True
    Me.opt_order_session_user_prom.Text = "xLevelAccount"
    Me.opt_order_session_user_prom.UseVisualStyleBackColor = True
    '
    'chk_view_data_account
    '
    Me.chk_view_data_account.AutoSize = True
    Me.chk_view_data_account.Location = New System.Drawing.Point(16, 47)
    Me.chk_view_data_account.Name = "chk_view_data_account"
    Me.chk_view_data_account.Size = New System.Drawing.Size(97, 17)
    Me.chk_view_data_account.TabIndex = 1
    Me.chk_view_data_account.Text = "xHolderData"
    Me.chk_view_data_account.UseVisualStyleBackColor = True
    '
    'gb_promo_type
    '
    Me.gb_promo_type.Controls.Add(Me.opt_promo_preassigned)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_all)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_system_period)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_per_points)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_system_auto)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_manual)
    Me.gb_promo_type.Location = New System.Drawing.Point(6, 3)
    Me.gb_promo_type.Name = "gb_promo_type"
    Me.gb_promo_type.Size = New System.Drawing.Size(172, 161)
    Me.gb_promo_type.TabIndex = 0
    Me.gb_promo_type.TabStop = False
    Me.gb_promo_type.Text = "xTipoPromo"
    '
    'opt_promo_preassigned
    '
    Me.opt_promo_preassigned.AutoSize = True
    Me.opt_promo_preassigned.Location = New System.Drawing.Point(12, 137)
    Me.opt_promo_preassigned.Name = "opt_promo_preassigned"
    Me.opt_promo_preassigned.Size = New System.Drawing.Size(101, 17)
    Me.opt_promo_preassigned.TabIndex = 5
    Me.opt_promo_preassigned.TabStop = True
    Me.opt_promo_preassigned.Text = "xPreassigned"
    Me.opt_promo_preassigned.UseVisualStyleBackColor = True
    '
    'opt_promo_all
    '
    Me.opt_promo_all.AutoSize = True
    Me.opt_promo_all.Location = New System.Drawing.Point(12, 18)
    Me.opt_promo_all.Name = "opt_promo_all"
    Me.opt_promo_all.Size = New System.Drawing.Size(46, 17)
    Me.opt_promo_all.TabIndex = 0
    Me.opt_promo_all.TabStop = True
    Me.opt_promo_all.Text = "xAll"
    Me.opt_promo_all.UseVisualStyleBackColor = True
    '
    'opt_promo_system_period
    '
    Me.opt_promo_system_period.AutoSize = True
    Me.opt_promo_system_period.Location = New System.Drawing.Point(12, 66)
    Me.opt_promo_system_period.Name = "opt_promo_system_period"
    Me.opt_promo_system_period.Size = New System.Drawing.Size(120, 17)
    Me.opt_promo_system_period.TabIndex = 2
    Me.opt_promo_system_period.TabStop = True
    Me.opt_promo_system_period.Text = "xSystemPeriodic"
    Me.opt_promo_system_period.UseVisualStyleBackColor = True
    '
    'opt_promo_per_points
    '
    Me.opt_promo_per_points.AutoSize = True
    Me.opt_promo_per_points.Location = New System.Drawing.Point(12, 114)
    Me.opt_promo_per_points.Name = "opt_promo_per_points"
    Me.opt_promo_per_points.Size = New System.Drawing.Size(89, 17)
    Me.opt_promo_per_points.TabIndex = 4
    Me.opt_promo_per_points.TabStop = True
    Me.opt_promo_per_points.Text = "xPer points"
    Me.opt_promo_per_points.UseVisualStyleBackColor = True
    '
    'opt_promo_system_auto
    '
    Me.opt_promo_system_auto.AutoSize = True
    Me.opt_promo_system_auto.Location = New System.Drawing.Point(12, 90)
    Me.opt_promo_system_auto.Name = "opt_promo_system_auto"
    Me.opt_promo_system_auto.Size = New System.Drawing.Size(101, 17)
    Me.opt_promo_system_auto.TabIndex = 3
    Me.opt_promo_system_auto.TabStop = True
    Me.opt_promo_system_auto.Text = "xSystemAuto"
    Me.opt_promo_system_auto.UseVisualStyleBackColor = True
    '
    'opt_promo_manual
    '
    Me.opt_promo_manual.AutoSize = True
    Me.opt_promo_manual.Location = New System.Drawing.Point(12, 42)
    Me.opt_promo_manual.Name = "opt_promo_manual"
    Me.opt_promo_manual.Size = New System.Drawing.Size(72, 17)
    Me.opt_promo_manual.TabIndex = 1
    Me.opt_promo_manual.TabStop = True
    Me.opt_promo_manual.Text = "xManual"
    Me.opt_promo_manual.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.FromDateText = "#5339"
    Me.uc_dsl.Location = New System.Drawing.Point(184, 3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(250, 81)
    Me.uc_dsl.TabIndex = 2
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.ToDateText = "#5340"
    '
    'chk_show_all
    '
    Me.chk_show_all.AutoSize = True
    Me.chk_show_all.Location = New System.Drawing.Point(96, 49)
    Me.chk_show_all.Name = "chk_show_all"
    Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_all.TabIndex = 3
    Me.chk_show_all.Text = "chk_show_all"
    Me.chk_show_all.UseVisualStyleBackColor = True
    '
    'opt_one_user
    '
    Me.opt_one_user.Location = New System.Drawing.Point(19, 18)
    Me.opt_one_user.Name = "opt_one_user"
    Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_user.TabIndex = 0
    Me.opt_one_user.Text = "xOne"
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.chk_show_all)
    Me.gb_user.Controls.Add(Me.opt_one_user)
    Me.gb_user.Controls.Add(Me.opt_all_users)
    Me.gb_user.Controls.Add(Me.cmb_user)
    Me.gb_user.Location = New System.Drawing.Point(601, 140)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(282, 75)
    Me.gb_user.TabIndex = 6
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'opt_all_users
    '
    Me.opt_all_users.Location = New System.Drawing.Point(19, 45)
    Me.opt_all_users.Name = "opt_all_users"
    Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_users.TabIndex = 1
    Me.opt_all_users.Text = "xAll"
    '
    'cmb_user
    '
    Me.cmb_user.AllowUnlistedValues = False
    Me.cmb_user.AutoCompleteMode = False
    Me.cmb_user.IsReadOnly = False
    Me.cmb_user.Location = New System.Drawing.Point(91, 18)
    Me.cmb_user.Name = "cmb_user"
    Me.cmb_user.SelectedIndex = -1
    Me.cmb_user.Size = New System.Drawing.Size(184, 24)
    Me.cmb_user.SufixText = "Sufix Text"
    Me.cmb_user.SufixTextVisible = True
    Me.cmb_user.TabIndex = 2
    Me.cmb_user.TextCombo = Nothing
    Me.cmb_user.TextVisible = False
    Me.cmb_user.TextWidth = 0
    '
    'chk_details
    '
    Me.chk_details.AutoSize = True
    Me.chk_details.Location = New System.Drawing.Point(16, 20)
    Me.chk_details.Name = "chk_details"
    Me.chk_details.Size = New System.Drawing.Size(72, 17)
    Me.chk_details.TabIndex = 0
    Me.chk_details.Text = "xDetails"
    Me.chk_details.UseVisualStyleBackColor = True
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.chk_details)
    Me.gb_options.Controls.Add(Me.chk_view_data_account)
    Me.gb_options.Location = New System.Drawing.Point(184, 92)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(246, 72)
    Me.gb_options.TabIndex = 3
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'gb_caption
    '
    Me.gb_caption.Controls.Add(Me.lbl_caption_subtotal2)
    Me.gb_caption.Controls.Add(Me.lbl_caption_subtotal1)
    Me.gb_caption.Controls.Add(Me.lbl_color_caption_subtotal1)
    Me.gb_caption.Controls.Add(Me.lbl_color_caption_subtotal2)
    Me.gb_caption.Location = New System.Drawing.Point(889, 140)
    Me.gb_caption.Name = "gb_caption"
    Me.gb_caption.Size = New System.Drawing.Size(211, 75)
    Me.gb_caption.TabIndex = 9
    Me.gb_caption.TabStop = False
    Me.gb_caption.Text = "xCaption"
    '
    'lbl_caption_subtotal2
    '
    Me.lbl_caption_subtotal2.AutoSize = True
    Me.lbl_caption_subtotal2.Location = New System.Drawing.Point(39, 46)
    Me.lbl_caption_subtotal2.Name = "lbl_caption_subtotal2"
    Me.lbl_caption_subtotal2.Size = New System.Drawing.Size(68, 13)
    Me.lbl_caption_subtotal2.TabIndex = 3
    Me.lbl_caption_subtotal2.Text = "xSubtotal2"
    '
    'lbl_caption_subtotal1
    '
    Me.lbl_caption_subtotal1.AutoSize = True
    Me.lbl_caption_subtotal1.Location = New System.Drawing.Point(39, 24)
    Me.lbl_caption_subtotal1.Name = "lbl_caption_subtotal1"
    Me.lbl_caption_subtotal1.Size = New System.Drawing.Size(68, 13)
    Me.lbl_caption_subtotal1.TabIndex = 1
    Me.lbl_caption_subtotal1.Text = "xSubtotal1"
    '
    'lbl_color_caption_subtotal1
    '
    Me.lbl_color_caption_subtotal1.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_caption_subtotal1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_caption_subtotal1.Location = New System.Drawing.Point(17, 23)
    Me.lbl_color_caption_subtotal1.Name = "lbl_color_caption_subtotal1"
    Me.lbl_color_caption_subtotal1.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_caption_subtotal1.TabIndex = 0
    '
    'lbl_color_caption_subtotal2
    '
    Me.lbl_color_caption_subtotal2.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_caption_subtotal2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_caption_subtotal2.Location = New System.Drawing.Point(17, 45)
    Me.lbl_color_caption_subtotal2.Name = "lbl_color_caption_subtotal2"
    Me.lbl_color_caption_subtotal2.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_caption_subtotal2.TabIndex = 2
    '
    'cmb_categories
    '
    Me.cmb_categories.AllowUnlistedValues = False
    Me.cmb_categories.AutoCompleteMode = False
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(315, 189)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(280, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 4
    Me.cmb_categories.TextCombo = Nothing
    Me.cmb_categories.TextWidth = 109
    '
    'lbl_msg_no_includes_auto
    '
    Me.lbl_msg_no_includes_auto.AutoSize = True
    Me.lbl_msg_no_includes_auto.Location = New System.Drawing.Point(15, 169)
    Me.lbl_msg_no_includes_auto.Name = "lbl_msg_no_includes_auto"
    Me.lbl_msg_no_includes_auto.Size = New System.Drawing.Size(200, 13)
    Me.lbl_msg_no_includes_auto.TabIndex = 11
    Me.lbl_msg_no_includes_auto.Text = "No includes automatic promotions"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_status_pending_draw)
    Me.gb_status.Controls.Add(Me.opt_status_bought)
    Me.gb_status.Controls.Add(Me.opt_status_all)
    Me.gb_status.Controls.Add(Me.opt_status_pending_player)
    Me.gb_status.Location = New System.Drawing.Point(1106, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(195, 123)
    Me.gb_status.TabIndex = 9
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'opt_status_pending_draw
    '
    Me.opt_status_pending_draw.AutoSize = True
    Me.opt_status_pending_draw.Location = New System.Drawing.Point(17, 68)
    Me.opt_status_pending_draw.Name = "opt_status_pending_draw"
    Me.opt_status_pending_draw.Size = New System.Drawing.Size(107, 17)
    Me.opt_status_pending_draw.TabIndex = 3
    Me.opt_status_pending_draw.TabStop = True
    Me.opt_status_pending_draw.Text = "xPendingDraw"
    Me.opt_status_pending_draw.UseVisualStyleBackColor = True
    '
    'opt_status_bought
    '
    Me.opt_status_bought.AutoSize = True
    Me.opt_status_bought.Location = New System.Drawing.Point(17, 91)
    Me.opt_status_bought.Name = "opt_status_bought"
    Me.opt_status_bought.Size = New System.Drawing.Size(72, 17)
    Me.opt_status_bought.TabIndex = 2
    Me.opt_status_bought.TabStop = True
    Me.opt_status_bought.Text = "xBought"
    Me.opt_status_bought.UseVisualStyleBackColor = True
    '
    'opt_status_all
    '
    Me.opt_status_all.AutoSize = True
    Me.opt_status_all.Location = New System.Drawing.Point(17, 22)
    Me.opt_status_all.Name = "opt_status_all"
    Me.opt_status_all.Size = New System.Drawing.Size(46, 17)
    Me.opt_status_all.TabIndex = 0
    Me.opt_status_all.TabStop = True
    Me.opt_status_all.Text = "xAll"
    Me.opt_status_all.UseVisualStyleBackColor = True
    '
    'opt_status_pending_player
    '
    Me.opt_status_pending_player.AutoSize = True
    Me.opt_status_pending_player.Location = New System.Drawing.Point(17, 45)
    Me.opt_status_pending_player.Name = "opt_status_pending_player"
    Me.opt_status_pending_player.Size = New System.Drawing.Size(113, 17)
    Me.opt_status_pending_player.TabIndex = 1
    Me.opt_status_pending_player.TabStop = True
    Me.opt_status_pending_player.Text = "xPendingPlayer"
    Me.opt_status_pending_player.UseVisualStyleBackColor = True
    '
    'frm_promotions_delivered_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1447, 631)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_promotions_delivered_report"
    Me.Text = "frm_promotions_delivered"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_promo_credit_type.ResumeLayout(False)
    Me.gb_promo_credit_type.PerformLayout()
    Me.gb_order.ResumeLayout(False)
    Me.gb_order.PerformLayout()
    Me.gb_promo_type.ResumeLayout(False)
    Me.gb_promo_type.PerformLayout()
    Me.gb_user.ResumeLayout(False)
    Me.gb_user.PerformLayout()
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.gb_caption.ResumeLayout(False)
    Me.gb_caption.PerformLayout()
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_promo_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_credit_type_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit_type_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit_type_non_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_order As System.Windows.Forms.GroupBox
  Friend WithEvents opt_order_prom_session_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_order_session_user_prom As System.Windows.Forms.RadioButton
  Friend WithEvents chk_view_data_account As System.Windows.Forms.CheckBox
  Friend WithEvents gb_promo_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_promo_preassigned As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_system_period As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_per_points As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_system_auto As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_manual As System.Windows.Forms.RadioButton
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
  Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_user As GUI_Controls.uc_combo
  Friend WithEvents chk_details As System.Windows.Forms.CheckBox
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents gb_caption As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_caption_subtotal2 As System.Windows.Forms.Label
  Friend WithEvents lbl_caption_subtotal1 As System.Windows.Forms.Label
  Friend WithEvents lbl_color_caption_subtotal1 As System.Windows.Forms.Label
  Friend WithEvents lbl_color_caption_subtotal2 As System.Windows.Forms.Label
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents lbl_msg_no_includes_auto As System.Windows.Forms.Label
  Friend WithEvents opt_order_account_day As System.Windows.Forms.RadioButton
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_status_pending_draw As System.Windows.Forms.RadioButton
  Friend WithEvents opt_status_bought As System.Windows.Forms.RadioButton
  Friend WithEvents opt_status_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_status_pending_player As System.Windows.Forms.RadioButton
End Class
