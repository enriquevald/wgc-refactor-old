'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_peru_frames
' DESCRIPTION:   Browser of frames for Peru
' AUTHOR:        Jos� Mart�nez L�pez
' CREATION DATE: 28-JUN-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 28-JUN-2012 JML    Initial version
' 20-JUL-2012 MPO    Bug, button resend: It's wrong when there aren't frames to resend
' 16-AUG-2012 DDM    Added function ShowHugeNumberOfRows 
' 23-SEP-2013 RRR    Added frame fields description columns
' 19-NOV-2013 LJM    Changes for auditing forms
' 03-SEP-2014 LEM    Added functionality to show terminal location data.
' 20-JAN-2015 FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 26-MAR-2015 ANM    Calling GetProviderIdListSelected always returns a query
'-------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_Controls
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.IO
Imports System.Text
Imports GUI_Controls.uc_grid

#End Region     ' Imports 

Public Class frm_peru_frames
  Inherits frm_base_print

#Region " Members "

  Private m_ds As DataSet

  ' For report filters 
  Private m_print_datetime As Date

  Private m_date_from As String
  Private m_date_to As String

  Private m_types As String
  Private m_status As String
  Private m_terminal As String

  Private m_only_preview As Boolean

  Private _m_details_showed As Boolean

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#End Region     ' Members

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_DATETIME As Integer = 1
  Private Const SQL_COLUMN_TYPE As Integer = 2
  Private Const SQL_COLUMN_STATUS As Integer = 3
  Private Const SQL_COLUMN_STATUS_CHANGED As Integer = 4
  Private Const SQL_COLUMN_REGISTRATION_CODE As Integer = 5
  Private Const SQL_COLUMN_PROVIDER As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 7
  Private Const SQL_COLUMN_DATA As Integer = 8
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 9

  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_ID As Integer
  Private GRID_COLUMN_DATETIME As Integer
  Private GRID_COLUMN_STATUS As Integer
  Private GRID_COLUMN_STATUS_CHANGED As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_TYPE As Integer
  Private GRID_COLUMN_SITE_REGISTRATION_CODE As Integer
  Private GRID_COLUMN_REGISTRATION_CODE As Integer
  Private GRID_COLUMN_COLECTOR_ID As Integer
  Private GRID_COLUMN_TECH_EVENTS
  Private GRID_COLUMN_DATA As Integer
  Private GRID_COLUMN_COIN_IN As Integer
  Private GRID_COLUMN_COIN_OUT As Integer
  Private GRID_COLUMN_PMA As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const COUNTER_PENDING As Integer = 1
  Private Const COUNTER_ERROR As Integer = 2

#End Region   ' Constants

#Region " Overrides "

  ' PURPOSE: Initializes the form id. 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PERU_FRAMES

    Call MyBase.GUI_SetFormId()

  End Sub            ' GUI_SetFormId

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub       ' GUI_Permissions

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    _m_details_showed = False

    ' - Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1083)  ' 1083 "Peru frames"

    ' The default BUTTON_PRINT is used to print Windows DataGrid.
    ' We need a custom PRINT button to print uc_grid.
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Enable custom buttons to use "print" and "excel" options.
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Visible = True
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Text = GLB_NLS_GUI_CONTROLS.GetString(5)  ' 5 "Imprimir"
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27) ' 27 "Excel"

    ' Enable custom button 2 to use show and hide frame details
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Size = New Size(Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Width, Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Height + 10)
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = True
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2665) ' "Mostrar Trama"

    ' - Buttons
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10) ' 10 "Salir"

    ' Date time filter
    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)        ' 201 "Fecha"
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)       ' 202 "Desde"
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)         ' 203 "Hasta"
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' - 
    Me.uc_frame_viewer.Init(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1084), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1105), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1106))

    'Me.btn_delete.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1085)
    Me.btn_pdte.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1086)
    Me.btn_pdte.Enabled = False

    gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1087)
    chk_economical.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1088)
    chk_technical.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1089)

    gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090)
    chk_sended.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1091)
    chk_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1092)
    chk_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1093)
    lbl_sended.Text = "  "
    lbl_pending.Text = "  "
    lbl_error.Text = "  "
    lbl_sended.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    lbl_pending.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    lbl_error.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    'Me.dg_peru_frames.SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub      ' GUI_InitControls

  Protected Overrides Sub GUI_FirstActivation()
    ' Start any thread, timer, etc.
  End Sub   ' GUI_FirstActivation

  Protected Overrides Sub GUI_SetInitialFocus()
    ' Set the focus
  End Sub   ' GUI_SetInitialFocus

  Public Overrides Function GUI_GetScreenType() As ENUM_SCREEN_TYPE
    Return ENUM_SCREEN_TYPE.MODE_SELECT
  End Function   ' GUI_GetScreenType

  ' PURPOSE: Overload function to use the buttons of type "custom" assigning them functions
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_print.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_PRINT
        m_only_preview = True
        Try
          Call GUI_ExcelResultList()
        Finally
          m_only_preview = False
        End Try

        Call MyBase.AuditFormClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call ChangeFrameDetailsVisibility()
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub       ' GUI_ButtonClick

  ' PURPOSE: GUI execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ExecuteQuery()
    Dim _idx_row As Integer
    Dim _redraw As Boolean
    Dim _sql_txt As System.Text.StringBuilder

    Me.btn_pdte.Enabled = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Enabled = False

    Try

      ' Check the filter 
      If Not GUI_FilterCheck() Then
        Return
      End If

      Me.dg_peru_frames.Clear()
      m_ds = New DataSet()

      GUI_ReportUpdateFilters2()

      _sql_txt = New System.Text.StringBuilder()
      _sql_txt.AppendLine("SELECT   WXM_ID ")
      _sql_txt.AppendLine("       , WXM_DATETIME ")
      _sql_txt.AppendLine("       , WXM_TYPE ")
      _sql_txt.AppendLine("       , WXM_STATUS ")
      _sql_txt.AppendLine("       , WXM_STATUS_CHANGED ")
      _sql_txt.AppendLine("       , TE_REGISTRATION_CODE ")
      _sql_txt.AppendLine("       , TE_PROVIDER_ID ")
      _sql_txt.AppendLine("       , TE_NAME ")
      _sql_txt.AppendLine("       , WXM_DATA ")
      _sql_txt.AppendLine("       , WXM_TERMINAL_ID ")
      _sql_txt.AppendLine("  FROM   WXP_001_MESSAGES ")
      _sql_txt.AppendLine("  LEFT   OUTER JOIN TERMINALS ")
      _sql_txt.AppendLine("    ON   TE_TERMINAL_ID = WXM_TERMINAL_ID ")
      _sql_txt.AppendLine(GetSqlWhere())
      _sql_txt.AppendLine("ORDER BY WXM_DATETIME DESC, WXM_ID DESC ")

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sql_txt.ToString())

          _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = Me.dtp_from.Value
          If Me.dtp_to.Checked Then
            _sql_cmd.Parameters.Add("@pFinDate", SqlDbType.DateTime).Value = Me.dtp_to.Value
          End If

          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, m_ds)
          End Using
        End Using
      End Using

      ' Inserts the result table records in the grid of the form
      If Not IsNothing(m_ds) Then

        'RCI & DDM 16-AUG-2012: Show or not, huge number of rows
        If Not frm_base_sel.ShowHugeNumberOfRows(m_ds.Tables(0).Rows.Count) Then
          Exit Sub
        End If

        _redraw = Me.dg_peru_frames.Redraw
        Me.dg_peru_frames.Redraw = False

        If Not GUI_DoEvents(Me.dg_peru_frames) Then
          Exit Sub
        End If

        If m_refresh_grid Then
          Call GUI_StyleSheet()

          m_refresh_grid = False
        End If

        For Each _dr As DataRow In m_ds.Tables(0).Rows
          ' add new row
          Me.dg_peru_frames.AddRow()
          _idx_row = Me.dg_peru_frames.NumRows - 1

          If Not GUI_SetupRow(_idx_row, _dr) Then
            Me.dg_peru_frames.DeleteRowFast(_idx_row)
          End If

          ' JAB 12-JUN-2012: DoEvents run each second.
          If Not GUI_DoEvents(Me.dg_peru_frames) Then
            Exit Sub
          End If

        Next

      End If

      Me.uc_frame_viewer.SetDefaultValues()

      If Me.dg_peru_frames.NumRows > 0 Then
        ' Select the first row
        Me.dg_peru_frames.CurrentRow = 0
        Me.dg_peru_frames.Row(0).IsSelected = True
        Call Me.dg_peru_frames.RepaintRow(0)

      Else
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

      ' Set focus to the data grid
      Call Me.dg_peru_frames.Focus()

    Catch

    Finally
      ' JAB 12-JUN-2012: Check if disposed
      If Not Me.IsDisposed Then
        Me.dg_peru_frames.Redraw = _redraw
        'Me.btn_delete.Enabled = (dg_peru_frames.NumRows > 0)
        Me.btn_pdte.Enabled = (dg_peru_frames.NumRows > 0)

        ' Print buttons disabled if no results
        Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Enabled = (dg_peru_frames.NumRows > 0)
        Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Enabled = (dg_peru_frames.NumRows > 0)

      End If

    End Try

  End Sub      ' GUI_ExecuteQuery

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()

  End Sub       ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _now As Date

    _now = WSI.Common.WGDB.Now
    ' Date selection 
    If Me.dtp_from.Value > _now Then
      ' 419 "Error en el intervalo de fechas: La fecha inicial no puede ser posterior a la fecha actual."
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(419), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_from.Focus()

      Return False
    End If

    If Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        ' 101 "Error en el intervalo de fechas: la fecha inicial tiene que ser anterior a la final."
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function  ' GUI_FilterCheck

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ExcelResultList()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor

    _prev_cursor = Me.Cursor
    Me.Cursor = Cursors.WaitCursor

    Try
      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      frm_base_sel.GUI_GenerateExcel(dg_peru_frames, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1083), m_print_datetime, _print_data, m_only_preview)

    Finally
      Me.Cursor = _prev_cursor
    End Try

  End Sub               ' GUI_ExcelResultList

#End Region   ' Overrides

#Region " Private "

  ' PURPOSE: Operations to be performed to generate the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_PrintGrid()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor

    _prev_cursor = Me.Cursor
    Me.Cursor = Cursors.WaitCursor

    Try
      _print_data = New GUI_Reports.CLASS_PRINT_DATA()

      Call GUI_ReportFilter(_print_data)
      Call frm_base_sel.GUI_ReportDataX(dg_peru_frames, _print_data)
      Call frm_base_sel.GUI_ReportParamsX(dg_peru_frames, _print_data, _
                                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(1083), _
                                          m_print_datetime)

      _print_data.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

      _print_data.Preview()

    Finally
      Me.Cursor = _prev_cursor

    End Try

  End Sub                     ' GUI_PrintResultList

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()
    Dim _initial_time As Date

    _initial_time = WSI.Common.Misc.TodayOpening()

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = True

    Me.dtp_to.Value = _initial_time.AddDays(1)
    Me.dtp_to.Checked = False

    chk_economical.Checked = True
    chk_technical.Checked = True
    chk_sended.Checked = False
    chk_pending.Checked = True
    chk_error.Checked = False

    Me.uc_frame_viewer.SetDefaultValues()

    chk_terminal_location.Checked = False

    'Me.dg_peru_frames.PanelRightVisible = False

  End Sub                  ' SetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.dg_peru_frames
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_PENDING).Visible = True
      .Counter(COUNTER_PENDING).BackColor = lbl_pending.BackColor
      .Counter(COUNTER_ERROR).Visible = True
      .Counter(COUNTER_ERROR).BackColor = lbl_error.BackColor
      .Counter(COUNTER_ERROR).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' ID
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = 0
      .Column(GRID_COLUMN_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DATETIME
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = " "
      .Column(GRID_COLUMN_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1095)
      .Column(GRID_COLUMN_DATETIME).Width = 2000
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TYPE
      .Column(GRID_COLUMN_TYPE).Header(0).Text = " "
      .Column(GRID_COLUMN_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1087)
      .Column(GRID_COLUMN_TYPE).Width = 500
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' STATUS
      .Column(GRID_COLUMN_STATUS).Header(0).Text = " "
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090)
      .Column(GRID_COLUMN_STATUS).Width = 950
      .Column(GRID_COLUMN_STATUS).HighLightWhenSelected = False
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' STATUS CHANGED
      .Column(GRID_COLUMN_STATUS_CHANGED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1096)
      .Column(GRID_COLUMN_STATUS_CHANGED).Header(0).Text = " "
      .Column(GRID_COLUMN_STATUS_CHANGED).Width = 2000
      .Column(GRID_COLUMN_STATUS_CHANGED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' FISCAL REGISTER
      .Column(GRID_COLUMN_REGISTRATION_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1100)
      .Column(GRID_COLUMN_REGISTRATION_CODE).Header(0).Text = " "
      .Column(GRID_COLUMN_REGISTRATION_CODE).Width = 1600
      .Column(GRID_COLUMN_REGISTRATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' SITE REGISTER
      .Column(GRID_COLUMN_SITE_REGISTRATION_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2657)
      .Column(GRID_COLUMN_SITE_REGISTRATION_CODE).Header(0).Text = " "
      .Column(GRID_COLUMN_SITE_REGISTRATION_CODE).Width = 1600
      .Column(GRID_COLUMN_SITE_REGISTRATION_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' COLECTOR ID
      .Column(GRID_COLUMN_COLECTOR_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2658)
      .Column(GRID_COLUMN_COLECTOR_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2659)
      .Column(GRID_COLUMN_COLECTOR_ID).Width = 1700
      .Column(GRID_COLUMN_COLECTOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' EVENTS
      .Column(GRID_COLUMN_TECH_EVENTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2660)
      .Column(GRID_COLUMN_TECH_EVENTS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2659)
      .Column(GRID_COLUMN_TECH_EVENTS).Width = 1300
      .Column(GRID_COLUMN_TECH_EVENTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = " "
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header0
        Else
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        End If
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      ' DATA
      .Column(GRID_COLUMN_DATA).Header(1).Text = ""
      .Column(GRID_COLUMN_DATA).Header(0).Text = ""
      .Column(GRID_COLUMN_DATA).Width = 0
      .Column(GRID_COLUMN_DATA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' FINAL COIN-IN
      .Column(GRID_COLUMN_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2661)
      .Column(GRID_COLUMN_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2664)
      .Column(GRID_COLUMN_COIN_IN).Width = 1250
      .Column(GRID_COLUMN_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' FINAL COIN-OUT
      .Column(GRID_COLUMN_COIN_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2662)
      .Column(GRID_COLUMN_COIN_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2664)
      .Column(GRID_COLUMN_COIN_OUT).Width = 1250
      .Column(GRID_COLUMN_COIN_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' FINAL PMA
      .Column(GRID_COLUMN_PMA).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2663)
      .Column(GRID_COLUMN_PMA).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2664)
      .Column(GRID_COLUMN_PMA).Width = 1250
      .Column(GRID_COLUMN_PMA).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub                    ' GUI_StyleSheet

  ' PURPOSE: Add data rows to the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None 
  '
  Private Function GUI_SetupRow(ByVal IdxRow As Integer, _
                                ByVal DRow As DataRow) As Boolean

    Dim _events_string As String
    Dim _wxp_data_array As List(Of String)
    Dim _terminal_data As List(Of Object)

    _wxp_data_array = New List(Of String)

    _wxp_data_array = WSI.Common.PeruFrameViewer.ToStringArray(DRow(SQL_COLUMN_DATA))

    ' ID
    Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_ID).Value = DRow(SQL_COLUMN_ID)

    ' DATETIME
    Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DRow(SQL_COLUMN_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' TYPE
    Select Case DRow(SQL_COLUMN_TYPE)
      Case WSI.Common.PERU_FRAME_TYPE.TYPE_ECONOMICAL
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1088).Substring(0, 1)
      Case WSI.Common.PERU_FRAME_TYPE.TYPE_TECHNICAL
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1089).Substring(0, 1)
    End Select

    ' STATUS 
    Select Case DRow(SQL_COLUMN_STATUS)
      Case WSI.Common.PERU_FRAME_STATUS.STATUS_SENT
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1091)
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).BackColor = lbl_sended.BackColor
      Case WSI.Common.PERU_FRAME_STATUS.STATUS_PENDING
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1092)
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).BackColor = lbl_pending.BackColor
        Me.dg_peru_frames.Counter(COUNTER_PENDING).Value += 1
      Case WSI.Common.PERU_FRAME_STATUS.STATUS_ERROR
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1093)
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).BackColor = lbl_error.BackColor
        Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.dg_peru_frames.Counter(COUNTER_ERROR).Value += 1
    End Select

    ' STATUS CHANGED
    If DRow(SQL_COLUMN_STATUS) = WSI.Common.PERU_FRAME_STATUS.STATUS_SENT Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_STATUS_CHANGED).Value = GUI_FormatDate(DRow(SQL_COLUMN_STATUS_CHANGED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' TERMINAL ID
    If IsNothing(DRow.Table.Columns(SQL_COLUMN_REGISTRATION_CODE)) OrElse IsDBNull(DRow(SQL_COLUMN_REGISTRATION_CODE)) Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_REGISTRATION_CODE).Value = String.Empty
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_REGISTRATION_CODE).Value = DRow(SQL_COLUMN_REGISTRATION_CODE)
    End If

    ' SITE REGISTER ID
    If IsNothing(_wxp_data_array(2)) Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_SITE_REGISTRATION_CODE).Value = String.Empty
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_SITE_REGISTRATION_CODE).Value = _wxp_data_array(2)
    End If

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DRow(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.dg_peru_frames.Cell(IdxRow, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' COLECTOR ID (TECHNICAL)
    If _wxp_data_array(0) = "T" And Not IsNothing(_wxp_data_array(4)) Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_COLECTOR_ID).Value = _wxp_data_array(4)
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_COLECTOR_ID).Value = String.Empty
    End If

    ' EVENTS (TECHNICAL)
    If _wxp_data_array(0) = "T" Then
      _events_string = String.Join(String.Empty, _wxp_data_array.ToArray(), 6, 10)
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_TECH_EVENTS).Value = _events_string
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_TECH_EVENTS).Value = String.Empty
    End If

    ' COIN IN (ECONOMICAL)
    If _wxp_data_array(0) = "E" And Not IsNothing(_wxp_data_array(7)) Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_COIN_IN).Value = _wxp_data_array(7)
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_COIN_IN).Value = String.Empty
    End If

    ' COIN OUT (ECONOMICAL)
    If _wxp_data_array(0) = "E" And Not IsNothing(_wxp_data_array(8)) Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_COIN_OUT).Value = _wxp_data_array(8)
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_COIN_OUT).Value = String.Empty
    End If

    ' PMA (ECONOMICAL)
    If _wxp_data_array(0) = "E" And Not IsNothing(_wxp_data_array(9)) Then
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_PMA).Value = _wxp_data_array(9)
    Else
      Me.dg_peru_frames.Cell(IdxRow, GRID_COLUMN_PMA).Value = String.Empty
    End If

    Return True

  End Function                 ' GUI_SetupRow

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""

    ' Filter Dates
    str_where = str_where & " AND WXM_DATETIME >= @pIniDate "
    If Me.dtp_to.Checked Then
      str_where = str_where & " AND WXM_DATETIME < @pFinDate "
    End If

    ' Tipo
    If chk_technical.Checked Or chk_economical.Checked Then
      str_where = str_where & "AND CHARINDEX(cast(WXM_TYPE as varchar), '"
      If chk_technical.Checked Then
        str_where = str_where & WSI.Common.PERU_FRAME_TYPE.TYPE_TECHNICAL
      End If
      If chk_economical.Checked Then
        str_where = str_where & WSI.Common.PERU_FRAME_TYPE.TYPE_ECONOMICAL
      End If
      str_where = str_where & "') > 0 "
    End If

    ' status
    If chk_sended.Checked Or chk_pending.Checked Or chk_error.Checked Then
      str_where = str_where & " AND CHARINDEX(cast(WXM_STATUS as varchar), '"
      If chk_sended.Checked Then
        str_where = str_where & WSI.Common.PERU_FRAME_STATUS.STATUS_SENT
      End If
      If chk_pending.Checked Then
        str_where = str_where & WSI.Common.PERU_FRAME_STATUS.STATUS_PENDING
      End If
      If chk_error.Checked Then
        str_where = str_where & WSI.Common.PERU_FRAME_STATUS.STATUS_ERROR
      End If
      str_where = str_where & "') > 0 "
    End If

    ' Filter Terminal
    str_where = str_where & " AND WXM_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    If str_where <> "" Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where
  End Function                  ' GetSqlWhere

  ' PURPOSE: Change status of Frame to pending
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeStatus()
    Dim _selected_frames As PeruFramesTable

    btn_pdte.Enabled = False

    Try
      _selected_frames = PeruFramesTable.CreatePeruFrameTable()

      If IsNothing(Me.dg_peru_frames.SelectedRows) Then Exit Sub

      For Each _selected_row As Integer In Me.dg_peru_frames.SelectedRows()

        _selected_frames.ChangeStatus(Me.dg_peru_frames.Cell(_selected_row, GRID_COLUMN_ID).Value, PERU_FRAME_STATUS.STATUS_PENDING)

      Next

      Using _sql_trx As New DB_TRX()
        If _selected_frames.Save(_sql_trx.SqlTransaction) Then
          _sql_trx.Commit()
          GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
        Else
          _sql_trx.Rollback()
        End If
      End Using
    Catch
    Finally
      btn_pdte.Enabled = (dg_peru_frames.NumRows > 0)
    End Try

  End Sub

  Private Sub ChangeFrameDetailsVisibility()
    Dim _off_set As Integer

    _off_set = 10

    If _m_details_showed = False Then

      _m_details_showed = True

      Me.dg_peru_frames.Height = 244
      'Me.uc_frame_viewer.Height = 267
      Me.uc_frame_viewer.Height = Me.panel_grids.Height - _
                                 (Me.dg_peru_frames.Height + Me.dg_peru_frames.Location.Y) - _
                                  _off_set

      Me.uc_frame_viewer.Location = New Point(3, 258)
      Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2666)

    Else

      _m_details_showed = False

      Me.dg_peru_frames.Height = 518
      Me.uc_frame_viewer.Height = 0
      Me.uc_frame_viewer.Location = New Point(3, 515)
      Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2665)

    End If
  End Sub

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_ID = 1
    GRID_COLUMN_DATETIME = 2
    GRID_COLUMN_STATUS = 3
    GRID_COLUMN_STATUS_CHANGED = 4
    GRID_INIT_TERMINAL_DATA = 5
    GRID_COLUMN_TYPE = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_SITE_REGISTRATION_CODE = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_REGISTRATION_CODE = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_COLECTOR_ID = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_TECH_EVENTS = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_DATA = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_COIN_IN = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_COIN_OUT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_PMA = TERMINAL_DATA_COLUMNS + 13

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 14
  End Sub

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090), m_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1087), m_types)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), m_terminal)

  End Sub      ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_ReportUpdateFilters2()

    m_print_datetime = frm_base_sel.GUI_GetPrintDateTime()

    m_date_from = ""
    m_date_to = ""
    m_terminal = ""
    m_types = ""
    m_status = ""

    ' Dates
    m_date_from = GUI_FormatDate(dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Type
    If chk_technical.Checked Or chk_economical.Checked Then
      If chk_technical.Checked Then
        If m_types.Length > 0 Then
          m_types = m_types & ", " & Me.chk_technical.Text
        Else
          m_types = Me.chk_technical.Text
        End If
      End If
      If chk_economical.Checked Then
        If m_types.Length > 0 Then
          m_types = m_types & ", " & Me.chk_economical.Text
        Else
          m_types = Me.chk_economical.Text
        End If
      End If
    End If

    ' Status
    If chk_sended.Checked Or chk_pending.Checked Or chk_error.Checked Then
      If chk_sended.Checked Then
        If m_status.Length > 0 Then
          m_status = m_status & ", " & Me.chk_sended.Text
        Else
          m_status = Me.chk_sended.Text
        End If
      End If
      If chk_pending.Checked Then
        If m_status.Length > 0 Then
          m_status = m_status & ", " & Me.chk_pending.Text
        Else
          m_status = Me.chk_pending.Text
        End If
      End If
      If chk_error.Checked Then
        If m_status.Length > 0 Then
          m_status = m_status & ", " & Me.chk_error.Text
        Else
          m_status = Me.chk_error.Text
        End If
      End If
    End If

    ' Terminals 
    m_terminal = Me.uc_pr_list.GetTerminalReportText()


  End Sub          ' GUI_ReportUpdateFilters

#End Region   ' GUI Reports

#End Region     ' Private

#Region " Public"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub                        ' ShowForEdit

#End Region       ' Public

#Region " Events "

  Private Sub dg_peru_frames_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_peru_frames.RowSelectedEvent

    If SelectedRow >= 0 Then
      Me.uc_frame_viewer.SetFrame(Me.m_ds.Tables(0).Rows(SelectedRow)(SQL_COLUMN_DATA))
    End If

  End Sub   ' dg_peru_frames_RowSelectedEvent

  Private Sub btn_pdte_ClickEvent() Handles btn_pdte.ClickEvent

    ChangeStatus()

  End Sub   ' btn_pdte_ClickEvent

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub        ' chk_terminal_location_CheckedChanged

#End Region      ' Events 

End Class     ' frm_peru_frames