'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotions
' DESCRIPTION:   Promotions Selection
' AUTHOR:        Raul Cervera
' CREATION DATE: 10-MAY-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-MAY-2010  RCI    Initial version
' 25-MAY-2012  JMM    Added automatic promotions
' 08-JUN-2012  JAB    Eliminate top clause of sql query.
' 06-AUG-2012  RRB    Add credit type filter 
' 22-AUG-2012  MPO    Added new type of promotion --> Promotion preassigned
' 20-SEP-2012  XIT    Added Categories
' 03-MAY-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 05-JUN-2013  DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
' 11-JUN-2013  RRR    Copy the data from a selected promotion to a new one
' 13-JUN-2013  DLL    Fixed Bug #850: Specific type of promotion can to be copy
' 19-JUN-2013  RRR    Fixed Bug #870: Points promotions should not be copied when ELP mode is set in general paramaters
' 19-MAR-2014  AMF    Added new filter to hide system promotions
' 07-APR-2014  JBP    Added only VIP filter.
' 27-MAY-2014  JBC    Fixed Bug WIG-953: Hidden check for only VIP 
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text


Public Class frm_promotions_sel
  Inherits frm_base_sel

#Region " Constants "
  Private Const FORM_DB_MIN_VERSION As Short = 204

  ' DB Columns
  Private Const SQL_COLUMN_PROMOTION_ID As Integer = 0
  Private Const SQL_COLUMN_DATE_START As Integer = 1
  Private Const SQL_COLUMN_DATE_FINISH As Integer = 2
  Private Const SQL_COLUMN_NAME As Integer = 3
  Private Const SQL_COLUMN_ENABLED As Integer = 4
  Private Const SQL_COLUMN_LEVEL_FILTER As Integer = 5
  Private Const SQL_COLUMN_CREDIT_TYPE As Integer = 6
  Private Const SQL_COLUMN_CATEGORY As Integer = 7
  Private Const SQL_COLUMN_TYPE_PROMO As Integer = 8
  Private Const SQL_COLUMN_VIP As Integer = 9
  Private Const SQL_COLUMN_PROMOGAME As Integer = 10

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PROMOTION_ID As Integer = 1
  Private Const GRID_COLUMN_DATE_START As Integer = 2
  Private Const GRID_COLUMN_DATE_FINISH As Integer = 3
  Private Const GRID_COLUMN_NAME As Integer = 4
  Private Const GRID_COLUMN_LEVEL As Integer = 5
  Private Const GRID_COLUMN_MONTH_DAY As Integer = 6
  Private Const GRID_COLUMN_PAYMENT_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_STATUS As Integer = 8
  Private Const GRID_COLUMN_CREDIT_TYPE As Integer = 9
  Private Const GRID_COLUMN_CATEGORY As Integer = 10
  Private Const GRID_COLUMN_TYPE_PROMO As Integer = 11
  Private Const GRID_COLUMN_VIP As Integer = 12
  Private Const GRID_COLUMN_PROMOGAME As Integer = 13

  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_NAME As Integer = 4400
  Private Const GRID_WIDTH_STATUS As Integer = 1800
  Private Const GRID_WIDTH_DATE As Integer = 2100
  Private Const GRID_WIDTH_LEVEL As Integer = 1500
  Private Const GRID_WIDTH_MONTH_DAY As Integer = 1500
  Private Const GRID_WIDTH_PAYMENT_AMOUNT As Integer = 1500
  Private Const GRID_WIDTH_CREDIT_TYPE As Integer = 2100
  Private Const GRID_WIDTH_CATEGORY As Integer = 2100
  Private Const GRID_WIDTH_VIP As Integer = 800
  Private Const GRID_WIDTH_PROMOGAME As Integer = 2100

#End Region ' Constants

#Region " Enums "

  Public Enum ENUM_PROMO_TYPE_MODE

    TYPE_MANUAL = 0
    TYPE_AUTOMATIC = 1
    TYPE_PREASSIGNED = 2

  End Enum

#End Region ' Enums

#Region " Members "

  ' Type of promotions shown
  Private m_promotion_type_mode As ENUM_PROMO_TYPE_MODE

  ' For report filters 
  Private m_name As String
  Private m_status As String
  Private m_init_date_from As String
  Private m_init_date_to As String
  Private m_promo_type_credit As String
  Private m_category As String
  Private m_hidde_system_promotions As String

#End Region ' Members

#Region " Properties "

  Private m_is_selection_mode As Boolean
  Public Property IsSelectionMode() As Boolean
    Get
      Return m_is_selection_mode
    End Get
    Set(ByVal value As Boolean)
      m_is_selection_mode = value
    End Set
  End Property

  Private m_promotion As CLASS_JUNKETS_FLYER.TYPE_PROMOTION
  Public ReadOnly Property PromotionSelected() As CLASS_JUNKETS_FLYER.TYPE_PROMOTION
    Get
      Return m_promotion
    End Get
  End Property

#End Region ' Properties

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTIONS

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Select Case Me.m_promotion_type_mode
      Case ENUM_PROMO_TYPE_MODE.TYPE_MANUAL

        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211) ' 211 "Promociones"

      Case ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC

        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(438) ' 438 "Promociones automáticas"

      Case ENUM_PROMO_TYPE_MODE.TYPE_PREASSIGNED

        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1236) ' 1236 "Promociones preasignadas"
        Me.chk_only_vip.Visible = False

      Case Else

        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211) ' 211 "Promociones"

    End Select

    ' Buttons
    If m_is_selection_mode Then
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    Else
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1)
      Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)
    End If

    ' Init Date
    Me.gb_init_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(251)
    Me.dtp_init_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_init_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_init_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_init_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    'Status
    Me.chk_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(255)
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(246)
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(247)

    Me.ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    Me.ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.chk_not_show_system_promotions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4746)
    If m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_MANUAL Then
      Me.chk_not_show_system_promotions.Visible = True
      Me.chk_not_show_system_promotions.Checked = True
    Else
      Me.chk_not_show_system_promotions.Visible = False
      Me.chk_not_show_system_promotions.Checked = False
    End If

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' RRB 06-AUG-2012: NLS credit type filter
    Me.gb_promo_credit_type.Text = GLB_NLS_GUI_INVOICING.GetString(336)
    Me.chk_credit_type_points.Text = GLB_NLS_GUI_INVOICING.GetString(343)
    Me.chk_credit_type_redeem.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    Me.chk_credit_type_non_redeem.Text = GLB_NLS_GUI_INVOICING.GetString(338)

    Me.cmb_category.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336) ' Categoría

    Me.cmb_category.Add(-1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337))
    Me.cmb_category.Add(PromotionCategories.Categories())

    ' VIP
    Me.chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4804)         ' 4804 "Exclusivo para clientes VIP" 

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim promo_id As Integer
    Dim promo_name As String
    Dim frm_edit As Object
    Dim _cls_promotion As CLASS_PROMOTION
    Dim _promotion As CLASS_PROMOTION.TYPE_PROMOTION

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion ID and Name, and launch the editor
    promo_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROMOTION_ID).Value
    promo_name = Me.Grid.Cell(idx_row, GRID_COLUMN_NAME).Value

    If m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_MANUAL Then
      frm_edit = New frm_promotion_edit
    ElseIf m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC Then
      frm_edit = New frm_promotion_auto_edit
    Else
      frm_edit = New frm_promotion_preassigned_edit
    End If

    If Not m_is_selection_mode Then

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Call frm_edit.ShowEditItem(promo_id, promo_name)

      frm_edit = Nothing

      Call Me.Grid.Focus()

    Else

      _cls_promotion = New CLASS_PROMOTION
      _promotion = New CLASS_PROMOTION.TYPE_PROMOTION
      m_promotion = New CLASS_JUNKETS_FLYER.TYPE_PROMOTION

      _promotion.promotion_id = promo_id
      _cls_promotion.ReadPromotion(_promotion, 0)

      m_promotion.id = _promotion.promotion_id
      m_promotion.name = _promotion.name

      If Not _promotion.enabled Then

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8136), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) <> ENUM_MB_RESULT.MB_RESULT_YES Then

          m_promotion = Nothing

          Return
        End If
      End If

      Me.Close()

        End If

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_init_from.Checked And Me.dtp_init_to.Checked And Me.dtp_init_from.Enabled And Me.dtp_init_to.Enabled Then
      If Me.dtp_init_from.Value > Me.dtp_init_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_init_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ''' <summary>
  ''' Build an SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As StringBuilder

    str_sql = New StringBuilder

    str_sql.AppendLine("SELECT     PM_PROMOTION_ID")
    str_sql.AppendLine("         , PM_DATE_START")
    str_sql.AppendLine("         , PM_DATE_FINISH")
    str_sql.AppendLine("         , PM_NAME")
    str_sql.AppendLine("         , PM_ENABLED")
    str_sql.AppendLine("         , PM_LEVEL_FILTER")
    str_sql.AppendLine("         , PM_CREDIT_TYPE")
    str_sql.AppendLine("         , PM_CATEGORY_ID")
    str_sql.AppendLine("         , PM_TYPE")
    str_sql.AppendLine("         , PM_VIP")
    str_sql.AppendLine("         , PG_NAME")
    str_sql.AppendLine("  FROM   PROMOTIONS")
    str_sql.AppendLine("  LEFT JOIN PROMOGAMES ")
    str_sql.AppendLine("  ON PROMOGAMES.PG_ID = PROMOTIONS.PM_PROMOGAME_ID")

    str_sql.AppendLine(GetSqlWhere())

    str_sql.AppendLine(" ORDER BY PM_DATE_START, PM_DATE_FINISH, PM_NAME")

    Return str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'Dim pm_type As Integer
    Dim str_type As String = ""
    Dim _yes_text As String
    Dim _no_text As String

    _yes_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
    _no_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    ' Date Start
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_START).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_START), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMM)
    ' Date Finish
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FINISH).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_FINISH), _
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Level
    If (Me.m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = GetLevelName(DbRow.Value(SQL_COLUMN_LEVEL_FILTER))
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_LEVEL).Value = ""
    End If

    ' Enabled
    Select Case DbRow.Value(SQL_COLUMN_ENABLED)
      Case True
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)

      Case False
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(248)

    End Select

    ' Promotion Credit Type
    Select Case CType(DbRow.Value(SQL_COLUMN_CREDIT_TYPE), ACCOUNT_PROMO_CREDIT_TYPE)
      Case ACCOUNT_PROMO_CREDIT_TYPE.NR1, _
           ACCOUNT_PROMO_CREDIT_TYPE.NR2, _
           ACCOUNT_PROMO_CREDIT_TYPE.NR3, _
           ACCOUNT_PROMO_CREDIT_TYPE.UNR1, _
           ACCOUNT_PROMO_CREDIT_TYPE.UNR2
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(338)

      Case ACCOUNT_PROMO_CREDIT_TYPE.POINT
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(343)

      Case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(337)

    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_CATEGORY).Value = PromotionCategories.Value(DbRow.Value(SQL_COLUMN_CATEGORY))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE_PROMO).Value = DbRow.Value(SQL_COLUMN_TYPE_PROMO)

    ' VIP
    Me.Grid.Cell(RowIndex, GRID_COLUMN_VIP).Value = _no_text

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_VIP)) Then
      If DbRow.Value(SQL_COLUMN_VIP) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VIP).Value = _yes_text
      End If
    End If

    'PROMO GAME
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PROMOGAME)) Then

      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOGAME).Value = DbRow.Value(SQL_COLUMN_PROMOGAME)

    End If

    Return True

  End Function ' GUI_SetupRow


  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.chk_active
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        'Type of promotions that can to be copy. 
        Dim _ls_type_promo As List(Of Promotion.PROMOTION_TYPE)
        Dim _is_copiable As Boolean
        Dim _indx_sel As Integer

        _ls_type_promo = New List(Of Promotion.PROMOTION_TYPE)
        _ls_type_promo.Add(Promotion.PROMOTION_TYPE.MANUAL)
        _ls_type_promo.Add(Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL)
        _ls_type_promo.Add(Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED)
        _ls_type_promo.Add(Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY)
        _ls_type_promo.Add(Promotion.PROMOTION_TYPE.PREASSIGNED)

        _is_copiable = False
        _indx_sel = -1

        If Not IsNothing(Me.Grid.SelectedRows) AndAlso Me.Grid.SelectedRows.Length > 0 Then
          _indx_sel = Me.Grid.SelectedRows(0)
        End If

        If _indx_sel > -1 Then
          _is_copiable = _ls_type_promo.Contains(Me.Grid.Cell(_indx_sel, GRID_COLUMN_TYPE_PROMO).Value())
        End If

        If _is_copiable Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2049), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            Call GUI_NewItemFromSelected()
          Else
            Call GUI_ShowNewPromotionForm()
          End If
        Else
          Call GUI_ShowNewPromotionForm()
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(308) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_init_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(308) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_init_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(253), m_name)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(280), m_status)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(336), m_promo_type_credit)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336), m_category)
    If m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_MANUAL Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4746), m_hidde_system_promotions)
    Else
      PrintData.SetFilter("", "", True)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(211)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_name = ""
    m_status = ""
    m_init_date_from = ""
    m_init_date_to = ""
    m_promo_type_credit = ""
    m_hidde_system_promotions = ""

    ' Status
    If Me.chk_active.Checked Then
      ' "Active only" filter supersedes any other filter
      m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(255)
    Else
      If Me.chk_enabled.Checked And Me.chk_disabled.Checked Then
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(287)
      Else
        If Me.chk_enabled.Checked Then
          m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(246)
        End If
        If Me.chk_disabled.Checked Then
          m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(247)
        End If
      End If

      ' Init Date From
      If Me.dtp_init_from.Checked Then
        m_init_date_from = GUI_FormatDate(Me.dtp_init_from.Value, _
                                          ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      ' Init Date To
      If Me.dtp_init_to.Checked Then
        m_init_date_to = GUI_FormatDate(Me.dtp_init_to.Value, _
                                        ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

    End If

    If Me.ef_promo_name.Value <> "" Then
      m_name = Me.ef_promo_name.Value
    End If


    ' Promotion Credit Type
    If Me.chk_credit_type_points.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(343)
    End If
    If Me.chk_credit_type_redeem.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(337)
    End If
    If Me.chk_credit_type_non_redeem.Checked Then
      If Not String.IsNullOrEmpty(m_promo_type_credit) Then
        m_promo_type_credit = m_promo_type_credit & ", "
      End If
      m_promo_type_credit = m_promo_type_credit & GLB_NLS_GUI_INVOICING.GetString(338)
    End If

    ' Promotion Categories
    m_category = Me.cmb_category.TextValue

    m_hidde_system_promotions = IIf(Me.chk_not_show_system_promotions.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ShowForEdit(MdiParent, ENUM_PROMO_TYPE_MODE.TYPE_MANUAL)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode choosing promotion type
  '
  '  PARAMS:
  '     - INPUT:
  '           - TypePromotion
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal PromotionTypeMode As ENUM_PROMO_TYPE_MODE)

    Me.m_promotion_type_mode = PromotionTypeMode

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Dim _level_column_header As String
    Dim _month_day_column_header As String
    Dim _payment_amount_column_header As String
    Dim _level_column_width As Integer
    Dim _month_day_column_width As Integer
    Dim _payment_amount_column_width As Integer

    With Me.Grid

      'TODO JMM 30-MAY-2012: At the moment, both modes show the same columns as before of automatic promotions
      _level_column_header = ""
      _month_day_column_header = ""
      _payment_amount_column_header = ""
      _level_column_width = 0
      _month_day_column_width = 0
      _payment_amount_column_width = 0

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' PROMO Id
      .Column(GRID_COLUMN_PROMOTION_ID).Header.Text = " "
      .Column(GRID_COLUMN_PROMOTION_ID).Width = 0
      .Column(GRID_COLUMN_PROMOTION_ID).Mapping = SQL_COLUMN_PROMOTION_ID

      '  Date Start
      .Column(GRID_COLUMN_DATE_START).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(269)
      .Column(GRID_COLUMN_DATE_START).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_START).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Date Finish
      .Column(GRID_COLUMN_DATE_FINISH).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(270)
      .Column(GRID_COLUMN_DATE_FINISH).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_FINISH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  PROMO Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  PROMO Level
      .Column(GRID_COLUMN_LEVEL).Header.Text = _level_column_header
      .Column(GRID_COLUMN_LEVEL).Width = _level_column_width
      .Column(GRID_COLUMN_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  PROMO Month's day
      .Column(GRID_COLUMN_MONTH_DAY).Header.Text = _month_day_column_header
      .Column(GRID_COLUMN_MONTH_DAY).Width = _month_day_column_width
      .Column(GRID_COLUMN_MONTH_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  PROMO Payment amount
      .Column(GRID_COLUMN_PAYMENT_AMOUNT).Header.Text = _payment_amount_column_header
      .Column(GRID_COLUMN_PAYMENT_AMOUNT).Width = _payment_amount_column_width
      .Column(GRID_COLUMN_PAYMENT_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  PROMO Enabled
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' PROMO Credit type
      .Column(GRID_COLUMN_CREDIT_TYPE).Header.Text = GLB_NLS_GUI_INVOICING.GetString(336)
      .Column(GRID_COLUMN_CREDIT_TYPE).Width = GRID_WIDTH_CREDIT_TYPE
      .Column(GRID_COLUMN_CREDIT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' PROMO Category
      .Column(GRID_COLUMN_CATEGORY).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
      .Column(GRID_COLUMN_CATEGORY).Width = GRID_WIDTH_CATEGORY
      .Column(GRID_COLUMN_CATEGORY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' PROMO Type
      .Column(GRID_COLUMN_TYPE_PROMO).Header.Text = " "
      .Column(GRID_COLUMN_TYPE_PROMO).WidthFixed = 0

      ' Vip
      .Column(GRID_COLUMN_VIP).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4805)         ' 4805 "VIP"
      .Column(GRID_COLUMN_VIP).Width = GRID_WIDTH_VIP
      .Column(GRID_COLUMN_VIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
      ' Promogame
      '.Column(GRID_COLUMN_PROMOGAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8606)         ' 8602 "Promogame"
      'If WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False) Then
      '  .Column(GRID_COLUMN_PROMOGAME).Width = GRID_WIDTH_PROMOGAME
      'Else
      .Column(GRID_COLUMN_PROMOGAME).Width = 0
      'End If
      '.Column(GRID_COLUMN_PROMOGAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim closing_time As Integer

    closing_time = GetDefaultClosingTime()
    Me.dtp_init_from.Value = New DateTime(Now.Year, Now.Month, 1, closing_time, 0, 0)
    Me.dtp_init_from.Checked = True
    Me.dtp_init_to.Value = Me.dtp_init_from.Value.AddMonths(1)
    Me.dtp_init_to.Checked = False

    Me.chk_active.Checked = True
    Me.chk_enabled.Checked = False
    Me.chk_disabled.Checked = False

    Me.ef_promo_name.Value = ""

    Me.chk_not_show_system_promotions.Checked = True

    ' Promotion Credit Type
    Me.chk_credit_type_points.Checked = False
    Me.chk_credit_type_redeem.Checked = False
    Me.chk_credit_type_non_redeem.Checked = False

    Me.cmb_category.Value = -1

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _preassigned_promo_type_list As String = ""
    Dim _automatic_promo_type_list As String = ""
    Dim _str_where_credit_type As String = ""
    Dim _str_aux As String = ""
    Dim _no_visible_promo_type_list As String = ""

    ' Filter Promo Status

    _automatic_promo_type_list = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL & ", " & Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED & ", " & Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY
    _preassigned_promo_type_list = Promotion.PROMOTION_TYPE.PREASSIGNED
    _no_visible_promo_type_list = Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN

    If m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_MANUAL Then
      If Me.chk_not_show_system_promotions.Checked Then
        _str_where = _str_where & " AND PM_TYPE = " & ENUM_PROMO_TYPE_MODE.TYPE_MANUAL
      Else
        _str_where = _str_where & " AND PM_TYPE NOT IN (" & _automatic_promo_type_list & "," & _preassigned_promo_type_list & "," & _no_visible_promo_type_list & ")"
      End If
    ElseIf m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC Then
      _str_where = _str_where & " AND PM_TYPE IN (" & _automatic_promo_type_list & ")"
    ElseIf m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_PREASSIGNED Then
      _str_where = _str_where & " AND PM_TYPE IN (" & _preassigned_promo_type_list & ")"
    End If

    ' Active status promotions filter takes precedence over any other filter
    If Me.chk_active.Checked = True Then
      _str_where = _str_where & " AND PM_ENABLED = 1 " & _
                              " AND (GetDate() BETWEEN PM_DATE_START AND PM_DATE_FINISH) "

    Else

      ' Filter INIT Dates
      If Me.dtp_init_from.Checked = True Then
        _str_where = _str_where & " AND (PM_DATE_START >= " & GUI_FormatDateDB(dtp_init_from.Value) & ") "
      End If

      If Me.dtp_init_to.Checked = True Then
        _str_where = _str_where & " AND (PM_DATE_START < " & GUI_FormatDateDB(dtp_init_to.Value) & ") "
      End If

      If Me.chk_enabled.Checked = True And Me.chk_disabled.Checked = False Then
        _str_where = _str_where & " AND PM_ENABLED = 1 "
      End If
      If Me.chk_disabled.Checked = True And Me.chk_enabled.Checked = False Then
        _str_where = _str_where & " AND PM_ENABLED = 0 "
      End If

    End If

    If Me.ef_promo_name.Value <> "" Then
      _str_where = _str_where & " AND " & GUI_FilterField("PM_NAME", Me.ef_promo_name.Value, False, False, True)
    End If

    ' Filter Credit Type
    If Me.chk_credit_type_points.Checked Then
      _str_where_credit_type = ACCOUNT_PROMO_CREDIT_TYPE.POINT
    End If
    If Me.chk_credit_type_redeem.Checked Then
      If Not String.IsNullOrEmpty(_str_where_credit_type) Then
        _str_where_credit_type = _str_where_credit_type & ","
      End If
      _str_where_credit_type = _str_where_credit_type & " " & ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    End If
    If Me.chk_credit_type_non_redeem.Checked Then
      If Not String.IsNullOrEmpty(_str_where_credit_type) Then
        _str_where_credit_type = _str_where_credit_type & ","
      End If
      _str_where_credit_type = _str_where_credit_type & " " & ACCOUNT_PROMO_CREDIT_TYPE.NR1 & ", " & _
                                                              ACCOUNT_PROMO_CREDIT_TYPE.NR2 & ", " & _
                                                              ACCOUNT_PROMO_CREDIT_TYPE.NR3 & ", " & _
                                                              ACCOUNT_PROMO_CREDIT_TYPE.UNR1 & ", " & _
                                                              ACCOUNT_PROMO_CREDIT_TYPE.UNR2
    End If
    If Not String.IsNullOrEmpty(_str_where_credit_type) Then
      _str_where_credit_type = " PM_CREDIT_TYPE IN ( " & _str_where_credit_type & ")"
      If Not String.IsNullOrEmpty(_str_where) Then
        _str_where = _str_where & " AND "
      End If
      _str_where = _str_where & _str_where_credit_type
    End If

    '  Category Filter
    If Not cmb_category.Value = -1 Then
      _str_where = _str_where & " AND PM_CATEGORY_ID = " & cmb_category.Value
    End If

    '   - Vip
    If Me.chk_only_vip.Checked Then
      _str_where = _str_where & " AND PM_VIP = 1"
    End If

    ' Final processing
    If Len(_str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new promotion to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_ShowNewPromotionForm(Optional ByVal PromotionCopiedId As Long = 0)

    If m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_MANUAL Then
      Dim frm_edit As frm_promotion_edit
      frm_edit = New frm_promotion_edit()
      frm_edit.ShowNewItem(PromotionCopiedId)
      frm_edit = Nothing
    ElseIf m_promotion_type_mode = ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC Then
      Dim frm_edit As frm_promotion_auto_edit
      frm_edit = New frm_promotion_auto_edit()
      frm_edit.ShowNewItem(PromotionCopiedId)
      frm_edit = Nothing
    Else
      Dim frm_edit As frm_promotion_preassigned_edit
      frm_edit = New frm_promotion_preassigned_edit()
      frm_edit.ShowNewItem(PromotionCopiedId)
      frm_edit = Nothing
    End If

    Call Me.Grid.Focus()

  End Sub ' NewPromotion

  ' PURPOSE : Adds a new promotion to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_NewItemFromSelected()

    Dim idx_row As Int32
    Dim promo_id As Integer

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion ID, and launch the editor
    promo_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROMOTION_ID).Value

    Call GUI_ShowNewPromotionForm(promo_id)

  End Sub ' NewPromotion

  ' PURPOSE : Gets level name from a level code
  '
  '  PARAMS :
  '     - INPUT :
  '          - LevelFilter
  '
  '     - OUTPUT :
  '
  ' RETURNS : Level name as is setup on cashier configuration
  Private Function GetLevelName(ByVal Level As Integer) As String

    Dim _str_binary As String
    Dim _bit_array As Char()
    Dim _idx As Integer
    Dim _level_key As String

    _str_binary = Convert.ToString(Level, 2)
    _str_binary = New String("0", 5 - _str_binary.Length) + _str_binary

    _bit_array = _str_binary.ToCharArray()
    Array.Reverse(_bit_array)

    'Look for the bit at the array that tells which level is activated
    For _idx = 0 To _bit_array.Length - 1
      If (_bit_array(_idx) = "1") Then

        'Once the level is found, let's build the key to get its name
        _level_key = "Level" & Strings.Right("00" & _idx, 2) & ".Name"

        Return GetCashierPlayerTrackingData(_level_key)

      End If
    Next

    Return ""

  End Function ' GetLevelName

#End Region ' Private Functions

#Region "Events"

  Private Sub chk_active_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_active.CheckedChanged
    Me.dtp_init_from.Enabled = Not Me.chk_active.Checked
    Me.dtp_init_to.Enabled = Not Me.chk_active.Checked
    Me.chk_enabled.Enabled = Not Me.chk_active.Checked
    Me.chk_disabled.Enabled = Not Me.chk_active.Checked
    'Me.ef_promo_name.Enabled = Not Me.chk_active.Checked  ' XIT Filter Must be activated always 
  End Sub ' chk_active_CheckedChanged

#End Region ' Events

End Class
