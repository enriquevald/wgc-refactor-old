'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminals_without_activity.vb
'
' DESCRIPTION:   Terminals without activity form
'
' AUTHOR:        Ra�l Cervera
'
' CREATION DATE: 17-DEC-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-DEC-2010  RCI    Initial version.
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 20-FEB-2015  ANM    Increased form's width due there is a new column in grid
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_terminals_without_activity
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents chk_only_active As System.Windows.Forms.CheckBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.chk_only_active = New System.Windows.Forms.CheckBox
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.chk_only_active)
    Me.panel_filter.Size = New System.Drawing.Size(916, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_active, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(916, 524)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(910, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(910, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(270, 5)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(337, 191)
    Me.uc_pr_list.TabIndex = 2
    '
    'chk_only_active
    '
    Me.chk_only_active.AutoSize = True
    Me.chk_only_active.Location = New System.Drawing.Point(613, 28)
    Me.chk_only_active.Name = "chk_only_active"
    Me.chk_only_active.Size = New System.Drawing.Size(155, 17)
    Me.chk_only_active.TabIndex = 3
    Me.chk_only_active.Text = "xOnly active machines"
    Me.chk_only_active.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(7, 8)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(17, 143)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(247, 17)
    Me.chk_terminal_location.TabIndex = 1
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_terminals_without_activity
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(924, 722)
    Me.Name = "frm_terminals_without_activity"
    Me.Text = "frm_terminals_without_activity"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private TERMINAL_DATA_COLUMNS As Int32

  ' DB Columns
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 3

  ' Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_DATE_FROM As Integer
  Private GRID_COLUMN_DATE_TO As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_STATUS As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_terminal As String
  Private m_date_from As String
  Private m_date_to As String
  Private m_only_active As String

  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE : Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINALS_WITHOUT_ACTIVITY

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(361)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_STATISTICS.Id(202))

    ' Only active machines
    Me.chk_only_active.Text = GLB_NLS_GUI_INVOICING.GetString(362)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call GUI_StyleSheet()

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    'str_sql = "SELECT " & _
    '          "         ISNULL (TE_PROVIDER_ID, '') " & _
    '          "       , ISNULL (TE_NAME, '') " & _
    '          "       , TE_STATUS " & _
    '          "  FROM   TERMINALS " & _
    '          " WHERE   TE_TYPE        = 1 " & _
    '          "   AND   TE_TERMINAL_ID NOT IN " & _
    '          "           ( SELECT   SPH_TERMINAL_ID " & _
    '                         "FROM   SALES_PER_HOUR_V "

    str_sql = "SELECT " & _
          "         ISNULL (TE_PROVIDER_ID, '') " & _
          "       , ISNULL (TE_NAME, '') " & _
          "       , TE_STATUS " & _
          "       , TE_TERMINAL_ID " & _
          "  FROM   TERMINALS " & _
          " WHERE   TE_TYPE        = 1 " & _
          "   AND   TE_TERMINAL_ID NOT IN " & _
          "           ( SELECT   SPH_TERMINAL_ID " & _
                         "FROM   SALES_PER_HOUR_V2 "

    str_sql = str_sql & GetSqlWhereSalesPerHour()
    str_sql = str_sql & " GROUP BY SPH_TERMINAL_ID ) "

    str_sql = str_sql & GetSqlWhereTerminals()
    str_sql = str_sql & " ORDER BY TE_PROVIDER_ID, TE_NAME"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    ' Date from
    If m_date_from <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = GUI_FormatDate(m_date_from, _
                                                                           ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                           ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_FROM).Value = ""
    End If

    ' Date to
    If m_date_to <> "" Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = GUI_FormatDate(m_date_to, _
                                                                         ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                         ENUM_FORMAT_TIME.FORMAT_HHMM)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_TO).Value = ""
    End If

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' Status
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GetTerminalStatusString(DbRow.Value(SQL_COLUMN_STATUS))

    Return True

  End Function ' GUI_SetupRow 

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(362), m_only_active)
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000
    PrintData.FilterHeaderWidth(3) = 2000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_only_active = ""
    m_terminal = ""
    m_date_from = ""
    m_date_to = ""

    ' Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Providers - Terminals
    m_terminal = Me.uc_pr_list.GetTerminalReportText()

    ' Only Active
    If Me.chk_only_active.Checked Then
      m_only_active = GLB_NLS_GUI_AUDITOR.GetString(336)
    Else
      m_only_active = GLB_NLS_GUI_AUDITOR.GetString(337)
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date from
      .Column(GRID_COLUMN_DATE_FROM).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_FROM).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(309)
      .Column(GRID_COLUMN_DATE_FROM).Width = 1800
      .Column(GRID_COLUMN_DATE_FROM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date to
      .Column(GRID_COLUMN_DATE_TO).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(204)
      .Column(GRID_COLUMN_DATE_TO).Header(1).Text = GLB_NLS_GUI_STATISTICS.GetString(310)
      .Column(GRID_COLUMN_DATE_TO).Width = 1800
      .Column(GRID_COLUMN_DATE_TO).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      '  Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(261)
      .Column(GRID_COLUMN_STATUS).Width = 1700
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WSI.Common.WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_only_active.Checked = True

    Call Me.uc_pr_list.SetDefaultValues()

    chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SalesPerHour part SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetSqlWhereSalesPerHour() As String

    Dim str_where As String = ""

    ' Filter Dates
    str_where = Me.uc_dsl.GetSqlFilterCondition("SPH_BASE_HOUR")
    If str_where.Length > 0 Then
      str_where = " AND " & str_where
    End If

    ' Filter Providers - Terminals
    str_where = str_where & " AND SPH_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    If str_where.Length > 0 Then
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where
  End Function ' GetSqlWhereSalesPerHour

  ' PURPOSE: Get Sql WHERE to build Terminals part SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetSqlWhereTerminals() As String

    Dim str_where As String = ""

    ' Filter Providers - Terminals
    str_where = str_where & " AND TE_TERMINAL_ID IN " & Me.uc_pr_list.GetProviderIdListSelected()

    ' Filter Only Active Machines
    If Me.chk_only_active.Checked Then
      str_where = str_where & " AND TE_STATUS = " & WSI.Common.TerminalStatus.ACTIVE
    End If

    Return str_where
  End Function ' GetSqlWhereTerminals

  ' PURPOSE: Get the string name of TerminalStatus.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status As WSI.Common.TerminalStatus
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String

  Private Function GetTerminalStatusString(ByVal Status As WSI.Common.TerminalStatus) As String

    Select Case Status
      Case WSI.Common.TerminalStatus.ACTIVE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)

      Case WSI.Common.TerminalStatus.OUT_OF_SERVICE
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)

      Case WSI.Common.TerminalStatus.RETIRED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(399)

      Case Else
        Return "Unknown"

    End Select
  End Function ' GetTerminalStatusString

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_DATE_FROM = 1
    GRID_COLUMN_DATE_TO = 2
    GRID_INIT_TERMINAL_DATA = 3
    GRID_COLUMN_STATUS = TERMINAL_DATA_COLUMNS + 3

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 4

  End Sub

#End Region  ' Private Functions

#Region " Events"

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region 'Events

End Class
