'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_command_send
' DESCRIPTION:   Send WCP commands
'                   
' AUTHOR:        Miquel Beltran Febrer
' CREATION DATE: 15-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-JUN-2010  MBF    Initial version
' 27-FEB-2012  MPO    Fixed Bug #189: The control terminals, only it must view the type terminals WIN (1), SAS_HOST (5), SITE_JACKPOT (101) and MOBILE_BANK
' 25-APR-2013  RBG    Fixed Bug #746: It makes no sense to work in this form with multiselection
' 11-JUN-2013  NMR    Preventing the focus losing when showing log details
' 23-MAY-2014  DRV    Fixed Bug WIG-512: Restart and Logger commands aren't audited
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 25-SEP-2014  LEM    Fixed Bug WIG-1295: Wrong refreshing when "Show terminal location" is activated.
' 30-SEP-2014  LEM    Fixed Bug WIG-1340: Option "Show terminal location" do not work correctly.
' 30-JAN-2015  ANM    Increased form's width due there is a new column in grid
' 18_AUG-2017  MS     [WIGOS-3657] Errors in Send Commands screen
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_command_send
  Inherits frm_base

#Region " Enums "

#End Region ' Enums

#Region " Constants "

  ' Indicates if Counters are visible or not.
  Private Const COUNTERS_VISIBLE As Boolean = False

#End Region ' Constants

#Region " Members "

  Private m_command_list() As TYPE_COMMAND_ITEM
  Private m_command_list_count As Integer = 0
  Private m_updating As Boolean
  Private m_last_update As Long

  Private m_refresh_grid As Boolean = False

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_COMMANDS_SEND

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Init gui controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_InitControls()

    Dim fore_color As Color
    Dim back_color As Color

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_CONTROLS.GetString(421)

    mdl_commands.TerminalReportType = ReportType.Provider

    Call StyleSheet()

    '' Me.gb_sdfsdf
    'Me.gb_last_update.Text = GLB_NLS_GUI_STATISTICS.GetString(416)
    'Me.chk_reciente.Text = GLB_NLS_GUI_STATISTICS.GetString(417)
    'Me.chk_old.Text = GLB_NLS_GUI_STATISTICS.GetString(418)

    Me.btn_logger.Text = GLB_NLS_GUI_CONTROLS.GetString(415)
    Me.btn_restart.Text = GLB_NLS_GUI_CONTROLS.GetString(416)
    Me.btn_clear.Text = GLB_NLS_GUI_CONTROLS.GetString(7)
    Me.btn_select.Text = GLB_NLS_GUI_CONTROLS.GetString(26)
    Me.btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' RCI 21-OCT-2010: Execute Permissions
    Me.btn_logger.Enabled = Me.Permissions.Read
    Me.btn_restart.Enabled = Me.Permissions.Execute

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)
    ' Text Status Labels
    Me.lbl_pending_send_text.Text = GLB_NLS_GUI_CONTROLS.GetString(402)
    Me.lbl_pending_reply_text.Text = GLB_NLS_GUI_CONTROLS.GetString(403)
    Me.lbl_replied_ok_text.Text = GLB_NLS_GUI_CONTROLS.GetString(404)
    Me.lbl_replied_error_text.Text = GLB_NLS_GUI_CONTROLS.GetString(405)
    Me.lbl_timeout_disconnected_text.Text = GLB_NLS_GUI_CONTROLS.GetString(406)
    Me.lbl_timeout_not_reply_text.Text = GLB_NLS_GUI_CONTROLS.GetString(407)
    ' Color Status Labels
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND, fore_color, back_color)
    Me.lbl_pending_send.BackColor = back_color
    Me.lbl_pending_send.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY, fore_color, back_color)
    Me.lbl_pending_reply.BackColor = back_color
    Me.lbl_pending_reply.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_OK, fore_color, back_color)
    Me.lbl_replied_ok.BackColor = back_color
    Me.lbl_replied_ok.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR, fore_color, back_color)
    Me.lbl_replied_error.BackColor = back_color
    Me.lbl_replied_error.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED, fore_color, back_color)
    Me.lbl_timeout_disconnected.BackColor = back_color
    Me.lbl_timeout_disconnected.ForeColor = fore_color
    WCP_Command_GetUpdateStatusColor(ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY, fore_color, back_color)
    Me.lbl_timeout_not_reply.BackColor = back_color
    Me.lbl_timeout_not_reply.ForeColor = fore_color

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.WCPTerminalTypes(), uc_provider.UC_FILTER_TYPE.ALL_ACTIVE)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Set filter default values
    Call SetDefaultValues()

    Me.gd_commands.Clear()

    m_updating = False
    m_last_update = 0

    Me.tmr_refresh.Interval = 5000
    Me.tmr_refresh.Start()

  End Sub ' GUI_InitControls

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Call Me.uc_pr_list.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Initialize all form filters with their default values and
  '          clear the data showed in the data grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ClearAllData()

    Call SetDefaultValues()

    Me.gd_commands.Clear()
    m_command_list = Nothing
    m_command_list_count = 0

    Me.btn_select.Enabled = False

  End Sub ' ClearAllData

  ' PURPOSE: Insert commands for each selected terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - CommandType
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertCommand(ByVal CommandType As WSI.Common.WCP_CommandCode, ByVal CmdParameters As String)

    Dim _list_terminal_id As Long()
    Dim _command_table As DataTable
    Dim _data_row As DataRow
    Dim _command_adapter As SqlDataAdapter
    Dim _str_sql As String
    Dim _sql_trans As SqlTransaction = Nothing
    Dim _commit_trx As Boolean
    Dim _cursor As Cursor
    Dim _auditor As AuditCommand
    Dim _now As Date

    ' Get terminals id list from control
    _list_terminal_id = Me.uc_pr_list.GetTerminalIdListSelected(uc_provider.UC_FILTER_TYPE.ALL_ACTIVE)

    If Not Me.uc_pr_list.TerminalListHasValues Then
      Exit Sub
    End If

    _cursor = Windows.Forms.Cursor.Current
    _commit_trx = False
    _command_table = InitCommandDataTable()

    Try
      If Not GUI_BeginSQLTransaction(_sql_trans) Then
        Exit Sub
      End If

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      _auditor = New AuditCommand()

      For Each _terminal_id As Long In _list_terminal_id
        _data_row = _command_table.NewRow()
        _data_row.Item("CMD_TERMINAL_ID") = _terminal_id
        _command_table.Rows.Add(_data_row)
        _auditor.AuditTerminalCommand(_terminal_id, CommandType, _sql_trans)
      Next
      _auditor.AuditNotify()

      _command_adapter = New SqlDataAdapter()
      _command_adapter.SelectCommand = Nothing
      _command_adapter.UpdateCommand = Nothing
      _command_adapter.DeleteCommand = Nothing

      _str_sql = "INSERT INTO WCP_COMMANDS " & _
                      " ( CMD_TERMINAL_ID " & _
                      " , CMD_CODE " & _
                      " , CMD_STATUS  "

      If Not String.IsNullOrEmpty(CmdParameters) Then
        _str_sql = _str_sql & _
                      " , CMD_PARAMETER "
      End If
      _str_sql = _str_sql & _
              " ) VALUES ( @TerminalId , @Code , 0 "


      If Not String.IsNullOrEmpty(CmdParameters) Then
        _str_sql = _str_sql & _
                      " , @Parameters "
      End If
      _str_sql = _str_sql & ")"
      _command_adapter.InsertCommand = New SqlCommand(_str_sql)
      _command_adapter.InsertCommand.Connection = _sql_trans.Connection
      _command_adapter.InsertCommand.Transaction = _sql_trans

      ' Add Event: Get new CMD_ID in insert statement
      AddHandler _command_adapter.RowUpdated, New SqlRowUpdatedEventHandler(AddressOf command_adapter_OnRowUpdated)

      _command_adapter.InsertCommand.Parameters.Add("@TerminalId", SqlDbType.Int, 0, "CMD_TERMINAL_ID")
      _command_adapter.InsertCommand.Parameters.Add("@Code", SqlDbType.Int).Value = CommandType
      If Not String.IsNullOrEmpty(CmdParameters) Then
        _command_adapter.InsertCommand.Parameters.Add("@Parameters", SqlDbType.NVarChar).Value = CmdParameters
      End If

      _command_adapter.Update(_command_table)

      _commit_trx = True

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
      GUI_EndSQLTransaction(_sql_trans, _commit_trx)
      Windows.Forms.Cursor.Current = _cursor
    End Try

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Add command to command list
    ReDim Preserve m_command_list(0 To m_command_list_count + _command_table.Rows.Count - 1)

    For Each _row As DataRow In _command_table.Select()
      _now = WSI.Common.WGDB.Now

      m_command_list(m_command_list_count).id = _row.Item("CMD_ID")
      m_command_list(m_command_list_count).terminal_id = _row.Item("CMD_TERMINAL_ID")
      m_command_list(m_command_list_count).code = CommandType
      m_command_list(m_command_list_count).status = 0
      m_command_list(m_command_list_count).date_send = _now
      m_command_list(m_command_list_count).date_status_change = _now
      m_command_list(m_command_list_count).previous_status = -1

      m_command_list_count = m_command_list_count + 1
    Next

    Me.gd_commands.Redraw = False

    UpdateGrid()

    Me.gd_commands.Redraw = True

    Windows.Forms.Cursor.Current = _cursor

  End Sub ' InsertCommand

  ' PURPOSE: Insert commands for each selected terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - CommandType
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InsertCommand(ByVal CommandType As WSI.Common.WCP_CommandCode)

    InsertCommand(CommandType, String.Empty)
  End Sub ' InsertCommand

  ' PURPOSE: Create a DataTable for the COMMANDS to send to the selected terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - DataTable
  Private Function InitCommandDataTable() As DataTable
    Dim _command_table As DataTable
    Dim _column As DataColumn

    _command_table = New DataTable("WCP_COMMANDS")

    _column = New DataColumn("CMD_ID")
    _column.DataType = System.Type.GetType("System.Int64")
    _column.ReadOnly = False
    _command_table.Columns.Add(_column)

    _column = New DataColumn("CMD_TERMINAL_ID")
    _column.DataType = System.Type.GetType("System.Int32")
    _column.ReadOnly = False
    _command_table.Columns.Add(_column)

    Return _command_table
  End Function ' InitCommandDataTable

  ' PURPOSE: Update command list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateGrid()

    Dim _counter_list() As Integer
    Dim _command_row_idx As Integer

    If m_refresh_grid Then
      Call StyleSheet()
    End If

    _counter_list = WCP_Command_CounterStatusInit()

    UpdateCommandsDatabase()

    For _command_row_idx = 0 To m_command_list_count - 1

      ' Check if new row
      If gd_commands.NumRows = _command_row_idx Then
        ' First, add new row
        gd_commands.AddRow()
      End If

      If m_command_list(_command_row_idx).previous_status <> m_command_list(_command_row_idx).status OrElse _
         m_refresh_grid Then

        m_command_list(_command_row_idx).previous_status = m_command_list(_command_row_idx).status
        UpdateGridRow(_command_row_idx)

      End If

      WCP_Command_CounterStatusIncrement(m_command_list(_command_row_idx).status, _counter_list)

    Next

    WCP_Command_CounterStatusUpdate(Me.gd_commands, _counter_list)

    m_refresh_grid = False

  End Sub ' UpdateGrid

  ' PURPOSE: Read command individually from DB and update status 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateCommandsDatabase()

    Dim _str_sql As String
    Dim _str_cmd_ids As String = ""
    Dim _table As DataTable
    Dim _num_db_rows As Integer
    Dim _idx_db_row As Integer
    Dim _idx_command_list As Integer
    Dim _last_idx_cmd As Integer
    Dim _found As Boolean
    Dim _select_enabled As Boolean
    Dim _data_row As DataRow

    If m_command_list Is Nothing Then
      Exit Sub
    End If

    _select_enabled = False

    Try

      For Each _command As TYPE_COMMAND_ITEM In m_command_list

        If _command.status = ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND Or _
           _command.status = ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY Then

          If (_str_cmd_ids <> "") Then
            _str_cmd_ids = _str_cmd_ids & ", "
          End If
          _str_cmd_ids = _str_cmd_ids & _command.id

        End If

      Next

      If _str_cmd_ids = "" Then
        Exit Sub
      End If

      _str_sql = "SELECT CMD_ID " & _
                     " , CMD_STATUS " & _
                     " , CMD_CREATED " & _
                     " , CMD_STATUS_CHANGED " & _
                     " , TE_NAME " & _
                     " , TE_PROVIDER_ID " & _
                  " FROM WCP_COMMANDS, TERMINALS " & _
                 " WHERE CMD_ID IN ( " & _str_cmd_ids & " ) " & _
                 "   AND TE_TERMINAL_ID = CMD_TERMINAL_ID " & _
                 " ORDER BY CMD_ID "

      _table = GUI_GetTableUsingSQL(_str_sql, m_command_list_count)

      'If IsNothing(_table) Then
      '  Exit Sub
      'End If

      _num_db_rows = _table.Rows.Count()
      _last_idx_cmd = 0

      For _idx_db_row = 0 To _num_db_rows - 1
        _found = False
        For _idx_command_list = 0 To m_command_list_count - 1

          _data_row = _table.Rows(_idx_db_row)

          If m_command_list(_last_idx_cmd).id = _data_row.Item("CMD_ID") Then
            m_command_list(_last_idx_cmd).status = _data_row.Item("CMD_STATUS")
            m_command_list(_last_idx_cmd).date_send = _data_row.Item("CMD_CREATED")
            m_command_list(_last_idx_cmd).date_status_change = _data_row.Item("CMD_STATUS_CHANGED")
            m_command_list(_last_idx_cmd).terminal_name = _data_row.Item("TE_NAME")
            If _data_row.IsNull("TE_PROVIDER_ID") Then
              m_command_list(_last_idx_cmd).provider_name = ""
            Else
              m_command_list(_last_idx_cmd).provider_name = _data_row.Item("TE_PROVIDER_ID")
            End If

            _found = True

            ' To enable button SELECT
            If m_command_list(_last_idx_cmd).code = WSI.Common.WCP_CommandCode.GetLogger AndAlso _
               m_command_list(_last_idx_cmd).status = ENUM_WCP_COMMANDS_STATUS.STATUS_OK Then
              _select_enabled = True
            End If

          End If

          _last_idx_cmd = (_last_idx_cmd + 1) Mod m_command_list_count
          If _found Then
            Exit For
          End If

        Next
      Next

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' UpdateCommand

  ' PURPOSE: Update the grid, insert row if not exists or change row if it exists 
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '     - OUTPUT
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateGridRow(ByVal RowIndex As Integer)

    WCP_Command_SetupRow(Me.gd_commands, RowIndex, m_command_list(RowIndex))

  End Sub ' UpdateGridRow


  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: 
  '     - None
  Private Sub StyleSheet()

    WCP_Command_StyleSheet(Me.gd_commands, COUNTERS_VISIBLE)
    Me.gd_commands.SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

  End Sub ' StyleSheet

  ' PURPOSE: Activated when a row of the grid is double clicked or selected
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Private Sub GUI_EditSelectedItem()

    Dim idx_row As Int32
    Dim cmd_id As UInt64
    Dim str_provider As String
    Dim str_terminal As String
    Dim str_created As String
    'Dim previous_top_most As Boolean

    ' Search the first row selected
    For idx_row = 0 To Me.gd_commands.NumRows - 1
      If Me.gd_commands.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.gd_commands.NumRows Then
      Return
    End If

    ' Get the Command ID
    cmd_id = WCP_Command_GetRowId(Me.gd_commands, idx_row)

    If cmd_id > 0 Then
      If Not ShowLogger(cmd_id, Me.MdiParent) Then
        str_provider = ""
        str_terminal = ""
        WCP_Command_GetRowProviderTerminal(Me.gd_commands, idx_row, str_provider, str_terminal)
        str_created = WCP_Command_GetRowCreatedDate(Me.gd_commands, idx_row)

        'previous_top_most = LoggerGetTopMost()
        'LoggerSetTopMost(False)
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(151), ENUM_MB_TYPE.MB_TYPE_ERROR, , , _
                        str_provider, str_terminal, str_created)
        'LoggerSetTopMost(previous_top_most)
      End If
    Else
      'previous_top_most = LoggerGetTopMost()
      'LoggerSetTopMost(False)
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(153), ENUM_MB_TYPE.MB_TYPE_WARNING)
      'LoggerSetTopMost(previous_top_most)
    End If
    'Me.gd_commands.Focus()

  End Sub 'GUI_EditSelectedItem

#End Region ' Private Functions

#Region " Events "

#Region " Timer Events "

  ' PURPOSE: Update command list of the grid, every interval ms of the tmr_refresh.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub tmr_refresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_refresh.Tick

    Me.gd_commands.Redraw = False

    '' update commands_list
    UpdateGrid()

    Me.gd_commands.Redraw = True

  End Sub ' tmr_refresh_Tick

#End Region ' Timer Events

#Region " Button Events "

  ' PURPOSE: Event handler routine for the Restart Button.
  '          After validating with user, send Restart Command to the selected terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_restart_ClickEvent() Handles btn_restart.ClickEvent

    'Dim previous_top_most As Boolean

    'previous_top_most = LoggerGetTopMost()
    'LoggerSetTopMost(False)

    ' 152 "The selected terminals will be restarted.\nDo you wish to continue?"
    If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(152), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, _
                  ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
      'InsertCommand(ENUM_WCP_COMMANDS.WCP_COMMAND_SUBS_MACHINE_PARTIAL_CREDIT)
      InsertCommand(WSI.Common.WCP_CommandCode.Restart)
    End If
    'LoggerSetTopMost(previous_top_most)

  End Sub ' btn_restart_ClickEvent

  ' PURPOSE: Event handler routine for the Logger Button.
  '          Send Logger Command to the selected terminals.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_logger_ClickEvent() Handles btn_logger.ClickEvent

    InsertCommand(WSI.Common.WCP_CommandCode.GetLogger)
  End Sub ' btn_logger_ClickEvent

  ' PURPOSE: Event handler routine for the Clear Button.
  '          Open Log Viewer for the selected command if it is Logger type command and in Ok status.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_clear_ClickEvent() Handles btn_clear.ClickEvent

    Call ClearAllData()

  End Sub ' btn_clear_ClickEvent

  ' PURPOSE: Event handler routine for the Select Button.
  '          Open Log Viewer for the selected command if it is Logger type command and in Ok status.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_select_ClickEvent() Handles btn_select.ClickEvent

    If (Me.gd_commands.NumRows > 0) Then
      Call GUI_EditSelectedItem()
    End If

  End Sub ' btn_select_ClickEvent

  ' PURPOSE: Close form.
  '
  '    - INPUT:
  '           - None
  '
  '    - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent

    Me.Close()

  End Sub ' btn_exit_ClickEvent

#End Region ' Button Events

#Region " DataGrid Events "


  ' PURPOSE: Event handler routine for when a element in the grid is selected.
  '          Open Log Viewer for the selected command if it is Logger type command and in Ok status.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub gd_commands_DataSelectedEvent() Handles gd_commands.DataSelectedEvent

    Call btn_select_ClickEvent()

  End Sub ' gd_commands_DataSelectedEvent

  ' PURPOSE: When leaving the DataGrid, set TopMost Property to false for the LogViewer Form.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  'Private Sub gd_commands_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles gd_commands.Leave
  '  LoggerSetTopMost(False)
  'End Sub ' gd_commands_Leave

  ' PURPOSE :
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub gd_commands_RowSelectedEvent(ByVal SelectedRow As Integer) Handles gd_commands.RowSelectedEvent

    Dim cmd_id As UInt64
    Dim enable_view_logger As Boolean

    enable_view_logger = False

    ' Get the Command ID
    cmd_id = WCP_Command_GetRowId(Me.gd_commands, SelectedRow)

    If cmd_id > 0 Then
      enable_view_logger = True
    End If

    Me.btn_select.Enabled = enable_view_logger

  End Sub

#End Region ' DataGrid Events

#Region " Sql Server Events "

  ' PURPOSE: Event handler routine called when a row is inserted/updated.
  '          Used to get the IDENTITY value for a new inserted row.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Shared Sub command_adapter_OnRowUpdated(ByVal sender As Object, ByVal args As SqlRowUpdatedEventArgs)
    Dim new_id As Integer

    If args.StatementType = StatementType.Insert Then
      new_id = GetSqlIdentity(args.Command)
      If new_id <> 0 Then
        args.Row("CMD_ID") = new_id
      End If
    End If

  End Sub ' command_adapter_OnRowUpdated

  ' PURPOSE: Get the IDENTITY value for the last inserted row.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Command As SqlCommand
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Integer: New ID

  Private Shared Function GetSqlIdentity(ByVal Command As SqlCommand) As Integer
    Dim new_id As Integer
    Dim value As Object
    Dim db_identity_cmd As SqlCommand

    new_id = 0

    ' Don't use Scope_Identity() for Sql Server, it will not work because the change in command object
    ' changes the scope from the original command.
    db_identity_cmd = New SqlCommand("SELECT @@IDENTITY")
    db_identity_cmd.Connection = Command.Connection
    db_identity_cmd.Transaction = Command.Transaction

    ' Retrieve the identity value and store it in the CategoryID column.
    value = db_identity_cmd.ExecuteScalar()
    If value IsNot Nothing And value IsNot DBNull.Value Then
      new_id = CInt(value)
    End If

    Return new_id
  End Function

#End Region ' Sql Server Events

#Region " Form Events "

  'Private Sub frm_command_send_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
  '  HideLogger()
  'End Sub ' frm_command_send_FormClosed

  'Private Sub frm_command_send_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
  '  LoggerSetTopMost(True)
  '  LoggerSetTopMost(False)
  'End Sub ' frm_command_send_Activated

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      mdl_commands.TerminalReportType = ReportType.Provider + ReportType.Location
    Else
      mdl_commands.TerminalReportType = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region ' Form Events

#End Region ' Events

  Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

    'InsertCommand(WSI.Common.WCP_CommandCode.SubsMachinePartialCredit)

  End Sub

  Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

    'Dim _parameters As String
    ''1 = sorteo
    ''2 = premio
    '_parameters = "<type>1</type><text>Nuevo sorteo especial 900</text><logo>http://www.google.es</logo>"

    'InsertCommand(WSI.Common.WCP_CommandCode.GameGateway, _parameters)
  End Sub
End Class ' frm_command_send

Class AuditCommand

#Region " Members "

  Dim m_terminal_id As Int32
  Dim m_terminal_name As String
  Dim m_command_type As WSI.Common.WCP_CommandCode
  Dim m_auditor As CLASS_AUDITOR_DATA

#End Region ' Members

#Region " Public Functions "

  ' PURPOSE: Construcor, inits CLASS_AUDITOR_DATA
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Sub New()
    m_auditor = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    m_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(4991), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4991))
  End Sub

  ' PURPOSE: Adds a terminal field to auditor data
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalId : Id 
  '           - CommandType: Restart/Log
  '           - Trx: SqlTransaction
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub AuditTerminalCommand(ByVal TerminalId As Int32, ByVal CommandType As WSI.Common.WCP_CommandCode, ByRef Trx As SqlTransaction)
    m_auditor.SetField(0, Nothing, GetAuditorDataDescription(TerminalId, CommandType, Trx), CLASS_AUDITOR_DATA.ENUM_FIELD_TYPE.FIELD_NO_DATA)
  End Sub

  ' PURPOSE: Notifies to auditor data
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub AuditNotify()
    m_auditor.Notify(CurrentUser.GuiId, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)
  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Returns the auditorData Description of the event
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalId : Id 
  '           - CommandType: Restart/Log
  '           - Trx: SqlTransaction
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetAuditorDataDescription(ByVal TerminalId As Int32, ByVal CommandType As WSI.Common.WCP_CommandCode, ByRef Trx As SqlTransaction) As String
    Dim _terminal_info As WSI.Common.Terminal.TerminalInfo
    Dim _description As String

    _terminal_info = New WSI.Common.Terminal.TerminalInfo()

    If WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalId, _terminal_info, Trx) Then
      _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4990, GetCommandName(CommandType), _terminal_info.Name)
    Else
      _description = ""
    End If

    Return _description
  End Function

  ' PURPOSE: Returns the nls of the command type
  '
  '  PARAMS:
  '     - INPUT:
  '           - CommandType: Restart/Log
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - string: The NLS of the command type
  Private Function GetCommandName(ByVal CommandType As WSI.Common.WCP_CommandCode) As String
    Dim _command_name As String

    Select Case CommandType
      Case WSI.Common.WCP_CommandCode.GetLogger
        _command_name = GLB_NLS_GUI_CONTROLS.GetString(415)
      Case WSI.Common.WCP_CommandCode.Restart
        _command_name = GLB_NLS_GUI_CONTROLS.GetString(416)
      Case Else
        _command_name = GLB_NLS_GUI_CONTROLS.GetString(408)
    End Select

    Return _command_name
  End Function

#End Region ' Private Functions

End Class
