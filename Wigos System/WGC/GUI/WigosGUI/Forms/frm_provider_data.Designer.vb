<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_provider_data
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dg_providers = New GUI_Controls.uc_grid
    Me.lbl_nota = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.AutoSize = True
    Me.panel_data.Controls.Add(Me.lbl_nota)
    Me.panel_data.Controls.Add(Me.dg_providers)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(333, 354)
    '
    'dg_providers
    '
    Me.dg_providers.CurrentCol = -1
    Me.dg_providers.CurrentRow = -1
    Me.dg_providers.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.dg_providers.Location = New System.Drawing.Point(0, 46)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.PanelRightVisible = False
    Me.dg_providers.Redraw = True
    Me.dg_providers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_providers.Size = New System.Drawing.Size(333, 308)
    Me.dg_providers.Sortable = False
    Me.dg_providers.SortAscending = True
    Me.dg_providers.SortByCol = 0
    Me.dg_providers.TabIndex = 1
    Me.dg_providers.ToolTipped = True
    Me.dg_providers.TopRow = -2
    '
    'lbl_nota
    '
    Me.lbl_nota.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_nota.Location = New System.Drawing.Point(3, 0)
    Me.lbl_nota.Name = "lbl_nota"
    Me.lbl_nota.Size = New System.Drawing.Size(327, 43)
    Me.lbl_nota.TabIndex = 4
    Me.lbl_nota.Text = "xNota"
    '
    'frm_provider_data
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(449, 362)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_provider_data"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_provider_data"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents dg_providers As GUI_Controls.uc_grid
  Friend WithEvents lbl_nota As System.Windows.Forms.Label
End Class
