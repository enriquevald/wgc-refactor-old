'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotion_edit
' DESCRIPTION:   New or Edit Promotion
' AUTHOR:        Raul Cervera 
' CREATION DATE: 10-MAY-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-MAY-2010  RCI    Initial version.
' 19-JAN-2012  RCI    Add Provider filter support in spent promotions.
' 23-MAY-2012  MPO    Added icon and image controls.
' 01-AUG-2012  MPO    Added new logic credit type.
' 20-AUG-2012  JAB    Added Promotion Global Limit. 
' 29-AUG-2012  MPO    Check one time the limits. (GUI_IsScreenDataOk)
' 06-SEP-2012  JAB    Disable the expiration groupbox (gb_expiration) when the credit type is "point" or "redemable".
' 25-SEP-2012  QMP    Added Promotion Category.
' 12-APR-2013  DHA    Added Expiration Limit.
' 17-APR-2013  DHA    Fixed Bug #723: Field 'Expiration Limit' only must validate when credits are non-redeemable, and its associated check is checked.
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty .
' 11-JUN-2013  RRR    Copy the data from a selected promotion to a new one.
' 19-JUN-2013  RRR    Fixed Bug #878: Error adding images to copied promotions.
' 07-JUL-2013  RBG    Change provider list for elements list.
' 29-AUG-2013  AMF    New field: pm_restricted_to_terminal_list.
' 12-SEP-2013  DHA    Added Cash Desk Draw promotions.
' 25-SEP-2013  JAB    Added new column in promotion limit tab,  "Disponible".
' 30-SEP-2013  JAB    Fixed Bug #235: Error when point promotion mode has selected (incorrect values).
' 30-SEP-2013  JAB    Column "Disponible" don't show negative values. (If negative then zero).
' 08-OCT-2013  XMS    WIG-267 Update field "Disponible".
' 08-OCT-2013  FBA    Added new filters by age range
' 14-OCT-2013  ANG    Fix bug WIG-64
' 25-OCT-2013  JML    Display 'redemption table' for promotions which belongs to 
' 05-NOV-2013  RMS    Added Description text and Applicable on PromoBOX checkbox
' 06-NOV-2013  CCG    Fixed Bug WIG-383: Error message checking required flags
' 07-NOV-2013  DLL    Fixed Bug WIG-391: Permit save negative number
' 07-NOV-2013  DLL    Refactor function to check required flags
' 18-NOV-2013  QMP    Hide small image (icon) and validate large image
' 19-NOV-2013  JCA    Fixed Bug WIG-416: Error when leaving blank the limit
' 05-DEC-2013  LJM    In case of external promotions, the tab restrictions must be enabled
' 17-FEB-2014  DHA    Added promotions EXTERNAL_NON_REDEEM_RESTRICTED_XX. These promotions types can be Enabled/Disabled and selected none/some/all terminals
' 07-APR-2014  JBP    Added only VIP property.
' 12-JUN-2014  XCD    Show Dialog message with comments if there are changes
' 16-JUN-2014  JCO    Enable terminal restrictions for all NR system promotions.
' 20-JAN-2015  ANM    Visible again small image (icon) due LCD displays (18-NOV-2013 QMP change)
' 26-ENE-2015  ANM    Increase token number's length to 5 
' 09-SEP-2015  AVZ    Backlog Item #3696: Added new filter by created account date
' 01-OCT-2015  AVZ    Backlog Item #4753: Edit filter by created account date - New requirements
' 04-OCT-2015  DLL    Bug 6144: Incorrect NLS for played promotion
' 04-NOV-2015  GMV    PBI 5836 - Garcia River - Discernir promos visibles en PromoBOX y Display Touch
' 07-JAN-2016  DLL    Hide Promotions by played. Not implemented
' 13-JAN-2015  JML    Product Backlog Item 6559: Cashier draw (RE in kind)
' 15-FEB-2016  RAB    Bug 8714:Promociones: campos no informados en la edici�n de la promoci�n
' 24-FEB-2016  RAB    Product Backlog Item 8257:Multiple buckets: Canje de buckets NR
' 02-MAR-2017  FOS    Bug 25235: Unexpected error
' 08-JUN-2017  EOR    Bug 27935: WIGOS-2709 GUI - Promotions page doesn't display complete
' 25-AUG-2017  RGR    Bug 29460:WIGOS-3770 The user is able to enable "Candado" in the no redimible promotions in a Tito environment
' 21-JUN-2018  DMT    PBI 33265 WIGOS13079 Editing promotions - incorrect behavior
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common

Public Class frm_promotion_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_PROMO_NAME As Integer = 20
  Private Const LEN_PROMO_DESC As Integer = 50
  Private Const LEN_FLAG_REQUIRED_DIGITS As Integer = 6
  Private Const LEN_FLAG_AWARDED_DIGITS As Integer = 2
  Private Const MAX_FLAGS_AWARDED As Integer = 10

  ' SELECT FLAG GRID
  Private Const GRID_FLAG_IDX As Integer = 0
  Private Const GRID_FLAG_CHECKED As Integer = 1
  Private Const GRID_FLAG_COLOR As Integer = 2
  Private Const GRID_FLAG_NAME As Integer = 3
  Private Const GRID_FLAG_COUNT As Integer = 4
  Private Const GRID_FLAG_ID_COLOR As Integer = 5

  Private Const GRID_SELECT_FLAG_COLUMNS As Integer = 6
  Private Const GRID_SELECT_FLAG_HEADER_ROWS As Integer = 1
  Private Const FORM_DB_MIN_VERSION As Short = 158

  ' TAB INDEXES
  Private Const PROMOTIONS_CASHIN_TABINDEX As Integer = 0
  Private Const PROMOTIONS_SPENT_TABINDEX As Integer = 1
  Private Const PROMOTIONS_TOKENS_TABINDEX As Integer = 2
  Private Const PROMOTIONS_FLAGS_TABINDEX As Integer = 3
  Private Const PROMOTIONS_RESTRICTED_TABINDEX As Integer = 4

#End Region ' Constants

#Region " Enums "

  Private Enum GRID_DATA_STATUS
    OK = 1
    VALUE_NOT_VALID = 2
    NO_ONE_CHECKED = 3
    MAX_FLAGS_EXCEEDED = 4
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_promotion_copied_id As Long
  Private m_promotion_copy_operation As Boolean
  Private m_input_promo_name As String
  Private DeleteOperation As Boolean
  'Private m_closing_time As Integer

  ' RCI 17-NOV-2010: Used to show label indicating permissions not applicable in Mobile Bank.
  Private m_mb_auto_promotion As Boolean

  ' BBA 28-AUG-2013: New fields Daily, Monthly and global Limit
  Private m_promotion_id As Long
  Private m_consumed_info_loaded As Boolean = False
  Private m_uc_terminals_played_group_filter_init_Y_pos As Integer = 0
  Private m_uc_terminals_group_filter_init_Y_pos As Integer = 0
  Private m_uc_terminals_group_filter_restricted_init_Y_pos As Integer = 0

  Private m_is_cash_draw_2_enabled As Boolean = False
  'Private m_is_pyramidal_dist_enabled As Boolean = False

#End Region ' Members

#Region "Structures"

  Private Structure INFO_FLAG
    Dim id As String
    Dim count As Integer
    Dim name As String
    Dim color As Integer
  End Structure

#End Region  ' Structures

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTION_EDIT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim closing_time As Integer
    Dim mb_auto_promotion As Integer

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    'Add any initialization after the MyBase.GUI_InitControls() call    
    closing_time = GetDefaultClosingTime()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(257)

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    ' Enabled control
    gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)
    opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Special Permission
    gb_special_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(400)
    chk_special_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(401)
    opt_special_permission_a.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(402)
    opt_special_permission_b.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(403)
    lbl_not_applicable_mb1.Visible = False
    lbl_not_applicable_mb1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(428)

    mb_auto_promotion = 0
    Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("MobileBank", "AutomaticallyAssignPromotion"), mb_auto_promotion)
    m_mb_auto_promotion = (mb_auto_promotion = 1)

    ' Name
    ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Type
    gb_credit_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
    opt_credit_non_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(338)
    opt_credit_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    opt_point.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)

    ' JBP 07-APR-2014: VIP
    Me.chk_only_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4804)          ' 4804 "Exclusivo para clientes VIP" 

    ' LA 23-FEB-2016
    Me.chk_apply_tax.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7143)          '7143  "Aplicar impuesto" 

    ' Promobox tab
    tab_promobox.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6928) ' 6928 "Options"
    chk_visible_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2868) '2868 "Mostrar en monitor superior"
    lbl_apply_on.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2869) '2869 "Allow request"
    chk_award_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(722) ' 722 "PromoBOX"
    chk_award_on_InTouch.Text = GeneralParam.GetString("DisplayTouch", "ProductName")

    ef_text_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2870)   '2870 "Display text"
    ef_text_on_PromoBOX.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_DESC)
    ef_text_on_PromoBOX.TextVisible = True
    lbl_no_text_on_promobox_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2872)   '2872 "Sin descripci�n s�lo se mostrar� la cantidad"
    Call UpdatePromoBOXTabState()

    ' Images
    lbl_icon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(719)
    lbl_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(687)
    Call img_icon.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)
    Call img_image.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)

    'DMT 21-JUN-2018
    ' PromoBOX Image
    img_image.Enabled = chk_visible_on_PromoBOX.Checked

    ' uc_chedule control
    uc_promo_schedule.SetDefaults()

    ' Filters
    tab_filters.TabPages(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(213)
    tab_filters.TabPages(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(216)
    ' AVZ 01-OCT-2015: Tab Control initialization
    tab_filters.TabPages(2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6720)
    tab_filters.TabPages(3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(404)
    tab_filters.TabPages(4).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(408)
    tab_flags_required.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1397) 'By flag
    chk_by_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)
    opt_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)
    chk_by_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_birth_date_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(217)
    opt_birth_month_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(218)
    'FBA 08-OCT-2013 new age range filter
    chk_by_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2730)
    opt_birth_include_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702)
    opt_birth_exclude_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703)
    ef_age_to1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2789)
    ef_age_to1.TextVisible = True
    ef_age_to2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2790)
    ef_age_to2.TextVisible = True
    ef_age_from1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_age_from2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_age_to1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_age_to2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' AVZ 01-OCT-2015: Controls initialization
    chk_by_created_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_created_account_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6721)
    opt_created_account_working_day_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6722)
    opt_created_account_working_day_plus.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6723)
    ef_created_account_working_day_plus.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6724)
    ef_created_account_working_day_plus.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    ef_created_account_working_day_plus.TextVisible = True

    opt_created_account_anniversary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6725)
    opt_created_account_anniversary_whole_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6726)
    opt_created_account_anniversary_day_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6727)
    chk_by_anniversary_range_of_years.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6728)
    ef_created_account_anniversary_year_from.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6729)
    ef_created_account_anniversary_year_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    ef_created_account_anniversary_year_from.TextVisible = True
    ef_created_account_anniversary_year_to.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6730)
    ef_created_account_anniversary_year_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    ef_created_account_anniversary_year_to.TextVisible = True

    chk_by_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(407)
    chk_level_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If
    chk_by_freq.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    ef_freq_last_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
    ef_freq_last_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_freq_min_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
    ef_freq_min_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_freq_min_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(411)
    ef_freq_min_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    chk_by_flag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    btn_Accounts_Afected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2834)

    ' Expiration
    'gb_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
    tp_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
    opt_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(219)
    opt_expiration_hours.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(219)
    ef_expiration_days.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(220, GUI_FormatDate(DateTime.Today.AddHours(closing_time), ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM))
    ef_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_expiration_hours.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(221)
    ef_expiration_hours.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' JML 25-OCT-2013 Add label for promotions from redemption table
    lbl_belongs_to_points_to_credit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2716) '' 2716 "Belongs to redemption table"

    ' DHA 12-APR-2013: Setting the visibilty of the controls
    Me.lbl_credits_non_redeemable.Enabled = False
    Me.lbl_credits_non_redeemable.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1901)

    Me.chk_expiration_limit.Enabled = False
    Me.chk_expiration_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1902)

    Me.dtp_expiration_limit.Enabled = False
    Me.dtp_expiration_limit.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_expiration_limit.Value = DateTime.MinValue

    ' Ticket Footer
    tp_ticketfooter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1284)

    ' Cash in
    tab_chash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(417)
    ' Minimum cash in
    ef_min_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(284)
    ef_min_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Additional cash in
    ef_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(226)
    ef_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Spent
    tab_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(418)
    ' Minimum spent
    ef_min_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(420)
    ef_min_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Additional spent
    ef_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(226)
    ef_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Token rewards
    tab_tokens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(419)
    ef_num_tokens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(228)
    ef_num_tokens.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
    ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
    ef_token_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
    ef_token_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Flags awarded
    tab_flags_awarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
    chk_flags_awarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)

    ' SDS 22-10-2015 
    ' Played
    tab_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6826)
    ' DLL 07-01-2016 Hide tab played. Not implemented
    tab_played.Parent = Nothing
    ' Minimum played
    ef_min_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6827)
    ef_min_played.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
    ef_min_played_reward.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ' Additional played
    ef_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6829)
    ef_played.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
    ef_played_reward.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)

    ' AMF 02-SEP-2013
    ' Restriction
    tab_restricted.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2561) 'Restricci�n

    'CUSTOMER LIMIT
    tab_by_customer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(523)
    ' Daily limit
    ef_daily_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)

    ' Monthly limit
    ef_monthly_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(235)

    ' Journey limit
    chk_jorney_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8745)

    'PROMOTION LIMIT
    tab_by_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(524)

    ' BBA 29-AUG-2013: New fields Daily, Monthly and global Limit
    ' Daily limit
    ef_promotion_limit_day.Text = GLB_NLS_GUI_INVOICING.GetString(401)
    ' Monthly limit
    ef_promotion_limit_month.Text = GLB_NLS_GUI_INVOICING.GetString(461)

    lbl_promo_daily_limit.Font = lbl_promotion_daily_limit.Font
    lbl_promo_daily_limit.BackColor = lbl_promotion_daily_limit.BackColor
    lbl_promo_daily_limit.ForeColor = lbl_promotion_daily_limit.LabelForeColor
    lbl_promotion_daily_limit.Visible = False

    lbl_promo_month_limit.Font = lbl_promotion_monthly_limit.Font
    lbl_promo_month_limit.BackColor = lbl_promotion_monthly_limit.BackColor
    lbl_promo_month_limit.ForeColor = lbl_promotion_monthly_limit.LabelForeColor
    lbl_promotion_monthly_limit.Visible = False

    lbl_promo_global_limit.Font = lbl_promotion_global_limit.Font
    lbl_promo_global_limit.BackColor = lbl_promotion_global_limit.BackColor
    lbl_promo_global_limit.ForeColor = lbl_promotion_global_limit.LabelForeColor
    lbl_promotion_global_limit.Visible = False



    lbl_promo_daily_limit.Visible = False
    lbl_promo_month_limit.Visible = False
    lbl_promo_global_limit.Visible = False

    ' JAB 20-AUG-2012 Added Promotion Global Limit
    ' Global limit
    ef_promotion_limit_global.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2554)
    lbl_limit_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(288)
    lbl_consumed_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2555)
    lbl_available_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(358)

    ' Won Lock
    gb_won_lock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(531) '531 Lock
    ef_won_lock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532) '532 Quantity
    ef_won_lock.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)

    ' Test
    gb_test.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(232)
    ef_test_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
    ef_test_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_test_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(421)
    ef_test_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_test_num_tokens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(228)
    ef_test_num_tokens.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' RCI 16-NOV-2010: BUG Text Visible
    ef_freq_last_days.TextVisible = True
    ef_freq_min_days.TextVisible = True
    ef_freq_min_cash_in.TextVisible = True

    ef_daily_limit.TextVisible = True
    ef_monthly_limit.TextVisible = True
    ef_promotion_limit_day.TextVisible = True
    ef_promotion_limit_month.TextVisible = True
    ' JAB 20-AUG-2012 Added Promotion Global Limit
    ef_promotion_limit_global.TextVisible = True

    ef_promotion_limit_day.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_limit_month.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_limit_global.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' BBA 28-AUG-2013: New fields Daily, Monthly and global Limit
    ef_promotion_consumed_day.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_consumed_month.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_consumed_global.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_consumed_day.TabStop = False
    ef_promotion_consumed_month.TabStop = False
    ef_promotion_consumed_global.TabStop = False

    ef_promotion_available_day.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_available_month.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_available_global.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_promotion_available_day.TabStop = False
    ef_promotion_available_month.TabStop = False
    ef_promotion_available_global.TabStop = False

    ef_min_cash_in.TextVisible = True
    ef_min_cash_in_reward.TextVisible = True
    ef_cash_in.TextVisible = True
    ef_cash_in_reward.TextVisible = True
    ef_min_spent.TextVisible = True
    ef_min_spent_reward.TextVisible = True
    ef_spent.TextVisible = True
    ef_spent_reward.TextVisible = True
    ef_num_tokens.TextVisible = True
    ef_token_reward.TextVisible = True
    ef_token_name.TextVisible = True

    ef_min_played.TextVisible = True
    ef_min_played_reward.TextVisible = True
    ef_played.TextVisible = True
    ef_played_reward.TextVisible = True

    Me.lbl_icon_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "200", "150")
    Me.lbl_image_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "1280", "1024")

    Me.img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

    ' Providers
    Me.uc_terminals_group_filter.SetGroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(477)
    Me.uc_terminals_group_filter_restricted.SetGroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(477)

    Me.uc_terminals_played_group_filter.SetGroupBoxText = GLB_NLS_GUI_CONTROLS.GetString(477)

    m_uc_terminals_played_group_filter_init_Y_pos = uc_terminals_played_group_filter.Location.Y
    m_uc_terminals_group_filter_init_Y_pos = uc_terminals_group_filter.Location.Y
    m_uc_terminals_group_filter_restricted_init_Y_pos = uc_terminals_group_filter_restricted.Location.Y

    ' Promotion Categories
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
    Me.cmb_categories.Add(PromotionCategories.Categories())

    'Me.img_icon.Enabled = Me.chk_visible_on_PromoBOX.Checked
    'Me.img_image.Enabled = Me.chk_visible_on_PromoBOX.Checked

    Me.Timer1.Interval = 1000
    Me.Timer1.Start()

    ' Promotion Flags
    Call InitializeRequiredFlags()
    Call InitializeAwardedFlags()

    If chk_by_flag.Checked Then
      btn_Accounts_Afected.Enabled = True
    Else
      btn_Accounts_Afected.Enabled = False
    End If

    'EOR 08-JUN-2017
    If Screen.PrimaryScreen.Bounds.Height > 599 And Screen.PrimaryScreen.Bounds.Height < 900 Then
      Me.Size = New Size(1110, 720)
    End If

    If Screen.PrimaryScreen.Bounds.Height >= 900 Then
      Me.Size = New Size(1110, 840)
    End If

    ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
    'Me.m_is_cash_draw_2_enabled = WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False)
    'Me.m_is_pyramidal_dist_enabled = WSI.Common.GeneralParam.GetBoolean("Features", "PyramidalDistributionReward", False)

    'If Me.m_is_cash_draw_2_enabled Then
    '  ' Initialize Promogames combo
    '  Call InitAwardWithGameCombobox()

    '  Me.chk_award_with_game.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8613)
    'Else
    Me.chk_award_with_game.Visible = False
    Me.cmb_award_with_game.Visible = False
    'End If

    ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
    'Call Me.InitUcPyramization()

    'QMP 18-NOV-2013: Hide small image (icon)
    'ANM 20-JAN-2015: Visible again due LCD displays
    'lbl_icon.Visible = False
    'lbl_icon_optimun_size.Visible = False
    'img_icon.Visible = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String
    Dim _nls_param2 As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT
    Dim _promo_item As CLASS_PROMOTION
    Dim _freq_last_days As Integer
    Dim _freq_min_days As Integer
    Dim _control_limit As UserControl
    Dim _selected_index As Integer
    Dim _legal_age As Integer
    Dim _date_from As Integer
    Dim _date_to As Integer

    _control_limit = Nothing
    _selected_index = 0

    _promo_item = DbReadObject


    ' Name
    If ef_promo_name.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_promo_name.Focus()

      Return False
    End If

    ' Special permission
    If chk_special_permission.Checked Then
      If opt_special_permission_a.Checked = False And opt_special_permission_b.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call opt_special_permission_a.Focus()

        Return False
      End If
    End If

    ' Expiration dates range
    If uc_promo_schedule.DateFrom > uc_promo_schedule.DateTo Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_promo_schedule.SetFocus(1)

      Return False
    End If

    ' No Week days selected
    If uc_promo_schedule.Weekday = uc_promo_schedule.NoWeekDaysSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_promo_schedule.SetFocus(4)

      Return False
    End If

    ' Gender filter
    If chk_by_gender.Checked Then
      If opt_gender_male.Checked = False And opt_gender_female.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 0
        Call opt_gender_male.Focus()

        Return False
      End If
    End If

    ' Birth Date filter
    If chk_by_birth_date.Checked Then
      If opt_birth_date_only.Checked = False And opt_birth_month_only.Checked = False And opt_birth_include_age.Checked = False And opt_birth_exclude_age.Checked = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 1
        Call opt_birth_date_only.Focus()

        Return False
      End If

      If opt_birth_date_only.Checked Then
        ' Daily limit mandatory for birthday filter
        If GUI_ParseCurrency(ef_daily_limit.Value) <= 0 _
        And GUI_ParseCurrency(ef_promotion_limit_day.Value) <= 0 Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          tab_filters.SelectedIndex = 1
          tab_limit.SelectedIndex = 0
          Call ef_daily_limit.Focus()

          Return False
        End If

        ' All weekdays must be marked if birthday-day only option is selected
        If uc_promo_schedule.Weekday <> uc_promo_schedule.AllWeekDaysSelected() Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(120), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          tab_filters.SelectedIndex = 1
          Call uc_promo_schedule.SetFocus(5)

          Return False
        End If

      ElseIf opt_birth_month_only.Checked Then
        ' Monthly limit mandatory for birthday filter
        If GUI_ParseCurrency(ef_monthly_limit.Value) <= 0 _
        And GUI_ParseCurrency(Me.ef_promotion_limit_month.Value) <= 0 Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(235)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          tab_filters.SelectedIndex = 1
          tab_limit.SelectedIndex = 0
          Call ef_monthly_limit.Focus()

          Return False
        End If
      End If
    End If

    'FBA 08-OCT-2013 new age range filter
    '255 is the maximum allowed value since it is saved in a byte, it must also be greater than 0
    Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier", "LegalAge"), _legal_age)
    If chk_by_age.Checked Then
      If opt_birth_exclude_age.Checked Then
        Integer.TryParse(ef_age_from2.Value, _date_from)
        Integer.TryParse(ef_age_to2.Value, _date_to)
        If ef_age_from2.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        ElseIf Not ef_age_from2.ValidateFormat Or ef_age_from2.Value > 255 Or ef_age_from2.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        End If
        If ef_age_to2.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to2.Focus()
          Return False
        ElseIf Not ef_age_to2.ValidateFormat Or ef_age_to2.Value > 255 Or ef_age_to2.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to2.Focus()
          Return False
        End If
        If _date_from > _date_to Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        End If
        If _date_from < _legal_age Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), _legal_age)
          tab_filters.SelectedIndex = 1
          ef_age_from2.Focus()
          Return False
        End If
      ElseIf opt_birth_include_age.Checked Then
        Integer.TryParse(ef_age_from1.Value, _date_from)
        Integer.TryParse(ef_age_to1.Value, _date_to)
        If ef_age_from1.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        ElseIf Not ef_age_from1.ValidateFormat Or ef_age_from1.Value > 255 Or ef_age_from1.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        End If
        If ef_age_to1.TextValue = "" Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to1.Focus()
          Return False
        ElseIf Not ef_age_to1.ValidateFormat Or ef_age_to1.Value > 255 Or ef_age_to1.Value = 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707))
          tab_filters.SelectedIndex = 1
          ef_age_to1.Focus()
          Return False
        End If
        If _date_from > _date_to Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        End If
        If _date_from < _legal_age Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), _legal_age)
          tab_filters.SelectedIndex = 1
          ef_age_from1.Focus()
          Return False
        End If
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1321), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2730), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702) & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703))
        Return False
      End If

    End If

    ' AVZ 01-OCT-2015: Validation
    If chk_by_created_account.Checked Then

      If Not opt_created_account_date.Checked And Not opt_created_account_anniversary.Checked Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call opt_created_account_date.Focus()

        Return False
      End If

      If opt_created_account_date.Checked And Not opt_created_account_working_day_only.Checked And Not opt_created_account_working_day_plus.Checked Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call opt_created_account_working_day_only.Focus()

        Return False
      End If

      If opt_created_account_anniversary.Checked And Not opt_created_account_anniversary_whole_month.Checked And Not opt_created_account_anniversary_day_month.Checked Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call opt_created_account_anniversary_whole_month.Focus()

        Return False
      End If

      If opt_created_account_date.Checked And opt_created_account_working_day_plus.Checked Then
        If ef_created_account_working_day_plus.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6731))
          tab_filters.SelectedIndex = 2
          Call ef_created_account_working_day_plus.Focus()
          Return False
        ElseIf ef_created_account_working_day_plus.Value < 0 Or ef_created_account_working_day_plus.Value > 99 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6731))
          tab_filters.SelectedIndex = 2
          ef_created_account_working_day_plus.Focus()
          Return False
        End If
      End If

      If opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked Then
        If ef_created_account_anniversary_year_from.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732))
          tab_filters.SelectedIndex = 2
          Call ef_created_account_anniversary_year_from.Focus()
          Return False
        ElseIf ef_created_account_anniversary_year_from.Value = 0 Or ef_created_account_anniversary_year_from.Value > 99 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732))
          tab_filters.SelectedIndex = 2
          ef_created_account_anniversary_year_from.Focus()
          Return False
        End If

        If ef_created_account_anniversary_year_to.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          tab_filters.SelectedIndex = 2
          Call ef_created_account_anniversary_year_to.Focus()
          Return False
        ElseIf ef_created_account_anniversary_year_to.Value = 0 Or ef_created_account_anniversary_year_to.Value > 99 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          tab_filters.SelectedIndex = 2
          ef_created_account_anniversary_year_to.Focus()
          Return False
        End If

        If Integer.Parse(ef_created_account_anniversary_year_from.Value) > Integer.Parse(ef_created_account_anniversary_year_to.Value) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6782), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          tab_filters.SelectedIndex = 2
          ef_created_account_anniversary_year_to.Focus()
          Return False
        End If
      End If

    End If

    ' Level filter
    If chk_by_level.Checked Then
      If ((chk_level_anonymous.Checked = False) Or (opt_point.Checked = True)) _
         And chk_level_01.Checked = False _
         And chk_level_02.Checked = False _
         And chk_level_03.Checked = False _
         And chk_level_04.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 3
        Call chk_level_anonymous.Focus()

        Return False
      End If
    End If

    ' Frequency filter
    If chk_by_freq.Checked Then
      If ef_freq_last_days.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tab_filters.SelectedIndex = 4
        Call ef_freq_last_days.Focus()

        Return False
      End If
      If ef_freq_min_days.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tab_filters.SelectedIndex = 4
        Call ef_freq_min_days.Focus()

        Return False
      End If
      If ef_freq_min_cash_in.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(411)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tab_filters.SelectedIndex = 4
        Call ef_freq_min_cash_in.Focus()

        Return False
      End If

      _freq_last_days = GUI_ParseNumber(ef_freq_last_days.Value)
      _freq_min_days = GUI_ParseNumber(ef_freq_min_days.Value)
      If _freq_last_days < _freq_min_days Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
        _nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1, _nls_param2)
        tab_filters.SelectedIndex = 4
        Call ef_freq_last_days.Focus()

        Return False
      End If
    End If

    ' Expiration 
    'If gb_expiration.Enabled Then
    If pa_expiration.Enabled Then

      If opt_expiration_days.Checked = False And opt_expiration_hours.Checked = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call opt_expiration_days.Focus()

        Return False
      End If

      If opt_expiration_days.Checked Then
        If GUI_ParseNumber(ef_expiration_days.Value) <= 0 Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          tc_expirarion_ticketfooter.SelectedIndex = 0
          Call ef_expiration_days.Focus()

          Return False
        End If
      End If

      If opt_expiration_hours.Checked Then
        If GUI_ParseNumber(ef_expiration_hours.Value) <= 0 Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
          Call ef_expiration_hours.Focus()

          Return False
        End If
      End If

      ' DHA 12-APR-2013: Event of chk_expiration_limit changed checked
      If chk_expiration_limit.Checked And dtp_expiration_limit.Value < uc_promo_schedule.DateTo Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1904), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        dtp_expiration_limit.Focus()

        Return False
      End If
    End If

    ' Amounts
    ' Cash In: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_cash_in.Value) > 0 Or GUI_ParseCurrency(ef_cash_in_reward.Value) > 0 Then

      If GUI_ParseNumber(ef_cash_in.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 0
        Call ef_cash_in.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_cash_in_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 0
        Call ef_cash_in_reward.Focus()

        Return False
      End If
    End If

    ' Min Spent: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_min_spent.Value) > 0 Or GUI_ParseCurrency(ef_min_spent_reward.Value) > 0 Then

      If GUI_ParseNumber(ef_min_spent.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_min_spent.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_min_spent_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_min_spent_reward.Focus()

        Return False
      End If
    End If

    ' Spent: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_spent.Value) > 0 Or GUI_ParseCurrency(ef_spent_reward.Value) > 0 Then
      If GUI_ParseNumber(ef_spent.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_spent.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_spent_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_spent_reward.Focus()

        Return False
      End If
    End If

    ' Min played: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_min_played.Value) > 0 Or GUI_ParseCurrency(ef_min_played_reward.Value) > 0 Then

      If GUI_ParseNumber(ef_min_played.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 2
        Call ef_min_played.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_min_played_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 2
        Call ef_min_played_reward.Focus()

        Return False
      End If
    End If

    ' Played: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_played.Value) > 0 Or GUI_ParseCurrency(ef_played_reward.Value) > 0 Then
      If GUI_ParseNumber(ef_played.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 2
        Call ef_spent.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_played_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 2
        Call ef_played_reward.Focus()

        Return False
      End If
    End If

    ' Providers (if listed or not listed must have at least one selected)
    If Not Me.uc_terminals_group_filter.CheckAtLeastOneSelected() Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(172)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      tabPromotions.SelectedIndex = 1
      Call uc_terminals_group_filter.Focus()

      Return False
    End If

    ' Providers (if listed or not listed must have at least one selected)
    If Not Me.uc_terminals_played_group_filter.CheckAtLeastOneSelected() Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(172)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      tabPromotions.SelectedIndex = 2
      Call uc_terminals_played_group_filter.Focus()

      Return False
    End If

    ' AMF 29-AUG-2013
    If opt_credit_non_redeemable.Checked Then
      ' Restricted Providers (if listed or not listed must have at least one selected)
      If Not Me.uc_terminals_group_filter_restricted.CheckAtLeastOneSelected() Then
        _nls_param1 = GLB_NLS_GUI_CONTROLS.GetString(477)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tabPromotions.SelectedIndex = 4
        Call uc_terminals_group_filter_restricted.Focus()

        Return False
      End If
    End If

    ' Tokens: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_num_tokens.Value) > 0 Or GUI_ParseCurrency(ef_token_reward.Value) > 0 Or ef_token_name.Value <> "" Then

      If GUI_ParseNumber(ef_num_tokens.Value) <= 0 Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(228)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tabPromotions.SelectedIndex = 2
        Call ef_num_tokens.Focus()

        Return False
      End If


      'RXM 29 AGO 2012 added "Points" option to nls_param1

      If GUI_ParseCurrency(ef_token_reward.Value) <= 0 Then
        If opt_credit_redeemable.Checked Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ElseIf opt_credit_non_redeemable.Checked Then
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        Else
          _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205) ' Points
        End If
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tabPromotions.SelectedIndex = 2
        Call ef_token_reward.Focus()

        Return False
      End If

      If ef_token_name.Value = "" Then
        _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        tabPromotions.SelectedIndex = 2
        Call ef_token_name.Focus()

        Return False
      End If

    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Select Case _promo_item.Type
        Case WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_PRIZE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON, _
             WSI.Common.Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE, _
             WSI.Common.Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_02, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_03, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_04, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_05, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_06, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_07, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_08, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_09

          Return True

      End Select
    End If


    ' At least a reward amount must be specified: either cash_in, spent played or tokens.
    If GUI_ParseCurrency(ef_min_cash_in_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_cash_in_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_min_spent_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_spent_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_played_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_min_played_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_token_reward.Value) <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      tabPromotions.SelectedIndex = 0
      Call ef_min_cash_in_reward.Focus()

      Return False
    End If

    ' Daily limit mandatory if no minimum cash in is set
    If GUI_ParseCurrency(ef_min_cash_in.Value) <= 0 And _
       GUI_ParseCurrency(ef_min_cash_in_reward.Value) > 0 And _
       GUI_ParseCurrency(ef_daily_limit.Value) <= 0 And _
       GUI_ParseCurrency(Me.ef_promotion_limit_day.Value) <= 0 Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)

      If GUI_ParseCurrency(Me.ef_daily_limit.Value) <= 0 Then
        Call ef_daily_limit.Focus()
        tab_limit.SelectedIndex = 0
      ElseIf GUI_ParseCurrency(Me.ef_promotion_limit_day.Value) <= 0 Then
        Call ef_promotion_limit_day.Focus()
        tab_limit.SelectedIndex = 1
      End If

      Return False
    End If

    '
    'LIMITS
    '

    ' Check minimum rewards do not exceed limit GLOBAL
    If GUI_ParseCurrency(ef_promotion_limit_global.Value) > 0 Then
      If GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_token_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_played_reward.Value) Then

        _selected_index = 1
        _control_limit = ef_promotion_limit_global

      End If
    End If

    ' Check minimum rewards do not exceed monthly limit GLOBAL
    If GUI_ParseCurrency(ef_promotion_limit_month.Value) > 0 Then
      If GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_token_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_played_reward.Value) Then

        _selected_index = 1
        _control_limit = ef_promotion_limit_month

      End If
    End If

    ' Check minimum rewards do not exceed daily limit GLOBAL
    If GUI_ParseCurrency(ef_promotion_limit_day.Value) > 0 Then
      If GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_token_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_played_reward.Value) Then

        _selected_index = 1
        _control_limit = ef_promotion_limit_day

      End If
    End If

    ' Check minimum rewards do not exceed monthly limit
    If GUI_ParseCurrency(ef_monthly_limit.Value) > 0 Then
      If GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_token_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_played_reward.Value) Then

        _selected_index = 0
        _control_limit = ef_monthly_limit

      End If
    End If

    ' Check minimum rewards do not exceed daily limit
    If GUI_ParseCurrency(ef_daily_limit.Value) > 0 Then
      If GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_token_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_played_reward.Value) Then

        _selected_index = 0
        _control_limit = ef_daily_limit

      End If
    End If

    '
    'LIMITS
    '

    ' Check fields for Won Lock
    If chk_won_lock.Checked Then

      If ef_won_lock.Value = "" Then
        _nls_param1 = ef_won_lock.Text
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
        Call ef_won_lock.Focus()

        Return False
      End If

      ' At least a reward amount must be specified
      If GUI_ParseCurrency(ef_min_cash_in_reward.Value) <= 0 And GUI_ParseCurrency(ef_cash_in_reward.Value) <= 0 And GUI_ParseCurrency(ef_token_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 0
        Call ef_min_cash_in_reward.Focus()

        Return False
      End If

    End If

    If Not IsNothing(_control_limit) Then

      _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(122), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2)

      If _rc = ENUM_MB_RESULT.MB_RESULT_NO Then
        tab_limit.SelectedIndex = _selected_index
        Call _control_limit.Focus()

        Return False
      End If

    End If

    If chk_by_flag.Checked Then
      Select Case IsFlagGridDataOK(dg_flags_required)
        Case GRID_DATA_STATUS.VALUE_NOT_VALID ' Flag checked without a count
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1398), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.NO_ONE_CHECKED ' No one flag checked
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          btn_Accounts_Afected.Enabled = chk_by_flag.Checked
          Return False
        Case GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
        Case GRID_DATA_STATUS.OK
        Case Else
      End Select
    End If

    If chk_flags_awarded.Checked Then
      Select Case IsFlagGridDataOK(dg_flags_awarded, Nothing, MAX_FLAGS_AWARDED)
        Case GRID_DATA_STATUS.VALUE_NOT_VALID ' Flag checked without a count
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1399), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.NO_ONE_CHECKED ' No one flag checked
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1417), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414).ToLower, MAX_FLAGS_AWARDED)
          Return False
        Case GRID_DATA_STATUS.OK
        Case Else
      End Select
    End If

    ' QMP 18-NOV-2013: Validate large image
    If chk_visible_on_PromoBOX.Checked And Me.img_image.Image Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1435), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call img_image.Focus()

      Return False
    End If

    If Me.m_is_cash_draw_2_enabled _
      And Me.chk_award_with_game.Checked AndAlso Me.cmb_award_with_game.SelectedIndex = -1 Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8677), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.cmb_award_with_game.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim promo_item As CLASS_PROMOTION

    promo_item = DbEditedObject

    promo_item.Name = ef_promo_name.Value
    promo_item.Enabled = opt_enabled.Checked
    promo_item.TextOnPromoBOX = ef_text_on_PromoBOX.Value

    If chk_special_permission.Checked Then
      If opt_special_permission_a.Checked Then
        promo_item.SpecialPermission = CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_A
      Else
        promo_item.SpecialPermission = CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_B
      End If
    Else
      promo_item.SpecialPermission = CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
    End If

    promo_item.DateStart = uc_promo_schedule.DateFrom
    promo_item.DateFinish = uc_promo_schedule.DateTo

    promo_item.ScheduleWeekday = uc_promo_schedule.Weekday
    promo_item.Schedule1TimeFrom = uc_promo_schedule.TimeFrom
    promo_item.Schedule1TimeTo = uc_promo_schedule.TimeTo
    promo_item.Schedule2Enabled = uc_promo_schedule.SecondTime
    If promo_item.Schedule2Enabled Then
      promo_item.Schedule2TimeFrom = uc_promo_schedule.SecondTimeFrom
      promo_item.Schedule2TimeTo = uc_promo_schedule.SecondTimeTo
    Else
      promo_item.Schedule2TimeFrom = 0
      promo_item.Schedule2TimeTo = 0
    End If

    promo_item.GenderFilter = CLASS_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
    If chk_by_gender.Checked Then
      If opt_gender_male.Checked Then
        promo_item.GenderFilter = CLASS_PROMOTION.ENUM_GENDER_FILTER.MEN_ONLY
      ElseIf opt_gender_female.Checked Then
        promo_item.GenderFilter = CLASS_PROMOTION.ENUM_GENDER_FILTER.WOMEN_ONLY
      End If
    End If

    promo_item.BirthdayFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
    If chk_by_birth_date.Checked Then
      If opt_birth_date_only.Checked Then
        promo_item.BirthdayFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.DAY_ONLY
      ElseIf opt_birth_month_only.Checked Then
        promo_item.BirthdayFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
      End If
    End If

    promo_item.AgeFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
    If chk_by_age.Checked Then
      If Me.ef_age_from1.Value = "" Then ef_age_from1.Value = 0
      If Me.ef_age_from2.Value = "" Then ef_age_from2.Value = 0
      If Me.ef_age_to1.Value = "" Then ef_age_to1.Value = 0
      If Me.ef_age_to2.Value = "" Then ef_age_to2.Value = 0
      If opt_birth_include_age.Checked Then
        promo_item.BirthdayFilter = WSI.Common.Promotion.EncodeBirthdayFilter(promo_item.BirthdayFilter, CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.INCLUDE_AGE, ef_age_from1.Value, ef_age_to1.Value)
        promo_item.AgeFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.INCLUDE_AGE
      ElseIf opt_birth_exclude_age.Checked Then
        promo_item.BirthdayFilter = WSI.Common.Promotion.EncodeBirthdayFilter(promo_item.BirthdayFilter, CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE, ef_age_from2.Value, ef_age_to2.Value)
        promo_item.AgeFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE
      End If
    End If

    promo_item.BirthdayAgeFrom = IIf(ef_age_from2.Value = "", 0, ef_age_from2.Value)
    promo_item.BirthdayAgeTo = IIf(ef_age_to2.Value = "", 0, ef_age_to2.Value)

    ' AVZ 01-OCT-2015
    promo_item.CreatedAccountFilter = CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
    Dim working_day_plus As Integer = 0
    Dim anniversary_year_from As Integer = 0
    Dim anniversary_year_to As Integer = 0

    If chk_by_created_account.Checked Then

      If opt_created_account_date.Checked Then
        If opt_created_account_working_day_only.Checked Then
          promo_item.CreatedAccountFilter = CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAY_ONLY
        ElseIf opt_created_account_working_day_plus.Checked Then
          promo_item.CreatedAccountFilter = CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAYS_INCLUDED

          If (ef_created_account_working_day_plus.TextValue <> String.Empty) Then
            working_day_plus = ef_created_account_working_day_plus.Value
          End If
        End If
      ElseIf opt_created_account_anniversary.Checked Then
        If opt_created_account_anniversary_whole_month.Checked Then
          promo_item.CreatedAccountFilter = CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WHOLE_MONTH
        ElseIf opt_created_account_anniversary_day_month.Checked Then
          promo_item.CreatedAccountFilter = CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WORKING_DAY_ONLY
        End If

        If chk_by_anniversary_range_of_years.Checked Then
          If (ef_created_account_anniversary_year_from.TextValue <> String.Empty) Then
            anniversary_year_from = ef_created_account_anniversary_year_from.Value
          End If
          If (ef_created_account_anniversary_year_to.TextValue <> String.Empty) Then
            anniversary_year_to = ef_created_account_anniversary_year_to.Value
          End If
        End If
      End If
    End If

    promo_item.CreatedAccountFilter = WSI.Common.Promotion.EncodeCreatedAccountFilter(promo_item.CreatedAccountFilter, working_day_plus, anniversary_year_from, anniversary_year_to)
    promo_item.CreatedAccountWorkingDaysIncluded = working_day_plus
    promo_item.CreatedAccountAnniversaryYearFrom = anniversary_year_from
    promo_item.CreatedAccountAnniversaryYearTo = anniversary_year_to

    promo_item.LevelFilter = GetLevelFilter()

    If chk_by_freq.Checked Then
      promo_item.FreqFilterLastDays = GUI_ParseNumber(ef_freq_last_days.Value)
      promo_item.FreqFilterMinDays = GUI_ParseNumber(ef_freq_min_days.Value)
      promo_item.FreqFilterMinCashIn = GUI_ParseCurrency(ef_freq_min_cash_in.Value)
    Else
      promo_item.FreqFilterLastDays = 0
      promo_item.FreqFilterMinDays = 0
      promo_item.FreqFilterMinCashIn = 0
    End If

    'If gb_expiration.Enabled Then
    If pa_expiration.Enabled Then
      If opt_expiration_days.Checked Then
        promo_item.ExpirationType = CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        promo_item.ExpirationValue = GUI_ParseNumber(ef_expiration_days.Value)
      Else
        promo_item.ExpirationType = CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.HOURS
        promo_item.ExpirationValue = GUI_ParseNumber(ef_expiration_hours.Value)
      End If

      ' DHA 09-APR-2013: 'Expiration Limit' field added
      If chk_expiration_limit.Checked Then
        promo_item.ExpirationLimit = Me.dtp_expiration_limit.Value
      Else
        promo_item.ExpirationLimit = DateTime.MinValue
      End If

    End If

    promo_item.MinCashIn = GUI_ParseCurrency(ef_min_cash_in.Value)
    promo_item.MinCashInReward = GUI_ParseCurrency(ef_min_cash_in_reward.Value)

    promo_item.CashIn = GUI_ParseCurrency(ef_cash_in.Value)
    promo_item.CashInReward = GUI_ParseCurrency(ef_cash_in_reward.Value)

    promo_item.MinSpent = GUI_ParseCurrency(ef_min_spent.Value)
    promo_item.MinSpentReward = GUI_ParseCurrency(ef_min_spent_reward.Value)

    promo_item.Spent = GUI_ParseCurrency(ef_spent.Value)
    promo_item.SpentReward = GUI_ParseCurrency(ef_spent_reward.Value)

    uc_terminals_group_filter.GetTerminalsFromControl(promo_item.TerminalList)
    ' AMF 29-AUG-2013
    uc_terminals_group_filter_restricted.GetTerminalsFromControl(promo_item.TerminalListRestricted)

    uc_terminals_played_group_filter.GetTerminalsFromControl(promo_item.TerminalListPlayed)

    promo_item.NumTokens = GUI_ParseNumber(ef_num_tokens.Value)
    promo_item.TokenReward = GUI_ParseCurrency(ef_token_reward.Value)
    promo_item.TokenName = ef_token_name.Value

    promo_item.DailyLimit = GUI_ParseCurrency(ef_daily_limit.Value)
    promo_item.MonthlyLimit = GUI_ParseCurrency(ef_monthly_limit.Value)

    promo_item.GlobalDailyLimit = GUI_ParseCurrency(Me.ef_promotion_limit_day.Value)
    promo_item.GlobalMonthlyLimit = GUI_ParseCurrency(Me.ef_promotion_limit_month.Value)

    ' JAB 20-AUG-2012 Added Promotion Global Limit
    promo_item.GlobalLimit = GUI_ParseCurrency(Me.ef_promotion_limit_global.Value)

    If promo_item.Type = WSI.Common.Promotion.PROMOTION_TYPE.MANUAL Then
      If opt_credit_non_redeemable.Checked Then
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
      ElseIf opt_credit_redeemable.Checked Then
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
      ElseIf opt_point.Checked Then
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
      Else
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
      End If
    End If

    promo_item.WonLockEnabled = False
    promo_item.WonLock = 0
    If opt_credit_non_redeemable.Checked Then
      promo_item.WonLockEnabled = chk_won_lock.Checked
      promo_item.WonLock = GUI_ParseCurrency(ef_won_lock.Value)
    End If

    promo_item.Icon = img_icon.Image
    promo_item.Image = img_image.Image

    'HBB: get the ef_ticket_footer and set it to the ticket_footer property
    promo_item.TicketFooter = tb_ticket_footer.Text

    promo_item.CategoryId = cmb_categories.Value

    ' XCD 03-OCT-2012
    If chk_flags_awarded.Checked Then
      promo_item.FlagsAwarded = GetFlagsDatatableFromGrid(dg_flags_awarded)
    Else
      promo_item.FlagsAwarded.Clear()
    End If

    If chk_by_flag.Checked Then
      promo_item.FlagsRequired = GetFlagsDatatableFromGrid(dg_flags_required)
      btn_Accounts_Afected.Enabled = True
    Else
      promo_item.FlagsRequired.Clear()
      btn_Accounts_Afected.Enabled = False
    End If

    ' JMM 14-NOV-2012
    promo_item.VisibleOnPromoBOX = Me.chk_visible_on_PromoBOX.Checked

    ' GMV 05-NOV-2015
    promo_item.AwardOnPromoBOX = If(Me.chk_award_on_PromoBOX.Checked, PROMOTION_AWARD_TARGET.PROMOBOX, 0) + If(Me.chk_award_on_InTouch.Checked, PROMOTION_AWARD_TARGET.INTOUCH, 0)

    ' JBP 07-APR-2014
    promo_item.IsVip = Me.chk_only_vip.Checked

    ' LA 23-FEB-2016
    If Me.chk_apply_tax.Visible Then
      promo_item.ApplyTax = Me.chk_apply_tax.Checked
    Else
      promo_item.ApplyTax = False
    End If

    If Me.m_is_cash_draw_2_enabled Then
      If Me.chk_award_with_game.Checked Then
        promo_item.Promogame = Me.cmb_award_with_game.SelectedValue
      Else
        promo_item.Promogame = -1
      End If
      ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
      'If Me.m_is_pyramidal_dist_enabled Then
      '  promo_item.PyramidalDist = Me.GetPyramidalDistData()
      'End If

    End If

    ' NFE 09-OCT-2015
    promo_item.MinPlayed = GUI_ParseCurrency(ef_min_played.Value)
    promo_item.MinPlayedReward = GUI_ParseCurrency(ef_min_played_reward.Value)
    promo_item.Played = GUI_ParseCurrency(ef_played.Value)
    promo_item.PlayedReward = GUI_ParseCurrency(ef_played_reward.Value)

    ' RLO 25-OCT-2017
    promo_item.JourneyLimit = Me.chk_jorney_limit.Checked

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim promo_item As CLASS_PROMOTION

    promo_item = DbReadObject

    ef_promo_name.Value = promo_item.Name
    m_promotion_id = promo_item.PromotionId
    If promo_item.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    ef_text_on_PromoBOX.Value = promo_item.TextOnPromoBOX

    lbl_belongs_to_points_to_credit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2716) '' 2716 "Belongs to redemption table"
    lbl_belongs_to_points_to_credit.Value += " " & promo_item.ChangeTableName
    lbl_belongs_to_points_to_credit.Visible = promo_item.BelongsToRedemptionTable

    Select Case promo_item.SpecialPermission
      Case CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
        chk_special_permission.Checked = False
        opt_special_permission_a.Enabled = False
        opt_special_permission_a.Checked = False
        opt_special_permission_b.Enabled = False
        opt_special_permission_b.Checked = False
        lbl_not_applicable_mb1.Visible = False

      Case CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_A
        chk_special_permission.Checked = True
        opt_special_permission_a.Checked = True
        opt_special_permission_b.Checked = False
        lbl_not_applicable_mb1.Visible = m_mb_auto_promotion

      Case CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_B
        chk_special_permission.Checked = True
        opt_special_permission_a.Checked = False
        opt_special_permission_b.Checked = True
        lbl_not_applicable_mb1.Visible = m_mb_auto_promotion

    End Select

    ' JBP 07-APR-2014: Vips
    Me.chk_only_vip.Checked = promo_item.IsVip




    uc_promo_schedule.DateFrom = promo_item.DateStart
    uc_promo_schedule.DateTo = promo_item.DateFinish
    uc_promo_schedule.Weekday = promo_item.ScheduleWeekday
    uc_promo_schedule.TimeFrom = promo_item.Schedule1TimeFrom
    uc_promo_schedule.TimeTo = promo_item.Schedule1TimeTo
    uc_promo_schedule.SecondTime = promo_item.Schedule2Enabled
    uc_promo_schedule.SecondTimeFrom = promo_item.Schedule2TimeFrom
    uc_promo_schedule.SecondTimeTo = promo_item.Schedule2TimeTo

    ' Gender Filter
    Select Case promo_item.GenderFilter
      Case CLASS_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
        chk_by_gender.Checked = False
        opt_gender_male.Enabled = False
        opt_gender_male.Checked = False
        opt_gender_female.Enabled = False
        opt_gender_female.Checked = False

      Case CLASS_PROMOTION.ENUM_GENDER_FILTER.MEN_ONLY
        chk_by_gender.Checked = True
        opt_gender_male.Checked = True
        opt_gender_female.Checked = False

      Case CLASS_PROMOTION.ENUM_GENDER_FILTER.WOMEN_ONLY
        chk_by_gender.Checked = True
        opt_gender_male.Checked = False
        opt_gender_female.Checked = True

    End Select

    ' Birthday Filter
    Select Case promo_item.BirthdayFilter
      Case CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        chk_by_birth_date.Checked = False
        opt_birth_date_only.Checked = False
        opt_birth_date_only.Enabled = False
        opt_birth_month_only.Checked = False
        opt_birth_month_only.Enabled = False

      Case CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.DAY_ONLY
        chk_by_birth_date.Checked = True
        opt_birth_date_only.Checked = True
        opt_birth_month_only.Checked = False

      Case CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
        chk_by_birth_date.Checked = True
        opt_birth_date_only.Checked = False
        opt_birth_month_only.Checked = True

    End Select

    Select Case promo_item.AgeFilter
      Case CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        opt_birth_include_age.Checked = False
        opt_birth_include_age.Enabled = False
        opt_birth_exclude_age.Checked = False
        opt_birth_exclude_age.Enabled = False
        ef_age_from2.Value = ""
        ef_age_from2.Enabled = False
        ef_age_from1.Value = ""
        ef_age_from1.Enabled = False
        ef_age_to2.Value = ""
        ef_age_to2.Enabled = False
        ef_age_to1.Value = ""
        ef_age_to1.Enabled = False

      Case CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.INCLUDE_AGE
        chk_by_age.Checked = True
        opt_birth_include_age.Checked = True
        opt_birth_exclude_age.Checked = False
        ef_age_from1.Enabled = True
        ef_age_from1.Value = promo_item.BirthdayAgeFrom
        ef_age_to1.Enabled = True
        ef_age_to1.Value = promo_item.BirthdayAgeTo

      Case CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.EXCLUDE_AGE
        chk_by_age.Checked = True
        opt_birth_include_age.Checked = False
        opt_birth_exclude_age.Checked = True
        ef_age_from2.Enabled = True
        ef_age_from2.Value = promo_item.BirthdayAgeFrom
        ef_age_to2.Enabled = True
        ef_age_to2.Value = promo_item.BirthdayAgeTo

    End Select

    ' AVZ 01-OCT-2015
    Select Case promo_item.CreatedAccountFilter
      Case CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
        chk_by_created_account.Checked = False
        opt_created_account_date.Checked = False
        opt_created_account_working_day_only.Checked = False
        opt_created_account_working_day_plus.Checked = False
        opt_created_account_anniversary.Checked = False
        opt_created_account_anniversary_whole_month.Checked = False
        opt_created_account_anniversary_day_month.Checked = False

        opt_created_account_date.Enabled = False
        opt_created_account_working_day_only.Enabled = False
        opt_created_account_working_day_plus.Enabled = False
        opt_created_account_anniversary.Enabled = False
        opt_created_account_anniversary_whole_month.Enabled = False
        opt_created_account_anniversary_day_month.Enabled = False
        chk_by_anniversary_range_of_years.Enabled = False
        ef_created_account_working_day_plus.Enabled = False
      Case CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAY_ONLY
        chk_by_created_account.Checked = True
        opt_created_account_date.Checked = True
        opt_created_account_working_day_only.Checked = True
        opt_created_account_working_day_plus.Checked = False
        opt_created_account_anniversary.Checked = False
        opt_created_account_anniversary_whole_month.Checked = False
        opt_created_account_anniversary_day_month.Checked = False

        opt_created_account_anniversary_whole_month.Enabled = False
        opt_created_account_anniversary_day_month.Enabled = False
        chk_by_anniversary_range_of_years.Enabled = False
      Case CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.WORKING_DAYS_INCLUDED
        chk_by_created_account.Checked = True
        opt_created_account_date.Checked = True
        opt_created_account_working_day_only.Checked = False
        opt_created_account_working_day_plus.Checked = True
        opt_created_account_anniversary.Checked = False
        opt_created_account_anniversary_whole_month.Checked = False
        opt_created_account_anniversary_day_month.Checked = False

        opt_created_account_anniversary_whole_month.Enabled = False
        opt_created_account_anniversary_day_month.Enabled = False
        chk_by_anniversary_range_of_years.Enabled = False
      Case CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WHOLE_MONTH
        chk_by_created_account.Checked = True
        opt_created_account_date.Checked = False
        opt_created_account_working_day_only.Checked = False
        opt_created_account_working_day_plus.Checked = False
        opt_created_account_anniversary.Checked = True
        opt_created_account_anniversary_whole_month.Checked = True
        opt_created_account_anniversary_day_month.Checked = False

        opt_created_account_working_day_only.Enabled = False
        opt_created_account_working_day_plus.Enabled = False
        ef_created_account_working_day_plus.Enabled = False
      Case CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.ANNIVERSARY_WORKING_DAY_ONLY
        chk_by_created_account.Checked = True
        opt_created_account_date.Checked = False
        opt_created_account_working_day_only.Checked = False
        opt_created_account_working_day_plus.Checked = False
        opt_created_account_anniversary.Checked = True
        opt_created_account_anniversary_whole_month.Checked = False
        opt_created_account_anniversary_day_month.Checked = True

        opt_created_account_working_day_only.Enabled = False
        opt_created_account_working_day_plus.Enabled = False
        ef_created_account_working_day_plus.Enabled = False
    End Select

    ef_created_account_working_day_plus.Value = promo_item.CreatedAccountWorkingDaysIncluded
    ef_created_account_anniversary_year_from.Value = promo_item.CreatedAccountAnniversaryYearFrom
    ef_created_account_anniversary_year_to.Value = promo_item.CreatedAccountAnniversaryYearTo
    chk_by_anniversary_range_of_years.Checked = promo_item.CreatedAccountAnniversaryYearFrom <> 0

    If Not chk_by_anniversary_range_of_years.Checked Then
      ef_created_account_anniversary_year_from.Enabled = False
      ef_created_account_anniversary_year_to.Enabled = False
    End If

    ' Level Filter
    SetLevelFilter(promo_item.LevelFilter)

    ' Frequency Filter
    If promo_item.FreqFilterLastDays = 0 Then
      chk_by_freq.Checked = False
      ef_freq_last_days.Enabled = False
      ef_freq_min_days.Enabled = False
      ef_freq_min_cash_in.Enabled = False
    Else
      chk_by_freq.Checked = True
      ef_freq_last_days.Enabled = True
      ef_freq_min_days.Enabled = True
      ef_freq_min_cash_in.Enabled = True

      ef_freq_last_days.Value = promo_item.FreqFilterLastDays
      ef_freq_min_days.Value = promo_item.FreqFilterMinDays
      ef_freq_min_cash_in.Value = promo_item.FreqFilterMinCashIn
    End If

    ' Select first activated tab, in order.
    ' AVZ 01-OCT-2015: Tab "Filter By created account date" added
    tab_filters.SelectedIndex = 0
    If chk_by_gender.Checked = False Then
      If chk_by_birth_date.Checked = False And chk_by_age.Checked = False Then
        If chk_by_created_account.Checked = False Then
          If chk_by_level.Checked = False Then
            If chk_by_freq.Checked = True Then
              tab_filters.SelectedIndex = 4
            End If
          Else
            tab_filters.SelectedIndex = 3
          End If
        Else
          tab_filters.SelectedIndex = 2
        End If
      Else
        tab_filters.SelectedIndex = 1
      End If
    End If

    ' Expiration type and value
    Select Case promo_item.ExpirationType
      Case CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.NOT_SET
        opt_expiration_days.Checked = False
        opt_expiration_hours.Checked = False
        ef_expiration_days.Enabled = False
        ef_expiration_hours.Enabled = False
        chk_expiration_limit.Enabled = False
        dtp_expiration_limit.Enabled = False
        Me.lbl_credits_non_redeemable.Enabled = False

      Case CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        opt_expiration_days.Checked = True
        opt_expiration_hours.Checked = False
        If promo_item.ExpirationValue > 0 Then
          ef_expiration_days.Value = promo_item.ExpirationValue
        End If
        ef_expiration_hours.Enabled = False
        chk_expiration_limit.Enabled = True
        dtp_expiration_limit.Enabled = False
        Me.lbl_credits_non_redeemable.Enabled = True

      Case CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.HOURS
        opt_expiration_days.Checked = False
        opt_expiration_hours.Checked = True
        If promo_item.ExpirationValue > 0 Then
          ef_expiration_hours.Value = promo_item.ExpirationValue
        End If
        ef_expiration_days.Enabled = False
        chk_expiration_limit.Enabled = True
        dtp_expiration_limit.Enabled = False
        Me.lbl_credits_non_redeemable.Enabled = True

    End Select

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW) Then
      opt_credit_non_redeemable.Checked = True
      WonLockEnable(False)
    End If

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT) Or (Me.m_promotion_copy_operation) Then
      If promo_item.MinCashIn > 0 Then
        ef_min_cash_in.Value = promo_item.MinCashIn
      End If
      If promo_item.MinCashInReward > 0 Then
        ef_min_cash_in_reward.Value = promo_item.MinCashInReward
      End If

      If promo_item.CashIn > 0 Then
        ef_cash_in.Value = promo_item.CashIn
      End If
      If promo_item.CashInReward > 0 Then
        ef_cash_in_reward.Value = promo_item.CashInReward
      End If

      If promo_item.MinSpent > 0 Then
        ef_min_spent.Value = promo_item.MinSpent
      End If
      If promo_item.MinSpentReward > 0 Then
        ef_min_spent_reward.Value = promo_item.MinSpentReward
      End If

      If promo_item.Spent > 0 Then
        ef_spent.Value = promo_item.Spent
      End If
      If promo_item.SpentReward > 0 Then
        ef_spent_reward.Value = promo_item.SpentReward
      End If

      If promo_item.NumTokens > 0 Then
        ef_num_tokens.Value = promo_item.NumTokens
      End If
      If promo_item.TokenReward > 0 Then
        ef_token_reward.Value = promo_item.TokenReward
      End If
      ef_token_name.Value = promo_item.TokenName

      If promo_item.DailyLimit > 0 Then
        ef_daily_limit.Value = promo_item.DailyLimit
      End If
      If promo_item.MonthlyLimit > 0 Then
        ef_monthly_limit.Value = promo_item.MonthlyLimit
      End If

      If promo_item.GlobalDailyLimit > 0 Then
        ef_promotion_limit_day.Value = promo_item.GlobalDailyLimit
      End If
      If promo_item.GlobalMonthlyLimit > 0 Then
        ef_promotion_limit_month.Value = promo_item.GlobalMonthlyLimit
      End If

      ' JAB 20-AUG-2012 Added Promotion Global Limit
      If promo_item.GlobalLimit > 0 Then
        ef_promotion_limit_global.Value = promo_item.GlobalLimit
      End If

      If promo_item.WonLock >= 0 Then
        chk_won_lock.Checked = promo_item.WonLockEnabled
        ef_won_lock.Value = promo_item.WonLock
        WonLockEnable(chk_won_lock.Checked)
      End If

      ' NFE 09-OCT-2015 Added Played
      If promo_item.MinPlayed > 0 Then
        ef_min_played.Value = promo_item.MinPlayed
      End If
      If promo_item.MinPlayedReward > 0 Then
        ef_min_played_reward.Value = promo_item.MinPlayedReward
      End If

      If promo_item.Played > 0 Then
        ef_played.Value = promo_item.Played
      End If
      If promo_item.PlayedReward > 0 Then
        ef_played_reward.Value = promo_item.PlayedReward
      End If

    End If

    uc_terminals_group_filter.SetTerminalList(promo_item.TerminalList)
    ' AMF 29-AUG-2013
    uc_terminals_group_filter_restricted.SetTerminalList(promo_item.TerminalListRestricted)

    uc_terminals_played_group_filter.SetTerminalList(promo_item.TerminalListPlayed)

    ' System-type promotions are protected
    If promo_item.Type <> WSI.Common.Promotion.PROMOTION_TYPE.MANUAL Then
      Call uc_promo_schedule.ProtectSystemPromotion()
      gb_credit_type.Enabled = False
      gb_enabled.Enabled = False
      gb_special_permission.Enabled = False
      tab_filters.Enabled = False
      gb_won_lock.Enabled = False
      gb_test.Enabled = False
      'LA 13-04-2016
      chk_apply_tax.Enabled = False

      'LJM 05-DEC-2013: Restrictions for External NR must be enabled
      'DHA 14-FEB-2014 Restrictions for External Restricted XX Promos
      'JCO 02-JUL-2014: Restrictions for all NR system promotions must be enabled
      tabPromotions.Enabled = False
      tabPromotions.SelectedIndex = PROMOTIONS_RESTRICTED_TABINDEX

      'DHA 14-FEB-2014 Possibility to enable External Restricted XX Promos
      If promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_02 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_03 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_04 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_05 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_06 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_07 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_08 Or _
          promo_item.Type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_09 Then

        gb_enabled.Enabled = True
      End If

      tab_limit.Enabled = False
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    End If



    ' Initialize test fields
    ef_test_cash_in.Value = GUI_FormatCurrency(0)
    ef_test_num_tokens.Value = GUI_FormatNumber(0)

    img_icon.Image = promo_item.Icon
    img_image.Image = promo_item.Image

    Select Case promo_item.CreditType
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
        opt_credit_non_redeemable.Checked = True
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
        opt_point.Checked = True
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        opt_credit_redeemable.Checked = True
      Case Else
        opt_credit_non_redeemable.Checked = True
    End Select

    ' AMF 29-AUG-2013
    If Not opt_credit_non_redeemable.Checked Then
      tabPromotions.TabPages.Remove(tab_restricted)
    End If

    If opt_point.Checked Or opt_credit_redeemable.Checked Then
      pa_expiration.Enabled = False
    End If

    'HBB: Initialize ticket_footer field
    tb_ticket_footer.Text = promo_item.TicketFooter

    cmb_categories.Value = promo_item.CategoryId

    ' JBP 07-APR-2014
    Me.chk_only_vip.Checked = promo_item.IsVip

    ' LA 23-FEB-2016
    If opt_credit_redeemable.Checked And WSI.Common.Misc.ReadGeneralParams("Cashier", "Promotions.RE.Tax.Enabled") Then
      Me.chk_apply_tax.Checked = promo_item.ApplyTax
    End If


    ' Initialize text labels that use values from entry fields
    Call RefreshLabelTexts()

    ' Init promo flags
    If promo_item.FlagsRequired.Rows.Count > 0 Then
      chk_by_flag.Checked = True
    Else
      chk_by_flag.Checked = False

    End If
    Call FillFlagsGrid(Me.dg_flags_required, promo_item.FlagsRequired, True)

    If promo_item.FlagsAwarded.Rows.Count > 0 Then
      chk_flags_awarded.Checked = True
    Else
      chk_flags_awarded.Checked = False
    End If

    Call FillFlagsGrid(Me.dg_flags_awarded, promo_item.FlagsAwarded, False)

    chk_visible_on_PromoBOX.Checked = promo_item.VisibleOnPromoBOX
    chk_award_on_PromoBOX.Checked = (promo_item.AwardOnPromoBOX And PROMOTION_AWARD_TARGET.PROMOBOX) = PROMOTION_AWARD_TARGET.PROMOBOX
    chk_award_on_InTouch.Checked = (promo_item.AwardOnPromoBOX And PROMOTION_AWARD_TARGET.INTOUCH) = PROMOTION_AWARD_TARGET.INTOUCH


    ' DHA 09-APR-2013: 'Expiration Limit' field added
    If promo_item.ExpirationLimit = DateTime.MinValue Then
      chk_expiration_limit.Checked = False
      dtp_expiration_limit.Value = promo_item.DateFinish
    Else
      chk_expiration_limit.Checked = True
      dtp_expiration_limit.Value = promo_item.ExpirationLimit
    End If

    If Me.m_is_cash_draw_2_enabled Then
      ' Set promogame value
      If promo_item.Promogame > 0 Then
        Me.chk_award_with_game.Checked = True
        Me.cmb_award_with_game.SelectedValue = promo_item.Promogame
      Else
        Me.chk_award_with_game.Checked = False
        Me.cmb_award_with_game.SelectedIndex = -1
        Me.cmb_award_with_game.Enabled = False
      End If

      ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
      '' Set Pyramidal distribution
      'Call Me.SetPyramidalDist(promo_item.PyramidalDist)
    End If

    ' RLO 25-OCT-2017
    chk_jorney_limit.Checked = promo_item.JourneyLimit

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim promo_class As CLASS_PROMOTION
    Dim nls_param1 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT
    Dim aux_date As DateTime

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROMOTION
        promo_class = DbEditedObject
        promo_class.PromotionId = 0
        promo_class.Enabled = True
        promo_class.SpecialPermission = CLASS_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
        promo_class.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
        promo_class.Type = WSI.Common.Promotion.PROMOTION_TYPE.MANUAL
        aux_date = Now.AddMonths(1)
        promo_class.DateStart = New DateTime(aux_date.Year, aux_date.Month, 1, GetDefaultClosingTime(), 0, 0)
        promo_class.DateFinish = promo_class.DateStart
        promo_class.DateFinish = promo_class.DateStart.AddMonths(1)
        promo_class.ScheduleWeekday = uc_promo_schedule.AllWeekDaysSelected()     ' All days
        promo_class.Schedule1TimeFrom = 0     ' 00:00:00
        promo_class.Schedule1TimeTo = 0       ' 00:00:00
        promo_class.Schedule2Enabled = False
        promo_class.Schedule2TimeFrom = 0     ' 00:00:00
        promo_class.Schedule2TimeTo = 0       ' 00:00:00
        promo_class.GenderFilter = CLASS_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
        promo_class.BirthdayFilter = CLASS_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        promo_class.BirthdayAgeFrom = 0
        promo_class.BirthdayAgeTo = 0
        ' AVZ 01-OCT-2015
        promo_class.CreatedAccountFilter = CLASS_PROMOTION.ENUM_CREATED_ACCOUNT_DATE_FILTER.NOT_SET
        promo_class.CreatedAccountWorkingDaysIncluded = 0
        promo_class.CreatedAccountAnniversaryYearFrom = 0
        promo_class.CreatedAccountAnniversaryYearTo = 0
        promo_class.LevelFilter = 0
        promo_class.ExpirationType = CLASS_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        promo_class.CategoryId = 0
        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            promo_class.PromoScreenMode = CLASS_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            promo_class.PromoScreenMode = CLASS_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_EDIT
        End Select

        ' RMS 05-NOV-2013   Change default values for promobox
        promo_class.VisibleOnPromoBOX = False
        promo_class.AwardOnPromoBOX = 0
        promo_class.TextOnPromoBOX = String.Empty

        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          promo_class = Me.DbEditedObject
          nls_param1 = m_input_promo_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_promo_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Dim _comments As frm_comments

        promo_class = Me.DbEditedObject

        ' TODO XCD COmments
        _comments = New frm_comments(NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(495)), _
                                     NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(496)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(1)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(2)), _
                                     False) ' Comments

        Try
          If _comments.ShowGPDialog() = Windows.Forms.DialogResult.OK Then
            promo_class.Comments = _comments.Comments
            Me.DbStatus = ENUM_STATUS.STATUS_OK
          Else
            promo_class.Comments = ""
            Me.DbStatus = ENUM_STATUS.STATUS_NOT_FOUND
          End If
        Finally
          _comments.Dispose()
        End Try

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_promo_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_FOUND Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(107), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        promo_class = Me.DbEditedObject
        nls_param1 = promo_class.Name
        rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(108), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          promo_class = Me.DbEditedObject
          promo_class.Comments = ""
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_promo_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal PromotionCopiedId As Long = 0)
    Dim _promo As CLASS_PROMOTION

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = PromotionCopiedId

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    If PromotionCopiedId > 0 Then
      _promo = DbEditedObject

      DbObjectId = PromotionCopiedId
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      DbObjectId = Nothing
      Me.m_promotion_copy_operation = True

      Call _promo.ResetIdValues()

    End If
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal PromoId As Integer, ByVal PromoName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = 0

    Me.DbObjectId = PromoId
    Me.m_input_promo_name = PromoName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> ENUM_STATUS.STATUS_OK Then

      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowEditItem

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.tb_ticket_footer.ContainsFocus Then

          Return True
        End If

        If Me.dg_flags_awarded.ContainsFocus() Then
          Return Me.dg_flags_awarded.KeyPressed(sender, e)
        End If

        If Me.dg_flags_required.ContainsFocus() Then
          Return Me.dg_flags_required.KeyPressed(sender, e)
        End If
      Case Chr(Keys.Escape)
        If Me.dg_flags_awarded.ContainsFocus() Then
          Return Me.dg_flags_awarded.KeyPressed(sender, e)
        End If

        If Me.dg_flags_required.ContainsFocus() Then
          Return Me.dg_flags_required.KeyPressed(sender, e)
        End If

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)
    End Select

    ' The key Enter event is done in tb_ticket_footer_automatic_promotion

  End Function ' GUI_KeyPress

#End Region ' Overrides

#Region " Private "

  ' PURPOSE: Update controls on PromoBOX tab
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdatePromoBOXTabState()
    ef_text_on_PromoBOX.Enabled = chk_award_on_PromoBOX.Checked Or chk_award_on_InTouch.Checked
    lbl_no_text_on_promobox_info.Visible = (ef_text_on_PromoBOX.Enabled And String.IsNullOrEmpty(ef_text_on_PromoBOX.Value))
  End Sub

  Private Sub SetLevelFilter(ByVal LevelFilter As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    chk_by_level.Checked = LevelFilter <> 0
    chk_by_level_CheckedChanged(Nothing, Nothing)

    str_binary = Convert.ToString(LevelFilter, 2)
    str_binary = New String("0", 5 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(4) = "0") Then chk_level_anonymous.Checked = False Else chk_level_anonymous.Checked = True ' Anonymous Player
    If (bit_array(3) = "0") Then chk_level_01.Checked = False Else chk_level_01.Checked = True ' Level 01
    If (bit_array(2) = "0") Then chk_level_02.Checked = False Else chk_level_02.Checked = True ' Level 02
    If (bit_array(1) = "0") Then chk_level_03.Checked = False Else chk_level_03.Checked = True ' Level 03
    If (bit_array(0) = "0") Then chk_level_04.Checked = False Else chk_level_04.Checked = True ' Level 04

  End Sub ' SetLevelFilter

  Private Function GetLevelFilter() As Integer
    Dim bit_array As Char()

    If Not chk_by_level.Checked Then
      Return 0
    End If

    bit_array = New String("00000").ToCharArray()

    If (Not opt_point.Checked) Then
      bit_array(4) = Convert.ToInt32(chk_level_anonymous.Checked).ToString()  ' Anonymous Player
    End If

    bit_array(3) = Convert.ToInt32(chk_level_01.Checked).ToString()  ' Level 01
    bit_array(2) = Convert.ToInt32(chk_level_02.Checked).ToString()  ' Level 02
    bit_array(1) = Convert.ToInt32(chk_level_03.Checked).ToString()  ' Level 03
    bit_array(0) = Convert.ToInt32(chk_level_04.Checked).ToString()  ' Level 04

    Return Convert.ToInt32(bit_array, 2)
  End Function ' GetLevelFilter

  Private Function CalculateReward(ByVal CashIn As Double, ByVal Spent As Double, ByVal NumTokens As Integer) As Double
    Dim accumulated_reward As Double
    Dim _minium_value_to_divide As String

    accumulated_reward = 0
    _minium_value_to_divide = "0,51"
    ' Min Cash In reward
    If GUI_ParseCurrency(ef_min_cash_in_reward.Value) > 0 Then
      If CashIn >= GUI_ParseCurrency(ef_min_cash_in.Value) Then
        accumulated_reward = GUI_ParseCurrency(ef_min_cash_in_reward.Value)
      End If
    End If

    ' Cash In reward
    If GUI_ParseCurrency(ef_cash_in_reward.Value) > 0 And GUI_ParseCurrency(ef_cash_in.Value) > 0 Then
      If CashIn >= GUI_ParseCurrency(ef_cash_in.Value) And CashIn >= GUI_ParseCurrency(ef_min_cash_in.Value) Then
        If (GUI_ParseCurrency(ef_cash_in.Value) < 0.51000000000000001) Then
          accumulated_reward = accumulated_reward + (CashIn \ GUI_ParseCurrency(_minium_value_to_divide)) * GUI_ParseCurrency(ef_cash_in_reward.Value)
        Else
          accumulated_reward = accumulated_reward + (CashIn \ GUI_ParseCurrency(ef_cash_in.Value)) * GUI_ParseCurrency(ef_cash_in_reward.Value)
        End If
      End If
    End If

    ' Min Spent reward
    If GUI_ParseCurrency(ef_min_spent_reward.Value) > 0 Then
      If Spent >= GUI_ParseCurrency(ef_min_spent.Value) Then
        accumulated_reward = accumulated_reward + GUI_ParseCurrency(ef_min_spent_reward.Value)
      End If
    End If

    ' Spent reward
    If GUI_ParseCurrency(ef_spent_reward.Value) > 0 And GUI_ParseCurrency(ef_spent.Value) > 0 Then
      If Spent >= GUI_ParseCurrency(ef_spent.Value) And Spent >= GUI_ParseCurrency(ef_min_spent.Value) Then
        If (GUI_ParseCurrency(ef_spent.Value) < 0.51000000000000001) Then
          accumulated_reward = accumulated_reward + (Spent \ GUI_ParseCurrency(_minium_value_to_divide)) * GUI_ParseCurrency(ef_spent_reward.Value)
        Else
          accumulated_reward = accumulated_reward + (Spent \ GUI_ParseCurrency(ef_spent.Value)) * GUI_ParseCurrency(ef_spent_reward.Value)
        End If
      End If
    End If

    ' Token reward
    If GUI_ParseCurrency(ef_token_reward.Value) > 0 And GUI_ParseNumber(ef_num_tokens.Value) > 0 Then
      If NumTokens >= GUI_ParseNumber(ef_num_tokens.Value) And CashIn >= GUI_ParseCurrency(ef_min_cash_in.Value) Then
        If (GUI_ParseCurrency(ef_num_tokens.Value) < 0.51000000000000001) Then
          accumulated_reward = accumulated_reward + (NumTokens \ GUI_ParseCurrency(_minium_value_to_divide)) * GUI_ParseCurrency(ef_token_reward.Value)
        Else
          accumulated_reward = accumulated_reward + (NumTokens \ GUI_ParseNumber(ef_num_tokens.Value)) * GUI_ParseCurrency(ef_token_reward.Value)
        End If
      End If
    End If

    Return accumulated_reward
  End Function ' CalculateReward

  Private Sub RefreshLabelTexts()
    Dim _empty_placeholder As String
    Dim _freq_last_days As String
    Dim _freq_min_days As String
    Dim _freq_min_cash_in As String
    Dim _str_promo_credit_type As String

    Dim _min_chash_in_reward As String
    Dim _cash_in_reward As String
    Dim _min_spent_reward As String
    Dim _spent_reward As String
    Dim _token_reward As String
    Dim _daily_limit As String
    Dim _monthly_limit As String
    Dim _promotion_limit_day As String
    Dim _promotion_limit_month As String
    Dim _promotion_limit_global As String
    Dim _min_played_reward As String
    Dim _played_reward As String

    _empty_placeholder = ""

    If Me.opt_credit_redeemable.Checked Then
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(518)
    ElseIf Me.opt_credit_non_redeemable.Checked Then
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(517)
    ElseIf Me.opt_point.Checked Then
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354).ToLower
    Else
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(517)
    End If

    '
    ' Initialize Rewards vars
    '
    If Me.opt_point.Checked Then
      _min_chash_in_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_cash_in_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _cash_in_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_cash_in_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _min_spent_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_spent_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _spent_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_spent_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _token_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_token_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _min_played_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_played_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _played_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_played_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _min_played_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_played_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _min_chash_in_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_cash_in_reward.Value))
      _cash_in_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_cash_in_reward.Value))
      _min_spent_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_spent_reward.Value))
      _spent_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_spent_reward.Value))
      _token_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_token_reward.Value))
      _min_played_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_played_reward.Value))
      _played_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_played_reward.Value))
      _min_played_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_played_reward.Value))
    End If

    ' Initialize text labels that use values from entry fields
    If GUI_ParseCurrency(ef_min_cash_in_reward.Value) > 0 Or GUI_ParseCurrency(ef_min_cash_in.Value) > 0 Then
      lbl_min_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(225, _min_chash_in_reward, _
                                                                              GUI_FormatCurrency(GUI_ParseCurrency(ef_min_cash_in.Value)), _
                                                                              _str_promo_credit_type)
    Else
      lbl_min_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(225, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_cash_in_reward.Value) > 0 Or GUI_ParseCurrency(ef_cash_in.Value) > 0 Then
      lbl_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(227, _cash_in_reward, _
                                                                          GUI_FormatCurrency(GUI_ParseCurrency(ef_cash_in.Value)), _
                                                                          _str_promo_credit_type)
    Else
      lbl_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(227, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_min_spent_reward.Value) > 0 Or GUI_ParseCurrency(ef_min_spent.Value) > 0 Then
      lbl_min_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(422, _min_spent_reward, _
                                                                            GUI_FormatCurrency(GUI_ParseCurrency(ef_min_spent.Value)), _
                                                                            _str_promo_credit_type)
    Else
      lbl_min_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(422, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_spent_reward.Value) > 0 Or GUI_ParseCurrency(ef_spent.Value) > 0 Then
      lbl_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(423, _spent_reward, _
                                                                        GUI_FormatCurrency(GUI_ParseCurrency(ef_spent.Value)), _
                                                                        _str_promo_credit_type)
    Else
      lbl_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(423, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_token_reward.Value) > 0 Or GUI_ParseNumber(ef_num_tokens.Value) > 0 Then
      lbl_token_reward.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(229, _token_reward, _
                                                                     GUI_FormatNumber(GUI_ParseNumber(ef_num_tokens.Value), 0), _
                                                                     ef_token_name.Value, _str_promo_credit_type)
    Else
      lbl_token_reward.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(229, _empty_placeholder, _empty_placeholder, _
                                                                     _empty_placeholder, _str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_min_played_reward.Value) > 0 Or GUI_ParseCurrency(ef_min_played.Value) > 0 Then
      lbl_min_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(449, _min_played_reward, _
                                                                           GUI_FormatCurrency(GUI_ParseCurrency(ef_min_played.Value)), _
                                                                           _str_promo_credit_type)
    Else
      lbl_min_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(449, _empty_placeholder, _empty_placeholder, _
                                                                     _empty_placeholder, _str_promo_credit_type)
    End If

    ' Se obtienen %1 %3 por cada %2 jugado
    If GUI_ParseCurrency(ef_played_reward.Value) > 0 Or GUI_ParseCurrency(ef_played.Value) > 0 Then
      lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(450, _played_reward, _
                                                                       GUI_FormatCurrency(GUI_ParseCurrency(ef_played.Value)), _
                                                                       _str_promo_credit_type)
    Else
      lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(450, _empty_placeholder, _empty_placeholder, _
                                                                     _empty_placeholder, _str_promo_credit_type)
    End If

    '
    'TAB CUSTOMER
    '

    If Me.opt_point.Checked Then
      _daily_limit = GUI_FormatNumber(GUI_ParseNumber(ef_daily_limit.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _monthly_limit = GUI_FormatNumber(GUI_ParseNumber(ef_monthly_limit.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _daily_limit = GUI_FormatCurrency(GUI_ParseCurrency(ef_daily_limit.Value))
      _monthly_limit = GUI_FormatCurrency(GUI_ParseCurrency(ef_monthly_limit.Value))
    End If

    If GUI_ParseCurrency(ef_daily_limit.Value) > 0 Then
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(231, _daily_limit, _
                                                                    _str_promo_credit_type)
    Else
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(237)
    End If
    If GUI_ParseCurrency(ef_monthly_limit.Value) > 0 Then
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(236, _monthly_limit, _
                                                                      _str_promo_credit_type)
    Else
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(238)
    End If

    lbl_jorney_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8746)

    '
    'TAB PROMOTION
    '
    If Me.opt_point.Checked Then
      _promotion_limit_day = GUI_FormatNumber(GUI_ParseNumber(ef_promotion_limit_day.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _promotion_limit_month = GUI_FormatNumber(GUI_ParseNumber(ef_promotion_limit_month.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _promotion_limit_global = GUI_FormatNumber(GUI_ParseNumber(ef_promotion_limit_global.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _promotion_limit_day = GUI_FormatCurrency(GUI_ParseCurrency(ef_promotion_limit_day.Value))
      _promotion_limit_month = GUI_FormatCurrency(GUI_ParseCurrency(ef_promotion_limit_month.Value))
      _promotion_limit_global = GUI_FormatCurrency(GUI_ParseCurrency(ef_promotion_limit_global.Value))
    End If

    If GUI_ParseCurrency(ef_promotion_limit_day.Value) > 0 Then
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(525, _promotion_limit_day, _str_promo_credit_type)
      Me.lbl_promo_daily_limit.Text = lbl_promotion_daily_limit.Value
      Me.lbl_promo_daily_limit.Location = New System.Drawing.Point(230, 31)
    Else
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(527)
      Me.lbl_promo_daily_limit.Text = lbl_promotion_daily_limit.Value
      Me.lbl_promo_daily_limit.Location = New System.Drawing.Point(230, 37)
    End If
    If GUI_ParseCurrency(ef_promotion_limit_month.Value) > 0 Then
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(526, _promotion_limit_month, _str_promo_credit_type)
      Me.lbl_promo_month_limit.Text = lbl_promotion_monthly_limit.Value
      Me.lbl_promo_month_limit.Location = New System.Drawing.Point(230, 62)
    Else
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(528)
      Me.lbl_promo_month_limit.Text = lbl_promotion_monthly_limit.Value
      Me.lbl_promo_month_limit.Location = New System.Drawing.Point(230, 68)
    End If
    If GUI_ParseCurrency(ef_promotion_limit_global.Value) > 0 Then
      lbl_promotion_global_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1248, _promotion_limit_global, _str_promo_credit_type)
      Me.lbl_promo_global_limit.Text = lbl_promotion_global_limit.Value
    Else
      lbl_promotion_global_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1249)
      Me.lbl_promo_global_limit.Text = lbl_promotion_global_limit.Value
    End If

    '
    'WON LOCK
    '
    If chk_won_lock.Checked Then
      If GUI_ParseCurrency(ef_won_lock.Value) >= 0 Then
        lbl_won_lock.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(249, ef_won_lock.Value)
      Else
        lbl_won_lock.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(249, _empty_placeholder)
      End If
    Else
      lbl_won_lock.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(249, _empty_placeholder)
    End If

    '
    ' TEST
    '
    Call Me.RefreshTestText(_str_promo_credit_type)

    _freq_last_days = _empty_placeholder
    _freq_min_days = _empty_placeholder
    _freq_min_cash_in = _empty_placeholder
    If chk_by_freq.Checked Then
      If GUI_ParseNumber(ef_freq_last_days.Value) > 0 Then
        _freq_last_days = GUI_FormatNumber(GUI_ParseNumber(ef_freq_last_days.Value), 0)
      End If
      If GUI_ParseNumber(ef_freq_min_days.Value) > 0 Then
        _freq_min_days = GUI_FormatNumber(GUI_ParseNumber(ef_freq_min_days.Value), 0)
      End If
      If GUI_ParseNumber(ef_freq_min_cash_in.Value) > 0 Then
        _freq_min_cash_in = GUI_FormatCurrency(GUI_ParseCurrency(ef_freq_min_cash_in.Value))
      End If
    End If
    lbl_freq_filter_01.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(412, _freq_last_days, _freq_min_days)
    lbl_freq_filter_02.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(413, _freq_min_cash_in)

  End Sub ' RefreshLabelTexts

  Private Sub RefreshTestText(PromoCreditType As String)
    Dim _calculated_reward As Double
    Dim _pyramization As Pyramization
    Dim _min_pyramization As Decimal
    Dim _max_pyramization As Decimal

    _min_pyramization = 0
    _max_pyramization = 0
    _calculated_reward = CalculateReward(GUI_ParseCurrency(ef_test_cash_in.Value), _
                                         GUI_ParseCurrency(ef_test_spent.Value), _
                                         GUI_ParseNumber(ef_test_num_tokens.Value))

    If Me.uc_pyramidal.IsEnabled() Then
      _pyramization = New Pyramization(_calculated_reward, Me.uc_pyramidal.Xml())
      _min_pyramization = _pyramization.Calculate(1)
      _max_pyramization = _pyramization.Calculate(100)

      If Me.opt_point.Checked Then
        lbl_test_result.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8766,
                                                                      GUI_FormatNumber(_calculated_reward, _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE), _
                                                                      PromoCreditType, _
                                                                      GUI_FormatCurrency(_min_pyramization), _
                                                                      GUI_FormatCurrency(_max_pyramization))
      Else
        lbl_test_result.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8766, _
                                                                      GUI_FormatCurrency(_calculated_reward), _
                                                                      PromoCreditType, _
                                                                      GUI_FormatCurrency(_min_pyramization), _
                                                                      GUI_FormatCurrency(_max_pyramization))
      End If
    Else ' Pyramization disabled
      If Me.opt_point.Checked Then
        lbl_test_result.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(234,
                                                                      GUI_FormatNumber(_calculated_reward, _
                                                                                       ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE), _
                                                                      PromoCreditType)
      Else
        lbl_test_result.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(234, _
                                                                      GUI_FormatCurrency(_calculated_reward), _
                                                                      PromoCreditType)
      End If
    End If

  End Sub

  Private Sub WonLockEnable(ByVal enabled As Boolean)

    ef_won_lock.Value = IIf(enabled, ef_won_lock.Value, "")
    ef_won_lock.Enabled = enabled

    ' MBF 01-OCT-2010 - Do not disable anything

    ' RCI 02-JUN-2011: Disable min cash-in and cash-in.
    ef_min_cash_in.Value = IIf(enabled, "", ef_min_cash_in.Value)
    ef_min_cash_in.Enabled = Not enabled

    ef_cash_in.Value = IIf(enabled, "", ef_cash_in.Value)
    ef_cash_in.Enabled = Not enabled

    ef_cash_in_reward.Value = IIf(enabled, "", ef_cash_in_reward.Value)
    ef_cash_in_reward.Enabled = Not enabled

    'ef_num_tokens.Value = IIf(enabled, "0", ef_num_tokens.Value)
    'ef_num_tokens.Enabled = Not enabled
    'ef_token_name.Value = IIf(enabled, "", ef_token_name.Value)
    'ef_token_name.Enabled = Not enabled
    'ef_token_reward.Value = IIf(enabled, "0", ef_token_reward.Value)
    'ef_token_reward.Enabled = Not enabled

  End Sub ' WonLockEnable

  Private Sub SwitchCredit(ByVal CreditType As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)
    Dim ls_ef As List(Of GUI_Controls.uc_entry_field)
    Dim ls_ef_limits As List(Of GUI_Controls.uc_entry_field)
    Dim _value As Object

    Select Case CreditType
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        If chk_by_level.Checked Then
          chk_level_anonymous.Enabled = Not Me.opt_point.Checked
        End If

        WonLockEnable(chk_won_lock.Checked)
        lbl_won_lock.Enabled = True
        pa_expiration.Enabled = True

        If (WSI.Common.TITO.Utils.IsTitoMode()) Then
          chk_won_lock.Enabled = False

        Else
          chk_won_lock.Enabled = True

        End If

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        If chk_by_level.Checked Then
          chk_level_anonymous.Enabled = Not Me.opt_point.Checked
        End If
        chk_won_lock.Checked = False
        chk_won_lock.Enabled = False
        WonLockEnable(False)
        lbl_won_lock.Enabled = False
        pa_expiration.Enabled = False

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        If chk_by_level.Checked Then
          chk_level_anonymous.Enabled = Not Me.opt_point.Checked
        End If
        chk_won_lock.Checked = False
        chk_won_lock.Enabled = False
        WonLockEnable(False)
        lbl_won_lock.Enabled = False
        pa_expiration.Enabled = False

      Case Else
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        chk_won_lock.Enabled = True
        WonLockEnable(chk_won_lock.Checked)
        lbl_won_lock.Enabled = True
        pa_expiration.Enabled = False

    End Select

    _value = Nothing

    ls_ef_limits = New List(Of GUI_Controls.uc_entry_field)
    ls_ef = New List(Of GUI_Controls.uc_entry_field)

    ls_ef_limits.Add(ef_promotion_limit_day)
    ls_ef_limits.Add(ef_promotion_limit_month)
    ls_ef_limits.Add(ef_promotion_limit_global)

    ls_ef_limits.Add(ef_promotion_consumed_day)
    ls_ef_limits.Add(ef_promotion_consumed_month)
    ls_ef_limits.Add(ef_promotion_consumed_global)

    ls_ef_limits.Add(ef_promotion_available_day)
    ls_ef_limits.Add(ef_promotion_available_month)
    ls_ef_limits.Add(ef_promotion_available_global)

    ls_ef_limits.Add(ef_monthly_limit)
    ls_ef_limits.Add(ef_daily_limit)

    ls_ef.Add(ef_min_spent_reward)
    ls_ef.Add(ef_cash_in_reward)
    ls_ef.Add(ef_min_cash_in_reward)
    ls_ef.Add(ef_spent_reward)
    ls_ef.Add(ef_token_reward)
    ls_ef.Add(ef_played_reward)
    ls_ef.Add(ef_min_played_reward)

    Select Case CreditType

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT

        For Each _uc As uc_entry_field In ls_ef_limits
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
          _uc.Value = _value
        Next
        For Each _uc As uc_entry_field In ls_ef
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
          _uc.Value = _value
        Next

      Case Else
        For Each _uc As uc_entry_field In ls_ef_limits
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
          _uc.Value = _value
        Next

        For Each _uc As uc_entry_field In ls_ef
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5, 2)
          _uc.Value = _value
        Next

    End Select
  End Sub

  ' XCD 2-OCT-2012
  Private Sub InitializeRequiredFlags()

    Call Me.dg_flags_required.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_flags_required.Column(GRID_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG CHECKED
    With Me.dg_flags_required.Column(GRID_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .HighLightWhenSelected = False
    End With

    ' FLAG COLOR
    With Me.dg_flags_required.Column(GRID_FLAG_COLOR)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .WidthFixed = 200
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_flags_required.Column(GRID_FLAG_NAME)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .Width = 3000
      .Editable = False
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_COUNT
    With Me.dg_flags_required.Column(GRID_FLAG_COUNT)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1416) 'Requeridas
      .WidthFixed = 1300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_FLAG_REQUIRED_DIGITS)
    End With

    ' COLUMN_FLAG_ID_COLOR
    With Me.dg_flags_required.Column(GRID_FLAG_ID_COLOR)
      .Width = 0
      .IsColumnPrintable = False
    End With

  End Sub

  Private Sub InitializeAwardedFlags()

    Call Me.dg_flags_awarded.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_flags_awarded.Column(GRID_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG CHECKED
    With Me.dg_flags_awarded.Column(GRID_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .HighLightWhenSelected = False
    End With

    ' FLAG COLOR
    With Me.dg_flags_awarded.Column(GRID_FLAG_COLOR)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .WidthFixed = 200
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_flags_awarded.Column(GRID_FLAG_NAME)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .Width = 3000
      .Editable = False
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_COUNT
    With Me.dg_flags_awarded.Column(GRID_FLAG_COUNT)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414) 'Otorgadas
      .WidthFixed = 1300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_FLAG_AWARDED_DIGITS)
    End With

    ' COLUMN_FLAG_ID_COLOR
    With Me.dg_flags_awarded.Column(GRID_FLAG_ID_COLOR)
      .Width = 0
      .IsColumnPrintable = False
    End With

  End Sub


  ' PURPOSE: Get flag grid data and convert to datatable
  '
  '    - INPUT:
  '       - FlagGrid : grid to get flag data
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - DataTable : Datatable with flags data
  Private Function GetFlagsDatatableFromGrid(ByRef FlagGrid As uc_grid) As DataTable
    Dim _flag_data As DataTable = New DataTable()
    Dim _flag_row As DataRow
    Dim _view As DataView
    Dim _row As Integer

    ' init datatable
    _flag_data.Columns.Add("PF_FLAG_ID", Type.GetType("System.Int64"))
    _flag_data.Columns.Add("PF_FLAG_COUNT", Type.GetType("System.Int32"))
    _flag_data.PrimaryKey = New DataColumn() {_flag_data.Columns("PF_FLAG_ID")}

    For _row = 0 To FlagGrid.NumRows() - 1
      If FlagGrid.Cell(_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _flag_row = _flag_data.NewRow()
        _flag_row("PF_FLAG_ID") = FlagGrid.Cell(_row, GRID_FLAG_IDX).Value

        _flag_row("PF_FLAG_COUNT") = GUI_ParseNumber(FlagGrid.Cell(_row, GRID_FLAG_COUNT).Value)
        _flag_data.Rows.Add(_flag_row)
      End If
    Next

    ' Sort datable
    _view = _flag_data.DefaultView
    _view.Sort = "PF_FLAG_ID ASC"

    Return _view.ToTable()

  End Function

  ' PURPOSE: grid with rows and data
  '
  '    - INPUT:
  '       - FlagGrid : grid to add data
  '       - FlagsData: flag count to be inserted
  '
  '    - OUTPUT:
  '
  Private Sub FillFlagsGrid(ByRef FlagsGrid As uc_grid, ByRef FlagsData As DataTable, ByVal bShowAutomaticFlags As Boolean)

    Dim _idx_flags_data As Integer
    Dim _idx_all_flags As Integer
    Dim _all_flags As DataTable = Nothing
    Dim _view As DataView
    Dim _sql_tx As SqlClient.SqlTransaction = Nothing
    Dim _flag_color As Color

    ' Get all flags
    If Not GUI_BeginSQLTransaction(_sql_tx) Then
      FlagsGrid.Clear()
      Exit Sub
    Else
      CLASS_FLAG.GetAllFlags(_sql_tx, _all_flags, bShowAutomaticFlags)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    ' Add flag count to all flags
    _all_flags.Columns.Add("PF_FLAG_COUNT", Type.GetType("System.Int32"))

    Dim _found As Boolean
    Dim _row As DataRow


    For _idx_flags_data = 0 To FlagsData.Rows.Count - 1
      _found = False
      For _idx_all_flags = 0 To _all_flags.Rows.Count - 1
        If FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_ID") = _all_flags.Rows(_idx_all_flags).Item("FL_FLAG_ID") Then
          _all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT") = FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_COUNT")
          _found = True
          Exit For
        End If
      Next
      If Not _found Then
        _row = _all_flags.NewRow()
        _row("FL_FLAG_ID") = FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_ID")
        _row("PF_FLAG_COUNT") = FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_COUNT")
        _row("FL_NAME") = "##-" + FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_ID").ToString()
        _row("FL_COLOR") = -1
        _all_flags.Rows.Add(_row)
      End If
    Next

    ' Get active flags or flags with count > 0
    _view = _all_flags.DefaultView()
    ' free resources
    _all_flags.Dispose()

    _view.RowFilter = "      ( FL_STATUS = " & CLASS_FLAG.STATUS.ENABLED & _
                      " AND  ( FL_EXPIRATION_TYPE <> " & FLAG_EXPIRATION_TYPE.DATETIME & _
                      " OR   ( FL_EXPIRATION_TYPE = " & FLAG_EXPIRATION_TYPE.DATETIME & _
                      " AND FL_EXPIRATION_DATE > '" & GUI_FormatDate(GUI_GetDateTime(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & "' )))" & _
                      " OR   ( PF_FLAG_COUNT > 0 ) "

    '_view.Sort = "CONVERT(PF_FLAG_COUNT,'System.Int32') DESC, FL_NAME"
    _view.Sort = "FL_TYPE ASC, FL_NAME ASC, FL_FLAG_ID ASC"

    ' Save filter flags
    _all_flags = _view.ToTable()
    ' free resources
    _view.Dispose()
    ' Remove irrelevant data
    _all_flags.Columns.Remove("FL_STATUS")
    _all_flags.Columns.Remove("FL_EXPIRATION_TYPE")
    _all_flags.Columns.Remove("FL_EXPIRATION_DATE")

    ' Change column name
    _all_flags.Columns("FL_FLAG_ID").ColumnName = "PF_FLAG_ID"

    ' Result - _all_flags datatable
    '-------------------------------------------------'
    ' PF_FLAG_ID | FL_NAME | FL_COLOR | PF_FLAG_COUNT '
    '-------------------------------------------------'

    ' Init Grid
    FlagsGrid.Clear()
    If (FlagsData.Rows.Count = 0) Then
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      FlagsGrid.Enabled = False
    Else
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      FlagsGrid.Enabled = True
    End If

    For _idx_all_flags = 0 To _all_flags.Rows.Count - 1
      FlagsGrid.AddRow()

      ' Promotion_Flag_Id
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_IDX).Value = _all_flags.Rows(_idx_all_flags).Item("PF_FLAG_ID").ToString

      ' Flag Color
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_ID_COLOR).Value = _all_flags.Rows(_idx_all_flags).Item("FL_COLOR")
      _flag_color = Color.FromArgb(_all_flags.Rows(_idx_all_flags).Item("FL_COLOR"))
      ' CHAPUZA para pintar la celda de color negro
      If (_flag_color.A = Color.Black.A And _
          _flag_color.R = Color.Black.R And _
          _flag_color.G = Color.Black.G And _
          _flag_color.B = Color.Black.B) Then
        _flag_color = Color.FromArgb(_flag_color.A, _flag_color.R, _flag_color.G, _flag_color.B + 1)
      End If
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COLOR).BackColor = _flag_color

      ' Flag Name
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_NAME).Value = _all_flags.Rows(_idx_all_flags).Item("FL_NAME").ToString

      'Flag Count
      If (String.IsNullOrEmpty(_all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT").ToString)) Then
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COUNT).Value = ""
        If FlagsGrid.Enabled Then
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        Else
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        End If
      Else
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COUNT).Value = GUI_FormatNumber(_all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT"), 0)
        If FlagsGrid.Enabled Then
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End If
      End If
    Next

  End Sub

  ' PURPOSE: Set grid behaviour
  '
  '    - INPUT:
  '       - FlagGrid : grid to be setted
  '       - IsChecked: status of drid ( active or no active)
  '
  '    - OUTPUT:
  '
  Private Sub SetFlagsGridStatus(ByRef FlagsGrid As uc_grid, ByVal IsChecked As Boolean)
    Dim _idx_row As Integer

    If IsChecked Then
      ' Current grid is active
      FlagsGrid.Enabled = True
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      For _idx_row = 0 To FlagsGrid.NumRows - 1
        ' Checked color
        Select Case FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value
          Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
          Case uc_grid.GRID_CHK_CHECKED_DISABLED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        End Select
        ' Name and count color
        FlagsGrid.Cell(_idx_row, GRID_FLAG_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Next

    Else
      ' Current grid is inactive
      FlagsGrid.Enabled = False
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

      For _idx_row = 0 To FlagsGrid.NumRows - 1
        ' Checked color
        Select Case FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value
          Case uc_grid.GRID_CHK_UNCHECKED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          Case uc_grid.GRID_CHK_CHECKED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End Select
        ' Name and count color
        FlagsGrid.Cell(_idx_row, GRID_FLAG_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Next
    End If
  End Sub

  ' PURPOSE: Check if flag grid has correct values
  '
  '    - INPUT:
  '       - FlagGrid : grid to check
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '       - Grid Status (OK, VALUE_NOT_VALID or NO_ONE_CHECKED)
  '
  Private Function IsFlagGridDataOK(ByRef FlagGrid As uc_grid, Optional ByRef FlagList As ArrayList = Nothing, Optional ByVal MaxFlags As Integer = -1) As Integer
    Dim _idx_row As Integer
    Dim _count_checked As Integer
    Dim _sum_count_flags As Integer
    Dim _info_flag As INFO_FLAG

    _count_checked = 0 ' Number of rows checked
    _sum_count_flags = 0 ' Number of flags required/assigned
    _info_flag = New INFO_FLAG()
    FlagList = New ArrayList()

    For _idx_row = 0 To FlagGrid.NumRows() - 1

      If FlagGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _count_checked = _count_checked + 1

        If String.IsNullOrEmpty(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value) Then
          ' Flag checked without a value
          FlagGrid.Redraw = False
          FlagGrid.ClearSelection()
          FlagGrid.IsSelected(_idx_row) = True
          FlagGrid.Redraw = True
          FlagGrid.Focus()
          Return GRID_DATA_STATUS.VALUE_NOT_VALID
        ElseIf GUI_ParseNumber(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value) < 1 Then
          ' Flag checked with value 0
          FlagGrid.Redraw = False
          FlagGrid.ClearSelection()
          FlagGrid.IsSelected(_idx_row) = True
          FlagGrid.Redraw = True
          FlagGrid.Focus()
          Return GRID_DATA_STATUS.VALUE_NOT_VALID
        ElseIf MaxFlags <> -1 Then
          _sum_count_flags = _sum_count_flags + GUI_ParseNumber(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value)
          If _sum_count_flags > MaxFlags Then
            ' overall number exceeded
            FlagGrid.Redraw = False
            FlagGrid.ClearSelection()
            FlagGrid.IsSelected(_idx_row) = True
            FlagGrid.Redraw = True
            FlagGrid.Focus()
            Return GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
          End If
        End If
        _info_flag.id = FlagGrid.Cell(_idx_row, GRID_FLAG_IDX).Value
        _info_flag.count = FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value
        _info_flag.name = FlagGrid.Cell(_idx_row, GRID_FLAG_NAME).Value
        _info_flag.color = IIf(FlagGrid.Cell(_idx_row, GRID_FLAG_ID_COLOR).Value.Length > 0, FlagGrid.Cell(_idx_row, GRID_FLAG_ID_COLOR).Value, 0)
        FlagList.Add(_info_flag)
      End If
    Next

    If _count_checked = 0 Then
      ' Active but without a flag selected
      FlagGrid.Redraw = False
      FlagGrid.ClearSelection()
      FlagGrid.SelectFirstRow(True)
      FlagGrid.Redraw = True
      Return GRID_DATA_STATUS.NO_ONE_CHECKED
    End If

    Return GRID_DATA_STATUS.OK
  End Function

  Private Sub InitAwardWithGameCombobox()
    Dim _promo_item As CLASS_PROMOTION
    Dim _game_type_list As List(Of PromoGame.GameType)


    _promo_item = DbReadObject
    _game_type_list = New List(Of PromoGame.GameType)

    _game_type_list.Add(PromoGame.GameType.PlayCash)

    cmb_award_with_game.DataSource = New PromoGame().GetByGameTypeListAndPromotionId(_game_type_list, _promo_item.PromotionId)
    cmb_award_with_game.DisplayMember = "Name"
    cmb_award_with_game.ValueMember = "Id"
  End Sub

  ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
  'Private Sub InitUcPyramizaion()
  '  Me.uc_pyramidal.Init()
  '  Me.uc_pyramidal.Visible = Me.m_is_pyramidal_dist_enabled
  'End Sub

  'Private Sub SetPyramidalDist(PyramidalDistData As String)
  '  If Me.m_is_pyramidal_dist_enabled Then
  '    Me.uc_pyramidal.Xml = PyramidalDistData
  '    Call Me.uc_pyramidal.SetScreemData()
  '  End If
  'End Sub

  'Private Function GetPyramidalDistData() As String

  '  If Me.uc_pyramidal.IsEnabled() Then
  '    Return Me.uc_pyramidal.Xml()
  '  End If

  '  Return String.Empty
  'End Function

#End Region ' Private

#Region " Events "

  Private Sub chk_special_permission_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_special_permission.CheckedChanged
    opt_special_permission_a.Enabled = chk_special_permission.Checked
    opt_special_permission_b.Enabled = chk_special_permission.Checked

    If Not chk_special_permission.Checked Then
      lbl_not_applicable_mb1.Visible = False
    Else
      If opt_special_permission_a.Checked Or _
         opt_special_permission_b.Checked Then
        lbl_not_applicable_mb1.Visible = m_mb_auto_promotion
      End If
    End If
  End Sub

  Private Sub opt_special_permission_a_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_special_permission_a.CheckedChanged
    lbl_not_applicable_mb1.Visible = m_mb_auto_promotion
  End Sub

  Private Sub opt_special_permission_b_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_special_permission_b.CheckedChanged
    lbl_not_applicable_mb1.Visible = m_mb_auto_promotion
  End Sub

  Private Sub chk_by_gender_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_gender.CheckedChanged
    opt_gender_male.Enabled = chk_by_gender.Checked
    opt_gender_female.Enabled = chk_by_gender.Checked
  End Sub

  Private Sub chk_by_birth_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_birth_date.CheckedChanged
    opt_birth_date_only.Enabled = chk_by_birth_date.Checked
    opt_birth_month_only.Enabled = chk_by_birth_date.Checked
  End Sub

  Private Sub chk_by_age_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_age.CheckedChanged
    opt_birth_include_age.Enabled = chk_by_age.Checked
    opt_birth_exclude_age.Enabled = chk_by_age.Checked
    If Not chk_by_age.Checked Then
      ef_age_from2.Enabled = False
      ef_age_from1.Enabled = False
      ef_age_to2.Enabled = False
      ef_age_to1.Enabled = False
      ef_age_from2.Value = ""
      ef_age_from1.Value = ""
      ef_age_to2.Value = ""
      ef_age_to1.Value = ""
    ElseIf opt_birth_include_age.Checked Then
      ef_age_from1.Enabled = True
      ef_age_to1.Enabled = True
    ElseIf opt_birth_exclude_age.Checked Then
      ef_age_from2.Enabled = True
      ef_age_to2.Enabled = True
    End If
  End Sub

  'FBA 08-OCT-2013 new age range filter
  Private Sub opt_include_exclude_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_birth_include_age.CheckedChanged, opt_birth_exclude_age.CheckedChanged
    ef_age_from2.Enabled = opt_birth_exclude_age.Checked
    ef_age_from1.Enabled = opt_birth_include_age.Checked
    ef_age_to2.Enabled = opt_birth_exclude_age.Checked
    ef_age_to1.Enabled = opt_birth_include_age.Checked
    If Not ef_age_from2.Enabled And Not ef_age_to2.Enabled And Not ef_age_from1.Enabled And Not ef_age_to1.Enabled Then
      ef_age_from1.Value = ""
      ef_age_to1.Value = ""
      ef_age_from2.Value = ""
      ef_age_to2.Value = ""
    ElseIf Not ef_age_from2.Enabled And Not ef_age_to2.Enabled Then
      ef_age_from2.Value = ""
      ef_age_to2.Value = ""
    ElseIf Not ef_age_from1.Enabled And Not ef_age_to1.Enabled Then
      ef_age_from1.Value = ""
      ef_age_to1.Value = ""
    End If
  End Sub

  ' AVZ 01-OCT-2015: new filter by created account date
  Private Sub chk_by_created_account_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_created_account.CheckedChanged
    opt_created_account_date.Enabled = chk_by_created_account.Checked
    opt_created_account_anniversary.Enabled = chk_by_created_account.Checked
    If chk_by_created_account.Checked Then
      EnableCreatedAccountDateOptions()
    Else
      opt_created_account_working_day_only.Enabled = False
      opt_created_account_working_day_plus.Enabled = False
      ef_created_account_working_day_plus.Enabled = False
      opt_created_account_anniversary_whole_month.Enabled = False
      opt_created_account_anniversary_day_month.Enabled = False
      chk_by_anniversary_range_of_years.Enabled = False
      ef_created_account_anniversary_year_from.Enabled = False
      ef_created_account_anniversary_year_to.Enabled = False
    End If
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub opt_created_account_date_CheckedChanged(sender As Object, e As EventArgs) Handles opt_created_account_date.CheckedChanged
    EnableCreatedAccountDateOptions()
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub opt_created_account_anniversary_CheckedChanged(sender As Object, e As EventArgs) Handles opt_created_account_anniversary.CheckedChanged
    EnableCreatedAccountDateOptions()
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub chk_by_anniversary_range_of_years_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_anniversary_range_of_years.CheckedChanged
    ef_created_account_anniversary_year_from.Enabled = chk_by_anniversary_range_of_years.Checked
    ef_created_account_anniversary_year_to.Enabled = chk_by_anniversary_range_of_years.Checked
  End Sub

  ' AVZ 01-OCT-2015
  Private Sub EnableCreatedAccountDateOptions()
    opt_created_account_working_day_only.Enabled = opt_created_account_date.Checked
    opt_created_account_working_day_plus.Enabled = opt_created_account_date.Checked
    ef_created_account_working_day_plus.Enabled = opt_created_account_date.Checked
    opt_created_account_anniversary_whole_month.Enabled = opt_created_account_anniversary.Checked
    opt_created_account_anniversary_day_month.Enabled = opt_created_account_anniversary.Checked
    chk_by_anniversary_range_of_years.Enabled = opt_created_account_anniversary.Checked
    ef_created_account_anniversary_year_from.Enabled = opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked
    ef_created_account_anniversary_year_to.Enabled = opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked
  End Sub

  Private Sub chk_by_level_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_level.CheckedChanged

    If chk_by_level.Checked Then
      chk_level_anonymous.Enabled = Not Me.opt_point.Checked
    Else
      chk_level_anonymous.Enabled = False
    End If

    chk_level_01.Enabled = chk_by_level.Checked
    chk_level_02.Enabled = chk_by_level.Checked
    chk_level_03.Enabled = chk_by_level.Checked
    chk_level_04.Enabled = chk_by_level.Checked
  End Sub

  Private Sub chk_by_freq_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_freq.CheckedChanged
    ef_freq_last_days.Enabled = chk_by_freq.Checked
    ef_freq_min_days.Enabled = chk_by_freq.Checked
    ef_freq_min_cash_in.Enabled = chk_by_freq.Checked
  End Sub

  Private Sub opt_expiration_days_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_expiration_days.CheckedChanged
    ef_expiration_days.Enabled = opt_expiration_days.Checked
  End Sub

  Private Sub opt_expiration_hours_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_expiration_hours.CheckedChanged
    ef_expiration_hours.Enabled = opt_expiration_hours.Checked
  End Sub

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Call RefreshLabelTexts()
  End Sub

  ' PURPOSE: Enables the text boxes related with the Won Lock
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub chk_won_lock_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_won_lock.CheckedChanged

    WonLockEnable(chk_won_lock.Checked)

  End Sub

  Private Sub opt_credit_non_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_non_redeemable.CheckedChanged

    chk_apply_tax.Visible = False

    ' AMF 29-AUG-2013
    If Not opt_credit_non_redeemable.Checked Then
      tabPromotions.TabPages.Remove(tab_restricted)
    Else
      If Not tabPromotions.Contains(tab_restricted) Then
        tabPromotions.TabPages.Add(tab_restricted)
      End If
    End If

    Call SwitchCredit(WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1)

  End Sub

  Private Sub opt_credit_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_redeemable.CheckedChanged


    If WSI.Common.Misc.ReadGeneralParams("Cashier", "Promotions.RE.Tax.Enabled") Then
      chk_apply_tax.Visible = True
    End If

    Call SwitchCredit(WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)

  End Sub

  Private Sub opt_point_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_point.CheckedChanged

    chk_apply_tax.Visible = False

    Call SwitchCredit(WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT)
  End Sub

  Private Sub chk_flags_awarded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_flags_awarded.CheckedChanged
    SetFlagsGridStatus(Me.dg_flags_awarded, chk_flags_awarded.Checked)
  End Sub


  Private Sub chk_by_flag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_flag.CheckedChanged
    SetFlagsGridStatus(Me.dg_flags_required, chk_by_flag.Checked)
    btn_Accounts_Afected.Enabled = chk_by_flag.Checked
  End Sub

  ' PURPOSE: Set checked/unchecked when grid flag count change value
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub dg_flags_awarded_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_flags_awarded.CellDataChangedEvent

    If dg_flags_awarded.Redraw = False Then
      Return
    End If

    If Column = GRID_FLAG_COUNT Then
      If String.IsNullOrEmpty(dg_flags_awarded.Cell(Row, GRID_FLAG_COUNT).Value) Then
        dg_flags_awarded.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_flags_awarded.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If

  End Sub

  ' PURPOSE: Set checked/unchecked when grid flag count change value
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub dg_flags_required_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_flags_required.CellDataChangedEvent

    If dg_flags_required.Redraw = False Then
      Return
    End If

    If Column = GRID_FLAG_COUNT Then
      If String.IsNullOrEmpty(dg_flags_required.Cell(Row, GRID_FLAG_COUNT).Value) Then
        dg_flags_required.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_flags_required.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If
  End Sub

  Private Sub chk_visible_on_PromoBOX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_visible_on_PromoBOX.CheckedChanged

    'Me.img_icon.Enabled = Me.chk_visible_on_PromoBOX.Checked
    'Me.img_image.Enabled = Me.chk_visible_on_PromoBOX.Checked
    img_image.Enabled = chk_visible_on_PromoBOX.Checked
  End Sub

  Private Sub chk_expiration_limit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_expiration_limit.CheckedChanged
    dtp_expiration_limit.Enabled = chk_expiration_limit.Checked
  End Sub

  ' PURPOSE: Calculate used credit for promotion (once)
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub tab_limit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tab_limit.SelectedIndexChanged
    Dim _promotion_credit_info As Promotion.TYPE_PROMO_CREDIT_USED
    Dim promo_item As CLASS_PROMOTION

    Dim _global_daily_available As String
    Dim _global_month_available As String
    Dim _global_available As String

    promo_item = DbReadObject

    Select Case tab_limit.SelectedIndex
      Case 1

        If m_consumed_info_loaded = False Then
          _promotion_credit_info = New Promotion.TYPE_PROMO_CREDIT_USED()

          Using _db_trx As New DB_TRX()

            If Promotion.GetUsedCreditPromotion(m_promotion_id, 0, False, _promotion_credit_info, _db_trx.SqlTransaction) Then

              If Me.opt_point.Checked Then
                ' Consumed
                ef_promotion_consumed_day.TextValue = GUI_FormatNumber(_promotion_credit_info.global_daily_credit_used, 2)
                ef_promotion_consumed_month.TextValue = GUI_FormatNumber(_promotion_credit_info.global_monthly_credit_used, 2)
                ef_promotion_consumed_global.TextValue = GUI_FormatNumber(_promotion_credit_info.global_credit_used, 2)

                ' Avaliable
                _global_daily_available = GUI_FormatNumber(promo_item.GlobalDailyLimit - _promotion_credit_info.global_daily_credit_used, 2)
                _global_month_available = GUI_FormatNumber(promo_item.GlobalMonthlyLimit - _promotion_credit_info.global_monthly_credit_used, 2)
                _global_available = GUI_FormatNumber(promo_item.GlobalLimit - _promotion_credit_info.global_credit_used, 2)

              Else
                ' Consumed
                ef_promotion_consumed_day.TextValue = GUI_FormatCurrency(_promotion_credit_info.global_daily_credit_used, 2)
                ef_promotion_consumed_month.TextValue = GUI_FormatCurrency(_promotion_credit_info.global_monthly_credit_used, 2)
                ef_promotion_consumed_global.TextValue = GUI_FormatCurrency(_promotion_credit_info.global_credit_used, 2)

                ' Avaliable
                _global_daily_available = GUI_FormatCurrency(promo_item.GlobalDailyLimit - _promotion_credit_info.global_daily_credit_used, 2)
                _global_month_available = GUI_FormatCurrency(promo_item.GlobalMonthlyLimit - _promotion_credit_info.global_monthly_credit_used, 2)
                _global_available = GUI_FormatCurrency(promo_item.GlobalLimit - _promotion_credit_info.global_credit_used, 2)

              End If

              ef_promotion_available_day.TextValue = IIf(GUI_ParseCurrency(_global_daily_available) <= 0, GUI_FormatCurrency(0, 2), _global_daily_available)
              ef_promotion_available_month.TextValue = IIf(GUI_ParseCurrency(_global_month_available) <= 0, GUI_FormatCurrency(0, 2), _global_month_available)
              ef_promotion_available_global.TextValue = IIf(GUI_ParseCurrency(_global_available) <= 0, GUI_FormatCurrency(0, 2), _global_available)

              m_consumed_info_loaded = True

              ' Consumido
              ef_promotion_consumed_day.IsReadOnly = True
              ef_promotion_consumed_day.Enabled = True
              ef_promotion_consumed_month.IsReadOnly = True
              ef_promotion_consumed_month.Enabled = True
              ef_promotion_consumed_global.IsReadOnly = True
              ef_promotion_consumed_global.Enabled = True

              ' Disponible
              ef_promotion_available_day.IsReadOnly = True
              ef_promotion_available_day.Enabled = True
              ef_promotion_available_month.IsReadOnly = True
              ef_promotion_available_month.Enabled = True
              ef_promotion_available_global.IsReadOnly = True
              ef_promotion_available_global.Enabled = True

            End If

          End Using
        End If

    End Select
  End Sub

  Private Sub ef_promotion_limit_day_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles ef_promotion_limit_day.Leave
    ' 19-NOV-2013  JCA    Fixed Bug WIG-416: Error when leaving blank the limit
    ef_promotion_available_day.Value = GetLimit(ef_promotion_limit_day.Value, ef_promotion_consumed_day.Value)
  End Sub

  Private Sub ef_promotion_limit_global_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles ef_promotion_limit_global.Leave
    ' 19-NOV-2013  JCA    Fixed Bug WIG-416: Error when leaving blank the limit
    ef_promotion_available_global.Value = GetLimit(ef_promotion_limit_global.Value, ef_promotion_consumed_global.Value)
  End Sub

  Private Sub ef_promotion_limit_month_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles ef_promotion_limit_month.Leave
    ' 19-NOV-2013  JCA    Fixed Bug WIG-416: Error when leaving blank the limit
    ef_promotion_available_month.Value = GetLimit(ef_promotion_limit_month.Value, ef_promotion_consumed_month.Value)
  End Sub

  Private Sub btn_Accounts_Afected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Accounts_Afected.Click
    Dim _frm As frm_account_flags
    Dim _idx As Integer
    Dim _have_values As Boolean
    Dim _aux As Integer
    Dim _flag_list As ArrayList

    _have_values = False
    _aux = 0
    _flag_list = Nothing

    Windows.Forms.Cursor.Current = Cursors.Default

    Select Case IsFlagGridDataOK(dg_flags_required, _flag_list, MAX_FLAGS_AWARDED)
      Case GRID_DATA_STATUS.VALUE_NOT_VALID ' Flag checked without a count
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1399), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return

      Case GRID_DATA_STATUS.NO_ONE_CHECKED ' No one flag checked
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return

      Case GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1417), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414).ToLower, MAX_FLAGS_AWARDED)
        Return

      Case GRID_DATA_STATUS.OK
      Case Else
    End Select

    _frm = New frm_account_flags

    'SET LEVELS AND DATETABLE
    _frm.FlagsAccountLevelsAdd(IIf(Not chk_by_level.Checked, False, Me.chk_level_01.Checked), _
                              IIf(Not chk_by_level.Checked, False, Me.chk_level_02.Checked), _
                              IIf(Not chk_by_level.Checked, False, Me.chk_level_03.Checked), _
                              IIf(Not chk_by_level.Checked, False, Me.chk_level_04.Checked), _
                              IIf(Not chk_level_anonymous.Checked, False, Me.chk_level_anonymous.Checked))


    For Each _flag As INFO_FLAG In _flag_list
      'SET FLAGS
      _frm.FlagsAccountAdd(_idx, _flag.id, _flag.count, _flag.name, IIf(_flag.color.ToString().Length > 0, _flag.color, 0))
    Next

    If _flag_list.Count > 0 Then
      _frm.ShowForEdit(Me.MdiParent, True)
    Else
      Call _frm.Dispose()
      _frm = Nothing
    End If

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  ' PURPOSE: Event on change checked state on AwardOnPromoBox
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub chk_award_on_PromoBOX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_award_on_PromoBOX.CheckedChanged, chk_award_on_InTouch.CheckedChanged
    chk_visible_on_PromoBOX.Enabled = chk_award_on_PromoBOX.Checked
    If Not chk_award_on_PromoBOX.Checked Then
      chk_visible_on_PromoBOX.Checked = False
    End If
    img_icon.Enabled = chk_award_on_PromoBOX.Checked Or chk_award_on_InTouch.Checked
    Call UpdatePromoBOXTabState()
  End Sub

  ' PURPOSE: Event to control the TextOnPromoBOX field value and update the PromoBOX tab state
  '
  '  PARAMS:
  '     - INPUT:
  '           - sender:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub ef_text_on_PromoBOX_EntryFieldValueChanged() Handles ef_text_on_PromoBOX.EntryFieldValueChanged
    Call UpdatePromoBOXTabState()
  End Sub

  ' 19-NOV-2013  JCA    Fixed Bug WIG-416: Error when leaving blank the limit
  ' PURPOSE: Returns the remainder if positive returns zero otherwise
  '
  '  PARAMS:
  '     - INPUT:
  '           - QuantityOne:
  '           - QuantityTwo:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - the remainder if positive returns zero otherwise
  Private Function GetLimit(ByVal QuantityOne As String, ByVal QuantityTwo As String) As Decimal
    Dim _quantityone As Decimal
    Dim _quantitytwo As Decimal

    Decimal.TryParse(QuantityOne, _quantityone)
    Decimal.TryParse(QuantityTwo, _quantitytwo)

    Return Math.Max(_quantityone - _quantitytwo, 0)

  End Function

#End Region ' Events 

  Private Sub tabPromotions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabPromotions.SelectedIndexChanged

    Dim _parent_panel As Panel

    uc_terminals_played_group_filter.Visible = (tabPromotions.SelectedIndex = tabPromotions.TabPages.IndexOf(tab_tokens))
    uc_terminals_group_filter.Visible = (tabPromotions.SelectedIndex = tabPromotions.TabPages.IndexOf(tab_spent))
    uc_terminals_group_filter_restricted.Visible = (tabPromotions.SelectedIndex = tabPromotions.TabPages.IndexOf(tab_restricted))

    ' To have into account the scroll position
    _parent_panel = TryCast(uc_terminals_played_group_filter.Parent, Panel)
    uc_terminals_played_group_filter.Location = New Point(uc_terminals_played_group_filter.Location.X, _parent_panel.AutoScrollPosition.Y + m_uc_terminals_played_group_filter_init_Y_pos)

    _parent_panel = TryCast(uc_terminals_group_filter.Parent, Panel)
    uc_terminals_group_filter.Location = New Point(uc_terminals_group_filter.Location.X, _parent_panel.AutoScrollPosition.Y + m_uc_terminals_group_filter_init_Y_pos)

    _parent_panel = TryCast(uc_terminals_group_filter_restricted.Parent, Panel)
    uc_terminals_group_filter_restricted.Location = New Point(uc_terminals_group_filter_restricted.Location.X, _parent_panel.AutoScrollPosition.Y + m_uc_terminals_group_filter_restricted_init_Y_pos)

  End Sub

  Private Sub chk_award_with_game_CheckedChanged(sender As Object, e As EventArgs) Handles chk_award_with_game.CheckedChanged
    Me.cmb_award_with_game.Enabled = Me.chk_award_with_game.Checked

    If Not Me.chk_award_with_game.Checked Then
      Me.cmb_award_with_game.SelectedIndex = -1
    End If

  End Sub

  Private Sub frm_promotion_edit_Load(sender As Object, e As EventArgs) Handles Me.Load
    chk_visible_on_PromoBOX.Enabled = Me.chk_award_on_PromoBOX.Checked
    If Not chk_award_on_PromoBOX.Checked Then
      chk_visible_on_PromoBOX.Enabled = False
    End If
    img_icon.Enabled = chk_award_on_PromoBOX.Checked Or chk_award_on_InTouch.Checked
  End Sub

End Class
