﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_cash_monitor
'   DESCRIPTION : Cashier an MB cash monitor
'        AUTHOR : David Rigal
' CREATION DATE : 20-JUL-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-JUL-2015  DRV    Initial version. Backlog Item 1415
' 19-AGO-2015  FJC    Backlog Item 3740 (pre-Sprint)
' 02-DIC-2015  FAV    Bug 6992: Fixed error in build the Where clause
' 02-MAR-2016  RAB    Bug 10154:Monitor de efectivo: No funciona el filtro de usuarios por "Varios"
' 07-MAR-2016  RAB    Bug 10322:Monitor cash: filter "less cash to" not working properly
' 19-APR-2016  ETP    Bug 11868: Logrand new checkbox and features.
' 20-APR-2016  ETP    Bug 11868 & 12206: set invisible to checkbox & Error in filters when is Tito mode
' 21-APR-2016  AMF    PBI 10953:CountR - Cash It Out: GUI - Envío/Recaudación de CountR - Modificación frm_cage_control
' 28-APR-2016  ETP    Bug 11868: Corrected MB order (by Name)
' 26-JUL-2017  MS     WIGOS-3594: Errors in Cash Monitor screen and exporting data to Excel
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Xml.Serialization

Public Class frm_cash_monitor
  Inherits frm_base_sel

#Region " Structs "

  Public Structure MBAndCashierAccounts
    Public MBId As Int64
    Public MBName As String
  End Structure

  Public Structure UsersGridRowID
    Public ID As Int64
    Public Terminal As String
    Public RowType As UserGridRowType
  End Structure

#End Region ' Structs

#Region " Enums "
  Public Enum UserGridRowType

    USERS_GRID_TYPE_CASHIER_USER = 0
    USERS_GRID_TYPE_MB_USER = 1
    USERS_GRID_TYPE_HEADER = 2

  End Enum
#End Region ' Enums

#Region " Constants "

  Private Const USERS_GRID_NUM_COLUMNS = 4
  Private Const USERS_GRID_ID = 0
  Private Const USERS_GRID_ID_WIDTH = 1
  Private Const USERS_GRID_CHECKED = 1
  Private Const USERS_GRID_CHECKED_WIDTH = 400
  Private Const USERS_GRID_DESCRIPTION = 2
  Private Const USERS_GRID_DESCRIPTION_WIDTH = 2910
  Private Const USERS_GRID_ROW_TYPE = 3
  ' Left padding for data rows in the mov. types data grid
  Private Const USERS_GRID_TAB As String = "     "

  Private Const GRID_NUM_COLUMNS = 13
  Private Const GRID_NUM_HEADERS = 2

  Private Const GRID_COLUMN_SELECT = 0
  Private Const GRID_COLUMN_USER_NAME = 1
  Private Const GRID_COLUMN_USER_FULL_NAME = 2
  Private Const GRID_COLUMN_USER_EMPLOYEE_CODE = 3
  Private Const GRID_COLUMN_CASHIER_BALANCE = 4
  Private Const GRID_COLUMN_CASHIER_NAME = 5
  Private Const GRID_COLUMN_MB_CASH = 6
  Private Const GRID_COLUMN_MB_RECHARGE_LIMIT = 7
  Private Const GRID_COLUMN_MB_NUM_RECHARGES = 8
  Private Const GRID_COLUMN_MB_RECHARGE_WORKING_DAY = 9
  Private Const GRID_COLUMN_MB_LAST_ACTIVITY = 10
  Private Const GRID_COLUMN_ID = 11
  Private Const GRID_COLUMN_ROW_TYPE = 12

  ' Column WIDTH
  Private Const GRID_COLUMN_WIDTH_SELECT = 200
  Private Const GRID_COLUMN_WIDTH_USER_NAME = 2000
  Private Const GRID_COLUMN_WIDTH_USER_FULL_NAME = 2500
  Private Const GRID_COLUMN_WIDTH_USER_EMPLOYEE_CODE = 2000
  Private Const GRID_COLUMN_WIDTH_CASHIER_BALANCE = 2000
  Private Const GRID_COLUMN_WIDTH_CASHIER_NAME = 2000
  Private Const GRID_COLUMN_WIDTH_MB_CASH = 2000
  Private Const GRID_COLUMN_WIDTH_MB_RECHARGE_LIMIT = 2000
  Private Const GRID_COLUMN_WIDTH_MB_NUM_RECHARGES = 2000
  Private Const GRID_COLUMN_WIDTH_MB_RECHARGE_WORKING_DAY = 2000
  Private Const GRID_COLUMN_WIDTH_MB_LAST_ACTIVITY = 2000
  Private Const GRID_COLUMN_WIDTH_ID = 0

  Private Const SQL_COLUMN_ROW_TYPE = 0
  Private Const SQL_COLUMN_ROW_ID = 1
  Private Const SQL_COLUMN_USR_NAME = 2
  Private Const SQL_COLUMN_USR_FULL_NAME = 3
  Private Const SQL_COLUMN_EMPLOYEE_CODE = 4
  Private Const SQL_COLUMN_CASHIER_BALANCE = 5
  Private Const SQL_COLUMN_MB_CASH = 6
  Private Const SQL_COLUMN_MB_RECHARGE_LIMIT = 7
  Private Const SQL_COLUMN_MB_NUM_RECHARGES = 8
  Private Const SQL_COLUMN_MB_LAST_ACTIVITY = 9
  Private Const SQL_COLUMN_CASHIER_NAME = 10
  Private Const SQL_COLUMN_MB_RECHARGE_WORKING_DAY = 11

  Private Const MB_ROW_TYPE = 1
  Private Const CASHIER_ROW_TYPE = 0

  Private Const FILTER_VALUE_NOTHING = "---"

#End Region 'Constants

#Region " Members "

  Private m_refresh_grid As Boolean = True
  Private m_first_time As Boolean = True
  Private m_selected_ids As List(Of UsersGridRowID)

  Private m_total_cashier_balance As Decimal
  Private m_total_mb_limit As Decimal
  Private m_total_mb_cash As Decimal
  Private m_total_mb_num_rechareges As Integer
  Private m_total_mb_amount_recharges As Decimal

  Private m_filter As CashMonitorFilter
  Private m_user_filter As String
  Private m_limit_mb_filter As String
  Private m_limit_cashier_filter As String
  Private m_first_time_search As Boolean
  Private m_exclude_no_cash As String
  Private m_exclude_no_limit_mb As String

  Private m_is_tito_mode As Boolean

#End Region 'Members

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASH_MONITOR

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _mb_accounts As List(Of MBAndCashierAccounts)
    Dim _cashier_accounts As List(Of MBAndCashierAccounts)

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6612)

    m_is_tito_mode = TITO.Utils.IsTitoMode()
    Me.m_first_time_search = True

    If m_is_tito_mode Then
      Me.Size = New Size(950, 700)
    End If

    Me.uc_cash_users.Size = New Size(242, 132)

    _mb_accounts = New List(Of MBAndCashierAccounts)
    _cashier_accounts = New List(Of MBAndCashierAccounts)

    Call GetMBUsers(_mb_accounts)
    Call GetCashierUsers(_cashier_accounts)
    Call FillUsersGrid(_mb_accounts, _cashier_accounts)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    Me.gb_cash_users.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
    Me.opt_several_users.Text = GLB_NLS_GUI_STATISTICS.GetString(211)
    Me.opt_all_users.Text = GLB_NLS_GUI_STATISTICS.GetString(206)

    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)

    Me.gb_mb_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
    Me.gb_cashier_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616)

    Me.ef_mb_recharge_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6613)
    Me.ef_mb_recharge_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)

    Me.ef_cashier_recharge_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6658)
    Me.ef_cashier_recharge_limit.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)

    Me.lbl_mb_limit_color.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
    Me.lbl_cashier_limit_color.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)

    If m_is_tito_mode Then
      Me.gb_mb_limit.Visible = False
      Me.chk_exclude_no_limit_mb.Visible = False
      Me.gb_options.Location = Me.gb_mb_limit.Location
    End If

    Me.tmr_monitor.Start()

    Call GUI_StyleSheet()

    Call SetFilterValues()

    GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.gb_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421)
    Me.chk_exclude_no_cash.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7280)
    Me.chk_exclude_no_limit_mb.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7281)

    IgnoreRowHeightFilterInExcelReport = True

    'Center Screen
    Me.CenterToScreen()

  End Sub

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.gb_cash_users

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    If m_refresh_grid Then
      GUI_StyleSheet()
    End If

    m_total_cashier_balance = 0
    m_total_mb_limit = 0
    m_total_mb_cash = 0
    m_total_mb_num_rechareges = 0
    m_total_mb_amount_recharges = 0

    m_refresh_grid = False
  End Sub

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx As Integer

    _idx = Me.Grid.AddRow()

    Me.Grid.Cell(_idx, GRID_COLUMN_USER_EMPLOYEE_CODE).Value = GLB_NLS_GUI_INVOICING.GetString(205)
    Me.Grid.Cell(_idx, GRID_COLUMN_CASHIER_BALANCE).Value = GUI_FormatCurrency(m_total_cashier_balance)
    Me.Grid.Cell(_idx, GRID_COLUMN_MB_RECHARGE_LIMIT).Value = GUI_FormatCurrency(m_total_mb_limit)
    Me.Grid.Cell(_idx, GRID_COLUMN_MB_CASH).Value = GUI_FormatCurrency(m_total_mb_cash)
    Me.Grid.Cell(_idx, GRID_COLUMN_MB_NUM_RECHARGES).Value = GUI_FormatNumber(m_total_mb_num_rechareges, 0)
    Me.Grid.Cell(_idx, GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Value = GUI_FormatCurrency(m_total_mb_amount_recharges)

    Me.Grid.Row(_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Me.Grid.Redraw = True

    MyBase.GUI_AfterLastRow()
  End Sub

  ' PURPOSE: Overrides the clear grid routine
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Sub GUI_ClearGrid()
    If m_first_time Then
      Call MyBase.GUI_ClearGrid()
    End If
  End Sub ' GUI_ClearGrid

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _is_cashier_session As Boolean

    ' Column select
    Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_USR_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = DbRow.Value(SQL_COLUMN_USR_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_NAME).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_USR_FULL_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_FULL_NAME).Value = DbRow.Value(SQL_COLUMN_USR_FULL_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_FULL_NAME).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_EMPLOYEE_CODE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_EMPLOYEE_CODE).Value = DbRow.Value(SQL_COLUMN_EMPLOYEE_CODE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_USER_EMPLOYEE_CODE).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ROW_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ROW_ID)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ROW_TYPE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_TYPE).Value = DbRow.Value(SQL_COLUMN_ROW_TYPE)

      _is_cashier_session = (DbRow.Value(SQL_COLUMN_ROW_TYPE) = UserGridRowType.USERS_GRID_TYPE_CASHIER_USER)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ROW_TYPE).Value = ""

      Return False
    End If

    ' Recharge Limit Cashier
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CASHIER_BALANCE)) AndAlso _is_cashier_session Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASHIER_BALANCE))
      If Not Me.chk_cashier_recharge_limit.Checked AndAlso DbRow.Value(SQL_COLUMN_CASHIER_BALANCE) < GUI_ParseCurrency(Me.ef_cashier_recharge_limit.Value) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      End If
      m_total_cashier_balance += DbRow.Value(SQL_COLUMN_CASHIER_BALANCE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_BALANCE).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_CASHIER_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CASHIER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MB_CASH)) AndAlso Not _is_cashier_session Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_CASH).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_CASH))
      m_total_mb_cash += DbRow.Value(SQL_COLUMN_MB_CASH)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_CASH).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MB_NUM_RECHARGES)) AndAlso Not _is_cashier_session Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_NUM_RECHARGES).Value = DbRow.Value(SQL_COLUMN_MB_NUM_RECHARGES)
      m_total_mb_num_rechareges += DbRow.Value(SQL_COLUMN_MB_NUM_RECHARGES)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_NUM_RECHARGES).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MB_RECHARGE_WORKING_DAY)) AndAlso Not _is_cashier_session Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_RECHARGE_WORKING_DAY))
      m_total_mb_amount_recharges += DbRow.Value(SQL_COLUMN_MB_RECHARGE_WORKING_DAY)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Value = ""
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MB_LAST_ACTIVITY)) AndAlso Not _is_cashier_session Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LAST_ACTIVITY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_MB_LAST_ACTIVITY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_LAST_ACTIVITY).Value = ""
    End If

    ' Recharge Limit MB
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MB_RECHARGE_LIMIT)) AndAlso Not _is_cashier_session Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_MB_RECHARGE_LIMIT))
      If Not Me.chk_mb_recharge_limit.Checked AndAlso DbRow.Value(SQL_COLUMN_MB_RECHARGE_LIMIT) < GUI_ParseCurrency(Me.ef_mb_recharge_limit.Value) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_01)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      End If
      m_total_mb_limit += DbRow.Value(SQL_COLUMN_MB_RECHARGE_LIMIT)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).Value = ""
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MB_RECHARGE_LIMIT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    End If

    Me.Size = Me.Size
    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Define the ExecuteQuery customized
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _function_name As String
    Dim _max_rows As Integer
    Dim _table As DataTable
    Dim _idx_row As Integer
    Dim _count As Integer
    Dim _more_than_max As Boolean
    Dim _db_row As CLASS_DB_ROW
    Dim _grid_rows As Integer
    Dim _has_changed As Boolean

    _table = Nothing
    _function_name = "GUI_ExecuteQueryCustom"
    _has_changed = False
    _max_rows = GUI_MaxRows()

    Try

      Me.tmr_monitor.Stop()

      SaveSelectedGridIds()

      _table = GUI_GetCustomDataTable()

      Call GUI_ReportUpdateFilters()

      If _table Is Nothing Then

        Return
      End If

      _grid_rows = _table.Rows.Count

      _more_than_max = False

      Call GUI_BeforeFirstRow()

      _count = 0
      _idx_row = 0

      For _idx_row = 0 To _table.Rows.Count - 1

        If _idx_row >= Me.Grid.NumRows Then
          Me.Grid.AddRow()
        End If

        _db_row = New CLASS_DB_ROW(_table.Rows(_idx_row))
        Me.Grid.Redraw = False

        If GUI_SetupRow(_idx_row, _db_row) Then
          Me.Grid.Redraw = True
        End If

      Next

      While _idx_row < Me.Grid.NumRows
        Me.Grid.DeleteRow(_idx_row)
      End While

      Call GUI_AfterLastRow()

      SelectLastSelectedRow()

      If _more_than_max Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(111), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, , , CStr(_max_rows))
      End If

    Catch ex As Exception
      ' An error has occurred in the query execution
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Cash Monitor", _
                            _function_name, _
                            ex.Message)
      Me.Grid.Redraw = True
    Finally

      Me.tmr_monitor.Start()
    End Try

  End Sub ' GUI_ExecuteQueryCustom

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Indicates if to select the first row of the grid or not
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean
  '
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Dim _select_first As Boolean

    _select_first = m_first_time
    m_first_time = False

    Return _select_first
  End Function ' GUI_SelectFirstRow

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean
    If m_first_time_search Then
      If AuditType = AUDIT_FLAGS.ACCESS Then
        m_first_time_search = False
      End If

      Return True
    Else
      Return AuditType <> AUDIT_FLAGS.SEARCH
    End If

  End Function ' GUI

  ' PURPOSE: Get custom DataTable
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DataTable
  Protected Overrides Function GUI_GetCustomDataTable() As DataTable

    Dim _ds As DataSet
    Dim _dw As DataView
    Dim _sb As System.Text.StringBuilder
    Dim _value As String
    Dim _account_filter As String
    Dim _account_sort As String
    Dim _mb_filter As String
    Dim _cashier_filter As String
    Dim _sr_xml As IO.StringReader
    Dim _sr_schema As IO.StringReader
    Dim _row_filter As String

    _ds = New DataSet()
    _dw = New DataView()
    _sb = New System.Text.StringBuilder()
    _value = String.Empty
    _account_filter = String.Empty
    _account_sort = String.Empty
    _sr_schema = Nothing
    _sr_xml = Nothing
    _cashier_filter = ""
    _mb_filter = ""

    Try

      _sb.AppendLine(" SELECT   MD_DATA      ")
      _sb.AppendLine("        , MD_SCHEMA    ")
      _sb.AppendLine("   FROM   MONITOR_DATA ")
      _sb.AppendLine("  WHERE   MD_TYPE = 2  ")

      Using _db_trx As DB_TRX = New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _data_reader As SqlDataReader = _cmd.ExecuteReader()
            If _data_reader.Read() Then
              _sr_xml = New IO.StringReader(_data_reader.GetString(0))
              _sr_schema = New IO.StringReader(_data_reader.GetString(1))
            End If

          End Using

          If _sr_schema IsNot Nothing And _sr_xml IsNot Nothing Then
            _ds.ReadXmlSchema(_sr_schema)
            _ds.ReadXml(_sr_xml, XmlReadMode.Auto)
            _dw = _ds.Tables(0).DefaultView

            _account_sort = "ROW_TYPE, USR_NAME"
            _dw.Sort = _account_sort

            'RAB 15/03/2016: Bug 10154
            If (RowTypeExistIntoCashUsers(UserGridRowType.USERS_GRID_TYPE_CASHIER_USER)) Then
              _cashier_filter = GetSelectedUsersByType(UserGridRowType.USERS_GRID_TYPE_CASHIER_USER)
            End If

            If (RowTypeExistIntoCashUsers(UserGridRowType.USERS_GRID_TYPE_MB_USER)) Then
              _mb_filter = GetSelectedUsersByType(UserGridRowType.USERS_GRID_TYPE_MB_USER)
            End If


            ' Limit recharge MB
            If (GUI_ParseCurrency(ef_mb_recharge_limit.TextValue) > 0) Then
              _mb_filter = _mb_filter & IIf(chk_mb_recharge_limit.Checked, " AND MB_RECHARGE_LIMIT < " & GUI_ParseCurrency(ef_mb_recharge_limit.TextValue), "")
            End If

            ' Limit recharge Cashier
            If (GUI_ParseCurrency(ef_cashier_recharge_limit.TextValue) > 0) Then
              _cashier_filter = _cashier_filter & IIf(chk_cashier_recharge_limit.Checked, " AND CASHIER_BALANCE < " & GUI_ParseCurrency(ef_cashier_recharge_limit.TextValue), "")
            End If

            If (chk_exclude_no_cash.Checked) Then

              _cashier_filter = _cashier_filter & " AND CASHIER_BALANCE <> 0"
            End If

            If (chk_exclude_no_limit_mb.Checked) Then

              _mb_filter = _mb_filter & " AND MB_RECHARGE_LIMIT <> 0"

            End If

            ' Mount filter several or all
            If Not _mb_filter.Equals("") Then
              _row_filter = "((" & _cashier_filter & ") OR (" & _mb_filter & "))"
            Else
              _row_filter = _cashier_filter
            End If

            _dw.RowFilter = _row_filter

            Return _dw.ToTable()
          End If

        End Using
      End Using

    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

    Return New DataTable()

  End Function 'GUI_GetCustomDataTable


#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim str_check_filter As String

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(603), m_user_filter)

    ' Cashier
    If m_limit_cashier_filter <> String.Empty Then
      str_check_filter = IIf(Me.chk_cashier_recharge_limit.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6664), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6663))
      PrintData.SetFilter(str_check_filter & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6658).ToLower, m_limit_cashier_filter)
    End If

    ' MB
    If Not m_is_tito_mode Then
      If m_limit_mb_filter <> String.Empty Then
        str_check_filter = IIf(Me.chk_mb_recharge_limit.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6664), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6663))
        PrintData.SetFilter(str_check_filter & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6613).ToLower, m_limit_mb_filter)
      End If
    End If


    ' Exclude no cash
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7280), m_exclude_no_cash)

    ' Exclude no limit MB
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7281), m_exclude_no_limit_mb)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_user_filter = IIf(opt_all_users.Checked, opt_all_users.Text, opt_several_users.Text)

    ' Recharge Cashier
    If String.IsNullOrEmpty(ef_cashier_recharge_limit.TextValue) OrElse GUI_FormatCurrency(ef_cashier_recharge_limit.TextValue) <= 0 Then
      m_limit_cashier_filter = FILTER_VALUE_NOTHING
    Else
      m_limit_cashier_filter = GUI_FormatCurrency(ef_cashier_recharge_limit.TextValue)
    End If

    ' Recharge MB
    If String.IsNullOrEmpty(ef_mb_recharge_limit.TextValue) OrElse GUI_FormatCurrency(ef_mb_recharge_limit.TextValue) <= 0 Then
      m_limit_mb_filter = FILTER_VALUE_NOTHING
    Else
      m_limit_mb_filter = GUI_FormatCurrency(ef_mb_recharge_limit.TextValue)
    End If

    ' Exclude no cash
    m_exclude_no_cash = IIf(Me.chk_exclude_no_cash.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

    ' Exclude no limit MB
    m_exclude_no_limit_mb = IIf(Me.chk_exclude_no_limit_mb.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(359), GLB_NLS_GUI_PLAYER_TRACKING.GetString(360))

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region 'OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    Call SaveSelectedUserIds()
    ' MB
    m_filter.m_mb_recharge_limit = GUI_ParseCurrency(ef_mb_recharge_limit.TextValue)
    m_filter.m_chk_mb_recharge_limit = chk_mb_recharge_limit.Checked

    ' Cashier
    m_filter.m_cashier_recharge_limit = GUI_ParseCurrency(ef_cashier_recharge_limit.TextValue)
    m_filter.m_chk_cashier_recharge_limit = chk_cashier_recharge_limit.Checked

    m_filter.m_chk_exclude_no_cash = chk_exclude_no_cash.Checked
    m_filter.m_chk_exclude_no_limit_mb = False

    m_filter.SaveFilter(CurrentUser.Id, ENUM_FORM.FORM_CASH_MONITOR)

    MyBase.GUI_Closing(CloseCanceled)
  End Sub
#End Region 'Public Functions

#Region " Private Functions "

  ' PURPOSE: Reads and set the saved filters
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDBFilters()
    Dim _id As UsersGridRowID
    _id = New UsersGridRowID()
    m_filter = New CashMonitorFilter()
    m_filter = m_filter.GetFilter(CurrentUser.Id, ENUM_FORM.FORM_CASH_MONITOR)

    If m_filter Is Nothing Then
      SetDefaultValues()

      Return
    End If
    chk_mb_recharge_limit.Checked = m_filter.m_chk_mb_recharge_limit
    ef_mb_recharge_limit.TextValue = GUI_FormatCurrency(m_filter.m_mb_recharge_limit)

    chk_cashier_recharge_limit.Checked = m_filter.m_chk_cashier_recharge_limit
    ef_cashier_recharge_limit.TextValue = GUI_FormatCurrency(m_filter.m_cashier_recharge_limit)

    chk_exclude_no_cash.Checked = m_filter.m_chk_exclude_no_cash
    chk_exclude_no_limit_mb.Checked = False

    Me.opt_all_users.Checked = True
    If m_filter.m_users_list IsNot Nothing AndAlso m_filter.m_users_list.Count > 0 Then
      Me.opt_several_users.Checked = True
      For _idx As Integer = 0 To Me.uc_cash_users.NumRows - 1
        If Me.uc_cash_users.Cell(_idx, USERS_GRID_ROW_TYPE).Value <> UserGridRowType.USERS_GRID_TYPE_HEADER Then
          _id.ID = Me.uc_cash_users.Cell(_idx, USERS_GRID_ID).Value
          _id.RowType = Me.uc_cash_users.Cell(_idx, USERS_GRID_ROW_TYPE).Value

          If m_filter.m_users_list.Contains(_id) Then
            Me.uc_cash_users.Cell(_idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
            m_refresh_grid = False
            Call uc_cash_users_CellDataChangedEvent(_idx, USERS_GRID_CHECKED)
          End If
        End If
      Next
    End If
  End Sub

  'PURPOSE: return SQL filter for the specifid user type
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Private Function GetSelectedUsersByType(ByVal RowType As UserGridRowType) As String
    Dim _sb As StringBuilder
    Dim _has_values As Boolean = False

    _sb = New StringBuilder()

    _sb.AppendLine(" ROW_TYPE = " & RowType & " AND ROW_ID IN ( ")
    For _aux As Integer = 0 To uc_cash_users.NumRows - 1
      If uc_cash_users.Cell(_aux, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And uc_cash_users.Cell(_aux, USERS_GRID_ROW_TYPE).Value = RowType Then
        If _has_values Then
          _sb.AppendLine(",")
        End If
        _sb.AppendLine(uc_cash_users.Cell(_aux, USERS_GRID_ID).Value)
        _has_values = True
      End If
    Next

    If _has_values Then
      Return _sb.ToString & " ) "
    Else
      If opt_several_users.Checked Then
        Return _sb.ToString & " -1 ) "
      Else
        Return "ROW_TYPE = " & RowType
      End If
    End If

    Return " ROW_TYPE = " & RowType
  End Function

  ' PURPOSE : Gets whether the RowType is located in the uc_cash_users control
  '
  '  PARAMS:
  '     - INPUT: UserGridRowType
  '     - OUTPUT:
  '
  ' RETURNS : True if exist or False if not exist
  Private Function RowTypeExistIntoCashUsers(ByVal RowType As UserGridRowType) As Boolean
    Dim _has_value As Boolean
    _has_value = False

    For _aux As Integer = 0 To uc_cash_users.NumRows - 1
      If (uc_cash_users.Cell(_aux, USERS_GRID_ROW_TYPE).Value = RowType) Then
        _has_value = True
        Exit For
      End If
    Next

    Return _has_value
  End Function

  ' PURPOSE : Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_NUM_COLUMNS, GRID_NUM_HEADERS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
      .Sortable = False

      '' Blank "select" column at the left of the grid
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_USER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
      .Column(GRID_COLUMN_USER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)
      .Column(GRID_COLUMN_USER_NAME).Width = GRID_COLUMN_WIDTH_USER_NAME
      .Column(GRID_COLUMN_USER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_USER_FULL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
      .Column(GRID_COLUMN_USER_FULL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(758)
      .Column(GRID_COLUMN_USER_FULL_NAME).Width = GRID_COLUMN_WIDTH_USER_FULL_NAME
      .Column(GRID_COLUMN_USER_FULL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_USER_EMPLOYEE_CODE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
      .Column(GRID_COLUMN_USER_EMPLOYEE_CODE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4704)
      .Column(GRID_COLUMN_USER_EMPLOYEE_CODE).Width = GRID_COLUMN_WIDTH_USER_EMPLOYEE_CODE
      .Column(GRID_COLUMN_USER_EMPLOYEE_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_CASHIER_BALANCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616)
      .Column(GRID_COLUMN_CASHIER_BALANCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6614)
      .Column(GRID_COLUMN_CASHIER_BALANCE).Width = GRID_COLUMN_WIDTH_CASHIER_BALANCE
      .Column(GRID_COLUMN_CASHIER_BALANCE).HighLightWhenSelected = False
      .Column(GRID_COLUMN_CASHIER_BALANCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_CASHIER_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616)
      .Column(GRID_COLUMN_CASHIER_NAME).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(209)
      .Column(GRID_COLUMN_CASHIER_NAME).Width = GRID_COLUMN_WIDTH_CASHIER_NAME
      .Column(GRID_COLUMN_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_MB_CASH).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
      .Column(GRID_COLUMN_MB_CASH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1359)
      .Column(GRID_COLUMN_MB_CASH).Width = IIf(m_is_tito_mode, 0, GRID_COLUMN_WIDTH_MB_CASH)
      .Column(GRID_COLUMN_MB_CASH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_MB_RECHARGE_LIMIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
      .Column(GRID_COLUMN_MB_RECHARGE_LIMIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6615)
      .Column(GRID_COLUMN_MB_RECHARGE_LIMIT).Width = IIf(m_is_tito_mode, 0, GRID_COLUMN_WIDTH_MB_RECHARGE_LIMIT)
      .Column(GRID_COLUMN_MB_RECHARGE_LIMIT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_MB_RECHARGE_LIMIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_MB_NUM_RECHARGES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
      .Column(GRID_COLUMN_MB_NUM_RECHARGES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6661)
      .Column(GRID_COLUMN_MB_NUM_RECHARGES).Width = IIf(m_is_tito_mode, 0, GRID_COLUMN_WIDTH_MB_NUM_RECHARGES)
      .Column(GRID_COLUMN_MB_NUM_RECHARGES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
      .Column(GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6659)
      .Column(GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Width = IIf(m_is_tito_mode, 0, GRID_COLUMN_WIDTH_MB_RECHARGE_WORKING_DAY)
      .Column(GRID_COLUMN_MB_RECHARGE_WORKING_DAY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_MB_LAST_ACTIVITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182)
      .Column(GRID_COLUMN_MB_LAST_ACTIVITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6662)
      .Column(GRID_COLUMN_MB_LAST_ACTIVITY).Width = IIf(m_is_tito_mode, 0, GRID_COLUMN_WIDTH_MB_LAST_ACTIVITY)
      .Column(GRID_COLUMN_MB_LAST_ACTIVITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      .Column(GRID_COLUMN_ROW_TYPE).Header(0).Text = ""
      .Column(GRID_COLUMN_ROW_TYPE).Header(1).Text = ""
      .Column(GRID_COLUMN_ROW_TYPE).Width = GRID_COLUMN_WIDTH_ID

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Save the selected Sessions Ids from the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SaveSelectedGridIds()
    Dim _idx As Integer
    Dim _row_id As UsersGridRowID

    If m_selected_ids IsNot Nothing Then
      m_selected_ids.Clear()
      m_selected_ids = Nothing
    End If

    m_selected_ids = New List(Of UsersGridRowID)
    _idx = 0
    While _idx < Me.Grid.NumRows
      If Me.Grid.Row(_idx).IsSelected Then
        _row_id = New UsersGridRowID
        If Integer.TryParse(Me.Grid.Cell(_idx, GRID_COLUMN_ID).Value, _row_id.ID) Then
          If Integer.TryParse(Me.Grid.Cell(_idx, GRID_COLUMN_ROW_TYPE).Value, _row_id.RowType) Then
            _row_id.Terminal = Me.Grid.Cell(_idx, GRID_COLUMN_CASHIER_NAME).Value
            m_selected_ids.Add(_row_id)
          End If
        End If
      End If
      _idx = _idx + 1
    End While
  End Sub ' SaveSelectedIds

  ' PURPOSE: Save the selected Sessions Ids from the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SaveSelectedUserIds()
    Dim _idx As Integer
    Dim _row_id As UsersGridRowID

    m_filter = New CashMonitorFilter()
    If m_filter.m_users_list IsNot Nothing Then
      m_filter.m_users_list.Clear()
      m_filter.m_users_list = Nothing
    End If

    m_filter.m_users_list = New List(Of UsersGridRowID)
    _idx = 0
    While _idx < Me.uc_cash_users.NumRows
      If Me.uc_cash_users.Cell(_idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And uc_cash_users.Cell(_idx, USERS_GRID_ROW_TYPE).Value <> UserGridRowType.USERS_GRID_TYPE_HEADER Then
        _row_id = New UsersGridRowID
        If Integer.TryParse(Me.uc_cash_users.Cell(_idx, USERS_GRID_ID).Value, _row_id.ID) Then
          If Integer.TryParse(Me.uc_cash_users.Cell(_idx, USERS_GRID_ROW_TYPE).Value, _row_id.RowType) Then
            m_filter.m_users_list.Add(_row_id)
          End If
        End If
      End If
      _idx = _idx + 1
    End While
  End Sub ' SaveSelectedIds

  ' PURPOSE: Select in the Grid the latest selected Alarm Ids that have been saved.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SelectLastSelectedRow()
    Dim _idx As Integer
    Dim _any_selected As Boolean
    Dim _row_id As UsersGridRowID

    If Me.Grid.NumRows = 0 Then
      Return
    End If
    If m_selected_ids Is Nothing Then
      Return
    End If
    If m_selected_ids.Count = 0 Then
      For _idx = 0 To Me.Grid.NumRows - 1
        If Me.Grid.Row(_idx).IsSelected Then
          Me.Grid.Row(_idx).IsSelected = False
          Call Me.Grid.RepaintRow(_idx)
        End If
      Next
      Return
    End If

    _any_selected = False
    Me.Grid.Redraw = False

    Try
      _idx = 0
      While _idx < Me.Grid.NumRows
        Me.Grid.Row(_idx).IsSelected = False
        Call Me.Grid.RepaintRow(_idx)
        _idx = _idx + 1
      End While

      _idx = 0
      While _idx < Me.Grid.NumRows
        If Not Integer.TryParse(Me.Grid.Cell(_idx, GRID_COLUMN_ID).Value, _row_id.ID) Then
          _idx = _idx + 1
          Continue While
        End If
        If Not Integer.TryParse(Me.Grid.Cell(_idx, GRID_COLUMN_ROW_TYPE).Value, _row_id.RowType) Then
          _idx = _idx + 1
          Continue While
        End If
        _row_id.Terminal = Me.Grid.Cell(_idx, GRID_COLUMN_CASHIER_NAME).Value
        If m_selected_ids.Contains(_row_id) Then
          _any_selected = True
          Me.Grid.Row(_idx).IsSelected = True
          Call Me.Grid.RepaintRow(_idx)
        End If
        _idx = _idx + 1
      End While

      If Not _any_selected And Me.Grid.CurrentRow >= 0 Then
        Me.Grid.Row(Me.Grid.CurrentRow).IsSelected = True
        Call Me.Grid.RepaintRow(Me.Grid.CurrentRow)
      End If

    Finally
      Me.Grid.Redraw = True
    End Try
  End Sub ' SelectLastSelectedRow

  ' PURPOSE : Set filters default values 
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetDefaultValues()

    Me.opt_all_users.Checked = True

    ' MB
    Me.chk_mb_recharge_limit.Checked = False
    Me.ef_mb_recharge_limit.Enabled = True
    Me.ef_mb_recharge_limit.TextValue = GUI_FormatCurrency(0)

    ' Cashier
    Me.chk_cashier_recharge_limit.Checked = False
    Me.ef_cashier_recharge_limit.Enabled = True
    Me.ef_cashier_recharge_limit.TextValue = GUI_FormatCurrency(0)
    Me.chk_exclude_no_cash.Checked = False
    Me.chk_exclude_no_limit_mb.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE : Set filters default values or sets the saved filters
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub SetFilterValues()
    If m_first_time Then
      SetDBFilters()
    Else
      SetDefaultValues()
    End If
  End Sub ' SetDefaultValues

  ' PURPOSE : Gets the MB users from DB
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Function GetMBUsers(ByRef _accounts As List(Of MBAndCashierAccounts)) As Boolean
    Dim _sb_query As StringBuilder
    Dim _item As MBAndCashierAccounts
    _sb_query = New StringBuilder
    _sb_query.AppendLine()
    _sb_query.AppendLine("   SELECT   MB_ACCOUNT_ID                                                                ")
    _sb_query.AppendLine("          , CASE WHEN MB_HOLDER_NAME IS NULL THEN CAST(MB_ACCOUNT_ID as nvarchar)        ")
    _sb_query.AppendLine(" 	          ELSE CAST(MB_ACCOUNT_ID as nvarchar) + ' - ' + MB_HOLDER_NAME END AS MB_NAME ")
    _sb_query.AppendLine("     FROM   MOBILE_BANKS                                                                 ")
    _sb_query.AppendLine("    WHERE   MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_ACCEPTOR)
    _sb_query.AppendLine("      AND   MB_ACCOUNT_TYPE <> " & WSI.Common.MB_USER_TYPE.SYS_PROMOBOX)
    _sb_query.AppendLine("      AND   MB_BLOCKED = 0                                                               ")
    _sb_query.AppendLine(" ORDER BY   MB_HOLDER_NAME                                                               ")

    Try
      Using _db_trx As New DB_TRX
        Using _sql_cmd As New SqlCommand(_sb_query.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)
            _accounts = New List(Of MBAndCashierAccounts)()
            While _sql_reader.Read()
              _item = New MBAndCashierAccounts()
              _item.MBId = _sql_reader(0)
              _item.MBName = _sql_reader.GetString(1)

              _accounts.Add(_item)
            End While
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    End Try

    Return True

  End Function

  ' PURPOSE : Gets the MB users from DB
  '
  '  PARAMS :
  '     - INPUT :
  '         - List(Of MBAndCashierAccounts)
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - True or False
  '
  Private Function GetCashierUsers(ByRef _accounts As List(Of MBAndCashierAccounts)) As Boolean
    Dim _sb_query As StringBuilder
    Dim _item As MBAndCashierAccounts
    _sb_query = New StringBuilder

    _sb_query.AppendLine()
    _sb_query.AppendLine("   SELECT   GU_USER_ID                                 ")
    _sb_query.AppendLine("          , GU_USERNAME                                ")
    _sb_query.AppendLine("     FROM   GUI_USERS                                  ")
    If WSI.Common.Misc.IsCountREnabled Then
      _sb_query.AppendLine("  WHERE   GU_USER_TYPE IN (@pTypeUser, @pTypeCountR) ")
    Else
      _sb_query.AppendLine("  WHERE   GU_USER_TYPE = @pTypeUser                  ")
    End If
    _sb_query.AppendLine(" ORDER BY   GU_USERNAME  ASC                           ")

    Try
      Using _db_trx As New DB_TRX
        Using _sql_cmd As New SqlCommand(_sb_query.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pTypeUser", SqlDbType.Int).Value = GU_USER_TYPE.USER

          If WSI.Common.Misc.IsCountREnabled Then
            _sql_cmd.Parameters.Add("@pTypeCountR", SqlDbType.Int).Value = GU_USER_TYPE.SYS_REDEMPTION
          End If

          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)
            _accounts = New List(Of MBAndCashierAccounts)()
            While _sql_reader.Read()
              _item = New MBAndCashierAccounts()
              _item.MBId = _sql_reader(0)
              _item.MBName = _sql_reader.GetString(1)

              _accounts.Add(_item)
            End While
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    End Try

    Return True

  End Function

  'PURPOSE: Fills the users filter Grid whit the specified Users
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Private Function FillUsersGrid(ByVal MBAccounts As List(Of MBAndCashierAccounts), ByVal CashierAccounts As List(Of MBAndCashierAccounts)) As Boolean

    Call UsersGridStyleSheet()

    If Not m_is_tito_mode Then
      AddOneRowHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1182))
      For Each _user As MBAndCashierAccounts In MBAccounts
        AddOneRowData(_user.MBId, _user.MBName, UserGridRowType.USERS_GRID_TYPE_MB_USER)
      Next
    End If
    AddOneRowHeader(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6616))
    For Each _user As MBAndCashierAccounts In CashierAccounts
      AddOneRowData(_user.MBId, _user.MBName, UserGridRowType.USERS_GRID_TYPE_CASHIER_USER)
    Next

    Return True
  End Function

  ' PURPOSE : Define all users Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub UsersGridStyleSheet()
    With Me.uc_cash_users
      Call .Init(USERS_GRID_NUM_COLUMNS, Editable:=True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(USERS_GRID_ID).Header.Text = ""
      .Column(USERS_GRID_ID).WidthFixed = USERS_GRID_ID_WIDTH

      ' Hidden columns
      .Column(USERS_GRID_ROW_TYPE).Header.Text = ""
      .Column(USERS_GRID_ROW_TYPE).WidthFixed = USERS_GRID_ID_WIDTH

      ' GRID_COL_CHECKBOX
      .Column(USERS_GRID_CHECKED).Header.Text = ""
      .Column(USERS_GRID_CHECKED).WidthFixed = USERS_GRID_CHECKED_WIDTH
      .Column(USERS_GRID_CHECKED).Fixed = True
      .Column(USERS_GRID_CHECKED).Editable = True
      .Column(USERS_GRID_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(USERS_GRID_DESCRIPTION).Header.Text = ""
      .Column(USERS_GRID_DESCRIPTION).WidthFixed = USERS_GRID_DESCRIPTION_WIDTH
      .Column(USERS_GRID_DESCRIPTION).Fixed = True
      .Column(USERS_GRID_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With
  End Sub

  ' PURPOSE: Add a new header row the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - HeaderMsg: textual description
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowHeader(ByVal HeaderMsg As String)

    uc_cash_users.AddRow()

    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_DESCRIPTION).Value = HeaderMsg
    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_ID).Value = -1
    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_ROW_TYPE).Value = UserGridRowType.USERS_GRID_TYPE_HEADER

    Call uc_cash_users.Row(uc_cash_users.NumRows - 1).SetSignalAllowExpand(USERS_GRID_DESCRIPTION)
  End Sub ' AddOneRowHeader

  ' PURPOSE: Add a new data row at the data grid filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - UserID: numeric identifier
  '           - UserName: textual description
  '           - RowType: enum row type
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneRowData(ByVal UserID As Integer, ByVal UserName As String, ByVal RowType As UserGridRowType)
    Dim prev_redraw As Boolean

    prev_redraw = Me.uc_cash_users.Redraw
    Me.uc_cash_users.Redraw = False

    uc_cash_users.AddRow()

    Me.uc_cash_users.Redraw = False

    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_ID).Value = UserID
    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_DESCRIPTION).Value = USERS_GRID_TAB & UserName
    uc_cash_users.Cell(uc_cash_users.NumRows - 1, USERS_GRID_ROW_TYPE).Value = RowType

    Me.uc_cash_users.Row(uc_cash_users.NumRows - 1).Height = 0

    Me.uc_cash_users.Redraw = prev_redraw
  End Sub ' AddOneRowData

  ' PURPOSE : Checks if the specified row is a header
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Function RowIsHeader(ByVal RowIdx As Integer) As Boolean

    If Me.uc_cash_users.Cell(RowIdx, USERS_GRID_ROW_TYPE).Value = UserGridRowType.USERS_GRID_TYPE_HEADER Then

      Return True
    Else

      Return False
    End If

  End Function ' RowIsHeader

  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    For idx = FirstRow + 1 To uc_cash_users.NumRows - 1
      If Me.uc_cash_users.Cell(idx, USERS_GRID_ROW_TYPE).Value = UserGridRowType.USERS_GRID_TYPE_HEADER Then

        Exit Sub
      End If

      Me.uc_cash_users.Cell(idx, USERS_GRID_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been unchecked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _checked As Boolean

    _idx_bottom_header = -1
    _idx_header = -1
    ' Go look for the header from current row upwards
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If

      If Me.uc_cash_users.Cell(_idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED And _idx_header = -1 Then
        ' One or more upper rows are already unchecked
        _checked = True
      End If
    Next

    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.uc_cash_users.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.uc_cash_users.Cell(_idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _checked = True
      End If
    Next

    'update de check status of the header row when some or all the dependant rows has been checked
    If Not _checked Then
      Me.uc_cash_users.Cell(_idx_header, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Else
      Me.uc_cash_users.Cell(_idx_header, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If

  End Sub ' TryToUncheckHeader

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToCheckHeader(ByVal FirstRow As Integer)
    Dim _idx As Integer
    Dim _idx_header As Integer
    Dim _idx_bottom_header As Integer
    Dim _is_unchecked As Boolean

    _is_unchecked = False
    _idx_header = -1
    _idx_bottom_header = -1

    ' Go look for the header from current row upwards usually controls if any item is unchecked
    For _idx = FirstRow - 1 To 0 Step -1
      If RowIsHeader(_idx) And _idx_header = -1 Then
        _idx_header = _idx
      End If
      If _idx_header = -1 And Me.uc_cash_users.Cell(_idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If

    Next
    ' Go look for the bottom header from current row downwards
    For _idx = FirstRow + 1 To Me.uc_cash_users.NumRows - 1
      If RowIsHeader(_idx) And _idx_bottom_header = -1 Then
        _idx_bottom_header = _idx
      End If
      If _idx_bottom_header = -1 And Me.uc_cash_users.Cell(_idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        _is_unchecked = True
      End If
    Next

    ' Updates the header check status if all rows are checked
    If _is_unchecked = False Then
      Me.uc_cash_users.Cell(_idx_header, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.uc_cash_users.Cell(_idx_header, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
    End If
  End Sub ' TryToCheckHeader

  ' PURPOSE: What to do when no data found
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  '
  Protected Overrides Sub GUI_NoDataFound()
  End Sub ' GUI_NoDataFound

#End Region ' Private Functions

#Region "Events"

  Private Sub uc_cash_users_DataSelectedEvent() Handles uc_cash_users.DataSelectedEvent
    Dim _idx As Integer
    Dim _last_row As Integer
    Dim _row_is_header As Boolean

    If Me.opt_several_users.Checked = False Then
      Me.opt_several_users.Checked = True
    End If

    With Me.uc_cash_users
      _row_is_header = RowIsHeader(.CurrentRow)

      If _row_is_header And .CurrentCol = USERS_GRID_DESCRIPTION Then
        _last_row = .CurrentRow
        For _idx = .CurrentRow + 1 To .NumRows - 1
          If RowIsHeader(_idx) Then

            Exit For
          End If
          _last_row = _idx
        Next

        Me.uc_cash_users.CollapseExpand(.CurrentRow, _last_row, USERS_GRID_DESCRIPTION)

      End If
    End With
  End Sub ' uc_cash_users_DataSelectedEvent

  Private Sub uc_cash_users_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles uc_cash_users.CellDataChangedEvent
    Dim current_row As Integer

    ' Prevent recursive calls
    If m_refresh_grid = True Then

      Exit Sub
    End If

    ' Update radio buttons accordingly
    If Me.opt_several_users.Checked = False Then
      Me.opt_several_users.Checked = True
    End If

    With Me.uc_cash_users

      current_row = .CurrentRow
      m_refresh_grid = True

      If RowIsHeader(.CurrentRow) Then
        Call ChangeCheckofRows(.CurrentRow, .Cell(.CurrentRow, USERS_GRID_CHECKED).Value)
      Else
        If .Cell(.CurrentRow, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
          Call TryToUncheckHeader(.CurrentRow)
        Else
          Call TryToCheckHeader(.CurrentRow)
        End If
      End If

      m_refresh_grid = False
      .CurrentRow = current_row

    End With
  End Sub ' uc_cash_users_CellDataChangedEvent

  Private Sub opt_all_users_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_users.CheckedChanged
    Dim idx As Integer

    If Not opt_all_users.Checked Then
      Return
    End If

    For idx = 0 To uc_cash_users.NumRows - 1
      Me.uc_cash_users.Cell(idx, USERS_GRID_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub ' opt_all_users_Click

  Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles tmr_monitor.Tick

    Call GUI_ExecuteQueryCustom()
    Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, _
                                   ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
  End Sub

#End Region

#Region "FILTER CLASS"

  'Class to load and save filters in DB
  Public Class CashMonitorFilter
    Public m_users_list As List(Of UsersGridRowID)

    ' Filter Recharge MB
    Public m_mb_recharge_limit As Decimal
    Public m_chk_mb_recharge_limit As Boolean

    ' Filter Recharge Cashier
    Public m_cashier_recharge_limit As Decimal
    Public m_chk_cashier_recharge_limit As Boolean

    'Filter Options.
    Public m_chk_exclude_no_limit_mb As Boolean
    Public m_chk_exclude_no_cash As Boolean

    ' PURPOSE : Gets filter from DB for the specified user and form.
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS :
    Public Function GetFilter(ByVal UserId As Integer, ByVal FormId As Int32) As CashMonitorFilter

      Dim _sb As StringBuilder
      Dim _xml As String
      Dim _read_object As CashMonitorFilter
      Dim _deserializer As XmlSerializer
      Dim _string_reader As System.IO.StringReader

      _xml = ""
      _sb = New StringBuilder()
      _sb.AppendLine(" SELECT   GF_FILTER            ")
      _sb.AppendLine("   FROM   GUI_FILTERS          ")
      _sb.AppendLine("  WHERE   GF_USER_ID = " & UserId.ToString)
      _sb.AppendLine("    AND   GF_FORM_ID = " & FormId)

      Try
        Using _db_trx As New DB_TRX()
          Using _sql_cmd = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
            Using _reader As SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)
              If _reader.Read() Then
                _xml = _reader.GetString(0)
              Else
                Return Nothing
              End If
            End Using
          End Using
        End Using

        _read_object = New CashMonitorFilter()
        _string_reader = New IO.StringReader(_xml)
        _deserializer = New XmlSerializer(_read_object.GetType())
        _read_object = _deserializer.Deserialize(_string_reader)

      Catch _ex As Exception
        Log.Exception(_ex)
        Return Nothing
      End Try

      Return _read_object

    End Function

    ' PURPOSE : Saves filter to DB
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS :
    Public Function SaveFilter(ByVal UserId As Integer, ByVal FormId As Integer) As CashMonitorFilter

      Dim _sb As StringBuilder
      Dim _sql_cmd As SqlCommand

      _sb = New StringBuilder()
      _sb.AppendLine(" IF NOT EXISTS (SELECT   GF_FILTER              ")
      _sb.AppendLine("                  FROM   GUI_FILTERS            ")
      _sb.AppendLine("                 WHERE   GF_USER_ID = @pUserId  ")
      _sb.AppendLine("                   AND   GF_FORM_ID = @pFormId )")
      _sb.AppendLine("      INSERT INTO   GUI_FILTERS   ")
      _sb.AppendLine("                  ( GF_USER_ID    ")
      _sb.AppendLine("                  , GF_FORM_ID    ")
      _sb.AppendLine("                  , GF_FILTER   ) ")
      _sb.AppendLine("           VALUES ( @pUserId      ")
      _sb.AppendLine("                  , @pFormId      ")
      _sb.AppendLine("                  , @pFilters  )  ")
      _sb.AppendLine("    ELSE  ")
      _sb.AppendLine("      UPDATE   GUI_FILTERS ")
      _sb.AppendLine("         SET   GF_FILTER = @pFilters ")
      _sb.AppendLine("       WHERE   GF_USER_ID = @pUserId ")
      _sb.AppendLine("         AND   GF_FORM_ID = @pFormId ")

      Try
        Using _db_trx As New DB_TRX()
          _sql_cmd = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId
          _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value = FormId
          _sql_cmd.Parameters.Add("@pFilters", SqlDbType.Xml).Value = Me.Serialize(Me)
          If _db_trx.ExecuteNonQuery(_sql_cmd) <> 1 Then

            Return Nothing
          End If


          _db_trx.Commit()
        End Using

      Catch _ex As Exception
        Log.Exception(_ex)
        Return Nothing
      End Try

      Return Nothing

    End Function

    ' PURPOSE : Serialize the specifidet object
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS :
    Private Function Serialize(_value As Object) As String
      Dim _serializer As XmlSerializer

      _serializer = New XmlSerializer(_value.GetType())

      Try
        Using _writer As New IO.StringWriter()

          _serializer.Serialize(_writer, _value)


          Return _writer.ToString()
        End Using
      Catch _ex As Exception
        Log.Exception(_ex)

        Return Nothing
      End Try
    End Function

  End Class
#End Region

End Class

