'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_site_jackpot_configuration.vb
'
' DESCRIPTION : Site Jackpot Configuration screen
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-NOV-2010  RCI    Initial version.
' 10-APR-2012  MPO    Added chk_winner_name and chk_terminal_name
'                     At the moment the controls set to false
' 04-FEB-2013  ICS    Fixed Bug #562: Exception in GUI_IsScreenDataOk 
'                     when ef_contribution or ef_min_occupation are empty.
' 15-JUL-2013  LEM    Added PaymentMode with Manual or Automatic options
' 07-MAR-2014  JBC    Changed Provider control.
' 02-JUN-2014  XCD    Show Dialog message with comments if there are changes
' 11-JUL-2014  JMM    Weeks days sorted depending on system's first day of week
' 07-AUG-2014  FJC    Add check range in parameters (Range.ContributionPct, 
'                     Range.Jackpot<n>.ContributionPct, Range.Jackpot<n>.Minimum,
'                     Range.Occupation 
' 21-OCT-2014  JMV    Fixed WIG-1532 and WIG-1534
' 15-APR-2015  ANM    Add week days contribution if user wants
' 04-MAY-2015  DCS    Add new informative message with contribution average weekdays
' 09-JUL-2015  FAV    Fixed WIG-2535: Unhandled Exception when the user save data 
'                     with 'Contribution', 'Min', 'Max' or 'Average' are empty
' 22-FEB-2016  ETP    Product Backlog Item 9751: TITO Jackpot de sala: Only registered accounts.
' 21-NOV-2016  GMV    Product Backlog Item 19930:Jackpots - GUI - Show on WinUp setting
' 22-AUG-2017  EOR    Bug 29423: WIGOS-4566 A message to must select any terminal in 
'                     'Site jackpot configuration' appears having selected 'All' in 'Participating terminals'
' 22-DEC-2017  EOR    Bug 31150: WIGOS-6133 Incorrect scroll down displayed in the screen "Site jackpot contribution" when the GUI is in english 
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports System.Collections
Imports System.Globalization

Public Class frm_site_jackpot_configuration
  Inherits frm_base_edit

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_NAME As Integer = 1
  Private Const GRID_COLUMN_CONTRIBUTION As Integer = 2
  Private Const GRID_COLUMN_MIN_BET_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_MIN_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_MAX_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_DESIRED_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_FREQUENCY As Integer = 7
  Private Const GRID_COLUMN_ACCUMULATED As Integer = 8
  Private Const GRID_COLUMN_SHOW_ON_WINUP As Integer = 9

#End Region 'Constants

#Region " Structure "
  <StructLayout(LayoutKind.Sequential)> _
  Private Class TYPE_SITE_JACKPOT_PARAMS_RANGE
    ' if block_on_out_of_range acivated, apply range control
    Public block_on_out_of_range As Boolean = False
    Public contribution_pct_range_min As Decimal
    Public contribution_pct_range_max As Decimal
    Public occupattion_pct_range_min As Decimal
    Public occupattion_pct_range_max As Decimal
    Public jackpot_instance_range As New List(Of TYPE_SITE_JACKPOT_INSTANCE_PARAMS_RANGE)

  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Private Class TYPE_SITE_JACKPOT_INSTANCE_PARAMS_RANGE

    ' if block_on_out_of_range acivated, apply range control
    Public contribution_pct_range_min As Decimal
    Public contribution_pct_range_max As Decimal
    Public min_amount_range_min As Decimal
    Public min_amount_range_max As Decimal
  End Class

  Private Class SITE_JACKPOT_RANGE_ERROR

    Public contribution_perc_error As Boolean = False
    Public contribution_perc_value As Decimal
    Public level_range_errors As Dictionary(Of Integer, Decimal)
    Public level_contribution_errors As Dictionary(Of Integer, Decimal)
    Public ocupation_perc_error As Boolean = False
    Public ocupation_perc_value As Decimal

    Public Sub New()
      level_range_errors = New Dictionary(Of Integer, Decimal)
      level_contribution_errors = New Dictionary(Of Integer, Decimal)
    End Sub

  End Class

#End Region 'Structure

#Region " Members "
  Private contribution_frecuency As Decimal        'indicates the properly contribution value to calculate frecuency
  Private m_winUp_enabled As Boolean                 'indicates wether WinUP module is installed
#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITE_JACKPOT_CONFIGURATION

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _week_days_chk_array(6) As CheckBox
    Dim _week_days_lbl_array(6) As Label
    Dim _week_days_ef_array(6) As uc_entry_field

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Initialize Form Controls

    ' - Form
    Me.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(330)

    '- Buttons

    ' - Labels
    '   - Jackpot system 
    gb_jackpot_flag.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(252)
    opt_enabled.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(218)
    opt_disabled.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(250)

    '   - Contribution
    gb_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(488)
    lbl_percentaje.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(IIf(chk_only_redeemable.Checked, 333, 334))
    lbl_sunday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(295)
    lbl_monday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(289)
    lbl_tuesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(290)
    lbl_wednesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(291)
    lbl_thursday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(292)
    lbl_friday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(293)
    lbl_saturday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(294)
    _week_days_lbl_array(0) = lbl_sunday
    _week_days_lbl_array(1) = lbl_monday
    _week_days_lbl_array(2) = lbl_tuesday
    _week_days_lbl_array(3) = lbl_wednesday
    _week_days_lbl_array(4) = lbl_thursday
    _week_days_lbl_array(5) = lbl_friday
    _week_days_lbl_array(6) = lbl_saturday
    Call SortControlByFirstDayOfWeek(_week_days_lbl_array)
    _week_days_ef_array(0) = ef_sunday
    _week_days_ef_array(1) = ef_monday
    _week_days_ef_array(2) = ef_tuesday
    _week_days_ef_array(3) = ef_wednesday
    _week_days_ef_array(4) = ef_thursday
    _week_days_ef_array(5) = ef_friday
    _week_days_ef_array(6) = ef_saturday
    Call SortControlByFirstDayOfWeek(_week_days_ef_array)
    opt_fix_contribution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6216)
    opt_week_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(476)
    ef_last_month_played.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(338)
    btn_load_last_month_played.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(345)
    chk_only_redeemable.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(332)
    Call AddHandlers()

    '   - Awarding
    gb_wrk_days.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(296)
    dtp_working_from.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    '     - Working Days
    chk_monday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(289)
    chk_tuesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(290)
    chk_wednesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(291)
    chk_thursday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(292)
    chk_friday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(293)
    chk_saturday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(294)
    chk_sunday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(295)

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    ef_min_occupation.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(346)
    chk_exclude_account_with_promotion.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(340)

    chk_exclude_anonymous_accounts.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(347)

    If WSI.Common.TITO.Utils.IsTitoMode Then
      chk_exclude_anonymous_accounts.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7138)
    End If

    '   - Payout Mode 
    gb_payment_mode.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(490)
    opt_manual_payment.Text = CLASS_SITE_JACKPOT_PARAMS.PaymentModeString(HANDPAY_PAYMENT_MODE.MANUAL)
    opt_automatic_payment.Text = CLASS_SITE_JACKPOT_PARAMS.PaymentModeString(HANDPAY_PAYMENT_MODE.AUTOMATIC)

    ' Configure Data Grid
    Me.m_winUp_enabled = WSI.Common.GeneralParam.GetBoolean("Features", "WinUP")

    Call GUI_StyleView()

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Set date picker obj properties
    Me.dtp_working_from.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from.ShowUpDown = True
    Me.dtp_working_to.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to.ShowUpDown = True

    ' Format entry fields
    Me.ef_contribution.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_last_month_played.SetFilter(FORMAT_MONEY, 10)
    Me.ef_min_occupation.SetFilter(FORMAT_NUMBER, 6, 2)
    Me.ef_sunday.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_monday.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_tuesday.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_wednesday.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_thursday.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_friday.SetFilter(FORMAT_NUMBER, 8, 4)
    Me.ef_saturday.SetFilter(FORMAT_NUMBER, 8, 4)

    ' Maximum overflow checkbox label
    Me.chk_allow_exceed_max.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(493)

    If WSI.Common.GeneralParam.GetBoolean("SiteJackpot", "ShowAllowExceedMaximum") Then
      Me.chk_allow_exceed_max.Visible = True
    Else
      Me.chk_allow_exceed_max.Visible = False
    End If

    ' Total labels
    Me.lbl_accumulated_total.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(335)
    Me.lbl_to_compensate.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(336)
    Me.lbl_current_compensate_pct.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(494)

    ' Titles
    Me.cmb_anim_interval_01.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(355)
    Me.cmb_anim_interval_02.Text = ""
    Me.cmb_recent_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(356)

    ' 355 "Duración de la animación inicial del Jackpot"
    Me.lbl_animation_interval_01.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(355)
    ' 348 "Duración de la animación final del Jackpot"
    Me.lbl_animation_interval_02.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(348)
    ' 356 "Se anuncia el último jackpot durante "
    Me.lbl_recent_interval.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(356)

    Me.gb_parameters.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(323)

    ' Animation interval 01
    ' The database handles the data in seconds
    Me.cmb_anim_interval_01.Add(15, "15 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(310))               ' 310 "seconds"
    Me.cmb_anim_interval_01.Add(30, "30 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(310))               ' 310 "seconds"
    Me.cmb_anim_interval_01.Add(1 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(311))            ' 311 "minute"
    Me.cmb_anim_interval_01.Add(2 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"
    Me.cmb_anim_interval_01.Add(3 * 60, "3 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"
    Me.cmb_anim_interval_01.Add(4 * 60, "4 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"
    Me.cmb_anim_interval_01.Add(5 * 60, "5 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"

    ' Animation interval 02
    ' The database handles the data in seconds
    Me.cmb_anim_interval_02.Add(0, GLB_NLS_GUI_JACKPOT_MGR.GetString(357))                        ' 357 "No Hay"
    Me.cmb_anim_interval_02.Add(15, "15 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(310))               ' 310 "seconds"
    Me.cmb_anim_interval_02.Add(30, "30 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(310))               ' 310 "seconds"
    Me.cmb_anim_interval_02.Add(1 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(311))            ' 311 "minute"
    Me.cmb_anim_interval_02.Add(2 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"
    Me.cmb_anim_interval_02.Add(3 * 60, "3 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"
    Me.cmb_anim_interval_02.Add(4 * 60, "4 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"
    Me.cmb_anim_interval_02.Add(5 * 60, "5 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(312))            ' 312 "minutes"

    ' Recent jackpot interval
    ' The database handles the data in seconds
    Me.cmb_recent_interval.Add(1 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(313))        ' 313 "hour"
    Me.cmb_recent_interval.Add(2 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(314))        ' 314 "hours"
    Me.cmb_recent_interval.Add(1 * 24 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(315))   ' 315 "day"
    Me.cmb_recent_interval.Add(2 * 24 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(316))   ' 316 "days"
    Me.cmb_recent_interval.Add(7 * 24 * 60 * 60, "1 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(324))   ' 324 "week"
    Me.cmb_recent_interval.Add(14 * 24 * 60 * 60, "2 " + GLB_NLS_GUI_JACKPOT_MGR.GetString(325))  ' 325 "week"

    ' Promo Messages
    Me.gb_promo_message.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(321)
    Me.lbl_message_1.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(322) + " 1 "
    Me.lbl_message_2.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(322) + " 2 "
    Me.lbl_message_3.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(322) + " 3 "

    Me.tmr_refresh_jackpot.Interval = 60000
    Me.tmr_refresh_jackpot.Start()

    chk_winner_name.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(358)
    chk_terminal_name.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(359)
    chk_winner_name.Enabled = Me.Permissions.Write
    chk_terminal_name.Enabled = Me.Permissions.Write
    chk_winner_name.Checked = False
    chk_terminal_name.Checked = False

    If WSI.Common.GeneralParam.GetBoolean("SiteJackpot", "Bonusing.Enabled") Then
      chk_exclude_account_with_promotion.Enabled = False

      gb_payment_mode.Enabled = False

      chk_winner_name.Enabled = False

    End If


    'Disable Close Windows in Button OK
    CloseOnOkClick = False

    'Hide Node All 'EOR 22-AUG-2017
    dg_providers.ShowNodeAll() = False

    'Adjust columns when enabled WinUp 'EOR 22-DEC-2017
    If m_winUp_enabled Then
      dg_jackpot_list.Column(GRID_COLUMN_INDEX).Width = 700
      dg_jackpot_list.Column(GRID_COLUMN_NAME).Width = 1200
      dg_jackpot_list.Column(GRID_COLUMN_MIN_BET_AMOUNT).Width = 1450
      dg_jackpot_list.Column(GRID_COLUMN_MIN_AMOUNT).Width = 1450
      dg_jackpot_list.Column(GRID_COLUMN_MAX_AMOUNT).Width = 1450
      dg_jackpot_list.Column(GRID_COLUMN_DESIRED_AMOUNT).Width = 1450
      dg_jackpot_list.Column(GRID_COLUMN_FREQUENCY).Width = 1500
      dg_jackpot_list.Column(GRID_COLUMN_ACCUMULATED).Width = 1480
    End If


  End Sub 'GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim idx As Integer
    Dim min_amount As Double
    Dim max_amount As Double
    Dim desired_amount As Double
    Dim acc_contribution As Double
    Dim _contributionpct_grid As Double

    Dim _confirmation_result As GUI_CommonMisc.ENUM_MB_RESULT = ENUM_MB_RESULT.MB_RESULT_OK
    Dim _msg_error_range As String = String.Empty
    Dim _error_range As Boolean = False
    Dim jp_params_range As TYPE_SITE_JACKPOT_PARAMS_RANGE
    Dim _site_jackpot_range_errors As SITE_JACKPOT_RANGE_ERROR
    Dim _first_error_range = True
    Dim _wrong_day As Label
    Dim _ef_wrong_day As uc_entry_field

    jp_params_range = GetParamsRangeSiteJackpot()

    _site_jackpot_range_errors = New SITE_JACKPOT_RANGE_ERROR()

    If Not Me.dg_providers.CheckAtLeastOneSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(172))
      Call dg_providers.Focus()

      Return False
    End If

    ' Check if ef_contribution is empty
    If opt_fix_contribution.Checked Then
      If String.IsNullOrEmpty(ef_contribution.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_percentaje.Text & " - " & opt_fix_contribution.Text)
        Call Me.ef_contribution.Focus()

        Return False
      End If
    End If
    If opt_week_days.Checked Then
      _wrong_day = Nothing
      _ef_wrong_day = Nothing
      'in reverse mode to obtain at last the FIRST wrong day
      If String.IsNullOrEmpty(ef_saturday.Value) Then
        _wrong_day = lbl_saturday
        _ef_wrong_day = ef_saturday
      End If
      If String.IsNullOrEmpty(ef_friday.Value) Then
        _wrong_day = lbl_friday
        _ef_wrong_day = ef_friday
      End If
      If String.IsNullOrEmpty(ef_thursday.Value) Then
        _wrong_day = lbl_thursday
        _ef_wrong_day = ef_thursday
      End If
      If String.IsNullOrEmpty(ef_wednesday.Value) Then
        _wrong_day = lbl_wednesday
        _ef_wrong_day = ef_wednesday
      End If
      If String.IsNullOrEmpty(ef_tuesday.Value) Then
        _wrong_day = lbl_tuesday
        _ef_wrong_day = ef_tuesday
      End If
      If String.IsNullOrEmpty(ef_monday.Value) Then
        _wrong_day = lbl_monday
        _ef_wrong_day = ef_monday
      End If
      If String.IsNullOrEmpty(ef_sunday.Value) Then
        _wrong_day = lbl_sunday
        _ef_wrong_day = ef_sunday
      End If

      If Not _wrong_day Is Nothing Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_percentaje.Text & " - " & opt_week_days.Text & " - " & _wrong_day.Text)
        _ef_wrong_day.Focus()
        Return False
      End If
    End If

    ' Check if ef_contribution is negative
    If opt_fix_contribution.Checked Then
      If ef_contribution.Value < 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_percentaje.Text & " - " & opt_fix_contribution.Text)
        Call Me.ef_contribution.Focus()

        Return False
      End If
    End If
    If opt_week_days.Checked Then
      _wrong_day = Nothing
      _ef_wrong_day = Nothing

      'in reverse mode to obtain at last the FIRST wrong day
      If ef_saturday.Value < 0 Then
        _wrong_day = lbl_saturday
        _ef_wrong_day = ef_saturday
      End If
      If ef_friday.Value < 0 Then
        _wrong_day = lbl_friday
        _ef_wrong_day = ef_friday
      End If
      If ef_thursday.Value < 0 Then
        _wrong_day = lbl_thursday
        _ef_wrong_day = ef_thursday
      End If
      If ef_wednesday.Value < 0 Then
        _wrong_day = lbl_wednesday
        _ef_wrong_day = ef_wednesday
      End If
      If ef_tuesday.Value < 0 Then
        _wrong_day = lbl_tuesday
        _ef_wrong_day = ef_tuesday
      End If
      If ef_monday.Value < 0 Then
        _wrong_day = lbl_monday
        _ef_wrong_day = ef_monday
      End If
      If ef_sunday.Value < 0 Then
        _wrong_day = lbl_sunday
        _ef_wrong_day = ef_sunday
      End If

      If Not _wrong_day Is Nothing Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , lbl_percentaje.Text & " - " & opt_week_days.Text & " - " & _wrong_day.Text)
        _ef_wrong_day.Focus()
        Return False
      End If
    End If

    If opt_fix_contribution.Checked Then
      If ContributionMoreThan100(ef_contribution) = False Then
        Return False
      End If
    End If
    If opt_week_days.Checked Then
      If ContributionMoreThan100(ef_sunday) = False Then
        Return False
      End If
      If ContributionMoreThan100(ef_monday) = False Then
        Return False
      End If
      If ContributionMoreThan100(ef_tuesday) = False Then
        Return False
      End If
      If ContributionMoreThan100(ef_wednesday) = False Then
        Return False
      End If
      If ContributionMoreThan100(ef_thursday) = False Then
        Return False
      End If
      If ContributionMoreThan100(ef_friday) = False Then
        Return False
      End If
      If ContributionMoreThan100(ef_saturday) = False Then
        Return False
      End If
    End If


    ' Check if ef_min_occupation is empty
    If String.IsNullOrEmpty(ef_min_occupation.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_min_occupation.Text)
      Call Me.ef_min_occupation.Focus()

      Return False
    End If

    If ContributionMoreThan100(ef_min_occupation) = False Then
      Return False
    End If

    'Contribution PCT Range 
    If opt_fix_contribution.Checked Then
      If ContributionRange(ef_contribution, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
    End If
    If opt_week_days.Checked Then
      If ContributionRange(ef_sunday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
      If ContributionRange(ef_monday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
      If ContributionRange(ef_tuesday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
      If ContributionRange(ef_wednesday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
      If ContributionRange(ef_thursday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
      If ContributionRange(ef_friday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
      If ContributionRange(ef_saturday, jp_params_range, _error_range, _site_jackpot_range_errors) = False Then
        Return False
      End If
    End If

    ' Check consistence between parameters set for every jackpot instance
    For idx = 0 To Me.dg_jackpot_list.NumRows - 1
      min_amount = IIf(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value = String.Empty, 0, Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value)
      max_amount = IIf(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value = String.Empty, 0, Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value)
      desired_amount = IIf(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value = String.Empty, 0, Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value)
      _contributionpct_grid = IIf(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value = String.Empty, 0, Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value)

      If max_amount < min_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(131), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MAX_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      If desired_amount < min_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_DESIRED_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      If desired_amount > max_amount Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_DESIRED_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      'Min Amount Range
      If (min_amount < jp_params_range.jackpot_instance_range(idx).min_amount_range_min OrElse _
         min_amount > jp_params_range.jackpot_instance_range(idx).min_amount_range_max) Then

        If jp_params_range.block_on_out_of_range = True Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(135), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value, jp_params_range.jackpot_instance_range(idx).min_amount_range_min, jp_params_range.jackpot_instance_range(idx).min_amount_range_max)
          Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MIN_AMOUNT
          Me.dg_jackpot_list.CurrentRow = idx
          Call Me.dg_jackpot_list.Focus()

          Return False
        Else
          _error_range = True
        End If

        If Not _site_jackpot_range_errors.level_range_errors.ContainsKey(idx) Then
          _site_jackpot_range_errors.level_range_errors.Add(idx, min_amount)
        End If

      End If

      'JackPot pct% Contribution
      If (_contributionpct_grid < jp_params_range.jackpot_instance_range(idx).contribution_pct_range_min OrElse _
         _contributionpct_grid > jp_params_range.jackpot_instance_range(idx).contribution_pct_range_max) Then

        If jp_params_range.block_on_out_of_range = True Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(134), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value, jp_params_range.jackpot_instance_range(idx).contribution_pct_range_min, jp_params_range.jackpot_instance_range(idx).contribution_pct_range_max)
          Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_CONTRIBUTION
          Me.dg_jackpot_list.CurrentRow = idx
          Call Me.dg_jackpot_list.Focus()

          Return False
        Else
          _error_range = True
        End If

        If Not _site_jackpot_range_errors.level_contribution_errors.ContainsKey(idx) Then
          _site_jackpot_range_errors.level_contribution_errors.Add(idx, _contributionpct_grid)
        End If
      End If
    Next

    ' Check accumulated contribution: must be 100
    acc_contribution = 0
    For idx = 0 To dg_jackpot_list.NumRows - 1
      acc_contribution += IIf(dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value = String.Empty, 0, dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value)
    Next
    acc_contribution = Math.Round(acc_contribution, 2, MidpointRounding.AwayFromZero)
    If acc_contribution <> 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(102), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_CONTRIBUTION
      Me.dg_jackpot_list.CurrentRow = 0
      Call Me.dg_jackpot_list.Focus()

      Return False
    End If

    ' Check for overlapping values (check for maximums)
    idx = dg_jackpot_list.NumRows - 1
    While idx >= 1
      Dim _max_value As Integer
      Dim _min_value As Integer

      _max_value = CInt(IIf(dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value = String.Empty, 0, dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value))
      _min_value = CInt(IIf(dg_jackpot_list.Cell(idx - 1, GRID_COLUMN_MIN_AMOUNT).Value = String.Empty, 0, dg_jackpot_list.Cell(idx - 1, GRID_COLUMN_MIN_AMOUNT).Value))

      'Max. amount
      If _max_value > _min_value Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(132), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & "-" & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value)
        Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MAX_AMOUNT
        Me.dg_jackpot_list.CurrentRow = idx
        Call Me.dg_jackpot_list.Focus()

        Return False
      End If

      idx -= 1

    End While

    'Occupattion Range
    If ef_min_occupation.Value < jp_params_range.occupattion_pct_range_min OrElse _
      ef_min_occupation.Value > jp_params_range.occupattion_pct_range_max Then

      If jp_params_range.block_on_out_of_range = True Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(136), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , jp_params_range.occupattion_pct_range_min, jp_params_range.occupattion_pct_range_max)
        'Call dg_providers.Focus()
        ef_min_occupation.Focus()

        Return False
      Else
        _error_range = True
      End If
      _site_jackpot_range_errors.ocupation_perc_error = True
      _site_jackpot_range_errors.ocupation_perc_value = ef_min_occupation.Value
    End If

    If _error_range = True Then
      ' GLB_NLS_GUI_PLAYER_TRACKING(368)  "Porcentaje"  | NLS_ID_GUI_JACKPOT_MANAGER(488) "Contribution"  |  NLS_ID_GUI_PLAYER_TRACKING(5101)	"Rango"
      ' NLS_ID_GUI_JACKPOT_MANAGER(283)   "Contribución %" NLS_ID_GUI_JACKPOT_MANAGER(125)   "Occupation %"

      'Contribution % Range
      If (_site_jackpot_range_errors.contribution_perc_error) Then
        _msg_error_range = mdl_NLS.NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(488)) & ": " & _
          GUI_FormatNumber(_site_jackpot_range_errors.contribution_perc_value, 4) & ", " & _
          mdl_NLS.NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(645)) & " " & _
          "[" & GUI_FormatNumber(jp_params_range.contribution_pct_range_min, 4) & " - " & GUI_FormatNumber(jp_params_range.contribution_pct_range_max, 4) & "]"
        If _first_error_range Then
          _first_error_range = False
        End If
      End If

      'Occupattion % Range
      If _site_jackpot_range_errors.ocupation_perc_error Then
        _msg_error_range &= vbCrLf
        _msg_error_range &= mdl_NLS.NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2058)) & ": " & _
         GUI_FormatNumber(_site_jackpot_range_errors.ocupation_perc_value, 2) & ", " & _
         mdl_NLS.NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(645)) & " " & _
          "[" & GUI_FormatNumber(jp_params_range.occupattion_pct_range_min, 2) & " - " & GUI_FormatNumber(jp_params_range.occupattion_pct_range_max, 2) & "]"
        If _first_error_range Then
          Call Me.ef_min_occupation.Focus()
          _first_error_range = False
        End If
      End If

      For idx = 0 To Me.dg_jackpot_list.NumRows - 1
        Dim _contribution_value As Decimal = 0
        Dim _minimum_value As Decimal = 0

        _site_jackpot_range_errors.level_contribution_errors.TryGetValue(idx, _contribution_value)
        _site_jackpot_range_errors.level_range_errors.TryGetValue(idx, _minimum_value)
        If _contribution_value <> 0 OrElse _minimum_value <> 0 Then
          _msg_error_range &= vbCrLf
          _msg_error_range &= mdl_NLS.NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(443)) & " " & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value & " - " & Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value
        End If

        'Contribution % JACKPOT <n> Range
        If _contribution_value <> 0 Then
          _msg_error_range &= vbCrLf
          _msg_error_range &= "   - " & mdl_NLS.NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(488)) & ": " & _
            GUI_FormatNumber(_contribution_value, 2) & ", " & _
            mdl_NLS.NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(645)) & " [" & _
            GUI_FormatNumber(jp_params_range.jackpot_instance_range(idx).contribution_pct_range_min, 2) & " - " & _
            GUI_FormatNumber(jp_params_range.jackpot_instance_range(idx).contribution_pct_range_max, 2) & "]"
          If _first_error_range Then
            Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_CONTRIBUTION
            Me.dg_jackpot_list.CurrentRow = idx
            Call Me.dg_jackpot_list.Focus()
            _first_error_range = False
          End If
        End If

        'Range Minimum JACKPOT <n>
        If _minimum_value <> 0 Then
          _msg_error_range &= vbCrLf
          _msg_error_range &= "   - " & mdl_NLS.NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(285)) & ": " & _
            GUI_FormatNumber(_minimum_value, 2) & ", " & _
            mdl_NLS.NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(645)) & " [" & _
            GUI_FormatNumber(jp_params_range.jackpot_instance_range(idx).min_amount_range_min, 2) & " - " & _
            GUI_FormatNumber(jp_params_range.jackpot_instance_range(idx).min_amount_range_max, 2) & "]"
          If _first_error_range Then
            Me.dg_jackpot_list.CurrentCol = GRID_COLUMN_MIN_AMOUNT
            Me.dg_jackpot_list.CurrentRow = idx
            Call Me.dg_jackpot_list.Focus()
            _first_error_range = False
          End If
        End If

      Next

      'MessagBox Confirmation
      _confirmation_result = NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, , _msg_error_range)
      If _confirmation_result = ENUM_MB_RESULT.MB_RESULT_NO Then

        Return False
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_GetScreenData()

    Dim jp_params_edit As CLASS_SITE_JACKPOT_PARAMS
    Dim jp_params_read As CLASS_SITE_JACKPOT_PARAMS
    Dim temp_date As Date
    Dim second As Integer
    Dim idx As Integer

    jp_params_edit = Me.DbEditedObject
    jp_params_read = Me.DbReadObject

    jp_params_edit.Enabled = Me.opt_enabled.Checked

    dg_providers.GetTerminalsFromControl(jp_params_edit.TerminalList)

    temp_date = Me.dtp_working_from.Value
    second = Format_HhMmSsToSecond(temp_date)
    jp_params_edit.WrkStart = second

    temp_date = Me.dtp_working_to.Value
    second = Format_HhMmSsToSecond(temp_date)
    jp_params_edit.WrkEnd = second
    jp_params_edit.ContributionFix = opt_fix_contribution.Checked
    If opt_fix_contribution.Checked Then
      jp_params_edit.ContributionPct(0) = GUI_ParseNumber(Me.ef_contribution.Tag)
    Else
      jp_params_edit.ContributionPct(1) = GUI_ParseNumber(Me.ef_sunday.Value)
      jp_params_edit.ContributionPct(2) = GUI_ParseNumber(Me.ef_monday.Value)
      jp_params_edit.ContributionPct(3) = GUI_ParseNumber(Me.ef_tuesday.Value)
      jp_params_edit.ContributionPct(4) = GUI_ParseNumber(Me.ef_wednesday.Value)
      jp_params_edit.ContributionPct(5) = GUI_ParseNumber(Me.ef_thursday.Value)
      jp_params_edit.ContributionPct(6) = GUI_ParseNumber(Me.ef_friday.Value)
      jp_params_edit.ContributionPct(7) = GUI_ParseNumber(Me.ef_saturday.Value)
    End If
    jp_params_edit.JackpotInstances.Clear()

    jp_params_edit.WorkingDays = GetWorkingDays()

    If opt_manual_payment.Checked Then
      jp_params_edit.PaymentMode = HANDPAY_PAYMENT_MODE.MANUAL
    ElseIf opt_automatic_payment.Checked Then
      jp_params_edit.PaymentMode = HANDPAY_PAYMENT_MODE.AUTOMATIC
    End If

    jp_params_edit.MinOccupationPct = GUI_ParseNumber(Me.ef_min_occupation.Value)
    jp_params_edit.ExcludePromotions = Me.chk_exclude_account_with_promotion.Checked
    jp_params_edit.ExcludeAnonymous = Me.chk_exclude_anonymous_accounts.Checked

    For idx = 0 To Me.dg_jackpot_list.NumRows - 1
      Dim jackpot_inst As New CLASS_SITE_JACKPOT_PARAMS.TYPE_SITE_JACKPOT_INSTANCE

      jackpot_inst.index = GUI_ParseNumber(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value)
      jackpot_inst.name = Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value
      jackpot_inst.contribution_pct = GUI_ParseNumber(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value)
      jackpot_inst.min_bet_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_BET_AMOUNT).Value)
      jackpot_inst.min_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value)
      jackpot_inst.max_amount = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value)
      jackpot_inst.average = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value)
      jackpot_inst.show_on_winup = GridValuetoBool(Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_SHOW_ON_WINUP).Value)

      jp_params_edit.JackpotInstances.Add(jackpot_inst)
    Next

    jp_params_edit.AnimInterval_01 = cmb_anim_interval_01.Value
    jp_params_edit.AnimInterval_02 = cmb_anim_interval_02.Value
    jp_params_edit.RecentInterval = cmb_recent_interval.Value

    jp_params_edit.PromoMess(0) = txt_message_1.Text
    jp_params_edit.PromoMess(1) = txt_message_2.Text
    jp_params_edit.PromoMess(2) = txt_message_3.Text

    jp_params_edit.OnlyRedeemable = Me.chk_only_redeemable.Checked
    jp_params_edit.ShowTerminalName = Me.chk_terminal_name.Checked
    jp_params_edit.ShowWinnerName = Me.chk_winner_name.Checked

    jp_params_edit.AllowExceedMaximum = Me.chk_allow_exceed_max.Checked

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim jackpot_parameters As CLASS_SITE_JACKPOT_PARAMS
    Dim second As Integer
    Dim minute As Integer
    Dim hour As Integer
    Dim day As Integer
    Dim date_time As Date

    date_time = GUI_GetDateTime()
    jackpot_parameters = Me.DbReadObject

    ' Enabled / Disabled
    If jackpot_parameters.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    ' Working Values
    Call Format_SecondsToDdHhMmSs(jackpot_parameters.WrkStart, day, hour, minute, second)
    Me.dtp_working_from.Value = New DateTime(date_time.Year, date_time.Month, date_time.Day, hour, minute, second)

    Call Format_SecondsToDdHhMmSs(jackpot_parameters.WrkEnd, day, hour, minute, second)
    Me.dtp_working_to.Value = New DateTime(date_time.Year, date_time.Month, date_time.Day, hour, minute, second)

    ' Contribution Pct
    If jackpot_parameters.ContributionFix Then
      opt_fix_contribution.Checked = True
      Call ShowAndEnableContributionDaysOfWeek(New List(Of Decimal), False)
      ef_contribution.Value = jackpot_parameters.ContributionPct(0)
      GetContributionValue()
    Else
      opt_week_days.Checked = True
      Call ShowAndEnableContributionDaysOfWeek(jackpot_parameters.ContributionPct, True)
      GetContributionValue()
    End If
    ef_contribution.Tag = jackpot_parameters.ContributionPct(0)   'to remember the value

    ' Only Redeemable
    chk_only_redeemable.Checked = jackpot_parameters.OnlyRedeemable
    chk_only_redeemable_CheckedChanged(Nothing, Nothing)

    ' Set Jackpot List
    Call SetJackpotList(jackpot_parameters)

    ' Working Days
    Call SetWorkingDays(jackpot_parameters.WorkingDays)

    ' PaymentMode
    If jackpot_parameters.PaymentMode = HANDPAY_PAYMENT_MODE.MANUAL Then
      opt_manual_payment.Checked = True
    Else
      opt_automatic_payment.Checked = True
    End If

    ' Minimum Occupation
    Me.ef_min_occupation.Value = jackpot_parameters.MinOccupationPct

    ' Exclude Promotions
    chk_exclude_account_with_promotion.Checked = jackpot_parameters.ExcludePromotions

    ' Exclude Anonymous Accounts
    chk_exclude_anonymous_accounts.Checked = jackpot_parameters.ExcludeAnonymous

    Me.cmb_anim_interval_01.Value = jackpot_parameters.AnimInterval_01
    Me.cmb_anim_interval_02.Value = jackpot_parameters.AnimInterval_02
    Me.cmb_recent_interval.Value = jackpot_parameters.RecentInterval

    ' Promo Messages
    Me.txt_message_1.Text = jackpot_parameters.PromoMess(0)
    Me.txt_message_2.Text = jackpot_parameters.PromoMess(1)
    Me.txt_message_3.Text = jackpot_parameters.PromoMess(2)

    Me.dg_providers.SetTerminalList(jackpot_parameters.TerminalList)

    Me.chk_terminal_name.Checked = jackpot_parameters.ShowTerminalName
    Me.chk_winner_name.Checked = jackpot_parameters.ShowWinnerName

    Me.chk_allow_exceed_max.Checked = jackpot_parameters.AllowExceedMaximum

    RefreshJackpot(jackpot_parameters)

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim jackpot_parameters As CLASS_SITE_JACKPOT_PARAMS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_SITE_JACKPOT_PARAMS
        jackpot_parameters = DbEditedObject
        jackpot_parameters.Enabled = False

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Dim _comments As frm_comments

        jackpot_parameters = Me.DbEditedObject

        _comments = New frm_comments(NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(497)), _
                                     NLS_GetString(GLB_NLS_GUI_JACKPOT_MGR.Id(496)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(1)), _
                                     NLS_GetString(GLB_NLS_GUI_CONTROLS.Id(2)), _
                                     False) ' Comments

        Try
          If _comments.ShowGPDialog() = Windows.Forms.DialogResult.OK Then
            jackpot_parameters.Comments = _comments.Comments
            Me.DbStatus = ENUM_STATUS.STATUS_OK
          Else
            jackpot_parameters.Comments = ""
            Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED
          End If
        Finally
          _comments.Dispose()
        End Try

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK AndAlso Me.DbStatus <> ENUM_STATUS.STATUS_NOT_SUPPORTED Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          jackpot_parameters = Me.DbEditedObject
          jackpot_parameters.Comments = ""
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_jackpot_list.ContainsFocus Then
      Return Me.dg_jackpot_list.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    If ButtonId = ENUM_BUTTON.BUTTON_OK Then

      ' Assign to ERROR, so the message will be only shown when OK.

      DbStatus = ENUM_STATUS.STATUS_ERROR

      MyBase.GUI_ButtonClick(ButtonId)

      If DbStatus = ENUM_STATUS.STATUS_OK Then

        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)
      End If

    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If


  End Sub ' GUI_ButtonClick


#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Set jackpot active pct list
  '
  '  PARAMS:
  '     - INPUT:
  '           - JackpotParams
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub SetJackpotList(ByVal JackpotParams As CLASS_SITE_JACKPOT_PARAMS)

    Dim idx As Integer
    Dim last_month_played As Decimal

    Decimal.TryParse(ef_last_month_played.Value, last_month_played)

    Call Me.dg_jackpot_list.Clear()
    idx = 0

    For Each jackpot_instance As CLASS_SITE_JACKPOT_PARAMS.TYPE_SITE_JACKPOT_INSTANCE In JackpotParams.JackpotInstances

      Me.dg_jackpot_list.AddRow()

      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_NAME).Value = jackpot_instance.name
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_CONTRIBUTION).Value = GUI_FormatNumber(jackpot_instance.contribution_pct, 2)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_BET_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.min_bet_amount)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MIN_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.min_amount)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_MAX_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.max_amount)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_DESIRED_AMOUNT).Value = GUI_FormatCurrency(jackpot_instance.average)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_INDEX).Value = CStr(jackpot_instance.index)
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_SHOW_ON_WINUP).Value = BoolToGridValue(jackpot_instance.show_on_winup)

      If String.IsNullOrEmpty(ef_last_month_played.Value) Then
        Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_FREQUENCY).Value = ""
      Else
        Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_FREQUENCY).Value = CalculateFrequency(last_month_played, _
                                                                                       CDbl(contribution_frecuency), _
                                                                                       jackpot_instance.contribution_pct, _
                                                                                       jackpot_instance.average)
      End If

      idx += 1
    Next

  End Sub ' SetJackpotList

  ' PURPOSE: Configure Grid columns
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_StyleView()
    With Me.dg_jackpot_list

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      ' Grid Columns

      'Index
      .Column(GRID_COLUMN_INDEX).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_INDEX).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(288)
      .Column(GRID_COLUMN_INDEX).Width = 800
      .Column(GRID_COLUMN_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Name
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
      .Column(GRID_COLUMN_NAME).Width = 1700
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Editable = True
      .Column(GRID_COLUMN_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_NAME).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_NAME).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 20)

      'Contribution %
      .Column(GRID_COLUMN_CONTRIBUTION).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CONTRIBUTION).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(283)
      .Column(GRID_COLUMN_CONTRIBUTION).Width = 1500
      .Column(GRID_COLUMN_CONTRIBUTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CONTRIBUTION).Editable = True
      .Column(GRID_COLUMN_CONTRIBUTION).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_CONTRIBUTION).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_CONTRIBUTION).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 5, 2)

      'Min Bet Amount
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(284)
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).Editable = True
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MIN_BET_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS)

      'Min. Amount
      .Column(GRID_COLUMN_MIN_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MIN_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(285)
      .Column(GRID_COLUMN_MIN_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_MIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MIN_AMOUNT).Editable = True
      .Column(GRID_COLUMN_MIN_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MIN_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MIN_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Max. Amount
      .Column(GRID_COLUMN_MAX_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MAX_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(286)
      .Column(GRID_COLUMN_MAX_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_MAX_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MAX_AMOUNT).Editable = True
      .Column(GRID_COLUMN_MAX_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MAX_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MAX_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Average Amount
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(287)
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Width = 1500
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_DESIRED_AMOUNT).Editable = True
      .Column(GRID_COLUMN_DESIRED_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_DESIRED_AMOUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_DESIRED_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, MAX_AMOUNT_DIGITS, 2)

      'Fequency
      .Column(GRID_COLUMN_FREQUENCY).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_FREQUENCY).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(339)
      .Column(GRID_COLUMN_FREQUENCY).Width = 1700
      .Column(GRID_COLUMN_FREQUENCY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_FREQUENCY).Editable = False

      'Accumulated (Amount)
      .Column(GRID_COLUMN_ACCUMULATED).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_ACCUMULATED).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(234)
      .Column(GRID_COLUMN_ACCUMULATED).Width = 1530
      .Column(GRID_COLUMN_ACCUMULATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_ACCUMULATED).Editable = False

      'Show on WinUP
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(499) '"Visible On WinUP"
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Width = IIf(Me.m_winUp_enabled, 1000, 0)
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_SHOW_ON_WINUP).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_SHOW_ON_WINUP).Editable = True

    End With

  End Sub ' GUI_StyleView

  Private Sub SetWorkingDays(ByVal Days As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    str_binary = Convert.ToString(Days, 2)
    str_binary = New String("0", 7 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(6) = "0") Then Me.chk_sunday.Checked = False Else Me.chk_sunday.Checked = True ' Sunday
    If (bit_array(5) = "0") Then Me.chk_monday.Checked = False Else Me.chk_monday.Checked = True ' Monday 
    If (bit_array(4) = "0") Then Me.chk_tuesday.Checked = False Else Me.chk_tuesday.Checked = True ' Tuesday
    If (bit_array(3) = "0") Then Me.chk_wednesday.Checked = False Else Me.chk_wednesday.Checked = True ' Wednesday
    If (bit_array(2) = "0") Then Me.chk_thursday.Checked = False Else Me.chk_thursday.Checked = True ' Thursday
    If (bit_array(1) = "0") Then Me.chk_friday.Checked = False Else Me.chk_friday.Checked = True ' Friday
    If (bit_array(0) = "0") Then Me.chk_saturday.Checked = False Else Me.chk_saturday.Checked = True ' Saturday


  End Sub

  Private Function GetWorkingDays() As Integer
    Dim bit_array As Char()

    bit_array = New String("0000000").ToCharArray()

    bit_array(6) = Convert.ToInt32(Me.chk_sunday.Checked).ToString()  ' Sunday
    bit_array(5) = Convert.ToInt32(Me.chk_monday.Checked).ToString()  ' Monday 
    bit_array(4) = Convert.ToInt32(Me.chk_tuesday.Checked).ToString()  ' Tuesday
    bit_array(3) = Convert.ToInt32(Me.chk_wednesday.Checked).ToString()  ' Wednesday
    bit_array(2) = Convert.ToInt32(Me.chk_thursday.Checked).ToString()  ' Thursday
    bit_array(1) = Convert.ToInt32(Me.chk_friday.Checked).ToString()  ' Friday
    bit_array(0) = Convert.ToInt32(Me.chk_saturday.Checked).ToString()  ' Saturday

    Return Convert.ToInt32(bit_array, 2)

  End Function

  ' PURPOSE: Refresch Jackpot parameters: Accumulated for index jackpot and global to compensate.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshJackpot()
    Dim jackpot_parameters As New CLASS_SITE_JACKPOT_PARAMS

    If jackpot_parameters.DB_Read(0, 0) <> ENUM_STATUS.STATUS_OK Then
      Return
    End If

    RefreshJackpot(jackpot_parameters)
  End Sub ' RefreshJackpot

  ' PURPOSE: Refresch Jackpot parameters: Accumulated for index jackpot and global to compensate.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshJackpot(ByVal JackpotParams As CLASS_SITE_JACKPOT_PARAMS)
    Dim total_accumulated As Double
    Dim idx As Integer

    ' To Compensate
    If JackpotParams.ToCompensate < 0 Then
      Me.lbl_to_compensate.Value = GUI_FormatCurrency(Math.Abs(JackpotParams.ToCompensate))
      Me.lbl_to_compensate.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(337)
      Me.lbl_to_compensate.LabelForeColor = System.Drawing.Color.Blue
    Else
      Me.lbl_to_compensate.Value = GUI_FormatCurrency(JackpotParams.ToCompensate)
      Me.lbl_to_compensate.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(336)
      Me.lbl_to_compensate.LabelForeColor = System.Drawing.Color.Red
    End If

    If JackpotParams.CurrentCompensationPct < 0 Then
      Me.lbl_current_compensate_pct.Value = FormatPercent(0)
    Else
      Me.lbl_current_compensate_pct.Value = FormatPercent(JackpotParams.CurrentCompensationPct / 100.0)
    End If

    total_accumulated = 0
    idx = 0

    For Each jackpot_instance As CLASS_SITE_JACKPOT_PARAMS.TYPE_SITE_JACKPOT_INSTANCE In JackpotParams.JackpotInstances
      Me.dg_jackpot_list.Cell(idx, GRID_COLUMN_ACCUMULATED).Value = GUI_FormatCurrency(jackpot_instance.accumulated)
      total_accumulated += jackpot_instance.accumulated
      idx += 1
    Next

    Me.lbl_accumulated_total.Value = GUI_FormatCurrency(total_accumulated)

  End Sub ' RefreshJackpot

  ' PURPOSE: Get General params Range Site Jackpot 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '         
  ' RETURNS:
  '     - TYPE_SITE_JACKPOT_PARAMS_RANGE
  '
  Private Function GetParamsRangeSiteJackpot() As TYPE_SITE_JACKPOT_PARAMS_RANGE
    Dim _m_params_range As New TYPE_SITE_JACKPOT_PARAMS_RANGE
    Dim _idx_grid As Byte = 0
    Dim _idx_jackpot_instance As Byte = 0
    Dim _num_rows_grid As Byte = Me.dg_jackpot_list.NumRows
    Dim _minimum_value_jackpot_instance As Double = 5000

    'BlockOnOutOfRange
    _m_params_range.block_on_out_of_range = WSI.Common.GeneralParam.GetBoolean("SiteJackpot", "Range.BlockOnOutOfRange", True)

    'Range.ContributionPct
    WSI.Common.GeneralParam.GetDecimalRange("SiteJackpot", "Range.ContributionPct", _m_params_range.contribution_pct_range_min, _m_params_range.contribution_pct_range_max, 0, 0.05)

    'Range.Occupation
    WSI.Common.GeneralParam.GetDecimalRange("SiteJackpot", "Range.Occupation", _m_params_range.occupattion_pct_range_min, _m_params_range.occupattion_pct_range_max, 0, 100)

    For _idx_grid = 0 To _num_rows_grid - 1
      Dim _m_params_range_instance_jackpot As New TYPE_SITE_JACKPOT_INSTANCE_PARAMS_RANGE

      _idx_jackpot_instance = Me.dg_jackpot_list.Cell(_idx_grid, GRID_COLUMN_INDEX).Value()

      'Range.Jackpot<n> ContributionPct 
      WSI.Common.GeneralParam.GetDecimalRange("SiteJackpot", "Range.Jackpot0" & _idx_jackpot_instance & ".ContributionPct", _m_params_range_instance_jackpot.contribution_pct_range_min, _m_params_range_instance_jackpot.contribution_pct_range_max, 0, 100)

      'Range.Jackpot<n> Minimum
      WSI.Common.GeneralParam.GetDecimalRange("SiteJackpot", "Range.Jackpot0" & _idx_jackpot_instance & ".Minimum", _m_params_range_instance_jackpot.min_amount_range_min, _m_params_range_instance_jackpot.min_amount_range_max, 0, _minimum_value_jackpot_instance)

      _m_params_range.jackpot_instance_range.Add(_m_params_range_instance_jackpot)

      _minimum_value_jackpot_instance *= 10
    Next

    Return _m_params_range
  End Function 'GetParamsRangeSiteJackpot

  ' PURPOSE:  Sorts the week days control depending on the system's first day of week
  '           It's private because is used only here. Adapt it and convert it in public if needed in other places
  '           It's a modified copy of mdl_Misc.vb/SortCheckBoxesByFirstDayOfWeek
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SortControlByFirstDayOfWeek(ByVal WeekDaysArray() As Control)
    Dim _aux_array(6) As Control
    Dim _first_day_of_week As Int32
    Dim _idx As Int32
    Dim _idx_index As Int32

    ' Check the input array length
    If WeekDaysArray.Length <> _aux_array.Length Then
      Return
    End If

    ' Check system's first day of week
    _first_day_of_week = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek()
    If _first_day_of_week = 0 Then
      Return
    End If

    ' Store week days arrays into a temporary arrays
    For _idx = 0 To WeekDaysArray.Length - 1
      If TypeOf WeekDaysArray(0) Is Label Then
        _aux_array(_idx) = New Label
      ElseIf TypeOf WeekDaysArray(0) Is uc_entry_field Then
        _aux_array(_idx) = New uc_entry_field
      End If
      _aux_array(_idx).Location = WeekDaysArray(_idx).Location
      _aux_array(_idx).TabIndex = WeekDaysArray(_idx).TabIndex
    Next

    ' Store the output week days array starting on the proper day
    For _idx = 0 To WeekDaysArray.Length - 1
      _idx_index = (_first_day_of_week + _idx) Mod 7

      WeekDaysArray(_idx_index).Location = _aux_array(_idx).Location
      WeekDaysArray(_idx_index).TabIndex = _aux_array(_idx).TabIndex
    Next

  End Sub

  Private Function GetContributionValue() As Double
    CalculateAvgContributionWeekDays()

    If opt_fix_contribution.Checked Then
      Return Double.TryParse(ef_contribution.Value, contribution_frecuency)
    Else
      Select Case Today.DayOfWeek
        Case DayOfWeek.Sunday
          Return Double.TryParse(ef_sunday.Value, contribution_frecuency)
        Case DayOfWeek.Monday
          Return Double.TryParse(ef_monday.Value, contribution_frecuency)
        Case DayOfWeek.Tuesday
          Return Double.TryParse(ef_tuesday.Value, contribution_frecuency)
        Case DayOfWeek.Wednesday
          Return Double.TryParse(ef_wednesday.Value, contribution_frecuency)
        Case DayOfWeek.Thursday
          Return Double.TryParse(ef_thursday.Value, contribution_frecuency)
        Case DayOfWeek.Friday
          Return Double.TryParse(ef_friday.Value, contribution_frecuency)
        Case DayOfWeek.Saturday
          Return Double.TryParse(ef_saturday.Value, contribution_frecuency)
      End Select
    End If

  End Function

  Private Sub ShowAndEnableContributionDaysOfWeek(ByVal ContributionPct As List(Of Decimal), ByVal pbEnabled As Boolean)
    If Not ContributionPct Is Nothing Then
      If ContributionPct.Count = 0 Then
        ef_sunday.Value = ""
        ef_monday.Value = ""
        ef_tuesday.Value = ""
        ef_wednesday.Value = ""
        ef_thursday.Value = ""
        ef_friday.Value = ""
        ef_saturday.Value = ""
      Else
        ef_sunday.Value = ContributionPct(1)
        ef_monday.Value = ContributionPct(2)
        ef_tuesday.Value = ContributionPct(3)
        ef_wednesday.Value = ContributionPct(4)
        ef_thursday.Value = ContributionPct(5)
        ef_friday.Value = ContributionPct(6)
        ef_saturday.Value = ContributionPct(7)
      End If
    End If
    ef_contribution.Enabled = Not pbEnabled
    ef_sunday.Enabled = pbEnabled
    ef_monday.Enabled = pbEnabled
    ef_tuesday.Enabled = pbEnabled
    ef_wednesday.Enabled = pbEnabled
    ef_thursday.Enabled = pbEnabled
    ef_friday.Enabled = pbEnabled
    ef_saturday.Enabled = pbEnabled
  End Sub

  ' PURPOSE:  Calculate Average contribution with all contribution days
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - Average As Decimal
  '
  ' RETURNS:
  '     - None
  Private Function CalculateAvgContributionWeekDays() As Decimal
    Dim _avg_contribution_frequency As Decimal

    _avg_contribution_frequency = 0

    _avg_contribution_frequency = (GUI_ParseNumber(Me.ef_monday.Value) + GUI_ParseNumber(Me.ef_saturday.Value) + GUI_ParseNumber(Me.ef_sunday.Value) + GUI_ParseNumber(Me.ef_thursday.Value) + GUI_ParseNumber(Me.ef_tuesday.Value) + GUI_ParseNumber(Me.ef_wednesday.Value) + GUI_ParseNumber(Me.ef_friday.Value)) / 7

    If (Not IsNumeric(ef_last_month_played.Value)) Or Me.opt_fix_contribution.Checked Then
      lbl_info_avg_contribution_week_days.Visible = False
    Else
      lbl_info_avg_contribution_week_days.Visible = True
    End If
    lbl_info_avg_contribution_week_days.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(498, Decimal.Round(_avg_contribution_frequency, 3).ToString)

    Return GUI_ParseNumber(_avg_contribution_frequency)

  End Function ' GetAvgContributionWeekDays

  Private Sub AddHandlers()
    AddHandler Me.ef_monday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
    AddHandler Me.ef_tuesday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
    AddHandler Me.ef_wednesday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
    AddHandler Me.ef_thursday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
    AddHandler Me.ef_friday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
    AddHandler Me.ef_saturday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
    AddHandler Me.ef_sunday.EntryFieldValueChanged, AddressOf ef_WeekDay_EntryFieldValueChanged
  End Sub ' AddHandlers

  Private Sub UpdateFrequency()
    Dim _jackpot_parameters As CLASS_SITE_JACKPOT_PARAMS
    Dim _idx As Integer
    Dim _instance_contribution_pct As Double
    Dim _instance_average As Double
    Dim _last_month_played As Decimal
    Dim _avg_contribution_frequency As Decimal

    _jackpot_parameters = Me.DbReadObject

    If Not _jackpot_parameters Is Nothing Then

      Decimal.TryParse(ef_last_month_played.Value, _last_month_played)

      _avg_contribution_frequency = CalculateAvgContributionWeekDays()

      _idx = 0
      For Each jackpot_instance As CLASS_SITE_JACKPOT_PARAMS.TYPE_SITE_JACKPOT_INSTANCE In _jackpot_parameters.JackpotInstances
        Double.TryParse(Me.dg_jackpot_list.Cell(_idx, GRID_COLUMN_CONTRIBUTION).Value, _instance_contribution_pct)
        _instance_average = GUI_ParseCurrency(Me.dg_jackpot_list.Cell(_idx, GRID_COLUMN_DESIRED_AMOUNT).Value)

        If String.IsNullOrEmpty(ef_last_month_played.Value) Then
          Me.dg_jackpot_list.Cell(_idx, GRID_COLUMN_FREQUENCY).Value = ""
        Else
          If Me.opt_week_days.Checked Then

            Me.dg_jackpot_list.Cell(_idx, GRID_COLUMN_FREQUENCY).Value = CalculateFrequency(_last_month_played, _
                                                                                                   CDbl(_avg_contribution_frequency), _
                                                                                                   _instance_contribution_pct, _
                                                                                                   _instance_average)
          Else
            Me.dg_jackpot_list.Cell(_idx, GRID_COLUMN_FREQUENCY).Value = CalculateFrequency(_last_month_played, _
                                                                                                   CDbl(contribution_frecuency), _
                                                                                                   _instance_contribution_pct, _
                                                                                                   _instance_average)
          End If

        End If

        _idx += 1
      Next
    End If
  End Sub ' UpdateFrequency





  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue


  ' PURPOSE:  Cast from  uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValuetoBool(ByVal enabled As String) As Boolean
    If enabled = uc_grid.GRID_CHK_CHECKED Or enabled = uc_grid.GRID_CHK_CHECKED_DISABLED Then
      Return True
    Else
      Return False
    End If
  End Function 'GridValuetoBool


#End Region ' Private Functions

#Region "Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(False)
    End If

  End Sub ' ShowEditItem

#End Region ' Public Functions

#Region "Events"

  ' PURPOSE: Handle event when edit value for any week day
  '
  '  PARAMS:
  '     - INPUT:
  '           - mdiparent
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Private Sub ef_WeekDay_EntryFieldValueChanged()
    If opt_week_days.Checked Then
      Call GetContributionValue()
      UpdateFrequency()
    End If
  End Sub ' ef_WeekDay_EntryFieldValueChanged

  Private Sub ef_contribution_EntryFieldValueChanged() Handles ef_contribution.EntryFieldValueChanged
    If opt_fix_contribution.Checked Then
      Call GetContributionValue()
      ef_contribution.Tag = ef_contribution.Value
      UpdateFrequency()
    End If
  End Sub

  Private Sub ef_last_month_played_EntryFieldValueChanged() Handles ef_last_month_played.EntryFieldValueChanged
    UpdateFrequency()
  End Sub

  Private Sub dg_jackpot_list_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_jackpot_list.CellDataChangedEvent
    UpdateFrequency()
  End Sub

  Private Sub chk_only_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_only_redeemable.CheckedChanged
    If chk_only_redeemable.Checked Then
      lbl_percentaje.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(333)
    Else
      lbl_percentaje.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(334)
    End If
  End Sub

  Private Sub btn_load_last_month_played_ClickEvent() Handles btn_load_last_month_played.ClickEvent
    Dim prev_value As String

    prev_value = ef_last_month_played.Value
    ef_last_month_played.Value = GUI_FormatCurrency(CalculateLastMonthPlayed(chk_only_redeemable.Checked, False))

    If prev_value <> ef_last_month_played.Value Then
      UpdateFrequency()
    End If
  End Sub

  ' PURPOSE: Timer operation to refresh the grid.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub tmr_refresh_jackpot_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_refresh_jackpot.Tick
    RefreshJackpot()
  End Sub

  Private Sub opt_fix_contribution_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_fix_contribution.Click
    Call ShowAndEnableContributionDaysOfWeek(Nothing, False)
    Call GetContributionValue()
    Call UpdateFrequency()
  End Sub

  Private Sub opt_week_days_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_week_days.Click
    Dim _contributionPct = New List(Of Decimal)
    Dim _contrib As Decimal
    Dim jp_params_read As CLASS_SITE_JACKPOT_PARAMS

    jp_params_read = Me.DbReadObject
    If String.IsNullOrEmpty(ef_monday.Value) Then
      For Each _contrib In jp_params_read.ContributionPct
        If String.IsNullOrEmpty(ef_contribution.Value) Then
          _contributionPct.Add(0)
        Else
          _contributionPct.Add(ef_contribution.Value)
        End If
      Next
      Call ShowAndEnableContributionDaysOfWeek(_contributionPct, True)
    Else
      Call ShowAndEnableContributionDaysOfWeek(Nothing, True)
    End If

    Call GetContributionValue()
    Call UpdateFrequency()
  End Sub

  ' PURPOSE: Commom error message
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Function ContributionMoreThan100(ByVal pef As uc_entry_field) As Boolean
    If GUI_FormatNumber(pef.Value) > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call pef.Focus()
      Return False
    Else
      Return True
    End If

    If GUI_FormatNumber(pef.Value) < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call pef.Focus()
      Return False
    Else
      Return True
    End If
  End Function

  Private Function ContributionRange(ByVal pef As uc_entry_field, ByVal jp_params_range As TYPE_SITE_JACKPOT_PARAMS_RANGE, _
      ByRef _error_range As Boolean, ByRef _site_jackpot_range_errors As SITE_JACKPOT_RANGE_ERROR) As Boolean

    If (pef.Value < jp_params_range.contribution_pct_range_min OrElse _
       pef.Value > jp_params_range.contribution_pct_range_max) Then

      If jp_params_range.block_on_out_of_range = True Then
        NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(133), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , jp_params_range.contribution_pct_range_min, jp_params_range.contribution_pct_range_max)
        Call pef.Focus()
        Return False
      Else
        _error_range = True
      End If
      _site_jackpot_range_errors.contribution_perc_error = True
      _site_jackpot_range_errors.contribution_perc_value = pef.Value
    End If
    Return True

  End Function

#End Region ' Events

End Class