<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_progressive_handpay
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_progrssive_name = New GUI_Controls.uc_entry_field
    Me.uc_progressive_levels = New GUI_Controls.uc_combo
    Me.uc_progressive_terminals = New GUI_Controls.uc_combo
    Me.ef_progressive_amount = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.ef_progressive_amount)
    Me.panel_data.Controls.Add(Me.uc_progressive_terminals)
    Me.panel_data.Controls.Add(Me.uc_progressive_levels)
    Me.panel_data.Controls.Add(Me.ef_progrssive_name)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(428, 98)
    '
    'ef_progrssive_name
    '
    Me.ef_progrssive_name.DoubleValue = 0
    Me.ef_progrssive_name.IntegerValue = 0
    Me.ef_progrssive_name.IsReadOnly = False
    Me.ef_progrssive_name.Location = New System.Drawing.Point(3, 3)
    Me.ef_progrssive_name.Name = "ef_progrssive_name"
    Me.ef_progrssive_name.Size = New System.Drawing.Size(406, 24)
    Me.ef_progrssive_name.SufixText = "Sufix Text"
    Me.ef_progrssive_name.SufixTextVisible = True
    Me.ef_progrssive_name.TabIndex = 0
    Me.ef_progrssive_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_progrssive_name.TextValue = ""
    Me.ef_progrssive_name.Value = ""
    '
    'uc_progressive_levels
    '
    Me.uc_progressive_levels.AllowUnlistedValues = False
    Me.uc_progressive_levels.AutoCompleteMode = False
    Me.uc_progressive_levels.IsReadOnly = True
    Me.uc_progressive_levels.Location = New System.Drawing.Point(3, 34)
    Me.uc_progressive_levels.Name = "uc_progressive_levels"
    Me.uc_progressive_levels.SelectedIndex = -1
    Me.uc_progressive_levels.Size = New System.Drawing.Size(200, 24)
    Me.uc_progressive_levels.SufixText = "Sufix Text"
    Me.uc_progressive_levels.SufixTextVisible = True
    Me.uc_progressive_levels.TabIndex = 1
    '
    'uc_progressive_terminals
    '
    Me.uc_progressive_terminals.AllowUnlistedValues = False
    Me.uc_progressive_terminals.AutoCompleteMode = False
    Me.uc_progressive_terminals.IsReadOnly = True
    Me.uc_progressive_terminals.Location = New System.Drawing.Point(209, 34)
    Me.uc_progressive_terminals.Name = "uc_progressive_terminals"
    Me.uc_progressive_terminals.SelectedIndex = -1
    Me.uc_progressive_terminals.Size = New System.Drawing.Size(200, 24)
    Me.uc_progressive_terminals.SufixText = "Sufix Text"
    Me.uc_progressive_terminals.SufixTextVisible = True
    Me.uc_progressive_terminals.TabIndex = 2
    '
    'ef_progressive_amount
    '
    Me.ef_progressive_amount.DoubleValue = 0
    Me.ef_progressive_amount.IntegerValue = 0
    Me.ef_progressive_amount.IsReadOnly = False
    Me.ef_progressive_amount.Location = New System.Drawing.Point(3, 64)
    Me.ef_progressive_amount.Name = "ef_progressive_amount"
    Me.ef_progressive_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_progressive_amount.SufixText = "Sufix Text"
    Me.ef_progressive_amount.SufixTextVisible = True
    Me.ef_progressive_amount.TabIndex = 3
    Me.ef_progressive_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_progressive_amount.TextValue = ""
    Me.ef_progressive_amount.Value = ""
    '
    'frm_progressive_handpay
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(526, 106)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_progressive_handpay"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_progressive_handpay"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_progressive_levels As GUI_Controls.uc_combo
  Friend WithEvents ef_progrssive_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_progressive_amount As GUI_Controls.uc_entry_field
  Friend WithEvents uc_progressive_terminals As GUI_Controls.uc_combo
End Class
