<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_peru_frames
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_peru_frames = New GUI_Controls.uc_grid
    Me.btn_pdte = New GUI_Controls.uc_button
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.chk_economical = New System.Windows.Forms.CheckBox
    Me.chk_technical = New System.Windows.Forms.CheckBox
    Me.gb_type = New System.Windows.Forms.GroupBox
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_error = New System.Windows.Forms.Label
    Me.lbl_sended = New System.Windows.Forms.Label
    Me.lbl_pending = New System.Windows.Forms.Label
    Me.chk_error = New System.Windows.Forms.CheckBox
    Me.chk_sended = New System.Windows.Forms.CheckBox
    Me.chk_pending = New System.Windows.Forms.CheckBox
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.uc_frame_viewer = New GUI_Controls.uc_frame_viewer
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_grids.SuspendLayout()
    Me.panel_buttons.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.uc_frame_viewer)
    Me.panel_grids.Controls.Add(Me.dg_peru_frames)
    Me.panel_grids.Location = New System.Drawing.Point(4, 203)
    Me.panel_grids.Size = New System.Drawing.Size(1226, 526)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_peru_frames, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.uc_frame_viewer, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_pdte)
    Me.panel_buttons.Location = New System.Drawing.Point(1138, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 526)
    Me.panel_buttons.Controls.SetChildIndex(Me.btn_pdte, 0)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 189)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 193)
    Me.pn_separator_line.Size = New System.Drawing.Size(1226, 10)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1226, 10)
    '
    'dg_peru_frames
    '
    Me.dg_peru_frames.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_peru_frames.CurrentCol = -1
    Me.dg_peru_frames.CurrentRow = -1
    Me.dg_peru_frames.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_peru_frames.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_peru_frames.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_peru_frames.Location = New System.Drawing.Point(3, 6)
    Me.dg_peru_frames.Name = "dg_peru_frames"
    Me.dg_peru_frames.PanelRightVisible = True
    Me.dg_peru_frames.Redraw = True
    Me.dg_peru_frames.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_peru_frames.Size = New System.Drawing.Size(1129, 518)
    Me.dg_peru_frames.Sortable = False
    Me.dg_peru_frames.SortAscending = True
    Me.dg_peru_frames.SortByCol = 0
    Me.dg_peru_frames.TabIndex = 1
    Me.dg_peru_frames.ToolTipped = True
    Me.dg_peru_frames.TopRow = -2
    '
    'btn_pdte
    '
    Me.btn_pdte.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_pdte.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_pdte.Location = New System.Drawing.Point(0, 207)
    Me.btn_pdte.Name = "btn_pdte"
    Me.btn_pdte.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_pdte.Size = New System.Drawing.Size(90, 30)
    Me.btn_pdte.TabIndex = 12
    Me.btn_pdte.ToolTipped = False
    Me.btn_pdte.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'uc_pr_list
    '
    Me.uc_pr_list.FilterByCurrency = False
    Me.uc_pr_list.Location = New System.Drawing.Point(433, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(406, 188)
    Me.uc_pr_list.TabIndex = 3
    '
    'chk_economical
    '
    Me.chk_economical.Location = New System.Drawing.Point(13, 18)
    Me.chk_economical.Name = "chk_economical"
    Me.chk_economical.Size = New System.Drawing.Size(146, 18)
    Me.chk_economical.TabIndex = 0
    Me.chk_economical.Text = "xEconomical"
    '
    'chk_technical
    '
    Me.chk_technical.Location = New System.Drawing.Point(13, 42)
    Me.chk_technical.Name = "chk_technical"
    Me.chk_technical.Size = New System.Drawing.Size(146, 20)
    Me.chk_technical.TabIndex = 1
    Me.chk_technical.Text = "xTechnical"
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.chk_economical)
    Me.gb_type.Controls.Add(Me.chk_technical)
    Me.gb_type.Location = New System.Drawing.Point(262, 6)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(165, 72)
    Me.gb_type.TabIndex = 2
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xType"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.lbl_sended)
    Me.gb_status.Controls.Add(Me.lbl_pending)
    Me.gb_status.Controls.Add(Me.chk_error)
    Me.gb_status.Controls.Add(Me.chk_sended)
    Me.gb_status.Controls.Add(Me.chk_pending)
    Me.gb_status.Location = New System.Drawing.Point(6, 84)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(250, 92)
    Me.gb_status.TabIndex = 1
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_error
    '
    Me.lbl_error.AutoSize = True
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(19, 66)
    Me.lbl_error.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_error.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(15, 15)
    Me.lbl_error.TabIndex = 4
    Me.lbl_error.Text = "E"
    '
    'lbl_sended
    '
    Me.lbl_sended.AutoSize = True
    Me.lbl_sended.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_sended.Location = New System.Drawing.Point(19, 43)
    Me.lbl_sended.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_sended.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_sended.Name = "lbl_sended"
    Me.lbl_sended.Size = New System.Drawing.Size(15, 15)
    Me.lbl_sended.TabIndex = 2
    Me.lbl_sended.Text = "S"
    '
    'lbl_pending
    '
    Me.lbl_pending.AutoSize = True
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(19, 20)
    Me.lbl_pending.MaximumSize = New System.Drawing.Size(15, 15)
    Me.lbl_pending.MinimumSize = New System.Drawing.Size(15, 15)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(15, 15)
    Me.lbl_pending.TabIndex = 0
    Me.lbl_pending.Text = "P"
    '
    'chk_error
    '
    Me.chk_error.Location = New System.Drawing.Point(52, 65)
    Me.chk_error.Name = "chk_error"
    Me.chk_error.Size = New System.Drawing.Size(99, 16)
    Me.chk_error.TabIndex = 5
    Me.chk_error.Text = "xError"
    '
    'chk_sended
    '
    Me.chk_sended.Location = New System.Drawing.Point(52, 42)
    Me.chk_sended.Name = "chk_sended"
    Me.chk_sended.Size = New System.Drawing.Size(99, 16)
    Me.chk_sended.TabIndex = 3
    Me.chk_sended.Text = "xSended"
    '
    'chk_pending
    '
    Me.chk_pending.Location = New System.Drawing.Point(52, 18)
    Me.chk_pending.Name = "chk_pending"
    Me.chk_pending.Size = New System.Drawing.Size(99, 18)
    Me.chk_pending.TabIndex = 1
    Me.chk_pending.Text = "xPending"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(250, 72)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xOpen Date"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(239, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(239, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_frame_viewer
    '
    Me.uc_frame_viewer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.uc_frame_viewer.AutoScroll = True
    Me.uc_frame_viewer.Location = New System.Drawing.Point(3, 525)
    Me.uc_frame_viewer.Name = "uc_frame_viewer"
    Me.uc_frame_viewer.Size = New System.Drawing.Size(1062, 304)
    Me.uc_frame_viewer.TabIndex = 2
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(827, 24)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(247, 17)
    Me.chk_terminal_location.TabIndex = 4
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'frm_peru_frames
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1234, 733)
    Me.Name = "frm_peru_frames"
    Me.Text = "frm_peru_frames"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_buttons.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_peru_frames As GUI_Controls.uc_grid
  Friend WithEvents btn_pdte As GUI_Controls.uc_button
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_economical As System.Windows.Forms.CheckBox
  Friend WithEvents chk_technical As System.Windows.Forms.CheckBox
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_sended As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_error As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents lbl_sended As System.Windows.Forms.Label
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents uc_frame_viewer As GUI_Controls.uc_frame_viewer
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
