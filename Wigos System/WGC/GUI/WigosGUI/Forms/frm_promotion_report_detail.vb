'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotion_report_detail
' DESCRIPTION:   Promotion Report Detail
' AUTHOR:        Raul Cervera
' CREATION DATE: 09-JUN-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-JUN-2011  RCI    Initial version
' 31-MAY-2011  JAB    Fixed Bug #307: Add Note Acceptor promotions
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck
' 06-MAY-2014  JBP    Added Vip client filter at reports.
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data

Public Class frm_promotion_report_detail
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents lbl_promotion As System.Windows.Forms.Label
  Friend WithEvents lbl_account As System.Windows.Forms.Label

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.lbl_promotion = New System.Windows.Forms.Label
    Me.lbl_account = New System.Windows.Forms.Label
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_account)
    Me.panel_filter.Controls.Add(Me.lbl_promotion)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1096, 146)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_promotion, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_account, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 150)
    Me.panel_data.Size = New System.Drawing.Size(1096, 509)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1090, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1090, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(249, 3)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 2
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(237, 72)
    Me.gb_date.TabIndex = 11
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(222, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(222, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'lbl_promotion
    '
    Me.lbl_promotion.AutoSize = True
    Me.lbl_promotion.Location = New System.Drawing.Point(576, 20)
    Me.lbl_promotion.Name = "lbl_promotion"
    Me.lbl_promotion.Size = New System.Drawing.Size(179, 13)
    Me.lbl_promotion.TabIndex = 12
    Me.lbl_promotion.Text = "xPromotion: xPromotionName"
    '
    'lbl_account
    '
    Me.lbl_account.AutoSize = True
    Me.lbl_account.Location = New System.Drawing.Point(576, 41)
    Me.lbl_account.Name = "lbl_account"
    Me.lbl_account.Size = New System.Drawing.Size(165, 13)
    Me.lbl_account.TabIndex = 13
    Me.lbl_account.Text = "xAccount: xAccountIdName"
    '
    'frm_promotion_report_detail
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1104, 663)
    Me.Name = "frm_promotion_report_detail"
    Me.Text = "frm_promotion_report_detail"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_DATETIME As Integer = 0
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 1
  Private Const SQL_COLUMN_ACCOUNT_NAME As Integer = 2
  Private Const SQL_COLUMN_CASHIER_NAME As Integer = 3
  Private Const SQL_COLUMN_OPERATION_CODE As Integer = 4
  Private Const SQL_COLUMN_POINTS As Integer = 5
  Private Const SQL_COLUMN_CASH_IN As Integer = 6
  Private Const SQL_COLUMN_NON_REDEEMABLE As Integer = 7
  Private Const SQL_COLUMN_REDEEMABLE As Integer = 8

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_DATETIME As Integer = 1
  Private Const GRID_COLUMN_ACCOUNT_ID_NAME As Integer = 2
  Private Const GRID_COLUMN_CASHIER_NAME As Integer = 3
  Private Const GRID_COLUMN_POINTS As Integer = 4
  Private Const GRID_COLUMN_CASH_IN As Integer = 5
  Private Const GRID_COLUMN_NON_REDEEMABLE As Integer = 6
  Private Const GRID_COLUMN_REDEEMABLE As Integer = 7

  Private Const GRID_COLUMNS As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_ACCOUNT As Integer = 3000
  Private Const GRID_WIDTH_CASHIER As Integer = 1500
  Private Const GRID_WIDTH_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_POINTS As Integer = 1000

  ' Row types
  Private Const ROW_SUBTYPE_PROMO_TOTAL As Integer = 0
  Private Const ROW_SUBTYPE_PROMO_BY_ACCOUNT As Integer = 1

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_account As String
  Private m_track_data As String
  Private m_holder As String
  Private m_holder_is_vip As String

  Private m_detail_from_date As Date
  Private m_detail_to_date As Date
  Private m_detail_type As Integer
  Private m_detail_promotion_id As Long
  Private m_detail_account_id As Long
  Private m_detail_promotion_name As String
  Private m_detail_account_id_name As String

  ' For total amount row
  Dim m_total_points As Double
  Dim m_total_cash_in As Double
  Dim m_total_non_redeemable As Double
  Dim m_total_redeemable As Double

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROMOTION_REPORT_DETAIL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(355)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Label Promotion
    Me.lbl_promotion.Text = GLB_NLS_GUI_INVOICING.GetString(311) & ": " & m_detail_promotion_name

    If m_detail_type = ROW_SUBTYPE_PROMO_BY_ACCOUNT Then
      ' Label Account
      Me.lbl_account.Text = GLB_NLS_GUI_INVOICING.GetString(230) & ": " & m_detail_account_id_name
      Me.lbl_account.Visible = True
      ' Account filter is not visible when detail is by promotion and account.
      Me.uc_account_sel1.Visible = False

      Me.ClientSize = New System.Drawing.Size(Me.ClientSize.Width - 200, Me.ClientSize.Height)
      Me.lbl_promotion.Location = New System.Drawing.Point(Me.lbl_promotion.Location.X - Me.uc_account_sel1.Width - 5, Me.lbl_promotion.Location.Y)
      Me.lbl_account.Location = New System.Drawing.Point(Me.lbl_account.Location.X - Me.uc_account_sel1.Width - 5, Me.lbl_account.Location.Y)
    Else
      Me.lbl_account.Visible = False
      Me.uc_account_sel1.Visible = True
    End If

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_total_points = 0
    m_total_cash_in = 0
    m_total_non_redeemable = 0
    m_total_redeemable = 0

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    Me.Grid.AddRow()

    Dim idx_row As Integer = Me.Grid.NumRows - 1

    ' Dates
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATETIME).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    ' Total Points
    Me.Grid.Cell(idx_row, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(m_total_points, 0)

    ' Total Cash-In
    Me.Grid.Cell(idx_row, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(m_total_cash_in, _
                                                                          ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Total Non-Redeemable
    Me.Grid.Cell(idx_row, GRID_COLUMN_NON_REDEEMABLE).Value = GUI_FormatCurrency(m_total_non_redeemable, _
                                                                                 ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Total Redeemable
    Me.Grid.Cell(idx_row, GRID_COLUMN_REDEEMABLE).Value = GUI_FormatCurrency(m_total_redeemable, _
                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_sel1.ValidateFormat Then
      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim str_sql As String

    str_sql = "SELECT " & _
              "         AO_DATETIME DATETIME " & _
              "       , AO_ACCOUNT_ID ACCOUNT_ID" & _
              "       , (SELECT AC_HOLDER_NAME FROM ACCOUNTS WHERE AC_ACCOUNT_ID = AO_ACCOUNT_ID)  ACCOUNT_NAME " & _
              "       , (SELECT GU_USERNAME FROM GUI_USERS, CASHIER_SESSIONS WHERE GU_USER_ID = CS_USER_ID AND CS_SESSION_ID = AO_CASHIER_SESSION_ID) CASHIER_NAME " & _
              "       , AO_CODE OPERATION_CODE " & _
              "       , CASE WHEN AO_CODE       = 7 THEN AO_AMOUNT ELSE 0 END POINTS " & _
              "       , CASE WHEN AO_CODE IN (3, 5, 110) THEN AO_AMOUNT ELSE 0 END CASH_IN " & _
              "       , CASE WHEN AO_WON_LOCK  >= 0 THEN AO_NON_REDEEMABLE ELSE 0 END NON_REDEEMABLE " & _
              "       , CASE WHEN AO_WON_LOCK  <  0 THEN AO_NON_REDEEMABLE ELSE 0 END REDEEMABLE " & _
              "  FROM   ACCOUNT_OPERATIONS " & _
              " WHERE   AO_CODE IN (3, 5, 7, 110) " & _
              "   AND   AO_PROMO_ID = " & m_detail_promotion_id & " "

    If m_detail_type = ROW_SUBTYPE_PROMO_BY_ACCOUNT Then
      str_sql = str_sql & " AND AO_ACCOUNT_ID = " & m_detail_account_id & " "
    End If

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY DATETIME DESC "

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    ' Datetime
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATETIME).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATETIME), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Account Id + Name
    If m_detail_type <> ROW_SUBTYPE_PROMO_BY_ACCOUNT Then

      If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
        If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_NAME) Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID) & " - " & DbRow.Value(SQL_COLUMN_ACCOUNT_NAME)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID_NAME).Value = ""
      End If

    End If

    ' Cashier Name
    If Not DbRow.IsNull(SQL_COLUMN_CASHIER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = DbRow.Value(SQL_COLUMN_CASHIER_NAME)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHIER_NAME).Value = ""
    End If

    ' Default values cash-in / points
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = ""
    Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = ""

    ' Points
    If DbRow.Value(SQL_COLUMN_OPERATION_CODE) = 7 Then
      If Not DbRow.IsNull(SQL_COLUMN_POINTS) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_POINTS), 0)
        m_total_points += DbRow.Value(SQL_COLUMN_POINTS)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_POINTS).Value = GUI_FormatNumber(0, 0)
      End If
    End If

    ' Cash-In
    If DbRow.Value(SQL_COLUMN_OPERATION_CODE) <> 7 Then
      If Not DbRow.IsNull(SQL_COLUMN_CASH_IN) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASH_IN), _
                                                                               ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        m_total_cash_in += DbRow.Value(SQL_COLUMN_CASH_IN)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_IN).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      End If
    End If

    ' Non-Redeemable
    If Not DbRow.IsNull(SQL_COLUMN_NON_REDEEMABLE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_REDEEMABLE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NON_REDEEMABLE), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_total_non_redeemable += DbRow.Value(SQL_COLUMN_NON_REDEEMABLE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NON_REDEEMABLE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Redeemable
    If Not DbRow.IsNull(SQL_COLUMN_REDEEMABLE) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDEEMABLE), _
                                                                                ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_total_redeemable += DbRow.Value(SQL_COLUMN_REDEEMABLE)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDEEMABLE).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.dtp_from
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(309), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_STATISTICS.GetString(310), m_date_to)

    If m_detail_type = ROW_SUBTYPE_PROMO_BY_ACCOUNT Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(311), m_detail_promotion_name)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_detail_account_id_name)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_track_data)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_holder_is_vip)

      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(311), m_detail_promotion_name)
    End If

    PrintData.FilterValueWidth(1) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""

    ' Date 
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(dtp_from.Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(dtp_to.Value, _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Account
    m_account = Me.uc_account_sel1.Account()
    m_track_data = Me.uc_account_sel1.TrackData()
    m_holder = Me.uc_account_sel1.Holder()
    m_holder_is_vip = Me.uc_account_sel1.HolderIsVip()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal FromDate As Date, _
                         ByVal ToDate As Date, _
                         ByVal Type As Integer, _
                         ByVal PromotionId As Long, _
                         ByVal AccountId As Long, _
                         ByVal PromotionName As String, _
                         ByVal AccountIdName As String)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    m_detail_from_date = FromDate
    m_detail_to_date = ToDate
    m_detail_type = Type
    m_detail_promotion_id = PromotionId
    m_detail_account_id = AccountId
    m_detail_promotion_name = PromotionName
    m_detail_account_id_name = AccountIdName

    Me.MdiParent = MdiParent
    Me.Display(False)

    ' For Show window before filter apply 
    Call Application.DoEvents()
    System.Threading.Thread.Sleep(100)
    Call Application.DoEvents()

    If Me.Visible Then
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Sortable = False

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Datetime
      .Column(GRID_COLUMN_DATETIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(410)
      .Column(GRID_COLUMN_DATETIME).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Account
      If m_detail_type = ROW_SUBTYPE_PROMO_BY_ACCOUNT Then
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Header(0).Text = ""
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Width = 0
        .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Width = GRID_WIDTH_ACCOUNT
        .Column(GRID_COLUMN_ACCOUNT_ID_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      End If

      ' Cashier
      .Column(GRID_COLUMN_CASHIER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(210)
      .Column(GRID_COLUMN_CASHIER_NAME).Width = GRID_WIDTH_CASHIER
      .Column(GRID_COLUMN_CASHIER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Points
      .Column(GRID_COLUMN_POINTS).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(305)
      .Column(GRID_COLUMN_POINTS).Width = GRID_WIDTH_POINTS
      .Column(GRID_COLUMN_POINTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cash-In
      .Column(GRID_COLUMN_CASH_IN).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(42)
      .Column(GRID_COLUMN_CASH_IN).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Non-Redeemable
      .Column(GRID_COLUMN_NON_REDEEMABLE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(50)
      .Column(GRID_COLUMN_NON_REDEEMABLE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_NON_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Redeemable
      .Column(GRID_COLUMN_REDEEMABLE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      .Column(GRID_COLUMN_REDEEMABLE).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_REDEEMABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    If m_detail_to_date = Date.MaxValue Then
      Me.dtp_to.Value = WSI.Common.Misc.TodayOpening()
      Me.dtp_to.Checked = False
    Else
      Me.dtp_to.Value = m_detail_to_date
      Me.dtp_to.Checked = True
    End If

    If m_detail_from_date = Date.MinValue Then
      Me.dtp_from.Value = Me.dtp_to.Value.AddDays(-1)
      Me.dtp_from.Checked = False
    Else
      Me.dtp_from.Value = m_detail_from_date
      Me.dtp_from.Checked = True
    End If

    Me.uc_account_sel1.Clear()

  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String
    Dim str_where As String = ""
    Dim str_where_account As String = ""

    ' Filter Dates
    If Me.dtp_from.Checked Then
      str_where = str_where & " AND AO_DATETIME >= " & GUI_FormatDateDB(dtp_from.Value) & " "
    End If

    If Me.dtp_to.Checked = True Then
      str_where = str_where & " AND AO_DATETIME < " & GUI_FormatDateDB(dtp_to.Value) & " "
    End If

    ' Filter Account
    str_where_account = Me.uc_account_sel1.GetFilterSQL()
    If str_where_account <> "" Then
      str_where = str_where & " AND " & _
                  " AO_ACCOUNT_ID IN (SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE " & str_where_account & ")"
    End If

    Return str_where
  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Events "

#End Region ' Events

End Class ' frm_promotion_report_detail
