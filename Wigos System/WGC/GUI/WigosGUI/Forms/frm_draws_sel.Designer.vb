<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_draws_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_ending_date = New System.Windows.Forms.GroupBox()
    Me.dtp_end_to = New GUI_Controls.uc_date_picker()
    Me.dtp_end_from = New GUI_Controls.uc_date_picker()
    Me.gb_draw_status = New System.Windows.Forms.GroupBox()
    Me.chk_draw_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_draw_closed = New System.Windows.Forms.CheckBox()
    Me.chk_draw_opened = New System.Windows.Forms.CheckBox()
    Me.ef_draw_name = New GUI_Controls.uc_entry_field()
    Me.chk_only_vip = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_ending_date.SuspendLayout()
    Me.gb_draw_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_only_vip)
    Me.panel_filter.Controls.Add(Me.gb_draw_status)
    Me.panel_filter.Controls.Add(Me.gb_ending_date)
    Me.panel_filter.Controls.Add(Me.ef_draw_name)
    Me.panel_filter.Size = New System.Drawing.Size(1219, 105)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_draw_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_ending_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_draw_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_only_vip, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 109)
    Me.panel_data.Size = New System.Drawing.Size(1219, 456)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1213, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1213, 4)
    '
    'gb_ending_date
    '
    Me.gb_ending_date.Controls.Add(Me.dtp_end_to)
    Me.gb_ending_date.Controls.Add(Me.dtp_end_from)
    Me.gb_ending_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_ending_date.Name = "gb_ending_date"
    Me.gb_ending_date.Size = New System.Drawing.Size(257, 72)
    Me.gb_ending_date.TabIndex = 0
    Me.gb_ending_date.TabStop = False
    Me.gb_ending_date.Text = "xEndingDate"
    '
    'dtp_end_to
    '
    Me.dtp_end_to.Checked = True
    Me.dtp_end_to.IsReadOnly = False
    Me.dtp_end_to.Location = New System.Drawing.Point(6, 38)
    Me.dtp_end_to.Name = "dtp_end_to"
    Me.dtp_end_to.ShowCheckBox = True
    Me.dtp_end_to.ShowUpDown = False
    Me.dtp_end_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_end_to.SufixText = "Sufix Text"
    Me.dtp_end_to.SufixTextVisible = True
    Me.dtp_end_to.TabIndex = 1
    Me.dtp_end_to.TextWidth = 50
    Me.dtp_end_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_end_from
    '
    Me.dtp_end_from.Checked = True
    Me.dtp_end_from.IsReadOnly = False
    Me.dtp_end_from.Location = New System.Drawing.Point(6, 14)
    Me.dtp_end_from.Name = "dtp_end_from"
    Me.dtp_end_from.ShowCheckBox = True
    Me.dtp_end_from.ShowUpDown = False
    Me.dtp_end_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_end_from.SufixText = "Sufix Text"
    Me.dtp_end_from.SufixTextVisible = True
    Me.dtp_end_from.TabIndex = 0
    Me.dtp_end_from.TextWidth = 50
    Me.dtp_end_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'gb_draw_status
    '
    Me.gb_draw_status.Controls.Add(Me.chk_draw_disabled)
    Me.gb_draw_status.Controls.Add(Me.chk_draw_closed)
    Me.gb_draw_status.Controls.Add(Me.chk_draw_opened)
    Me.gb_draw_status.Location = New System.Drawing.Point(269, 6)
    Me.gb_draw_status.Name = "gb_draw_status"
    Me.gb_draw_status.Size = New System.Drawing.Size(131, 92)
    Me.gb_draw_status.TabIndex = 1
    Me.gb_draw_status.TabStop = False
    Me.gb_draw_status.Text = "xStatus"
    '
    'chk_draw_disabled
    '
    Me.chk_draw_disabled.AutoSize = True
    Me.chk_draw_disabled.Location = New System.Drawing.Point(6, 65)
    Me.chk_draw_disabled.Name = "chk_draw_disabled"
    Me.chk_draw_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_draw_disabled.TabIndex = 2
    Me.chk_draw_disabled.Text = "xDisabled"
    Me.chk_draw_disabled.UseVisualStyleBackColor = True
    '
    'chk_draw_closed
    '
    Me.chk_draw_closed.AutoSize = True
    Me.chk_draw_closed.Location = New System.Drawing.Point(6, 42)
    Me.chk_draw_closed.Name = "chk_draw_closed"
    Me.chk_draw_closed.Size = New System.Drawing.Size(72, 17)
    Me.chk_draw_closed.TabIndex = 1
    Me.chk_draw_closed.Text = "xClosed"
    Me.chk_draw_closed.UseVisualStyleBackColor = True
    '
    'chk_draw_opened
    '
    Me.chk_draw_opened.AutoSize = True
    Me.chk_draw_opened.Location = New System.Drawing.Point(6, 19)
    Me.chk_draw_opened.Name = "chk_draw_opened"
    Me.chk_draw_opened.Size = New System.Drawing.Size(77, 17)
    Me.chk_draw_opened.TabIndex = 0
    Me.chk_draw_opened.Text = "xOpened"
    Me.chk_draw_opened.UseVisualStyleBackColor = True
    '
    'ef_draw_name
    '
    Me.ef_draw_name.DoubleValue = 0.0R
    Me.ef_draw_name.IntegerValue = 0
    Me.ef_draw_name.IsReadOnly = False
    Me.ef_draw_name.Location = New System.Drawing.Point(383, 19)
    Me.ef_draw_name.Name = "ef_draw_name"
    Me.ef_draw_name.PlaceHolder = Nothing
    Me.ef_draw_name.Size = New System.Drawing.Size(252, 24)
    Me.ef_draw_name.SufixText = "Sufix Text"
    Me.ef_draw_name.SufixTextVisible = True
    Me.ef_draw_name.TabIndex = 2
    Me.ef_draw_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_draw_name.TextValue = ""
    Me.ef_draw_name.Value = ""
    Me.ef_draw_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_only_vip
    '
    Me.chk_only_vip.AutoSize = True
    Me.chk_only_vip.Location = New System.Drawing.Point(465, 51)
    Me.chk_only_vip.Name = "chk_only_vip"
    Me.chk_only_vip.Size = New System.Drawing.Size(83, 17)
    Me.chk_only_vip.TabIndex = 3
    Me.chk_only_vip.Text = "xOnlyVips"
    Me.chk_only_vip.UseVisualStyleBackColor = True
    '
    'frm_draws_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1227, 569)
    Me.Name = "frm_draws_sel"
    Me.Text = "frm_draws_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_ending_date.ResumeLayout(False)
    Me.gb_draw_status.ResumeLayout(False)
    Me.gb_draw_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_ending_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_end_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_end_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_draw_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_draw_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_draw_closed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_draw_opened As System.Windows.Forms.CheckBox
  Friend WithEvents ef_draw_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_only_vip As System.Windows.Forms.CheckBox
End Class
