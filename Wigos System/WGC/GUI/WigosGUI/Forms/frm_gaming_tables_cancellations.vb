'-------------------------------------------------------------------
' Copyright � 2007-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_cancellations
' DESCRIPTION:   This screen allows to view cancellations of gaming tables sessions:
' AUTHOR:        Sergi Mart�nez
' CREATION DATE: 30-JAN-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-JAN-2014  SMN    Initial version.
' 06-JUN-2014  JAB    Added two decimal characters in denominations fields.
' 01-DEC-2014  JMV    Added: Total rows, "Show Details" check, "Operation date" column, from date check
'                     Changed some names and other esthetic changes
' 03-DEC-2014  JMV    WIG-1801
' 13-JUL-2016  RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt report to MultiCurrency
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

#End Region


Public Class frm_gaming_tables_cancellations
  Inherits frm_base_sel

#Region " Members "
  ' Filter members
  Private m_report_date_from As String
  Private m_report_date_to As String
  Private m_report_date_type As String
  Private m_num_purchases As Integer
  Private m_num_sales As Integer
  Private m_operation_id As Integer
  Private m_added_first_row As Boolean
  Private m_total_quantity_per_operation As Integer

  'Totals 
  Private m_total_purchase_amount As Double
  Private m_total_sale_amount As Double
  Private m_total_quantity_purchase_amount As Integer
  Private m_total_quantity_sale_amount As Integer

  'Selected currency
  Private m_selected_currency As String

#End Region

#Region "Enums"

  Private Enum ENUM_UPDATE_STATUS
    STATUS_SALES = 0
    STATUS_PURCHASES = 1
  End Enum

#End Region ' Enums

  ' Grid counters
  Private Const COUNTER_PURCHASES As Integer = 1
  Private Const COUNTER_SALES As Integer = 2

  ' Grid Setup
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMNS As Integer = 10

  ' Grid Colums
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CM_DATE_CANCELATION As Integer = 1
  Private Const GRID_COLUMN_CM_TYPE As Integer = 2
  Private Const GRID_COLUMN_TERMINAL As Integer = 3
  Private Const GRID_COLUMN_USER As Integer = 4
  Private Const GRID_COLUMN_CM_DATE_OPERATION As Integer = 5
  Private Const GRID_COLUMN_QUANTITY As Integer = 6
  Private Const GRID_COLUMN_CM_CURRENCY_DENOMINATION As Integer = 7
  Private Const GRID_COLUMN_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_CM_MOVEMENT_ID As Integer = 9

  ' Grid Columns Width
  Private Const GRID_COLUMN_TERMINAL_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_CM_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_AMOUNT_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_QUANTITY_WIDTH As Integer = 1200
  Private Const GRID_COLUMN_CM_CURRENCY_DENOMINATION_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_CM_TYPE_WIDTH As Integer = 1200
  Private Const GRID_COLUMN_OPENING_DATE_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_USER_WIDTH As Integer = 2000

  ' SQL Columns
  Private Const SQL_COLUMN_TERMINAL As Integer = 0
  Private Const SQL_COLUMN_CM_DATE_CANCELATION As Integer = 1
  Private Const SQL_COLUMN_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_QUANTITY As Integer = 3
  Private Const SQL_COLUMN_CM_CURRENCY_DENOMINATION As Integer = 4
  Private Const SQL_COLUMN_CM_MOVEMENT_ID As Integer = 5
  Private Const SQL_COLUMN_CM_TYPE As Integer = 6
  Private Const SQL_COLUMN_USER As Integer = 7
  Private Const SQL_COLUMN_CM_OPERATION_ID As Integer = 8
  Private Const SQL_COLUMN_CM_DATE_OPERATION As Integer = 9

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_GAMING_TABLES_CANCELLATIONS

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    ' TODO
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4585)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Set filter default values
    Call SetDefaultValues()

    ' Date
    Me.gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5768)
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.Checked = False
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Terminals 
    Me.gb_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(209)
    Me.opt_one_cashier.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_cashiers.Text = GLB_NLS_GUI_INVOICING.GetString(219)

    ' Users 
    Me.gb_user.Text = GLB_NLS_GUI_INVOICING.GetString(220)
    Me.opt_one_user.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all_users.Text = GLB_NLS_GUI_INVOICING.GetString(219)
    Me.chk_show_all.Text = GLB_NLS_GUI_CONFIGURATION.GetString(90)

    ' Type
    Me.gb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3458)
    Me.chk_type_purchase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2343)
    Me.chk_type_sale.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2344)

    Me.chk_show_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1361)

    ' Grid
    Call GUI_StyleSheet()

    ' Set combo with Cashier Terminals

    Call SetCombo(Me.cmb_cashier, " SELECT " & _
                                  "    CT.CT_CASHIER_ID," & _
                                  "    ISNULL(GT.GT_NAME,CT.CT_NAME) AS TERMINAL " & _
                                  " FROM CASHIER_TERMINALS CT LEFT JOIN GAMING_TABLES GT ON CT_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID " & _
                                  " ORDER BY ISNULL(GT.GT_NAME,CT.CT_NAME) ")

    ' Set combo with Users
    Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & _
                    WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    ' TODO
    ' Me.ActiveControl = Me.opt_opening_session

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  '
Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                           ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _cm_type As Integer
    Dim _fore_color As Color
    Dim _back_color As Color
    Dim _show_details As Boolean

    _show_details = chk_show_details.Checked

    With Me.Grid

      If Not m_added_first_row Then
        m_operation_id = DbRow.Value(SQL_COLUMN_CM_OPERATION_ID)
        m_added_first_row = True
      End If

      If m_operation_id = DbRow.Value(SQL_COLUMN_CM_OPERATION_ID) Then
        m_total_quantity_per_operation += DbRow.Value(SQL_COLUMN_QUANTITY)
      Else
        m_operation_id = DbRow.Value(SQL_COLUMN_CM_OPERATION_ID)
        m_total_quantity_per_operation = DbRow.Value(SQL_COLUMN_QUANTITY)
      End If

      ' Table / Table type name
      If Not DbRow.IsNull(SQL_COLUMN_CM_DATE_CANCELATION) Then
        .Cell(RowIndex, GRID_COLUMN_CM_DATE_CANCELATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CM_DATE_CANCELATION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
      If Not DbRow.IsNull(SQL_COLUMN_CM_DATE_OPERATION) Then
        .Cell(RowIndex, GRID_COLUMN_CM_DATE_OPERATION).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CM_DATE_OPERATION), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If
      .Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
      .Cell(RowIndex, GRID_COLUMN_CM_MOVEMENT_ID).Value = DbRow.Value(SQL_COLUMN_CM_MOVEMENT_ID)

      If Not DbRow.IsNull(SQL_COLUMN_TERMINAL) Then
        .Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL)
      End If

      _cm_type = DbRow.Value(SQL_COLUMN_CM_TYPE)
      Select Case (_cm_type)
        Case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL, CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE
          GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_PURCHASES, _fore_color, _back_color)
          Me.Grid.Row(RowIndex).BackColor = _back_color
          .Cell(RowIndex, GRID_COLUMN_CM_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2343)
          If (_show_details And m_total_quantity_per_operation <> 0) Then
            .Cell(RowIndex, GRID_COLUMN_QUANTITY).Value = GUI_FormatNumber(m_total_quantity_per_operation, 0)
          End If
          m_num_purchases += 1
          If Not DbRow.IsNull(SQL_COLUMN_USER) Then
            .Cell(RowIndex, GRID_COLUMN_USER).Value = DbRow.Value(SQL_COLUMN_USER)
          End If
          ' Total calculation
          m_total_purchase_amount += DbRow.Value(SQL_COLUMN_AMOUNT)

        Case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL, CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE
          GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_SALES, _fore_color, _back_color)
          Me.Grid.Row(RowIndex).BackColor = _back_color
          .Cell(RowIndex, GRID_COLUMN_CM_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2344)
          If (_show_details And m_total_quantity_per_operation <> 0) Then
            .Cell(RowIndex, GRID_COLUMN_QUANTITY).Value = GUI_FormatNumber(m_total_quantity_per_operation, 0)
          End If
          m_num_sales += 1
          If Not DbRow.IsNull(SQL_COLUMN_USER) Then
            .Cell(RowIndex, GRID_COLUMN_USER).Value = DbRow.Value(SQL_COLUMN_USER)
          End If
          ' Total calculation
          m_total_sale_amount += DbRow.Value(SQL_COLUMN_AMOUNT)
          m_total_quantity_sale_amount += DbRow.Value(SQL_COLUMN_QUANTITY)

        Case Else ' DETAILS

          If (_cm_type = CASHIER_MOVEMENT.CHIPS_PURCHASE) Then
            m_total_quantity_purchase_amount += DbRow.Value(SQL_COLUMN_QUANTITY)
          End If
          If (_cm_type = CASHIER_MOVEMENT.CHIPS_SALE) Then
            m_total_quantity_sale_amount += DbRow.Value(SQL_COLUMN_QUANTITY)
          End If
          If Not DbRow.IsNull(SQL_COLUMN_CM_CURRENCY_DENOMINATION) Then
            .Cell(RowIndex, GRID_COLUMN_CM_CURRENCY_DENOMINATION).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_CM_CURRENCY_DENOMINATION), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          End If
          If (_show_details) Then
            .Cell(RowIndex, GRID_COLUMN_QUANTITY).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_QUANTITY), 0)
          Else
            ' If not showing details, delete row
            Return False
          End If

      End Select



    End With

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Me.Grid.Counter(COUNTER_PURCHASES).Value = m_num_purchases
    Me.Grid.Counter(COUNTER_SALES).Value = m_num_sales

    ' Add total rows
    Dim idx_row As Integer

    If (chk_type_purchase.Checked Or (Not (chk_type_purchase.Checked) And Not (chk_type_sale.Checked))) Then
      Me.Grid.AddRow()
      idx_row = Me.Grid.NumRows - 1
      Me.Grid.Cell(idx_row, GRID_COLUMN_INDEX).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2343)  '"Compra: "
      Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_USER).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_DATE_CANCELATION).Value = GLB_NLS_GUI_INVOICING.GetString(205)  '"TOTAL: "
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_DATE_OPERATION).Value = ""
      If (m_total_quantity_purchase_amount <> 0 And chk_show_details.Checked) Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_QUANTITY).Value = GUI_FormatNumber(m_total_quantity_purchase_amount, 0)
      Else
        Me.Grid.Cell(idx_row, GRID_COLUMN_QUANTITY).Value = ""
      End If
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_CURRENCY_DENOMINATION).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(m_total_purchase_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_MOVEMENT_ID).Value = ""

      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

    If (chk_type_sale.Checked Or (Not (chk_type_purchase.Checked) And Not (chk_type_sale.Checked))) Then
      Me.Grid.AddRow()
      idx_row = Me.Grid.NumRows - 1

      Me.Grid.Cell(idx_row, GRID_COLUMN_INDEX).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2344)  '"Venta: "
      Me.Grid.Cell(idx_row, GRID_COLUMN_TERMINAL).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_USER).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_DATE_CANCELATION).Value = GLB_NLS_GUI_INVOICING.GetString(205)  '"TOTAL: "
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_DATE_OPERATION).Value = ""
      If (m_total_quantity_sale_amount <> 0 And chk_show_details.Checked) Then
        Me.Grid.Cell(idx_row, GRID_COLUMN_QUANTITY).Value = GUI_FormatNumber(m_total_quantity_sale_amount, 0)
      Else
        Me.Grid.Cell(idx_row, GRID_COLUMN_QUANTITY).Value = ""
      End If
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_CURRENCY_DENOMINATION).Value = ""
      Me.Grid.Cell(idx_row, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(m_total_sale_amount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, False)
      Me.Grid.Cell(idx_row, GRID_COLUMN_CM_MOVEMENT_ID).Value = ""

      Me.Grid.Row(idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If



  End Sub 'GUI_AfterLastRow


  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()
    m_num_purchases = 0
    m_num_sales = 0
    m_operation_id = -1
    m_added_first_row = False
    m_total_quantity_per_operation = 0

    m_total_purchase_amount = 0
    m_total_sale_amount = 0
    m_total_quantity_purchase_amount = 0
    m_total_quantity_sale_amount = 0

  End Sub 'GUI_BeforeFirstRow


  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_report_date_from = String.Empty
    m_report_date_to = String.Empty
    m_report_date_type = String.Empty

    m_report_date_from = Me.dtp_from.Value
    If (dtp_to.Checked) Then
      m_report_date_to = Me.dtp_to.Value
    End If

    If Not Me.chk_type_purchase.Checked And Me.chk_type_sale.Checked Then
      m_report_date_type = chk_type_sale.Text
    ElseIf Me.chk_type_purchase.Checked And Not Me.chk_type_sale.Checked Then
      m_report_date_type = chk_type_sale.Text
    Else
      m_report_date_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353) ' All
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _sql_cmd As SqlCommand
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _type As Integer
    Dim _date_to As Date

    m_selected_currency = uc_multicurrency.SelectedCurrency

    If Me.chk_type_sale.Checked And Not Me.chk_type_purchase.Checked Then
      _type = 0 ' show only Sales
    ElseIf Not Me.chk_type_sale.Checked And Me.chk_type_purchase.Checked Then
      _type = 1 ' show only Purchases
    Else
      _type = -1 ' show all
    End If

    Call GUI_StyleSheet()

    _date_to = Date.MaxValue
    If (dtp_to.Checked) Then
      _date_to = Me.dtp_to.Value
    End If

    _sql_cmd = New SqlCommand("GT_Cancellations")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = IIf(cmb_cashier.Enabled, cmb_cashier.Value, DBNull.Value)
    _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Me.dtp_from.Value
    _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = _date_to
    _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _type
    _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = IIf(cmb_user.Enabled, cmb_user.Value, DBNull.Value)
    _sql_cmd.Parameters.Add("@pSelectedCurrency", SqlDbType.NVarChar).Value = m_selected_currency

    _table = GUI_GetTableUsingCommand(_sql_cmd, Integer.MaxValue)

    Call GUI_BeforeFirstRow()

    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer

    For Each _row In _table.Rows

      Try
        db_row = New CLASS_DB_ROW(_row)

        If GUI_CheckOutRowBeforeAdd(db_row) Then
          ' Add the db row to the grid
          Me.Grid.AddRow()
          count = count + 1
          idx_row = Me.Grid.NumRows - 1

          If Not GUI_SetupRow(idx_row, db_row) Then
            ' The row can not be added
            Me.Grid.DeleteRowFast(idx_row)
            count = count - 1
          End If
        End If


      Catch ex As OutOfMemoryException
        Throw ex

      Catch exception As Exception
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        Call Trace.WriteLine(exception.ToString())
        Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                              "frm_gaming_tables_income_report", _
                              "GUI_SetupRow", _
                              exception.Message)
        Exit For
      End Try

      db_row = Nothing

      If Not GUI_DoEvents(Me.Grid) Then
        Exit Sub
      End If

    Next

    Call GUI_AfterLastRow()
  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    Dim _fore_color As Color
    Dim _back_color As Color

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .IsSortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_PURCHASES, _fore_color, _back_color)
      .Counter(COUNTER_PURCHASES).Visible = False
      .Counter(COUNTER_PURCHASES).BackColor = _back_color
      .Counter(COUNTER_PURCHASES).ForeColor = _fore_color

      GetUpdateStatusColor(ENUM_UPDATE_STATUS.STATUS_SALES, _fore_color, _back_color)
      .Counter(COUNTER_SALES).Visible = False
      .Counter(COUNTER_SALES).BackColor = _back_color
      .Counter(COUNTER_SALES).ForeColor = _fore_color

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' CM_MOVEMENT_ID
      .Column(GRID_COLUMN_CM_MOVEMENT_ID).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_CM_MOVEMENT_ID).Width = 0
      .Column(GRID_COLUMN_CM_MOVEMENT_ID).HighLightWhenSelected = False
      .Column(GRID_COLUMN_CM_MOVEMENT_ID).IsColumnPrintable = False

      ' CM_TYPE
      .Column(GRID_COLUMN_CM_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3458)
      .Column(GRID_COLUMN_CM_TYPE).Width = GRID_COLUMN_CM_TYPE_WIDTH
      .Column(GRID_COLUMN_CM_TYPE).IsColumnPrintable = True

      ' Terminal
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)
      .Column(GRID_COLUMN_TERMINAL).Width = GRID_COLUMN_TERMINAL_WIDTH
      .Column(GRID_COLUMN_TERMINAL).IsColumnPrintable = True

      ' Cancelation Date
      .Column(GRID_COLUMN_CM_DATE_CANCELATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5768)
      .Column(GRID_COLUMN_CM_DATE_CANCELATION).Width = GRID_COLUMN_CM_DATE_WIDTH
      .Column(GRID_COLUMN_CM_DATE_CANCELATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Operation Date
      .Column(GRID_COLUMN_CM_DATE_OPERATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5769)
      .Column(GRID_COLUMN_CM_DATE_OPERATION).Width = GRID_COLUMN_CM_DATE_WIDTH
      .Column(GRID_COLUMN_CM_DATE_OPERATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Amount
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_COLUMN_AMOUNT_WIDTH
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Quantity
      .Column(GRID_COLUMN_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2042)
      .Column(GRID_COLUMN_QUANTITY).Width = GRID_COLUMN_QUANTITY_WIDTH
      .Column(GRID_COLUMN_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Currency Denomination
      .Column(GRID_COLUMN_CM_CURRENCY_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4357)
      .Column(GRID_COLUMN_CM_CURRENCY_DENOMINATION).Width = GRID_COLUMN_CM_CURRENCY_DENOMINATION_WIDTH
      .Column(GRID_COLUMN_CM_CURRENCY_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' User
      .Column(GRID_COLUMN_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2253)
      .Column(GRID_COLUMN_USER).Width = GRID_COLUMN_USER_WIDTH
      .Column(GRID_COLUMN_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set the query method to use
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Date From Filter
    If Not String.IsNullOrEmpty(m_report_date_from) Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(202), m_report_date_from)
    End If

    ' Date To Filter
    If Not String.IsNullOrEmpty(m_report_date_to) Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(203), m_report_date_to)
    End If

    ' Date Type Filter
    If Not String.IsNullOrEmpty(m_report_date_type) Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(211), m_report_date_type)
    End If

    ' Currency type
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_selected_currency)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - TRUE: filter values are accepted
  '   - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Date selection 
    If (dtp_to.Checked) Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If


    Return True
  End Function ' GUI_FilterCheck

#End Region

#Region " Private Functions "
  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub SetDefaultValues()
    Dim _closing_time As Integer
    Dim _now As Date

    _now = WGDB.Now
    _closing_time = GetDefaultClosingTime()
    dtp_from.Value = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    dtp_to.Checked = False
    dtp_to.Value = dtp_from.Value.AddDays(1)

    opt_all_cashiers.Checked = True
    cmb_cashier.Enabled = False
    chk_type_purchase.Checked = True
    chk_show_details.Checked = True
    chk_type_sale.Checked = True
    m_num_purchases = 0
    m_num_sales = 0

    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False

    opt_all_users.Checked = True

    'uc_multi_currency_site_sel
    uc_multicurrency.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382)
    uc_multicurrency.OpenModeControl = uc_multi_currency_site_sel.OPEN_MODE.OnlyCurrency
    Me.uc_multicurrency.Init()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get color by SERVICE status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status:  ENUM_SERVICE_STATUS
  '     - OUTPUT:
  '           - ForeColor: Foreground color
  '           - BackColor: Background color
  '
  ' RETURNS:
  '
  Private Sub GetUpdateStatusColor(ByVal Status As ENUM_UPDATE_STATUS, _
                                    ByRef ForeColor As Color, _
                                    ByRef BackColor As Color)
    Select Case Status

      Case ENUM_UPDATE_STATUS.STATUS_SALES
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      Case ENUM_UPDATE_STATUS.STATUS_PURCHASES
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      Case Else
        ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_00)

    End Select
  End Sub 'GetUpdateStatusColor


#End Region

  Private Sub opt_all_cashiers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_cashiers.Click
    Me.cmb_cashier.Enabled = False
  End Sub

  Private Sub opt_several_cashiers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_cashier.Click
    Me.cmb_cashier.Enabled = True
  End Sub

  Private Sub opt_one_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_one_user.Click
    Me.cmb_user.Enabled = True
    Me.chk_show_all.Enabled = True
  End Sub

  Private Sub opt_all_users_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_users.Click
    Me.cmb_user.Enabled = False
    Me.chk_show_all.Enabled = False
  End Sub

  Private Sub chk_show_all_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_show_all.CheckedChanged
    If chk_show_all.Checked Then
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Else
      Call SetCombo(Me.cmb_user, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_BLOCK_REASON = " & WSI.Common.GUI_USER_BLOCK_REASON.NONE & " ORDER BY GU_USERNAME")
    End If
  End Sub
End Class