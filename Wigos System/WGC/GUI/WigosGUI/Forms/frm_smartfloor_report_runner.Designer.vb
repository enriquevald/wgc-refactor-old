﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_smartfloor_report_runner
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.rb_create = New System.Windows.Forms.RadioButton()
    Me.rb_solve = New System.Windows.Forms.RadioButton()
    Me.ds_selector = New GUI_Controls.uc_daily_session_selector()
    Me.chk_Show_Detail = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_Show_Detail)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Size = New System.Drawing.Size(1247, 160)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_Show_Detail, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 164)
    Me.panel_data.Size = New System.Drawing.Size(1247, 488)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1241, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1241, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.rb_create)
    Me.gb_date.Controls.Add(Me.rb_solve)
    Me.gb_date.Controls.Add(Me.ds_selector)
    Me.gb_date.Location = New System.Drawing.Point(6, 6)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(274, 129)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'rb_create
    '
    Me.rb_create.AutoSize = True
    Me.rb_create.Checked = True
    Me.rb_create.Location = New System.Drawing.Point(10, 20)
    Me.rb_create.Name = "rb_create"
    Me.rb_create.Size = New System.Drawing.Size(121, 17)
    Me.rb_create.TabIndex = 1
    Me.rb_create.TabStop = True
    Me.rb_create.Text = "xSearchCreation"
    Me.rb_create.UseVisualStyleBackColor = True
    '
    'rb_solve
    '
    Me.rb_solve.AutoSize = True
    Me.rb_solve.Location = New System.Drawing.Point(10, 43)
    Me.rb_solve.Name = "rb_solve"
    Me.rb_solve.Size = New System.Drawing.Size(131, 17)
    Me.rb_solve.TabIndex = 2
    Me.rb_solve.Text = "xSearchResolution"
    Me.rb_solve.UseVisualStyleBackColor = True
    '
    'ds_selector
    '
    Me.ds_selector.ClosingTime = 0
    Me.ds_selector.ClosingTimeEnabled = False
    Me.ds_selector.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_selector.FromDateSelected = False
    Me.ds_selector.Location = New System.Drawing.Point(7, 46)
    Me.ds_selector.Name = "ds_selector"
    Me.ds_selector.ShowBorder = False
    Me.ds_selector.Size = New System.Drawing.Size(256, 79)
    Me.ds_selector.TabIndex = 2
    Me.ds_selector.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_selector.ToDateSelected = True
    '
    'chk_Show_Detail
    '
    Me.chk_Show_Detail.AutoSize = True
    Me.chk_Show_Detail.Location = New System.Drawing.Point(14, 138)
    Me.chk_Show_Detail.Name = "chk_Show_Detail"
    Me.chk_Show_Detail.Size = New System.Drawing.Size(131, 17)
    Me.chk_Show_Detail.TabIndex = 2
    Me.chk_Show_Detail.Text = "xShowInformation"
    Me.chk_Show_Detail.UseVisualStyleBackColor = True
    '
    'frm_smartfloor_report_runner
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1255, 656)
    Me.Name = "frm_smartfloor_report_runner"
    Me.Text = "fm_smartfloor_report_runner"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_date.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents rb_create As System.Windows.Forms.RadioButton
  Friend WithEvents rb_solve As System.Windows.Forms.RadioButton
  Friend WithEvents chk_Show_Detail As System.Windows.Forms.CheckBox
  Friend WithEvents ds_selector As GUI_Controls.uc_daily_session_selector
End Class
