<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminals_edit_status
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.cmb_draw_type = New GUI_Controls.uc_combo
    Me.panel_buttons = New System.Windows.Forms.Panel
    Me.btn_ok = New GUI_Controls.uc_button
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.lbl_terminal_info = New System.Windows.Forms.Label
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'cmb_draw_type
    '
    Me.cmb_draw_type.IsReadOnly = False
    Me.cmb_draw_type.Location = New System.Drawing.Point(14, 49)
    Me.cmb_draw_type.Name = "cmb_draw_type"
    Me.cmb_draw_type.SelectedIndex = -1
    Me.cmb_draw_type.Size = New System.Drawing.Size(383, 25)
    Me.cmb_draw_type.SufixText = "Sufix Text"
    Me.cmb_draw_type.SufixTextVisible = True
    Me.cmb_draw_type.TabIndex = 14
    Me.cmb_draw_type.TextWidth = 204
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_ok)
    Me.panel_buttons.Controls.Add(Me.btn_cancel)
    Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_buttons.Location = New System.Drawing.Point(403, 4)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(90, 72)
    Me.panel_buttons.TabIndex = 17
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_ok.Location = New System.Drawing.Point(0, 12)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 3
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 42)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 4
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_terminal_info
    '
    Me.lbl_terminal_info.AutoSize = True
    Me.lbl_terminal_info.Location = New System.Drawing.Point(10, 20)
    Me.lbl_terminal_info.Name = "lbl_terminal_info"
    Me.lbl_terminal_info.Size = New System.Drawing.Size(300, 13)
    Me.lbl_terminal_info.TabIndex = 18
    Me.lbl_terminal_info.Text = "Cambiar estado del terminal MAC-1234-5678-EFAB"
    '
    'frm_terminals_edit_status
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(498, 80)
    Me.Controls.Add(Me.lbl_terminal_info)
    Me.Controls.Add(Me.panel_buttons)
    Me.Controls.Add(Me.cmb_draw_type)
    Me.Name = "frm_terminals_edit_status"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_terminals_edit_status"
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents cmb_draw_type As GUI_Controls.uc_combo
  Private WithEvents panel_buttons As System.Windows.Forms.Panel
  Private WithEvents btn_ok As GUI_Controls.uc_button
  Private WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents lbl_terminal_info As System.Windows.Forms.Label
End Class
