﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_meters_comparison
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dtp_working_day = New GUI_Controls.uc_date_picker()
    Me.uc_terminals = New GUI_Controls.uc_provider()
    Me.list_meters_group = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.list_meters_group)
    Me.panel_filter.Controls.Add(Me.uc_terminals)
    Me.panel_filter.Controls.Add(Me.dtp_working_day)
    Me.panel_filter.Size = New System.Drawing.Size(1088, 202)
    Me.panel_filter.Controls.SetChildIndex(Me.dtp_working_day, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_terminals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.list_meters_group, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 206)
    Me.panel_data.Size = New System.Drawing.Size(1088, 351)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1082, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1082, 4)
    '
    'dtp_working_day
    '
    Me.dtp_working_day.Checked = True
    Me.dtp_working_day.IsReadOnly = False
    Me.dtp_working_day.Location = New System.Drawing.Point(3, 13)
    Me.dtp_working_day.Name = "dtp_working_day"
    Me.dtp_working_day.ShowCheckBox = False
    Me.dtp_working_day.ShowUpDown = False
    Me.dtp_working_day.Size = New System.Drawing.Size(250, 24)
    Me.dtp_working_day.SufixText = "Sufix Text"
    Me.dtp_working_day.SufixTextVisible = True
    Me.dtp_working_day.TabIndex = 0
    Me.dtp_working_day.TextWidth = 125
    Me.dtp_working_day.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'uc_terminals
    '
    Me.uc_terminals.Location = New System.Drawing.Point(285, 2)
    Me.uc_terminals.Name = "uc_terminals"
    Me.uc_terminals.Size = New System.Drawing.Size(325, 190)
    Me.uc_terminals.TabIndex = 12
    '
    'list_meters_group
    '
    Me.list_meters_group.GroupBoxText = "xCheckedList"
    Me.list_meters_group.Location = New System.Drawing.Point(645, 2)
    Me.list_meters_group.m_resize_width = 301
    Me.list_meters_group.multiChoice = True
    Me.list_meters_group.Name = "list_meters_group"
    Me.list_meters_group.SelectedIndexes = New Integer(-1) {}
    Me.list_meters_group.SelectedIndexesList = ""
    Me.list_meters_group.SelectedIndexesListLevel2 = ""
    Me.list_meters_group.SelectedValuesArray = New String(-1) {}
    Me.list_meters_group.SelectedValuesList = ""
    Me.list_meters_group.SetLevels = 2
    Me.list_meters_group.Size = New System.Drawing.Size(301, 173)
    Me.list_meters_group.TabIndex = 13
    Me.list_meters_group.ValuesArray = New String(-1) {}
    '
    'frm_terminal_meters_comparison
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1096, 561)
    Me.Name = "frm_terminal_meters_comparison"
    Me.Text = "Form1"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dtp_working_day As GUI_Controls.uc_date_picker
  Friend WithEvents uc_terminals As GUI_Controls.uc_provider
  Friend WithEvents list_meters_group As GUI_Controls.uc_checked_list
End Class
