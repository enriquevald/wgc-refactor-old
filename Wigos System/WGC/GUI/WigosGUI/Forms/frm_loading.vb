﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_loading.vb
' DESCRIPTION:   Form wait process generic
' AUTHOR:        David Perelló
' CREATION DATE: 03-OCT-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-OCT-2017  DPC    Initial version
' -------------------------------------------------------------------

Public Class frm_loading

  Public WriteOnly Property Message()
    Set(ByVal value)
      lb_message.Text = value
    End Set
  End Property

  Public WriteOnly Property Image()
    Set(ByVal value)
      pb_gif.Image = value
    End Set
  End Property

End Class