<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_stock
  Inherits GUI_Controls.frm_base_print

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.tb_options = New System.Windows.Forms.TabControl()
    Me.tb_cash = New System.Windows.Forms.TabPage()
    Me.tb_chips = New System.Windows.Forms.TabPage()
    Me.panel_grids.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.tb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.tb_options)
    Me.panel_grids.Location = New System.Drawing.Point(5, 37)
    Me.panel_grids.Size = New System.Drawing.Size(844, 469)
    Me.panel_grids.Controls.SetChildIndex(Me.tb_options, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(756, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 469)
    '
    'panel_filter
    '
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(844, 10)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 14)
    Me.pn_separator_line.Size = New System.Drawing.Size(844, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(844, 4)
    '
    'tb_options
    '
    Me.tb_options.Controls.Add(Me.tb_cash)
    Me.tb_options.Controls.Add(Me.tb_chips)
    Me.tb_options.Location = New System.Drawing.Point(0, 3)
    Me.tb_options.Name = "tb_options"
    Me.tb_options.SelectedIndex = 0
    Me.tb_options.Size = New System.Drawing.Size(875, 462)
    Me.tb_options.TabIndex = 2
    '
    'tb_cash
    '
    Me.tb_cash.Location = New System.Drawing.Point(4, 22)
    Me.tb_cash.Name = "tb_cash"
    Me.tb_cash.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_cash.Size = New System.Drawing.Size(867, 436)
    Me.tb_cash.TabIndex = 0
    Me.tb_cash.Text = "tb_cash"
    Me.tb_cash.UseVisualStyleBackColor = True
    '
    'tb_chips
    '
    Me.tb_chips.Location = New System.Drawing.Point(4, 22)
    Me.tb_chips.Name = "tb_chips"
    Me.tb_chips.Padding = New System.Windows.Forms.Padding(3)
    Me.tb_chips.Size = New System.Drawing.Size(867, 436)
    Me.tb_chips.TabIndex = 1
    Me.tb_chips.Text = "tb_chips"
    Me.tb_chips.UseVisualStyleBackColor = True
    '
    'frm_cage_stock
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
    Me.AutoScroll = True
    Me.ClientSize = New System.Drawing.Size(854, 510)
    Me.Name = "frm_cage_stock"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_session_detail"
    Me.panel_grids.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.tb_options.ResumeLayout(False)
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents tb_options As System.Windows.Forms.TabControl
  Friend WithEvents tb_cash As System.Windows.Forms.TabPage
  Friend WithEvents tb_chips As System.Windows.Forms.TabPage
End Class
