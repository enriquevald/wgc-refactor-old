'-----------------------------------------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-----------------------------------------------------------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_tables_session_edit.vb
' DESCRIPTION:   Edit gaming tables session
' AUTHOR:        David Hern�ndez
' CREATION DATE: 12-JAN-2015
'
' REVISION HISTORY:
'
' Date         Author  Description
'------------- ------- -------------------------------------------------------------------------------------------------
' 12-JAN-2015  DHA     Initial version
' 11-JUL-2016  JML     Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
' 13-JUL-2016  SMN     Fixed Bug 14837:Mesa de juego (Cashless,TITO): No se pueden sacar a los jugadores cuando est�n jugando
' 29-JUL-2016  RAB     Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to multicurrency
' 26-OCT-2017  RAB     Bug 30435:WIGOS-6138 Gaming tables point assignment: player session manual entry allows point assignment to anonymous user
' 03-NOV-2017  RAB     Bug 30537:WIGOS-6337 Gaming tables point assignment: Computed and assigned points are calculated and saved to anonymous user in gaming table session editor
' 06-NOV-2017  RAB     Bug 30537:WIGOS-6337 Gaming tables point assignment: Computed and assigned points are calculated and saved to anonymous user in gaming table session editor
'-----------------------------------------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common

Public Class frm_gaming_tables_session_edit
  Inherits frm_base_edit

#Region " Constants "

  Public Const EMPTY_STRING As String = "---"
  Public Const SPEED_SLOW As Integer = 0
  Public Const SPEED_NORMAL As Integer = 1
  Public Const SPEED_FAST As Integer = 2

#End Region ' Constants

#Region "Members"

  Public Event OneAccountFilterChanged()

  Private m_gt_session As CLASS_GAMING_TABLE_SESSION
  Private m_gt_types_names As DataTable
  'Private m_table_speed As Integer
  Private m_table_session_start As DateTime
  Private m_table_session_end As DateTime
  Private m_account_id As Int64
  Private m_account_name As String
  Private m_virtual_account As Int64
  Private m_virtual_terminal As Int64
  Private m_virtual_terminal_name As String
  Private m_elp_mode As Int32
  Private m_current_theoric_hold As Decimal
  Private m_anonymous_user As Boolean

#End Region ' Members

#Region "Properties"

#End Region ' Properties

#Region "Privates"

  ' PURPOSE: Load controls
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub LoadControls()

    Dim _gt_types As DataView

    ' Speed    
    cmb_speed.Add(GTPlayerTrackingSpeed.Slow, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5021))
    cmb_speed.Add(GTPlayerTrackingSpeed.Medium, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5022))
    cmb_speed.Add(GTPlayerTrackingSpeed.Fast, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5023))

    cmb_speed.Value = GTPlayerTrackingSpeed.Medium

    ' Skill    
    cmb_skill.Add(GTPlaySessionPlayerSkill.Soft, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5025))
    cmb_skill.Add(GTPlaySessionPlayerSkill.Average, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5026))
    cmb_skill.Add(GTPlaySessionPlayerSkill.Hard, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5027))

    cmb_skill.Value = GTPlaySessionPlayerSkill.Average

    ' Read Gaming Table Info
    If m_gt_session.ReadGamingTablesInfo(m_gt_types_names) Then

      _gt_types = m_gt_types_names.DefaultView

      For Each _row As DataRow In _gt_types.ToTable(True, "GTT_GAMING_TABLE_TYPE_ID", "GTT_NAME").Rows
        cmb_type.Add(_row(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_TYPE_ID), _row(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_TYPE_NAME))
      Next

    End If

    FillCurrenciesCombo()
    m_anonymous_user = False

  End Sub

  ' PURPOSE : Fill Currencies Combo
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillCurrenciesCombo()
    Dim _table As DataTable
    Dim _currency_accepted As String
    Dim _currency_array As String()
    Dim _idx_combo As Integer

    _idx_combo = 0
    _table = Nothing
    _currency_accepted = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted")
    _currency_array = _currency_accepted.Split(";")

    Me.cmb_isoCode.Clear()
    For Each _currency As String In _currency_array
      If (_currency <> Cage.CHIPS_ISO_CODE) Then
        _idx_combo = _idx_combo + 1
        Me.cmb_isoCode.Add(_idx_combo, _currency)
      End If
    Next

  End Sub 'FillCurrenciesCombo

  ' PURPOSE : Check if not empty ef_account_id and ef_card_track values and enable/disable other controls 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub account_or_trackdata_EntryFieldValueChanged() Handles ef_account_id.EntryFieldValueChanged, ef_card_track.EntryFieldValueChanged

    lbl_holder_name.Text = EMPTY_STRING
    m_account_id = 0
    m_account_name = ""

    If ef_account_id.TextValue <> "" Or ef_card_track.TextValue <> "" Then
      Me.ef_account_id.Enabled = (Me.ef_account_id.TextValue <> "")
      Me.ef_card_track.Enabled = (Me.ef_card_track.TextValue <> "")
      Me.btn_search_account.Enabled = True

    Else
      Me.ef_account_id.Enabled = True
      Me.ef_account_id.IsReadOnly = False
      Me.ef_card_track.IsReadOnly = False
      Me.ef_card_track.Enabled = True
      Me.btn_search_account.Enabled = False

    End If

    RaiseEvent OneAccountFilterChanged()

  End Sub

  ' PURPOSE : Build WHERE part for SQL query from field values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - String: WHERE sentence (require FROM or INNER with dbo.accounts table)
  '
  Public Function AccountGetFilterSQL() As String

    Dim str_sql As String
    Dim internal_card_id As String
    Dim rc As Boolean
    Dim card_type As Integer

    str_sql = ""

    If Me.ef_account_id.TextValue <> "" And ef_account_id.Enabled Then
      str_sql = " " & "AC_ACCOUNT_ID" & " = " & Me.ef_account_id.Value
    End If

    ' Filter Track
    If Me.ef_card_track.TextValue <> "" And ef_card_track.Enabled Then

      ' add and and
      If str_sql <> "" Then
        str_sql += " AND "
      End If

      ' First convert external (encrypted) card number into the internal id used in the database
      internal_card_id = ""
      If Me.ef_card_track.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
        rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_card_track.TextValue, internal_card_id, card_type)

        If rc = True Then
          str_sql += " " & "AC_TRACK_DATA" & " = '" & internal_card_id & "'"
        Else
          str_sql += " " & "AC_TRACK_DATA" & " = '" & Me.ef_card_track.TextValue & "'"
        End If
      Else
        str_sql += " " & "AC_TRACK_DATA" & " = '" & Me.ef_card_track.TextValue & "'"
      End If

    End If

    Return str_sql
  End Function

  ' PURPOSE : Update label with the computed points
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub UpdateComputedPointsControls()

    If m_anonymous_user Or chk_unknown.Checked Then
      lbl_computed_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683) & ": 0"
      ef_points.Value = 0
    Else
      lbl_computed_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683) & ": " & ComputePoints()
      ef_points.Value = ComputePoints()
    End If

  End Sub

  ' PURPOSE : Compute the points
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - Integer: computed points
  '
  Private Function ComputePoints() As Integer
    Dim _computed_points As Integer
    Dim _played_amount As Decimal
    Dim _current_amount As Decimal
    Dim _plays As Int32
    Dim _chips_in As Decimal
    Dim _chips_out As Decimal
    Dim _buy_in As Decimal
    Dim _account_id As Int64
    Dim _max_amount As Decimal
    Dim _wonBucketDict As WonBucketDict
    Dim _seat As Seat

    _computed_points = 0
    _played_amount = 0
    _current_amount = 0
    _plays = 0
    _chips_in = 0
    _chips_out = 0
    _buy_in = 0
    _max_amount = 0

    ef_buy_in.IsReadOnly = cmb_isoCode.TextValue(cmb_isoCode.SelectedIndex) <> CurrencyExchange.GetNationalCurrency()

    Decimal.TryParse(ef_played_amount.Value, _played_amount)
    'Decimal.TryParse(ef_current_bet.Value, _current_amount)
    'Int32.TryParse(ef_plays.Value, _plays)
    Decimal.TryParse(ef_chips_in.Value, _chips_in)
    Decimal.TryParse(ef_chips_out.Value, _chips_out)
    Decimal.TryParse(ef_buy_in.Value, _buy_in)

    If chk_unknown.Checked Then
      _account_id = m_virtual_account
    Else
      If m_account_id = 0 Then
        Return 0
      End If
      _account_id = m_account_id
    End If

    ' Calculate points with the Max amount(PlayedAmount, N�Plays*Current)
    '_max_amount = Math.Max(_played_amount, _plays * _current_amount)
    _computed_points = 0
    _wonBucketDict = New WonBucketDict()
    _seat = New Seat()
    _seat.PlaySession = New TablePlaySession()

    _seat.PlaySession.PlayerIsoCode = cmb_isoCode.TextValue(cmb_isoCode.SelectedIndex)
    _seat.PlaySession.PlayedAmount = _played_amount
    _seat.VirtualTerminalId = m_virtual_terminal
    _seat.AccountId = _account_id
    _seat.TheoricHold = m_current_theoric_hold

    WSI.Common.GamingTableBusinessLogic.Get_CalculateBuckets(_seat, _wonBucketDict)

    If (_wonBucketDict.Dict.ContainsKey(Buckets.BucketId.RedemptionPoints)) Then
      _computed_points = _wonBucketDict.Dict(Buckets.BucketId.RedemptionPoints).WonValue
    End If

    Return _computed_points
  End Function

  ' PURPOSE : Reset controls values and DBObject
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub ResetFields()

    Me.DbEditedObject = New CLASS_GAMING_TABLE_SESSION
    m_gt_session = DbEditedObject

    cmb_table_session_ValueChangedEvent()

    m_gt_session.AccountId = 0
    m_gt_session.StartSession = dtp_start_session.Value
    m_gt_session.EndSession = dtp_end_session.Value
    m_gt_session.Speed = GTPlayerTrackingSpeed.Medium
    m_gt_session.Skill = GTPlaySessionPlayerSkill.Average

    chk_unknown.Checked = False
    ef_account_id.Value = ""
    ef_account_id.Enabled = True
    ef_card_track.Value = ""
    ef_card_track.Enabled = True
    lbl_holder_name.Text = EMPTY_STRING

    cmb_speed.Value = GTPlayerTrackingSpeed.Medium
    cmb_skill.Value = GTPlaySessionPlayerSkill.Average

    ef_played_amount.Value = ""
    ef_current_bet.Value = ""
    ef_plays.Value = ""
    ef_min_bet.Value = ""
    ef_max_bet.Value = ""
    ef_chips_in.Value = ""
    ef_chips_out.Value = ""
    ef_buy_in.Value = ""
    ef_points.Value = ""

    UpdateComputedPointsControls()

    m_account_id = 0
    m_account_name = ""

  End Sub

#End Region ' Privates

#Region "Public"

#End Region ' Public

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_GAMING_TABLE_SESSION

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    m_elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode")

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(13)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.CloseOnOkClick = False

    ' Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5856)

    ' Table
    gb_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419)

    ' Type
    cmb_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)

    ' Name
    cmb_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4979)

    ' Table Session
    cmb_table_session.Text = GLB_NLS_GUI_CONTROLS.GetString(14)

    ' Table Session Status
    lbl_table_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4407)

    ' Table Session Start
    lbl_table_session_start.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101) & ": "

    ' Table Session End
    lbl_table_session_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2464) & ": "

    ' Account 
    gb_account.Text = GLB_NLS_GUI_INVOICING.GetString(230)

    ' Unkown
    chk_unknown.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020)

    ' Account id
    ef_account_id.Text = GLB_NLS_GUI_STATISTICS.GetString(207)
    ef_account_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 10)

    ' Trackdata
    ef_card_track.Text = GLB_NLS_GUI_STATISTICS.GetString(209)
    ef_card_track.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)

    ' Search
    btn_search_account.Text = GLB_NLS_GUI_CONTROLS.GetString(8)
    btn_search_account.Enabled = False

    ' Account Name
    lbl_holder_name.Text = EMPTY_STRING

    ' Gaming Session
    gb_gaming_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2590)

    ' Start session
    dtp_start_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285)
    dtp_start_session.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_start_session.Value = DateTime.Now.Date.AddHours(GetDefaultClosingTime())

    ' End time
    dtp_end_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286)
    dtp_end_session.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_end_session.Value = DateTime.Now.Date.AddHours(GetDefaultClosingTime())

    ' Walk time
    dtp_walk_time.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4984)
    dtp_walk_time.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_walk_time.Value = DateTime.Now.Date

    ' Speed
    cmb_speed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5019)

    ' Skill
    cmb_skill.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5024)

    ' Iso Code
    cmb_isoCode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430)

    ' GP Bet
    gb_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5857)

    ' Played amount
    ef_played_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
    ef_played_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' Current bet
    ef_current_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486)
    ef_current_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' No of plays
    ef_plays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4983)
    ef_plays.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 10)

    ' Min bet
    ef_min_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(420)
    ef_min_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' Max bet
    ef_max_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(845)
    ef_max_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' GP Chips
    gb_chips.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2941)

    ' Chips In
    ef_chips_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5287)
    ef_chips_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' Chips Out
    ef_chips_out.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5288)
    ef_chips_out.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' Buy-In
    If (GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", String.Empty) = String.Empty) Then
      ef_buy_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5289)
    Else
      ef_buy_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5073, GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text"))
    End If

    ef_buy_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' GB Points
    gb_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2612)

    ' Awared Points
    ef_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2684)
    ef_points.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 10)

    ' Computed Points
    lbl_computed_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2683) & ":"

    If m_elp_mode > 0 Then
      gb_points.Enabled = False
    End If

    LoadControls()

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      ResetFields()
      DbReadObject = DbEditedObject.Duplicate
    End If

  End Sub

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        If ef_account_id.ContainsFocus Or ef_card_track.ContainsFocus Then
          btn_search_account_Click(Nothing, Nothing)
        Else
          Call MyBase.GUI_ButtonClick(ButtonId)

        End If
      Case Else

        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _value_walk_time As TimeSpan
    Dim _value_game_time As TimeSpan
    Dim _value_d As Decimal
    Dim _value_i As Integer

    m_gt_session = Me.DbEditedObject

    ' Table Id
    m_gt_session.TableId = Me.cmb_name.Value

    ' Table Name
    m_gt_session.TableName = Me.cmb_name.TextValue

    ' Table Session Id
    m_gt_session.TableSessionId = Me.cmb_table_session.Value

    ' Table Session Name
    m_gt_session.TableSessionName = Me.cmb_table_session.TextValue

    ' Account Id
    If chk_unknown.Checked Then
      m_gt_session.AccountId = m_virtual_account
      m_gt_session.AccountName = ""
    Else
      m_gt_session.AccountId = m_account_id
      m_gt_session.AccountName = m_account_name
    End If

    ' Terminal
    If chk_unknown.Checked Or m_elp_mode > 0 Then
      m_gt_session.TerminalId = 0
      m_gt_session.TerminalName = ""
    Else
      m_gt_session.TerminalId = m_virtual_terminal
      m_gt_session.TerminalName = m_virtual_terminal_name
    End If

    ' Start
    m_gt_session.StartSession = dtp_start_session.Value

    ' End
    m_gt_session.EndSession = dtp_end_session.Value

    ' Walk Time
    _value_walk_time = dtp_walk_time.Value - DateTime.Now.Date
    m_gt_session.WalkTime = _value_walk_time.TotalSeconds()

    ' Game Time
    _value_game_time = dtp_end_session.Value - dtp_start_session.Value
    _value_game_time = _value_game_time - _value_walk_time
    m_gt_session.GameTime = _value_game_time.TotalSeconds()

    ' Speed
    m_gt_session.Speed = Me.cmb_speed.Value

    ' Skill
    m_gt_session.Skill = Me.cmb_skill.Value

    ' Played Amount
    If Decimal.TryParse(ef_played_amount.Value, _value_d) Then
      m_gt_session.PlayedAmount = _value_d
    End If

    ' Current Bet
    If Decimal.TryParse(ef_current_bet.Value, _value_d) Then
      m_gt_session.CurrentBet = _value_d
    End If

    ' Plays
    If Integer.TryParse(ef_plays.Value, _value_i) Then
      m_gt_session.Plays = _value_i
    End If

    ' Min Bet
    If Decimal.TryParse(ef_min_bet.Value, _value_d) Then
      m_gt_session.MinBet = _value_d
    End If

    ' Max Bet
    If Decimal.TryParse(ef_max_bet.Value, _value_d) Then
      m_gt_session.MaxBet = _value_d
    End If

    ' Chips In
    If Decimal.TryParse(ef_chips_in.Value, _value_d) Then
      m_gt_session.ChipsIn = _value_d
    End If

    ' Chips Out
    If Decimal.TryParse(ef_chips_out.Value, _value_d) Then
      m_gt_session.ChipsOut = _value_d
    End If

    ' Buy In
    If Decimal.TryParse(ef_buy_in.Value, _value_d) Then
      m_gt_session.BuyIn = _value_d
    End If

    ' Awared Points
    If Decimal.TryParse(ef_points.Value, _value_d) Then
      m_gt_session.Points = _value_d
    End If

    ' Computed Point
    If m_anonymous_user Or chk_unknown.Checked Then
      m_gt_session.ComputedPoints = 0
    Else
      m_gt_session.ComputedPoints = ComputePoints()
    End If

    ' Iso Code
    m_gt_session.IsoCode = Me.cmb_isoCode.TextValue(Me.cmb_isoCode.SelectedIndex)

  End Sub

  ' PURPOSE: Validate the data presented on the screen. The following values are not empty: Holder Name, RFC, CURP
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - True:  Data is ok
  '     - False: Data is NOT ok
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _value As TimeSpan
    Dim _played_amount As Decimal
    Dim _current_bet As Decimal
    Dim _plays As Int32

    _played_amount = 0
    _current_bet = 0
    _plays = 0

    If m_table_session_end = DateTime.MaxValue Then
      m_table_session_end = DateTime.Now
    End If

    ' Empty Table
    If cmb_name.Value = 0 Then
      ' 219 "El campo 'Mesa' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(4419))
      Call Me.cmb_name.Focus()

      Return False
    End If

    If cmb_table_session.Value = 0 Then
      ' 219 "El campo 'Sessi�n' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                GLB_NLS_GUI_CONTROLS.GetString(14))
      Call Me.cmb_table_session.Focus()

      Return False
    End If

    If Not chk_unknown.Checked And m_account_id = 0 Then
      ' 219 "El campo 'Cuenta' es obligatorio"
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(219), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                GLB_NLS_GUI_INVOICING.GetString(230))
      Call Me.ef_account_id.Focus()

      Return False
    End If

    If dtp_start_session.Value > dtp_end_session.Value Then
      ' 219 "%1 debe ser mayor que %2."
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(130), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , dtp_end_session.Text _
                                , dtp_start_session.Text)
      Call Me.dtp_end_session.Focus()

      Return False
    End If


    If dtp_start_session.Value < m_table_session_start Then
      '  "El valor del campo '%1' debe ser mayor o igual que la fecha de apertura de la sesi�n de juego."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5867), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285))
      Call Me.dtp_start_session.Focus()

      Return False
    End If

    If m_table_session_end < dtp_end_session.Value Then
      '  "El valor del campo '%1' debe ser menor o igual que la fecha de cierre de la sesi�n de juego."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5868), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286))
      Call Me.dtp_end_session.Focus()

      Return False
    End If

    _value = dtp_walk_time.Value - DateTime.Now.Date
    If dtp_start_session.Value.AddSeconds(_value.TotalSeconds()) > dtp_end_session.Value Then
      '  "El valor del campo '%1' debe ser menor o igual que la duraci�n de la sesi�n de juego ('%2' � '%3')."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5866), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK, _
                                , _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(4984), _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(5286), _
                                GLB_NLS_GUI_PLAYER_TRACKING.GetString(5285))
      Call Me.dtp_walk_time.Focus()

      Return False
    End If

    Decimal.TryParse(ef_played_amount.Value, _played_amount)
    Int32.TryParse(ef_plays.Value, _plays)
    Decimal.TryParse(ef_current_bet.Value, _current_bet)

    If _played_amount <> _plays * _current_bet And GeneralParam.GetBoolean("GamingTable.PlayerTracking", "ValidateBetFormulaOnManualEntry", True) Then
      ' "No se cumple la formula Monto = N� de jugadas * Actual. �Seguro que desea guardar los cambios?
      If ENUM_MB_RESULT.MB_RESULT_NO = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5874), _
                                                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                                      ENUM_MB_BTN.MB_BTN_YES_NO, _
                                                      , _
                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372), _
                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(4983), _
                                                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(4486)) Then
        Call Me.ef_current_bet.Focus()

        Return False
      End If
    End If

    Return True

  End Function

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        'ResetFields()

        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' "Se ha producido un error al insertar la sesi�n de juego en mesa."
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5858), ENUM_MB_TYPE.MB_TYPE_ERROR)
        Else
          ' " Los cambios se han guardado correctamente."
          Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), ENUM_MB_TYPE.MB_TYPE_INFO)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        ' Not used

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        ' Not used

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        ResetFields()
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

#End Region ' Overrides

#Region "Events"

  Private Sub cmb_type_ValueChangedEvent() Handles cmb_type.ValueChangedEvent

    Dim _filtered_rows As DataRow()
    Dim _datatable_filtered As DataTable
    Dim _gt_names As DataView

    cmb_name.Clear()

    _datatable_filtered = New DataTable
    _datatable_filtered = m_gt_types_names.Clone()

    If Not IsNothing(m_gt_types_names) Then
      _filtered_rows = m_gt_types_names.Select("GTT_GAMING_TABLE_TYPE_ID = " & cmb_type.Value)
      For Each _row As DataRow In _filtered_rows
        _datatable_filtered.ImportRow(_row)
      Next

      If _datatable_filtered.Rows.Count > 0 Then
        _gt_names = _datatable_filtered.DefaultView

        For Each _row As DataRow In _gt_names.ToTable(True, "GT_GAMING_TABLE_ID", "GT_NAME").Rows
          cmb_name.Add(_row(0), _row(1))
        Next

        cmb_name_ValueChangedEvent()

      End If
    End If
  End Sub

  Private Sub cmb_name_ValueChangedEvent() Handles cmb_name.ValueChangedEvent
    Dim _filtered_rows As DataRow()
    Dim _datatable_filtered As DataTable
    Dim _gt_names As DataView

    cmb_table_session.Clear()

    _datatable_filtered = New DataTable
    _datatable_filtered = m_gt_types_names.Clone()

    If Not IsNothing(m_gt_types_names) Then
      _filtered_rows = m_gt_types_names.Select("GT_GAMING_TABLE_ID = " & cmb_name.Value)

      For Each _row As DataRow In _filtered_rows
        _datatable_filtered.ImportRow(_row)
      Next

      If _datatable_filtered.Rows.Count > 0 Then
        _gt_names = _datatable_filtered.DefaultView

        m_virtual_account = _gt_names(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_VIRTUAL_ACCOUNT)
        m_virtual_terminal = _gt_names(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_VIRTUAL_TERMINAL)
        m_virtual_terminal_name = _gt_names(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_VIRTUAL_TERMINAL_NAME)
        m_current_theoric_hold = _gt_names(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_THEORICAL_HOLD)

        For Each _row As DataRow In _gt_names.ToTable(True, "GTS_GAMING_TABLE_SESSION_ID", "CS_NAME").Rows
          cmb_table_session.Add(Convert.ToInt32(_row(0)), _row(1))
        Next

      End If

      UpdateComputedPointsControls()

      cmb_table_session_ValueChangedEvent()
    End If

  End Sub

  Private Sub cmb_table_session_ValueChangedEvent() Handles cmb_table_session.ValueChangedEvent
    Dim _filtered_rows As DataRow()

    lbl_table_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4407) & " " & EMPTY_STRING

    If Not IsNothing(m_gt_types_names) Then
      _filtered_rows = m_gt_types_names.Select("GTS_GAMING_TABLE_SESSION_ID = " & cmb_table_session.Value)

      If _filtered_rows.Length > 0 Then
        m_table_session_start = _filtered_rows(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_CASHIER_SESSION_OPENING)

        ' Table Session Status
        If CASHIER_SESSION_STATUS.OPEN = _filtered_rows(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_CASHIER_SESSION_STATUS) Then
          lbl_table_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4407) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4408)
        Else
          lbl_table_session_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4407) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(4409)
        End If

        ' Table Session Start
        lbl_table_session_start.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2101) & ": " & GUI_FormatDate(m_table_session_start, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

        ' Set controls datetime value
        dtp_start_session.Value = m_table_session_start
        dtp_end_session.Value = m_table_session_start
        dtp_walk_time.Value = DateTime.Now.Date

        If IsDBNull(_filtered_rows(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_CASHIER_SESSION_CLOSING)) Then
          m_table_session_end = DateTime.MaxValue
          ' Table Session End
          lbl_table_session_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2464) & ": " & EMPTY_STRING
        Else
          m_table_session_end = _filtered_rows(0).Item(CLASS_GAMING_TABLE_SESSION.DT_COLUMN_CASHIER_SESSION_CLOSING)
          ' Table Session End
          lbl_table_session_end.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2464) & ": " & GUI_FormatDate(m_table_session_end, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        End If
      End If
    End If

  End Sub

  Private Sub chk_unknown_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_unknown.CheckedChanged

    ef_account_id.Enabled = Not chk_unknown.Checked
    ef_card_track.Enabled = Not chk_unknown.Checked
    btn_search_account.Enabled = Not chk_unknown.Checked

    If Not chk_unknown.Checked Then
      lbl_holder_name.Text = EMPTY_STRING
      account_or_trackdata_EntryFieldValueChanged()
      gb_points.Enabled = True And Not m_elp_mode > 0
    Else
      lbl_holder_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5020)
      gb_points.Enabled = False
      ef_points.Value = ""
    End If

    UpdateComputedPointsControls()
  End Sub

  Private Sub btn_search_account_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_search_account.Click

    Dim _account_id As Int64
    Dim _account_name As String
    Dim _anonym_account As Boolean

    _account_id = 0
    _account_name = String.Empty
    m_account_id = 0
    _anonym_account = False

    lbl_holder_name.Text = EMPTY_STRING

    If String.IsNullOrEmpty(ef_account_id.Value) And String.IsNullOrEmpty(ef_card_track.Value) Then
      Return
    End If

    If m_gt_session.ReadAccountInfo(AccountGetFilterSQL(), _account_id, _account_name, _anonym_account) Then
      lbl_holder_name.Text = _account_id & " - " & _account_name
      m_account_id = _account_id
      m_account_name = _account_name
      m_anonymous_user = _anonym_account

      If _anonym_account Then
        gb_points.Enabled = False
        ef_points.Value = 0
      Else
        gb_points.Enabled = True
      End If

      UpdateComputedPointsControls()
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5870), _
                                ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                ENUM_MB_BTN.MB_BTN_OK)
      If Not String.IsNullOrEmpty(ef_account_id.Text) Then
        Call Me.ef_account_id.Focus()
      Else
        Call Me.ef_card_track.Focus()
      End If

    End If

  End Sub

  Private Sub chips_in_or_chips_out_or_buy_in_EntryFieldValueChanged() Handles ef_played_amount.EntryFieldValueChanged, ef_current_bet.EntryFieldValueChanged, ef_plays.EntryFieldValueChanged, ef_chips_in.EntryFieldValueChanged, ef_chips_out.EntryFieldValueChanged, ef_buy_in.EntryFieldValueChanged, cmb_isoCode.ValueChangedEvent

    UpdateComputedPointsControls()

  End Sub

#End Region ' Events

End Class