<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_softcounts
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_collection_params = New System.Windows.Forms.GroupBox
    Me.lbl_notes = New System.Windows.Forms.Label
    Me.tb_notes = New System.Windows.Forms.TextBox
    Me.ef_floor_id = New GUI_Controls.uc_entry_field
    Me.ef_stacker_id = New GUI_Controls.uc_entry_field
    Me.ef_extraction_date = New GUI_Controls.uc_entry_field
    Me.ef_insertion_date = New GUI_Controls.uc_entry_field
    Me.ef_employee = New GUI_Controls.uc_entry_field
    Me.ef_terminal = New GUI_Controls.uc_entry_field
    Me.gb_total = New System.Windows.Forms.GroupBox
    Me.dg_total = New GUI_Controls.uc_grid
    Me.tb_currency = New System.Windows.Forms.TabControl
    Me.tab_tickets = New System.Windows.Forms.TabPage
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.tf_pending_tickets = New GUI_Controls.uc_text_field
    Me.Label2 = New System.Windows.Forms.Label
    Me.tf_error = New GUI_Controls.uc_text_field
    Me.Label3 = New System.Windows.Forms.Label
    Me.tf_ok = New GUI_Controls.uc_text_field
    Me.dg_inserted_tickets = New GUI_Controls.uc_grid
    Me.ef_tickets = New GUI_Controls.uc_entry_field
    Me.btn_insert_ticket = New GUI_Controls.uc_button
    Me.tab_ticket = New System.Windows.Forms.TabPage
    Me.ef_num_of_tickets = New GUI_Controls.uc_entry_field
    Me.gb_status1 = New System.Windows.Forms.GroupBox
    Me.lbl_pending = New System.Windows.Forms.Label
    Me.tf_pending = New GUI_Controls.uc_text_field
    Me.lbl_error = New System.Windows.Forms.Label
    Me.tf_error1 = New GUI_Controls.uc_text_field
    Me.Label5 = New System.Windows.Forms.Label
    Me.tf_ok1 = New GUI_Controls.uc_text_field
    Me.dg_collection_tickets = New GUI_Controls.uc_grid
    Me.ef_ticket = New GUI_Controls.uc_entry_field
    Me.btn_save_tickets = New GUI_Controls.uc_button
    Me.gb_note_counter_events = New System.Windows.Forms.GroupBox
    Me.lb_error_messages = New System.Windows.Forms.Label
    Me.gb_note_counter_status = New System.Windows.Forms.GroupBox
    Me.lb_note_counter_status = New System.Windows.Forms.Label
    Me.lbl_counter = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_collection_params.SuspendLayout()
    Me.gb_total.SuspendLayout()
    Me.tb_currency.SuspendLayout()
    Me.tab_tickets.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.tab_ticket.SuspendLayout()
    Me.gb_status1.SuspendLayout()
    Me.gb_note_counter_events.SuspendLayout()
    Me.gb_note_counter_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_note_counter_events)
    Me.panel_data.Controls.Add(Me.gb_note_counter_status)
    Me.panel_data.Controls.Add(Me.tb_currency)
    Me.panel_data.Controls.Add(Me.gb_total)
    Me.panel_data.Controls.Add(Me.gb_collection_params)
    Me.panel_data.Size = New System.Drawing.Size(654, 618)
    '
    'gb_collection_params
    '
    Me.gb_collection_params.Controls.Add(Me.lbl_notes)
    Me.gb_collection_params.Controls.Add(Me.tb_notes)
    Me.gb_collection_params.Controls.Add(Me.ef_floor_id)
    Me.gb_collection_params.Controls.Add(Me.ef_stacker_id)
    Me.gb_collection_params.Controls.Add(Me.ef_extraction_date)
    Me.gb_collection_params.Controls.Add(Me.ef_insertion_date)
    Me.gb_collection_params.Controls.Add(Me.ef_employee)
    Me.gb_collection_params.Controls.Add(Me.ef_terminal)
    Me.gb_collection_params.Location = New System.Drawing.Point(10, 15)
    Me.gb_collection_params.Name = "gb_collection_params"
    Me.gb_collection_params.Size = New System.Drawing.Size(631, 157)
    Me.gb_collection_params.TabIndex = 5
    Me.gb_collection_params.TabStop = False
    Me.gb_collection_params.Text = "xParams"
    '
    'lbl_notes
    '
    Me.lbl_notes.AutoSize = True
    Me.lbl_notes.Location = New System.Drawing.Point(65, 112)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(46, 13)
    Me.lbl_notes.TabIndex = 8
    Me.lbl_notes.Text = "xNotes"
    Me.lbl_notes.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'tb_notes
    '
    Me.tb_notes.Location = New System.Drawing.Point(110, 109)
    Me.tb_notes.MaxLength = 199
    Me.tb_notes.Multiline = True
    Me.tb_notes.Name = "tb_notes"
    Me.tb_notes.Size = New System.Drawing.Size(514, 36)
    Me.tb_notes.TabIndex = 7
    '
    'ef_floor_id
    '
    Me.ef_floor_id.DoubleValue = 0
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(404, 18)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.Size = New System.Drawing.Size(220, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 1
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 100
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_stacker_id
    '
    Me.ef_stacker_id.DoubleValue = 0
    Me.ef_stacker_id.IntegerValue = 0
    Me.ef_stacker_id.IsReadOnly = False
    Me.ef_stacker_id.Location = New System.Drawing.Point(28, 18)
    Me.ef_stacker_id.Name = "ef_stacker_id"
    Me.ef_stacker_id.Size = New System.Drawing.Size(200, 24)
    Me.ef_stacker_id.SufixText = "Sufix Text"
    Me.ef_stacker_id.SufixTextVisible = True
    Me.ef_stacker_id.TabIndex = 0
    Me.ef_stacker_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_id.TextValue = ""
    Me.ef_stacker_id.Value = ""
    Me.ef_stacker_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_extraction_date
    '
    Me.ef_extraction_date.DoubleValue = 0
    Me.ef_extraction_date.IntegerValue = 0
    Me.ef_extraction_date.IsReadOnly = False
    Me.ef_extraction_date.Location = New System.Drawing.Point(404, 79)
    Me.ef_extraction_date.Name = "ef_extraction_date"
    Me.ef_extraction_date.Size = New System.Drawing.Size(220, 24)
    Me.ef_extraction_date.SufixText = "Sufix Text"
    Me.ef_extraction_date.SufixTextVisible = True
    Me.ef_extraction_date.TabIndex = 5
    Me.ef_extraction_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_extraction_date.TextValue = ""
    Me.ef_extraction_date.TextWidth = 100
    Me.ef_extraction_date.Value = ""
    Me.ef_extraction_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_insertion_date
    '
    Me.ef_insertion_date.DoubleValue = 0
    Me.ef_insertion_date.IntegerValue = 0
    Me.ef_insertion_date.IsReadOnly = False
    Me.ef_insertion_date.Location = New System.Drawing.Point(404, 48)
    Me.ef_insertion_date.Name = "ef_insertion_date"
    Me.ef_insertion_date.Size = New System.Drawing.Size(220, 24)
    Me.ef_insertion_date.SufixText = "Sufix Text"
    Me.ef_insertion_date.SufixTextVisible = True
    Me.ef_insertion_date.TabIndex = 3
    Me.ef_insertion_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_insertion_date.TextValue = ""
    Me.ef_insertion_date.TextWidth = 100
    Me.ef_insertion_date.Value = ""
    Me.ef_insertion_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_employee
    '
    Me.ef_employee.DoubleValue = 0
    Me.ef_employee.IntegerValue = 0
    Me.ef_employee.IsReadOnly = False
    Me.ef_employee.Location = New System.Drawing.Point(7, 78)
    Me.ef_employee.Name = "ef_employee"
    Me.ef_employee.Size = New System.Drawing.Size(376, 24)
    Me.ef_employee.SufixText = "Sufix Text"
    Me.ef_employee.SufixTextVisible = True
    Me.ef_employee.TabIndex = 4
    Me.ef_employee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_employee.TextValue = ""
    Me.ef_employee.TextWidth = 100
    Me.ef_employee.Value = ""
    Me.ef_employee.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_terminal
    '
    Me.ef_terminal.DoubleValue = 0
    Me.ef_terminal.IntegerValue = 0
    Me.ef_terminal.IsReadOnly = False
    Me.ef_terminal.Location = New System.Drawing.Point(7, 48)
    Me.ef_terminal.Name = "ef_terminal"
    Me.ef_terminal.Size = New System.Drawing.Size(376, 24)
    Me.ef_terminal.SufixText = "Sufix Text"
    Me.ef_terminal.SufixTextVisible = True
    Me.ef_terminal.TabIndex = 2
    Me.ef_terminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal.TextValue = ""
    Me.ef_terminal.TextWidth = 100
    Me.ef_terminal.Value = ""
    Me.ef_terminal.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_total
    '
    Me.gb_total.Controls.Add(Me.dg_total)
    Me.gb_total.Location = New System.Drawing.Point(10, 178)
    Me.gb_total.Name = "gb_total"
    Me.gb_total.Size = New System.Drawing.Size(631, 125)
    Me.gb_total.TabIndex = 6
    Me.gb_total.TabStop = False
    Me.gb_total.Text = "xTotal"
    '
    'dg_total
    '
    Me.dg_total.CurrentCol = -1
    Me.dg_total.CurrentRow = -1
    Me.dg_total.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_total.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_total.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_total.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_total.Location = New System.Drawing.Point(3, 17)
    Me.dg_total.Name = "dg_total"
    Me.dg_total.PanelRightVisible = False
    Me.dg_total.Redraw = True
    Me.dg_total.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_total.Size = New System.Drawing.Size(625, 105)
    Me.dg_total.Sortable = False
    Me.dg_total.SortAscending = True
    Me.dg_total.SortByCol = 0
    Me.dg_total.TabIndex = 0
    Me.dg_total.ToolTipped = True
    Me.dg_total.TopRow = -2
    '
    'tb_currency
    '
    Me.tb_currency.Controls.Add(Me.tab_tickets)
    Me.tb_currency.Location = New System.Drawing.Point(13, 309)
    Me.tb_currency.Name = "tb_currency"
    Me.tb_currency.SelectedIndex = 0
    Me.tb_currency.Size = New System.Drawing.Size(628, 236)
    Me.tb_currency.TabIndex = 7
    '
    'tab_tickets
    '
    Me.tab_tickets.Controls.Add(Me.gb_status)
    Me.tab_tickets.Controls.Add(Me.dg_inserted_tickets)
    Me.tab_tickets.Controls.Add(Me.ef_tickets)
    Me.tab_tickets.Controls.Add(Me.btn_insert_ticket)
    Me.tab_tickets.Location = New System.Drawing.Point(4, 22)
    Me.tab_tickets.Name = "tab_tickets"
    Me.tab_tickets.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_tickets.Size = New System.Drawing.Size(620, 210)
    Me.tab_tickets.TabIndex = 2
    Me.tab_tickets.Text = "TabPage"
    Me.tab_tickets.UseVisualStyleBackColor = True
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.Label1)
    Me.gb_status.Controls.Add(Me.tf_pending_tickets)
    Me.gb_status.Controls.Add(Me.Label2)
    Me.gb_status.Controls.Add(Me.tf_error)
    Me.gb_status.Controls.Add(Me.Label3)
    Me.gb_status.Controls.Add(Me.tf_ok)
    Me.gb_status.Location = New System.Drawing.Point(467, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(123, 105)
    Me.gb_status.TabIndex = 111
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'Label1
    '
    Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label1.Location = New System.Drawing.Point(10, 69)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(16, 16)
    Me.Label1.TabIndex = 112
    '
    'tf_pending_tickets
    '
    Me.tf_pending_tickets.IsReadOnly = True
    Me.tf_pending_tickets.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_pending_tickets.LabelForeColor = System.Drawing.Color.Black
    Me.tf_pending_tickets.Location = New System.Drawing.Point(5, 65)
    Me.tf_pending_tickets.Name = "tf_pending_tickets"
    Me.tf_pending_tickets.Size = New System.Drawing.Size(116, 24)
    Me.tf_pending_tickets.SufixText = "Sufix Text"
    Me.tf_pending_tickets.SufixTextVisible = True
    Me.tf_pending_tickets.TabIndex = 111
    Me.tf_pending_tickets.TabStop = False
    Me.tf_pending_tickets.TextVisible = False
    Me.tf_pending_tickets.TextWidth = 30
    Me.tf_pending_tickets.Value = "xPending"
    '
    'Label2
    '
    Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label2.Location = New System.Drawing.Point(10, 44)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(16, 16)
    Me.Label2.TabIndex = 110
    '
    'tf_error
    '
    Me.tf_error.IsReadOnly = True
    Me.tf_error.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error.Location = New System.Drawing.Point(4, 40)
    Me.tf_error.Name = "tf_error"
    Me.tf_error.Size = New System.Drawing.Size(116, 24)
    Me.tf_error.SufixText = "Sufix Text"
    Me.tf_error.SufixTextVisible = True
    Me.tf_error.TabIndex = 109
    Me.tf_error.TabStop = False
    Me.tf_error.TextVisible = False
    Me.tf_error.TextWidth = 30
    Me.tf_error.Value = "xInactive"
    '
    'Label3
    '
    Me.Label3.BackColor = System.Drawing.SystemColors.Window
    Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label3.Location = New System.Drawing.Point(10, 19)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(16, 16)
    Me.Label3.TabIndex = 106
    '
    'tf_ok
    '
    Me.tf_ok.IsReadOnly = True
    Me.tf_ok.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_ok.LabelForeColor = System.Drawing.Color.Black
    Me.tf_ok.Location = New System.Drawing.Point(4, 15)
    Me.tf_ok.Name = "tf_ok"
    Me.tf_ok.Size = New System.Drawing.Size(114, 24)
    Me.tf_ok.SufixText = "Sufix Text"
    Me.tf_ok.SufixTextVisible = True
    Me.tf_ok.TabIndex = 104
    Me.tf_ok.TabStop = False
    Me.tf_ok.TextVisible = False
    Me.tf_ok.TextWidth = 30
    Me.tf_ok.Value = "xRunning"
    '
    'dg_inserted_tickets
    '
    Me.dg_inserted_tickets.CurrentCol = -1
    Me.dg_inserted_tickets.CurrentRow = -1
    Me.dg_inserted_tickets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_inserted_tickets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_inserted_tickets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_inserted_tickets.Location = New System.Drawing.Point(6, 41)
    Me.dg_inserted_tickets.Name = "dg_inserted_tickets"
    Me.dg_inserted_tickets.PanelRightVisible = False
    Me.dg_inserted_tickets.Redraw = True
    Me.dg_inserted_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_inserted_tickets.Size = New System.Drawing.Size(452, 169)
    Me.dg_inserted_tickets.Sortable = False
    Me.dg_inserted_tickets.SortAscending = True
    Me.dg_inserted_tickets.SortByCol = 0
    Me.dg_inserted_tickets.TabIndex = 1
    Me.dg_inserted_tickets.ToolTipped = True
    Me.dg_inserted_tickets.TopRow = -2
    '
    'ef_tickets
    '
    Me.ef_tickets.DoubleValue = 0
    Me.ef_tickets.IntegerValue = 0
    Me.ef_tickets.IsReadOnly = False
    Me.ef_tickets.Location = New System.Drawing.Point(6, 6)
    Me.ef_tickets.Name = "ef_tickets"
    Me.ef_tickets.Size = New System.Drawing.Size(324, 24)
    Me.ef_tickets.SufixText = "Sufix Text"
    Me.ef_tickets.SufixTextVisible = True
    Me.ef_tickets.TabIndex = 0
    Me.ef_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tickets.TextValue = ""
    Me.ef_tickets.TextVisible = False
    Me.ef_tickets.Value = ""
    Me.ef_tickets.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_insert_ticket
    '
    Me.btn_insert_ticket.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_insert_ticket.Location = New System.Drawing.Point(336, 6)
    Me.btn_insert_ticket.Name = "btn_insert_ticket"
    Me.btn_insert_ticket.Size = New System.Drawing.Size(90, 30)
    Me.btn_insert_ticket.TabIndex = 1
    Me.btn_insert_ticket.ToolTipped = False
    Me.btn_insert_ticket.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'tab_ticket
    '
    Me.tab_ticket.Controls.Add(Me.ef_num_of_tickets)
    Me.tab_ticket.Controls.Add(Me.gb_status1)
    Me.tab_ticket.Controls.Add(Me.dg_collection_tickets)
    Me.tab_ticket.Controls.Add(Me.ef_ticket)
    Me.tab_ticket.Controls.Add(Me.btn_save_tickets)
    Me.tab_ticket.Location = New System.Drawing.Point(4, 22)
    Me.tab_ticket.Name = "tab_ticket"
    Me.tab_ticket.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_ticket.Size = New System.Drawing.Size(700, 286)
    Me.tab_ticket.TabIndex = 1
    Me.tab_ticket.Text = "TabPage2"
    Me.tab_ticket.UseVisualStyleBackColor = True
    '
    'ef_num_of_tickets
    '
    Me.ef_num_of_tickets.DoubleValue = 0
    Me.ef_num_of_tickets.IntegerValue = 0
    Me.ef_num_of_tickets.IsReadOnly = False
    Me.ef_num_of_tickets.Location = New System.Drawing.Point(530, 116)
    Me.ef_num_of_tickets.Name = "ef_num_of_tickets"
    Me.ef_num_of_tickets.Size = New System.Drawing.Size(122, 24)
    Me.ef_num_of_tickets.SufixText = "Sufix Text"
    Me.ef_num_of_tickets.SufixTextVisible = True
    Me.ef_num_of_tickets.TabIndex = 114
    Me.ef_num_of_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_num_of_tickets.TextValue = ""
    Me.ef_num_of_tickets.TextVisible = False
    Me.ef_num_of_tickets.TextWidth = 76
    Me.ef_num_of_tickets.Value = ""
    Me.ef_num_of_tickets.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_status1
    '
    Me.gb_status1.Controls.Add(Me.lbl_pending)
    Me.gb_status1.Controls.Add(Me.tf_pending)
    Me.gb_status1.Controls.Add(Me.lbl_error)
    Me.gb_status1.Controls.Add(Me.tf_error1)
    Me.gb_status1.Controls.Add(Me.Label5)
    Me.gb_status1.Controls.Add(Me.tf_ok1)
    Me.gb_status1.Location = New System.Drawing.Point(530, 6)
    Me.gb_status1.Name = "gb_status1"
    Me.gb_status1.Size = New System.Drawing.Size(123, 105)
    Me.gb_status1.TabIndex = 111
    Me.gb_status1.TabStop = False
    Me.gb_status1.Text = "xStatus"
    '
    'lbl_pending
    '
    Me.lbl_pending.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(10, 69)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending.TabIndex = 112
    '
    'tf_pending
    '
    Me.tf_pending.IsReadOnly = True
    Me.tf_pending.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_pending.LabelForeColor = System.Drawing.Color.Black
    Me.tf_pending.Location = New System.Drawing.Point(5, 65)
    Me.tf_pending.Name = "tf_pending"
    Me.tf_pending.Size = New System.Drawing.Size(116, 24)
    Me.tf_pending.SufixText = "Sufix Text"
    Me.tf_pending.SufixTextVisible = True
    Me.tf_pending.TabIndex = 111
    Me.tf_pending.TabStop = False
    Me.tf_pending.TextVisible = False
    Me.tf_pending.TextWidth = 30
    Me.tf_pending.Value = "xPending"
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 110
    '
    'tf_error1
    '
    Me.tf_error1.IsReadOnly = True
    Me.tf_error1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error1.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error1.Location = New System.Drawing.Point(4, 40)
    Me.tf_error1.Name = "tf_error1"
    Me.tf_error1.Size = New System.Drawing.Size(116, 24)
    Me.tf_error1.SufixText = "Sufix Text"
    Me.tf_error1.SufixTextVisible = True
    Me.tf_error1.TabIndex = 109
    Me.tf_error1.TabStop = False
    Me.tf_error1.TextVisible = False
    Me.tf_error1.TextWidth = 30
    Me.tf_error1.Value = "xInactive"
    '
    'Label5
    '
    Me.Label5.BackColor = System.Drawing.SystemColors.Window
    Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label5.Location = New System.Drawing.Point(10, 19)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(16, 16)
    Me.Label5.TabIndex = 106
    '
    'tf_ok1
    '
    Me.tf_ok1.IsReadOnly = True
    Me.tf_ok1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_ok1.LabelForeColor = System.Drawing.Color.Black
    Me.tf_ok1.Location = New System.Drawing.Point(4, 15)
    Me.tf_ok1.Name = "tf_ok1"
    Me.tf_ok1.Size = New System.Drawing.Size(114, 24)
    Me.tf_ok1.SufixText = "Sufix Text"
    Me.tf_ok1.SufixTextVisible = True
    Me.tf_ok1.TabIndex = 104
    Me.tf_ok1.TabStop = False
    Me.tf_ok1.TextVisible = False
    Me.tf_ok1.TextWidth = 30
    Me.tf_ok1.Value = "xRunning"
    '
    'dg_collection_tickets
    '
    Me.dg_collection_tickets.CurrentCol = -1
    Me.dg_collection_tickets.CurrentRow = -1
    Me.dg_collection_tickets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_collection_tickets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_collection_tickets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_collection_tickets.Location = New System.Drawing.Point(6, 41)
    Me.dg_collection_tickets.Name = "dg_collection_tickets"
    Me.dg_collection_tickets.PanelRightVisible = False
    Me.dg_collection_tickets.Redraw = True
    Me.dg_collection_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_collection_tickets.Size = New System.Drawing.Size(514, 210)
    Me.dg_collection_tickets.Sortable = False
    Me.dg_collection_tickets.SortAscending = True
    Me.dg_collection_tickets.SortByCol = 0
    Me.dg_collection_tickets.TabIndex = 1
    Me.dg_collection_tickets.ToolTipped = True
    Me.dg_collection_tickets.TopRow = -2
    '
    'ef_ticket
    '
    Me.ef_ticket.DoubleValue = 0
    Me.ef_ticket.IntegerValue = 0
    Me.ef_ticket.IsReadOnly = False
    Me.ef_ticket.Location = New System.Drawing.Point(6, 6)
    Me.ef_ticket.Name = "ef_ticket"
    Me.ef_ticket.Size = New System.Drawing.Size(400, 24)
    Me.ef_ticket.SufixText = "Sufix Text"
    Me.ef_ticket.SufixTextVisible = True
    Me.ef_ticket.TabIndex = 0
    Me.ef_ticket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket.TextValue = ""
    Me.ef_ticket.TextVisible = False
    Me.ef_ticket.Value = ""
    Me.ef_ticket.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_save_tickets
    '
    Me.btn_save_tickets.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_save_tickets.Location = New System.Drawing.Point(430, 6)
    Me.btn_save_tickets.Name = "btn_save_tickets"
    Me.btn_save_tickets.Size = New System.Drawing.Size(90, 30)
    Me.btn_save_tickets.TabIndex = 1
    Me.btn_save_tickets.ToolTipped = False
    Me.btn_save_tickets.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_note_counter_events
    '
    Me.gb_note_counter_events.Controls.Add(Me.lb_error_messages)
    Me.gb_note_counter_events.Location = New System.Drawing.Point(184, 561)
    Me.gb_note_counter_events.Name = "gb_note_counter_events"
    Me.gb_note_counter_events.Size = New System.Drawing.Size(454, 50)
    Me.gb_note_counter_events.TabIndex = 14
    Me.gb_note_counter_events.TabStop = False
    Me.gb_note_counter_events.Text = "xNoteCounterEvents"
    '
    'lb_error_messages
    '
    Me.lb_error_messages.ForeColor = System.Drawing.Color.Red
    Me.lb_error_messages.Location = New System.Drawing.Point(6, 13)
    Me.lb_error_messages.Name = "lb_error_messages"
    Me.lb_error_messages.Size = New System.Drawing.Size(442, 30)
    Me.lb_error_messages.TabIndex = 14
    Me.lb_error_messages.Text = "xError"
    '
    'gb_note_counter_status
    '
    Me.gb_note_counter_status.Controls.Add(Me.lb_note_counter_status)
    Me.gb_note_counter_status.Controls.Add(Me.lbl_counter)
    Me.gb_note_counter_status.Location = New System.Drawing.Point(13, 561)
    Me.gb_note_counter_status.Name = "gb_note_counter_status"
    Me.gb_note_counter_status.Size = New System.Drawing.Size(161, 50)
    Me.gb_note_counter_status.TabIndex = 13
    Me.gb_note_counter_status.TabStop = False
    Me.gb_note_counter_status.Text = "xNoteCounterStatus"
    '
    'lb_note_counter_status
    '
    Me.lb_note_counter_status.BackColor = System.Drawing.Color.Red
    Me.lb_note_counter_status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lb_note_counter_status.Location = New System.Drawing.Point(10, 21)
    Me.lb_note_counter_status.Name = "lb_note_counter_status"
    Me.lb_note_counter_status.Size = New System.Drawing.Size(16, 16)
    Me.lb_note_counter_status.TabIndex = 111
    '
    'lbl_counter
    '
    Me.lbl_counter.AutoSize = True
    Me.lbl_counter.Location = New System.Drawing.Point(38, 22)
    Me.lbl_counter.Name = "lbl_counter"
    Me.lbl_counter.Size = New System.Drawing.Size(59, 13)
    Me.lbl_counter.TabIndex = 15
    Me.lbl_counter.Text = "Available"
    Me.lbl_counter.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'frm_terminal_softcounts
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(756, 627)
    Me.Name = "frm_terminal_softcounts"
    Me.Text = "frm_terminal_softcounts"
    Me.panel_data.ResumeLayout(False)
    Me.gb_collection_params.ResumeLayout(False)
    Me.gb_collection_params.PerformLayout()
    Me.gb_total.ResumeLayout(False)
    Me.tb_currency.ResumeLayout(False)
    Me.tab_tickets.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.tab_ticket.ResumeLayout(False)
    Me.gb_status1.ResumeLayout(False)
    Me.gb_note_counter_events.ResumeLayout(False)
    Me.gb_note_counter_status.ResumeLayout(False)
    Me.gb_note_counter_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_collection_params As System.Windows.Forms.GroupBox
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stacker_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extraction_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_insertion_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_employee As GUI_Controls.uc_entry_field
  Friend WithEvents ef_terminal As GUI_Controls.uc_entry_field
  Friend WithEvents gb_total As System.Windows.Forms.GroupBox
  Friend WithEvents dg_total As GUI_Controls.uc_grid
  Friend WithEvents tb_currency As System.Windows.Forms.TabControl
  Friend WithEvents tb_notes As System.Windows.Forms.TextBox
  Friend WithEvents lbl_notes As System.Windows.Forms.Label
  Friend WithEvents tab_ticket As System.Windows.Forms.TabPage
  Friend WithEvents ef_num_of_tickets As GUI_Controls.uc_entry_field
  Friend WithEvents gb_status1 As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents tf_pending As GUI_Controls.uc_text_field
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents tf_error1 As GUI_Controls.uc_text_field
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents tf_ok1 As GUI_Controls.uc_text_field
  Friend WithEvents dg_collection_tickets As GUI_Controls.uc_grid
  Friend WithEvents ef_ticket As GUI_Controls.uc_entry_field
  Friend WithEvents btn_save_tickets As GUI_Controls.uc_button
  Friend WithEvents tab_tickets As System.Windows.Forms.TabPage
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents tf_pending_tickets As GUI_Controls.uc_text_field
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents tf_error As GUI_Controls.uc_text_field
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents tf_ok As GUI_Controls.uc_text_field
  Friend WithEvents dg_inserted_tickets As GUI_Controls.uc_grid
  Friend WithEvents ef_tickets As GUI_Controls.uc_entry_field
  Friend WithEvents btn_insert_ticket As GUI_Controls.uc_button
  Friend WithEvents gb_note_counter_events As System.Windows.Forms.GroupBox
  Friend WithEvents lb_error_messages As System.Windows.Forms.Label
  Friend WithEvents gb_note_counter_status As System.Windows.Forms.GroupBox
  Friend WithEvents lb_note_counter_status As System.Windows.Forms.Label
  Friend WithEvents lbl_counter As System.Windows.Forms.Label
End Class
