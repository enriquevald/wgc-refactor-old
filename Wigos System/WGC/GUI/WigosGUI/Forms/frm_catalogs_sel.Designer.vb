<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_catalogs_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_catalogs_name = New GUI_Controls.uc_entry_field()
    Me.gb_catalogs_status = New System.Windows.Forms.GroupBox()
    Me.chk_status_disabled = New System.Windows.Forms.CheckBox()
    Me.chk_status_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_catalogs_type = New System.Windows.Forms.GroupBox()
    Me.chk_type_system = New System.Windows.Forms.CheckBox()
    Me.chk_type_user = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_catalogs_status.SuspendLayout()
    Me.gb_catalogs_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_catalogs_name)
    Me.panel_filter.Controls.Add(Me.gb_catalogs_type)
    Me.panel_filter.Controls.Add(Me.gb_catalogs_status)
    Me.panel_filter.Size = New System.Drawing.Size(1136, 105)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_catalogs_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_catalogs_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_catalogs_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 109)
    Me.panel_data.Size = New System.Drawing.Size(1136, 456)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1130, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1130, 4)
    '
    'ef_catalogs_name
    '
    Me.ef_catalogs_name.DoubleValue = 0.0R
    Me.ef_catalogs_name.IntegerValue = 0
    Me.ef_catalogs_name.IsReadOnly = False
    Me.ef_catalogs_name.Location = New System.Drawing.Point(34, 20)
    Me.ef_catalogs_name.Name = "ef_catalogs_name"
    Me.ef_catalogs_name.PlaceHolder = Nothing
    Me.ef_catalogs_name.Size = New System.Drawing.Size(252, 24)
    Me.ef_catalogs_name.SufixText = "Sufix Text"
    Me.ef_catalogs_name.SufixTextVisible = True
    Me.ef_catalogs_name.TabIndex = 2
    Me.ef_catalogs_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_catalogs_name.TextValue = ""
    Me.ef_catalogs_name.Value = ""
    Me.ef_catalogs_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_catalogs_status
    '
    Me.gb_catalogs_status.Controls.Add(Me.chk_status_disabled)
    Me.gb_catalogs_status.Controls.Add(Me.chk_status_enabled)
    Me.gb_catalogs_status.Location = New System.Drawing.Point(439, 20)
    Me.gb_catalogs_status.Name = "gb_catalogs_status"
    Me.gb_catalogs_status.Size = New System.Drawing.Size(118, 64)
    Me.gb_catalogs_status.TabIndex = 11
    Me.gb_catalogs_status.TabStop = False
    Me.gb_catalogs_status.Text = "status"
    '
    'chk_status_disabled
    '
    Me.chk_status_disabled.AutoSize = True
    Me.chk_status_disabled.Location = New System.Drawing.Point(9, 38)
    Me.chk_status_disabled.Name = "chk_status_disabled"
    Me.chk_status_disabled.Size = New System.Drawing.Size(82, 17)
    Me.chk_status_disabled.TabIndex = 2
    Me.chk_status_disabled.Text = "xDisabled"
    Me.chk_status_disabled.UseVisualStyleBackColor = True
    '
    'chk_status_enabled
    '
    Me.chk_status_enabled.AutoSize = True
    Me.chk_status_enabled.Location = New System.Drawing.Point(9, 16)
    Me.chk_status_enabled.Name = "chk_status_enabled"
    Me.chk_status_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_status_enabled.TabIndex = 1
    Me.chk_status_enabled.Text = "xEnabled"
    Me.chk_status_enabled.UseVisualStyleBackColor = True
    '
    'gb_catalogs_type
    '
    Me.gb_catalogs_type.Controls.Add(Me.chk_type_system)
    Me.gb_catalogs_type.Controls.Add(Me.chk_type_user)
    Me.gb_catalogs_type.Location = New System.Drawing.Point(315, 20)
    Me.gb_catalogs_type.Name = "gb_catalogs_type"
    Me.gb_catalogs_type.Size = New System.Drawing.Size(118, 64)
    Me.gb_catalogs_type.TabIndex = 12
    Me.gb_catalogs_type.TabStop = False
    Me.gb_catalogs_type.Text = "type"
    '
    'chk_type_system
    '
    Me.chk_type_system.AutoSize = True
    Me.chk_type_system.Location = New System.Drawing.Point(9, 38)
    Me.chk_type_system.Name = "chk_type_system"
    Me.chk_type_system.Size = New System.Drawing.Size(82, 17)
    Me.chk_type_system.TabIndex = 2
    Me.chk_type_system.Text = "xDisabled"
    Me.chk_type_system.UseVisualStyleBackColor = True
    '
    'chk_type_user
    '
    Me.chk_type_user.AutoSize = True
    Me.chk_type_user.Location = New System.Drawing.Point(9, 16)
    Me.chk_type_user.Name = "chk_type_user"
    Me.chk_type_user.Size = New System.Drawing.Size(78, 17)
    Me.chk_type_user.TabIndex = 1
    Me.chk_type_user.Text = "xEnabled"
    Me.chk_type_user.UseVisualStyleBackColor = True
    '
    'frm_catalogs_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1144, 569)
    Me.Name = "frm_catalogs_sel"
    Me.Text = "frm_catalogss_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_catalogs_status.ResumeLayout(False)
    Me.gb_catalogs_status.PerformLayout()
    Me.gb_catalogs_type.ResumeLayout(False)
    Me.gb_catalogs_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
    Friend WithEvents ef_catalogs_name As GUI_Controls.uc_entry_field
    Friend WithEvents gb_catalogs_type As System.Windows.Forms.GroupBox
    Friend WithEvents chk_type_system As System.Windows.Forms.CheckBox
    Friend WithEvents chk_type_user As System.Windows.Forms.CheckBox
    Friend WithEvents gb_catalogs_status As System.Windows.Forms.GroupBox
    Friend WithEvents chk_status_disabled As System.Windows.Forms.CheckBox
    Friend WithEvents chk_status_enabled As System.Windows.Forms.CheckBox
End Class
