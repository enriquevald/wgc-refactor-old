<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_groups_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.gb_group_status = New System.Windows.Forms.GroupBox
    Me.chk_enabled = New System.Windows.Forms.CheckBox
    Me.chk_disabled = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_group_status)
    Me.panel_filter.Controls.Add(Me.ef_name)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1002, 82)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 86)
    Me.panel_data.Size = New System.Drawing.Size(1002, 422)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(996, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(996, 4)
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(13, 12)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.Size = New System.Drawing.Size(209, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    '
    'gb_group_status
    '
    Me.gb_group_status.Controls.Add(Me.chk_enabled)
    Me.gb_group_status.Controls.Add(Me.chk_disabled)
    Me.gb_group_status.Location = New System.Drawing.Point(241, 3)
    Me.gb_group_status.Name = "gb_group_status"
    Me.gb_group_status.Size = New System.Drawing.Size(89, 71)
    Me.gb_group_status.TabIndex = 2
    Me.gb_group_status.TabStop = False
    Me.gb_group_status.Text = "xEnabled"
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(13, 19)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(48, 17)
    Me.chk_enabled.TabIndex = 1
    Me.chk_enabled.Text = "xNo"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(13, 42)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(53, 17)
    Me.chk_disabled.TabIndex = 0
    Me.chk_disabled.Text = "xYes"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'frm_groups_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1012, 512)
    Me.Name = "frm_groups_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_group_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_status.ResumeLayout(False)
    Me.gb_group_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_group_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
End Class
