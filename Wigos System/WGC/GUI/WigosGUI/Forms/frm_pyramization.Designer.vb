﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_pyramization
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_pyramization))
    Me.lbl_info = New System.Windows.Forms.Label()
    Me.lbl_sum = New System.Windows.Forms.Label()
    Me.bt_cancel = New System.Windows.Forms.Button()
    Me.bt_accept = New System.Windows.Forms.Button()
    Me.bt_remove = New System.Windows.Forms.Button()
    Me.bt_add = New System.Windows.Forms.Button()
    Me.dgv_pyramid = New System.Windows.Forms.DataGridView()
    Me.panel_data.SuspendLayout()
    CType(Me.dgv_pyramid, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.lbl_info)
    Me.panel_data.Controls.Add(Me.lbl_sum)
    Me.panel_data.Controls.Add(Me.bt_cancel)
    Me.panel_data.Controls.Add(Me.bt_accept)
    Me.panel_data.Controls.Add(Me.bt_remove)
    Me.panel_data.Controls.Add(Me.bt_add)
    Me.panel_data.Controls.Add(Me.dgv_pyramid)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(0, 0)
    Me.panel_data.Size = New System.Drawing.Size(387, 333)
    '
    'lbl_info
    '
    Me.lbl_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_info.Location = New System.Drawing.Point(3, 226)
    Me.lbl_info.Name = "lbl_info"
    Me.lbl_info.Size = New System.Drawing.Size(355, 61)
    Me.lbl_info.TabIndex = 22
    Me.lbl_info.Text = "xInfo"
    Me.lbl_info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_sum
    '
    Me.lbl_sum.AutoSize = True
    Me.lbl_sum.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_sum.Location = New System.Drawing.Point(32, 206)
    Me.lbl_sum.Name = "lbl_sum"
    Me.lbl_sum.Size = New System.Drawing.Size(43, 20)
    Me.lbl_sum.TabIndex = 21
    Me.lbl_sum.Text = "x100"
    '
    'bt_cancel
    '
    Me.bt_cancel.Location = New System.Drawing.Point(299, 290)
    Me.bt_cancel.Name = "bt_cancel"
    Me.bt_cancel.Size = New System.Drawing.Size(71, 23)
    Me.bt_cancel.TabIndex = 20
    Me.bt_cancel.Text = "xCancel"
    Me.bt_cancel.UseVisualStyleBackColor = True
    '
    'bt_accept
    '
    Me.bt_accept.Location = New System.Drawing.Point(12, 290)
    Me.bt_accept.Name = "bt_accept"
    Me.bt_accept.Size = New System.Drawing.Size(71, 23)
    Me.bt_accept.TabIndex = 19
    Me.bt_accept.Text = "xAccept"
    Me.bt_accept.UseVisualStyleBackColor = True
    '
    'bt_remove
    '
    Me.bt_remove.Location = New System.Drawing.Point(335, 32)
    Me.bt_remove.Name = "bt_remove"
    Me.bt_remove.Size = New System.Drawing.Size(35, 23)
    Me.bt_remove.TabIndex = 18
    Me.bt_remove.Text = "-"
    Me.bt_remove.UseVisualStyleBackColor = True
    '
    'bt_add
    '
    Me.bt_add.Location = New System.Drawing.Point(335, 3)
    Me.bt_add.Name = "bt_add"
    Me.bt_add.Size = New System.Drawing.Size(35, 23)
    Me.bt_add.TabIndex = 17
    Me.bt_add.Text = "+"
    Me.bt_add.UseVisualStyleBackColor = True
    '
    'dgv_pyramid
    '
    Me.dgv_pyramid.AllowUserToAddRows = False
    Me.dgv_pyramid.AllowUserToDeleteRows = False
    Me.dgv_pyramid.AllowUserToResizeColumns = False
    Me.dgv_pyramid.AllowUserToResizeRows = False
    Me.dgv_pyramid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv_pyramid.Location = New System.Drawing.Point(3, 3)
    Me.dgv_pyramid.MultiSelect = False
    Me.dgv_pyramid.Name = "dgv_pyramid"
    Me.dgv_pyramid.RowHeadersVisible = False
    Me.dgv_pyramid.Size = New System.Drawing.Size(320, 196)
    Me.dgv_pyramid.TabIndex = 16
    '
    'frm_pyramization
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
    Me.ClientSize = New System.Drawing.Size(387, 333)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.HidePanelButtons = True
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_pyramization"
    Me.Padding = New System.Windows.Forms.Padding(0)
    Me.Text = "frm_pyramidal_values"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    CType(Me.dgv_pyramid, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Protected WithEvents lbl_info As System.Windows.Forms.Label
  Friend WithEvents lbl_sum As System.Windows.Forms.Label
  Friend WithEvents bt_cancel As System.Windows.Forms.Button
  Friend WithEvents bt_accept As System.Windows.Forms.Button
  Friend WithEvents bt_remove As System.Windows.Forms.Button
  Friend WithEvents bt_add As System.Windows.Forms.Button
  Friend WithEvents dgv_pyramid As System.Windows.Forms.DataGridView
End Class
