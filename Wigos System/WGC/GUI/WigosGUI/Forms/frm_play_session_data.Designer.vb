<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_play_session_data
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.txt_data = New System.Windows.Forms.TextBox
    Me.btn_exit = New GUI_Controls.uc_button
    Me.SuspendLayout()
    '
    'txt_data
    '
    Me.txt_data.Location = New System.Drawing.Point(8, 7)
    Me.txt_data.MaxLength = 255
    Me.txt_data.Multiline = True
    Me.txt_data.Name = "txt_data"
    Me.txt_data.ReadOnly = True
    Me.txt_data.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_data.Size = New System.Drawing.Size(526, 159)
    Me.txt_data.TabIndex = 10
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(444, 185)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 11
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_play_session_data
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(542, 222)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.txt_data)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_play_session_data"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.Text = "frm_play_session_data"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents txt_data As System.Windows.Forms.TextBox
  Friend WithEvents btn_exit As GUI_Controls.uc_button
End Class
