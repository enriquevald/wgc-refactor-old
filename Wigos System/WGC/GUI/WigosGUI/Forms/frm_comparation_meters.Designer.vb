<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_comparation_meters
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector()
    Me.gb_options = New System.Windows.Forms.GroupBox()
    Me.lbl_detail_filter = New System.Windows.Forms.Label()
    Me.chk_show_imbalance = New System.Windows.Forms.CheckBox()
    Me.chk_show_terminals = New System.Windows.Forms.CheckBox()
    Me.chk_show_terminal_location = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_options)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1248, 244)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_options, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 248)
    Me.panel_data.Size = New System.Drawing.Size(1248, 296)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1242, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1242, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(266, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(393, 194)
    Me.uc_pr_list.TabIndex = 5
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(3, 5)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(257, 114)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_options
    '
    Me.gb_options.Controls.Add(Me.lbl_detail_filter)
    Me.gb_options.Controls.Add(Me.chk_show_imbalance)
    Me.gb_options.Controls.Add(Me.chk_show_terminals)
    Me.gb_options.Controls.Add(Me.chk_show_terminal_location)
    Me.gb_options.Location = New System.Drawing.Point(6, 125)
    Me.gb_options.Name = "gb_options"
    Me.gb_options.Size = New System.Drawing.Size(257, 112)
    Me.gb_options.TabIndex = 14
    Me.gb_options.TabStop = False
    Me.gb_options.Text = "xOptions"
    '
    'lbl_detail_filter
    '
    Me.lbl_detail_filter.AutoSize = True
    Me.lbl_detail_filter.Location = New System.Drawing.Point(17, 91)
    Me.lbl_detail_filter.Name = "lbl_detail_filter"
    Me.lbl_detail_filter.Size = New System.Drawing.Size(44, 13)
    Me.lbl_detail_filter.TabIndex = 14
    Me.lbl_detail_filter.Text = "Label1"
    '
    'chk_show_imbalance
    '
    Me.chk_show_imbalance.AutoSize = True
    Me.chk_show_imbalance.Location = New System.Drawing.Point(20, 66)
    Me.chk_show_imbalance.Name = "chk_show_imbalance"
    Me.chk_show_imbalance.Size = New System.Drawing.Size(124, 17)
    Me.chk_show_imbalance.TabIndex = 13
    Me.chk_show_imbalance.Text = "xShowImbalance"
    Me.chk_show_imbalance.UseVisualStyleBackColor = True
    '
    'chk_show_terminals
    '
    Me.chk_show_terminals.AutoSize = True
    Me.chk_show_terminals.Location = New System.Drawing.Point(22, 20)
    Me.chk_show_terminals.Name = "chk_show_terminals"
    Me.chk_show_terminals.Size = New System.Drawing.Size(122, 17)
    Me.chk_show_terminals.TabIndex = 11
    Me.chk_show_terminals.Text = "xDetailTerminals"
    Me.chk_show_terminals.UseVisualStyleBackColor = True
    '
    'chk_show_terminal_location
    '
    Me.chk_show_terminal_location.AutoSize = True
    Me.chk_show_terminal_location.Location = New System.Drawing.Point(39, 43)
    Me.chk_show_terminal_location.Name = "chk_show_terminal_location"
    Me.chk_show_terminal_location.Size = New System.Drawing.Size(169, 17)
    Me.chk_show_terminal_location.TabIndex = 12
    Me.chk_show_terminal_location.Text = "xShow terminals location"
    Me.chk_show_terminal_location.UseVisualStyleBackColor = True
    '
    'frm_comparation_meters
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1258, 548)
    Me.Name = "frm_comparation_meters"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_comparation_meters"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_options.ResumeLayout(False)
    Me.gb_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_options As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_detail_filter As System.Windows.Forms.Label
  Friend WithEvents chk_show_imbalance As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_terminal_location As System.Windows.Forms.CheckBox
End Class
