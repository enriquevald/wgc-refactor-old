'-------------------------------------------------------------------
' Copyright � 2016 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bank_reconciliation_user
' DESCRIPTION:   Information various from the movements of account and session playing 
' AUTHOR:        Luis Tenorio
' CREATION DATE: 05-AGO-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-AGO-2016  LTC    Initial version. Product Backlog Item 15199:TPV Televisa: GUI, Banking cards reconciliation
' 23-AGO-2016  LTC    Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
' 03-NOV-2016  AMF    Bug 20019:WigosGUI: Formato incorrecto de la ventana de selecci�n de usuario "Conciliaci�n de tarjetas bancarias"
' 07-FEB-2018  DHA    Bug 31436:WIGOS-8001 [Ticket #12150] Todos: Error Reporte de Sesi�n de Caja Tarjeta Bancaria/PinPad
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports WSI.Common.PinPad
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Text

Public Class frm_bank_reconciliation_user
  Inherits frm_base

#Region "Constants"

  Private Const DATATABLE_DRAWAL_ID As Integer = 0
  Private Const DATATABLE_DRAWAL_LABEL As Integer = 1
  Private Const DATATABLE_DRAWAL_DATETIME As Integer = 2
  Private Const DATATABLE_USERNAME As Integer = 3
  Private Const DATATABLE_USER_ID As Integer = 4
  Private Const DATATABLE_CASHIER_SESSION_ID As Integer = 5

#End Region

#Region " Enums "
#End Region

#Region " Members "

  Dim m_withdrawal_movements As DataTable
#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BANKING_RECONCILIATION

    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7490)

    ' - Buttons
    Me.btn_accept.Text = GLB_NLS_GUI_CONTROLS.GetString(568)
    Me.btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' - Frames
    Me.fra_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)

    ' Combo
    Call Fill_Combo_users(Me.cmb_users)

  End Sub

#End Region

#Region " Events "

  Private Sub btn_accept_Click() Handles btn_accept.ClickEvent

    Dim frm As frm_banking_reconciliation
    Dim _row As DataRow()
    Dim _user_id As Int32
    Dim _user_name As String
    Dim _withdrawal As String
    Dim _cashier_session As Int64

    ' 23-AGO-2016  LTC
    If Me.cmb_users.Count > 0 And Not m_withdrawal_movements Is Nothing Then

      _row = m_withdrawal_movements.Select("DRAWAL_ID = " & Me.cmb_users.Value)

      If _row.Length > 0 AndAlso Not String.IsNullOrEmpty(_row(0)(DATATABLE_USER_ID)) AndAlso Not String.IsNullOrEmpty(_row(0)(DATATABLE_USERNAME)) _
        AndAlso Not String.IsNullOrEmpty(_row(0)(DATATABLE_DRAWAL_DATETIME)) AndAlso Not String.IsNullOrEmpty(_row(0)(DATATABLE_CASHIER_SESSION_ID)) Then

        _user_id = _row(0)(DATATABLE_USER_ID)
        _user_name = _row(0)(DATATABLE_USERNAME)
        _withdrawal = _row(0)(DATATABLE_DRAWAL_DATETIME)
        _cashier_session = _row(0)(DATATABLE_CASHIER_SESSION_ID)

        frm = New frm_banking_reconciliation()

        Call frm.ShowForEdit(_user_id, _user_name, _withdrawal, _cashier_session, Me)
        Call Fill_Combo_users(Me.cmb_users)

      End If

    End If

  End Sub

  Private Sub btn_exit_Click() Handles btn_exit.ClickEvent
    Me.Close()
  End Sub

#End Region

#Region "Public Functions"

  ' PURPOSE: Constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub

#End Region

#Region " Private Functions "

  ' PURPOSE : Fill the combo box of levels
  '
  '  PARAMS:
  '     - INPUT:
  '         - Combo: control to be filled
  '     - OUTPUT:
  '
  ' RETURNS :
  Private Sub Fill_Combo_users(ByVal Combo As uc_combo)

    Dim _sb_sql As New StringBuilder

    m_withdrawal_movements = New DataTable

    Combo.Clear()

    ' 23-AGO-2016  LTC
    _sb_sql.AppendLine(" SELECT   ROW_NUMBER() OVER (ORDER BY a.DRAWAL_LABEL) AS DRAWAL_ID ")
    _sb_sql.AppendLine("        , a.DRAWAL_LABEL ")
    _sb_sql.AppendLine("        , a.DRAWAL_DATETIME ")
    _sb_sql.AppendLine("        , a.GU_USERNAME ")
    _sb_sql.AppendLine("        , a.GU_USER_ID ")
    _sb_sql.AppendLine("        , a.AO_CASHIER_SESSION_ID ")
    _sb_sql.AppendLine("  FROM  (")
    _sb_sql.AppendLine("           SELECT  ")
    _sb_sql.AppendLine("                    DISTINCT U.GU_USERNAME + '   |   ' + CONVERT(VARCHAR(20),R.PTC_DRAWAL,112) + ' ' + CONVERT(VARCHAR(20),R.PTC_DRAWAL,108) AS DRAWAL_LABEL")
    _sb_sql.AppendLine("                  , CONVERT(VARCHAR(20),R.PTC_DRAWAL,112) + ' ' + CONVERT(VARCHAR(20),R.PTC_DRAWAL,108) AS DRAWAL_DATETIME ")
    _sb_sql.AppendLine("                  , U.GU_USERNAME             AS GU_USERNAME ")
    _sb_sql.AppendLine("                  , U.GU_USER_ID              AS GU_USER_ID ")
    _sb_sql.AppendLine("                  , A.AO_CASHIER_SESSION_ID   AS AO_CASHIER_SESSION_ID ")
    _sb_sql.AppendLine("             FROM   PINPAD_TRANSACTIONS T ")
    _sb_sql.AppendLine("            INNER JOIN GUI_USERS U ON U.GU_USER_ID = T.PT_USER_ID ")
    _sb_sql.AppendLine("            INNER JOIN ACCOUNT_OPERATIONS A ON A.AO_OPERATION_ID = T.PT_OPERATION_ID ")
    _sb_sql.AppendLine("            INNER JOIN PINPAD_TRANSACTIONS_RECONCILIATION R  ON R.PTC_ID = T.PT_ID ")
    _sb_sql.AppendLine("            WHERE   T.PT_STATUS = " + WSI.Common.PinPad.PinPadTransactions.STATUS.APPROVED.GetHashCode.ToString() + "")
    _sb_sql.AppendLine("                    AND A.AO_CODE IN (" & WSI.Common.OperationCode.CASH_IN.GetHashCode() & "," & WSI.Common.OperationCode.CASH_ADVANCE.GetHashCode() & ") ")
    _sb_sql.AppendLine("                    AND R.PTC_RECONCILIATE = 0  ")
    _sb_sql.AppendLine("                    AND R.PTC_DRAWAL_STATUS = 0  ")
    _sb_sql.AppendLine(" ) a ")


    m_withdrawal_movements = GUI_GetTableUsingSQL(_sb_sql.ToString(), Integer.MaxValue)

    If IsNothing(m_withdrawal_movements) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(80), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Exit Sub
    End If

    Combo.Add(m_withdrawal_movements, DATATABLE_DRAWAL_ID, DATATABLE_DRAWAL_LABEL)

    If m_withdrawal_movements.Rows.Count > 0 Then
      Combo.SelectedIndex = 0
    End If

  End Sub

#End Region


End Class