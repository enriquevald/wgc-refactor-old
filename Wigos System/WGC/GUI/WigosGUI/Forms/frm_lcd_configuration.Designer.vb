<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_lcd_configuration
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_functionalities = New GUI_Controls.uc_grid
    Me.gb_functionalities = New System.Windows.Forms.GroupBox
    Me.gb_lcd_logo = New System.Windows.Forms.GroupBox
    Me.img_preview = New GUI_Controls.uc_image
    Me.lb_img_description = New System.Windows.Forms.Label
    Me.gb_lcd_video = New System.Windows.Forms.GroupBox
    Me.lnk_video_preview = New System.Windows.Forms.LinkLabel
    Me.btn_change_intro_video = New GUI_Controls.uc_button
    Me.panel_data.SuspendLayout()
    Me.gb_functionalities.SuspendLayout()
    Me.gb_lcd_logo.SuspendLayout()
    Me.gb_lcd_video.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.gb_lcd_logo)
    Me.panel_data.Controls.Add(Me.gb_lcd_video)
    Me.panel_data.Controls.Add(Me.gb_functionalities)
    Me.panel_data.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(633, 373)
    '
    'dg_functionalities
    '
    Me.dg_functionalities.CurrentCol = -1
    Me.dg_functionalities.CurrentRow = -1
    Me.dg_functionalities.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_functionalities.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_functionalities.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_functionalities.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_functionalities.Location = New System.Drawing.Point(3, 17)
    Me.dg_functionalities.Name = "dg_functionalities"
    Me.dg_functionalities.PanelRightVisible = False
    Me.dg_functionalities.Redraw = True
    Me.dg_functionalities.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_functionalities.Size = New System.Drawing.Size(298, 353)
    Me.dg_functionalities.Sortable = False
    Me.dg_functionalities.SortAscending = True
    Me.dg_functionalities.SortByCol = 0
    Me.dg_functionalities.TabIndex = 2
    Me.dg_functionalities.ToolTipped = True
    Me.dg_functionalities.TopRow = -2
    '
    'gb_functionalities
    '
    Me.gb_functionalities.Controls.Add(Me.dg_functionalities)
    Me.gb_functionalities.Dock = System.Windows.Forms.DockStyle.Left
    Me.gb_functionalities.Location = New System.Drawing.Point(0, 0)
    Me.gb_functionalities.Name = "gb_functionalities"
    Me.gb_functionalities.Size = New System.Drawing.Size(304, 373)
    Me.gb_functionalities.TabIndex = 3
    Me.gb_functionalities.TabStop = False
    Me.gb_functionalities.Text = "xFunctionalities"
    '
    'gb_lcd_logo
    '
    Me.gb_lcd_logo.Controls.Add(Me.img_preview)
    Me.gb_lcd_logo.Controls.Add(Me.lb_img_description)
    Me.gb_lcd_logo.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_lcd_logo.Location = New System.Drawing.Point(304, 53)
    Me.gb_lcd_logo.Name = "gb_lcd_logo"
    Me.gb_lcd_logo.Size = New System.Drawing.Size(329, 320)
    Me.gb_lcd_logo.TabIndex = 4
    Me.gb_lcd_logo.TabStop = False
    Me.gb_lcd_logo.Text = "xLcdDisplayLogo"
    '
    'img_preview
    '
    Me.img_preview.AutoSize = True
    Me.img_preview.ButtonDeleteEnabled = True
    Me.img_preview.FreeResize = False
    Me.img_preview.Image = Nothing
    Me.img_preview.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_preview.ImageLayout = System.Windows.Forms.ImageLayout.None
    Me.img_preview.ImageName = Nothing
    Me.img_preview.Location = New System.Drawing.Point(12, 33)
    Me.img_preview.Margin = New System.Windows.Forms.Padding(0)
    Me.img_preview.Name = "img_preview"
    Me.img_preview.Size = New System.Drawing.Size(307, 279)
    Me.img_preview.TabIndex = 9
    '
    'lb_img_description
    '
    Me.lb_img_description.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_img_description.ForeColor = System.Drawing.Color.Navy
    Me.lb_img_description.Location = New System.Drawing.Point(9, 17)
    Me.lb_img_description.Name = "lb_img_description"
    Me.lb_img_description.Size = New System.Drawing.Size(310, 16)
    Me.lb_img_description.TabIndex = 8
    Me.lb_img_description.Text = "xSAMPLE"
    '
    'gb_lcd_video
    '
    Me.gb_lcd_video.Controls.Add(Me.lnk_video_preview)
    Me.gb_lcd_video.Controls.Add(Me.btn_change_intro_video)
    Me.gb_lcd_video.Dock = System.Windows.Forms.DockStyle.Top
    Me.gb_lcd_video.Location = New System.Drawing.Point(304, 0)
    Me.gb_lcd_video.Name = "gb_lcd_video"
    Me.gb_lcd_video.Size = New System.Drawing.Size(329, 53)
    Me.gb_lcd_video.TabIndex = 5
    Me.gb_lcd_video.TabStop = False
    Me.gb_lcd_video.Text = "xLcdDisplayVideo"
    '
    'lnk_video_preview
    '
    Me.lnk_video_preview.Location = New System.Drawing.Point(6, 16)
    Me.lnk_video_preview.Name = "lnk_video_preview"
    Me.lnk_video_preview.Size = New System.Drawing.Size(217, 30)
    Me.lnk_video_preview.TabIndex = 4
    Me.lnk_video_preview.TabStop = True
    Me.lnk_video_preview.Text = "xPreview"
    Me.lnk_video_preview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'btn_change_intro_video
    '
    Me.btn_change_intro_video.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_change_intro_video.Enabled = False
    Me.btn_change_intro_video.Location = New System.Drawing.Point(229, 16)
    Me.btn_change_intro_video.Name = "btn_change_intro_video"
    Me.btn_change_intro_video.Size = New System.Drawing.Size(90, 30)
    Me.btn_change_intro_video.TabIndex = 2
    Me.btn_change_intro_video.ToolTipped = False
    Me.btn_change_intro_video.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_lcd_configuration
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(731, 381)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_lcd_configuration"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_lcd_configuration"
    Me.panel_data.ResumeLayout(False)
    Me.gb_functionalities.ResumeLayout(False)
    Me.gb_lcd_logo.ResumeLayout(False)
    Me.gb_lcd_logo.PerformLayout()
    Me.gb_lcd_video.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_functionalities As GUI_Controls.uc_grid
  Friend WithEvents gb_functionalities As System.Windows.Forms.GroupBox
  Friend WithEvents gb_lcd_logo As System.Windows.Forms.GroupBox
  Friend WithEvents lb_img_description As System.Windows.Forms.Label
  Friend WithEvents img_preview As GUI_Controls.uc_image
  Friend WithEvents gb_lcd_video As System.Windows.Forms.GroupBox
  Friend WithEvents btn_change_intro_video As GUI_Controls.uc_button
  Friend WithEvents lnk_video_preview As System.Windows.Forms.LinkLabel
End Class
