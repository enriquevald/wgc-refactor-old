<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_viewer_digitalized_documents
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.panel_grids = New System.Windows.Forms.Panel
    Me.pnl_select_document_image = New System.Windows.Forms.Panel
    Me.lbl_digitized_documents = New System.Windows.Forms.Label
    Me.lb_documents = New System.Windows.Forms.ListBox
    Me.pnl_document_image = New System.Windows.Forms.Panel
    Me.panel_buttons = New System.Windows.Forms.Panel
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.panel_grids.SuspendLayout()
    Me.pnl_select_document_image.SuspendLayout()
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.pnl_select_document_image)
    Me.panel_grids.Controls.Add(Me.pnl_document_image)
    Me.panel_grids.Controls.Add(Me.panel_buttons)
    Me.panel_grids.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_grids.Location = New System.Drawing.Point(5, 4)
    Me.panel_grids.Name = "panel_grids"
    Me.panel_grids.Size = New System.Drawing.Size(695, 598)
    Me.panel_grids.TabIndex = 11
    '
    'pnl_select_document_image
    '
    Me.pnl_select_document_image.Controls.Add(Me.lbl_digitized_documents)
    Me.pnl_select_document_image.Controls.Add(Me.lb_documents)
    Me.pnl_select_document_image.Dock = System.Windows.Forms.DockStyle.Left
    Me.pnl_select_document_image.Location = New System.Drawing.Point(0, 0)
    Me.pnl_select_document_image.Name = "pnl_select_document_image"
    Me.pnl_select_document_image.Size = New System.Drawing.Size(179, 598)
    Me.pnl_select_document_image.TabIndex = 13
    '
    'lbl_digitized_documents
    '
    Me.lbl_digitized_documents.Font = New System.Drawing.Font("Verdana", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_digitized_documents.Location = New System.Drawing.Point(8, 5)
    Me.lbl_digitized_documents.Margin = New System.Windows.Forms.Padding(5)
    Me.lbl_digitized_documents.Name = "lbl_digitized_documents"
    Me.lbl_digitized_documents.Size = New System.Drawing.Size(166, 23)
    Me.lbl_digitized_documents.TabIndex = 12
    Me.lbl_digitized_documents.Text = "xDigitizedDocuments"
    Me.lbl_digitized_documents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lb_documents
    '
    Me.lb_documents.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_documents.FormattingEnabled = True
    Me.lb_documents.ItemHeight = 18
    Me.lb_documents.Location = New System.Drawing.Point(8, 36)
    Me.lb_documents.Name = "lb_documents"
    Me.lb_documents.Size = New System.Drawing.Size(166, 202)
    Me.lb_documents.TabIndex = 11
    '
    'pnl_document_image
    '
    Me.pnl_document_image.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.pnl_document_image.BackColor = System.Drawing.SystemColors.AppWorkspace
    Me.pnl_document_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.pnl_document_image.Location = New System.Drawing.Point(182, 0)
    Me.pnl_document_image.Name = "pnl_document_image"
    Me.pnl_document_image.Size = New System.Drawing.Size(421, 595)
    Me.pnl_document_image.TabIndex = 12
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_cancel)
    Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_buttons.Location = New System.Drawing.Point(607, 0)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(88, 598)
    Me.panel_buttons.TabIndex = 8
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 568)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 9
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_viewer_digitalized_documents
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_cancel
    Me.ClientSize = New System.Drawing.Size(705, 606)
    Me.Controls.Add(Me.panel_grids)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_viewer_digitalized_documents"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_viewer_digitalized_documents"
    Me.TopMost = True
    Me.panel_grids.ResumeLayout(False)
    Me.pnl_select_document_image.ResumeLayout(False)
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Protected WithEvents panel_grids As System.Windows.Forms.Panel
  Protected WithEvents panel_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents lb_documents As System.Windows.Forms.ListBox
  Friend WithEvents pnl_document_image As System.Windows.Forms.Panel
  Friend WithEvents pnl_select_document_image As System.Windows.Forms.Panel
  Friend WithEvents lbl_digitized_documents As System.Windows.Forms.Label
End Class
