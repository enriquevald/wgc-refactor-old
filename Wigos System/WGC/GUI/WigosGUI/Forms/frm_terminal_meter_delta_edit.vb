'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_meter_delta_edit.vb
'
' DESCRIPTION:   This screen allows view, create and edit Meters Delta (dynamic fields)
'
' AUTHOR:        Fernando Jim�nez
'
' CREATION DATE: 30-MAR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-MAR-2015  FJC    Initial version. Item 981
' 16-APR-2015  SGB    Fixed Bug 1102: Not is possible repeat delta name.
'--------------------------------------------------------------------

Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region

Public Class frm_terminal_meter_delta_edit
  Inherits frm_base_edit

#Region " Const"
  Private Const FORM_DB_MIN_VERSION As Short = 264
#End Region

#Region " Members"
  Public m_current_tmd As CLASS_TERMINAL_METER_DELTA.TYPE_TERMINAL_METER_DELTA

#End Region

#Region " Overrides"

  ' PURPOSE: Sets form Id 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_METER_DELTA_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_group_name
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _max_number_dig As Integer

    _max_number_dig = 19

    Call MyBase.GUI_InitControls()


    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)                   ' Aceptar
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(4)              ' Cancelar
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    ' GB's
    Me.gb_cents.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6086)    'Cents
    Me.gb_quantity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6087) 'Quantity

    ' Group Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6063) ' Delta Meters Editor
    Me.ef_group_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6084)
    Me.ef_group_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 100)
    Me.ef_group_name.IsAccessible = True


  End Sub ' GUI_InitControls

  ' PURPOSE: Get data from screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _tmd_item_read As CLASS_TERMINAL_METER_DELTA
    Dim _tmd_item_edited As CLASS_TERMINAL_METER_DELTA
    Dim _ctrl_find() As Control
    Dim _ctrl_uc_entry_field As uc_entry_field
    Dim _value As Int64

    _tmd_item_read = DbReadObject
    _tmd_item_edited = DbEditedObject

    'Description
    _tmd_item_edited.TmdDescription = Me.ef_group_name.TextValue

    'Rest of dynamic values (Big Increments)
    For Each _row As DataRow In _tmd_item_edited.TmdBigIncDataTable.Rows
      _ctrl_find = Nothing

      'Find UcEntryField into Form
      _ctrl_find = Me.Controls.Find(_row("FIELDNAME"), True)

      If Not _ctrl_find Is Nothing AndAlso _
             _ctrl_find.Length > 0 Then

        If TypeOf _ctrl_find(0) Is uc_entry_field Then

          ' If we find control, get value and set into: _tmd_item_edited.TmdBigIncDataTable  
          _ctrl_uc_entry_field = _ctrl_find(0)
          _value = GUI_ParseNumberLong(_ctrl_uc_entry_field.Value)
          If _value > 0 Then
            _row.Item("VALUE") = _value
          End If
        End If
      End If
    Next

  End Sub ' GUI_GetScreenData

  ' PURPOSE: To determine if screen data are ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _tmd_item_read As CLASS_TERMINAL_METER_DELTA
    Dim _tmd_item_edited As CLASS_TERMINAL_METER_DELTA
    Dim _ctrl_find() As Control
    Dim _ctrl_uc_entry_field As uc_entry_field

    _tmd_item_read = DbReadObject
    _tmd_item_edited = DbEditedObject

    ' Group Name
    If Me.ef_group_name.TextValue = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_group_name.Text, 1)
      Me.ef_group_name.Focus()

      Return False
    End If

    ' SGB 16-APR-2015 Fixed Bug 1102: Not is possible repeat delta name.
    If _tmd_item_read.CheckNameAlreadyExists(Me.ef_group_name.TextValue, _tmd_item_read.TmdId) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6212), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , LCase(Me.ef_group_name.Text), 1)
      Me.ef_group_name.Focus()

      Return False
    End If

    'Rest of dynamic values (Big Increments)
    For Each _row As DataRow In _tmd_item_edited.TmdBigIncDataTable.Rows
      _ctrl_find = Nothing

      'Find UcEntryField into Form
      _ctrl_find = Me.Controls.Find(_row("FIELDNAME"), True)
      If Not _ctrl_find Is Nothing AndAlso _
             _ctrl_find.Length > 0 Then

        If TypeOf _ctrl_find(0) Is uc_entry_field Then

          ' If we find control, check value throw message if required
          _ctrl_uc_entry_field = _ctrl_find(0)
          If GUI_ParseNumber(_ctrl_uc_entry_field.TextValue) <= 0 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _ctrl_uc_entry_field.Text, 1)
            _ctrl_uc_entry_field.Focus()

            Return False
          End If
        End If
      End If
    Next

    Return True
  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _tmd_item As CLASS_TERMINAL_METER_DELTA
    Dim _max_point_y As Integer
    Dim _off_set As Integer

    _tmd_item = DbReadObject
    _off_set = 30

    'Set Group Name
    Me.ef_group_name.TextValue = _tmd_item.TmdDescription

    'Create Controls into form and get max height
    _max_point_y = CreateControlsBigInc(_tmd_item)

    'Set Height 
    _max_point_y += _off_set

    'Set Height  to GroupBox
    Me.gb_cents.Height = _max_point_y
    Me.gb_quantity.Height = _max_point_y

    'Set Height
    _max_point_y += _off_set

    'Set Height to Form
    Me.Height = Me.gb_cents.Location.Y + Me.gb_cents.Size.Height + SystemInformation.CaptionHeight + 35

    'Center Screen
    Me.CenterToScreen()

    'Call set form features resolutions when size's form exceeds desktop area
    Call GUI_AutoAdjustResolutionForm(Me, Nothing, Me.panel_data)

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_OK        ' Aceptar cambios
        MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _terminal_delta_meter_class As CLASS_TERMINAL_METER_DELTA
    Dim _nls_param1 As String

    If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation

      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TERMINAL_METER_DELTA
        _terminal_delta_meter_class = DbEditedObject
        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
          _terminal_delta_meter_class.TmdId = -1
          _terminal_delta_meter_class.TmdDescription = ""

        End If

        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            _terminal_delta_meter_class.TmdScreenMode = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            _terminal_delta_meter_class.TmdScreenMode = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_SCREEN_MODE.MODE_EDIT
        End Select

        DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then

          _terminal_delta_meter_class = Me.DbEditedObject
          _nls_param1 = _terminal_delta_meter_class.TmdDescription

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6093), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then

          _terminal_delta_meter_class = Me.DbEditedObject
          _nls_param1 = _terminal_delta_meter_class.TmdDescription

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5256), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK _
           AndAlso Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND Then

          _terminal_delta_meter_class = Me.DbEditedObject
          _nls_param1 = _terminal_delta_meter_class.TmdDescription

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6094), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          _terminal_delta_meter_class = Me.DbEditedObject
        End If

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub ' GUI_DB_Operation

#End Region

#Region " Public Functions"

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(Optional ByVal TmdId As Integer = -1)
    DbObjectId = TmdId
    DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.m_current_tmd = New CLASS_TERMINAL_METER_DELTA.TYPE_TERMINAL_METER_DELTA
    Me.m_current_tmd.tmd_id = TmdId
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.m_current_tmd.tmd_screen_mode = Me.ScreenMode

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowEditItem

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub 'ShowNewItem

#End Region

#Region " Private Functions"

  ' PURPOSE: Create one uc_entry_fields dinamically
  '
  '  PARAMS:
  '     - INPUT:
  '       - NameFieldDB:    Name of the field in the DB
  '       - NameTextField:  Name of the "literal" text for the uc_entry_field
  '       - Group:          Group of the uc_entry_field (Cents or Quantity)
  '       - Order:          Order of the uc_entry_field
  '       - Value:          Value of the uc_entry_field (String.empty if new_mode or DB_value if edit_mode)
  '     - OUTPUT:
  '
  ' RETURNS:
  '       - Last pos_y for the last control created
  '
  Private Function AddUcEntryField(ByVal NameFieldDB As String, _
                                   ByVal NameTextField As String, _
                                   ByVal Group As CLASS_TERMINAL_METER_DELTA.ENUM_TERMINAL_METER_DELTA_GROUP, _
                                   ByVal Order As Integer, _
                                   ByVal Value As String) As Integer

    Dim _uc_field_entry As uc_entry_field
    Dim _off_set_y As Integer
    Dim _height_ctrl As Integer
    Try

      _off_set_y = 20
      _height_ctrl = 24
      _uc_field_entry = New uc_entry_field

      _uc_field_entry.Text = NameTextField
      _uc_field_entry.Name = NameFieldDB
      _uc_field_entry.Location = New Point(6, (Order * _height_ctrl) + _off_set_y)
      _uc_field_entry.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 18)

      If GUI_ParseNumber(Value) > 0 Then
        _uc_field_entry.TextValue = GUI_FormatNumber(Value, 0)
      Else
        _uc_field_entry.TextValue = String.Empty
      End If

      Select Case Group
        Case CLASS_TERMINAL_METER_DELTA.ENUM_TERMINAL_METER_DELTA_GROUP.GROUP_CENTS
          ' CENTS

          _uc_field_entry.Size = New Size(289, 24)
          _uc_field_entry.TextWidth = 120
          Me.gb_cents.Controls.Add(_uc_field_entry)
        Case CLASS_TERMINAL_METER_DELTA.ENUM_TERMINAL_METER_DELTA_GROUP.GROUP_QUANTITY
          ' QUANTITY

          _uc_field_entry.Size = New Size(309, 24)
          _uc_field_entry.TextWidth = 140
          Me.gb_quantity.Controls.Add(_uc_field_entry)
      End Select

      Return (Order * _height_ctrl) + _off_set_y

    Catch _ex As Exception
      Log.Exception(_ex)

      Return 0
    End Try

  End Function

  ' PURPOSE: Create all uc_entry_fields dinamically depending of DataTable 
  '          Input parameter TmdItem, has all relation of uc_entryfields in a Datatable
  '  PARAMS:
  '     - INPUT:
  '       - TmdItem:    CLASS_TERMINAL_METER_DELTA
  '     - OUTPUT:
  '
  ' RETURNS:
  '       - Last pos_y for the last control created
  '
  Private Function CreateControlsBigInc(ByVal TmdItem As CLASS_TERMINAL_METER_DELTA) As Integer
    Dim _max_heigth As Integer
    Dim _max_heigth_aux As Integer
    Dim _norepeat As List(Of String)

    Try

      _max_heigth = 0
      _max_heigth_aux = 0

      _norepeat = New List(Of String)
      For Each _dt_row As DataRow In TmdItem.TmdBigIncDataTable.Rows
        If Not _norepeat.Contains(_dt_row("FIELDNAME").ToString().ToUpper()) Then

          'Read Fields
          _max_heigth_aux = AddUcEntryField(_dt_row("FIELDNAME").ToString, _
                                            _dt_row("NAMETEXT").ToString, _
                                            _dt_row("GROUP"), _
                                            _dt_row("ORDER"), _
                                            _dt_row("VALUE").ToString)

          If _max_heigth_aux > _max_heigth Then
            _max_heigth = _max_heigth_aux
          End If
          _norepeat.Add(_dt_row("FIELDNAME").ToString().ToUpper())
        Else
          Log.Message("duplicate meter delta sent to database, ignored")
        End If
      Next

      Return _max_heigth

    Catch _ex As Exception
      Log.Exception(_ex)

      Return 0
    End Try
  End Function

#End Region

End Class