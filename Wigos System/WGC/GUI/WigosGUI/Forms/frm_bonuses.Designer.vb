<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bonuses
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_bonuses))
    Me.ef_amount_from = New GUI_Controls.uc_entry_field
    Me.ef_amount_to = New GUI_Controls.uc_entry_field
    Me.gb_amount = New System.Windows.Forms.GroupBox
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.btn_uncheck_all = New System.Windows.Forms.Button
    Me.dg_status = New GUI_Controls.uc_grid
    Me.btn_check_all = New System.Windows.Forms.Button
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_amount.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.gb_amount)
    Me.panel_filter.Controls.Add(Me.gb_date)
    resources.ApplyResources(Me.panel_filter, "panel_filter")
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_amount, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    '
    'panel_data
    '
    resources.ApplyResources(Me.panel_data, "panel_data")
    '
    'pn_separator_line
    '
    resources.ApplyResources(Me.pn_separator_line, "pn_separator_line")
    '
    'pn_line
    '
    resources.ApplyResources(Me.pn_line, "pn_line")
    '
    'ef_amount_from
    '
    Me.ef_amount_from.DoubleValue = 0
    Me.ef_amount_from.IntegerValue = 0
    Me.ef_amount_from.IsReadOnly = False
    resources.ApplyResources(Me.ef_amount_from, "ef_amount_from")
    Me.ef_amount_from.Name = "ef_amount_from"
    Me.ef_amount_from.SufixText = "Sufix Text"
    Me.ef_amount_from.SufixTextVisible = True
    Me.ef_amount_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount_from.TextValue = ""
    Me.ef_amount_from.Value = ""
    '
    'ef_amount_to
    '
    Me.ef_amount_to.DoubleValue = 0
    Me.ef_amount_to.IntegerValue = 0
    Me.ef_amount_to.IsReadOnly = False
    resources.ApplyResources(Me.ef_amount_to, "ef_amount_to")
    Me.ef_amount_to.Name = "ef_amount_to"
    Me.ef_amount_to.SufixText = "Sufix Text"
    Me.ef_amount_to.SufixTextVisible = True
    Me.ef_amount_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_amount_to.TextValue = ""
    Me.ef_amount_to.Value = ""
    '
    'gb_amount
    '
    Me.gb_amount.Controls.Add(Me.ef_amount_from)
    Me.gb_amount.Controls.Add(Me.ef_amount_to)
    resources.ApplyResources(Me.gb_amount, "gb_amount")
    Me.gb_amount.Name = "gb_amount"
    Me.gb_amount.TabStop = False
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.btn_uncheck_all)
    Me.gb_status.Controls.Add(Me.dg_status)
    Me.gb_status.Controls.Add(Me.btn_check_all)
    resources.ApplyResources(Me.gb_status, "gb_status")
    Me.gb_status.Name = "gb_status"
    Me.gb_status.TabStop = False
    '
    'btn_uncheck_all
    '
    resources.ApplyResources(Me.btn_uncheck_all, "btn_uncheck_all")
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.UseVisualStyleBackColor = True
    '
    'dg_status
    '
    Me.dg_status.CurrentCol = -1
    Me.dg_status.CurrentRow = -1
    Me.dg_status.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_status.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_status.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    resources.ApplyResources(Me.dg_status, "dg_status")
    Me.dg_status.Name = "dg_status"
    Me.dg_status.PanelRightVisible = False
    Me.dg_status.Redraw = True
    Me.dg_status.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_status.Sortable = False
    Me.dg_status.SortAscending = True
    Me.dg_status.SortByCol = 0
    Me.dg_status.ToolTipped = True
    Me.dg_status.TopRow = -2
    '
    'btn_check_all
    '
    resources.ApplyResources(Me.btn_check_all, "btn_check_all")
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    resources.ApplyResources(Me.gb_date, "gb_date")
    Me.gb_date.Name = "gb_date"
    Me.gb_date.TabStop = False
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    resources.ApplyResources(Me.dtp_to, "dtp_to")
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    resources.ApplyResources(Me.dtp_from, "dtp_from")
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_pr_list
    '
    resources.ApplyResources(Me.uc_pr_list, "uc_pr_list")
    Me.uc_pr_list.Name = "uc_pr_list"
    '
    'chk_terminal_location
    '
    resources.ApplyResources(Me.chk_terminal_location, "chk_terminal_location")
    Me.chk_terminal_location.Name = "chk_terminal_location"
    '
    'frm_bonuses
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    resources.ApplyResources(Me, "$this")
    Me.Name = "frm_bonuses"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_amount.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_amount_to As GUI_Controls.uc_entry_field
  Friend WithEvents ef_amount_from As GUI_Controls.uc_entry_field
  Friend WithEvents gb_amount As System.Windows.Forms.GroupBox
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents dg_status As GUI_Controls.uc_grid
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Public WithEvents btn_uncheck_all As System.Windows.Forms.Button
  Public WithEvents btn_check_all As System.Windows.Forms.Button
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
End Class
