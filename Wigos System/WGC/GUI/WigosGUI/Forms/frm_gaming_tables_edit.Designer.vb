<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_gaming_tables_edit))
    Me.gb_area_island = New System.Windows.Forms.GroupBox()
    Me.cmb_area = New GUI_Controls.uc_combo()
    Me.ef_floor_id = New GUI_Controls.uc_entry_field()
    Me.cmb_bank = New GUI_Controls.uc_combo()
    Me.ef_smoking = New GUI_Controls.uc_entry_field()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_gaming_table = New System.Windows.Forms.GroupBox()
    Me.chk_management_drop_box = New System.Windows.Forms.CheckBox()
    Me.ef_theoric_hold = New GUI_Controls.uc_entry_field()
    Me.cmb_provider = New GUI_Controls.uc_combo()
    Me.lbl_status = New System.Windows.Forms.Label()
    Me.lbl_session_status = New System.Windows.Forms.Label()
    Me.ef_table_num_seats = New GUI_Controls.uc_entry_field()
    Me.cmb_table_type = New GUI_Controls.uc_combo()
    Me.txt_name = New GUI_Controls.uc_entry_field()
    Me.txt_code = New GUI_Controls.uc_entry_field()
    Me.ef_table_speed_slow = New GUI_Controls.uc_entry_field()
    Me.cmb_cashiers = New System.Windows.Forms.ComboBox()
    Me.gb_cashier_terminal = New System.Windows.Forms.GroupBox()
    Me.gb_table_bet = New System.Windows.Forms.GroupBox()
    Me.tab_currencies = New System.Windows.Forms.TabControl()
    Me.gb_table_speed = New System.Windows.Forms.GroupBox()
    Me.lbl_speed_fast = New System.Windows.Forms.Label()
    Me.lbl_speed_info = New System.Windows.Forms.Label()
    Me.lbl_speed_time = New System.Windows.Forms.Label()
    Me.lbl_speed_normal = New System.Windows.Forms.Label()
    Me.lbl_speed_slow = New System.Windows.Forms.Label()
    Me.ef_table_speed_fast = New GUI_Controls.uc_entry_field()
    Me.ef_table_speed_normal = New GUI_Controls.uc_entry_field()
    Me.gb_table_idle = New System.Windows.Forms.GroupBox()
    Me.lbl_idle_info = New System.Windows.Forms.Label()
    Me.ef_table_idle_plays = New GUI_Controls.uc_entry_field()
    Me.lbl_idle_plays = New System.Windows.Forms.Label()
    Me.tab_chipsSets = New System.Windows.Forms.TabControl()
    Me.gb_chips_config = New System.Windows.Forms.GroupBox()
    Me.lbl_info_tab_chips = New System.Windows.Forms.Label()
    Me.gb_operational_type = New System.Windows.Forms.GroupBox()
    Me.cmb_operation_type = New GUI_Controls.uc_combo()
    Me.chk_operation_type = New System.Windows.Forms.CheckBox()
    Me.panel_table_design = New WSI.Common.GamingTablePanel()
    Me.gb_description = New System.Windows.Forms.GroupBox()
    Me.txt_description = New System.Windows.Forms.TextBox()
    Me.lbl_linked = New System.Windows.Forms.Label()
    Me.chk_integrated = New System.Windows.Forms.CheckBox()
    Me.panel_data.SuspendLayout()
    Me.gb_area_island.SuspendLayout()
    Me.gb_gaming_table.SuspendLayout()
    Me.gb_cashier_terminal.SuspendLayout()
    Me.gb_table_bet.SuspendLayout()
    Me.gb_table_speed.SuspendLayout()
    Me.gb_table_idle.SuspendLayout()
    Me.gb_chips_config.SuspendLayout()
    Me.gb_operational_type.SuspendLayout()
    Me.gb_description.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_description)
    Me.panel_data.Controls.Add(Me.gb_operational_type)
    Me.panel_data.Controls.Add(Me.gb_chips_config)
    Me.panel_data.Controls.Add(Me.gb_table_idle)
    Me.panel_data.Controls.Add(Me.panel_table_design)
    Me.panel_data.Controls.Add(Me.gb_table_speed)
    Me.panel_data.Controls.Add(Me.gb_table_bet)
    Me.panel_data.Controls.Add(Me.gb_gaming_table)
    Me.panel_data.Controls.Add(Me.gb_cashier_terminal)
    Me.panel_data.Controls.Add(Me.gb_area_island)
    Me.panel_data.Size = New System.Drawing.Size(716, 716)
    '
    'gb_area_island
    '
    Me.gb_area_island.Controls.Add(Me.cmb_area)
    Me.gb_area_island.Controls.Add(Me.ef_floor_id)
    Me.gb_area_island.Controls.Add(Me.cmb_bank)
    Me.gb_area_island.Controls.Add(Me.ef_smoking)
    Me.gb_area_island.Location = New System.Drawing.Point(352, 3)
    Me.gb_area_island.Name = "gb_area_island"
    Me.gb_area_island.Size = New System.Drawing.Size(361, 131)
    Me.gb_area_island.TabIndex = 4
    Me.gb_area_island.TabStop = False
    Me.gb_area_island.Text = "xAreaIsland"
    '
    'cmb_area
    '
    Me.cmb_area.AllowUnlistedValues = False
    Me.cmb_area.AutoCompleteMode = False
    Me.cmb_area.IsReadOnly = False
    Me.cmb_area.Location = New System.Drawing.Point(11, 12)
    Me.cmb_area.Name = "cmb_area"
    Me.cmb_area.SelectedIndex = -1
    Me.cmb_area.Size = New System.Drawing.Size(327, 24)
    Me.cmb_area.SufixText = "Sufix Text"
    Me.cmb_area.SufixTextVisible = True
    Me.cmb_area.TabIndex = 0
    Me.cmb_area.TextCombo = Nothing
    Me.cmb_area.TextWidth = 120
    '
    'ef_floor_id
    '
    Me.ef_floor_id.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.ef_floor_id.DoubleValue = 0.0R
    Me.ef_floor_id.IntegerValue = 0
    Me.ef_floor_id.IsReadOnly = False
    Me.ef_floor_id.Location = New System.Drawing.Point(11, 100)
    Me.ef_floor_id.Name = "ef_floor_id"
    Me.ef_floor_id.PlaceHolder = Nothing
    Me.ef_floor_id.Size = New System.Drawing.Size(327, 24)
    Me.ef_floor_id.SufixText = "Sufix Text"
    Me.ef_floor_id.SufixTextVisible = True
    Me.ef_floor_id.TabIndex = 3
    Me.ef_floor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_floor_id.TextValue = ""
    Me.ef_floor_id.TextWidth = 120
    Me.ef_floor_id.Value = ""
    Me.ef_floor_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_bank
    '
    Me.cmb_bank.AllowUnlistedValues = False
    Me.cmb_bank.AutoCompleteMode = False
    Me.cmb_bank.IsReadOnly = False
    Me.cmb_bank.Location = New System.Drawing.Point(11, 70)
    Me.cmb_bank.Name = "cmb_bank"
    Me.cmb_bank.SelectedIndex = -1
    Me.cmb_bank.Size = New System.Drawing.Size(327, 24)
    Me.cmb_bank.SufixText = "Sufix Text"
    Me.cmb_bank.SufixTextVisible = True
    Me.cmb_bank.TabIndex = 2
    Me.cmb_bank.TextCombo = Nothing
    Me.cmb_bank.TextWidth = 120
    '
    'ef_smoking
    '
    Me.ef_smoking.DoubleValue = 0.0R
    Me.ef_smoking.IntegerValue = 0
    Me.ef_smoking.IsReadOnly = False
    Me.ef_smoking.Location = New System.Drawing.Point(11, 40)
    Me.ef_smoking.Name = "ef_smoking"
    Me.ef_smoking.PlaceHolder = Nothing
    Me.ef_smoking.Size = New System.Drawing.Size(327, 24)
    Me.ef_smoking.SufixText = "Sufix Text"
    Me.ef_smoking.SufixTextVisible = True
    Me.ef_smoking.TabIndex = 1
    Me.ef_smoking.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_smoking.TextValue = ""
    Me.ef_smoking.TextWidth = 120
    Me.ef_smoking.Value = ""
    Me.ef_smoking.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Checked = True
    Me.chk_enabled.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_enabled.Location = New System.Drawing.Point(129, 103)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 3
    Me.chk_enabled.Text = "xEnabled"
    '
    'gb_gaming_table
    '
    Me.gb_gaming_table.Controls.Add(Me.chk_management_drop_box)
    Me.gb_gaming_table.Controls.Add(Me.ef_theoric_hold)
    Me.gb_gaming_table.Controls.Add(Me.cmb_provider)
    Me.gb_gaming_table.Controls.Add(Me.lbl_status)
    Me.gb_gaming_table.Controls.Add(Me.lbl_session_status)
    Me.gb_gaming_table.Controls.Add(Me.ef_table_num_seats)
    Me.gb_gaming_table.Controls.Add(Me.chk_enabled)
    Me.gb_gaming_table.Controls.Add(Me.cmb_table_type)
    Me.gb_gaming_table.Controls.Add(Me.txt_name)
    Me.gb_gaming_table.Controls.Add(Me.txt_code)
    Me.gb_gaming_table.Location = New System.Drawing.Point(18, 3)
    Me.gb_gaming_table.Name = "gb_gaming_table"
    Me.gb_gaming_table.Size = New System.Drawing.Size(324, 253)
    Me.gb_gaming_table.TabIndex = 0
    Me.gb_gaming_table.TabStop = False
    Me.gb_gaming_table.Text = "xGamingTable"
    '
    'chk_management_drop_box
    '
    Me.chk_management_drop_box.AutoSize = True
    Me.chk_management_drop_box.Checked = True
    Me.chk_management_drop_box.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_management_drop_box.Location = New System.Drawing.Point(129, 228)
    Me.chk_management_drop_box.Name = "chk_management_drop_box"
    Me.chk_management_drop_box.Size = New System.Drawing.Size(155, 17)
    Me.chk_management_drop_box.TabIndex = 22
    Me.chk_management_drop_box.Text = "xManagementDropbox"
    '
    'ef_theoric_hold
    '
    Me.ef_theoric_hold.DoubleValue = 0.0R
    Me.ef_theoric_hold.IntegerValue = 0
    Me.ef_theoric_hold.IsReadOnly = False
    Me.ef_theoric_hold.Location = New System.Drawing.Point(6, 199)
    Me.ef_theoric_hold.Name = "ef_theoric_hold"
    Me.ef_theoric_hold.PlaceHolder = Nothing
    Me.ef_theoric_hold.Size = New System.Drawing.Size(312, 24)
    Me.ef_theoric_hold.SufixText = "Sufix Text"
    Me.ef_theoric_hold.SufixTextVisible = True
    Me.ef_theoric_hold.TabIndex = 7
    Me.ef_theoric_hold.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoric_hold.TextValue = ""
    Me.ef_theoric_hold.TextWidth = 120
    Me.ef_theoric_hold.Value = ""
    Me.ef_theoric_hold.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_provider
    '
    Me.cmb_provider.AllowUnlistedValues = False
    Me.cmb_provider.AutoCompleteMode = False
    Me.cmb_provider.IsReadOnly = False
    Me.cmb_provider.Location = New System.Drawing.Point(6, 171)
    Me.cmb_provider.Name = "cmb_provider"
    Me.cmb_provider.SelectedIndex = -1
    Me.cmb_provider.Size = New System.Drawing.Size(312, 24)
    Me.cmb_provider.SufixText = "Sufix Text"
    Me.cmb_provider.SufixTextVisible = True
    Me.cmb_provider.TabIndex = 6
    Me.cmb_provider.TextCombo = Nothing
    Me.cmb_provider.TextWidth = 120
    '
    'lbl_status
    '
    Me.lbl_status.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_status.Location = New System.Drawing.Point(126, 125)
    Me.lbl_status.Name = "lbl_status"
    Me.lbl_status.Size = New System.Drawing.Size(117, 18)
    Me.lbl_status.TabIndex = 4
    Me.lbl_status.Text = "xStatus"
    '
    'lbl_session_status
    '
    Me.lbl_session_status.Location = New System.Drawing.Point(8, 125)
    Me.lbl_session_status.Name = "lbl_session_status"
    Me.lbl_session_status.Size = New System.Drawing.Size(117, 18)
    Me.lbl_session_status.TabIndex = 21
    Me.lbl_session_status.Text = "xSessionStatus"
    Me.lbl_session_status.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'ef_table_num_seats
    '
    Me.ef_table_num_seats.DoubleValue = 0.0R
    Me.ef_table_num_seats.IntegerValue = 0
    Me.ef_table_num_seats.IsReadOnly = True
    Me.ef_table_num_seats.Location = New System.Drawing.Point(6, 144)
    Me.ef_table_num_seats.Name = "ef_table_num_seats"
    Me.ef_table_num_seats.PlaceHolder = Nothing
    Me.ef_table_num_seats.Size = New System.Drawing.Size(212, 24)
    Me.ef_table_num_seats.SufixText = "Sufix Text"
    Me.ef_table_num_seats.SufixTextVisible = True
    Me.ef_table_num_seats.TabIndex = 5
    Me.ef_table_num_seats.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_table_num_seats.TextValue = ""
    Me.ef_table_num_seats.TextWidth = 120
    Me.ef_table_num_seats.Value = ""
    Me.ef_table_num_seats.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_table_type
    '
    Me.cmb_table_type.AllowUnlistedValues = False
    Me.cmb_table_type.AutoCompleteMode = False
    Me.cmb_table_type.IsReadOnly = False
    Me.cmb_table_type.Location = New System.Drawing.Point(6, 74)
    Me.cmb_table_type.Name = "cmb_table_type"
    Me.cmb_table_type.SelectedIndex = -1
    Me.cmb_table_type.Size = New System.Drawing.Size(312, 24)
    Me.cmb_table_type.SufixText = "Sufix Text"
    Me.cmb_table_type.SufixTextVisible = True
    Me.cmb_table_type.TabIndex = 2
    Me.cmb_table_type.TextCombo = Nothing
    Me.cmb_table_type.TextWidth = 120
    '
    'txt_name
    '
    Me.txt_name.DoubleValue = 0.0R
    Me.txt_name.IntegerValue = 0
    Me.txt_name.IsReadOnly = False
    Me.txt_name.Location = New System.Drawing.Point(6, 45)
    Me.txt_name.Name = "txt_name"
    Me.txt_name.PlaceHolder = Nothing
    Me.txt_name.Size = New System.Drawing.Size(312, 24)
    Me.txt_name.SufixText = "Sufix Text"
    Me.txt_name.SufixTextVisible = True
    Me.txt_name.TabIndex = 1
    Me.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_name.TextValue = ""
    Me.txt_name.TextWidth = 120
    Me.txt_name.Value = ""
    Me.txt_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'txt_code
    '
    Me.txt_code.DoubleValue = 0.0R
    Me.txt_code.IntegerValue = 0
    Me.txt_code.IsReadOnly = False
    Me.txt_code.Location = New System.Drawing.Point(6, 15)
    Me.txt_code.Name = "txt_code"
    Me.txt_code.PlaceHolder = Nothing
    Me.txt_code.Size = New System.Drawing.Size(312, 24)
    Me.txt_code.SufixText = "Sufix Text"
    Me.txt_code.SufixTextVisible = True
    Me.txt_code.TabIndex = 0
    Me.txt_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.txt_code.TextValue = ""
    Me.txt_code.TextWidth = 120
    Me.txt_code.Value = ""
    Me.txt_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_table_speed_slow
    '
    Me.ef_table_speed_slow.DoubleValue = 0.0R
    Me.ef_table_speed_slow.IntegerValue = 0
    Me.ef_table_speed_slow.IsReadOnly = False
    Me.ef_table_speed_slow.Location = New System.Drawing.Point(10, 27)
    Me.ef_table_speed_slow.Name = "ef_table_speed_slow"
    Me.ef_table_speed_slow.PlaceHolder = Nothing
    Me.ef_table_speed_slow.Size = New System.Drawing.Size(68, 24)
    Me.ef_table_speed_slow.SufixText = "Sufix Text"
    Me.ef_table_speed_slow.SufixTextVisible = True
    Me.ef_table_speed_slow.TabIndex = 0
    Me.ef_table_speed_slow.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_table_speed_slow.TextValue = ""
    Me.ef_table_speed_slow.TextWidth = 0
    Me.ef_table_speed_slow.Value = ""
    Me.ef_table_speed_slow.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_cashiers
    '
    Me.cmb_cashiers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_cashiers.FormattingEnabled = True
    Me.cmb_cashiers.Location = New System.Drawing.Point(105, 14)
    Me.cmb_cashiers.Name = "cmb_cashiers"
    Me.cmb_cashiers.Size = New System.Drawing.Size(136, 21)
    Me.cmb_cashiers.TabIndex = 2
    '
    'gb_cashier_terminal
    '
    Me.gb_cashier_terminal.Controls.Add(Me.chk_integrated)
    Me.gb_cashier_terminal.Controls.Add(Me.lbl_linked)
    Me.gb_cashier_terminal.Controls.Add(Me.cmb_cashiers)
    Me.gb_cashier_terminal.Location = New System.Drawing.Point(352, 136)
    Me.gb_cashier_terminal.Name = "gb_cashier_terminal"
    Me.gb_cashier_terminal.Size = New System.Drawing.Size(361, 48)
    Me.gb_cashier_terminal.TabIndex = 5
    Me.gb_cashier_terminal.TabStop = False
    Me.gb_cashier_terminal.Text = "xCashierTerminal"
    '
    'gb_table_bet
    '
    Me.gb_table_bet.Controls.Add(Me.tab_currencies)
    Me.gb_table_bet.Location = New System.Drawing.Point(352, 187)
    Me.gb_table_bet.Name = "gb_table_bet"
    Me.gb_table_bet.Size = New System.Drawing.Size(361, 120)
    Me.gb_table_bet.TabIndex = 6
    Me.gb_table_bet.TabStop = False
    Me.gb_table_bet.Text = "xTableBet"
    '
    'tab_currencies
    '
    Me.tab_currencies.Location = New System.Drawing.Point(6, 17)
    Me.tab_currencies.Name = "tab_currencies"
    Me.tab_currencies.SelectedIndex = 0
    Me.tab_currencies.Size = New System.Drawing.Size(348, 96)
    Me.tab_currencies.TabIndex = 0
    '
    'gb_table_speed
    '
    Me.gb_table_speed.Controls.Add(Me.lbl_speed_fast)
    Me.gb_table_speed.Controls.Add(Me.lbl_speed_info)
    Me.gb_table_speed.Controls.Add(Me.lbl_speed_time)
    Me.gb_table_speed.Controls.Add(Me.lbl_speed_normal)
    Me.gb_table_speed.Controls.Add(Me.lbl_speed_slow)
    Me.gb_table_speed.Controls.Add(Me.ef_table_speed_fast)
    Me.gb_table_speed.Controls.Add(Me.ef_table_speed_normal)
    Me.gb_table_speed.Controls.Add(Me.ef_table_speed_slow)
    Me.gb_table_speed.Location = New System.Drawing.Point(18, 336)
    Me.gb_table_speed.Name = "gb_table_speed"
    Me.gb_table_speed.Size = New System.Drawing.Size(324, 80)
    Me.gb_table_speed.TabIndex = 2
    Me.gb_table_speed.TabStop = False
    Me.gb_table_speed.Text = "xTableSpeed"
    '
    'lbl_speed_fast
    '
    Me.lbl_speed_fast.Location = New System.Drawing.Point(166, 11)
    Me.lbl_speed_fast.Name = "lbl_speed_fast"
    Me.lbl_speed_fast.Size = New System.Drawing.Size(75, 18)
    Me.lbl_speed_fast.TabIndex = 33
    Me.lbl_speed_fast.Text = "xSpeedFast"
    Me.lbl_speed_fast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_speed_info
    '
    Me.lbl_speed_info.AutoSize = True
    Me.lbl_speed_info.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_speed_info.Location = New System.Drawing.Point(14, 56)
    Me.lbl_speed_info.Name = "lbl_speed_info"
    Me.lbl_speed_info.Size = New System.Drawing.Size(73, 13)
    Me.lbl_speed_info.TabIndex = 32
    Me.lbl_speed_info.Text = "xSpeedInfo"
    Me.lbl_speed_info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_speed_time
    '
    Me.lbl_speed_time.Location = New System.Drawing.Point(239, 30)
    Me.lbl_speed_time.Name = "lbl_speed_time"
    Me.lbl_speed_time.Size = New System.Drawing.Size(80, 18)
    Me.lbl_speed_time.TabIndex = 31
    Me.lbl_speed_time.Text = "xSpeedTime"
    '
    'lbl_speed_normal
    '
    Me.lbl_speed_normal.Location = New System.Drawing.Point(88, 11)
    Me.lbl_speed_normal.Name = "lbl_speed_normal"
    Me.lbl_speed_normal.Size = New System.Drawing.Size(75, 18)
    Me.lbl_speed_normal.TabIndex = 28
    Me.lbl_speed_normal.Text = "xSpeedNormal"
    Me.lbl_speed_normal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_speed_slow
    '
    Me.lbl_speed_slow.Location = New System.Drawing.Point(8, 11)
    Me.lbl_speed_slow.Name = "lbl_speed_slow"
    Me.lbl_speed_slow.Size = New System.Drawing.Size(75, 18)
    Me.lbl_speed_slow.TabIndex = 27
    Me.lbl_speed_slow.Text = "xSpeedSlow"
    Me.lbl_speed_slow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'ef_table_speed_fast
    '
    Me.ef_table_speed_fast.DoubleValue = 0.0R
    Me.ef_table_speed_fast.IntegerValue = 0
    Me.ef_table_speed_fast.IsReadOnly = False
    Me.ef_table_speed_fast.Location = New System.Drawing.Point(169, 27)
    Me.ef_table_speed_fast.Name = "ef_table_speed_fast"
    Me.ef_table_speed_fast.PlaceHolder = Nothing
    Me.ef_table_speed_fast.Size = New System.Drawing.Size(68, 24)
    Me.ef_table_speed_fast.SufixText = "Sufix Text"
    Me.ef_table_speed_fast.SufixTextVisible = True
    Me.ef_table_speed_fast.TabIndex = 2
    Me.ef_table_speed_fast.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_table_speed_fast.TextValue = ""
    Me.ef_table_speed_fast.TextVisible = False
    Me.ef_table_speed_fast.TextWidth = 0
    Me.ef_table_speed_fast.Value = ""
    Me.ef_table_speed_fast.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_table_speed_normal
    '
    Me.ef_table_speed_normal.DoubleValue = 0.0R
    Me.ef_table_speed_normal.IntegerValue = 0
    Me.ef_table_speed_normal.IsReadOnly = False
    Me.ef_table_speed_normal.Location = New System.Drawing.Point(91, 27)
    Me.ef_table_speed_normal.Name = "ef_table_speed_normal"
    Me.ef_table_speed_normal.PlaceHolder = Nothing
    Me.ef_table_speed_normal.Size = New System.Drawing.Size(68, 24)
    Me.ef_table_speed_normal.SufixText = "Sufix Text"
    Me.ef_table_speed_normal.SufixTextVisible = True
    Me.ef_table_speed_normal.TabIndex = 1
    Me.ef_table_speed_normal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_table_speed_normal.TextValue = ""
    Me.ef_table_speed_normal.TextWidth = 0
    Me.ef_table_speed_normal.Value = ""
    Me.ef_table_speed_normal.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_table_idle
    '
    Me.gb_table_idle.Controls.Add(Me.lbl_idle_info)
    Me.gb_table_idle.Controls.Add(Me.ef_table_idle_plays)
    Me.gb_table_idle.Controls.Add(Me.lbl_idle_plays)
    Me.gb_table_idle.Location = New System.Drawing.Point(18, 256)
    Me.gb_table_idle.Name = "gb_table_idle"
    Me.gb_table_idle.Size = New System.Drawing.Size(324, 80)
    Me.gb_table_idle.TabIndex = 1
    Me.gb_table_idle.TabStop = False
    Me.gb_table_idle.Text = "xTableIdle"
    '
    'lbl_idle_info
    '
    Me.lbl_idle_info.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_idle_info.Location = New System.Drawing.Point(6, 44)
    Me.lbl_idle_info.Name = "lbl_idle_info"
    Me.lbl_idle_info.Size = New System.Drawing.Size(312, 32)
    Me.lbl_idle_info.TabIndex = 1
    Me.lbl_idle_info.Text = "xIdleInfo"
    '
    'ef_table_idle_plays
    '
    Me.ef_table_idle_plays.DoubleValue = 0.0R
    Me.ef_table_idle_plays.IntegerValue = 0
    Me.ef_table_idle_plays.IsReadOnly = False
    Me.ef_table_idle_plays.Location = New System.Drawing.Point(15, 17)
    Me.ef_table_idle_plays.Name = "ef_table_idle_plays"
    Me.ef_table_idle_plays.PlaceHolder = Nothing
    Me.ef_table_idle_plays.Size = New System.Drawing.Size(91, 24)
    Me.ef_table_idle_plays.SufixText = "Sufix Text"
    Me.ef_table_idle_plays.SufixTextVisible = True
    Me.ef_table_idle_plays.TabIndex = 0
    Me.ef_table_idle_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_table_idle_plays.TextValue = ""
    Me.ef_table_idle_plays.TextWidth = 0
    Me.ef_table_idle_plays.Value = ""
    Me.ef_table_idle_plays.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_idle_plays
    '
    Me.lbl_idle_plays.Location = New System.Drawing.Point(112, 20)
    Me.lbl_idle_plays.Name = "lbl_idle_plays"
    Me.lbl_idle_plays.Size = New System.Drawing.Size(80, 18)
    Me.lbl_idle_plays.TabIndex = 1
    Me.lbl_idle_plays.Text = "xIdleInfo"
    Me.lbl_idle_plays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'tab_chipsSets
    '
    Me.tab_chipsSets.Location = New System.Drawing.Point(15, 45)
    Me.tab_chipsSets.Name = "tab_chipsSets"
    Me.tab_chipsSets.SelectedIndex = 0
    Me.tab_chipsSets.Size = New System.Drawing.Size(333, 122)
    Me.tab_chipsSets.TabIndex = 0
    '
    'gb_chips_config
    '
    Me.gb_chips_config.Controls.Add(Me.tab_chipsSets)
    Me.gb_chips_config.Controls.Add(Me.lbl_info_tab_chips)
    Me.gb_chips_config.Location = New System.Drawing.Point(352, 426)
    Me.gb_chips_config.Name = "gb_chips_config"
    Me.gb_chips_config.Size = New System.Drawing.Size(361, 175)
    Me.gb_chips_config.TabIndex = 7
    Me.gb_chips_config.TabStop = False
    Me.gb_chips_config.Text = "xChipsConfig"
    '
    'lbl_info_tab_chips
    '
    Me.lbl_info_tab_chips.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_info_tab_chips.Location = New System.Drawing.Point(8, 17)
    Me.lbl_info_tab_chips.Name = "lbl_info_tab_chips"
    Me.lbl_info_tab_chips.Size = New System.Drawing.Size(333, 33)
    Me.lbl_info_tab_chips.TabIndex = 36
    Me.lbl_info_tab_chips.Text = "xChipsInfo"
    Me.lbl_info_tab_chips.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'gb_operational_type
    '
    Me.gb_operational_type.BackColor = System.Drawing.SystemColors.Control
    Me.gb_operational_type.Controls.Add(Me.cmb_operation_type)
    Me.gb_operational_type.Controls.Add(Me.chk_operation_type)
    Me.gb_operational_type.Location = New System.Drawing.Point(352, 313)
    Me.gb_operational_type.Name = "gb_operational_type"
    Me.gb_operational_type.Size = New System.Drawing.Size(361, 103)
    Me.gb_operational_type.TabIndex = 7
    Me.gb_operational_type.TabStop = False
    Me.gb_operational_type.Text = "xOperationalType"
    '
    'cmb_operation_type
    '
    Me.cmb_operation_type.AllowUnlistedValues = False
    Me.cmb_operation_type.AutoCompleteMode = False
    Me.cmb_operation_type.IsReadOnly = False
    Me.cmb_operation_type.Location = New System.Drawing.Point(7, 34)
    Me.cmb_operation_type.Name = "cmb_operation_type"
    Me.cmb_operation_type.SelectedIndex = -1
    Me.cmb_operation_type.Size = New System.Drawing.Size(157, 24)
    Me.cmb_operation_type.SufixText = "Sufix Text"
    Me.cmb_operation_type.SufixTextVisible = True
    Me.cmb_operation_type.TabIndex = 5
    Me.cmb_operation_type.TextCombo = Nothing
    Me.cmb_operation_type.TextWidth = 40
    '
    'chk_operation_type
    '
    Me.chk_operation_type.AutoSize = True
    Me.chk_operation_type.Location = New System.Drawing.Point(175, 38)
    Me.chk_operation_type.Name = "chk_operation_type"
    Me.chk_operation_type.Size = New System.Drawing.Size(139, 17)
    Me.chk_operation_type.TabIndex = 4
    Me.chk_operation_type.Text = "xDuermen en mesa"
    Me.chk_operation_type.UseVisualStyleBackColor = True
    '
    'panel_table_design
    '
    Me.panel_table_design.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_table_design.AutoScroll = True
    Me.panel_table_design.BackColor = System.Drawing.SystemColors.Control
    Me.panel_table_design.BackgroundPicture = Nothing
    Me.panel_table_design.CanMoveSeats = False
    Me.panel_table_design.DoubleBufferred = True
    Me.panel_table_design.Enabled = False
    Me.panel_table_design.Location = New System.Drawing.Point(19, 426)
    Me.panel_table_design.ModeDesign = False
    Me.panel_table_design.Name = "panel_table_design"
    Me.panel_table_design.ReadOnly = False
    Me.panel_table_design.Size = New System.Drawing.Size(323, 234)
    Me.panel_table_design.TabIndex = 3
    '
    'gb_description
    '
    Me.gb_description.Controls.Add(Me.txt_description)
    Me.gb_description.Location = New System.Drawing.Point(352, 607)
    Me.gb_description.Name = "gb_description"
    Me.gb_description.Size = New System.Drawing.Size(361, 106)
    Me.gb_description.TabIndex = 8
    Me.gb_description.TabStop = False
    Me.gb_description.Text = "xDescription"
    '
    'txt_description
    '
    Me.txt_description.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_description.Location = New System.Drawing.Point(14, 25)
    Me.txt_description.Multiline = True
    Me.txt_description.Name = "txt_description"
    Me.txt_description.Size = New System.Drawing.Size(333, 75)
    Me.txt_description.TabIndex = 9
    '
    'lbl_linked
    '
    Me.lbl_linked.Location = New System.Drawing.Point(10, 15)
    Me.lbl_linked.Name = "lbl_linked"
    Me.lbl_linked.Size = New System.Drawing.Size(89, 18)
    Me.lbl_linked.TabIndex = 22
    Me.lbl_linked.Text = "xLinked"
    Me.lbl_linked.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'chk_integrated
    '
    Me.chk_integrated.AutoSize = True
    Me.chk_integrated.Location = New System.Drawing.Point(247, 16)
    Me.chk_integrated.Name = "chk_integrated"
    Me.chk_integrated.Size = New System.Drawing.Size(93, 17)
    Me.chk_integrated.TabIndex = 23
    Me.chk_integrated.Text = "xIntegrated"
    Me.chk_integrated.UseVisualStyleBackColor = True
    '
    'frm_gaming_tables_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(818, 724)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_gaming_tables_edit"
    Me.Text = "frm_gaming_tables_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_area_island.ResumeLayout(False)
    Me.gb_gaming_table.ResumeLayout(False)
    Me.gb_gaming_table.PerformLayout()
    Me.gb_cashier_terminal.ResumeLayout(False)
    Me.gb_cashier_terminal.PerformLayout()
    Me.gb_table_bet.ResumeLayout(False)
    Me.gb_table_speed.ResumeLayout(False)
    Me.gb_table_speed.PerformLayout()
    Me.gb_table_idle.ResumeLayout(False)
    Me.gb_chips_config.ResumeLayout(False)
    Me.gb_operational_type.ResumeLayout(False)
    Me.gb_operational_type.PerformLayout()
    Me.gb_description.ResumeLayout(False)
    Me.gb_description.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_area_island As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_area As GUI_Controls.uc_combo
  Friend WithEvents ef_floor_id As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_bank As GUI_Controls.uc_combo
  Friend WithEvents ef_smoking As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_gaming_table As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_table_type As GUI_Controls.uc_combo
  Friend WithEvents txt_name As GUI_Controls.uc_entry_field
  Friend WithEvents txt_code As GUI_Controls.uc_entry_field
  Friend WithEvents gb_cashier_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_cashiers As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_session_status As System.Windows.Forms.Label
  Friend WithEvents lbl_status As System.Windows.Forms.Label
  Friend WithEvents ef_table_num_seats As GUI_Controls.uc_entry_field
  Friend WithEvents ef_table_speed_slow As GUI_Controls.uc_entry_field
  Friend WithEvents gb_table_bet As System.Windows.Forms.GroupBox
  Friend WithEvents gb_table_speed As System.Windows.Forms.GroupBox
  Friend WithEvents ef_table_speed_fast As GUI_Controls.uc_entry_field
  Friend WithEvents ef_table_speed_normal As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_speed_normal As System.Windows.Forms.Label
  Friend WithEvents lbl_speed_slow As System.Windows.Forms.Label
  Friend WithEvents lbl_speed_info As System.Windows.Forms.Label
  Friend WithEvents lbl_speed_time As System.Windows.Forms.Label
  Friend WithEvents cmb_provider As GUI_Controls.uc_combo
  Friend WithEvents panel_table_design As WSI.Common.GamingTablePanel
  Friend WithEvents gb_table_idle As System.Windows.Forms.GroupBox
  Friend WithEvents ef_table_idle_plays As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_idle_plays As System.Windows.Forms.Label
  Friend WithEvents lbl_idle_info As System.Windows.Forms.Label
  Friend WithEvents ef_theoric_hold As GUI_Controls.uc_entry_field
  Friend WithEvents tab_chipsSets As System.Windows.Forms.TabControl
  Friend WithEvents gb_chips_config As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_info_tab_chips As System.Windows.Forms.Label
  Friend WithEvents lbl_speed_fast As System.Windows.Forms.Label
  Friend WithEvents tab_currencies As System.Windows.Forms.TabControl
  Friend WithEvents chk_management_drop_box As System.Windows.Forms.CheckBox
  Friend WithEvents gb_operational_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_operation_type As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_operation_type As GUI_Controls.uc_combo
  Friend WithEvents gb_description As System.Windows.Forms.GroupBox
  Friend WithEvents txt_description As System.Windows.Forms.TextBox
  Friend WithEvents chk_integrated As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_linked As System.Windows.Forms.Label
End Class
