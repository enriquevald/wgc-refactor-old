﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_intellia_filter_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_grid_filters = New GUI_Controls.uc_grid()
    Me.grb_filter_information = New System.Windows.Forms.GroupBox()
    Me.btn_color = New System.Windows.Forms.Button()
    Me.btn_del_range = New System.Windows.Forms.Button()
    Me.btn_add_range = New System.Windows.Forms.Button()
    Me.uc_grid_ranges = New GUI_Controls.uc_grid()
    Me.cd_button_color = New System.Windows.Forms.ColorDialog()
    Me.panel_data.SuspendLayout()
    Me.grb_filter_information.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.grb_filter_information)
    Me.panel_data.Controls.Add(Me.uc_grid_filters)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(669, 668)
    '
    'uc_grid_filters
    '
    Me.uc_grid_filters.CurrentCol = -1
    Me.uc_grid_filters.CurrentRow = -1
    Me.uc_grid_filters.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_filters.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_filters.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_BORDER
    Me.uc_grid_filters.Location = New System.Drawing.Point(13, 15)
    Me.uc_grid_filters.Name = "uc_grid_filters"
    Me.uc_grid_filters.PanelRightVisible = False
    Me.uc_grid_filters.Redraw = True
    Me.uc_grid_filters.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.uc_grid_filters.Size = New System.Drawing.Size(646, 369)
    Me.uc_grid_filters.Sortable = False
    Me.uc_grid_filters.SortAscending = True
    Me.uc_grid_filters.SortByCol = 0
    Me.uc_grid_filters.TabIndex = 0
    Me.uc_grid_filters.ToolTipped = True
    Me.uc_grid_filters.TopRow = -2
    Me.uc_grid_filters.WordWrap = False
    '
    'grb_filter_information
    '
    Me.grb_filter_information.Controls.Add(Me.btn_color)
    Me.grb_filter_information.Controls.Add(Me.btn_del_range)
    Me.grb_filter_information.Controls.Add(Me.btn_add_range)
    Me.grb_filter_information.Controls.Add(Me.uc_grid_ranges)
    Me.grb_filter_information.Location = New System.Drawing.Point(13, 400)
    Me.grb_filter_information.Name = "grb_filter_information"
    Me.grb_filter_information.Size = New System.Drawing.Size(646, 265)
    Me.grb_filter_information.TabIndex = 1
    Me.grb_filter_information.TabStop = False
    Me.grb_filter_information.Text = "GroupBox1"
    '
    'btn_color
    '
    Me.btn_color.Location = New System.Drawing.Point(565, 178)
    Me.btn_color.Name = "btn_color"
    Me.btn_color.Size = New System.Drawing.Size(75, 23)
    Me.btn_color.TabIndex = 5
    Me.btn_color.Text = "Button1"
    Me.btn_color.UseVisualStyleBackColor = True
    '
    'btn_del_range
    '
    Me.btn_del_range.Location = New System.Drawing.Point(565, 236)
    Me.btn_del_range.Name = "btn_del_range"
    Me.btn_del_range.Size = New System.Drawing.Size(75, 23)
    Me.btn_del_range.TabIndex = 4
    Me.btn_del_range.Text = "Button2"
    Me.btn_del_range.UseVisualStyleBackColor = True
    '
    'btn_add_range
    '
    Me.btn_add_range.Location = New System.Drawing.Point(565, 207)
    Me.btn_add_range.Name = "btn_add_range"
    Me.btn_add_range.Size = New System.Drawing.Size(75, 23)
    Me.btn_add_range.TabIndex = 3
    Me.btn_add_range.Text = "Button1"
    Me.btn_add_range.UseVisualStyleBackColor = True
    '
    'uc_grid_ranges
    '
    Me.uc_grid_ranges.CurrentCol = -1
    Me.uc_grid_ranges.CurrentRow = -1
    Me.uc_grid_ranges.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_ranges.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_ranges.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_BORDER
    Me.uc_grid_ranges.Location = New System.Drawing.Point(7, 31)
    Me.uc_grid_ranges.Name = "uc_grid_ranges"
    Me.uc_grid_ranges.PanelRightVisible = False
    Me.uc_grid_ranges.Redraw = True
    Me.uc_grid_ranges.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.uc_grid_ranges.Size = New System.Drawing.Size(552, 228)
    Me.uc_grid_ranges.Sortable = False
    Me.uc_grid_ranges.SortAscending = True
    Me.uc_grid_ranges.SortByCol = 0
    Me.uc_grid_ranges.TabIndex = 2
    Me.uc_grid_ranges.ToolTipped = True
    Me.uc_grid_ranges.TopRow = -2
    Me.uc_grid_ranges.WordWrap = False
    '
    'frm_intellia_filter_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(773, 679)
    Me.Name = "frm_intellia_filter_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_intellia_filter_edit"
    Me.panel_data.ResumeLayout(False)
    Me.grb_filter_information.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_grid_filters As GUI_Controls.uc_grid
  Friend WithEvents grb_filter_information As System.Windows.Forms.GroupBox
  Friend WithEvents uc_grid_ranges As GUI_Controls.uc_grid
  Friend WithEvents btn_del_range As System.Windows.Forms.Button
  Friend WithEvents btn_add_range As System.Windows.Forms.Button
  Friend WithEvents btn_color As System.Windows.Forms.Button
  Friend WithEvents cd_button_color As System.Windows.Forms.ColorDialog
End Class
