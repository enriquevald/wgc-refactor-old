﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_creditline_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_creditline_edit))
    Me.bt_open_account_summary = New System.Windows.Forms.Button()
    Me.gb_account = New System.Windows.Forms.GroupBox()
    Me.ef_id_account = New GUI_Controls.uc_entry_field()
    Me.ef_holder_name = New GUI_Controls.uc_entry_field()
    Me.gb_signers = New System.Windows.Forms.GroupBox()
    Me.dg_signers = New GUI_Controls.uc_grid()
    Me.bt_sign = New System.Windows.Forms.Button()
    Me.lbl_count_signers = New System.Windows.Forms.Label()
    Me.lbl_signers = New System.Windows.Forms.Label()
    Me.gb_settings = New System.Windows.Forms.GroupBox()
    Me.ef_creation_date = New GUI_Controls.uc_entry_field()
    Me.dtp_expiration = New GUI_Controls.uc_date_picker()
    Me.ef_spent = New GUI_Controls.uc_entry_field()
    Me.ef_iban = New GUI_Controls.uc_entry_field()
    Me.ef_tto = New GUI_Controls.uc_entry_field()
    Me.ef_credit_limit = New GUI_Controls.uc_entry_field()
    Me.cb_status = New GUI_Controls.uc_combo()
    Me.panel_data.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.gb_signers.SuspendLayout()
    Me.gb_settings.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_settings)
    Me.panel_data.Controls.Add(Me.gb_signers)
    Me.panel_data.Controls.Add(Me.gb_account)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(579, 440)
    '
    'bt_open_account_summary
    '
    Me.bt_open_account_summary.Location = New System.Drawing.Point(475, 24)
    Me.bt_open_account_summary.Name = "bt_open_account_summary"
    Me.bt_open_account_summary.Size = New System.Drawing.Size(75, 21)
    Me.bt_open_account_summary.TabIndex = 2
    Me.bt_open_account_summary.Text = "xAccounts"
    Me.bt_open_account_summary.UseVisualStyleBackColor = True
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.ef_id_account)
    Me.gb_account.Controls.Add(Me.ef_holder_name)
    Me.gb_account.Controls.Add(Me.bt_open_account_summary)
    Me.gb_account.Location = New System.Drawing.Point(18, 13)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(560, 62)
    Me.gb_account.TabIndex = 0
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xAccount"
    '
    'ef_id_account
    '
    Me.ef_id_account.DoubleValue = 0.0R
    Me.ef_id_account.IntegerValue = 0
    Me.ef_id_account.IsReadOnly = False
    Me.ef_id_account.Location = New System.Drawing.Point(14, 22)
    Me.ef_id_account.Name = "ef_id_account"
    Me.ef_id_account.PlaceHolder = Nothing
    Me.ef_id_account.Size = New System.Drawing.Size(100, 24)
    Me.ef_id_account.SufixText = "Sufix Text"
    Me.ef_id_account.SufixTextVisible = True
    Me.ef_id_account.TabIndex = 4
    Me.ef_id_account.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_id_account.TextValue = ""
    Me.ef_id_account.TextWidth = 0
    Me.ef_id_account.Value = ""
    Me.ef_id_account.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_holder_name
    '
    Me.ef_holder_name.DoubleValue = 0.0R
    Me.ef_holder_name.IntegerValue = 0
    Me.ef_holder_name.IsReadOnly = False
    Me.ef_holder_name.Location = New System.Drawing.Point(121, 22)
    Me.ef_holder_name.Name = "ef_holder_name"
    Me.ef_holder_name.PlaceHolder = Nothing
    Me.ef_holder_name.Size = New System.Drawing.Size(348, 24)
    Me.ef_holder_name.SufixText = "Sufix Text"
    Me.ef_holder_name.SufixTextVisible = True
    Me.ef_holder_name.TabIndex = 3
    Me.ef_holder_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_holder_name.TextValue = ""
    Me.ef_holder_name.TextWidth = 0
    Me.ef_holder_name.Value = ""
    Me.ef_holder_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_signers
    '
    Me.gb_signers.Controls.Add(Me.dg_signers)
    Me.gb_signers.Controls.Add(Me.bt_sign)
    Me.gb_signers.Controls.Add(Me.lbl_count_signers)
    Me.gb_signers.Controls.Add(Me.lbl_signers)
    Me.gb_signers.Location = New System.Drawing.Point(18, 247)
    Me.gb_signers.Name = "gb_signers"
    Me.gb_signers.Size = New System.Drawing.Size(560, 190)
    Me.gb_signers.TabIndex = 2
    Me.gb_signers.TabStop = False
    Me.gb_signers.Text = "xAccount"
    '
    'dg_signers
    '
    Me.dg_signers.CurrentCol = -1
    Me.dg_signers.CurrentRow = -1
    Me.dg_signers.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_signers.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_signers.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_signers.Location = New System.Drawing.Point(14, 26)
    Me.dg_signers.Name = "dg_signers"
    Me.dg_signers.PanelRightVisible = False
    Me.dg_signers.Redraw = True
    Me.dg_signers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_signers.Size = New System.Drawing.Size(364, 148)
    Me.dg_signers.Sortable = False
    Me.dg_signers.SortAscending = True
    Me.dg_signers.SortByCol = 0
    Me.dg_signers.TabIndex = 0
    Me.dg_signers.ToolTipped = True
    Me.dg_signers.TopRow = -2
    Me.dg_signers.WordWrap = False
    '
    'bt_sign
    '
    Me.bt_sign.Enabled = False
    Me.bt_sign.Location = New System.Drawing.Point(387, 151)
    Me.bt_sign.Name = "bt_sign"
    Me.bt_sign.Size = New System.Drawing.Size(75, 23)
    Me.bt_sign.TabIndex = 3
    Me.bt_sign.Text = "xSign"
    Me.bt_sign.UseVisualStyleBackColor = True
    '
    'lbl_count_signers
    '
    Me.lbl_count_signers.AutoSize = True
    Me.lbl_count_signers.Location = New System.Drawing.Point(384, 43)
    Me.lbl_count_signers.Name = "lbl_count_signers"
    Me.lbl_count_signers.Size = New System.Drawing.Size(91, 13)
    Me.lbl_count_signers.TabIndex = 2
    Me.lbl_count_signers.Text = "xCountSigners"
    '
    'lbl_signers
    '
    Me.lbl_signers.AutoSize = True
    Me.lbl_signers.Location = New System.Drawing.Point(384, 30)
    Me.lbl_signers.Name = "lbl_signers"
    Me.lbl_signers.Size = New System.Drawing.Size(57, 13)
    Me.lbl_signers.TabIndex = 1
    Me.lbl_signers.Text = "xSigners"
    '
    'gb_settings
    '
    Me.gb_settings.Controls.Add(Me.ef_creation_date)
    Me.gb_settings.Controls.Add(Me.dtp_expiration)
    Me.gb_settings.Controls.Add(Me.ef_spent)
    Me.gb_settings.Controls.Add(Me.ef_iban)
    Me.gb_settings.Controls.Add(Me.ef_tto)
    Me.gb_settings.Controls.Add(Me.ef_credit_limit)
    Me.gb_settings.Controls.Add(Me.cb_status)
    Me.gb_settings.Location = New System.Drawing.Point(18, 81)
    Me.gb_settings.Name = "gb_settings"
    Me.gb_settings.Size = New System.Drawing.Size(560, 160)
    Me.gb_settings.TabIndex = 1
    Me.gb_settings.TabStop = False
    Me.gb_settings.Text = "xSettings"
    '
    'ef_creation_date
    '
    Me.ef_creation_date.DoubleValue = 0.0R
    Me.ef_creation_date.IntegerValue = 0
    Me.ef_creation_date.IsReadOnly = False
    Me.ef_creation_date.Location = New System.Drawing.Point(264, 55)
    Me.ef_creation_date.Name = "ef_creation_date"
    Me.ef_creation_date.PlaceHolder = Nothing
    Me.ef_creation_date.Size = New System.Drawing.Size(286, 24)
    Me.ef_creation_date.SufixText = "Sufix Text"
    Me.ef_creation_date.SufixTextVisible = True
    Me.ef_creation_date.TabIndex = 5
    Me.ef_creation_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_creation_date.TextValue = ""
    Me.ef_creation_date.TextWidth = 130
    Me.ef_creation_date.Value = ""
    Me.ef_creation_date.ValueForeColor = System.Drawing.Color.Blue
    '
    'dtp_expiration
    '
    Me.dtp_expiration.Checked = True
    Me.dtp_expiration.IsReadOnly = False
    Me.dtp_expiration.Location = New System.Drawing.Point(264, 22)
    Me.dtp_expiration.Name = "dtp_expiration"
    Me.dtp_expiration.ShowCheckBox = True
    Me.dtp_expiration.ShowUpDown = False
    Me.dtp_expiration.Size = New System.Drawing.Size(286, 24)
    Me.dtp_expiration.SufixText = "Sufix Text"
    Me.dtp_expiration.SufixTextVisible = True
    Me.dtp_expiration.TabIndex = 3
    Me.dtp_expiration.TextWidth = 130
    Me.dtp_expiration.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_spent
    '
    Me.ef_spent.DoubleValue = 0.0R
    Me.ef_spent.IntegerValue = 0
    Me.ef_spent.IsReadOnly = False
    Me.ef_spent.Location = New System.Drawing.Point(17, 86)
    Me.ef_spent.Name = "ef_spent"
    Me.ef_spent.PlaceHolder = Nothing
    Me.ef_spent.Size = New System.Drawing.Size(237, 24)
    Me.ef_spent.SufixText = "Sufix Text"
    Me.ef_spent.SufixTextVisible = True
    Me.ef_spent.TabIndex = 2
    Me.ef_spent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_spent.TextValue = ""
    Me.ef_spent.TextWidth = 110
    Me.ef_spent.Value = ""
    Me.ef_spent.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_iban
    '
    Me.ef_iban.DoubleValue = 0.0R
    Me.ef_iban.IntegerValue = 0
    Me.ef_iban.IsReadOnly = False
    Me.ef_iban.Location = New System.Drawing.Point(46, 121)
    Me.ef_iban.Name = "ef_iban"
    Me.ef_iban.PlaceHolder = Nothing
    Me.ef_iban.Size = New System.Drawing.Size(504, 24)
    Me.ef_iban.SufixText = "Sufix Text"
    Me.ef_iban.SufixTextVisible = True
    Me.ef_iban.TabIndex = 6
    Me.ef_iban.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_iban.TextValue = ""
    Me.ef_iban.Value = ""
    Me.ef_iban.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tto
    '
    Me.ef_tto.DoubleValue = 0.0R
    Me.ef_tto.IntegerValue = 0
    Me.ef_tto.IsReadOnly = False
    Me.ef_tto.Location = New System.Drawing.Point(17, 54)
    Me.ef_tto.Name = "ef_tto"
    Me.ef_tto.PlaceHolder = Nothing
    Me.ef_tto.Size = New System.Drawing.Size(237, 24)
    Me.ef_tto.SufixText = "Sufix Text"
    Me.ef_tto.SufixTextVisible = True
    Me.ef_tto.TabIndex = 1
    Me.ef_tto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tto.TextValue = ""
    Me.ef_tto.TextWidth = 110
    Me.ef_tto.Value = ""
    Me.ef_tto.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_credit_limit
    '
    Me.ef_credit_limit.DoubleValue = 0.0R
    Me.ef_credit_limit.IntegerValue = 0
    Me.ef_credit_limit.IsReadOnly = False
    Me.ef_credit_limit.Location = New System.Drawing.Point(17, 22)
    Me.ef_credit_limit.Name = "ef_credit_limit"
    Me.ef_credit_limit.PlaceHolder = Nothing
    Me.ef_credit_limit.Size = New System.Drawing.Size(237, 24)
    Me.ef_credit_limit.SufixText = "Sufix Text"
    Me.ef_credit_limit.SufixTextVisible = True
    Me.ef_credit_limit.TabIndex = 0
    Me.ef_credit_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_credit_limit.TextValue = ""
    Me.ef_credit_limit.TextWidth = 110
    Me.ef_credit_limit.Value = ""
    Me.ef_credit_limit.ValueForeColor = System.Drawing.Color.Blue
    '
    'cb_status
    '
    Me.cb_status.AllowUnlistedValues = False
    Me.cb_status.AutoCompleteMode = False
    Me.cb_status.IsReadOnly = False
    Me.cb_status.Location = New System.Drawing.Point(260, 86)
    Me.cb_status.Name = "cb_status"
    Me.cb_status.SelectedIndex = -1
    Me.cb_status.Size = New System.Drawing.Size(290, 24)
    Me.cb_status.SufixText = "Sufix Text"
    Me.cb_status.SufixTextVisible = True
    Me.cb_status.TabIndex = 5
    Me.cb_status.TextCombo = Nothing
    Me.cb_status.TextWidth = 133
    '
    'frm_creditline_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(698, 448)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_creditline_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_creditline_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_account.ResumeLayout(False)
    Me.gb_signers.ResumeLayout(False)
    Me.gb_signers.PerformLayout()
    Me.gb_settings.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents bt_open_account_summary As System.Windows.Forms.Button
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents gb_settings As System.Windows.Forms.GroupBox
  Friend WithEvents gb_signers As System.Windows.Forms.GroupBox
  Friend WithEvents bt_sign As System.Windows.Forms.Button
  Friend WithEvents lbl_count_signers As System.Windows.Forms.Label
  Friend WithEvents lbl_signers As System.Windows.Forms.Label
  Friend WithEvents ef_spent As GUI_Controls.uc_entry_field
  Friend WithEvents ef_iban As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tto As GUI_Controls.uc_entry_field
  Friend WithEvents ef_credit_limit As GUI_Controls.uc_entry_field
  Friend WithEvents cb_status As GUI_Controls.uc_combo
  Friend WithEvents dg_signers As GUI_Controls.uc_grid
  Friend WithEvents dtp_expiration As GUI_Controls.uc_date_picker
  Friend WithEvents ef_holder_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_id_account As GUI_Controls.uc_entry_field
  Friend WithEvents ef_creation_date As GUI_Controls.uc_entry_field
End Class
