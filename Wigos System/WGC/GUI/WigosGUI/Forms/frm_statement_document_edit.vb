'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_statement_document_edit
' DESCRIPTION:   edit document of statement
' AUTHOR:        Samuel Gonz�lez
' CREATION DATE: 22-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-APR-2015  SGB    Initial version. Backlog Item 1129
' 18-JUN-2013  YNM    Fixed Bug WIG-2456 / WIG-2464: add some validations.
' 01-JUL-2015  YNM    Fixed Bug WIG-2464

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.IO
Imports System.Drawing.Printing
Imports WSI.Common

Public Class frm_statement_document_edit
  Inherits frm_base_edit

#Region " Constants "

#End Region ' Constants

#Region " Members "

#End Region ' Members

#Region " Properties "

#End Region ' Properties

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STATEMENT_DOCUMENT_EDIT
    ' Call Base Form procedure
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_title

  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _statement As CLASS_STATEMENT

    If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '''Wrong DbVersion message already shown, do not show again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        Me.DbEditedObject = New CLASS_STATEMENT
        _statement = DbEditedObject
        Me.DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), ENUM_MB_TYPE.MB_TYPE_ERROR)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6224)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Enabled = True

    Call img_preview.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)

    'configuration statement
    Me.gb_config_statement.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6225)
    Me.ef_fiscal_year_start_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6226)
    Me.ef_number_prev_year.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6232)
    Me.ef_number_prev_year.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6228)

    'configuration page format
    Me.gb_config_page_format.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6230)
    Me.cmb_paper_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6231)
    Me.ef_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6233)
    Me.ef_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 30)
    Me.ef_header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6234)
    Me.lb_img_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "113", "624")
    Me.ef_logo.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6242)
    Me.ef_footer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6287)

    ' Harcoded values of the combo box.
    Me.cmb_paper_size.Add(PageSize.A4, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6236))          ' 6236 "A4"
    Me.cmb_paper_size.Add(PageSize.A5, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6237))          ' 6237 "A5"
    Me.cmb_paper_size.Add(PageSize.Letter, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6235))      ' 6235 "Letter"

  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _statement As CLASS_STATEMENT

    _statement = DbReadObject()

    If String.IsNullOrEmpty(ef_number_prev_year.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_number_prev_year.Text)
      Call ef_number_prev_year.Focus()
      Return False
    ElseIf ef_number_prev_year.Value <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_number_prev_year.Text, "1")
      Call ef_number_prev_year.Focus()
      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _statement As CLASS_STATEMENT
    Dim _date_string As String
    Dim _string_to_date As DateTime

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _statement = DbReadObject

    _date_string = _statement.fiscal_year_start.Substring(2, 2) & "/" _
                                      & _statement.fiscal_year_start.Substring(0, 2) & "/" & Now.Year.ToString() & " " _
                                      & _statement.fiscal_year_start.Substring(4, 2) & ":00:00"

    _string_to_date = DateTime.ParseExact(_date_string, "dd/MM/yyyy HH:mm:ss", Nothing)

    Me.dp_fiscal_year_start_date.Value = _string_to_date
    Me.chk_enabled.Checked = _statement.enabled
    Me.cmb_paper_size.SelectedIndex = _statement.paper_size
    Me.ef_title.TextValue = _statement.title
    Me.ef_number_prev_year.TextValue = _statement.number_prev_year
    Me.txt_header.Text = _statement.header.Replace("\n", vbCrLf)
    Me.img_preview.Image = _statement.logo
    Me.txt_footer.Text = _statement.footer.Replace("\n", vbCrLf)

  End Sub 'GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()

    Dim _statement As CLASS_STATEMENT

    _statement = DbEditedObject

    _statement.fiscal_year_start = Me.dp_fiscal_year_start_date.Value.Month.ToString("D2") _
                                  + Me.dp_fiscal_year_start_date.Value.Day.ToString("D2") _
                                  + Me.dp_fiscal_year_start_date.Value.Hour.ToString("D2")
    _statement.enabled = Me.chk_enabled.Checked
    _statement.paper_size = Me.cmb_paper_size.Value
    _statement.paper_size_string = Me.cmb_paper_size.TextValue
    _statement.title = Me.ef_title.TextValue
    _statement.number_prev_year = IIf(String.IsNullOrEmpty(Me.ef_number_prev_year.TextValue), 1, Me.ef_number_prev_year.TextValue)
    _statement.header = Me.txt_header.Text.Replace(vbCrLf, "\n")
    _statement.logo = Me.img_preview.Image
    _statement.footer = Me.txt_footer.Text.Replace(vbCrLf, "\n")

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Ignore [ENTER] key when editing comments
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If e.KeyChar = Chr(Keys.Enter) And Me.txt_header.ContainsFocus Then
      Return True
    End If

    If e.KeyChar = Chr(Keys.Enter) And Me.txt_footer.ContainsFocus Then
      Return True
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_OK
        Dim _lines As Integer
        _lines = 4
        If txt_header.Lines.Length > _lines Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6497), ENUM_MB_TYPE.MB_TYPE_ERROR,
                          ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(6234), _lines.ToString())
          txt_header.Focus()
          Return
        End If
    End Select
    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub

#End Region ' Overrides

#Region " Private Functions "

#End Region ' Private Functions

#Region " Public Functions "

#End Region ' Public Functions 

#Region " Events "

#End Region ' Events


End Class