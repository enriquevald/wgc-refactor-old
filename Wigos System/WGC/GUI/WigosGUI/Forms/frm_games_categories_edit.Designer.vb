﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_games_categories_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.tb_description = New System.Windows.Forms.TextBox()
    Me.ef_category_title = New GUI_Controls.uc_entry_field()
    Me.gb_category_state = New System.Windows.Forms.GroupBox()
    Me.rb_category_disabled = New System.Windows.Forms.RadioButton()
    Me.rb_category_enabled = New System.Windows.Forms.RadioButton()
    Me.lbl_category_title = New System.Windows.Forms.Label()
    Me.lbl_category_description = New System.Windows.Forms.Label()
    Me.Uc_imageList = New GUI_Controls.uc_image()
    Me.panel_data.SuspendLayout()
    Me.gb_category_state.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.Uc_imageList)
    Me.panel_data.Controls.Add(Me.lbl_category_description)
    Me.panel_data.Controls.Add(Me.lbl_category_title)
    Me.panel_data.Controls.Add(Me.gb_category_state)
    Me.panel_data.Controls.Add(Me.tb_description)
    Me.panel_data.Controls.Add(Me.ef_category_title)
    Me.panel_data.Location = New System.Drawing.Point(8, 7)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(5)
    Me.panel_data.Size = New System.Drawing.Size(631, 729)
    '
    'tb_description
    '
    Me.tb_description.Location = New System.Drawing.Point(105, 39)
    Me.tb_description.Margin = New System.Windows.Forms.Padding(5)
    Me.tb_description.MaxLength = 200
    Me.tb_description.Multiline = True
    Me.tb_description.Name = "tb_description"
    Me.tb_description.Size = New System.Drawing.Size(402, 115)
    Me.tb_description.TabIndex = 1
    '
    'ef_category_title
    '
    Me.ef_category_title.DoubleValue = 0.0R
    Me.ef_category_title.IntegerValue = 0
    Me.ef_category_title.IsReadOnly = False
    Me.ef_category_title.Location = New System.Drawing.Point(103, 3)
    Me.ef_category_title.Margin = New System.Windows.Forms.Padding(5)
    Me.ef_category_title.Name = "ef_category_title"
    Me.ef_category_title.PlaceHolder = Nothing
    Me.ef_category_title.Size = New System.Drawing.Size(406, 25)
    Me.ef_category_title.SufixText = "Sufix Text"
    Me.ef_category_title.SufixTextVisible = True
    Me.ef_category_title.TabIndex = 0
    Me.ef_category_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_category_title.TextValue = ""
    Me.ef_category_title.TextWidth = 0
    Me.ef_category_title.Value = ""
    Me.ef_category_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_category_state
    '
    Me.gb_category_state.Controls.Add(Me.rb_category_disabled)
    Me.gb_category_state.Controls.Add(Me.rb_category_enabled)
    Me.gb_category_state.Location = New System.Drawing.Point(519, -2)
    Me.gb_category_state.Margin = New System.Windows.Forms.Padding(5)
    Me.gb_category_state.Name = "gb_category_state"
    Me.gb_category_state.Padding = New System.Windows.Forms.Padding(5)
    Me.gb_category_state.Size = New System.Drawing.Size(107, 103)
    Me.gb_category_state.TabIndex = 2
    Me.gb_category_state.TabStop = False
    Me.gb_category_state.Text = "Habilitado"
    '
    'rb_category_disabled
    '
    Me.rb_category_disabled.AutoSize = True
    Me.rb_category_disabled.Location = New System.Drawing.Point(13, 61)
    Me.rb_category_disabled.Margin = New System.Windows.Forms.Padding(5)
    Me.rb_category_disabled.Name = "rb_category_disabled"
    Me.rb_category_disabled.Size = New System.Drawing.Size(40, 17)
    Me.rb_category_disabled.TabIndex = 2
    Me.rb_category_disabled.TabStop = True
    Me.rb_category_disabled.Text = "No"
    Me.rb_category_disabled.UseVisualStyleBackColor = True
    '
    'rb_category_enabled
    '
    Me.rb_category_enabled.AutoSize = True
    Me.rb_category_enabled.Location = New System.Drawing.Point(13, 27)
    Me.rb_category_enabled.Margin = New System.Windows.Forms.Padding(5)
    Me.rb_category_enabled.Name = "rb_category_enabled"
    Me.rb_category_enabled.Size = New System.Drawing.Size(36, 17)
    Me.rb_category_enabled.TabIndex = 1
    Me.rb_category_enabled.TabStop = True
    Me.rb_category_enabled.Text = "Sí"
    Me.rb_category_enabled.UseVisualStyleBackColor = True
    '
    'lbl_category_title
    '
    Me.lbl_category_title.AutoSize = True
    Me.lbl_category_title.Location = New System.Drawing.Point(3, 8)
    Me.lbl_category_title.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_category_title.Name = "lbl_category_title"
    Me.lbl_category_title.Size = New System.Drawing.Size(58, 13)
    Me.lbl_category_title.TabIndex = 25
    Me.lbl_category_title.Text = "xCatTitle"
    '
    'lbl_category_description
    '
    Me.lbl_category_description.AutoSize = True
    Me.lbl_category_description.Location = New System.Drawing.Point(4, 39)
    Me.lbl_category_description.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
    Me.lbl_category_description.Name = "lbl_category_description"
    Me.lbl_category_description.Size = New System.Drawing.Size(78, 13)
    Me.lbl_category_description.TabIndex = 26
    Me.lbl_category_description.Text = "xDescription"
    '
    'Uc_imageList
    '
    Me.Uc_imageList.AutoSize = True
    Me.Uc_imageList.ButtonDeleteEnabled = True
    Me.Uc_imageList.FreeResize = False
    Me.Uc_imageList.Image = Nothing
    Me.Uc_imageList.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Uc_imageList.ImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.Uc_imageList.ImageName = Nothing
    Me.Uc_imageList.Location = New System.Drawing.Point(105, 167)
    Me.Uc_imageList.Margin = New System.Windows.Forms.Padding(0)
    Me.Uc_imageList.Name = "Uc_imageList"
    Me.Uc_imageList.Size = New System.Drawing.Size(427, 379)
    Me.Uc_imageList.TabIndex = 27
    Me.Uc_imageList.Transparent = False
    '
    'frm_games_categories_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(744, 556)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Margin = New System.Windows.Forms.Padding(5)
    Me.Name = "frm_games_categories_edit"
    Me.Padding = New System.Windows.Forms.Padding(8, 7, 8, 7)
    Me.Text = "frm_games_category_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_category_state.ResumeLayout(False)
    Me.gb_category_state.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tb_description As System.Windows.Forms.TextBox
  Friend WithEvents ef_category_title As GUI_Controls.uc_entry_field
  Friend WithEvents gb_category_state As System.Windows.Forms.GroupBox
  Friend WithEvents rb_category_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents rb_category_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_category_title As System.Windows.Forms.Label
  Friend WithEvents lbl_category_description As System.Windows.Forms.Label
  Friend WithEvents Uc_imageList As GUI_Controls.uc_image
End Class
