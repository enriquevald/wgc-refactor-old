﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_threshold_movements_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_threshold_movements_report))
        Me.gb_date = New System.Windows.Forms.GroupBox()
        Me.dtp_to = New GUI_Controls.uc_date_picker()
        Me.dtp_from = New GUI_Controls.uc_date_picker()
        Me.ef_holder_name = New GUI_Controls.uc_entry_field()
        Me.gb_user = New System.Windows.Forms.GroupBox()
        Me.chk_show_all = New System.Windows.Forms.CheckBox()
        Me.opt_one_user = New System.Windows.Forms.RadioButton()
        Me.opt_all_users = New System.Windows.Forms.RadioButton()
        Me.cmb_user = New GUI_Controls.uc_combo()
        Me.uc_transaction_type = New GUI_Controls.uc_checked_list()
        Me.gp_client = New System.Windows.Forms.GroupBox()
        Me.panel_filter.SuspendLayout()
        Me.panel_data.SuspendLayout()
        Me.pn_separator_line.SuspendLayout()
        Me.gb_date.SuspendLayout()
        Me.gb_user.SuspendLayout()
        Me.gp_client.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_filter
        '
        Me.panel_filter.Controls.Add(Me.gp_client)
        Me.panel_filter.Controls.Add(Me.uc_transaction_type)
        Me.panel_filter.Controls.Add(Me.gb_user)
        Me.panel_filter.Controls.Add(Me.gb_date)
        Me.panel_filter.Size = New System.Drawing.Size(1152, 170)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.uc_transaction_type, 0)
        Me.panel_filter.Controls.SetChildIndex(Me.gp_client, 0)
        '
        'panel_data
        '
        Me.panel_data.Location = New System.Drawing.Point(4, 174)
        Me.panel_data.Size = New System.Drawing.Size(1152, 514)
        '
        'pn_separator_line
        '
        Me.pn_separator_line.Size = New System.Drawing.Size(1146, 23)
        '
        'pn_line
        '
        Me.pn_line.Size = New System.Drawing.Size(1146, 4)
        '
        'gb_date
        '
        Me.gb_date.Controls.Add(Me.dtp_to)
        Me.gb_date.Controls.Add(Me.dtp_from)
        Me.gb_date.Location = New System.Drawing.Point(6, 15)
        Me.gb_date.Name = "gb_date"
        Me.gb_date.Size = New System.Drawing.Size(284, 85)
        Me.gb_date.TabIndex = 0
        Me.gb_date.TabStop = False
        Me.gb_date.Text = "xDate"
        '
        'dtp_to
        '
        Me.dtp_to.Checked = True
        Me.dtp_to.IsReadOnly = False
        Me.dtp_to.Location = New System.Drawing.Point(6, 49)
        Me.dtp_to.Name = "dtp_to"
        Me.dtp_to.ShowCheckBox = True
        Me.dtp_to.ShowUpDown = False
        Me.dtp_to.Size = New System.Drawing.Size(254, 24)
        Me.dtp_to.SufixText = "Sufix Text"
        Me.dtp_to.SufixTextVisible = True
        Me.dtp_to.TabIndex = 1
        Me.dtp_to.TextWidth = 50
        Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'dtp_from
        '
        Me.dtp_from.Checked = True
        Me.dtp_from.IsReadOnly = False
        Me.dtp_from.Location = New System.Drawing.Point(6, 21)
        Me.dtp_from.Name = "dtp_from"
        Me.dtp_from.ShowCheckBox = True
        Me.dtp_from.ShowUpDown = False
        Me.dtp_from.Size = New System.Drawing.Size(254, 24)
        Me.dtp_from.SufixText = "Sufix Text"
        Me.dtp_from.SufixTextVisible = True
        Me.dtp_from.TabIndex = 0
        Me.dtp_from.TextWidth = 50
        Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
        '
        'ef_holder_name
        '
        Me.ef_holder_name.DoubleValue = 0.0R
        Me.ef_holder_name.IntegerValue = 0
        Me.ef_holder_name.IsReadOnly = False
        Me.ef_holder_name.Location = New System.Drawing.Point(6, 20)
        Me.ef_holder_name.Name = "ef_holder_name"
        Me.ef_holder_name.PlaceHolder = Nothing
        Me.ef_holder_name.ShortcutsEnabled = True
        Me.ef_holder_name.Size = New System.Drawing.Size(272, 25)
        Me.ef_holder_name.SufixText = "Sufix Text"
        Me.ef_holder_name.SufixTextVisible = True
        Me.ef_holder_name.TabIndex = 0
        Me.ef_holder_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.ef_holder_name.TextValue = ""
        Me.ef_holder_name.TextWidth = 0
        Me.ef_holder_name.Value = ""
        Me.ef_holder_name.ValueForeColor = System.Drawing.Color.Blue
        '
        'gb_user
        '
        Me.gb_user.Controls.Add(Me.chk_show_all)
        Me.gb_user.Controls.Add(Me.opt_one_user)
        Me.gb_user.Controls.Add(Me.opt_all_users)
        Me.gb_user.Controls.Add(Me.cmb_user)
        Me.gb_user.Location = New System.Drawing.Point(624, 15)
        Me.gb_user.Name = "gb_user"
        Me.gb_user.Size = New System.Drawing.Size(272, 80)
        Me.gb_user.TabIndex = 4
        Me.gb_user.TabStop = False
        Me.gb_user.Text = "xUser"
        '
        'chk_show_all
        '
        Me.chk_show_all.AutoSize = True
        Me.chk_show_all.Location = New System.Drawing.Point(81, 49)
        Me.chk_show_all.Name = "chk_show_all"
        Me.chk_show_all.Size = New System.Drawing.Size(102, 17)
        Me.chk_show_all.TabIndex = 3
        Me.chk_show_all.Text = "chk_show_all"
        Me.chk_show_all.UseVisualStyleBackColor = True
        '
        'opt_one_user
        '
        Me.opt_one_user.Location = New System.Drawing.Point(8, 18)
        Me.opt_one_user.Name = "opt_one_user"
        Me.opt_one_user.Size = New System.Drawing.Size(72, 24)
        Me.opt_one_user.TabIndex = 0
        Me.opt_one_user.Text = "xOne"
        '
        'opt_all_users
        '
        Me.opt_all_users.Location = New System.Drawing.Point(8, 44)
        Me.opt_all_users.Name = "opt_all_users"
        Me.opt_all_users.Size = New System.Drawing.Size(64, 24)
        Me.opt_all_users.TabIndex = 1
        Me.opt_all_users.Text = "xAll"
        '
        'cmb_user
        '
        Me.cmb_user.AllowUnlistedValues = False
        Me.cmb_user.AutoCompleteMode = False
        Me.cmb_user.IsReadOnly = False
        Me.cmb_user.Location = New System.Drawing.Point(78, 18)
        Me.cmb_user.Name = "cmb_user"
        Me.cmb_user.SelectedIndex = -1
        Me.cmb_user.Size = New System.Drawing.Size(184, 24)
        Me.cmb_user.SufixText = "Sufix Text"
        Me.cmb_user.SufixTextVisible = True
        Me.cmb_user.TabIndex = 2
        Me.cmb_user.TextCombo = Nothing
        Me.cmb_user.TextVisible = False
        Me.cmb_user.TextWidth = 0
        '
        'uc_transaction_type
        '
        Me.uc_transaction_type.GroupBoxText = "xStatusList"
        Me.uc_transaction_type.Location = New System.Drawing.Point(296, 15)
        Me.uc_transaction_type.m_resize_width = 322
        Me.uc_transaction_type.multiChoice = True
        Me.uc_transaction_type.Name = "uc_transaction_type"
        Me.uc_transaction_type.SelectedIndexes = New Integer(-1) {}
        Me.uc_transaction_type.SelectedIndexesList = ""
        Me.uc_transaction_type.SelectedIndexesListLevel2 = ""
        Me.uc_transaction_type.SelectedValuesArray = New String(-1) {}
        Me.uc_transaction_type.SelectedValuesList = ""
        Me.uc_transaction_type.SelectedValuesListLevel2 = ""
        Me.uc_transaction_type.SetLevels = 2
        Me.uc_transaction_type.Size = New System.Drawing.Size(322, 144)
        Me.uc_transaction_type.TabIndex = 3
        Me.uc_transaction_type.ValuesArray = New String(-1) {}
        '
        'gp_client
        '
        Me.gp_client.Controls.Add(Me.ef_holder_name)
        Me.gp_client.Location = New System.Drawing.Point(6, 106)
        Me.gp_client.Name = "gp_client"
        Me.gp_client.Size = New System.Drawing.Size(284, 53)
        Me.gp_client.TabIndex = 2
        Me.gp_client.TabStop = False
        Me.gp_client.Text = "xUser"
        '
        'frm_threshold_movements_report
        '
        Me.ClientSize = New System.Drawing.Size(1160, 692)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_threshold_movements_report"
        Me.Text = "frm_threshold_movements_report"
        Me.panel_filter.ResumeLayout(False)
        Me.panel_data.ResumeLayout(False)
        Me.pn_separator_line.ResumeLayout(False)
        Me.gb_date.ResumeLayout(False)
        Me.gb_user.ResumeLayout(False)
        Me.gb_user.PerformLayout()
        Me.gp_client.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gb_date As System.Windows.Forms.GroupBox
    Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
    Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
    Friend WithEvents ef_holder_name As GUI_Controls.uc_entry_field
    Friend WithEvents gb_user As System.Windows.Forms.GroupBox
    Friend WithEvents chk_show_all As System.Windows.Forms.CheckBox
    Friend WithEvents opt_one_user As System.Windows.Forms.RadioButton
    Friend WithEvents opt_all_users As System.Windows.Forms.RadioButton
    Friend WithEvents cmb_user As GUI_Controls.uc_combo
    Friend WithEvents uc_transaction_type As GUI_Controls.uc_checked_list
    Friend WithEvents gp_client As System.Windows.Forms.GroupBox

End Class
