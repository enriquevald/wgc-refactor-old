<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ticket_configuration_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dg_principal = New GUI_Controls.uc_grid()
    Me.gp_general_options = New System.Windows.Forms.GroupBox()
    Me.lbl_message_info = New System.Windows.Forms.Label()
    Me.cb_reprint = New System.Windows.Forms.CheckBox()
    Me.cb_print = New System.Windows.Forms.CheckBox()
    Me.cb_generate = New System.Windows.Forms.CheckBox()
    Me.uc_entry_reprint = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gp_general_options.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gp_general_options)
    Me.panel_data.Controls.Add(Me.dg_principal)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(712, 344)
    '
    'dg_principal
    '
    Me.dg_principal.CurrentCol = -1
    Me.dg_principal.CurrentRow = -1
    Me.dg_principal.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_principal.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_principal.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_principal.Location = New System.Drawing.Point(7, 132)
    Me.dg_principal.Name = "dg_principal"
    Me.dg_principal.PanelRightVisible = False
    Me.dg_principal.Redraw = True
    Me.dg_principal.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_principal.Size = New System.Drawing.Size(698, 204)
    Me.dg_principal.Sortable = False
    Me.dg_principal.SortAscending = True
    Me.dg_principal.SortByCol = 0
    Me.dg_principal.TabIndex = 1
    Me.dg_principal.ToolTipped = True
    Me.dg_principal.TopRow = -2
    '
    'gp_general_options
    '
    Me.gp_general_options.Controls.Add(Me.lbl_message_info)
    Me.gp_general_options.Controls.Add(Me.cb_reprint)
    Me.gp_general_options.Controls.Add(Me.cb_print)
    Me.gp_general_options.Controls.Add(Me.cb_generate)
    Me.gp_general_options.Controls.Add(Me.uc_entry_reprint)
    Me.gp_general_options.Location = New System.Drawing.Point(7, 3)
    Me.gp_general_options.Name = "gp_general_options"
    Me.gp_general_options.Size = New System.Drawing.Size(523, 123)
    Me.gp_general_options.TabIndex = 0
    Me.gp_general_options.TabStop = False
    Me.gp_general_options.Text = "#Operaciones globales#"
    '
    'lbl_message_info
    '
    Me.lbl_message_info.AutoSize = True
    Me.lbl_message_info.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_message_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_message_info.Location = New System.Drawing.Point(169, 41)
    Me.lbl_message_info.Name = "lbl_message_info"
    Me.lbl_message_info.Size = New System.Drawing.Size(98, 14)
    Me.lbl_message_info.TabIndex = 4
    Me.lbl_message_info.Text = "xmessage_info"
    '
    'cb_reprint
    '
    Me.cb_reprint.AutoSize = True
    Me.cb_reprint.Location = New System.Drawing.Point(21, 64)
    Me.cb_reprint.Name = "cb_reprint"
    Me.cb_reprint.Size = New System.Drawing.Size(82, 17)
    Me.cb_reprint.TabIndex = 2
    Me.cb_reprint.Text = "#reprint#"
    Me.cb_reprint.UseVisualStyleBackColor = True
    '
    'cb_print
    '
    Me.cb_print.AutoSize = True
    Me.cb_print.Location = New System.Drawing.Point(21, 41)
    Me.cb_print.Name = "cb_print"
    Me.cb_print.Size = New System.Drawing.Size(70, 17)
    Me.cb_print.TabIndex = 1
    Me.cb_print.Text = "#print#"
    Me.cb_print.UseVisualStyleBackColor = True
    '
    'cb_generate
    '
    Me.cb_generate.AutoSize = True
    Me.cb_generate.Location = New System.Drawing.Point(21, 18)
    Me.cb_generate.Name = "cb_generate"
    Me.cb_generate.Size = New System.Drawing.Size(95, 17)
    Me.cb_generate.TabIndex = 0
    Me.cb_generate.Text = "#generate#"
    Me.cb_generate.UseVisualStyleBackColor = True
    '
    'uc_entry_reprint
    '
    Me.uc_entry_reprint.DoubleValue = 0.0R
    Me.uc_entry_reprint.IntegerValue = 0
    Me.uc_entry_reprint.IsReadOnly = False
    Me.uc_entry_reprint.Location = New System.Drawing.Point(48, 87)
    Me.uc_entry_reprint.Name = "uc_entry_reprint"
    Me.uc_entry_reprint.PlaceHolder = Nothing
    Me.uc_entry_reprint.Size = New System.Drawing.Size(466, 24)
    Me.uc_entry_reprint.SufixText = "Sufix Text"
    Me.uc_entry_reprint.SufixTextVisible = True
    Me.uc_entry_reprint.TabIndex = 3
    Me.uc_entry_reprint.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_reprint.TextValue = ""
    Me.uc_entry_reprint.TextWidth = 120
    Me.uc_entry_reprint.Value = ""
    Me.uc_entry_reprint.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_ticket_configuration_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(816, 351)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_ticket_configuration_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_ticket_configuration_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gp_general_options.ResumeLayout(False)
    Me.gp_general_options.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_principal As GUI_Controls.uc_grid
  Friend WithEvents gp_general_options As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_reprint As GUI_Controls.uc_entry_field
  Friend WithEvents cb_reprint As System.Windows.Forms.CheckBox
  Friend WithEvents cb_print As System.Windows.Forms.CheckBox
  Friend WithEvents cb_generate As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_message_info As System.Windows.Forms.Label
End Class
