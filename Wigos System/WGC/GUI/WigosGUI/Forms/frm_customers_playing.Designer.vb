﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_customers_playing
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_order_by = New System.Windows.Forms.GroupBox()
    Me.opt_provider_terminal = New System.Windows.Forms.RadioButton()
    Me.opt_date_started = New System.Windows.Forms.RadioButton()
    Me.opt_account = New System.Windows.Forms.RadioButton()
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.gb_level = New System.Windows.Forms.GroupBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.gb_duration_average = New System.Windows.Forms.GroupBox()
    Me.dtp_duration = New GUI_Controls.uc_date_picker()
    Me.lbl_duration = New System.Windows.Forms.Label()
    Me.opt_working_day = New System.Windows.Forms.RadioButton()
    Me.opt_session = New System.Windows.Forms.RadioButton()
    Me.uc_entry_average_bet = New GUI_Controls.uc_entry_field()
    Me.uc_pr_list = New GUI_Controls.uc_provider()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_order_by.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_duration_average.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.gb_duration_average)
    Me.panel_filter.Controls.Add(Me.gb_level)
    Me.panel_filter.Controls.Add(Me.tf_last_update)
    Me.panel_filter.Controls.Add(Me.gb_order_by)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1288, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_order_by, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_last_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_level, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_duration_average, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 194)
    Me.panel_data.Size = New System.Drawing.Size(1288, 458)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1282, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1282, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(0, 4)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 0
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_order_by
    '
    Me.gb_order_by.Controls.Add(Me.opt_provider_terminal)
    Me.gb_order_by.Controls.Add(Me.opt_date_started)
    Me.gb_order_by.Controls.Add(Me.opt_account)
    Me.gb_order_by.Location = New System.Drawing.Point(314, 7)
    Me.gb_order_by.Name = "gb_order_by"
    Me.gb_order_by.Size = New System.Drawing.Size(161, 131)
    Me.gb_order_by.TabIndex = 1
    Me.gb_order_by.TabStop = False
    Me.gb_order_by.Text = "xOrder By"
    '
    'opt_provider_terminal
    '
    Me.opt_provider_terminal.AutoSize = True
    Me.opt_provider_terminal.Location = New System.Drawing.Point(18, 97)
    Me.opt_provider_terminal.Name = "opt_provider_terminal"
    Me.opt_provider_terminal.Size = New System.Drawing.Size(130, 17)
    Me.opt_provider_terminal.TabIndex = 2
    Me.opt_provider_terminal.TabStop = True
    Me.opt_provider_terminal.Text = "xProviderTerminal"
    Me.opt_provider_terminal.UseVisualStyleBackColor = True
    '
    'opt_date_started
    '
    Me.opt_date_started.AutoSize = True
    Me.opt_date_started.Location = New System.Drawing.Point(18, 58)
    Me.opt_date_started.Name = "opt_date_started"
    Me.opt_date_started.Size = New System.Drawing.Size(101, 17)
    Me.opt_date_started.TabIndex = 1
    Me.opt_date_started.TabStop = True
    Me.opt_date_started.Text = "xDateStarted"
    Me.opt_date_started.UseVisualStyleBackColor = True
    '
    'opt_account
    '
    Me.opt_account.AutoSize = True
    Me.opt_account.Location = New System.Drawing.Point(18, 19)
    Me.opt_account.Name = "opt_account"
    Me.opt_account.Size = New System.Drawing.Size(77, 17)
    Me.opt_account.TabIndex = 0
    Me.opt_account.TabStop = True
    Me.opt_account.Text = "xAccount"
    Me.opt_account.UseVisualStyleBackColor = True
    '
    'tmr_monitor
    '
    Me.tmr_monitor.Interval = 10000
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(315, 158)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(575, 25)
    Me.tf_last_update.SufixText = "Sufix Text"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 125
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 140
    Me.tf_last_update.Value = "xLastUpdate"
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(674, 6)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(129, 131)
    Me.gb_level.TabIndex = 2
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(11, 19)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 0
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(11, 47)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 1
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(11, 75)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 2
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(11, 103)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 3
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'gb_duration_average
    '
    Me.gb_duration_average.Controls.Add(Me.dtp_duration)
    Me.gb_duration_average.Controls.Add(Me.lbl_duration)
    Me.gb_duration_average.Controls.Add(Me.opt_working_day)
    Me.gb_duration_average.Controls.Add(Me.opt_session)
    Me.gb_duration_average.Controls.Add(Me.uc_entry_average_bet)
    Me.gb_duration_average.Location = New System.Drawing.Point(481, 7)
    Me.gb_duration_average.Name = "gb_duration_average"
    Me.gb_duration_average.Size = New System.Drawing.Size(187, 131)
    Me.gb_duration_average.TabIndex = 3
    Me.gb_duration_average.TabStop = False
    '
    'dtp_duration
    '
    Me.dtp_duration.Checked = True
    Me.dtp_duration.IsReadOnly = False
    Me.dtp_duration.Location = New System.Drawing.Point(97, 61)
    Me.dtp_duration.Name = "dtp_duration"
    Me.dtp_duration.ShowCheckBox = False
    Me.dtp_duration.ShowUpDown = True
    Me.dtp_duration.Size = New System.Drawing.Size(84, 24)
    Me.dtp_duration.SufixText = "Sufix Text"
    Me.dtp_duration.SufixTextVisible = True
    Me.dtp_duration.TabIndex = 3
    Me.dtp_duration.TextWidth = 0
    Me.dtp_duration.Value = New Date(2013, 2, 22, 0, 0, 0, 0)
    '
    'lbl_duration
    '
    Me.lbl_duration.AutoSize = True
    Me.lbl_duration.Location = New System.Drawing.Point(41, 65)
    Me.lbl_duration.Name = "lbl_duration"
    Me.lbl_duration.Size = New System.Drawing.Size(63, 13)
    Me.lbl_duration.TabIndex = 5
    Me.lbl_duration.Text = "xDuration"
    Me.lbl_duration.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'opt_working_day
    '
    Me.opt_working_day.AutoSize = True
    Me.opt_working_day.Location = New System.Drawing.Point(16, 38)
    Me.opt_working_day.Name = "opt_working_day"
    Me.opt_working_day.Size = New System.Drawing.Size(102, 17)
    Me.opt_working_day.TabIndex = 1
    Me.opt_working_day.TabStop = True
    Me.opt_working_day.Text = "xWorkingDay"
    Me.opt_working_day.UseVisualStyleBackColor = True
    '
    'opt_session
    '
    Me.opt_session.AutoSize = True
    Me.opt_session.Location = New System.Drawing.Point(16, 12)
    Me.opt_session.Name = "opt_session"
    Me.opt_session.Size = New System.Drawing.Size(76, 17)
    Me.opt_session.TabIndex = 0
    Me.opt_session.TabStop = True
    Me.opt_session.Text = "xSession"
    Me.opt_session.UseVisualStyleBackColor = True
    '
    'uc_entry_average_bet
    '
    Me.uc_entry_average_bet.DoubleValue = 0.0R
    Me.uc_entry_average_bet.IntegerValue = 0
    Me.uc_entry_average_bet.IsReadOnly = False
    Me.uc_entry_average_bet.Location = New System.Drawing.Point(6, 89)
    Me.uc_entry_average_bet.Name = "uc_entry_average_bet"
    Me.uc_entry_average_bet.PlaceHolder = Nothing
    Me.uc_entry_average_bet.Size = New System.Drawing.Size(175, 24)
    Me.uc_entry_average_bet.SufixText = "Sufix Text"
    Me.uc_entry_average_bet.SufixTextVisible = True
    Me.uc_entry_average_bet.TabIndex = 4
    Me.uc_entry_average_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.uc_entry_average_bet.TextValue = ""
    Me.uc_entry_average_bet.TextWidth = 91
    Me.uc_entry_average_bet.Value = ""
    Me.uc_entry_average_bet.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(809, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(377, 180)
    Me.uc_pr_list.TabIndex = 126
    Me.uc_pr_list.TerminalListHasValues = False
    '
    'frm_customers_playing
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1298, 656)
    Me.Name = "frm_customers_playing"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_customers_playing"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_order_by.ResumeLayout(False)
    Me.gb_order_by.PerformLayout()
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_duration_average.ResumeLayout(False)
    Me.gb_duration_average.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_order_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_provider_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_date_started As System.Windows.Forms.RadioButton
  Friend WithEvents opt_account As System.Windows.Forms.RadioButton
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents gb_duration_average As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_average_bet As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_duration As GUI_Controls.uc_date_picker
  Friend WithEvents opt_working_day As System.Windows.Forms.RadioButton
  Friend WithEvents opt_session As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_duration As System.Windows.Forms.Label
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
End Class
