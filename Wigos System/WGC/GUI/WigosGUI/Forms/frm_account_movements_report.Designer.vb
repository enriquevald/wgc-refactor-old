<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_account_movements_report
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dt_date_to = New GUI_Controls.uc_date_picker()
    Me.dt_date_from = New GUI_Controls.uc_date_picker()
    Me.btn_exit = New GUI_Controls.uc_button()
    Me.btn_generate_xml = New GUI_Controls.uc_button()
    Me.lbl_filename = New GUI_Controls.uc_text_field()
    Me.btn_generate_excel = New GUI_Controls.uc_button()
    Me.lbl_num_records = New GUI_Controls.uc_text_field()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dt_date_to)
    Me.gb_date.Controls.Add(Me.dt_date_from)
    Me.gb_date.Location = New System.Drawing.Point(14, 12)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(243, 80)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "fra_date"
    '
    'dt_date_to
    '
    Me.dt_date_to.Checked = True
    Me.dt_date_to.IsReadOnly = False
    Me.dt_date_to.Location = New System.Drawing.Point(2, 48)
    Me.dt_date_to.Name = "dt_date_to"
    Me.dt_date_to.ShowCheckBox = False
    Me.dt_date_to.ShowUpDown = False
    Me.dt_date_to.Size = New System.Drawing.Size(223, 24)
    Me.dt_date_to.SufixText = "Sufix Text"
    Me.dt_date_to.SufixTextVisible = True
    Me.dt_date_to.TabIndex = 1
    Me.dt_date_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dt_date_from
    '
    Me.dt_date_from.Checked = True
    Me.dt_date_from.IsReadOnly = False
    Me.dt_date_from.Location = New System.Drawing.Point(2, 16)
    Me.dt_date_from.Name = "dt_date_from"
    Me.dt_date_from.ShowCheckBox = False
    Me.dt_date_from.ShowUpDown = False
    Me.dt_date_from.Size = New System.Drawing.Size(223, 24)
    Me.dt_date_from.SufixText = "Sufix Text"
    Me.dt_date_from.SufixTextVisible = True
    Me.dt_date_from.TabIndex = 0
    Me.dt_date_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_exit.Location = New System.Drawing.Point(201, 153)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 5
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_generate_xml
    '
    Me.btn_generate_xml.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_generate_xml.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_generate_xml.Location = New System.Drawing.Point(105, 153)
    Me.btn_generate_xml.Name = "btn_generate_xml"
    Me.btn_generate_xml.Size = New System.Drawing.Size(90, 30)
    Me.btn_generate_xml.TabIndex = 4
    Me.btn_generate_xml.ToolTipped = False
    Me.btn_generate_xml.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'lbl_filename
    '
    Me.lbl_filename.AutoSize = True
    Me.lbl_filename.IsReadOnly = True
    Me.lbl_filename.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_filename.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_filename.Location = New System.Drawing.Point(20, 98)
    Me.lbl_filename.Name = "lbl_filename"
    Me.lbl_filename.Size = New System.Drawing.Size(271, 24)
    Me.lbl_filename.SufixText = "Sufix Text"
    Me.lbl_filename.SufixTextVisible = True
    Me.lbl_filename.TabIndex = 1
    Me.lbl_filename.TabStop = False
    Me.lbl_filename.TextWidth = 0
    Me.lbl_filename.Value = "xFilename"
    '
    'btn_generate_excel
    '
    Me.btn_generate_excel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_generate_excel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_generate_excel.Location = New System.Drawing.Point(9, 153)
    Me.btn_generate_excel.Name = "btn_generate_excel"
    Me.btn_generate_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_generate_excel.TabIndex = 3
    Me.btn_generate_excel.ToolTipped = False
    Me.btn_generate_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'lbl_num_records
    '
    Me.lbl_num_records.AutoSize = True
    Me.lbl_num_records.IsReadOnly = True
    Me.lbl_num_records.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_num_records.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_num_records.Location = New System.Drawing.Point(20, 122)
    Me.lbl_num_records.Name = "lbl_num_records"
    Me.lbl_num_records.Size = New System.Drawing.Size(271, 24)
    Me.lbl_num_records.SufixText = "Sufix Text"
    Me.lbl_num_records.SufixTextVisible = True
    Me.lbl_num_records.TabIndex = 2
    Me.lbl_num_records.TabStop = False
    Me.lbl_num_records.TextWidth = 0
    Me.lbl_num_records.Value = "xNumRecords"
    '
    'frm_account_movements_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_exit
    Me.ClientSize = New System.Drawing.Size(299, 190)
    Me.Controls.Add(Me.lbl_num_records)
    Me.Controls.Add(Me.btn_generate_excel)
    Me.Controls.Add(Me.lbl_filename)
    Me.Controls.Add(Me.btn_generate_xml)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.gb_date)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_account_movements_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_account_movements_report"
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dt_date_to As GUI_Controls.uc_date_picker
  Friend WithEvents dt_date_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents btn_generate_xml As GUI_Controls.uc_button
  Friend WithEvents lbl_filename As GUI_Controls.uc_text_field
  Friend WithEvents btn_generate_excel As GUI_Controls.uc_button
  Friend WithEvents lbl_num_records As GUI_Controls.uc_text_field
End Class
