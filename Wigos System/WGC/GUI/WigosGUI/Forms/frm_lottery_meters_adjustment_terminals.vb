﻿'-----------------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-----------------------------------------------------------------------------
'
' MODULE NAME:   frm_lottery_meters_adjustment_terminals
' DESCRIPTION:
' AUTHOR:        Fernando Jiménez
' CREATION DATE: 20-DIC-2017
'
' REVISION HISTORY:
'
' Date         Author       Description
' -----------  ------       ---------------------------------------------------------
' 20-DIC-2017  FJC          Initial version
' 19-JAN-2018  OMC          Error: WIGOS-7813 AGG-Lotery: Close working day
' 12-FEB-2017  FJC          WIGOS-8174 AGG - Terminals Screen doesn't save pending changes
'------------------------------------------------------------------------------------

#Region "Imports"

Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports WSI.Common.EGMMeterAdjustment

#End Region

Public Class frm_lottery_meters_adjustment_terminals
  Inherits frm_base_sel_edit

#Region "Constants"

  Private Const GRID_HEADERS_COUNT As Integer = 2

  'Index Columns
  Private Const GRID_COLUMNS_COUNT As Integer = 20

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 1
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 2
  Private Const GRID_COLUMN_PROVIDER_NAME As Integer = 3
  Private Const GRID_COLUMN_MONEY_GAIN As Integer = 4
  Private Const GRID_COLUMN_CREDIT_COIN_IN As Integer = 5
  Private Const GRID_COLUMN_MONEY_COIN_IN As Integer = 6
  Private Const GRID_COLUMN_MONEY_COIN_IN_INCREMENT As Integer = 7
  Private Const GRID_COLUMN_CREDIT_COIN_OUT As Integer = 8
  Private Const GRID_COLUMN_MONEY_COIN_OUT As Integer = 9
  Private Const GRID_COLUMN_MONEY_COIN_OUT_INCREMENT As Integer = 10
  Private Const GRID_COLUMN_CREDIT_JACKPOT As Integer = 11
  Private Const GRID_COLUMN_MONEY_JACKPOT As Integer = 12
  Private Const GRID_COLUMN_MONEY_JACKPOT_INCREMENT As Integer = 13
  Private Const GRID_COLUMN_GUI_USER As Integer = 14
  Private Const GRID_COLUMN_ANOMALY_ID As Integer = 15
  Private Const GRID_COLUMN_ANOMALY_NAME As Integer = 16
  Private Const GRID_COLUMN_MODIFIED As Integer = 17
  Private Const GRID_COLUMN_VALIDATED As Integer = 18
  Private Const GRID_COLUMN_VALIDATED_EXCEL As Integer = 19

  'SQL Columns
  Private Const SQL_COLUMN_WORKING_DAY As Integer = 0
  Private Const SQL_COLUMN_SITE_ID As Integer = 1
  Private Const SQL_COLUMN_WORKING_DAY_STATUS As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 4
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 5
  Private Const SQL_COLUMN_MONEY_GAIN As Integer = 6
  Private Const SQL_COLUMN_CREDIT_COIN_IN As Integer = 7
  Private Const SQL_COLUMN_CREDIT_COIN_OUT As Integer = 8
  Private Const SQL_COLUMN_CREDIT_JACKPOT As Integer = 9
  Private Const SQL_COLUMN_MONEY_COIN_IN As Integer = 10
  Private Const SQL_COLUMN_MONEY_COIN_OUT As Integer = 11
  Private Const SQL_COLUMN_MONEY_JACKPOT As Integer = 12
  Private Const SQL_COLUMN_MONEY_COIN_IN_INCREMENT As Integer = 13
  Private Const SQL_COLUMN_MONEY_COIN_OUT_INCREMENT As Integer = 14
  Private Const SQL_COLUMN_MONEY_JACKPOT_INCREMENT As Integer = 15
  Private Const SQL_COLUMN_WARNING As Integer = 16
  Private Const SQL_COLUMN_MODIFIED As Integer = 17
  Private Const SQL_COLUMN_STATUS As Integer = 18
  Private Const SQL_COLUMN_USER_VALIDATED_DATETIME As Integer = 19
  Private Const SQL_COLUMN_LAST_UPDATED_DATETIME As Integer = 20
  Private Const SQL_COLUMN_LAST_UPDATED_USER_ID As Integer = 21
  Private Const SQL_COLUMN_LAST_UPDATED_USER_NAME As Integer = 22
  Private Const SQL_COLUMN_ARE_MISSING_PERIOD_METERS As Integer = 23
  Private Const SQL_COLUMN_NUM_BIG_INCREMENTS As Integer = 24

  'Width
  Private Const GRID_WIDTH_ZERO As Integer = 1
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_TERMINAL_ID As Integer = 0
  Private Const GRID_WIDTH_TERMINAL_NAME As Integer = 2000
  Private Const GRID_WIDTH_PROVIDER_NAME As Integer = 2000
  Private Const GRID_WIDTH_GAIN As Integer = 2000
  Private Const GRID_WIDTH_COIN_IN As Integer = 1500
  Private Const GRID_WIDTH_COIN_OUT As Integer = 1500
  Private Const GRID_WIDTH_JACKPOT As Integer = 1500
  Private Const GRID_WIDTH_GUI_USER_NAME As Integer = 1500
  Private Const GRID_WIDTH_ANOMALY As Integer = 2100
  Private Const GRID_WIDTH_MODIFIED As Integer = 1150
  Private Const GRID_WIDTH_VALIDATED As Integer = 1150
  Private Const GRID_WIDTH_VALIDATED_EXCEL As Integer = 1000

#End Region

#Region "Members"
  Private m_is_multisite_center As Boolean

  Private m_query_as_datatable As DataTable
  Private m_readonly As Boolean
  Private m_site_id As Integer
  Private m_site_name As String
  Private m_working_day As Integer

  ' Totals (Absolut meter)
  Private m_total_money_coin_in As Decimal
  Private m_total_money_coin_out As Decimal
  Private m_total_money_jackpot As Decimal

  ' Totals (Increments meter)
  Private m_total_credit_gain_increment As Long
  Private m_total_credit_coin_in_increment As Long
  Private m_total_credit_coin_out_increment As Long
  Private m_total_credit_jackpot_increment As Long

  Private m_total_money_gain_increment As Decimal
  Private m_total_money_coin_in_increment As Decimal
  Private m_total_money_coin_out_increment As Decimal
  Private m_total_money_jackpot_increment As Decimal

  ' For report filters
  Private m_report_site As String
  Private m_report_working_day As Date
  Private m_report_show_in As String
  Private m_report_show_with_anomalies As Boolean

  ' Buttons
  Private m_save_button As uc_button
  Private m_close_button As uc_button

  Private ReadOnly m_is_edit_mode As Boolean
  Private m_saved_almost_once As Boolean

  Private m_previous_working_days_closed As Boolean
  Private m_first_time_previous_working_days_closed As Boolean = True

  Private m_text_yes As String
  Private m_text_no As String

  Private m_is_first_time As Boolean
  Private m_initial_gain As String
  Private m_initial_coin_in As String
  Private m_initial_coin_out As String
  Private m_initial_jackpot As String

  Private m_SAS_period_meters_records_per_day As Int32

#End Region

#Region "Properties"
  Property SaveAlmostOnce
    Get
      Return m_saved_almost_once
    End Get
    Set(value)
      m_saved_almost_once = value
    End Set
  End Property

  ReadOnly Property ArePreviousWorkingDayClosed
    Get
      If m_first_time_previous_working_days_closed Then
        m_previous_working_days_closed = PreviousWorkingDaysAreClosed()
        m_first_time_previous_working_days_closed = False
      End If

      Return m_previous_working_days_closed
    End Get
  End Property
#End Region

#Region "Constructors"

  Public Sub New(ByVal FormId As ENUM_FORM)
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Initialize members
    Me.FormId = FormId

    MyBase.GUI_SetFormId()

    Me.m_save_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0)
    Me.m_close_button = Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1)

    Me.m_is_edit_mode = True
    m_is_first_time = True
  End Sub

#End Region

#Region "Overrides"

  ''' <summary>
  ''' Initializes the form id.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_SetFormId() ' GUI_SetFormId
    Me.FormId = ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TERMINALS
    MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ''' <summary>
  ''' Initialize every form control
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8814)

    'Is multisite center?
    m_is_multisite_center = WSI.Common.Misc.IsCenter

    m_text_yes = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6692) 'Yes
    m_text_no = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6693)  'No

    m_SAS_period_meters_records_per_day = GetSASPeriodMetersRecordsPerDay()

    Call SetReadOnlyMode()

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True

    ' Button close Working day
    m_close_button.Visible = True
    m_close_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8940)
    m_close_button.Type = uc_button.ENUM_BUTTON_TYPE.USER
    m_close_button.Size = New System.Drawing.Size(m_close_button.Width, m_close_button.Height + 15)
    m_close_button.Enabled = False

    ' Button Save Data
    m_save_button.Visible = True
    m_save_button.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2262)
    m_save_button.Enabled = False

    ' Button Cancel
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)

    ' Site
    Me.gb_site_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(380)  '"xSite"
    Me.uc_entry_site.Text = GLB_NLS_GUI_CONFIGURATION.GetString(380) '"xSite"
    Me.uc_entry_site.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 100)
    Me.uc_entry_site.TextValue = Me.m_site_name
    Me.uc_entry_site.Enabled = False

    ' Date
    Me.gb_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
    Me.uc_working_day.Value = Me.GetDateFromInt(Me.m_working_day)
    Me.uc_working_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8944)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(346)
    Call SetStatusTextAndColor()

    ' Credit & Currency
    ' The from will work always by credits. The group box that allows to choose credits or currency won't be visible
    Me.gb_credit_currency.Visible = False
    Me.opt_Credit.Visible = False
    Me.opt_Money.Visible = False
    Me.gb_credit_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8624)
    Me.opt_Credit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3334)
    Me.opt_Money.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)

    ' Show Terminals with anomalies
    Me.chk_show_only_with_anomalies.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8939)

    ' Show Increments
    Me.chk_show_increments.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9008)

    ' Totals
    Me.lbl_gain_caption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8824)
    Me.lbl_coin_in_caption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8825)
    Me.lbl_coin_out_caption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8826)
    Me.lbl_jackpot_caption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8827)

    InitializeTotalsCaptions()

    Me.SetDefaultValues()
    Me.GUI_StyleSheet()

    Call SwitchSiteFilter()

    Me.ManageButtonsEnabled()

    Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

    If Not CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read Then
      Me.Width -= 100
      Me.panel_filter.Height -= 25
    End If
  End Sub ' GUI_InitControls

  ''' <summary>
  ''' Show/hide site filter control depending if we are center or not
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SwitchSiteFilter()
    If (Not Me.m_is_multisite_center) Then
      Dim _offset_x As Integer

      gb_site_name.Visible = False

      'Get offset to move rest of filters
      _offset_x = gb_working_day.Location.X - gb_site_name.Location.X

      'Move to the left
      gb_working_day.Location = New Point(gb_working_day.Location.X - _offset_x, gb_working_day.Location.Y)
      gb_status.Location = New Point(gb_status.Location.X - _offset_x, gb_status.Location.Y)
      gb_credit_currency.Location = New Point(gb_credit_currency.Location.X - _offset_x, gb_credit_currency.Location.Y)
    End If
  End Sub

  ''' <summary>
  ''' Set status text and color
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetStatusTextAndColor()
    If IsWorkingDayClosed() Then
      Me.lbl_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4350).ToUpper
      Me.lbl_status.ForeColor = Color.Red
    Else
      Me.lbl_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4349).ToUpper
      Me.lbl_status.ForeColor = Color.Green
    End If
  End Sub

  Private Sub InitializeTotalsCaptions()
    Me.panel_totals.Visible = CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read

    Me.lblTotales.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2306).ToUpper

    Me.lblTotalGain.Text = "0"
    Me.lblTotalGain.TextAlign = ContentAlignment.MiddleRight
    Me.lblTotalGain.ForeColor = System.Drawing.Color.Blue

    Me.lblTotalCoinIn.Text = "0"
    Me.lblTotalCoinIn.TextAlign = ContentAlignment.MiddleRight
    Me.lblTotalCoinIn.ForeColor = System.Drawing.Color.Blue

    Me.lblTotalCoinOut.Text = "0"
    Me.lblTotalCoinOut.TextAlign = ContentAlignment.MiddleRight
    Me.lblTotalCoinOut.ForeColor = System.Drawing.Color.Blue

    Me.lblTotalJackPot.Text = "0"
    Me.lblTotalJackPot.TextAlign = ContentAlignment.MiddleRight
    Me.lblTotalJackPot.ForeColor = System.Drawing.Color.Blue
  End Sub

  ''' <summary>
  ''' Initialize all form filters with their default values
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_FilterReset()

    SetDefaultValues()

  End Sub ' GUI_FilterReset

  ''' <summary>
  ''' Sets initial focus
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.opt_Credit
  End Sub ' GUI_SetInitialFocus

  ''' <summary>
  ''' Event before row add
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Return CalculateTotals(DbRow)
  End Function

  ''' <summary>
  ''' Calculate totals
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <remarks></remarks>
  Public Function CalculateTotals(ByVal DbRow As CLASS_DB_ROW) As Boolean
    ' Gain - Increment
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_GAIN) Then
      Me.m_total_money_gain_increment += DbRow.Value(SQL_COLUMN_MONEY_GAIN)
    End If

    ' Coin In - Increment
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_IN_INCREMENT) Then
      Me.m_total_money_coin_in_increment += DbRow.Value(SQL_COLUMN_MONEY_COIN_IN_INCREMENT)
    End If

    ' Coin Out - Increment
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_OUT_INCREMENT) Then
      Me.m_total_money_coin_out_increment += DbRow.Value(SQL_COLUMN_MONEY_COIN_OUT_INCREMENT)
    End If

    ' JackPot - Increment
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_JACKPOT_INCREMENT) Then
      Me.m_total_money_jackpot_increment += DbRow.Value(SQL_COLUMN_MONEY_JACKPOT_INCREMENT)
    End If

    ' Coin In
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_IN) Then
      Me.m_total_money_coin_in += DbRow.Value(SQL_COLUMN_MONEY_COIN_IN)
    End If

    ' Coin Out
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_OUT) Then
      Me.m_total_money_coin_out += DbRow.Value(SQL_COLUMN_MONEY_COIN_OUT)
    End If

    ' JackPot
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_JACKPOT) Then
      Me.m_total_money_jackpot += DbRow.Value(SQL_COLUMN_MONEY_JACKPOT)
    End If

    Return True
  End Function

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _color_row As System.Drawing.Color
    Dim _anomaly_description As String
    Dim _anomaly_id As Integer
    Dim _status As Integer
    Dim _are_there_missing_period_records As Boolean
    Dim _are_there_big_increments As Boolean

    _color_row = Color.White 'ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
    _anomaly_description = String.Empty
    _status = 0

    ' Terminal Id
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_ID) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)
    End If

    ' Terminal Name
    If Not DbRow.IsNull(SQL_COLUMN_TERMINAL_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    End If

    ' Provider Name
    If Not DbRow.IsNull(SQL_COLUMN_PROVIDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER_NAME).Value = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)
    End If

    ' Coin In (Credit)
    If Not DbRow.IsNull(SQL_COLUMN_CREDIT_COIN_IN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_COIN_IN).Value = DbRow.Value(SQL_COLUMN_CREDIT_COIN_IN)
    End If

    ' Coin Out (Credit)
    If Not DbRow.IsNull(SQL_COLUMN_CREDIT_COIN_OUT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_COIN_OUT).Value = DbRow.Value(SQL_COLUMN_CREDIT_COIN_OUT)
    End If

    ' JackPot (Credit)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_JACKPOT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CREDIT_JACKPOT).Value = DbRow.Value(SQL_COLUMN_CREDIT_JACKPOT)
    End If

    ' Gain (Money)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_GAIN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_GAIN).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_GAIN), _
                                                                                         ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Coin In (Money)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_IN) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_COIN_IN).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_COIN_IN), _
                                                                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Coin In (Money) (Increment)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_IN_INCREMENT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_COIN_IN_INCREMENT).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_COIN_IN_INCREMENT), _
                                                                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Coin Out (Money)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_OUT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_COIN_OUT).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_COIN_OUT), _
                                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' Coin Out (Money) (Increment)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_COIN_OUT_INCREMENT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_COIN_OUT_INCREMENT).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_COIN_OUT_INCREMENT), _
                                                                                             ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' JackPot (Money)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_JACKPOT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_JACKPOT).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_JACKPOT), _
                                                                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' JackPot (Money) (Increment)
    If Not DbRow.IsNull(SQL_COLUMN_MONEY_JACKPOT_INCREMENT) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MONEY_JACKPOT_INCREMENT).Value = GUI_GetCurrencyValue_Format(DbRow.Value(SQL_COLUMN_MONEY_JACKPOT_INCREMENT), _
                                                                                            ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End If

    ' GUI User Name
    If Not DbRow.IsNull(SQL_COLUMN_LAST_UPDATED_USER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_GUI_USER).Value = DbRow.Value(SQL_COLUMN_LAST_UPDATED_USER_NAME)
    End If

    'Anomalies
    _are_there_missing_period_records = IIf(DbRow.IsNull(SQL_COLUMN_ARE_MISSING_PERIOD_METERS), False, DbRow.Value(SQL_COLUMN_ARE_MISSING_PERIOD_METERS))
    _are_there_big_increments = IIf(DbRow.IsNull(SQL_COLUMN_NUM_BIG_INCREMENTS), False, (DbRow.Value(SQL_COLUMN_NUM_BIG_INCREMENTS) > 0))

    ' Warning (Anomaly - Name & Id) & Missing Period Records & Big Increments
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ANOMALY_ID).Value = 0
    If ((Not DbRow.IsNull(SQL_COLUMN_WARNING)) OrElse (_are_there_missing_period_records) OrElse (_are_there_big_increments)) Then
      _anomaly_id = IIf(DbRow.IsNull(SQL_COLUMN_WARNING), 0, DbRow.Value(SQL_COLUMN_WARNING))

      EGMMeterAdjustmentCommon.GetColorByPriorityWarning(_anomaly_id, _are_there_missing_period_records, _are_there_big_increments, _color_row, _anomaly_description)

      If ((_anomaly_id = 0) AndAlso _are_there_big_increments) Then
        _anomaly_id = EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DISCARDED
      End If

      Me.Grid.Cell(RowIndex, GRID_COLUMN_ANOMALY_NAME).Value = _anomaly_description   ' Anomaly description
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ANOMALY_ID).Value = _anomaly_id              ' Anomaly Id

      Me.Grid.Row(RowIndex).BackColor = _color_row
    End If

    If (Not String.IsNullOrEmpty(_anomaly_description)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INDEX).Value = "*"
    End If

    ' Modified
    If Not DbRow.IsNull(SQL_COLUMN_MODIFIED) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MODIFIED).Value = GetBooleanText(DbRow.Value(SQL_COLUMN_MODIFIED))
    End If

    ' Status (Validated; System, etc)
    If Not DbRow.IsNull(SQL_COLUMN_STATUS) Then
      _status = DbRow.Value(SQL_COLUMN_STATUS)

      If EGMMeterAdjustmentCommon.GetMaskFlagBitEgm(_status, EGMMeterAdjustmentCommon.EGMStatus.VALIDATED) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALIDATED).Value = uc_grid.GRID_CHK_CHECKED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALIDATED_EXCEL).Value = m_text_yes
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALIDATED).Value = uc_grid.GRID_CHK_UNCHECKED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_VALIDATED_EXCEL).Value = m_text_no
      End If
    End If

    Return True
  End Function ' GUI_SetupRow

  ''' <summary>
  ''' Perform final processing for the grid data (totalisator row)
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_AfterLastRow() ' GUI_AfterLastRow

    ' If the chech Show only terminal with anolamies are checked, the totals showed must to be the totals of all the working day, not only the totals of the terminals with anomalies
    If (m_is_first_time) Then
      m_initial_gain = GUI_GetCurrencyValue_Format(m_total_money_gain_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_initial_coin_in = GUI_GetCurrencyValue_Format(m_total_money_coin_in_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_initial_coin_out = GUI_GetCurrencyValue_Format(m_total_money_coin_out_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_initial_jackpot = GUI_GetCurrencyValue_Format(m_total_money_jackpot_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      m_is_first_time = False
    Else
      lblTotalGain.Text = m_initial_gain
      lblTotalCoinIn.Text = m_initial_coin_in
      lblTotalCoinOut.Text = m_initial_coin_out
      lblTotalJackPot.Text = m_initial_jackpot
    End If

    DrawTotal()
    RefreshCreditMoney()
    ManageButtonsEnabled()

  End Sub ' GUI_AfterLastRow

  ''' <summary>
  '''
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeFirstRow()
    ResetCounters()
  End Sub

  ''' <summary>
  ''' Select first row only if grid has more than one row
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_SelectFirstRow() As Boolean
    Return Me.Grid.NumRows > 1
  End Function ' GUI_SelectFirstRow

  ''' <summary>
  ''' Enable or disable Movements Button if grid has more than one row
  ''' </summary>
  ''' <param name="SelectedRow"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    If (Me.Grid.NumRows <= 0) Then
      Return
    End If
  End Sub ' GUI_RowSelectedEvent

  ''' <summary>
  ''' Set proper values for form filters being sent to the report
  ''' </summary>
  ''' <param name="PrintData"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA) ' GUI_ReportFilter
    If (Me.m_is_multisite_center) Then
      ' Site
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(380), Me.m_report_site)
    End If

    ' Working Day
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6177), Me.m_report_working_day)

    ' Show Terminals with anomalies
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8939), GetBooleanText(Me.m_report_show_with_anomalies))

    ' Credit & Currency
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8624), _
                        IIf(Me.opt_Credit.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3334), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)))

    If CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read Then
      ' Total Gain
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8824), _
                          GUI_GetCurrencyValue_Format(Me.m_total_money_gain_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

      ' Total Coin In
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883) & " (Inc) " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8825), _
                          GUI_GetCurrencyValue_Format(Me.m_total_money_coin_in_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

      ' Total Coin Out
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883) & " (Inc) " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8826), _
                          GUI_GetCurrencyValue_Format(Me.m_total_money_coin_out_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))

      ' Total Jackpot
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6883) & " (Inc) " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(8827), _
                          GUI_GetCurrencyValue_Format(Me.m_total_money_jackpot_increment, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT))
    End If
  End Sub ' GUI_ReportFilter

  ''' <summary>
  ''' Set texts corresponding to the provided filter values for the report
  ''' </summary>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ReportUpdateFilters()
    ' Site Name
    m_report_site = Me.uc_entry_site.TextValue

    ' Working Day
    m_report_working_day = Me.uc_working_day.Value

    ' Show in (credit or Money)
    m_report_show_in = IIf(Me.opt_Credit.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3334), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057))

    ' Show Only Terminals woth anomalies
    m_report_show_with_anomalies = Me.chk_show_only_with_anomalies.Checked

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Executed before start Edition
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <param name="EditionCanceled"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean)
    If Me.Grid.Cell(Row, GRID_COLUMN_TERMINAL_ID).Value = -1 OrElse Me.m_readonly Then
      EditionCanceled = True
    Else
      EditionCanceled = False
    End If
  End Sub ' GUI_BeforeStartEditionEvent


  ''' <summary>
  ''' Executed before start Edition
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) ' GUI_CellDataChangedEvent
    Dim _new_status_value_grid As Boolean
    Dim _value_status_db As Integer

    If Me.m_query_as_datatable Is Nothing OrElse Me.m_readonly Then

      Return
    End If

    Select Case Column
      Case GRID_COLUMN_VALIDATED
        ' Validated
        _new_status_value_grid = Me.Grid.Cell(Row, Column).Value
        _new_status_value_grid = Not _new_status_value_grid

        _value_status_db = Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_STATUS)

        EGMMeterAdjustmentCommon.SetMaskFlagBit(_value_status_db, EGMMeterAdjustmentCommon.EGMStatus.VALIDATED, _new_status_value_grid)

        Me.m_query_as_datatable.Rows(Row)(SQL_COLUMN_STATUS) = _value_status_db

        ' Column validated (Only for Excel Report)
        Me.Grid.Cell(Row, GRID_COLUMN_VALIDATED_EXCEL).Value = GetBooleanText(_new_status_value_grid)

        m_close_button.Enabled = ArePreviousWorkingDayClosed AndAlso AreAllAnomaliesValidated() AndAlso WorkingDayISNotCurrentDay()
    End Select
  End Sub ' GUI_CellDataChangedEvent

  ''' <summary>
  ''' Process clicks on data grid (doible-clicks) and select button
  ''' </summary>
  ''' <param name="ButtonId"></param>
  ''' <remarks></remarks>
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON) ' GUI_ButtonClick
    Dim OldStatus As WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus

    OldStatus = EGMWorkingDayStatus.NONE

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0 ' Save
        If Not SavePendingChanges() Then
          NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return
        End If

        ' TODO: Transaction not atomic. ==> In some GUI screens, it works like this
        AuditChanges(OldStatus)

        Me.AcceptChanges()

        NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), _
                   ENUM_MB_TYPE.MB_TYPE_INFO, _
                   ENUM_MB_BTN.MB_BTN_OK)

      Case ENUM_BUTTON.BUTTON_CUSTOM_1 ' Close Working Day
        If Not CloseWorkingDayManagement(OldStatus) Then
          NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return
        End If

        ' TODO: Transaction not atomic. ==> In some GUI screens, it works like this
        AuditChanges(OldStatus)

        Me.AcceptChanges()

        Me.Close()

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        m_is_first_time = True
        GUI_RowSelectedEvent(0)

        Me.ManageButtonsEnabled()

      Case ENUM_BUTTON.BUTTON_SELECT ' SELECT BUTTON
        m_is_first_time = True
        Me.ShowCountersForm()


      Case ENUM_BUTTON.BUTTON_FILTER_RESET
        Me.SetDefaultValues()

      Case ENUM_BUTTON.BUTTON_EXCEL _
         , ENUM_BUTTON.BUTTON_PRINT
        Me.Grid.Column(GRID_COLUMN_VALIDATED_EXCEL).Width = GRID_WIDTH_VALIDATED_EXCEL

      Case Else
    End Select

    Call MyBase.GUI_ButtonClick(ButtonId)

    If ((ButtonId = ENUM_BUTTON.BUTTON_EXCEL) OrElse (ButtonId = ENUM_BUTTON.BUTTON_PRINT)) Then
      Me.Grid.Column(GRID_COLUMN_VALIDATED_EXCEL).Width = GRID_WIDTH_ZERO
    End If

    Call ManageButtonsEnabled()
  End Sub ' GUI_ButtonClick

  ''' <summary>
  ''' Executed just before closing form
  ''' </summary>
  ''' <param name="CloseCanceled"></param>
  ''' <remarks></remarks>
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
      CloseCanceled = Not DiscardChanges()
    End If
  End Sub ' GUI_Closing

#End Region

#Region "Private"

  ''' <summary>
  ''' Sets the default date values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetDefaultValues()
    Me.chk_show_only_with_anomalies.Checked = False
    Me.opt_Credit.Checked = True
  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Define all Main Grid Columns
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()
    Dim _str_name_header As String

    _str_name_header = String.Empty

    With Me.Grid
      .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT, True)
      .Sortable = True
      .SelectionMode = SelectionMode.One

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Terminal Id.
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_TERMINAL_ID).Width = GRID_WIDTH_TERMINAL_ID

      ' Terminal Name.
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6636)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Provider Name.
      .Column(GRID_COLUMN_PROVIDER_NAME).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_PROVIDER_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7236)
      .Column(GRID_COLUMN_PROVIDER_NAME).Width = GRID_WIDTH_PROVIDER_NAME
      .Column(GRID_COLUMN_PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Gain (Money).
      .Column(GRID_COLUMN_MONEY_GAIN).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_MONEY_GAIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8824)
      .Column(GRID_COLUMN_MONEY_GAIN).Width = IIf(CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read, GRID_WIDTH_GAIN, 0)
      .Column(GRID_COLUMN_MONEY_GAIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin In (Credit).
      .Column(GRID_COLUMN_CREDIT_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_CREDIT_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8825)
      .Column(GRID_COLUMN_CREDIT_COIN_IN).Width = IIf(Me.opt_Credit.Checked, GRID_WIDTH_COIN_IN, 0)
      .Column(GRID_COLUMN_CREDIT_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin In (Money).
      .Column(GRID_COLUMN_MONEY_COIN_IN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_MONEY_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8825)
      .Column(GRID_COLUMN_MONEY_COIN_IN).Width = IIf(Me.opt_Money.Checked, GRID_WIDTH_COIN_IN, 0)
      .Column(GRID_COLUMN_MONEY_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin In (Money) (Increment).
      .Column(GRID_COLUMN_MONEY_COIN_IN_INCREMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_MONEY_COIN_IN_INCREMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9009)
      .Column(GRID_COLUMN_MONEY_COIN_IN_INCREMENT).Width = IIf(chk_show_increments.Checked, GRID_WIDTH_COIN_IN, 0)
      .Column(GRID_COLUMN_MONEY_COIN_IN_INCREMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin Out (Credit).
      .Column(GRID_COLUMN_CREDIT_COIN_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_CREDIT_COIN_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8826)
      .Column(GRID_COLUMN_CREDIT_COIN_OUT).Width = IIf(Me.opt_Credit.Checked, GRID_WIDTH_COIN_OUT, 0)
      .Column(GRID_COLUMN_CREDIT_COIN_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin Out (Money).
      .Column(GRID_COLUMN_MONEY_COIN_OUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_MONEY_COIN_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8826)
      .Column(GRID_COLUMN_MONEY_COIN_OUT).Width = IIf(Me.opt_Money.Checked, GRID_WIDTH_COIN_OUT, 0)
      .Column(GRID_COLUMN_MONEY_COIN_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Coin Out (Money) (Increment).
      .Column(GRID_COLUMN_MONEY_COIN_OUT_INCREMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_MONEY_COIN_OUT_INCREMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9010)
      .Column(GRID_COLUMN_MONEY_COIN_OUT_INCREMENT).Width = IIf(chk_show_increments.Checked, GRID_WIDTH_COIN_OUT, 0)
      .Column(GRID_COLUMN_MONEY_COIN_OUT_INCREMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' JackPot (Credit).
      .Column(GRID_COLUMN_CREDIT_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_CREDIT_JACKPOT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8827)
      .Column(GRID_COLUMN_CREDIT_JACKPOT).Width = IIf(Me.opt_Credit.Checked, GRID_WIDTH_JACKPOT, 0)
      .Column(GRID_COLUMN_CREDIT_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' JackPot (Money).
      .Column(GRID_COLUMN_MONEY_JACKPOT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_MONEY_JACKPOT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8827)
      .Column(GRID_COLUMN_MONEY_JACKPOT).Width = IIf(Me.opt_Money.Checked, GRID_WIDTH_JACKPOT, 0)
      .Column(GRID_COLUMN_MONEY_JACKPOT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' JackPot (Money) (Increment).
      .Column(GRID_COLUMN_MONEY_JACKPOT_INCREMENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973)
      .Column(GRID_COLUMN_MONEY_JACKPOT_INCREMENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9011)
      .Column(GRID_COLUMN_MONEY_JACKPOT_INCREMENT).Width = IIf(chk_show_increments.Checked, GRID_WIDTH_JACKPOT, 0)
      .Column(GRID_COLUMN_MONEY_JACKPOT_INCREMENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' GUI User
      .Column(GRID_COLUMN_GUI_USER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_GUI_USER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7786)
      .Column(GRID_COLUMN_GUI_USER).Width = GRID_WIDTH_GUI_USER_NAME
      .Column(GRID_COLUMN_GUI_USER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Anomaly Id.
      .Column(GRID_COLUMN_ANOMALY_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_ANOMALY_ID).Header(1).Text = "xAnomaliaId"
      .Column(GRID_COLUMN_ANOMALY_ID).Width = 0
      .Column(GRID_COLUMN_ANOMALY_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Anomaly Name.
      .Column(GRID_COLUMN_ANOMALY_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_ANOMALY_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8828)
      .Column(GRID_COLUMN_ANOMALY_NAME).Width = GRID_WIDTH_ANOMALY
      .Column(GRID_COLUMN_ANOMALY_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Modified.
      .Column(GRID_COLUMN_MODIFIED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_MODIFIED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8941)
      .Column(GRID_COLUMN_MODIFIED).Width = GRID_WIDTH_MODIFIED
      .Column(GRID_COLUMN_MODIFIED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Validated
      .Column(GRID_COLUMN_VALIDATED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_VALIDATED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8942)
      .Column(GRID_COLUMN_VALIDATED).Width = GRID_WIDTH_VALIDATED
      .Column(GRID_COLUMN_VALIDATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_VALIDATED).IsColumnPrintable = False
      .Column(GRID_COLUMN_VALIDATED).IsMerged = False
      .Column(GRID_COLUMN_VALIDATED).Editable = True
      .Column(GRID_COLUMN_VALIDATED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Validated (Only for Excel Report)
      .Column(GRID_COLUMN_VALIDATED_EXCEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
      .Column(GRID_COLUMN_VALIDATED_EXCEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8942)
      .Column(GRID_COLUMN_VALIDATED_EXCEL).Width = 1
      .Column(GRID_COLUMN_VALIDATED_EXCEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_VALIDATED_EXCEL).IsColumnPrintable = True
    End With
  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Ask for custom datatable
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.CUSTOM_DATATABLE
  End Function

  ''' <summary>
  ''' Build an SQL query from conditions set in the filters
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  '''
  Protected Overrides Function GUI_GetCustomDataTable() As DataTable
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    ' Declare temporal table
    _sb.AppendLine("DECLARE @TEMP_BIG_INCREMENT_TABLE TABLE                                                                     ")
    _sb.AppendLine("(                                                                                                           ")
    _sb.AppendLine("    TBIT_DENOMINATION     DECIMAL(19, 2)                                                                    ")
    _sb.AppendLine("  , TBIT_MAX_VALUE        BIGINT                                                                            ")
    _sb.AppendLine(");                                                                                                          ")
    _sb.AppendLine("")

    'Insert into temporal table the big increments GP parsed and converted
    _sb.AppendLine("INSERT INTO   @TEMP_BIG_INCREMENT_TABLE                                                                     ")
    _sb.AppendLine("  SELECT CONVERT(DECIMAL(19, 2), DENOMINATION)                                                              ")
    _sb.AppendLine("       , CONVERT(BIGINT, MAXVALUE)                                                                          ")
    _sb.AppendLine("    FROM (SELECT LEFT(SST_VALUE, CHARINDEX(':', SST_VALUE) - 1)  AS  DENOMINATION                           ")
    _sb.AppendLine("               , LTRIM(RIGHT(SST_VALUE, LEN(SST_VALUE) - CHARINDEX(':', SST_VALUE) ))  AS  MAXVALUE         ")
    _sb.AppendLine("            FROM DBO.SPLITSTRINGINTOTABLE((SELECT   GP_KEY_VALUE                                            ")
    _sb.AppendLine("                                             FROM   GENERAL_PARAMS                                          ")
    _sb.AppendLine("                                            WHERE   GP_GROUP_KEY    =  'AGG'                                ")
    _sb.AppendLine("                                              AND   GP_SUBJECT_KEY  =  'MaxValuesOfBigIncrement')           ")
    _sb.AppendLine("                                           ,';', 1)                                                         ")
    _sb.AppendLine("         ) AS TT                                                                                            ")

    _sb.AppendLine("    SELECT    EMD_WORKING_DAY                                              AS  WORKING_DAY                  ")
    _sb.AppendLine("            , EMD_SITE_ID                                                  AS  SITE_ID                      ")
    _sb.AppendLine("            , ED_STATUS                                                    AS  WORKING_DAY_STATUS           ")
    _sb.AppendLine("            , EMD_TERMINAL_ID                                              AS  TERMINAL_ID                  ")
    _sb.AppendLine("            , TE_NAME                                                      AS  TERMINAL_NAME                ")
    _sb.AppendLine("            , PV_NAME                                                      AS  PROVIDER_NAME                ")
    _sb.AppendLine("            , ISNULL(ComputedGain.MONEY_GAIN , 0)                          AS  MONEY_GAIN                   ")
    _sb.AppendLine("            , EMD_MC_0000                                                  AS  CREDIT_COIN_IN               ")  ' Coin In  (Credit)
    _sb.AppendLine("            , EMD_MC_0001                                                  AS  CREDIT_COIN_OUT              ")  ' Coin Out (Credit)
    _sb.AppendLine("            , EMD_MC_0002                                                  AS  CREDIT_JACKPOT               ")  ' JackPot  (Credit)
    _sb.AppendLine("            , EMD_MC_0000 * ISNULL(TE_SAS_ACCOUNTING_DENOM, 0.01)          AS  MONEY_COIN_IN                ")  ' Coin In  (Money)
    _sb.AppendLine("            , EMD_MC_0001 * ISNULL(TE_SAS_ACCOUNTING_DENOM, 0.01)          AS  MONEY_COIN_OUT               ")  ' Coin Out (Money)
    _sb.AppendLine("            , EMD_MC_0002 * ISNULL(TE_SAS_ACCOUNTING_DENOM, 0.01)          AS  MONEY_JACKPOT                ")  ' JackPot  (Money)
    _sb.AppendLine("            , ISNULL(ComputedGain.COIN_IN_INCREMENT, 0.00)                 AS  MONEY_INCREMENT_COIN_IN      ")
    _sb.AppendLine("            , ISNULL(ComputedGain.COIN_OUT_INCREMENT, 0.00)                AS  MONEY_INCREMENT_COIN_OUT     ")
    _sb.AppendLine("            , ISNULL(ComputedGain.JACKPOT_INCREMENT, 0.00)                 AS  MONEY_INCREMENT_JACKPOT      ")
    _sb.AppendLine("            , EMD_WARNING                                                  AS  ANOMALIA                     ")
    _sb.AppendLine("            , EMD_USER_PERIOD_MODIFIED                                     AS  MODIFIED                     ")  ' Modified (Si / No)
    _sb.AppendLine("            , EMD_STATUS                                                   AS  STATUS                       ")  ' Status (System, Validated Not Validated)
    _sb.AppendLine("            , EMD_USER_VALIDATED_DATETIME                                  AS  VALIDATED_DATETIME           ")  ' DateTime
    _sb.AppendLine("            , EMD_LAST_UPDATED                                             AS  LAST_UPDATED_DATETIME        ")  ' DateTime
    _sb.AppendLine("            , EMD_LAST_UPDATED_USER_ID                                     AS  LAST_UPDATED_USER_ID         ")  ' Gui-User
    _sb.AppendLine("            , GU_USERNAME                                                  AS  LAST_UPDATED_USER_NAME       ")  ' Gui-User Name
    _sb.AppendLine("            , CAST((CASE WHEN (ISNULL(ComputedNumPeriodMeters.NUM_PERIOD_METERS, 0) < " & m_SAS_period_meters_records_per_day & ") ")
    _sb.AppendLine("                      THEN 1                                                                                ")
    _sb.AppendLine("                      ELSE 0                                                                                ")
    _sb.AppendLine("                    END)  AS  BIT)                                         AS  ARE_MISSING_PERIOD_METERS    ")   ' Are missing number of SAS period meters?
    _sb.AppendLine("            , ISNULL(ComputedIncrements.NUM_BIGINCREMENTS, 0)              AS  NUM_BIGINCREMENTS            ")
    _sb.AppendLine("      FROM    EGM_METERS_BY_DAY                                                                             ")
    _sb.AppendLine("INNER JOIN    EGM_DAILY                                                                                     ")
    _sb.AppendLine("        ON    ED_SITE_ID      =  EMD_SITE_ID                                                                ")
    _sb.AppendLine("       AND    ED_WORKING_DAY  =  EMD_WORKING_DAY                                                            ")
    _sb.AppendLine("INNER JOIN    TERMINALS                                                                                     ")
    _sb.AppendLine("        ON    TE_TERMINAL_ID  =  EMD_TERMINAL_ID                                                            ")

    If (m_is_multisite_center) Then
      _sb.AppendLine("       AND    TE_SITE_ID = EMD_SITE_ID                                                                    ")
    End If

    _sb.AppendLine("INNER JOIN    PROVIDERS                                                                                     ")
    _sb.AppendLine("        ON    PV_ID = TE_PROV_ID                                                                            ")

    _sb.AppendLine(" LEFT JOIN    GUI_USERS                                                                                     ")
    _sb.AppendLine("        ON    GU_USER_ID      = EMD_LAST_UPDATED_USER_ID                                                    ")
    _sb.AppendLine(" LEFT JOIN                                                                                                  ")
    _sb.AppendLine("   (                                                                                                        ")
    'Get Gain and increments
    _sb.AppendLine("   SELECT     EMP_SITE_ID                                                                                   ")
    _sb.AppendLine("            , EMP_TERMINAL_ID                                                                               ")
    _sb.AppendLine("            , EMP_WORKING_DAY                                                                               ")
    _sb.AppendLine("            , SUM(EMP_MC_0000_INCREMENT                                                                     ")
    _sb.AppendLine("                - ( EMP_MC_0001_INCREMENT                                                                   ")
    _sb.AppendLine("                  + EMP_MC_0002_INCREMENT)) AS  MONEY_GAIN                                                  ")
    _sb.AppendLine("            , SUM(EMP_MC_0000_INCREMENT)    AS  COIN_IN_INCREMENT                                           ")
    _sb.AppendLine("            , SUM(EMP_MC_0001_INCREMENT)    AS  COIN_OUT_INCREMENT                                          ")
    _sb.AppendLine("            , SUM(EMP_MC_0002_INCREMENT)    AS  JACKPOT_INCREMENT                                           ")
    _sb.AppendLine("      FROM    EGM_METERS_BY_PERIOD                                                                          ")
    _sb.AppendLine("     WHERE    EMP_SITE_ID       = " & Me.m_site_id)
    _sb.AppendLine("       AND    EMP_WORKING_DAY   = " & Me.m_working_day)
    _sb.AppendLine("       AND    EMP_USER_IGNORED  = 0                                                                         ")
    _sb.AppendLine("       AND    EMP_RECORD_TYPE IN (" & ENUM_SAS_METER_HISTORY.TSMH_HOURLY & ", " & _
                                                          EGMMeterAdjustmentConstants.RECORD_TYPE_CUT & ")                      ")
    _sb.AppendLine("  GROUP BY    EMP_SITE_ID                                                                                   ")
    _sb.AppendLine("            , EMP_TERMINAL_ID                                                                               ")
    _sb.AppendLine("            , EMP_WORKING_DAY                                                                               ")
    _sb.AppendLine("   )  AS ComputedGain                                                                                       ")
    _sb.AppendLine("        ON    EMD_SITE_ID      =  ComputedGain.EMP_SITE_ID                                                  ")
    _sb.AppendLine("       AND    EMD_TERMINAL_ID  =  ComputedGain.EMP_TERMINAL_ID                                              ")
    _sb.AppendLine("       AND    EMD_WORKING_DAY  =  ComputedGain.EMP_WORKING_DAY                                              ")
    'Get number of period meters
    _sb.AppendLine(" LEFT JOIN                                                                                                  ")
    _sb.AppendLine("   (                                                                                                        ")
    _sb.AppendLine("   SELECT     EMP_SITE_ID                                                                                   ")
    _sb.AppendLine("            , EMP_TERMINAL_ID                                                                               ")
    _sb.AppendLine("            , EMP_WORKING_DAY                                                                               ")
    _sb.AppendLine("            , COUNT(*)        AS  NUM_PERIOD_METERS                                                         ") 'Count the period records
    _sb.AppendLine("      FROM    EGM_METERS_BY_PERIOD                                                                          ")
    _sb.AppendLine("     WHERE    EMP_SITE_ID       = " & Me.m_site_id)
    _sb.AppendLine("       AND    EMP_WORKING_DAY   = " & Me.m_working_day)
    _sb.AppendLine("       AND    EMP_RECORD_TYPE  IN  (" & ENUM_SAS_METER_HISTORY.TSMH_HOURLY & ")                             ")
    _sb.AppendLine("  GROUP BY    EMP_SITE_ID                                                                                   ")
    _sb.AppendLine("            , EMP_TERMINAL_ID                                                                               ")
    _sb.AppendLine("            , EMP_WORKING_DAY                                                                               ")
    _sb.AppendLine("   )  AS ComputedNumPeriodMeters                                                                            ")
    _sb.AppendLine("        ON    EMD_SITE_ID      =  ComputedNumPeriodMeters.EMP_SITE_ID                                       ")
    _sb.AppendLine("       AND    EMD_TERMINAL_ID  =  ComputedNumPeriodMeters.EMP_TERMINAL_ID                                   ")
    _sb.AppendLine("       AND    EMD_WORKING_DAY  =  ComputedNumPeriodMeters.EMP_WORKING_DAY                                   ")
    'Big increments
    _sb.AppendLine(" LEFT JOIN                                                                                                  ")
    _sb.AppendLine("   (                                                                                                        ")
    _sb.AppendLine("   SELECT     EMP_SITE_ID                                                                                   ")
    _sb.AppendLine("            , EMP_WORKING_DAY                                                                               ")
    _sb.AppendLine("            , EMP_TERMINAL_ID                                                                               ")
    _sb.AppendLine("            , SUM(CASE WHEN (ISNULL(EMP_SAS_ACCOUNTING_DENOM, 0) <> 0)                                      ")
    _sb.AppendLine("                    THEN (CASE                                                                              ")
    _sb.AppendLine("                            WHEN (EMP_MC_0000_INCREMENT / EMP_SAS_ACCOUNTING_DENOM > TBIT_MAX_VALUE) THEN 1 ")
    _sb.AppendLine("                            WHEN (EMP_MC_0001_INCREMENT / EMP_SAS_ACCOUNTING_DENOM > TBIT_MAX_VALUE) THEN 1 ")
    _sb.AppendLine("                            WHEN (EMP_MC_0002_INCREMENT / EMP_SAS_ACCOUNTING_DENOM > TBIT_MAX_VALUE) THEN 1 ")
    _sb.AppendLine("                            ELSE 0                                                                          ")
    _sb.AppendLine("                          END)                                                                              ")
    _sb.AppendLine("                    ELSE 0                                                                                  ")
    _sb.AppendLine("                  END)  AS  NUM_BIGINCREMENTS                                                               ")
    _sb.AppendLine("      FROM    EGM_METERS_BY_PERIOD                                                                          ")
    _sb.AppendLine("INNER JOIN    @TEMP_BIG_INCREMENT_TABLE                                                                     ")
    _sb.AppendLine("        ON    EMP_SAS_ACCOUNTING_DENOM = TBIT_DENOMINATION                                                  ")
    _sb.AppendLine("     WHERE    EMP_WORKING_DAY   = " & Me.m_working_day)
    _sb.AppendLine("       AND    EMP_SITE_ID       = " & Me.m_site_id)
    _sb.AppendLine("  GROUP BY    EMP_SITE_ID                                                                                   ")
    _sb.AppendLine("            , EMP_WORKING_DAY                                                                               ")
    _sb.AppendLine("            , EMP_TERMINAL_ID                                                                               ")
    _sb.AppendLine("   )  AS ComputedIncrements                                                                                 ")
    _sb.AppendLine("        ON    EMD_SITE_ID      =  ComputedIncrements.EMP_SITE_ID                                            ")
    _sb.AppendLine("       AND    EMD_WORKING_DAY  =  ComputedIncrements.EMP_WORKING_DAY                                        ")
    _sb.AppendLine("       AND    EMD_TERMINAL_ID  =  ComputedIncrements.EMP_TERMINAL_ID                                        ")

    _sb.AppendLine(GetSqlWhere())

    _sb.AppendLine(GetSqlOrderBy())

    ' Set data table
    Me.m_query_as_datatable = GUI_GetTableUsingSQL(_sb.ToString(), Integer.MaxValue)

    Call SetReadOnlyMode()

    Return Me.m_query_as_datatable.Copy
  End Function ' GUI_FilterGetSqlQuery

  ''' <summary>
  ''' Set read only mode
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetReadOnlyMode()
    Me.m_readonly = IsWorkingDayClosed()
  End Sub

  ''' <summary>
  ''' Get SQl Where
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlWhere() As String
    Dim _sb_where As StringBuilder

    _sb_where = New StringBuilder()
    _sb_where.AppendLine(" WHERE  EMD_SITE_ID     = " & Me.m_site_id)
    _sb_where.AppendLine("   AND  EMD_WORKING_DAY = " & Me.m_working_day)

    ' Filter Open Sessions / Historic sessions
    If Me.chk_show_only_with_anomalies.Checked Then
      _sb_where.AppendLine("   AND  (  (EMD_WARNING IS NOT NULL                                                                                     ")
      _sb_where.AppendLine("             AND  (EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.METER_RESET & " <> 0        ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.ROLLOVER & " <> 0           ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DISCARDED & " <> 0          ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.DENOM_CHANGE & " <> 0       ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.SERVICE_RAM_CLEAR & " <> 0  ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MACHINE_RAM_CLEAR & " <> 0  ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MISSING_COUNTERS & " <> 0   ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.MISSING_METERS & " <> 0     ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.NEW_METERS & " <> 0         ")
      _sb_where.AppendLine("               OR  EMD_WARNING & " & EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.FIRST_TIME & " <> 0         ")
      _sb_where.AppendLine("                  )                                                                                                     ")
      _sb_where.AppendLine("           )                                                                                                            ")
      _sb_where.AppendLine("          OR  (ComputedIncrements.NUM_BIGINCREMENTS  >  0)                                                              ")
      _sb_where.AppendLine("          OR  (ISNULL(ComputedNumPeriodMeters.NUM_PERIOD_METERS, 0) < " & m_SAS_period_meters_records_per_day & ")      ")   ' Are missing number of SAS period meters?
      _sb_where.AppendLine("        )                                                                                                               ")
    End If

    Return _sb_where.ToString
  End Function


  ''' <summary>
  ''' Returns the SQL order by
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSqlOrderBy() As String
    Dim _sql_order As StringBuilder

    _sql_order = New StringBuilder
    _sql_order.AppendLine("  ORDER BY   EMD_WARNING  DESC                             ")
    _sql_order.AppendLine("           , ComputedIncrements.NUM_BIGINCREMENTS  DESC    ")
    _sql_order.AppendLine("           , ARE_MISSING_PERIOD_METERS  DESC               ")
    _sql_order.AppendLine("           , TE_NAME  ASC                                  ")

    Return _sql_order.ToString
  End Function

  ''' <summary>
  ''' Check if changes have been made in the form
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsPendingChanges() As Boolean
    If Me.m_query_as_datatable Is Nothing Then
      Return False
    End If

    For Each _row As DataRow In Me.m_query_as_datatable.Rows
      If _row.RowState = DataRowState.Modified Then
        For _idx_col As Integer = 0 To Me.m_query_as_datatable.Columns.Count - 1
          If Not IsDBNull(_row.Item(_idx_col)) Then
            If _row.Item(_idx_col, DataRowVersion.Current) <> _row.Item(_idx_col, DataRowVersion.Original) Then
              Return True
            End If
          End If
        Next
      End If
    Next

    Return False
  End Function ' IsPendingChanges

  ''' <summary>
  ''' Close Working Day (Save Pending (validated) if needed)
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CloseWorkingDayManagement(ByRef OldStatus As EGMMeterAdjustment.EGMWorkingDayStatus) As Boolean
    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        If Not SavePendingChanges(True, _db_trx.SqlTransaction) Then

          Return False
        End If

        If Not DB_CloseWorkingDay(OldStatus, _db_trx.SqlTransaction) Then

          Return False
        End If

        _db_trx.Commit()

        m_saved_almost_once = True

        Return True
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Function

  ''' <summary>
  ''' Save Pending changes
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SavePendingChanges() As Boolean
    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        If SavePendingChanges(False, _db_trx.SqlTransaction) Then

          _db_trx.Commit()

          Return True
        End If
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)

    End Try

    Return False
  End Function

  ''' <summary>
  ''' Save Pending changes
  ''' </summary>
  ''' <remarks></remarks>
  Private Function SavePendingChanges(ByVal IsClosing As Boolean, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _has_new_meters As Boolean
    Dim _is_all_updated As Boolean
    Dim _first_time As Boolean

    _first_time = True
    _is_all_updated = True

    Try
      For Each _row As DataRow In Me.m_query_as_datatable.Rows
        _has_new_meters = False

        If _row.RowState = DataRowState.Modified Then

          If Not DB_UpdateTerminalValidated(_row, _has_new_meters, SqlTrans) Then
            _is_all_updated = False

            Exit For
          End If

          If _first_time Then
            If Not DB_UpdateStatusWorkingDay(WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus.IN_PROGRESS, SqlTrans) Then
              _is_all_updated = False

              Exit For
            End If
          End If

          If _has_new_meters Then
            If Not DB_UpdateEgmWorkingDayHasNewMeters(_row, SqlTrans) Then
              _is_all_updated = False

              Exit For
            End If
          End If

          _first_time = False
          _is_all_updated = True
        End If

        If Not DB_UpdateTerminalCloseSesion(_row, SqlTrans) Then
          _is_all_updated = False

          Exit For
        End If
      Next

      If (_is_all_updated AndAlso IsClosing AndAlso (Me.m_query_as_datatable.Rows.Count > 0)) Then
        If Not DB_UpdateNumberOfTerminalsConnectedDay(Me.m_query_as_datatable.Rows(0), Me.m_query_as_datatable.Rows.Count, SqlTrans) Then
          _is_all_updated = False
        End If
      End If

      Return _is_all_updated
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' SavePendingChanges

  ''' <summary>
  ''' Update data for no detect changes
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AcceptChanges()
    Me.m_query_as_datatable.AcceptChanges()
  End Sub ' AcceptChanges

  ''' <summary>
  ''' Update statuts of Working Day
  ''' </summary>
  ''' <param name="NewStatus"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateStatusWorkingDay(ByVal NewStatus As EGMMeterAdjustment.EGMWorkingDayStatus, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try
      _query.AppendLine("UPDATE   EGM_DAILY                                     ")
      _query.AppendLine("   SET   ED_STATUS                      = @pNewStatus  ")
      _query.AppendLine("       , ED_LAST_UPDATED_USER           = @pUserId     ")
      _query.AppendLine("       , ED_LAST_UPDATED_USER_DATETIME  = GETDATE()    ")
      _query.AppendLine(" WHERE   ED_WORKING_DAY                 = @pWorkingDay ")
      _query.AppendLine("   AND   ED_SITE_ID                     = @pSiteId     ")
      _query.AppendLine("   AND   ED_STATUS IN (@pStatusOpen, @pStatusProgress) ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        '* SET
        ' Status (Validated, etc.)
        _cmd.Parameters.Add("@pNewStatus", SqlDbType.TinyInt).Value = NewStatus

        ' Update user Id
        _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CurrentUser.Id

        '* WHERE
        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Me.m_site_id
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = Me.m_working_day
        _cmd.Parameters.Add("@pStatusOpen", SqlDbType.TinyInt).Value = EGMMeterAdjustment.EGMWorkingDayStatus.OPEN
        _cmd.Parameters.Add("@pStatusProgress", SqlDbType.TinyInt).Value = EGMMeterAdjustment.EGMWorkingDayStatus.IN_PROGRESS

        _cmd.ExecuteNonQuery()

        Return True
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateStatusWorkingDay

  ''' <summary>
  ''' Close Working Day
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_CloseWorkingDay(ByRef OldStatus As EGMMeterAdjustment.EGMWorkingDayStatus, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try
      Using _da As New SqlDataAdapter()
        _query.AppendLine("UPDATE   ED                                                    ")
        _query.AppendLine("   SET   ED.ED_STATUS                      = @pNewStatus       ")
        _query.AppendLine("       , ED.ED_LAST_UPDATED_USER           = @pUserId          ")
        _query.AppendLine("       , ED.ED_LAST_UPDATED_USER_DATETIME  = GETDATE()         ")
        _query.AppendLine("OUTPUT   DELETED.ED_STATUS                                     ")
        _query.AppendLine("  FROM   EGM_DAILY AS ED                                       ")
        _query.AppendLine(" ,                                                             ")
        _query.AppendLine(" (SELECT   COUNT(*) AS NUM_ANOM                                ")
        _query.AppendLine("    FROM   EGM_METERS_BY_DAY                                   ")
        _query.AppendLine("   WHERE   EMD_WORKING_DAY                 = @pWorkingDay      ")
        _query.AppendLine("     AND   EMD_SITE_ID                     = @pSiteId          ")
        _query.AppendLine("     AND   EMD_WARNING IS NOT NULL                             ")
        _query.AppendLine("     AND   EMD_WARNING > 0) AS A                               ")
        _query.AppendLine(" ,                                                             ")
        _query.AppendLine(" (SELECT   COUNT(*) AS NUM_ANOM_VALID                          ")
        _query.AppendLine("    FROM   EGM_METERS_BY_DAY                                   ")
        _query.AppendLine("   WHERE   EMD_WORKING_DAY                 = @pWorkingDay      ")
        _query.AppendLine("     AND   EMD_SITE_ID                     = @pSiteId          ")
        _query.AppendLine("     AND   EMD_WARNING IS NOT NULL                             ")
        _query.AppendLine("     AND   EMD_WARNING > 0                                     ")
        _query.AppendLine("     AND   EMD_STATUS & @pStatusValidated <> 0 ) AS B          ")
        _query.AppendLine("   WHERE   A.NUM_ANOM                      = B.NUM_ANOM_VALID  ")
        _query.AppendLine("     AND   ED_WORKING_DAY                  = @pWorkingDay      ")
        _query.AppendLine("     AND   ED_SITE_ID                      = @pSiteId          ")
        _query.AppendLine("     AND   ED_STATUS  IN  (@pStatusOpen, @pStatusInProgress)   ")

        Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
          '* SET
          ' Status (Validated, etc.)
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED
          ' Update user Id
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CurrentUser.Id

          '* WHERE
          _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Me.m_site_id
          _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = Me.m_working_day
          _cmd.Parameters.Add("@pStatusValidated", SqlDbType.Int).Value = EGMMeterAdjustmentCommon.EGMStatus.VALIDATED

          _cmd.Parameters.Add("@pStatusOpen", SqlDbType.TinyInt).Value = EGMMeterAdjustment.EGMWorkingDayStatus.OPEN
          _cmd.Parameters.Add("@pStatusInProgress", SqlDbType.TinyInt).Value = EGMMeterAdjustment.EGMWorkingDayStatus.IN_PROGRESS

          Using _reader As SqlDataReader = _cmd.ExecuteReader()
            If _reader.Read() Then
              If Not _reader.IsDBNull(0) Then
                OldStatus = _reader(0)

                Return True
              End If
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_CloseWorkingDay

  ''' <summary>
  ''' Update Field Validated Terminals
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateTerminalValidated(ByVal CurrentTerminalRow As DataRow, ByRef HasNewMeters As Boolean, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try
      _query.AppendLine(" UPDATE   EGM_METERS_BY_DAY                                            ")
      _query.AppendLine("    SET   EMD_STATUS                    = @pStatus                     ")
      _query.AppendLine("        , EMD_USER_VALIDATED_DATETIME   = @pUserValidateDateTime       ")
      _query.AppendLine("        , EMD_LAST_UPDATED              = GETDATE()                    ")
      _query.AppendLine("        , EMD_LAST_UPDATED_USER_ID      = @pLastUpdatedUserId          ")
      _query.AppendLine(" OUTPUT   DELETED.EMD_WARNING & @pNewMeters AS HAS_NEW_METERS          ")
      _query.AppendLine("  WHERE   EMD_SITE_ID                   = @pSiteId                     ")
      _query.AppendLine("    AND   EMD_TERMINAL_ID               = @pTerminalId                 ")
      _query.AppendLine("    AND   EMD_WORKING_DAY               = @pWorkingDay                 ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        '* SET

        ' Status (Validated, etc.)
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_STATUS)
        _cmd.Parameters.Add("@pNewMeters", SqlDbType.Int).Value = EGMMeterAdjustmentCommon.EGMSasMetersHistoryAdjustment.NEW_METERS

        ' Validated DateTime
        If EGMMeterAdjustmentCommon.GetMaskFlagBitEgm(CurrentTerminalRow(SQL_COLUMN_STATUS), EGMMeterAdjustmentCommon.EGMStatus.VALIDATED) Then
          _cmd.Parameters.Add("@pUserValidateDateTime", SqlDbType.DateTime).Value = WGDB.Now
        Else
          _cmd.Parameters.Add("@pUserValidateDateTime", SqlDbType.DateTime).Value = DBNull.Value
        End If

        ' Update user Id
        _cmd.Parameters.Add("@pLastUpdatedUserId", SqlDbType.Int).Value = CurrentUser.Id

        '* WHERE
        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_SITE_ID)
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_TERMINAL_ID)
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_WORKING_DAY)

        Using _reader As SqlDataReader = _cmd.ExecuteReader()
          If _reader.Read() Then
            If Not _reader.IsDBNull(0) Then
              HasNewMeters = Integer.Parse(_reader(0)) > 0
            End If

            Return True
          End If
        End Using
      End Using
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateTerminalValidated

  ''' <summary>
  ''' Update Field HasNewMeters
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateEgmWorkingDayHasNewMeters(ByVal CurrentTerminalRow As DataRow, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try
      _query.AppendLine(" UPDATE   EGM_DAILY                          ")
      _query.AppendLine("    SET   ED_HAS_NEW_METERS  = 0             ")
      _query.AppendLine("  WHERE   ED_SITE_ID         = @pSiteId      ")
      _query.AppendLine("    AND   ED_WORKING_DAY     = @pWorkingDay  ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        '* WHERE
        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_SITE_ID)
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_WORKING_DAY)

        Return _cmd.ExecuteNonQuery() = 1
      End Using

    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateEgmWorkingDayHasNewMeters

  Private Function DB_PreviousWorkingDaysAreClosed(ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _return As Boolean = False
    Dim _query As StringBuilder

    _query = New StringBuilder()

    Try
      _query.AppendLine(" SELECT   COUNT(ED_STATUS)                                 ")
      _query.AppendLine("   FROM   EGM_DAILY                                        ")
      _query.AppendLine("  WHERE   ED_WORKING_DAY <  @pWorkingDay                   ")
      _query.AppendLine("    AND   ED_SITE_ID     =  @pSiteId                       ")
      _query.AppendLine("    AND   ED_STATUS  IN  (@pStatusOpen, @pStatusProgress)  ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        '* WHERE
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = m_working_day
        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = m_site_id
        _cmd.Parameters.Add("@pStatusOpen", SqlDbType.TinyInt).Value = EGMMeterAdjustment.EGMWorkingDayStatus.OPEN
        _cmd.Parameters.Add("@pStatusProgress", SqlDbType.TinyInt).Value = EGMMeterAdjustment.EGMWorkingDayStatus.IN_PROGRESS

        Dim _result As Integer = _cmd.ExecuteScalar()
        _return = (_result = 0)
      End Using

    Catch _ex As Exception

      Log.Exception(_ex)
    End Try

    Return _return
  End Function 'DB_PreviousWorkingDaysAreClosed

  ''' <summary>
  ''' Ask user to discard changes or not
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DiscardChanges() As Boolean
    ' If pending changes MsgBox
    If (Me.m_is_edit_mode AndAlso IsPendingChanges()) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
        ' To avoid the application to answer about loosing changes for a second time when exiting
        Me.m_query_as_datatable = Nothing

        Return True
      Else
        Return False
      End If
    End If

    Return True
  End Function ' DiscardChanges

  ''' <summary>
  ''' Ask user to discard changes or not
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Sub CheckAndSaveChanges()
    ' If pending changes MsgBox
    If Me.m_is_edit_mode AndAlso IsPendingChanges() Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9007), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then

        If Not Me.SavePendingChanges() Then
          NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(109), _
                     ENUM_MB_TYPE.MB_TYPE_ERROR, _
                     ENUM_MB_BTN.MB_BTN_OK)
        End If
      End If
    End If
  End Sub ' DiscardChanges

  Private Function GetWorkingDayStatus() As WSI.Common.EGMMeterAdjustment.EGMWorkingDayStatus
    Dim _wd As EGMMeterAdjustmentWorkingDay
    _wd = New EGMMeterAdjustmentWorkingDay()

    Return _wd.GetWorkingDayStatus(Me.m_working_day, Me.m_site_id)
  End Function

  ''' <summary>
  ''' Get if WorkingDay is Closed
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsWorkingDayClosed() As Boolean
    Dim _wd As EGMMeterAdjustmentWorkingDay
    _wd = New EGMMeterAdjustmentWorkingDay()

    Return _wd.IsWorkingDayClosed(Me.m_working_day, Me.m_site_id)
  End Function

  ''' <summary>
  ''' Auditor Data
  ''' </summary>
  ''' <param name="OldStatus"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function AuditChanges(ByVal OldStatus As EGMMeterAdjustment.EGMWorkingDayStatus) As Boolean
    Dim _curr_auditor As CLASS_AUDITOR_DATA
    Dim _original_value As Boolean
    Dim _current_value As Boolean

    Try
      _curr_auditor = New CLASS_AUDITOR_DATA(AUDIT_EGM_METERS)

      ' Set Name and Identifier
      _curr_auditor.SetName(GLB_NLS_GUI_PLAYER_TRACKING.Id(8814), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8814))

      ' Site (only if multisite)
      If (Me.m_is_multisite_center) Then
        _curr_auditor.SetField(0, m_site_name, GLB_NLS_GUI_CONFIGURATION.GetString(380))
      End If

      If OldStatus <> EGMWorkingDayStatus.NONE Then
        _curr_auditor.SetField(0, GetDateFromInt(m_working_day).ToShortDateString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6177) & "(" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6177) & ")")
        _curr_auditor.SetField(0, EGMMeterAdjustment.EGMWorkingDayStatus.CLOSED.ToString() & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548) & " = " & OldStatus.ToString & ")", GLB_NLS_GUI_PLAYER_TRACKING.GetString(8649))
      Else
        _curr_auditor.SetField(0, GetDateFromInt(m_working_day).ToShortDateString(), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6177))
      End If

      For Each _row As DataRow In m_query_as_datatable.Rows
        ' Validated
        _original_value = IIf(IsDBNull(_row(SQL_COLUMN_STATUS, DataRowVersion.Original)), _
                              AUDIT_NONE_STRING, _row(SQL_COLUMN_STATUS, DataRowVersion.Original))

        _current_value = IIf(IsDBNull(_row(SQL_COLUMN_STATUS)), _
                             AUDIT_NONE_STRING, _row(SQL_COLUMN_STATUS))

        If (_original_value <> _current_value) Then
          _curr_auditor.SetField(0, _row(SQL_COLUMN_TERMINAL_NAME), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6636))

          _curr_auditor.SetField(0, GetBooleanText(_current_value) & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6548) & ":" & GetBooleanText(_original_value) & ")", _
                                    GLB_NLS_GUI_PLAYER_TRACKING.GetString(8942))
        End If
      Next ' For _idx_row

      ' Notify
      _curr_auditor.Notify(CurrentUser.GuiId, _
                           CurrentUser.Id, _
                           CurrentUser.Name, _
                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                           0)

      Return True
    Catch ex As Exception
      Log.Exception(ex)

    End Try

    Return False
  End Function ' AuditChanges

  ''' <summary>
  ''' Draw Totals
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DrawTotal()
    Dim _idx_row As Integer
    Dim _str_total As String

    ' Grid
    If (Me.Grid.NumRows > 0) AndAlso CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read Then
      _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838).ToUpper & ":"  '"TOTAL"

      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value = -1
      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = _str_total

      ' Credit
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_COIN_IN).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_coin_in.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_COIN_OUT).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_coin_out.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_CREDIT_JACKPOT).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_jackpot.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      ' Money
      Me.Grid.Cell(_idx_row, GRID_COLUMN_MONEY_GAIN).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_gain.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_MONEY_COIN_IN).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_coin_in.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_MONEY_COIN_OUT).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_coin_out.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Me.Grid.Cell(_idx_row, GRID_COLUMN_MONEY_JACKPOT).Value = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_jackpot.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If
  End Sub

  ''' <summary>
  ''' Reset Counters
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetCounters()
    m_total_credit_gain_increment = 0
    m_total_credit_coin_in_increment = 0
    m_total_credit_coin_out_increment = 0
    m_total_credit_jackpot_increment = 0

    m_total_money_coin_in = 0
    m_total_money_coin_out = 0
    m_total_money_jackpot = 0

    m_total_money_gain_increment = 0
    m_total_money_coin_in_increment = 0
    m_total_money_coin_out_increment = 0
    m_total_money_jackpot_increment = 0
  End Sub

  ''' <summary>
  ''' Set Enabled state for button "Close WorkingDay"
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function AreAllAnomaliesValidated() As Boolean
    If (Me.Grid.NumRows <= 0) OrElse (Not HasPermissionsToAdjustAndCloseWorkingDay()) Then

      Return False
    End If

    For _idx_row As Integer = 0 To (Me.Grid.NumRows - 1)
      If Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_ID).Value > 0 AndAlso _
         Me.Grid.Cell(_idx_row, GRID_COLUMN_ANOMALY_ID).Value > 0 AndAlso _
         Me.Grid.Cell(_idx_row, GRID_COLUMN_VALIDATED).Value = uc_grid.GRID_CHK_UNCHECKED Then

        Return False
      End If
    Next

    Return True
  End Function

  ''' <summary>
  ''' Set Enabled state for button "Close WorkingDay"
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function WorkingDayISNotCurrentDay() As Boolean

    Return cls_lottery.BigIntToDate(m_working_day) <> DateTime.Today

    'Return True
  End Function

  ''' <summary>
  ''' Show data in Credit or Money
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RefreshCreditMoney()
    If (Me.Grid.NumColumns <= 0) Then

      Return
    End If

    With Me.Grid
      ' Coin In (Credit).
      .Column(GRID_COLUMN_CREDIT_COIN_IN).WidthFixed = IIf(Me.opt_Credit.Checked, GRID_WIDTH_COIN_IN, 0)

      ' Coin Out (Credit).
      .Column(GRID_COLUMN_CREDIT_COIN_OUT).WidthFixed = IIf(Me.opt_Credit.Checked, GRID_WIDTH_COIN_OUT, 0)

      ' JackPot (Credit).
      .Column(GRID_COLUMN_CREDIT_JACKPOT).WidthFixed = IIf(Me.opt_Credit.Checked, GRID_WIDTH_JACKPOT, 0)

      ' Gain (Money).
      .Column(GRID_COLUMN_MONEY_GAIN).WidthFixed = IIf(CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE).Read, GRID_WIDTH_GAIN, 0)

      ' Coin In (Money).
      .Column(GRID_COLUMN_MONEY_COIN_IN).WidthFixed = IIf(Me.opt_Money.Checked, GRID_WIDTH_COIN_IN, 0)

      ' Coin Out (Money).
      .Column(GRID_COLUMN_MONEY_COIN_OUT).WidthFixed = IIf(Me.opt_Money.Checked, GRID_WIDTH_COIN_OUT, 0)

      ' JackPot (Money).
      .Column(GRID_COLUMN_MONEY_JACKPOT).WidthFixed = IIf(Me.opt_Money.Checked, GRID_WIDTH_JACKPOT, 0)
    End With

    ' Totals (always in currency)
    Me.lblTotalGain.Text = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_gain.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.lblTotalCoinIn.Text = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_coin_in.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.lblTotalCoinOut.Text = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_coin_out.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Me.lblTotalJackPot.Text = GUI_GetCurrencyValue_Format(Convert.ToDecimal(m_initial_jackpot.Replace("$", "")), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
  End Sub

  ''' <summary>
  ''' Show data in Credit or Money
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RefreshIncrement()
    If (Me.Grid.NumColumns <= 0) Then

      Return
    End If

    With Me.Grid
      ' Coin In (Money) (Increment).
      .Column(GRID_COLUMN_MONEY_COIN_IN_INCREMENT).WidthFixed = IIf(Me.chk_show_increments.Checked, GRID_WIDTH_COIN_IN, 0)

      ' Coin Out (Money) (Increment).
      .Column(GRID_COLUMN_MONEY_COIN_OUT_INCREMENT).WidthFixed = IIf(Me.chk_show_increments.Checked, GRID_WIDTH_COIN_OUT, 0)

      ' Coin In (Money) (Increment).
      .Column(GRID_COLUMN_MONEY_JACKPOT_INCREMENT).WidthFixed = IIf(Me.chk_show_increments.Checked, GRID_WIDTH_JACKPOT, 0)
    End With
  End Sub


  ''' <summary>
  ''' Get Date Format form integer value
  ''' </summary>
  ''' <param name="Value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetDateFromInt(Value As Integer) As DateTime
    Try
      Return DateTime.ParseExact(Value, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
    Catch ex As Exception
      Log.Exception(ex)
    End Try

    Return New DateTime()
  End Function

  ''' <summary>
  ''' Check permissions for user
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function HasPermissionsToAdjustAndCloseWorkingDay() As Boolean
    Return (CurrentUser.Permissions(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_ADJUST_AND_CLOSE_WORKING_DAY).Write)
  End Function

  ''' <summary>
  ''' Manage Enabled property for following buttons (Close Working Day and Save)
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ManageButtonsEnabled()
    'Save data
    m_save_button.Enabled = ((Me.Grid.NumRows > 0) AndAlso (HasPermissionsToAdjustAndCloseWorkingDay()) AndAlso (Not Me.m_readonly))

    'Close Working day

    m_close_button.Enabled = ArePreviousWorkingDayClosed AndAlso ((Me.Grid.NumRows > 0) AndAlso _
                             HasPermissionsToAdjustAndCloseWorkingDay() AndAlso _
                             AreAllAnomaliesValidated() AndAlso _
                             (Not Me.m_readonly) AndAlso WorkingDayISNotCurrentDay())
  End Sub

  ''' <summary>
  ''' Checks if previous working days refered to current are o aren't closed.
  ''' </summary>
  ''' <remarks></remarks>
  Private Function PreviousWorkingDaysAreClosed() As Boolean
    Using _db_trx As DB_TRX = New DB_TRX()
      m_previous_working_days_closed = DB_PreviousWorkingDaysAreClosed(_db_trx.SqlTransaction)
    End Using

    Return m_previous_working_days_closed

  End Function 'PreviousWorkingDaysAreClosed


  ''' <summary>
  ''' Show Meters Form for one terminal
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ShowCountersForm()
    Dim _frm As frm_lottery_meters_adjustment_meters
    Dim _cur_row As Long

    _cur_row = Grid.CurrentRow
    If Me.Grid.Cell(Grid.CurrentRow, GRID_COLUMN_TERMINAL_ID).Value <= 0 Then

      Return
    End If

    _frm = New frm_lottery_meters_adjustment_meters(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_METERS)

    _frm.ShowForEdit(m_site_id, _
                     Me.GetDateFromInt(Me.m_working_day), _
                     (Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_TERMINAL_ID).Value), _
                     (Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_TERMINAL_NAME).Value), _
                     Me.m_site_name, _
                     (Me.Grid.Cell(Me.Grid.CurrentRow, GRID_COLUMN_PROVIDER_NAME).Value))

    If _frm.SaveAlmostOnce Then
      Me.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    End If

    ' Maintain previous row selected
    Me.Grid.CurrentRow = _cur_row
    Me.Grid.Row(_cur_row).IsSelected = True
    Me.Grid.TopRow = _cur_row

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ''' <summary>
  ''' Update emd_connected when the session it is closing
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateTerminalCloseSesion(ByVal CurrentTerminalRow As DataRow, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder
    Dim _count As Int32

    _query = New StringBuilder()

    Try
      _query.AppendLine(" UPDATE   EGM_METERS_BY_DAY              ")
      _query.AppendLine("    SET   EMD_CONNECTED   = @pConnected  ")
      _query.AppendLine("  WHERE   EMD_WORKING_DAY = @pWorkingDay ")
      _query.AppendLine("    AND   EMD_SITE_ID     = @pSite       ")
      _query.AppendLine("    AND   EMD_TERMINAL_ID = @pTerminalId ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pConnected", SqlDbType.Bit).Value = True
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_WORKING_DAY)
        _cmd.Parameters.Add("@pSite", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_SITE_ID)
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_TERMINAL_ID)

        _count = _cmd.ExecuteNonQuery()
      End Using

      Return (_count = 1)
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateTerminalCloseSesion

  ''' <summary>
  ''' Set terminals connected
  ''' </summary>
  ''' <param name="CurrentTerminalRow"></param>
  ''' <param name="TerminalsConnected"></param>
  ''' <param name="SqlTrans"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DB_UpdateNumberOfTerminalsConnectedDay(ByVal CurrentTerminalRow As DataRow, ByVal TerminalsConnected As Int32, ByVal SqlTrans As SqlTransaction) As Boolean
    Dim _query As StringBuilder
    Dim _count As Int32

    _query = New StringBuilder()

    Try
      _query.AppendLine(" UPDATE EGM_DAILY                                     ")
      _query.AppendLine("    SET ED_TERMINALS_CONNECTED = @pTerminalsConnected ")
      _query.AppendLine("  WHERE ED_WORKING_DAY         = @pWorkingDay         ")
      _query.AppendLine("    AND ED_SITE_ID             = @pSite               ")

      Using _cmd As New SqlCommand(_query.ToString, SqlTrans.Connection, SqlTrans)
        _cmd.Parameters.Add("@pTerminalsConnected", SqlDbType.Int).Value = TerminalsConnected
        _cmd.Parameters.Add("@pWorkingDay", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_WORKING_DAY)
        _cmd.Parameters.Add("@pSite", SqlDbType.Int).Value = CurrentTerminalRow(SQL_COLUMN_SITE_ID)

        _count = _cmd.ExecuteNonQuery()
      End Using

      Return (_count = 1)
    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return False
  End Function ' DB_UpdateTerminalsConnectedDay

  ''' <summary>
  ''' Get SAS meter interval (minutes)
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSASMeterInterval() As Int32
    Dim _str_SAS_meter_interval As String

    _str_SAS_meter_interval = GeneralParam.GetString("SasHost", "SasMeterInterval", "60")

    Return Int32.Parse(_str_SAS_meter_interval)
  End Function

  ''' <summary>
  ''' Get number of period records by day
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetSASPeriodMetersRecordsPerDay() As Int32
    Const K_MINUTES_DAY As Int32 = (60 * 24)

    Return (K_MINUTES_DAY / GetSASMeterInterval())
  End Function

  ''' <summary>
  ''' Get boolean text
  ''' </summary>
  ''' <param name="Value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetBooleanText(ByVal Value As Boolean) As String
    If (Value) Then
      Return m_text_yes
    Else
      Return m_text_no
    End If
  End Function

#End Region

#Region "Public"

  ''' <summary>
  ''' Opens dialog with default settings for edit mode
  ''' </summary>
  ''' <param name="SiteId"></param>
  ''' <param name="SiteName"></param>
  ''' <param name="WorkingDay"></param>
  ''' <remarks></remarks>
  Public Sub ShowForEdit(ByVal SiteId As Integer, ByVal SiteName As String, ByVal WorkingDay As Integer)
    m_site_id = SiteId
    m_site_name = SiteName
    m_working_day = WorkingDay

    Me.ScreenMode = ENUM_SCREEN_TYPE.MODE_EDIT
    Me.MdiParent = MdiParent
    Me.Display(True)

  End Sub ' ShowForEdit

#End Region

#Region "Events"

  ''' <summary>
  ''' Event for Show in Credit or Money
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub Credit_Money_CheckedChanged(sender As Object, e As EventArgs) Handles opt_Credit.CheckedChanged, opt_Money.CheckedChanged
    Me.RefreshCreditMoney()
  End Sub

  ''' <summary>
  ''' Event for Show Only Machines with anomalies
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_show_only_with_anomalies_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_only_with_anomalies.CheckedChanged
    Me.CheckAndSaveChanges()
    MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
  End Sub

  ''' <summary>
  '''
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_show_increments_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_increments.CheckedChanged
    RefreshIncrement()
  End Sub
#End Region


End Class