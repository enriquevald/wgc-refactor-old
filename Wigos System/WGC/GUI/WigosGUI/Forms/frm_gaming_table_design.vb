'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_gaming_table_design
' DESCRIPTION:   
' AUTHOR:        David Her�ndez
' CREATION DATE: 15-MAY-2014
'
' REVISION HISTORY:
'
' Date        Author       Description
' ----------  ------       -----------------------------------------
' 15-MAY-2014 DHA          First Release.
' 29-SEP-2014 DHA          Fixed Bug #WIG-1322: if gaming table session is in use, design can't be modified
' 30-SEP-2014 DHA          Fixed Bug #WIG-1346: Cancel button was validating if there are seats ocupateds when come from window Table Edit
' 20-JAN-2015 FJC          Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 15-APR-2015 DHA          Fixed Bug #WIG-2213: error when number of seats field are empty
' 09-AGU-2017 DHA          Bug 29273:WIGOS-4300 Exception error displayed when creating new Gambling Table with different gambling table design
' 22-JUN-2018 LQB          Bug 33282:WIGOS-13055 Tables with integrated cashier saved without seats 
'------------------------------------------------------------------- 

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Reports.PrintDataset
Imports System.Text
Imports System.Runtime.InteropServices
Imports GUI_Controls.frm_base_sel
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common.TITO
Imports WigosGUI.NEW_COLLECTION

Public Class frm_gaming_table_design
  Inherits frm_base_edit

  Dim m_gaming_table_desing As DataTable
  Dim m_gaming_table_id As Integer
  Dim m_gaming_table_xml As String
  Dim m_gaming_table_name As String
  Dim m_save_gaming_table_design As Boolean
  Dim m_cancel_by_user As Boolean

#Region "Constants"

  Private Const GRID_COLUMN_TABLE_ID As Integer = 0
  Private Const GRID_COLUMN_CHECKED As Integer = 1
  Private Const GRID_COLUMN_TABLE_NAME As Integer = 2

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 0

#End Region 'Constants

#Region "Overrides functions"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FROM_GAMING_TABLE_DESIGN
    Call MyBase.GUI_SetFormId()

  End Sub

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '         - DbOperation
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_GAMING_TABLE_DESING

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            '' Trick : Unsupported feature message already shown. Not show again
            Exit Sub
          End If
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          If Me.DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED Then
            '' Trick : Unsupported feature message already shown. Not show again
            Exit Sub
          End If
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case Else

        Call MyBase.GUI_DB_Operation(DbOperation)
    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _gaming_table_desing As CLASS_GAMING_TABLE_DESING
    Dim _item As CLASS_GAMING_TABLE_DESING.TYPE_GAMING_TABLE_DESIGN
    Dim _row As DataRow
    Dim _table As DataRow()

    m_gaming_table_desing = New DataTable("GAMING_TABLES")
    m_gaming_table_desing.Columns.Add("GT_GAMING_TABLE_ID", Type.GetType("System.Int32"))
    m_gaming_table_desing.Columns.Add("GT_NAME", Type.GetType("System.String"))
    m_gaming_table_desing.Columns.Add("GT_NUM_SEATS", Type.GetType("System.Int32"))
    m_gaming_table_desing.Columns.Add("GT_DESIGN_XML", Type.GetType("System.String"))
    m_gaming_table_desing.Columns.Add("ROW_NOT_UPDATED", Type.GetType("System.Boolean"))

    _row = m_gaming_table_desing.NewRow()

    Try

      _gaming_table_desing = DbReadObject

      For Each _item In _gaming_table_desing.GamingTablesDesingList

        _row = m_gaming_table_desing.NewRow()

        _row("GT_GAMING_TABLE_ID") = _item.GamingTableID
        _row("GT_NAME") = _item.GamingTableName
        _row("GT_NUM_SEATS") = _item.NumSeats
        _row("GT_DESIGN_XML") = _item.DesingXml

        m_gaming_table_desing.Rows.Add(_row)

      Next

      If m_gaming_table_desing.Rows.Count > 0 And m_gaming_table_id > 0 And m_save_gaming_table_design Then
        _table = m_gaming_table_desing.Select("GT_GAMING_TABLE_ID = " & m_gaming_table_id)

        If _table.Length > 0 Then
          gb_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4993, _table(0)("GT_NAME"))
        End If
      Else
        gb_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4993, m_gaming_table_name)
      End If

      Call InitializeTable()

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_GetScreenData()
    Dim _gaming_table_desing As CLASS_GAMING_TABLE_DESING
    Dim _item As CLASS_GAMING_TABLE_DESING.TYPE_GAMING_TABLE_DESIGN
    Dim _row As DataRow
    Dim _xml_table As String
    Dim _gaming_table_session As GamingTablesSessions

    _gaming_table_desing = DbEditedObject
    _xml_table = String.Empty
    _gaming_table_session = Nothing

    panel_table_design.GetXMLGamingTable(_xml_table)

    Using _db_trx As New DB_TRX()

      For Each _row In m_gaming_table_desing.Rows

        _item = _gaming_table_desing.FindItemByGamingTableId(_row("GT_GAMING_TABLE_ID"))

        If GetGamingTableSelected().Contains(_item.GamingTableID) And GamingTablesSessions.GetOpenedSession(_item.GamingTableID, _gaming_table_session, _db_trx.SqlTransaction) Then
          If _gaming_table_session Is Nothing Then
            ' DHA 15-APR-2015
            If String.IsNullOrEmpty(ef_num_seats.Value) Then
              _item.NumSeats = -1
            Else
              _item.NumSeats = ef_num_seats.Value
            End If
            _item.DesingXml = _xml_table
            _item.updated = True
          Else
            _item.session_opened = True
          End If
        End If

      Next

    End Using

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _rows As DataRow()
    Dim _gaming_tables_status As DataTable
    Dim _show_message_seat As Boolean
    Dim _show_message_table_session_open As Boolean
    Dim _gaming_table_session As GamingTablesSessions

    _gaming_tables_status = New DataTable
    _show_message_seat = False
    _show_message_table_session_open = False
    _gaming_table_session = Nothing
    m_cancel_by_user = False

    ' Table Num Seats is null
    If String.IsNullOrEmpty(Me.ef_num_seats.Value) Then
      ' "You must enter a value for %1."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_num_seats.Text)
      ' DHA 15-APR-2015 Avoid to show message gaming tables not updated
      m_cancel_by_user = True
      Call Me.ef_num_seats.Focus()

      Return False
    End If

    ' Table Num Seats exceeded
    If GUI_ParseNumber(Me.ef_num_seats.Value) > GeneralParam.GetInt32("GamingTables", "Seats.MaxAllowedSeats", 10) Then
      ' "The number of enabled seats exceeds the maximum allowed."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4956), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_num_seats.Text)
      Call Me.ef_num_seats.Focus()

      Return False
    End If

    ' Table Num Seats equal to 0
    If (Me.ef_num_seats.Value = "0") Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(4994))
      Call Me.ef_num_seats.Focus()
      Return False
    End If

    For Each _id_gaming_table As Integer In GetGamingTableSelected()
      _rows = m_gaming_table_desing.Select("GT_GAMING_TABLE_ID = " & _id_gaming_table)

      If _rows.Length = 0 Then
        Continue For
      End If

      If _rows(0)("GT_NUM_SEATS") <> Me.ef_num_seats.Value And m_gaming_table_id <> _rows(0)("GT_GAMING_TABLE_ID") Then
        _show_message_seat = True
      End If
    Next

    If _show_message_seat Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5000), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , ) = ENUM_MB_RESULT.MB_RESULT_NO Then
        m_cancel_by_user = True
        Call Me.dg_gaming_tables.Focus()
        Return False
      End If
    End If

    If Not m_save_gaming_table_design Then
      Using _db_trx As New DB_TRX()
        If GamingTablesSessions.GetOpenedSession(m_gaming_table_id, _gaming_table_session, _db_trx.SqlTransaction) Then
          If Not _gaming_table_session Is Nothing Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5586), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK, , )
            Return False
          Else
            panel_table_design.GetXMLGamingTable(m_gaming_table_xml)
          End If

        End If
      End Using
    End If

    Return True

  End Function ' GUI_IsScreenDataOk


  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_OK
        If gb_gaming_tables_to_apply.Visible Then
          MyBase.CloseOnOkClick = False
          Call MyBase.GUI_ButtonClick(ButtonId)
          ShowGamingTableUpdated()
          MyBase.CloseOnOkClick = True
        Else
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4997)
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(31)
    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    AddHandler panel_table_design.SelectedObjectChanged, AddressOf panel_table_design_SelectedObjectChanged

    gb_configuration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4993, String.Empty)
    ef_num_seats.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4994)
    ef_num_seats.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)

    lbl_properties.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4995)
    gb_gaming_tables_to_apply.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4996)

    gb_design.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(486) ' NLS_ID_GUI_PLAYER_TRACKING(486)   "Preview"

    If m_gaming_table_id = 0 Or Not m_save_gaming_table_design Then
      gb_configuration.Size = New Size(gb_configuration.Width, gb_design.Height)
      gb_gaming_tables_to_apply.Visible = False
    End If

    Call GUI_StyleSheetGamingTableGrid()

    ' Set grid gaming tables
    Call SetGamingTablesGrid()

  End Sub


#End Region

#Region "Public Functions"

  ' PURPOSE : Functions to load the form
  '
  '  PARAMS :
  '     - INPUT :
  '           - GamingTableId
  '
  '     - OUTPUT :
  '
  ' RETURNS:

  Public Overloads Sub ShowEditItem(ByVal GamingTableId As Integer, Optional ByRef SaveGamingTableDesign As Boolean = True, Optional ByRef GamingTableXml As String = "", Optional ByVal TableName As String = "")
    Dim _gaming_tables_status As DataTable
    Dim _list_tables As List(Of Integer)

    _list_tables = New List(Of Integer)
    _list_tables.Add(GamingTableId)

    _gaming_tables_status = Nothing
    m_gaming_table_id = GamingTableId
    m_gaming_table_xml = GamingTableXml
    m_gaming_table_name = TableName
    m_save_gaming_table_design = SaveGamingTableDesign

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    If Not SaveGamingTableDesign Then
      Me.ScreenMode = -1
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

    GamingTableXml = m_gaming_table_xml
  End Sub


  Public Sub InitializeTable()

    Dim _rows As DataRow()
    Dim _idx_row As Integer

    ' Load table
    If (m_gaming_table_id <> 0 And m_save_gaming_table_design And panel_table_design.LoadGamingTable(m_gaming_table_id, True)) Then
      _rows = m_gaming_table_desing.Select("GT_GAMING_TABLE_ID = " & m_gaming_table_id)
      If _rows.Length > 0 Then
        ef_num_seats.Value = IIf(_rows(0)("GT_NUM_SEATS") Is DBNull.Value, 0, _rows(0)("GT_NUM_SEATS"))
      Else
        ef_num_seats.Value = 0
      End If
    Else
      panel_table_design.LoadGamingTable(m_gaming_table_xml)
      ef_num_seats.Value = panel_table_design.NumSeats

    End If

    panel_table_design.SetGamingTableAsSelectedObject()

    ' Check gaming table
    For _idx_row = 0 To Me.dg_gaming_tables.NumRows - 1

      If dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_TABLE_ID).Value = m_gaming_table_id Then
        dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      Else
        dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If
    Next

  End Sub

  ' PURPOSE : Set combo with cashier alias
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 

  Public Sub SetGamingTablesGrid()
    Dim _table As DataTable
    'Dim _new_table As DataTable
    'Dim _query As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder

    '_query = "SELECT GT_GAMING_TABLE_ID, GT_NAME, GT_NUM_SEATS FROM GAMING_TABLES"

    _sb.AppendLine("	SELECT   GT_GAMING_TABLE_ID		                                ")
    _sb.AppendLine("	       , GT_NAME, GT_NUM_SEATS		                            ")
    _sb.AppendLine("	  FROM   GAMING_TABLES		                                    ")
    _sb.AppendLine("	 WHERE   GT_GAMING_TABLE_ID <> " & Me.m_gaming_table_id & "	  ")

    '_table = GUI_GetTableUsingSQL(_query, Integer.MaxValue)

    _table = GUI_GetTableUsingSQL(_sb.ToString, Integer.MaxValue)

    For Each _row As DataRow In _table.Rows
      Dim _prev_redraw As Boolean

      _prev_redraw = Me.dg_gaming_tables.Redraw
      Me.dg_gaming_tables.Redraw = False

      dg_gaming_tables.AddRow()

      Me.dg_gaming_tables.Redraw = False

      dg_gaming_tables.Cell(dg_gaming_tables.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      dg_gaming_tables.Cell(dg_gaming_tables.NumRows - 1, GRID_COLUMN_TABLE_ID).Value = _row("GT_GAMING_TABLE_ID")
      dg_gaming_tables.Cell(dg_gaming_tables.NumRows - 1, GRID_COLUMN_TABLE_NAME).Value = _row("GT_NAME") & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5001, IIf(_row("GT_NUM_SEATS") Is DBNull.Value, 0, _row("GT_NUM_SEATS"))) & ")"

      Me.dg_gaming_tables.Redraw = _prev_redraw
    Next


  End Sub ' SetCombo


  ' PURPOSE: Get selected gaming table ids
  '          
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetGamingTableSelected() As List(Of Integer)

    Dim _idx_row As Integer
    Dim _gaming_table_ids As List(Of Integer)

    _gaming_table_ids = New List(Of Integer)

    For _idx_row = 0 To Me.dg_gaming_tables.NumRows - 1
      If dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _gaming_table_ids.Add(dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_TABLE_ID).Value)
      End If
    Next

    If Me.m_gaming_table_id <> 0 Then
      _gaming_table_ids.Add(Me.m_gaming_table_id)
    End If

    Return _gaming_table_ids
  End Function

  Private Sub ShowGamingTableUpdated()
    Dim _gaming_table_desing As CLASS_GAMING_TABLE_DESING
    Dim _item As CLASS_GAMING_TABLE_DESING.TYPE_GAMING_TABLE_DESIGN
    Dim _table As DataRow()
    Dim _xml_table As String
    Dim _tables_not_updated As Boolean
    Dim _idx_row As Int32
    Dim _tables_not_updated_names As String
    Dim _tables_not_updated_names_count As Integer

    _xml_table = String.Empty
    _tables_not_updated = False
    panel_table_design.GetXMLGamingTable(_xml_table)
    _tables_not_updated_names = vbCrLf
    _tables_not_updated_names_count = 0

    Try

      _gaming_table_desing = DbEditedObject

      ' Update gaming table data
      For Each _item In _gaming_table_desing.GamingTablesDesingList

        _table = m_gaming_table_desing.Select("GT_GAMING_TABLE_ID = " & _item.gaming_table_id)

        If _table.Length > 0 Then

          _table(0)("ROW_NOT_UPDATED") = False 'GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

          If GetGamingTableSelected().Contains(_item.GamingTableID) Then
            ' Table updated
            If Not m_cancel_by_user And Not _item.session_opened And _item.updated Then
              _table(0)("GT_NUM_SEATS") = ef_num_seats.Value
              _table(0)("GT_DESIGN_XML") = _xml_table
              ' Table not updated
            ElseIf Not m_cancel_by_user And (_item.session_opened Or Not _item.updated) Then
              _tables_not_updated = True
              If _tables_not_updated_names_count < 4 Then
                _tables_not_updated_names &= vbCrLf & " - " & _item.GamingTableName
              ElseIf _tables_not_updated_names_count = 4 Then
                _tables_not_updated_names &= vbCrLf & " - ..."
              End If
              _tables_not_updated_names_count += 1
              _table(0)("ROW_NOT_UPDATED") = True ' GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
            End If
          End If
        End If

        _item.updated = False

      Next

      m_gaming_table_desing.AcceptChanges()

      For _idx_row = 0 To dg_gaming_tables.NumRows - 1

        _table = m_gaming_table_desing.Select("GT_GAMING_TABLE_ID = " & Me.dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_TABLE_ID).Value)

        If _table.Length > 0 Then

          dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_TABLE_NAME).Value = _table(0)("GT_NAME") & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5001, IIf(_table(0)("GT_NUM_SEATS") Is DBNull.Value, 0, _table(0)("GT_NUM_SEATS"))) & ")"

          If Not _table(0)("ROW_NOT_UPDATED") Is DBNull.Value And _table(0)("ROW_NOT_UPDATED") Then
            Me.dg_gaming_tables.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          Else
            Me.dg_gaming_tables.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
          End If
        End If

      Next

      Me.dg_gaming_tables.Redraw = True
    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

    If _tables_not_updated Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5605), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_OK, , _tables_not_updated_names)
    End If

  End Sub

  ' PURPOSE: Define all Grid Source Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetGamingTableGrid()
    With Me.dg_gaming_tables
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_COLUMN_TABLE_ID).Header.Text = ""
      .Column(GRID_COLUMN_TABLE_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_COLUMN_CHECKED).Fixed = True
      .Column(GRID_COLUMN_CHECKED).Editable = True
      .Column(GRID_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_CHECKED).HighLightWhenSelected = False

      ' GRID_COL_DESCRIPTION
      .Column(GRID_COLUMN_TABLE_NAME).Header.Text = ""
      .Column(GRID_COLUMN_TABLE_NAME).WidthFixed = 3520
      .Column(GRID_COLUMN_TABLE_NAME).Fixed = True
      .Column(GRID_COLUMN_TABLE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_TABLE_NAME).HighLightWhenSelected = False


    End With

  End Sub 'GUI_StyleSheetType

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

#End Region ' Public Functions

#Region "Events"

  Private Sub ef_num_seats_EntryFieldValueChanged() Handles ef_num_seats.EntryFieldValueChanged

    'If Not String.IsNullOrEmpty(ef_num_seats.Value) And GUI_ParseNumber(ef_num_seats.Value) >= 0 Then
    '  panel_table_design.UpdateSeatsTable(ef_num_seats.Value)
    'End If

  End Sub

  Private Sub pg_properties_PropertyValueChanged(ByVal s As System.Object, ByVal e As System.Windows.Forms.PropertyValueChangedEventArgs) Handles pg_properties.PropertyValueChanged
    panel_table_design.UpdateObject(pg_properties.SelectedObject)
  End Sub

  Private Sub panel_table_design_SelectedObjectChanged(ByVal sender As System.Object, ByVal e As WSI.Common.ObjectPanelEventArgs) Handles panel_table_design.SelectedObjectChanged
    If panel_table_design.SelectedObject Is Nothing Then
      pg_properties.SelectedObject = Nothing
      lbl_properties.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4995)   ' 4995 -  "Propiedades"
    ElseIf TypeOf panel_table_design.SelectedObject Is WSI.Common.GamingTableObject Then
      pg_properties.SelectedObject = DirectCast(panel_table_design.SelectedObject, GamingTableObject).TableProperties
      lbl_properties.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5406)  ' 5406 -  "Propiedades mesa"
    ElseIf TypeOf panel_table_design.SelectedObject Is WSI.Common.GamingSeatObject Then
      pg_properties.SelectedObject = DirectCast(panel_table_design.SelectedObject, GamingSeatObject).SeatProperties
      lbl_properties.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5407)  ' 5407 -  "Propiedades asiento"
    End If
  End Sub

  Private Sub dg_gaming_tables_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_gaming_tables.CellDataChangedEvent
    Dim _idx_row As Integer

    For Each _idx_row In dg_gaming_tables.SelectedRows
      If _dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_TABLE_ID).Value = m_gaming_table_id Then
        dg_gaming_tables.Cell(_idx_row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        Exit For
      End If
    Next
  End Sub

  ' PURPOSE: Handler to control key pressed intro
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar

      Case Chr(Keys.Enter)
        If Me.ActiveControl.Name = ef_num_seats.Name Then
          ef_num_seats_Leave(Nothing, Nothing)
        End If
        Return True

      Case Chr(Keys.Escape)
        If Me.pg_properties.ContainsFocus() Then
          Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Focus()
          Return True
        End If
        Return MyBase.GUI_KeyPress(sender, e)

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)

    End Select

  End Function ' GUI_KeyPress

  Private Sub ef_num_seats_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_num_seats.Leave
    Dim _max_seats As Int32
    If Not String.IsNullOrEmpty(ef_num_seats.Value) And GUI_ParseNumber(ef_num_seats.Value) >= 0 Then
      _max_seats = GeneralParam.GetInt32("GamingTables", "Seats.MaxAllowedSeats", 10)
      If ef_num_seats.Value > _max_seats Then
        ef_num_seats.Value = _max_seats
      End If
      panel_table_design.UpdateSeatsTable(ef_num_seats.Value)
    End If
  End Sub

#End Region 'Events

End Class