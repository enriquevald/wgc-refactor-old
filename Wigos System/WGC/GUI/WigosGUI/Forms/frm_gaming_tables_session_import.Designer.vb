<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_gaming_tables_session_import
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dti_excel_import = New GUI_Controls.uc_data_import()
    Me.gb_output = New System.Windows.Forms.GroupBox()
    Me.pgb_import_progress = New System.Windows.Forms.ProgressBar()
    Me.tb_output = New System.Windows.Forms.TextBox()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.gb_output.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_description)
    Me.panel_data.Controls.Add(Me.dti_excel_import)
    Me.panel_data.Controls.Add(Me.gb_output)
    Me.panel_data.Size = New System.Drawing.Size(667, 438)
    '
    'dti_excel_import
    '
    Me.dti_excel_import.AutoSize = True
    Me.dti_excel_import.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.dti_excel_import.ExcelUseCurrentRegion = True
    Me.dti_excel_import.Location = New System.Drawing.Point(9, 30)
    Me.dti_excel_import.LogFile = Nothing
    Me.dti_excel_import.Name = "dti_excel_import"
    Me.dti_excel_import.Size = New System.Drawing.Size(545, 31)
    Me.dti_excel_import.TabIndex = 7
    Me.dti_excel_import.Value = ""
    '
    'gb_output
    '
    Me.gb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_output.Controls.Add(Me.pgb_import_progress)
    Me.gb_output.Controls.Add(Me.tb_output)
    Me.gb_output.Location = New System.Drawing.Point(3, 67)
    Me.gb_output.Name = "gb_output"
    Me.gb_output.Size = New System.Drawing.Size(652, 369)
    Me.gb_output.TabIndex = 6
    Me.gb_output.TabStop = False
    Me.gb_output.Text = "xOutput"
    '
    'pgb_import_progress
    '
    Me.pgb_import_progress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.pgb_import_progress.Location = New System.Drawing.Point(6, 339)
    Me.pgb_import_progress.Name = "pgb_import_progress"
    Me.pgb_import_progress.Size = New System.Drawing.Size(640, 23)
    Me.pgb_import_progress.TabIndex = 1
    '
    'tb_output
    '
    Me.tb_output.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tb_output.Location = New System.Drawing.Point(6, 21)
    Me.tb_output.Multiline = True
    Me.tb_output.Name = "tb_output"
    Me.tb_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.tb_output.Size = New System.Drawing.Size(640, 318)
    Me.tb_output.TabIndex = 0
    '
    'lbl_description
    '
    Me.lbl_description.AutoSize = True
    Me.lbl_description.Location = New System.Drawing.Point(73, 14)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(119, 13)
    Me.lbl_description.TabIndex = 8
    Me.lbl_description.Text = "Seleccionar archivo"
    '
    'frm_gaming_tables_session_import
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(769, 447)
    Me.Name = "frm_gaming_tables_session_import"
    Me.Text = "frm_gaming_tables_session_import"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_output.ResumeLayout(False)
    Me.gb_output.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dti_excel_import As GUI_Controls.uc_data_import
  Friend WithEvents gb_output As System.Windows.Forms.GroupBox
  Friend WithEvents pgb_import_progress As System.Windows.Forms.ProgressBar
  Friend WithEvents tb_output As System.Windows.Forms.TextBox
  Friend WithEvents lbl_description As System.Windows.Forms.Label
End Class
