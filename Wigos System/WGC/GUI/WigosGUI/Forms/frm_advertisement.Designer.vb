<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_advertisement
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_campaign = New System.Windows.Forms.GroupBox()
    Me.dtp_past = New GUI_Controls.uc_date_picker()
    Me.dtp_now = New GUI_Controls.uc_date_picker()
    Me.opt_custom = New System.Windows.Forms.RadioButton()
    Me.dtp_future = New GUI_Controls.uc_date_picker()
    Me.opt_future = New System.Windows.Forms.RadioButton()
    Me.opt_past = New System.Windows.Forms.RadioButton()
    Me.opt_now = New System.Windows.Forms.RadioButton()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.chk_disabled = New System.Windows.Forms.CheckBox()
    Me.ef_campaign_name = New GUI_Controls.uc_entry_field()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_campaign.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.GroupBox1)
    Me.panel_filter.Controls.Add(Me.gb_enabled)
    Me.panel_filter.Controls.Add(Me.gb_campaign)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Location = New System.Drawing.Point(6, 5)
    Me.panel_filter.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.panel_filter.Padding = New System.Windows.Forms.Padding(4, 4, 0, 0)
    Me.panel_filter.Size = New System.Drawing.Size(1276, 190)
    Me.panel_filter.TabIndex = 1
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_campaign, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_enabled, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.GroupBox1, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(6, 195)
    Me.panel_data.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.panel_data.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.panel_data.Size = New System.Drawing.Size(1276, 632)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 4)
    Me.pn_separator_line.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.pn_separator_line.Padding = New System.Windows.Forms.Padding(0, 10, 0, 10)
    Me.pn_separator_line.Size = New System.Drawing.Size(1268, 30)
    '
    'pn_line
    '
    Me.pn_line.Location = New System.Drawing.Point(0, 10)
    Me.pn_line.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.pn_line.Size = New System.Drawing.Size(1268, 4)
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Enabled = False
    Me.gb_date.Location = New System.Drawing.Point(377, 67)
    Me.gb_date.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.gb_date.Size = New System.Drawing.Size(314, 115)
    Me.gb_date.TabIndex = 2
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(9, 67)
    Me.dtp_to.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(285, 28)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 64
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(9, 25)
    Me.dtp_from.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(285, 28)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 64
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_campaign
    '
    Me.gb_campaign.Controls.Add(Me.dtp_past)
    Me.gb_campaign.Controls.Add(Me.dtp_now)
    Me.gb_campaign.Controls.Add(Me.opt_custom)
    Me.gb_campaign.Controls.Add(Me.dtp_future)
    Me.gb_campaign.Controls.Add(Me.opt_future)
    Me.gb_campaign.Controls.Add(Me.opt_past)
    Me.gb_campaign.Controls.Add(Me.opt_now)
    Me.gb_campaign.Location = New System.Drawing.Point(12, 3)
    Me.gb_campaign.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.gb_campaign.Name = "gb_campaign"
    Me.gb_campaign.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.gb_campaign.Size = New System.Drawing.Size(348, 180)
    Me.gb_campaign.TabIndex = 0
    Me.gb_campaign.TabStop = False
    Me.gb_campaign.Text = "xCampaign"
    '
    'dtp_past
    '
    Me.dtp_past.Checked = True
    Me.dtp_past.Enabled = False
    Me.dtp_past.IsReadOnly = False
    Me.dtp_past.Location = New System.Drawing.Point(132, 94)
    Me.dtp_past.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_past.Name = "dtp_past"
    Me.dtp_past.ShowCheckBox = False
    Me.dtp_past.ShowUpDown = False
    Me.dtp_past.Size = New System.Drawing.Size(189, 28)
    Me.dtp_past.SufixText = "Sufix Text"
    Me.dtp_past.SufixTextVisible = True
    Me.dtp_past.TabIndex = 2
    Me.dtp_past.TextVisible = False
    Me.dtp_past.TextWidth = 0
    Me.dtp_past.Value = New Date(2012, 5, 18, 18, 6, 28, 171)
    '
    'dtp_now
    '
    Me.dtp_now.Checked = True
    Me.dtp_now.Enabled = False
    Me.dtp_now.IsReadOnly = False
    Me.dtp_now.Location = New System.Drawing.Point(132, 34)
    Me.dtp_now.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_now.Name = "dtp_now"
    Me.dtp_now.ShowCheckBox = False
    Me.dtp_now.ShowUpDown = False
    Me.dtp_now.Size = New System.Drawing.Size(189, 28)
    Me.dtp_now.SufixText = "Sufix Text"
    Me.dtp_now.SufixTextVisible = True
    Me.dtp_now.TabIndex = 0
    Me.dtp_now.TextVisible = False
    Me.dtp_now.TextWidth = 0
    Me.dtp_now.Value = New Date(2012, 5, 18, 18, 6, 28, 171)
    '
    'opt_custom
    '
    Me.opt_custom.AutoSize = True
    Me.opt_custom.Location = New System.Drawing.Point(23, 132)
    Me.opt_custom.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.opt_custom.Name = "opt_custom"
    Me.opt_custom.Size = New System.Drawing.Size(93, 21)
    Me.opt_custom.TabIndex = 6
    Me.opt_custom.TabStop = True
    Me.opt_custom.Text = "xCustom"
    Me.opt_custom.UseVisualStyleBackColor = True
    '
    'dtp_future
    '
    Me.dtp_future.Checked = True
    Me.dtp_future.Enabled = False
    Me.dtp_future.IsReadOnly = False
    Me.dtp_future.Location = New System.Drawing.Point(132, 64)
    Me.dtp_future.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.dtp_future.Name = "dtp_future"
    Me.dtp_future.ShowCheckBox = False
    Me.dtp_future.ShowUpDown = False
    Me.dtp_future.Size = New System.Drawing.Size(189, 28)
    Me.dtp_future.SufixText = "Sufix Text"
    Me.dtp_future.SufixTextVisible = True
    Me.dtp_future.TabIndex = 1
    Me.dtp_future.TextVisible = False
    Me.dtp_future.TextWidth = 0
    Me.dtp_future.Value = New Date(2012, 5, 18, 18, 6, 28, 171)
    '
    'opt_future
    '
    Me.opt_future.AutoSize = True
    Me.opt_future.Location = New System.Drawing.Point(23, 69)
    Me.opt_future.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.opt_future.Name = "opt_future"
    Me.opt_future.Size = New System.Drawing.Size(92, 21)
    Me.opt_future.TabIndex = 4
    Me.opt_future.TabStop = True
    Me.opt_future.Text = "xFuturas"
    Me.opt_future.UseVisualStyleBackColor = True
    '
    'opt_past
    '
    Me.opt_past.AutoSize = True
    Me.opt_past.Location = New System.Drawing.Point(23, 99)
    Me.opt_past.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.opt_past.Name = "opt_past"
    Me.opt_past.Size = New System.Drawing.Size(95, 21)
    Me.opt_past.TabIndex = 5
    Me.opt_past.TabStop = True
    Me.opt_past.Text = "xPasadas"
    Me.opt_past.UseVisualStyleBackColor = True
    '
    'opt_now
    '
    Me.opt_now.AutoSize = True
    Me.opt_now.Checked = True
    Me.opt_now.Location = New System.Drawing.Point(23, 41)
    Me.opt_now.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.opt_now.Name = "opt_now"
    Me.opt_now.Size = New System.Drawing.Size(97, 21)
    Me.opt_now.TabIndex = 3
    Me.opt_now.TabStop = True
    Me.opt_now.Text = "xActuales"
    Me.opt_now.UseVisualStyleBackColor = True
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(35, 37)
    Me.chk_enabled.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(94, 21)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.chk_disabled)
    Me.gb_enabled.Controls.Add(Me.chk_enabled)
    Me.gb_enabled.Enabled = False
    Me.gb_enabled.Location = New System.Drawing.Point(708, 67)
    Me.gb_enabled.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.gb_enabled.Size = New System.Drawing.Size(195, 115)
    Me.gb_enabled.TabIndex = 3
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'chk_disabled
    '
    Me.chk_disabled.AutoSize = True
    Me.chk_disabled.Location = New System.Drawing.Point(35, 72)
    Me.chk_disabled.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.chk_disabled.Name = "chk_disabled"
    Me.chk_disabled.Size = New System.Drawing.Size(98, 21)
    Me.chk_disabled.TabIndex = 1
    Me.chk_disabled.Text = "xDisabled"
    Me.chk_disabled.UseVisualStyleBackColor = True
    '
    'ef_campaign_name
    '
    Me.ef_campaign_name.DoubleValue = 0.0R
    Me.ef_campaign_name.Enabled = False
    Me.ef_campaign_name.IntegerValue = 0
    Me.ef_campaign_name.IsReadOnly = False
    Me.ef_campaign_name.Location = New System.Drawing.Point(8, 16)
    Me.ef_campaign_name.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.ef_campaign_name.Name = "ef_campaign_name"
    Me.ef_campaign_name.PlaceHolder = Nothing
    Me.ef_campaign_name.Size = New System.Drawing.Size(509, 28)
    Me.ef_campaign_name.SufixText = "Sufix Text"
    Me.ef_campaign_name.SufixTextVisible = True
    Me.ef_campaign_name.TabIndex = 0
    Me.ef_campaign_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_campaign_name.TextValue = ""
    Me.ef_campaign_name.TextWidth = 77
    Me.ef_campaign_name.Value = ""
    Me.ef_campaign_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.ef_campaign_name)
    Me.GroupBox1.Location = New System.Drawing.Point(377, 3)
    Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.GroupBox1.Size = New System.Drawing.Size(527, 55)
    Me.GroupBox1.TabIndex = 1
    Me.GroupBox1.TabStop = False
    '
    'frm_advertisement
    '
    'Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 17.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1288, 832)
    Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
    Me.Name = "frm_advertisement"
    Me.Padding = New System.Windows.Forms.Padding(6, 5, 6, 5)
    Me.Text = "frm_advertisement"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_campaign.ResumeLayout(False)
    Me.gb_campaign.PerformLayout()
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_enabled.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_campaign As System.Windows.Forms.GroupBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents opt_future As System.Windows.Forms.RadioButton
  Friend WithEvents opt_past As System.Windows.Forms.RadioButton
  Friend WithEvents opt_now As System.Windows.Forms.RadioButton
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents chk_disabled As System.Windows.Forms.CheckBox
  Friend WithEvents opt_custom As System.Windows.Forms.RadioButton
  Friend WithEvents ef_campaign_name As GUI_Controls.uc_entry_field
  Friend WithEvents dtp_now As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_past As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_future As GUI_Controls.uc_date_picker
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
