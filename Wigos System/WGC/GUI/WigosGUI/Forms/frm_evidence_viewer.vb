'-------------------------------------------------------------------
' Copyright � 2007-2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_evidence_viewer
' DESCRIPTION:   Shows the documents related to a prize evidence
' AUTHOR:        Javier Molina
' CREATION DATE: 01-FEB-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-FEB-2012  JMM    Initial version.
' 09-FEB-2012  JMM    Operation data added
' 10-FEB-2012  JMM    Evidence printing added
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 22-MAR-2017  ATB    Product Backlog Item 25951: Third TAX - Reports
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports WSI.Common

Public Class frm_evidence_viewer
  Inherits frm_base_edit

#Region " Structures "

  Public Class WITHOLDING_OPERATION_DATA
    Public m_oeperation_id As Int64
    Public m_account_id As Int64
    Public m_op_date As Date
    Public m_holder_name As String
    Public m_RFC As String
    Public m_CURP As String
    Public m_prize As Decimal
    Public m_tax1 As Decimal
    Public m_tax2 As Decimal
    Public m_tax3 As Decimal
    Public m_total_payment As Decimal
    Public m_doc_list As DocumentList

    Public Sub New(ByVal OperationID As Int64, _
                   ByVal AccountID As Int64, _
                   ByVal OpDate As Date, _
                   ByVal HolderName As String, _
                   ByVal RFC As String, _
                   ByVal CURP As String, _
                   ByVal Prize As Decimal, _
                   ByVal Tax1 As Decimal, _
                   ByVal Tax2 As Decimal, _
                   ByVal Tax3 As Decimal, _
                   ByVal TotalPayment As Decimal, _
                   ByVal DocList As DocumentList)
      m_oeperation_id = OperationID
      m_account_id = AccountID
      m_op_date = OpDate
      m_holder_name = HolderName
      m_RFC = RFC
      m_CURP = CURP
      m_prize = Prize
      m_tax1 = Tax1
      m_tax2 = Tax2
      m_tax3 = Tax3
      m_total_payment = TotalPayment
      m_doc_list = DocList
    End Sub
  End Class
#End Region ' Structures

#Region " Members "
  Private m_operation_data As WITHOLDING_OPERATION_DATA
  Private m_temp_files() As String
  Private m_temp_folder As String

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_EVIDENCE_VIEWER

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :

  Protected Overrides Sub GUI_InitControls()

    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' Evidence viewer
    Me.Text = GLB_NLS_GUI_INVOICING.GetString(444)

    'Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(5)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True

    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(25)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = True




    'Operation data fields
    'group box
    Me.gb_operation.Text = GLB_NLS_GUI_INVOICING.GetString(350)

    'date
    Me.dt_operation_date.Text = GLB_NLS_GUI_INVOICING.GetString(201)
    Me.dt_operation_date.IsReadOnly = True
    Me.dt_operation_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'holder name
    Me.ef_evidence_holder_name.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.ef_evidence_holder_name.IsReadOnly = True

    'RFC
    Me.ef_evidence_RFC.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    Me.ef_evidence_RFC.IsReadOnly = True

    'CURP
    Me.ef_evidence_CURP.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)
    Me.ef_evidence_CURP.IsReadOnly = True

    'Gross prize
    Me.ef_prize_amount.Text = GLB_NLS_GUI_INVOICING.GetString(33)
    Me.ef_prize_amount.IsReadOnly = True
    Me.ef_prize_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    'Tax1
    Me.ef_tax1_amount.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name")
    Me.ef_tax1_amount.IsReadOnly = True
    Me.ef_tax1_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    'Tax2
    Me.ef_tax2_amount.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name")
    Me.ef_tax2_amount.IsReadOnly = True
    Me.ef_tax2_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    'Tax3
    Me.ef_tax3_amount.Text = WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name")
    Me.ef_tax3_amount.IsReadOnly = True
    Me.ef_tax3_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    ' Total payment
    Me.ef_total_payment.Text = GLB_NLS_GUI_INVOICING.GetString(432)
    Me.ef_total_payment.IsReadOnly = True
    Me.ef_total_payment.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9, 2)

    


    Call InitTabControl()

  End Sub 'GUI_InitControls  

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    'Temp PDF files clean up
    Call Me.wb_evidence.Dispose()
    Call DeleteTempFiles(m_temp_folder)
  End Sub 'GUI_Closing

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Me.dt_operation_date.Value = m_operation_data.m_op_date
    Me.ef_evidence_holder_name.Value = m_operation_data.m_holder_name
    Me.ef_evidence_RFC.Value = m_operation_data.m_RFC
    Me.ef_evidence_CURP.Value = m_operation_data.m_CURP
    Me.ef_prize_amount.Value = m_operation_data.m_prize
    Me.ef_tax1_amount.Value = m_operation_data.m_tax1
    Me.ef_tax2_amount.Value = m_operation_data.m_tax2
    Me.ef_tax3_amount.Value = m_operation_data.m_tax3
    Me.ef_total_payment.Value = m_operation_data.m_total_payment

  End Sub 'GUI_SetScreenData

  ' PURPOSE: Process clicks on data grid (double-clicks) and select button
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call PrintEvidence()
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        ShowEvidenceEdit()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Initilizes the tab control where the documents will be shown and shows the first one
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub InitTabControl()

    Dim _idx As Integer

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      If m_operation_data.m_doc_list.Count < 1 Then

        '108 "No documents related to the evidence found."
        NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id((108)), _
                 ENUM_MB_TYPE.MB_TYPE_INFO, _
                 ENUM_MB_BTN.MB_BTN_OK)

        Call Me.Close()

        Return
      End If

      m_temp_folder = ""
      m_temp_files = New String(m_operation_data.m_doc_list.Count - 1) {}

      For _idx = 0 To m_operation_data.m_doc_list.Count - 1

        Select Case _idx
          Case 0
            'add the Tax1 document tab
            Me.tab_evidence_viewer.TabPages.Add(GLB_NLS_GUI_INVOICING.GetString(437) & " " & WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name"))

          Case 1
            'add the Tax2 document tab
            Me.tab_evidence_viewer.TabPages.Add(GLB_NLS_GUI_INVOICING.GetString(437) & " " & WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name"))

          Case 2
            'add the Tax3 document tab
            Me.tab_evidence_viewer.TabPages.Add(GLB_NLS_GUI_INVOICING.GetString(437) & " " & WSI.Common.Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name"))

          Case Else
            'from third document and up (if there are), the tab will show the filename
            Me.tab_evidence_viewer.TabPages.Add(m_operation_data.m_doc_list(_idx).Name)

        End Select

      Next

      'when all documents tabs loaded, let's show the first one
      Call ShowTabPagePDF(0)

    Catch ex As Exception

      NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id((123)), _
                 ENUM_MB_TYPE.MB_TYPE_ERROR, _
                 ENUM_MB_BTN.MB_BTN_OK)

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Evidence Viewer ", _
                            "InitTabControl", _
                            ex.Message)

      'If something goes wrong loading evidences from DB, the form should be closed
      Call Me.Close()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try

  End Sub 'InitTabControl

  ' PURPOSE: Gets the tab page activated for the user and shows its corresponding PDF on the web browser
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub ShowTabPagePDF(ByVal TabIndex As Integer)

    Dim _document_path As String

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

      'set the webbrowser to a blank page
      Call wb_evidence.Navigate("about:blank")

      If TabIndex < Me.tab_evidence_viewer.TabPages.Count Then

        If TabIndex < Me.m_operation_data.m_doc_list.Count Then

          'If there is a envidence related to the current tab...
          If Not Me.m_operation_data.m_doc_list(TabIndex) Is Nothing Then
            '...we look for its temp PDF file...
            _document_path = GetTempPDF(TabIndex)

            If _document_path IsNot Nothing Then
              '...and the tell the webbrowser to show it
              Call Me.wb_evidence.Navigate(_document_path & "#toolbar=0&statusbar=0&navpanes=0&view=FitH")

              Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
            End If
          End If
        End If

        'Finally, let's place the webbrowser control on the tab selected by user
        Me.wb_evidence.Parent = Me.tab_evidence_viewer.TabPages(TabIndex)
        Me.wb_evidence.Dock = DockStyle.Fill
      End If

    Catch ex As Exception

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Evidence Viewer ", _
                            "ShowTabPagePDF", _
                            ex.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default

    End Try
  End Sub 'ShowTabPagePDF

  ' PURPOSE: Saves a stream to a temp file to load it into the webbrowser control
  '         
  ' PARAMS:
  '    - INPUT:
  '       StreamPDF
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Function GetTempPDF(ByVal IdxPDF As Integer) As String
    Dim _temp_file As String

    Try

      'And empty file name means that this evidence haven't be saved to a temp file previously
      If m_temp_folder = "" Then

        'let's make a random and expected "unique" filename to save the evidence to a temp file
        m_temp_folder = Path.GetRandomFileName()
        m_temp_folder = m_temp_folder.Replace(".", "")
        m_temp_folder = Path.GetTempPath & m_temp_folder

        'ensure that the file doesn't exist
        While Directory.Exists(m_temp_folder)

          'if exists, let's make another random and expected "unique" filename
          m_temp_folder = Path.GetRandomFileName()
          m_temp_folder = m_temp_folder.Replace(".", "")
          m_temp_folder = Path.GetTempPath & m_temp_folder

        End While

        Directory.CreateDirectory(m_temp_folder)
      End If

      'loop up for a previously saved PDF
      _temp_file = m_temp_files(IdxPDF)

      If _temp_file Is Nothing _
        Or Not File.Exists(_temp_file) Then

        'if there is not a previously saved PDF, let's genereate it
        _temp_file = m_temp_folder & "\" & Me.m_operation_data.m_doc_list(IdxPDF).Name
        Call WriteStreamToTempFolder(_temp_file, Me.m_operation_data.m_doc_list(IdxPDF).Content)
        m_temp_files(IdxPDF) = _temp_file

      End If

      Return _temp_file

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "Evidence Viewer ", _
                            "GetTempPDF", _
                            ex.Message)

      Return Nothing

    End Try

  End Function 'GetTempPDF

  ' PURPOSE: Save the file stored previously on the DB to a temp file
  '         
  ' PARAMS:
  '    - INPUT:
  '       FilePath
  '        Buffer
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub WriteStreamToTempFolder(ByVal FilePath As String, ByVal Buffer() As Byte)
    Dim _file_stream As FileStream

    _file_stream = New FileStream(FilePath, FileMode.CreateNew, FileAccess.Write, FileShare.Read)

    _file_stream.Write(Buffer, 0, Buffer.Length)

    _file_stream.Close()

  End Sub 'WriteStreamToTempFolder

  ' PURPOSE: PDF temp files clean up 
  '         
  ' PARAMS:
  '    - INPUT:
  '       TempFiles
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub DeleteTempFiles(ByVal TempFolder As String)
    Dim _temp_files() As String
    Dim _temp_file As String
    Dim _error_found As Boolean

    Try

      'First, we check if there is a temp folder
      If TempFolder <> "" Then

        Do
          _error_found = False

          'look up for all the files on the temp folder
          _temp_files = Directory.GetFiles(TempFolder)

          For Each _temp_file In _temp_files
            'try to delete all the previously generated temp files
            Try
              File.Delete(_temp_file)

            Catch ex As Exception
              'if any delete fails...
              _error_found = True

              Call Trace.WriteLine(ex.ToString())
            End Try

          Next

          '...we will try it again a bit later
          If _error_found Then
            System.Threading.Thread.Sleep(1000)
          End If

        Loop While _error_found 'we'll loop until ensure all the temp files are deleted

        Directory.Delete(TempFolder)

      End If

    Catch ex As Exception

      Call Trace.WriteLine(ex.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      "Evidence Viewer ", _
                      "DeleteTempFiles", _
                      ex.Message)

    End Try

  End Sub 'DeleteTempFiles

  ' PURPOSE: Prints the evidence
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub PrintEvidence()

    Me.wb_evidence.ShowPrintDialog()

  End Sub 'PrintEvidence
  ' PURPOSE: Open Evidence Form Edit Dialog
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :
  Private Sub ShowEvidenceEdit()

    Dim frm_edit As Object
    frm_edit = New frm_evidence_edit
    Call frm_edit.ShowEditItem(m_operation_data.m_oeperation_id)

    frm_edit = Nothing

  End Sub
#End Region ' Private Functions

#Region " Public Functions "

  ' PURPOSE: Opens dialog getting the operation id needed to load the PDF from database
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                         ByVal OperationData As WITHOLDING_OPERATION_DATA)

    m_operation_data = OperationData

    Me.MdiParent = Nothing
    Me.Display(True)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

  Private Sub tab_evidence_viewer_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tab_evidence_viewer.Selected
    Call ShowTabPagePDF(e.TabPageIndex)
  End Sub



#End Region ' Events





End Class