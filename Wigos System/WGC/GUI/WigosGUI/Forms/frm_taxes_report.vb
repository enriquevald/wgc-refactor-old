'-------------------------------------------------------------------
' Copyright © 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_taxes_report
' DESCRIPTION:   This screen allows to view the taxes report:
'                           - between two dates 
'
' AUTHOR:        Alberto Cuesta
' CREATION DATE: 28-SEP-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-SEP-2010  ACC    Initial version
' 22-MAY-2013  RRB    Fixed Bug #795: Filtering with sessions instead of range date.
' 29-JAN-2014  RCI    Added CashIn tax support.
' 19-FEB-2014  AMF    Fixed Bug WIG-647: Control minimum version
' 26-APR-2016  EOR    Product Backlog Item 11298: Codere - Tax Custody: Reports GUI 
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Class frm_taxes_report
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_show_rows As System.Windows.Forms.GroupBox
  Friend WithEvents chk_company_a As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promotion As System.Windows.Forms.CheckBox
  Friend WithEvents chk_company_b As System.Windows.Forms.CheckBox

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.gb_show_rows = New System.Windows.Forms.GroupBox
    Me.chk_promotion = New System.Windows.Forms.CheckBox
    Me.chk_company_b = New System.Windows.Forms.CheckBox
    Me.chk_company_a = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_show_rows.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_show_rows)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Size = New System.Drawing.Size(479, 206)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_show_rows, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 210)
    Me.panel_data.Size = New System.Drawing.Size(479, 338)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(473, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(473, 4)
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(10, 2)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.Size = New System.Drawing.Size(257, 120)
    Me.uc_dsl.TabIndex = 0
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_show_rows
    '
    Me.gb_show_rows.Controls.Add(Me.chk_promotion)
    Me.gb_show_rows.Controls.Add(Me.chk_company_b)
    Me.gb_show_rows.Controls.Add(Me.chk_company_a)
    Me.gb_show_rows.Location = New System.Drawing.Point(10, 123)
    Me.gb_show_rows.Name = "gb_show_rows"
    Me.gb_show_rows.Size = New System.Drawing.Size(340, 79)
    Me.gb_show_rows.TabIndex = 1
    Me.gb_show_rows.TabStop = False
    '
    'chk_promotion
    '
    Me.chk_promotion.Checked = True
    Me.chk_promotion.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_promotion.Location = New System.Drawing.Point(6, 53)
    Me.chk_promotion.Name = "chk_promotion"
    Me.chk_promotion.Size = New System.Drawing.Size(312, 20)
    Me.chk_promotion.TabIndex = 2
    Me.chk_promotion.Text = "xPromotion Non Redeemable Funds"
    Me.chk_promotion.UseVisualStyleBackColor = True
    '
    'chk_company_b
    '
    Me.chk_company_b.Checked = True
    Me.chk_company_b.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_company_b.Location = New System.Drawing.Point(6, 33)
    Me.chk_company_b.Name = "chk_company_b"
    Me.chk_company_b.Size = New System.Drawing.Size(312, 20)
    Me.chk_company_b.TabIndex = 1
    Me.chk_company_b.Text = "xCompany B"
    Me.chk_company_b.UseVisualStyleBackColor = True
    '
    'chk_company_a
    '
    Me.chk_company_a.Checked = True
    Me.chk_company_a.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_company_a.Location = New System.Drawing.Point(6, 14)
    Me.chk_company_a.Name = "chk_company_a"
    Me.chk_company_a.Size = New System.Drawing.Size(312, 20)
    Me.chk_company_a.TabIndex = 0
    Me.chk_company_a.Text = "xCompany A"
    Me.chk_company_a.UseVisualStyleBackColor = True
    '
    'frm_taxes_report
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(487, 552)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.Name = "frm_taxes_report"
    Me.Text = "frm_taxes_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_show_rows.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 3
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CONCEPT As Integer = 1
  'Private Const GRID_COLUMN_OPERATION As Integer = 2
  Private Const GRID_COLUMN_VALUE As Integer = 2

  ' Width
  Private Const GRID_WIDTH_CONCEPT As Integer = 3300
  Private Const GRID_WIDTH_OPERATION As Integer = 0 '1600
  Private Const GRID_WIDTH_VALUE As Integer = 2180

  Private Const FORM_DB_MIN_VERSION As Short = 183

#End Region ' Constants

#Region " Members "

  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_company_A As String
  Private m_company_B As String
  Private m_company_A_name As String
  Private m_company_B_name As String
  Private m_nr_promo As String

  ' For amount grid
  Dim total_gross_amount As Double
  Dim total_federal_taxes_amount As Double
  Dim total_state_taxes_amount As Double

  Dim m_company_b_cheched As Boolean

#End Region ' Members

#Region " OVERRIDES "

  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function 'GUI_MaxRows

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TAXES_REPORT

    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()
    Dim _session_data As New Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _sql_trans As SqlTransaction = Nothing
    Dim _date_from As Date
    Dim _date_to As Date

    _date_from = Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime)
    _date_to = Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime)

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_INVOICING.GetString(346)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201))

    Try

      _sql_trans = WGDB.Connection().BeginTransaction()

      ' Populate structure from DB
      Cashier.ReadCashierSessionData(_sql_trans, _date_from, _date_to, _session_data)

      m_company_b_cheched = _session_data.split_enabled_b

      m_company_A_name = _session_data.split_a_company_name
      m_company_B_name = _session_data.split_b_company_name

      Me.chk_company_a.Text = _session_data.split_a_company_name
      Me.chk_company_b.Text = _session_data.split_b_company_name
      Me.chk_promotion.Text = GLB_NLS_GUI_INVOICING.GetString(351)

      If (Not m_company_b_cheched) Then
        Call HideCompanyB()
      End If

    Catch
    End Try

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate >= Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE : Fill results in the grid
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _session_data As New Cashier.TYPE_CASHIER_SESSION_STATS
    Dim _sql_trans As SqlTransaction = Nothing
    Dim _idx_row As Integer
    Dim _date_from As Date
    Dim _date_to As Date
    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.Opening(WGDB.Now, Me.uc_dsl.ClosingTime, 0)
    _date_from = IIf(Me.uc_dsl.FromDateSelected, Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), New Date(2007, 1, 1, Me.uc_dsl.ClosingTime, 0, 0))
    _date_to = IIf(Me.uc_dsl.ToDateSelected, Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _today_opening.AddDays(1))

    Try

      Call Me.Grid.Clear()
      Me.Grid.Redraw = False

      _sql_trans = WGDB.Connection().BeginTransaction()

      ' Populate structure from DB
      Cashier.ReadCashierSessionData(_sql_trans, _date_from, _date_to, _session_data)

      With Me.Grid

        If (chk_company_a.Checked) Then
          '    - Company A ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = _session_data.split_a_company_name
          '    - Inputs net A ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + _session_data.split_a_name
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "A"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_a_total_in_net.ToString()
          '    - Devoluciones net - A ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + GLB_NLS_GUI_INVOICING.GetString(352)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "F"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_a_total_dev_net.ToString()
          '    - Prizes
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + GLB_NLS_GUI_INVOICING.GetString(298)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "C"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_prize_gross.ToString()
          '    - Base Taxes A ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + GLB_NLS_GUI_INVOICING.GetString(354) + " " + _session_data.split_a_tax_name
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "B"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_a_base_taxes.ToString()
          '    - Taxes A ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + _session_data.split_a_tax_name + " " + Percentage.ToString(_session_data.split_a_tax_pct)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "B"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_a_taxes.ToString()

          If GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") OrElse CType(_session_data.cash_in_tax_split1, Decimal) > 0 Then
            '    - CashIn Taxes ->
            .AddRow()
            _idx_row = Me.Grid.NumRows - 1
            .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + GeneralParam.GetString("Cashier", "CashInTax.Name") + " " + Percentage.ToString(_session_data.cash_in_tax_pct)
            ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "B"
            .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.cash_in_tax_split1.ToString()
          End If

          'EOR 26-APR-2016
          If WSI.Common.Misc.IsTaxCustodyEnabled OrElse CType(_session_data.tax_custody, Decimal) > 0 Then
            .AddRow()
            _idx_row = Me.Grid.NumRows - 1
            .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + WSI.Common.Misc.TaxCustodyName + " " + Percentage.ToString(WSI.Common.Misc.TaxCustodyPct)
            .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.tax_custody.ToString()
          End If

          .AddRow()

          '    - Prizes taxes 1
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + _session_data.prize_taxes_1_name + " " + Percentage.ToString(_session_data.report_prize_tax_1_pct)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "D"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_prize_taxes_1.ToString()
          '    - Prizes taxes 2
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + _session_data.prize_taxes_2_name + " " + Percentage.ToString(_session_data.report_prize_tax_2_pct)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "E"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_prize_taxes_2.ToString()

          '''    - Total in cashier A
          ''.AddRow()
          ''_idx_row = Me.Grid.NumRows - 1
          ''.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          ''.Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = GLB_NLS_GUI_INVOICING.GetString(299)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "A+B-C+D+E-F"
          ''.Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_a_total_in_cashier.ToString()
          ''If (CType(_session_data.report_a_total_in_cashier, Decimal) < 0) Then
          ''  .Cell(_idx_row, GRID_COLUMN_VALUE).ForeColor = Color.Crimson
          ''End If
        End If

        If (chk_company_a.Checked And chk_company_b.Checked) Then
          .AddRow()
          .AddRow()
        End If

        If (chk_company_b.Checked) Then
          '    - Company B ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = _session_data.split_b_company_name
          '    - Inputs net B ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + _session_data.split_b_name
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "G"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_b_total_in_dev_net.ToString()
          '    - Inputs taxes B ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + _session_data.split_b_tax_name + " " + Percentage.ToString(_session_data.split_b_tax_pct)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "H"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_b_total_in_dev_taxes.ToString()

          If (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") AndAlso GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB")) OrElse _
              CType(_session_data.cash_in_tax_split2, Decimal) > 0 Then
            '    - CashIn Taxes ->
            .AddRow()
            _idx_row = Me.Grid.NumRows - 1
            .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = "   " + GeneralParam.GetString("Cashier", "CashInTax.Name") + " " + Percentage.ToString(_session_data.cash_in_tax_pct)
            ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "B"
            .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.cash_in_tax_split2.ToString()
          End If
          '''    - Total in cashier B
          ''.AddRow()
          ''_idx_row = Me.Grid.NumRows - 1
          ''.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
          ''.Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = GLB_NLS_GUI_INVOICING.GetString(299)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "G+H"
          ''.Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_b_total_in_cashier.ToString()
          ''If (CType(_session_data.report_b_total_in_cashier, Decimal) < 0) Then
          ''  .Cell(_idx_row, GRID_COLUMN_VALUE).ForeColor = Color.Crimson
          ''End If
        End If

        If ((chk_company_a.Checked Or chk_company_b.Checked) And chk_promotion.Checked) Then
          .AddRow()
          .AddRow()
        End If

        If (chk_promotion.Checked) Then
          '    - Promoción Fondos No Redimibles - A ->
          .AddRow()
          _idx_row = Me.Grid.NumRows - 1
          .Cell(_idx_row, GRID_COLUMN_CONCEPT).Value = GLB_NLS_GUI_INVOICING.GetString(351)
          ''.Cell(_idx_row, GRID_COLUMN_OPERATION).Value = "B+G+H"
          .Cell(_idx_row, GRID_COLUMN_VALUE).Value = _session_data.report_funds_non_redeemable_promotion.ToString()
        End If

      End With

      Me.Grid.Redraw = True

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "frm_taxes_report", _
                            "GUI_ExecuteQueryCustom", _
                            ex.Message)
      Me.Grid.Redraw = True

    End Try

  End Sub

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(347) & " - " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(347) & " - " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(351), m_nr_promo)
    PrintData.SetFilter(m_company_A_name & ":", m_company_A)

    If Not String.IsNullOrEmpty(m_company_B_name) Then
      PrintData.SetFilter(m_company_B_name & ":", m_company_B)
    Else
      PrintData.SetFilter("", "", True)
    End If

    PrintData.FilterHeaderWidth(1) = 2200
    PrintData.FilterValueWidth(1) = 2200

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_INVOICING.GetString(346)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub ' GUI_ReportParams

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_company_A = ""
    m_company_B = ""

    'Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    If Me.chk_company_a.Checked Then
      m_company_A = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_company_A = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    If (Not Me.chk_company_b Is Nothing) Then
      If Me.chk_company_b.Checked Then
        m_company_B = GLB_NLS_GUI_INVOICING.GetString(479)
      Else
        m_company_B = GLB_NLS_GUI_INVOICING.GetString(480)
      End If
    End If

    If Me.chk_promotion.Checked Then
      m_nr_promo = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_nr_promo = GLB_NLS_GUI_INVOICING.GetString(480)
    End If


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(0).Visible = False
      .PanelRightVisible = False

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Concept
      .Column(GRID_COLUMN_CONCEPT).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(348)
      .Column(GRID_COLUMN_CONCEPT).Width = GRID_WIDTH_CONCEPT
      .Column(GRID_COLUMN_CONCEPT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ''' Operation
      ''.Column(GRID_COLUMN_OPERATION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(350)
      ''.Column(GRID_COLUMN_OPERATION).Width = GRID_WIDTH_OPERATION
      ''.Column(GRID_COLUMN_OPERATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Value
      .Column(GRID_COLUMN_VALUE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(349)
      .Column(GRID_COLUMN_VALUE).Width = GRID_WIDTH_VALUE
      .Column(GRID_COLUMN_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Sortable = False
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub SetDefaultValues()

    Dim from_date As DateTime
    Dim closing_time As Integer

    closing_time = GetDefaultClosingTime()

    from_date = New DateTime(Now.Year, Now.Month, 1)
    Me.uc_dsl.FromDate = from_date.AddMonths(-1)
    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = Me.uc_dsl.FromDate.AddMonths(1)
    Me.uc_dsl.ToDateSelected = True

    Me.uc_dsl.ClosingTime = closing_time

    Me.chk_company_a.Checked = True
    Me.chk_company_b.Checked = m_company_b_cheched
    Me.chk_promotion.Checked = True

    If (Not m_company_b_cheched) Then
      Me.chk_company_b.Enabled = False
    End If

  End Sub ' SetDefaultValues

  ' PURPOSE: Called if Company B are disabled, hide controls related
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub HideCompanyB()
    chk_company_b.Checked = False
    chk_company_b.Visible = False
    chk_promotion.Location = chk_company_b.Location
    gb_show_rows.Height = chk_promotion.Bottom + chk_company_a.Top
    panel_filter.AutoSize = True
  End Sub

#End Region ' Private Functions

End Class
