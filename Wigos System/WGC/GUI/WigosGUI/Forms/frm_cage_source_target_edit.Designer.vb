<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_source_target_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btn_add = New GUI_Controls.uc_button
    Me.btn_del = New GUI_Controls.uc_button
    Me.dg_list = New GUI_Controls.uc_grid
    Me.btn_concepts = New System.Windows.Forms.Button
    Me.btn_relation = New System.Windows.Forms.Button
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.btn_relation)
    Me.panel_data.Controls.Add(Me.btn_concepts)
    Me.panel_data.Controls.Add(Me.dg_list)
    Me.panel_data.Controls.Add(Me.btn_del)
    Me.panel_data.Controls.Add(Me.btn_add)
    Me.panel_data.Location = New System.Drawing.Point(8, 7)
    Me.panel_data.Size = New System.Drawing.Size(465, 398)
    '
    'btn_add
    '
    Me.btn_add.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add.Location = New System.Drawing.Point(14, 368)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(50, 21)
    Me.btn_add.TabIndex = 10
    Me.btn_add.ToolTipped = False
    Me.btn_add.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'btn_del
    '
    Me.btn_del.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_del.Location = New System.Drawing.Point(70, 368)
    Me.btn_del.Name = "btn_del"
    Me.btn_del.Size = New System.Drawing.Size(50, 21)
    Me.btn_del.TabIndex = 11
    Me.btn_del.ToolTipped = False
    Me.btn_del.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    '
    'dg_list
    '
    Me.dg_list.CurrentCol = -1
    Me.dg_list.CurrentRow = -1
    Me.dg_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_list.Location = New System.Drawing.Point(3, 3)
    Me.dg_list.Name = "dg_list"
    Me.dg_list.PanelRightVisible = False
    Me.dg_list.Redraw = True
    Me.dg_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_list.Size = New System.Drawing.Size(459, 357)
    Me.dg_list.Sortable = False
    Me.dg_list.SortAscending = True
    Me.dg_list.SortByCol = 0
    Me.dg_list.TabIndex = 9
    Me.dg_list.ToolTipped = True
    Me.dg_list.TopRow = -2
    '
    'btn_concepts
    '
    Me.btn_concepts.Location = New System.Drawing.Point(202, 368)
    Me.btn_concepts.Name = "btn_concepts"
    Me.btn_concepts.Size = New System.Drawing.Size(75, 21)
    Me.btn_concepts.TabIndex = 12
    Me.btn_concepts.Text = "xConcepts"
    Me.btn_concepts.UseVisualStyleBackColor = True
    '
    'btn_relation
    '
    Me.btn_relation.Location = New System.Drawing.Point(283, 368)
    Me.btn_relation.Name = "btn_relation"
    Me.btn_relation.Size = New System.Drawing.Size(75, 21)
    Me.btn_relation.TabIndex = 13
    Me.btn_relation.Text = "xRelation"
    Me.btn_relation.UseVisualStyleBackColor = True
    '
    'frm_cage_source_target_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(569, 409)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_source_target_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_source_target_edit"
    Me.panel_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btn_add As GUI_Controls.uc_button
  Friend WithEvents btn_del As GUI_Controls.uc_button
  Friend WithEvents dg_list As GUI_Controls.uc_grid
  Friend WithEvents btn_concepts As System.Windows.Forms.Button
  Friend WithEvents btn_relation As System.Windows.Forms.Button
End Class
