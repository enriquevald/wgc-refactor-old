<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cage_sessions_report
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_header = New System.Windows.Forms.GroupBox
    Me.tf_cg_status = New GUI_Controls.uc_text_field
    Me.lbl_cg_name = New GUI_Controls.uc_text_field
    Me.web_browser = New System.Windows.Forms.WebBrowser
    Me.panel_data.SuspendLayout()
    Me.gb_header.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.web_browser)
    Me.panel_data.Controls.Add(Me.gb_header)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(1240, 765)
    '
    'gb_header
    '
    Me.gb_header.Controls.Add(Me.tf_cg_status)
    Me.gb_header.Controls.Add(Me.lbl_cg_name)
    Me.gb_header.Location = New System.Drawing.Point(5, 3)
    Me.gb_header.Name = "gb_header"
    Me.gb_header.Size = New System.Drawing.Size(1218, 52)
    Me.gb_header.TabIndex = 10
    Me.gb_header.TabStop = False
    '
    'tf_cg_status
    '
    Me.tf_cg_status.IsReadOnly = True
    Me.tf_cg_status.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cg_status.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cg_status.Location = New System.Drawing.Point(956, 14)
    Me.tf_cg_status.Name = "tf_cg_status"
    Me.tf_cg_status.Size = New System.Drawing.Size(250, 24)
    Me.tf_cg_status.SufixText = "Sufix Text"
    Me.tf_cg_status.SufixTextVisible = True
    Me.tf_cg_status.TabIndex = 122
    Me.tf_cg_status.TabStop = False
    Me.tf_cg_status.TextWidth = 100
    Me.tf_cg_status.Value = "xStatus"
    '
    'lbl_cg_name
    '
    Me.lbl_cg_name.IsReadOnly = True
    Me.lbl_cg_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cg_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_cg_name.Location = New System.Drawing.Point(6, 14)
    Me.lbl_cg_name.Name = "lbl_cg_name"
    Me.lbl_cg_name.Size = New System.Drawing.Size(810, 24)
    Me.lbl_cg_name.SufixText = "Sufix Text"
    Me.lbl_cg_name.SufixTextVisible = True
    Me.lbl_cg_name.TabIndex = 10
    Me.lbl_cg_name.TextWidth = 0
    Me.lbl_cg_name.Value = "xCage Session Name"
    '
    'web_browser
    '
    Me.web_browser.Location = New System.Drawing.Point(5, 61)
    Me.web_browser.MinimumSize = New System.Drawing.Size(20, 20)
    Me.web_browser.Name = "web_browser"
    Me.web_browser.ScrollBarsEnabled = False
    Me.web_browser.Size = New System.Drawing.Size(1227, 676)
    Me.web_browser.TabIndex = 206
    '
    'frm_cage_sessions_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1344, 763)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cage_sessions_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cage_sessions_report"
    Me.panel_data.ResumeLayout(False)
    Me.gb_header.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_header As System.Windows.Forms.GroupBox
  Friend WithEvents tf_cg_status As GUI_Controls.uc_text_field
  Friend WithEvents lbl_cg_name As GUI_Controls.uc_text_field
  Friend WithEvents web_browser As System.Windows.Forms.WebBrowser
End Class
