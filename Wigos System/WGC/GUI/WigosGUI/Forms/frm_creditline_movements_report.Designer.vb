﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_creditline_movements_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_account_sel1 = New GUI_Controls.uc_account_sel()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_sites_sel = New GUI_Controls.uc_sites_sel()
    Me.gb_type = New System.Windows.Forms.GroupBox()
    Me.opt_all_types = New System.Windows.Forms.RadioButton()
    Me.opt_several_types = New System.Windows.Forms.RadioButton()
    Me.dg_filter = New GUI_Controls.uc_grid()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.gb_type)
    Me.panel_filter.Controls.Add(Me.uc_sites_sel)
    Me.panel_filter.Controls.Add(Me.uc_account_sel1)
    Me.panel_filter.Size = New System.Drawing.Size(1252, 195)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_sites_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 199)
    Me.panel_data.Size = New System.Drawing.Size(1252, 495)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1246, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1246, 4)
    '
    'uc_account_sel1
    '
    Me.uc_account_sel1.Account = ""
    Me.uc_account_sel1.AccountText = ""
    Me.uc_account_sel1.Anon = False
    Me.uc_account_sel1.AutoSize = True
    Me.uc_account_sel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel1.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel1.DisabledHolder = False
    Me.uc_account_sel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel1.Holder = ""
    Me.uc_account_sel1.Location = New System.Drawing.Point(304, 12)
    Me.uc_account_sel1.MassiveSearchNumbers = ""
    Me.uc_account_sel1.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel1.MassiveSearchType = 0
    Me.uc_account_sel1.Name = "uc_account_sel1"
    Me.uc_account_sel1.SearchTrackDataAsInternal = True
    Me.uc_account_sel1.ShowMassiveSearch = False
    Me.uc_account_sel1.ShowVipClients = True
    Me.uc_account_sel1.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel1.TabIndex = 2
    Me.uc_account_sel1.Telephone = ""
    Me.uc_account_sel1.TrackData = ""
    Me.uc_account_sel1.Vip = False
    Me.uc_account_sel1.WeddingDate = New Date(CType(0, Long))
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 15)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(284, 85)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 49)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(254, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 21)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(254, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_sites_sel
    '
    Me.uc_sites_sel.Location = New System.Drawing.Point(991, 12)
    Me.uc_sites_sel.MultiSelect = True
    Me.uc_sites_sel.Name = "uc_sites_sel"
    Me.uc_sites_sel.ShowIsoCode = False
    Me.uc_sites_sel.ShowMultisiteRow = True
    Me.uc_sites_sel.Size = New System.Drawing.Size(293, 166)
    Me.uc_sites_sel.TabIndex = 4
    '
    'gb_type
    '
    Me.gb_type.Controls.Add(Me.opt_all_types)
    Me.gb_type.Controls.Add(Me.opt_several_types)
    Me.gb_type.Controls.Add(Me.dg_filter)
    Me.gb_type.Location = New System.Drawing.Point(625, 15)
    Me.gb_type.Name = "gb_type"
    Me.gb_type.Size = New System.Drawing.Size(341, 170)
    Me.gb_type.TabIndex = 5
    Me.gb_type.TabStop = False
    Me.gb_type.Text = "xMov. Type"
    '
    'opt_all_types
    '
    Me.opt_all_types.Location = New System.Drawing.Point(8, 45)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_types.TabIndex = 1
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.Location = New System.Drawing.Point(8, 17)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(72, 24)
    Me.opt_several_types.TabIndex = 0
    Me.opt_several_types.Text = "xSeveral"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter.Location = New System.Drawing.Point(86, 16)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(249, 148)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 2
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    Me.dg_filter.WordWrap = False
    '
    'frm_creditline_movements_report
    '
    Me.ClientSize = New System.Drawing.Size(1260, 698)
    Me.Name = "frm_creditline_movements_report"
    Me.Text = "frm_creditline_movements_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_type.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_sel1 As GUI_Controls.uc_account_sel
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_sites_sel As GUI_Controls.uc_sites_sel
  Friend WithEvents gb_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents dg_filter As GUI_Controls.uc_grid

End Class
