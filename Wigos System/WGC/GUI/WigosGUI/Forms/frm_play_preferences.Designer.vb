<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_play_preferences
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_play_preferences))
    Me.uc_account_filter = New GUI_Controls.uc_account_sel()
    Me.tab_preferences = New System.Windows.Forms.TabControl()
    Me.gb_account_data = New System.Windows.Forms.GroupBox()
    Me.ef_holder_level_enter = New GUI_Controls.uc_entry_field()
    Me.ef_account_level = New GUI_Controls.uc_entry_field()
    Me.ef_account_gender = New GUI_Controls.uc_entry_field()
    Me.ef_account_activity = New GUI_Controls.uc_entry_field()
    Me.ef_account_age = New GUI_Controls.uc_entry_field()
    Me.ef_account_points = New GUI_Controls.uc_entry_field()
    Me.ef_account_name = New GUI_Controls.uc_entry_field()
    Me.ef_account_card = New GUI_Controls.uc_entry_field()
    Me.ef_account_id = New GUI_Controls.uc_entry_field()
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_account_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ds_from_to)
    Me.panel_filter.Controls.Add(Me.uc_account_filter)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 137)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_filter, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ds_from_to, 0)
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_account_data)
    Me.panel_data.Controls.Add(Me.tab_preferences)
    Me.panel_data.Location = New System.Drawing.Point(4, 141)
    Me.panel_data.Size = New System.Drawing.Size(1226, 507)
    Me.panel_data.TabIndex = 0
    Me.panel_data.Controls.SetChildIndex(Me.tab_preferences, 0)
    Me.panel_data.Controls.SetChildIndex(Me.gb_account_data, 0)
    Me.panel_data.Controls.SetChildIndex(Me.pn_separator_line, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(3, 123)
    Me.pn_separator_line.Size = New System.Drawing.Size(1132, 13)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1132, 4)
    '
    'uc_account_filter
    '
    Me.uc_account_filter.Account = ""
    Me.uc_account_filter.AccountText = ""
    Me.uc_account_filter.Anon = False
    Me.uc_account_filter.AutoSize = True
    Me.uc_account_filter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_filter.BirthDate = New Date(CType(0, Long))
    Me.uc_account_filter.DisabledHolder = True
    Me.uc_account_filter.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_filter.Holder = ""
    Me.uc_account_filter.Location = New System.Drawing.Point(274, 0)
    Me.uc_account_filter.MassiveSearchNumbers = ""
    Me.uc_account_filter.MassiveSearchNumbersToEdit = ""
    Me.uc_account_filter.MassiveSearchType = 0
    Me.uc_account_filter.Name = "uc_account_filter"
    Me.uc_account_filter.SearchTrackDataAsInternal = True
    Me.uc_account_filter.ShowMassiveSearch = False
    Me.uc_account_filter.ShowVipClients = True
    Me.uc_account_filter.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_filter.TabIndex = 1
    Me.uc_account_filter.Telephone = ""
    Me.uc_account_filter.TrackData = ""
    Me.uc_account_filter.Vip = False
    Me.uc_account_filter.WeddingDate = New Date(CType(0, Long))
    '
    'tab_preferences
    '
    Me.tab_preferences.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tab_preferences.Location = New System.Drawing.Point(3, 136)
    Me.tab_preferences.Name = "tab_preferences"
    Me.tab_preferences.SelectedIndex = 0
    Me.tab_preferences.Size = New System.Drawing.Size(1132, 368)
    Me.tab_preferences.TabIndex = 10
    '
    'gb_account_data
    '
    Me.gb_account_data.Controls.Add(Me.ef_holder_level_enter)
    Me.gb_account_data.Controls.Add(Me.ef_account_level)
    Me.gb_account_data.Controls.Add(Me.ef_account_gender)
    Me.gb_account_data.Controls.Add(Me.ef_account_activity)
    Me.gb_account_data.Controls.Add(Me.ef_account_age)
    Me.gb_account_data.Controls.Add(Me.ef_account_points)
    Me.gb_account_data.Controls.Add(Me.ef_account_name)
    Me.gb_account_data.Controls.Add(Me.ef_account_card)
    Me.gb_account_data.Controls.Add(Me.ef_account_id)
    Me.gb_account_data.Dock = System.Windows.Forms.DockStyle.Top
    Me.gb_account_data.Location = New System.Drawing.Point(3, 3)
    Me.gb_account_data.Name = "gb_account_data"
    Me.gb_account_data.Size = New System.Drawing.Size(1132, 120)
    Me.gb_account_data.TabIndex = 0
    Me.gb_account_data.TabStop = False
    Me.gb_account_data.Text = "xAccountData"
    '
    'ef_holder_level_enter
    '
    Me.ef_holder_level_enter.DoubleValue = 0.0R
    Me.ef_holder_level_enter.IntegerValue = 0
    Me.ef_holder_level_enter.IsReadOnly = True
    Me.ef_holder_level_enter.Location = New System.Drawing.Point(657, 80)
    Me.ef_holder_level_enter.Name = "ef_holder_level_enter"
    Me.ef_holder_level_enter.PlaceHolder = Nothing
    Me.ef_holder_level_enter.ShortcutsEnabled = True
    Me.ef_holder_level_enter.Size = New System.Drawing.Size(254, 26)
    Me.ef_holder_level_enter.SufixText = "Sufix Text"
    Me.ef_holder_level_enter.SufixTextVisible = True
    Me.ef_holder_level_enter.TabIndex = 7
    Me.ef_holder_level_enter.TabStop = False
    Me.ef_holder_level_enter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_holder_level_enter.TextValue = ""
    Me.ef_holder_level_enter.TextWidth = 125
    Me.ef_holder_level_enter.Value = ""
    Me.ef_holder_level_enter.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_level
    '
    Me.ef_account_level.DoubleValue = 0.0R
    Me.ef_account_level.IntegerValue = 0
    Me.ef_account_level.IsReadOnly = True
    Me.ef_account_level.Location = New System.Drawing.Point(6, 80)
    Me.ef_account_level.Name = "ef_account_level"
    Me.ef_account_level.PlaceHolder = Nothing
    Me.ef_account_level.ShortcutsEnabled = True
    Me.ef_account_level.Size = New System.Drawing.Size(217, 26)
    Me.ef_account_level.SufixText = "Sufix Text"
    Me.ef_account_level.SufixTextVisible = True
    Me.ef_account_level.TabIndex = 1
    Me.ef_account_level.TabStop = False
    Me.ef_account_level.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_level.TextValue = ""
    Me.ef_account_level.Value = ""
    Me.ef_account_level.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_gender
    '
    Me.ef_account_gender.DoubleValue = 0.0R
    Me.ef_account_gender.IntegerValue = 0
    Me.ef_account_gender.IsReadOnly = True
    Me.ef_account_gender.Location = New System.Drawing.Point(205, 50)
    Me.ef_account_gender.Name = "ef_account_gender"
    Me.ef_account_gender.PlaceHolder = Nothing
    Me.ef_account_gender.ShortcutsEnabled = True
    Me.ef_account_gender.Size = New System.Drawing.Size(141, 26)
    Me.ef_account_gender.SufixText = "Sufix Text"
    Me.ef_account_gender.SufixTextVisible = True
    Me.ef_account_gender.TabIndex = 8
    Me.ef_account_gender.TabStop = False
    Me.ef_account_gender.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_gender.TextValue = ""
    Me.ef_account_gender.Value = ""
    Me.ef_account_gender.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_activity
    '
    Me.ef_account_activity.DoubleValue = 0.0R
    Me.ef_account_activity.IntegerValue = 0
    Me.ef_account_activity.IsReadOnly = True
    Me.ef_account_activity.Location = New System.Drawing.Point(657, 50)
    Me.ef_account_activity.Name = "ef_account_activity"
    Me.ef_account_activity.PlaceHolder = Nothing
    Me.ef_account_activity.ShortcutsEnabled = True
    Me.ef_account_activity.Size = New System.Drawing.Size(254, 26)
    Me.ef_account_activity.SufixText = "Sufix Text"
    Me.ef_account_activity.SufixTextVisible = True
    Me.ef_account_activity.TabIndex = 5
    Me.ef_account_activity.TabStop = False
    Me.ef_account_activity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_activity.TextValue = ""
    Me.ef_account_activity.TextWidth = 125
    Me.ef_account_activity.Value = ""
    Me.ef_account_activity.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_age
    '
    Me.ef_account_age.DoubleValue = 0.0R
    Me.ef_account_age.IntegerValue = 0
    Me.ef_account_age.IsReadOnly = True
    Me.ef_account_age.Location = New System.Drawing.Point(17, 50)
    Me.ef_account_age.Name = "ef_account_age"
    Me.ef_account_age.PlaceHolder = Nothing
    Me.ef_account_age.ShortcutsEnabled = True
    Me.ef_account_age.Size = New System.Drawing.Size(116, 26)
    Me.ef_account_age.SufixText = "Sufix Text"
    Me.ef_account_age.SufixTextVisible = True
    Me.ef_account_age.TabIndex = 6
    Me.ef_account_age.TabStop = False
    Me.ef_account_age.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_age.TextValue = ""
    Me.ef_account_age.TextWidth = 70
    Me.ef_account_age.Value = ""
    Me.ef_account_age.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_points
    '
    Me.ef_account_points.DoubleValue = 0.0R
    Me.ef_account_points.IntegerValue = 0
    Me.ef_account_points.IsReadOnly = True
    Me.ef_account_points.Location = New System.Drawing.Point(215, 80)
    Me.ef_account_points.Name = "ef_account_points"
    Me.ef_account_points.PlaceHolder = Nothing
    Me.ef_account_points.ShortcutsEnabled = True
    Me.ef_account_points.Size = New System.Drawing.Size(160, 26)
    Me.ef_account_points.SufixText = "Sufix Text"
    Me.ef_account_points.SufixTextVisible = True
    Me.ef_account_points.TabIndex = 3
    Me.ef_account_points.TabStop = False
    Me.ef_account_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_points.TextValue = ""
    Me.ef_account_points.TextWidth = 70
    Me.ef_account_points.Value = ""
    Me.ef_account_points.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_name
    '
    Me.ef_account_name.DoubleValue = 0.0R
    Me.ef_account_name.IntegerValue = 0
    Me.ef_account_name.IsReadOnly = True
    Me.ef_account_name.Location = New System.Drawing.Point(722, 20)
    Me.ef_account_name.Name = "ef_account_name"
    Me.ef_account_name.PlaceHolder = Nothing
    Me.ef_account_name.ShortcutsEnabled = True
    Me.ef_account_name.Size = New System.Drawing.Size(390, 26)
    Me.ef_account_name.SufixText = "Sufix Text"
    Me.ef_account_name.SufixTextVisible = True
    Me.ef_account_name.TabIndex = 4
    Me.ef_account_name.TabStop = False
    Me.ef_account_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_name.TextValue = ""
    Me.ef_account_name.TextWidth = 60
    Me.ef_account_name.Value = ""
    Me.ef_account_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_card
    '
    Me.ef_account_card.DoubleValue = 0.0R
    Me.ef_account_card.IntegerValue = 0
    Me.ef_account_card.IsReadOnly = True
    Me.ef_account_card.Location = New System.Drawing.Point(205, 20)
    Me.ef_account_card.Name = "ef_account_card"
    Me.ef_account_card.PlaceHolder = Nothing
    Me.ef_account_card.ShortcutsEnabled = True
    Me.ef_account_card.Size = New System.Drawing.Size(441, 28)
    Me.ef_account_card.SufixText = "Sufix Text"
    Me.ef_account_card.SufixTextVisible = True
    Me.ef_account_card.TabIndex = 2
    Me.ef_account_card.TabStop = False
    Me.ef_account_card.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_card.TextValue = ""
    Me.ef_account_card.Value = ""
    Me.ef_account_card.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_account_id
    '
    Me.ef_account_id.DoubleValue = 0.0R
    Me.ef_account_id.IntegerValue = 0
    Me.ef_account_id.IsReadOnly = True
    Me.ef_account_id.Location = New System.Drawing.Point(17, 20)
    Me.ef_account_id.Name = "ef_account_id"
    Me.ef_account_id.PlaceHolder = Nothing
    Me.ef_account_id.ShortcutsEnabled = True
    Me.ef_account_id.Size = New System.Drawing.Size(165, 26)
    Me.ef_account_id.SufixText = "Sufix Text"
    Me.ef_account_id.SufixTextVisible = True
    Me.ef_account_id.TabIndex = 0
    Me.ef_account_id.TabStop = False
    Me.ef_account_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_id.TextValue = ""
    Me.ef_account_id.TextWidth = 70
    Me.ef_account_id.Value = ""
    Me.ef_account_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = False
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.FromDateText = "#5339"
    Me.ds_from_to.Location = New System.Drawing.Point(6, 3)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.ShowBorder = True
    Me.ds_from_to.Size = New System.Drawing.Size(260, 84)
    Me.ds_from_to.TabIndex = 0
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    Me.ds_from_to.ToDateText = "#5340"
    '
    'frm_play_preferences
    '
    Me.ClientSize = New System.Drawing.Size(1234, 652)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_play_preferences"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_account_data.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_account_filter As GUI_Controls.uc_account_sel
  Friend WithEvents tab_preferences As System.Windows.Forms.TabControl
  Friend WithEvents gb_account_data As System.Windows.Forms.GroupBox
  Friend WithEvents ef_account_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_card As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_age As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_points As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_activity As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_gender As GUI_Controls.uc_entry_field
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
  Friend WithEvents ef_account_level As GUI_Controls.uc_entry_field
  Friend WithEvents ef_holder_level_enter As GUI_Controls.uc_entry_field

End Class
