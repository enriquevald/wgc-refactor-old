<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_player_edit_config
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.chk_ask_before_print = New System.Windows.Forms.CheckBox()
    Me.chk_acount_customize_enabled = New System.Windows.Forms.CheckBox()
    Me.tab_config = New System.Windows.Forms.TabControl()
    Me.documents = New System.Windows.Forms.TabPage()
    Me.gb_default_values = New System.Windows.Forms.GroupBox()
    Me.cb_default_country = New GUI_Controls.uc_combo()
    Me.cb_default_document = New GUI_Controls.uc_combo()
    Me.dg_principal_documents_config = New GUI_Controls.uc_grid()
    Me.xPrincipal = New System.Windows.Forms.TabPage()
    Me.dg_principal_config = New GUI_Controls.uc_grid()
    Me.xContacto = New System.Windows.Forms.TabPage()
    Me.dg_contact_config = New GUI_Controls.uc_grid()
    Me.xDireccion = New System.Windows.Forms.TabPage()
    Me.dg_address_config = New GUI_Controls.uc_grid()
    Me.xBeneficiary = New System.Windows.Forms.TabPage()
    Me.dg_beneficiary_config = New GUI_Controls.uc_grid()
    Me.lbl_edition = New System.Windows.Forms.Label()
    Me.panel_data.SuspendLayout()
    Me.tab_config.SuspendLayout()
    Me.documents.SuspendLayout()
    Me.gb_default_values.SuspendLayout()
    Me.xPrincipal.SuspendLayout()
    Me.xContacto.SuspendLayout()
    Me.xDireccion.SuspendLayout()
    Me.xBeneficiary.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tab_config)
    Me.panel_data.Controls.Add(Me.chk_ask_before_print)
    Me.panel_data.Controls.Add(Me.chk_acount_customize_enabled)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(829, 525)
    '
    'chk_ask_before_print
    '
    Me.chk_ask_before_print.AutoSize = True
    Me.chk_ask_before_print.Location = New System.Drawing.Point(24, 495)
    Me.chk_ask_before_print.Name = "chk_ask_before_print"
    Me.chk_ask_before_print.Size = New System.Drawing.Size(118, 17)
    Me.chk_ask_before_print.TabIndex = 1
    Me.chk_ask_before_print.Text = "xAskBeforePrint"
    Me.chk_ask_before_print.UseVisualStyleBackColor = True
    '
    'chk_acount_customize_enabled
    '
    Me.chk_acount_customize_enabled.AutoSize = True
    Me.chk_acount_customize_enabled.Location = New System.Drawing.Point(14, 472)
    Me.chk_acount_customize_enabled.Name = "chk_acount_customize_enabled"
    Me.chk_acount_customize_enabled.Size = New System.Drawing.Size(183, 17)
    Me.chk_acount_customize_enabled.TabIndex = 0
    Me.chk_acount_customize_enabled.Text = "xAccountCustomizeEnabled"
    Me.chk_acount_customize_enabled.UseVisualStyleBackColor = True
    '
    'tab_config
    '
    Me.tab_config.Controls.Add(Me.documents)
    Me.tab_config.Controls.Add(Me.xPrincipal)
    Me.tab_config.Controls.Add(Me.xContacto)
    Me.tab_config.Controls.Add(Me.xDireccion)
    Me.tab_config.Controls.Add(Me.xBeneficiary)
    Me.tab_config.Location = New System.Drawing.Point(3, 3)
    Me.tab_config.Name = "tab_config"
    Me.tab_config.SelectedIndex = 0
    Me.tab_config.Size = New System.Drawing.Size(823, 462)
    Me.tab_config.TabIndex = 0
    '
    'documents
    '
    Me.documents.Controls.Add(Me.lbl_edition)
    Me.documents.Controls.Add(Me.gb_default_values)
    Me.documents.Controls.Add(Me.dg_principal_documents_config)
    Me.documents.Location = New System.Drawing.Point(4, 22)
    Me.documents.Name = "documents"
    Me.documents.Padding = New System.Windows.Forms.Padding(3)
    Me.documents.Size = New System.Drawing.Size(815, 436)
    Me.documents.TabIndex = 4
    Me.documents.Text = "xDocuments"
    Me.documents.UseVisualStyleBackColor = True
    '
    'gb_default_values
    '
    Me.gb_default_values.Controls.Add(Me.cb_default_country)
    Me.gb_default_values.Controls.Add(Me.cb_default_document)
    Me.gb_default_values.Location = New System.Drawing.Point(7, 10)
    Me.gb_default_values.Name = "gb_default_values"
    Me.gb_default_values.Size = New System.Drawing.Size(285, 106)
    Me.gb_default_values.TabIndex = 0
    Me.gb_default_values.TabStop = False
    Me.gb_default_values.Text = "xDefaultValues"
    '
    'cb_default_country
    '
    Me.cb_default_country.AllowUnlistedValues = False
    Me.cb_default_country.AutoCompleteMode = True
    Me.cb_default_country.IsReadOnly = False
    Me.cb_default_country.Location = New System.Drawing.Point(6, 60)
    Me.cb_default_country.Name = "cb_default_country"
    Me.cb_default_country.SelectedIndex = -1
    Me.cb_default_country.Size = New System.Drawing.Size(265, 24)
    Me.cb_default_country.SufixText = "Sufix Text"
    Me.cb_default_country.SufixTextVisible = True
    Me.cb_default_country.TabIndex = 2
    Me.cb_default_country.TextCombo = Nothing
    '
    'cb_default_document
    '
    Me.cb_default_document.AllowUnlistedValues = False
    Me.cb_default_document.AutoCompleteMode = False
    Me.cb_default_document.IsReadOnly = False
    Me.cb_default_document.Location = New System.Drawing.Point(6, 30)
    Me.cb_default_document.Name = "cb_default_document"
    Me.cb_default_document.SelectedIndex = -1
    Me.cb_default_document.Size = New System.Drawing.Size(265, 24)
    Me.cb_default_document.SufixText = "Sufix Text"
    Me.cb_default_document.SufixTextVisible = True
    Me.cb_default_document.TabIndex = 1
    Me.cb_default_document.TextCombo = Nothing
    '
    'dg_principal_documents_config
    '
    Me.dg_principal_documents_config.CurrentCol = -1
    Me.dg_principal_documents_config.CurrentRow = -1
    Me.dg_principal_documents_config.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_principal_documents_config.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_principal_documents_config.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_principal_documents_config.Location = New System.Drawing.Point(3, 122)
    Me.dg_principal_documents_config.Name = "dg_principal_documents_config"
    Me.dg_principal_documents_config.PanelRightVisible = False
    Me.dg_principal_documents_config.Redraw = True
    Me.dg_principal_documents_config.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_principal_documents_config.Size = New System.Drawing.Size(806, 308)
    Me.dg_principal_documents_config.Sortable = False
    Me.dg_principal_documents_config.SortAscending = True
    Me.dg_principal_documents_config.SortByCol = 0
    Me.dg_principal_documents_config.TabIndex = 3
    Me.dg_principal_documents_config.ToolTipped = True
    Me.dg_principal_documents_config.TopRow = -2
    '
    'xPrincipal
    '
    Me.xPrincipal.Controls.Add(Me.dg_principal_config)
    Me.xPrincipal.Location = New System.Drawing.Point(4, 22)
    Me.xPrincipal.Name = "xPrincipal"
    Me.xPrincipal.Padding = New System.Windows.Forms.Padding(3)
    Me.xPrincipal.Size = New System.Drawing.Size(815, 436)
    Me.xPrincipal.TabIndex = 0
    Me.xPrincipal.Text = "xPrincipal"
    Me.xPrincipal.UseVisualStyleBackColor = True
    '
    'dg_principal_config
    '
    Me.dg_principal_config.CurrentCol = -1
    Me.dg_principal_config.CurrentRow = -1
    Me.dg_principal_config.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_principal_config.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_principal_config.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_principal_config.Location = New System.Drawing.Point(3, 7)
    Me.dg_principal_config.Name = "dg_principal_config"
    Me.dg_principal_config.PanelRightVisible = False
    Me.dg_principal_config.Redraw = True
    Me.dg_principal_config.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_principal_config.Size = New System.Drawing.Size(806, 423)
    Me.dg_principal_config.Sortable = False
    Me.dg_principal_config.SortAscending = True
    Me.dg_principal_config.SortByCol = 0
    Me.dg_principal_config.TabIndex = 4
    Me.dg_principal_config.ToolTipped = True
    Me.dg_principal_config.TopRow = -2
    '
    'xContacto
    '
    Me.xContacto.Controls.Add(Me.dg_contact_config)
    Me.xContacto.Location = New System.Drawing.Point(4, 22)
    Me.xContacto.Name = "xContacto"
    Me.xContacto.Padding = New System.Windows.Forms.Padding(3)
    Me.xContacto.Size = New System.Drawing.Size(815, 436)
    Me.xContacto.TabIndex = 1
    Me.xContacto.Text = "xContacto"
    Me.xContacto.UseVisualStyleBackColor = True
    '
    'dg_contact_config
    '
    Me.dg_contact_config.CurrentCol = -1
    Me.dg_contact_config.CurrentRow = -1
    Me.dg_contact_config.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_contact_config.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_contact_config.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_contact_config.Location = New System.Drawing.Point(3, 7)
    Me.dg_contact_config.Name = "dg_contact_config"
    Me.dg_contact_config.PanelRightVisible = False
    Me.dg_contact_config.Redraw = True
    Me.dg_contact_config.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_contact_config.Size = New System.Drawing.Size(806, 423)
    Me.dg_contact_config.Sortable = False
    Me.dg_contact_config.SortAscending = True
    Me.dg_contact_config.SortByCol = 0
    Me.dg_contact_config.TabIndex = 5
    Me.dg_contact_config.ToolTipped = True
    Me.dg_contact_config.TopRow = -2
    '
    'xDireccion
    '
    Me.xDireccion.Controls.Add(Me.dg_address_config)
    Me.xDireccion.Location = New System.Drawing.Point(4, 22)
    Me.xDireccion.Name = "xDireccion"
    Me.xDireccion.Size = New System.Drawing.Size(815, 436)
    Me.xDireccion.TabIndex = 2
    Me.xDireccion.Text = "xDireccion"
    Me.xDireccion.UseVisualStyleBackColor = True
    '
    'dg_address_config
    '
    Me.dg_address_config.CurrentCol = -1
    Me.dg_address_config.CurrentRow = -1
    Me.dg_address_config.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_address_config.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_address_config.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_address_config.Location = New System.Drawing.Point(3, 7)
    Me.dg_address_config.Name = "dg_address_config"
    Me.dg_address_config.PanelRightVisible = False
    Me.dg_address_config.Redraw = True
    Me.dg_address_config.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_address_config.Size = New System.Drawing.Size(806, 423)
    Me.dg_address_config.Sortable = False
    Me.dg_address_config.SortAscending = True
    Me.dg_address_config.SortByCol = 0
    Me.dg_address_config.TabIndex = 6
    Me.dg_address_config.ToolTipped = True
    Me.dg_address_config.TopRow = -2
    '
    'xBeneficiary
    '
    Me.xBeneficiary.Controls.Add(Me.dg_beneficiary_config)
    Me.xBeneficiary.Location = New System.Drawing.Point(4, 22)
    Me.xBeneficiary.Name = "xBeneficiary"
    Me.xBeneficiary.Size = New System.Drawing.Size(815, 436)
    Me.xBeneficiary.TabIndex = 3
    Me.xBeneficiary.Text = "xBeneficiario"
    Me.xBeneficiary.UseVisualStyleBackColor = True
    '
    'dg_beneficiary_config
    '
    Me.dg_beneficiary_config.CurrentCol = -1
    Me.dg_beneficiary_config.CurrentRow = -1
    Me.dg_beneficiary_config.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_beneficiary_config.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_beneficiary_config.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_beneficiary_config.Location = New System.Drawing.Point(3, 7)
    Me.dg_beneficiary_config.Name = "dg_beneficiary_config"
    Me.dg_beneficiary_config.PanelRightVisible = False
    Me.dg_beneficiary_config.Redraw = True
    Me.dg_beneficiary_config.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_beneficiary_config.Size = New System.Drawing.Size(806, 423)
    Me.dg_beneficiary_config.Sortable = False
    Me.dg_beneficiary_config.SortAscending = True
    Me.dg_beneficiary_config.SortByCol = 0
    Me.dg_beneficiary_config.TabIndex = 7
    Me.dg_beneficiary_config.ToolTipped = True
    Me.dg_beneficiary_config.TopRow = -2
    '
    'lbl_edition
    '
    Me.lbl_edition.AutoSize = True
    Me.lbl_edition.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_edition.Location = New System.Drawing.Point(312, 19)
    Me.lbl_edition.Name = "lbl_edition"
    Me.lbl_edition.Size = New System.Drawing.Size(146, 13)
    Me.lbl_edition.TabIndex = 126
    Me.lbl_edition.Text = "Edici�n s�lo en el centro"
    Me.lbl_edition.Visible = False
    '
    'frm_player_edit_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(933, 533)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_player_edit_config"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_required_field"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.tab_config.ResumeLayout(False)
    Me.documents.ResumeLayout(False)
    Me.documents.PerformLayout()
    Me.gb_default_values.ResumeLayout(False)
    Me.xPrincipal.ResumeLayout(False)
    Me.xContacto.ResumeLayout(False)
    Me.xDireccion.ResumeLayout(False)
    Me.xBeneficiary.ResumeLayout(False)
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents chk_ask_before_print As System.Windows.Forms.CheckBox
  Friend WithEvents chk_acount_customize_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents tab_config As System.Windows.Forms.TabControl
  Friend WithEvents xPrincipal As System.Windows.Forms.TabPage
  Friend WithEvents xContacto As System.Windows.Forms.TabPage
  Friend WithEvents xDireccion As System.Windows.Forms.TabPage
  Friend WithEvents xBeneficiary As System.Windows.Forms.TabPage
  Friend WithEvents dg_principal_documents_config As GUI_Controls.uc_grid
  Friend WithEvents dg_principal_config As GUI_Controls.uc_grid
  Friend WithEvents dg_contact_config As GUI_Controls.uc_grid
  Friend WithEvents dg_address_config As GUI_Controls.uc_grid
  Friend WithEvents dg_beneficiary_config As GUI_Controls.uc_grid
  Friend WithEvents documents As System.Windows.Forms.TabPage
  Friend WithEvents gb_default_values As System.Windows.Forms.GroupBox
  Friend WithEvents cb_default_country As GUI_Controls.uc_combo
  Friend WithEvents cb_default_document As GUI_Controls.uc_combo
  Friend WithEvents lbl_edition As System.Windows.Forms.Label
End Class
