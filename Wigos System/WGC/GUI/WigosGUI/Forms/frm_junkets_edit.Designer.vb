﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_junkets_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_code = New GUI_Controls.uc_entry_field()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.ef_creation = New GUI_Controls.uc_entry_field()
    Me.ef_code_rep = New GUI_Controls.uc_entry_field()
    Me.ef_name_rep = New GUI_Controls.uc_entry_field()
    Me.ef_com_points = New GUI_Controls.uc_entry_field()
    Me.ef_com_buyin = New GUI_Controls.uc_entry_field()
    Me.ef_com_netwin = New GUI_Controls.uc_entry_field()
    Me.ef_com_coinin = New GUI_Controls.uc_entry_field()
    Me.ef_com_enrolled = New GUI_Controls.uc_entry_field()
    Me.ef_com_visits = New GUI_Controls.uc_entry_field()
    Me.ef_com_cutomers = New GUI_Controls.uc_entry_field()
    Me.gb_comissions = New System.Windows.Forms.GroupBox()
    Me.gb_representative_junket = New System.Windows.Forms.GroupBox()
    Me.bt_search_rep = New System.Windows.Forms.Button()
    Me.gb_comments = New System.Windows.Forms.GroupBox()
    Me.txt_comments = New System.Windows.Forms.TextBox()
    Me.gb_details = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_data_range = New System.Windows.Forms.GroupBox()
    Me.gb_flyers = New System.Windows.Forms.GroupBox()
    Me.dgv_flyers = New System.Windows.Forms.DataGridView()
    Me.bt_new = New System.Windows.Forms.Button()
    Me.bt_select = New System.Windows.Forms.Button()
    Me.panel_data.SuspendLayout()
    Me.gb_comissions.SuspendLayout()
    Me.gb_representative_junket.SuspendLayout()
    Me.gb_comments.SuspendLayout()
    Me.gb_details.SuspendLayout()
    Me.gb_data_range.SuspendLayout()
    Me.gb_flyers.SuspendLayout()
    CType(Me.dgv_flyers, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_flyers)
    Me.panel_data.Controls.Add(Me.gb_data_range)
    Me.panel_data.Controls.Add(Me.gb_details)
    Me.panel_data.Controls.Add(Me.gb_comments)
    Me.panel_data.Controls.Add(Me.gb_representative_junket)
    Me.panel_data.Controls.Add(Me.gb_comissions)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(659, 724)
    '
    'ef_code
    '
    Me.ef_code.DoubleValue = 0.0R
    Me.ef_code.IntegerValue = 0
    Me.ef_code.IsReadOnly = False
    Me.ef_code.Location = New System.Drawing.Point(5, 20)
    Me.ef_code.Name = "ef_code"
    Me.ef_code.PlaceHolder = Nothing
    Me.ef_code.Size = New System.Drawing.Size(168, 24)
    Me.ef_code.SufixText = "Sufix Text"
    Me.ef_code.SufixTextVisible = True
    Me.ef_code.TabIndex = 0
    Me.ef_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_code.TextValue = ""
    Me.ef_code.TextWidth = 50
    Me.ef_code.Value = ""
    Me.ef_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(196, 20)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(245, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 55
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_creation
    '
    Me.ef_creation.DoubleValue = 0.0R
    Me.ef_creation.IntegerValue = 0
    Me.ef_creation.IsReadOnly = False
    Me.ef_creation.Location = New System.Drawing.Point(224, 49)
    Me.ef_creation.Name = "ef_creation"
    Me.ef_creation.PlaceHolder = Nothing
    Me.ef_creation.Size = New System.Drawing.Size(217, 24)
    Me.ef_creation.SufixText = "Sufix Text"
    Me.ef_creation.SufixTextVisible = True
    Me.ef_creation.TabIndex = 2
    Me.ef_creation.TabStop = False
    Me.ef_creation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_creation.TextValue = ""
    Me.ef_creation.TextWidth = 110
    Me.ef_creation.Value = ""
    Me.ef_creation.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_code_rep
    '
    Me.ef_code_rep.DoubleValue = 0.0R
    Me.ef_code_rep.IntegerValue = 0
    Me.ef_code_rep.IsReadOnly = False
    Me.ef_code_rep.Location = New System.Drawing.Point(5, 23)
    Me.ef_code_rep.Name = "ef_code_rep"
    Me.ef_code_rep.PlaceHolder = Nothing
    Me.ef_code_rep.Size = New System.Drawing.Size(168, 24)
    Me.ef_code_rep.SufixText = "Sufix Text"
    Me.ef_code_rep.SufixTextVisible = True
    Me.ef_code_rep.TabIndex = 0
    Me.ef_code_rep.TabStop = False
    Me.ef_code_rep.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_code_rep.TextValue = ""
    Me.ef_code_rep.TextWidth = 50
    Me.ef_code_rep.Value = ""
    Me.ef_code_rep.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name_rep
    '
    Me.ef_name_rep.DoubleValue = 0.0R
    Me.ef_name_rep.IntegerValue = 0
    Me.ef_name_rep.IsReadOnly = False
    Me.ef_name_rep.Location = New System.Drawing.Point(196, 24)
    Me.ef_name_rep.Name = "ef_name_rep"
    Me.ef_name_rep.PlaceHolder = Nothing
    Me.ef_name_rep.Size = New System.Drawing.Size(245, 24)
    Me.ef_name_rep.SufixText = "Sufix Text"
    Me.ef_name_rep.SufixTextVisible = True
    Me.ef_name_rep.TabIndex = 1
    Me.ef_name_rep.TabStop = False
    Me.ef_name_rep.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name_rep.TextValue = ""
    Me.ef_name_rep.TextWidth = 55
    Me.ef_name_rep.Value = ""
    Me.ef_name_rep.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_points
    '
    Me.ef_com_points.DoubleValue = 0.0R
    Me.ef_com_points.IntegerValue = 0
    Me.ef_com_points.IsReadOnly = False
    Me.ef_com_points.Location = New System.Drawing.Point(15, 110)
    Me.ef_com_points.Name = "ef_com_points"
    Me.ef_com_points.PlaceHolder = Nothing
    Me.ef_com_points.Size = New System.Drawing.Size(286, 24)
    Me.ef_com_points.SufixText = "Sufix Text"
    Me.ef_com_points.SufixTextVisible = True
    Me.ef_com_points.SufixTextWidth = 145
    Me.ef_com_points.TabIndex = 3
    Me.ef_com_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_points.TextValue = ""
    Me.ef_com_points.TextWidth = 0
    Me.ef_com_points.Value = ""
    Me.ef_com_points.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_buyin
    '
    Me.ef_com_buyin.DoubleValue = 0.0R
    Me.ef_com_buyin.IntegerValue = 0
    Me.ef_com_buyin.IsReadOnly = False
    Me.ef_com_buyin.Location = New System.Drawing.Point(336, 50)
    Me.ef_com_buyin.Name = "ef_com_buyin"
    Me.ef_com_buyin.PlaceHolder = Nothing
    Me.ef_com_buyin.Size = New System.Drawing.Size(272, 24)
    Me.ef_com_buyin.SufixText = "Sufix Text"
    Me.ef_com_buyin.SufixTextVisible = True
    Me.ef_com_buyin.SufixTextWidth = 130
    Me.ef_com_buyin.TabIndex = 5
    Me.ef_com_buyin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_buyin.TextValue = ""
    Me.ef_com_buyin.TextWidth = 0
    Me.ef_com_buyin.Value = ""
    Me.ef_com_buyin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_netwin
    '
    Me.ef_com_netwin.DoubleValue = 0.0R
    Me.ef_com_netwin.IntegerValue = 0
    Me.ef_com_netwin.IsReadOnly = False
    Me.ef_com_netwin.Location = New System.Drawing.Point(336, 81)
    Me.ef_com_netwin.Name = "ef_com_netwin"
    Me.ef_com_netwin.PlaceHolder = Nothing
    Me.ef_com_netwin.Size = New System.Drawing.Size(272, 24)
    Me.ef_com_netwin.SufixText = "Sufix Text"
    Me.ef_com_netwin.SufixTextVisible = True
    Me.ef_com_netwin.SufixTextWidth = 130
    Me.ef_com_netwin.TabIndex = 6
    Me.ef_com_netwin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_netwin.TextValue = ""
    Me.ef_com_netwin.TextWidth = 0
    Me.ef_com_netwin.Value = ""
    Me.ef_com_netwin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_coinin
    '
    Me.ef_com_coinin.DoubleValue = 0.0R
    Me.ef_com_coinin.IntegerValue = 0
    Me.ef_com_coinin.IsReadOnly = False
    Me.ef_com_coinin.Location = New System.Drawing.Point(336, 19)
    Me.ef_com_coinin.Name = "ef_com_coinin"
    Me.ef_com_coinin.PlaceHolder = Nothing
    Me.ef_com_coinin.Size = New System.Drawing.Size(272, 24)
    Me.ef_com_coinin.SufixText = "Sufix Text"
    Me.ef_com_coinin.SufixTextVisible = True
    Me.ef_com_coinin.SufixTextWidth = 130
    Me.ef_com_coinin.TabIndex = 4
    Me.ef_com_coinin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_coinin.TextValue = ""
    Me.ef_com_coinin.TextWidth = 0
    Me.ef_com_coinin.Value = ""
    Me.ef_com_coinin.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_enrolled
    '
    Me.ef_com_enrolled.DoubleValue = 0.0R
    Me.ef_com_enrolled.IntegerValue = 0
    Me.ef_com_enrolled.IsReadOnly = False
    Me.ef_com_enrolled.Location = New System.Drawing.Point(15, 79)
    Me.ef_com_enrolled.Name = "ef_com_enrolled"
    Me.ef_com_enrolled.PlaceHolder = Nothing
    Me.ef_com_enrolled.Size = New System.Drawing.Size(286, 24)
    Me.ef_com_enrolled.SufixText = "Sufix Text"
    Me.ef_com_enrolled.SufixTextVisible = True
    Me.ef_com_enrolled.SufixTextWidth = 145
    Me.ef_com_enrolled.TabIndex = 2
    Me.ef_com_enrolled.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_enrolled.TextValue = ""
    Me.ef_com_enrolled.TextWidth = 0
    Me.ef_com_enrolled.Value = ""
    Me.ef_com_enrolled.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_visits
    '
    Me.ef_com_visits.DoubleValue = 0.0R
    Me.ef_com_visits.IntegerValue = 0
    Me.ef_com_visits.IsReadOnly = False
    Me.ef_com_visits.Location = New System.Drawing.Point(16, 50)
    Me.ef_com_visits.Name = "ef_com_visits"
    Me.ef_com_visits.PlaceHolder = Nothing
    Me.ef_com_visits.Size = New System.Drawing.Size(285, 24)
    Me.ef_com_visits.SufixText = "Sufix Text"
    Me.ef_com_visits.SufixTextVisible = True
    Me.ef_com_visits.SufixTextWidth = 145
    Me.ef_com_visits.TabIndex = 1
    Me.ef_com_visits.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_visits.TextValue = ""
    Me.ef_com_visits.TextWidth = 0
    Me.ef_com_visits.Value = ""
    Me.ef_com_visits.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_com_cutomers
    '
    Me.ef_com_cutomers.DoubleValue = 0.0R
    Me.ef_com_cutomers.IntegerValue = 0
    Me.ef_com_cutomers.IsReadOnly = False
    Me.ef_com_cutomers.Location = New System.Drawing.Point(15, 20)
    Me.ef_com_cutomers.Name = "ef_com_cutomers"
    Me.ef_com_cutomers.PlaceHolder = Nothing
    Me.ef_com_cutomers.Size = New System.Drawing.Size(286, 24)
    Me.ef_com_cutomers.SufixText = "Sufix Text"
    Me.ef_com_cutomers.SufixTextVisible = True
    Me.ef_com_cutomers.SufixTextWidth = 145
    Me.ef_com_cutomers.TabIndex = 0
    Me.ef_com_cutomers.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_com_cutomers.TextValue = ""
    Me.ef_com_cutomers.TextWidth = 0
    Me.ef_com_cutomers.Value = ""
    Me.ef_com_cutomers.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_comissions
    '
    Me.gb_comissions.Controls.Add(Me.ef_com_cutomers)
    Me.gb_comissions.Controls.Add(Me.ef_com_points)
    Me.gb_comissions.Controls.Add(Me.ef_com_buyin)
    Me.gb_comissions.Controls.Add(Me.ef_com_netwin)
    Me.gb_comissions.Controls.Add(Me.ef_com_coinin)
    Me.gb_comissions.Controls.Add(Me.ef_com_enrolled)
    Me.gb_comissions.Controls.Add(Me.ef_com_visits)
    Me.gb_comissions.Location = New System.Drawing.Point(18, 169)
    Me.gb_comissions.Name = "gb_comissions"
    Me.gb_comissions.Size = New System.Drawing.Size(638, 152)
    Me.gb_comissions.TabIndex = 3
    Me.gb_comissions.TabStop = False
    Me.gb_comissions.Text = "xComissions"
    '
    'gb_representative_junket
    '
    Me.gb_representative_junket.Controls.Add(Me.bt_search_rep)
    Me.gb_representative_junket.Controls.Add(Me.ef_code_rep)
    Me.gb_representative_junket.Controls.Add(Me.ef_name_rep)
    Me.gb_representative_junket.Location = New System.Drawing.Point(18, 100)
    Me.gb_representative_junket.Name = "gb_representative_junket"
    Me.gb_representative_junket.Size = New System.Drawing.Size(638, 63)
    Me.gb_representative_junket.TabIndex = 2
    Me.gb_representative_junket.TabStop = False
    Me.gb_representative_junket.Text = "xRepresentative"
    '
    'bt_search_rep
    '
    Me.bt_search_rep.Location = New System.Drawing.Point(453, 24)
    Me.bt_search_rep.Name = "bt_search_rep"
    Me.bt_search_rep.Size = New System.Drawing.Size(90, 24)
    Me.bt_search_rep.TabIndex = 2
    Me.bt_search_rep.Text = "xSelect"
    Me.bt_search_rep.UseVisualStyleBackColor = True
    '
    'gb_comments
    '
    Me.gb_comments.Controls.Add(Me.txt_comments)
    Me.gb_comments.Location = New System.Drawing.Point(18, 327)
    Me.gb_comments.Name = "gb_comments"
    Me.gb_comments.Size = New System.Drawing.Size(638, 159)
    Me.gb_comments.TabIndex = 4
    Me.gb_comments.TabStop = False
    Me.gb_comments.Text = "xComments"
    '
    'txt_comments
    '
    Me.txt_comments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txt_comments.Location = New System.Drawing.Point(16, 25)
    Me.txt_comments.Multiline = True
    Me.txt_comments.Name = "txt_comments"
    Me.txt_comments.Size = New System.Drawing.Size(604, 128)
    Me.txt_comments.TabIndex = 0
    '
    'gb_details
    '
    Me.gb_details.Controls.Add(Me.ef_code)
    Me.gb_details.Controls.Add(Me.ef_name)
    Me.gb_details.Controls.Add(Me.ef_creation)
    Me.gb_details.Location = New System.Drawing.Point(18, 9)
    Me.gb_details.Name = "gb_details"
    Me.gb_details.Size = New System.Drawing.Size(447, 85)
    Me.gb_details.TabIndex = 0
    Me.gb_details.TabStop = False
    Me.gb_details.Text = "xDetails"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 49)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(161, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 40
    Me.dtp_to.Value = New Date(2017, 4, 3, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(161, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 40
    Me.dtp_from.Value = New Date(2017, 4, 3, 0, 0, 0, 0)
    '
    'gb_data_range
    '
    Me.gb_data_range.Controls.Add(Me.dtp_to)
    Me.gb_data_range.Controls.Add(Me.dtp_from)
    Me.gb_data_range.Location = New System.Drawing.Point(471, 9)
    Me.gb_data_range.Name = "gb_data_range"
    Me.gb_data_range.Size = New System.Drawing.Size(185, 85)
    Me.gb_data_range.TabIndex = 1
    Me.gb_data_range.TabStop = False
    Me.gb_data_range.Text = "xDataRange"
    '
    'gb_flyers
    '
    Me.gb_flyers.Controls.Add(Me.dgv_flyers)
    Me.gb_flyers.Controls.Add(Me.bt_new)
    Me.gb_flyers.Controls.Add(Me.bt_select)
    Me.gb_flyers.Location = New System.Drawing.Point(18, 492)
    Me.gb_flyers.Name = "gb_flyers"
    Me.gb_flyers.Size = New System.Drawing.Size(638, 228)
    Me.gb_flyers.TabIndex = 5
    Me.gb_flyers.TabStop = False
    Me.gb_flyers.Text = "xFlyers"
    '
    'dgv_flyers
    '
    Me.dgv_flyers.AllowUserToAddRows = False
    Me.dgv_flyers.AllowUserToDeleteRows = False
    Me.dgv_flyers.AllowUserToResizeRows = False
    Me.dgv_flyers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgv_flyers.Location = New System.Drawing.Point(16, 20)
    Me.dgv_flyers.MultiSelect = False
    Me.dgv_flyers.Name = "dgv_flyers"
    Me.dgv_flyers.ReadOnly = True
    Me.dgv_flyers.RowHeadersVisible = False
    Me.dgv_flyers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
    Me.dgv_flyers.RowTemplate.Height = 17
    Me.dgv_flyers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
    Me.dgv_flyers.Size = New System.Drawing.Size(520, 190)
    Me.dgv_flyers.TabIndex = 0
    '
    'bt_new
    '
    Me.bt_new.Location = New System.Drawing.Point(545, 159)
    Me.bt_new.Name = "bt_new"
    Me.bt_new.Size = New System.Drawing.Size(83, 24)
    Me.bt_new.TabIndex = 1
    Me.bt_new.Text = "xNew"
    Me.bt_new.UseVisualStyleBackColor = True
    '
    'bt_select
    '
    Me.bt_select.Enabled = False
    Me.bt_select.Location = New System.Drawing.Point(546, 188)
    Me.bt_select.Name = "bt_select"
    Me.bt_select.Size = New System.Drawing.Size(83, 24)
    Me.bt_select.TabIndex = 2
    Me.bt_select.TabStop = False
    Me.bt_select.Text = "xSelect"
    Me.bt_select.UseVisualStyleBackColor = True
    '
    'frm_junkets_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(778, 732)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_junkets_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_junkets_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_comissions.ResumeLayout(False)
    Me.gb_representative_junket.ResumeLayout(False)
    Me.gb_comments.ResumeLayout(False)
    Me.gb_comments.PerformLayout()
    Me.gb_details.ResumeLayout(False)
    Me.gb_data_range.ResumeLayout(False)
    Me.gb_flyers.ResumeLayout(False)
    CType(Me.dgv_flyers, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_representative_junket As System.Windows.Forms.GroupBox
  Friend WithEvents bt_search_rep As System.Windows.Forms.Button
  Friend WithEvents ef_code_rep As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name_rep As GUI_Controls.uc_entry_field
  Friend WithEvents gb_comissions As System.Windows.Forms.GroupBox
  Friend WithEvents ef_com_cutomers As GUI_Controls.uc_entry_field
  Friend WithEvents ef_com_points As GUI_Controls.uc_entry_field
  Friend WithEvents ef_com_buyin As GUI_Controls.uc_entry_field
  Friend WithEvents ef_com_netwin As GUI_Controls.uc_entry_field
  Friend WithEvents ef_com_coinin As GUI_Controls.uc_entry_field
  Friend WithEvents ef_com_enrolled As GUI_Controls.uc_entry_field
  Friend WithEvents ef_com_visits As GUI_Controls.uc_entry_field
  Friend WithEvents ef_creation As GUI_Controls.uc_entry_field
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_code As GUI_Controls.uc_entry_field
  Friend WithEvents gb_comments As System.Windows.Forms.GroupBox
  Friend WithEvents txt_comments As System.Windows.Forms.TextBox
  Friend WithEvents gb_details As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gb_data_range As System.Windows.Forms.GroupBox
  Friend WithEvents gb_flyers As System.Windows.Forms.GroupBox
  Friend WithEvents bt_new As System.Windows.Forms.Button
  Friend WithEvents bt_select As System.Windows.Forms.Button
  Friend WithEvents dgv_flyers As System.Windows.Forms.DataGridView
End Class
