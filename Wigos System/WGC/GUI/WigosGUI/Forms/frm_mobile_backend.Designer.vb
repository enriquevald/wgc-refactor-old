﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_mobile_backend
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.wb_backend = New System.Windows.Forms.WebBrowser()
    Me.SuspendLayout()
    '
    'wb_backend
    '
    Me.wb_backend.AllowWebBrowserDrop = False
    Me.wb_backend.CausesValidation = False
    Me.wb_backend.Dock = System.Windows.Forms.DockStyle.Fill
    Me.wb_backend.Location = New System.Drawing.Point(4, 4)
    Me.wb_backend.MinimumSize = New System.Drawing.Size(20, 20)
    Me.wb_backend.Name = "wb_backend"
    Me.wb_backend.ScriptErrorsSuppressed = True
    Me.wb_backend.Size = New System.Drawing.Size(988, 496)
    Me.wb_backend.TabIndex = 1
    Me.wb_backend.WebBrowserShortcutsEnabled = False
    '
    'frm_mobile_backend
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(996, 504)
    Me.Controls.Add(Me.wb_backend)
    Me.Name = "frm_mobile_backend"
    Me.Text = "frm_mobile_backend"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents wb_backend As System.Windows.Forms.WebBrowser
End Class
