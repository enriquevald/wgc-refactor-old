'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_site_jackpot_history.vb
'
' DESCRIPTION:   Site Jackpot history form
'
' AUTHOR:        Ra�l Cervera
'
' CREATION DATE: 30-NOV-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-NOV-2010  RCI    Initial version.
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 16-DEC-2013  RMS    In TITO Mode virtual account id should be hidden
' 25-FEB-2014  RMS    Added Provider and Terminal columns
' 03-SEP-2014  LEM    Added functionality to show terminal location data.
' 14-JAN-2015  DCS    Update to show in MS and add Subtotals
' 11-FEB-2015  FJC    Fixed Bug WIG-2040.
' 07-MAY-2015  FOS    Add multicurrency control
' 22-OCT-2015  SDS    Bug 5170: Currency Symbol in amount
' 26-SEP-2017  ETP    Fixed Bug 29922:[WIGOS-4563] Multisite: wrong site number when exporting data to Excel or Print
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Data
Imports System.Text
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_site_jackpot_history
  Inherits frm_base_sel

#Region "Constants"
  Private TERMINAL_DATA_COLUMNS As Int32

  ' DB Columns
  Private Const SQL_COLUMN_JACKPOT_TYPE_INDEX As Integer = 0
  Private Const SQL_COLUMN_JACKPOT_TYPE As Integer = 1
  Private Const SQL_COLUMN_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_DATE_AWARDED As Integer = 3
  Private Const SQL_COLUMN_JACKPOT_PAID As Integer = 4
  Private Const SQL_COLUMN_ACCOUNT_ID As Integer = 5
  Private Const SQL_COLUMN_HOLDER_NAME As Integer = 6
  'Private Const SQL_COLUMN_HP_EXPIRED As Integer = 7
  Private Const SQL_COLUMN_ACCOUNT_TYPE As Integer = 7
  Private Const SQL_COLUMN_PROVIDER_NAME As Integer = 8
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 9
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 10
  Private Const SQL_COLUMN_SITE As Integer = 11

  ' Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_DATE_AWARDED As Integer
  Private GRID_COLUMN_JACKPOT_TYPE_INDEX As Integer
  Private GRID_COLUMN_JACKPOT_TYPE As Integer
  Private GRID_COLUMN_AMOUNT As Integer
  Private GRID_COLUMN_STATUS As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_ACCOUNT_ID As Integer
  Private GRID_COLUMN_HOLDER_NAME As Integer
  Private GRID_COLUMN_SITE As Integer

  ' Grid Columns
  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const TOTAL_JACKPOT_INSTANCES As Integer = 3
  Private Const FORMAT_ID As String = "00#"

  Private Const CONVERT_CURRENCY_VALUE As Integer = 1
  Private Const MULTI_CURRENCY_COLUMN_ISO_CODE As Integer = 12

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0

#End Region ' Constants

#Region " Members "

  ' For Subtotals
  Private m_subtotal_jackpot_type_id As String
  Private m_subtotal_jackpot_type As String
  Private m_site_id As String
  Private m_subtotal_amount_level As Decimal
  Private m_total_amount_site As Decimal

  ' For Totals
  Private m_total_amount_level_1 As Decimal
  Private m_total_amount_level_2 As Decimal
  Private m_total_amount_level_3 As Decimal
  Private m_total_amount As Decimal

  Private m_grouped_type As ENUM_GROUPED_TYPE
  Private m_subtotal_type As ENUM_SUBTOTAL_TYPE

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_jackpot_type As String
  Private m_site As String
  Private m_options As String
  Private m_currency_option As String
  Private m_sites_checked As String

  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False
  Private m_is_center As Boolean = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)
  Private m_is_multi_currency As Boolean = GeneralParam.GetBoolean("MultiSite.Multicurrency", "Enabled", False)


  Private Enum ENUM_GROUPED_TYPE
    DETAILED = 0
    DETAILED_EXCLUDE_INACTIVITY = 1
    ONLY_TOTALS = 2
    ONLY_TOTALS_EXCLUDE_INACTIVITY = 3
  End Enum ' ENUM_GROUPED_TYPE

  Private Enum ENUM_SUBTOTAL_TYPE
    SUBTOTAL_LEVEL = 0
    SUBTOTAL_SITE = 1
    TOTAL_LEVEL = 2
    TOTAL = 3
  End Enum ' ENUM_SUBTOTAL_TYPE

#End Region ' Members

#Region "Overrides"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_SITE_JACKPOT_HISTORY

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
   
    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(350)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_JACKPOT_MGR.Id(207))

    ' Jackpot Type
    gb_jackpot_type.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(351)
    SetJackpotValues()

    ' Options
    Me.gb_options.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5682) ' Options
    '    Empty rows
    Me.chk_exclude_inactivity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4738) ' Exclude rows without activity
    '    Detailed
    Me.chk_level_totals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1361) ' Show details
    Me.chk_level_totals.Checked = True
    '    Terminal location
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)
    If m_is_center Then
      Me.chk_terminal_location.Visible = False
    End If

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    If m_is_center AndAlso m_is_multi_currency Then
      Call InitializeMultiCurrencyControls()
    ElseIf m_is_center Then
      Call InitializeMultiSiteControls()
    Else
      ' Sites and special designer configuration
      Me.uc_sites_sel.Visible = False
      Me.uc_multi_currency_sel.Visible = False
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.uc_dsl
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    ' Multisite Multicurrencies - Sites selection
    If m_is_center AndAlso m_is_multi_currency Then
      Me.ApplyFormatCurrency = uc_multi_currency_sel.IsGridApplyChanges()
      UpdateSiteCurrenciesList()
      Me.MultiSiteCurrencies = Me.uc_multi_currency_sel.CurrenciesTable()
      If Not Me.uc_multi_currency_sel.FilterCheckSites() Then

        Return False
      End If
    ElseIf m_is_center Then
      ' Multisite: Sites selection
      If Not Me.uc_sites_sel.FilterCheckSites() Then

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

 


  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder
    Dim _exclude_inactivity As Boolean

    _sb = New StringBuilder()

    _exclude_inactivity = (m_grouped_type = ENUM_GROUPED_TYPE.DETAILED_EXCLUDE_INACTIVITY) Or (m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS_EXCLUDE_INACTIVITY)

    _sb.AppendLine("	  SELECT   SJI_INDEX ")
    _sb.AppendLine("	         , HP_SITE_JACKPOT_NAME   ")
    _sb.AppendLine("	         , HP_AMOUNT   ")
    _sb.AppendLine("	         , HP_DATETIME   ")
    _sb.AppendLine("	         , HP_STATUS    ")
    _sb.AppendLine("	         , HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID   ")
    _sb.AppendLine("	         , AC_HOLDER_NAME   ")
    _sb.AppendLine("	         , AC_TYPE   ")
    _sb.AppendLine("	         , PV_NAME  ")
    _sb.AppendLine("	         , TE_NAME  ")
    _sb.AppendLine("	         , HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID  					  ")
    If m_is_center Then
      _sb.AppendLine("	         , ST_SITE_ID ")
      ' Only need iso_code if site is multicurrency and all selected sites have iso_code reported
      If m_is_multi_currency AndAlso Not uc_multi_currency_sel.HasSitesWithoutCurrency Then
        _sb.AppendLine("	         , SC_ISO_CODE")
      End If
      ' Create table with 3 Jackpots and all sites for join and create empty info
      _sb.AppendLine("	 FROM    ( SELECT   ST_SITE_ID  ")
      _sb.AppendLine("	                  , SJI_INDEX   ")
      _sb.AppendLine("	             FROM   SITES  ")
      _sb.AppendLine("				            , (      SELECT 1 AS SJI_INDEX  ")
      _sb.AppendLine("					             UNION SELECT 2 AS SJI_INDEX  ")
      _sb.AppendLine("						           UNION SELECT 3 AS SJI_INDEX  ")
      _sb.AppendLine("				              ) AS SJI   ")
      '  if site is multicurrency and all selected sites have iso_code reported
      If m_is_multi_currency AndAlso Not uc_multi_currency_sel.HasSitesWithoutCurrency Then
        _sb.AppendLine("		        WHERE   ST_SITE_ID IN ( " & m_sites_checked)
        _sb.AppendLine("		        ) ")
      End If
      _sb.AppendLine("	    	   ) AS HP ")

      _sb.AppendLine("	    LEFT   OUTER JOIN   HANDPAYS  ON   HP_SITE_ID = ST_SITE_ID ")
      _sb.AppendLine("	     		                         AND   HP_SITE_JACKPOT_INDEX = SJI_INDEX  ")
    Else
      _sb.AppendLine("	    FROM   SITE_JACKPOT_INSTANCES ")
      _sb.AppendLine("	    LEFT   OUTER JOIN   HANDPAYS  ON   HP_SITE_JACKPOT_INDEX = SJI_INDEX  ")
    End If
    _sb.AppendLine("	    		                         AND   HP_TYPE = " & WSI.Common.HANDPAY_TYPE.SITE_JACKPOT)
    If Me.uc_dsl.GetSqlFilterCondition("HP_DATETIME").Length > 0 Then
      _sb.AppendLine("                                 AND " & Me.uc_dsl.GetSqlFilterCondition("HP_DATETIME"))
    End If
    _sb.AppendLine("	    LEFT   OUTER JOIN ACCOUNTS    ON   HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = AC_ACCOUNT_ID ")
    _sb.AppendLine("	    LEFT   OUTER JOIN TERMINALS   ON   TE_TERMINAL_ID=HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID 	 ")
    If m_is_center Then
      _sb.AppendLine("		                               AND   TE_SITE_ID = HP_SITE_ID  ")
    End If
    _sb.AppendLine("	    LEFT   OUTER JOIN PROVIDERS   ON   PV_ID= TE_PROV_ID  ")

    '  if site is multicurrency and all selected sites have iso_code reported
    If m_is_center AndAlso m_is_multi_currency AndAlso Not uc_multi_currency_sel.HasSitesWithoutCurrency Then
      _sb.AppendLine("	    LEFT   OUTER JOIN SITE_CURRENCIES   ON   SC_SITE_ID = HP.ST_SITE_ID ")
      _sb.AppendLine("	                                       AND   SC_TYPE = 1   ")
    End If
    _sb.AppendLine(GetSqlWhere(_exclude_inactivity))

    If m_is_center Then
      _sb.AppendLine("	   ORDER   BY ST_SITE_ID, SJI_INDEX, HP_DATETIME DESC ")
    Else
      _sb.AppendLine("	   ORDER   BY SJI_INDEX, HP_DATETIME DESC ")
    End If

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)

    If m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS Or m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS_EXCLUDE_INACTIVITY Then
      Return False
    End If

    ' Site
    If m_is_center Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE))
    End If

    ' Jackpot Type ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT_TYPE_INDEX).Value = DbRow.Value(SQL_COLUMN_JACKPOT_TYPE_INDEX)

    ' Jackpot Type
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_TYPE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_JACKPOT_TYPE).Value = DbRow.Value(SQL_COLUMN_JACKPOT_TYPE)
    End If

    ' Date Awarded
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_DATE_AWARDED)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_AWARDED).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE_AWARDED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

        ' Amount
        'SDS 22-10-2015 format whith Currency symbol
        If Not IsDBNull(DbRow.Value(SQL_COLUMN_AMOUNT)) Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = SelectTypeCurrency(m_is_center, DbRow.Value(SQL_COLUMN_AMOUNT))
        Else
            Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = SelectTypeCurrency(m_is_center, 0)
        End If

    ' Status
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_PAID)) Then
      Select Case DbRow.Value(SQL_COLUMN_JACKPOT_PAID) And WSI.Common.HANDPAY_STATUS.MASK_STATUS
        Case WSI.Common.HANDPAY_STATUS.PAID
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_JACKPOT_MGR.GetString(352) ' Transferido
        Case WSI.Common.HANDPAY_STATUS.EXPIRED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_JACKPOT_MGR.GetString(354) ' Caducado
        Case WSI.Common.HANDPAY_STATUS.PENDING
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_JACKPOT_MGR.GetString(353) ' No Transferido
      End Select
    End If
    '    TODO: Use generic function HandpayStatusString -> At Jan 2015 only works for tito
    '    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = WSI.Common.Handpays.HandpayStatusString(DbRow.Value(SQL_COLUMN_JACKPOT_PAID))

    ' Terminal Report
    If m_is_center Then
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_PROVIDER_NAME)) Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA).Value = DbRow.Value(SQL_COLUMN_PROVIDER_NAME)
      End If
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_NAME)) Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + 1).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      End If
    Else
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_ID)) Then
        _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
        For _idx As Int32 = 0 To _terminal_data.Count - 1
          If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
            Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
          End If
        Next
      End If
    End If

    ' Account Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = String.Empty
    If Not DbRow.IsNull(SQL_COLUMN_ACCOUNT_ID) Then
      If Not DbRow.Value(SQL_COLUMN_ACCOUNT_ID) = 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_ACCOUNT_ID).Value = DbRow.Value(SQL_COLUMN_ACCOUNT_ID)
      End If
    End If

    ' Holder Name
    If DbRow.IsNull(SQL_COLUMN_HOLDER_NAME) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = String.Empty
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_HOLDER_NAME).Value = DbRow.Value(SQL_COLUMN_HOLDER_NAME)
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False

    Call ResetCounters(ENUM_SUBTOTAL_TYPE.TOTAL)

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    ' Level Subtotal
    If Not String.IsNullOrEmpty(m_subtotal_jackpot_type_id) AndAlso m_subtotal_jackpot_type_id <> DbRow.Value(SQL_COLUMN_JACKPOT_TYPE_INDEX) Then
      Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL)

      Call CalculateLevelAmounts()

      Call ResetCounters(ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL)
    End If

    If m_is_center Then
      ' Site Subtotal
      If Not String.IsNullOrEmpty(m_site_id) AndAlso m_site_id <> DbRow.Value(SQL_COLUMN_SITE) Then

        ' Calcule and add subtotal level before subtotal site
        Call CalculateLevelAmounts()
        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL)

        ' Now print subtotal site
        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.SUBTOTAL_SITE)

        Call ResetCounters(ENUM_SUBTOTAL_TYPE.SUBTOTAL_SITE)
      End If

    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_JACKPOT_TYPE)) Then
      m_subtotal_jackpot_type = DbRow.Value(SQL_COLUMN_JACKPOT_TYPE)
    End If

    m_subtotal_jackpot_type_id = DbRow.Value(SQL_COLUMN_JACKPOT_TYPE_INDEX)

    If m_is_center Then
      m_site_id = DbRow.Value(SQL_COLUMN_SITE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_AMOUNT)) Then
      m_subtotal_amount_level = m_subtotal_amount_level + DbRow.Value(SQL_COLUMN_AMOUNT)
      m_total_amount = m_total_amount + DbRow.Value(SQL_COLUMN_AMOUNT)

      If m_is_center Then
        m_total_amount_site = m_total_amount_site + DbRow.Value(SQL_COLUMN_AMOUNT)
      End If

    End If

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _exclude_inactivity As Boolean
    Dim _jackpot_selected As Boolean

    _jackpot_selected = chk_jackpot1.Checked Or chk_jackpot2.Checked Or chk_jackpot3.Checked

    _exclude_inactivity = (m_grouped_type = ENUM_GROUPED_TYPE.DETAILED_EXCLUDE_INACTIVITY) Or (m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS_EXCLUDE_INACTIVITY)

    Call CalculateLevelAmounts()

    If Not (_exclude_inactivity And m_subtotal_amount_level = 0) Then
      Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL)
    End If

    If m_is_center Then

      If Not (_exclude_inactivity And m_total_amount_site = 0) Then
        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.SUBTOTAL_SITE)
      End If

      ' Total level 1
      If Not (_exclude_inactivity And m_total_amount_level_1 = 0) And (Not _jackpot_selected Or chk_jackpot1.Checked) Then
        m_subtotal_jackpot_type_id = chk_jackpot1.Tag
        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.TOTAL_LEVEL)
      End If

      ' Total level 2
      If Not (_exclude_inactivity And m_total_amount_level_2 = 0) And (Not _jackpot_selected Or chk_jackpot2.Checked) Then
        m_subtotal_jackpot_type_id = chk_jackpot2.Tag
        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.TOTAL_LEVEL)
      End If

      ' Total level 3
      If Not (_exclude_inactivity And m_total_amount_level_3 = 0) And (Not _jackpot_selected Or chk_jackpot3.Checked) Then
        m_subtotal_jackpot_type_id = chk_jackpot3.Tag
        Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.TOTAL_LEVEL)
      End If
    End If

    Call DrawSubTotal(ENUM_SUBTOTAL_TYPE.TOTAL)

  End Sub ' GUI_AfterLastRow

#End Region ' Overrides

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(208), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(209), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_JACKPOT_MGR.GetString(351), m_jackpot_type)
    If m_is_center Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1771), m_site)
    End If
    If m_is_multi_currency Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6282), Me.uc_multi_currency_sel.GetTextCurrenciesSelected(m_currency_option))
      PrintExchangeCurrency(PrintData)
    End If
    PrintData.SetFilter("", m_options, False, True)

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_jackpot_type = ""
    m_site = ""
    m_options = ""
    m_currency_option = ""
    m_sites_checked = ""

    ' Date From
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.uc_dsl.FromDate.AddHours(Me.uc_dsl.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Date To
    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.uc_dsl.ToDate.AddHours(Me.uc_dsl.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If

    ' Jackpot type
    If Me.chk_jackpot1.Checked And Me.chk_jackpot2.Checked And Me.chk_jackpot3.Checked Then
      m_jackpot_type = GLB_NLS_GUI_JACKPOT_MGR.GetString(201)
    Else
      If Me.chk_jackpot1.Checked Then
        m_jackpot_type = chk_jackpot1.Text
      End If

      If Me.chk_jackpot2.Checked Then
        If m_jackpot_type.Length > 0 Then
          m_jackpot_type = m_jackpot_type & ", " & chk_jackpot2.Text
        Else
          m_jackpot_type = chk_jackpot2.Text
        End If
      End If

      If Me.chk_jackpot3.Checked Then
        If m_jackpot_type.Length > 0 Then
          m_jackpot_type = m_jackpot_type & ", " & chk_jackpot3.Text
        Else
          m_jackpot_type = chk_jackpot3.Text
        End If
      End If
    End If

    ' Site
    If m_is_center And Not String.IsNullOrEmpty(Me.uc_sites_sel.GetSitesIdListSelected()) Then
      m_site = Me.uc_sites_sel.GetSitesIdListSelectedToPrint()
    End If

    'Site- MultiCurrency
    If m_is_center AndAlso CurrencyMultisite.IsEnabledMultiCurrency Then
      m_site = Me.uc_multi_currency_sel.GetSitesIdListSelectedToPrint()
      m_currency_option = Me.uc_multi_currency_sel.IsGridApplyChanges
      m_sites_checked = Me.uc_multi_currency_sel.GetSitesChecked()
    End If

    ' Options
    If Me.chk_level_totals.Checked Then
      m_options = Me.chk_level_totals.Text & ". "
    End If

    If Me.chk_terminal_location.Checked Then
      m_options = m_options & Me.chk_terminal_location.Text & ". "
    End If

    If Me.chk_exclude_inactivity.Checked Then
      m_options = m_options & Me.chk_exclude_inactivity.Text & ". "
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub

#End Region ' GUI Reports

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region "Private Functions"

  ' PURPOSE: Increment right level amount with current subtotal amount
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CalculateLevelAmounts()

    If m_is_center Then
      If Not String.IsNullOrEmpty(m_site_id) And Not String.IsNullOrEmpty(m_subtotal_jackpot_type_id) Then
        ' Add subtotal level to total level only for MS
        Select Case m_subtotal_jackpot_type_id
          Case chk_jackpot1.Tag
            m_total_amount_level_1 += m_subtotal_amount_level
          Case chk_jackpot2.Tag
            m_total_amount_level_2 += m_subtotal_amount_level
          Case chk_jackpot3.Tag
            m_total_amount_level_3 += m_subtotal_amount_level
        End Select
      End If
    End If

  End Sub ' CalculateLevelAmounts

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)
    End If

    _final_time = _final_time.Date
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = True
    Me.uc_dsl.FromDate = _final_time.AddDays(-1)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ClosingTime = _closing_time

    Me.chk_jackpot1.Checked = True
    Me.chk_jackpot2.Checked = True
    Me.chk_jackpot3.Checked = True

    Me.chk_terminal_location.Checked = False
    Me.chk_level_totals.Checked = True
    Me.chk_exclude_inactivity.Checked = True

    If m_is_center AndAlso Not m_is_multi_currency Then
      Me.uc_sites_sel.SetDefaultValues(False)
    End If
    If m_is_multi_currency Then
      Me.uc_multi_currency_sel.SetDefaultValues(False)
    End If


  End Sub ' SetDefaultValues

  ' PURPOSE: Set checkboxes for Jackpot types
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetJackpotValues()

    Dim str_sql As String
    Dim data_table As DataTable
    Dim idx As Integer = 0

    chk_jackpot1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "1")
    chk_jackpot2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "2")
    chk_jackpot3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "3")

    ' That values were used to compare enums with right jackpot
    chk_jackpot1.Tag = 1
    chk_jackpot2.Tag = 2
    chk_jackpot3.Tag = 3

    If Not m_is_center Then
      str_sql = "SELECT SJI_INDEX, SJI_NAME FROM SITE_JACKPOT_INSTANCES"

      data_table = GUI_GetTableUsingSQL(str_sql, TOTAL_JACKPOT_INSTANCES)

      chk_jackpot1.Text = chk_jackpot1.Text & " (" & data_table.Rows(0).Item(1) & ")"
      chk_jackpot2.Text = chk_jackpot2.Text & " (" & data_table.Rows(1).Item(1) & ")"
      chk_jackpot3.Text = chk_jackpot3.Text & " (" & data_table.Rows(2).Item(1) & ")"
    End If

  End Sub ' SetJackpotValues

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      .Button(BUTTON_TYPE_ADD).Visible = False
      .Button(BUTTON_TYPE_DELETE).Visible = False

      .IsSortable = False

      '  Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = 100
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Site
      If m_is_center Then
        .Column(GRID_COLUMN_SITE).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' ID Site
        .Column(GRID_COLUMN_SITE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
        .Column(GRID_COLUMN_SITE).Width = 1400
      End If

      ' Jackpot Type Index
      .Column(GRID_COLUMN_JACKPOT_TYPE_INDEX).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652) ' Jackpot 
      .Column(GRID_COLUMN_JACKPOT_TYPE_INDEX).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623) ' Level
      .Column(GRID_COLUMN_JACKPOT_TYPE_INDEX).Width = 600
      .Column(GRID_COLUMN_JACKPOT_TYPE_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Jackpot Type
      .Column(GRID_COLUMN_JACKPOT_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5652) ' Jackpot 
      .Column(GRID_COLUMN_JACKPOT_TYPE).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282) ' Name
      .Column(GRID_COLUMN_JACKPOT_TYPE).Width = 1600
      .Column(GRID_COLUMN_JACKPOT_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date Awarded
      .Column(GRID_COLUMN_DATE_AWARDED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2618) ' Prize 
      .Column(GRID_COLUMN_DATE_AWARDED).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(203) ' Date awarded
      .Column(GRID_COLUMN_DATE_AWARDED).Width = 2000
      .Column(GRID_COLUMN_DATE_AWARDED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Amount
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2618) ' Prize 
      .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(211) ' Amount
      .Column(GRID_COLUMN_AMOUNT).Width = 1800
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2618) ' Prize 
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(215) ' Status
      .Column(GRID_COLUMN_STATUS).Width = 1600
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)  ' Terminal
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      ' Account Id
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)  ' Account
      .Column(GRID_COLUMN_ACCOUNT_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795) ' Number
      .Column(GRID_COLUMN_ACCOUNT_ID).Width = 1020
      .Column(GRID_COLUMN_ACCOUNT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Holder Name
      .Column(GRID_COLUMN_HOLDER_NAME).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(230)  ' Account
      .Column(GRID_COLUMN_HOLDER_NAME).Header(1).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282) ' Name
      .Column(GRID_COLUMN_HOLDER_NAME).Width = 3000
      .Column(GRID_COLUMN_HOLDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere(ByVal ExcludeInactivity As Boolean) As String

    Dim sql_where As String
    Dim isfirst As Boolean = True


    sql_where = " WHERE SJI_INDEX IN ( "

    If Me.chk_jackpot1.Checked Then
      sql_where = sql_where & " " & chk_jackpot1.Tag
    End If

    If Me.chk_jackpot2.Checked Then
      If Me.chk_jackpot1.Checked Then
        sql_where = sql_where & " , " & chk_jackpot2.Tag
      Else
        sql_where = sql_where & "   " & chk_jackpot2.Tag
      End If
    End If

    If Me.chk_jackpot3.Checked Then
      If Me.chk_jackpot1.Checked Or Me.chk_jackpot2.Checked Then
        sql_where = sql_where & " , " & chk_jackpot3.Tag
      Else
        sql_where = sql_where & "   " & chk_jackpot3.Tag
      End If
    End If

    If Not Me.chk_jackpot1.Checked And Not Me.chk_jackpot2.Checked And Not Me.chk_jackpot3.Checked Then
      sql_where = sql_where & " " & chk_jackpot1.Tag & ", " & chk_jackpot2.Tag & ", " & chk_jackpot3.Tag
    End If

    sql_where = sql_where & " )"

    If m_is_center Then

      If Not String.IsNullOrEmpty(Me.uc_sites_sel.GetSitesIdListSelectedAll()) Then
        sql_where = sql_where & " AND ST_SITE_ID IN (" & Me.uc_sites_sel.GetSitesIdListSelectedAll() & ")"


      End If

    End If

    If ExcludeInactivity Then
      sql_where = sql_where & "  AND   NOT HP_SITE_JACKPOT_INDEX IS NULL "
    End If

    Return sql_where
  End Function ' GetSqlWhere

  ' PURPOSE: Re index columns grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    If Not m_is_center Then
      GRID_COLUMN_INDEX = 0
      GRID_COLUMN_JACKPOT_TYPE_INDEX = 1
      GRID_COLUMN_JACKPOT_TYPE = 2
      GRID_COLUMN_DATE_AWARDED = 3
      GRID_COLUMN_AMOUNT = 4
      GRID_COLUMN_STATUS = 5
      GRID_INIT_TERMINAL_DATA = 6
      GRID_COLUMN_ACCOUNT_ID = TERMINAL_DATA_COLUMNS + 6
      GRID_COLUMN_HOLDER_NAME = TERMINAL_DATA_COLUMNS + 7

      GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 8
    Else
      GRID_COLUMN_INDEX = 0
      GRID_COLUMN_SITE = 1
      GRID_COLUMN_JACKPOT_TYPE_INDEX = 2
      GRID_COLUMN_JACKPOT_TYPE = 3
      GRID_COLUMN_DATE_AWARDED = 4
      GRID_COLUMN_AMOUNT = 5
      GRID_COLUMN_STATUS = 6
      GRID_INIT_TERMINAL_DATA = 7
      GRID_COLUMN_ACCOUNT_ID = TERMINAL_DATA_COLUMNS + 7
      GRID_COLUMN_HOLDER_NAME = TERMINAL_DATA_COLUMNS + 8

      GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 9
    End If

  End Sub ' GridColumnReIndex

  ' PURPOSE: Write Subtotals Amounts in the Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub DrawSubTotal(ByVal SubtotalType As ENUM_SUBTOTAL_TYPE)
    Dim _idx_row As Integer
    Dim _str_total As String
    Dim _amount As Decimal
    Dim _color As ENUM_GUI_COLOR
    Dim _idx_col_text As Integer

    _str_total = String.Empty
    _idx_col_text = 0

    Select Case SubtotalType
      Case ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL
        If String.IsNullOrEmpty(m_subtotal_jackpot_type_id) Then
          Exit Sub
        End If
        Select Case m_subtotal_jackpot_type_id
          Case chk_jackpot1.Tag
            _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "1") ' Level 1
          Case chk_jackpot2.Tag
            _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "2") ' Level 2
          Case chk_jackpot3.Tag
            _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "3") ' Level 3
        End Select

        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1280) & _str_total & ":"
        _color = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
        _amount = m_subtotal_amount_level
        _idx_col_text = GRID_COLUMN_JACKPOT_TYPE

      Case ENUM_SUBTOTAL_TYPE.SUBTOTAL_SITE

        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2282, String.Format("{0:000}", Integer.Parse(m_site_id)))
        _color = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00
        _amount = m_total_amount_site
        _idx_col_text = GRID_COLUMN_SITE

      Case ENUM_SUBTOTAL_TYPE.TOTAL_LEVEL

        Select Case m_subtotal_jackpot_type_id
          Case chk_jackpot1.Tag
            _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "1")
            _amount = m_total_amount_level_1

          Case chk_jackpot2.Tag
            _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "2")
            _amount = m_total_amount_level_2

          Case chk_jackpot3.Tag
            _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2623, "3")
            _amount = m_total_amount_level_3

        End Select

        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838) & " " & _str_total & ":"
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        _idx_col_text = GRID_COLUMN_JACKPOT_TYPE

      Case ENUM_SUBTOTAL_TYPE.TOTAL
        _str_total = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2838).ToUpper & ":"
        _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        _amount = m_total_amount
        If m_is_center Then
          _idx_col_text = GRID_COLUMN_SITE
        Else
          _idx_col_text = GRID_COLUMN_JACKPOT_TYPE
        End If

    End Select

    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    If m_is_center And (SubtotalType = ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL Or SubtotalType = ENUM_SUBTOTAL_TYPE.SUBTOTAL_SITE) Then
      Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE).Value = String.Format("{0:000}", Integer.Parse(m_site_id))
    End If

    Me.Grid.Cell(_idx_row, _idx_col_text).Value = _str_total
        'SDS 22-10-2015 format whith Currency symbol & Type
        Me.Grid.Cell(_idx_row, GRID_COLUMN_AMOUNT).Value = SelectTypeCurrency(m_is_center, _amount)

    Me.Grid.Row(_idx_row).BackColor = GetColor(_color)
  End Sub ' DrawSubTotal

  ' PURPOSE: Reset Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetCounters(ByVal SubtotalType As ENUM_SUBTOTAL_TYPE)

    Select Case SubtotalType
      Case ENUM_SUBTOTAL_TYPE.SUBTOTAL_LEVEL
        m_subtotal_amount_level = 0
        m_subtotal_jackpot_type_id = String.Empty
        m_subtotal_jackpot_type = String.Empty

      Case ENUM_SUBTOTAL_TYPE.SUBTOTAL_SITE
        m_subtotal_amount_level = 0
        m_subtotal_jackpot_type_id = String.Empty
        m_subtotal_jackpot_type = String.Empty
        m_total_amount_site = 0
        m_site_id = String.Empty

      Case ENUM_SUBTOTAL_TYPE.TOTAL_LEVEL
      Case ENUM_SUBTOTAL_TYPE.TOTAL
        m_subtotal_amount_level = 0
        m_subtotal_jackpot_type_id = String.Empty
        m_subtotal_jackpot_type = String.Empty
        m_total_amount_site = 0
        m_site_id = String.Empty
        m_total_amount_level_1 = 0
        m_total_amount_level_2 = 0
        m_total_amount_level_3 = 0
        m_total_amount = 0

    End Select

    End Sub 'ResetCounters
    ' PURPOSE: Select by center multicurrency or formatcurrency
    '
    '  PARAMS:
    '     - INPUT:
    '           - IsCenter
    '           -Value
    '     - OUTPUT:
    '           - none
    '
    ' RETURNS:
    '     - String Currency
    'SDS 28-10-2015 
    Private Function SelectTypeCurrency(ByVal IsCenter As Boolean, ByVal Value As Double) As String
        If IsCenter = True Then
            Return GUI_MultiCurrencyValue_Format(Value)
        Else
            Return GUI_FormatCurrency(Value, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
        End If
    End Function

  ' PURPOSE: Get all the columns that apply currency format
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetColumnsToChangeCurrency() As ArrayList
    Dim _columns_change_currency As ArrayList
    _columns_change_currency = New ArrayList
    _columns_change_currency.Add(SQL_COLUMN_AMOUNT)


    Return _columns_change_currency
  End Function ' GetColumnsToChangeCurrency

  ' PURPOSE: Initialize controls when form show multicurrency
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitializeMultiCurrencyControls()
    ' controls
    Me.uc_sites_sel.Visible = False
    Me.uc_multi_currency_sel.Visible = True
    Me.uc_multi_currency_sel.DateFrom = Me.uc_dsl.FromDate
    Me.uc_multi_currency_sel.DateTo = Me.uc_dsl.ToDate
    Me.uc_multi_currency_sel.Init()
    Me.uc_multi_currency_sel.Location = New Point(8, 5)
    Me.uc_multi_currency_sel.Width = 482
    Me.uc_multi_currency_sel.Height = 170
    Me.panel_filter.Height = 175
    Me.uc_dsl.Location = New Point(Me.uc_multi_currency_sel.Location.X + Me.uc_multi_currency_sel.Width + 10, 8)
    Me.gb_jackpot_type.Width = Me.gb_jackpot_type.Width - 50
    Me.gb_jackpot_type.Location = New Point(Me.uc_dsl.Location.X + Me.uc_dsl.Width + 10, 8)
    Me.gb_options.Height = Me.gb_options.Height - 25
    Me.gb_options.Location = New Point(Me.gb_jackpot_type.Location.X + Me.gb_jackpot_type.Width + 10, 8)
    ' properties
    Me.MultiSiteCurrencies = uc_multi_currency_sel.CurrenciesTable()
    Me.ColumnIsoCode = MULTI_CURRENCY_COLUMN_ISO_CODE
    Me.ColumnsChangeCurrency = GetColumnsToChangeCurrency()

    Call AddHandlers()
  End Sub ' InitializeMultiCurrencyControls

  ' PURPOSE: Initialize controls when form show multisite, not multicurrency
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitializeMultiSiteControls()
    Me.uc_multi_currency_sel.Visible = False
    Me.uc_sites_sel.ShowMultisiteRow = False
    Me.uc_sites_sel.Init()
    Me.uc_sites_sel.SetDefaultValues(False)
    Me.panel_filter.Height = 172
    Me.uc_sites_sel.Location = New Point(7, 5)
    Me.uc_dsl.Location = New Point(Me.uc_sites_sel.Location.X + Me.uc_sites_sel.Width + 10, 8)
    Me.gb_jackpot_type.Width = Me.gb_jackpot_type.Width - 50
    Me.gb_jackpot_type.Location = New Point(Me.uc_dsl.Location.X + Me.uc_dsl.Width + 10, 8)
    Me.gb_options.Height = Me.gb_options.Height - 25
    Me.gb_options.Location = New Point(Me.gb_jackpot_type.Location.X + Me.gb_jackpot_type.Width + 10, 8)
  End Sub ' InitializeMultiSiteControls

  ' PURPOSE: Sent the date to find the average
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UpdateSiteCurrenciesList()

    If uc_dsl.FromDateSelected Then
      Me.uc_multi_currency_sel.DateFrom = GUI_FormatDate(Me.uc_dsl.FromDate)
    Else
      Me.uc_multi_currency_sel.DateFrom = GUI_FormatDate(New Date(2007, 1, 1)) ' min date value
    End If

    If uc_dsl.ToDateSelected Then
      Me.uc_multi_currency_sel.DateTo = GUI_FormatDate(Me.uc_dsl.ToDate)
    Else
      Me.uc_multi_currency_sel.DateTo = GUI_FormatDate(WGDB.Now)
    End If

  End Sub ' UpdateSiteCurrenciesList

  ' PURPOSE: Print the exchange average in the excel sheet
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PrintExchangeCurrency(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _is_multi_currency As Boolean

    _is_multi_currency = (Not String.IsNullOrEmpty(m_currency_option) AndAlso m_currency_option.Equals("True"))

    If _is_multi_currency Then
      Dim _exchange_currency As String
      Dim _exchange_currency_list As String()
      _exchange_currency_list = Me.uc_multi_currency_sel.GetAverageCurrencyList.Split(New Char() {";"c})

      For Each _exchange_currency In _exchange_currency_list
        PrintData.SetFilter(" ", _exchange_currency)
      Next

    End If

  End Sub ' PrintExchangeCurrency


#End Region ' Private Functions

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub ' chk_terminal_location_CheckedChanged

  Private Sub chk_level_totals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_level_totals.CheckedChanged

    If chk_level_totals.Checked Then
      If chk_exclude_inactivity.Checked Then
        m_grouped_type = ENUM_GROUPED_TYPE.DETAILED_EXCLUDE_INACTIVITY
      Else
        m_grouped_type = ENUM_GROUPED_TYPE.DETAILED
      End If
      chk_terminal_location.Enabled = True
    Else
      If chk_exclude_inactivity.Checked Then
        m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS_EXCLUDE_INACTIVITY
      Else
        m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS
      End If
    End If

  End Sub ' chk_level_totals_CheckedChanged

  Private Sub chk_exclude_inactivity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_exclude_inactivity.CheckedChanged

    If chk_exclude_inactivity.Checked Then
      If chk_level_totals.Checked Then
        m_grouped_type = ENUM_GROUPED_TYPE.DETAILED_EXCLUDE_INACTIVITY
      Else
        m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS_EXCLUDE_INACTIVITY
      End If
    Else
      If chk_level_totals.Checked Then
        m_grouped_type = ENUM_GROUPED_TYPE.DETAILED
      Else
        m_grouped_type = ENUM_GROUPED_TYPE.ONLY_TOTALS
      End If
    End If

  End Sub ' chk_exclude_inactivity_CheckedChanged



  Private Sub AddHandlers()
    AddHandler Me.uc_dsl.DatePickerCloseUp, AddressOf UpdateSiteCurrenciesList
    AddHandler Me.uc_dsl.DatePickerValueChanged, AddressOf UpdateSiteCurrenciesList
  End Sub ' AddHandlers

#End Region

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub
End Class ' frm_site_jackpot_history
