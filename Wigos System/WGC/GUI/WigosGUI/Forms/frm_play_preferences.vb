'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_play_preferences
' DESCRIPTION:   
' AUTHOR:        Luis Ernesto Mesa  
' CREATION DATE: 24-DEC-2012
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-DEC-2012  LEM    Initial version.
' 12-FEB-2013  JMM    uc_account_sel.ValidateFormat added on FilterCheck.
' 15-MAR-2013  HBB    Fixed Bug #643: Take in account closing time.
' 18-MAR-2013  HBB    Fixed Bug #648: Take in account multi-denomination.
' 19-MAR-2013  ICS    Fixed Bug #650: Player info not appear in Excel file.
' 15-APR-2013  LEM    Correct the Excel sheets title and name.
' 19-NOV-2013  LJM    Changes for auditing forms.
' 04-DEC-2013  LJM	  Changes to use the base function to fulfill tabs with grids.
' 29-MAY-2014  JAB    Added permissions: To buttons "Excel" and "Print".
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 06-JAN-2016  SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
'--------------------------------------------------------------------

#Region " Imports "

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Reports.PrintDataset
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

#End Region

Public Class frm_play_preferences
  Inherits frm_base_sel

#Region " Structures "

  Public Structure PLAY_PREFERENCES_TOTALS
    Dim opening As Date
    Dim cash_in As Decimal
    Dim played As Decimal
    Dim won As Decimal
    Dim cash_out As Decimal
    Dim minutes As Integer
    Dim sessions As Integer
  End Structure

#End Region ' Structures

#Region " Enums "

  Enum GRID_COLUMNS
    INDEX = 0
    OPENING = 1
    PROVIDER_NAME = 2
    TERMINAL_NAME = 3
    GAME = 4
    DENOMINATION = 5
    CASH_IN = 6
    COIN_IN = 7
    COIN_OUT = 8
    CASH_OUT = 9
    TIME = 10
    COUNT = 11
  End Enum

  Enum SQL_COLUMNS
    CASH_IN = 0
    COIN_IN = 1
    COIN_OUT = 2
    CASH_OUT = 3
    TIME = 4
    COUNT = 5
    PROVIDER_NAME = 6
    TERMINAL_NAME = 7
    GAME = 8
    DENOMINATION = 9
    MULTI_DENOMINATION = 10
    OPENING = 11
  End Enum

  Enum ACCOUNT_DATA_ROW
    ID = 0
    CARD = 1
    NAME = 2
    LEVEL = 3
    POINTS = 4
    LAST_ACTIVITY = 5
    BIRTH_DATE = 6
    GENDER = 7
    USERTYPE = 8
    HOLDERLEVELENTERED = 9
    ACCOUNTTYPE = 10
  End Enum

  Enum TABS
    TERMINAL = 0
    DETALLE = 1
  End Enum

  Enum TAB_CONTROLS
    GRID = 0
    LABEL = 1
  End Enum

#End Region 'Enums

#Region " Members "

  Private m_print_datetime As Date
  Private m_date_from As String
  Private m_date_to As String
  Private m_id_account As String
  Private m_card_account As String
  Private m_holder_name As String
  Private m_totals As PLAY_PREFERENCES_TOTALS
  Private m_subtotals As PLAY_PREFERENCES_TOTALS

  Private m_terminal_str As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1743)
  Private m_detail_str As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1744)

#End Region 'Members

#Region " Constants "

  Private Const GRID_COLUMNS_COUNT As Integer = 12
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const INDEX_COLUMN_WIDTH As Integer = 200
  Private Const OPENING_COLUMN_WIDTH As Integer = 1200
  Private Const TERMINAL_NAME_COLUMN_WIDTH As Integer = 2000
  Private Const PROVIDER_NAME_COLUMN_WIDTH As Integer = 2000
  Private Const GAME_NAME_COLUMN_WIDTH As Integer = 2000
  Private Const DENOMINATION_COLUMN_WIDTH As Integer = 1000
  Private Const CASH_IN_COLUMN_WIDTH As Integer = 1400
  Private Const COIN_IN_COLUMN_WIDTH As Integer = 1400
  Private Const _COLUMN_WIDTH As Integer = 1400
  Private Const COIN_OUT_COLUMN_WIDTH As Integer = 1400
  Private Const CASH_OUT_COLUMN_WIDTH As Integer = 1400
  Private Const TIME_COLUMN_WIDTH As Integer = 1400
  Private Const COUNT_COLUMN_WIDTH As Integer = 1100

#End Region 'Constants

#Region " Overrides "

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  '
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected _
       And Me.ds_from_to.FromDate >= Me.ds_from_to.ToDate Then

      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.ds_from_to.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.uc_account_filter.Account) And String.IsNullOrEmpty(Me.uc_account_filter.TrackData) Then
      Call NLS_MsgBox(GLB_NLS_GUI_STATISTICS.Id(136), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.uc_account_filter.Focus()

      Return False
    End If

    ' Check fields on uc_account_sel are ok
    If Not Me.uc_account_filter.ValidateFormat() Then

      Return False
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PLAY_PREFERENCES
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Dim _print_data As New GUI_Reports.CLASS_PRINT_DATA

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_FILTER_APPLY

        Call ExecuteQueryCustom()

        GUI_ReportFilter(_print_data)
        Call MyBase.AuditFormClick(ButtonId, _print_data)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(499)                     ' Play Preferences

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10) ' Exit
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = False

    Me.gb_account_data.Text = GLB_NLS_GUI_STATISTICS.GetString(207)                     ' Account
    Me.ef_account_id.Text = GLB_NLS_GUI_STATISTICS.GetString(208)                       ' Number
    Me.ef_account_card.Text = GLB_NLS_GUI_STATISTICS.GetString(209)                     ' Card
    Me.ef_account_name.Text = GLB_NLS_GUI_STATISTICS.GetString(210)                     ' Holder
    Me.ef_account_level.Text = GLB_NLS_GUI_CLASS_II.GetString(414)                      ' Level
    Me.ef_account_points.Text = GLB_NLS_GUI_CLASS_II.GetString(415)                     ' Points
    Me.ef_account_activity.Text = GLB_NLS_GUI_INVOICING.GetString(467)                  ' Last Activity
    Me.ef_account_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723)             ' Gender
    Me.ef_holder_level_enter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1781)         ' Level Enter
    Me.ef_account_age.Text = GLB_NLS_GUI_INVOICING.GetString(415)                       ' Age

    'set filters
    Me.ef_account_points.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER)
    Me.ef_account_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER)
    Me.ef_account_age.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER)

    ' It looks like redundant code, but it's necessary.
    Me.ef_account_id.IsReadOnly = True
    Me.ef_account_card.IsReadOnly = True
    Me.ef_account_name.IsReadOnly = True
    Me.ef_account_level.IsReadOnly = True
    Me.ef_account_points.IsReadOnly = True
    Me.ef_account_activity.IsReadOnly = True
    Me.ef_account_gender.IsReadOnly = True
    Me.ef_holder_level_enter.IsReadOnly = True
    Me.ef_account_age.IsReadOnly = True

    Me.ds_from_to.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(830), True)

    Call InitTabPages()

    Call SetDefaultValues()

    Me.uc_account_filter.ShowVipClients = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

#End Region 'Overrides

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257).ToLower(), m_date_from) ' Date from
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258).ToLower(), m_date_to)   ' Date to
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_id_account)                                                   ' Account 
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_card_account)                                                 ' Card
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_holder_name)                                                  ' Titular
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(415), ef_account_age.Value)                                           ' Age
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1723), ef_account_gender.Value)                                 ' Gender
    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(414), ef_account_level.Value)                                          ' Level
    PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(415), ef_account_points.Value)                                         ' Points
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467), ef_account_activity.Value)                                      ' Activity
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1781), ef_holder_level_enter.Value)                             ' Level enter

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_id_account = ""
    m_card_account = ""

    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    m_id_account = ef_account_id.Value
    m_card_account = ef_account_card.Value
    m_holder_name = ef_account_name.Value

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try

      _print_data = New GUI_Reports.CLASS_PRINT_DATA()
      Call GUI_ReportFilter(_print_data)

      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime

        For Each _tab_page As TabPage In tab_preferences.TabPages
          _sheet_layout = New ExcelReportLayout.SheetLayout()
          _sheet_layout.Title = Me.FormTitle
          _sheet_layout.Name = _tab_page.Controls(TAB_CONTROLS.LABEL).Text
          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.Title = _tab_page.Controls(TAB_CONTROLS.LABEL).Text
          _uc_grid_def.UcGrid = _tab_page.Controls(TAB_CONTROLS.GRID)
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
          .ListOfSheets.Add(_sheet_layout)
        Next

      End With

      Call _report_layout.GUI_GenerateExcel()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try
  End Sub ' One grid per sheet

#End Region 'GUI Reports

#Region " Publics "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, _
                          Optional ByVal IdAccount As Integer = 0, _
                          Optional ByVal InitialDate As Date = Nothing, _
                          Optional ByVal InitialDateSelected As Boolean = True, _
                          Optional ByVal FinalDate As Date = Nothing, _
                          Optional ByVal FinalDateSelected As Boolean = True, _
                          Optional ByVal ShowTab As TABS = TABS.TERMINAL)

    Me.MdiParent = MdiParent
    Me.Display(False)

    If Me.Permissions.Read Then
      If SetInitialValues(IdAccount, InitialDate, InitialDateSelected, FinalDate, FinalDateSelected, ShowTab) = True Then
        Call ExecuteQueryCustom()
      End If
    End If

  End Sub ' ShowForEdit

#End Region 'Publics

#Region " Privates "

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Private Function GetSqlCommand() As SqlCommand
    Dim _cmd As SqlCommand
    Dim _min_date As Date
    Dim _max_date As Date

    _min_date = New DateTime(2007, 1, 1)
    _max_date = New DateTime(Now.Year + 10, 1, 1)

    _cmd = New SqlCommand(SqlQuery.CommandText("PlayPreferences"))

    _cmd.Parameters.Add("@pRefOpening", SqlDbType.DateTime).Value = WSI.Common.Misc.Opening(_min_date)
    _cmd.Parameters.Add("@pMultigame", SqlDbType.NVarChar).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(634)      ' Multigame

    If ds_from_to.FromDateSelected Then
      _cmd.Parameters.Add("@pFechaIni", SqlDbType.DateTime).Value = ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime)
    Else
      _cmd.Parameters.Add("@pFechaIni", SqlDbType.DateTime).Value = _min_date
    End If

    If ds_from_to.ToDateSelected Then
      _cmd.Parameters.Add("@pFechaFin", SqlDbType.DateTime).Value = ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime)
    Else
      _cmd.Parameters.Add("@pFechaFin", SqlDbType.DateTime).Value = _max_date
    End If

    _cmd.Parameters.Add("@pSQLaccount", SqlDbType.VarChar).Value = _
           " SELECT   AC_ACCOUNT_ID                                                                                   " & _
           "        , AC_TRACK_DATA                                                                                   " & _
           "        , AC_HOLDER_NAME                                                                                  " & _
           "        , (SELECT   GP_KEY_VALUE                                                                          " & _
           "             FROM   GENERAL_PARAMS                                                                        " & _
           "            WHERE   GP_GROUP_KEY   = 'PlayerTracking'                                                     " & _
           "              AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name'" & _
           "          ) AS AC_LEVEL                                                                                   " & _
           "        , DBO.GETBUCKETVALUE(" & Buckets.BucketType.RedemptionPoints & ",AC_ACCOUNT_ID) AS CBU_VALUE           " & _
           "        , AC_LAST_ACTIVITY                                                                                " & _
           "        , AC_HOLDER_BIRTH_DATE                                                                            " & _
           "        , AC_HOLDER_GENDER                                                                                " & _
           "        , AC_USER_TYPE                                                                                    " & _
           "        , AC_HOLDER_LEVEL_ENTERED                                                                         " & _
           "        , AC_TYPE                                                                                         " & _
           "   FROM   ACCOUNTS " & _
           "  WHERE   " & uc_account_filter.GetFilterSQL()

    Return _cmd
  End Function ' GetSqlCommand

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Private Function SetupRow(ByVal TabIndex As TABS, _
                            ByVal DtRow As DataRow) As Boolean
    Dim _row As CLASS_DB_ROW
    Dim _row_idx As Integer
    Dim _grid As uc_grid

    _grid = tab_preferences.TabPages(TabIndex).Controls(TAB_CONTROLS.GRID)

    _row_idx = _grid.AddRow()
    _row = New CLASS_DB_ROW(DtRow)

    ' date 
    If Not _row.IsNull(SQL_COLUMNS.OPENING) And TabIndex = TABS.DETALLE Then
      _grid.Cell(_row_idx, GRID_COLUMNS.OPENING).Value = GUI_FormatDate(_row.Value(SQL_COLUMNS.OPENING), _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    End If

    ' minutes 
    If Not _row.IsNull(SQL_COLUMNS.TIME) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.TIME).Value = GUI_FormatNumber(_row.Value(SQL_COLUMNS.TIME), 0)
    End If

    ' sessions 
    If Not _row.IsNull(SQL_COLUMNS.COUNT) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.COUNT).Value = GUI_FormatNumber(_row.Value(SQL_COLUMNS.COUNT), 0)
    End If

    ' provider
    If Not _row.IsNull(SQL_COLUMNS.PROVIDER_NAME) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.PROVIDER_NAME).Value = _row.Value(SQL_COLUMNS.PROVIDER_NAME)
    End If

    ' terminal
    If Not _row.IsNull(SQL_COLUMNS.TERMINAL_NAME) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.TERMINAL_NAME).Value = _row.Value(SQL_COLUMNS.TERMINAL_NAME)
    End If

    ' game
    If Not _row.IsNull(SQL_COLUMNS.GAME) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.GAME).Value = _row.Value(SQL_COLUMNS.GAME)
    End If

    ' denomination & multi_denomination
    If Not _row.IsNull(SQL_COLUMNS.DENOMINATION) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.DENOMINATION).Value = GUI_FormatNumber(_row.Value(SQL_COLUMNS.DENOMINATION), 2)

    ElseIf Not _row.IsNull(SQL_COLUMNS.MULTI_DENOMINATION) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.DENOMINATION).Value = _row.Value(SQL_COLUMNS.MULTI_DENOMINATION)

    End If

    ' cash in
    If Not _row.IsNull(SQL_COLUMNS.CASH_IN) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.CASH_IN).Value = GUI_FormatCurrency(_row.Value(SQL_COLUMNS.CASH_IN))
    End If

    ' coin in
    If Not _row.IsNull(SQL_COLUMNS.COIN_IN) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.COIN_IN).Value = GUI_FormatCurrency(_row.Value(SQL_COLUMNS.COIN_IN))
    End If

    ' coin out
    If Not _row.IsNull(SQL_COLUMNS.COIN_OUT) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.COIN_OUT).Value = GUI_FormatCurrency(_row.Value(SQL_COLUMNS.COIN_OUT))
    End If

    ' cash out
    If Not _row.IsNull(SQL_COLUMNS.CASH_OUT) Then
      _grid.Cell(_row_idx, GRID_COLUMNS.CASH_OUT).Value = GUI_FormatCurrency(_row.Value(SQL_COLUMNS.CASH_OUT))
    End If

    Return True

  End Function ' SetupRow

  ' PURPOSE : Sets the values of a total row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Private Function SetupTotalRow(ByVal TabIndex As TABS) As Boolean
    Dim _first_col As Integer
    Dim _row_idx As Integer
    Dim _grid As uc_grid

    _grid = tab_preferences.TabPages(TabIndex).Controls(TAB_CONTROLS.GRID)

    _row_idx = _grid.AddRow()

    If TabIndex = TABS.TERMINAL Then
      _first_col = GRID_COLUMNS.TERMINAL_NAME
    End If
    If TabIndex = TABS.DETALLE Then
      _first_col = GRID_COLUMNS.OPENING
    End If

    Call SetTotalsOnGrid(_row_idx, _grid, m_totals)
    Call ClearTotals(m_subtotals)
    Call ClearTotals(m_totals)

    _grid.Cell(_row_idx, _first_col).Value = GLB_NLS_GUI_INVOICING.GetString(205)
    _grid.Row(_row_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    Return True
  End Function ' SetupTotalRow

  ' PURPOSE : Sets the values of a subtotal row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Private Function SetupSubTotalRow(ByVal IndexTable As TABS) As Boolean
    Dim _first_col As Integer
    Dim _row_idx As Integer
    Dim _grid As uc_grid

    _grid = tab_preferences.TabPages(IndexTable).Controls(TAB_CONTROLS.GRID)

    _row_idx = _grid.AddRow()

    If IndexTable = TABS.TERMINAL Then
      _first_col = GRID_COLUMNS.TERMINAL_NAME
    End If
    If IndexTable = TABS.DETALLE Then
      _first_col = GRID_COLUMNS.OPENING
    End If

    Call SetTotalsOnGrid(_row_idx, _grid, m_subtotals)
    Call ClearTotals(m_subtotals)

    _grid.Cell(_row_idx, _first_col).Value = GLB_NLS_GUI_INVOICING.GetString(278)
    _grid.Row(_row_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Return True
  End Function ' SetupSubTotalRow

  ' PURPOSE: Execute sql query to retrieve data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS: TRUE ()
  Private Function LoadPlaySessionsFromDB() As Boolean
    Dim _ds As DataSet
    Dim _idf As Integer

    _ds = New DataSet()

    Using _db_trx As New DB_TRX()
      Using _da As New SqlDataAdapter()
        _da.SelectCommand = GetSqlCommand()
        _db_trx.Fill(_da, _ds)
      End Using
    End Using

    Call ClearGrids()
    Call ClearAccountFields()

    If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then

      Call SaveAccountData(_ds.Tables(0))

      For _idf = 1 To _ds.Tables.Count - 1
        Call TableToGrid(_idf - 1, _ds.Tables(_idf))
      Next

      Return True
    End If

    Return False
  End Function ' LoadPlaySessionsFromDB

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    Me.ds_from_to.ToDate = _today_opening
    Me.ds_from_to.ToDateSelected = True
    Me.ds_from_to.FromDate = _today_opening.AddMonths(-1)

    Me.ds_from_to.ClosingTime = _today_opening.Hour
    Me.uc_account_filter.Clear()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get default values from external call
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function SetInitialValues(ByVal IdAccount As Integer, ByVal InitialDate As Date, ByVal InitialDateSelected As Boolean, ByVal FinalDate As Date, ByVal FinalDateSelected As Boolean, ByVal ShowTab As TABS) As Boolean
    Dim _do_search As Boolean

    _do_search = False

    If IdAccount > 0 Then
      Me.m_id_account = Convert.ToString(IdAccount)
      Me.uc_account_filter.Account = IdAccount
      _do_search = True
    End If

    If InitialDate <> Nothing Then
      Me.m_date_from = GUI_FormatDate(InitialDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.ds_from_to.FromDate = InitialDate
      _do_search = True
    End If

    If FinalDate <> Nothing Then
      Me.m_date_to = GUI_FormatDate(FinalDate, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      Me.ds_from_to.ToDate = FinalDate
      Me.ds_from_to.ToDateSelected = FinalDateSelected
      _do_search = True
    End If

    If ShowTab >= 0 And ShowTab < tab_preferences.TabPages.Count Then
      tab_preferences.SelectTab(ShowTab)
    End If

    Return _do_search
  End Function ' GetDefaultValues

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GridStyleSheet(ByVal TabIndex As TABS)
    Dim _tab_grid As uc_grid

    _tab_grid = tab_preferences.TabPages(TabIndex).Controls(TAB_CONTROLS.GRID)

    With _tab_grid

      .Visible = False

      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)

      ' Index
      .Column(GRID_COLUMNS.INDEX).Header(0).Text = String.Empty
      .Column(GRID_COLUMNS.INDEX).IsColumnPrintable = False
      .Column(GRID_COLUMNS.INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMNS.INDEX).Width = INDEX_COLUMN_WIDTH

      ' Date
      If TabIndex = TABS.DETALLE Then
        .Column(GRID_COLUMNS.OPENING).Width = OPENING_COLUMN_WIDTH
      Else
        .Column(GRID_COLUMNS.OPENING).Width = 0
      End If

      ' Terminal
      If TabIndex = TABS.TERMINAL Or TabIndex = TABS.DETALLE Then
        .Column(GRID_COLUMNS.TERMINAL_NAME).Width = TERMINAL_NAME_COLUMN_WIDTH
        .Column(GRID_COLUMNS.PROVIDER_NAME).Width = PROVIDER_NAME_COLUMN_WIDTH
        .Column(GRID_COLUMNS.GAME).Width = GAME_NAME_COLUMN_WIDTH
        .Column(GRID_COLUMNS.DENOMINATION).Width = DENOMINATION_COLUMN_WIDTH
      Else
        .Column(GRID_COLUMNS.TERMINAL_NAME).Width = 0
        .Column(GRID_COLUMNS.PROVIDER_NAME).Width = 0
        .Column(GRID_COLUMNS.GAME).Width = 0
        .Column(GRID_COLUMNS.DENOMINATION).Width = 0
      End If

      ' date
      .Column(GRID_COLUMNS.OPENING).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(410)
      .Column(GRID_COLUMNS.OPENING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  PROVIDER Name
      .Column(GRID_COLUMNS.PROVIDER_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMNS.PROVIDER_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Terminal Name
      .Column(GRID_COLUMNS.TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COLUMNS.TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Game
      .Column(GRID_COLUMNS.GAME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(411)
      .Column(GRID_COLUMNS.GAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Denomination
      .Column(GRID_COLUMNS.DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1777)
      .Column(GRID_COLUMNS.DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cash In on Play Session
      .Column(GRID_COLUMNS.CASH_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(379)
      .Column(GRID_COLUMNS.CASH_IN).Width = CASH_IN_COLUMN_WIDTH
      .Column(GRID_COLUMNS.CASH_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Played Amount
      .Column(GRID_COLUMNS.COIN_IN).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(385)
      .Column(GRID_COLUMNS.COIN_IN).Width = COIN_IN_COLUMN_WIDTH
      .Column(GRID_COLUMNS.COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Won Amount
      .Column(GRID_COLUMNS.COIN_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(386)
      .Column(GRID_COLUMNS.COIN_OUT).Width = COIN_OUT_COLUMN_WIDTH
      .Column(GRID_COLUMNS.COIN_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Cash Out on Play Session
      .Column(GRID_COLUMNS.CASH_OUT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(380)
      .Column(GRID_COLUMNS.CASH_OUT).Width = CASH_OUT_COLUMN_WIDTH
      .Column(GRID_COLUMNS.CASH_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Time
      .Column(GRID_COLUMNS.TIME).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(500)
      .Column(GRID_COLUMNS.TIME).Width = TIME_COLUMN_WIDTH
      .Column(GRID_COLUMNS.TIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count
      .Column(GRID_COLUMNS.COUNT).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(355)
      .Column(GRID_COLUMNS.COUNT).Width = COUNT_COLUMN_WIDTH
      .Column(GRID_COLUMNS.COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Visible = True

    End With

  End Sub ' GUI_StyleSheet

  '------------------------------------------------------------------------------
  ' PURPOSE : Create TabPage and Datagrids in Tabcontrol.
  '
  '  PARAMS :
  '      - INPUT :
  '           Grid Control uc_grid
  '           TabPage 
  '           TabPageText TabPage Text
  '           TabPageName TabPage Name
  '           ValuePanelRightVisible Boolean for view the right panel
  '           Title The Label
  '           TitleText The text to the label
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  '      - 
  '   
  Private Sub CreateTabPage(ByVal TabPageText As String, _
                            ByVal TabPageName As String, _
                            ByVal ValuePanelRightVisible As Boolean, _
                            ByVal TitleText As String)

    Dim _tab_grid As uc_grid
    _tab_grid = New uc_grid()
    AddHandler _tab_grid.SetToolTipTextEvent, AddressOf uc_grid_SetToolTipTextEvent

    Call CreateDataGridInTabControl(_tab_grid, TabPageText, TabPageName, ValuePanelRightVisible, TitleText, tab_preferences)

  End Sub ' CreateTabsAndGrids

  ' PURPOSE: Set data from datatable to grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub TableToGrid(ByVal TabIndex As TABS, ByVal Dt As DataTable)

    For Each _row As DataRow In Dt.Rows
      Call ProcessTotals(TabIndex, _row)
      Call SetupRow(TabIndex, _row)
    Next

    Call ResumeTotal(TabIndex)

  End Sub ' TableToGrid

  ' PURPOSE: Initialize tabpage control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub InitTabPages()

    Call CreateTabPage(m_terminal_str, "tbp_" & m_terminal_str, True, m_terminal_str)
    Call CreateTabPage(m_detail_str, "tbp_" & m_detail_str, True, m_detail_str)

    GridStyleSheet(TABS.TERMINAL)
    GridStyleSheet(TABS.DETALLE)

  End Sub ' InitTabPages

  ' PURPOSE: execute query with filter checks and pending changes control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Protected Sub ExecuteQueryCustom()

    If GUI_FilterCheck() Then

      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      Try
        m_print_datetime = GUI_GetPrintDateTime()

        GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Enabled = False
        GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = False

        If Not LoadPlaySessionsFromDB() Then
          Call GUI_NoDataFound()
        Else
          GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Enabled = CurrentUser.Permissions(ENUM_FORM.FORM_EXCEL_ENABLED).Read
        End If

        Call GUI_ReportUpdateFilters()

        GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Enabled = True

      Finally
        Windows.Forms.Cursor.Current = Cursors.Default
      End Try

    End If

  End Sub ' ExecuteQueryCustom

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub SaveAccountData(ByVal Dt As DataTable)
    Dim _row As CLASS_DB_ROW
    Dim _track_data As String

    _row = New CLASS_DB_ROW(Dt.Rows(0))

    ef_account_id.TextValue = _row.Value(ACCOUNT_DATA_ROW.ID)

    ' Track Data
    _track_data = ""
    If Not _row.IsNull(ACCOUNT_DATA_ROW.CARD) Then
      If CardNumber.VisibleTrackData(_track_data, _row.Value(ACCOUNT_DATA_ROW.CARD), MAGNETIC_CARD_TYPES.CARD_TYPE_PLAYER, _
                                     CType(_row.Value(ACCOUNT_DATA_ROW.ACCOUNTTYPE), AccountType)) Then
        ef_account_card.TextValue = _track_data
      End If
    End If

    If Not _row.IsNull(ACCOUNT_DATA_ROW.NAME) Then
      ef_account_name.TextValue = _row.Value(ACCOUNT_DATA_ROW.NAME)
    End If
    If Not _row.IsNull(ACCOUNT_DATA_ROW.LEVEL) Then
      ef_account_level.TextValue = _row.Value(ACCOUNT_DATA_ROW.LEVEL)

      If Not _row.IsNull(ACCOUNT_DATA_ROW.POINTS) Then
        ef_account_points.TextValue = GUI_FormatNumber(Math.Truncate(_row.Value(ACCOUNT_DATA_ROW.POINTS)), 0)
      End If
    End If
    If Not _row.IsNull(ACCOUNT_DATA_ROW.LAST_ACTIVITY) Then
      ef_account_activity.TextValue = GUI_FormatDate(_row.Value(ACCOUNT_DATA_ROW.LAST_ACTIVITY), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    If Not _row.IsNull(ACCOUNT_DATA_ROW.BIRTH_DATE) Then
      ef_account_age.TextValue = WSI.Common.Misc.CalculateAge(_row.Value(ACCOUNT_DATA_ROW.BIRTH_DATE))
    End If
    If Not _row.IsNull(ACCOUNT_DATA_ROW.GENDER) Then
      Select Case _row.Value(ACCOUNT_DATA_ROW.GENDER)
        Case 1
          ef_account_gender.TextValue = GLB_NLS_GUI_INVOICING.GetString(413)
        Case 2
          ef_account_gender.TextValue = GLB_NLS_GUI_INVOICING.GetString(414)
      End Select
      If Not _row.IsNull(ACCOUNT_DATA_ROW.HOLDERLEVELENTERED) Then
        ef_holder_level_enter.TextValue = GUI_FormatDate(_row.Value(ACCOUNT_DATA_ROW.HOLDERLEVELENTERED), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
    End If

  End Sub ' SaveAccountData

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub ProcessTotals(ByVal TabIndex As TABS, ByVal DtRow As DataRow)
    Dim _totals As PLAY_PREFERENCES_TOTALS

    Call LoadTotalsFromRow(TabIndex, DtRow, _totals)

    If TabIndex = TABS.DETALLE Then
      If m_subtotals.opening <> Nothing And m_subtotals.opening <> _totals.opening Then
        SetupSubTotalRow(TabIndex)
      End If
    End If

    Call UpdateTotals(TabIndex, m_subtotals, _totals)
    Call UpdateTotals(TabIndex, m_totals, _totals)

  End Sub ' ProcessCounters

  Private Sub ResumeTotal(ByVal TabIndex As TABS)

    If TabIndex = TABS.DETALLE Then
      Call SetupSubTotalRow(TabIndex)
    End If

    SetupTotalRow(TabIndex)

  End Sub ' Resume Total

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub ClearTotals(ByRef Totals As PLAY_PREFERENCES_TOTALS)
    Totals.won = 0
    Totals.played = 0
    Totals.cash_in = 0
    Totals.minutes = 0
    Totals.cash_out = 0
    Totals.sessions = 0
    Totals.opening = Nothing
  End Sub ' ClearTotals

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub LoadTotalsFromRow(ByVal IndexTable As TABS, ByVal DtRow As DataRow, ByRef Totals As PLAY_PREFERENCES_TOTALS)
    Dim _row As CLASS_DB_ROW

    _row = New CLASS_DB_ROW(DtRow)

    If Not _row.IsNull(SQL_COLUMNS.CASH_IN) Then
      Totals.cash_in = _row.Value(SQL_COLUMNS.CASH_IN)
    End If
    If Not _row.IsNull(SQL_COLUMNS.COIN_IN) Then
      Totals.played = _row.Value(SQL_COLUMNS.COIN_IN)
    End If
    If Not _row.IsNull(SQL_COLUMNS.COIN_OUT) Then
      Totals.won = _row.Value(SQL_COLUMNS.COIN_OUT)
    End If
    If Not _row.IsNull(SQL_COLUMNS.CASH_OUT) Then
      Totals.cash_out = _row.Value(SQL_COLUMNS.CASH_OUT)
    End If
    If Not _row.IsNull(SQL_COLUMNS.TIME) Then
      Totals.minutes = _row.Value(SQL_COLUMNS.TIME)
    End If
    If Not _row.IsNull(SQL_COLUMNS.COUNT) Then
      Totals.sessions = _row.Value(SQL_COLUMNS.COUNT)
    End If
    If Not _row.IsNull(SQL_COLUMNS.OPENING) And IndexTable = TABS.DETALLE Then
      Totals.opening = _row.Value(SQL_COLUMNS.OPENING)
    End If

  End Sub ' LoadTotalsFromRow

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub SetTotalsOnGrid(ByVal IndexRow As Integer, ByRef DbGrid As uc_grid, ByVal Totals As PLAY_PREFERENCES_TOTALS)

    DbGrid.Cell(IndexRow, GRID_COLUMNS.CASH_IN).Value = GUI_FormatCurrency(Totals.cash_in)
    DbGrid.Cell(IndexRow, GRID_COLUMNS.CASH_OUT).Value = GUI_FormatCurrency(Totals.cash_out)
    DbGrid.Cell(IndexRow, GRID_COLUMNS.COIN_IN).Value = GUI_FormatCurrency(Totals.played)
    DbGrid.Cell(IndexRow, GRID_COLUMNS.COIN_OUT).Value = GUI_FormatCurrency(Totals.won)
    DbGrid.Cell(IndexRow, GRID_COLUMNS.TIME).Value = GUI_FormatNumber(Totals.minutes, 0)
    DbGrid.Cell(IndexRow, GRID_COLUMNS.COUNT).Value = GUI_FormatNumber(Totals.sessions, 0)

  End Sub ' SetTotalsOnGrid

  ' PURPOSE: 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  Private Sub UpdateTotals(ByVal TabIndex As TABS, ByRef TagTotals As PLAY_PREFERENCES_TOTALS, ByVal SrcTotals As PLAY_PREFERENCES_TOTALS)

    TagTotals.cash_in = TagTotals.cash_in + SrcTotals.cash_in
    TagTotals.played = TagTotals.played + SrcTotals.played
    TagTotals.won = TagTotals.won + SrcTotals.won
    TagTotals.cash_out = TagTotals.cash_out + SrcTotals.cash_out
    TagTotals.minutes = TagTotals.minutes + SrcTotals.minutes
    TagTotals.sessions = TagTotals.sessions + SrcTotals.sessions

    If TabIndex = TABS.DETALLE Then
      TagTotals.opening = SrcTotals.opening
    End If

  End Sub ' UpdateTotals

  Private Sub ClearGrids()
    Dim _tab_grid As uc_grid

    For Each _tab As TabPage In tab_preferences.TabPages
      _tab_grid = _tab.Controls(TAB_CONTROLS.GRID)
      _tab_grid.Clear()
    Next

  End Sub

#End Region 'Privates

  Private Sub ClearAccountFields()
    ef_account_id.Value = ""
    ef_account_level.Value = ""
    ef_account_card.Value = ""
    ef_account_points.Value = ""
    ef_account_name.Value = ""
    ef_account_activity.Value = ""
    ef_account_age.Value = ""
    ef_account_gender.Value = ""
    ef_holder_level_enter.Value = ""
  End Sub

  Private Sub uc_grid_SetToolTipTextEvent(ByVal GridRow As Integer, ByVal GridCol As Integer, ByRef Txt As String)
    Call Grid_SetToolTipText(GridRow, GridCol, Txt)
  End Sub ' dg_filter_SetToolTipTextEvent

  Protected Sub Grid_SetToolTipText(ByVal RowIndex As Integer, _
                                           ByVal ColIndex As Integer, _
                                           ByRef ToolTipTxt As String)

    If RowIndex = -3 And ColIndex = GRID_COLUMNS.DENOMINATION Then
      ToolTipTxt = GLB_NLS_GUI_CONFIGURATION.GetString(420)
    End If

  End Sub ' GUI_SetToolTipText

End Class
