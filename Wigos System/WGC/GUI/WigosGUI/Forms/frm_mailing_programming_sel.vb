'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_mailing_programming_sel
' DESCRIPTION:   Automatic Mailing Programming Selection
' AUTHOR:        Raul Cervera
' CREATION DATE: 22-DEC-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-DEC-2010  RCI    Initial version
' 04-JUN-2012  JAB    Eliminate top clause of sql query
' 29-NOV-2012  RRB    Added Terminals Without Activity mail type
' 25-APR-2013  RBG    #746 It makes no sense to work in this form with multiselection
' 22-MAY-2013  ACM    Fixed Bug #794: Filter any occurrence in description. Not just the first characters
' 05-JUN-2013  DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
' 07-OCT-2013  DLL    Added WXP SAS Meters mail type.
' 28-JUN-2016  EOR    Product Backlog Item 14627: Generic Reports: Added Mailing
' 31-OCT-2016  ESE    Bug 19552: Mailing - Generic report, validate permissions
' 31-OCT-2016  DMT    Product Backlog Item 19343:Feedback Module - GUI - Formulario de Configuraci�n
' 29-JUN-2017  DHA    PBI 28420:WIGOS-3189 Score report - Email configuration - Selection form - Add filter "Type"
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common.GenericReport
Imports WSI.Common

Public Class frm_mailing_programming_sel
  Inherits frm_base_sel

#Region " Constants "

  Private Const VALUE_ENUM_TYPE As Integer = 100

  ' DB Columns
  Private Const SQL_COLUMN_PROG_ID As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1
  Private Const SQL_COLUMN_ENABLED As Integer = 2
  Private Const SQL_COLUMN_TYPE As Integer = 3

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PROG_ID As Integer = 1
  Private Const GRID_COLUMN_NAME As Integer = 2
  Private Const GRID_COLUMN_STATUS As Integer = 3
  Private Const GRID_COLUMN_TYPE As Integer = 4

  Private Const GRID_COLUMNS As Integer = 5
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_NAME As Integer = 3800
  Private Const GRID_WIDTH_STATUS As Integer = 1500
  Private Const GRID_WIDTH_TYPE As Integer = 3590 'EOR 28-JUN-2016

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_name As String
  Private m_status As String
  Private m_report_type As WSI.Common.MAILING_PROGRAMMING_TYPE
  Private m_report_type_name As String
  Private m_with_wxp_machine_meters As Boolean
  Private m_class_mailing As New CLASS_MAILING_PROGRAMMING 'EOR 28-JUN-2016

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAILING_PROGRAMMING_SEL

    Call MyBase.GUI_SetFormId()
  End Sub

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Set form Name
    Me.Text = GLB_NLS_GUI_STATISTICS.GetString(425)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = True

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_STATISTICS.GetString(4)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    'Status
    Me.gb_prog_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2117)
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)
    Me.chk_disabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219)

    Me.ef_prog_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    Me.ef_prog_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    FillComboType()
    'General Param
    Me.m_with_wxp_machine_meters = WSI.Common.GeneralParam.GetBoolean("ExternalProtocol", "Enabled", False)

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim idx_row As Short
    Dim prog_id As Integer
    Dim prog_name As String
    Dim frm_edit As Object

    ' Search the first row selected
    For idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(idx_row).IsSelected Then
        Exit For
      End If
    Next

    If idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion ID and Name, and launch the editor
    prog_id = Me.Grid.Cell(idx_row, GRID_COLUMN_PROG_ID).Value
    prog_name = Me.Grid.Cell(idx_row, GRID_COLUMN_NAME).Value

    frm_edit = New frm_mailing_programming_edit

    Call frm_edit.ShowEditItem(prog_id, prog_name)

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim str_sql As String

    str_sql = "SELECT " & _
              "         MP_PROG_ID " & _
              "       , MP_NAME " & _
              "       , MP_ENABLED " & _
              "       , MP_TYPE " & _
              "  FROM   MAILING_PROGRAMMING "

    str_sql = str_sql & GetSqlWhere()
    str_sql = str_sql & " ORDER BY MP_PROG_ID DESC"

    Return str_sql

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Evaluate the row value before add
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_CheckOutRowBeforeAdd(DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _rtc_report_tool_id As Integer
    Dim _str_sql As String
    Dim _aux_permission As Integer
    Dim _result As Integer

    _rtc_report_tool_id = 0
    _aux_permission = 0

    'If MP_TYPE value is bigger than 100 evaluate the generic report permissions. Super user always has permissions
    If DbRow.Value(3) > 100 And Not CurrentUser.IsSuperUser Then
      _rtc_report_tool_id = DbRow.Value(3) - 100 'Obtain the report tool id to search the permissions

      Try
        'Searh generic report permissions
        Using _db_trx As New WSI.Common.DB_TRX()
          _str_sql = " SELECT GPF_EXECUTE_PERM " & _
                     " FROM GUI_PROFILE_FORMS " & _
                     " INNER JOIN REPORT_TOOL_CONFIG ON (GPF_FORM_ID = RTC_FORM_ID) " & _
                     " WHERE  GPF_PROFILE_ID = " & CurrentUser.ProfileId & _
                     "        AND GPF_GUI_ID = " & CurrentUser.GuiId & _
                     "        AND RTC_REPORT_TOOL_ID = " & _rtc_report_tool_id

          Using _cmd As New SqlClient.SqlCommand(_str_sql)
            'If hasn�t permissions ROW CAN NOT BE ADDED
            _result = _db_trx.ExecuteScalar(_cmd)
            If _result = 0 Then
              Return False
            End If
          End Using
        End Using
      Catch
        Return False
      End Try
    End If

    'ROW SHOULD BE ADDED
    Return True
  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    'Dim pm_type As Integer
    Dim str_type As String = ""

    ' Assign Mapped columns (search for 'mapping' string in this file)
    Call MyBase.GUI_SetupRow(RowIndex, DbRow)

    ' Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_NAME).Value = DbRow.Value(SQL_COLUMN_NAME)

    ' Enabled
    Select Case DbRow.Value(SQL_COLUMN_ENABLED)
      Case True
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_CONTROLS.GetString(281)

      Case False
        Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_CONTROLS.GetString(282)

    End Select

    ' Type
    Select Case DbRow.Value(SQL_COLUMN_TYPE)
      Case WSI.Common.MAILING_PROGRAMMING_TYPE.CASH_INFORMATION
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_STATISTICS.GetString(427)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.WSI_STATISTICS
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_STATISTICS.GetString(439)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_STATISTICS.GetString(440)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.ACCOUNT_MOVEMENTS
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_STATISTICS.GetString(473)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.TERMINALS_WITHOUT_ACTIVITY
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_INVOICING.GetString(361)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.WXP_MACHINE_METERS
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2692)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_TICKET_OUT_REPORT
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5805)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.MACHINE_AND_GAME_REPORT
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5820)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_HOLD_VS_WIN
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7193)
        'DMT 31-OCT-2016
      Case WSI.Common.MAILING_PROGRAMMING_TYPE.WIN_UP_FEEDBACK
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7728)

      Case WSI.Common.MAILING_PROGRAMMING_TYPE.SCORE_REPORT
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8458)

      Case Else 'EOR 28-JUN-2016
        If CInt(DbRow.Value(SQL_COLUMN_TYPE)) > 100 And CInt(DbRow.Value(SQL_COLUMN_TYPE)) < 200 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = m_class_mailing.GetTypeOfMailing(CInt(DbRow.Value(SQL_COLUMN_TYPE)) - 100)
        Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_TYPE).Value = GLB_NLS_GUI_CONTROLS.GetString(408)
        End If
    End Select

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.chk_enabled
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        Call GUI_ShowNewMailingProgrammingForm()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0
        Call GUI_ShowParameters()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(280), m_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(253), m_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(343), IIf(m_report_type = 0, "---", m_report_type_name))

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_name = ""
    m_status = ""

    ' Status
    If Me.chk_enabled.Checked And Me.chk_disabled.Checked Then
      m_status = GLB_NLS_GUI_CONTROLS.GetString(276)
    Else
      If Me.chk_enabled.Checked Then
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)
      End If
      If Me.chk_disabled.Checked Then
        m_status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5219)
      End If
    End If

    m_name = Me.ef_prog_name.Value

    m_report_type = cmb_prog_type.Value
    m_report_type_name = cmb_prog_type.TextValue

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    With Me.Grid

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 200
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Programming Id
      .Column(GRID_COLUMN_PROG_ID).Header.Text = " "
      .Column(GRID_COLUMN_PROG_ID).Width = 0
      .Column(GRID_COLUMN_PROG_ID).Mapping = SQL_COLUMN_PROG_ID

      ' Programming Name
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      .Column(GRID_COLUMN_NAME).Width = GRID_WIDTH_NAME
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '  Programming Enabled
      .Column(GRID_COLUMN_STATUS).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Programming Type
      .Column(GRID_COLUMN_TYPE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)
      .Column(GRID_COLUMN_TYPE).Width = GRID_WIDTH_TYPE
      .Column(GRID_COLUMN_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_enabled.Checked = True
    Me.chk_disabled.Checked = False

    Me.ef_prog_name.Value = ""

    Me.cmb_prog_type.SelectedIndex = 0

  End Sub ' SetDefaultValues

  Private Sub FillComboType()

    ' Type
    cmb_prog_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(343)
    cmb_prog_type.Add(0, String.Empty)
    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.CASH_INFORMATION, GLB_NLS_GUI_STATISTICS.GetString(427))
    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.WSI_STATISTICS, GLB_NLS_GUI_STATISTICS.GetString(439))

    If FeatureChips.IsGamingTablesEnabled Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7366))
    Else
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.CONNECTED_TERMINALS, GLB_NLS_GUI_STATISTICS.GetString(440))
    End If

    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.TERMINALS_WITHOUT_ACTIVITY, GLB_NLS_GUI_INVOICING.GetString(361))

    ' Don't show ticket out report if we are in TITO or General Param is 1
    If Not WSI.Common.GeneralParam.GetBoolean("WigosGUI", "HideTicketOutReport", True) AndAlso WSI.Common.TITO.Utils.IsTitoMode Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_TICKET_OUT_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5805))
    End If

    ' Don't show machine and game report if we are not in TITO or General Param is 1
    If WSI.Common.Misc.ShowMachineAndGameReport() Then
      _cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.MACHINE_AND_GAME_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(5820))
    End If

    'Temporary disabled
    ' Don't show report if we are not in TITO 
    If Not WSI.Common.GeneralParam.GetBoolean("WigosGUI", "HideTITOHoldvsWinReport", True) AndAlso Not WSI.Common.Misc.IsCashlessMode() Then
      _cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.TITO_HOLD_VS_WIN, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7193))
    End If

    cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.WIN_UP_FEEDBACK, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7728))

    If WSI.Common.GamingTableBusinessLogic.IsEnabledGTWinLoss() And WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() Then
      cmb_prog_type.Add(WSI.Common.MAILING_PROGRAMMING_TYPE.SCORE_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(8458))
    End If

    Dim _cons_request As GenericReport.ConstructorRequest
    _cons_request = New GenericReport.ConstructorRequest()

    _cons_request.ProfileID = CurrentUser.ProfileId
    _cons_request.ShowAll = CurrentUser.IsSuperUser
    _cons_request.OnlyReportsMailing = True

    _cons_request.ModeType = GetVisibleProfileForms(CurrentUser.GuiId)

    Dim _service As IReportToolConfigService
    Dim _request As GenericReport.ReportToolConfigRequest
    Dim _reponse As ReportToolConfigResponse

    _service = New ReportToolConfigService(_cons_request)
    _request = New GenericReport.ReportToolConfigRequest()
    _reponse = New ReportToolConfigResponse

    For Each _locationMenu As GenericReport.LocationMenu In [Enum].GetValues(GetType(GenericReport.LocationMenu))

      If _service.ExistLocationMenu(_locationMenu) Then
        _request.LocationMenu = _locationMenu
        _reponse = _service.GetMenuConfiguration(_request)

        If (_reponse.Success) Then

          For Each item As ReportToolConfigDTO In _reponse.ReportToolConfigs
            Me._cmb_prog_type.Add(item.ReportID + VALUE_ENUM_TYPE, item.ReportName)
          Next

        End If

      End If

    Next

  End Sub

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim str_where As String = ""

    ' Filter Programming Status
    If Me.chk_enabled.Checked And Not Me.chk_disabled.Checked Then
      str_where = str_where & " AND MP_ENABLED = 1 "
    End If
    If Me.chk_disabled.Checked And Not Me.chk_enabled.Checked Then
      str_where = str_where & " AND MP_ENABLED = 0 "
    End If

    If Not Me.m_with_wxp_machine_meters Then
      str_where = str_where & " AND MP_TYPE <> " & WSI.Common.MAILING_PROGRAMMING_TYPE.WXP_MACHINE_METERS
    End If

    ' Filter Programming NAME
    If Me.ef_prog_name.Value <> "" Then
      str_where = str_where & " AND " & GUI_FilterField("MP_NAME", ef_prog_name.Value, False, False, True)
    End If

    If m_report_type <> 0 Then
      str_where = str_where & " AND MP_TYPE = " & m_report_type
    End If

    ' Final processing
    If Len(str_where) > 0 Then
      ' Discard the leading ' AND ' and place 'Where' instead
      str_where = Strings.Right(str_where, Len(str_where) - 5)
      str_where = " WHERE " & str_where
    End If

    Return str_where
  End Function ' GetSqlWhere

  ' PURPOSE : Adds a new mailing programming to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_ShowNewMailingProgrammingForm()

    Dim frm_edit As frm_mailing_programming_edit

    frm_edit = New frm_mailing_programming_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_ShowNewMailingProgrammingForm

  ' PURPOSE : Adds a new mailing programming to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Private Sub GUI_ShowParameters()

    Dim frm_edit As frm_mailing_parameters

    frm_edit = New frm_mailing_parameters
    Call frm_edit.ShowForEdit()

    frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub ' GUI_ShowParameters

#End Region ' Private Functions

#Region "Events"

#End Region ' Events

End Class ' frm_mailing_programming_sel
