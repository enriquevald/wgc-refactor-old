<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_currency_exchange
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.dg_currencies_exchange = New GUI_Controls.uc_grid
    Me.gb_currency = New System.Windows.Forms.GroupBox
    Me.lbl_variable = New System.Windows.Forms.Label
    Me.lbl_fixed = New System.Windows.Forms.Label
    Me.lbl_promotion_NR2_msg = New GUI_Controls.uc_text_field
    Me.lbl_commissions_msg = New GUI_Controls.uc_text_field
    Me.lbl_Net_amount_msg = New GUI_Controls.uc_text_field
    Me.lbl_national_amount_msg = New GUI_Controls.uc_text_field
    Me.lbl_recharge_msg = New GUI_Controls.uc_text_field
    Me.ef_variable_promotions_NR2 = New GUI_Controls.uc_entry_field
    Me.ef_fixed_promotions_NR2 = New GUI_Controls.uc_entry_field
    Me.ef_variable_commissions = New GUI_Controls.uc_entry_field
    Me.ef_fixed_commissions = New GUI_Controls.uc_entry_field
    Me.lbl_decimals_msg = New GUI_Controls.uc_text_field
    Me.ef_decimals = New GUI_Controls.uc_entry_field
    Me.ef_change = New GUI_Controls.uc_entry_field
    Me.lbl_currency = New GUI_Controls.uc_text_field
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.ef_national_currency = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.gb_currency.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.ef_national_currency)
    Me.panel_data.Controls.Add(Me.gb_currency)
    Me.panel_data.Controls.Add(Me.dg_currencies_exchange)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(536, 478)
    '
    'dg_currencies_exchange
    '
    Me.dg_currencies_exchange.CurrentCol = -1
    Me.dg_currencies_exchange.CurrentRow = -1
    Me.dg_currencies_exchange.Location = New System.Drawing.Point(3, 37)
    Me.dg_currencies_exchange.Name = "dg_currencies_exchange"
    Me.dg_currencies_exchange.PanelRightVisible = False
    Me.dg_currencies_exchange.Redraw = True
    Me.dg_currencies_exchange.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_currencies_exchange.Size = New System.Drawing.Size(530, 86)
    Me.dg_currencies_exchange.Sortable = False
    Me.dg_currencies_exchange.SortAscending = True
    Me.dg_currencies_exchange.SortByCol = 0
    Me.dg_currencies_exchange.TabIndex = 2
    Me.dg_currencies_exchange.ToolTipped = True
    Me.dg_currencies_exchange.TopRow = -2
    '
    'gb_currency
    '
    Me.gb_currency.Controls.Add(Me.lbl_variable)
    Me.gb_currency.Controls.Add(Me.lbl_fixed)
    Me.gb_currency.Controls.Add(Me.lbl_promotion_NR2_msg)
    Me.gb_currency.Controls.Add(Me.lbl_commissions_msg)
    Me.gb_currency.Controls.Add(Me.lbl_Net_amount_msg)
    Me.gb_currency.Controls.Add(Me.lbl_national_amount_msg)
    Me.gb_currency.Controls.Add(Me.lbl_recharge_msg)
    Me.gb_currency.Controls.Add(Me.ef_variable_promotions_NR2)
    Me.gb_currency.Controls.Add(Me.ef_fixed_promotions_NR2)
    Me.gb_currency.Controls.Add(Me.ef_variable_commissions)
    Me.gb_currency.Controls.Add(Me.ef_fixed_commissions)
    Me.gb_currency.Controls.Add(Me.lbl_decimals_msg)
    Me.gb_currency.Controls.Add(Me.ef_decimals)
    Me.gb_currency.Controls.Add(Me.ef_change)
    Me.gb_currency.Controls.Add(Me.lbl_currency)
    Me.gb_currency.Location = New System.Drawing.Point(3, 129)
    Me.gb_currency.Name = "gb_currency"
    Me.gb_currency.Size = New System.Drawing.Size(530, 272)
    Me.gb_currency.TabIndex = 3
    Me.gb_currency.TabStop = False
    '
    'lbl_variable
    '
    Me.lbl_variable.Anchor = System.Windows.Forms.AnchorStyles.Left
    Me.lbl_variable.AutoSize = True
    Me.lbl_variable.Location = New System.Drawing.Point(364, 117)
    Me.lbl_variable.Name = "lbl_variable"
    Me.lbl_variable.Size = New System.Drawing.Size(61, 13)
    Me.lbl_variable.TabIndex = 16
    Me.lbl_variable.Text = "xVariable"
    Me.lbl_variable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_fixed
    '
    Me.lbl_fixed.Anchor = System.Windows.Forms.AnchorStyles.Left
    Me.lbl_fixed.AutoSize = True
    Me.lbl_fixed.Location = New System.Drawing.Point(169, 117)
    Me.lbl_fixed.Name = "lbl_fixed"
    Me.lbl_fixed.Size = New System.Drawing.Size(44, 13)
    Me.lbl_fixed.TabIndex = 15
    Me.lbl_fixed.Text = "xFixed"
    Me.lbl_fixed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_promotion_NR2_msg
    '
    Me.lbl_promotion_NR2_msg.AutoSize = True
    Me.lbl_promotion_NR2_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_promotion_NR2_msg.IsReadOnly = True
    Me.lbl_promotion_NR2_msg.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_promotion_NR2_msg.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_promotion_NR2_msg.Location = New System.Drawing.Point(303, 242)
    Me.lbl_promotion_NR2_msg.Name = "lbl_promotion_NR2_msg"
    Me.lbl_promotion_NR2_msg.Size = New System.Drawing.Size(211, 23)
    Me.lbl_promotion_NR2_msg.SufixText = "Sufix Text"
    Me.lbl_promotion_NR2_msg.SufixTextVisible = True
    Me.lbl_promotion_NR2_msg.TabIndex = 14
    Me.lbl_promotion_NR2_msg.TabStop = False
    Me.lbl_promotion_NR2_msg.TextWidth = 0
    Me.lbl_promotion_NR2_msg.Value = "xPromotion NR2: [ ]"
    '
    'lbl_commissions_msg
    '
    Me.lbl_commissions_msg.AutoSize = True
    Me.lbl_commissions_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_commissions_msg.IsReadOnly = True
    Me.lbl_commissions_msg.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_commissions_msg.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_commissions_msg.Location = New System.Drawing.Point(66, 242)
    Me.lbl_commissions_msg.Name = "lbl_commissions_msg"
    Me.lbl_commissions_msg.Size = New System.Drawing.Size(220, 23)
    Me.lbl_commissions_msg.SufixText = "Sufix Text"
    Me.lbl_commissions_msg.SufixTextVisible = True
    Me.lbl_commissions_msg.TabIndex = 13
    Me.lbl_commissions_msg.TabStop = False
    Me.lbl_commissions_msg.TextWidth = 0
    Me.lbl_commissions_msg.Value = "xCommissions: [ ]"
    '
    'lbl_Net_amount_msg
    '
    Me.lbl_Net_amount_msg.AutoSize = True
    Me.lbl_Net_amount_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_Net_amount_msg.IsReadOnly = True
    Me.lbl_Net_amount_msg.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_Net_amount_msg.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_Net_amount_msg.Location = New System.Drawing.Point(303, 221)
    Me.lbl_Net_amount_msg.Name = "lbl_Net_amount_msg"
    Me.lbl_Net_amount_msg.Size = New System.Drawing.Size(211, 23)
    Me.lbl_Net_amount_msg.SufixText = "Sufix Text"
    Me.lbl_Net_amount_msg.SufixTextVisible = True
    Me.lbl_Net_amount_msg.TabIndex = 12
    Me.lbl_Net_amount_msg.TabStop = False
    Me.lbl_Net_amount_msg.TextWidth = 0
    Me.lbl_Net_amount_msg.Value = "xNet Amount: [ ]"
    '
    'lbl_national_amount_msg
    '
    Me.lbl_national_amount_msg.AutoSize = True
    Me.lbl_national_amount_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_national_amount_msg.IsReadOnly = True
    Me.lbl_national_amount_msg.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_national_amount_msg.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_national_amount_msg.Location = New System.Drawing.Point(66, 221)
    Me.lbl_national_amount_msg.Name = "lbl_national_amount_msg"
    Me.lbl_national_amount_msg.Size = New System.Drawing.Size(220, 23)
    Me.lbl_national_amount_msg.SufixText = "Sufix Text"
    Me.lbl_national_amount_msg.SufixTextVisible = True
    Me.lbl_national_amount_msg.TabIndex = 11
    Me.lbl_national_amount_msg.TabStop = False
    Me.lbl_national_amount_msg.TextWidth = 0
    Me.lbl_national_amount_msg.Value = "xNational Amount: [ ]"
    '
    'lbl_recharge_msg
    '
    Me.lbl_recharge_msg.AutoSize = True
    Me.lbl_recharge_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_recharge_msg.IsReadOnly = True
    Me.lbl_recharge_msg.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_recharge_msg.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_recharge_msg.Location = New System.Drawing.Point(47, 201)
    Me.lbl_recharge_msg.Name = "lbl_recharge_msg"
    Me.lbl_recharge_msg.Size = New System.Drawing.Size(230, 23)
    Me.lbl_recharge_msg.SufixText = "Sufix Text"
    Me.lbl_recharge_msg.SufixTextVisible = True
    Me.lbl_recharge_msg.TabIndex = 10
    Me.lbl_recharge_msg.TabStop = False
    Me.lbl_recharge_msg.TextWidth = 0
    Me.lbl_recharge_msg.Value = "xFor a recharge of 100 USD"
    '
    'ef_variable_promotions_NR2
    '
    Me.ef_variable_promotions_NR2.DoubleValue = 0
    Me.ef_variable_promotions_NR2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_variable_promotions_NR2.IntegerValue = 0
    Me.ef_variable_promotions_NR2.IsReadOnly = False
    Me.ef_variable_promotions_NR2.Location = New System.Drawing.Point(325, 170)
    Me.ef_variable_promotions_NR2.Name = "ef_variable_promotions_NR2"
    Me.ef_variable_promotions_NR2.Size = New System.Drawing.Size(173, 25)
    Me.ef_variable_promotions_NR2.SufixText = "Sufix Text"
    Me.ef_variable_promotions_NR2.SufixTextVisible = True
    Me.ef_variable_promotions_NR2.SufixTextWidth = 50
    Me.ef_variable_promotions_NR2.TabIndex = 9
    Me.ef_variable_promotions_NR2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_variable_promotions_NR2.TextValue = ""
    Me.ef_variable_promotions_NR2.TextWidth = 0
    Me.ef_variable_promotions_NR2.Value = ""
    '
    'ef_fixed_promotions_NR2
    '
    Me.ef_fixed_promotions_NR2.DoubleValue = 0
    Me.ef_fixed_promotions_NR2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_fixed_promotions_NR2.IntegerValue = 0
    Me.ef_fixed_promotions_NR2.IsReadOnly = False
    Me.ef_fixed_promotions_NR2.Location = New System.Drawing.Point(6, 170)
    Me.ef_fixed_promotions_NR2.Name = "ef_fixed_promotions_NR2"
    Me.ef_fixed_promotions_NR2.Size = New System.Drawing.Size(295, 25)
    Me.ef_fixed_promotions_NR2.SufixText = "Sufix Text"
    Me.ef_fixed_promotions_NR2.SufixTextVisible = True
    Me.ef_fixed_promotions_NR2.SufixTextWidth = 50
    Me.ef_fixed_promotions_NR2.TabIndex = 8
    Me.ef_fixed_promotions_NR2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_promotions_NR2.TextValue = ""
    Me.ef_fixed_promotions_NR2.TextWidth = 120
    Me.ef_fixed_promotions_NR2.Value = ""
    '
    'ef_variable_commissions
    '
    Me.ef_variable_commissions.DoubleValue = 0
    Me.ef_variable_commissions.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_variable_commissions.IntegerValue = 0
    Me.ef_variable_commissions.IsReadOnly = False
    Me.ef_variable_commissions.Location = New System.Drawing.Point(325, 140)
    Me.ef_variable_commissions.Name = "ef_variable_commissions"
    Me.ef_variable_commissions.Size = New System.Drawing.Size(173, 25)
    Me.ef_variable_commissions.SufixText = "Sufix Text"
    Me.ef_variable_commissions.SufixTextVisible = True
    Me.ef_variable_commissions.SufixTextWidth = 50
    Me.ef_variable_commissions.TabIndex = 7
    Me.ef_variable_commissions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_variable_commissions.TextValue = ""
    Me.ef_variable_commissions.TextWidth = 0
    Me.ef_variable_commissions.Value = ""
    '
    'ef_fixed_commissions
    '
    Me.ef_fixed_commissions.DoubleValue = 0
    Me.ef_fixed_commissions.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_fixed_commissions.IntegerValue = 0
    Me.ef_fixed_commissions.IsReadOnly = False
    Me.ef_fixed_commissions.Location = New System.Drawing.Point(6, 140)
    Me.ef_fixed_commissions.Name = "ef_fixed_commissions"
    Me.ef_fixed_commissions.Size = New System.Drawing.Size(295, 25)
    Me.ef_fixed_commissions.SufixText = "Sufix Text"
    Me.ef_fixed_commissions.SufixTextVisible = True
    Me.ef_fixed_commissions.SufixTextWidth = 50
    Me.ef_fixed_commissions.TabIndex = 6
    Me.ef_fixed_commissions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_commissions.TextValue = ""
    Me.ef_fixed_commissions.TextWidth = 120
    Me.ef_fixed_commissions.Value = ""
    '
    'lbl_decimals_msg
    '
    Me.lbl_decimals_msg.AutoSize = True
    Me.lbl_decimals_msg.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_decimals_msg.IsReadOnly = True
    Me.lbl_decimals_msg.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_decimals_msg.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_decimals_msg.Location = New System.Drawing.Point(126, 81)
    Me.lbl_decimals_msg.Name = "lbl_decimals_msg"
    Me.lbl_decimals_msg.Size = New System.Drawing.Size(367, 23)
    Me.lbl_decimals_msg.SufixText = "Sufix Text"
    Me.lbl_decimals_msg.SufixTextVisible = True
    Me.lbl_decimals_msg.TabIndex = 5
    Me.lbl_decimals_msg.TabStop = False
    Me.lbl_decimals_msg.TextWidth = 0
    Me.lbl_decimals_msg.Value = "xDecimals: Accuracy of the amounts resulting."
    '
    'ef_decimals
    '
    Me.ef_decimals.DoubleValue = 0
    Me.ef_decimals.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_decimals.IntegerValue = 0
    Me.ef_decimals.IsReadOnly = False
    Me.ef_decimals.Location = New System.Drawing.Point(254, 50)
    Me.ef_decimals.Name = "ef_decimals"
    Me.ef_decimals.Size = New System.Drawing.Size(191, 25)
    Me.ef_decimals.SufixText = "Sufix Text"
    Me.ef_decimals.SufixTextVisible = True
    Me.ef_decimals.TabIndex = 4
    Me.ef_decimals.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_decimals.TextValue = ""
    Me.ef_decimals.TextWidth = 70
    Me.ef_decimals.Value = ""
    '
    'ef_change
    '
    Me.ef_change.DoubleValue = 0
    Me.ef_change.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_change.IntegerValue = 0
    Me.ef_change.IsReadOnly = False
    Me.ef_change.Location = New System.Drawing.Point(54, 50)
    Me.ef_change.Name = "ef_change"
    Me.ef_change.Size = New System.Drawing.Size(196, 25)
    Me.ef_change.SufixText = "Sufix Text"
    Me.ef_change.SufixTextVisible = True
    Me.ef_change.TabIndex = 3
    Me.ef_change.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_change.TextValue = ""
    Me.ef_change.TextWidth = 70
    Me.ef_change.Value = ""
    '
    'lbl_currency
    '
    Me.lbl_currency.AutoSize = True
    Me.lbl_currency.IsReadOnly = True
    Me.lbl_currency.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_currency.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_currency.Location = New System.Drawing.Point(78, 20)
    Me.lbl_currency.Name = "lbl_currency"
    Me.lbl_currency.Size = New System.Drawing.Size(367, 24)
    Me.lbl_currency.SufixText = "Sufix Text"
    Me.lbl_currency.SufixTextVisible = True
    Me.lbl_currency.TabIndex = 2
    Me.lbl_currency.TabStop = False
    Me.lbl_currency.TextWidth = 0
    Me.lbl_currency.Value = "xCurrency"
    '
    'Timer1
    '
    Me.Timer1.Enabled = True
    '
    'ef_national_currency
    '
    Me.ef_national_currency.DoubleValue = 0
    Me.ef_national_currency.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_national_currency.IntegerValue = 0
    Me.ef_national_currency.IsReadOnly = False
    Me.ef_national_currency.Location = New System.Drawing.Point(9, 6)
    Me.ef_national_currency.Name = "ef_national_currency"
    Me.ef_national_currency.Size = New System.Drawing.Size(439, 25)
    Me.ef_national_currency.SufixText = "Sufix Text"
    Me.ef_national_currency.SufixTextVisible = True
    Me.ef_national_currency.TabIndex = 17
    Me.ef_national_currency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_national_currency.TextValue = ""
    Me.ef_national_currency.TextWidth = 120
    Me.ef_national_currency.Value = ""
    '
    'frm_currency_exchange
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(659, 410)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_currency_exchange"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_currency_exchange"
    Me.panel_data.ResumeLayout(False)
    Me.gb_currency.ResumeLayout(False)
    Me.gb_currency.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_currencies_exchange As GUI_Controls.uc_grid
  Friend WithEvents gb_currency As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_currency As GUI_Controls.uc_text_field
  Friend WithEvents lbl_decimals_msg As GUI_Controls.uc_text_field
  Friend WithEvents ef_decimals As GUI_Controls.uc_entry_field
  Friend WithEvents ef_change As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_national_amount_msg As GUI_Controls.uc_text_field
  Friend WithEvents lbl_recharge_msg As GUI_Controls.uc_text_field
  Friend WithEvents ef_variable_promotions_NR2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_fixed_promotions_NR2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_variable_commissions As GUI_Controls.uc_entry_field
  Friend WithEvents ef_fixed_commissions As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_promotion_NR2_msg As GUI_Controls.uc_text_field
  Friend WithEvents lbl_commissions_msg As GUI_Controls.uc_text_field
  Friend WithEvents lbl_Net_amount_msg As GUI_Controls.uc_text_field
  Friend WithEvents lbl_variable As System.Windows.Forms.Label
  Friend WithEvents lbl_fixed As System.Windows.Forms.Label
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents ef_national_currency As GUI_Controls.uc_entry_field
End Class
