'-------------------------------------------------------------------
' Copyright � 2007-2014 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_progressive_edit
' DESCRIPTION:   Progressive edition.
' AUTHOR:        Jes�s �ngel Blanco
' CREATION DATE: 18-AUG-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-AUG-2014  JAB    Initial version.
' 03-SEP-2014  JAB    The sum of the contributions levels  must be equal to 100.
' 08-SEP-2014  FJC    Check At Least One terminal Selected when add / edit a progressive
' 07-OCT-2014  FJC    Fixed Bug 1427: Error en la Edici�n de JackPots Progresivos.
' 15-DEC-2014  JMM    Allow to modify levels amount
' 17-DEC-2014  JMM    Fixed bug WIG-1851: Unhandled exception on clicking "Update Amount" when there's only one level on the progressive
' 23-FEB-2015  FJC    Fixed Bug WIG-2096: Error when we are editing existing levels from a progressive.
' 25-FEB-2015  FJC    Fixed Bug WIG-2108: Error when we are removing existing levels from a progressive.
' 21-NOV-2016  GMV    Product Backlog Item 19930:Jackpots - GUI - Show on WinUp setting
' 08-AUG-2017  AMF    Bug 29249:[WIGOS-4253] A message to must select any terminal in Progressive jackpots appears even having selected All terminals.
' 08-FEB-2018  LTC    PBI 31459: WIGOS-8097 AGG - Progressive Jackpots configuration for brand, server and multiseat 
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE


Public Class frm_progressive_edit
  Inherits frm_base_edit

#Region " Const "

  Private Const FORM_DB_MIN_VERSION As Short = 224 ' TODO: Revisar que la base de datos sea la correcta.

  ' QUERY columns
  Private Const SQL_COLUMN_PGL_LEVEL_ID As Integer = 0
  Private Const SQL_COLUMN_PGL_NAME As Integer = 1
  Private Const SQL_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 2
  Private Const SQL_COLUMN_PGL_AMOUNT As Integer = 3
  Private Const SQL_COLUMN_PGL_DELETEABLE_LEVEL As Integer = 4
  Private Const SQL_COLUMN_PGL_SHOW_ON_WINUP As Integer = 5

  ' Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_PGL_LEVEL_ID As Integer = 1
  Private Const GRID_COLUMN_PGL_NAME As Integer = 2
  Private Const GRID_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 3
  Private Const GRID_COLUMN_PGL_AMOUNT As Integer = 4
  Private Const GRID_COLUMN_PGL_DELETEABLE_LEVEL As Integer = 5
  Private Const GRID_COLUMN_PGL_SHOW_ON_WINUP As Integer = 6

  Private Const GRID_COLUMNS As Integer = 7

  ' Width Columns
  ' INDEX
  Private Const GRID_WIDTH_COLUMN_INDEX As Integer = 200
  Private Const GRID_WIDTH_COLUMN_PGL_LEVEL_ID As Integer = 930
  Private Const GRID_WIDTH_COLUMN_PGL_NAME As Integer = 2870
  Private Const GRID_WIDTH_COLUMN_PGL_CONTRIBUTION_PCT As Integer = 2000
  Private Const GRID_WIDTH_COLUMN_PGL_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_COLUMN_PGL_SHOW_ON_WINUP As Integer = 1000

  Private Const GRID_MAX_ROWS As Integer = 100

#End Region ' Const

#Region " Members "

  Public m_current_progressive As CLASS_PROGRESSIVE.TYPE_PROGRESSIVE
  Public m_last_level_id As Integer

  Private m_currencies_selected() As Integer
  Private m_delete_operation As Boolean

  Private m_dt_terminals_to_check As DataTable

  Private m_winUp_enabled As Boolean                 'indicates wether WinUP module is installed

#End Region ' Members

#Region " Enums "

  Public Enum FUNCTION_MODE
    FROM_ADD_LEVEL = 0
    FROM_IS_SCREEN_DATA_OK = 1
  End Enum

#End Region ' Enums

#Region " Overrides "

  ' PURPOSE: Sets form Id 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_PROGRESSIVE_EDIT
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(Optional ByVal ProgressiveId As Integer = -1)

    ' Sets the screen mode
    DbObjectId = ProgressiveId
    DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

    Me.m_current_progressive = New CLASS_PROGRESSIVE.TYPE_PROGRESSIVE
    Me.m_current_progressive.progressive_id = ProgressiveId
    Me.ScreenMode = IIf(m_current_progressive.progressive_id <> -1, ENUM_SCREEN_MODE.MODE_EDIT, ENUM_SCREEN_MODE.MODE_NEW)
    Me.m_current_progressive.progressive_screen_mode = Me.ScreenMode
    Me.MdiParent = MdiParent

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then
      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowEditItem

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _dt_jackpots_granted As DataTable = Nothing
    Dim _str_lbl As StringBuilder
    Dim _date_progressive_granted As Date
    Dim _str_date_progressive_granted As String
    Dim _amount_progressive_granted As Decimal
    Dim _str_amount_progressive_granted As String
    Dim _terminal_progressive_granted As String


    _str_lbl = New StringBuilder

    _amount_progressive_granted = 0
    _terminal_progressive_granted = AUDIT_NONE_STRING
    _str_amount_progressive_granted = AUDIT_NONE_STRING
    _str_date_progressive_granted = AUDIT_NONE_STRING


    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5264) ' Edici�n de jackpot progresivo

    Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)                   ' Aceptar
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(4)              ' Cancelar

    Me.GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = False

    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)                                                  ' A�adir 
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)                                                  ' Borrar 
    Me.btn_update.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5795)                                      ' Actualizar
    Me.btn_update.Width = 120
    Me.btn_update.Height = 21

    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5209)                                         ' Nombre
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_name.IsAccessible = True

    Me.ef_contribution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5213)                                 ' Contribuci�n (% cuatro decimales)
    Me.ef_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_contribution.IsAccessible = True

    Me.ef_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214)                                       ' Monto (solo lectura)
    Me.ef_amount.IsReadOnly = True
    Me.ef_amount.TextAlign = HorizontalAlignment.Right

    ' LTC 2018-FEB-08
    Me.ef_brand.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9014)                                        ' Marca
    Me.ef_brand.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_brand.IsAccessible = True

    Me.ef_server.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9015)                                       ' Servidor
    Me.ef_server.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_server.IsAccessible = True

    Me.chk_multiseat.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9012)                                   ' Unique Machine
    Me.chk_multiseat.Visible = WSI.Common.Misc.IsAGGEnabled()

    Me.chk_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5218)                                      ' Habilitado
    Me.gb_levels.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5210)                                       ' Niveles

    Me.uc_terminals_group_filter.SetGroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5358)            ' Lista de terminales
    Me.uc_terminals_group_filter.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.OnlyListed ' Only "Select terminal" element

    ' Grid
    Me.m_winUp_enabled = WSI.Common.GeneralParam.GetBoolean("Features", "WinUP")
    Call GUI_StyleSheet()

    ' Labels
    ' Terminals Related to a Progressive
    Me.lbl_term_particip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5571) & _
                                GetNumTerminalsProgressive(m_current_progressive.progressive_id)          'Participating Terminals

    ' Last Granted Progressive (�ltimo progresivo otorgado)
    If GetLastGrantedProgressive(m_current_progressive.progressive_id, _dt_jackpots_granted) Then
      _date_progressive_granted = _dt_jackpots_granted.Rows(0)(0)
      _str_date_progressive_granted = GUI_FormatDate(_date_progressive_granted, , ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      _amount_progressive_granted = _dt_jackpots_granted.Rows(0)(1)
      _str_amount_progressive_granted = GUI_FormatCurrency(_amount_progressive_granted, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True)
      _terminal_progressive_granted = _dt_jackpots_granted.Rows(0)(2).ToString & "-" & _dt_jackpots_granted.Rows(0)(3).ToString

    End If

    _str_lbl.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5572))  '�ltimo progresivo otorgado)

    'Date
    _str_lbl.AppendLine("         " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(604) & ": " & _str_date_progressive_granted)

    'Amount
    _str_lbl.AppendLine("         " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372) & ": " & _str_amount_progressive_granted)

    'Terminal
    _str_lbl.AppendLine("         " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5364) & ": " & _terminal_progressive_granted)

    'Fill Label
    Me.lbl_progres_otorgado.Text = _str_lbl.ToString

  End Sub ' GUI_InitControls

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ef_name
  End Sub 'GUI_SetInitialFocus

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _progressive_class As CLASS_PROGRESSIVE
    Dim _nls_param1 As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT

    If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_NOT_SUPPORTED Then
      '' Wrong DbVersion message alredy showed,not show error message again.
      Exit Sub
    End If

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_PROGRESSIVE
        _progressive_class = DbEditedObject
        If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
          _progressive_class.ProgressiveId = -1
          _progressive_class.ProgressiveName = ""
          _progressive_class.ProgressiveCreated = GUI_FormatDate(WSI.Common.WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
          _progressive_class.ProgressiveContributionPct = GUI_FormatNumber(0, 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 7)
          _progressive_class.ProgressiveNumLevels = 0
          _progressive_class.ProgressiveAmount = GUI_FormatCurrency(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
          _progressive_class.ProgressiveLevelsList = New DataTable()
          _progressive_class.ProgressiveStatus = WSI.Common.Progressives.PROGRESSIVE_STATUS.ACTIVE
        End If

        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            _progressive_class.ProgressiveScreenMode = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            _progressive_class.ProgressiveScreenMode = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_SCREEN_MODE.MODE_EDIT
        End Select

        DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then

          _progressive_class = Me.DbEditedObject
          _nls_param1 = m_current_progressive.progressive_name

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5256), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK Then

          _progressive_class = Me.DbEditedObject
          _nls_param1 = m_current_progressive.progressive_name

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5256), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_OK Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK _
        AndAlso Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_NOT_FOUND Then

          _progressive_class = Me.DbEditedObject
          _nls_param1 = m_current_progressive.progressive_name

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5257), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_DEPENDENCIES And m_delete_operation Then

          _progressive_class = Me.DbEditedObject
          _nls_param1 = m_current_progressive.progressive_name

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5254), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)

        ElseIf Me.DbStatus <> CLASS_BASE.ENUM_STATUS.STATUS_OK And m_delete_operation Then

          _progressive_class = Me.DbEditedObject
          _nls_param1 = m_current_progressive.progressive_name

          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5258), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        _progressive_class = Me.DbEditedObject
        _nls_param1 = m_current_progressive.progressive_name

        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5259), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _nls_param1)

        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = CLASS_BASE.ENUM_STATUS.STATUS_ERROR
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        If Me.DbEditedObject IsNot Nothing Then
          _progressive_class = Me.DbEditedObject
        End If

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub ' GUI_DB_Operation

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _progressive_item As CLASS_PROGRESSIVE
    Dim _dg_current_row As Integer

    _progressive_item = DbReadObject
    m_last_level_id = 0

    Me.ef_name.TextValue = _progressive_item.ProgressiveName
    Me.chk_status.Checked = IIf(_progressive_item.ProgressiveStatus = WSI.Common.Progressives.PROGRESSIVE_STATUS.NOT_ACTIVE, False, True)
    Me.ef_contribution.TextValue = GUI_FormatNumber(_progressive_item.ProgressiveContributionPct, 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 7)
    Me.ef_amount.TextValue = GUI_FormatCurrency(_progressive_item.ProgressiveAmount, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
    ' LTC 2018-FEB-08
    Me.ef_brand.TextValue = _progressive_item.ProgressiveBrand
    Me.ef_server.TextValue = _progressive_item.ProgressiveServer
    Me.chk_multiseat.Checked = _progressive_item.ProgressiveIsMultiseat
    Me.uc_terminals_group_filter.SetTerminalList(_progressive_item.ProgressiveTerminalList)
    Me.uc_terminals_group_filter.SelectedIndex = 0

    For Each _row As DataRow In _progressive_item.ProgressiveLevelsList.Rows
      _dg_current_row = Me.dg_levels.AddRow()
      Me.dg_levels.Cell(_dg_current_row, GRID_COLUMN_PGL_LEVEL_ID).Value = GUI_FormatNumber(_row(SQL_COLUMN_PGL_LEVEL_ID), 0)
      Me.dg_levels.Cell(_dg_current_row, GRID_COLUMN_PGL_NAME).Value = _row(SQL_COLUMN_PGL_NAME)
      Me.dg_levels.Cell(_dg_current_row, GRID_COLUMN_PGL_CONTRIBUTION_PCT).Value = GUI_FormatNumber(_row(SQL_COLUMN_PGL_CONTRIBUTION_PCT), 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
      Me.dg_levels.Cell(_dg_current_row, GRID_COLUMN_PGL_AMOUNT).Value = GUI_FormatCurrency(_row(SQL_COLUMN_PGL_AMOUNT), , ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
      Me.dg_levels.Cell(_dg_current_row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value = _row(SQL_COLUMN_PGL_DELETEABLE_LEVEL)        'Deleteable Level
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        Me.dg_levels.Cell(_dg_current_row, GRID_COLUMN_PGL_SHOW_ON_WINUP).Value = BoolToGridValue(_row(SQL_COLUMN_PGL_SHOW_ON_WINUP))
      End If


      If _row(SQL_COLUMN_PGL_LEVEL_ID) > m_last_level_id Then
        m_last_level_id = _row(SQL_COLUMN_PGL_LEVEL_ID)
      End If
    Next

    If Me.dg_levels.NumRows > 0 Then
      Me.dg_levels.SelectFirstRow(True)
      Me.dg_levels_RowSelectedEvent(0)
      dg_levels.CurrentRow = 0
    End If

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Get data from screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_GetScreenData()

    Dim _progressive_item_read As CLASS_PROGRESSIVE
    Dim _progressive_item_edited As CLASS_PROGRESSIVE
    Dim _row As DataRow
    Dim _count As Integer

    _progressive_item_read = DbReadObject
    _progressive_item_edited = DbEditedObject

    _progressive_item_edited.ProgressiveName = Me.ef_name.TextValue
    _progressive_item_edited.ProgressiveStatus = IIf(Me.chk_status.Checked, WSI.Common.Progressives.PROGRESSIVE_STATUS.ACTIVE, WSI.Common.Progressives.PROGRESSIVE_STATUS.NOT_ACTIVE)
    If _progressive_item_edited.ProgressiveStatus <> _progressive_item_read.ProgressiveStatus Then
      _progressive_item_edited.ProgressiveStatusChanged = GUI_FormatDate(WGDB.Now(), ENUM_FORMAT_DATE.FORMAT_DATE_LONG, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    _progressive_item_edited.ProgressiveContributionPct = GUI_ParseNumber(Me.ef_contribution.TextValue)
    _progressive_item_edited.ProgressiveBrand = Me.ef_brand.TextValue
    _progressive_item_edited.ProgressiveServer = Me.ef_server.TextValue
    _progressive_item_edited.ProgressiveIsMultiseat = Me.chk_multiseat.Checked

    Me.uc_terminals_group_filter.GetTerminalsFromControl(_progressive_item_edited.ProgressiveTerminalList)

    _count = 0
    _progressive_item_edited.ProgressiveLevelsList = New DataTable()
    _progressive_item_edited.ProgressiveLevelsList.Columns.Add("pgl_level_id", Type.GetType("System.Int64"))
    _progressive_item_edited.ProgressiveLevelsList.Columns.Add("pgl_name", Type.GetType("System.String"))
    _progressive_item_edited.ProgressiveLevelsList.Columns.Add("pgl_contribution_pct", Type.GetType("System.Decimal"))
    _progressive_item_edited.ProgressiveLevelsList.Columns.Add("pgl_amount", Type.GetType("System.Decimal"))

    _progressive_item_edited.ProgressiveLevelsList.Columns.Add("pgl_deleteable_level", Type.GetType("System.Int32"))
    If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
      _progressive_item_edited.ProgressiveLevelsList.Columns.Add("pgl_show_on_winup", Type.GetType("System.Boolean"))
    End If


    Dim _total_levels_amount As Decimal

    _total_levels_amount = 0

    While _count < dg_levels.NumRows
      _row = _progressive_item_edited.ProgressiveLevelsList.Rows.Add()
      _row(SQL_COLUMN_PGL_LEVEL_ID) = GUI_ParseNumber(dg_levels.Cell(_count, GRID_COLUMN_PGL_LEVEL_ID).Value)
      _row(SQL_COLUMN_PGL_NAME) = dg_levels.Cell(_count, GRID_COLUMN_PGL_NAME).Value
      _row(SQL_COLUMN_PGL_CONTRIBUTION_PCT) = GUI_ParseNumber(dg_levels.Cell(_count, GRID_COLUMN_PGL_CONTRIBUTION_PCT).Value)
      _row(SQL_COLUMN_PGL_AMOUNT) = GUI_ParseCurrency(dg_levels.Cell(_count, GRID_COLUMN_PGL_AMOUNT).Value)
      _row(SQL_COLUMN_PGL_DELETEABLE_LEVEL) = dg_levels.Cell(_count, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value
      If WSI.Common.GeneralParam.GetBoolean("Features", "WinUP", False) Then
        _row(SQL_COLUMN_PGL_SHOW_ON_WINUP) = GridValuetoBool(dg_levels.Cell(_count, GRID_COLUMN_PGL_SHOW_ON_WINUP).Value)
      End If


      _total_levels_amount += GUI_ParseCurrency(dg_levels.Cell(_count, GRID_COLUMN_PGL_AMOUNT).Value)

      _count += 1
    End While

    _progressive_item_edited.ProgressiveAmount = _total_levels_amount

    _progressive_item_edited.ProgressiveNumLevels = _progressive_item_edited.ProgressiveLevelsList.Rows.Count

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_OK        ' Aceptar cambios
        MyBase.GUI_ButtonClick(ButtonId)

      Case ENUM_BUTTON.BUTTON_DELETE    ' Borrar progresivo
        'Call RemoveProgressive()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0  ' Borrar Nivel
        'Call RemoveProgressiveLevel()

      Case ENUM_BUTTON.BUTTON_CUSTOM_1  ' A�adir Nivel
        'Call AddProgressiveLevel()

      Case Else
        MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: To control the key pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If dg_levels.ContainsFocus Then
          dg_levels.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)

        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function ' GUI_KeyPress

  ' PURPOSE: To determine if screen data are ok.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _progressive_item_read As CLASS_PROGRESSIVE
    Dim _progressive_item_edited As CLASS_PROGRESSIVE
    Dim _terminal_list As TerminalList

    _progressive_item_read = DbReadObject
    _progressive_item_edited = DbEditedObject


    ' Check if levels that are in table: "Progressive_provision_levels" are not in levels of the grid. 
    '   If that is true: ERROR, else all OK and go next step.
    If Not CheckProvisionedLevelsIntegrity(m_current_progressive.progressive_id, dg_levels) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5996), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Check Progressive name
    If String.IsNullOrEmpty(ef_name.TextValue) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5299), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Check Duplicate Progressive name
    If CheckProgressiveNameExists(m_current_progressive.progressive_id, ef_name.TextValue) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(114), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , _
                      GLB_NLS_GUI_PLAYER_TRACKING.GetString(5340), ef_name.TextValue)

      Return False
    End If

    ' Check progressive total percentage (>100%)
    If WSI.Common.Format.ParseCurrency(Me.ef_contribution.TextValue) > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5300), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Me.ef_contribution.TextValue = GUI_FormatNumber(100, 4, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE, 7)

      Return False
    End If

    ' Check progressive total percentage (=0%)
    If WSI.Common.Format.ParseCurrency(Me.ef_contribution.TextValue) = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5324), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Check uc_terminals_group_filter
    _terminal_list = New TerminalList
    uc_terminals_group_filter.GetTerminalsFromControl(_terminal_list)

    If String.IsNullOrEmpty(_terminal_list.ToXml) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5326), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    Me.m_dt_terminals_to_check = GetTerminalsToCheck(_progressive_item_read.ProgressiveTerminalList.ToXml, _terminal_list.ToXml, _progressive_item_edited.TerminalList)

    ' Check At Least One terminal Selected 
    If uc_terminals_group_filter.CheckAtLeastOneSelected = False Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5326), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Check terminals in use
    If AnyTerminalUsed(m_dt_terminals_to_check) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5351), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Progressive num levels
    If dg_levels.NumRows < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5325), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    If dg_levels.NumRows > 32 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5672), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    'Progressive level name blank
    If LevelsNameBlank(dg_levels) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(253))

      Return False
    End If

    'Progressive level name repeat
    If LevelsNameRepeat(dg_levels) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2796), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , GLB_NLS_GUI_PLAYER_TRACKING.GetString(253))

      Return False
    End If

    ' Check progressive percentage level
    If PercentageComplete(dg_levels, FUNCTION_MODE.FROM_IS_SCREEN_DATA_OK) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5301), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    ' Check all percentages levels (between 0 and 100)
    If Not PercentagesCorrect(dg_levels) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5327), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Dim _aux_dt As DataRow
      Dim _amounts_modified As Boolean
      Dim _idx_row As Integer

      _amounts_modified = False

      Try
        ' Check if any level amounts have been changed
        While _idx_row < dg_levels.NumRows
          If _idx_row < _progressive_item_edited.ProgressiveLevelsList.Rows.Count Then ' Fixed Bug WIG-2096
            _aux_dt = _progressive_item_edited.ProgressiveLevelsList.Select("PGL_LEVEL_ID = " + dg_levels.Cell(_idx_row, GRID_COLUMN_PGL_LEVEL_ID).Value)(0)

            If _aux_dt(SQL_COLUMN_PGL_AMOUNT) <> GUI_ParseCurrency(dg_levels.Cell(_idx_row, GRID_COLUMN_PGL_AMOUNT).Value) Then
              _amounts_modified = True
            End If
          End If

          _idx_row += 1
        End While

        If _amounts_modified Then
          '5796 "El monto no se va aprovisionar ni a b�veda ni a las m�quinas. �Desea continuar?"
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5796), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

            Return False
          End If
        End If

      Catch _ex As Exception
        Log.Exception(_ex)

        Return False
      End Try
    End If

    Return True
  End Function ' GUI_IsScreenDataOk

#End Region ' Overrides

#Region " Privates "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()

    With Me.dg_levels
      Call .Init(GRID_COLUMNS, 1, True)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_COLUMN_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Num levels
      .Column(GRID_COLUMN_PGL_LEVEL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443) 'nivel
      .Column(GRID_COLUMN_PGL_LEVEL_ID).Width = GRID_WIDTH_COLUMN_PGL_LEVEL_ID
      .Column(GRID_COLUMN_PGL_LEVEL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Name
      .Column(GRID_COLUMN_PGL_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5209)                      ' Nombre
      .Column(GRID_COLUMN_PGL_NAME).Width = GRID_WIDTH_COLUMN_PGL_NAME
      .Column(GRID_COLUMN_PGL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGL_NAME).IsMerged = False
      .Column(GRID_COLUMN_PGL_NAME).Editable = True
      .Column(GRID_COLUMN_PGL_NAME).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PGL_NAME).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

      ' Contribution pct
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5213)          ' Contribuci�n
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Width = GRID_WIDTH_COLUMN_PGL_CONTRIBUTION_PCT
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).IsMerged = False
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).Editable = True
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_PGL_CONTRIBUTION_PCT).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)

      ' Amount
      .Column(GRID_COLUMN_PGL_AMOUNT).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5214)          ' Amount
      .Column(GRID_COLUMN_PGL_AMOUNT).Width = GRID_WIDTH_COLUMN_PGL_AMOUNT
      .Column(GRID_COLUMN_PGL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Deleteable Level 
      .Column(GRID_COLUMN_PGL_DELETEABLE_LEVEL).Width = 0
      .Column(GRID_COLUMN_PGL_DELETEABLE_LEVEL).Header.Text = ""
      .Column(GRID_COLUMN_PGL_DELETEABLE_LEVEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Show on WinUP
      '.Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).Header(0).Text = ""
      .Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).Header.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(499) '"Visible On WinUP"
      .Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).Width = IIf(Me.m_winUp_enabled, 1000, 0)
      .Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_PGL_SHOW_ON_WINUP).Editable = True

      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    End With

  End Sub 'GUI_StyleSheet

  ' PURPOSE: Insert new row in the "uc_grid" component
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub AddProgressiveLevel()
    Dim _row As Int32

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    If dg_levels.NumRows + 1 > 32 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5672), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    If PercentageComplete(dg_levels, FUNCTION_MODE.FROM_ADD_LEVEL) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5296), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    If dg_levels.NumRows < GRID_MAX_ROWS Then

      _row = dg_levels.AddRow()
      m_last_level_id = dg_levels.NumRows

      dg_levels.ClearSelection()
      dg_levels.IsSelected(_row) = True
      dg_levels.Redraw = True
      dg_levels.TopRow = _row

      With dg_levels
        .Cell(_row, GRID_COLUMN_PGL_LEVEL_ID).Value = m_last_level_id ' This is a new level
        .Cell(_row, GRID_COLUMN_PGL_NAME).Value = ""
        .Cell(_row, GRID_COLUMN_PGL_CONTRIBUTION_PCT).Value = GUI_FormatNumber(0, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
        .Cell(_row, GRID_COLUMN_PGL_AMOUNT).Value = GUI_FormatCurrency(0, , ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
        .Cell(_row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_DELETEABLE
        .Cell(_row, GRID_COLUMN_PGL_SHOW_ON_WINUP).Value = BoolToGridValue(False)

        .CurrentCol = GRID_COLUMN_PGL_CONTRIBUTION_PCT
        .CurrentRow = _row
        .Focus()
      End With

      dg_levels_RowSelectedEvent(_row)

    End If

  End Sub ' AddProgressiveLevel

  ' PURPOSE: Remove row in the "uc_grid" component
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub RemoveProgressiveLevel()
    Dim _deleteable_level As Integer
    Dim _row_to_delete As Boolean
    Dim _reason_level_deleteable As CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS

    _row_to_delete = False

    Try
      If Not Permissions.Delete Then
        Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Exit Sub
      End If

      If Not dg_levels.SelectedRows Is Nothing AndAlso dg_levels.SelectedRows.Length > 0 Then

        For Each _selected_row As Integer In dg_levels.SelectedRows

          _deleteable_level = dg_levels.Cell(_selected_row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value
          Select Case _deleteable_level
            Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_DELETEABLE
              'Level IS Deleteable (if is not the last)
              _row_to_delete = True
              If (_selected_row + 1) <> dg_levels.NumRows Then
                'Level NOT deleteable because it's the last
                _row_to_delete = False
                _reason_level_deleteable = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_ISNT_LAST_LEVEL_GRID
              End If
            Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_PROVISIONED_LEVEL

              'Level NOT Deleteable. Reason: Level has been provisioned
              _row_to_delete = False
              _reason_level_deleteable = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_PROVISIONED_LEVEL
            Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO

              'Level NOT Deleteable. Reason: Amount different than zero
              _row_to_delete = False
              _reason_level_deleteable = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO
          End Select

          If Not _row_to_delete Then
            Select Case _reason_level_deleteable
              Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_PROVISIONED_LEVEL

                'Level NOT Deleteable. Reason: Level has been provisioned
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5996), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
              Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO

                'Level NOT Deleteable. Reason: Amount different than zero
                Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5295), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
              Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_ISNT_LAST_LEVEL_GRID

                ' Button is disabled when setting a level who is not the last.
            End Select

            Exit For
          End If

          Exit For
        Next

        If _row_to_delete Then
          Call RemoveLevels(dg_levels)

          If dg_levels.NumRows > 0 Then
            For x As Integer = 0 To dg_levels.NumRows - 1
              dg_levels.IsSelected(x) = False
            Next x

            'Goes to the last row
            dg_levels.IsSelected(dg_levels.NumRows - 1) = True
            dg_levels_RowSelectedEvent(dg_levels.NumRows - 1)
            dg_levels.CurrentRow = dg_levels.NumRows - 1

          End If
        End If

      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Exit Sub
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

  End Sub ' RemoveProgressiveLevel

  ' PURPOSE: Remove row in the "uc_grid" component
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub RemoveLevels(ByVal Grid As GUI_Controls.uc_grid)

    Erase m_currencies_selected
    m_currencies_selected = Grid.SelectedRows

    If IsNothing(m_currencies_selected) Then
      Exit Sub
    End If

    For Each _selected_row As Integer In Grid.SelectedRows

      Grid.IsSelected(_selected_row) = False
      Grid.DeleteRowFast(_selected_row)

    Next

  End Sub ' RemoveLevels

  ' PURPOSE: This function indicate if percentage is equal or over 100%
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function PercentageComplete(ByVal Grid As GUI_Controls.uc_grid, ByVal FunctionMode As Integer) As Boolean
    Dim _result As Boolean
    Dim _count As Integer
    Dim _percentage As Decimal

    _count = 0
    _percentage = FormatNumber(0, 2)
    _result = True

    While _count < dg_levels.NumRows
      _percentage += GUI_ParseNumber(dg_levels.Cell(_count, GRID_COLUMN_PGL_CONTRIBUTION_PCT).Value)
      _count += 1
    End While

    Select Case FunctionMode
      Case FUNCTION_MODE.FROM_ADD_LEVEL
        _result = (_percentage >= FormatNumber(100, 2))
      Case FUNCTION_MODE.FROM_IS_SCREEN_DATA_OK
        _result = (_percentage <> FormatNumber(100, 2))
    End Select

    Return _result
  End Function ' PercentageComplete

  ' PURPOSE: This function indicate if percentage is between 0 and 100
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function PercentagesCorrect(ByVal Grid As GUI_Controls.uc_grid) As Boolean
    Dim _count As Integer
    Dim _percentage As Decimal

    _count = 0
    _percentage = FormatNumber(0, 2)

    While _count < dg_levels.NumRows
      _percentage = GUI_ParseNumber(dg_levels.Cell(_count, GRID_COLUMN_PGL_CONTRIBUTION_PCT).Value)
      If _percentage <= 0 Or _percentage > 100 Then
        Return False
      End If

      _count += 1
    End While

    Return True
  End Function ' PercentagesCorrect

  ' PURPOSE: check there's no levels name repeat
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if data is ok; False is data not ok
  '
  Private Function LevelsNameRepeat(ByVal Grid As GUI_Controls.uc_grid) As Boolean
    Dim _result As Boolean
    Dim _count As Integer
    Dim _count2 As Integer
    Dim _level_name As String
    Dim _level_name_ant As String

    _count = 0
    _count2 = 0
    _result = False

    While _count < dg_levels.NumRows And Not _result

      _level_name = dg_levels.Cell(_count, GRID_COLUMN_PGL_NAME).Value
      While _count2 < dg_levels.NumRows And Not _result
        If _count2 <> _count Then
          _level_name_ant = dg_levels.Cell(_count2, GRID_COLUMN_PGL_NAME).Value
          If _level_name_ant = _level_name Then
            _result = True
          End If
        End If
        _count2 += 1
      End While
      _count2 = 0
      _count += 1
    End While

    Return _result
  End Function ' LevelsNameRepeat

  ' PURPOSE: check there's no levels name blank
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean: True if data is ok; False is data not ok
  '
  Private Function LevelsNameBlank(ByVal Grid As GUI_Controls.uc_grid) As Boolean
    Dim _result As Boolean
    Dim _count As Integer
    Dim _level_name As String

    _count = 0
    _result = False

    While _count < dg_levels.NumRows And Not _result

      _level_name = dg_levels.Cell(_count, GRID_COLUMN_PGL_NAME).Value
      If _level_name = String.Empty Then
        _result = True
      End If

      _count += 1
    End While

    Return _result
  End Function ' LevelsNameBlank

  ' PURPOSE: This function indicate if some terminal are in use.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Function AnyTerminalUsed(ByVal TerminaslList As DataTable) As Boolean
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _str_terminals As String
    Dim _sb_prov_used As StringBuilder
    Dim _data As Object

    _sql_transaction = Nothing

    If TerminaslList Is Nothing Then
      Return False
    End If

    If TerminaslList.Rows.Count = 0 Then
      Return False
    End If

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If

      _str_terminals = ""
      For Each _row As DataRow In TerminaslList.Rows
        If Not String.IsNullOrEmpty(_str_terminals) Then
          _str_terminals += ", "
        End If
        _str_terminals += _row(0).ToString()
      Next

      _sb_prov_used = New StringBuilder()

      _sb_prov_used.AppendLine(" SELECT   TG_TERMINAL_ID                      ")
      _sb_prov_used.AppendLine("   FROM   TERMINAL_GROUPS                     ")
      _sb_prov_used.AppendLine("  WHERE   TG_ELEMENT_TYPE = @pElementType     ")
      _sb_prov_used.AppendLine("    AND   TG_ELEMENT_ID  <> @pElementId       ")
      _sb_prov_used.AppendLine("    AND   TG_TERMINAL_ID IN(                  ")
      _sb_prov_used.AppendLine(_str_terminals)
      _sb_prov_used.AppendLine("                            )                 ")

      _sql_cmd = New SqlCommand(_sb_prov_used.ToString())
      _sql_cmd.Connection = _sql_transaction.Connection
      _sql_cmd.Transaction = _sql_transaction

      _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = WSI.Common.EXPLOIT_ELEMENT_TYPE.PROGRESSIVE
      _sql_cmd.Parameters.Add("@pElementId", SqlDbType.Int).Value = Me.m_current_progressive.progressive_id

      _data = _sql_cmd.ExecuteScalar()

      If IsNothing(_data) Then
        Return False
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    Finally

      GUI_EndSQLTransaction(_sql_transaction, True)

    End Try

    Return True
  End Function ' AnyTerminalUsed


  Private Function GetTerminalsToCheck(ByVal XmlDbRead As String, ByVal XmlDbEdited As String, ByRef TerminalList As DataTable) As DataTable
    Dim _result As DataTable
    Dim _terminals_db_read As DataTable
    Dim _terminals_db_edited As DataTable
    Dim _new_terminal As Boolean

    _result = New DataTable()
    _terminals_db_read = New DataTable()
    _terminals_db_edited = New DataTable()

    'Call WSI.Common.GroupsService.GetTerminalListFromXml(XmlDbRead, _terminals_db_read, EXPLOIT_ELEMENT_TYPE.PROGRESSIVE)
    'Call WSI.Common.GroupsService.GetTerminalListFromXml(XmlDbEdited, _terminals_db_edited, EXPLOIT_ELEMENT_TYPE.PROGRESSIVE)
    Call WSI.Common.GroupsService.GetTerminalListFromXml2(XmlDbRead, XmlDbEdited, _terminals_db_read, _terminals_db_edited, EXPLOIT_ELEMENT_TYPE.PROGRESSIVE)

    For Each _col As DataColumn In _terminals_db_edited.Columns
      _result.Columns.Add(_col.ColumnName, _col.DataType)
    Next

    TerminalList = _terminals_db_edited

    For Each _row_db_edited As DataRow In _terminals_db_edited.Rows
      _new_terminal = True

      For Each _row_db_read As DataRow In _terminals_db_read.Rows
        If _row_db_edited(0) = _row_db_read(0) Then
          _new_terminal = False

          Exit For
        End If
      Next

      If _new_terminal Then
        Dim _current_column As Int16
        Dim _row_to_add As DataRow

        _current_column = 0
        _row_to_add = _result.Rows.Add()
        While _current_column < _terminals_db_edited.Columns.Count
          _row_to_add(_current_column) = _row_db_edited(_current_column)
          _current_column += 1
        End While

      End If
    Next

    Return _result
  End Function ' GetTerminalsToCheck

  ' PURPOSE: This function returns num of terminals assigned to a progressive
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProgressiveId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Number terminals participating in a progressive
  '
  Private Function GetNumTerminalsProgressive(ByVal ProgressiveId As Long) As Integer
    Dim _dt_terminals As DataTable

    _dt_terminals = New DataTable

    If WSI.Common.Progressives.GetProgressiveTerminals(ProgressiveId, _dt_terminals) Then
      Return _dt_terminals.Rows.Count
    Else
      Return 0
    End If
  End Function ' GetNumTerminalsProgressive

  ' PURPOSE: This function returns the last granted progressive (�ltimo progresivo otorgado)
  '
  '  PARAMS:
  '     - INPUT:
  '           - ProgressiveId
  '     - OUTPUT:
  '           - dataTable (DtJackPotGranted)
  '
  ' RETURNS:
  '     - True  : has find data
  '     - False : error or hasn't find data 
  '
  Private Function GetLastGrantedProgressive(ByVal ProgressiveId As Long, _
                                             ByRef DtJackPotGranted As DataTable) As Boolean
    Dim _str_sql As System.Text.StringBuilder
    Dim _adapter As SqlDataAdapter

    _str_sql = New System.Text.StringBuilder()

    Try

      ' Get Select and from
      _str_sql.AppendLine("SELECT	TOP (1)  HP_DATETIME                                ")
      _str_sql.AppendLine("              , HP_AMOUNT                                  ")
      _str_sql.AppendLine("				       , PV_NAME                                    ")
      _str_sql.AppendLine("              , TE_NAME                                    ")
      _str_sql.AppendLine("         FROM  HANDPAYS                                    ")
      _str_sql.AppendLine("   INNER JOIN	TERMINALS                                   ")
      _str_sql.AppendLine("           ON	HP_TERMINAL_ID = TE_TERMINAL_ID             ")
      _str_sql.AppendLine("   INNER JOIN	PROVIDERS                                   ")
      _str_sql.AppendLine("		        ON	PV_ID = TE_PROV_ID                          ")
      _str_sql.AppendLine("        WHERE  HP_PROGRESSIVE_ID = @pProgressiveId         ")
      _str_sql.AppendLine("          AND  (HP_LEVEL BETWEEN CONVERT(VARBINARY(8),1)   ")
      _str_sql.AppendLine("          AND  CONVERT(VARBINARY(8),20))                   ")
      _str_sql.AppendLine("          AND  HP_TYPE IN(@pHpType1, @pHpType2, @pHpType3) ")
      _str_sql.AppendLine("     ORDER BY	HP_DATETIME DESC                            ")

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.Int).Value = ProgressiveId
          _sql_cmd.Parameters.Add("@pHpType1", SqlDbType.Int).Value = WSI.Common.HANDPAY_TYPE.JACKPOT
          _sql_cmd.Parameters.Add("@pHpType2", SqlDbType.Int).Value = WSI.Common.HANDPAY_TYPE.MANUAL_JACKPOT
          _sql_cmd.Parameters.Add("@pHpType3", SqlDbType.Int).Value = WSI.Common.HANDPAY_TYPE.SPECIAL_PROGRESSIVE

          _adapter = New SqlDataAdapter(_sql_cmd)
          DtJackPotGranted = New DataTable()
          _adapter.Fill(DtJackPotGranted)
        End Using
      End Using

      If IsNothing(DtJackPotGranted) Then
        Return False
      End If

      If DtJackPotGranted.Rows.Count() < 1 Then
        Return False
      End If

    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    Finally

    End Try

    Return True

  End Function ' GetLastGrantedProgressive

  ' PURPOSE: This function indicate if some progressive name are in use.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Progressive Name
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True (exists); False (not exists)
  '
  Private Function CheckProgressiveNameExists(ByVal ProgressiveId As String, ByVal ProgressiveName As String) As Boolean
    Dim _sql_cmd As SqlCommand
    Dim _sql_transaction As System.Data.SqlClient.SqlTransaction
    Dim _str_sql As StringBuilder
    Dim _data As Object

    _sql_transaction = Nothing

    Try

      If Not GUI_BeginSQLTransaction(_sql_transaction) Then
        'TODO JMM 28-NOV-2012: Return an empty object
      End If

      _str_sql = New StringBuilder()

      _str_sql.AppendLine(" SELECT   PGS_PROGRESSIVE_ID                     ")
      _str_sql.AppendLine("   FROM   PROGRESSIVES                           ")
      _str_sql.AppendLine("  WHERE   PGS_NAME = @pProgressiveName           ")
      _str_sql.AppendLine("    AND   PGS_PROGRESSIVE_ID <> @pProgressiveId  ")

      _sql_cmd = New SqlCommand(_str_sql.ToString())
      _sql_cmd.Connection = _sql_transaction.Connection
      _sql_cmd.Transaction = _sql_transaction

      _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId
      _sql_cmd.Parameters.Add("@pProgressiveName", SqlDbType.VarChar, 100).Value = ProgressiveName

      _data = _sql_cmd.ExecuteScalar()

      If IsNothing(_data) Then
        Return False
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    Finally

      GUI_EndSQLTransaction(_sql_transaction, True)

    End Try

    Return True
  End Function ' AnyTerminalUsed

  ' PURPOSE: This function check if provisioned levels are in the grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Progressive Id
  '           - DgLevels (ucgrid)
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True (all OK); False (there are levels that aren't in the grid (ERROR) )
  '
  Private Function CheckProvisionedLevelsIntegrity(ByVal ProgressiveId As String, ByVal DgLevels As uc_grid) As Boolean
    Dim _str_sql As System.Text.StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _dt_levels As DataTable
    Dim _is_find As Boolean
    Dim _idx As Integer

    _str_sql = New System.Text.StringBuilder()
    _is_find = False
    _idx = 0

    Try

      ' Get differents levels who are provisioned from progressive 
      ' Only get top 10 for getting the best performance (revised by AC & FJ). 
      '   If they are more than 10 levels this check will not run (revised by AC & FJ).
      _str_sql.AppendLine("SELECT   DISTINCT           PPL_LEVEL_ID                                   ")
      _str_sql.AppendLine(" FROM                                                                      ")
      _str_sql.AppendLine(" (SELECT TOP 10             PPL.PPL_LEVEL_ID                               ")
      _str_sql.AppendLine("           FROM             PROGRESSIVES_PROVISIONS PP                     ")
      _str_sql.AppendLine("     INNER JOIN             PROGRESSIVES_LEVELS PL                         ")
      _str_sql.AppendLine("             ON             PP.PGP_PROGRESSIVE_ID = @plProgressiveId       ")
      _str_sql.AppendLine("            AND             PP.PGP_PROGRESSIVE_ID = PL.PGL_PROGRESSIVE_ID  ")
      _str_sql.AppendLine("     INNER JOIN             PROGRESSIVES_PROVISIONS_LEVELS PPL             ")
      _str_sql.AppendLine("             ON             PPL.PPL_PROVISION_ID = PP.PGP_PROVISION_ID     ")
      _str_sql.AppendLine("            AND             PL.PGL_LEVEL_ID = PPL.PPL_LEVEL_ID             ")
      _str_sql.AppendLine("       ORDER BY             PPL.PPL_PROVISION_ID                           ")
      _str_sql.AppendLine("  ) PLS ")

      Using _db_trx As New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@plProgressiveId", SqlDbType.Int).Value = ProgressiveId

          _adapter = New SqlDataAdapter(_sql_cmd)
          _dt_levels = New DataTable()
          _adapter.Fill(_dt_levels)
        End Using
      End Using

      'If there no levels all OK (exit)
      If IsNothing(_dt_levels) OrElse _
         _dt_levels.Rows.Count <= 0 Then

        Return True
      End If

      ' Check if levels who are provisioned are in the grid (by level_id)
      For Each row As DataRow In _dt_levels.Rows
        For _idx = 0 To DgLevels.NumRows - 1
          If DgLevels.Cell(_idx, GRID_COLUMN_PGL_LEVEL_ID).Value = row(0) Then
            _is_find = True
          End If
        Next

        If Not _is_find Then

          Return False
        End If
        _is_find = False
      Next

    Catch _ex As Exception
      Log.Exception(_ex)

      Return False
    Finally

    End Try

    Return True

  End Function ' AnyTerminalUsed



  ' PURPOSE: Add new row .
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
    Call AddProgressiveLevel()
  End Sub ' btn_add_ClickEvent

  ' PURPOSE: Delete row not added.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
    Call RemoveProgressiveLevel()
  End Sub ' btn_del_ClickEvent



  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function 'BoolToGridValue


  ' PURPOSE:  Cast from  uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValuetoBool(ByVal enabled As String) As Boolean
    If enabled = uc_grid.GRID_CHK_CHECKED Or enabled = uc_grid.GRID_CHK_CHECKED_DISABLED Then
      Return True
    Else
      Return False
    End If
  End Function 'BoolToGridValue

#End Region ' Private Functions

#Region " Public Functions "

#End Region ' Public Functions

#Region " Events "

  Private Sub dg_levels_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_levels.CellDataChangedEvent
    Dim _deleteable_column As Integer
    Dim _dbl_amount_for_checking As Double

    Try

      _deleteable_column = 0
      _dbl_amount_for_checking = 0

      'If we are editing amount column then...
      If Column = GRID_COLUMN_PGL_AMOUNT Then
        If dg_levels.Cell(Row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value <> String.Empty Then

          'Check Level status ant
          _deleteable_column = GUI_ParseNumber(dg_levels.Cell(Row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value)
          Select Case _deleteable_column
            Case CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_DELETEABLE, _
                 CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO, _
                 CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_ISNT_LAST_LEVEL_GRID

              'Check amount
              If dg_levels.Cell(Row, GRID_COLUMN_PGL_AMOUNT).Value <> String.Empty Then
                If GUI_FormatCurrency(GUI_ParseCurrency(dg_levels.Cell(Row, GRID_COLUMN_PGL_AMOUNT).Value)) = 0 Then

                  'amount is zero, so level is DELETEABLE
                  dg_levels.Cell(Row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_DELETEABLE
                Else

                  'amount is different than zero, so level is NOT DELETABLE
                  dg_levels.Cell(Row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_NOT_DELETEABLE_AMOUNT_DIFF_THAN_ZERO
                End If
              Else

                'if is string.empty, level is deletable.
                dg_levels.Cell(Row, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value = CLASS_PROGRESSIVE.ENUM_PROGRESSIVE_LEVELS.LEVEL_DELETEABLE
              End If
          End Select
        End If
      End If

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

  Private Sub dg_levels_RowSelectedEvent(ByVal SelectedRow As Integer) Handles dg_levels.RowSelectedEvent

    If SelectedRow >= 0 AndAlso _
       Not String.IsNullOrEmpty(dg_levels.Cell(SelectedRow, GRID_COLUMN_PGL_DELETEABLE_LEVEL).Value) AndAlso _
       dg_levels.CurrentRow = SelectedRow Then

      If (SelectedRow + 1 <> Me.dg_levels.NumRows AndAlso _
         Me.dg_levels.NumRows > 1) OrElse _
         dg_levels.SelectedRows Is Nothing OrElse _
         dg_levels.SelectedRows.Length <= 0 Then

        Me.btn_del.Enabled = False
      Else
        Me.btn_del.Enabled = Me.Permissions.Delete
      End If
    End If
  End Sub

  Private Sub btn_update_ClickEvent() Handles btn_update.ClickEvent
    Try
      With Me.dg_levels
        .Column(GRID_COLUMN_PGL_AMOUNT).Editable = True
        .Column(GRID_COLUMN_PGL_AMOUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_PGL_AMOUNT).EditionControl.EntryField.IsReadOnly = False
        .Column(GRID_COLUMN_PGL_AMOUNT).EditionControl.EntryField.SetFilter(FORMAT_MONEY, 12)
        .Column(GRID_COLUMN_PGL_AMOUNT).HighLightWhenSelected = False

        .Focus()
        .CurrentCol = GRID_COLUMN_PGL_AMOUNT
      End With

      Me.btn_update.Enabled = False

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try
  End Sub

#End Region ' Events  

End Class