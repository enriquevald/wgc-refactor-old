<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_automatic_promotions_reports
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_promo_type = New System.Windows.Forms.GroupBox
    Me.opt_promo_preassigned = New System.Windows.Forms.RadioButton
    Me.opt_promo_all = New System.Windows.Forms.RadioButton
    Me.opt_promo_system_period = New System.Windows.Forms.RadioButton
    Me.opt_promo_per_points = New System.Windows.Forms.RadioButton
    Me.opt_promo_system_auto = New System.Windows.Forms.RadioButton
    Me.opt_promo_manual = New System.Windows.Forms.RadioButton
    Me.uc_dsl = New GUI_Controls.uc_daily_session_selector
    Me.gb_order = New System.Windows.Forms.GroupBox
    Me.opt_order_prom_level_account = New System.Windows.Forms.RadioButton
    Me.opt_order_level_account_prom = New System.Windows.Forms.RadioButton
    Me.chk_view_data_account = New System.Windows.Forms.CheckBox
    Me.uc_account_sel = New GUI_Controls.uc_account_sel
    Me.ef_promo_name = New GUI_Controls.uc_entry_field
    Me.gb_status_promo = New System.Windows.Forms.GroupBox
    Me.opt_promo_status_active = New System.Windows.Forms.RadioButton
    Me.opt_promo_status_canceled = New System.Windows.Forms.RadioButton
    Me.opt_promo_status_not_awarded = New System.Windows.Forms.RadioButton
    Me.opt_promo_status_expired = New System.Windows.Forms.RadioButton
    Me.opt_promo_status_awarded = New System.Windows.Forms.RadioButton
    Me.chk_promo_status_not_played = New System.Windows.Forms.CheckBox
    Me.chk_credit_type_non_redeem = New System.Windows.Forms.CheckBox
    Me.chk_credit_type_points = New System.Windows.Forms.CheckBox
    Me.chk_credit_type_redeem = New System.Windows.Forms.CheckBox
    Me.gb_promo_credit_type = New System.Windows.Forms.GroupBox
    Me.cmb_categories = New GUI_Controls.uc_combo
    Me.chk_promo_with_cost = New System.Windows.Forms.CheckBox
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.lbl_msg_no_includes_auto = New System.Windows.Forms.Label
    Me.opt_promo_status_exausted = New System.Windows.Forms.RadioButton
    Me.opt_promo_status_redeemed = New System.Windows.Forms.RadioButton
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_promo_type.SuspendLayout()
    Me.gb_order.SuspendLayout()
    Me.gb_status_promo.SuspendLayout()
    Me.gb_promo_credit_type.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_msg_no_includes_auto)
    Me.panel_filter.Controls.Add(Me.cmb_categories)
    Me.panel_filter.Controls.Add(Me.GroupBox1)
    Me.panel_filter.Controls.Add(Me.gb_promo_credit_type)
    Me.panel_filter.Controls.Add(Me.gb_status_promo)
    Me.panel_filter.Controls.Add(Me.ef_promo_name)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Controls.Add(Me.gb_order)
    Me.panel_filter.Controls.Add(Me.gb_promo_type)
    Me.panel_filter.Controls.Add(Me.uc_dsl)
    Me.panel_filter.Size = New System.Drawing.Size(1197, 235)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dsl, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_promo_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_order, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_promo_name, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status_promo, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_promo_credit_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.GroupBox1, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_categories, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_msg_no_includes_auto, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 239)
    Me.panel_data.Size = New System.Drawing.Size(1197, 369)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1191, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1191, 4)
    '
    'gb_promo_type
    '
    Me.gb_promo_type.Controls.Add(Me.opt_promo_preassigned)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_all)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_system_period)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_per_points)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_system_auto)
    Me.gb_promo_type.Controls.Add(Me.opt_promo_manual)
    Me.gb_promo_type.Location = New System.Drawing.Point(6, 3)
    Me.gb_promo_type.Name = "gb_promo_type"
    Me.gb_promo_type.Size = New System.Drawing.Size(170, 187)
    Me.gb_promo_type.TabIndex = 0
    Me.gb_promo_type.TabStop = False
    Me.gb_promo_type.Text = "xTypePromo"
    '
    'opt_promo_preassigned
    '
    Me.opt_promo_preassigned.AutoSize = True
    Me.opt_promo_preassigned.Location = New System.Drawing.Point(12, 150)
    Me.opt_promo_preassigned.Name = "opt_promo_preassigned"
    Me.opt_promo_preassigned.Size = New System.Drawing.Size(101, 17)
    Me.opt_promo_preassigned.TabIndex = 5
    Me.opt_promo_preassigned.TabStop = True
    Me.opt_promo_preassigned.Text = "xPreassigned"
    Me.opt_promo_preassigned.UseVisualStyleBackColor = True
    '
    'opt_promo_all
    '
    Me.opt_promo_all.AutoSize = True
    Me.opt_promo_all.Location = New System.Drawing.Point(12, 21)
    Me.opt_promo_all.Name = "opt_promo_all"
    Me.opt_promo_all.Size = New System.Drawing.Size(46, 17)
    Me.opt_promo_all.TabIndex = 0
    Me.opt_promo_all.TabStop = True
    Me.opt_promo_all.Text = "xAll"
    Me.opt_promo_all.UseVisualStyleBackColor = True
    '
    'opt_promo_system_period
    '
    Me.opt_promo_system_period.AutoSize = True
    Me.opt_promo_system_period.Location = New System.Drawing.Point(12, 71)
    Me.opt_promo_system_period.Name = "opt_promo_system_period"
    Me.opt_promo_system_period.Size = New System.Drawing.Size(120, 17)
    Me.opt_promo_system_period.TabIndex = 2
    Me.opt_promo_system_period.TabStop = True
    Me.opt_promo_system_period.Text = "xSystemPeriodic"
    Me.opt_promo_system_period.UseVisualStyleBackColor = True
    '
    'opt_promo_per_points
    '
    Me.opt_promo_per_points.AutoSize = True
    Me.opt_promo_per_points.Location = New System.Drawing.Point(12, 122)
    Me.opt_promo_per_points.Name = "opt_promo_per_points"
    Me.opt_promo_per_points.Size = New System.Drawing.Size(89, 17)
    Me.opt_promo_per_points.TabIndex = 4
    Me.opt_promo_per_points.TabStop = True
    Me.opt_promo_per_points.Text = "xPer points"
    Me.opt_promo_per_points.UseVisualStyleBackColor = True
    '
    'opt_promo_system_auto
    '
    Me.opt_promo_system_auto.AutoSize = True
    Me.opt_promo_system_auto.Location = New System.Drawing.Point(12, 97)
    Me.opt_promo_system_auto.Name = "opt_promo_system_auto"
    Me.opt_promo_system_auto.Size = New System.Drawing.Size(101, 17)
    Me.opt_promo_system_auto.TabIndex = 3
    Me.opt_promo_system_auto.TabStop = True
    Me.opt_promo_system_auto.Text = "xSystemAuto"
    Me.opt_promo_system_auto.UseVisualStyleBackColor = True
    '
    'opt_promo_manual
    '
    Me.opt_promo_manual.AutoSize = True
    Me.opt_promo_manual.Location = New System.Drawing.Point(12, 47)
    Me.opt_promo_manual.Name = "opt_promo_manual"
    Me.opt_promo_manual.Size = New System.Drawing.Size(72, 17)
    Me.opt_promo_manual.TabIndex = 1
    Me.opt_promo_manual.TabStop = True
    Me.opt_promo_manual.Text = "xManual"
    Me.opt_promo_manual.UseVisualStyleBackColor = True
    '
    'uc_dsl
    '
    Me.uc_dsl.ClosingTime = 0
    Me.uc_dsl.ClosingTimeEnabled = True
    Me.uc_dsl.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.Location = New System.Drawing.Point(334, 3)
    Me.uc_dsl.Name = "uc_dsl"
    Me.uc_dsl.ShowBorder = True
    Me.uc_dsl.Size = New System.Drawing.Size(250, 81)
    Me.uc_dsl.TabIndex = 2
    Me.uc_dsl.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dsl.ToDateSelected = True
    '
    'gb_order
    '
    Me.gb_order.Controls.Add(Me.opt_order_prom_level_account)
    Me.gb_order.Controls.Add(Me.opt_order_level_account_prom)
    Me.gb_order.Location = New System.Drawing.Point(902, 105)
    Me.gb_order.Name = "gb_order"
    Me.gb_order.Size = New System.Drawing.Size(168, 69)
    Me.gb_order.TabIndex = 8
    Me.gb_order.TabStop = False
    Me.gb_order.Text = "xOrder"
    '
    'opt_order_prom_level_account
    '
    Me.opt_order_prom_level_account.AutoSize = True
    Me.opt_order_prom_level_account.Location = New System.Drawing.Point(20, 20)
    Me.opt_order_prom_level_account.Name = "opt_order_prom_level_account"
    Me.opt_order_prom_level_account.Size = New System.Drawing.Size(120, 17)
    Me.opt_order_prom_level_account.TabIndex = 0
    Me.opt_order_prom_level_account.TabStop = True
    Me.opt_order_prom_level_account.Text = "xPromotionLevel"
    Me.opt_order_prom_level_account.UseVisualStyleBackColor = True
    '
    'opt_order_level_account_prom
    '
    Me.opt_order_level_account_prom.AutoSize = True
    Me.opt_order_level_account_prom.Location = New System.Drawing.Point(20, 43)
    Me.opt_order_level_account_prom.Name = "opt_order_level_account_prom"
    Me.opt_order_level_account_prom.Size = New System.Drawing.Size(107, 17)
    Me.opt_order_level_account_prom.TabIndex = 1
    Me.opt_order_level_account_prom.TabStop = True
    Me.opt_order_level_account_prom.Text = "xLevelAccount"
    Me.opt_order_level_account_prom.UseVisualStyleBackColor = True
    '
    'chk_view_data_account
    '
    Me.chk_view_data_account.AutoSize = True
    Me.chk_view_data_account.Location = New System.Drawing.Point(16, 15)
    Me.chk_view_data_account.Name = "chk_view_data_account"
    Me.chk_view_data_account.Size = New System.Drawing.Size(97, 17)
    Me.chk_view_data_account.TabIndex = 3
    Me.chk_view_data_account.Text = "xHolderData"
    Me.chk_view_data_account.UseVisualStyleBackColor = True
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(588, 1)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 4
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(641, 143)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.PlaceHolder = Nothing
    Me.ef_promo_name.Size = New System.Drawing.Size(254, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 5
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 67
    Me.ef_promo_name.Value = ""
    Me.ef_promo_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_status_promo
    '
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_redeemed)
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_exausted)
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_active)
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_canceled)
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_not_awarded)
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_expired)
    Me.gb_status_promo.Controls.Add(Me.opt_promo_status_awarded)
    Me.gb_status_promo.Location = New System.Drawing.Point(182, 3)
    Me.gb_status_promo.Name = "gb_status_promo"
    Me.gb_status_promo.Size = New System.Drawing.Size(146, 187)
    Me.gb_status_promo.TabIndex = 1
    Me.gb_status_promo.TabStop = False
    Me.gb_status_promo.Text = "xStatusPromotion"
    '
    'opt_promo_status_active
    '
    Me.opt_promo_status_active.AutoSize = True
    Me.opt_promo_status_active.Location = New System.Drawing.Point(12, 19)
    Me.opt_promo_status_active.Name = "opt_promo_status_active"
    Me.opt_promo_status_active.Size = New System.Drawing.Size(67, 17)
    Me.opt_promo_status_active.TabIndex = 0
    Me.opt_promo_status_active.TabStop = True
    Me.opt_promo_status_active.Text = "xActive"
    Me.opt_promo_status_active.UseVisualStyleBackColor = True
    '
    'opt_promo_status_canceled
    '
    Me.opt_promo_status_canceled.AutoSize = True
    Me.opt_promo_status_canceled.Location = New System.Drawing.Point(12, 87)
    Me.opt_promo_status_canceled.Name = "opt_promo_status_canceled"
    Me.opt_promo_status_canceled.Size = New System.Drawing.Size(85, 17)
    Me.opt_promo_status_canceled.TabIndex = 3
    Me.opt_promo_status_canceled.TabStop = True
    Me.opt_promo_status_canceled.Text = "xCanceled"
    Me.opt_promo_status_canceled.UseVisualStyleBackColor = True
    '
    'opt_promo_status_not_awarded
    '
    Me.opt_promo_status_not_awarded.AutoSize = True
    Me.opt_promo_status_not_awarded.Location = New System.Drawing.Point(12, 110)
    Me.opt_promo_status_not_awarded.Name = "opt_promo_status_not_awarded"
    Me.opt_promo_status_not_awarded.Size = New System.Drawing.Size(102, 17)
    Me.opt_promo_status_not_awarded.TabIndex = 4
    Me.opt_promo_status_not_awarded.TabStop = True
    Me.opt_promo_status_not_awarded.Text = "XNotAwarded"
    Me.opt_promo_status_not_awarded.UseVisualStyleBackColor = True
    '
    'opt_promo_status_expired
    '
    Me.opt_promo_status_expired.AutoSize = True
    Me.opt_promo_status_expired.Location = New System.Drawing.Point(12, 64)
    Me.opt_promo_status_expired.Name = "opt_promo_status_expired"
    Me.opt_promo_status_expired.Size = New System.Drawing.Size(75, 17)
    Me.opt_promo_status_expired.TabIndex = 2
    Me.opt_promo_status_expired.TabStop = True
    Me.opt_promo_status_expired.Text = "xExpired"
    Me.opt_promo_status_expired.UseVisualStyleBackColor = True
    '
    'opt_promo_status_awarded
    '
    Me.opt_promo_status_awarded.AutoSize = True
    Me.opt_promo_status_awarded.Location = New System.Drawing.Point(12, 41)
    Me.opt_promo_status_awarded.Name = "opt_promo_status_awarded"
    Me.opt_promo_status_awarded.Size = New System.Drawing.Size(82, 17)
    Me.opt_promo_status_awarded.TabIndex = 1
    Me.opt_promo_status_awarded.TabStop = True
    Me.opt_promo_status_awarded.Text = "xAwarded"
    Me.opt_promo_status_awarded.UseVisualStyleBackColor = True
    '
    'chk_promo_status_not_played
    '
    Me.chk_promo_status_not_played.AutoSize = True
    Me.chk_promo_status_not_played.Location = New System.Drawing.Point(16, 38)
    Me.chk_promo_status_not_played.Name = "chk_promo_status_not_played"
    Me.chk_promo_status_not_played.Size = New System.Drawing.Size(90, 17)
    Me.chk_promo_status_not_played.TabIndex = 4
    Me.chk_promo_status_not_played.Text = "xNotPlayed"
    Me.chk_promo_status_not_played.UseVisualStyleBackColor = True
    '
    'chk_credit_type_non_redeem
    '
    Me.chk_credit_type_non_redeem.AutoSize = True
    Me.chk_credit_type_non_redeem.Location = New System.Drawing.Point(19, 21)
    Me.chk_credit_type_non_redeem.Name = "chk_credit_type_non_redeem"
    Me.chk_credit_type_non_redeem.Size = New System.Drawing.Size(130, 17)
    Me.chk_credit_type_non_redeem.TabIndex = 0
    Me.chk_credit_type_non_redeem.Text = "xNon Redeemable"
    Me.chk_credit_type_non_redeem.UseVisualStyleBackColor = True
    '
    'chk_credit_type_points
    '
    Me.chk_credit_type_points.AutoSize = True
    Me.chk_credit_type_points.Location = New System.Drawing.Point(19, 72)
    Me.chk_credit_type_points.Name = "chk_credit_type_points"
    Me.chk_credit_type_points.Size = New System.Drawing.Size(67, 17)
    Me.chk_credit_type_points.TabIndex = 2
    Me.chk_credit_type_points.Text = "xPoints"
    Me.chk_credit_type_points.UseVisualStyleBackColor = True
    '
    'chk_credit_type_redeem
    '
    Me.chk_credit_type_redeem.AutoSize = True
    Me.chk_credit_type_redeem.Location = New System.Drawing.Point(19, 47)
    Me.chk_credit_type_redeem.Name = "chk_credit_type_redeem"
    Me.chk_credit_type_redeem.Size = New System.Drawing.Size(111, 17)
    Me.chk_credit_type_redeem.TabIndex = 1
    Me.chk_credit_type_redeem.Text = "xRedeemeable"
    Me.chk_credit_type_redeem.UseVisualStyleBackColor = True
    '
    'gb_promo_credit_type
    '
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_points)
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_redeem)
    Me.gb_promo_credit_type.Controls.Add(Me.chk_credit_type_non_redeem)
    Me.gb_promo_credit_type.Location = New System.Drawing.Point(902, 3)
    Me.gb_promo_credit_type.Name = "gb_promo_credit_type"
    Me.gb_promo_credit_type.Size = New System.Drawing.Size(168, 101)
    Me.gb_promo_credit_type.TabIndex = 7
    Me.gb_promo_credit_type.TabStop = False
    Me.gb_promo_credit_type.Text = "xPromotionCreditType"
    '
    'cmb_categories
    '
    Me.cmb_categories.AllowUnlistedValues = False
    Me.cmb_categories.AutoCompleteMode = False
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(601, 173)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(295, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 6
    Me.cmb_categories.TextWidth = 109
    '
    'chk_promo_with_cost
    '
    Me.chk_promo_with_cost.AutoSize = True
    Me.chk_promo_with_cost.Location = New System.Drawing.Point(16, 61)
    Me.chk_promo_with_cost.Name = "chk_promo_with_cost"
    Me.chk_promo_with_cost.Size = New System.Drawing.Size(84, 17)
    Me.chk_promo_with_cost.TabIndex = 5
    Me.chk_promo_with_cost.Text = "xWithCost"
    Me.chk_promo_with_cost.UseVisualStyleBackColor = True
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.chk_promo_status_not_played)
    Me.GroupBox1.Controls.Add(Me.chk_view_data_account)
    Me.GroupBox1.Controls.Add(Me.chk_promo_with_cost)
    Me.GroupBox1.Location = New System.Drawing.Point(334, 86)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(250, 84)
    Me.GroupBox1.TabIndex = 3
    Me.GroupBox1.TabStop = False
    '
    'lbl_msg_no_includes_auto
    '
    Me.lbl_msg_no_includes_auto.AutoSize = True
    Me.lbl_msg_no_includes_auto.Location = New System.Drawing.Point(15, 207)
    Me.lbl_msg_no_includes_auto.Name = "lbl_msg_no_includes_auto"
    Me.lbl_msg_no_includes_auto.Size = New System.Drawing.Size(207, 13)
    Me.lbl_msg_no_includes_auto.TabIndex = 12
    Me.lbl_msg_no_includes_auto.Text = "xNo includes automatic promotions"
    '
    'opt_promo_status_exausted
    '
    Me.opt_promo_status_exausted.AutoSize = True
    Me.opt_promo_status_exausted.Location = New System.Drawing.Point(12, 133)
    Me.opt_promo_status_exausted.Name = "opt_promo_status_exausted"
    Me.opt_promo_status_exausted.Size = New System.Drawing.Size(84, 17)
    Me.opt_promo_status_exausted.TabIndex = 5
    Me.opt_promo_status_exausted.TabStop = True
    Me.opt_promo_status_exausted.Text = "xExausted"
    Me.opt_promo_status_exausted.UseVisualStyleBackColor = True
    '
    'opt_promo_status_redeemed
    '
    Me.opt_promo_status_redeemed.AutoSize = True
    Me.opt_promo_status_redeemed.Location = New System.Drawing.Point(12, 156)
    Me.opt_promo_status_redeemed.Name = "opt_promo_status_redeemed"
    Me.opt_promo_status_redeemed.Size = New System.Drawing.Size(93, 17)
    Me.opt_promo_status_redeemed.TabIndex = 6
    Me.opt_promo_status_redeemed.TabStop = True
    Me.opt_promo_status_redeemed.Text = "xRedeemed"
    Me.opt_promo_status_redeemed.UseVisualStyleBackColor = True
    '
    'frm_automatic_promotions_reports
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1205, 612)
    Me.Name = "frm_automatic_promotions_reports"
    Me.Text = "frm_automatic_promotions_reports"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_promo_type.ResumeLayout(False)
    Me.gb_promo_type.PerformLayout()
    Me.gb_order.ResumeLayout(False)
    Me.gb_order.PerformLayout()
    Me.gb_status_promo.ResumeLayout(False)
    Me.gb_status_promo.PerformLayout()
    Me.gb_promo_credit_type.ResumeLayout(False)
    Me.gb_promo_credit_type.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_promo_type As System.Windows.Forms.GroupBox
  Friend WithEvents uc_dsl As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_order As System.Windows.Forms.GroupBox
  Friend WithEvents chk_view_data_account As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents opt_promo_system_auto As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_manual As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_per_points As System.Windows.Forms.RadioButton
  Friend WithEvents gb_status_promo As System.Windows.Forms.GroupBox
  Friend WithEvents opt_promo_status_expired As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_status_awarded As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_status_not_awarded As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_system_period As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_status_canceled As System.Windows.Forms.RadioButton
  Friend WithEvents chk_promo_status_not_played As System.Windows.Forms.CheckBox
  Friend WithEvents opt_promo_status_active As System.Windows.Forms.RadioButton
  Friend WithEvents opt_order_prom_level_account As System.Windows.Forms.RadioButton
  Friend WithEvents opt_order_level_account_prom As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_all As System.Windows.Forms.RadioButton
  Friend WithEvents chk_credit_type_non_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit_type_points As System.Windows.Forms.CheckBox
  Friend WithEvents chk_credit_type_redeem As System.Windows.Forms.CheckBox
  Friend WithEvents gb_promo_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_promo_preassigned As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents chk_promo_with_cost As System.Windows.Forms.CheckBox
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_msg_no_includes_auto As System.Windows.Forms.Label
  Friend WithEvents opt_promo_status_redeemed As System.Windows.Forms.RadioButton
  Friend WithEvents opt_promo_status_exausted As System.Windows.Forms.RadioButton
End Class