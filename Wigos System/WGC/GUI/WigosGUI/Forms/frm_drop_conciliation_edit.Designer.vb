﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_drop_conciliation_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_open_date_value = New System.Windows.Forms.Label()
    Me.lbl_open_date = New System.Windows.Forms.Label()
    Me.lbl_table_session_value = New System.Windows.Forms.Label()
    Me.lbl_table_session = New System.Windows.Forms.Label()
    Me.ef_ticket = New GUI_Controls.uc_entry_field()
    Me.ef_total_tickets = New GUI_Controls.uc_entry_field()
    Me.btn_entry = New GUI_Controls.uc_button()
    Me.dg_entry_tickets = New GUI_Controls.uc_grid()
    Me.gb_gaming_table_session = New System.Windows.Forms.GroupBox()
    Me.ef_drop = New GUI_Controls.uc_entry_field()
    Me.ef_collected = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gb_gaming_table_session.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.ef_collected)
    Me.panel_data.Controls.Add(Me.ef_drop)
    Me.panel_data.Controls.Add(Me.gb_gaming_table_session)
    Me.panel_data.Controls.Add(Me.dg_entry_tickets)
    Me.panel_data.Controls.Add(Me.btn_entry)
    Me.panel_data.Controls.Add(Me.ef_total_tickets)
    Me.panel_data.Controls.Add(Me.ef_ticket)
    Me.panel_data.Location = New System.Drawing.Point(8, 4)
    Me.panel_data.Size = New System.Drawing.Size(921, 537)
    '
    'lbl_open_date_value
    '
    Me.lbl_open_date_value.Location = New System.Drawing.Point(127, 36)
    Me.lbl_open_date_value.Name = "lbl_open_date_value"
    Me.lbl_open_date_value.Size = New System.Drawing.Size(220, 13)
    Me.lbl_open_date_value.TabIndex = 3
    Me.lbl_open_date_value.Text = "xOpen_date_value"
    Me.lbl_open_date_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_open_date
    '
    Me.lbl_open_date.Location = New System.Drawing.Point(1, 36)
    Me.lbl_open_date.Name = "lbl_open_date"
    Me.lbl_open_date.Size = New System.Drawing.Size(120, 13)
    Me.lbl_open_date.TabIndex = 2
    Me.lbl_open_date.Text = "xOpen_date"
    Me.lbl_open_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_table_session_value
    '
    Me.lbl_table_session_value.Location = New System.Drawing.Point(127, 17)
    Me.lbl_table_session_value.Name = "lbl_table_session_value"
    Me.lbl_table_session_value.Size = New System.Drawing.Size(220, 13)
    Me.lbl_table_session_value.TabIndex = 1
    Me.lbl_table_session_value.Text = "xTable_session_value"
    Me.lbl_table_session_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_table_session
    '
    Me.lbl_table_session.Location = New System.Drawing.Point(1, 17)
    Me.lbl_table_session.Name = "lbl_table_session"
    Me.lbl_table_session.Size = New System.Drawing.Size(120, 13)
    Me.lbl_table_session.TabIndex = 0
    Me.lbl_table_session.Text = "xTable_session"
    Me.lbl_table_session.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_ticket
    '
    Me.ef_ticket.DoubleValue = 0.0R
    Me.ef_ticket.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_ticket.IntegerValue = 0
    Me.ef_ticket.IsReadOnly = False
    Me.ef_ticket.Location = New System.Drawing.Point(8, 70)
    Me.ef_ticket.Name = "ef_ticket"
    Me.ef_ticket.PlaceHolder = Nothing
    Me.ef_ticket.Size = New System.Drawing.Size(322, 25)
    Me.ef_ticket.SufixText = "Sufix Text"
    Me.ef_ticket.SufixTextVisible = True
    Me.ef_ticket.TabIndex = 5
    Me.ef_ticket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket.TextValue = ""
    Me.ef_ticket.TextWidth = 120
    Me.ef_ticket.Value = ""
    Me.ef_ticket.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_total_tickets
    '
    Me.ef_total_tickets.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.ef_total_tickets.DoubleValue = 0.0R
    Me.ef_total_tickets.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_total_tickets.IntegerValue = 0
    Me.ef_total_tickets.IsReadOnly = False
    Me.ef_total_tickets.Location = New System.Drawing.Point(80, 509)
    Me.ef_total_tickets.Name = "ef_total_tickets"
    Me.ef_total_tickets.PlaceHolder = Nothing
    Me.ef_total_tickets.Size = New System.Drawing.Size(250, 25)
    Me.ef_total_tickets.SufixText = "Sufix Text"
    Me.ef_total_tickets.SufixTextVisible = True
    Me.ef_total_tickets.TabIndex = 6
    Me.ef_total_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_total_tickets.TextValue = ""
    Me.ef_total_tickets.TextWidth = 120
    Me.ef_total_tickets.Value = ""
    Me.ef_total_tickets.ValueForeColor = System.Drawing.Color.Blue
    '
    'btn_entry
    '
    Me.btn_entry.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_entry.Location = New System.Drawing.Point(336, 74)
    Me.btn_entry.Name = "btn_entry"
    Me.btn_entry.Size = New System.Drawing.Size(63, 21)
    Me.btn_entry.TabIndex = 7
    Me.btn_entry.ToolTipped = False
    Me.btn_entry.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'dg_entry_tickets
    '
    Me.dg_entry_tickets.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.dg_entry_tickets.CurrentCol = -1
    Me.dg_entry_tickets.CurrentRow = -1
    Me.dg_entry_tickets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_entry_tickets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_entry_tickets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_entry_tickets.Location = New System.Drawing.Point(4, 103)
    Me.dg_entry_tickets.Name = "dg_entry_tickets"
    Me.dg_entry_tickets.PanelRightVisible = True
    Me.dg_entry_tickets.Redraw = True
    Me.dg_entry_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_entry_tickets.Size = New System.Drawing.Size(914, 400)
    Me.dg_entry_tickets.Sortable = False
    Me.dg_entry_tickets.SortAscending = True
    Me.dg_entry_tickets.SortByCol = 0
    Me.dg_entry_tickets.TabIndex = 8
    Me.dg_entry_tickets.ToolTipped = True
    Me.dg_entry_tickets.TopRow = -2
    Me.dg_entry_tickets.WordWrap = False
    '
    'gb_gaming_table_session
    '
    Me.gb_gaming_table_session.Controls.Add(Me.lbl_table_session_value)
    Me.gb_gaming_table_session.Controls.Add(Me.lbl_table_session)
    Me.gb_gaming_table_session.Controls.Add(Me.lbl_open_date)
    Me.gb_gaming_table_session.Controls.Add(Me.lbl_open_date_value)
    Me.gb_gaming_table_session.Location = New System.Drawing.Point(4, 4)
    Me.gb_gaming_table_session.Name = "gb_gaming_table_session"
    Me.gb_gaming_table_session.Size = New System.Drawing.Size(395, 60)
    Me.gb_gaming_table_session.TabIndex = 9
    Me.gb_gaming_table_session.TabStop = False
    Me.gb_gaming_table_session.Text = "xGaming table session"
    '
    'ef_drop
    '
    Me.ef_drop.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ef_drop.DoubleValue = 0.0R
    Me.ef_drop.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_drop.IntegerValue = 0
    Me.ef_drop.IsReadOnly = False
    Me.ef_drop.Location = New System.Drawing.Point(601, 509)
    Me.ef_drop.Name = "ef_drop"
    Me.ef_drop.PlaceHolder = Nothing
    Me.ef_drop.Size = New System.Drawing.Size(250, 25)
    Me.ef_drop.SufixText = "Sufix Text"
    Me.ef_drop.SufixTextVisible = True
    Me.ef_drop.TabIndex = 10
    Me.ef_drop.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_drop.TextValue = ""
    Me.ef_drop.TextWidth = 120
    Me.ef_drop.Value = ""
    Me.ef_drop.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_collected
    '
    Me.ef_collected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.ef_collected.DoubleValue = 0.0R
    Me.ef_collected.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_collected.IntegerValue = 0
    Me.ef_collected.IsReadOnly = False
    Me.ef_collected.Location = New System.Drawing.Point(345, 509)
    Me.ef_collected.Name = "ef_collected"
    Me.ef_collected.PlaceHolder = Nothing
    Me.ef_collected.Size = New System.Drawing.Size(250, 25)
    Me.ef_collected.SufixText = "Sufix Text"
    Me.ef_collected.SufixTextVisible = True
    Me.ef_collected.TabIndex = 11
    Me.ef_collected.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_collected.TextValue = ""
    Me.ef_collected.TextWidth = 120
    Me.ef_collected.Value = ""
    Me.ef_collected.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_drop_conciliation_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1028, 545)
    Me.MinimumSize = New System.Drawing.Size(1044, 350)
    Me.Name = "frm_drop_conciliation_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_drop_conciliation_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_gaming_table_session.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_total_tickets As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ticket As GUI_Controls.uc_entry_field
  Friend WithEvents dg_entry_tickets As GUI_Controls.uc_grid
  Friend WithEvents btn_entry As GUI_Controls.uc_button
  Friend WithEvents lbl_open_date_value As System.Windows.Forms.Label
  Friend WithEvents lbl_open_date As System.Windows.Forms.Label
  Friend WithEvents lbl_table_session_value As System.Windows.Forms.Label
  Friend WithEvents lbl_table_session As System.Windows.Forms.Label
  Friend WithEvents gb_gaming_table_session As System.Windows.Forms.GroupBox
  Friend WithEvents ef_drop As GUI_Controls.uc_entry_field
  Friend WithEvents ef_collected As GUI_Controls.uc_entry_field
End Class
