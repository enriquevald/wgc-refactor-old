﻿'-------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_bucket_sel
'   DESCRIPTION : Selection buckets
'        AUTHOR : Samuel González
' CREATION DATE : 04-DIC-2015
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-DIC-2015  SGB    Initial version. Backlog Item 7191
' 01-MAR-2016  ETP    Fixed BUG 8949: Needs to round or truncate values before add.
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls.frm_base_print
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.Buckets

Public Class frm_bucket_sel
  Inherits frm_base_sel

#Region " Structures "

#End Region ' Structures

#Region " Constants "

  Private Const GRID_COLUMNS As Integer = 9
  Private Const GRID_HEADER_ROWS As Integer = 2

  'DB Columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_BUCKET_NAME As Integer = 1
  Private Const SQL_COLUMN_ENABLED As Integer = 2
  Private Const SQL_COLUMN_VISIBLE As Integer = 3
  'Private Const SQL_COLUMN_ORDER_REPORT As Integer = 4
  Private Const SQL_COLUMN_ACUMULATED_BUCKET As Integer = 4
  Private Const SQL_COLUMN_BUCKET_TYPE = 5




  'Grid Columns
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_BUCKET_NAME As Integer = 2
  Private Const GRID_COLUMN_BUCKET_TYPE As Integer = 3

  Private Const GRID_COLUMN_ENABLED As Integer = 4
  'Private Const GRID_COLUMN_ORDER_REPORT As Integer = 4
  Private Const GRID_COLUMN_ACUMULATED_BUCKET As Integer = 5
  Private Const GRID_COLUMN_VISIBLE_CASHIER As Integer = 6
  Private Const GRID_COLUMN_VISIBLE_PROMOBOX As Integer = 7
  Private Const GRID_COLUMN_VISIBLE_LCD As Integer = 8

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_BUCKET_NAME As Integer = 5000
  Private Const GRID_WIDTH_BUCKET_TYPE As Integer = 5000

  Private Const GRID_WIDTH_ENABLED As Integer = 1200
  '  Private Const GRID_WIDTH_ORDER_REPORT As Integer = 2000
  Private Const GRID_WIDTH_ACUMULATED_BUCKET As Integer = 2000
  Private Const GRID_WIDTH_VISIBLE_CASHIER As Integer = 1200
  Private Const GRID_WIDTH_VISIBLE_PROMOBOX As Integer = 0 '1200
  Private Const GRID_WIDTH_VISIBLE_LCD As Integer = 0 '1200

#End Region ' Constants

#Region " Enums"

#End Region ' Enums

#Region " Members "

  Private m_print_datetime As Date

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_BUCKET_SEL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6967)

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_CONTROLS.GetString(569)

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Style Sheet
    Call GUI_StyleSheet()

    'Execute query to open form.
    Call GUI_ExecuteQuery()

  End Sub ' GUI_InitControls

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _result As Boolean

    _result = True

    With Me.Grid

      'Bucket id
      .Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)

      ''Bucket type
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_BUCKET_TYPE)) Then
        Select Case DbRow.Value(SQL_COLUMN_BUCKET_TYPE)

          Case BucketType.RedemptionPoints
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7028)

          Case BucketType.RankingLevelPoints
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7029)

          Case BucketType.Credit_NR
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7030)

          Case BucketType.Credit_RE
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7031)

          Case BucketType.Comp1_RedemptionPoints
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7032)

          Case BucketType.Comp2_Credit_NR
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7033)

          Case BucketType.Comp3_Credit_RE
            .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7034)
        End Select

      Else
        .Cell(RowIndex, GRID_COLUMN_BUCKET_TYPE).Value = String.Empty

      End If

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_BUCKET_TYPE)) Then
        .Cell(RowIndex, GRID_COLUMN_BUCKET_NAME).Value = DbRow.Value(SQL_COLUMN_BUCKET_NAME)
      End If

      'Enabled
      .Cell(RowIndex, GRID_COLUMN_ENABLED).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(DbRow.Value(SQL_COLUMN_ENABLED), 359, 360))

      'Visible CASHIER
      Dim VisibleDB As Long = DbRow.Value(SQL_COLUMN_VISIBLE)
      Dim VisibleCashier As Boolean = Buckets.ReadDeviceVisibility(VisibleDB, Buckets.BucketVisibility.Cashier)
      .Cell(RowIndex, GRID_COLUMN_VISIBLE_CASHIER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(VisibleCashier, 359, 360))

      'Visible PromoBOX

      Dim VisiblePromoBox As Boolean = Buckets.ReadDeviceVisibility(VisibleDB, Buckets.BucketVisibility.Promobox)
      .Cell(RowIndex, GRID_COLUMN_VISIBLE_PROMOBOX).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(VisiblePromoBox, 359, 360))

      'Visible LCD
      Dim VisibleLCD As Boolean = Buckets.ReadDeviceVisibility(VisibleDB, Buckets.BucketVisibility.LCD)
      .Cell(RowIndex, GRID_COLUMN_VISIBLE_LCD).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(IIf(VisibleLCD, 359, 360))

      'Order report
      '.Cell(RowIndex, GRID_COLUMN_ORDER_REPORT).Value = DbRow.Value(SQL_COLUMN_ORDER_REPORT)

      'Acumulated bucket
      If Not IsDBNull(SQL_COLUMN_BUCKET_TYPE) AndAlso GetBucketUnits(DbRow.Value(SQL_COLUMN_BUCKET_TYPE)) = BucketUnits.Money Then
        .Cell(RowIndex, GRID_COLUMN_ACUMULATED_BUCKET).Value = GUI_FormatCurrency(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_ACUMULATED_BUCKET)), 0, DbRow.Value(SQL_COLUMN_ACUMULATED_BUCKET)))
      Else
        .Cell(RowIndex, GRID_COLUMN_ACUMULATED_BUCKET).Value = GUI_FormatNumber(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_ACUMULATED_BUCKET)), 0, DbRow.Value(SQL_COLUMN_ACUMULATED_BUCKET)), 0)
      End If
    End With

    Return _result

  End Function ' GUI_SetupRow

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder
    Dim _str_point_buckets As String

    _str_point_buckets = CStr(Buckets.BucketId.Credit_NR) + ","
    _str_point_buckets += CStr(Buckets.BucketId.Credit_RE) + ","
    _str_point_buckets += CStr(Buckets.BucketId.Comp2_Credit_NR) + ","
    _str_point_buckets += CStr(Buckets.BucketId.Comp3_Credit_RE)

    _sb = New System.Text.StringBuilder()


    _sb.AppendLine("    Select BU_BUCKET_ID                                                            ")
    _sb.AppendLine("       , BU_NAME                                                                   ")
    _sb.AppendLine("       , BU_ENABLED                                                                ")
    _sb.AppendLine("       , BU_VISIBLE_FLAGS                                                          ")

    _sb.AppendLine("       , ( SELECT CASE BU_BUCKET_ID                                                ")
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.RedemptionPoints) + "         ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,0,1))                      ") ' Points: truncated
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.RankingLevelPoints) + "       ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,0,1))                      ") ' Points: truncated
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.Credit_NR) + "                ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,2,0))                      ") ' Credit: rounded
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.Credit_RE) + "                ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,2,0))                      ") ' Credit: rounded
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.Comp1_RedemptionPoints) + "   ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,0,1))                      ") ' Points: truncated
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.Comp2_Credit_NR) + "          ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,2,0))                      ") ' Credit: rounded
    _sb.AppendLine("                     WHEN  " + CStr(Buckets.BucketId.Comp3_Credit_RE) + "          ")
    _sb.AppendLine("	                          THEN SUM(ROUND(CB.CBU_VALUE,2,0))                      ") ' Credit: rounded
    _sb.AppendLine("				           ELSE                                                            ")
    _sb.AppendLine("						                0                                                      ") ' Unknown Bucket: 0.
    _sb.AppendLine("                   END                                                             ")
    _sb.AppendLine("            FROM   CUSTOMER_BUCKET CB                                              ")
    _sb.AppendLine("           WHERE   BU_BUCKET_ID = CBU_BUCKET_ID    )                               ")
    _sb.AppendLine("           AS CBU_VALUE                                                            ")

    _sb.AppendLine("       , BU_BUCKET_TYPE                                                            ")
    _sb.AppendLine("  FROM   BUCKETS                                                                   ")
    _sb.AppendLine(" WHERE   NOT ( BU_BUCKET_ID = " + CStr(Buckets.BucketId.RankingLevelPoints_Generated))
    _sb.AppendLine("               OR BU_BUCKET_ID = " + CStr(Buckets.BucketId.RankingLevelPoints_Discretional))
    _sb.AppendLine("             )")
    _sb.AppendLine("  ORDER BY BU_ORDER_ON_REPORTS                                                     ")




    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Set sql command.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SqlCommand
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _id_bucket As Int64
    Dim _frm_edit As frm_bucket_edit

    If Me.Grid.SelectedRows.Length < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    _idx_row = Me.Grid.SelectedRows(0)
    _id_bucket = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _frm_edit = New frm_bucket_edit
    Call _frm_edit.ShowEditItem(_id_bucket)
    _frm_edit = Nothing
    MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  'Protected Overrides Sub GUI_ExcelResultList()

  '  Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
  '  Dim _prev_cursor As Windows.Forms.Cursor
  '  Dim _report_layout As ExcelReportLayout
  '  Dim _sheet_layout As ExcelReportLayout.SheetLayout
  '  Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition

  '  _prev_cursor = Me.Cursor
  '  Cursor = Cursors.WaitCursor

  '  Try
  '    _print_data = New GUI_Reports.CLASS_PRINT_DATA()
  '    Call GUI_ReportFilter(_print_data)
  '    _report_layout = New ExcelReportLayout()

  '    With _report_layout
  '      .PrintData = _print_data
  '      .PrintDateTime = m_print_datetime
  '      _sheet_layout = New ExcelReportLayout.SheetLayout()
  '      _sheet_layout.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6968)

  '      _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
  '      _uc_grid_def.UcGrid = dg_bucket
  '      _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
  '      .ListOfSheets.Add(_sheet_layout)
  '    End With

  '    _report_layout.GUI_GenerateExcel()

  '  Finally
  '    Cursor = _prev_cursor
  '  End Try

  'End Sub ' GUI_ExcelResultList

#End Region ' Overrides

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _total_columns As Integer

    _total_columns = GRID_COLUMNS

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Id
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID

      ' Name bucket
      .Column(GRID_COLUMN_BUCKET_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6969)
      .Column(GRID_COLUMN_BUCKET_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      .Column(GRID_COLUMN_BUCKET_NAME).Width = GRID_WIDTH_BUCKET_NAME
      .Column(GRID_COLUMN_BUCKET_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Type bucket
      .Column(GRID_COLUMN_BUCKET_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6969)
      .Column(GRID_COLUMN_BUCKET_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6972)
      .Column(GRID_COLUMN_BUCKET_TYPE).Width = GRID_WIDTH_BUCKET_TYPE
      .Column(GRID_COLUMN_BUCKET_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Enabled
      .Column(GRID_COLUMN_ENABLED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6969)
      .Column(GRID_COLUMN_ENABLED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6973)
      .Column(GRID_COLUMN_ENABLED).Width = GRID_WIDTH_ENABLED
      .Column(GRID_COLUMN_ENABLED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      ' Total bucket
      .Column(GRID_COLUMN_ACUMULATED_BUCKET).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6969)
      .Column(GRID_COLUMN_ACUMULATED_BUCKET).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6975)
      .Column(GRID_COLUMN_ACUMULATED_BUCKET).Width = GRID_WIDTH_ACUMULATED_BUCKET
      .Column(GRID_COLUMN_ACUMULATED_BUCKET).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Order report
      '.Column(GRID_COLUMN_ORDER_REPORT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6971)
      '.Column(GRID_COLUMN_ORDER_REPORT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6974)
      '.Column(GRID_COLUMN_ORDER_REPORT).Width = GRID_WIDTH_ORDER_REPORT
      '.Column(GRID_COLUMN_ORDER_REPORT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Visible CASHIER
      .Column(GRID_COLUMN_VISIBLE_CASHIER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6976)
      .Column(GRID_COLUMN_VISIBLE_CASHIER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6977)
      .Column(GRID_COLUMN_VISIBLE_CASHIER).Width = GRID_WIDTH_VISIBLE_CASHIER
      .Column(GRID_COLUMN_VISIBLE_CASHIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Visible PromoBOX
      .Column(GRID_COLUMN_VISIBLE_PROMOBOX).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6976)
      .Column(GRID_COLUMN_VISIBLE_PROMOBOX).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6978)
      .Column(GRID_COLUMN_VISIBLE_PROMOBOX).Width = GRID_WIDTH_VISIBLE_PROMOBOX
      .Column(GRID_COLUMN_VISIBLE_PROMOBOX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Visible LCD
      .Column(GRID_COLUMN_VISIBLE_LCD).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6976)
      .Column(GRID_COLUMN_VISIBLE_LCD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6979)
      .Column(GRID_COLUMN_VISIBLE_LCD).Width = GRID_WIDTH_VISIBLE_LCD
      .Column(GRID_COLUMN_VISIBLE_LCD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With
  End Sub ' GUI_StyleSheet

#End Region ' Private Functions

#Region " Public functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public functions

#Region " Events "

#End Region ' Events

  Private Function GRID_COLUMN_ORDER_REPORT() As Integer
    Throw New NotImplementedException
  End Function

End Class