'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_draw_summary_sel
' DESCRIPTION:   Terminal draws summary report
' AUTHOR:        Xavier guirado
' CREATION DATE: 26-GEN-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ---------------------------------------------------------------------------------------------
' 16-JUL-2013  MMG    Initial version
' 02-AUG-2017  FJC    WIGOS-4148 Terminal Draw: summary screen does not show data
'------------------------------------------------------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports System.Text

Public Class frm_terminal_draw_summary
  Inherits GUI_Controls.frm_base_sel

#Region "Constants"

  Private Const FORM_DB_MIN_VERSION As Short = 149

  ' DB Columns
  Private Const SQL_COLUMN_DRAW_JOURNEY As Integer = 0
  Private Const SQL_COLUMN_DRAW_COUNT As Integer = 1
  Private Const SQL_COLUMN_REDIMIBLE_BET As Integer = 2
  Private Const SQL_COLUMN_REDIMIBLE_WON As Integer = 3
  Private Const SQL_COLUMN_NONREDIMIBLE_WON As Integer = 4
  Private Const SQL_COLUMN_SITE_ID As Integer = 5

  'Grid columns
  Private GRID_COLUMN_INDEX As Integer = 0
  Private GRID_COLUMN_SITE_ID As Integer = 1
  Private GRID_COLUMN_DRAW_JOURNEY As Integer = 2
  Private Const GRID_COLUMN_DRAW_COUNT As Integer = 3
  Private Const GRID_COLUMN_REDIMIBLE_BET As Integer = 4
  Private Const GRID_COLUMN_REDIMIBLE_WON As Integer = 5
  Private Const GRID_COLUMN_NONREDIMIBLE_WON As Integer = 6

  Private Const GRID_COLUMN_SELECTABLE As Integer = 7

  Private GRID_COLUMNS As Integer = 8
  Private GRID_HEADER_ROWS As Integer = 2

  ' Columns Width
  Private Const GRID_WIDTH_COLUMN_INDEX As Integer = 200
  Private Const GRID_WIDTH_SITE_ID As Integer = 900
  Private Const GRID_WIDTH_DRAW_JOURNEY As Integer = 1300
  Private Const GRID_WIDTH_DRAW_COUNT As Integer = 1400
  Private Const GRID_WIDTH_REDIMIBLE_BET As Integer = 1500
  Private Const GRID_WIDTH_REDIMIBLE_WON As Integer = 1300
  Private Const GRID_WIDTH_NONREDIMIBLE_WON As Integer = 1600

  ' Total/Subtotal titles Index
  Private IDX_TOTAL As Integer = GRID_COLUMN_SITE_ID
  Private IDX_SUBTOTAL As Integer = GRID_COLUMN_DRAW_JOURNEY

  'Enum for indicating when a row is selectable or not
  Private Enum SELECTABLE_ROW As Integer
    YES = 1
    NO = 0
  End Enum

#End Region 'Constants

#Region "Members"

  ' For grid subtotals  
  Private m_subtotal_draws_count As Integer
  Private m_subtotal_redimible_bet As Double
  Private m_subtotal_redimible_won As Double
  Private m_subtotal_nonredimible_won As Double
  Private m_previous_date As String
  Private m_previous_site As String

  ' For grid totalizators
  Dim m_total_draws As Double
  Dim m_total_redimible_bet As Double
  Dim m_total_redimible_won As Double
  Dim m_total_nonredimible_won As Double

  ' For report filters  
  Private m_date_from As String
  Private m_date_to As String
  Private m_site_id As String
  Private m_grouped_by As String

#End Region 'Members

#Region "OVERRIDES"

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TERMINAL_DRAW_SUMMARY
    Call GUI_SetMinDbVersion(FORM_DB_MIN_VERSION)

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId


  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    'Set form name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7849)

    'Set form Width (Only in CashDesk Draw Generator)
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
      Me.Width = 726

    End If

    ' Date time filter
    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(401))
    Me.uc_dsl.ClosingTimeEnabled = False

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False

    'Site filter
    Me.ef_site_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906)
    Me.ef_site_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 3, 0)

    '"Group by" filter
    Me.gb_group_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(781)
    Me.rb_opt_site_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2652)
    Me.rb_opt_day_site.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2653)

    'Display/hide controls depending of the GUI mode
    If Not (GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI) Then 'Not in Cashdesk Draws Generator GUI mode
      Me.ef_site_id.Visible = False
      Me.ef_site_id.TabStop = False
      Me.gb_group_by.Visible = False
      Me.rb_opt_day_site.TabStop = False
      Me.rb_opt_site_day.TabStop = False

    End If

    ' Set filter default values
    Call SetDefaultValues()

    ' Grid
    Me.Grid.IsSortable = False
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Initialize members and accumulators related to the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_BeforeFirstRow()
    'Inicialize previous_date
    m_previous_date = ""

    'Inicialize previous_site
    m_previous_site = ""

    'Inicialize Subtotals
    ResetSubTotals()

    'Inicialize Totals 
    m_total_draws = 0
    m_total_redimible_bet = 0
    m_total_redimible_won = 0
    m_total_nonredimible_won = 0

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer
    Dim _text_subtotal As String
    Dim _text_total As String

    If Me.Grid.NumRows <> 0 Then
      _idx_row = Me.Grid.NumRows - 1

      _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375) 'Subtotal:

      If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
        WriteTotal(IDX_SUBTOTAL, _
                    m_subtotal_draws_count, _
                    m_subtotal_redimible_bet, _
                    m_subtotal_redimible_won, _
                    m_subtotal_nonredimible_won, _
                    ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _
                    _text_subtotal)
      Else
        IDX_TOTAL = GRID_COLUMN_DRAW_JOURNEY

      End If
    End If


    _text_total = GLB_NLS_GUI_INVOICING.GetString(205) 'Total:

    WriteTotal(IDX_TOTAL, _
                  m_total_draws, _
                  m_total_redimible_bet, _
                  m_total_redimible_won, _
                  m_total_nonredimible_won, _
                  ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, _
                  _text_total)

  End Sub ' GUI_AfterLastRow


  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                             ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean


    'Site ID (Only in Cash desk draws Generator GUI mode)
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE_ID).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_SITE_ID), _
                                                                                       0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End If

    'Journey
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_JOURNEY).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DRAW_JOURNEY), _
                                                                                  ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    'Journey's draws count
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DRAW_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_DRAW_COUNT), _
                                                                              0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    'Redimible bet
    If Not ((DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET)) OrElse (DbRow.Value(SQL_COLUMN_REDIMIBLE_BET) = 0)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDIMIBLE_BET), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_BET).Value = ""

    End If

    'Redimible won
    If Not ((DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON)) OrElse (DbRow.Value(SQL_COLUMN_REDIMIBLE_WON) = 0)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_REDIMIBLE_WON), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_REDIMIBLE_WON).Value = ""

    End If

    'Non redimible won
    If Not ((DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON)) OrElse (DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON) = 0)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NONREDIMIBLE_WON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON), _
                                                                                    ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NONREDIMIBLE_WON).Value = ""

    End If

    'Mark row as selectable, because is not a subtotal nor total row
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SELECTABLE).Value = SELECTABLE_ROW.YES

    'Subtotals increment (only if Cashdesk Draws Generator mode)
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
      m_subtotal_draws_count += IIf(DbRow.IsNull(SQL_COLUMN_DRAW_COUNT), 0, DbRow.Value(SQL_COLUMN_DRAW_COUNT))
      m_subtotal_redimible_bet += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_BET))
      m_subtotal_redimible_won += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_WON))
      m_subtotal_nonredimible_won += IIf(DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON))

    End If

    'Totalizators increment
    m_total_draws += DbRow.Value(SQL_COLUMN_DRAW_COUNT)
    m_total_redimible_bet += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_BET), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_BET))
    m_total_redimible_won += IIf(DbRow.IsNull(SQL_COLUMN_REDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_REDIMIBLE_WON))
    m_total_nonredimible_won += IIf(DbRow.IsNull(SQL_COLUMN_NONREDIMIBLE_WON), 0, DbRow.Value(SQL_COLUMN_NONREDIMIBLE_WON))

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    Dim _base_date As String
    Dim _closing_time As String

    _str_sql = New StringBuilder()
    _closing_time = GetDefaultClosingTime().ToString("00")

    _base_date = "01/01/2007 " & _closing_time & ":00:00"

    _str_sql.AppendLine("SELECT       DATEADD(DAY, DATEDIFF (HOUR, '" & _base_date & "', CD_DRAW_DATETIME) / 24,'" & _base_date & "') ")
    _str_sql.AppendLine("           , COUNT(CD_DRAW_ID)                                                                               ")
    _str_sql.AppendLine("           , SUM(CD_RE_BET)                                                                                  ")
    _str_sql.AppendLine("           , SUM(CD_RE_WON)                                                                                  ")
    _str_sql.AppendLine("           , SUM(CD_NR_WON)                                                                                  ")

    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
      _str_sql.AppendLine("         , CD_SITE_ID                                                                                      ")

    End If

    _str_sql.AppendLine("       FROM  CASHDESK_DRAWS                                                                                  ")
    _str_sql.AppendLine(" INNER JOIN  TERMINALS                                                                                       ")
    _str_sql.AppendLine("         ON  TERMINALS.TE_TERMINAL_ID = CASHDESK_DRAWS.CD_TERMINAL                                           ")
    _str_sql.AppendLine("        AND  TERMINALS.TE_TERMINAL_TYPE = " & TerminalTypes.SAS_HOST)

    _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine(GetStrSqlGroupBy(_base_date))

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Check dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate.Date > Me.uc_dsl.ToDate.Date Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False

      End If

    End If

    'Check Site ID format
    If Me.ef_site_id.Visible And Not Me.ef_site_id.ValidateFormat Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , Me.ef_site_id.Text)
      Me.ef_site_id.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_ShowSelectedItem()
        ' Return

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '              and process counters (& totals rows)
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
      ProcessCounters(DbRow)

    End If

    Return True

  End Function ' GUI_CheckOutRowBeforeAdd

#End Region 'OVERRIDES

#Region "GUI Reports"

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), m_date_to)

    'Site ID 
    If Me.ef_site_id.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906), m_site_id)

    End If

    'Grouped by
    If Me.gb_group_by.Visible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(781), m_grouped_by)

    End If

    ' With
    PrintData.FilterHeaderWidth(1) = 1600
    PrintData.FilterValueWidth(1) = 2500

  End Sub ' GUI_ReportFilter


  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_site_id = ""

    'Date 
    If Me.uc_dsl.FromDateSelected Then
      m_date_from = GUI_FormatDate(New DateTime(Me.uc_dsl.FromDate.Year, Me.uc_dsl.FromDate.Month, Me.uc_dsl.FromDate.Day, Me.uc_dsl.ClosingTime, 0, 0), _
                    ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    End If

    If Me.uc_dsl.ToDateSelected Then
      m_date_to = GUI_FormatDate(New DateTime(Me.uc_dsl.ToDate.Year, Me.uc_dsl.ToDate.Month, Me.uc_dsl.ToDate.Day, Me.uc_dsl.ClosingTime, 0, 0), _
                  ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    End If

    If Me.ef_site_id.Visible And Me.gb_group_by.Visible Then
      'Site ID
      m_site_id = Me.ef_site_id.TextValue

      'Grouped by
      m_grouped_by = IIf(Me.rb_opt_site_day.Checked, _
                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(2652), _
                         GLB_NLS_GUI_PLAYER_TRACKING.GetString(2653))

    End If

  End Sub 'GUI_ReportUpdateFilters

#End Region 'GUI Reports

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION

    Me.MdiParent = MdiParent
    Call Me.Display(False)

  End Sub ' ShowForEdit

  Protected Overrides Sub GUI_ShowSelectedItem()

    Dim _idx_row As Int32
    Dim _frm_sel As frm_terminal_draw_report
    Dim _cell_date As DateTime
    Dim _cell_site_id As String

    'Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For

      End If

    Next

    If Not IsValidDataRow(_idx_row) Then
      Return

    End If

    _cell_date = GUI_ParseDateTime(Me.Grid.Cell(_idx_row, GRID_COLUMN_DRAW_JOURNEY).Value, _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    'If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
    '  _cell_site_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value

    'Else
    '  _cell_site_id = Nothing

    'End If
    _cell_site_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_SITE_ID).Value

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm_sel = New frm_terminal_draw_report

    Call _frm_sel.ShowSelectedItem(Me.MdiParent, _cell_date, _cell_site_id)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub 'GUI_ShowSelectedItem

#End Region 'Public Functions

#Region "Private Functions"

  ' PURPOSE: Checks whether the specified row contains data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '     - TRUE: selected row contains data
  '     - FALSE: selected row does not exist or contains no data

  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    ' Skip Totalizator row!!!
    If IdxRow < 0 Or IdxRow >= (Me.Grid.NumRows - 1) Then
      Return False

    End If

    If Me.Grid.Cell(IdxRow, GRID_COLUMN_SELECTABLE).Value = SELECTABLE_ROW.NO Then
      Return False

    End If

    Return True
  End Function ' SetDefaultValues

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _final_time As Date
    Dim _now As Date
    'Date filter
    _now = WSI.Common.WGDB.Now
    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _final_time > _now Then
      _final_time = _final_time.AddDays(-1)

    End If

    _final_time = _final_time.Date
    Me.uc_dsl.FromDate = New DateTime(_final_time.Year, _final_time.Month, 1, _closing_time, 0, 0)
    Me.uc_dsl.FromDateSelected = True
    Me.uc_dsl.ToDate = _final_time
    Me.uc_dsl.ToDateSelected = False
    Me.uc_dsl.ClosingTime = _closing_time

    '"Group by" filter
    Me.rb_opt_site_day.Checked = True

    'Site ID filter
    Me.ef_site_id.Value = ""

  End Sub ' SetDefaultValues

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Sub GUI_StyleSheet()

    If Me.rb_opt_site_day.Checked Then
      GRID_COLUMN_SITE_ID = 1
      GRID_COLUMN_DRAW_JOURNEY = 2
      IDX_TOTAL = GRID_COLUMN_SITE_ID
      IDX_SUBTOTAL = GRID_COLUMN_DRAW_JOURNEY

    Else
      GRID_COLUMN_DRAW_JOURNEY = 1
      GRID_COLUMN_SITE_ID = 2
      IDX_TOTAL = GRID_COLUMN_DRAW_JOURNEY
      IDX_SUBTOTAL = GRID_COLUMN_SITE_ID

    End If

    With Me.Grid

      'Grid creation
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index Id
      With .Column(GRID_COLUMN_INDEX)
        .Header(0).Text = ""
        .Header(1).Text = ""
        .Width = GRID_WIDTH_COLUMN_INDEX
        .HighLightWhenSelected = False
        .IsColumnPrintable = False
      End With

      'Site ID (Only visible in Draw Generator GUI Mode)
      With .Column(GRID_COLUMN_SITE_ID)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1906)
        .Width = IIf((GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI), GRID_WIDTH_SITE_ID, 0)
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Draw journey  
      With .Column(GRID_COLUMN_DRAW_JOURNEY)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(401)
        .Width = GRID_WIDTH_DRAW_JOURNEY
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      End With

      'Jouney's draws count  
      With .Column(GRID_COLUMN_DRAW_COUNT)
        .Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2569)
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2365)
        .Width = GRID_WIDTH_DRAW_COUNT
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Redimible bet
      With .Column(GRID_COLUMN_REDIMIBLE_BET)
        .Header(0).Text = ""
        .Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7929)
        .Width = GRID_WIDTH_REDIMIBLE_BET
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Redimible won
      With .Column(GRID_COLUMN_REDIMIBLE_WON)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(257)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
        .Width = GRID_WIDTH_REDIMIBLE_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Non redimible won
      With .Column(GRID_COLUMN_NONREDIMIBLE_WON)
        .Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(257)
        .Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(241)
        .Width = GRID_WIDTH_NONREDIMIBLE_WON
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With

      'Selectable
      With .Column(GRID_COLUMN_SELECTABLE)
        .Header(0).Text = ""
        .Header(1).Text = ""
        .Width = 0
        .Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End With


    End With

    Me.Grid.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Returns the sql query for Cashdesk Draws Generator Mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - String with the sql query
  '
  ' RETURNS:
  '     - None
  Private Function GetStrSqlGroupBy(ByVal DateIni As String) As String
    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New StringBuilder()
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
      If Me.rb_opt_site_day.Checked Then 'Group by site, day
        _str_sql.AppendLine("GROUP BY CD_SITE_ID, DATEADD(DAY, DATEDIFF (HOUR, '" & DateIni & "', CD_DRAW_DATETIME) / 24,'" & DateIni & "')")
        _str_sql.AppendLine("ORDER BY CD_SITE_ID, DATEADD(DAY, DATEDIFF (HOUR, '" & DateIni & "', CD_DRAW_DATETIME) / 24,'" & DateIni & "')")

      Else 'Group by day, site
        _str_sql.AppendLine("GROUP BY DATEADD(DAY, DATEDIFF (HOUR, '" & DateIni & "', CD_DRAW_DATETIME) / 24,'" & DateIni & "'), CD_SITE_ID")
        _str_sql.AppendLine("ORDER BY DATEADD(DAY, DATEDIFF (HOUR, '" & DateIni & "', CD_DRAW_DATETIME) / 24,'" & DateIni & "'), CD_SITE_ID")

      End If
    Else
      _str_sql.AppendLine("GROUP BY DATEADD(DAY, DATEDIFF (HOUR, '" & DateIni & "', CD_DRAW_DATETIME) / 24,'" & DateIni & "')")
      _str_sql.AppendLine("ORDER BY DATEADD(DAY, DATEDIFF (HOUR, '" & DateIni & "', CD_DRAW_DATETIME) / 24,'" & DateIni & "')")
    End If


    Return _str_sql.ToString()

  End Function


  ' PURPOSE: Write accumulated values on a new row of the Grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub WriteTotal(ByVal IdxColumn As Integer, ByVal DrawsCount As Integer, ByVal RedimibleBet As Double, ByVal RedimibleWon As Double, ByVal NonRedimibleWon As Double, ByVal Color As ControlColor.ENUM_GUI_COLOR, ByVal Title As String)

    Dim _idx_row As Integer

    _idx_row = Me.Grid.NumRows

    Me.Grid.AddRow()

    ' "Total" or "Subtotal" title
    Me.Grid.Cell(_idx_row, IdxColumn).Value = Title

    'Draws Count Subtotal or total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_DRAW_COUNT).Value = GUI_FormatNumber(DrawsCount.ToString(), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)

    ' Redimible Bet Subtotal or total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_REDIMIBLE_BET).Value = GUI_FormatCurrency(RedimibleBet, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Redimible Won Subtotal or total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_REDIMIBLE_WON).Value = GUI_FormatCurrency(RedimibleWon, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Non redimible Won Subtotal or total
    Me.Grid.Cell(_idx_row, GRID_COLUMN_NONREDIMIBLE_WON).Value = GUI_FormatCurrency(NonRedimibleWon, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    ' Row Color
    Me.Grid.Row(_idx_row).BackColor = GetColor(Color)

    'Mark as non selectable row, because is a total or subtotal row
    Me.Grid.Cell(_idx_row, GRID_COLUMN_SELECTABLE).Value = SELECTABLE_ROW.NO

  End Sub ' WriteTotal

  ' PURPOSE : Process total counters: Draw row of totals, update and reset total counters.
  '
  '  PARAMS :
  '     - INPUT : 
  '           - DbRow As CLASS_DB_ROW
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub ProcessCounters(ByVal DbRow As CLASS_DB_ROW)

    Dim _row_date As String
    Dim _row_site As String
    Dim _text_subtotal As String
    Dim _non_selectable As Boolean

    _non_selectable = False

    _row_date = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DRAW_JOURNEY), _
                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                            ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    _row_site = DbRow.Value(SQL_COLUMN_SITE_ID)

    If Me.rb_opt_day_site.Checked Then 'Grouped by day and site
      If (Not String.IsNullOrEmpty(m_previous_date)) And (m_previous_date <> _row_date) Then
        _non_selectable = True

      End If

    Else 'Grouped by site and day
      If (Not String.IsNullOrEmpty(m_previous_site)) And (m_previous_site <> _row_site) Then
        _non_selectable = True

      End If

    End If

    If _non_selectable Then
      _text_subtotal = GLB_NLS_GUI_INVOICING.GetString(375)
      WriteTotal(IDX_SUBTOTAL, m_subtotal_draws_count, m_subtotal_redimible_bet, m_subtotal_redimible_won, m_subtotal_nonredimible_won, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01, _text_subtotal)
      ResetSubTotals()

    End If

    m_previous_date = _row_date
    m_previous_site = _row_site

  End Sub ' ProcessCounters

  ' PURPOSE: Set value '0' on subtotals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResetSubTotals()
    m_subtotal_draws_count = 0
    m_subtotal_redimible_bet = 0
    m_subtotal_redimible_won = 0
    m_subtotal_nonredimible_won = 0

  End Sub 'ResetSubTotals

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Private Function GetSqlWhere() As String
    Dim _str_where As String
    Dim _where_added As Boolean

    _str_where = ""
    _where_added = False

    ' Filter Dates
    If Me.uc_dsl.FromDateSelected = True Then
      If _where_added = False Then
        _str_where = _str_where & " WHERE "
        _where_added = True
      Else
        _str_where = _str_where & " AND "
      End If
      ' DDM & DHA 04-MAR-2014: Draws grouped by ClosingTime
      _str_where = _str_where & "  (CD_DRAW_DATETIME >= " & GUI_FormatDateDB(Me.uc_dsl.FromDate.AddHours(uc_dsl.ClosingTime)) & ") "
    End If

    If Me.uc_dsl.ToDateSelected = True Then
      If _where_added = False Then
        _str_where = _str_where & " WHERE "
        _where_added = True
      Else
        _str_where = _str_where & " AND "
      End If
      ' DDM & DHA 04-MAR-2014: Draws grouped by ClosingTime
      _str_where = _str_where & "  (CD_DRAW_DATETIME < " & GUI_FormatDateDB(Me.uc_dsl.ToDate.AddHours(uc_dsl.ClosingTime)) & ") "
    End If

    'Filter by Site ID (Only in Cash desk draws Generator GUI mode  )
    If GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI And Not String.IsNullOrEmpty(Me.ef_site_id.Value) Then 'Cash desk draws Generator GUI mode      
      If _where_added = False Then
        _str_where = _str_where & " WHERE "
        _where_added = True
      Else
        _str_where = _str_where & " AND "
      End If

      _str_where = _str_where & " CD_SITE_ID= '" & Me.ef_site_id.Value & "'"

    End If

    Return _str_where
  End Function ' GetSqlWhere

#End Region 'Private Functions

#Region "Events"


#End Region 'Events

End Class